﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.IO;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;
using Riskmaster.Models;
using Riskmaster.Cache;


namespace Riskmaster.UI.Utilities.UI_Admin.Customization
{
    public partial class CustomizeRMXPortal : System.Web.UI.Page
    {
        //xmlDoc will have the customization XML from app_data folder.
        XmlDocument xmlDoc = new XmlDocument();
        //p_objXmlDocument will serve as the base xml for common portlet grid binding.
        XmlDocument p_objXmlDocument = new XmlDocument();
        //p_objXmlDocumentInd will serve as the base XML for Individual portlet grid binding.
        XmlDocument p_objXmlDocumentInd = new XmlDocument();
        string INDIVIDUALPORTLET = "//IndividualPortletsList";
        string COMMONPORTLET = "//AllUsersPortlets";
        string sSelectedportletsdata = string.Empty;
        XmlElement objLstRow = null;
        XmlElement objRowTxt = null;
        XmlElement objElement = null;
        int iPosition = 1;
        int iMaxIdInd = 0;
        XmlNode objNode = null;
        private string PageID = RMXResourceProvider.PageId("CustomizeRMXPortal.aspx");
        protected void Page_Load(object sender, EventArgs e)
        {
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, PageID, "CustomizeRMXPortalValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "CustomizeRMXPortalValidations", sValidationResources, true);

            save.Attributes.Add("onclick", "Javascript:return ValidateRMXPortalSave()");
            //string strSql = "SELECT CONTENT FROM CUSTOMIZE WHERE UPPER(FILENAME)='CUSTOMIZE_PORTALINFO'";
            PortalData oPortalData = new PortalData();
            oPortalData.Sql = "SELECT CONTENT FROM CUSTOMIZE WHERE UPPER(FILENAME)='CUSTOMIZE_PORTALINFO'";
            oPortalData.ClientId = Convert.ToInt32(AppHelper.ClientId);
            string content = AppHelper.GetResponse<string>("RMService/Portal/ContentInfo", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oPortalData); 


            xmlDoc.LoadXml(content);

            //xmlDoc.Load("C:/Customization.xml");
            //xmlDoc.Load(Server.MapPath("~/App_Data/Customization.xml"));
            if(!IsPostBack)
            {
                CommonPortletXMLForGrid(xmlDoc);
                IndividualPortletXMLForGrid(xmlDoc);

                // Rsolanki2: mits 23786: RMX portal setting should be saved to database instead of web server
                //strSql = "SELECT CONTENT FROM CUSTOMIZE WHERE UPPER(FILENAME)='CUSTOMIZE_HOMEPAGE'";
                oPortalData.Sql = "SELECT CONTENT FROM CUSTOMIZE WHERE UPPER(FILENAME)='CUSTOMIZE_HOMEPAGE'";
                oPortalData.ClientId = Convert.ToInt32(AppHelper.ClientId);
                string HomePage = AppHelper.GetResponse<string>("RMService/Portal/ContentInfo", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oPortalData); 
                
                xmlDoc.LoadXml(HomePage);

                //xmlDoc.Load(Server.MapPath("~/App_Data/HomePage.xml"));
                txtHomePage.Value = xmlDoc.SelectSingleNode("//HomePage").InnerText;
               
            } // if
            hdnURL.Value = txtHomePage.Value;
            #region Common Portlet Add,Edit and Tab order change functionality.
            if (CommonPortletsGrid_Action.Text == "add")
            {
                //New entries added in grid.
                XmlDocument objXMLDoc = new XmlDocument();
                XmlTextReader xmlReader = new XmlTextReader(new StringReader(CommonPortletXML.Text));
                objXMLDoc.Load(xmlReader);

                objElement = (XmlElement)objXMLDoc.SelectSingleNode("//AllUsersPortlets");
                if(objElement == null)
                {
                    string sPortletXML = 
                        "<PortletSettings><AllUsersPortlets>"
                        + objXMLDoc.SelectSingleNode("//CommonPortlets").InnerXml
                        + "</AllUsersPortlets></PortletSettings>";
                    objXMLDoc.LoadXml(sPortletXML);
                    objElement = (XmlElement)objXMLDoc.SelectSingleNode("//AllUsersPortlets");
                } // if

                objLstRow = objXMLDoc.CreateElement("option");
                
                xmlReader = new XmlTextReader(new StringReader(AddedControlValue.Text));

                 //if you already have an XmlDocument then use that, otherwise
                 //create one
                XmlDocument xmlDocument = new XmlDocument();
                XmlNode node = xmlDocument.ReadNode(xmlReader);
                node.SelectSingleNode("//SequenceId").InnerText = SequenceID.Text;
                node.SelectSingleNode("//RowID").InnerText = SequenceID.Text;
                iPosition++;
                AddedControlValue.Text = node.InnerXml;
                objLstRow.InnerXml = AddedControlValue.Text;
                objElement.AppendChild(objLstRow);
                CommonPortletsGrid_Action.Text = "";
                //objElement.AppendChild(node);
                p_objXmlDocument = objXMLDoc;
                CommonPortletXML.Text = objXMLDoc.InnerXml.ToString();
                // Add an Extra Node for new data grid binding.
                AddBlankRow(ref p_objXmlDocument,COMMONPORTLET);
            } // if

            if(CommonPortletsGrid_Action.Text == "edit")
            {
                //commonportlets grid editing
                XmlDocument objXMLDoc = new XmlDocument();
                objXMLDoc.LoadXml(CommonPortletXML.Text);
                //The edited row of the grid.

                p_objXmlDocument = objXMLDoc;
                CommonPortletXML.Text = objXMLDoc.InnerXml.ToString();
                // Add an Extra Node for new data grid binding.
                AddBlankRow(ref p_objXmlDocument,COMMONPORTLET);
            } // if
            else if(CommonPortletsGrid_Action.Text == "tab")
            {
                XmlDocument objXMLDoc = new XmlDocument();
                objXMLDoc.LoadXml(CommonPortletXML.Text);
                if (objXMLDoc.SelectSingleNode("//DocumentElement") != null)
                {
                    CommonPortletXML.Text = objXMLDoc.SelectSingleNode("//DocumentElement").InnerXml;
                    //commonportlets grid editing
                    string sPortletXML = string.Empty;
                    sPortletXML = "<PortletSettings><AllUsersPortlets><listhead><SequenceId>" + AppHelper.GetResourceValue(PageID, "gvHdrCommonSequenceID", "0") + "</SequenceId><Name>" + AppHelper.GetResourceValue(PageID, "gvHdrCommonNameofPortlet", "0") + "</Name><urlPortlet>" + AppHelper.GetResourceValue(PageID, "gvHdrCommonURLofPortlet", "0") + "</urlPortlet></listhead>"
                         + CommonPortletXML.Text
                         + "</AllUsersPortlets></PortletSettings>";
                    CommonPortletXML.Text = sPortletXML;

                    objXMLDoc.LoadXml(CommonPortletXML.Text);
                    //The edited row of the grid.
                    CommonPortletsGrid_Action.Text = "";
                }
                else if (objXMLDoc.SelectSingleNode("//CommonPortlets") != null)
                {
                    CommonPortletXML.Text = objXMLDoc.SelectSingleNode("//CommonPortlets").InnerXml;
                    //commonportlets grid editing
                    string sPortletXML = string.Empty;
                    sPortletXML = "<PortletSettings><AllUsersPortlets>"
                        + CommonPortletXML.Text
                        + "</AllUsersPortlets></PortletSettings>";
                    CommonPortletXML.Text = sPortletXML;

                    objXMLDoc.LoadXml(CommonPortletXML.Text);
                    //The edited row of the grid.
                    CommonPortletsGrid_Action.Text = "";
                } // else

                p_objXmlDocument = objXMLDoc;
                //// Add an Extra Node for new data grid binding.
                AddBlankRow(ref p_objXmlDocument,COMMONPORTLET);
                //CommonPortletXML.Text = objXMLDoc.InnerXml.ToString();
            } // if
            else if (CommonPortletsGrid_Action.Text == "delete")
            {
                int iCount = 1;
                XmlDocument objXMLDoc = new XmlDocument();
                objXMLDoc.LoadXml(CommonPortletXML.Text);
                string sPortletXML = string.Empty;
                sPortletXML = "<PortletSettings><AllUsersPortlets><listhead><SequenceId>" + AppHelper.GetResourceValue(PageID, "gvHdrCommonSequenceID", "0") + "</SequenceId><Name>" + AppHelper.GetResourceValue(PageID, "gvHdrCommonNameofPortlet", "0") + "</Name><urlPortlet>" + AppHelper.GetResourceValue(PageID, "gvHdrCommonURLofPortlet", "0") + "</urlPortlet></listhead>";
                objElement = (XmlElement)objXMLDoc.SelectSingleNode("//AllUsersPortlets");
                if (objElement == null)
                {
                    string sPortletXMLBeforeDelete = 
                        "<PortletSettings><AllUsersPortlets>"
                        + objXMLDoc.SelectSingleNode("//CommonPortlets").InnerXml
                        + "</AllUsersPortlets></PortletSettings>";
                    objXMLDoc.LoadXml(sPortletXMLBeforeDelete);
                } // if

                foreach (XmlNode node in objXMLDoc.SelectNodes("/PortletSettings/AllUsersPortlets/option/SequenceId"))
                {
                    if(node.InnerText != CommonPortletsSelectedId.Text)
                    {
                        sPortletXML += node.ParentNode.OuterXml.ToString();
                    } // if
                }
                sPortletXML += "</AllUsersPortlets></PortletSettings>";
                CommonPortletXML.Text = sPortletXML;
                objXMLDoc.LoadXml(CommonPortletXML.Text);
                foreach (XmlNode node in objXMLDoc.SelectNodes("/PortletSettings/AllUsersPortlets/option/SequenceId"))
                {
                    node.InnerText = iCount.ToString();
                    node.NextSibling.InnerText = iCount.ToString();
                    iCount++;
                }
                p_objXmlDocument = objXMLDoc;
                CommonPortletXML.Text = objXMLDoc.InnerXml.ToString();
                AddBlankRow(ref p_objXmlDocument, COMMONPORTLET);
                CommonPortletsGrid_Action.Text = "";
            }
            //Need to check if p_objXmlDocument is null then need to create p_objXmlDocument from CommonPortletXML.Text value.
            if(p_objXmlDocument == null || p_objXmlDocument.InnerXml == "")
            {
                XmlDocument objXMLDoc = new XmlDocument();

                objXMLDoc.LoadXml(CommonPortletXML.Text);
                p_objXmlDocument = objXMLDoc;
                AddBlankRow(ref p_objXmlDocument, COMMONPORTLET);
            } // if

            if(CommonPortletsGrid_Action.Text != "save")
            {
                CommonPortletsGrid.BindData(p_objXmlDocument);
                txtCommonPortletGenXML.Text = CommonPortletsGrid.GenerateXml().OuterXml; 
            } // if

            #endregion

            #region Individual Portlet Add,Edit and Tab order change functionality.

            if (IndividualPortletsGrid_Action.Text == "add")
            {
                //New entries added in grid.
                XmlDocument objXMLDoc = new XmlDocument();
                XmlTextReader xmlReader = new XmlTextReader(new StringReader(IndividualPortletXML.Text));
                objXMLDoc.Load(xmlReader);

                objElement = (XmlElement)objXMLDoc.SelectSingleNode("//IndividualPortletsList");
                objLstRow = objXMLDoc.CreateElement("option");

                xmlReader = new XmlTextReader(new StringReader(AddedControlValue.Text));

                //if you already have an XmlDocument then use that, otherwise
                //create one
                XmlDocument xmlDocument = new XmlDocument();
                XmlNode node = xmlDocument.ReadNode(xmlReader);
                node.SelectSingleNode("//SequenceId").InnerText = SequenceID.Text;
                node.SelectSingleNode("//RowID").InnerText = txtMaxId.Text;
                txtMaxId.Text = (int.Parse(txtMaxId.Text) + 1).ToString();
                iPosition++;
                AddedControlValue.Text = node.InnerXml;
                objLstRow.InnerXml = AddedControlValue.Text;
                objElement.AppendChild(objLstRow);
                IndividualPortletsGrid_Action.Text = "";
                //objElement.AppendChild(node);
                p_objXmlDocumentInd = objXMLDoc;
                IndividualPortletXML.Text = objXMLDoc.InnerXml.ToString();
                // Add an Extra Node for new data grid binding.
                AddBlankRow(ref p_objXmlDocumentInd, INDIVIDUALPORTLET);
            }
            else
                //rsolanki2 : mits 24221 : edit portlet functionality issue
            if (IndividualPortletsGrid_Action.Text == "edit")
            {
                //New entries added in grid.
                XmlDocument objXMLDoc = new XmlDocument();
                XmlTextReader xmlReader = new XmlTextReader(new StringReader(IndividualPortletXML.Text));
                objXMLDoc.Load(xmlReader);

                //objElement = (XmlElement)objXMLDoc.SelectSingleNode("//IndividualPortletsList");
                //objLstRow = objXMLDoc.CreateElement("option");

                xmlReader = new XmlTextReader(new StringReader(AddedControlValue.Text));

                //if you already have an XmlDocument then use that, otherwise
                //create one
                XmlDocument xmlDocument = new XmlDocument();
                XmlNode node = xmlDocument.ReadNode(xmlReader);
                //node.SelectSingleNode("//SequenceId").InnerText = SequenceID.Text;
                //node.SelectSingleNode("//RowID").InnerText = txtMaxId.Text;
                //txtMaxId.Text = (int.Parse(txtMaxId.Text) + 1).ToString();
                //iPosition++;
                AddedControlValue.Text = node.InnerXml;

                objLstRow = (XmlElement)objXMLDoc.SelectSingleNode("//option[./SequenceId=" 
                    //+ node.SelectSingleNode("//SequenceId").InnerText 
                    + SequenceID.Text
                    + "]");

                //objLstRow.InnerXml = node.InnerText; //SelectSingleNode("//option")
                objLstRow.InnerXml = AddedControlValue.Text; //SelectSingleNode("//option")
                //objElement.AppendChild(objLstRow);
                IndividualPortletsGrid_Action.Text = "";
                //objElement.AppendChild(node);
                p_objXmlDocumentInd = objXMLDoc;
                IndividualPortletXML.Text = objXMLDoc.InnerXml.ToString();
                // Add an Extra Node for new data grid binding.
                AddBlankRow(ref p_objXmlDocumentInd, INDIVIDUALPORTLET);
            }
            else
            if (IndividualPortletsGrid_Action.Text == "delete")
            {
                int iCount = 1;
                XmlDocument objXMLDoc = new XmlDocument();
                objXMLDoc.LoadXml(IndividualPortletXML.Text);
                string sPortletXML = string.Empty;
                sPortletXML = "<PortletSettings><IndividualPortletsList><listhead><SequenceId>" + AppHelper.GetResourceValue(PageID, "gvHdrCommonSequenceID", "0") + "</SequenceId><Name>" + AppHelper.GetResourceValue(PageID, "gvHdrCommonNameofPortlet", "0") + "</Name><urlPortlet>" + AppHelper.GetResourceValue(PageID, "gvHdrCommonURLofPortlet", "0") + "</urlPortlet></listhead>";
                foreach (XmlNode node in objXMLDoc.SelectNodes("/PortletSettings/IndividualPortletsList/option/RowID"))
                {
                    if (node.InnerText != IndividualPortletsSelectedId.Text)
                    {
                        sPortletXML += node.ParentNode.OuterXml.ToString();
                    } // if
                }
                sPortletXML += "</IndividualPortletsList></PortletSettings>";
                IndividualPortletXML.Text = sPortletXML;
                objXMLDoc.LoadXml(IndividualPortletXML.Text);
                foreach (XmlNode node in objXMLDoc.SelectNodes("/PortletSettings/IndividualPortletsList/option/SequenceId"))
                {
                    node.InnerText = iCount.ToString();
                    //node.NextSibling.InnerText = iCount.ToString();
                    iCount++;
                }
                p_objXmlDocumentInd = objXMLDoc;
                AddBlankRow(ref p_objXmlDocumentInd, INDIVIDUALPORTLET);
                IndividualPortletsGrid_Action.Text = "";
            }
            if (p_objXmlDocumentInd == null || p_objXmlDocumentInd.InnerXml == "")
            {
                XmlDocument objXMLDoc = new XmlDocument();

                objXMLDoc.LoadXml(IndividualPortletXML.Text);
                p_objXmlDocumentInd = objXMLDoc;
                AddBlankRow(ref p_objXmlDocumentInd, INDIVIDUALPORTLET);
            } // if

            if(IndividualPortletsGrid_Action.Text != "save")
            {
                IndividualPortletsGrid.BindData(p_objXmlDocumentInd);
                txtIndPortletGenXML.Text = IndividualPortletsGrid.GenerateXml().OuterXml; 
            } // if

            #endregion // Individual Portlet Add,Edit and Tab order change functionality.

            #region Filling hdnselectedportletsdata.
            // Rsolanki2: mits 23786: RMX portal setting should be saved to database instead of web server
            //strSql = "SELECT CONTENT FROM CUSTOMIZE WHERE UPPER(FILENAME)='CUSTOMIZE_PORTALINFO'";
            oPortalData.Sql = "SELECT CONTENT FROM CUSTOMIZE WHERE UPPER(FILENAME)='CUSTOMIZE_PORTALINFO'";
            oPortalData.ClientId = Convert.ToInt32(AppHelper.ClientId);
            content = AppHelper.GetResponse<string>("RMService/Portal/ContentInfo", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oPortalData);
            xmlDoc.LoadXml(content);
            foreach(XmlNode objNodeIndividualPortletsInfo in xmlDoc.SelectNodes("/PortalSettings/IndividualPortletsInfo/PortletInfo"))
            {
                sSelectedportletsdata += "#" + objNodeIndividualPortletsInfo.Attributes["id"].Value.ToString() + "$";
                //foreach(XmlNode objNodePortlets in xmlDoc.SelectNodes("/PortalSettings/Portlets/id["+ objNodeIndividualPortletsInfo.Attributes["id"].Value + "]"))
                foreach (XmlNode objNodePortlets in xmlDoc.SelectNodes("/PortalSettings/Portlets/id"))
                {
                    if(objNodePortlets.InnerText.ToString() == objNodeIndividualPortletsInfo.Attributes["id"].Value.ToString())
                    {
                        sSelectedportletsdata += "," + objNodePortlets.ParentNode.Attributes["LoginName"].Value.ToString() ;
                    } // if
                } // foreach
                sSelectedportletsdata += ",$" + objNodeIndividualPortletsInfo.Attributes["id"].Value.ToString() + "#";
            } // foreach
            hdnselectedportletsdata.Value = sSelectedportletsdata.ToString();
            xmlDoc = null;
            #endregion // Filling hdnselectedportletsdata.

        }

        /// <summary>
        /// This function is to add a default row in XML which is required for user data grid binding.
        /// </summary>
        /// <param name="p_objXmlDocument"></param>
        public void AddBlankRow(ref XmlDocument p_objXmlDocument,string sPortletType)
        {
            objElement = (XmlElement)p_objXmlDocument.SelectSingleNode(sPortletType);
            objLstRow = p_objXmlDocument.CreateElement("option");
            objRowTxt = p_objXmlDocument.CreateElement("SequenceId");
            objLstRow.AppendChild(objRowTxt);
            objRowTxt = p_objXmlDocument.CreateElement("RowID");
            objLstRow.AppendChild(objRowTxt);
            objRowTxt = p_objXmlDocument.CreateElement("Name");
            objLstRow.AppendChild(objRowTxt);
            objRowTxt = p_objXmlDocument.CreateElement("urlPortlet");
            objLstRow.AppendChild(objRowTxt);
            objElement.AppendChild(objLstRow);
        }

        protected void save_Click(object sender, ImageClickEventArgs e)
        {
            XmlDocument objXMLDoc = new XmlDocument();
            XmlDocument objCustomizationXML = new XmlDocument();
            XmlDocument objTempXML = new XmlDocument();
            XmlDocument objGridXML = new XmlDocument();
            try
            {
                #region Saving Common Portlet Grid Data to Customization XML.

                objGridXML.LoadXml(txtCommonPortletGenXML.Text);
                objLstRow = objCustomizationXML.CreateElement("option");
                objCustomizationXML.AppendChild(objLstRow);
                objElement = (XmlElement)objCustomizationXML.SelectSingleNode("//option");
                //string strSql = "SELECT CONTENT FROM CUSTOMIZE WHERE UPPER(FILENAME)='CUSTOMIZE_PORTALINFO'";

                PortalData oPortalData = new PortalData();
                oPortalData.Sql = "SELECT CONTENT FROM CUSTOMIZE WHERE UPPER(FILENAME)='CUSTOMIZE_PORTALINFO'";
                oPortalData.ClientId = Convert.ToInt32(AppHelper.ClientId);
                string content = AppHelper.GetResponse<string>("RMService/Portal/ContentInfo", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oPortalData); 


                objXMLDoc.LoadXml(content);


                //objXMLDoc.Load("C:/Customization.xml");
                //objXMLDoc.Load(Server.MapPath("~/App_Data/Customization.xml"));
                foreach (XmlNode objNode in objGridXML.SelectNodes("/CommonPortlets/option"))
                {
                    objLstRow = objCustomizationXML.CreateElement("Portlet");
                    objTempXML.LoadXml(objNode.OuterXml);
                    if (objTempXML.SelectSingleNode("//urlPortlet").InnerText == AppHelper.GetResourceValue(PageID, "gvUrlEditingLocked", "0"))
                    {
                        objLstRow.InnerText = objXMLDoc.SelectSingleNode("/PortalSettings/AllUsersPortlets/Portlet[@lockURLEditing='true']").InnerText;
                        objLstRow.SetAttribute("name", objTempXML.SelectSingleNode("//Name").InnerText);
                        objLstRow.SetAttribute("MainRMXPortlet", "true");
                        objLstRow.SetAttribute("lockURLEditing", "true");
                        objLstRow.SetAttribute("RMXTabPreLoginRequired", "false");
                    }
                    else
                    {
                        objLstRow.InnerText = objTempXML.SelectSingleNode("//urlPortlet").InnerText;
                        objLstRow.SetAttribute("name", objTempXML.SelectSingleNode("//Name").InnerText);
                        objLstRow.SetAttribute("MainRMXPortlet", "false");
                        objLstRow.SetAttribute("lockURLEditing", "false");
                        objLstRow.SetAttribute("RMXTabPreLoginRequired", "false");
                    } // else
                    objElement.AppendChild(objLstRow);
                } // foreach

                //Changing the underline Customization xml with the changed xml.
                objXMLDoc.SelectSingleNode("/PortalSettings/AllUsersPortlets").InnerXml = objCustomizationXML.SelectSingleNode("//option").InnerXml;
                //PortalClient.ContentUpdate(objXMLDoc.InnerXml.ToString(), "CUSTOMIZE_PORTALINFO");

                oPortalData.Content = objXMLDoc.InnerXml.ToString();
                oPortalData.FileName = "CUSTOMIZE_PORTALINFO";
                oPortalData.Token = AppHelper.Token;
                AppHelper.GetResponse("RMService/Portal/Content", AppHelper.HttpVerb.PUT, AppHelper.APPLICATION_JSON, oPortalData); 

                //objXMLDoc.Save("C:/Customization.xml");
                //objXMLDoc.Save(Server.MapPath("~/App_Data/Customization.xml"));
                objXMLDoc = null;
                objCustomizationXML = null;
                objTempXML = null;
                objGridXML = null;

                # endregion

                #region Saving Individual Portlet Grid Data to Customization XML.

                objXMLDoc = new XmlDocument();
                objCustomizationXML = new XmlDocument();
                objTempXML = new XmlDocument();
                objGridXML = new XmlDocument();
                objGridXML.LoadXml(txtIndPortletGenXML.Text);
                objLstRow = objCustomizationXML.CreateElement("option");
                objCustomizationXML.AppendChild(objLstRow);
                objElement = (XmlElement)objCustomizationXML.SelectSingleNode("//option");
                // Rsolanki2: mits 23786: RMX portal setting should be saved to database instead of web server
                //strSql = "SELECT CONTENT FROM CUSTOMIZE WHERE UPPER(FILENAME)='CUSTOMIZE_PORTALINFO'";

                oPortalData.Sql = "SELECT CONTENT FROM CUSTOMIZE WHERE UPPER(FILENAME)='CUSTOMIZE_PORTALINFO'";
                oPortalData.ClientId = Convert.ToInt32(AppHelper.ClientId);
                content = AppHelper.GetResponse<string>("RMService/Portal/ContentInfo", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oPortalData); 


                objXMLDoc.LoadXml(content);


                foreach (XmlNode objNode in objGridXML.SelectNodes("/IndividualPortlets/option"))
                {
                    objLstRow = objCustomizationXML.CreateElement("PortletInfo");
                    objTempXML.LoadXml(objNode.OuterXml);
                    objLstRow.InnerText = objTempXML.SelectSingleNode("//urlPortlet").InnerText;
                    objLstRow.SetAttribute("name", objTempXML.SelectSingleNode("//Name").InnerText);
                    if (objTempXML.SelectSingleNode("//RowID").InnerText != "")
                    {
                        objLstRow.SetAttribute("id", objTempXML.SelectSingleNode("//RowID").InnerText);
                    }
                    else
                    {
                        objLstRow.SetAttribute("id", txtMaxId.Text);
                        txtMaxId.Text = (int.Parse(txtMaxId.Text) + 1).ToString();
                    } // else
                    objElement.AppendChild(objLstRow);
                }

                objXMLDoc.SelectSingleNode("/PortalSettings/IndividualPortletsInfo").InnerXml = objCustomizationXML.SelectSingleNode("//option").InnerXml;
                //PortalClient.ContentUpdate(objXMLDoc.InnerXml.ToString(), "CUSTOMIZE_PORTALINFO");
                oPortalData.Content = objXMLDoc.InnerXml.ToString();
                oPortalData.FileName = "CUSTOMIZE_PORTALINFO";
                oPortalData.Token = AppHelper.Token;
                AppHelper.GetResponse("RMService/Portal/Content", AppHelper.HttpVerb.PUT, AppHelper.APPLICATION_JSON, oPortalData); 


                #endregion // Saving Individual Portlet Grid Data to Customization XML.

                string[] arrDeletedPortletID = IndPortletsSelectedIdCollection.Text.Split(",".ToCharArray()[0]);
                if (arrDeletedPortletID != null)
                {
                    foreach (string sTmpDeletedPortletID in arrDeletedPortletID)
                    {
                        foreach (XmlNode objNode in objXMLDoc.SelectNodes("/PortalSettings/Portlets/id"))
                        {
                            if (objNode.InnerText == sTmpDeletedPortletID)
                            {
                                objNode.ParentNode.RemoveChild(objNode);
                            } // if
                        }
                    }
                }
                //PortalClient.ContentUpdate(objXMLDoc.InnerXml.ToString(), "CUSTOMIZE_PORTALINFO");
                oPortalData.Content = objXMLDoc.InnerXml.ToString();
                oPortalData.FileName = "CUSTOMIZE_PORTALINFO";
                oPortalData.Token = AppHelper.Token;
                AppHelper.GetResponse("RMService/Portal/Content", AppHelper.HttpVerb.PUT, AppHelper.APPLICATION_JSON, oPortalData); 

                //objXMLDoc.Save(Server.MapPath("~/App_Data/Customization.xml"));

                #region Saving Home page.

                // Rsolanki2: mits 23786: RMX portal setting should be saved to database instead of web server
                //strSql = "SELECT CONTENT FROM CUSTOMIZE WHERE UPPER(FILENAME)='CUSTOMIZE_HOMEPAGE'";
                oPortalData.Sql = "SELECT CONTENT FROM CUSTOMIZE WHERE UPPER(FILENAME)='CUSTOMIZE_HOMEPAGE'";
                oPortalData.ClientId = Convert.ToInt32(AppHelper.ClientId);
                string HomePage = AppHelper.GetResponse<string>("RMService/Portal/ContentInfo", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oPortalData); 


                objXMLDoc.LoadXml(HomePage);

                //objXMLDoc.Load(Server.MapPath("~/App_Data/HomePage.xml"));
                objXMLDoc.SelectSingleNode("//HomePage").InnerText = txtHomePage.Value;

                //PortalClient.ContentUpdate(objXMLDoc.InnerXml.ToString(), "CUSTOMIZE_HOMEPAGE");
                oPortalData.Content = objXMLDoc.InnerXml.ToString();
                oPortalData.FileName = "CUSTOMIZE_HOMEPAGE";
                oPortalData.Token = AppHelper.Token;
                AppHelper.GetResponse("RMService/Portal/Content", AppHelper.HttpVerb.PUT, AppHelper.APPLICATION_JSON, oPortalData); 


                //objXMLDoc.Save(Server.MapPath("~/App_Data/HomePage.xml"));

                #endregion

                #region Binding saved xml to grid

                // Rsolanki2: mits 23786: RMX portal setting should be saved to database instead of web server
                //strSql = "SELECT CONTENT FROM CUSTOMIZE WHERE UPPER(FILENAME)='CUSTOMIZE_PORTALINFO'";
                oPortalData.Sql = "SELECT CONTENT FROM CUSTOMIZE WHERE UPPER(FILENAME)='CUSTOMIZE_PORTALINFO'";
                oPortalData.ClientId = Convert.ToInt32(AppHelper.ClientId);
                content = AppHelper.GetResponse<string>("RMService/Portal/ContentInfo", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oPortalData); 

                objXMLDoc.LoadXml(content);


                //objXMLDoc.Load(Server.MapPath("~/App_Data/Customization.xml"));
                p_objXmlDocument = new XmlDocument();
                p_objXmlDocumentInd = new XmlDocument();
                CommonPortletXMLForGrid(objXMLDoc);
                IndividualPortletXMLForGrid(objXMLDoc);

                if (CommonPortletsGrid_Action.Text == "save")
                {
                    CommonPortletsGrid.BindData(p_objXmlDocument);
                    txtCommonPortletGenXML.Text = CommonPortletsGrid.GenerateXml().OuterXml;
                } // if
                if (IndividualPortletsGrid_Action.Text == "save")
                {
                    IndividualPortletsGrid.BindData(p_objXmlDocumentInd);
                    txtIndPortletGenXML.Text = IndividualPortletsGrid.GenerateXml().OuterXml;
                } // if
                #endregion

                //HttpContext.Current.Cache.Remove("RMXPortalHomePage");
                //HttpContext.Current.Cache.Remove("RMXPortalSettings");
                CacheCommonFunctions.RemoveValueFromCache("RMXPortalHomePage", AppHelper.ClientId);
                CacheCommonFunctions.RemoveValueFromCache("RMXPortalSettings", AppHelper.ClientId);

            } // try
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            
        } // constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlDoc"></param>
        private void CommonPortletXMLForGrid(XmlDocument xmlDoc)
        {
            #region Common Portlet XML that will be used to bind Grid.
            objLstRow = p_objXmlDocument.CreateElement("PortletSettings");
            p_objXmlDocument.AppendChild(objLstRow);
            objLstRow = p_objXmlDocument.CreateElement("AllUsersPortlets");
            objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//PortletSettings");
            objElement.AppendChild(objLstRow);
            objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//AllUsersPortlets");
            objLstRow = p_objXmlDocument.CreateElement("listhead");
            objRowTxt = p_objXmlDocument.CreateElement("SequenceId");
            objRowTxt.InnerText = AppHelper.GetResourceValue(PageID, "gvHdrCommonSequenceID", "0");
            objLstRow.AppendChild(objRowTxt);
            objRowTxt = p_objXmlDocument.CreateElement("Name");
            objRowTxt.InnerText = AppHelper.GetResourceValue(PageID, "gvHdrCommonNameofPortlet", "0");
            objLstRow.AppendChild(objRowTxt);
            objRowTxt = p_objXmlDocument.CreateElement("urlPortlet");
            objRowTxt.InnerText = AppHelper.GetResourceValue(PageID, "gvHdrCommonURLofPortlet", "0");
            objLstRow.AppendChild(objRowTxt);
            objElement.AppendChild(objLstRow);

            foreach (XmlNode node in xmlDoc.SelectNodes("//PortalSettings/AllUsersPortlets/Portlet"))
            {
                //objLstRow = p_objXmlDocument.CreateElement("listrow");
                objLstRow = p_objXmlDocument.CreateElement("option");
                objRowTxt = p_objXmlDocument.CreateElement("SequenceId");
                objRowTxt.InnerText = iPosition.ToString();
                objLstRow.AppendChild(objRowTxt);
                objRowTxt = p_objXmlDocument.CreateElement("RowID");
                objRowTxt.InnerText = iPosition.ToString();
                objLstRow.AppendChild(objRowTxt);
                objRowTxt = p_objXmlDocument.CreateElement("Name");
                objRowTxt.InnerText = node.Attributes["name"].Value;
                objLstRow.AppendChild(objRowTxt);
                objRowTxt = p_objXmlDocument.CreateElement("urlPortlet");
                if (node.Attributes["lockURLEditing"].Value == "true")
                {
                    //objRowTxt.InnerText = "** URL Editing Locked **";
                    objRowTxt.InnerText = AppHelper.GetResourceValue(PageID, "gvUrlEditingLocked", "0");
                }
                else
                {
                    objRowTxt.InnerText = node.InnerText;
                } // else
                objLstRow.AppendChild(objRowTxt);
                objElement.AppendChild(objLstRow);
                iPosition++;
            }

            iPosition++;
            iPosition = 1;

            CommonPortletXML.Text = p_objXmlDocument.InnerXml.ToString();
            AddBlankRow(ref p_objXmlDocument, COMMONPORTLET);
            #endregion
        } // method: CommonPortletXMLForGrid

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlDoc"></param>
        private void IndividualPortletXMLForGrid(XmlDocument xmlDoc)
        {
            #region Creating XML for Individual portlet Grid binding.

            //Creating XML required to bind data with second grid.
            objLstRow = p_objXmlDocumentInd.CreateElement("PortletSettings");
            p_objXmlDocumentInd.AppendChild(objLstRow);
            objElement = (XmlElement)p_objXmlDocumentInd.SelectSingleNode("//PortletSettings");
            objLstRow = p_objXmlDocumentInd.CreateElement("IndividualPortletsList");
            objElement.AppendChild(objLstRow);
            objElement = (XmlElement)p_objXmlDocumentInd.SelectSingleNode("//IndividualPortletsList");
            objLstRow = p_objXmlDocumentInd.CreateElement("listhead");
            objRowTxt = p_objXmlDocumentInd.CreateElement("SequenceId");
            objRowTxt.InnerText = AppHelper.GetResourceValue(PageID, "gvHdrCommonSequenceID", "0");
            objLstRow.AppendChild(objRowTxt);
            objRowTxt = p_objXmlDocumentInd.CreateElement("Name");
            objRowTxt.InnerText = AppHelper.GetResourceValue(PageID, "gvHdrCommonNameofPortlet", "0");
            objLstRow.AppendChild(objRowTxt);
            objRowTxt = p_objXmlDocumentInd.CreateElement("urlPortlet");
            objRowTxt.InnerText = AppHelper.GetResourceValue(PageID, "gvHdrCommonURLofPortlet", "0");
            objLstRow.AppendChild(objRowTxt);
            objElement.AppendChild(objLstRow);

            foreach (XmlNode node in xmlDoc.SelectNodes("//PortalSettings/IndividualPortletsInfo/PortletInfo"))
            {
                objLstRow = p_objXmlDocumentInd.CreateElement("option");
                objRowTxt = p_objXmlDocumentInd.CreateElement("SequenceId");
                objRowTxt.InnerText = iPosition.ToString();
                objLstRow.AppendChild(objRowTxt);
                objRowTxt = p_objXmlDocumentInd.CreateElement("RowID");
                //objRowTxt.InnerText = iPosition.ToString();
                objRowTxt.InnerText = node.Attributes["id"].Value.ToString();
                if (int.Parse(node.Attributes["id"].Value) > iMaxIdInd)
                {
                    iMaxIdInd = int.Parse(node.Attributes["id"].Value); // +1;
                    txtMaxId.Text = (iMaxIdInd + 1).ToString();
                } // if
                objLstRow.AppendChild(objRowTxt);
                objRowTxt = p_objXmlDocumentInd.CreateElement("Name");
                objRowTxt.InnerText = node.Attributes["name"].Value;
                objLstRow.AppendChild(objRowTxt);
                objRowTxt = p_objXmlDocumentInd.CreateElement("urlPortlet");
                objRowTxt.InnerText = node.InnerText;
                objLstRow.AppendChild(objRowTxt);
                objElement.AppendChild(objLstRow);
                iPosition++;
            }
            if (txtMaxId.Text == "")
            {
                txtMaxId.Text = "0";
            }
            IndividualPortletXML.Text = p_objXmlDocumentInd.InnerXml.ToString();
            AddBlankRow(ref p_objXmlDocumentInd, INDIVIDUALPORTLET);

            #endregion // Creating XML for Individual portlet Grid binding.
        } // method: IndividualPortletXMLForGrid

    }
}
