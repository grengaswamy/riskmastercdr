<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomizeReports.aspx.cs" Inherits="Riskmaster.UI.Utilities.UI_Admin.Customization.CustomizeReports" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
  <title>System Customization (Reports)</title>
  <link rel="stylesheet" href="../../../../Content/system.css" type="text/css"/>
  <script language="javaScript" src="../../../../Scripts/form.js" type="text/javascript"></script>
</head>
 <body class="10pt" onload="parent.MDIScreenLoaded();">
 <script language="javascript" type="text/javascript">

     function openEditWindow(defaulttext, controlname) {
         var currentvalue = document.getElementById(controlname).value;
		 //bram4 - MITS:35786 - Multilingualchanges
         //added a query string dummy due to a bug in .Net Framework.
         //When decoding on server side - the first value is skipped.
         //Refer: http://stackoverflow.com/questions/659887/get-url-parameters-from-a-string-in-net#comment873809_659929         
         m_codeWindow = window.open('../Customization/CustomizeEditWindow.aspx?dummy=dummy&' + 'currentvalue=' + encodeURIComponent(currentvalue) + '&defaulttext=' + defaulttext + '&controlname=' + controlname, 'CustomizeEditWindow',
									'width=500,height=400' + ',top=' + (screen.availHeight - 400) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=no,scrollbars=no');
     }

     function setDefaultValue(name1, name2) {

         document.getElementById(name1).value = document.getElementById(name2).value;
         return false;
     }

     function CheckUnCheck(CtrlName) {
         switch (CtrlName) {
             case 'Reports':
                 var chkAvailableReports = document.getElementById('AvailableReports');
                 var chkJobQueue = document.getElementById('JobQueue');
                 var chkDeleteReport = document.getElementById('DeleteReport');
                 var chkScheduleReports = document.getElementById('ScheduleReports');
                 var chkViewScheduledReports = document.getElementById('ViewScheduledReports');
                 chkAvailableReports.checked = document.getElementById(CtrlName).checked;
                 chkJobQueue.checked = document.getElementById(CtrlName).checked;
                 chkDeleteReport.checked = document.getElementById(CtrlName).checked;
                 chkScheduleReports.checked = document.getElementById(CtrlName).checked;
                 chkViewScheduledReports.checked = document.getElementById(CtrlName).checked;
                 break;
             case 'ExecSummary':
                 var chkConfiguration = document.getElementById('Configuration');
                 var chkClaim = document.getElementById('Claim');
                 var chkEvent = document.getElementById('Event');
                 chkConfiguration.checked = document.getElementById(CtrlName).checked;
                 chkClaim.checked = document.getElementById(CtrlName).checked;
                 chkEvent.checked = document.getElementById(CtrlName).checked;
                 break;
             case 'SMNet':
                 var chkDesigner = document.getElementById('Designer');
                 var chkDraftReports = document.getElementById('DraftReports');
                 var chkPostDraftReports = document.getElementById('PostDraftReports');
                 var chkPostNewReport = document.getElementById('PostNewReport');
                 chkDesigner.checked = document.getElementById(CtrlName).checked;
                 chkDraftReports.checked = document.getElementById(CtrlName).checked;
                 chkPostDraftReports.checked = document.getElementById(CtrlName).checked;
                 chkPostNewReport.checked = document.getElementById(CtrlName).checked;
                 break;
             case 'OtherReports':
                 var chkOSHA300 = document.getElementById('OSHA300');
                 var chkOSHA301 = document.getElementById('OSHA301');
                 var chkOSHA300A = document.getElementById('OSHA300A');
                 var chkOSHASharpsLog = document.getElementById('OSHASharpsLog');
                 var chkDCC = document.getElementById('DCC');
                 chkOSHA300.checked = document.getElementById(CtrlName).checked;
                 chkOSHA301.checked = document.getElementById(CtrlName).checked;
                 chkOSHA300A.checked = document.getElementById(CtrlName).checked;
                 chkOSHASharpsLog.checked = document.getElementById(CtrlName).checked;
                 chkDCC.checked = document.getElementById(CtrlName).checked;
                 break;
         }
     }

     function Validation() {
         if (IsNumeric(document.getElementById("ReportEmailFrom_hidden").value) == true && document.getElementById("ReportEmailFrom_hidden").value != "") {
             //alert("Please enter alphabets in Override Sender Name");
             alert(CustomizeReportsValidations.ValidationEnterAlphabetsinSenderName);
             return false;
         }

         if (IsNumeric(document.getElementById("ReportEmailFromAddr_hidden").value) == true && document.getElementById("ReportEmailFromAddr_hidden").value != "") {
             //alert("Please enter alphabets in Override Sender Address");
             alert(CustomizeReportsValidations.ValidationEnterAlphabetsinSenderAddress);
             return false;
         }
         return true;
     }

     function IsNumeric(strString)
     //  check for valid numeric strings	
     {
         var strValidChars = "0123456789";
         var strChar;
         var blnResult = true;

         // if (strString.length == 0) return false;
         //  test strString consists of valid characters listed above
         for (i = 0; i < strString.length && blnResult == true; i++) {
             strChar = strString.charAt(i);
             if (strValidChars.indexOf(strChar) == -1) {
                 blnResult = false;
             }
         }
         return blnResult;
     }
				
 </script>
 <form id="frmData" method="post" runat="server" >
 <uc1:ErrorControl ID="ErrorControl1" runat="server" />
 <div class="msgheader"><asp:Label ID="lblSystemCustomization" runat="server" Text="<%$ Resources :lblSystemCustomization  %>"></asp:Label></div>
 <table border="0" cellspacing="0" cellpadding="0" width="100%">
    <tr><td><br /></td></tr>
    <tr>
     <td width="100%">
      <table border="0" cellspacing="0" cellpadding="0" width="100%">
       <tr>
        <td colspan="2" class="ctrlgroup">&nbsp;<asp:Label ID="lblReportEmailSetting" runat="server" Text="<%$ Resources : lblReportEmailSetting %>"></asp:Label></td>
       </tr>
       <tr>
        <td width="30%" class="datatd">
            <asp:Label ID="lblOverrideSenderName" runat="server" Text="<%$ Resources : lblOverrideSenderName %>"></asp:Label></td>
        <td width="70%" class="datatd">
        <% 
            if (ReportEmailFrom_hidden.Text != "")
            {
                ReportEmailFrom.Text = ReportEmailFrom_hidden.Text;
            }
            else
            {
                ReportEmailFrom.Text = "***";
            }
           ReportEmailFrom.OnClientClick = "openEditWindow('','" + ReportEmailFrom_hidden.ClientID + "');";  %>
         <asp:LinkButton runat="server" Font-Underline="true" id="ReportEmailFrom" class="LinkBoldBlue" ></asp:LinkButton>
         <asp:TextBox style="display:none" runat="server" rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportEmail/From" type="hidden" ID="ReportEmailFrom_hidden"/>
        </td>
       </tr>
       <tr>
        <td class="datatd1">
            <asp:Label ID="lblOverrideSenderAddress" runat="server" Text="<%$ Resources : lblOverrideSenderAddress %>"></asp:Label></td>
        <td class="datatd1">
        <% 
            if (ReportEmailFromAddr_hidden.Text != "")
            {
                ReportEmailFromAddr.Text = ReportEmailFromAddr_hidden.Text;
            }
            else
            {
                ReportEmailFromAddr.Text = "***";
            } 
           ReportEmailFromAddr.OnClientClick = "openEditWindow('','" + ReportEmailFromAddr_hidden.ClientID + "');";  %>
         <asp:LinkButton runat="server" Font-Underline="true" id="ReportEmailFromAddr" class="LinkBoldBlue" ></asp:LinkButton>
         <asp:TextBox style="display:none" runat="server" rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportEmail/FromAddr" type="hidden"  id="ReportEmailFromAddr_hidden" /></td>
       </tr>
      </table>
     </td>
    </tr>
    <tr>
     <td width="100%">
      <table border="0" cellspacing="0" cellpadding="0" width="100%">
       <tr>
        <td colspan="3" class="ctrlgroup">&nbsp;<asp:Label ID="lblReportMenuLinks" runat="server" Text="<%$ Resources : lblReportMenuLinks %>"></asp:Label></td>
       </tr>
       <tr class="colheader3">
        <td width="25%"></td>
        <td width="10%">
            <asp:Label ID="lblShow" runat="server" Text="<%$ Resources :lblShow %>" Font-Italic="true"></asp:Label></td>
        <td width="65%">
            <asp:Label ID="lblLabel" runat="server" Text="<%$ Resources :lblLabel %>" Font-Italic="true"></asp:Label></td>
       </tr>
       <tr>
        <td class="ctrlgroup">
            <asp:Label ID="lblBusinessIntelligence" runat="server" Text="<%$ Resources :lblBusinessIntelligence %>" Font-Italic="true"></asp:Label></td>
        <td class="datatd"><asp:CheckBox FormType="Customize" runat="server" type="checkbox" rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportMenu/BILabel/@value" appearance="full" id="CheckBox1"  onclick="CheckUnCheck(this.id);" /></td>
        <td class="datatd">
            <asp:TextBox runat="server" onblur="return isProperString(this);" type="text" rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportMenu/BILabel/@title" ID="BILabel" />  
            <asp:TextBox runat="server" style="display:none" type="hidden" rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportMenu/BILabel/@default"  ID="BILabelDefault"/>
            <input type="submit" runat="server"  id="BILabelButton" value="<%$ Resources : btnDefault %>" class="button" style="width:100"  onclick="return setDefaultValue('BILabel', 'BILabelDefault');"/></td> 
       </tr>
       <tr class="datatd">
       <td><br /></td>
       </tr>
       <tr>
        <td class="ctrlgroup">
            <asp:Label ID="lblReport" runat="server" Font-Italic="true" Text="<%$ Resources : lblReport %>"></asp:Label></td>
        <td class="datatd"><asp:CheckBox FormType="Customize" runat="server" type="checkbox" rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportMenu/ReportLabel/@value" appearance="full" id="Reports"  onclick="CheckUnCheck(this.id);" /></td>
        <td class="datatd">
            <asp:TextBox runat="server" onblur="return isProperString(this);" type="text" rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportMenu/ReportLabel/@title" ID="ReportLabel" />  
            <asp:TextBox runat="server" style="display:none" type="hidden" rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportMenu/ReportLabel/@default"  ID="ReportLabelDefault"/>
            <input type="submit" runat="server"  id="ReportLabelButton" value="<%$ Resources: btnDefault %>" class="button" style="width:100"  onclick="return setDefaultValue('ReportLabel', 'ReportLabelDefault');"/></td>
       </tr>
       <tr>
        <td class="datatd1">
            <asp:Label ID="lblAvailableReports" runat="server" Text="<%$ Resources : lblAvailableReports %>"></asp:Label></td>
        <td class="datatd1"><asp:CheckBox FormType="Customize" runat="server" type="checkbox"   rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportMenu/ReportLabel/AvlReports" appearance="full" id="AvailableReports"  /></td>
        <td class="datatd1">&nbsp;</td>
       </tr>
       <tr>
        <td class="datatd">
            <asp:Label ID="lblJobQueue" runat="server" Text="<%$ Resources : lblJobQueue %>"></asp:Label></td>
        <td class="datatd"><asp:CheckBox FormType="Customize" runat="server" type="checkbox"   rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportMenu/ReportLabel/JobQueue" appearance="full" id="JobQueue"  /></td>
        <td class="datatd">&nbsp;</td>
       </tr>
       <tr>
        <td class="datatd"><asp:Label ID="lblDeleteReport" runat="server" Text="<%$ Resources : lblDeleteReport %>"></asp:Label></td>
        <td class="datatd"><asp:CheckBox FormType="Customize" runat="server" type="checkbox"  rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportMenu/ReportLabel/DeleteReport" appearance="full" id="DeleteReport"  /></td>
        <td class="datatd">&nbsp;</td>
       </tr>
       <tr>
        <td class="datatd1"><asp:Label ID="lblScheduleReport" runat="server" Text="<%$ Resources : lblScheduleReport %>"></asp:Label></td>
        <td class="datatd1"><asp:CheckBox FormType="Customize" runat="server" type="checkbox"   rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportMenu/ReportLabel/ScheduleReport" appearance="full" id="ScheduleReports"  /></td>
        <td class="datatd1">&nbsp;</td>
       </tr>
       <tr>
        <td class="datatd"><asp:Label ID="lblViewScheduledReports" runat="server" Text="<%$ Resources : lblViewScheduledReports %>"></asp:Label></td>
        <td class="datatd"><asp:CheckBox FormType="Customize" runat="server" type="checkbox"   rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportMenu/ReportLabel/ViewSchedReport" appearance="full" id="ViewScheduledReports"  /></td>
        <td class="datatd">&nbsp;</td>
       </tr>
       <tr>
        <td class="ctrlgroup"><asp:Label ID="lblSMNet" runat="server" Font-Italic="true" Text="<%$ Resources : lblSMNet %>"></asp:Label></td>
        <td class="datatd1"><asp:CheckBox FormType="Customize" runat="server" type="checkbox"  rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportMenu/SMLabel/@value" appearance="full" id="SMNet" onclick="CheckUnCheck(this.id);" /></td>
        <td class="datatd1">
            <asp:TextBox runat="server" onblur="return isProperString(this);" type="text" rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportMenu/SMLabel/@title" ID="SMLabel" /> 
            <asp:TextBox runat="server" style="display:none" type="text" rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportMenu/SMLabel/@default" ID="SMLabelDefault" /> 
            <input id="SMLabelButton" runat="server" type="submit" value="<%$ Resources : btnDefault  %>" class="button" style="width:100" onclick="return setDefaultValue('SMLabel', 'SMLabelDefault');" /></td>
       </tr>
       <tr>
        <td class="datatd"><asp:Label ID="lblDesigner" runat="server" Text="<%$ Resources : lblDesigner %>"></asp:Label></td>
        <td class="datatd"><asp:CheckBox FormType="Customize" runat="server" type="checkbox" rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportMenu/SMLabel/Designer" appearance="full" id="Designer"  /></td>
        <td class="datatd">&nbsp;</td>
       </tr>
       <tr>
        <td class="datatd1"><asp:Label ID="lblDraftReports" runat="server" Text="<%$ Resources : lblDraftReports %>"></asp:Label></td>
        <td class="datatd1"><asp:CheckBox FormType="Customize" runat="server" type="checkbox" rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportMenu/SMLabel/DraftReport" appearance="full" id="DraftReports" /></td>
        <td class="datatd1">&nbsp;</td>
       </tr>
       <tr>
        <td class="datatd"><asp:Label ID="lblPostDraftReports" runat="server" Text="<%$ Resources : lblPostDraftReports %>"></asp:Label></td>
        <td class="datatd"><asp:CheckBox FormType="Customize" runat="server" type="checkbox" rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportMenu/SMLabel/PostDraftRpt" appearance="full" id="PostDraftReports"  /></td>
        <td class="datatd">&nbsp;</td>
       </tr>
       <tr>
        <td class="datatd1"><asp:Label ID="lblPostSMWorldReport" runat="server" Text="<%$ Resources : lblPostSMWorldReport %>"></asp:Label></td>
        <td class="datatd1"><asp:CheckBox FormType="Customize" runat="server" type="checkbox" rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportMenu/ReportLabel/NewReport" appearance="full" id="PostNewReport"  /></td>
        <td class="datatd1">&nbsp;</td>
       </tr>
       <tr>
        <td class="ctrlgroup"><asp:Label ID="lblExecSummary" runat="server" Font-Italic="true" Text="<%$ Resources : lblExecSummary %>"></asp:Label></td>
        <td class="datatd1"><asp:CheckBox FormType="Customize" runat="server" type="checkbox" rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportMenu/ExecSummLabel/@value" appearance="full" id="ExecSummary" onclick="CheckUnCheck(this.id);" /></td>
        <td class="datatd1">
            <asp:TextBox runat="server" onblur="return isProperString(this);" type="text"  rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportMenu/ExecSummLabel/@title" id="ExecSummLabel"/> 
            <asp:TextBox runat="server" style="display:none" type="text"  rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportMenu/ExecSummLabel/@default" id="ExecSummLabelDefault"/> 
            <input type="submit" value="<%$ Resources : btnDefault %>" runat="server" class="button" style="width:100" id="ExecSummLabelButton" onclick="return setDefaultValue('ExecSummLabel', 'ExecSummLabelDefault');" /></td>
       </tr>
       <tr>
        <td class="datatd"><asp:Label ID="lblConfiguration" runat="server" Text="<%$ Resources : lblConfiguration %>"></asp:Label></td>
        <td class="datatd">
            <asp:CheckBox FormType="Customize" runat="server" type="checkbox" rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportMenu/ExecSummLabel/Configuration" appearance="full" id="Configuration"  /></td>
        <td class="datatd">&nbsp;</td>
       </tr>
       <tr>
        <td class="datatd1"><asp:Label ID="lblClaim" runat="server" Text="<%$ Resources : lblClaim %>"></asp:Label></td>
        <td class="datatd1">
            <asp:CheckBox FormType="Customize" runat="server" type="checkbox" rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportMenu/ExecSummLabel/Claim" appearance="full" id="Claim"  /></td>
        <td class="datatd1">&nbsp;</td>
       </tr>
       <tr>
        <td class="datatd"><asp:Label ID="lblEvent" runat="server" Text="<%$ Resources : lblEvent %>"></asp:Label></td>
        <td class="datatd">
            <asp:CheckBox FormType="Customize" runat="server" type="checkbox" rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportMenu/ExecSummLabel/Event" appearance="full" id="Event"  /></td>
        <td class="datatd">&nbsp;</td>
       </tr>
       <tr>
        <td class="ctrlgroup"><asp:Label ID="lblOtherReports" runat="server" Font-Italic="true" Text="<%$ Resources : lblOtherReports %>"></asp:Label></td>
        <td class="datatd1">
            <asp:CheckBox FormType="Customize" runat="server" type="checkbox" rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportMenu/OtherLabel/@value" appearance="full" id="OtherReports" onclick="CheckUnCheck(this.id);" /></td>
        <td class="datatd1">
            <asp:TextBox runat="server" onblur="return isProperString(this);" type="text" rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportMenu/OtherLabel/@title"  id="OtherLabel"/>  
            <asp:TextBox style="display:none" runat="server" type="text" rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportMenu/OtherLabel/@default"  id="OtherLabelDefault"/>  
            <input id="OtherLabelButton" runat="server" type="submit" value="<%$ Resources: btnDefault %>" class="button" style="width:100" onclick="return setDefaultValue('OtherLabel', 'OtherLabelDefault');" /></td>
       </tr>
       <tr>
        <td class="datatd1"><asp:Label ID="lblOSHA300" runat="server" Text="<%$ Resources : lblOSHA300 %>"></asp:Label></td>
        <td class="datatd1">
            <asp:CheckBox FormType="Customize" runat="server" type="checkbox" rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportMenu/OtherLabel/OSHA300" appearance="full" id="OSHA300"  /></td>
        <td class="datatd1">&nbsp;</td>
       </tr>
       <tr>
        <td class="datatd"><asp:Label ID="lblOSHA301" runat="server" Text="<%$ Resources : lblOSHA301 %>"></asp:Label></td>
        <td class="datatd">
            <asp:CheckBox FormType="Customize" runat="server" type="checkbox" rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportMenu/OtherLabel/OSHA301" appearance="full" id="OSHA301" /></td>
        <td class="datatd">&nbsp;</td>
       </tr>
       <tr>
        <td class="datatd1"><asp:Label ID="lblOSHA300A" runat="server" Text="<%$ Resources : lblOSHA300A %>"></asp:Label></td>
        <td class="datatd1">
            <asp:CheckBox FormType="Customize" runat="server" type="checkbox" rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportMenu/OtherLabel/OSHA300A" appearance="full" id="OSHA300A"  /></td>
        <td class="datatd1">&nbsp;</td>
       </tr>
       <tr>
        <td class="datatd"><asp:Label ID="lblOSHASharpsLog" runat="server" Text="<%$ Resources : lblOSHASharpsLog %>"></asp:Label></td>
        <td class="datatd">
            <asp:CheckBox FormType="Customize" runat="server" type="checkbox" rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportMenu/OtherLabel/OSHASharpsLog" appearance="full" id="OSHASharpsLog"  /></td>
        <td class="datatd">&nbsp;</td>
       </tr>
       <tr>
        <td class="datatd1"><asp:Label ID="lblDCC" runat="server" Text="<%$ Resources : lblDCC %>"></asp:Label></td>
        <td class="datatd1">
            <asp:CheckBox FormType="Customize" runat="server" type="checkbox" rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportMenu/OtherLabel/DCCLabel" appearance="full" id="DCC"  /></td>
        <td class="datatd1">
            <asp:TextBox runat="server" onblur="return isProperString(this);" type="text" rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportMenu/OtherLabel/DCCLabel/@title"  id="DCCLabel" />  
            <asp:TextBox style="display:none" runat="server" type="text" rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportMenu/OtherLabel/DCCLabel/@default"  id="DCCLabelDefault" />  
            <input id="DCCLabelButton" runat="server" type="submit" value="<%$ Resources : btnDefault %>" class="button" style="width:100" onclick="return setDefaultValue('DCCLabel', 'DCCLabelDefault');" /></td>
       </tr>
      </table>
     </td>
    </tr>
    <tr>
     <td width="100%">
      <table border="0" cellspacing="0" cellpadding="0" width="100%">
       <tr>
        <td colspan="3" class="ctrlgroup">&nbsp;<asp:Label ID="lblReportQueueButtons" runat="server" Text="<%$ Resources : lblReportQueueButtons %>"></asp:Label></td>
       </tr>
       <tr class="colheader3">
        <td width="25%"></td>
        <td width="10%"><asp:Label ID="lblShow1" runat="server" Font-Italic="true" Text="<%$ Resources : lblShow1 %>"></asp:Label></td>
        <td width="65%"><asp:Label ID="lblLabel1" runat="server"  Font-Italic="true" Text="<%$ Resources : lblLabel1 %>"></asp:Label></td>
       </tr>
       <tr>
        <td class="datatd"><asp:Label ID="lblArchive" runat="server" Text="<%$ Resources : lblArchive %>"></asp:Label></td>
        <td class="datatd">
            <asp:CheckBox FormType="Customize" runat="server" ID="Archive" type="checkbox" rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportMenu/ReportQueue/Archive" appearance="full"  /></td>
        <td class="datatd">&nbsp;</td>
       </tr>
       <tr>
        <td class="datatd1"><asp:Label ID="lblEmail" runat="server" Text="<%$ Resources : lblEmail %>"></asp:Label></td>
        <td class="datatd1">
            <asp:CheckBox FormType="Customize" runat="server" ID="Email" type="checkbox" rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportMenu/ReportQueue/Email" appearance="full"  /></td>
        <td class="datatd1">&nbsp;</td>
       </tr>
       <tr>
        <td class="datatd"><asp:Label ID="lblDelete" runat="server" Text="<%$ Resources : lblDelete %>"></asp:Label></td>
        <td class="datatd">
            <asp:CheckBox FormType="Customize" runat="server" ID="Del" type="checkbox" rmxref="Instance/Document/customize_reports/RMAdminSettings/ReportMenu/ReportQueue/Delete" appearance="full"  /></td>
        <td class="datatd">&nbsp;</td>
       </tr>
      </table>
     </td>
    </tr>
    <tr>
     <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
     <td colspan="2">
        <asp:Button runat="server" ID="btnsave" type="submit" Text="<%$ Resources : btnSave %>" class="button" style="width:100" OnClientClick="return Validation();" OnClick="Save"/>  
        <asp:Button runat="server" ID="btnrefresh" type="submit" Text="<%$ Resources : btnRefresh %>" class="button" style="width:100" OnClick="Refresh"/>
     </td>
    </tr>
   </table>
  </form>
 </body>
</html>

