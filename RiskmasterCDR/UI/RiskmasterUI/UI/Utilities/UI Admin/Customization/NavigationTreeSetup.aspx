﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NavigationTreeSetup.aspx.cs" 
    Inherits="Riskmaster.UI.UI.Utilities.UI_Admin.Customization.NavigationTreeSetup" %>

<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" TagName="UserControlDataGrid"
    TagPrefix="dg" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Navigation Tree Settings</title>
    <link rel="stylesheet" href="../../../../App_Themes/RMX_Default/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../../../Content/zpcal/themes/system.css" type="text/css" />

    <script language="javaScript" src="../../../../Scripts/form.js" type="text/javascript"></script>

    <script language="javaScript" src="../../../../Scripts/Utilities.js" type="text/javascript"></script>

    <script language="JavaScript" src="../../../../Scripts/drift.js" type="text/javascript"></script>

    <script language="JavaScript" src="../../../../Scripts/WaitDialog.js" type="text/javascript">        { var i; }</script>
</head>
<body onload="parent.MDIScreenLoaded()">
  
    <form id="frmData" runat="server">
    <asp:HiddenField ID="hdnURL" Value="" runat="server" />
    <rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />
    <div id="toolbardrift" class="toolbardrift">
        <div class="toolBarButton" runat="server" id="div_save" xmlns="">
            <asp:imagebutton ID="Imagebutton1" runat="server" onclick="NavigateSave"
                src="../../../../Images/save.gif" alternatetext="<%$ Resources:imgbtnAltSave %>"
                validationgroup="vgSave" onmouseover="this.src='../../../../Images/tb_save_mo.png';this.style.zoom='110%'"
                onmouseout="this.src='../../../../Images/save.gif';this.style.zoom='100%'" style="height:28px;width:28px;border-width:0px;"/>
        </div>
    </div>
    <div class="msgheader2" id="formtitle">
        <%-- <asp:Label ID="lblAdjusterExpTitle" runat="server" Text="<%$ Resources:lblAdjusterExpTitle %>"></asp:Label> --%>

    </div>
    <div class="customItemContainer">
        <div class="colheader3">
            <div class="customShowColumn"><asp:Label ID="lblDisplay" runat="server" Text="<%$ Resources:lblDisplay %>"></asp:Label></div> 
        </div>
        <div class="datatd">
            <div class="customLeftColumn"><asp:Label ID="lblAdjusterExpansion" runat="server" Text="<%$ Resources:lblAdjusterExpansion %>"></asp:Label></div>
            <div class="customShowColumn"><asp:CheckBox FormType="Customize" runat="server" ID="chkAdjusterExpansion" type="checkbox" rmxref="Instance/Document/ADJCustomization/AdjusterExpansion" appearance="full" /></div>
        </div>
       <%--ngupta73 : MITS- 34260/RMA-4362: START--%>
       <%-- <div class="datatd">--%>
       <div class="datatd" runat ="server" id="divPolicyExpansion">
       <%--ngupta73 : MITS- 34260/RMA-4362: END--%>
            <div class="customLeftColumn"><asp:Label ID="lblPolicyExpansion" runat="server" Text="<%$ Resources:lblPolicyExpansion %>"></asp:Label></div>
            <div class="customShowColumn"><asp:CheckBox FormType="Customize" runat="server" ID="chkPolicyExpansion" type="checkbox" rmxref="Instance/Document/ADJCustomization/PolicyExpansion" appearance="full" /></div>
        </div>
    </div>

    <uc2:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    
    <input type="text" name="" value="" id="SysClassName" style="display: none" />
    <input type="text" name="" value="" id="SysCmd" style="display: none" />
    <input type="text" name="" value="" id="SysFormIdName" style="display: none" />
    <input type="text" name="" value="" id="SysFormPIdName" style="display: none" />
    <input type="text" name="" value="" id="SysFormName" style="display: none" />
    <input type="text" name="" value="" id="SysCmdQueue" style="display: none" />
       
  <asp:TextBox style="display:none" runat="server" id="CheckCarrierClaim" RMXType="hidden" Text="" rmxref="Instance/Document/ADJCustomization/CheckCarrierClaim" />  <%--ngupta73 : MITS- 34260/RMA-4362--%>

    </form>
</body>
</html>
