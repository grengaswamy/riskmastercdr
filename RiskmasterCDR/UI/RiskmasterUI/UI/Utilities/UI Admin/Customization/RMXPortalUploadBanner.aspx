﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RMXPortalUploadBanner.aspx.cs"
    Inherits="Riskmaster.UI.UI.Utilities.UI_Admin.Customization.RMXPortalUploadBanner" %>

<meta http-equiv="pragma" content="no-cache" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Upload Banner</title>
    <link rel="stylesheet" href="~/App_Themes/RMX_Default/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../../../Content/zpcal/themes/system.css" type="text/css" />

    <script language="javaScript" src="../../../../Scripts/form.js" type="text/javascript"></script>

</head>
<body class="rowlight" runat="server">

    <script type="text/javascript">
        function validateImageUpload() {
            var fileName = document.getElementById('btnfileUpload');
            fileName = fileName.value;
            if (fileName != null && fileName != "") { //alert("please choose a image to upload."); return false; }
                var extloc = fileName.lastIndexOf('.');
                var ext = "";
                ext = fileName.substring(extloc + 1, fileName.length);
                ext = ext.toUpperCase();
                if (!(ext == 'JPEG' || ext == 'JPG' || ext == 'GIF' || ext == 'BMP')) {
                    alert('please choose either JPG / BMP / GIF image');
                    return false;
                }
                return true;
            }
            return true;
        }
    </script>

    <form id="frmData" runat="server" name="frmData" method="post" enctype="multipart/form-data">
    <rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />
    <input type="hidden" id="wsrp_rewrite_action_1" name="" value="" />
    <div id="toolbardrift" name="toolbardrift" class="toolbardrift">
        <table border="0" class="toolbar" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center" valign="middle" height="32">
                    <div class="toolBarButton" runat="server" id="div_save" xmlns="">
                        <asp:ImageButton runat="server" src="../../../../Images/save.gif" Width="28" Height="28"
                            border="0" ID="save" AlternateText="Save" ValidationGroup="vgSave" onmouseover="this.src='../../../../Images/save2.gif';this.style.zoom='110%'"
                            onmouseout="this.src='../../../../Images/save.gif';this.style.zoom='100%'" OnClick="save_Click" /></div>
                </td>
            </tr>
        </table>
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="4">
        <tr>
            <td class="ctrlgroup" colspan="4">
                Existing Banner at RMX-Portal
            </td>
        </tr>
        <tr>
            <td width="3%">
                Existing image:
            </td>
        </tr>
        <tr>
            <td>
                <asp:Image ImageUrl="RMXPortalUploadBanner.aspx?src=rmx_logo.gif" runat="server" ID="imgPortal" />
            </td>
        </tr>
        <tr>
            <td>
                <br />
            </td>
        </tr>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="ctrlgroup" colspan="4">
                Update Image
            </td>
        </tr>
        <tr>
            <td colspan="1">
                Upload image
            </td>
            <td>
                <asp:FileUpload ID="btnfileUpload" runat="server" />
                <%--<input id="btnfileUpload" type="file" name="file" onkeydown="return false;"/>--%>
            </td>
            <td>
                <%--<input type="submit" value="Upload" onclick="return validateImageUpload();"/>--%>
                <%--<asp:Button Text="upload" runat="server" ID="btnUpload" OnClick="btnUpload_Click" />--%>
            </td>
        </tr>
        <tr colspan="5">
            <td style="font-size: 11; text-align: left;">
                (The image size for the banner should be 540 * 54 px.)
            </td>
        </tr>
        <%--        <tr>
            <td>
                Current Background Color:
            </td>
            
        </tr>--%>
        <tr>
            <td colspan="1">
                Please enter the background color in Hex Format(Eg. #6AADE4):
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtbkcolor" />
                <%--                <asp:Button runat="server" ID ="btnSaveBkColor" Text="Save" 
                    onclick="btnSaveBkColor_Click"/>--%>
            </td>
            <td>
                <asp:Label runat="server" ID="lblbkcolor" Width="59%" />
            </td>
        </tr>
        <tr>
            <td colspan="1">
                Please enter the font color for login value in Hex Format(Eg. #FFFFFF):
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtLoginValueFontColor" />
                <%--                <asp:Button runat="server" ID ="btnSaveBkColor" Text="Save" 
                    onclick="btnSaveBkColor_Click"/>--%>
            </td>
            <td>
                <asp:Label runat="server" ID="lblLoginValueFontColor" Width="59%" />
            </td>
        </tr>
        <tr>
            <td colspan="1">
                Please enter the font color for login label in Hex Format(Eg. #025A82):
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtLoginLabelFontColor" />
                <%--                <asp:Button runat="server" ID ="btnSaveBkColor" Text="Save" 
                    onclick="btnSaveBkColor_Click"/>--%>
            </td>
            <td>
                <asp:Label runat="server" ID="lblLoginLabelFontColor" Width="59%" />
            </td>
        </tr>
        <tr>
            <td colspan="1">
                Please enter the font color for logout value in Hex Format(Eg. #025A82):
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtLogoutFontColor" />
                <%--                <asp:Button runat="server" ID ="btnSaveBkColor" Text="Save" 
                    onclick="btnSaveBkColor_Click"/>--%>
            </td>
            <td>
                <asp:Label runat="server" ID="lblLogoutFontColor" Width="59%" />
            </td>
        </tr>
         <tr>
            <td colspan="1">
                Please enter the font color for forgot password link value in Hex Format(Eg. #000000):
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtForgotPasswordColor" />
                <%--                <asp:Button runat="server" ID ="btnSaveBkColor" Text="Save" 
                    onclick="btnSaveBkColor_Click"/>--%>
            </td>
            <td>
                <asp:Label runat="server" ID="lblForgotPasswordColor" Width="59%" />
            </td>
        </tr>
        <tr>
            <td>
                <br />
                <br />
            </td>
        </tr>
        <tr>
            <td colspan="4" style="text-align: center;">
                <input type="button" value="  Close  " onclick="window.close()" />
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
