﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.RMXResourceManager;
using Riskmaster.Models;
using Riskmaster.Cache;
namespace Riskmaster.UI.UI.Utilities.UI_Admin.Customization
{
    public partial class PortalUserTree : NonFDMBasePageCWS
    {
        public XmlDocument Model = null;
        public XmlDocument objXmlDocument = null;
        private string MessageTemplate = "<Message><Authorization>b5848889-63e9-4ce9-8f26-eba5593d61f0</Authorization><Call>" +
           "<Function>RMXPortalCustomizationAdaptor.GetUserList</Function></Call><Document><form></form></Document></Message>";
        protected void Page_Load(object sender, EventArgs e)
        {

            // Changes done on 31/03/2014 for multilingual
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("PortalUserTree.aspx"), "CustomizePortalUserTree", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "CustomizePortalUserTree", sValidationResources, true);

            string sLabelResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("PortalUserTree.aspx"), "sLabelResources", ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "sLabelResources", sLabelResources, true);
            // End Changes done on 31/03/2014 for multilingual
            LTV.Attributes.Add("onclick", "return OnCheckBoxCheckChanged(event)");
            Savebtn.Attributes.Add("onclick", "return Save()");
            Model = new XmlDocument();
            objXmlDocument = new XmlDocument();
            bool bOrphanRecordsAdded = false;
            if (!IsPostBack)
            {
                string sReturn = AppHelper.CallCWSService(MessageTemplate.ToString());
                Model.LoadXml(sReturn);
                Model.LoadXml(Model.SelectSingleNode("//Document").InnerXml);
                //Model.Load(@"C:/Source/temp.xml");
                TreeNode parentNode = LTV.FindNode("0");
                foreach(XmlNode objNodeGroup in Model.SelectNodes("//Group"))
                {
                    parentNode = AddChildNode(parentNode, objNodeGroup.Attributes["name"].Value, objNodeGroup.Attributes["name"].Value);
                    objXmlDocument.LoadXml(objNodeGroup.OuterXml);
                    foreach (XmlNode objNodeUsers in objXmlDocument.SelectNodes("//User"))
                    {
                        TreeNode oTempNode = AddChildNode(parentNode, objNodeUsers.Attributes["user_id"].Value, objNodeUsers.Attributes["login_name"].Value);
                    }
                        //if (objNodeUsers.ParentNode.Name == "Group" && objNodeUsers.ParentNode.Attributes["name"].Value == objNodeGroup.Attributes["name"].Value)
                        //{
                        //    parentNode = AddChildNode(parentNode, objNodeUsers.Attributes["user_id"].Value, objNodeUsers.Attributes["login_name"].Value);
                        //} // if
                        //else if(objNodeUsers.ParentNode.Name == "GroupAndUser" && bOrphanRecordsAdded)
                        //{
                        //    parentNode = LTV.FindNode("0");
                        //    parentNode = AddChildNode(parentNode, objNodeUsers.Attributes["user_id"].Value, objNodeUsers.Attributes["login_name"].Value);
                        //    bOrphanRecordsAdded = true;
                        //} // else
                    parentNode = LTV.FindNode("0");
                    //}
                }
                foreach (XmlNode objNodeUsers in Model.SelectNodes("/GroupAndUser/User"))
                {
                    TreeNode oTempNode = AddChildNode(parentNode, objNodeUsers.Attributes["user_id"].Value, objNodeUsers.Attributes["login_name"].Value);
                }
            } // if
            else
            {
                string sReturn = AppHelper.CallCWSService(MessageTemplate.ToString());
                Model.LoadXml(sReturn);
                Model.LoadXml(Model.SelectSingleNode("//Document").InnerXml);
            }
            DataBind();
        }

        //protected void LTV_SelectedNodeChanged(object sender, EventArgs e)
        //{
        //    this.highlighteduser.Text = "";
        //    if (LTV.SelectedNode == null) return;
        //    else if (LTV.SelectedNode.Depth >= 1 && LTV.SelectedNode.ChildNodes.Count == 0)
        //    {
        //        this.highlighteduser.Text = "Highlighted User: ";
        //        this.highlighteduser.Text += LTV.SelectedNode.Text;
        //        this.hdnselecteduser.Value = LTV.SelectedNode.Text;
        //    }
        //}
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private TreeNode AddChildNode(TreeNode oParentNode, string p_Value, string p_sName)
        {
            TreeNode childNode = null;
            try
            {
                childNode = new TreeNode(p_sName, p_Value.ToString());
                childNode.PopulateOnDemand = false;
                childNode.Target = "_self";
                oParentNode.ChildNodes.Add(childNode);
            } // try
            catch (Exception e)
            {
            }
            return childNode;
        }

        protected void Savebtn_Click(object sender, EventArgs e)
        {
            XmlDocument objCustomizationXML = new XmlDocument();
            XmlElement objNodeId = null;
            string sSelectedportletsdata = string.Empty;
            
            // Rsolanki2: mits 23786: RMX portal setting should be saved to database instead of web server
            //string strSql = "SELECT CONTENT FROM CUSTOMIZE WHERE UPPER(FILENAME)='CUSTOMIZE_PORTALINFO'";

            PortalData oPortalData = new PortalData();
            oPortalData.Sql = "SELECT CONTENT FROM CUSTOMIZE WHERE UPPER(FILENAME)='CUSTOMIZE_PORTALINFO'";
            oPortalData.ClientId = Convert.ToInt32(AppHelper.ClientId);
            string content = AppHelper.GetResponse<string>("RMService/Portal/ContentInfo", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oPortalData); 


            objCustomizationXML.LoadXml(content);

            //objCustomizationXML.Load("C:/Customization.xml");
            //objCustomizationXML.Load(Server.MapPath("~/App_Data/Customization.xml"));
            string[] arrSelectedNodes = null;
            string[] arrSelectedLoginNames = null;
            arrSelectedNodes = selectednodes.Value.Split(",".ToCharArray()[0]);
            arrSelectedLoginNames = hdnSelectedLoginNames.Value.Split(",".ToCharArray()[0]);
            if (arrSelectedNodes != null)
            {
                foreach (string sTmpSelectedNodes in arrSelectedNodes)
                {
                    if (objCustomizationXML.SelectSingleNode("/PortalSettings/Portlets[@LoginName='" + sTmpSelectedNodes + "']") != null)
                    {
                        string sPortletID = objCustomizationXML.SelectSingleNode("/PortalSettings/Portlets[@LoginName='" + sTmpSelectedNodes + "']").InnerText;
                        if(sPortletID.IndexOf(selectedportletid.Value) == -1)
                        {
                            objNodeId = objCustomizationXML.CreateElement("id");
                            objNodeId.InnerText = selectedportletid.Value;
                            objCustomizationXML.SelectSingleNode("/PortalSettings/Portlets[@LoginName='" + sTmpSelectedNodes + "']").AppendChild(objNodeId);
                        } // if
                    }
                    else
                    {
                        objNodeId = objCustomizationXML.CreateElement("Portlets");
                        objNodeId.SetAttribute("LoginName", sTmpSelectedNodes);
                        objNodeId.InnerXml = "<id>" + selectedportletid.Value + "</id>";
                        objCustomizationXML.DocumentElement.AppendChild(objNodeId);
                    } // else

                }// foreach
                foreach (string sTmpSelectedLoginNames in arrSelectedLoginNames)
                {
                    bool bLoginNameExist = false;
                    string sTmpSelected = string.Empty;
                    foreach(string sTmpSelectedNodes in arrSelectedNodes)
                    {
                        //sTmpSelected = sTmpSelectedNodes;
                        if(sTmpSelectedLoginNames == sTmpSelectedNodes)
                        {
                            bLoginNameExist = true;
                            break;
                        } // if

                    } // foreach
                    if (!bLoginNameExist)
                    {
                        // akaushik5 Changed for MITS 33551 Starts
                        //    if (objCustomizationXML.SelectSingleNode("/PortalSettings/Portlets[@LoginName='" + sTmpSelectedLoginNames + "']") != null)
                        //    {

                        //        foreach (XmlNode objNode in objCustomizationXML.SelectNodes("/PortalSettings/Portlets[@LoginName='" + sTmpSelectedLoginNames + "']/id"))
                        //        {
                        //            if(objNode.InnerXml == selectedportletid.Value)
                        //            {
                        //                objCustomizationXML.SelectSingleNode("/PortalSettings/Portlets[@LoginName='" + sTmpSelectedLoginNames + "']").RemoveChild(objNode);
                        //                break;
                        //            } // if
                        //        } // foreach    
                        //    } // if
                        XmlNodeList objParentNodes = objCustomizationXML.SelectNodes("/PortalSettings/Portlets[@LoginName='" + sTmpSelectedLoginNames + "' and id='" + selectedportletid.Value + "']");

                        if (objParentNodes != null && objParentNodes.Count > 0)
                        {
                            foreach (XmlNode objParentNode in objParentNodes)
                            {
                                if (objParentNode != null && objParentNode.ChildNodes != null && objParentNode.ChildNodes.Count > 0)
                                {
                                    foreach (XmlNode objNode in objParentNode.ChildNodes.Cast<XmlNode>().ToList().FindAll(x => x.InnerXml.Equals(selectedportletid.Value)))
                                    {
                                        objParentNode.RemoveChild(objNode);
                                    }
                                }
                            }
                        }
                        // akaushik5 Changed for MITS 33551 Ends
                    } // if
                } // foreach

                oPortalData.Content = objCustomizationXML.InnerXml.ToString();
                oPortalData.FileName = "CUSTOMIZE_PORTALINFO";
                oPortalData.Token = AppHelper.Token;
                AppHelper.GetResponse("RMService/Portal/Content", AppHelper.HttpVerb.PUT, AppHelper.APPLICATION_JSON, oPortalData); 
                //PortalClient.ContentUpdate(objCustomizationXML.InnerXml.ToString(), "CUSTOMIZE_PORTALINFO");

                //HttpContext.Current.Cache.Remove("RMXPortalSettings");    
                CacheCommonFunctions.RemoveValueFromCache("RMXPortalSettings", AppHelper.ClientId);

                //objCustomizationXML.Save("C:/Customization.xml");
                //objCustomizationXML.Save(Server.MapPath("~/App_Data/Customization.xml"));
            }
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load("C:/Customization.xml");
            //foreach (XmlNode objNodeIndividualPortletsInfo in xmlDoc.SelectNodes("/PortalSettings/IndividualPortletsInfo/PortletInfo"))
            //{
            //    sSelectedportletsdata += "#" + objNodeIndividualPortletsInfo.Attributes["id"].Value.ToString() + "$";
            //    //foreach(XmlNode objNodePortlets in xmlDoc.SelectNodes("/PortalSettings/Portlets/id["+ objNodeIndividualPortletsInfo.Attributes["id"].Value + "]"))
            //    foreach (XmlNode objNodePortlets in xmlDoc.SelectNodes("/PortalSettings/Portlets/id"))
            //    {
            //        if (objNodePortlets.InnerText.ToString() == objNodeIndividualPortletsInfo.Attributes["id"].Value.ToString())
            //        {
            //            sSelectedportletsdata += objNodePortlets.ParentNode.Attributes["LoginName"].Value.ToString() + ",";
            //        } // if
            //    } // foreach
            //    sSelectedportletsdata += "$" + objNodeIndividualPortletsInfo.Attributes["id"].Value.ToString() + "#";
            //} // foreach
            //hdnSelectedLoginNames.Value = sSelectedportletsdata.ToString();
            //xmlDoc = null;
        }
    }
}
