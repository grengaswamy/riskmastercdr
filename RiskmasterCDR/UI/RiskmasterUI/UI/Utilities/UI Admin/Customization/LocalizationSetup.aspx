﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LocalizationSetup.aspx.cs"
    Inherits="Riskmaster.UI.Utilities.UI_Admin.Customization.LocalizationSetup" ValidateRequest="false" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Resource Localization Interface</title>
    <link rel="stylesheet" href="../../../../Content/rmnet.css" type="text/css" />
    <script language="javaScript" src="../../../../Scripts/form.js" type="text/javascript">       
    
    </script>
    

     <script language="javaScript" type="text/javascript">
         function validateSearch() {
             if (document.getElementById('txtSearch') != null) {
             if (document.getElementById('txtSearch').value == "") {
                 alert("Please enter filter criteria ");
                 return false;
             }
                 else {

                     return true;
                 }
             }
         }
         function validateText() {
             if (document.getElementById('tbxText') != null) {
             if (document.getElementById('tbxText').value == "") {
                 alert("Please enter text ");
                 return false;
             }
             else {

                     return true;
                 }
             }
         }
       </script>
  
</head>
<body onload="parent.MDIScreenLoaded();">
    <form id="frmData" runat="server">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <div id="Div6" name="toolbardrift" class="toolbardrift">
        <table border="0" class="toolbar" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center" valign="middle" height="32">
                    <div class="toolBarButton" runat="server" id="div_save" xmlns="">
                        <asp:ImageButton runat="server" src="../../../../Images/save.gif" Width="28" Height="28"
                            border="0" ID="ImageButton1" AlternateText="Save" ValidationGroup="vgSave" onmouseover="this.src='../../../../Images/save2.gif';this.style.zoom='110%'"
                            onmouseout="this.src='../../../../Images/save.gif';this.style.zoom='100%'" OnClick="btnSave_Click" OnClientClick="return validateText();"/></div>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <div class="msgheader" id="formtitle">
        Resource Localization Setup</div>
    <div class="errtextheader">
    </div>
    <br />
    <table width="100%">
        <tr>
            <td width="20%">
                <asp:Label runat="server" class="required" ID="lblLanguages" Text="Languages :" />
            </td>
            <td >
                <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                    
                       <%-- <ajaxToolkit:ComboBox ID="lstLangList" AutoPostBack="true" runat="server" AutoCompleteMode="Append"
                            OnSelectedIndexChanged="lstLangList_SelectedIndexChanged" CssClass="WindowsStyle" />
                    --%>
                      <asp:DropDownList ID="lstLangList" runat="server"  AutoPostBack="true"
                            OnSelectedIndexChanged="lstLangList_SelectedIndexChanged" Width="95%" TabIndex="1">
                               </asp:DropDownList>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td width="20%">
                <asp:Label runat="server" class="required" ID="lblErrorsOrPages" Text="Errors/Pages/MDI/Search:" />
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:DropDownList ID="drpErrorsOrPages" runat="server" TabIndex="2" AutoPostBack="true"
                            OnSelectedIndexChanged="drpErrorsOrPages_SelectedIndexChanged" Width="95%">
                            <asp:ListItem Value="0">Globally used errors and alert messages</asp:ListItem>
                            <asp:ListItem Value="1">Page specific controls and alert messages</asp:ListItem>
                            <asp:ListItem Value="2">Menu</asp:ListItem>
                            <asp:ListItem value="3">Search</asp:ListItem>

                        </asp:DropDownList>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td width="20%">
                <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                    <ContentTemplate>
                        <asp:Label runat="server" class="required" ID="lblResrcSet" Text="List of Pages :" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                    <ContentTemplate>
                        <asp:ListBox runat="server" ID="drpPageList" Rows="5" AutoPostBack="true" OnSelectedIndexChanged="drpPageList_SelectedIndexChanged"
                            Width="95%" TabIndex="4"/>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td width="20%">
                <asp:UpdatePanel ID="UpdatePanel10" runat="server" UpdateMode="Conditional">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="drpErrorsOrPages" EventName="SelectedIndexChanged" />
                    </Triggers>
                    <ContentTemplate>
                        <asp:Label runat="server" class="required" ID="lblType" Text="Type of Control :" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Conditional">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="drpErrorsOrPages" EventName="SelectedIndexChanged" />
                        <asp:AsyncPostBackTrigger ControlID="drpPageList" EventName="SelectedIndexChanged" />                        
                    </Triggers>
                    <ContentTemplate>
                        <asp:DropDownList ID="drpType" runat="server" TabIndex="5" AutoPostBack="true" OnSelectedIndexChanged="drpType_SelectedIndexChanged"
                            Width="95%">
                        </asp:DropDownList>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
         <tr>
            <td>
                &nbsp;
            </td>
        </tr>
         <tr>
            <td colspan="2" align="center">
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="tbxSearchField" runat="server" onchange="setDataChanged(true);" TextMode="MultiLine" Width="95%"
                            Rows="25" TabIndex="3"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        
         <tr>
            <td width="20%">
                <asp:UpdatePanel ID="UpdatePanel16" runat="server">
                    <ContentTemplate>
                        <asp:Label runat="server" class="required" ID="lblAdminTable" Text="Admin Tracking Table:" visible="false" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel17" runat="server" UpdateMode="Conditional">
                     <Triggers>                      
                        <asp:AsyncPostBackTrigger ControlID="drpPageList" EventName="SelectedIndexChanged" />
                         <asp:AsyncPostBackTrigger ControlID="drpType" EventName="SelectedIndexChanged" />
                         <asp:AsyncPostBackTrigger ControlID="lstLangList" EventName="SelectedIndexChanged" />
                         <asp:AsyncPostBackTrigger ControlID="drpErrorsOrPages" EventName="SelectedIndexChanged" />

                    </Triggers>
                    <ContentTemplate>
                        <asp:ListBox runat="server" ID="lstAdminTable" Rows="8" AutoPostBack="true" OnSelectedIndexChanged="lstAdminTable_SelectedIndexChanged"
                            Width="95%" TabIndex="6" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td width="20%">
             <asp:UpdatePanel ID="UpdatePanel13" runat="server" UpdateMode="Conditional">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="drpErrorsOrPages" EventName="SelectedIndexChanged" />
                    </Triggers>
                    <ContentTemplate>
                <asp:Label runat="server" ID="lblSearch" Text="Enter Id or text to search:" />
                 </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
            <asp:UpdatePanel ID="UpdatePanel12" runat="server" UpdateMode="Conditional">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="drpErrorsOrPages" EventName="SelectedIndexChanged" />
                    </Triggers>
                    <ContentTemplate>
                <asp:TextBox runat="server" ID="txtSearch" Width="79%" TabIndex="7"/>
                <asp:Button runat="server" ID="btnSearch" Text="Search" OnClick="btnSearch_Click" OnClientClick="return validateSearch();"
                    Width="7%" TabIndex="8"/>
                <asp:Button runat="server" ID="btnReset" Text="Reset" OnClick="btnReset_Click" Width="7%" TabIndex="9"/>
                </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td width="20%">
                <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                    <ContentTemplate>
                        <asp:Label runat="server" class="required" ID="lblFieldlst" Text="List of Controls:" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:ListBox runat="server" ID="lstFieldList" Rows="8" AutoPostBack="true" OnSelectedIndexChanged="lstFieldList_SelectedIndexChanged"
                            Width="95%" TabIndex="10"/>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td width="20%">
                <asp:UpdatePanel ID="UpdatePanel14" runat="server">
                    <ContentTemplate>
                        <asp:Label runat="server" class="required" ID="lblSearchField" Text="Search Field:" visible="false" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel15" runat="server">
                    <Triggers> 
                         <asp:AsyncPostBackTrigger ControlID="lstLangList" EventName="SelectedIndexChanged" />
                    </Triggers>
                    <ContentTemplate>
                        <asp:ListBox runat="server" ID="lstSearchField" Rows="8" AutoPostBack="true" OnSelectedIndexChanged="lstSearchField_SelectedIndexChanged"
                            Width="95%" TabIndex="11" visible="false"/>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td width="20%">
                <asp:UpdatePanel ID="UpdatePanel11" runat="server" UpdateMode="Conditional">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="drpErrorsOrPages" EventName="SelectedIndexChanged" />
                    </Triggers>
                    <ContentTemplate>
                        <asp:Label runat="server" class="required" ID="lblText" Text="Text:" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="tbxText" runat="server" onchange="setDataChanged(true);" TextMode="MultiLine"
                            Rows="2" Width="95%" TabIndex="12"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
     <asp:HiddenField ID="PostBackSender" runat="server" />
     <asp:HiddenField ID="hdnSearchKey" runat="server" />
    
    </form>
   
</body>

</html>
