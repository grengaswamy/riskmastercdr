﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomizationCaptions.aspx.cs" Inherits="Riskmaster.UI.Utilities.UI_Admin.Customization.CustomizationCaptions" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<head id="head1" runat="server">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
   
  <title>System Customization (Captions/Paths)</title>
  <link rel="stylesheet" href="../../../../Content/system.css"  type="text/css"/>
  <script type="text/javascript" src="/../../Scripts/zapatec/utils/zapatec.js"></script>
  <script type="text/javascript" src="/../../Scripts/zapatec/zpwin/src/window.js"></script>
  <script type="text/javascript" src="/../../Scripts/zapatec/zpwin/src/dialog.js"></script>
  <script type="text/javascript" src="/../../Scripts/zapatec/zpcal/src/calendar.js"></script>
  <script type="text/javascript" src="/../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>
  <script type="text/javascript" src="/../../Scripts/zapatec/zpcal/src/calendar-setup.js"></script>
  <script type="text/javascript" src="/../../Scripts/calendar-alias.js"></script>
  <script type="text/javascript" src="/../../Scripts/zapatec/zpgrid/src/zpgrid.js"></script>
  <script type="text/javascript" src="/../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js"></script>
  <script type="text/javascript" src="/../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js"></script>
  <script type="text/javascript" src="/../../Scripts/zapatec/zpgrid/src/zpgrid-query.js"></script>
  <script type="text/javascript" src="/../../Scripts/rmx-common-ref.js"></script>
 </head>
 <body class="10pt">
 <script language="javascript">

             function openEditWindow(defaulttext, controlname) 
					{
					    var defaultvalue = document.getElementById(defaulttext).value;
					    var currentvalue = document.getElementById(controlname).value;

					    m_codeWindow = window.open('../Customization/CustomizeEditWindow.aspx?defaulttext=' + defaulttext + '&controlname=' + controlname + '&defaultvalue=' + defaultvalue + '&currentvalue=' + currentvalue, 'CustomizeEditWindow',
									'width=500,height=400' + ',top=' + (screen.availHeight - 400) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=no,scrollbars=no');
					}
				</script><form id="frmData" name="frmData" method="post" runat="server">
				<input type="hidden" id="wsrp_rewrite_action_1" name="" value=""/>
				<div class="msgheader">System Customization
    			</div>
   <table border="0" cellspacing="0" cellpadding="0" width="100%">
    <tr>
     <td colspan="2" class="ctrlgroup">&nbsp;Captions/Messages		
     </td>
    </tr>
    <tr>
     <td class="datatd" width="30%"><strong>Company Name:</strong></td>
     <td class="datatd" width="70%">
     <% 
     CompanyName.Text = CompanyName_hidden.Text;
     CompanyName.OnClientClick = "openEditWindow('" + CompanyName_default_hidden.ClientID + "','" + CompanyName_hidden.ClientID + "');";%>
     <asp:LinkButton runat="server" id="CompanyName" class="LinkBoldBlue" Font-Underline="true"></asp:LinkButton>
         <asp:TextBox style="display:none" runat="server"  type="hidden" rmxref="Instance/Document/customize_captions/RMAdminSettings/Captions/CompanyName" id="CompanyName_hidden"/>
         <asp:TextBox style="display:none" runat="server" type="hidden" rmxref="Instance/Document/customize_captions/RMAdminSettings/Captions/CompanyName/@default" id="CompanyName_default_hidden"/>
      </td>
    </tr>
    <tr>
     <td class="datatd1"><strong>Application Title:</strong></td>
     <td class="datatd1">
     <%AppTitle.Text = AppTitle_hidden.Text;
         AppTitle.OnClientClick = "openEditWindow('" + AppTitle_default_hidden.ClientID + "','" + AppTitle_hidden.ClientID + "');";%>
     <asp:LinkButton runat="server" id="AppTitle" class="LinkBoldBlue" Font-Underline="true"></asp:LinkButton>
         <asp:TextBox style="display:none" runat="server" type="hidden" rmxref="Instance/Document/customize_captions/RMAdminSettings/Captions/AppTitle" id="AppTitle_hidden"/>
         <asp:TextBox style="display:none" runat="server" type="hidden" rmxref="Instance/Document/customize_captions/RMAdminSettings/Captions/AppTitle/@default" id="AppTitle_default_hidden"/>
      </td>
    </tr>
    <tr>
     <td class="datatd"><strong>Report Application Title:</strong></td>
     <td class="datatd">
      <% ReportAppTitle.Text = ReportAppTitle_hidden.Text;
          ReportAppTitle.OnClientClick="openEditWindow('" + ReportAppTitle_default_hidden.ClientID + "','" + ReportAppTitle_hidden.ClientID + "');";%>
     <asp:LinkButton runat="server" id="ReportAppTitle"  class="LinkBoldBlue" Font-Underline="true"></asp:LinkButton>
         <asp:TextBox style="display:none" runat="server" type="hidden" rmxref="Instance/Document/customize_captions/RMAdminSettings/Captions/ReportAppTitle" id="ReportAppTitle_hidden"/>
         <asp:TextBox style="display:none" runat="server" type="hidden" rmxref="Instance/Document/customize_captions/RMAdminSettings/Captions/ReportAppTitle/@default" id="ReportAppTitle_default_hidden"/>
      </td>
    </tr>
    <tr>
     <td class="datatd1"><strong>Contact on Error:</strong></td>
     <td class="datatd1">
     <%ErrContact.Text = ErrContact_hidden.Text;
         ErrContact.OnClientClick = "openEditWindow('" + ErrContact_default_hidden.ClientID + "','" + ErrContact_hidden.ClientID + "');";%> 
     <asp:LinkButton runat="server" id="ErrContact" class="LinkBoldBlue" Font-Underline="true"></asp:LinkButton>
     <asp:TextBox style="display:none" runat="server" type="hidden" rmxref="Instance/Document/customize_captions/RMAdminSettings/Captions/ErrContact" id="ErrContact_hidden"/>
     <asp:TextBox style="display:none" runat="server" type="hidden" rmxref="Instance/Document/customize_captions/RMAdminSettings/Captions/ErrContact/@default" id="ErrContact_default_hidden"/>
      </td>
    </tr>
    <tr>
     <td class="datatd"><strong>Copyright Message:</strong></td>
     <td class="datatd">
     <%AppCopyright.Text = AppCopyright_hidden.Text;
         AppCopyright.OnClientClick = "openEditWindow('" + AppCopyright_default_hidden.ClientID + "','" + AppCopyright_hidden.ClientID + "');"; %>
     <asp:LinkButton runat="server" id="AppCopyright" class="LinkBoldBlue" Font-Underline="true"></asp:LinkButton>
     <asp:TextBox style="display:none" runat="server" type="hidden" rmxref="Instance/Document/customize_captions/RMAdminSettings/Captions/AppCopyright" id="AppCopyright_hidden"/>
     <asp:TextBox style="display:none" runat="server" type="hidden" rmxref="Instance/Document/customize_captions/RMAdminSettings/Captions/AppCopyright/@default" id="AppCopyright_default_hidden"/>
      </td>
    </tr>
    <tr>
     <td class="datatd1"><strong>Alert Existing Record Required:</strong></td>
     <td class="datatd1">
      <%AlertExistingRec.Text = AlertExistingRec_hidden.Text;
      AlertExistingRec.OnClientClick = "openEditWindow('" + AlertExistingRec_default_hidden.ClientID + "','" + AlertExistingRec_hidden.ClientID + "');";%>
     <asp:LinkButton runat="server" id="AlertExistingRec" class="LinkBoldBlue" Font-Underline="true"></asp:LinkButton>
     <asp:TextBox style="display:none" runat="server" type="hidden" rmxref="Instance/Document/customize_captions/RMAdminSettings/Captions/AlertExistingRec" id="AlertExistingRec_hidden"/>
     <asp:TextBox style="display:none" runat="server" type="hidden" rmxref="Instance/Document/customize_captions/RMAdminSettings/Captions/AlertExistingRec/@default" id="AlertExistingRec_default_hidden"/>
       </td>
    </tr>
    <tr>
     <td colspan="2">&nbsp;	
     </td>
    </tr>
    <tr>
     <td colspan="2" class="ctrlgroup">&nbsp;Paths
     </td>
    </tr>
    <tr>
     <td><strong>Override Documents Path:</strong></td>
     <td>
     <%OverrideDocPath.Text = OverrideDocPath_hidden.Text;
     OverrideDocPath.OnClientClick = "openEditWindow('" + OverrideDocPath_default_hidden.ClientID + "','" + OverrideDocPath_hidden.ClientID + "');";  %>
     <asp:LinkButton runat="server" id="OverrideDocPath" class="LinkBoldBlue" Font-Underline="true"></asp:LinkButton>
     <asp:TextBox style="display:none" runat="server" type="hidden" rmxref="Instance/Document/customize_captions/RMAdminSettings/Paths/OverrideDocPath" id="OverrideDocPath_hidden"/>
     <asp:TextBox style="display:none" runat="server" type="hidden" rmxref="Instance/Document/customize_captions/RMAdminSettings/Paths/OverrideDocPath/@default" id="OverrideDocPath_default_hidden"/>
      </td>
    </tr>
    <tr>
     <td colspan="2">&nbsp;
     </td>
    </tr>
    <tr>
     <td colspan="2">
     <asp:Button type="submit" runat="Server" ID="btnSave"  OnClick="Save" Text="Save" class="button" style="width:100" />
     <asp:Button type="submit" runat="Server" ID="btnRefresh" Text="Refresh" OnClick="Refresh" class="button" style="width:100" />
     </td>
    </tr>
   </table><input type="hidden" runat="Server" name="$node^5" value="rmx-widget-handle-4" id="SysWindowId"/>
   <input type="hidden" runat="Server" name="$instance"/>
   </form>
 </body>
</html>