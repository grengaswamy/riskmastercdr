<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomizeEditWindow.aspx.cs" Inherits="Riskmaster.UI.Utilities.UI_Admin.Customization.CustomizeEditWindow" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
 <head id="Head1" runat="server">
  <title>Edit Value</title>
  <link rel="stylesheet" href="../../../../Content/system.css" type="text/css" />
 </head>
 <body class="10pt" >
    <script language="javascript" type="text/javascript">
				
					function RestoreDefault()
					{
					   	document.forms[0].currenttext.value=document.forms[0].defaulttext.value;
						return false;
					}
					
					function trimAll(sString) 
                    {
                        while (sString.substring(0,1) == ' ')
                        {
                        sString = sString.substring(1, sString.length);
                        }
                        while (sString.substring(sString.length-1, sString.length) == ' ')
                        {
                        sString = sString.substring(0,sString.length-1);
                        }
                        return sString;
                    }
				
					function SubmitValue()
					{
						ctrl_hidden=window.opener.document.getElementById(document.forms[0].controlname.value);
											
						if(document.forms[0].currenttext.value==null || trimAll(document.forms[0].currenttext.value)=="")
						{
                //alert("New value Field cannot be left blank");
                alert(CustomizeEditWindowValidations.ValidationBlankField);
						    document.forms[0].currenttext.focus();
						}
						else
						{
						    if (ctrl_hidden!=null)
							    ctrl_hidden.value=document.forms[0].currenttext.value;
						
					    	window.opener.document.forms[0].submit();				
						    window.close();
						
						    return false;
						}    
					}
				
	</script>
	<form id="frmData"  method="post" runat="server">
	<uc1:ErrorControl ID="ErrorControl1" runat="server" />
	<table border="0" cellspacing="0" cellpadding="0" width="100%">
    <tr>
     <td colspan="2"><strong><asp:Label runat="server" ID="defaultvalue" Text="Default Value"/></strong></td>
    </tr>
    <tr>
     <td width="70%"><asp:TextBox  runat="server" Height="80px" TextMode="MultiLine" Rows="10" Columns="20" Wrap="true" id="defaulttext" style="width:95%" disabled="true"></asp:TextBox></td>
     <td width="30%" valign="middle"><asp:Button ID="btndefault" style="width:80%"  runat="server" type="submit" Text="Restore Default" class="button" OnClientClick="return RestoreDefault();; "/></td>
    </tr>
    <tr>
     <td colspan="2"><strong>
         <asp:Label ID="lblEnterDesiredVal" runat="server" Text="<%$ Resources : lblEnterDesiredVal %>"></asp:Label></strong></td>
    </tr>
    <tr>
     <td width="70%"><asp:TextBox runat="server" Columns="20" Rows="10" Wrap="true" id="currenttext" TextMode="MultiLine" style="width:95%" Height="80px" ></asp:TextBox></td>
     <td width="10%"><asp:Button ID="btnsave" runat="server" type="submit" 
             Text="<%$ Resources : btnSave %>" class="button" style="width:80%" 
             OnClientClick="return SubmitValue();; " /></td>
    </tr>
    <tr>
     <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
     <td align="center"><asp:Button ID="btncancel" runat="server" type="submit" Text="<%$ Resources : btncancel %>" class="button" style="width:80" OnClientClick="javascript:window.close();return false;; "/></td>
     <td><asp:TextBox style="display:none" runat="server" value="" id="controlname" /></td>
    </tr>
   </table>
  </form>
 </body>
</html>
