﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="PortalUserTree.aspx.cs"
    Inherits="Riskmaster.UI.UI.Utilities.UI_Admin.Customization.PortalUserTree" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
    body {
  padding:0;
  margin:1em;
}

#progressbar {
  position:relative;
  width:300px; height:20px;
  border:1px solid black;
  overflow:hidden;
}

#progressbar div {
  background:#316AC5;
  width:0; height:100%;
}

</style>
    <script language="javaScript" src="../../../../Scripts/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script language="javaScript" src="../../../../Scripts/zapatectree.js" type="text/javascript"></script>
    
     <script type="text/javascript">
         
         var AllocatedUsers ;//= document.getElementById("hdnSelectedLoginNames").value;
         function pleaseWait(state) {
             var objPW = document.getElementById("PleaseWaitDialog1_pleaseWaitFrame");

             if (state == "start") {
                 objPW.style.display = "block";
             }
             else { //stop
                 objPW.style.display = "none";
             }
         }
         function OnCheckBoxCheckChanged(evt) {
             var src = window.event != window.undefined ? window.event.srcElement : evt.target;
             //Highlight the USER only code
             if (src.tagName.toLowerCase() == "a") {
                 return GetSelectedNodeValue(src);
             }
             //Highlight the USER only code

             var isChild = false; //variable to check if node has child/not
             var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
             if (isChkBoxClick) {
                 var parentTable = GetParentByTagName("table", src);
                 var nxtSibling = parentTable.nextSibling;
                 if (nxtSibling && nxtSibling.nodeType == 1)//check if nxt sibling is not null & is an element node 
                 {
                     if (nxtSibling.tagName.toLowerCase() == "div") //if node has children 
                     {
                         //check or uncheck children at all levels
                         isChild = true;
                         CheckUncheckChildren(parentTable.nextSibling, src.checked);
                     }

                 }
                 //Check if all Siblings are Unchecked /Not 
                 var isAllSiblingsUnChecked = AreAllSiblingsUnChecked(src);
                 CheckUncheckParentsOnly(src, isAllSiblingsUnChecked, isChild);
             }
         }
         function isExists(value) {

             var val = document.getElementById("hdnSelectedLoginNames").value;
             val = val.replace('#', '');
             val = val.replace('$', '');
             var arr = val.split(",");
             for (i = 0; i < arr.length; i++) {
                 if (arr[i] != "") {
                     if (arr[i] == value)
                         return true;
                 }
             }
             return false;
         }
         function SetBgColorForAllCheckBoxes() {
             arr = document.getElementsByTagName("input");
             for (var i = 0; i < document.getElementsByTagName("input").length; i++) {
                 if (arr[i].type == "checkbox") {
                     arr[i].style.backgroundColor = "white";
                 }
             }
         }
         // rsolanki2: start of the progressbar update

         function SelectUsers() {
             var checkboxname;
             m_bIsTreeStillLoading = true;             
             AllocatedUsers = ","+document.getElementById("hdnSelectedLoginNames").value;
             SetBgColorForAllCheckBoxes();
             (function() {
                 var i, length, data, el, start, arr;
                 //arr = GetTreeHandle().getElementsByTagName("a");
                 //arr = GetTreeHandle().getElementsByClassName("<%#LTV.ClientID%>_0");
                 arr = $(".<%#LTV.ClientID%>_0")
                 length = arr.length;
                 if (length > 70) {
                     //showing the progress bar only when there are more than 70 nodes 
                     document.getElementById('progressbar').style.display = '';
                 }
                 el = document.getElementById("progressbar").firstChild;
                 //alert("element :" + el + "  ;width:" + el.style.width);

                 function sort(progressFn) {
                     i = 1; // inital to 1 wecan skip the 'groups and users' node this way.

                     (function() {
                         try {
                            
                             $("#<%#LTV.ClientID%>" + arr[i].id.replace("<%#LTV.ClientID%>", "").replace("t", "n") + "CheckBox")[0].checked = (AllocatedUsers.indexOf(',' + arr[i].innerText + ',') == -1)?false:true;
                             
                         }
                         catch (e)
                             { }

                         i++;
                         progressFn(i, length);
                         if (i < length) {
                             setTimeout(arguments.callee, 0);
                         }
                     })();
                 }

                 sort(function(value, total) {
                     try {
                         el.style.width = (100 * value / total) + "%";
                         if (value >= total) {
                             document.getElementById('progressbar').style.display = 'none';
                             arr = "";
                             // for
                             setTimeout("javascript:client_OnTreeNodeChecked()", 0);
                             //m_bIsTreeStillLoading = false;
                         }
                     }
                     catch (e)
        { }
                 });

             })();
             //alert("element :" + el + "  ;width:" + el.style.width);
         }

         // rsolanki2: end of the progressbar update
         
         
         function SelectUsers2() {
             setTimeout(function() { }, 0);
             pleaseWait('start');
             setTimeout(function() { }, 0);
             SetBgColorForAllCheckBoxes();
             setTimeout(function() { }, 0);
             var obj = GetTreeHandle();
             var treename = "<%#LTV.ClientID%>"; ;
             var arr = obj.getElementsByTagName("a");
             for (var i = 0; i < obj.getElementsByTagName("a").length; i++) {

                 if (arr[i].className == treename + "_0") {

                     var r = arr[i].id.replace("<%#LTV.ClientID%>", "")
                     var r1 = r.replace("t", "n");
                     var checkboxname = "<%#LTV.ClientID%>" + r1 + "CheckBox";
                     if (isExists(arr[i].innerText)) {
                         document.getElementById(checkboxname).checked = true;
                         setTimeout(function() { }, 0);
                         var val = AreAllSiblingsUnChecked(document.getElementById(checkboxname));
                         setTimeout(function() { }, 0);
                         CheckUncheckParentsOnly(document.getElementById(checkboxname), val);
                         setTimeout(function() { }, 0);

                     }
                     else {
                         document.getElementById(checkboxname).checked = false;
                     }
                 }
             }

             pleaseWait('stop');
         }
         function GetTreeHandle() {
             var tree;
             var treeName = "<%#LTV.ClientID%>"; ;
             // Get a handle to the TreeView.
             tree = document.getElementById(treeName);
             if (null == tree || undefined == tree)
                 return null;
             return tree;
         }
         function GetSelectedNodeValue(src) {
             var treeName = "<%#LTV.ClientID%>" + "_Data";
             var name = eval(treeName).selectedNodeID.value;
            if (!HasChildren(src)) { //RMA 7097 gaurav singhal; added conditional statement Firefox issue FIx
                document.getElementById('hdnselecteduser').value = (src.innerText != window.undefined ? src.innerText : src.innerHTML);
                document.getElementById('highlighteduser').innerHTML = sLabelResources.lblHighlightedUser + "<b style='text-align:center;'>" + document.forms[0].hdnselecteduser.value + "</b><br/><br/>";
             }
             else {
                 document.forms[0].hdnselecteduser.value = "";
                 document.getElementById('highlighteduser').innerHTML = "";
             }
             return false;

         }
         function HasChildren(src) {
             var parentTable = GetParentByTagName("table", src);
             var nxtSibling = parentTable.nextSibling;

             if (nxtSibling && nxtSibling.nodeType == 1)//check if nxt sibling is not null & is an element node 
             {
                 if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                 {
                     return true;
                 }
             }
         }
         function GetNodeValue(node) {
             //node value
             var nodeValue = "";
             var nodePath = node.href.substring(node.href.indexOf(",") + 2, node.href.length - 2);
             var nodeValues = nodePath.split("\\");
             if (nodeValues.length > 1)
                 nodeValue = nodeValues[nodeValues.length - 1];
             else
                 nodeValue = nodeValues[0].substr(1); return nodeValue;
         }
         function CheckUncheckChildren(childContainer, check) {
             var childChkBoxes = childContainer.getElementsByTagName("input");
             var childChkBoxCount = childChkBoxes.length;
             for (var i = 0; i < childChkBoxCount; i++) {
                 childChkBoxes[i].checked = check;
             }
         }


         function IfAnyChildChecked() {
             var childChkBoxes = document.getElementsByTagName("input");
             var bln = false;
             var no = 0;
             var count = 0;
             var childChkBoxCount = childChkBoxes.length;
             for (var i = 0; i < childChkBoxCount; i++) {
                 if (childChkBoxes[i].type == "checkbox") {
                     count++;
                     if (childChkBoxes[i].id != "LTVn0CheckBox") {
                         if (childChkBoxes[i].checked == true)
                             no++;
                     }
                 }
             }

             if (no == 0) {
                 return true;
             }
             if ((++no) == count) {
                 return true;
             }
             else {
                 return false;
             }
         }

         function CheckUncheckParentsOnly(srcChild, check, isChild) {
             //alert(srcChild + " " + check + " " + isChild);
             var parentDiv = GetParentByTagName("div", srcChild);
             var parentNodeTable = parentDiv.previousSibling;
             if (isChild == null)
                 isChild = false;

             if (parentNodeTable) {

                 if (isChild) {
                     if (!srcChild.checked) {
                         srcChild.style.backgroundColor = "white";

                         if (srcChild.id == "LTVn0CheckBox") {
                             SetBgColorForAllCheckBoxes();
                         }
                     }
                 }
                 var allchecked = IfAnySiblingsChecked(srcChild);
                 var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
                 if (inpElemsInParentTable.length > 0) {
                     var parentNodeChkBox = inpElemsInParentTable[0];
                     parentNodeChkBox.checked = check;

                     if (allchecked == false)
                         parentNodeChkBox.style.backgroundColor = "white";
                     else {

                         if (!AreAllSiblingsChecked(srcChild)) {
                             parentNodeChkBox.style.backgroundColor = "#999999";
                         }
                         else
                             parentNodeChkBox.style.backgroundColor = "white";
                     }
                     if (parentNodeChkBox.id == "LTVn0CheckBox") {
                         if (IfAnyChildChecked()) {
                             parentNodeChkBox.style.backgroundColor = "white";
                         }
                         else {
                             parentNodeChkBox.style.backgroundColor = "#999999";
                         }
                         return false;
                     }

                     //do the same recursively
                     var val = IfAnySiblingsChecked(parentNodeChkBox);
                     //setTimeout("CheckUncheckParentsOnly(" + parentNodeChkBox + "," + val + ")", 0);
                     setTimeout(function() { CheckUncheckParentsOnly(parentNodeChkBox, val) }, 0);
                     //CheckUncheckParentsOnly(parentNodeChkBox, val);
                 }
             }
         }
         function AreAllSiblingsChecked(chkBox) {
             var parentDiv = GetParentByTagName("div", chkBox);
             var childCount = parentDiv.childNodes.length;
             for (var i = 0; i < childCount; i++) {
                 if (parentDiv.childNodes[i].nodeType == 1) //check if the child node is an element node 
                 {
                     if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                         var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                         //if any of sibling nodes are not checked, return false
                         if (!prevChkBox.checked) {
                             return false;
                         }
                     }
                 }
             }
             return true;
         }
         function IfAnySiblingsChecked(chkBox) {
             var parentDiv = GetParentByTagName("div", chkBox);
             var childCount = parentDiv.childNodes.length;
             for (var i = 0; i < childCount; i++) {
                 if (parentDiv.childNodes[i].nodeType == 1) //check if the child node is an element node 
                 {
                     if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                         var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                         //if any of sibling nodes are not checked, return false 
                         if (prevChkBox.checked) {
                             return true;
                         }
                     }
                 }
             }
             return false;
         }
         function AreAllSiblingsUnChecked(chkBox) {
             var parentDiv = GetParentByTagName("div", chkBox);
             var childCount = parentDiv.childNodes.length;
             for (var i = 0; i < childCount; i++) {
                 if (parentDiv.childNodes[i].nodeType == 1) //check if the child node is an element node 
                 {
                     if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                         var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                         //if any of sibling nodes are not checked, return false 
                         if (prevChkBox.checked) {
                             return true;
                         }
                     }
                 }
             }
             return false;
         }
         //utility function to get the container of an element by tagname 
         function GetParentByTagName(parentTagName, childElementObj) {
             var parent = childElementObj.parentNode;
             while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
                 parent = parent.parentNode;
             }
             return parent;
         }

         //abisht for saving
         function Save() {
             var objDataChanged = window.parent.document.getElementById('DataChanged');
             if (objDataChanged != null) {
                 if (objDataChanged.value == "true") {
                     alert(CustomizePortalUserTree.datachanged);
                     // alert("Please save the portlets data to proceed.");
                     return false;
                 }
             }
             if (document.forms[0].hdngriddatachangedflag.value == "") {
                 alert(CustomizePortalUserTree.radiobutton);
                 //alert("Please select a radio button in User Specific Customization Grid to proceed.");
                 return false;
             }

             //    var result = tree.findAll(function(node) {
             //        return node.data.isChecked;
             //    });

             //    var allNodes = document.getElementById("selectednodes");
             //    allNodes.value = "";
             //    for (var ii = 0; ii < result.length; ii++) {
             //        var str = result[ii].data.label.replace(/\n/g, "").replace(/^\s*/, "").replace(/\s*$/, "");
             //        if (!result[ii].children[0]) {
             //            if (ii == (result.length - 1))
             //                allNodes.value = allNodes.value + str
             //            else
             //                allNodes.value = allNodes.value + str + ",";
             //        }
             //    }
             setTimeout(function() { }, 0);
             pleaseWait('start');
             setTimeout(function() { }, 0);
             SetBgColorForAllCheckBoxes();
             setTimeout(function() { }, 0);
             var obj = GetTreeHandle();
             var treename = "<%#LTV.ClientID%>"; ;
             var arr = obj.getElementsByTagName("a");
             var allNodes = document.getElementById("selectednodes");
             allNodes.value = "";
             for (var i = 0; i < obj.getElementsByTagName("a").length; i++) {

                 if (arr[i].className == treename + "_0") {
                     var r = arr[i].id.replace("<%#LTV.ClientID%>", "")
                     var r1 = r.replace("t", "n");
                     var checkboxname = "<%#LTV.ClientID%>" + r1 + "CheckBox";
                     var objCheckBox = document.getElementById(checkboxname);
                     if (document.getElementById(checkboxname).checked == true) {
                         var str = arr[i].innerText.replace(/\n/g, "").replace(/^\s*/, "").replace(/\s*$/, "");
                         if (!HasChildren(arr[i])) {
                             if (allNodes.value == "") {
                                 allNodes.value = str;
                             }
                             else {
                                 allNodes.value = allNodes.value + "," + str;
                             } 
                         }
                     }
                 } 
             }
             //document.forms[0].SelectedID.value = selectedId;
             window.parent.document.forms[0].hdnselecteduserid.value = "clearsession";
             //alert(window.parent.document.forms[0].hdnaction.value);
             //return false;
             document.forms[0].hdnrefreshparentflag.value = "true";
             //MITS 12376 Raman Bhatia : Adding null check
             if (window.top.document.getElementById('RMXisZapatecTreeWindowSaving') != null) {
                 window.top.document.getElementById('RMXisZapatecTreeWindowSaving').value = "true";
             }

             //window.parent.document.forms[0].submit();
             //window.parent.window.location = 'home?pg=riskmaster/Funds/choice';
             return true;
         }
         function pageLoaded() {
             if (document.forms[0].hdnrefreshparentflag.value == "true") {
                 window.parent.document.forms[0].submit();
             }
         }
         //tkatsarski: 12/10/14 - RMA-4897: If RMA user wasn't highlighted and the "Change Tab Order" was pressed nothing was happening. Alert is added so the user will know that RMA user has to be selected first. 
         function changeTabOrder() {
             if (document.getElementById('hdnselecteduser').value == '') {
                 alert("To change the tab order, first highlight a single user.");
                 return false;
             }//RMA 7097 gaurav singhal modified query string
             window.parent.window.open('ChangeTabOrderIndividual.aspx?username=' + document.getElementById('hdnselecteduser').value, 'ChangeTabOrderIndividual', 'status=0,toolbar=0,location=0,menubar=0,resizeble=0,scrollbars=1,height=360,width=710');
             return false;
         }
    </script>

</head>
<body onload="pageLoaded();">
    <form id="frmData" runat="server">
    <table>
    <div id="PleaseWaitDialog1_pleaseWaitFrame" class="pleaseWaitFrame" style="display:none">
        <img onclick="pleaseWait('stop');" src="/RiskmasterUI/Images/Loading1.gif" />
       
    </div>
        <tr>
            <td>
<%--                <input type="submit" value="Save Selected Portlet User Access Information" class="button"
                    style="width: 330px;" id="Savebtn" onclick="return Save();" />--%>
                    <asp:Button runat="server" 
                    Text="<%$ Resources:btnSave %>" class = "button" 
                    style = "width: 330px;" ID = "Savebtn" onclick="Savebtn_Click" />
            </td>
        </tr>
        <tr>
            <td>
                <input type="submit" runat="server" value="<%$ Resources:btnTabOrder %>" class="button"
                    style="width: 330px;" id="TabOrderbtn" onclick="changeTabOrder()" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="highlighteduser"></asp:Label>
            </td>
        </tr>
    </table>
    <div id="progressbar" style="display:none;"><div></div></div>
        <%--        <asp:TreeView ID="LTV" PopulateNodesFromClient="true" ShowLines="true" ShowExpandCollapse="true"
            runat="server" SelectedNodeStyle-Font-Bold="true" SelectedNodeStyle-BackColor="Cyan"
            ForeColor="Black" Target="_self" ExpandDepth="1" ShowCheckBoxes="All"
            DataSourceID="XmlDataSource1" OnSelectedNodeChanged = "LTV_SelectedNodeChanged" EnableViewState="true">
            <DataBindings>
                <asp:TreeNodeBinding DataMember="GroupAndUser" TextField="name" />
                <asp:TreeNodeBinding DataMember="Group" TextField="name" />
                <asp:TreeNodeBinding DataMember="User" ValueField="user_id" TextField="login_name" />
            </DataBindings>
        </asp:TreeView>
        <asp:XmlDataSource ID="XmlDataSource1" runat="server" DataFile="C:/Source/temp.xml">
        </asp:XmlDataSource>--%>
        <asp:TreeView ID="LTV" PopulateNodesFromClient="true" ShowLines="true" ShowExpandCollapse="true"
            runat="server" SelectedNodeStyle-Font-Bold="true" SelectedNodeStyle-BackColor="#d6d7d7"
            ForeColor="Black" Target="_self" ExpandDepth="1" ShowCheckBoxes="All" 
            EnableViewState="true">
            <Nodes>
                <asp:TreeNode Text="<%$ Resources:LTV %>" ShowCheckBox="true" Expanded="true" Value="0"
                    PopulateOnDemand="false"></asp:TreeNode>
            </Nodes>
        </asp:TreeView>
    </div>
    <input type="hidden" runat="server" value="" id="selectedportletid" />
    <input type="hidden" runat="server" value="" id="hdngriddatachangedflag" />
    <input type="hidden" runat="server" value="" id="hdnrefreshparentflag" />
    <input type="hidden" runat="server" value="" id="hdnselecteduser" />
    <input type="hidden" runat="server" value="" id="hdnSelectedLoginNames" />
    <input type="hidden" runat="server" value="" id="selectednodes" />
    </form>
</body>
</html>
