﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="RecentClaimConfig.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.UI_Admin.Customization.RecentClaimConfig" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
      <title>Recent Claim Header Configuration</title>
    <script src="../../../../Scripts/form.js" type="text/javascript"></script>
    <script src="../../../../Scripts/ExecutiveSummary.js" type="text/javascript"></script>
    <script type="text/javascript" language="Javascript">
        function setDefaultValue(name, value) {
            document.getElementById(name).value = value;
            return false;
        }
    </script>
</head>
<body onload="if(parent.MDIScreenLoaded != null) parent.MDIScreenLoaded();">
    <form id="frmData" runat="server">
    <input type="hidden" value="" id="hiddenSelectedUser" />
    <input type="hidden" value="DiaryConfig" id="hTabName" />
    <uc1:ErrorControl ID="ErrorControl" runat="server" />
    <div class="msgheader" id="formtitle">
        <asp:Label ID="lblformtitle" runat="server" Text="<%$ Resources:lblformtitle %>"></asp:Label>
        <!-- lblformtitle -->
    </div>
    <table border="0" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td>
                <table border="0" cellspacing="0" celpadding="0">
                    <tr>
                        <td class="Selected">
                            <a class="Selected" name="RecentClaimConfig">
                             <asp:Label ID="lblRecentClaimConfig" runat="server" Text="<%$ Resources:lblRecentClaimConfig %>"></asp:Label>
                            <%--<span style="text-decoration: none">Recent Claim Config</span>--%>
                            </a>
                        </td>
                        <td style="border-bottom: none; border-left-style: solid; border-left-color: #999999;
                            border-width: 2px; border-top-style: solid; border-top-width: 2px; border-top-color: #FFFFFF;">
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr valign="top">
            <td valign="top">
                <div style="position: relative; left: 0; top: 0; width: 780px; height: 280px; overflow: auto;valign:top" class="singletopborder">
                    <table border="0" cellspacing="0" celpadding="0" width="100%" valign="top" align="center">
                        <tr valign="top">
                            <td valign="top">
                                <table border="0" cellspacing="0" width="100%" align="center">
                                    <tr>
                                        <td width="20%" valign="top">
                                            <asp:CheckBox ID="chkClaimNumber" runat="server" Checked="true" Text="<%$ Resources:chkClaimNumber %>" />
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="<%$ Resources:chkClaimNumber %>" runat="server" id="txtClaimNumber" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <asp:Button CssClass="button" Width="50" ID="btnClaimNumber" runat="server" Text="<%$ Resources:btnDefault %>" tabindex="3" OnClick="btnClaimNumber_Click" />
                                        </td>
                                        <td width="20%" valign="top">
                                             <asp:CheckBox ID="chkLOB" runat="server" Checked="true" Text="<%$ Resources:chkLOB %>" />
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" id="txtLOB" runat="server"  value="<%$ Resources:chkLOB %>" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <asp:Button CssClass="button" Width="50" ID="btnLOB" runat="server" Text="<%$ Resources:btnDefault %>" OnClick="btnLOB_Click"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" valign="top">
                                            <asp:CheckBox ID="chkEventNumber" runat="server" Checked="true" Text="<%$ Resources:chkEventNumber %>" />
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="<%$ Resources:chkEventNumber %>" id="txtEventNumber" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <asp:Button CssClass="button" Width="50" ID="btnEventNumber" runat="server" Text="<%$ Resources:btnDefault %>" OnClick="btnEventNumber_Click"/>
                                        </td>
                                        <td width="20%" valign="top">
                                            <asp:CheckBox ID="chkDateOfEvent" runat="server" Checked="true" Text="<%$ Resources:chkDateOfEvent %>" />
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="<%$ Resources:chkDateOfEvent %>" runat="server" id="txtDateOfEvent" />
                                        </td>
                                        <td width="25%" valign="middle">
                                           <asp:Button CssClass="button" Width="50" ID="btnDateOfEvent" runat="server" Text="<%$ Resources:btnDefault %>" OnClick="btnDateOfEvent_Click"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" valign="top">
                                             <asp:CheckBox ID="chkDateOfClaim" runat="server" Checked="true" Text="<%$ Resources:chkDateOfClaim %>" />
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="<%$ Resources:chkDateOfClaim %>" runat="server" id="txtDateOfClaim" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <asp:Button CssClass="button" Width="50" ID="btnDateOfClaim" runat="server" Text="<%$ Resources:btnDefault %>" OnClick="btnDateOfClaim_Click"/>
                                        </td>
                                        <td width="20%" valign="top">
                                            <asp:CheckBox ID="chkClaimant" runat="server" Checked="true" Text="<%$ Resources:chkClaimant %>" onclick="CheckboxClick()" />
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="<%$ Resources:chkClaimant %>" id="txtClaimant" runat="server"/>
                                        </td>
                                        <td width="25%" valign="middle">
                                            <asp:Button CssClass="button" Width="50" ID="btnClaimant" runat="server" Text="<%$ Resources:btnDefault %>" OnClick="btnClaimant_Click"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" valign="top">
                                             <asp:CheckBox ID="chkClaimType" runat="server" Checked="true" Text="<%$ Resources:chkClaimType %>" />
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="<%$ Resources:chkClaimType %>" id="txtClaimType" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                             <asp:Button CssClass="button" Width="50" ID="btnClaimType" runat="server" Text="<%$ Resources:btnDefault %>" OnClick="btnClaimType_Click"/>
                                            
                                        </td>
                                        <td width="20%" valign="top">
                                            <asp:CheckBox ID="chkClaimStatus" runat="server" Checked="true" Text="<%$ Resources:chkClaimStatus %>" />
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="<%$ Resources:chkClaimStatus %>" id="txtClaimStatus" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <asp:Button CssClass="button" Width="50" ID="btnClaimStatus" runat="server" Text="<%$ Resources:btnDefault %>" OnClick="btnClaimStatus_Click"/>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <table border="0" cellspacing="0" celpadding="0" width="80%" valign="top">
                    <tr>
                        <td>
                            <div class="small">
                                <asp:Label ID="lbltext1" runat="server" Text="<%$ Resources:lbltext1 %>"></asp:Label> 
                                <%--Use this page to select the columns you want to see in Recent Claims Header.--%>
                                <br>
                                <asp:Label ID="lbltext2" runat="server" Text="<%$ Resources:lbltext2 %>"></asp:Label> 
                                <%--Once you have made all of your selections, click the "Save Configuration" button
                                to store the settings for use.--%>
                            </div>
                        </td>
                    </tr>
                </table>
                <table border="0" cellspacing="0" celpadding="0" width="30%" valign="top">
                    <tr>
                        <td>
                            <asp:Button ID="btnSaveConfiguration" runat="server" Text="<%$ Resources:btnSaveConfiguration %>" 
                                CssClass="button" onclick="btnSaveConfiguration_Click" /><%--Save Configuration--%>
                        </td>
                        <td>
                            <input type="submit" value="<%$ Resources:btnSelectAll %>" id="btnSelectAll" class="button"
                                onclick="return SelectAll();; " runat="server"  /><%--Select All--%>
                        </td>
                        <td>
                            <input type="submit" value="<%$ Resources:btnUnselectAll %>" id="btnUnselectAll" class="button"
                                onclick="return UnselectAll();; " runat="server" /><%--Unselect All--%>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
