﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Data;
using System.IO;
using Riskmaster.Common;
namespace Riskmaster.UI.UI.Utilities.UI_Admin.Customization
{
    public partial class ChangeTabOrder : System.Web.UI.Page
    {
        XmlElement objLstRow = null;
        XmlElement objRowTxt = null;
        XmlElement objElement = null;
        XmlDocument p_objXmlDocument = new XmlDocument();

        protected void Page_Load(object sender, EventArgs e)
        {
            imgMoveUp.Attributes.Add("onclick", "javascript:return ChangeTabOrderUpdateInfo();");
            imgMoveDown.Attributes.Add("onclick", "javascript:return ChangeTabOrderUpdateInfo();");
            if(IsPostBack)
            {
                
                if(CommonPortletXML.Text!="")
                {
                    p_objXmlDocument.LoadXml(CommonPortletXML.Text);

                    DataSet usersRecordsSet = null;


                    usersRecordsSet = ConvertXmlDocToDataSet(p_objXmlDocument);
                    GridView gvChangetabOrderGrid = (GridView)this.Form.FindControl("gvChangetabOrderGrid");
                    usersRecordsSet.Tables["option"].DefaultView.Sort = "SequenceId" + " " + "ASC";
                    
                    gvChangetabOrderGrid.DataSource = usersRecordsSet.Tables["option"].DefaultView;
                    
                    gvChangetabOrderGrid.DataBind();
                    GridRowCount.Text = gvChangetabOrderGrid.Rows.Count.ToString();
                    //objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//AllUsersPortlets");
                    //objLstRow = p_objXmlDocument.CreateElement("option");
                    //objRowTxt = p_objXmlDocument.CreateElement("SequenceId");
                    //objLstRow.AppendChild(objRowTxt);
                    //objRowTxt = p_objXmlDocument.CreateElement("RowID");
                    //objLstRow.AppendChild(objRowTxt);
                    //objRowTxt = p_objXmlDocument.CreateElement("Name");
                    //objLstRow.AppendChild(objRowTxt);
                    //objRowTxt = p_objXmlDocument.CreateElement("urlPortlet");
                    //objLstRow.AppendChild(objRowTxt);
                    //objElement.AppendChild(objLstRow);
                    //ChangetabOrderGrid.BindData(p_objXmlDocument);
                } // if
            } // if
        }

        public DataSet ConvertXmlDocToDataSet(XmlDocument diaryDoc)
        {
            DataSet dSet = null;
            try
            {
                dSet = new DataSet();
                DataTable dt = new DataTable("option");


                DataColumn dc = new DataColumn();
                dc.ColumnName = "SequenceId";
                dc.DataType = System.Type.GetType("System.Int32");
                dt.Columns.Add(dc);


                dc = new DataColumn();
                dc.ColumnName = "RowID";
                dc.DataType = System.Type.GetType("System.String");
                dt.Columns.Add(dc);

                dc = new DataColumn();
                dc.ColumnName = "Name";
                dc.DataType = System.Type.GetType("System.String");
                dt.Columns.Add(dc);

                dc = new DataColumn();
                dc.ColumnName = "urlPortlet";
                dc.DataType = System.Type.GetType("System.String");
                dt.Columns.Add(dc);

                foreach (XmlNode node in diaryDoc.SelectNodes("//option"))
                {
                    DataRow dr = dt.NewRow();
                    dr["RowID"] = Conversion.ConvertObjToStr(node.SelectSingleNode("RowID").InnerText);
                    dr["SequenceId"] = Conversion.ConvertObjToInt(node.SelectSingleNode("SequenceId").InnerText);
                    dr["Name"] = Conversion.ConvertObjToStr(node.SelectSingleNode("Name").InnerText);
                    dr["urlPortlet"] = Conversion.ConvertObjToStr(node.SelectSingleNode("urlPortlet").InnerText);
                    dt.Rows.Add(dr);
                }
                //dSet.ReadXml(new XmlNodeReader(diaryDoc));
                dSet.Tables.Add(dt);

                return dSet;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }
        protected void imgMoveUp_Click(object sender, ImageClickEventArgs e)
        {
            if (SelectedID.Text != "1" && SelectedID.Text != "0")
            {
                foreach (XmlNode node in p_objXmlDocument.SelectNodes("//option/RowID"))
                {
                    if (node.InnerText == SelectedID.Text)
                    {
                        //objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//PortletSettings/AllUsersPortlets/option/SequenceId");
                        //objElement.InnerText = (int.Parse(SelectedID.Text) - 1).ToString();
                        node.PreviousSibling.InnerText = (int.Parse(SelectedID.Text) - 1).ToString();
                    } // if
                    if (node.InnerText == (int.Parse(SelectedID.Text) - 1).ToString())
                    {
                        //objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//PortletSettings/AllUsersPortlets/option/SequenceId");
                        //objElement.InnerText = SelectedID.Text;
                        node.PreviousSibling.InnerText = SelectedID.Text;
                    } // if

                }
                foreach (XmlNode node in p_objXmlDocument.SelectNodes("//option/RowID"))
                {
                    node.InnerText = node.PreviousSibling.InnerText;
                }

                DataSet usersRecordsSet = null;

                usersRecordsSet = ConvertXmlDocToDataSet(p_objXmlDocument);
                GridView gvChangetabOrderGrid = (GridView)this.Form.FindControl("gvChangetabOrderGrid");
                usersRecordsSet.Tables["option"].DefaultView.Sort = "SequenceId" + " " + "ASC";
               // usersRecordsSet.Tables["option"].DefaultView.
                
                gvChangetabOrderGrid.DataSource = usersRecordsSet.Tables["option"].DefaultView;

                DataRow[] dr = usersRecordsSet.Tables["option"].Select("0=0", "SequenceId ASC");
                DataTable dt = new DataTable("option");
                foreach(DataColumn dc in usersRecordsSet.Tables["option"].Columns)
                {
                    dt.Columns.Add(dc.ColumnName);
                } // foreach

                foreach(DataRow drTemp in dr)
                {
                    dt.ImportRow(drTemp);
                } // foreach

                gvChangetabOrderGrid.DataBind();
                StringWriter objWriter = new StringWriter();
                dt.WriteXml(objWriter,true);
                string sChangedPortletXML = objWriter.ToString();
                XmlDocument objChangedPortletXML = new XmlDocument();
                objChangedPortletXML.LoadXml(sChangedPortletXML);
                string sOption = objChangedPortletXML.InnerXml.ToString();
                
                //SortChanged.Text = "true";
                CommonPortletXML.Text = sOption;
            } // if            
        }

        protected void imgMoveDown_Click(object sender, ImageClickEventArgs e)
        {
            if (SelectedID.Text != GridRowCount.Text && SelectedID.Text != "0")
            {
                foreach (XmlNode node in p_objXmlDocument.SelectNodes("//option/RowID"))
                {
                    if (node.InnerText == SelectedID.Text)
                    {
                        //objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//PortletSettings/AllUsersPortlets/option/SequenceId");
                        //objElement.InnerText = (int.Parse(SelectedID.Text) - 1).ToString();
                        node.PreviousSibling.InnerText = (int.Parse(SelectedID.Text) + 1).ToString();
                    } // if
                    if (node.InnerText == (int.Parse(SelectedID.Text) + 1).ToString())
                    {
                        //objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//PortletSettings/AllUsersPortlets/option/SequenceId");
                        //objElement.InnerText = SelectedID.Text;
                        node.PreviousSibling.InnerText = SelectedID.Text;
                    } // if

                }
                foreach (XmlNode node in p_objXmlDocument.SelectNodes("//option/RowID"))
                {
                    node.InnerText = node.PreviousSibling.InnerText;
                }

                DataSet usersRecordsSet = null;

                usersRecordsSet = ConvertXmlDocToDataSet(p_objXmlDocument);
                GridView gvChangetabOrderGrid = (GridView)this.Form.FindControl("gvChangetabOrderGrid");
                usersRecordsSet.Tables["option"].DefaultView.Sort = "SequenceId" + " " + "ASC";

                gvChangetabOrderGrid.DataSource = usersRecordsSet.Tables["option"].DefaultView;

                DataRow[] dr = usersRecordsSet.Tables["option"].Select("0=0", "SequenceId ASC");
                DataTable dt = new DataTable("option");
                foreach (DataColumn dc in usersRecordsSet.Tables["option"].Columns)
                {
                    dt.Columns.Add(dc.ColumnName);
                } // foreach

                foreach (DataRow drTemp in dr)
                {
                    dt.ImportRow(drTemp);
                } // foreach
                gvChangetabOrderGrid.DataBind();

                StringWriter objWriter = new StringWriter();
                dt.WriteXml(objWriter, true);
                string sChangedPortletXML = objWriter.ToString();
                XmlDocument objChangedPortletXML = new XmlDocument();
                objChangedPortletXML.LoadXml(sChangedPortletXML);
                string sOption = objChangedPortletXML.InnerXml.ToString();

                //SortChanged.Text = "true";
                CommonPortletXML.Text = sOption;
            }
        }

    }
}
