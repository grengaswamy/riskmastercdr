﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="CustomizeRMXPortal.aspx.cs"
    Inherits="Riskmaster.UI.Utilities.UI_Admin.Customization.CustomizeRMXPortal" ValidateRequest="false" %>

<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" TagName="UserControlDataGrid"
    TagPrefix="dg" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>RMX Portal Customization</title>
    <link rel="stylesheet" href="../../../../App_Themes/RMX_Default/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../../../Content/zpcal/themes/system.css" type="text/css" />

    <script language="javaScript" src="../../../../Scripts/form.js" type="text/javascript"></script>

    <script language="javaScript" src="../../../../Scripts/Utilities.js" type="text/javascript"></script>

    <script language="JavaScript" src="../../../../Scripts/drift.js" type="text/javascript"></script>

    <script language="JavaScript" src="../../../../Scripts/WaitDialog.js" type="text/javascript">        { var i; }
    </script>
    <script language="JavaScript">
    function CheckPortalSetting() {

            var URL = document.getElementById("txtHomePage").value.toLowerCase();
            var hdnURL = document.getElementById("hdnURL").value.toLowerCase();
            if ((!(URL.match('riskmaster.aspx'))) && (hdnURL.match('riskmaster.aspx'))) {
                var ret = false;
                // ret = confirm("Do you really want to change the default home page?");
                ret = confirm(CustomizeRMXPortalValidations.changehomepage);
                if (ret) {

                // rsolanki2 : mits 24221
                document.getElementById("CommonPortletsGrid_Action").value = "save";
                document.getElementById("IndividualPortletsGrid_Action").value = "save";

                setDataChanged(false);
                document.getElementById("SysPageDataChanged").value = false;
                document.getElementById("DataChanged").value = false;
                                
                return true;
            }
            else {
                return false;
            }

        }
        else {
            // rsolanki2 : mits 24221
            document.getElementById("CommonPortletsGrid_Action").value = "save";
            document.getElementById("IndividualPortletsGrid_Action").value = "save";
            
            document.getElementById("DataChanged").value = false;
            document.getElementById("SysPageDataChanged").value = false;
            setDataChanged(false);
            m_DataChanged = false;
            return true;
        }
   
     return false;
    }
    </script>
</head>
<body onload="pageLoaded();parent.MDIScreenLoaded()">
  
    <form id="frmData" runat="server">
    <asp:HiddenField ID="hdnURL" Value="" runat="server" />
    <rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />
    <input type="hidden" name="hTabName" />
    <div id="toolbardrift" name="toolbardrift" class="toolbardrift">
        <table border="0" class="toolbar" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center" valign="middle" height="32">
                    <div class="toolBarButton" runat="server" id="div_save" xmlns="">
                        <asp:ImageButton runat="server" src="../../../../Images/save.gif" Width="28" Height="28"
                            border="0" ID="save" AlternateText="<%$ Resources: imgbtnSave %>" ValidationGroup="vgSave" onmouseover="this.src='../../../../Images/save2.gif';this.style.zoom='110%'"
                            onmouseout="this.src='../../../../Images/save.gif';this.style.zoom='100%'" OnClientClick=" return CheckPortalSetting();" OnClick="save_Click" /></div>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <div class="msgheader" id="formtitle">
    <asp:Label ID="lblmsgheader" runat="server"  Text="<%$ Resources:lblmsgheader %>"></asp:Label>
      <!--  RMX Portal Customization -->
      </div>
    <div class="errtextheader">
    </div>
    <br />
    <table border="0">
        <tr>
            <td>
            <div class="tabGroup" id="TabsDivGroup" runat="server">
                <div class="Selected" nowrap="true" name="TABSCommonPortalConfiguration" id="TABSCommonPortalConfiguration" runat="server">
                   <a class="Selected" href="#" onclick="tabChange(this.name);return false;" name="CommonPortalConfiguration"
                            id="LINKTABSCommonPortalConfiguration">
                           <!-- <span style="text-decoration: none">Portlet Customization</span> !-->
                           <span style="text-decoration: none"><asp:Label ID="lbltabGroup1" runat="server"  Text="<%$ Resources:lbltabGroup1 %>"></asp:Label> </span> 
                            </a>
                </div>
                <div class="tabSpace" runat="server" id="TBSCommonPortalConfiguration">
                  <nbsp />
                  <nbsp />
                </div>
                <div class="NotSelected" nowrap="true" name="TABSPortalConfiguration" id="TABSPortalConfiguration" runat="server">
                  <a class="NotSelected1" href="#" onclick="tabChange(this.name);return false;" name="PortalConfiguration"
                            id="LINKTABSPortalConfiguration">
                             <span style="text-decoration: none"><asp:Label ID="lbltabGroup2" runat="server"  Text="<%$ Resources:lbltabGroup2 %>"></asp:Label></span>
                            <!--<span style="text-decoration: none">User Specific Customization</span> !-->
                            </a>
                </div>
                <div class="tabSpace" runat="server" id="TBSPortalConfiguration">
                  <nbsp />
                  <nbsp />
                </div>
            </div>  
         
            <div class="singletopborder" style="display: block; position: relative; left: 0;
                top: 0;">
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <table border="0" cellspacing="0" cellpadding="0" name="FORMTABCommonPortalConfiguration"
                                id="FORMTABCommonPortalConfiguration">
                                <tr id="">
                                    <td>
                                         <asp:Label ID="lblFORMTABCommonPortalConfiguration" runat="server"  Text="<%$ Resources:lblFORMTABCommonPortalConfiguration %>"></asp:Label>
                                        <!-- Home Page of RMX Portal: !-->
                                         &nbsp;&nbsp;
                                    </td>
                                    <td>
                                        <div title="" style="padding: 0px; margin: 0px">
                                            <input type="text" runat="server" id="txtHomePage" size="30" tabindex="3" onchange="setDataChanged(true);"/>
                                        </div>
                                    </td>
                                </tr>
                                <tr id="Tr1">
                                    <td colspan="2">
                                        <input type="submit" runat="server" value="<%$ Resources:btnLogo %>" class="button" onclick="window.open('RMXPortalUploadBanner.aspx', 'UploadBanner', 'status=0,toolbar=0,location=0,menubar=0,resizeble=0,scrollbars=1,height=360,width=710'); return false;"
                                            id="btnLogo" tabindex="4" />
                                    </td>
                                </tr>
                                <tr id="Tr2">
                                    <td colspan="6">
                                        <hr width="100%" size="1" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="800px" colspan = "2">
                                        <div style="overflow: auto; width: 100%; position: relative; height: 200px">
                                            <dg:UserControlDataGrid runat="server" ID="CommonPortletsGrid" GridName="CommonPortletsGrid"
                                                GridTitle="<%$ Resources:gvCommonPortletsGrid %>" Target="PortletSettings/AllUsersPortlets"
                                                Ref="" Unique_Id="RowID" ShowRadioButton="true" ShowCheckBox="False" Width="680px"
                                                Height="150px" ShowHeader="True" LinkColumn="" PopupWidth="470" PopupHeight="170"
                                                Type="GridAndButtons" ImgNewToolTip="<%$ Resources:ttNew %>" ImgEditToolTip="<%$ Resources:ttEdit %>" ImgDeleteToolTip="<%$ Resources:ttDelete %>" />
                                        </div>
                                    </td>
                                </tr>
                                <tr id="Tr3">
                                    <td colspan="2">
                                        <input type="submit" runat="server" value="<%$ Resources:btnChangeTabOrder %>" class="button" onclick="window.open('ChangeTabOrder.aspx', 'ChangeTabOrder', 'status=0,toolbar=0,location=0,menubar=0,resizeble=0,scrollbars=1,height=360,width=710'); return false;"
                                            id="btnChangeTabOrder" tabindex="4" />
                                        <%--                                            <input type="submit" value="Change Tab Order" class="button" onclick="openChangeTabOrderWindow();"
                                            id="btnChangeTabOrder" tabindex="4" />--%>
                                    </td>
                                </tr>
                            </table>
                            <table border="0" cellspacing="0" cellpadding="0" name="FORMTABPortalConfiguration"
                                id="FORMTABPortalConfiguration" style="display: none;">
                                <tr id="Tr4">
                                    <td width="800px">
                                        <div style="overflow: auto; width: 99%; position: relative; height: 160px">
                                            <dg:UserControlDataGrid runat="server" ID="IndividualPortletsGrid" GridName="IndividualPortletsGrid"
                                                GridTitle="<%$ Resources:gvIndividualPortletsGrid %>" Target="/PortletSettings/IndividualPortletsList"
                                                Ref="" Unique_Id="RowID" ShowRadioButton="true" ShowCheckBox="False" Width="680px"
                                                Height="135px" ShowHeader="True" LinkColumn="" PopupWidth="470" PopupHeight="170"
                                                Type="GridAndButtons" OnClick="UpdateTree(this);" ImgNewToolTip="<%$ Resources:ttNew %>" ImgEditToolTip="<%$ Resources:ttEdit %>" ImgDeleteToolTip="<%$ Resources:ttDelete %>" />
                                        </div>
                                    </td>
                                </tr>
                                <tr id="Tr5">
                                    <td colspan="1">
                                        <div >
                                            <iframe name="portalzapatectree" width="86%" height="300px" align="left" src="PortalUserTree.aspx">
                                            </iframe>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top">
                        </td>
                    </tr>
                </table>
            </div>
            </td>
        </tr>
    </table>    
            <table>
                <tr>
                    <td>
                        <uc2:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
                    </td>
                </tr>
            </table>
        
    <asp:TextBox Style="display: none" runat="server" ID="CommonPortletsSelectedId" />
    <asp:TextBox Style="display: none" runat="server" ID="CommonPortletsGrid_Action" />
    <asp:TextBox Style="display: none" runat="server" ID="CommonPortletsGrid_RowAddedFlag" />
    <asp:TextBox Style="display: none" runat="server" ID="CommonPortletsGrid_RowDeletedFlag" />
    <asp:TextBox Style="display: none" runat="server" ID="CommonPortletsGrid_RowEditFlag" />
    <asp:TextBox Style="display: none" runat="server" ID="AddedControlValue" value="" />
    <asp:TextBox Style="display: none" runat="server" ID="CommonPortletXML" value="" />
    <asp:TextBox Style="display: none" runat="server" ID="txtCommonPortletGenXML" value="" />
    <asp:TextBox Style="display: none" runat="server" ID="SequenceID" value="" />
    <asp:TextBox Style="display: none" runat="server" ID="IndividualPortletsSelectedId" />
    <asp:TextBox Style="display: none" runat="server" ID="IndPortletsSelectedIdCollection" />
    <asp:TextBox Style="display: none" runat="server" ID="IndividualPortletsGrid_Action" />
    <asp:TextBox Style="display: none" runat="server" ID="IndividualPortletsGrid_RowAddedFlag" />
    <asp:TextBox Style="display: none" runat="server" ID="IndividualPortletsGrid_RowDeletedFlag" />
    <asp:TextBox Style="display: none" runat="server" ID="IndividualPortletsGrid_RowEditFlag" />
    <asp:TextBox Style="display: none" runat="server" ID="AddedControlValueInd" value="" />
    <asp:TextBox Style="display: none" runat="server" ID="IndividualPortletXML" value="" />
    <asp:TextBox Style="display: none" runat="server" ID="txtIndPortletGenXML" value="" />
    <asp:TextBox Style="display: none" runat="server" ID="SequenceIDInd" value="" />
    <asp:TextBox Style="display: none" runat="server" ID="txtMaxId" value="" Text="1" />
    <input type="text" name="" value="" id="SysClassName" style="display: none" />
    <input type="text" name="" value="" id="SysCmd" style="display: none" />
    <input type="text" name="" value="" id="SysFormIdName" style="display: none" />
    <input type="text" name="" value="" id="SysFormPIdName" style="display: none" />
    <input type="text" name="" value="" id="SysFormName" style="display: none" />
    <input type="text" runat="server" value="" style="display: none" id="selectedusername" />
    <input type="text" runat="server" value="" style="display: none" id="hdnaction" />
    <input type="text" runat="server" value="" style="display: none" id="hdnselectedportletsdata" />
    <input type="text" runat="server" value="" style="display: none" id="dltallflag" />
    <input type="text" runat="server" value="" style="display: none" id="hdnselecteduserid" />
    <input type="text" runat="server" value="" style="display: none" id="DataChanged" />
    <input type="text" runat="server" value="" style="display: none" id="SysPageDataChanged" />
    </form>
</body>
</html>
