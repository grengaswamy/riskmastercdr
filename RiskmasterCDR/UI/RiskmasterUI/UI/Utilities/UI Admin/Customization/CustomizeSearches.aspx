<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomizeSearches.aspx.cs" Inherits="Riskmaster.UI.Utilities.UI_Admin.Customization.CustomizeSearches" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml">
 <head runat="server">
  <title>System Customization (Searches)</title>
  <link rel="stylesheet" href="../../../../Content/system.css" type="text/css"/>
  <script language="javaScript" src="../../../../Scripts/form.js" type="text/javascript"></script>
 </head>
 <body class="10pt" onload="parent.MDIScreenLoaded();">
  <form id="frmData" name="frmData" method="post" runat="server">
  <uc1:ErrorControl ID="ErrorControl1" runat="server" /> 
  <div class="msgheader">System Customization</div>
  <table border="0" cellspacing="0" cellpadding="0" width="100%">
    <tr><td><br /></td></tr>
    <tr>
     <td colspan="2" class="ctrlgroup">&nbsp;Search Links</td>
    </tr>
    <tr>
     <td width="40%"></td>
     <td width="60%"><i>Show</i></td>
    </tr>
    <tr>
     <td class="datatd">Claims</td>
     <td class="datatd">
        <asp:CheckBox ID="Claims" FormType="Customize" runat="server" type="checkbox" rmxref="Instance/Document/customize_search/RMAdminSettings/Search/Claim" appearance="full"/></td>
    </tr>
    <tr>
     <td class="datatd1">Events</td>
     <td class="datatd1">
        <asp:CheckBox ID="Events" FormType="Customize" runat="server" type="checkbox"  rmxref="Instance/Document/customize_search/RMAdminSettings/Search/Event" appearance="full" /></td>
    </tr>
    <tr>
     <td class="datatd">Employees</td>
     <td class="datatd">
        <asp:CheckBox ID="Employee" FormType="Customize" runat="server" type="checkbox" rmxref="Instance/Document/customize_search/RMAdminSettings/Search/Employee" appearance="full" /></td>
    </tr>
    <tr>
     <td class="datatd1">Entities</td>
     <td class="datatd1">
        <asp:CheckBox ID="Entity" FormType="Customize" runat="server" type="checkbox" rmxref="Instance/Document/customize_search/RMAdminSettings/Search/Entity" appearance="full" /></td>
    </tr>
    <tr>
     <td class="datatd">Vehicles</td>
     <td class="datatd">
        <asp:CheckBox ID="Vehicle" FormType="Customize" runat="server" type="checkbox" rmxref="Instance/Document/customize_search/RMAdminSettings/Search/Vehicle" appearance="full" /></td>
    </tr>
    <tr>
     <td class="datatd1">Policies</td>
     <td class="datatd1">
        <asp:CheckBox ID="Policy" FormType="Customize" runat="server" type="checkbox" rmxref="Instance/Document/customize_search/RMAdminSettings/Search/Policy" appearance="full" /></td>
    </tr>
    <tr>
     <td class="datatd">Funds</td>
     <td class="datatd">
        <asp:CheckBox ID="Fund" FormType="Customize" runat="server" type="checkbox" rmxref="Instance/Document/customize_search/RMAdminSettings/Search/Fund" appearance="full" /></td>
    </tr>
    <tr>
     <td class="datatd1">Patients</td>
     <td class="datatd1">
        <asp:CheckBox ID="Patient" FormType="Customize" runat="server" type="checkbox" rmxref="Instance/Document/customize_search/RMAdminSettings/Search/Patient" appearance="full" /></td>
    </tr>
    <tr>
     <td class="datatd">Physicians</td>
     <td class="datatd">
        <asp:CheckBox ID="Physician" FormType="Customize" runat="server" type="checkbox" rmxref="Instance/Document/customize_search/RMAdminSettings/Search/Physician" appearance="full" /></td>
    </tr>
    <tr>
     <td class="datatd1">Leave Plan</td>
     <td class="datatd1">
        <asp:CheckBox ID="LeavePlan" FormType="Customize" runat="server" type="checkbox" rmxref="Instance/Document/customize_search/RMAdminSettings/Search/LeavePlan" appearance="full" /></td>
    </tr>
    <tr>
     <td class="datatd">Admin Tracking</td>
     <td class="datatd">
        <asp:CheckBox ID="AdminTrack" FormType="Customize" runat="server" type="checkbox" rmxref="Instance/Document/customize_search/RMAdminSettings/Search/AdminTrack" appearance="full" /></td>
    </tr>
    <tr>
     <td class="datatd1">Medical Staff</td>
     <td class="datatd1">
        <asp:CheckBox ID="MedStaff" FormType="Customize" runat="server" type="checkbox" rmxref="Instance/Document/customize_search/RMAdminSettings/Search/MedStaff" appearance="full" /></td>
    </tr>
    <tr>
     <td class="datatd">Disability Plan</td>
     <td class="datatd">
        <asp:CheckBox ID="DisPlan" FormType="Customize" runat="server" type="checkbox" rmxref="Instance/Document/customize_search/RMAdminSettings/Search/DisPlan" appearance="full" /></td>
    </tr>
    <%--Anu Tennyson: MITS 18291 on 10/26/2009 Starts--%>
    <tr>
     <td class="datatd1">Property</td>
     <td class="datatd1">
        <asp:CheckBox ID="Property" FormType="Customize" runat="server" type="checkbox" rmxref="Instance/Document/customize_search/RMAdminSettings/Search/Property" appearance="full" /></td>
    </tr>
    <%--Anu Tennyson: MITS 18291 on 10/26/2009 Ends--%>
          <%--aaggarwal29: MITS 36415 start--%>
     <tr>
     <td class="datatd">Catastrophe</td>
     <td class="datatd">
        <asp:CheckBox ID="Catastrophe" FormType="Customize" runat="server" type="checkbox" rmxref="Instance/Document/customize_search/RMAdminSettings/Search/Catastrophe" appearance="full" Checked="true" /></td>
    </tr>
     <%--aaggarwal29: MITS 36415 end--%>
    <%--Added by Amitosh for R8 enhancements--%>
        <tr>
     <td class="datatd1">Diary</td>
     <td class="datatd1">
        <asp:CheckBox ID="Diary" FormType="Customize" runat="server" type="checkbox" rmxref="Instance/Document/customize_search/RMAdminSettings/Search/Diary" appearance="full" Checked="true"/></td>
    </tr>
    <tr>
     <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
     <td colspan="2">
        <asp:Button ID="btnSave" runat="server" OnClick="Save" type="submit" Text="Save" class="button" style="width:100"/>  
        <asp:Button ID="btnRefresh" runat="server" OnClick="Refresh" type="submit" Text="Refresh" class="button" style="width:100"/>
     </td>
    </tr>
   </table>
  </form>
 </body>
</html>
