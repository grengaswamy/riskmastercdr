﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.UI.Utilities.UI_Admin.Customization
{
    /// <summary>
    /// Author  :   Gurpreet Singh Bindra
    /// Dated   :   15th, Apr 2014
    /// Purpose :   MITS#35365 to handle the person involved MDI menu.
    /// </summary>
    /// <returns></returns>
    public partial class PersonInvMenuConfig : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    LoadPersonInvMenuConfigDetails();
                }
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnSaveConfiguration_Click(object sender, EventArgs e)
        {
            try
            {
                SetPersonInvMenuConfigDetails();
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        
        #region Message Template
        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetConfigMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
               <Authorization></Authorization>
               <Call>
                  <Function>PersonInvMenuConfigAdaptor.GetPersonInvMenuConfig</Function>
               </Call>
               <Document>
                  <PersonInvMenuConfig>
                     <AddExistingEmployee></AddExistingEmployee> 
                     <AddNewEmployee></AddNewEmployee> 
                     <AddExistingMedicalStaff></AddExistingMedicalStaff> 
                     <AddNewMedicalStaff></AddNewMedicalStaff>
                     <AddExistingOtherPerson></AddExistingOtherPerson>
                     <AddNewOtherPerson></AddNewOtherPerson> 
                     <AddExistingDriver></AddExistingDriver> 
                     <AddNewDriver></AddNewDriver> 
                     <AddExistingPatient></AddExistingPatient> 
                     <AddNewPatient></AddNewPatient> 
                     <AddExistingPhysician></AddExistingPhysician> 
                     <AddNewPhysician></AddNewPhysician> 
                     <AddExistingWitness></AddExistingWitness> 
                     <AddNewWitness></AddNewWitness>                           
                  </PersonInvMenuConfig>
               </Document>
            </Message>
            ");

            return oTemplate;
        }

        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement SetConfigMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization></Authorization>
              <Call>
                <Function>PersonInvMenuConfigAdaptor.SetPersonInvMenuConfig</Function>
              </Call>
              <Document>
                 <PersonInvMenuConfig>
                          <AddExistingEmployee></AddExistingEmployee> 
                          <AddNewEmployee></AddNewEmployee> 
                          <AddExistingMedicalStaff></AddExistingMedicalStaff> 
                          <AddNewMedicalStaff></AddNewMedicalStaff>
                          <AddExistingOtherPerson></AddExistingOtherPerson>
                          <AddNewOtherPerson></AddNewOtherPerson> 
                          <AddExistingDriver></AddExistingDriver> 
                          <AddNewDriver></AddNewDriver> 
                          <AddExistingPatient></AddExistingPatient> 
                          <AddNewPatient></AddNewPatient> 
                          <AddExistingPhysician></AddExistingPhysician> 
                          <AddNewPhysician></AddNewPhysician> 
                          <AddExistingWitness></AddExistingWitness> 
                          <AddNewWitness></AddNewWitness>                           
                    </PersonInvMenuConfig>
              </Document>
            </Message>
            ");

            return oTemplate;
        }
        #endregion Message Template

        #region Private Methods
        /// <summary>
        /// get the Person Involved Menu configuration 
        /// </summary>
        /// <returns></returns>
        private XmlDocument GetPersonInvMenuConfigDetails()
        {
            XElement messageElement = null;
            string sReturn = string.Empty;
            XmlDocument resultDoc = null;

            try
            {
                messageElement = GetConfigMessageTemplate();
                sReturn = AppHelper.CallCWSService(messageElement.ToString());
                resultDoc = new XmlDocument();
                resultDoc.LoadXml(sReturn);
                return resultDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Sets the person Invloved menu configuration
        /// </summary>
        /// <returns></returns>
        private XmlDocument SetPersonInvMenuConfigDetails()
        {
            XElement messageElement = null;
            XElement tempElement = null;
            string sReturn = string.Empty;
            XmlDocument resultDoc = null;

            try
            {
                messageElement = SetConfigMessageTemplate();

                tempElement = messageElement.XPathSelectElement("./Document/PersonInvMenuConfig/AddExistingEmployee");
                if (chkEmployeeExisting.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/PersonInvMenuConfig/AddNewEmployee");
                if (chkEmployeeNew.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/PersonInvMenuConfig/AddExistingMedicalStaff");
                if (chkMedicalStaffExting.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/PersonInvMenuConfig/AddNewMedicalStaff");
                if (chkMedicalStaffNew.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/PersonInvMenuConfig/AddExistingOtherPerson");
                if (chkOtherPersonExt.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/PersonInvMenuConfig/AddNewOtherPerson");
                if (chkOtherPersonNew.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/PersonInvMenuConfig/AddExistingDriver");
                if (chkExtDriver.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/PersonInvMenuConfig/AddNewDriver");
                if (chkNewDriver.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/PersonInvMenuConfig/AddExistingPatient");
                if (chkExtPatient.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/PersonInvMenuConfig/AddNewPatient");
                if (chkNewPatient.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/PersonInvMenuConfig/AddExistingPhysician");
                if (chkExtPhysician.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/PersonInvMenuConfig/AddNewPhysician");
                if (chkNewPhysician.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/PersonInvMenuConfig/AddExistingWitness");
                if (chkExtWitness.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/PersonInvMenuConfig/AddNewWitness");
                if (chkNewWitness.Checked)
                {
                    tempElement.Value = "True";
                }

                sReturn = AppHelper.CallCWSService(messageElement.ToString());
                resultDoc = new XmlDocument();
                resultDoc.LoadXml(sReturn);
                return resultDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Load the Values from the databse in to the respective controls
        /// </summary>
        private void LoadPersonInvMenuConfigDetails()
        {
            XmlDocument resultDoc = null;
            XmlNode menuConfigNode = null;

            resultDoc = GetPersonInvMenuConfigDetails();

            if (resultDoc != null)
            {
                //set obtained values of Config
                menuConfigNode = resultDoc.SelectSingleNode("//Document/PersonInvMenuConfig");

                if (menuConfigNode != null)
                {
                    if (menuConfigNode.SelectSingleNode("AddExistingEmployee") != null &&
                        string.Compare(menuConfigNode.SelectSingleNode("AddExistingEmployee").InnerText, "True", true) == 0)
                    {
                        chkEmployeeExisting.Checked = true;
                    }
                    else
                    {
                        chkEmployeeExisting.Checked = false;
                    }

                    if (menuConfigNode.SelectSingleNode("AddNewEmployee") != null &&
                        string.Compare(menuConfigNode.SelectSingleNode("AddNewEmployee").InnerText, "True", true) == 0)
                    {
                        chkEmployeeNew.Checked = true;
                    }
                    else
                    {
                        chkEmployeeNew.Checked = false;
                    }

                    if (menuConfigNode.SelectSingleNode("AddExistingMedicalStaff") != null &&
                        string.Compare(menuConfigNode.SelectSingleNode("AddExistingMedicalStaff").InnerText, "True", true) == 0)
                    {
                        chkMedicalStaffExting.Checked = true;
                    }
                    else
                    {
                        chkMedicalStaffExting.Checked = false;
                    }

                    if (menuConfigNode.SelectSingleNode("AddNewMedicalStaff") != null &&
                        string.Compare(menuConfigNode.SelectSingleNode("AddNewMedicalStaff").InnerText, "True", true) == 0)
                    {
                        chkMedicalStaffNew.Checked = true;
                    }
                    else
                    {
                        chkMedicalStaffNew.Checked = false;
                    }

                    if (menuConfigNode.SelectSingleNode("AddExistingOtherPerson") != null &&
                        string.Compare(menuConfigNode.SelectSingleNode("AddExistingOtherPerson").InnerText, "True", true) == 0)
                    {
                        chkOtherPersonExt.Checked = true;
                    }
                    else
                    {
                        chkOtherPersonExt.Checked = false;
                    }

                    if (menuConfigNode.SelectSingleNode("AddNewOtherPerson") != null &&
                        string.Compare(menuConfigNode.SelectSingleNode("AddNewOtherPerson").InnerText, "True", true) == 0)
                    {
                        chkOtherPersonNew.Checked = true;
                    }
                    else
                    {
                        chkOtherPersonNew.Checked = false;
                    }

                    if (menuConfigNode.SelectSingleNode("AddExistingDriver") != null &&
                        string.Compare(menuConfigNode.SelectSingleNode("AddExistingDriver").InnerText, "True", true) == 0)
                    {
                        chkExtDriver.Checked = true;
                    }
                    else
                    {
                        chkExtDriver.Checked = false;
                    }
                    if (menuConfigNode.SelectSingleNode("AddNewDriver") != null &&
                        string.Compare(menuConfigNode.SelectSingleNode("AddNewDriver").InnerText, "True", true) == 0)
                    {
                        chkNewDriver.Checked = true;
                    }
                    else
                    {
                        chkNewDriver.Checked = false;
                    }
                    if (menuConfigNode.SelectSingleNode("AddExistingPatient") != null &&
                        string.Compare(menuConfigNode.SelectSingleNode("AddExistingPatient").InnerText, "True", true) == 0)
                    {
                        chkExtPatient.Checked = true;
                    }
                    else
                    {
                        chkExtPatient.Checked = false;
                    }
                    if (menuConfigNode.SelectSingleNode("AddNewPatient") != null &&
                        string.Compare(menuConfigNode.SelectSingleNode("AddNewPatient").InnerText, "True", true) == 0)
                    {
                        chkNewPatient.Checked = true;
                    }
                    else
                    {
                        chkNewPatient.Checked = false;
                    }
                    if (menuConfigNode.SelectSingleNode("AddExistingPhysician") != null &&
                        string.Compare(menuConfigNode.SelectSingleNode("AddExistingPhysician").InnerText, "True", true) == 0)
                    {
                        chkExtPhysician.Checked = true;
                    }
                    else
                    {
                        chkExtPhysician.Checked = false;
                    }
                    if (menuConfigNode.SelectSingleNode("AddNewPhysician") != null &&
                        string.Compare(menuConfigNode.SelectSingleNode("AddNewPhysician").InnerText, "True", true) == 0)
                    {
                        chkNewPhysician.Checked = true;
                    }
                    else
                    {
                        chkNewPhysician.Checked = false;
                    }
                    if (menuConfigNode.SelectSingleNode("AddExistingWitness") != null &&
                        string.Compare(menuConfigNode.SelectSingleNode("AddExistingWitness").InnerText, "True", true) == 0)
                    {
                        chkExtWitness.Checked = true;
                    }
                    else
                    {
                        chkExtWitness.Checked = false;
                    }
                    if (menuConfigNode.SelectSingleNode("AddNewWitness") != null &&
                        string.Compare(menuConfigNode.SelectSingleNode("AddNewWitness").InnerText, "True", true) == 0)
                    {
                        chkNewWitness.Checked = true;
                    }
                    else
                    {
                        chkNewWitness.Checked = false;
                    } 
                }
            }
        }
        #endregion
    }
}