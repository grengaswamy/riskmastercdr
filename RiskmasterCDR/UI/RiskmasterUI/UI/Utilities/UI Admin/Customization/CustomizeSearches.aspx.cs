﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Utilities.UI_Admin.Customization
{
    public partial class CustomizeSearches :  NonFDMBasePageCWS
    {
        public string sReturn = "";
        public string sCWSresponse = "";
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    XElement oMessageElement = GetMessageTemplate();

                    CallCWS("RMAdminSettingsAdaptor.GetAdminConfig", oMessageElement, out sCWSresponse, false, true);

                    ErrorControl1.errorDom = sCWSresponse;

                }
                catch (Exception ex)
                {
                    ErrorHelper.logErrors(ex);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ex, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                }

            }
        }

        protected void Save(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            bReturnStatus = CallCWSFunction("RMAdminSettingsAdaptor.SaveAdminConfig");
            if (bReturnStatus)
            {

                try
                {
                    XElement oMessageElement = GetMessageTemplate();

                    CallCWS("RMAdminSettingsAdaptor.GetAdminConfig", oMessageElement, out sCWSresponse, false, true);

                    ErrorControl1.errorDom = sCWSresponse;
                }
                catch (Exception ex)
                {
                    ErrorHelper.logErrors(ex);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ex, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                }
            }
        }

        protected void Refresh(object sender, EventArgs e)
        {

            try
            {

                XElement oMessageElement = GetMessageTemplate();

                CallCWS("RMAdminSettingsAdaptor.GetAdminConfig", oMessageElement, out sCWSresponse, false, true);

                ErrorControl1.errorDom = sCWSresponse;
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization></Authorization> 
             <Call>
              <Function></Function> 
              </Call>
             <Document>
              <FileName>customize_search</FileName> 
             </Document>
            </Message>


            ");

            return oTemplate;
        }
    }
}
