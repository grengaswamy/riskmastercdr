﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebLinksSetup.aspx.cs"
    Inherits="Riskmaster.UI.UI.Utilities.UI_Admin.Customization.WebLinksSetup" ValidateRequest="false" %>

<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="dg" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Web Links Setup</title>
    <link href="../../../../Content/zpcal/themes/system.css" rel="stylesheet" type="text/css" />
    <script src="../../../../Scripts/form.js" type="text/javascript"></script>
    <script src="../../../../Scripts/cul.js" type="text/javascript"></script>
    <script type="text/javascript">


        function onNewClick() {

            document.forms[0].reset();

            document.forms[0].lstUsers.length = 0;
            return true;
        }

        function OnSave() {
            
            document.forms[0].hdnURLType.value = "";
            var sUsers = "";

            if (document.forms[0].tbURLShortName.value == "") {
                //ksahu5-ML-MITS 34310 Start 
                //alert("Please enter the URL ");
                alert(WebLinksSetupValidations.ValidationURLStringCheck);
                //ksahu5-ML-MITS 34310 End
                return false;

            } if (document.forms[0].tbURLString.value == "") {
                //ksahu5-ML-MITS 34310 Start 
                //alert("Please enter the URL ");
                alert(WebLinksSetupValidations.ValidationURLStringCheck);
                //ksahu5-ML-MITS 34310 End
                return false;

            }
            if (document.forms[0].tbURLString.value == "") {
                //ksahu5-ML-MITS 34310 Start 
                //alert("Please enter the URL ");
                alert(WebLinksSetupValidations.ValidationURLStringCheck);
                //ksahu5-ML-MITS 34310 End
                return false;

            }
            if ((document.forms[0].tbURLString.value).indexOf("http") < 0) {
                //ksahu5-ML-MITS 34310 Start 
                //alert("The URL must start with http:// or https://");
                alert(WebLinksSetupValidations.ValidationURLStringIndexCheck);
                //ksahu5-ML-MITS 34310 End
                return false;

            }

          
            for (var i = 0; i < document.forms[0].lstSelectedURL.options.length; i++) {
                if (document.forms[0].lstSelectedURL.selectedIndex >= 0 && document.forms[0].lstSelectedURL.options[document.forms[0].lstSelectedURL.selectedIndex].text == document.forms[0].tbURLShortName.value) {
                    break;
                }
                if (document.forms[0].lstSelectedURL.options[i].innerText == document.forms[0].tbURLShortName.value) {
                    //ksahu5-ML-MITS 34310 Start 
                    //alert("Please select a unique Short name for the URL.");
                    alert(WebLinksSetupValidations.ValidationURLUniqueShortNameCheck);
                    //ksahu5-ML-MITS 34310 End
                    return false;
                }
            }

            var rdOptionList = eval('document.forms[0].rdURLType');
            var sRadioValue = document.getElementById('hdnURLType');
            for (var iItems = 0; iItems < rdOptionList.length; iItems++) {
                if (rdOptionList[iItems].checked) {
                    sRadioValue.value = rdOptionList[iItems].value;
                    break;
                }
            }

            if (sRadioValue.value.toString() == "Private" && document.getElementById('lstUsers').length == 0) {
                //ksahu5-ML-MITS 34310 Start 
                //alert("You need to select the User for private URL");
                alert(WebLinksSetupValidations.ValidationPrivateURLCheck);
                //ksahu5-ML-MITS 34310 End
                return false;
            }
            return true;
        }
        function AddParam() {

            var sValue = document.forms[0].cboSelectParameter.options[document.forms[0].cboSelectParameter.selectedIndex].value;
            var sURLString = document.forms[0].tbURLString.value;
            sURLString = sURLString + "<" + sValue + ">";
            document.forms[0].tbURLString.value = sURLString;

        }

        function AddFunction() {

            var sValue = document.forms[0].cboSelectFunction.options[document.forms[0].cboSelectFunction.selectedIndex].value;
            var sURLString = document.forms[0].tbURLString.value;
            sURLString = sURLString + "@" + sValue + "()" + "@";
            document.forms[0].tbURLString.value = sURLString;

        }
        function onDeleteClick() {
            if (document.forms[0].lstSelectedURL != null) {
             //kkaur8 added equals condition for MITS 33683
                if (document.forms[0].lstSelectedURL.selectedIndex >= 0) {
                    return true;
                }
                else {
                    //ksahu5-ML-MITS 34310 Start 
                    //alert("Please select URL.");
                    alert(WebLinksSetupValidations.ValidationDeleteClickCheck);
                    //ksahu5-ML-MITS 34310 End
                    return false;

                }
            }
        }
    </script>
</head>
<body onload="parent.MDIScreenLoaded();">
    <form id="frmData" runat="server">
    <dg:ErrorControl ID="ErrorControl1" runat="server" />
    <div id="toolbardrift" name="toolbardrift" class="toolbardrift">
        <table border="0" class="toolbar" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center" valign="middle" height="32">
                     <%----**ksahu5-ML-MITS34310 Start**--%>
                    <asp:ImageButton ID="btnNew" ImageUrl="~/Images/tb_new_active.png" class="bold" ToolTip="<%$ Resources:ttNew %>"
                        runat="server" OnClientClick="return onNewClick();" OnClick="btnNew_Click" />
                    <asp:ImageButton ID="btnSave" ImageUrl="~/Images/tb_save_active.png" class="bold"
                        ToolTip="<%$ Resources:ttSave %>" runat="server" OnClientClick="return OnSave(); " OnClick="btnSave_Click" />
                    <asp:ImageButton ID="btnDelete" ImageUrl="~/Images/tb_delete_active.png" class="bold"
                        ToolTip="<%$ Resources:ttDelete %>" runat="server" OnClientClick="onDeleteClick()" OnClick="btnDelete_Click" />
                    <%----**ksahu5-ML-MITS34310 End**--%>
                </td>
            </tr>
        </table>
    </div>
    <div class="msgheader">
       <%--**ksahu5-ML-MITS34310** Custom URL Definition--%>
        <asp:Label runat="server" ID="lblCusUrlDef" Text="<%$ Resources:lblCusUrlDef %>" />
    </div>
    <div>
        <asp:TextBox Style="display: none" runat="server" ID="hdnSelectedURLs" Text="" rmxref="Instance/Document/form/group/WebLink/SelectedURLs"
            rmxtype="hidden" />
        <asp:TextBox Style="display: none" runat="server" ID="hdnURLType" Text="" rmxref="Instance/Document/form/group/WebLink/URLType"
            rmxtype="hidden" />
        <asp:TextBox Style="display: none" runat="server" ID="rowid" Text="" rmxref="Instance/Document/form/group/WebLink/rowId"
            rmxtype="hidden" />
        <table border="0" cellspacing="0" cellpadding="0" width="100%" >
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" >
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                    </tr>
                                    <tr>
                                        <td>
                                            <%--**ksahu5-ML-MITS34310**   Select URL--%>
                                            <asp:Label runat="server" ID="lblSelUrl" Text="<%$ Resources:lblSelUrl %>" />
                                        </td>
                                    </tr>
                                    <tr>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;
                            </td>
                            <td>
                                <asp:ListBox runat="server" ID="lstSelectedURL" Rows="10" OnSelectedIndexChanged="lstSelectedURL_SelectedIndexChanged"
                                    AutoPostBack="true" />
                            </td>
                            <td>
                                <br />
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" >
                        <tr>
                            <td>
                                <%-- **ksahu5-ML-MITS34310** User:--%>
                                <asp:Label runat="server" ID="lblUser" Text="<%$ Resources:lblUser %>" />
                                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <br />
                                <select multiple="true" id="lstUsers" size="3" runat="server" style="width: 190px;
                                    height: 71px">
                                </select>
                                <input type="button" class="CodeLookupControl" value="" id="cmdAddCustomizedListUser"
                                    onclick="AddCustomizedListUser('weblinksetup','lstUsers','UserId','UserName')" runat="server"/> 
                                <input type="button" class="BtnRemove" value="" id="cmdDelCustomizedListUser" style="width: 21px"
                                    onclick="DelCustomizedListUser('weblinksetup','lstUsers','UserId','UserName')" runat="server"/>
                                <asp:TextBox Style="display: none" runat="server"  ID="UserId"
                                    Text="" rmxref="Instance/Document/form/group/WebLink/SelectedUsersID" rmxtype="hidden" />
                                <asp:TextBox Style="display: none" runat="server"  ID="UserName"
                                    Text="" rmxref="Instance/Document/form/group/WebLink/SelectedUsers" rmxtype="hidden" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <br />
                </td>
                <td></td>
            </tr>
            <tr>
                <td colspan="2">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <%--**ksahu5-ML-MITS34310** URL Short Name--%>
                                <asp:Label runat="server" ID="lblUrlShortName" Text="<%$ Resources:lblUrlShortName %>" />
                            </td>
                            <td>
                                &nbsp;&nbsp;
                            </td>
                            <td>
                                <asp:TextBox ID="tbURLShortName" runat="server" MaxLength='24' rmxref="Instance/Document/form/group/displaycolumn/control/URLShortName"
                                    onchange="setDataChanged(true)"></asp:TextBox>
                            </td>
                            <td>
                             <%--**ksahu5-ML-MITS34310 Start**--%>
                              <%-- <asp:RadioButtonList ID="rdURLType" runat="server" RepeatDirection="Horizontal" RMXType="RadioButtonList" OnSelectedIndexChanged="rdUrlType_SelectionChange" AutoPostBack="true">
                                <asp:ListItem Value="Public" Text="Public" Selected="True" />
                                <asp:ListItem Value="Private" Text="Private" />
                                </asp:RadioButtonList>--%>
                                <asp:RadioButtonList ID="rdURLType" runat="server" RepeatDirection="Horizontal" RMXType="RadioButtonList" OnSelectedIndexChanged="rdUrlType_SelectionChange" AutoPostBack="true">
                                <asp:ListItem Value="Public" Text="<%$ Resources:liPublic %>" Selected="True" />
                                <asp:ListItem Value="Private" Text="<%$ Resources:liPrivate %>" />
                                </asp:RadioButtonList>
                                <%--**ksahu5-ML-MITS34310 End**--%>
                            </td>
                        </tr>
                    </table>
                  </td>
            </tr>
        </table>
    </div>
    <div>
        <br />
    </div>
    <%--WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691 --%>
    <div>
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    Use As Supplemental Link :
                </td>
                <td>
                    &nbsp;&nbsp;
                </td>
                <td>
                    <asp:CheckBox ID="chkUseAsSupplementalLink" runat="server" value="false" rmxref="Instance/Document/form/group/displaycolumn/control/UseAsSupplementalLink" />
                </td>
            </tr>
        </table>
    </div>
    <%--WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691 --%>
    <div>
        <br />
    </div>
    </div>
    <div>
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                   <%--**ksahu5-ML-MITS34310** URL String--%>
                   <asp:Label runat="server" ID="lblUrlString" Text="<%$ Resources:lblUrlString %>" />
                </td>
                <td>
                    &nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox ID="tbURLString" runat="server" Height="105px" Width="472px" MaxLength='225'
                        rmxref="Instance/Document/form/group/displaycolumn/control/URLString" onchange="setDataChanged(true)"
                        TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <div>
        <br />
        <br />
        <br />
    </div>
    <div>
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                   <%--**ksahu5-ML-MITS34310** Select parameter:--%>
                   <asp:Label runat="server" ID="lblSelParam" Text="<%$ Resources:lblSelParam %>" />
                </td>
                <td>
                    &nbsp;&nbsp;
                </td>
                <td>
                    <asp:DropDownList ID="cboSelectParameter" runat="server">
                        <%--Commenting some parameters because there is no implementation for them--%>
                        <%--   <asp:ListItem Value="@">@-Object Property</asp:ListItem>
                        <asp:ListItem Value="@C.">@C.-Claim Object Property</asp:ListItem>
                        <asp:ListItem Value="@CM.">@CM.-Claim Method</asp:ListItem>
                        <asp:ListItem Value="@F.">@F.-Form Property</asp:ListItem>
                        <asp:ListItem Value="@M.">@M.-Object Method</asp:ListItem>--%>
                        <%--     <asp:ListItem Value="APP.NAME">APP.NAME-Application Name</asp:ListItem>
                        <asp:ListItem Value="APP.PATH">APP.PATH-Application Path</asp:ListItem>
                        <asp:ListItem Value="APP.VERSION">APP.VERSION-Application Version</asp:ListItem>
                        <asp:ListItem Value="AR">AR-Admin Rights</asp:ListItem>--%>
                        <%--**ksahu5-ML-MITS34310 Start**--%>
                    <%-- <asp:ListItem Value="CLAIM">CLAIM-Claim Number</asp:ListItem>
                        <asp:ListItem Value="CLAIMANT.N">CLAIMANT.N-Claimant Name</asp:ListItem>
                        <asp:ListItem Value="CLAIMANTID.N">CLAIMANTID.N-Claimant Technical ID</asp:ListItem>
                        <asp:ListItem Value="CLAIMID">CLAIMID-Claim Technical ID</asp:ListItem>--%>
                         <asp:ListItem Value="CLAIM" Text="<%$ Resources:liClaim Number %>"></asp:ListItem>
                         <asp:ListItem Value="CLAIMANT.N" Text="<%$ Resources:liClaimantName %>"></asp:ListItem>
                         <asp:ListItem Value="CLAIMANTID.N" Text="<%$ Resources:liClaimantTechnicalID %>"></asp:ListItem>
                         <asp:ListItem Value="CLAIMID" Text="<%$ Resources:liClaimTechnicalID %>"></asp:ListItem>
                        <%--**ksahu5-ML-MITS34310 End**--%>
                        <%--  <asp:ListItem Value="COMPUTER">COMPUTER-Computer Name</asp:ListItem>--%>
                        <%--   <asp:ListItem Value="CS">CS-Connection String</asp:ListItem>--%>
                        <%--  <asp:ListItem Value="DATABASE">DATABASE-Data Base	</asp:ListItem>--%>
                        <%--<asp:ListItem Value="DATABASEID">DATABASEID-Data Base Technical Key</asp:ListItem>--%>
                        <asp:ListItem Value="DATABASEID" Text="<%$ Resources:liDbTechnicalKey %>"></asp:ListItem>
                       <%-- <asp:ListItem Value="DATE">DATE-Current Date</asp:ListItem>--%>
                        <asp:ListItem Value="DATE" Text="<%$ Resources:liCurrentDate %>"></asp:ListItem>
                        <%-- <asp:ListItem Value="DID">DID-DSN Id</asp:ListItem>--%>
                        <%--<asp:ListItem Value="DOCPATH">DOCPATH-Document Management Path</asp:ListItem>--%>
                        <asp:ListItem Value="DOCPATH" Text="<%$ Resources:liDocMgmtPath %>"></asp:ListItem>
                        <%--    <asp:ListItem Value="DS">DS-Database Status</asp:ListItem>
                        <asp:ListItem Value="EM">EM-Email Address</asp:ListItem>--%>
                         <%--**ksahu5-ML-MITS34310 Start**--%>
                        <%-- <asp:ListItem Value="EVENT">EVENT-Event Number</asp:ListItem>
                        <asp:ListItem Value="EVENTID">EVENTID	-Event Technical ID</asp:ListItem>
                        <asp:ListItem Value="GID">GID-Group id</asp:ListItem>--%>
                        <asp:ListItem Value="EVENT" Text="<%$ Resources:liEvtNbr %>"></asp:ListItem>
                        <asp:ListItem Value="EVENTID"  Text="<%$ Resources:liEvtTechnicalID %>"></asp:ListItem>
                        <asp:ListItem Value="GID" Text="<%$ Resources:liGroupid %>"></asp:ListItem>
                         <%--**ksahu5-ML-MITS34310 End**--%>

                        <%--       <asp:ListItem Value="GROUP">GROUP-User Group Name</asp:ListItem>
                        <asp:ListItem Value="GROUPID">GROUPID-User Group ID</asp:ListItem>--%>
                        <%--      <asp:ListItem Value="ISSUESYSTEM">ISSUESYSTEM-Policy Issue System</asp:ListItem>--%>
                        <%--    <asp:ListItem Value="LOGINNAME">LOGINNAME-User Login</asp:ListItem>
                        <asp:ListItem Value="PASSWORD">PASSWORD-User Password</asp:ListItem>--%>
                        <%--  <asp:ListItem Value="PIrole[.Property]">PIrole[.Property]-Person Involved Role</asp:ListItem>--%>
                        <%--    <asp:ListItem Value="POLAGENTCODE">POLAGENTCODE-Policy Agent Code</asp:ListItem>
                        <asp:ListItem Value="POLAGENTNAME">POLAGENTNAME-Policy Agent Name	</asp:ListItem>
                        <asp:ListItem Value="POLBRANCH">POLBRANCH-Policy Key</asp:ListItem>--%>
                         <%--**ksahu5-ML-MITS34310 Start**--%>
                      <%--  <asp:ListItem Value="POLICY">POLICY-Policy Number</asp:ListItem>
                        <asp:ListItem Value="POLICYID">POLICYID-PolicyTechnical ID</asp:ListItem>--%>
                         <asp:ListItem Value="POLICY" Text="<%$ Resources:liPolicyNumber %>"></asp:ListItem>
                        <asp:ListItem Value="POLICYID" Text="<%$ Resources:liPolicyTechnicalID %>"></asp:ListItem>
                         <%--**ksahu5-ML-MITS34310 End**--%>
                        <%--   <asp:ListItem Value="POLKEY">POLKEY-Policy Key</asp:ListItem>
                        <asp:ListItem Value="POLLOB">POLLOB-Policy Line of Business</asp:ListItem>
                        <asp:ListItem Value="POLLOCATION">POLLOCATION-Policy Location</asp:ListItem>
                        <asp:ListItem Value="POLMCO">POLMCO-Policy Master Company</asp:ListItem>--%>
                          <%--**ksahu5-ML-MITS34310 Start**--%>
                      <%--  <asp:ListItem Value="POLMODULE">POLMODULE-Policy Module</asp:ListItem>
                        <asp:ListItem Value="POLSYMBOL">POLSYMBOL-Policy Symbol</asp:ListItem>--%>
                        <asp:ListItem Value="POLMODULE" Text="<%$ Resources:liPolicyModule %>"></asp:ListItem>
                        <asp:ListItem Value="POLSYMBOL" Text="<%$ Resources:liPolicySymbol %>"></asp:ListItem>
                          <%--**ksahu5-ML-MITS34310 End**--%>
                        <%--  <asp:ListItem Value="SP">SP-Storage Path</asp:ListItem>
                        <asp:ListItem Value="ST">ST-Storage Type</asp:ListItem>--%>
                          <%--**ksahu5-ML-MITS34310 Start**--%>
                     <%--   <asp:ListItem Value="TIME">TIME-Current Time</asp:ListItem>
                        <asp:ListItem Value="UFN">UFN-User First Name</asp:ListItem>
                        <asp:ListItem Value="UID">UID-User Id</asp:ListItem>
                        <asp:ListItem Value="ULN">ULN-User Last Name</asp:ListItem>
                        <asp:ListItem Value="USERID">USERID-User Technical ID</asp:ListItem>
                        <asp:ListItem Value="USERNAME">USERNAME-User Full Name</asp:ListItem>--%>
                        <asp:ListItem Value="TIME" Text="<%$ Resources:liTmCurrentTime %>"></asp:ListItem>
                        <asp:ListItem Value="UFN" Text="<%$ Resources:liUserFirstName %>"></asp:ListItem>
                        <asp:ListItem Value="UID" Text="<%$ Resources:liUserId %>"></asp:ListItem>
                        <asp:ListItem Value="ULN" Text="<%$ Resources:liUserLastName %>"></asp:ListItem>
                        <asp:ListItem Value="USERID" Text="<%$ Resources:liUserTechnicalID %>"></asp:ListItem>
                        <asp:ListItem Value="USERNAME" Text="<%$ Resources:liUserFullName %>"></asp:ListItem>
                         <%--**ksahu5-ML-MITS34310 End**--%>
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;&nbsp;
                </td>
                <td>
                        <%--**ksahu5-ML-MITS34310 Start**--%>
                        <%-- <input type="button" id="btnSelectParameter" value="Add To URL" onclick="AddParam()" />--%>
                        <input type="button" id="btnSelectParameter" value="<%$ Resources:btnAddtoURL %>" onclick="AddParam()" runat="server"/> 
                        <%--**ksahu5-ML-MITS34310 End**--%>
                     
                </td>
            </tr>
            <tr>
                <td>
                    <%-- **ksahu5-ML-MITS34310**  Select Function:--%>
                     <asp:Label runat="server" ID="lblSelFun" Text="<%$ Resources:lblSelFun %>" />
                </td>
                <td>
                    &nbsp;&nbsp;
                </td>
                <td>
                    <asp:DropDownList ID="cboSelectFunction" runat="server">
                        <%--Commenting some functions because there is no implementation for them--%>
                        <%--**ksahu5-ML-MITS34310 Start**--%>
                        <%--<asp:ListItem Value="DAY">Day-DAY</asp:ListItem>
                        <asp:ListItem Value="DOW(date,day)">Dayof the Week-DOW</asp:ListItem>
                        <asp:ListItem Value="DSPLYDATE">Display Date-DSPLYDATE</asp:ListItem>
                        <asp:ListItem Value="DSPLYTIME">Display Time-DSPLYTIME</asp:ListItem>--%>
                        <asp:ListItem Value="DAY" Text="<%$ Resources:liDay %>"></asp:ListItem>
                        <asp:ListItem Value="DOW(date,day)" Text="<%$ Resources:liDOW %>"></asp:ListItem>
                        <asp:ListItem Value="DSPLYDATE" Text="<%$ Resources:liDsplDate %>"></asp:ListItem>
                        <asp:ListItem Value="DSPLYTIME" Text="<%$ Resources:liDsplTime %>"></asp:ListItem>
                         <%--**ksahu5-ML-MITS34310 End**--%>

                        <%--   <asp:ListItem Value="ENC">Encript a value-ENC</asp:ListItem>
                        <asp:ListItem Value="ENTITYNAME">Entity Name-ENTITYNAME</asp:ListItem>--%>
                         <%--**ksahu5-ML-MITS34310 Start**--%>
                       <%-- <asp:ListItem Value="FORMAT(*,&quot;####0.00&quot;)">Format Currency-FORMAT(*,&quot;####0.00&quot;)</asp:ListItem>--%>
                        <asp:ListItem Value="FORMAT(*,&quot;####0.00&quot;)"  Text="<%$ Resources:liCurrencyFORMAT %>"></asp:ListItem>
                         <%--**ksahu5-ML-MITS34310 End**--%>
                        <%--  <asp:ListItem Value="FORMAT(*,&quot;hh:mm:ss&quot;)">Format Time-FORMAT(*,&quot;hh:mm:ss&quot;)</asp:ListItem>
                        <asp:ListItem Value="FORMAT(*,&quot;mask&quot;)">Format(data,mask)-FORMAT(*,&quot;mask&quot;)</asp:ListItem>--%>
                        <%--**ksahu5-ML-MITS34310 Start**--%>
                       <%-- <asp:ListItem Value="FORMAT(*,&quot;mm/dd/yyyy&quot;)">Format Date-FORMAT(*,&quot;mm/dd/yyyy&quot;)</asp:ListItem>--%>
                         <asp:ListItem Value="FORMAT(*,&quot;mm/dd/yyyy&quot;)" Text="<%$ Resources:liDateFORMAT %>"></asp:ListItem>
                       <%-- <asp:ListItem Value="HOUR">Hour-HOUR	</asp:ListItem>--%>
                         <asp:ListItem Value="HOUR" Text="<%$ Resources:liHour %>"></asp:ListItem>
                       <%-- <asp:ListItem Value="IIFB(*,iftrue,iffalse)">Immediate If Boolean-IIFB(*,iftrue,iffalse)</asp:ListItem>--%>
                         <asp:ListItem Value="IIFB(*,iftrue,iffalse)" Text="<%$ Resources:liBooleanIIFB %>" ></asp:ListItem>
                       <%-- <asp:ListItem Value="IIFN(*,>0,=0,<0)">Immediate If Number-IIFN(*,>0,=0,<0)</asp:ListItem>--%>
                         <asp:ListItem Value="IIFN(*,>0,=0,<0)" Text="<%$ Resources:liImmidiateIIFN %>"></asp:ListItem>
                        <%--<asp:ListItem Value="IIFS(*,notblank,blank)">Immediate If String-IIFS(*,notblank,blank)</asp:ListItem>--%>
                        <asp:ListItem Value="IIFS(*,notblank,blank)" Text="<%$ Resources:liImmidiateStrIIFS %>" ></asp:ListItem>
                       <%-- <asp:ListItem Value="LCASE">Lower Case-LCASE</asp:ListItem>--%>
                        <asp:ListItem Value="LCASE" Text="<%$ Resources:liLcase %>"></asp:ListItem>
                       <%-- <asp:ListItem Value="LEFT(*,length)">Left-LEFT(*,length)</asp:ListItem>--%>
                         <asp:ListItem Value="LEFT(*,length)"  Text="<%$ Resources:liLeftLength %>" ></asp:ListItem>
                        <%--**ksahu5-ML-MITS34310 End**--%>

                        <%--        <asp:ListItem Value="LUDESC">Lookup Description-LUDESC</asp:ListItem>
                        <asp:ListItem Value="LUSC">Lookup Short Code-LUSC</asp:ListItem>--%>
                        <%--**ksahu5-ML-MITS34310 Start**--%>
                        <%--<asp:ListItem Value="MID(*,start,length)">Substring-MID(*,start,length)</asp:ListItem>--%>
                        <asp:ListItem Value="MID(*,start,length)" Text="<%$ Resources:liSubstrMid %>"></asp:ListItem>
                        <%--**ksahu5-ML-MITS34310 End**--%>
                        <%--**ksahu5-ML-MITS34310 Start**--%>
                       <%--<asp:ListItem Value="MIN">Minute-MIN</asp:ListItem>
                        <asp:ListItem Value="MON">Month-MON</asp:ListItem>--%>
                         <asp:ListItem Value="MIN" Text="<%$ Resources:liMinute %>"></asp:ListItem>
                        <asp:ListItem Value="MON" Text="<%$ Resources:liMonth %>"></asp:ListItem>
                        <%--**ksahu5-ML-MITS34310 End**--%>
                        <%--               <asp:ListItem Value="PROPERCASE">Proper Case-PROPERCASE</asp:ListItem>--%>
                         <%--**ksahu5-ML-MITS34310 Start**--%>
                        <%--<asp:ListItem Value="RIGHT(*,length)">Right-RIGHT(*,length)</asp:ListItem>--%>
                        <asp:ListItem Value="RIGHT(*,length)" Text="<%$ Resources:liRight %>"></asp:ListItem>
                        <%--<asp:ListItem Value="SEC">Second-SEC</asp:ListItem>--%>
                        <asp:ListItem Value="SEC" Text="<%$ Resources:liSecond %>"></asp:ListItem>
                         <%--**ksahu5-ML-MITS34310 End**--%>
                        <%--  <asp:ListItem Value="SELECT(*,sellist,...)">Select a value-SELECT(*,sellist,...)</asp:ListItem>--%>
                        <%--     <asp:ListItem Value="UCASE">Upper Case-UCASE	</asp:ListItem>--%>
                          <%--**ksahu5-ML-MITS34310 Start**--%>
                       <%-- <asp:ListItem Value="YEAR">Year-YEAR	</asp:ListItem>--%>
                         <asp:ListItem Value="YEAR" Text="<%$ Resources:liYear %>"></asp:ListItem>
                         <%--**ksahu5-ML-MITS34310 End**--%>
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;&nbsp;
                </td>
                <td>
                     <%--**ksahu5-ML-MITS34310 Start**--%>
                   <%-- <input type="button" runat="server" value="Add To URL" onclick="AddFunction()" />--%>
                    <input type="button" runat="server" value="<%$ Resources:btnAddtoURL %>" onclick="AddFunction()" />
                     <%--**ksahu5-ML-MITS34310 End**--%>
                   
                </td>
            </tr>
        </table>
    </div>
    <asp:TextBox Style="display: none" runat="server" rmxretainvalue="true" ID="UrlList"
        Text="" rmxref="Instance/Document/form/group/displaycolumn/control/UrlList" rmxtype="hidden" />
    <asp:TextBox Style="display: none" runat="server" rmxretainvalue="true" ID="availUsers"
        Text="" rmxref="Instance/Document/form/group/displaycolumn/control/AvialUser"
        rmxtype="hidden" />
    <asp:TextBox Style="display: none" runat="server" rmxretainvalue="true" ID="userList"
        Text="" rmxref="Instance/Document/form/group/displaycolumn/control/UserList"
        rmxtype="hidden" />
    </form>
</body>
</html>
