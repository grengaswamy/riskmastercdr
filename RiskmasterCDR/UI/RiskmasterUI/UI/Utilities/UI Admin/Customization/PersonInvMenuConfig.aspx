﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PersonInvMenuConfig.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.UI_Admin.Customization.PersonInvMenuConfig" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
      <title>Person Involved Menu Configuration</title>
    <script src="../../../../Scripts/form.js" type="text/javascript"></script>
    <script src="../../../../Scripts/ExecutiveSummary.js" type="text/javascript"></script>
    <script type="text/javascript" language="Javascript">
        function setDefaultValue(name, value) {
            document.getElementById(name).value = value;
            return false;
        }
    </script>
</head>
<body onload="if(parent.MDIScreenLoaded != null) parent.MDIScreenLoaded();">
    <form id="frmData" runat="server">
    <input type="hidden" value="" id="hiddenSelectedUser" />
    <input type="hidden" value="DiaryConfig" id="hTabName" />
    <uc1:ErrorControl ID="ErrorControl" runat="server" />
    <div class="msgheader" id="formtitle">
        <%--Person Involved Menu Configuration--%>
        <asp:Label ID="lblPersonInvolvedHead" runat="server" Text="<%$ Resources:lblPersonInvolvedHead %>"></asp:Label>
    </div>
    <table border="0" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td>
                <table border="0" cellspacing="0" celpadding="0">
                    <tr>
                        <td class="Selected">
                            <a class="Selected" name="PIMenuConfig"><span style="text-decoration: none">Person Involved Menu Config</span>
                            </a>
                        </td>
                        <td style="border-bottom: none; border-left-style: solid; border-left-color: #999999;
                            border-width: 2px; border-top-style: solid; border-top-width: 2px; border-top-color: #FFFFFF;">
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr valign="top">
            <td valign="top">
                <div style="position: relative; left: 0; top: 0; width: 780px; height: 250px; overflow: auto;valign:top" class="singletopborder">
                    <table border="0" cellspacing="0" celpadding="0" width="100%" valign="top" align="center">
                        <tr valign="top">
                            <td valign="top">
                                <table border="0" cellspacing="0" width="100%" align="center">
                <tr id="">
                    <td><asp:Label ID="lblAddExisting" runat="server" Text="<%$ Resources:lblAddExisting %>"></asp:Label><%--Add Existing--%>
                    </td>
                    <td colspan="2"></td>
                    <td><asp:Label ID="lblAddNew" runat="server" Text="<%$ Resources:lblAddNew %>"></asp:Label><%--Add New--%>
                    </td>
                </tr>
                <tr id="">
                <td><asp:Label ID="lblEmployee" runat="server" Text="<%$ Resources:lblEmployee %>"></asp:Label><%--Employee:--%>
                    </td>
                    <td>
                        <asp:CheckBox ID="chkEmployeeExisting" onclick="setDataChanged(true);" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='AddExistingEmployee']"
                            type="checkbox" value="True" runat="server" />
                    </td>
                    <td></td>
                    <td><asp:Label ID="lblEmployee1" runat="server" Text="<%$ Resources:lblEmployee %>"></asp:Label><%--Employee:--%>
                    </td>
                    <td>
                        <asp:CheckBox ID="chkEmployeeNew" onclick="setDataChanged(true);"
                            rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='AddNewEmployee']"
                            value="True" type="checkbox" runat="server" />
                    </td>
                </tr>
                <tr id="">
                <td><asp:Label ID="lblMedStaff" runat="server" Text="<%$ Resources:lblMedStaff %>"></asp:Label><%--Medical Staff:--%>
                    </td>
                    <td>
                        <asp:CheckBox ID="chkMedicalStaffExting" onclick="setDataChanged(true);" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='AddExistingMedicalStaff']"
                            type="checkbox" value="True" runat="server" />
                    </td>
                    <td></td>
                    <td><asp:Label ID="lblMedStaff1" runat="server" Text="<%$ Resources:lblMedStaff %>"></asp:Label><%--Medical Staff:--%>
                    </td>
                    <td>
                        <asp:CheckBox ID="chkMedicalStaffNew" onclick="setDataChanged(true);"
                            rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='AddNewMedicalStaff']"
                            value="True" type="checkbox" runat="server" />
                    </td>
                </tr>
                <tr id="">
                <td><asp:Label ID="lblOtherPerson" runat="server" Text="<%$ Resources:lblOtherPerson %>"></asp:Label><%--Other Person:--%>
                    </td>
                    <td>
                        <asp:CheckBox ID="chkOtherPersonExt" onclick="setDataChanged(true);" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='AddExistingOtherPerson']"
                            type="checkbox" value="True" runat="server" />
                    </td>
                    <td></td>
                    <td><asp:Label ID="lblOtherPerson1" runat="server" Text="<%$ Resources:lblOtherPerson %>"></asp:Label><%--Other Person:--%>
                    </td>
                    <td>
                        <asp:CheckBox ID="chkOtherPersonNew" onclick="setDataChanged(true);"
                            rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='AddNewOtherPerson']"
                            value="True" type="checkbox" runat="server" />
                    </td>
                </tr>
                <tr id="">
                <td><asp:Label ID="lblDriver" runat="server" Text="<%$ Resources:lblDriver %>"></asp:Label><%--Driver:--%>
                    </td>
                    <td>
                        <asp:CheckBox ID="chkExtDriver" onclick="setDataChanged(true);" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='AddExistingDriver']"
                            type="checkbox" value="True" runat="server" />
                    </td>
                    <td></td>
                    <td><asp:Label ID="lblDriver1" runat="server" Text="<%$ Resources:lblDriver %>"></asp:Label><%--Driver:--%>
                    </td>
                    <td>
                        <asp:CheckBox ID="chkNewDriver" onclick="setDataChanged(true);"
                            rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='AddNewDriver']"
                            value="True" type="checkbox" runat="server" />
                    </td>
                </tr>
                <tr id="">
                <td><asp:Label ID="lblPatient" runat="server" Text="<%$ Resources:lblPatient %>"></asp:Label><%--Patient:--%>
                    </td>
                    <td>
                        <asp:CheckBox ID="chkExtPatient" onclick="setDataChanged(true);" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='AddExistingPatient']"
                            type="checkbox" value="True" runat="server" />
                    </td>
                    <td></td>
                    <td><asp:Label ID="lblPatient1" runat="server" Text="<%$ Resources:lblPatient %>"></asp:Label><%--Patient:--%>
                    </td>
                    <td>
                        <asp:CheckBox ID="chkNewPatient" onclick="setDataChanged(true);"
                            rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='AddNewPatient']"
                            value="True" type="checkbox" runat="server" />
                    </td>
                </tr>
                <tr id="">
                <td><asp:Label ID="lblPhysician" runat="server" Text="<%$ Resources:lblPhysician %>"></asp:Label><%--Physician:--%>
                    </td>
                    <td>
                        <asp:CheckBox ID="chkExtPhysician" onclick="setDataChanged(true);" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='AddExistingPhysician']"
                            type="checkbox" value="True" runat="server" />
                    </td>
                    <td></td>
                    <td><asp:Label ID="lblPhysician1" runat="server" Text="<%$ Resources:lblPhysician %>"></asp:Label><%--Physician:--%>
                    </td>
                    <td>
                        <asp:CheckBox ID="chkNewPhysician" onclick="setDataChanged(true);"
                            rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='AddNewPhysician']"
                            value="True" type="checkbox" runat="server" />
                    </td>
                </tr>
                <tr id="">
                <td><asp:Label ID="lblWitness" runat="server" Text="<%$ Resources:lblWitness %>"></asp:Label><%--Witness:--%>
                    </td>
                    <td>
                        <asp:CheckBox ID="chkExtWitness" onclick="setDataChanged(true);" rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='AddExistingWitness']"
                            type="checkbox" value="True" runat="server" />
                    </td>
                    <td></td>
                    <td><asp:Label ID="lblWitness1" runat="server" Text="<%$ Resources:lblWitness %>"></asp:Label><%--Witness:--%>
                    </td>
                    <td>
                        <asp:CheckBox ID="chkNewWitness" onclick="setDataChanged(true);"
                            rmxref="/Instance/Document/form/SystemSettings/group/displaycolumn/systemcontrol[@name ='AddNewWitness']"
                            value="True" type="checkbox" runat="server" />
                    </td>
                </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <table border="0" cellspacing="0" celpadding="0" width="80%" valign="top">
                    <tr>
                        <td>
                            <div class="small">
                                <asp:Label ID="lbltext1" runat="server" Text="<%$ Resources:lblDisclaimer1 %>"></asp:Label>
                                <%--Use this page to select the options you want to see in Person Involved Menu tree.--%>
                                <br>
                                <asp:Label ID="lbltext2" runat="server" Text="<%$ Resources:lblDisclaimer2 %>"></asp:Label> 
                                <%--Once you have made all of your selections, click the "Save Configuration" button
                                to store the settings for use.--%>
                            </div>
                        </td>
                    </tr>
                </table>
                <table border="0" cellspacing="0" celpadding="0" width="30%" valign="top">
                    <tr>
                        <td>
                            <asp:Button ID="btnSaveConfiguration" runat="server" Text="<%$ Resources:btnSaveConfiguration %>" 
                                CssClass="button" onclick="btnSaveConfiguration_Click" /><%--Save Configuration--%>
                        </td>
                        <td>
                            <input type="submit" value='<%$ Resources:btnSelectAll %>' id="btnSelectAll" class="button" runat="server"
                                onclick="return SelectAll();; " /><%--Select All--%>
                        </td>
                        <td>
                            <input type="submit" value='<%$ Resources:btnUnselectAll %>' id="btnUnselectAll" class="button" runat="server"
                                onclick="return UnselectAll();; " /><%--Unselect All--%>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
