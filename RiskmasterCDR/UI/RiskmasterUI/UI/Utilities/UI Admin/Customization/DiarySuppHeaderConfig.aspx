﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DiarySuppHeaderConfig.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.UI_Admin.Customization.DiarySuppHeaderConfig" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Additional Diary Header Configuration</title>
    <script src="../../../../Scripts/form.js" type="text/javascript"></script>
    <script src="../../../../Scripts/jquery/jquery-1.8.0.js" type="text/javascript"></script>
    <script type="text/javascript" language="Javascript">

        $('document').ready(function () {
            if ($.trim($('#hdnCloseWindow').val()) === 'True') {
                window.close();
            }
        });

        var m_Wnd = null;
        var ns, ie, ieversion;
        var browserName = navigator.appName;                   // detect browser 
        var browserVersion = navigator.appVersion;
        var arrHelpMsg = null;
        if (browserName == "Netscape") {
            ie = 0;
            ns = 1;
        }
        else		//Assume IE
        {
            ieversion = browserVersion.substr(browserVersion.search("MSIE ") + 5, 1);
            ie = 1;
            ns = 0;
        }

        function MoveField(Direction, lstDiarySuppList) {
            if (lstDiarySuppList.selectedIndex < 0) {
                alert("Please select the field.");
                return;
            }

            if (lstDiarySuppList.selectedIndex == 0 && Direction == 0 || lstDiarySuppList.selectedIndex == lstDiarySuppList.options.length - 1 && Direction == 1)
                return;
            if (lstDiarySuppList.options[lstDiarySuppList.selectedIndex].value == "") {
                alert("System item cannot be moved.");
                return;
            }

            var sText, sValue;
            var sText2, sValue2;
            var i = lstDiarySuppList.selectedIndex;
            var o1, o2;
            sText = lstDiarySuppList.options[i].text;
            sValue = lstDiarySuppList.options[i].value;
            if (Direction == 0) {
                sText2 = lstDiarySuppList.options[i - 1].text;
                sValue2 = lstDiarySuppList.options[i - 1].value;


                if (sValue2 == "")
                    return;
                o1 = new Option(sText, sValue, false, false);
                o2 = new Option(sText2, sValue2, false, false);
                lstDiarySuppList.options[i] = o2;
                lstDiarySuppList.options[i - 1] = o1;
            }
            else {
                sText2 = lstDiarySuppList.options[i + 1].text;
                sValue2 = lstDiarySuppList.options[i + 1].value;

                if (sValue2 == "")
                    return;
                o1 = new Option(sText, sValue, false, false);
                o2 = new Option(sText2, sValue2, false, false);
                lstDiarySuppList.options[i] = o2;
                lstDiarySuppList.options[i + 1] = o1;
            }
            o1.selected = true;
        }

        function EditField() {
            if (document.forms[0].lstDiarySuppList.selectedIndex < 0) {
                alert("Please select field."); //abansal23:  MITS 14347
                return false;
            }

            if (document.forms[0].lstDiarySuppList.options[document.forms[0].lstDiarySuppList.selectedIndex].value == "") {
                alert("This is system field and it cannot be edited.");
                return;
            }

            if (m_Wnd != null)
                m_Wnd.close();

            SetFormValues(document.forms[0].lstDiarySuppList.options[document.forms[0].lstDiarySuppList.selectedIndex].text);

            m_Wnd = window.open('DiarySuppEditField.aspx', 'edit', 'width=450,height=200' + ',top=' + (screen.availHeight - 600) / 2 + ',left=' + (screen.availWidth - 600) / 2 + ',resizable=yes,scrollbars=yes');
        }

        function DeleteField(lstDiarySuppList) {
            if (lstDiarySuppList.selectedIndex < 0) {
                alert("Please select field."); //abansal23:  MITS 14347
                return false;
            }

            if (lstDiarySuppList.options[lstDiarySuppList.selectedIndex].value == "") {
                alert("This is system field and it cannot be removed.");
                return;
            }

            //if(!self.confirm("Delete selected item?"))
            //	return;

            var i = lstDiarySuppList.selectedIndex;
            lstDiarySuppList.options[i] = null;
            if (i < lstDiarySuppList.options.length)
                lstDiarySuppList.selectedIndex = i;
        }

        function AddField() {

            if (document.forms[0].cboDiarySuppFields.selectedIndex < 0)
                return false;
            var sValue = document.forms[0].cboDiarySuppFields.options[document.forms[0].cboDiarySuppFields.selectedIndex].value;
            var sText = document.forms[0].cboDiarySuppFields.options[document.forms[0].cboDiarySuppFields.selectedIndex].text;
            var opt = null;
            var sFormListValue; //gagnihotri MITS 12617 06/06/2008


            for (var f = 0; f < document.forms[0].lstDiarySuppList.options.length; f++) {
                if (document.forms[0].lstDiarySuppList.options[f].value == sValue) {
                    alert("Field is already in use.");
                    return false;
                }
            }
            opt = new Option(sText, sValue, false, false);

            if (ie && document.forms[0].lstDiarySuppList.options.selectedIndex >= 0) {
                document.forms[0].lstDiarySuppList.options.add(opt, document.forms[0].lstDiarySuppList.options.selectedIndex);
                document.forms[0].lstDiarySuppList.selectedIndex = document.forms[0].lstDiarySuppList.selectedIndex - 1;
            }
            else {
                document.forms[0].lstDiarySuppList.options.add(opt, document.forms[0].lstDiarySuppList.options.length);
                document.forms[0].lstDiarySuppList.selectedIndex = document.forms[0].lstDiarySuppList.options.length - 1;
            }
            return true;
        }

        function OnEditFieldOK() {
            var arr;
//            if (document.forms[0].lstDiarySuppList.selectedIndex >= 0) {
//                arr = document.forms[0].lstDiarySuppList.options[document.forms[0].lstDiarySuppList.selectedIndex].value.split("|");
//            }
            var s = m_Wnd.document.forms[0].txtCaption.value;

            if (replace(s, " ", "") == "") {
                m_Wnd.close();
                m_Wnd = null;
                return false;
            }
            if (m_Wnd.document.forms[0].fieldcaption.value != "0" && m_Wnd.document.forms[0].fieldcaption.value != "") {
                document.forms[0].lstDiarySuppList.options[document.forms[0].lstDiarySuppList.selectedIndex].text = s;
            }

            //document.forms[0].lstDiarySuppList.options[document.forms[0].lstDiarySuppList.selectedIndex].value = arr[0]+;
            
            //rsushilaggar mits 25388 Date 07/07/2011
            //if (arr.length > 2) {
            //          document.forms[0].lstDiarySuppList.options[document.forms[0].lstDiarySuppList.selectedIndex].value += "|" + arr[2];
            //}
            m_Wnd.close();
            m_Wnd = null;
            return true;
        }

        function SetFormValues(sValue) {
            if (sValue != "") {
                document.forms[0].FieldName.value = sValue;
                document.forms[0].FieldCaption.value = "Field";
            }
            else
            {
                document.forms[0].FieldName.value = "Invalid Parameters.";
            }
        }

        function onWndClose() {
            m_Wnd = null;
            return true;
        }

        function pageLoaded() {
            self.onunload = pageUnload;
            document.onWndClose = onWndClose;
            document.OnEditFieldOK = OnEditFieldOK;
            self.onfocus = onWindowFocus;
            return true;
        }

        function pageUnload() {
            if (m_Wnd != null)
                m_Wnd.close();
            m_Wnd = null;
            return true;
        }

        function onWindowFocus() {
            if (m_Wnd != null)
                m_Wnd.close();
            m_Wnd = null;
            return true;
        }

        function replace(sSource, sSearchFor, sReplaceWith) {
            var arr = new Array();
            if (sSource != null)
                arr = sSource.split(sSearchFor);
            return arr.join(sReplaceWith);
        }

        function onSubmitForm() 
        {
            var s = "", s1 = "", s2 = "", s3 = "";

            for (var i = 0; i < document.forms[0].lstDiarySuppList.options.length; i++) 
            {
                if (document.forms[0].lstDiarySuppList.options[i].value != "" && document.forms[0].lstDiarySuppList.options[i].text != "") 
                {
                    arr = document.forms[0].lstDiarySuppList.options[i].value.split("|")
                    s = s + replace(arr[0], "|", "") + "|";
                    s1 = s1 + replace(document.forms[0].lstDiarySuppList.options[i].text, "|", "") + "|";
                    s2 = s2 + replace(arr[1], "|", "") + "|";
                    s3 = s3 + replace(arr[2], "|", "") + "|";
                } 
            }
            document.forms[0].fields.value = s;
            document.forms[0].captions.value = s1;
            document.forms[0].fieldtypes.value = s2;
            document.forms[0].field_ids.value = s3;
            //PleaseWaitDialog1.Show();
            return true;
        }

    </script>
</head>
<body onload="if(parent.MDIScreenLoaded != null) parent.MDIScreenLoaded();">
    <form id="frmData" runat="server">
    <asp:textbox style="display: none" runat="server"  id="FieldCaption"/>
    <asp:textbox style="display: none" runat="server"  id="FieldName"/>
    <asp:textbox style="display: none" runat="server"  id="fields"/>
    <asp:textbox style="display: none" runat="server"  id="captions"/>
    <asp:textbox style="display: none" runat="server"  id="fieldtypes"/>
    <asp:textbox style="display: none" runat="server"  id="field_ids"/>
    <rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />
    <div class="msgheader" id="formtitle">
        <asp:Label ID="lblAdditionalDiaryHeader" runat="server" Text="Additional Diary Header Configuration"></asp:Label>
    </div>
    <table width="100%" border="0">
        <tr>
            <td class="ctrlgroup2">
                WPA Diary Supplemental Fields
            </td>
        </tr>
        <tr>
            <td valign="top">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td rowspan="2">
                            <asp:DropDownList runat="server" Width="150" ID="cboDiarySuppFields" />
                            <br />
                            <asp:ListBox runat="server" Width="150" ID="lstDiarySuppList" Rows="12" />
                        </td>
                        <td valign="top">
                            <asp:Button UseSubmitBehavior="false" runat="server" Text="Add" class="button"
                                OnClientClick="AddField();return false;" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <asp:ImageButton ID="up" runat="server" UseSubmitBehavior="false" src="../../../../Images/up.gif"
                                Width="20" Height="20" border="0" alt="Up" title="Move Up" OnClientClick="MoveField(0,document.forms[0].lstDiarySuppList);return false;" />
                            <asp:ImageButton ID="down" runat="server" UseSubmitBehavior="false" src="../../../../Images/down.gif"
                                Width="20" Height="20" border="0" alt="Down" title="Move Down" OnClientClick="MoveField(1,document.forms[0].lstDiarySuppList);return false;" />
                            <asp:ImageButton ID="edit" runat="server" UseSubmitBehavior="false" src="../../../../Images/edit.gif"
                                Width="20" Height="20" border="0" alt="Edit" title="Edit" OnClientClick="EditField();return false;" />
                            <asp:ImageButton ID="delete" runat="server" UseSubmitBehavior="false" src="../../../../Images/delete3.gif"
                                Width="20" Height="20" border="0" alt="Delete" title="Remove" OnClientClick="DeleteField(document.forms[0].lstDiarySuppList);return false;" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
        <asp:Button UseSubmitBehavior="false" runat="server" Text="Save" class="button" onclientClick="if(!onSubmitForm()) return false;" OnClick="Save"/>
        </tr>
    </table>
    <asp:HiddenField ID='hdnCloseWindow' runat="server" />
    </form>
</body>
</html>
