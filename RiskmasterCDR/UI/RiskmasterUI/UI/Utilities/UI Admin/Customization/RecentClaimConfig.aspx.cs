﻿//
//Page created for MITS 23633 By Npradeepshar on 11/05/2011
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.UI.Utilities.UI_Admin.Customization
{
    public partial class RecentClaimConfig : System.Web.UI.Page
    {
        string PageID = RMXResourceProvider.PageId("RecentClaimConfig.aspx");
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    LoadRecentClaimConfigDetails();
                }
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnSaveConfiguration_Click(object sender, EventArgs e)
        {
            try
            {
                SetRecentClaimConfigDetails();
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        #region Message Template
        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetConfigMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization></Authorization>
              <Call>
                <Function>RecentRecordsAdaptor.GetRecentClaimConfig</Function>
              </Call>
              <Document>
                 <RecentClaimConfig>
                    <IsAdminUser>False</IsAdminUser> 
                    <UserId /> 
                  </RecentClaimConfig>
              </Document>
            </Message>
            ");

            return oTemplate;
        }

        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement SetConfigMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization></Authorization>
              <Call>
                <Function>RecentRecordsAdaptor.SetRecentClaimConfig</Function>
              </Call>
              <Document>
                 <RecentClaimConfig>
                          <ClaimNumber></ClaimNumber> 
                          <Lob></Lob> 
                          <EventNumber></EventNumber> 
                          <DateOfEvent></DateOfEvent>
                          <DateOfClaim></DateOfClaim>
                          <Claimant></Claimant> 
                          <ClaimType></ClaimType> 
                          <ClaimStatus></ClaimStatus> 
                          <ClaimNumberHeader></ClaimNumberHeader> 
                          <LobHeader></LobHeader> 
                          <EventNumberHeader></EventNumberHeader> 
                          <DateOfEventHeader></DateOfEventHeader> 
                          <DateOfClaimHeader></DateOfClaimHeader> 
                          <ClaimantHeader></ClaimantHeader> 
                          <ClaimTypeHeader></ClaimTypeHeader>
                          <ClaimStatusHeader></ClaimStatusHeader> 
                    </RecentClaimConfig>
              </Document>
            </Message>
            ");

            return oTemplate;
        }
        #endregion Message Template

        #region Private Methods
        /// <summary>
        /// get the Recent Claim configuration 
        /// </summary>
        /// <returns></returns>
        private XmlDocument GetRecentClaimConfigDetails()
        {
            XElement messageElement = null;
            string sReturn = string.Empty;
            XmlDocument resultDoc = null;

            try
            {
                messageElement = GetConfigMessageTemplate();
                sReturn = AppHelper.CallCWSService(messageElement.ToString());
                resultDoc = new XmlDocument();
                resultDoc.LoadXml(sReturn);
                return resultDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Sets the recent claim configuration
        /// </summary>
        /// <returns></returns>
        private XmlDocument SetRecentClaimConfigDetails()
        {
            XElement messageElement = null;
            XElement tempElement = null;
            string sReturn = string.Empty;
            XmlDocument resultDoc = null;

            try
            {
                messageElement = SetConfigMessageTemplate();

                tempElement = messageElement.XPathSelectElement("./Document/RecentClaimConfig/ClaimNumber");
                if (chkClaimNumber.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/RecentClaimConfig/Lob");
                if (chkLOB.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/RecentClaimConfig/EventNumber");
                if (chkEventNumber.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/RecentClaimConfig/DateOfEvent");
                if (chkDateOfEvent.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/RecentClaimConfig/DateOfClaim");
                if (chkDateOfClaim.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/RecentClaimConfig/Claimant");
                if (chkClaimant.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/RecentClaimConfig/ClaimType");
                if (chkClaimType.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/RecentClaimConfig/ClaimStatus");
                if (chkClaimStatus.Checked)
                {
                    tempElement.Value = "True";
                }

                tempElement = messageElement.XPathSelectElement("./Document/RecentClaimConfig/ClaimNumberHeader");
                tempElement.Value = txtClaimNumber.Value;

                tempElement = messageElement.XPathSelectElement("./Document/RecentClaimConfig/LobHeader");
                tempElement.Value = txtLOB.Value;

                tempElement = messageElement.XPathSelectElement("./Document/RecentClaimConfig/EventNumberHeader");
                tempElement.Value = txtEventNumber.Value;

                tempElement = messageElement.XPathSelectElement("./Document/RecentClaimConfig/DateOfEventHeader");
                tempElement.Value = txtDateOfEvent.Value;

                tempElement = messageElement.XPathSelectElement("./Document/RecentClaimConfig/DateOfClaimHeader");
                tempElement.Value = txtDateOfClaim.Value;

                tempElement = messageElement.XPathSelectElement("./Document/RecentClaimConfig/ClaimantHeader");
                tempElement.Value = txtClaimant.Value;

                tempElement = messageElement.XPathSelectElement("./Document/RecentClaimConfig/ClaimTypeHeader");
                tempElement.Value = txtClaimType.Value;

                tempElement = messageElement.XPathSelectElement("./Document/RecentClaimConfig/ClaimStatusHeader");
                tempElement.Value = txtClaimStatus.Value;


                sReturn = AppHelper.CallCWSService(messageElement.ToString());
                resultDoc = new XmlDocument();
                resultDoc.LoadXml(sReturn);
                return resultDoc;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Load the Values from the databse in to the respective controls
        /// </summary>
        private void LoadRecentClaimConfigDetails()
        {
            XmlDocument resultDoc = null;
            XmlNode diaryConfigNode = null;

            resultDoc = GetRecentClaimConfigDetails();

            if (resultDoc != null)
            {
                //set obtained values of Config
                diaryConfigNode = resultDoc.SelectSingleNode("//Document/RecentClaimConfig");

                if (diaryConfigNode != null)
                {
                    if (diaryConfigNode.SelectSingleNode("ClaimNumber").InnerText == "True")
                    {
                        chkClaimNumber.Checked = true;
                    }
                    else
                    {
                        chkClaimNumber.Checked = false;
                    }

                    if (diaryConfigNode.SelectSingleNode("Lob").InnerText == "True")
                    {
                        chkLOB.Checked = true;
                    }
                    else
                    {
                        chkLOB.Checked = false;
                    }

                    if (diaryConfigNode.SelectSingleNode("EventNumber").InnerText == "True")
                    {
                        chkEventNumber.Checked = true;
                    }
                    else
                    {
                        chkEventNumber.Checked = false;
                    }

                    if (diaryConfigNode.SelectSingleNode("DateOfEvent").InnerText == "True")
                    {
                        chkDateOfEvent.Checked = true;
                    }
                    else
                    {
                        chkDateOfEvent.Checked = false;
                    }

                    if (diaryConfigNode.SelectSingleNode("DateOfClaim").InnerText == "True")
                    {
                        chkDateOfClaim.Checked = true;
                    }
                    else
                    {
                        chkDateOfClaim.Checked = false;
                    }

                    if (diaryConfigNode.SelectSingleNode("Claimant").InnerText == "True")
                    {
                        chkClaimant.Checked = true;
                    }
                    else
                    {
                        chkClaimant.Checked = false;
                    }

                    if (diaryConfigNode.SelectSingleNode("ClaimType").InnerText == "True")
                    {
                        chkClaimType.Checked = true;
                    }
                    else
                    {
                        chkClaimType.Checked = false;
                    }
                    if (diaryConfigNode.SelectSingleNode("ClaimStatus").InnerText == "True")
                    {
                        chkClaimStatus.Checked = true;
                    }
                    else
                    {
                        chkClaimStatus.Checked = false;
                    }

                    txtClaimNumber.Value = diaryConfigNode.SelectSingleNode("ClaimNumberHeader").InnerText;
                    txtLOB.Value = diaryConfigNode.SelectSingleNode("LobHeader").InnerText;
                    txtEventNumber.Value = diaryConfigNode.SelectSingleNode("EventNumberHeader").InnerText;
                    txtDateOfEvent.Value = diaryConfigNode.SelectSingleNode("DateOfEventHeader").InnerText;
                    txtDateOfClaim.Value = diaryConfigNode.SelectSingleNode("DateOfClaimHeader").InnerText;
                    txtClaimant.Value = diaryConfigNode.SelectSingleNode("ClaimantHeader").InnerText;
                    txtClaimType.Value = diaryConfigNode.SelectSingleNode("ClaimTypeHeader").InnerText;
                    txtClaimStatus.Value = diaryConfigNode.SelectSingleNode("ClaimStatusHeader").InnerText;
                }
            }
        }
        #endregion

        protected void btnClaimNumber_Click(object sender, EventArgs e)
        {
            txtClaimNumber.Value = AppHelper.GetResourceValue(PageID, "chkClaimNumber", "0");
        }

        protected void btnLOB_Click(object sender, EventArgs e)
        {
            txtLOB.Value = AppHelper.GetResourceValue(PageID, "chkLOB", "0");
        }

        protected void btnEventNumber_Click(object sender, EventArgs e)
        {
            txtEventNumber.Value = AppHelper.GetResourceValue(PageID, "chkEventNumber", "0");
        }

        protected void btnDateOfEvent_Click(object sender, EventArgs e)
        {
            txtDateOfEvent.Value = AppHelper.GetResourceValue(PageID, "chkDateOfEvent", "0");
        }

        protected void btnDateOfClaim_Click(object sender, EventArgs e)
        {
            txtDateOfClaim.Value = AppHelper.GetResourceValue(PageID, "chkDateOfClaim", "0");
        }

        protected void btnClaimant_Click(object sender, EventArgs e)
        {
            txtClaimant.Value = AppHelper.GetResourceValue(PageID, "chkClaimant", "0");
        }

        protected void btnClaimType_Click(object sender, EventArgs e)
        {
            txtClaimType.Value = AppHelper.GetResourceValue(PageID, "chkClaimType", "0");
        }

        protected void btnClaimStatus_Click(object sender, EventArgs e)
        {
            txtClaimStatus.Value = AppHelper.GetResourceValue(PageID, "chkClaimStatus", "0");
        }
    }
}
