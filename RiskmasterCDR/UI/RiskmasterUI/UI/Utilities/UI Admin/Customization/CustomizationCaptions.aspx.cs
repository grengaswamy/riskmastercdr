﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.BusinessAdaptor.Common;
using System.Data;
using System.Xml.Linq;

namespace Riskmaster.UI.Utilities.UI_Admin.Customization
{
    public partial class CustomizationCaptions : NonFDMBasePageCWS
    {
        private string sCWSresponse = "";
        protected void Page_Load(object sender, EventArgs e)
        {
           if (!IsPostBack)
            {
                try
                {
                    XElement oMessageElement = GetMessageTemplate();

                    CallCWS("RMAdminSettingsAdaptor.GetAdminConfig", oMessageElement, out sCWSresponse, false, true);
                }
                catch (Exception ex)
                {
                    ErrorHelper.logErrors(ex);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ex, BusinessAdaptorErrorType.SystemError);
                    ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
                }

            }
        }
        protected void Save(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            bReturnStatus = CallCWSFunction("RMAdminSettingsAdaptor.SaveAdminConfig");
            if (bReturnStatus)
            {
                try
                {
                    XElement oMessageElement = GetMessageTemplate();
                    CallCWS("RMAdminSettingsAdaptor.GetAdminConfig", oMessageElement, out sCWSresponse, false, true);
                }
                catch (Exception ex)
                {
                    ErrorHelper.logErrors(ex);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ex, BusinessAdaptorErrorType.SystemError);
                    ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
                }
            }

        }

        protected void Refresh(object sender, EventArgs e)
        {
            try
            {
                XElement oMessageElement = GetMessageTemplate();
                CallCWS("RMAdminSettingsAdaptor.GetAdminConfig", oMessageElement, out sCWSresponse, false, true);
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization></Authorization> 
             <Call>
              <Function></Function> 
              </Call>
             <Document>
              <FileName>customize_captions</FileName> 
             </Document>
            </Message>


            ");

            return oTemplate;
        }
    }
}
