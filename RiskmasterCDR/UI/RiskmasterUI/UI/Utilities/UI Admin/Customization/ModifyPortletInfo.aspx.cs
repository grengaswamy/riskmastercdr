﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.UI.FDM;

namespace Riskmaster.UI.UI.Utilities.UI_Admin.Customization
{
    public partial class ModifyPortletInfo : GridPopupBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // aravi5 Modified for Jira Id: 7240 RMA Header Tab is not in proper design of rma standard in IE11 Full Mode
            btnOk.Attributes.Add("onclick", "javascript:Save()");
            btnCancel.Attributes.Add("onclick", "Close()");
            GridPopupPageload();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            FormMode.Value = "close";
            base.btnOk_Click(sender, e);
        }
    }
}
