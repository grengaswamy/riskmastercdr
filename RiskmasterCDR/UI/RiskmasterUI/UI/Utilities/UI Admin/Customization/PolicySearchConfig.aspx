﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PolicySearchConfig.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.UI_Admin.Customization.PolicySearchConfig" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
      <title>Policy Search Configuration</title>
    <script src="../../../../Scripts/form.js" type="text/javascript"></script>
    <script src="../../../../Scripts/ExecutiveSummary.js" type="text/javascript"></script>
    <script type="text/javascript" language="Javascript">
        function setDefaultValue(name, value) {
            document.getElementById(name).value = value;
            return false;
        }
    </script>
</head>
<body onload="if(parent.MDIScreenLoaded != null) parent.MDIScreenLoaded();">
    <form id="frmData" runat="server">
    <input type="hidden" value="" id="hiddenSelectedUser" />
    <input type="hidden" value="DiaryConfig" id="hTabName" />
    <uc1:ErrorControl ID="ErrorControl" runat="server" />
    <div class="msgheader" id="formtitle">
        Policy Search Configuration
    </div>
    <table border="0" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td>
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="Selected">
                            <a class="Selected" name="PolicySearchConfig"><span style="text-decoration: none">Policy Search Config</span>
                            </a>
                        </td>
                        <td style="border-bottom: none; border-left-style: solid; border-left-color: #999999;
                            border-width: 2px; border-top-style: solid; border-top-width: 2px; border-top-color: #FFFFFF;">
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr valign="top">
            <td valign="top">
                <div style="position: relative; left: 0; top: 0; width: 780px; height: 280px; overflow: auto;valign:top" class="singletopborder">
                    <table border="0" cellspacing="0" cellpadding="0" width="100%" valign="top" align="center">
                        <tr valign="top">
                            <td valign="top">
                                <table border="0" cellspacing="0" width="100%" align="center">
                                    <tr>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkSystemName" runat="server" checked ="checked" />System Name
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="System Name" runat="server" id="txtSystemName" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnSystemName" value="Default" onclick="return setDefaultValue('txtSystemName','System Name');"
                                                class="button" style="width: 50" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkPolicySymbol" runat="server" checked ="checked" />Policy Symbol
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Policy Symbol" runat="server" id="txtPolicySymbol" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnPolicySymbol" value="Default" onclick="return setDefaultValue('txtPolicySymbol','Policy Symbol');"
                                                class="button" style="width: 50" />
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" runat="server" id="chkModule" checked ="checked"/>Module
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" id="txtModule" runat="server"  value="Module" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnModule" value="Default" onclick="return setDefaultValue('txtModule','Module');" 
                                                class="button" style="width: 50" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" runat="server" id="chkLOB" checked ="checked"/>Line of Business
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" id="txtLOB" runat="server"  value="Line of Business" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnLOB" value="Default" onclick="return setDefaultValue('txtLOB','Line of Business');" 
                                                class="button" style="width: 50" />
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" runat="server" id="chkPolicyNumber" checked ="checked" />Policy Number
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Policy Number" runat="server" id="txtPolicyNumber" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnPolicyNumber" value="Default" onclick="return setDefaultValue('txtPolicyNumber','PolicyNumber');"
                                                class="button" style="width: 50" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkPolicyState" runat="server" checked ="checked"/>Policy State
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Policy State" runat="server" id="txtPolicyState" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnPolicyState" value="Default" onclick="return setDefaultValue('txtPolicyState','Policy State');"
                                                class="button" style="width: 50" />
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkLocationCompany" onclick="CheckboxClick()" runat="server" checked ="checked" />Location Company
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Location Company" id="txtLocationCompany" runat="server"/>
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnLocationCompany" value="Default" onclick="return setDefaultValue('txtLocationCompany','Location Company');"
                                                class="button" style="width: 50" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkDOL" runat="server" checked ="checked"/>Date of Loss
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Date of Loss" id="txtDOL" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnDOL" value="Default" onclick="return setDefaultValue('txtDOL','Date of Loss');"
                                                class="button" style="width: 50" />
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkAgentNumber" runat="server" checked ="checked" />Agent Number
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Agent Number" id="txtAgentNumber" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnAgentNumber" value="Default" onclick="return setDefaultValue('txtAgentNumber','Agent Number');"
                                                class="button" style="width: 50" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkInsuredZip" runat="server" checked ="checked"/>Insured Zip
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Insured Zip" id="txtInsuredZip" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnInsuredZip" value="Default" onclick="return setDefaultValue('txtInsuredZip','Insured Zip');"
                                                class="button" style="width: 50" />
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkCustomerNumber" runat="server" checked ="checked" />Customer Number
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Customer Number" id="txtCustomerNumber" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnCustomerNumber" value="Default" onclick="return setDefaultValue('txtCustomerNumber','Customer Number');"
                                                class="button" style="width: 50" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkMasterCompany" runat="server" checked ="checked"/>Master Company
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Master Company" id="txtMasterCompany" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnMasterCompany" value="Default" onclick="return setDefaultValue('txtMasterCompany','Master Company');"
                                                class="button" style="width: 50" />
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkInsuredCity" runat="server" checked ="checked" />Insured City
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Insured City" id="txtInsuredCity" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnInsuredCity" value="Default" onclick="return setDefaultValue('txtInsuredCity','Insured City');"
                                                class="button" style="width: 50" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkInsuredSSN" runat="server" checked ="checked"/>Insured SSN
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Insured SSN" id="txtInsuredSSN" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnInsuredSSN" value="Default" onclick="return setDefaultValue('txtInsuredSSN','Insured SSN');"
                                                class="button" style="width: 50" />
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkGroupNumber" runat="server" checked ="checked" />Group Number
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Group Number" id="txtGroupNumber" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnGroupNumber" value="Default" onclick="return setDefaultValue('txtGroupNumber','Group Number');"
                                                class="button" style="width: 50" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkInsuredLastName" runat="server" checked ="checked"/>Insured Last Name
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Insured Last Name" id="txtInsuredLastName" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnInsuredLastName" value="Default" onclick="return setDefaultValue('txtInsuredLastName','Insured Last Name');"
                                                class="button" style="width: 50" />
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkInsuredFirstName" runat="server" checked ="checked" />Insured First Name
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Insured First Name" id="txtInsuredFirstName" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnInsuredFirstName" value="Default" onclick="return setDefaultValue('txtInsuredFirstName','Insured First Name');"
                                                class="button" style="width: 50" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" valign="top">
                                            <input type="checkbox" id="chkInsuredName" runat="server" checked ="checked"/>Insured Name
                                        </td>
                                        <td width="20%" valign="top">
                                            <input type="text" value="Insured Name" id="txtInsuredName" runat="server" />
                                        </td>
                                        <td width="25%" valign="middle">
                                            <input type="button" id="btnInsuredName" value="Default" onclick="return setDefaultValue('txtInsuredName','Insured Name');"
                                                class="button" style="width: 50" />
                                        </td>                                       
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <table border="0" cellspacing="0" cellpadding="0" width="80%" valign="top">
                    <tr>
                        <td>
                            <div class="small">
                                Use this page to select the fields you want to see in Policy System Download Search screen.
                                <br>
                                Once you have made all of your selections, click the "Save Configuration" button
                                to store the settings for use.
                            </div>
                        </td>
                    </tr>
                </table>
                <table border="0" cellspacing="0" cellpadding="0" width="30%" valign="top">
                    <tr>
                        <td>
                            <asp:Button ID="btnSaveConfiguration" runat="server" Text="Save Configuration" 
                                CssClass="button" onclick="btnSaveConfiguration_Click" />
                        </td>
                        <td>
                            <input type="submit" value="Select All" id="btnSelectAll" class="button"
                                onclick="return SelectAll();; " />
                        </td>
                        <td>
                            <input type="submit" value="Unselect All" id="btnUnselectAll" class="button"
                                onclick="return UnselectAll();; " />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
