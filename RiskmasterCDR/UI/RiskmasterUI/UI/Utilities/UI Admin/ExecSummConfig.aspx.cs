﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Riskmaster.UI.ExecSummConfig
{
    public partial class ExecSummConfig : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            string sreturnValue = "";
            Control ctrlTemp = null;
            if (!IsPostBack)
            {
                //code changed by Nitin on 15/04/2009 for Mits 15424
                if (Request.QueryString["IsAdmin"] != null)
                {
                    IsAdminUser.Text = Request.QueryString["IsAdmin"]; 
                }
                else
                {
                    IsAdminUser.Text = "False";
                }

                bReturnStatus = CallCWSFunctionBind("ExecutiveSummaryAdaptor.GetUserPreference", out sreturnValue);
                if (bReturnStatus)
                {
                    XmlDocument xmlReturnValue = new XmlDocument();
                    xmlReturnValue.LoadXml(sreturnValue);

                    if (IsAdminUser.Text == "True")
                    {
                        BindpageControls(xmlReturnValue);
                    }
                    else
                    {
                        pnlAdmin.Visible = false;
                    }
                    
                    // Apply Security Settings. Set the Controls to Readonly if set from Security
                    ApplySecuritySettings(xmlReturnValue);

                    //Changed for MITS 19197  : Start
                    //08/26/2009 MITS 13153
                    if (xmlReturnValue.SelectSingleNode("//ExecutiveSummaryConfig/Policy/UseEnhPolFlag").InnerText == "-1")
                    {
                        //Control ctrlTemp = null;
                        ctrlTemp = this.FindControl("TABSpolicy");
                        if (ctrlTemp != null)
                            ctrlTemp.Visible = false;
                        ctrlTemp = this.FindControl("TABSpolicymgmt");
                        if (ctrlTemp != null)
                            ctrlTemp.Visible = true;
                    }
                    else if (xmlReturnValue.SelectSingleNode("//ExecutiveSummaryConfig/Policy/UseEnhPolFlag").InnerText == "1")
                    {
                        //Control ctrlTemp = null;
                        ctrlTemp = this.FindControl("TABSpolicy");
                        if (ctrlTemp != null)
                            ctrlTemp.Visible = true;
                        ctrlTemp = this.FindControl("TABSpolicymgmt");
                        if (ctrlTemp != null)
                            ctrlTemp.Visible = false;
                    }
                    //Changed for MITS 19197  : End

                    
                    //Rupal:start, r8 enh, when carrier claims is on, VA, DI and PC claims tab should not be visible
                    if (xmlReturnValue.SelectSingleNode("//ExecutiveSummaryConfig/CarrierClaims").InnerText == "-1")
                    {
                        //Control ctrlTemp = null;
                        ctrlTemp = this.FindControl("TABSvaclaims");
                        if (ctrlTemp != null)
                            ctrlTemp.Visible = false;
                        ctrlTemp = this.FindControl("TABSpcclaims");
                        if (ctrlTemp != null)
                            ctrlTemp.Visible = false;
                        ctrlTemp = this.FindControl("TABSdiclaims");
                        if (ctrlTemp != null)
                            ctrlTemp.Visible = false;

                        //Aman MITS 27175--Start
                        if (xmlReturnValue.SelectSingleNode("//ExecutiveSummaryConfig/UseGCClaims").InnerText != "True")
                        {
                            //if UseGCClaims is set to false then make the tab invisible
                            ctrlTemp = null;
                            ctrlTemp = this.FindControl("TABSgcclaims");
                            if (ctrlTemp != null)
                                ctrlTemp.Visible = false;
                        }
                        if (xmlReturnValue.SelectSingleNode("//ExecutiveSummaryConfig/UseWCClaims").InnerText != "True")
                        {
                            //if UseWCClaims is set to false then make the tab invisible
                            ctrlTemp = null;
                            ctrlTemp = this.FindControl("TABSwcclaims");
                            if (ctrlTemp != null)
                                ctrlTemp.Visible = false;
                        }
                        //Aman MITS 27175--End
                    }
                    //Aman MITS 27175--Start
                    else
                    {
                        if (xmlReturnValue.SelectSingleNode("//ExecutiveSummaryConfig/UseNonOccClaims").InnerText != "True")
                        {
                            //if USEDIClaims is set to false then make the tab invisible
                            //Control ctrlTemp = null;
                            ctrlTemp = this.FindControl("TABSdiclaims");
                            if (ctrlTemp != null)
                                ctrlTemp.Visible = false;
                        }
                        if (xmlReturnValue.SelectSingleNode("//ExecutiveSummaryConfig/UseGCClaims").InnerText != "True")
                        {
                            //if UseGCClaims is set to false then make the tab invisible
                            //Control ctrlTemp = null;
                            ctrlTemp = this.FindControl("TABSgcclaims");
                            if (ctrlTemp != null)
                                ctrlTemp.Visible = false;
                        }
                        if (xmlReturnValue.SelectSingleNode("//ExecutiveSummaryConfig/UseWCClaims").InnerText != "True")
                        {
                            //if UseWCClaims is set to false then make the tab invisible
                            //Control ctrlTemp = null;
                            ctrlTemp = this.FindControl("TABSwcclaims");
                            if (ctrlTemp != null)
                                ctrlTemp.Visible = false;
                        }
                        if (xmlReturnValue.SelectSingleNode("//ExecutiveSummaryConfig/UseVAClaims").InnerText != "True")
                        {
                            //if UseVAClaims is set to false then make the tab invisible
                            //Control ctrlTemp = null;
                            ctrlTemp = this.FindControl("TABSvaclaims");
                            if (ctrlTemp != null)
                                ctrlTemp.Visible = false;
                        }
                        if (xmlReturnValue.SelectSingleNode("//ExecutiveSummaryConfig/UsePCClaims").InnerText != "True")
                        {
                            //if UsePCClaims is set to false then make the tab invisible
                            //Control ctrlTemp = null;
                            ctrlTemp = this.FindControl("TABSpcclaims");
                            if (ctrlTemp != null)
                                ctrlTemp.Visible = false;
                        }
                    }//Aman MITS 27175--End
                    //rupal:end
                    
                }
            }

        }

        protected void AddUserGroup(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            string sreturnValue = "";

            //code changed by Nitin on 15/04/2009 for Mits 15424
            if (hiddenSelectedUser.Text != "")
            {
                UserId.Text = hiddenSelectedUser.Text;
            }

            bReturnStatus = CallCWSFunctionBind("ExecutiveSummaryAdaptor.GetUserPreference", out sreturnValue);
             if (bReturnStatus)
             {
                 XmlDocument xmlReturnValue = new XmlDocument();
                 xmlReturnValue.LoadXml(sreturnValue);
                 BindpageControls(xmlReturnValue);

                 // Apply Security Settings. Set the Controls to Readonly if set from Security
                 ApplySecuritySettings(xmlReturnValue);

                 //changed by gagan for mits 21226 : Start

                 //08/26/2009 MITS 13153
                 if (xmlReturnValue.SelectSingleNode("//ExecutiveSummaryConfig/Policy/UseEnhPolFlag").InnerText == "-1")
                 {
                     Control ctrlTemp = null;
                     ctrlTemp = this.FindControl("TABSpolicy");
                     if (ctrlTemp != null)
                         ctrlTemp.Visible = false;
                 }

                 else if (xmlReturnValue.SelectSingleNode("//ExecutiveSummaryConfig/Policy/UseEnhPolFlag").InnerText == "1")
                 {
                     Control ctrlTemp = null;
                     ctrlTemp = this.FindControl("TABSpolicymgmt");
                     if (ctrlTemp != null)
                         ctrlTemp.Visible = false;
                 }

                 


                 //changed by gagan for mits 21226 : End

             }
        }

        protected void DeleteUserGroup(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            string sreturnValue = "";

            //code changed by Nitin on 15/04/2009 for Mits 15424
            if (hiddenSelectedUser.Text != "")
            {
                UserId.Text = hiddenSelectedUser.Text;
            }
            
            bReturnStatus = CallCWSFunctionBind("ExecutiveSummaryAdaptor.GetUserPreference", out sreturnValue);
            if (bReturnStatus)
            {
                XmlDocument xmlReturnValue = new XmlDocument();
                xmlReturnValue.LoadXml(sreturnValue);
                
                BindpageControls(xmlReturnValue);

                // Apply Security Settings. Set the Controls to Readonly if set from Security
                ApplySecuritySettings(xmlReturnValue);

                //08/26/2009 MITS 13153
                if (xmlReturnValue.SelectSingleNode("//ExecutiveSummaryConfig/Policy/UseEnhPolFlag").InnerText == "-1")
                {
                    Control ctrlTemp = null;
                    ctrlTemp = this.FindControl("TABSpolicy");
                    if (ctrlTemp != null)
                        ctrlTemp.Visible = false;
                }
            }
        }

        protected void Save(object sender, EventArgs e)
        {
            string sreturnValue = "";
            bool bReturnStatus = false;
            
            if (IsAdminUser.Text == "True")
            {
                UserIDs.Text = hiddenSelectedUser.Text;
            }
            
            bReturnStatus = CallCWSFunction("ExecutiveSummaryAdaptor.SaveUserPreference", out sreturnValue);

             //igupta3 Mits : 31732 changes starts
            if (bReturnStatus)
            {
                UpdateLists();
            }           
        }
        
        private void UpdateLists()
        {
            string svalues = string.Empty;
            string[] lsthndValues = txtRelatedComponents.Text.Split(',');
            ListItem lstItem = null;

            foreach (ListItem lstItemvalue in lstUsersGroups.Items)
            {
                cboAllUsersGroups.Items.Add(lstItemvalue);

            }
            lstUsersGroups.Items.Clear();
            for (int Icount = 0; Icount < lsthndValues.Length; Icount++)
            {

                lstItem = cboAllUsersGroups.Items.FindByValue(lsthndValues[Icount]);
                if (cboAllUsersGroups.Items.Contains(lstItem))
                {
                    lstUsersGroups.Items.Add(lstItem);
                    cboAllUsersGroups.Items.Remove(lstItem);
                }
            }

        }
        //igupta3 Mits : 31732 changes ends        


        private void BindpageControls(XmlDocument usersXDoc)
        {
            DataSet usersRecordsSet = null;
            
            usersRecordsSet = ConvertXmlDocToDataSet(usersXDoc);
            ListBox lstAllUsersGroup = (ListBox)this.Form.FindControl("cboAllUsersGroups"); //igupta3 Mits : 31732
            //DropDownList lstAllUsersGroup = (DropDownList)this.Form.FindControl("cboAllUsersGroups");
            lstAllUsersGroup.DataSource = usersRecordsSet.Tables["User"].DefaultView;
            lstAllUsersGroup.DataTextField = "Name";
            lstAllUsersGroup.DataValueField = "UserID";
            lstAllUsersGroup.DataBind();
            usersRecordsSet.Dispose();

        }


        private void ApplySecuritySettings(XmlDocument objAtrributesDoc)
        {
            XmlNodeList objNodeList = objAtrributesDoc.SelectNodes("//ExecutiveSummaryConfig/*");
            string sControlName = string.Empty;
            CheckBox chkControl;
            foreach (XmlNode objNode in objNodeList)
            {
                //if (objNode.Name != "Users" && objNode.Name != "CaseManagementFlag")
                if (objNode.Name != "Users" && objNode.Name != "CaseManagementFlag" && objNode.Name != "CarrierClaims" && objNode.Name != "UseNonOccClaims" && objNode.Name != "UseGCClaims" && objNode.Name != "UseWCClaims" && objNode.Name != "UseVAClaims" && objNode.Name != "UsePCClaims") //Aman MITS 27175
                {
                    foreach(XmlElement objElem in objNode)
                    {
                        // Retrieve the Control. Enable it
                        sControlName = "chk" + objElem.Name + objNode.Name;
                        chkControl = (CheckBox) this.FindControl(sControlName);
                        if(chkControl != null)
                        {
                            chkControl.Enabled = true;
                            if (objElem.Attributes["readonly"] != null && objElem.Attributes["readonly"].Value.ToLower() == "true")
                            {
                                chkControl.Enabled = false;
                            }
                        }
                    }
                }
            }
        }
    }
}
