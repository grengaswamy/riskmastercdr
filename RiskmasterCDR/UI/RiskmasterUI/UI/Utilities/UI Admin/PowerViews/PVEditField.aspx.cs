﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.UI.Utilities.UI_Admin.PowerViews
{
    public partial class PVEditField : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("PVEditField.aspx"), "PVEditFieldValidations", ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "PVEditFieldValidations", sValidationResources, true);
        }
    }
}
