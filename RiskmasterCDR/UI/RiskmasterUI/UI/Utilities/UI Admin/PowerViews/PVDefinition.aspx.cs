﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Utilities
{
    public partial class PVDefinition : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            string sreturnValue = "";
            if (!IsPostBack)
            {
                Page OriginalPage = (Page)Context.Handler;
                TextBox textOrigRowId = (TextBox)OriginalPage.FindControl("rowid");
                TextBox textOrigPVName = (TextBox)OriginalPage.FindControl("pvname");
                Label lblPVName = (Label)this.Form.FindControl("lblViewName");
                Label lblOrigPVName = (Label)OriginalPage.FindControl("lblViewName");
                TextBox txtRowId = (TextBox)this.Form.FindControl("rowid");
                if (txtRowId != null && textOrigRowId != null && textOrigRowId.Text != "")
                {
                    txtRowId.Text = textOrigRowId.Text;

                }
                if (lblPVName != null && textOrigPVName != null && textOrigPVName.Text != "")
                {
                    lblPVName.Text = textOrigPVName.Text;


                }
                else if (lblPVName != null && lblOrigPVName != null && lblOrigPVName.Text != "")
                {
                    lblPVName.Text = lblOrigPVName.Text;


                }
                //XElement oTemplate = GetMessageTemplate();
                bReturnStatus = CallCWSFunction("PVDefinationAdaptor.Get", out sreturnValue);
                if (bReturnStatus)
                {
                    BindPageControls(sreturnValue);
                }
            }
        }
        protected void GoToPVList(object sender, EventArgs e)
        {
            TextBox txtSave = (TextBox)this.Form.FindControl("txtSave");
            if (txtSave != null && txtSave.Text == "Save")
            {
                SaveData(true);
            }

            Response.Redirect("PVList.aspx");
        }
        protected void Save(object sender, EventArgs e)
        {
            SaveData(false);
        }
        protected void GoToFormEdit(object sender, EventArgs e)
        {
            
            Server.Transfer("PVFormEdit.aspx");
        }
        private void SaveData(bool bGoToPVList)
        {
            bool bReturnStatus = false;
            string sreturnValue = "";
            try
            {
                bReturnStatus = CallCWSFunction("PVDefinationAdaptor.Save", out sreturnValue);

                ErrorControl1.errorDom = sreturnValue;

                XmlDocument xmlServiceDoc = new XmlDocument();
                xmlServiceDoc.LoadXml(sreturnValue);

                if ((xmlServiceDoc.SelectSingleNode("//MsgStatusCd").InnerText == "Success") && !bGoToPVList)
                {
                    bReturnStatus = CallCWSFunction("PVDefinationAdaptor.Get", out sreturnValue);
                    BindPageControls(sreturnValue);
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }
        private void BindPageControls(string sreturnValue)
        {
            DataSet powerViewDefinitionRecordsSet = null;
            XmlDocument powerViewDefinitionXDoc = new XmlDocument();
            powerViewDefinitionXDoc.LoadXml(sreturnValue);
            powerViewDefinitionRecordsSet = ConvertXmlDocToDataSet(powerViewDefinitionXDoc);
            
            DropDownList lstAvailForms = (DropDownList)this.Form.FindControl("cboAvailForms");
            if (powerViewDefinitionRecordsSet.Tables["AvailFormsoption"] != null)
            {
                
                lstAvailForms.DataSource = powerViewDefinitionRecordsSet.Tables["AvailFormsoption"].DefaultView;
                lstAvailForms.DataTextField = "AvailFormsoption_Text";
                lstAvailForms.DataValueField = "value";
                lstAvailForms.DataBind();
            }
            else
            {
                lstAvailForms.Items.Clear();

            }
            DropDownList lstAvailUsers = (DropDownList)this.Form.FindControl("cboAvialUser");
            if (powerViewDefinitionRecordsSet.Tables["AvialUseroption"] != null)
            {
                
                lstAvailUsers.DataSource = powerViewDefinitionRecordsSet.Tables["AvialUseroption"].DefaultView;
                lstAvailUsers.DataTextField = "AvialUseroption_Text";
                lstAvailUsers.DataValueField = "value";
                lstAvailUsers.DataBind();
            }
            else
            {
                lstAvailUsers.Items.Clear();
            }
            SortCboUserList(ref lstAvailUsers);

            ListBox lstFormList = (ListBox)this.Form.FindControl("lstFormList");
            if (powerViewDefinitionRecordsSet.Tables["FormListoption"] != null)
            {
                
                lstFormList.DataSource = powerViewDefinitionRecordsSet.Tables["FormListoption"].DefaultView;
                lstFormList.DataTextField = "FormListoption_Text";
                lstFormList.DataValueField = "value";
                lstFormList.DataBind();
            }
            else
            {
                lstFormList.Items.Clear();
            }
            ListBox lstUserList = (ListBox)this.Form.FindControl("lstUserList");
            if (powerViewDefinitionRecordsSet.Tables["UserListoption"] != null && powerViewDefinitionRecordsSet.Tables["UserListoption"].Columns.Contains("UserListoption_Text"))
            {
               
                lstUserList.DataSource = powerViewDefinitionRecordsSet.Tables["UserListoption"].DefaultView;
                lstUserList.DataTextField = "UserListoption_Text";
                lstUserList.DataValueField = "value";
                lstUserList.DataBind();
            }
            else
            {
                lstUserList.Items.Clear();
            }

            powerViewDefinitionRecordsSet.Dispose();
        }

        private void SortCboUserList(ref DropDownList objDDL)
        {
            ArrayList arrTextList = new ArrayList();
            ArrayList arrValueList = new ArrayList();
          
            foreach (ListItem li in objDDL.Items)
            {
                arrTextList.Add(li.Text);
            }

            arrTextList.Sort();


            foreach (object item in arrTextList)
            {
                string value = objDDL.Items.FindByText(item.ToString()).Value;
                arrValueList.Add(value);
            }
            objDDL.Items.Clear();

            for (int i = 0; i < arrTextList.Count; i++)
            {
                ListItem objItem = new ListItem(arrTextList[i].ToString(), arrValueList[i].ToString());
                objDDL.Items.Add(objItem);

            }

        }


    }
}
