﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PVEditField.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.UI_Admin.PowerViews.PVEditField" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <script>
    var pOptions = new Array();
    pOptions.length = 0;
    var globalIndex = -1;
    var m_DataChange = false;
    function Save() {
        if ((window.opener.document.forms[0].FieldVisible.value == "1") &&
            (document.forms[0].required.checked) &&
            (window.opener.document.forms[0].IsBaseEditAllow.value == "False")) {
            document.forms[0].chkReadOnly.checked = false;
        }
        //document.forms[0] = document.forms[0]; //srajindersin MITS 34846 1/7/2014
        if (trimIt(document.forms[0].txtCaption.value) == "") {

            alert("Please enter the Caption.");
            document.forms[0].txtCaption.focus();
            return false;
        }

        if (document.forms[0].txtCaption.value.search("-") >= 0) {
            //nadim MITS 7408
            if (document.forms[0].txtCaption.value.substr(0, 1) == "-") {
                alert("'-' character cannot be put at first position");
                return false;
            }
            //else
            //alert("'-' character cannot be part of field caption.");
            // return false;
            //MITS 7408
        }

        if (document.forms[0].txtCaption.value.search("[\[]") >= 0) {
            alert("'[' character cannot be part of field caption.");
            return false;
        }

        if (document.forms[0].txtCaption.value.search("[\]]") >= 0) {
            alert("']' character cannot be part of field caption.");
            return false;
        }

        if (window.opener != null) {
            window.opener.OnEditFieldOK();
        }
        return true;
    }

    function Cancel() {
        self.close();
    }

    function handleUnload() {
        if (window.opener != null)
            window.opener.onWndClose();
        return true;
    }
    //Geeta 02/02/07 : Modified for FMLA Grid Edit field 
    function handleLoad() {
        if (document.forms[0].txtCaption != null && document.forms[0].txtCaption.disabled == false)
            document.forms[0].txtCaption.focus();
        return true;
    }
    function trimIt(aString) {
        // RETURNS INPUT ARGUMENT WITHOUT ANY LEADING OR TRAILING SPACES
        return (aString.replace(/^ +/, '')).replace(/ +$/, '');
    }
    var sName, sCaption;

    //var sValueParent = window.opener.document.forms[0].lstFormList.options[window.opener.document.forms[0].lstFormList.selectedIndex].text;
    var sFieldCaption = window.opener.document.forms[0].FieldCaption.value;  

</script>
    <script type="text/javascript">
        function OnloadFuncntion() {
            var IsfieldRequired = 0;
            var FieldDisabled = false;
            document.forms[0].txtCaption.value = window.opener.document.forms[0].FieldName.value;
            document.forms[0].txtHelp.value = window.opener.document.forms[0].HelpMsg.value;
            var obj;
            obj = document.getElementById("trcheck");
            
            if (window.opener.document.forms[0].FieldVisible.value == "1") {
                if (window.opener.document.forms[0].FieldRequired.value == "0")
                    document.forms[0].required.checked = false;
                else
                {
                    document.forms[0].required.checked = true;
                    IsfieldRequired = 1;
                }
            }
            else {
                obj.style.visibility = "hidden";
                obj.style.display = "none";
                obj = document.getElementById("trReadonly");
                if (obj != null) {
                    obj.style.visibility = "hidden";
                    obj.style.display = "none";
                }
            }
            if (window.opener.document.forms[0].BaseReadonly.value == "1") {
                document.forms[0].chkReadOnly.disabled = true;
                document.forms[0].chkReadOnly.checked = true;
                FieldDisabled = true;
                document.getElementById('lblMessage').innerHTML = PVEditFieldValidations.lblMessageReadonlymsg;
            }
            //if (window.opener.document.forms[0].IsBaseEditAllow.value == "True" && window.opener.document.forms[0].FieldDisabled.value == "true") {//JIRA 8436
            if (window.opener.document.forms[0].IsBaseEditAllow.value == "True" && (IsfieldRequired == 1)) {
                document.getElementById('lblMessage').innerHTML = PVEditFieldValidations.lblMessageRequiredSavemsg;
                //} else if (window.opener.document.forms[0].IsBaseEditAllow.value == "False" && window.opener.document.forms[0].FieldDisabled.value == "true") {//JIRA 8436
            } else if (window.opener.document.forms[0].IsBaseEditAllow.value == "False" && (IsfieldRequired == 1)) {
                document.getElementById('lblMessage').innerHTML = PVEditFieldValidations.lblMessageRequiredmsg;
                document.forms[0].chkReadOnly.disabled = true;
                document.forms[0].chkReadOnly.checked = false;
                //chkreadonlystate = 1;
            }
            if (window.opener.document.forms[0].FieldReadOnly.value == "true")
                document.forms[0].chkReadOnly.checked = true;
            
            if (window.opener.document.forms[0].FieldDisabled.value == "true" || (FieldDisabled))
                document.forms[0].required.disabled = true;
            else
                document.forms[0].required.disabled = false;

            //-- ABhateja, 08.24.2006, MITS 7629 -START-
            if (window.opener.document.forms[0].CaptionDisabled.value == "true") {
                document.forms[0].txtCaption.disabled = true;
                document.forms[0].required.disabled = true;
            }
            else {
                if (window.opener.document.forms[0].NoCaption.value == "true")//rjhamb Mits 22221 Disabling only the caption field
                {
                    document.forms[0].txtCaption.disabled = true;
                }
                // No need to enable required field once it disable from above cases.
                //else {
                //    document.forms[0].required.disabled = false;
                //}
                //document.forms[0].required.disabled = false;
            }

            //-- ABhateja, 08.24.2006, MITS 7629 -END-
            if (window.opener.document.forms[0].FieldType.value == "Caption")
                document.forms[0].groupcaption.value = "1";
            else if (window.opener.document.forms[0].FieldType.value == "Button")
                document.forms[0].buttoncaption.value = "1";
            else if (window.opener.document.forms[0].FieldType.value == "Toolbar")
                document.forms[0].toolbarcaption.value = "1";
            else if (window.opener.document.forms[0].FieldType.value == "Message")
                document.forms[0].messagecaption.value = "1";

            obj = document.getElementById("txtHelp");
            if (window.opener.document.forms[0].FieldCaption.value != "Field")
                obj.parentElement.parentElement.style.display = "none";
        }
    </script>
</head>
<body onload="OnloadFuncntion();">
    <form id="form1" runat="server">
    <div>
    <input type="hidden" id="groupcaption" value="0"><input type="hidden" id="buttoncaption" value="0"><input type="hidden" id="toolbarcaption" value="0"><input type="hidden" id="messagecaption" value="0"><table width="95%" border="0" align="center">
    <tr>
     <td class="msgheader" colspan="2">Edit&nbsp;<script language="javascript">window.document.write(sFieldCaption);</script></td>
    </tr>
    <tr>
     <td class="required">Caption:</td>
     <td><input type="text" size="22" id="txtCaption" maxlength="50"></td>
    </tr>
    <tr>
     <td>Help Message:</td>
     <td><textarea type="text" cols="40" rows="3" id="txtHelp" maxlength="50"></textarea></td>
    </tr>
    <tr id="trcheck">
     <td>
      Required:
      
     </td>
     <td><input type="checkbox" id="required" value="1"></td>
    </tr>
        <tr id="trReadonly">
            <td>Read-Only:
      
            </td>
            <td style="font-style: italic; font-size: 9pt; font-weight: normal;" colspan="2">
                <input type="checkbox" id="chkReadOnly" value="1"/> 
                <asp:Label ID="lblMessage" runat="server" Text="" ></asp:Label>
            </td>
        </tr>
        <tr/>
        <tr/>
        <tr></tr>
        <tr></tr>
    <tr>
     <td colspan="2" align="center"><input type="button" class="button" value="  OK  " onClick="Save();"><input type="button" class="button" value="Cancel" onClick="Cancel();"></td>
    </tr>
   </table>
    </div>
    
    </form>
</body>
</html>
