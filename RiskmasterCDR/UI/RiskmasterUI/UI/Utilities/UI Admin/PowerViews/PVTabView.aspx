﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PVTabView.aspx.cs" Inherits="Riskmaster.UI.UI.Utilities.UI_Admin.PowerViews.PVTabView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Tab Views</title>
    <script language="javascript" type="text/javascript">
        function onPageLoad() {
            document.forms[0].FieldCaptionList.value = window.opener.document.forms[0].FieldCaptionList.value;

            //Logic for making table follows

            var row;
            var col;
            var intnew = 0;
            var sString = window.opener.document.forms[0].FieldCaptionList.value;
            var tbody = document.getElementById("tbl").getElementsByTagName("TBODY")[0];
            var arrString = sString.split('|');
            var i = 0;
            for (i = 0; i < arrString.length; i++) {
                if (arrString[i] == "")
                    continue;
                if (arrString[i].substring(0, 5) == "[*** ") {
                    if (intnew == 1) {
                        row = document.createElement("TR");
                        col = document.createElement("TD");
                        col.setAttribute("WIDTH", "50%");
                        col.innerHTML = " ";
                        row.appendChild(col);
                        tbody.appendChild(row);
                    }
                    row = document.createElement("TR");
                    col = document.createElement("TD");
                    col.setAttribute("WIDTH", "50%");
                    col.setAttribute("CLASS", "ctrlgroup");
                    col.setAttribute("COLSPAN", "2");
                    col.innerHTML = arrString[i];
                    row.appendChild(col);
                    tbody.appendChild(row);
                    intNew = 0;
                }
                else if (arrString[i].substring(0, 5) == "[MS* ") {
                    if (intnew == 1) {
                        row = document.createElement("TR");
                        col = document.createElement("TD");
                        col.setAttribute("WIDTH", "50%");
                        col.innerHTML = " ";
                        row.appendChild(col);
                        tbody.appendChild(row);
                    }
                    row = document.createElement("TR");
                    col = document.createElement("TD");
                    col.setAttribute("WIDTH", "50%");
                    col.setAttribute("colspan", "2");
                    col.innerHTML = arrString[i];
                    row.appendChild(col);
                    tbody.appendChild(row);
                    intNew = 0;
                }
                else if (intnew == 0) {
                    row = document.createElement("TR");
                    col = document.createElement("TD");
                    col.setAttribute("WIDTH", "50%");
                    col.innerHTML = arrString[i];
                    row.appendChild(col);
                    tbody.appendChild(row);
                    intnew = 1;
                }
                else {
                    col = document.createElement("TD");
                    col.setAttribute("WIDTH", "50%");
                    col.innerHTML = arrString[i];
                    row.appendChild(col);
                    tbody.appendChild(row);
                    intnew = 0;
                }
            }
            //Logic for making table ends
            return true;
        }

    
    </script>
</head>
<body onload="onPageLoad()" >
    <form id="frmData" name="frmData" runat="server" >
    <TABLE id="tbl"  WIDTH="100%" border = "1"  CELLSPACING="0" CELLPADDING="0">
		<TBODY id="tabMain"></TBODY>
	</TABLE>
	
	<input type="hidden" id="FieldCaptionList" />
    </form>
</body>
</html>
