﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.Utilities
{
    public partial class PVList : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            string sreturnValue = "";
            //MITS 34921   - rkulavil begin
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("PVList.aspx"), "PVListValidations",
                                            ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "PVListValidations", sValidationResources, true);
            //MITS 34921   - rkulavil end
            if (!IsPostBack)
            {
               
                bReturnStatus = CallCWSFunction("PVListAdaptor.Get", out sreturnValue);
                if (bReturnStatus)
                {
                    BindPageControls(sreturnValue);
                }

            }

        }
        protected void Clone(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            string sRowId = ((HtmlControl)sender).Attributes["rowid"];
            string sPVName = ((HtmlControl)sender).Attributes["pvname"];
            string sreturnValue = "";
            TextBox txtPVName = (TextBox)this.Form.FindControl("pvname");
            if (txtPVName != null)
            {
                txtPVName.Text = sPVName;
            }
            TextBox txtRowId = (TextBox)this.Form.FindControl("rowid");
            if (txtRowId != null)
            {
                txtRowId.Text = sRowId;
            }
            bReturnStatus = CallCWSFunctionBind("PVCreateAdaptor.Clone", out sreturnValue);
            if (bReturnStatus)
            {
               
                bReturnStatus = CallCWSFunction("PVListAdaptor.Get", out sreturnValue);
                if (bReturnStatus)
                {
                    BindPageControls(sreturnValue);
                }
            }

        }
        protected void Delete(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            string sRowId = ((HtmlControl)sender).Attributes["rowid"];
            string sPVName = ((HtmlControl)sender).Attributes["pvname"];
            string sreturnValue = "";
            TextBox txtPVName = (TextBox)this.Form.FindControl("pvname");
            if (txtPVName != null)
            {
                txtPVName.Text = sPVName;
            }
            TextBox txtRowId = (TextBox)this.Form.FindControl("rowid");
            if (txtRowId != null)
            {
                txtRowId.Text = sRowId;
            }
            bReturnStatus = CallCWSFunctionBind("PVCreateAdaptor.Delete", out sreturnValue);
            if (bReturnStatus)
            {

                bReturnStatus = CallCWSFunction("PVListAdaptor.Get", out sreturnValue);
                if (bReturnStatus)
                {
                    BindPageControls(sreturnValue);
                }
            }
        }
        protected void Edit(object sender, EventArgs e)
        {
            string sRowId = ((HtmlControl)sender).Attributes["rowid"];
            TextBox txtRowId = (TextBox)this.Form.FindControl("rowid");
            if (txtRowId != null)
            {
                txtRowId.Text = sRowId;
            }
            Server.Transfer("PVCreate.aspx");


        }
        protected void GoToDefinition(object sender, EventArgs e)
        {
            string sRowId = ((HtmlControl)sender).Attributes["rowid"];
            string sPVName = ((HtmlControl)sender).Attributes["pvname"];
            TextBox txtRowId = (TextBox)this.Form.FindControl("rowid");
            if (txtRowId != null)
            {
                txtRowId.Text = sRowId;
            }
            TextBox txtPVName = (TextBox)this.Form.FindControl("pvname");
            if (txtPVName != null)
            {
                txtPVName.Text = sPVName;
            }
            Server.Transfer("PVDefinition.aspx");


        }
        protected void CreateNew(object sender, EventArgs e)
        {
            Response.Redirect("PVCreate.aspx");
        }
        private void BindPageControls(string sreturnValue)
        {
            DataSet powerViewListRecordsSet = null;
            XmlDocument powerViewListXDoc = new XmlDocument();
            powerViewListXDoc.LoadXml(sreturnValue);
            powerViewListRecordsSet = ConvertXmlDocToDataSet(powerViewListXDoc);
            if (powerViewListRecordsSet.Tables.Contains("rowtext"))
            {
                grdPowerViewList.DataSource = powerViewListRecordsSet.Tables["rowtext"];
                grdPowerViewList.DataBind();
                grdPowerViewList.Columns[grdPowerViewList.Columns.Count - 1].Visible = false;
            }
            else
            {
                grdPowerViewList.DataSource = null;
                grdPowerViewList.DataBind();
            }

        }
    }
}
