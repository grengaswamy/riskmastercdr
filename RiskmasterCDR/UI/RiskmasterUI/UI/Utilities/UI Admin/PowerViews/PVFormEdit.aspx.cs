﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
namespace Riskmaster.UI.Utilities
{
    public partial class PVFormEdit : NonFDMBasePageCWS
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                bool b;
            }
            catch (Exception )
            {
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            string sreturnValue = "";
            if (!IsPostBack)
            {
                Page OriginalPage = (Page)Context.Handler;
                TextBox textOrigRowId = (TextBox)OriginalPage.FindControl("rowid");
                Label lblOrigPVName = (Label)OriginalPage.FindControl("lblViewName");
                Label lblPVName = (Label)this.Form.FindControl("lblViewName");
                TextBox txtRowId = (TextBox)this.Form.FindControl("rowid");
                TextBox txtFormName = (TextBox)this.Form.FindControl("txtFormName");
                ListBox lstFormList = (ListBox)OriginalPage.FindControl("lstFormList");
                if (lstFormList != null && txtFormName!=null)
                {
                    txtFormName.Text = lstFormList.SelectedValue;
                }
                if (txtRowId != null && textOrigRowId != null && textOrigRowId.Text != "")
                {
                    txtRowId.Text = textOrigRowId.Text;

                }
                if (lblPVName != null && lblOrigPVName != null && lblOrigPVName.Text != "")
                {
                    lblPVName.Text = lblOrigPVName.Text;


                }
               
                bReturnStatus = CallCWSFunctionBind("PVFormEditAdaptor.Get", out sreturnValue);
                if (bReturnStatus)
                {
                    BindPageControls(sreturnValue);

                    // akaushik5 Added for MITS 30290 Starts
                    if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["DynamicViews"]) && ConfigurationManager.AppSettings["DynamicViews"].Contains(txtFormName.Text.Replace(".xml", string.Empty)))
                    {
                        (this.Form.FindControl("ShowRequiredTabs") as TextBox).Text = "true";
                        this.CreateDynamicTabsCheckbox(sreturnValue);
                    }
                    else 
                    {
                        (this.Form.FindControl("ShowRequiredTabs") as TextBox).Text = "false";
                    }
                    // akaushik5 Added for MITS 30290 Ends
                }
            }
        }

        
        protected void Save(object sender, EventArgs e)
        {
            SaveData();
           
        }

        // akaushik5 Added for MITS 30290 Starts
        /// <summary>
        /// Creates the dynamic tabs checkbox.
        /// </summary>
        /// <param name="sreturnValue">The sreturn value.</param>
        private void CreateDynamicTabsCheckbox(string sreturnValue)
        {
            ListBox lstFormList = (ListBox)this.Form.FindControl("lstFormList");
            CheckBoxList checkBoxList = (CheckBoxList)this.Form.FindControl("chkListRequiredTabs");
            XmlDocument xmlServiceDoc = new XmlDocument();
            xmlServiceDoc.LoadXml(sreturnValue);
            string sRequiredTabs = string.Empty;

            if (xmlServiceDoc.SelectSingleNode("//form/group/FormRequiredTabs") != null)
            {
                sRequiredTabs = xmlServiceDoc.SelectSingleNode("//form/group/FormRequiredTabs").InnerText;
            }

            if (!object.ReferenceEquals(checkBoxList, null))
            {
                foreach (ListItem item in lstFormList.Items)
                {
                    if (item.Text.StartsWith("[*** "))
                    {
                        ListItem listItem = new ListItem(item.Text.Replace("[*** ", string.Empty).Replace(" ***]", string.Empty), item.Value);
                        listItem.Selected = sRequiredTabs.Contains(item.Value);
                        checkBoxList.Items.Add(listItem);
                    }
                }
            }
        }
        // akaushik5 Added for MITS 30290 Ends

        private void SaveData()
        {
            try
            {
                bool bReturnStatus = false;
                string sreturnValue = "";
                // akaushik5 Added for MITS 30290 Starts
                this.setRequiredTabs();
                // akaushik5 Added for MITS 30290 Ends
                bReturnStatus = CallCWSFunction("PVFormEditAdaptor.Save", out sreturnValue);
                ErrorControl1.errorDom = sreturnValue;

                XmlDocument xmlServiceDoc = new XmlDocument();
                xmlServiceDoc.LoadXml(sreturnValue);

                if ((xmlServiceDoc.SelectSingleNode("//MsgStatusCd").InnerText == "Success"))
                {
                    Server.Transfer("PVDefinition.aspx");
                }
            }
            catch(Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        // akaushik5 Added for MITS 30290 Starts
        /// <summary>
        /// Sets the required tabs.
        /// </summary>
        private void setRequiredTabs()
        {
            TextBox txtRequiredTabs = this.Form.FindControl("txtRequiredTabs") as TextBox;
            CheckBoxList chkListRequiredTabs = this.Form.FindControl("chkListRequiredTabs") as CheckBoxList;
            if (txtRequiredTabs != null && chkListRequiredTabs != null && chkListRequiredTabs.Items.Count > 0)
            { 
                txtRequiredTabs.Text = string.Empty;
                foreach(ListItem listItem in chkListRequiredTabs.Items)
                {
                    if (listItem.Selected)
                    {
                        txtRequiredTabs.Text += listItem.Value + ",";
                    }
                }

                txtRequiredTabs.Text = txtRequiredTabs.Text.TrimEnd(',');
            }
        }
        // akaushik5 Added for MITS 30290 Ends
        
        private void BindPageControls(string sreturnValue)
        {
            DataSet formEditRecordsSet = null;
            XmlDocument formEditXDoc = new XmlDocument();
            formEditXDoc.LoadXml(sreturnValue);
            formEditRecordsSet = ConvertXmlDocToDataSet(formEditXDoc);

            DropDownList cboFormFields = (DropDownList)this.Form.FindControl("cboFormFields");
            if (formEditRecordsSet.Tables["FormFieldsoption"] != null)
            {

                cboFormFields.DataSource = formEditRecordsSet.Tables["FormFieldsoption"].DefaultView;
                cboFormFields.DataTextField = "FormFieldsoption_Text";
                cboFormFields.DataValueField = "value";
                cboFormFields.DataBind();
                //asharma326 JIRA 6411
                var FormFieldsoptionColumns = formEditRecordsSet.Tables["FormFieldsoption"].Columns.Cast<DataColumn>()
                                  .Where(c => c.ColumnName.StartsWith("basereadonly"));
                if (FormFieldsoptionColumns.Count() > 0)
                {
                    for (int i = 0; i < cboFormFields.Items.Count; i++)
                    {
                        cboFormFields.Items[i].Attributes["basereadonly"] = (formEditRecordsSet.Tables["FormFieldsoption"].Rows[i]["basereadonly"]) == System.DBNull.Value ? string.Empty :
                            (string)(formEditRecordsSet.Tables["FormFieldsoption"].Rows[i]["basereadonly"]);
                    }
                }
            }
            else
            {
                cboFormFields.Items.Clear();

            }
            DropDownList cboCommandButtons = (DropDownList)this.Form.FindControl("cboCommandButtons");
            if (formEditRecordsSet.Tables["CommandButtonsoption"] != null)
            {

                cboCommandButtons.DataSource = formEditRecordsSet.Tables["CommandButtonsoption"].DefaultView;
                cboCommandButtons.DataTextField = "CommandButtonsoption_Text";
                cboCommandButtons.DataValueField = "value";
                cboCommandButtons.DataBind();
            }
            else
            {
                cboCommandButtons.Items.Clear();
            }
            DropDownList cboToolBarButtons = (DropDownList)this.Form.FindControl("cboToolBarButtons");
            if (formEditRecordsSet.Tables["ToolBarButtonsoption"] != null)
            {

                cboToolBarButtons.DataSource = formEditRecordsSet.Tables["ToolBarButtonsoption"].DefaultView;
                cboToolBarButtons.DataTextField = "ToolBarButtonsoption_Text";
                cboToolBarButtons.DataValueField = "value";
                cboToolBarButtons.DataBind();
            }
            else
            {
                cboToolBarButtons.Items.Clear();
            }
            ListBox lstFormList = (ListBox)this.Form.FindControl("lstFormList");
            if (formEditRecordsSet.Tables["FormListoption"] != null)
            {

                lstFormList.DataSource = formEditRecordsSet.Tables["FormListoption"].DefaultView;
                lstFormList.DataTextField = "FormListoption_Text";
                lstFormList.DataValueField = "value";
                lstFormList.DataBind();
                //asharma326 JIRA 6411
                var FormListoptionColumns = formEditRecordsSet.Tables["FormListoption"].Columns.Cast<DataColumn>()
                                  .Where(c => c.ColumnName.StartsWith("basereadonly"));
                if (FormListoptionColumns.Count() > 0)
                {
                    for (int i = 0; i < lstFormList.Items.Count; i++)
                    {
                        lstFormList.Items[i].Attributes["basereadonly"] = (formEditRecordsSet.Tables["FormListoption"].Rows[i]["basereadonly"]) == System.DBNull.Value ? string.Empty :
                            (string)(formEditRecordsSet.Tables["FormListoption"].Rows[i]["basereadonly"]);
                    }
                }
                var FormListoptionReadonlyColumns = formEditRecordsSet.Tables["FormListoption"].Columns.Cast<DataColumn>()
                                  .Where(c => c.ColumnName.StartsWith("powerviewreadonly"));
                if (FormListoptionReadonlyColumns.Count() > 0)
                {
                    for (int i = 0; i < lstFormList.Items.Count; i++)
                    {
                        lstFormList.Items[i].Attributes["powerviewreadonly"] = (formEditRecordsSet.Tables["FormListoption"].Rows[i]["powerviewreadonly"]) == System.DBNull.Value ? string.Empty :
                            (string)(formEditRecordsSet.Tables["FormListoption"].Rows[i]["powerviewreadonly"]);
                    }
                }
            }
            else
            {
                lstFormList.Items.Clear();
            }
            ListBox lstButtonList = (ListBox)this.Form.FindControl("lstButtonList");
            if (formEditRecordsSet.Tables["ButtonListoption"] != null)
            {

                lstButtonList.DataSource = formEditRecordsSet.Tables["ButtonListoption"].DefaultView;
                lstButtonList.DataTextField = "ButtonListoption_Text";
                lstButtonList.DataValueField = "value";
                lstButtonList.DataBind();
            }
            else
            {
                lstButtonList.Items.Clear();
            }
            ListBox lstToolBarList = (ListBox)this.Form.FindControl("lstToolBarList");
            if (formEditRecordsSet.Tables["ToolBarListoption"] != null)
            {

                lstToolBarList.DataSource = formEditRecordsSet.Tables["ToolBarListoption"].DefaultView;
                lstToolBarList.DataTextField = "ToolBarListoption_Text";
                lstToolBarList.DataValueField = "value";
                lstToolBarList.DataBind();
            }
            else
            {
                lstToolBarList.Items.Clear();
            }



            formEditRecordsSet.Dispose();

        }
        protected void Cancel(object sender, EventArgs e)
        {
          
            Server.Transfer("PVDefinition.aspx");


        }

        
        protected void btnReset_Click(object sender, EventArgs e)
        {
            txtReset.Text = "1";
            string sreturnValue = "";
            bool bReturnStatus = CallCWSFunctionBind("PVFormEditAdaptor.Get", out sreturnValue);
            if (bReturnStatus)
            {
                BindPageControls(sreturnValue);
            }

            txtReset.Text = "0";
        }

      

   
    }
}
