﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using System.IO;
using System.Xml.XPath;
using Riskmaster.UI.Shared.Controls;

namespace Riskmaster.UI.UI.Utilities.RSWSheetCustomization
{

    public partial class RSWCustomization : NonFDMBasePageCWS
    {
         XElement XmlTemplate = null;
    
         bool bReturnStatus = false;
         XmlDocument XmlDoc = new XmlDocument();
         string sreturnValue = string.Empty;
         string sNode = null;
         string sNotes = null;
         string strSelected = string.Empty;
         XmlNode XNodeTemp = null;
         int iClaimId = 0;
         string sXML = "";
        XmlNode objNode = null;
      

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (AppHelper.GetQueryStringValue("sClaimId") != null)
                {

                    iClaimId = Convert.ToInt32(AppHelper.GetQueryStringValue("sClaimId"));


                }
                if (iClaimId != 0)
                {
                    btnSave.Visible = false;
                    btnSubmit.Visible = true;
                    lstLOB.Enabled = false;
                    lstClaimType.Enabled = false;



                }
                else
                {
                    btnSave.Visible = true;
                    btnSubmit.Visible = false;
                    lstLOB.Enabled = true;
                    lstClaimType.Enabled = true;
                    frmCustomData.Attributes.Add("onload", "pageLoaded()");
                    ClientScript.RegisterStartupScript(this.GetType(), "script", "parent.MDIScreenLoaded();", true);
                }

            
                if (!IsPostBack)
                {

                    XmlTemplate = GetMessageTemplate();
                    bReturnStatus = CallCWS("RSWCustomizationAdaptor.GetLOB", XmlTemplate, out sreturnValue, false, true);
                    if (bReturnStatus)
                    {

                        XmlDoc.LoadXml(sreturnValue);
                        sNode = null;
                        sNode = (string)XmlDoc.SelectSingleNode("//LOB/LOBParms/LineOfBusiness/Types[@selected='1']/@value").Value;

                        sNode = null;

                        BindData(XmlDoc);
                        hdnXML.Text = XmlDoc.InnerXml;
                        sXML = hdnXML.Text;
                    }
                }
                if (state.Value != "btnSaveClicked")
                {
                    if (IsPostBack)
                    {

                        if (hdnAction.Text == "ReserveChange")
                        {
                            bReturnStatus = false;
                            sreturnValue = string.Empty;

                            ViewState["ClaimType"] = lstClaimType.SelectedIndex;
                            ViewState["ClaimTypeSelectedValue"] = lstClaimType.SelectedValue;
                            XmlTemplate = GetMessageTemplateForResClaimType();

                            bReturnStatus = CallCWS("RSWCustomizationAdaptor.GetLOB", XmlTemplate, out sreturnValue, true, true);
                            if (bReturnStatus)
                                XmlDoc.LoadXml(sreturnValue);
                            hdnAction.Text = string.Empty;
                            BindData(XmlDoc);
                            hdnXML.Text = XmlDoc.InnerXml;
                            sXML = hdnXML.Text;
                        }
                      
                        else if (hdnAddReservetype.Text == "SelectLOB")
                        {


                            XmlTemplate = GetMessageLOBTemplate();
                            bReturnStatus = CallCWS("RSWCustomizationAdaptor.GetLOB", XmlTemplate, out sreturnValue, false, true);
                            if (bReturnStatus)
                            {
                                // ErrorControl1.errorDom = sreturnValue;//For Getting Errors of webservice
                                XmlDoc.LoadXml(sreturnValue);
                            }
                            sNode = null;
                            LstReservetype.Items.Clear();
                            LstSelectTranstype.Items.Clear();
                            LstTransType.Items.Clear();

                            BindData(XmlDoc);

                            hdnXML.Text = XmlDoc.InnerXml;
                            sXML = hdnXML.Text;
                        }

                        else if (hdnAction.Text == "UpdateClaimResType")
                        {


                            XmlTemplate = GetMessageTemplate();
                            bReturnStatus = CallCWS("RSWCustomizationAdaptor.GetLOB", XmlTemplate, out sreturnValue, false, true);
                            if (bReturnStatus)
                            {
                                // ErrorControl1.errorDom = sreturnValue;//For Getting Errors of webservice
                                XmlDoc.LoadXml(sreturnValue);
                            }
                            sNode = null;
                            LstReservetype.Items.Clear();
                            LstSelectTranstype.Items.Clear();
                            LstTransType.Items.Clear();

                            BindData(XmlDoc);

                            hdnXML.Text = XmlDoc.InnerXml;
                        }

                        sXML = hdnXML.Text;
                    }
                }
                else if (hdnCriteria.Text != "")
                {
                    if (iClaimId != 0)
                    {
                        string strchecked = "";

                        if (hdnCriteria.Text != "")
                        {
                            if (chkSettings.Checked)
                                strchecked = "-1";
                            else
                                strchecked = "0";


                        }
                      
                        XmlDoc = GetXmlString(hdnXML.Text.ToString());
                        //  objNode = null;
                        objNode = XmlDoc.SelectSingleNode("//SysSetCustom");
                        if (objNode != null)
                        {
                            if (objNode.InnerText != "")
                            {
                                objNode.InnerText = strchecked;

                            }

                        }
                        hdnXML.Text = XmlDoc.InnerXml;
                    }

                }
                ViewState["lobSelected"] = lstLOB.SelectedValue;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        private XmlDocument GetXmlString(string strfile)
        {
            XmlDocument XMLdoc = null;

            try
            {
                 XMLdoc = new XmlDocument();
                 XMLdoc.LoadXml(strfile);
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return XMLdoc;
        }				
        private XElement GetMessageTemplateForResClaimType()
        {
            try
            {
                string strchecked = "";
                StringBuilder sXml = null;
                XElement oTemplate = null;
                string strClaimType = string.Empty;
                if (hdnCriteria.Text != "")
                {
                    if (chkSettings.Checked)
                        strchecked = "-1";
                    else
                        strchecked = "0";

                    if (chkSettings.Checked)
                        strClaimType = "True";
                    else
                        strClaimType = string.Empty;
                }
                if (ViewState["ClaimTypeSelectedValue"] != null)
                {
                    hdnClaimReserveTypes.Text = ViewState["ClaimTypeSelectedValue"].ToString();
                }
                else
                {
                    hdnClaimReserveTypes.Text = string.Empty;
                }
                sXml = new StringBuilder("<Message>");
                sXml.Append("<Authorization></Authorization>");
                sXml.Append("<Call><Function></Function></Call><Document><GetLOB>");
                sXml.Append("<SysSetCustom>" + strchecked + "</SysSetCustom>");
                sXml.Append("<LineOfBusiness >" + lstLOB.SelectedValue.ToString() + "</LineOfBusiness ><ResClaimType>" + strClaimType + "</ResClaimType><ClaimTypes>" + hdnClaimReserveTypes.Text + "</ClaimTypes></GetLOB>");
                sXml.Append("</Document></Message>");
                oTemplate = XElement.Parse(sXml.ToString());
                return oTemplate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
          
        }
       
        private XElement GetMessageTemplate()
        {
            string strchecked = "";
            string strClaimType = string.Empty;
            StringBuilder sXml;
            XElement oTemplate = null;
            try
            {
                if (hdnCriteria.Text != "")
                {
                    if (chkSettings.Checked)
                        strchecked = "-1";
                    else
                        strchecked = "0";

                    if (chkSettings.Checked)
                        strClaimType = "True";
                    else
                        strClaimType = string.Empty;

                }

                if (iClaimId != 0)
                {
                    sXml = new StringBuilder("<Message>");
                    sXml.Append("<Authorization></Authorization>");
                    sXml.Append("<Call><Function></Function></Call><Document><GetLOB>");
                    sXml.Append("<SysSetCustom>" + strchecked + "</SysSetCustom>");
                    sXml.Append("<ClaimId>" + iClaimId + "</ClaimId>");
                    sXml.Append("<LineOfBusiness >" + lstLOB.SelectedValue.ToString() + "</LineOfBusiness ><ResClaimType /><ClaimTypes /></GetLOB>");
                    sXml.Append("</Document></Message>");

                }
                else
                {
                    sXml = new StringBuilder("<Message>");
                    sXml.Append("<Authorization></Authorization>");
                    sXml.Append("<Call><Function></Function></Call><Document><GetLOB>");
                    sXml.Append("<SysSetCustom>" + strchecked + "</SysSetCustom>");
                    sXml.Append("<LineOfBusiness >" + lstLOB.SelectedValue.ToString() + "</LineOfBusiness ><ResClaimType /><ClaimTypes /></GetLOB>");
                    sXml.Append("</Document></Message>");

                }
                oTemplate = XElement.Parse(sXml.ToString());

                return oTemplate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }
        private XElement GetMessageLOBTemplate()
        {
            
            string strClaimType = string.Empty;
            StringBuilder sXml = null;
            XElement oTemplate = null;
            try
            {
                if (hdnCriteria.Text != string.Empty)
                {
                    //Mits:22613 skhare7
                    //if (chkSettings.Checked)
                    //    strchecked = "1";
                    //else
                    //    strchecked = "0";
                    //end Mits:22613
                    if (chkSettings.Checked)
                        strClaimType = "True";
                    else
                        strClaimType = string.Empty;
                }
                // if(hdnAddReservetype.Text="SelectLOB")

                sXml = new StringBuilder("<Message>");
                sXml.Append("<Authorization></Authorization>");
                sXml.Append("<Call><Function></Function></Call><Document><GetLOB>");
                //Mits:22613 skhare7 
                sXml.Append("<SysSetCustom/>");
               // sXml.Append("<SysSetCustom>" + "" + "</SysSetCustom>");
                //Mits:22613 skhare7 End
                sXml.Append("<LOBSelect>" + hdnAddReservetype.Text + "</LOBSelect>");
                sXml.Append("<LineOfBusiness >" + lstLOB.SelectedValue.ToString() + "</LineOfBusiness ><ResClaimType /><ClaimTypes /></GetLOB>");
                sXml.Append("</Document></Message>");
                oTemplate = XElement.Parse(sXml.ToString());
                return oTemplate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private XElement GetTransTypeTemplate()
        {
            string strClaimType = string.Empty;
            string strchecked = "";
            string sReserveType=null;
            StringBuilder sXml = null;
            XElement oTemplate = null;
            try
            {
                if (hdnCriteria.Text != "")
                {
                    if (chkSettings.Checked)
                        strClaimType = "True";
                    else
                        strClaimType = string.Empty;


                    if (chkSettings.Checked)
                        strchecked = "-1";
                    else
                        strchecked = "0";
                }
                if (ViewState["ClaimTypeSelectedValue"] != null)
                {
                    hdnClaimReserveTypes.Text = ViewState["ClaimTypeSelectedValue"].ToString();
                }
                else
                {
                    hdnClaimReserveTypes.Text = string.Empty;
                }
                if (ViewState["ReserveType"] != null)
                    sReserveType = ViewState["ReserveType"].ToString();

                sXml = new StringBuilder("<Message>");
                sXml.Append("<Authorization></Authorization>");
                sXml.Append("<Call><Function></Function></Call><Document><GetTranstype>");
                sXml.Append("<SysSetCustom>" + strchecked + "</SysSetCustom>");
                sXml.Append("<LineOfBusiness >" + lstLOB.SelectedValue.ToString() + "</LineOfBusiness ><ResClaimType>" + strClaimType + "</ResClaimType><ClaimTypes>" + hdnClaimReserveTypes.Text + "</ClaimTypes><TransReserveType>" + sReserveType + "</TransReserveType></GetTranstype>");
                sXml.Append("</Document></Message>");
                oTemplate = XElement.Parse(sXml.ToString());
                return oTemplate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       

      
        private void BindData(XmlDocument XmlDocData)
        {
            
            string sReserve = null;
            XmlNode objNode = null;
            ListItem item = null;          
            XmlNode NodeReserve = null;
            XmlNodeList nodes = null;
            try
            {
                ViewState["ReserveType"] = null;
                LstReservetype.Items.Clear();
                LstTransType.Items.Clear();
                LstSelectTranstype.Items.Clear();
                item=   new ListItem();
                NodeReserve = XmlDocData.SelectSingleNode("//Reserves");
                if (NodeReserve != null)
                {
                    if (!NodeReserve.HasChildNodes)
                    {
                        LstReservetype.Items.Clear();
                        LstTransType.Items.Clear();
                        LstSelectTranstype.Items.Clear();
                        ddlReserveType.Items.Clear();
                    }
                }


                nodes = XmlDocData.SelectNodes("//ReserveCustomized");


                int iTransCount = 0;
                iTransCount = ((nodes.Count) - (nodes.Count - 1));
                if (nodes.Count > 0)
                {
                    for (int i = 0; i <= nodes.Count - 1; i++)
                    {
                        item = new ListItem();
                        item.Text = nodes[i].Attributes["Name"].Value;
                        item.Value = nodes[i].Attributes["value"].Value;
                        // nodes[i].re
                        LstReservetype.Items.Add(item);

                        if (i == 0)
                        {
                            if (LstReservetype.Items.Count > 0)
                                LstReservetype.SelectedValue = nodes[i].Attributes["value"].Value;

                            if (ViewState["ReserveType"] == null)

                                ViewState["ReserveType"] = LstReservetype.SelectedValue;


                            XmlNode node = nodes[i];
                            foreach (XmlElement ele in node)
                            {
                                item = new ListItem();
                                if (ele.LocalName == "TransCustomTypes")
                                {
                                    if (node["TransCustomTypes"] != null)
                                    {
                                        item.Text = ele.InnerText;
                                        item.Value = ele.Attributes["value"].Value;

                                        LstSelectTranstype.Items.Add(item);
                                    }
                                }
                                item = new ListItem();
                                if (ele.LocalName == "TransactionTypes")
                                {
                                    if (node["TransactionTypes"] != null)
                                    {
                                        item.Text = ele.InnerText;
                                        item.Value = ele.Attributes["value"].Value;

                                        LstTransType.Items.Add(item);
                                    }
                                }


                            }

                        }

                    }


                }


                objNode = XmlDocData.SelectSingleNode("//SysSetCustom");

                if (objNode != null)
                {

                    if (objNode.InnerText != null)
                    {
                        if (objNode.InnerText == "0")
                            chkSettings.Checked = false;
                        else if (objNode.InnerText == "-1")
                            chkSettings.Checked = true;

                    }


                }
                objNode = XmlDocData.SelectSingleNode("//ResClaimType");
                if (objNode != null)
                {

                    if (objNode.InnerText != null)
                    {
                        hdnUpdateClaimResType.Text = objNode.InnerText;
                    }
                }

            }

            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnAdd_Click1(object sender, EventArgs e)
        {
          
            ListItem item;

            XmlDocument XMLdoc = null;
            XmlNode objnode = null;
            XmlNode objParent=null;
            XmlElement XMLele = null;
            XmlNode objNodeChild = null;
            XmlNode objNodeNew = null;
            try
            {
                XmlDoc = GetXmlString(hdnXML.Text.Trim());

                XMLdoc = new XmlDocument();
                objnode = XmlDoc.SelectSingleNode("//ReserveCustomized");
                if (objnode != null)
                {
                    objParent = objnode.ParentNode;

                }
                else
                {

                    objParent = XmlDoc.SelectSingleNode("//ReservesNew");
                    if (objParent == null)
                    {
                        objNodeNew = XmlDoc.SelectSingleNode("//Reserves");

                        objNodeChild = XmlDoc.CreateElement("ReservesNew");
                        objNodeNew.AppendChild(objNodeChild);
                        objParent = XmlDoc.SelectSingleNode("//ReservesNew");

                    }
                }


                if (LstReservetype.Items.Count > 0)
                {
                    for (int j = 0; j <= LstReservetype.Items.Count-1 ; j++)
                    {
                        if (LstReservetype.Items[j].Value != ddlReserveType.SelectedValue)
                        {

                            if (j == (LstReservetype.Items.Count - 1))
                            {
                                item = new ListItem();
                                item.Text = ddlReserveType.SelectedItem.Text;
                                item.Value = ddlReserveType.SelectedValue;
                                LstReservetype.Items.Add(item);
                                XMLele = XmlDoc.CreateElement("ReserveCustomized");
                                XMLele.SetAttribute("value", ddlReserveType.SelectedValue);
                                XMLele.SetAttribute("Name", ddlReserveType.SelectedItem.Text);
                                objParent.AppendChild(XMLele);



                            }
                        }
                        //MITS:22787 skhare7
                        else
                        {
                            break;
                        }
                        //MITS:22787
                    }
                }

                else
                {
                    XMLele = XmlDoc.CreateElement("ReserveCustomized");
                    XMLele.SetAttribute("value", ddlReserveType.SelectedValue);
                    XMLele.SetAttribute("Name", ddlReserveType.SelectedItem.Text);
                    objParent.AppendChild(XMLele);

                    item = new ListItem();
                    item.Text = ddlReserveType.SelectedItem.Text;
                    item.Value = ddlReserveType.SelectedValue;
                    LstReservetype.Items.Add(item);


                }

                hdnXML.Text = XmlDoc.InnerXml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
       
        }
        private void PopulateTransType(XmlDocument doc)
        {
            string sNewXMl = doc.InnerXml;
            ListItem item  =null;
         
            XmlDocument XmlDoc  =null;

            try
            {
                XmlDoc = new XmlDocument();
                item = new ListItem();
                XmlDoc = GetXmlString(hdnXML.Text.Trim());
                XmlNodeList NodesNew = XmlDoc.SelectNodes("//ReserveCustomized");

                if (NodesNew != null)
                {
                    for (int i = 0; i <= NodesNew.Count - 1; i++)
                    {
                        XmlNode NodeCustom = NodesNew[i];
                        if (NodesNew[i].Attributes["value"].Value == ViewState["ReserveType"].ToString())
                        {
                            if (NodesNew[i].HasChildNodes)
                            {
                                foreach (XmlElement ele in NodeCustom)
                                {

                                    item = new ListItem();
                                    if (ele.LocalName == "TransCustomTypes")
                                    {
                                        if (NodeCustom["TransCustomTypes"] != null)
                                        {
                                            item.Text = ele.InnerText;
                                            item.Value = ele.Attributes["value"].Value;

                                            LstSelectTranstype.Items.Add(item);
                                        }

                                    }
                                    if (ele.LocalName == "TransactionTypes")
                                    {
                                        if (NodeCustom["TransactionTypes"] != null)
                                        {
                                            item.Text = ele.InnerText;
                                            item.Value = ele.Attributes["value"].Value;

                                            LstTransType.Items.Add(item);
                                        }

                                    }
                                }



                            }
                            else
                            {
                                XmlDocument xmlDocData = new XmlDocument();
                                xmlDocData.LoadXml(sNewXMl);
                                XmlNodeList nodes = xmlDocData.SelectNodes("//TransTypes");
                                foreach (XmlNode node in nodes)
                                {
                                    foreach (XmlElement XMLele in node)
                                    {
                                        item = new ListItem();
                                        if (XMLele.LocalName == "Types")
                                        {
                                            if (node["Types"] != null)
                                            {
                                                item.Text = XMLele.InnerText;
                                                item.Value = XMLele.Attributes["value"].Value;

                                                LstTransType.Items.Add(item);
                                            }
                                        }

                                    }

                                }

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        protected void LstTransType_SelectedIndexChanged(object sender, EventArgs e)
        {

            try
            {
                ViewState["TranTypeSelected"] = LstTransType.SelectedValue;
                ViewState["TranTypeTextSelected"] = LstTransType.SelectedItem.Text;
              
            }
            catch (Exception ex)
            {
                throw ex;
            }
          
        }

        protected void LstSelectTranstype_SelectedIndexChanged(object sender, EventArgs e)
           
        {
            try
            {
                if (LstSelectTranstype.SelectedValue != "")
                {
                    ViewState["TranTypeCustomSelected"] = LstSelectTranstype.SelectedValue;

                    ViewState["TranTypeTextCustomSelected"] = LstSelectTranstype.SelectedItem.Text;
                  
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnSNext_Click(object sender, EventArgs e)
        {

            XmlDocument XmlDocData = null;
             XmlNode objnode = null;
            XmlNode objParent=null;
            XmlElement XMLele = null;
            XmlElement XMLAddEle = null;
         
            ListItem item = null;
            XmlNodeList nodes = null;
            try
            {
                XmlDocData = new XmlDocument();
                XmlDocData = GetXmlString(hdnXML.Text.Trim());

                nodes = XmlDocData.SelectNodes("//ReserveCustomized");

            
                    if (nodes.Count > 0)
                    {
                        for (int i = 0; i <= nodes.Count - 1; i++)
                        {
                            if (nodes[i].Attributes["value"].Value == LstReservetype.SelectedValue)
                            {
                                if (!nodes[i].HasChildNodes)
                                {
                                    for (int count = 0; count <= LstTransType.Items.Count - 1; count++)
                                    {

                                        XMLele = XmlDocData.CreateElement("TransactionTypes");
                                        XMLele.SetAttribute("value", LstTransType.Items[count].Value.ToString());
                                        XMLele.InnerText = LstTransType.Items[count].Text.ToString();
                                        nodes[i].AppendChild(XMLele);


                                    }

                                    for (int count = 0; count <= LstSelectTranstype.Items.Count - 1; count++)
                                    {

                                        XMLele = XmlDocData.CreateElement("TransCustomTypes");
                                        XMLele.SetAttribute("value", LstSelectTranstype.Items[count].Value.ToString());
                                        XMLele.InnerText = LstSelectTranstype.Items[count].Text;
                                        nodes[i].AppendChild(XMLele);

                                    }

                                }
                            }

                        }
                        if (ViewState["TranTypeSelected"] != null && ViewState["TranTypeTextSelected"] != null)
                        {
                            if (LstSelectTranstype.Items.Count > 0)
                            {
                                for (int i = 0; i <= LstSelectTranstype.Items.Count - 1; i++)
                                {

                                    if (LstSelectTranstype.Items[i].Value == ViewState["TranTypeSelected"].ToString())
                                    {
                                        return;

                                    }
                                }

                            }
                            if (nodes.Count > 0)
                            {
                                for (int i = 0; i <= nodes.Count - 1; i++)
                                {

                                    foreach (XmlNode node in nodes)
                                    {
                                        if (nodes[i].Attributes["value"].Value == LstReservetype.SelectedValue)
                                        {
                                            XmlNode newnode;
                                            newnode = nodes[i];
                                            foreach (XmlElement ele in newnode)
                                            {
                                                if (ele.LocalName == "TransactionTypes")
                                                {
                                                    if (ViewState["TranTypeSelected"] != null)
                                                    {
                                                        if (ele.Attributes["value"].Value == ViewState["TranTypeSelected"].ToString())
                                                        {
                                                            newnode.RemoveChild(ele);
                                                            XMLele = XmlDocData.CreateElement("TransCustomTypes");
                                                            XMLele.SetAttribute("value", ViewState["TranTypeSelected"].ToString());
                                                            XMLele.InnerText = ViewState["TranTypeTextSelected"].ToString();
                                                            newnode.AppendChild(XMLele);

                                                        }


                                                    }

                                                }
                                            }

                                        }

                                    }
                                }

                            }


                            item = new ListItem();
                            item.Text = ViewState["TranTypeTextSelected"].ToString();
                            item.Value = ViewState["TranTypeSelected"].ToString();
                            LstSelectTranstype.Items.Add(item);
                            LstTransType.Items.Remove(item);

                        }
                    }

                    hdnXML.Text = XmlDocData.InnerXml;
                
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        protected void btnMNext_Click(object sender, EventArgs e)
        {

            XmlDocument XmlDocData = null;
            XmlNode objnode = null;
            XmlNode objParent = null;
            XmlElement XMLele = null;
            XmlElement XMLAddEle = null;
            ListItem itm = null;
            XmlNodeList nodes = null;
            try
            {
                XmlDocData = new XmlDocument();
                XmlDocData = GetXmlString(hdnXML.Text.Trim());
                nodes = XmlDocData.SelectNodes("//ReserveCustomized");
              

                if (nodes.Count > 0)
                {
                    for (int i = 0; i <= nodes.Count - 1; i++)
                    {

                        foreach (XmlNode node in nodes)
                        {
                            if (nodes[i].Attributes["value"].Value == LstReservetype.SelectedValue)
                            {
                                if (!node.HasChildNodes)
                                {
                                    for (int count = 0; count <= LstTransType.Items.Count - 1; count++)
                                    {

                                        XMLele = XmlDocData.CreateElement("TransactionTypes");
                                        XMLele.SetAttribute("value", LstTransType.Items[count].Value.ToString());
                                        XMLele.InnerText = LstTransType.Items[count].Text.ToString();
                                        node.AppendChild(XMLele);

                                    }

                                    for (int count = 0; count <= LstSelectTranstype.Items.Count - 1; count++)
                                    {

                                        XMLele = XmlDocData.CreateElement("TransCustomTypes");
                                        XMLele.SetAttribute("value", LstSelectTranstype.Items[count].Value.ToString());
                                        XMLele.InnerText = LstSelectTranstype.Items[count].Text;
                                        node.AppendChild(XMLele);



                                    }

                                }
                            }
                        }

                    }
                }

                if (nodes.Count > 0)
                {
                    for (int i = 0; i <= nodes.Count - 1; i++)
                    {

                        foreach (XmlNode node in nodes)
                        {
                            if (nodes[i].Attributes["value"].Value == LstReservetype.SelectedValue)
                            {
                                XmlNode newnode;
                                newnode = nodes[i];
                                for (int count = 0; count <= LstTransType.Items.Count - 1; count++)
                                {
                                    foreach (XmlElement ele in newnode)
                                    {
                                        if (ele.LocalName == "TransactionTypes")
                                        {

                                            if (LstTransType.Items[count].Value == ele.Attributes["value"].Value)
                                            {
                                                newnode.RemoveChild(ele);
                                                XMLele = XmlDocData.CreateElement("TransCustomTypes");
                                                XMLele.SetAttribute("value", LstTransType.Items[count].Value);
                                                XMLele.InnerText = LstTransType.Items[count].Text;
                                                newnode.AppendChild(XMLele);
                                            }
                                        }

                                    }
                                }

                            }

                        }
                    }

                }




                for (int i = 0; i <= LstTransType.Items.Count - 1; i++)
                {
                    itm = new ListItem();
                    itm.Text = LstTransType.Items[i].Text;
                    itm.Value = LstTransType.Items[i].Value.ToString();
                    LstSelectTranstype.Items.Add(itm);

                }
                LstTransType.Items.Clear();
                hdnXML.Text = XmlDocData.InnerXml;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        protected void btnSback_Click(object sender, EventArgs e)
        {


            ListItem itm = null;
            XmlDocument XmlDocData = null;
            XmlNode objnode = null;
            XmlNode objParent = null;
            XmlElement XMLele = null;
            XmlNodeList nodes = null;
            XmlElement XMLAddEle = null;
            try
            {
                XmlDocData = new XmlDocument();
                XmlDocData = GetXmlString(hdnXML.Text.Trim());

                nodes = XmlDocData.SelectNodes("//ReserveCustomized");



                    if (ViewState["TranTypeCustomSelected"] != null && ViewState["TranTypeTextCustomSelected"] != null)
                    {
                           if (LstTransType.Items.Count > 0)
                             {
                               for (int i = 0; i <= LstTransType.Items.Count - 1; i++)
                                  {

                                      if (LstTransType.Items[i].Value == ViewState["TranTypeCustomSelected"].ToString())
                                      {
                                          return;
                                      
                                      }
                                   }

                                }
                      
                               int iTransCount = 0;
                               iTransCount = ((nodes.Count) - (nodes.Count - 1));
                               if (nodes.Count > 0)
                               {
                                   for (int i = 0; i <= nodes.Count - 1; i++)
                                   {

                                       foreach (XmlNode node in nodes)
                                       {
                                           if (nodes[i].Attributes["value"].Value == LstReservetype.SelectedValue)
                                           {
                                               foreach (XmlElement ele in node)
                                               {
                                                   if (ele.LocalName == "TransCustomTypes")
                                                   {

                                                       if (ele.Attributes["value"].Value == ViewState["TranTypeCustomSelected"].ToString())
                                                       {
                                                           node.RemoveChild(ele);
                                                           XMLele = XmlDocData.CreateElement("TransactionTypes");
                                                           XMLele.SetAttribute("value", ViewState["TranTypeCustomSelected"].ToString());
                                                           XMLele.InnerText = ViewState["TranTypeTextCustomSelected"].ToString();
                                                           node.AppendChild(XMLele);
                                                       }
                                                   }

                                               }

                                           }
                                       }

                                   }

                               }

                            

                               itm = new ListItem();
                               itm.Text = ViewState["TranTypeTextCustomSelected"].ToString();
                               itm.Value = ViewState["TranTypeCustomSelected"].ToString();
                               LstTransType.Items.Add(itm);
                               LstSelectTranstype.Items.Remove(itm);
                           }
                    hdnXML.Text = XmlDocData.InnerXml;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }


        protected void btnMback_Click(object sender, EventArgs e)
        {
            ListItem itm = null;
            XmlDocument XmlDocData = null;
            XmlNode objnode = null;
            XmlNode objParent = null;
            XmlElement XMLele = null;
            XmlElement XMLAddEle = null;
            XmlNodeList nodes = null;
          
            try
            {
                XmlDocData = new XmlDocument();
                XmlDocData = GetXmlString(hdnXML.Text.Trim());
                nodes = XmlDocData.SelectNodes("//ReserveCustomized");


                int iTransCount = 0;
                iTransCount = ((nodes.Count) - (nodes.Count - 1));
                if (nodes.Count > 0)
                {
                    for (int i = 0; i <= nodes.Count - 1; i++)
                    {

                        foreach (XmlNode node in nodes)
                        {
                            if (nodes[i].Attributes["value"].Value == LstReservetype.SelectedValue)
                            {
                                for (int count = 0; count <= LstSelectTranstype.Items.Count - 1; count++)
                                {
                                    foreach (XmlElement ele in node)
                                    {
                                        if (ele.LocalName == "TransCustomTypes")
                                        {

                                            if (ele.Attributes["value"].Value == LstSelectTranstype.Items[count].Value)
                                            {
                                                node.RemoveChild(ele);
                                                XMLele = XmlDocData.CreateElement("TransactionTypes");
                                                XMLele.SetAttribute("value", LstSelectTranstype.Items[count].Value);
                                                XMLele.InnerText = LstSelectTranstype.Items[count].Text.ToString();
                                                node.AppendChild(XMLele);
                                            }
                                        }

                                    }
                                }

                            }
                        }

                    }

                }

                for (int i = 0; i <= LstSelectTranstype.Items.Count - 1; i++)
                {
                    itm = new ListItem();
                    itm.Text = LstSelectTranstype.Items[i].Text;
                    itm.Value = LstSelectTranstype.Items[i].Value.ToString();
                    LstTransType.Items.Add(itm);

                }
                LstSelectTranstype.Items.Clear();
                hdnXML.Text = XmlDocData.InnerXml;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        protected void LstReservetype_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ViewState["ReserveType"] = LstReservetype.SelectedValue;
                LstTransType.Items.Clear();
                LstSelectTranstype.Items.Clear();
                XmlTemplate = GetTransTypeTemplate();
                bReturnStatus = CallCWS("RSWCustomizationAdaptor.GetTranstype", XmlTemplate, out sreturnValue, false, true);
                if (bReturnStatus)
                {

                    XmlDoc.LoadXml(sreturnValue);
                    sNode = null;
                    PopulateTransType(XmlDoc);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

          
        }

        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {

            ListItem item;
            XmlDocument XMLdoc = null;
            XmlNode objnode = null;
            XmlNode objParent = null;
            XmlNodeList nodes = null;
            try
            {
                XMLdoc = new XmlDocument();
                XmlDoc = GetXmlString(hdnXML.Text.Trim());
                //MITS:22792 skhare7
                if (LstReservetype.Items.Count > 0)
                {  //MITS:22792
                    if (LstReservetype.SelectedIndex != -1)
                    {
                        LstReservetype.Items.RemoveAt(LstReservetype.SelectedIndex);
                    }
                    nodes = XmlDoc.SelectNodes("//ReserveCustomized");

                    objParent = XmlDoc.SelectSingleNode("//ReserveCustomized").ParentNode;

                    if (nodes.Count > 0)
                    {
                        for (int i = 0; i <= nodes.Count - 1; i++)
                        {
                            if (ViewState["ReserveType"]!=null) 
                            {
                                if (nodes[i].Attributes["value"].Value == ViewState["ReserveType"].ToString())
                                {
                                    nodes[i].RemoveAll();
                                    objParent.RemoveChild(nodes[i]);
                                   
                                }
                            }

                        }

                    }
                }
                if (LstTransType.Items.Count > 0)
                    LstTransType.Items.Clear();
                if (LstSelectTranstype.Items.Count > 0)
                    LstSelectTranstype.Items.Clear();
                hdnXML.Text = XmlDoc.InnerXml;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        protected void btnSave_Click(object sender, ImageClickEventArgs e)
        {

            XmlDocument XMLdoc;
     
            string sXMLData = null;
            string strClaimType = string.Empty;
            string strchecked = "";
            XElement oTemplate = null;
            StringBuilder sXml = null;
           try
           {
                if (chkSettings.Checked)
                    strchecked = "-1";
                else
                    strchecked = "0";

                if (chkSettings.Checked)
                    strClaimType = "True";
                else
                    strClaimType = string.Empty;
         //   }
            int iStartIndex = 0;
            int iLastIndex = 0;
            iStartIndex = hdnXML.Text.IndexOf("<LOB>");
            iLastIndex = hdnXML.Text.IndexOf("</LOB>");
            sXMLData = hdnXML.Text.Substring(iStartIndex, (iLastIndex + 6 - iStartIndex));
            
             sXml = new StringBuilder("<Message>");
            sXml.Append("<Authorization></Authorization>");
            sXml.Append("<Call><Function></Function></Call><Document><Save>");
            sXml.Append(sXMLData.ToString());
            sXml.Append("<SysSetting>" + strchecked + "</SysSetting>");
            sXml.Append("</Save>");
            sXml.Append("</Document></Message>");


             oTemplate = XElement.Parse(sXml.ToString());
         
         
            bReturnStatus = CallCWS("RSWCustomizationAdaptor.Save", oTemplate, out sreturnValue, false, true);
            if (bReturnStatus)
            {
               
                    XmlDoc.LoadXml(sreturnValue);
                    sNode = null;

                    bReturnStatus = false;
                    XmlTemplate = GetMessageTemplate();
                    bReturnStatus = CallCWS("RSWCustomizationAdaptor.GetLOB", XmlTemplate, out sreturnValue, false, true);
                    if (bReturnStatus)
                    {
                        XmlDoc.LoadXml(sreturnValue);
             
                     BindData(XmlDoc);
                     hdnXML.Text = XmlDoc.InnerXml;
                    }
                
            }
    
            state.Value = "";
        }


           catch (Exception ee)
           {
               ErrorHelper.logErrors(ee);
               BusinessAdaptorErrors err = new BusinessAdaptorErrors();
               err.Add(ee, BusinessAdaptorErrorType.SystemError);
               ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
           }


       }

        protected void chkSettings_CheckedChanged(object sender, EventArgs e)
        {

              XmlDocument XMLdoc = null;
              string strClaimType = string.Empty;
              string strchecked = "";
             XElement oTemplate = null;
            StringBuilder sXml = null;
             string sXMLData = null;
            try
            {
                if (AppHelper.GetQueryStringValue("sClaimId") != null)
                {

                    iClaimId = Convert.ToInt32(AppHelper.GetQueryStringValue("sClaimId"));

                }
                if (iClaimId != 0)
                {
                    XMLdoc = new XmlDocument();
                    XMLdoc = GetXmlString(sXML.Trim());

                    if (chkSettings.Checked)
                        strchecked = "-1";
                    else
                        strchecked = "0";
                    if (XMLdoc.SelectSingleNode("//SysSetCustom") != null)
                    {
                        XMLdoc.SelectSingleNode("//SysSetCustom").InnerText = strchecked;

                        hdnXML.Text = XMLdoc.InnerXml;
                    }
                }
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       
    }
}