﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="RSWCustomization.aspx.cs"
    Inherits="Riskmaster.UI.UI.Utilities.RSWSheetCustomization.RSWCustomization" ValidateRequest="false" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register TagPrefix="uc" TagName="CodeLookUp" Src="~/UI/Shared/Controls/CodeLookUp.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
    <%@ OutputCache Location="None" VaryByParam="None" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>ReserveWorkSheet Customization</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css">
    <link rel="stylesheet" href="../../../Content/zpcal/themes/system.css" type="text/css">
    <script src="../../Scripts/form.js" type="text/javascript"></script>
    <script src="../../../Scripts/WaitDialog.js" type="text/javascript"></script>
    <script type="text/javascript">


        function EnableClaimType() {
            try {

                document.frmCustomData.hdnAddReservetype.value = "";
                if (document.frmCustomData.chkSettings.checked == true) {
                    document.frmCustomData.hdnCriteria.value = "Y";

                    document.frmCustomData.submit();
                    document.frmCustomData.chkSettings.checked = true;

                }
                else if (document.frmCustomData.chkSettings.checked == false) {
                    document.frmCustomData.hdnCriteria.value = "N";

                    document.frmCustomData.submit();

                }
            } catch (ex) { }
        }

        function post_value() {
            //debugger;
            //window.opener.document.getElementById('hdnCustomTransXML').value = document.frmCustomData.hdnXML.value;
            //JIRA RMA-9408 nshah28 start
            if (document.forms["frmCustomData"] != null)
            {
                window.opener.document.getElementById('hdnCustomTransXML').value = document.forms["frmCustomData"].hdnXML.value;
            } //RMA-9408 End
            window.opener.document.getElementById('hdnXMLValue').value = "Yes";
            window.opener.document.forms[0].submit();
            self.close();
        }

        function SaveClick() {
            try {

                var elem1 = document.getElementById('styleimage');
                elem1.style.display = '';
                document.getElementById("state").value = 'btnSaveClicked';
            }
            catch (ex) { }
        }



        function EnableClaimTypeStartUp() {
            try {
                //   debugger;
                var elem1 = document.getElementById('SpecifyClmType1');
                var elem2 = document.getElementById('SpecifyClmType2');
                //mbahl3 mits 24483
                if (document.getElementById('hdnUpdateClaimResType').value == -1) {
                    elem1.style.display = '';
                    elem2.style.display = '';
                }
                    //mbahl3 mits 24483
                else if (document.getElementById('hdnUpdateClaimResType').value == 0) {
                    elem1.style.display = 'none';
                    elem2.style.display = 'none';

                }
                var elemimg = document.getElementById('styleimage');

                elemimg.style.display = 'none';
            }
            catch (ex) { }
        }
        function submitLOB() {
            //mbahl3 mits 24483
            //if (document.frmCustomData.lstClaimType != null)
            //    document.frmCustomData.lstClaimType.value = "";
            //    document.frmCustomData.hdnAddReservetype.value = 'SelectLOB';
            //    document.frmCustomData.submit();

            if (document.getElementById('frmCustomData').lstClaimType != null)
                document.getElementById('lstClaimType').value = "";
            document.getElementById('hdnAddReservetype').value = 'SelectLOB';
            document.getElementById('frmCustomData').submit();
            //mbahl3 mits 24483
        }
        function AddReserveType() {
            //debugger;
            //mbahl3 mits 24483
            //document.frmCustomData.hdnAddReservetype.value = 'AddReserve';
            //document.frmCustomData.submit();
           
            AddReserveTypeCommonFunction(); //JIRA RMA-9595 nshah28

            document.getElementById('frmCustomData').submit();
            //mbahl3 mits 24483
        }

        //JIRA RMA-9595 nshah28 start
        function AddReserveTypeOnClick() {
            AddReserveTypeCommonFunction();
        }

        //Creating common function
        function AddReserveTypeCommonFunction() {
            var elem1 = document.getElementById('styleimage');
            elem1.style.display = '';
            document.getElementById('hdnAddReservetype').value = 'AddReserve';
        }

        //RMA-9595 End

        function AddTransType() {
            //debugger;
            var elem1 = document.getElementById('styleimage');

            elem1.style.display = 'none';
            //document.frmCustomData.hdnAddReservetype.value = 'AddReserve';
            //document.frmCustomData.submit();
            //mbahl3 mits 24483
            document.getElementById('hdnAddReservetype').value = 'AddReserve';
            document.getElementById('frmCustomData').submit();
            //mbahl3 mits 24483
        }
        function submitValue() {
            //mbahl3 mits 24483
            //document.frmCustomData.hdnCriteria.value = "Y";
            //document.frmCustomData.hdnAction.value = "ReserveChange";
            //if (document.frmCustomData.ddlReserveType != null)
            //    document.frmCustomData.ddlReserveType.value = "";
            //    document.frmCustomData.hdnAddReservetype.value = "";
            //    document.frmCustomData.submit();
            document.getElementById('hdnAction').value = "ReserveChange";
            if (document.getElementById('frmCustomData').ddlReserveType != null)
                document.getElementById('ddlReserveType').value = "";
            document.getElementById('hdnAddReservetype').value = "";
            document.getElementById('frmCustomData').submit();
            //mbahl3 mits 24483
        }

    </script>
  
</head>
<body   >
    <form id="frmCustomData" runat="server">
     <uc1:ErrorControl ID="ErrorControl" runat="server" />
    <div id="divContainer" runat="server">
                    <asp:ImageButton runat="server"  Width="28" Height="28"
                        border="0" ID="btnSave" AlternateText="<%$ Resources:imgbtnSave %>" OnClientClick="SaveClick();" 
                        onMouseOver="this.src='../../../Images/save.gif';this.style.zoom='110%'" 
                        onMouseOut="this.src='../../../Images/save.gif';this.style.zoom='100%'" 
                        ImageUrl="~/Images/save.gif" class="bold" ToolTip="<%$ Resources:imgbtnSave %>"   
                        OnClick="btnSave_Click" Visible="False" />
                    <asp:ImageButton runat="server"  Width="28" Height="28"
                        border="0" ID="btnSubmit" AlternateText="<%$ Resources:imgbtnSave %>" OnClientClick="post_value();"
                        onMouseOver="this.src='../../../Images/save.gif';this.style.zoom='110%'" 
                        onMouseOut="this.src='../../../Images/save.gif';this.style.zoom='100%'" 
                        ImageUrl="~/Images/save.gif" class="bold" ToolTip="Save" Visible="False"   
                         />
                    <asp:HiddenField ID="state"  runat="server" />
                      <asp:TextBox ID="hdnUpdateClaimResType" style="display:none"  runat="server" />
                      <asp:TextBox ID="hdnCriteria" style="display:none"  runat="server" />
                            <asp:TextBox ID="hdnLOBSelected" style="display:none" rmxref="/Instance/Document/LOB/LOBParms/LOBSelected" runat="server" />
    </div>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    <div id="toolbardrift" name="toolbardrift" class="toolbardrift">
        <table >
            <tr>
                <td  colspan="2">
                   <asp:CheckBox ID="chkSettings" 
                        type="checkbox" value="True"
                        runat="server"  onclick="EnableClaimType();" 
                        oncheckedchanged="chkSettings_CheckedChanged" Text="<%$ Resources:chkUseCustomizationSettings %>" />
                </td>
               
            </tr>
            <tr>
                <td  colspan="2">
                    &nbsp;</td>
               
            </tr>
            <tr>
                <td >
                    <asp:Label ID="lblLOB" runat="server" Text="<%$Resources:lblLOB %>"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="hdnAction" runat="server" Style="display: none" />
                    <asp:TextBox ID="hdnXML" runat="server" Style="display: none" />
                    <asp:DropDownList ID="lstLOB" type="combobox" rmxref="/Instance/Document/LOB/LOBParms/LineOfBusiness/Types[@selected='1']/@value"
                        itemsetref="/Instance/Document/LOB/LOBParms/LineOfBusiness" onchange="submitLOB();EnableClaimTypeStartUp();"
                        runat="server" BackColor="White">
                    </asp:DropDownList>
                </td>
                <asp:TextBox ID="hdnClaimReserveTypes" Style="display: none" runat="server" />
                <asp:TextBox ID="hdnReserveType" Style="display: none" runat="server" />
                <asp:TextBox ID="hdnAddReservetype" Style="display: none" runat="server" />
            </tr>
             <td >
                    <table id="SpecifyClmType1">
                        <tr>
                            <td>
                                <asp:Label ID="lblClaimType" runat="server" Text="<%$Resources:lblClaimType %>"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
                <td >
                    <table id="SpecifyClmType2">
                        <tr>
                            <td>
                                <asp:DropDownList ID="lstClaimType" onChange="submitValue();" rmxref="/Instance/Document/LOB/LOBParms/Options/ReserveOptions/ClaimTypes/Types[@selected='1']/@value"
                                    itemsetref="/Instance/Document/LOB/LOBParms/Options/ReserveOptions/ClaimTypes"
                                    runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
          
               
               
                <tr>
                    <td class="ctrlgroup" >
                     <asp:Label ID="lblReservesType" runat="server" Text="<%$Resources:lblReservesType %>"></asp:Label> 
                    </td>
                     <td >
                     
                    </td>
                    <td bgcolor="#CCCCCC" class="ctrlgroup"  >
                        &nbsp;
                        <asp:Label ID="lblTransType" runat="server" Text="<%$Resources:lblTransType %>"></asp:Label> 
                    </td>
                </tr>
               
                <tr>
                    <td bgcolor="White">
                        <asp:DropDownList ID="ddlReserveType" RMXRef="/Instance/Document/LOB/LOBParms/Options/ReserveOptions/ReserveTypes/Reserves"
                            itemsetref="/Instance/Document/LOB/LOBParms/Options/ReserveOptions/ReserveTypes/Reserves"
                            runat="server">
                        </asp:DropDownList>
                        </td>
                        <td >
                                                       
                           <%-- <asp:Button runat="server" Width="32px" Height="20px" Text="<%$ Resources:btnAdd %>"
                                ID="btnAdd" AlternateText="<%$ Resources:btnAddAltText %>" class="Bold" ToolTip="<%$ Resources:ttAddReserves %>"
                                OnClick="btnAdd_Click1"
                            OnClientClick="AddReserveType();"/>--%>

                             <%--//JIRA RMA-9595 nshah28 start--%>
                             <asp:Button runat="server" Width="32px" Height="20px" Text="<%$ Resources:btnAdd %>"
                                ID="btnAdd" AlternateText="<%$ Resources:btnAddAltText %>" class="Bold" ToolTip="<%$ Resources:ttAddReserves %>"
                                OnClick="btnAdd_Click1"
                            OnClientClick="AddReserveTypeOnClick();"/>
                            <%--RMA-9595 End--%>
                        
                     
                        
                                </td>
                            
                          
                   
                    <td>
                      
                        <table id="styleimage">
                                  <tr>
                                      <td>
                                          <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/loading1.gif" /></td>
                                  </tr>
                              </table>
                                         
                      
                       
                    </td>
                            
                          
                   
                </tr>
               
                <tr>
                 <td >
                     </td>
                  
                          <td >
                             
                    </td>  
                          
                   
                </tr>
                </table>
        <table>
        
        <tr>
        <td>
                        <asp:ListBox ID="LstReservetype" AutoPostBack="true" runat="server" Height="313px"
                            Width="212px" onchange="AddReserveType();" 
                            OnSelectedIndexChanged="LstReservetype_SelectedIndexChanged"></asp:ListBox>
        </td>
        <td>
                        <asp:ImageButton runat="server"  Width="28" Height="28"
                            border="0" ID="btnDelete" AlternateText="<%$ Resources:imgbtnDeleteRecord %>" onmouseover="this.src='../../../Images/delete2.gif';this.style.zoom='110%'"
                          ImageUrl="~/Images/delete.gif"   onmouseout="this.src='../../../Images/delete.gif';this.style.zoom='100%'" OnClick="btnDelete_Click" />
        </td>
        <td>
                        <asp:ListBox ID="LstTransType" runat="server" Height="319px"
                            Width="223px" onchange="AddTransType();" OnSelectedIndexChanged="LstTransType_SelectedIndexChanged" 
                            BackColor="White"></asp:ListBox>
        </td>
        <td>
                                <table >
                            <tr>
                                <td >
                                    <asp:Button ID="btnSNext" runat="server" class="button" Text=">" Width="33px" OnClick="btnSNext_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <asp:Button ID="btnMNext" runat="server" class="button" Text=">>" Width="33px" 
                                        OnClick="btnMNext_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <asp:Button ID="btnSback" runat="server" Text="<" class="button" Width="33px" OnClick="btnSback_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="btnMback" runat="server" Text="<<" class="button" Width="33px" 
                                        OnClick="btnMback_Click" />
                                </td>
                            </tr>
                        </table>
                    
        </td>
        <td>
                        <asp:ListBox ID="LstSelectTranstype" runat="server" Height="317px"
                            Width="229px" onchange="AddTransType();" Style="margin-left: 0px" 
                            OnSelectedIndexChanged="LstSelectTranstype_SelectedIndexChanged">
                        </asp:ListBox>
                    
        </td>
        <td>
								
								<script language="javascript" type="text/javascript">
								    try {


								        EnableClaimTypeStartUp();


								    } catch (ex) { }
									</script>	
							</td>
        </tr>
                        
        
        </table>
    </div>
    </form>
</body>
</html>
