﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoadOrgSet.aspx.cs" Inherits="Riskmaster.UI.Utilities.LoadOrgSet.LoadOrgSet" ValidateRequest="false" %>
<%@ Register  TagPrefix="dg" TagName="UserControlDataGrid" Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" %>
<%@ Register src="~/UI/Shared/Controls/ErrorControl.ascx" tagname="ErrorControl" tagprefix="uc3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Business Entity Security</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css">
    <link rel="stylesheet" href="../../../Content/zpcal/themes/system.css" type="text/css">

    <script language="JavaScript" src="../../../Scripts/form.js">
    </script>

</head>
<body>
    <form id="frmData" runat="server">
    <input type="hidden" id="wsrp_rewrite_action_1" name="" value="">
    <input type="hidden" id="hdnRowId" name="" value="" runat ="server">
    <input type="hidden" id="AdminUserID" name="" value="" runat ="server">
    <input type="hidden" id="AdminPwd" name="" value="" runat ="server">
    <input type="hidden" id="OracleRole" name="" value="" runat ="server">
    <input type="hidden" id="ConfRec" name="" value="" runat ="server"> <!-- pmittal5 03/08/10 - Confidential Record-->
    <table>
         <tr>
              <td colspan="2">
                  <uc3:ErrorControl ID="ErrorControl1" runat="server" />
             </td>
         </tr>
     </table>
    <div class="toolbardrift"
        id="toolbardrift" name="toolbardrift">
        <table class="toolbar" cellspacing="0" cellpadding="0" border="0">
            <tbody>
          
                <tr>
                    <td align="center" valign="middle" height="32">
                        <input type="image" name="$actionImg^setvalue%26node-ids%268%26content%26Print" src="../../../Images/recordsummary.gif"
                            alt="" id="RecordSummary" onserverclick="btnPrint_Click" onmouseover="&#xA;javascript:document.all[&#34;RecordSummary&#34;].src='../../../Images/recordsummary2.gif';&#xA;"
                            onmouseout="&#xA;javascript:document.all[&#34;RecordSummary&#34;].src='../../../Images/recordsummary.gif';&#xA;"
                            title="Record Summary" runat ="server" />
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <br>
    <br>
   <%-- <div class="msgheader" id="formtitle">
    </div>
    <div class="errtextheader">
    </div>--%>
    <table border="0">
        <tbody>
            <tr>
                <td>
                    <table border="0">
                        <tbody>
                            <tr>
                                <td class="ctrlgroup" colspan="2">
                                    Organizational Entity Security Setup
                                </td>
                            </tr>
                            <tr>
                                <td width="500px">
                                    <b></b>
                                    <table width="100%">
                                        <tr>
                                            <td width="100%">
                                                <div style="&#xa; width: 500px; height: 400px; &#xa; &#xa;">
                                                  <%-- 
                                                    <asp:GridView ID="GridView1" runat="server" HeaderStyle-CssClass="GridViewHeader" 
                                                        AutoGenerateColumns="false" AllowSorting="true" ShowHeader="false" Width ="100%"
                                                         AlternatingRowStyle-CssClass ="datatd1" RowStyle-CssClass ="datatd" BorderWidth ="1" BorderColor =Black>
                                                       <Columns>
                                                            <asp:TemplateField HeaderStyle-Width="5%" ItemStyle-Width="6%">
                                                                <ItemTemplate>
                                                               <input type="radio" id='RowSelector' name = "NoteSelector"  value='<%# DataBinder.Eval(Container.DataItem, "RowId")%>'/>
                                                               
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField  HeaderStyle-Width="95%" ItemStyle-Width="94%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblGroupName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "GroupName")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>--%>
                                                    <span>
                                                        <dg:usercontroldatagrid runat="server" id="OrgSetGrid" gridname="OrgSetGrid"
                                                             target="/Document/PassToWebService/OrgEntitySecurityList" ref="/Instance/Document/form//control[@name='HolidaysGrid']"
                                                            onclick="KeepRowForEdit('OrgSetGrid');" unique_id="RowId" showradiobutton="true"
                                                            width="100%" height="150px" hidenodes="|RowId|" showheader="false" linkcolumn=""
                                                            popupwidth="610px" popupheight="450px" type="GridAndButtons" RowDataParam ="Option" /><%--gridtitle="OrgSet"--%>
                                                    </span>
                                                    <asp:TextBox Style="display: none" runat="server" ID="OrgSetSelectedId" RMXType="id" />
                                                    <asp:TextBox Style="display: none" runat="server" ID="OrgSetGrid_RowDeletedFlag"
                                                        RMXType="id" Text="false" />
                                                    <asp:TextBox Style="display: none" runat="server" ID="OrgSetGrid_Action" RMXType="id" />
                                                    <asp:TextBox Style="display: none" runat="server" ID="OrgSetGrid_RowAddedFlag"
                                                        RMXType="id" Text="false" />
                                                </div>
                                            </td>
                                           <%-- <td width="20%" valign="top">
                                                <table width="100%" valign="top" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <input type="image" name="$actionImg^setvalue%26node-ids%268%26content%26New_OrgEntitySecurityGrid"
                                                                src="../../../Images/new.gif" alt="" id="New_OrgEntitySecurityGrid" onmouseover="&#xA;javascript:document.all[&#34;New_OrgEntitySecurityGrid&#34;].src='../../../Images/new2.gif';&#xA;"
                                                                onmouseout="&#xA;javascript:document.all[&#34;New_OrgEntitySecurityGrid&#34;].src='../../../Images/new.gif';&#xA;"
                                                                title="New" onclick="&#xA;return openGridAddEditWindow(&#34;OrgEntitySecurityGrid&#34;,&#34;add&#34;,610,450);&#xA;; "><br>
                                                            <input type="image" name="$actionImg^setvalue%26node-ids%268%26content%26Edit_OrgEntitySecurityGrid"
                                                                src="../../../Images/edittoolbar.gif" alt="" id="Edit_OrgEntitySecurityGrid"
                                                                onmouseover="&#xA;javascript:document.all[&#34;Edit_OrgEntitySecurityGrid&#34;].src='../../../Images/edittoolbar2.gif';&#xA;"
                                                                onmouseout="&#xA;javascript:document.all[&#34;Edit_OrgEntitySecurityGrid&#34;].src='../../../Images/edittoolbar.gif';&#xA;"
                                                                title="Edit" onclick="&#xA;return openGridAddEditWindow(&#34;OrgEntitySecurityGrid&#34;,&#34;edit&#34;,610,450);&#xA;; "><br>
                                                            <input type="image" name="$actionImg^setvalue%26node-ids%268%26content%26Delete_OrgEntitySecurityGrid"
                                                                src="../../../Images/delete.gif" alt="" id="Delete_OrgEntitySecurityGrid" onmouseover="&#xA;javascript:document.all[&#34;Delete_OrgEntitySecurityGrid&#34;].src='../../../Images/delete2.gif';&#xA;"
                                                                onmouseout="&#xA;javascript:document.all[&#34;Delete_OrgEntitySecurityGrid&#34;].src='../../../Images/delete.gif';&#xA;"
                                                                title="Delete" onclick="&#xA;return validateGridForDeletion(&#34;OrgEntitySecurityGrid&#34;);&#xA;; "><br>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>--%>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                </td>
                            </tr>
                        </tbody>
                    </table>  
    <ol class="gen">
        Please ensure that BES Scheduler is configured through Task Manager for  asynchronous processing of the BES groups.
        <br>
        Go to Utilities->Tools and designers->Task Manager->View Scheduled Tasks.
        <br/>
        Select BES scheduler.    
             
    </ol>
                    <input type="text" name="" value="" id="SysViewType" style="display: none"><input
                        type="text" name="" value="" id="SysCmd" style="display: none"><input type="text"
                            name="" value="" id="SysCmdConfirmSave" style="display: none"><input type="text"
                                name="" value="" id="SysCmdQueue" style="display: none"><input type="text" name=""
                                    value="" id="SysCmdText" style="display: none" rmxforms:value="Navigate"><input type="text"
                                        name="" value="" id="SysClassName" style="display: none" rmxforms:value=""><input
                                            type="text" name="" value="" id="SysSerializationConfig" style="display: none"><input
                                                type="text" name="" value="" id="SysFormIdName" style="display: none" rmxforms:value="RowId"><input
                                                    type="text" name="" value="" id="SysFormPIdName" style="display: none" rmxforms:value="RowId"><input
                                                        type="text" name="" value="" id="SysFormPForm" style="display: none" rmxforms:value="OrgEntitySecuritySetup"><input
                                                            type="text" name="" value="" id="SysInvisible" style="display: none" rmxforms:value=""><input
                                                                type="text" name="" value="" id="SysFormName" style="display: none" rmxforms:value="OrgEntitySecuritySetup"><input
                                                                    type="text" name="" value="" id="SysRequired" style="display: none">
                </td>
                <td valign="top">
                </td>
            </tr>
        </tbody>
    </table>
    <input type="hidden" name="$node^5" value="rmx-widget-handle-6" id="SysWindowId"><input
        type="hidden" name="$instance" value="H4sIAAAAAAAAAM1XW4/iNhR+nv6KyA99A8PMzradBtRtp1qhne2M5qKuVFWVSQ5gTWKntkOg2h/f&#xA;4zgXIDFlpSItD+Cc8/n4Ozd8Es6ENkxEEGzSROibDZ+QlTHZDaVFUQyLq6FUS3o5Gl3RTzMRJXkM&#xA;pIJmChZ844Ff0s1CqlTXYFh7gGOKgAGsQZgGvMn3wCiM5yV+k2cxMw0DuVnsAaWagxTDSKYUVTRT&#xA;MgKtpWoMZ0fhPIOEi8a6c+C//Jt+c3FxEVSfMAXDSkkjalQ6UgBiwOMpVVy/pkwbUPTx44vhCTcc&#xA;NL2TLL5XyycwIW3R/dZyDcpqIx2FtH7wHIwx4NLaontkG0DBRSwLayCk7boDewZtPsDWHVk/NFra&#xA;uN5y5B5GLDJIiHq0d1y8ommfGvNpBpFM8tRroYRIFWN8jyGwlNjAbDPwolYYClAKK2i6YIkGzMuO&#xA;qH+TU+6HGlPE9wVPLikzsZAH0A2/4a7RghV22IQMh/TTxztaN+rgGdIswS4YvMxsa5CD/bcyylPs&#xA;pjY1v8P8CdSaR3CLHu8lNsR6+1UYbrZPEOUKf++4NlUDREVb/Zj0slEe29JFs9qZpb/INJWibpwY&#xA;Elgym+Jme9VqtnlsqyGOtqh6n5asbVAdrQAPsh5aebnVLiiINSQSk0a6JZog+RWwbvGW2kdZzLAH&#xA;enXvlcyz31gKXX1IvWbD+8w6cOS46Qg/45C6h+NHT9/FKRchbQVdKp4DTyRyeSIRgHOyuD6RxcKY&#xA;M9IYn5oVBfqc0XhzKg2lzhmNU0vDjM8ZjLenssDr56xEvvsCIpfnJPL910Lkh6+FyNWJRIrii1mg&#xA;sO8+3JlyPHdp+MC0fpat1g0He9NB/11bAbtzRHPpVIAOog0LPY7Zudt8h9GD004JSg+yvMBecCqY&#xA;3VKv/qGIe5X3ikUJPMrkcB4LaSe6jcoO4YFA3yakwxIH6TwjAYoSVONCZvPcGClwrBmTQOcZjhuk&#xA;j4qRMpkz5Q2XnXtAOWOBnSAnREGEE6fO05SpLTmeEA1mzZIcgnK+ayY7HBKpG43J9EFxYV8DKqQ/&#xA;cbtUepO378uebmkL47TooZoJ/k85r7EkcNCgxgYO7GUZc40j69ZN7cdjE0lhlEx8rN4rHpMq5Hb9&#xA;TsQ/u5yilKklmJ5NtlxJsER4UwsFrlcTcj0aZRsSrIAvV7jzTfXIYxAyBqyTz2WDfSZBLvjfOfB4&#xA;QkoJOcxdPXZTW5E4Lzs3/vip9ONblkn9Y68zTvUnCTKZ5VlF6+14VAkaZtcosS8ftklBTcizysFJ&#xA;FIu5dAVQi/E19tUF2xZ4WLPxV1FfgvYRZa30abBOQdmacBnTW/2XDQGP7XOdKowohpQEZTHXIez9&#xA;F/CYy/5ve/bLY83TCp1/JWti6pY7L10ICpuymP4Lh+quvGARAAA=">
    </form>
</body>
</html>
