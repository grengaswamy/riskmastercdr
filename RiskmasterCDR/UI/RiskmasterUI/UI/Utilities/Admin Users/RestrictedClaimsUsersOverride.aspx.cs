﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.AppHelpers;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Utilities.AdminUsers
{
    /// <summary>
    /// Neha goel MITS# 33409 WWIG gap 10 restrcited claims system wide users screen
    /// </summary>
    public partial class RestrictedClaimsUsersOverride : NonFDMBasePageCWS
    {
        /// <summary>
        /// page load method
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            string sreturnValue = "";
            Control ctrlTemp = null;           
            try
            {
                string sReturn = "";
                if (!Page.IsPostBack)
                {                                                   
                    bReturnStatus = CallCWSFunctionBind("RestrictedClaimsUserOverrideAdaptor.Get", out sreturnValue);
                    if (bReturnStatus)
                    {
                        BindPageControls(sreturnValue);

                    }
                }
                
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);                
            }


        }

        /// <summary>
        /// Function called to save the users
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, ImageClickEventArgs e)
        {
            bool bReturnStatus = false;
            string sreturnValue = "";
            XmlDocument objXml = null;

            try
            {
                
                bReturnStatus = CallCWSFunctionBind("RestrictedClaimsUserOverrideAdaptor.Save", out sreturnValue);
                objXml = new XmlDocument();
                objXml.LoadXml(sreturnValue);
                if (bReturnStatus && !(objXml.SelectSingleNode("ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Error"))
                {
                    BindPageControls(sreturnValue);
                }

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }        

        /// <summary>
        /// Function to bind the controls to the page
        /// </summary>
        /// <param name="sreturnValue"></param>
        private void BindPageControls(string sreturnValue)
        {
            DataSet dsResClaimUserRecordsSet = null;
            XmlDocument xmlResClaimUserDoc = new XmlDocument();
            xmlResClaimUserDoc.LoadXml(sreturnValue);
            dsResClaimUserRecordsSet = ConvertXmlDocToDataSet(xmlResClaimUserDoc);
            if (dsResClaimUserRecordsSet.Tables["UserListoption"] != null)
            {

                lstUsers.DataSource = dsResClaimUserRecordsSet.Tables["UserListoption"];
                lstUsers.DataTextField = "UserListoption_Text";
                lstUsers.DataValueField = "value";
                lstUsers.DataBind();
            }
            else
            {
                lstUsers.Items.Clear();
            }            
            dsResClaimUserRecordsSet.Dispose();

        }
    }
}
