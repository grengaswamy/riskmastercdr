﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
//using Riskmaster.Models;
using System.ServiceModel;
using Riskmaster.UI.Shared;
using System.IO;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Utilities.BES
{
    public partial class BES : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                XmlDocument oFDMPageDom = new XmlDocument();
                string sReturn = "";
                //Preparing XML to send to service
                XElement oMessageElement = IsOrgSetFirstTimeTemplate();
                //Modify XML 

                CallService(oMessageElement, ref oFDMPageDom, ref sReturn);
                if (oFDMPageDom.SelectSingleNode("//Flag") != null)
                {
                    IsFirstTime.Value = oFDMPageDom.SelectSingleNode("//Flag").InnerText;
                    if (oFDMPageDom.SelectSingleNode("//Document/Flag").InnerText == "True")
                    {
                        btnDisable.Enabled = false;
                    }
                    else
                    {
                        chkConfRec.Enabled = false;  //pmittal5 03/08/10 - Disable Confidential Record checkbox if BES is already enabled
                    }
                }
                //pmittal5 03/09/10 - Confidential Record
                if (oFDMPageDom.SelectSingleNode("//ConfRec") != null)
                {
                    if (oFDMPageDom.SelectSingleNode("//Document/ConfRec").InnerText == "-1")
                    {
                        chkConfRec.Checked = true;
                    }
                    else
                    {
                        chkConfRec.Checked = false;  
                    }
                }
                //End- pmittal5
            }

        }
        /// <summary>
        /// This function would call the webservice to get output and return ouput string
        /// </summary>
        /// <param name="oMessageElement"></param>
        /// <param name="oFDMPageDom"></param>
        protected void CallService(XElement oMessageElement, ref XmlDocument oXMLOut, ref string p_sReturn)
        {
            p_sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
            oXMLOut.LoadXml(p_sReturn);
            XmlNode oInstanceNode = oXMLOut.SelectSingleNode("/ResultMessage/Document");
            XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
            oXMLOut.LoadXml(oInstanceNode.OuterXml);

        }

        /// <summary>
        /// CWS request message template for IsOrgSetFirstTime
        /// </summary>
        /// <returns></returns>
        private XElement IsOrgSetFirstTimeTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                 <Message>
                      <Authorization>a7abceb9-2cec-4b1e-9a2b-5938d7c09be1</Authorization> 
                      <Call>
                            <Function>ORGSECAdaptor.IsOrgSetFirstTime</Function> 
                      </Call>
                      <Document>
                            <Utility /> 
                            <Flag></Flag> 
                            <ConfRec></ConfRec> 
                      </Document>
                  </Message>



            ");
            return oTemplate;
        }

        protected void btnBesSetup_click(object sender, EventArgs e)
        {
            //pmittal5 03/09/10 - Confidential Record
            int iConfRec = 0;
            iConfRec = Common.Conversion.ConvertBoolToInt(chkConfRec.Checked);
            if (iConfRec == 1)
                iConfRec = -1;
            //Response.Redirect("~/UI/Utilities/LoadOrgSet/LoadOrgSet.aspx");  //pmittal5 03/09/10 - Confidential Record
            Response.Redirect("~/UI/Utilities/LoadOrgSet/LoadOrgSet.aspx?ConfRec=" + iConfRec);
        }

        protected void btnDisable_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/UI/Utilities/DisableBES/DisableBES.aspx");
        }

    }
}
