﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Data;

namespace Riskmaster.UI.Billing
{
    public partial class Batches : NonFDMBasePageCWS 
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {            
            XElement XmlTemplate = null;
            bool bReturnStatus = false;
            string sCWSresponse = "";
            XmlDocument objXmlDocument = null;
            DataTable objDataTable = null;
            XmlNodeList objNodeList = null;
            DataRow objRow = null;
            //XmlNode objNode = null;

            //Start - vkumar258-ML Changes
            string sCulture = AppHelper.GetCulture().ToString();
            if (sCulture != "en-US")
            {
                //Register Date Script
                AppHelper.CalenderClientScriptNonFDM(sCulture, this, 2);
                //Register Time Script
                AppHelper.TimeClientScript(sCulture, this);
            }
            //End - vkumar258-ML Changes

            try
            {
                XmlTemplate = GetMessageTemplate();

                bReturnStatus = CallCWS("BillingAdaptor.PerformAction", XmlTemplate, out sCWSresponse, false, true);

                if (action.Text != "Select")
                    selectedrow.Text = "";
                     

                objXmlDocument = new XmlDocument();
                objXmlDocument.LoadXml(sCWSresponse);

                objNodeList = objXmlDocument.SelectNodes("//Batch/listrow");

                objDataTable = new DataTable("Batches");
                objDataTable.Columns.Add("BatchRowId");
                objDataTable.Columns.Add("Status");
                objDataTable.Columns.Add("Type");
                objDataTable.Columns.Add("DateOpened");
                objDataTable.Columns.Add("BatchCount");

                objDataTable.Columns.Add("Amount");
                objDataTable.Columns.Add("DateClosed");
                objDataTable.Columns.Add("AddedByUser");


                foreach (XmlNode objNode in objNodeList)
                {
                    objRow = objDataTable.NewRow();
                    objRow["BatchRowId"] = objNode.SelectSingleNode("BatchRowId").InnerText;
                    objRow["Status"] = objNode.SelectSingleNode("Status").InnerText;
                    objRow["Type"] = objNode.SelectSingleNode("Type").InnerText;
                    objRow["DateOpened"] = objNode.SelectSingleNode("DateOpened").InnerText;
                    objRow["BatchCount"] = objNode.SelectSingleNode("BatchCount").InnerText;

                    objRow["Amount"] = objNode.SelectSingleNode("Amount").InnerText;
                    objRow["DateClosed"] = objNode.SelectSingleNode("DateClosed").InnerText;
                    objRow["AddedByUser"] = objNode.SelectSingleNode("AddedByUser").InnerText;                 

                    objDataTable.Rows.Add(objRow);
                }

                //Adding a blank row always so that column headers are visible
                //even if no row is there
                objRow = objDataTable.NewRow();
                objRow["BatchRowId"] = "";
                objRow["Status"] = "";
                objRow["Type"] = "";
                objRow["DateOpened"] = "";
                objRow["BatchCount"] = "";

                objRow["Amount"] = "";
                objRow["DateClosed"] = "";
                objRow["AddedByUser"] = "";   

                objDataTable.Rows.Add(objRow);

                Batches_Grid.DataSource = objDataTable;
                Batches_Grid.DataBind();


                objNodeList = objXmlDocument.SelectNodes("//BillingItems/listrow");

                objDataTable = new DataTable("BillingItems");
                objDataTable.Columns.Add("DateEntered");
                objDataTable.Columns.Add("Type");
                objDataTable.Columns.Add("Status");
                objDataTable.Columns.Add("Amount");
                objDataTable.Columns.Add("DateReconcile");

                objDataTable.Columns.Add("FinalBatchNum");
                objDataTable.Columns.Add("FinalBatchType");
                objDataTable.Columns.Add("EntryBatchNum");
                objDataTable.Columns.Add("EntryBatchType");
                objDataTable.Columns.Add("InvoiceId");


                foreach (XmlNode objNode in objNodeList)
                {
                    objRow = objDataTable.NewRow();
                    objRow["DateEntered"] = objNode.SelectSingleNode("DateEntered").InnerText;
                    objRow["Type"] = objNode.SelectSingleNode("Type").InnerText;
                    objRow["Status"] = objNode.SelectSingleNode("Status").InnerText;
                    objRow["Amount"] = objNode.SelectSingleNode("Amount").InnerText;
                    objRow["DateReconcile"] = objNode.SelectSingleNode("DateReconcile").InnerText;

                    objRow["FinalBatchNum"] = objNode.SelectSingleNode("FinalBatchNum").InnerText;
                    objRow["FinalBatchType"] = objNode.SelectSingleNode("FinalBatchType").InnerText;
                    objRow["EntryBatchNum"] = objNode.SelectSingleNode("EntryBatchNum").InnerText;
                    objRow["EntryBatchType"] = objNode.SelectSingleNode("EntryBatchType").InnerText;
                    objRow["InvoiceId"] = objNode.SelectSingleNode("InvoiceId").InnerText;


                    objDataTable.Rows.Add(objRow);
                }

                //Adding a blank row always so that column headers are visible
                //even if no row is there
                objRow = objDataTable.NewRow();
                objRow["DateEntered"] = "";
                objRow["Type"] = "";
                objRow["Status"] = "";
                objRow["Amount"] = "";
                objRow["DateReconcile"] = "";

                objRow["FinalBatchNum"] = "";
                objRow["FinalBatchType"] = "";
                objRow["EntryBatchNum"] = "";
                objRow["EntryBatchType"] = "";
                objRow["InvoiceId"] = "";


                objDataTable.Rows.Add(objRow);

                BillingItems_Grid.DataSource = objDataTable;
                BillingItems_Grid.DataBind();

                //MGaba2:MITS 22611: No Message was coming when view permission is revoked from SMS
               // ErrorControl1.errorDom = sCWSresponse;

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }            
        }



        /// <summary>
        /// 
        /// </summary>        
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>");

            sXml = sXml.Append("<Batches>");
            sXml = sXml.Append("<BatchStatusList/>");
            sXml = sXml.Append("<BatchTypeList/>");
            sXml = sXml.Append("<UsersList/>");
            sXml = sXml.Append("<Batch/>");
            sXml = sXml.Append("<BillingItems>");
            sXml = sXml.Append("<PrintDisbursement/>");
            sXml = sXml.Append("<BatchTotal/>");
            sXml = sXml.Append("</BillingItems>");

            sXml = sXml.Append("<Filter>");
            sXml = sXml.Append("<FromDate>" + DateFrom.Text + "</FromDate>");
            sXml = sXml.Append("<ToDate>" + DateTo.Text + "</ToDate>");

            sXml = sXml.Append("<SelectedBatchType>");
            sXml = sXml.Append(BatchType.SelectedValue); 
            sXml = sXml.Append("</SelectedBatchType>");

            sXml = sXml.Append("<SelectedBatchStatus>");
            sXml = sXml.Append(BatchStatus.SelectedValue);
            sXml = sXml.Append("</SelectedBatchStatus>");

            sXml = sXml.Append("<SelectedUser>");
            sXml = sXml.Append(UserId.SelectedValue);
            sXml = sXml.Append("</SelectedUser>");

            sXml = sXml.Append("<Action>");
            sXml = sXml.Append(action.Text);
            sXml = sXml.Append("</Action>");
            sXml = sXml.Append("<SelectedBatch>");
            sXml = sXml.Append("<BatchId>");
            if(selectedrow.Text !=  "-1")
                sXml = sXml.Append(selectedrow.Text);
            sXml = sXml.Append("</BatchId>");
            sXml = sXml.Append("</SelectedBatch>");
            sXml = sXml.Append("<ReconcileType>");
            sXml = sXml.Append(ReconcileType.Text);
            sXml = sXml.Append("</ReconcileType>");
            sXml = sXml.Append("<CancelPayPlan>");
            sXml = sXml.Append(bCancelPayPlan.Text);
            sXml = sXml.Append("</CancelPayPlan>");
            sXml = sXml.Append("</Filter>");
            sXml = sXml.Append("</Batches>");            
            
            sXml = sXml.Append("</Document></Message>");

                

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }


        //Setting visible false for the last row of Batches
        protected void Batches_DataBound(object sender, EventArgs e)
        {
            Batches_Grid.Rows[Batches_Grid.Rows.Count - 1].Visible = false;
        }    



        //Setting visible false for the last row of Batches
        protected void BillingItems_DataBound(object sender, EventArgs e)
        {
            BillingItems_Grid.Rows[BillingItems_Grid.Rows.Count - 1].Visible = false;
        }

        protected void Batches_Grid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string javascriptSelectRow = string.Empty;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                javascriptSelectRow = "SelectBatches('" + DataBinder.Eval(e.Row.DataItem, "BatchRowId").ToString() + "'," + "'" + DataBinder.Eval(e.Row.DataItem, "Status").ToString() + "',"  + "'" + DataBinder.Eval(e.Row.DataItem, "Type").ToString() + "'," + "'" + DataBinder.Eval(e.Row.DataItem, "BatchCount").ToString() + "'" + ")";

                //showing details of a particular row , after on click it at ,client side
                e.Row.Attributes.Add("onclick", javascriptSelectRow);
            }
        }    
        

    }

}
