﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintInvoicesNoticesFrame.aspx.cs"
    Inherits="Riskmaster.UI.Billing.PrintInvoicesNoticesFrame" %>

<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head" runat="server">
    <meta http-equiv="Pragma" content="no-cache" />
    <title>Billing Scheduler</title>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/Billing.js">        { var i; }</script>
</head>
<body onload="OpenPrintInvoicesNotices();">
    <form id="frmData" runat="server">
    <div>
        <table border="0" align="center">
            <tr>
                <td colspan="2">
                    <uc3:ErrorControl ID="ErrorControl1" runat="server" />
                </td>
            </tr>
        </table>
        <asp:TextBox ID="filename" Style="display: none" RMXRef="Instance/Document/PrintInvoicesNotices/FileName"
            runat="server" />
    </div>
    </form>
</body>
</html>
