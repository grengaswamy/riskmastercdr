﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Batches.aspx.cs" Inherits="Riskmaster.UI.Billing.Batches" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
        <uc4:CommonTasks ID="CommonTasks1" runat="server" />
        <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
        <%--<script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js"></script>
         <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js"></script>
        <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>--%>
         <script type="text/javascript" src="../../Scripts/Batches.js"></script> <!-- JIRA RMA-9287 : rkaur27-->
     <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <script type="text/javascript" src="../../Scripts/form.js">        { var i; } </script>
        <script language="javascript" type="text/javascript" >
            
            //Setting hidden variable as selected batch id on row selection
            function SelectBatches(strBatchID,strStatus,strType,strBatchCount) {
                
                var gridElementsRadio = document.getElementsByName('selectrdo');
                var found = false;

                if (gridElementsRadio != null) 
                {
                    for (var i = 0; i < gridElementsRadio.length; i++) 
                    {
                        if (gridElementsRadio[i].checked == true)
                         {
                             if (gridElementsRadio[i].value != strBatchID)
                             {
                                 document.forms[0].selectedrow.value = gridElementsRadio[i].value;
                                 if(document.forms[0].Status.value == "")
                                     document.forms[0].Status.value = strStatus;
                                 if (document.forms[0].Type.value == "")
                                     document.forms[0].Type.value = strType;
                                 if (document.forms[0].BatchCount.value == "")
                                    document.forms[0].BatchCount.value = strBatchCount;
                             }
                             else 
                             {
                                 document.forms[0].selectedrow.value = strBatchID;
                                 document.forms[0].Status.value = strStatus;
                                 document.forms[0].Type.value = strType;
                                 document.forms[0].BatchCount.value = strBatchCount;
                                 window.document.forms[0].action.value = "Select";
                                 document.forms[0].submit();
                             }
                             found = true;
                             break;
                        }
                    }
                }

                if (found == false)
                    document.forms[0].selectedrow.value = -1;                                       
            }
    </script>
</head>
<body onload="SelectAfterPostBack();parent.MDIScreenLoaded();">      
    <form id="frmData" runat="server">
    <div class="msgheader" id="formtitle">Batches</div>
     <table border="0">
        <tr>
      <td colspan="2">
        <uc3:ErrorControl ID="ErrorControl1" runat="server" />
       </td>
    </tr>  
    </table>
    
    
     <table border="0" cellspacing="0" cellpadding="0" id="FORMTABViewFilters">
           <tr id="">
            <td>Batch Type:&nbsp;&nbsp;</td>
            <td>
            <asp:DropDownList runat="server" RMXRef="/Instance/Document/Batches/Filter/SelectedBatchType"
                   id="BatchType" tabindex="1" itemsetref="/Instance/Document/Batches/BatchTypeList" />
            </td>
            <td>&nbsp;&nbsp;</td>
            <td>Batch Status:&nbsp;&nbsp;</td>
            <td width="100px">
            <asp:DropDownList runat="server" RMXRef="/Instance/Document/Batches/Filter/SelectedBatchStatus"
                   id="BatchStatus" tabindex="2" itemsetref="/Instance/Document/Batches/BatchStatusList" />                      
            </td>
           </tr>
           
           <tr id="">
            <td>Date From:&nbsp;&nbsp;</td>
            <td>
            <%--MGaba2: Mits 20034: Applied Date Formatting--%>
                <asp:TextBox runat="server" FormatAs="date" tabIndex="3" id="DateFrom" RMXType="date" onchange="dateLostFocus(this.id);" onblur="dateLostFocus(this.id);" />
                <%--<input type="button" class="DateLookupControl" name="txtDateFrombtn" />
                <script type="text/javascript">
                    Zapatec.Calendar.setup(
					    {
					        inputField: "DateFrom",
					        ifFormat: "%m/%d/%Y",
					        button: "txtDateFrombtn"
					    }
					    
					    );
			    </script>--%>
                <script type="text/javascript">
                    $(function () {
                        $("#DateFrom").datepicker({
                            showOn: "button",
                            buttonImage: "../../Images/calendar.gif",
                          //buttonImageOnly: true,
                            showOtherMonths: true,
                            selectOtherMonths: true,
                            changeYear: true
                        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "4");
                    });
                    </script>
			</td> 
            
            <td>&nbsp;&nbsp;</td>
            <td>Date To:&nbsp;&nbsp;</td>
             <td>
             <%--MGaba2: Mits 20034: Applied Date Formatting--%>
                <asp:TextBox runat="server" FormatAs="date" id="DateTo" tabIndex="5" RMXType="date" onchange="dateLostFocus(this.id);" onblur="dateLostFocus(this.id);" />
                <%--<input type="button" class="DateLookupControl" name="txtDateTobtn" />
                <script type="text/javascript">
                    Zapatec.Calendar.setup(
					    {
					        inputField: "DateTo",
					        ifFormat: "%m/%d/%Y",
					        button: "txtDateTobtn"
					    }
					    );
			    </script>--%>
                 <script type="text/javascript">
                     $(function () {
                         $("#DateTo").datepicker({
                             showOn: "button",
                             buttonImage: "../../Images/calendar.gif",
                            // buttonImageOnly: true,
                             showOtherMonths: true,
                             selectOtherMonths: true,
                             changeYear: true
                         }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "6");
                     });
                    </script>
			</td>           
            
            
           </tr>
           <tr id="">
            <td>User Id:&nbsp;&nbsp;</td>
            <td colspan="2">
            <asp:DropDownList runat="server" RMXRef="/Instance/Document/Batches/Filter/SelectedUser"
                   id="UserId" tabindex="7" itemsetref="/Instance/Document/Batches/UsersList" />                                  
            </td>
            <td>&nbsp;&nbsp;</td>
            <td colspan="2">
            <asp:button runat="server" type="button" class="button" id="Refresh" onclientclick="OnRefresh()" text="Refresh" tabindex="8"/></td>
           </tr>
       </table>
       
       <asp:textbox runat="server" id="selectedrow" style="display:none"/>   
       <asp:textbox runat="server" id="Status" style="display:none"/>   
       <asp:textbox runat="server" id="Type" style="display:none"/>   
       <asp:textbox runat="server" id="BatchCount" style="display:none"/>   
       
       <%--<asp:textbox runat="server" id="action" style="display:none"/>   --%>
       <asp:textbox runat="server" id="action" style="display:none" RMXRef="/Instance/Document/Batches/Filter/Action"/>   
       <asp:textbox runat="server" id="bPrintDisb" style="display:none" RMXRef="/Instance/Document/Batches/BillingItems/PrintDisbursement"/>   
       <asp:textbox runat="server" id="iBatchTotal" style="display:none" RMXRef="/Instance/Document/Batches/BillingItems/BatchTotal"/>   
       <asp:textbox runat="server" id="ReconcileType" style="display:none" RMXRef ="/Instance/Document/Batches/Filter/ReconcileType"/>   
       <asp:textbox runat="server" id="bCancelPayPlan" style="display:none" RMXRef ="/Instance/Document/Batches/Filter/CancelPayPlan"/>             
       
       <br />
       <br />
       <b>Batches</b>
       
       <asp:GridView DataKeyNames="BatchRowId" ID="Batches_Grid" runat="server" BorderWidth="1" 
         CellSpacing="1" CellPadding="1"  AllowPaging="false" Width="100%"  
         AutoGenerateColumns="false" GridLines = "None" 
        ondatabound="Batches_DataBound" onrowdatabound="Batches_Grid_RowDataBound">
        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="msgheader" />
		              <AlternatingRowStyle CssClass="datatd1" />
		              <RowStyle CssClass="datatd" />
		              <Columns>
		                  <asp:TemplateField  ControlStyle-Width="4%" ItemStyle-CssClass="data">
		                  <ItemTemplate>
		                  
		                   <%--<input type="radio" id="selectrdo" onclick=" document.forms[0].selectedrow.value = this.value; OnSelect();";--%>
		                   <input type="radio" id="selectrdo" onclick=" document.forms[0].selectedrow.value = this.value; ";
		                     name="Batches_Radio" value='<%# DataBinder.Eval(Container, "DataItem.BatchRowId")%>' />                               
		                  <%--  <input id="MyRadioButton" name="Batches" type="radio" onclick="OnClick"
                                value='<%# Unique_Id==""? "" : Eval("UniqueId") %>' />--%>
                          </ItemTemplate>
                                    <%--<ItemTemplate>                               
                                        <asp:RadioButton runat="server" id="checkedrow"/>
                                    </ItemTemplate>--%>                                     
                          </asp:TemplateField> 
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="Batch Number" Visible="false" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblBatchNumber" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BatchRowId")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="Status" Visible="true" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblStatus" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Status")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="Batch Type" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="Type" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Type")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="Date Opened" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblDateOpened" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DateOpened")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="Batch Count" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblBatchCount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BatchCount")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  
                          
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="Batch Amount" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblAmount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Amount")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  
  
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="Date Closed" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblDateClosed" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DateClosed")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  
  
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="AddedByUser" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblAddedByUser" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AddedByUser")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>                                                                  
		              </Columns> 
		 </asp:GridView>	 
		 
		 <br />
		 <br />		 
       
        <b>Associated Billing Items</b>
		 <asp:GridView ID="BillingItems_Grid" runat="server" BorderWidth="1" 
         CellSpacing="1" CellPadding="1"  AllowPaging="false" Width="100%"  
         AutoGenerateColumns="false" GridLines="Both" ondatabound="BillingItems_DataBound">
        <HeaderStyle CssClass="msgheader" />
		              <AlternatingRowStyle CssClass="datatd1"  />
		              <RowStyle CssClass="datatd" />
		              <Columns>		                                            
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="DateEntered" Visible="true" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblDateEntered" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DateEntered")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="Billing Item Type" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Type")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="Status" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblStatus" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Status")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="Amount" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblAmount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Amount")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>                            
                
  
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="Date Reconciled" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblDateReconcile" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DateReconcile")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  
  
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="Final Batch Number" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblFinalBatchNum" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FinalBatchNum")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>                                                                  
                          
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="Final Batch Type" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                   <ItemTemplate>
                                      <asp:Label ID="lblFinalBatchType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FinalBatchType")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>                                                                  

                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="Entry Batch Number" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblEntryBatchNum" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EntryBatchNum")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>                                                                  

                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="Entry Batch Type" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblEntryBatchType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EntryBatchType")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>                                                                  

                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="Invoice Number" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblInvoiceId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.InvoiceId")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          
		              </Columns> 
		 </asp:GridView>
       
       <br />
       <br />
    <table border="0" CELLPADDING="0" CELLSPACING="0">
     <tr>
        <td colspan="2"><asp:button runat="server"  type="button" class="button" id="Close" OnClientClick="return OnClose();" text="Close Batch" tabindex="9"/></td>
        <td colspan="2"><asp:button runat="server" type="button" class="button" id="Reverse" OnClientClick="return OnReverse();" text="Reverse Batch" tabindex="10"/></td>
        <td colspan="2"><asp:button runat="server" type="button" class="button" id="Abort" OnClientClick="return OnAbort();" text="Abort Batch" tabindex="11"/></td>
     </tr>
    </table>   
    
    
    </form>
</body>
</html>
