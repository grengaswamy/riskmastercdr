﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;

namespace Riskmaster.UI.Billing
{
    public partial class PremiumFrame : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XElement oMessageElement = null;
            XElement oSessionCmdElement = null;
            string sPolicyId = string.Empty;
            string sTermNumber = string.Empty;
            string sPolicyNumber = string.Empty;
            string sBillItemRowId = string.Empty;
            try
            {
                if (!IsPostBack)
                {
                    oMessageElement = GetMessageTemplate();

                    sPolicyId = AppHelper.GetQueryStringValue("PolicyId");
                    oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/Document/PolicyId");
                    oSessionCmdElement.Value = sPolicyId;

                    sTermNumber = AppHelper.GetQueryStringValue("TermNumber");
                    oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/Document/TermNumber");
                    oSessionCmdElement.Value = sTermNumber;

                    sPolicyNumber = AppHelper.GetQueryStringValue("PolicyNumber");
                    oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/Document/PolicyNumber");
                    oSessionCmdElement.Value = sPolicyNumber;

                    sBillItemRowId = AppHelper.GetQueryStringValue("selectedid");
                    oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/Document/BillingItemRowId");
                    oSessionCmdElement.Value = sBillItemRowId;
                    
                    CallCWSFunction("BillingAdaptor.GetPremiumItemData", oMessageElement);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>9fd029d5-380f-4eff-8cef-0669edb70b75</Authorization>");
            sXml = sXml.Append("<Call><Function>BillingAdaptor.GetPremiumItemData</Function></Call><Document>");
            sXml = sXml.Append("<Document><PolicyId>");
            sXml = sXml.Append("</PolicyId><TermNumber>");
            sXml = sXml.Append("</TermNumber><PolicyNumber>");
            sXml = sXml.Append("</PolicyNumber><BillingItemRowId>");
            sXml = sXml.Append("</BillingItemRowId>");
            sXml = sXml.Append("<PremiumItem></PremiumItem>");
            sXml = sXml.Append("</Document>");
            sXml = sXml.Append("</Document></Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;
        }
    }
}
