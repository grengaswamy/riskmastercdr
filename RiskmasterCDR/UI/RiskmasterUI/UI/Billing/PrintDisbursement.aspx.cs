﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;
using System.Xml;

namespace Riskmaster.UI.Billing
{
    public partial class PrintDisbursement : NonFDMBasePageCWS
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            XElement XmlTemplate = null;
            bool bReturnStatus;
            string sSelectedBillingItem = "";
            string sCWSresponse = "";

            try
            {
                sSelectedBillingItem = AppHelper.GetQueryStringValue("SelectedBillingItem");
                XmlTemplate = GetMessageTemplate(sSelectedBillingItem);                
                
                bReturnStatus = CallCWS("BillingAdaptor.PrintDisbursement", XmlTemplate, out sCWSresponse,false, true );

                HtmlControl frame = (HtmlControl)this.FindControl("embeddedFrame");
                frame.Attributes["src"] = "/RiskmasterUI/UI/PrintChecks/PrintSingleCheckFrame.aspx?"
                    + "transid=" + TransID.Text + "&totalamount=" + Amount.Text  
                    + "&accountname=" + Account.Text + "&Source=" + Source.Text 
                    + "SelDisbursementRowID=" + SelBillingRowID.Text;


            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
               // ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }



        private XElement GetMessageTemplate(string sSelectedBillingItem)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>");

            sXml = sXml.Append("<SelectedBillingItem>");
            sXml = sXml.Append(sSelectedBillingItem);
            sXml = sXml.Append("</SelectedBillingItem>");     

            sXml = sXml.Append("</Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }



    }
}
