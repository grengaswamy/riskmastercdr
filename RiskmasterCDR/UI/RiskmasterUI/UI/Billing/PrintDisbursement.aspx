﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintDisbursement.aspx.cs" Inherits="Riskmaster.UI.Billing.PrintDisbursement" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Print Disbursement</title>
     <%-- aravi5: Changes for RMA-10157,RMA-10491 Policy billing show modal dialogue issues Starts --%>
    <script type="text/javascript">
        function HideOverlaydiv() {
            var IEbrowser = false || !!document.documentMode; // At least IE6
            if (!IEbrowser) {
                if (window.opener.parent.parent.document.getElementById("overlaydiv") != null) {
                    window.opener.parent.parent.document.getElementById("overlaydiv").remove();
                }
                return false;
            }
        }
        function ShowOverlay() {
            var IEbrowser = false || !!document.documentMode; // At least IE6
            if (!IEbrowser) {
                if (window.opener.parent.parent.document.getElementById("overlaydiv") == null) {
                    var div = document.createElement('div');
                    div.id = 'overlaydiv';
                    div.className = 'overlay';
                    div.style.display = 'block';
                    window.opener.parent.parent.document.getElementById('cphHeaderBody').appendChild(div);
                }
            }
        }
        function onunload() {
            var IEbrowser = false || !!document.documentMode; //RMA-10491
            if (!IEbrowser && window.document.forms[0].hdnPrintBatchOk.value == "Processed") {
                window.opener.document.forms[0].submit();
                window.close();
                return false;
            }
        }
    </script>
    <%-- aravi5: Changes for RMA-10157 Policy billing show modal dialogue issues Ends --%>
</head>
<body onpagehide="HideOverlaydiv(); return false;" onunload="onunload();"><%-- aravi5: Changes for RMA-10491 --%>
    <form id="frmData" runat="server">
    <div>   
        
    <iframe name="embeddedFrame" id="embeddedFrame" runat="server" width="100%" height="100%" scrolling="no" frameborder="0" marginheight="0" marginwidth="0"/>    
    
    <asp:TextBox runat="server" ID="TransID" style="display: none" rmxref="/Instance/Document/CheckDetails/TransID" />
    <asp:TextBox runat="server" ID="Amount" style="display: none" rmxref="/Instance/Document/CheckDetails/Amount" />
    <asp:TextBox runat="server" ID="Account" style="display: none" rmxref="/Instance/Document/CheckDetails/Account" />
    <asp:TextBox runat="server" ID="Source" style="display: none" rmxref="/Instance/Document/CheckDetails/Source" />
    <asp:TextBox runat="server" ID="SelBillingRowID" style="display: none" rmxref="/Instance/Document/CheckDetails/SelBillingRowID" />                
    <input type='hidden' id="hdnPrintBatchOk" runat=server /></input>         
                
                
<%--       src="/RiskmasterUI/UI/Funds/PrintSingleCheck.aspx?IncludeClaimantInPayee='" + document.getElementById('IncludeClaimantInPayee').value
       + "&transid=" + document.getElementById('transid').value + 
       + "&totalamount=" + document.getElementById('totalamount').value
       + "&accountname=" + document.getElementById('accountname').value
       + "&Source=" + document.getElementById('Source').value
       + "&SelDisbursementRowID=" + + document.getElementById('SelDisbursementRowID').value
       
       />
--%>       

    <%--&amp;IncludeClaimantInPayee=<xsl:value-of select="document('oxf:instance')/Instance/Document/IncludeClaimantInPayee" />&amp;transid=<xsl:value-of select="//TransID"/>&amp;totalamount=<xsl:value-of select="//Amount"/>&amp;accountname=<xsl:value-of select="//Account"/>&amp;Source=<xsl:value-of select="//Source"/>&amp;SelDisbursementRowID=<xsl:value-of select="//SelBillingRowID"--%>
         
         

    
    
    </div>
    </form>
</body>
</html>
