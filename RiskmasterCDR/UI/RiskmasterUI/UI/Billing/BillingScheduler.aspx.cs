﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Text;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Billing
{
    public partial class BillingScheduler : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            XElement XmlTemplate = null;
            XmlDocument XmlDoc = new XmlDocument();
            string sreturnValue = null;
            try
            {
                if (action.Text == "delete")
                {
                    XmlTemplate = GetMessageTemplate1();
                    bReturnStatus = CallCWSFunction("BillingAdaptor.RemoveFileFromDisk", out sreturnValue, XmlTemplate);
                }
                XmlTemplate = GetMessageTemplate();
                bReturnStatus = CallCWSFunction("BillingAdaptor.GetInvoicesandNoticesList", out sreturnValue, XmlTemplate);
                
                if (bReturnStatus)
                {
                    XmlDoc.LoadXml(sreturnValue);
                    CreateNoticeList(XmlDoc);
                    CreateInvoiceList(XmlDoc);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>e2c4ab25-d34f-4ca8-8465-6a18d8aa64a9</Authorization>");
            sXml = sXml.Append("<Call><Function>BillingAdaptor.GetInvoicesandNoticesList</Function></Call><Document><BillingScheduler />");    
            sXml = sXml.Append("</Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        private XElement GetMessageTemplate1()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>0d2cd9cd-08e4-488b-ac7f-9a1c2779a3a0</Authorization>");
            sXml = sXml.Append("<Call><Function>BillingAdaptor.RemoveFileFromDisk</Function></Call><Document><BillingScheduler><FileName>");
            sXml = sXml.Append(filename.Text);
            sXml = sXml.Append("</FileName></BillingScheduler></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        protected void gvList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drvGridRow = (DataRowView)e.Row.DataItem;
                LinkButton lbLinkColumn;
                lbLinkColumn = (LinkButton)e.Row.FindControl("SNo");
                lbLinkColumn.Attributes.Add("href", "#");
                //Changed by Saurabh Arora for MITS 20558:Start
                //lbLinkColumn.Attributes.Add("onclick", "selectPDF('" + ((System.Web.UI.WebControls.Label)(e.Row.FindControl("Filename"))).Text + "');");
                lbLinkColumn.Attributes.Add("onclick", "selectPDF('" + ((System.Web.UI.WebControls.Label)(e.Row.FindControl("Filename"))).Text + "');return false;");
                //Changed by Saurabh Arora for MITS 20558:End

            }
        }

        protected void CreateNoticeList(XmlDocument XmlDoc_Notice)
        {
            gvNoticeList.HeaderStyle.CssClass = "msgheader";
            gvNoticeList.RowStyle.CssClass = "datatd1";
            gvNoticeList.AlternatingRowStyle.CssClass = "datatd";
            XmlNodeList NoticeRecords = XmlDoc_Notice.SelectNodes("/ResultMessage/Document/BillingScheduler/Notices/Notice");
            if (NoticeRecords.Count != 0)
            {
                DataTable objDataTable = null;
                XmlNode xNode;
                DataRow objRow;

                objDataTable = new DataTable("Notices");
                objDataTable.Columns.Add("SNo");
                objDataTable.Columns.Add("Date");
                objDataTable.Columns.Add("Time");
                objDataTable.Columns.Add("Filename");

                for (int i = 0; i < NoticeRecords.Count; i++)
                {
                    xNode = NoticeRecords[i];
                    objRow = objDataTable.NewRow();

                    objRow["SNo"] = xNode.SelectSingleNode("S_NO").InnerText;
                    objRow["Date"] = xNode.SelectSingleNode("Date").InnerText;
                    objRow["Time"] = xNode.SelectSingleNode("Time").InnerText;
                    objRow["Filename"] = xNode.SelectSingleNode("Filename").InnerText;

                    objDataTable.Rows.Add(objRow);
                }
                gvNoticeList.DataSource = objDataTable;
                gvNoticeList.DataBind();
            }
            else
            {
                gvNoticeList.Visible = false;
                lblMessage.Text = "<p class='gen' align='center'><br><br><br><h3 align='center'>&nbsp;&nbsp;&nbsp;&nbsp;                    There are no notices for printing!</h3></p>";
            }

        }
        protected void CreateInvoiceList(XmlDocument XmlDoc_Invoice)
        {

            gvInvoiceList.HeaderStyle.CssClass = "msgheader";
            gvInvoiceList.RowStyle.CssClass = "datatd1";
            gvInvoiceList.AlternatingRowStyle.CssClass = "datatd";
            XmlNodeList InvoiceRecords = XmlDoc_Invoice.SelectNodes("/ResultMessage/Document/BillingScheduler/Invoices/Invoice");
            if (InvoiceRecords.Count != 0)
            {
                DataTable objDataTable = new DataTable();
                XmlNode xNode;
                DataRow objRow;

                objDataTable = new DataTable("Invoices");
                objDataTable.Columns.Add("SNo");
                objDataTable.Columns.Add("Date");
                objDataTable.Columns.Add("Time");
                objDataTable.Columns.Add("Filename");

                for (int i = 0; i < InvoiceRecords.Count; i++)
                {
                    xNode = InvoiceRecords[i];
                    objRow = objDataTable.NewRow();

                    objRow["SNo"] = xNode.SelectSingleNode("S_NO").InnerText;
                    objRow["Date"] = xNode.SelectSingleNode("Date").InnerText;
                    objRow["Time"] = xNode.SelectSingleNode("Time").InnerText;
                    objRow["Filename"] = xNode.SelectSingleNode("Filename").InnerText;

                    objDataTable.Rows.Add(objRow);
                }
                gvInvoiceList.DataSource = objDataTable;
                gvInvoiceList.DataBind();
            }
            else
            {
                gvInvoiceList.Visible = false;
                lblMessage1.Text = "<p class='gen' align='center'><br><br><br><h3 align='center'>&nbsp;&nbsp;&nbsp;&nbsp;                    There are no invoices for printing!</h3></p>";
            }

        }


    }
}
