﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BillingScheduler.aspx.cs" Inherits="Riskmaster.UI.Billing.BillingScheduler" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Billing Scheduler</title>
        <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />

    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js">        { var i; }</script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/grid.js">        { var i; }</script>
        <script language="javascript" type="text/javascript">
            function selectPDF(sFileName)
            {  
              //RMX.window.open
              var width = screen.availWidth - 60;
	            var height = screen.availHeight - 60 ;
	            var sReturnValue = showModalDialog("/RiskmasterUI/UI/Billing/PrintInvoicesNoticesFrame.aspx?FileName=" + sFileName, null, 'dialogWidth:' + width + 'px;dialogHeight:' + height + 'px;dialogtop:' + (screen.availHeight - height) / 2 + '; dialogleft:' + (screen.availWidth - width) / 2 + ';resizable:yes;scrollbars:yes;status:no;center:yes;');
	            //var sReturnValue = window.open("/RiskmasterUI/UI/Billing/PrintInvoicesNoticesFrame.aspx?FileName=" + sFileName, 'Adjustment', 'width=' + width + ',height=' + height + ',top=' + (screen.availHeight - height) / 2 + ',left=' + (screen.availWidth - width) / 2 + ',resizable=no,scrollbars=no,status=no,center=yes');
              if(confirm("If the invoice/notice has been printed successully, do you wish to go ahead and delete it ?" ))
	            {
                //Delete the selected notice/invoice

//	                window.document.forms[0].ouraction.value = "delete";
	                window.document.forms[0].action.value = "delete";
                    window.document.forms[0].filename.value = sFileName;
                
//                    window.document.forms[0].method = "post" ;
                    window.document.forms[0].submit();                						            
	            }                       
            }					
	</script>    
</head>
<body onload="parent.MDIScreenLoaded();">
    <form id="frmData" runat="server">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <div class="msgheader" id="div_formtitle">
        <asp:Label ID="notice_title" runat="server" Text="Scheduled Notices" />
    </div>
    <table>
            <tr>
                <td>
                    <asp:gridview ID="gvNoticeList" runat="server" 
                        onrowdatabound="gvList_RowDataBound" AutoGenerateColumns="False" 
                        CssClass="singleborder" 
                        Width="560px" >
                        <HeaderStyle HorizontalAlign="Left" />
                        <Columns>            
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="S.No" Visible="true" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate >
                                      <asp:LinkButton ID="SNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SNo")%>' ></asp:LinkButton>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="Date" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="Date" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Date")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="Time" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="Time" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Time")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="Filename" Visible="false" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="Filename" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Filename")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                        </Columns>
                        </asp:gridview>
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </td>
            </tr>
    </table> 
    
    <div class="msgheader" id="div1">
        <asp:Label ID="invoice_title" runat="server" Text="Scheduled Invoices" />
    </div>
    <table>
            <tr>
                <td>
                    <asp:gridview ID="gvInvoiceList" runat="server" 
                        onrowdatabound="gvList_RowDataBound" AutoGenerateColumns="False" 
                        CssClass="singleborder" 
                        Width="560px" >
                        <HeaderStyle HorizontalAlign="Left" />
                        <Columns>            
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="S.No" Visible="true" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:LinkButton ID="SNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SNo")%>' ></asp:LinkButton>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="Date" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="Date" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Date")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="Time" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="Time" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Time")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  
                          <asp:TemplateField ControlStyle-Width="24%" HeaderText="Filename" Visible="false" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="Filename" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Filename")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                        </Columns>                        
                        </asp:gridview>
                    <asp:Label ID="lblMessage1" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        <asp:textbox ID="action" style="display:none" RMXRef="" runat="server" />
        <asp:textbox ID="filename" style="display:none" RMXRef="" runat="server" />
    </form>
</body>
</html>
