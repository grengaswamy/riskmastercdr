﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BatchReconciliationDetails.aspx.cs" Inherits="Riskmaster.UI.UI.Billing.BatchReconciliationDetails" %>

<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
     <uc4:CommonTasks ID="CommonTasks1" runat="server" />
        <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js"></script>
        <script type="text/javascript" src="../../Scripts/Batches.js"></script>
</head>
<body>
    <form id="frmData" runat="server">
    
    
    <table border="0">
 <tr>
      <td colspan="2">
        <uc3:ErrorControl ID="ErrorControl1" runat="server" />
       </td>
    </tr>  
    </table>
       <table border="0">
        <tbody>
         <tr>
          <td class="ctrlgroup" colSpan="2"></td>
         </tr>
         <tr>
          <td colspan="2">
           <table width="100%" align="left">
            <tr>
             <td class="ctrlgroup" width="100%">Please select the reconciliation type:</td>
            </tr>
           </table>
          </td>
         </tr>
         <tr>
          <td>Reconciliation Type:&nbsp;&nbsp;</td>
          <td>
          
          <div runat="server" class="completerow" id="div_reconciliationtypecode" xmlns="">                       
            <uc:CodeLookUp runat="server" ID="ReconciliationType" CodeTable="RECONCILE_TYPES" ControlName="ReconciliationType" RMXRef="/Instance/Document/Document/ReconciliationType" RMXType="code" tabindex="1" />                          
          </div>                   
          
         </tr>
         <tr>
          <td>Batch Total:&nbsp;&nbsp;</td>
          <td>          
          <asp:textbox RMXType="currency" runat="server" type="text" size="30" id="BatchTotal" maxlength="50" tabindex="2" RMXRef="/Instance/Document/Document/BatchTotal" onblur="currencyLostFocus(this);" onchange="setDataChanged(true);" readonly="true" style="background-color: silver;"/>
          </td>
         </tr>
         <tr>
          <td colspan="2">          
              &nbsp;</td>
         </tr>
        </tbody>
       </table>
       <table>
        <tbody>
         <tr>         
          <td>                  
          
            <asp:textbox ID="BatchId" style="display:none" RMXRef="/Instance/Document/Document/BatchId" runat="server" />            
            <asp:textbox ID="CheckPaidInFull" style="display:none" RMXRef="Instance/Document/Document/CheckPaidInFull" runat="server" />
            <asp:button runat="server" class="button" id="btnOk" style="width:120px" 
                  onclientclick="OnReconcile();" Text="Ok" tabindex="3" />
            <asp:button runat="server" class="button" text="Cancel" id="btnCancel" style="width:120px" 
                  onclientclick="OnCancel()"  tabindex="4" />
          </td>
         </tr>
        </tbody>
       </table>  
    
    
    </form>
</body>
</html>
  