﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Riskmaster.UI.Billing
{
    public partial class NewBillingItemTypeFrame : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
                policyid.Text = AppHelper.GetQueryStringValue("PolicyId");
                TermNumber.Text = AppHelper.GetQueryStringValue("TermNumber");
                PolicyNumber.Text = AppHelper.GetQueryStringValue("PolicyNumber");
        }
    }
}
