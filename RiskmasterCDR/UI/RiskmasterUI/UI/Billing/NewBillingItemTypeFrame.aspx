﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewBillingItemTypeFrame.aspx.cs" Inherits="Riskmaster.UI.Billing.NewBillingItemTypeFrame" %>

<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc1" %>

<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagPrefix="uc2" TagName="ErrorControl"  %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Select Billing Type</title>
    <base target="_self"/>
    <META HTTP-EQUIV="Pragma" CONTENT="no-cache"/>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/system.css" type="text/css" />
    <script type="text/javascript" src="/RiskmasterUI/Scripts/Billing.js"></script>
    <%-- aravi5: Changes for RMA-10188 New button - show modal dialog issue - policy billing Starts --%>
    <script type="text/javascript">
        function HideOverlaydiv() {
            var IEbrowser = false || !!document.documentMode; // At least IE6
            if (!IEbrowser) {
                if (window.opener.parent.parent.document.getElementById("overlaydiv") != null) {
                    window.opener.parent.parent.document.getElementById("overlaydiv").remove();
                }
                return false;
            }
        }
     </script>
    <%-- aravi5: Changes for RMA-10188 New button - show modal dialog issue - policy billing Ends --%>
</head>
<body onpagehide="HideOverlaydiv(); return false;"><%-- aravi5: Changes for RMA-10188 --%>
    <form name="frmData" id="frmData" runat="server">
          <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />
      <uc2:ErrorControl ID="ErrorControl1" runat="server" />
      <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" Text="Billing Type" />
        <asp:label id="formsubtitle" runat="server" Text="" />
      </div>
          <p>
      <asp:LinkButton ID="Link_Receipt" runat="server" onclientclick = " OnLinkClick('Receipt')">Receipt</asp:LinkButton>
          </p>
          <p>
      <asp:LinkButton ID="Link_Adjustment" runat="server" onclientclick = " OnLinkClick('Adjustment')">Adjustment</asp:LinkButton>
          </p>
          <p>
      <asp:LinkButton ID="Link_Disbursement" runat="server" onclientclick = " OnLinkClick('Disbursement')">Disbursement</asp:LinkButton>
          </p>
          
      <asp:TextBox style="display:none" runat="server" id="policyid" RMXRef="" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="PolicyNumber" RMXRef="" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="TermNumber" RMXRef="" RMXType="hidden" Text="" />   
    </form>
</body>
</html>
