<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DisbursementFrame.aspx.cs" Inherits="Riskmaster.UI.Billing.DisbursementFrame" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/UserControlDataGrid.ascx" tagname="UserControlDataGrid" tagprefix="dg" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagPrefix="uc2" TagName="ErrorControl"  %>
<%@ Register Assembly="MultiCurrencyCustomControl" Namespace="MultiCurrencyCustomControl"  TagPrefix="mc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove" xmlns:cul="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
  <head id="Head1" runat="server">
    <title>Disbursement</title>
    <base target="_self"/>
    <META HTTP-EQUIV="Pragma" CONTENT="no-cache"/>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/system.css" type="text/css" />
    <link rel="stylesheet" href="/RiskmasterUI/Content/zpcal/themes/system.css" type="text/css" />   
    <script language="JavaScript" src="../../Scripts/form.js">{var i;}
</script>
    <script language="JavaScript" src="../../Scripts/drift.js">{var i;}
</script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js">{var i;}
  </script>
    <%--<script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">{var i;}
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">{var i;}
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">{var i;}
	</script>--%>

       <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <script type="text/javascript" language="javascript" ></script>
    <script language="JavaScript" src="../../Scripts/Billing.js">{var i;}</script>
    <%-- aravi5: RMA-10436 On selecting a new billing type page shows a js error. Starts --%>
    <script type="text/javascript">
        function HideOverlaydiv() {
            var IEbrowser = false || !!document.documentMode; // At least IE6
            if (!IEbrowser) {
                if (window.opener.parent.parent.document.getElementById("overlaydiv") != null) {
                    window.opener.parent.parent.document.getElementById("overlaydiv").remove();
                }
                return false;
            }
        }
    </script>
    <%-- aravi5: RMA-10436 On selecting a new billing type page shows a js error. Ends --%>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
  </head>
  <body class="10pt" onpagehide="HideOverlaydiv(); return false;" onload="OnDisbursementLoad();"> <%-- aravi5: RMA-10436 --%>
    <form name="frmData" id="frmData" runat="server">
      <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />
      <uc2:ErrorControl ID="ErrorControl1" runat="server" />
      <asp:HiddenField runat="server" ID="wsrp_rewrite_action_1" value="" />
      <asp:TextBox style="display:none" runat="server" name="hTabName" id="hTabName" />
      <asp:ScriptManager ID="SMgr" runat="server" />
      <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" Text="Disbursement" />
        <asp:label id="formsubtitle" runat="server" Text="" />
      </div>
      <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" Text="" />
      </div>
      <div class="tabGroup" id="TabsDivGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSBillingItem" id="TABSBillingItem">
          <a class="Selected" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="BillingItem" id="LINKTABSBillingItem">Billing Item Details</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPBillingItem">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSDisbursement" id="TABSDisbursement">
          <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="Disbursement" id="LINKTABSDisbursement">Disbursement Details</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPDisbursement">
          <nbsp />
          <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSReconcilationDetails" id="TABSReconcilationDetails">
          <a class="NotSelected1" HREF="#" onClick="tabChange(this.name);" runat="server" RMXRef="" name="ReconcilationDetails" id="LINKTABSReconcilationDetails">Reconcilation Details</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPReconcilationDetails">
          <nbsp />
          <nbsp />
        </div>
      </div>
      <div class="topborder" runat="server" name="FORMTABBillingItem" id="FORMTABBillingItem"> <%--RMA-10274 --%>
        <table width="98%" border="0" cellspacing="0" celpadding="0" padding="0">
          <tr>
            <td>
              <div runat="server" class="half" id="div_policyName" xmlns="">
                <asp:label runat="server" class="label" id="lbl_policyName" Text="Policy Name" />
                <span class="formw">
                  <asp:TextBox runat="server" onchange="setDataChanged(true);" id="policyName" RMXRef="/Instance/Document/Disbursements/PolicyName" RMXType="text" readonly="true" style="&#xA;                                background-color: silver;&#xA;                            " />
                </span>
              </div>
              <div runat="server" class="half" id="div_DateEntered" xmlns="">
                <asp:label runat="server" class="label" id="lbl_DateEntered" Text="Date Entered" />
                <span class="formw">
                  <asp:TextBox runat="server" onchange="setDataChanged(true);" id="DateEntered" RMXRef="/Instance/Document/Disbursements/DateEntered" RMXType="text" readonly="true" style="&#xA;                                background-color: silver;&#xA;                            " />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_policyNumber" xmlns="">
                <asp:label runat="server" class="label" id="lbl_policyNumber" Text="Policy Number" />
                <span class="formw">
                  <asp:TextBox runat="server" onchange="setDataChanged(true);" id="policyNumber" RMXRef="/Instance/Document/Disbursements/PolicyNumber" RMXType="text" readonly="true" style="&#xA;                                background-color: silver;&#xA;                            " />
                </span>
              </div>
              <div runat="server" class="half" id="div_TermNumber" xmlns="">
                <asp:label runat="server" class="label" id="lbl_TermNumber" Text="Term Number" />
                <span class="formw">
                  <asp:TextBox runat="server" onchange="setDataChanged(true);" id="TermNumber" RMXRef="/Instance/Document/Disbursements/TermNumber" RMXType="text" readonly="true" style="&#xA;                                background-color: silver;&#xA;                            " />
                </span>
              </div>
            </td>
          </tr>
        </table>
      </div>
      <div class="topborder" style="display:none;" runat="server" name="FORMTABDisbursement" id="FORMTABDisbursement"> <%--RMA-10274 --%>
        <table width="98%" border="0" cellspacing="0" celpadding="0">
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_PaymentMethod" xmlns="">
                <asp:label runat="server" class="required" id="lbl_PaymentMethod" Text="Payment Method" />
                <span class="formw">
                  <asp:DropDownList runat="server" id="PaymentMethod" RMXRef="/Instance/Document/Disbursements/PaymentMethod" RMXType="combobox" ItemSetRef="/Instance/Document/Disbursements/CodeList" onchange="setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_Amount" xmlns="">
                <asp:label runat="server" class="required" id="lbl_Amount" Text="Amount" />
                <span class="formw">
                  <mc:CurrencyTextbox runat="server" id="Amount" RMXRef="/Instance/Document/Disbursements/Amount" RMXType="currency"  rmxforms:as="currency" onchange=";setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_BankAccount" xmlns="">
                <asp:label runat="server" class="required" id="lbl_BankAccount" Text="Bank Account" />
                <span class="formw">
                  <asp:DropDownList runat="server" id="BankAccount" RMXRef="/Instance/Document/Disbursements/BankAccount" RMXType="combobox" ItemSetRef="/Instance/Document/Disbursements/AccountList" onchange="setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_TransactionType" xmlns="">
                <asp:label runat="server" class="required" id="lbl_TransactionType" Text="Transaction Type" />
                <span class="formw">
                  <asp:DropDownList runat="server" id="TransactionType" RMXRef="/Instance/Document/Disbursements/TransactionType" RMXType="combobox" ItemSetRef="/Instance/Document/Disbursements/TransTypeCodes" onchange="setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_Printed" xmlns="">
                <asp:label runat="server" class="label" id="lbl_Printed" Text="Printed?" />
                <span class="formw">
                  <asp:CheckBox runat="server" onchange="setDataChanged(true);" id="Printed" RMXRef="/Instance/Document/Disbursements/PrintedFlag" RMXType="checkbox" disabled="true" style="background-color: silver;" Height="24px" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_Date" xmlns="">
                <asp:label runat="server" class="label" id="lbl_Date" Text="Date" />
                <span class="formw">
                  <asp:TextBox runat="server" FormatAs="date" id="Date" RMXRef="/Instance/Document/Disbursements/DatePrinted" RMXType="date" readonly="true" style="background-color: silver;" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                 <%-- <asp:button class="DateLookupControl" runat="server" id="Datebtn" />
                  <script type="text/javascript">
					Zapatec.Calendar.setup(
					{
					inputField : "Date",
					ifFormat : "%m/%d/%Y",
					button : "Datebtn"
					}
					);
				</script>--%>

                    <script type="text/javascript">
                        $(function () {
                            $("#Date").datepicker({
                                showOn: "button",
                                buttonImage: "../../Images/calendar.gif",
                                //buttonImageOnly: true,
                                showOtherMonths: true,
                                selectOtherMonths: true,
                                changeYear: true
                            }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });//vkumar258 ML Changes
                        });
                    </script>
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_CheckNumber" xmlns="">
                <asp:label runat="server" class="label" id="lbl_CheckNumber" Text="Check Number" />
                <span class="formw"><!--pmital5 Mits 14848 04/20/09 - Changed onblur function -->
                  <asp:TextBox runat="server" size="30" onblur="checkNumLostFocus(this);" id="CheckNumber" maxlength="18" RMXRef="/Instance/Document/Disbursements/CheckNumber" onchange="&#xA;                  AllowIntOnly(this);&#xA;                  setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_PayeeName" xmlns="">
                <asp:label runat="server" class="label" id="lbl_PayeeName" Text="Payee" />
                <span class="formw">
                  <asp:TextBox runat="server" onchange="setDataChanged(true);" id="PayeeName" RMXRef="/Instance/Document/Disbursements/PayeeName" RMXType="text" readonly="true" style="&#xA;                                background-color: silver;&#xA;                            " />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_InvoiceNumber" xmlns="">
                <asp:label runat="server" class="label" id="lbl_InvoiceNumber" Text="Invoice Number" />
                <span class="formw">
                  <asp:TextBox runat="server" size="30" onblur="numLostFocus(this);" id="InvoiceNumber" RMXRef="/Instance/Document/Disbursements/InvoiceNumber" onchange="&#xA;                  AllowIntOnly(this);&#xA;                  setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_NoticeNumber" xmlns="">
                <asp:label runat="server" class="label" id="lbl_NoticeNumber" Text="Notice Number" />
                <span class="formw">
                  <asp:TextBox runat="server" size="30" onblur="numLostFocus(this);" id="NoticeNumber" RMXRef="/Instance/Document/Disbursements/NoticeNumber" onchange="&#xA;                  AllowIntOnly(this);&#xA;                  setDataChanged(true);" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_Comments" xmlns="">
                <asp:label runat="server" class="label" id="lbl_Comments" Text="Comments" />
                <span class="formw">
                  <%--Change by kuladeep for MITS:24930 Start--%>
                  <%--<asp:TextBox runat="Server" id="Comments" RMXRef="/Instance/Document/Disbursements/Comment" RMXType="memo" readonly="true" style="background-color: silver;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />--%>
                  <asp:TextBox runat="Server" id="Comments" RMXRef="/Instance/Document/Disbursements/Comment" RMXType="memo" style="background-color: silver;" onchange="setDataChanged(true);" TextMode="MultiLine" Columns="30" rows="3" />
                  <%--Change by kuladeep for MITS:24930 End--%>
                  <input type="button" class="button" value="..." name="CommentsbtnMemo" tabindex="NaN" id="CommentsbtnMemo" onclick="EditMemo('Comments','')" />
                </span>
              </div>
            </td>
          </tr>
        </table>
      </div>
      <div class="topborder" style="display:none;" runat="server" name="FORMTABReconcilationDetails" id="FORMTABReconcilationDetails"> <%--RMA-10274 --%>
        <table width="98%" border="0" cellspacing="0" celpadding="0">
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_Status" xmlns="">
                <asp:label runat="server" class="label" id="lbl_Status" Text="Status" />
                <span class="formw">
                  <asp:TextBox runat="server" onchange="setDataChanged(true);" id="Status" RMXRef="/Instance/Document/Disbursements/Status" RMXType="text" readonly="true" style="&#xA;                                background-color: silver;&#xA;                            " />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_DateReconciled" xmlns="">
                <asp:label runat="server" class="label" id="lbl_DateReconciled" Text="Date Reconciled" />
                <span class="formw">
                  <asp:TextBox runat="server" onchange="setDataChanged(true);" id="DateReconciled" RMXRef="/Instance/Document/Disbursements/DateReconciled" RMXType="text" readonly="true" style="&#xA;                                background-color: silver;&#xA;                            " />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_FinalBatchType" xmlns="">
                <asp:label runat="server" class="label" id="lbl_FinalBatchType" Text="Final Batch Type" />
                <span class="formw">
                  <asp:TextBox runat="server" onchange="setDataChanged(true);" id="FinalBatchType" RMXRef="/Instance/Document/Disbursements/FinalBatchType" RMXType="text" readonly="true" style="&#xA;                                background-color: silver;&#xA;                            " />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_FinalBatchNo" xmlns="">
                <asp:label runat="server" class="label" id="lbl_FinalBatchNo" Text="Final Batch No" />
                <span class="formw">
                  <asp:TextBox runat="server" onchange="setDataChanged(true);" id="FinalBatchNo" RMXRef="/Instance/Document/Disbursements/FinalBatchNumber" RMXType="text" readonly="true" style="&#xA;                                background-color: silver;&#xA;                            " />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_EntryBatchType" xmlns="">
                <asp:label runat="server" class="label" id="lbl_EntryBatchType" Text="Entry Batch Type" />
                <span class="formw">
                  <asp:TextBox runat="server" onchange="setDataChanged(true);" id="EntryBatchType" RMXRef="/Instance/Document/Disbursements/EntryBatchType" RMXType="text" readonly="true" style="&#xA;                                background-color: silver;&#xA;                            " />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_EntryBatchNo" xmlns="">
                <asp:label runat="server" class="label" id="lbl_EntryBatchNo" Text="Entry Batch No" />
                <span class="formw">
                  <asp:TextBox runat="server" onchange="setDataChanged(true);" id="EntryBatchNo" RMXRef="/Instance/Document/Disbursements/EntryBatchNumber" RMXType="text" readonly="true" style="&#xA;                                background-color: silver;&#xA;                            " />
                </span>
              </div>
            </td>
          </tr>
        </table>
      </div>
      <div id="Div2" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_Ok">
          <script language="JavaScript" src="">{var i;}
          </script>
          <asp:button class="button" runat="server" id="Ok" RMXRef="" Text="Ok" width="100" UseSubmitBehavior="false"
                onClientClick="javascript: DisbursementOkclick();" onclick="Ok_Click" /> <%-- aravi5: RMA-10436 --%>
        </div>
        <div class="formButton" runat="server" id="div_Cancel">
          <script language="JavaScript" src="">{var i;}
          </script>
          <asp:button class="button" runat="server" id="Cancel" RMXRef="" Text="Cancel" width="100" onClientClick="window.close();" />
        </div>
      </div>
      
      <asp:TextBox style="display:none" runat="server" id="hdnDisableAll" RMXRef="/Instance/Document/DisableControls" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="hdnAction" RMXRef="" RMXType="hidden" Text="" /> 
      <asp:TextBox style="display:none" runat="server" id="hdnMessage" RMXRef="/Instance/Document/SaveMessage" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="hdnPolicyId" RMXRef="" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="hdnPolicyNumber" RMXRef="" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="hdnBillingItemRowId" RMXRef="/Instance/Document/BillingItemRowId" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="BillItemRowId" RMXRef="/Instance/Document/Receipts/BillItemRowId" RMXType="hidden" Text="" /> 
      <asp:TextBox style="display:none" runat="server" id="hdnDisableControls" RMXRef="/Instance/Document/DisableControls" RMXType="hidden" Text="" /> 
      <asp:TextBox style="display:none" runat="server" id="EntryBatchType_codeid" RMXRef="/Instance/Document/Receipts/EntryBatchType/@codeid" RMXType="hidden" Text="" /> 
      <asp:TextBox style="display:none" runat="server" id="FinalBatchType_codeid" RMXRef="/Instance/Document/Receipts/FinalBatchType/@codeid" RMXType="hidden" Text="" /> 
      <asp:TextBox style="display:none" runat="server" id="Status_codeid" RMXRef="/Instance/Document/Receipts/Status/@codeid" RMXType="hidden" Text="" /> 
      <asp:TextBox style="display:none" runat="server" name="formname" Text="frmData" />
      <asp:TextBox style="display:none" runat="server" name="SysRequired" id="SysRequired" Text="PaymentMethod|Amount|BankAccount|TransactionType|" />
      <asp:TextBox style="display:none" runat="server" name="SysFocusFields" Text="" />
      <input type="hidden" id="hdSaveButtonClicked" />
      <asp:TextBox runat="server" id="txtScreenFlowStack" style="display:none" />
      <input type="hidden" value="rmx-widget-handle-2" id="SysWindowId" />
      <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
  </body>
</html>
