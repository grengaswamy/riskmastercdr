﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;

namespace Riskmaster.UI.Billing
{
    public partial class DisbursementFrame : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XElement oMessageElement = null;
            XElement oSessionCmdElement = null;
            string sPolicyId = string.Empty;
            string sTermNumber = string.Empty;
            string sPolicyNumber = string.Empty;
            string sBillItemRowId = string.Empty;
            try
            {
                if (!IsPostBack)
                {
                    oMessageElement = GetMessageTemplate();

                    sPolicyId = AppHelper.GetQueryStringValue("PolicyId");
                    oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/Document/PolicyId");
                    oSessionCmdElement.Value = sPolicyId;
                    hdnPolicyId.Text = sPolicyId;

                    sTermNumber = AppHelper.GetQueryStringValue("TermNumber");
                    oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/Document/TermNumber");
                    oSessionCmdElement.Value = sTermNumber;

                    sPolicyNumber = AppHelper.GetQueryStringValue("PolicyNumber");
                    oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/Document/PolicyNumber");
                    oSessionCmdElement.Value = sPolicyNumber;
                    hdnPolicyNumber.Text = sPolicyNumber;

                    sBillItemRowId = AppHelper.GetQueryStringValue("selectedid");
                    oSessionCmdElement = oMessageElement.XPathSelectElement("./Document/Document/BillingItemRowId");
                    oSessionCmdElement.Value = sBillItemRowId;
                    hdnBillingItemRowId.Text = sBillItemRowId;

                    CallCWSFunction("BillingAdaptor.GetDisbursementData", oMessageElement);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        protected void Ok_Click(object sender, EventArgs e)
        {
            if (hdnAction.Text == "OK")
            {
                bool bReturnStatus = false;
                XElement oMessageElement = null;
                string sCWSresponse = "";
                hdnBillingItemRowId.Text = AppHelper.GetQueryStringValue("selectedid");
                oMessageElement = GetMessageTemplate();
                bReturnStatus = CallCWS("BillingAdaptor.SaveDisbursement", oMessageElement, out sCWSresponse,false,false);
                XmlDocument objReturnXml = new XmlDocument();
                objReturnXml.LoadXml(sCWSresponse);
                hdnMessage.Text = objReturnXml.SelectSingleNode("//SaveMessage").InnerText;
                hdnAction.Text = "";
            }
        }
        

        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>7ea36d97-abb4-4404-937a-3af09fd14e73</Authorization>");
            if (hdnAction.Text == "OK")
            {
                sXml = sXml.Append("<Call><Function>BillingAdaptor.SaveDisbursement</Function></Call><Document>");
            }
            else
            {
                sXml = sXml.Append("<Call><Function>BillingAdaptor.GetDisbursementData</Function></Call><Document>");
            }
            sXml = sXml.Append("<Document><PolicyId>");
            sXml = sXml.Append(hdnPolicyId.Text);
            sXml = sXml.Append("</PolicyId><TermNumber>");
            sXml = sXml.Append(TermNumber.Text);
            sXml = sXml.Append("</TermNumber><PolicyNumber>");
            sXml = sXml.Append(policyNumber.Text);
            sXml = sXml.Append("</PolicyNumber><BillingItemRowId>");
            sXml = sXml.Append(hdnBillingItemRowId.Text);
            sXml = sXml.Append("</BillingItemRowId>");
            if (hdnAction.Text == "OK")
            {
                string sReceipts = GetMessageString();
                sXml = sXml.Append("<Disbursements>");
                sXml = sXml.Append(sReceipts);
                sXml = sXml.Append("</Disbursements><SaveMessage>");
            }
            else
            {
                sXml = sXml.Append("<Disbursements></Disbursements><SaveMessage>");
            }
            sXml = sXml.Append(hdnMessage.Text);
            sXml = sXml.Append("</SaveMessage><DisableControls>");
            sXml = sXml.Append(hdnDisableControls.Text);
            sXml = sXml.Append("</DisableControls></Document>");
            sXml = sXml.Append("</Document></Message>");
            sXml = sXml.Replace("&", "&amp;");  //pmittal5 Mits 14848 05/19/09 
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;

        }



        private String GetMessageString()
        {
            string sAdj = "<Disbursements><PaymentMethod>";
            sAdj = sAdj + PaymentMethod.SelectedValue;
            sAdj = sAdj + "</PaymentMethod><CheckNumber>";
            sAdj = sAdj + CheckNumber.Text;
            sAdj = sAdj + "</CheckNumber><DatePrinted>";
            sAdj = sAdj + Date.Text;
            sAdj = sAdj + "</DatePrinted><PrintedFlag>";
            sAdj = sAdj + Printed.Text;
            sAdj = sAdj + "</PrintedFlag><PayeeName>";
            sAdj = sAdj + PayeeName.Text;
            sAdj = sAdj + "</PayeeName><Payee>";
            sAdj = sAdj + "</Payee><TransactionType>";
            sAdj = sAdj + TransactionType.SelectedValue;
            sAdj = sAdj + "</TransactionType><BankAccount>";
            sAdj = sAdj + BankAccount.SelectedValue;
            sAdj = sAdj + "</BankAccount><Amount>";
            sAdj = sAdj + Amount.AmountInString;
            sAdj = sAdj + "</Amount><BillItemRowId>";
            sAdj = sAdj + BillItemRowId.Text;
            sAdj = sAdj + "</BillItemRowId><PolicyName>";
            sAdj = sAdj + policyName.Text;
            sAdj = sAdj + "</PolicyName><PolicyNumber>";
            sAdj = sAdj + hdnPolicyNumber.Text;
            sAdj = sAdj + "</PolicyNumber><DateEntered>";
            sAdj = sAdj + DateEntered.Text;
            sAdj = sAdj + "</DateEntered><InvoiceNumber>";
            sAdj = sAdj + InvoiceNumber.Text;
            sAdj = sAdj + "</InvoiceNumber><NoticeNumber>";
            sAdj = sAdj + NoticeNumber.Text;
            sAdj = sAdj + "</NoticeNumber><DateReconciled>";
            sAdj = sAdj + DateReconciled.Text;
            sAdj = sAdj + "</DateReconciled><TermNumber>";
            sAdj = sAdj + TermNumber.Text;
            sAdj = sAdj + "</TermNumber><EntryBatchNumber>";
            sAdj = sAdj + EntryBatchNo.Text;
            sAdj = sAdj + "</EntryBatchNumber><EntryBatchType codeid=\"";
            if (EntryBatchType_codeid.Text != null)
                sAdj = sAdj + EntryBatchType_codeid.Text;
            sAdj = sAdj + "\">";
            sAdj = sAdj + EntryBatchType.Text;
            sAdj = sAdj + "</EntryBatchType><FinalBatchNumber>";
            sAdj = sAdj + FinalBatchNo.Text;
            sAdj = sAdj + "</FinalBatchNumber><FinalBatchType codeid=\"";
            if (FinalBatchType_codeid.Text != null)
                sAdj = sAdj + FinalBatchType_codeid.Text;
            sAdj = sAdj + "\">";
            sAdj = sAdj + FinalBatchType.Text;
            sAdj = sAdj + "</FinalBatchType><Status codeid=\"";
            if (Status_codeid.Text != null)
                sAdj = sAdj + Status_codeid.Text;
            sAdj = sAdj + "\">";
            sAdj = sAdj + Status.Text;
            sAdj = sAdj + "</Status><Comment>";
            sAdj = sAdj + Comments.Text;
            sAdj = sAdj + "</Comment><AccountList>";
            for (int i = 0; i < BankAccount.Items.Count; i++)
            {
                sAdj = sAdj + "<option value=\"";
                sAdj = sAdj + BankAccount.Items[i].Value;
                sAdj = sAdj + "\">";
                sAdj = sAdj + BankAccount.Items[i].Text;
                sAdj = sAdj + "</option>";
            }
            sAdj = sAdj + "</AccountList><CodeList>";
            for (int i = 0; i < PaymentMethod.Items.Count; i++)
            {
                sAdj = sAdj + "<option value=\"";
                sAdj = sAdj + PaymentMethod.Items[i].Value;
                sAdj = sAdj + "\">";
                sAdj = sAdj + PaymentMethod.Items[i].Text;
                sAdj = sAdj + "</option>";
            }
            sAdj = sAdj + "</CodeList><TransTypeCodes>";
            for (int i = 0; i < TransactionType.Items.Count; i++)
            {
                sAdj = sAdj + "<option value=\"";
                sAdj = sAdj + TransactionType.Items[i].Value;
                sAdj = sAdj + "\">";
                sAdj = sAdj + TransactionType.Items[i].Text;
                sAdj = sAdj + "</option>";
            }
            sAdj = sAdj + "</TransTypeCodes></Disbursements>";
            return sAdj;   
        }
    }
}
