﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.Linq;
using System.Text;
using System.IO;
using System.Xml;

namespace Riskmaster.UI.UI.Billing
{
    public partial class BatchReconciliationDetails : NonFDMBasePageCWS 
    {

        protected void Page_Load(object sender, EventArgs e)
        {            
            XElement XmlTemplate = null;
            bool bReturnStatus = false;
            string sCWSresponse = "";
            XmlDocument objXmlDocument = null;            
            string sBatchTotal = "";
            string sSelectedBatchId = "";

            try
            {
                sSelectedBatchId = AppHelper.GetQueryStringValue("iBatchId");
                sBatchTotal = AppHelper.GetQueryStringValue("iBatchTotal");


                XmlTemplate = GetMessageTemplate(sSelectedBatchId, sBatchTotal);

                bReturnStatus = CallCWS("BillingAdaptor.GetBatchReconciliationDetails", XmlTemplate, out sCWSresponse, false , true);

                objXmlDocument = new XmlDocument();
                objXmlDocument.LoadXml(sCWSresponse);



                ErrorControl1.errorDom = sCWSresponse;

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }





        private XElement GetMessageTemplate(string sSelectedBatchId, string sBatchTotal)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>");

            sXml = sXml.Append("<Document>");
            sXml = sXml.Append("<BatchId>" + sSelectedBatchId + "</BatchId>");
            sXml = sXml.Append("<ReconciliationType/>");
            sXml = sXml.Append("<BatchTotal >" + sBatchTotal + "</BatchTotal >");
            sXml = sXml.Append("<CheckPaidInFull />");
            sXml = sXml.Append("<Message />");
            sXml = sXml.Append("</Document>");
            sXml = sXml.Append("</Document></Message>");


            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }             

        
    }
}
