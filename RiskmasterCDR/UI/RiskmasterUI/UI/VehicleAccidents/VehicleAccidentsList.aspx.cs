﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.VehicleAccidents
{
    public partial class VehicleAccidentsList : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                bool bReturnStatus = false;
                XElement XmlTemplate = null;
                string sreturnValue = "";
                DataTable objTable = new DataTable();
                DataRow objRow;
                XmlNode xNode;
               
                if (!IsPostBack)
                {
                    //srajindersin - Pentesting - 27682
                    //UnitID.Value = Request.QueryString["unitid"];
                    //Vin.Value = Request.QueryString["vin"];
                    if (!string.IsNullOrEmpty(Request.QueryString["unitid"]))
                        UnitID.Value = AppHelper.HTMLCustomEncode(Request.QueryString["unitid"]);

                    if (!string.IsNullOrEmpty(Request.QueryString["vin"]))
                        Vin.Value = AppHelper.HTMLCustomEncode(Request.QueryString["vin"]);
                    //END srajindersin - Pentesting - 27682

                    if (!string.IsNullOrEmpty(Request.QueryString["OpenedFromPolicy"]))
                        hdnHasOpener.Value = AppHelper.HTMLCustomEncode(Request.QueryString["OpenedFromPolicy"]);
                    //END srajindersin - Pentesting - 27682

                    
                    
                    XmlTemplate = GetMessageTemplate(UnitID.Value);
                    bReturnStatus = CallCWS("VehicleAccidentsAdaptor.GetAccidentsList", XmlTemplate, out sreturnValue, false, false);
                    if (bReturnStatus)
                    {
                        XmlDocument XmlDoc = new XmlDocument();
                        XmlDoc.LoadXml(sreturnValue);
                        XmlNodeList AccList = XmlDoc.SelectNodes("//AccidentsList/Record");
                        objTable.Columns.Add("ClaimId");
                        objTable.Columns.Add("ClaimNumber");
                        objTable.Columns.Add("AccidentDate");
                        for (int i = 0; i < AccList.Count; i++)
                        {
                            xNode = AccList[i];
                            objRow = objTable.NewRow();

                            foreach (XmlNode childnode in xNode.ChildNodes)
                            {
                                switch (childnode.Name)
                                {
                                    case "ClaimNumber":
                                        //srajindersin - Pentesting - 27682
                                        //objRow["ClaimNumber"] = childnode.InnerText;
                                        objRow["ClaimNumber"] = AppHelper.HTMLCustomEncode(childnode.InnerText);
                                        //END srajindersin - Pentesting - 27682
                                        break;
                                    case "AccidentDate":
                                        //srajindersin - Pentesting - 27682
                                        //objRow["AccidentDate"] = childnode.InnerText;
                                        objRow["AccidentDate"] = AppHelper.HTMLCustomEncode(childnode.InnerText);
                                        //END srajindersin - Pentesting - 27682
                                        break;
                                    case "ClaimId":
                                        //srajindersin - Pentesting - 27682
                                        //objRow["ClaimId"] = childnode.InnerText;
                                        objRow["ClaimId"] = AppHelper.HTMLCustomEncode(childnode.InnerText);
                                        //END srajindersin - Pentesting - 27682
                                        break;

                                }
                            }
                            objTable.Rows.Add(objRow);

                        }
                        objTable.AcceptChanges();
                        GridViewVehAcc.Visible = true;
                        GridViewVehAcc.DataSource = objTable;
                        GridViewVehAcc.DataBind();
                    }


                }

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }
        private XElement GetMessageTemplate(string sUnitId)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><AccidentsList><Parameters><UnitId>");
            sXml = sXml.Append(sUnitId);
            sXml = sXml.Append("</UnitId>");
            sXml = sXml.Append("<LangCode>"+ AppHelper.GetLanguageCode() +"</LangCode>");
            sXml = sXml.Append("</Parameters></AccidentsList></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
        protected void GridViewVehAcc_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HyperLink rowHyperLink = null;
                rowHyperLink = new HyperLink();

                String sClaimID = "";
                String sClaimNumber = "";
                if (DataBinder.Eval(e.Row.DataItem, "ClaimId") == null)
                {
                    sClaimID = "";
                }
                else
                {
                    sClaimID = DataBinder.Eval(e.Row.DataItem, "ClaimId").ToString();
                }
                if (DataBinder.Eval(e.Row.DataItem, "ClaimNumber") == null)
                {
                    sClaimNumber = "";
                }
                else
                {
                    sClaimNumber = DataBinder.Eval(e.Row.DataItem, "ClaimNumber").ToString();
                }

                rowHyperLink.NavigateUrl = "#";
                if(string.Equals(hdnHasOpener.Value,"true",StringComparison.InvariantCultureIgnoreCase))
                rowHyperLink.Attributes.Add("onClick", "window.opener.parent.MDIShowScreen(" + sClaimID + ", 'claim');return false;");  //MITS 27112-Ritesh
                else
                    rowHyperLink.Attributes.Add("onClick", "parent.MDIShowScreen(" + sClaimID + ", 'claim');return false;");  //MITS 27112-Ritesh
                rowHyperLink.Text = sClaimNumber;
                rowHyperLink.CssClass = "HeaderNavy";
                e.Row.Cells[1].Controls.Add(rowHyperLink);
                e.Row.Cells[1].CssClass = "Bold2";
            }
        }
    }
}
