﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="VehicleAccidentsList.aspx.cs" Inherits="Riskmaster.UI.VehicleAccidents.VehicleAccidentsList" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
        <uc4:CommonTasks ID="CommonTasks1" runat="server" />
</head>
<body class="10pt" id="body1">
    <form id="form1" runat="server">
        <asp:HiddenField ID ="UnitID" runat ="server" />
        <asp:HiddenField ID ="Vin" runat ="server" />
        <table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tr>
                <td colspan="2">
                  <uc3:ErrorControl ID="ErrorControl1" runat="server" />
                </td>
             </tr>
           <tr>
              <td class="ctrlgroup2" colspan="2"><asp:Label ID="lblGroupText" runat="server" Text="<%$ Resources:lblGroupText %>"></asp:Label> <%=Vin.Value%></td>
           </tr>
           <tr>
           <td><br /></td>
           </tr>
           <tr class="ctrlgroup">
		        <asp:GridView ID="GridViewVehAcc" AutoGenerateColumns="False" runat="server" Font-Bold="True" 
		         CellPadding="0" GridLines="None" CellSpacing="0" Width="100%" OnRowDataBound="GridViewVehAcc_RowDataBound"  EmptyDataText ="<%$ Resources:lblEmpty %>" >
		         <HeaderStyle CssClass="ctrlgroup"/>
		         <rowstyle CssClass ="rowlight1" HorizontalAlign="Left" Font-Bold="false"  />
                 <alternatingrowstyle CssClass="rowdark1" HorizontalAlign="Left" Font-Bold="false" />
	                 <Columns>
                        <asp:TemplateField SortExpression="1" HeaderText="<%$ Resources:gvhdrAccDate %>" HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="msgheader" >
                            <ItemTemplate>
                                <%# Eval("AccidentDate")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField SortExpression="2" HeaderText="<%$ Resources:gvhdrClaimNum %>" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
		        </asp:GridView> 
		   </tr>
		   </table> 
		   <br />
		   <br />
            <asp:Button ID="btnBack" Text="<%$ Resources:btnBack %>" class="button" OnClientClick="javascript: history.back(); return false;" runat="server" />
    <div>
    <asp:HiddenField id="hdnHasOpener"  value="" runat="server"/>
    </div>
    </form>
</body>
</html>
