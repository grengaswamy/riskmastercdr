﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;
using Riskmaster.UI.Shared.Controls;

namespace Riskmaster.UI.AltWaitPeriods
{
    public partial class AltWaitWindow : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Read the Values from Querystring
                string sClassId = AppHelper.GetQueryStringValue("classid");
                string sWaitRowId = AppHelper.GetQueryStringValue("DisWaitRowId");

                hdnclassid.Text = sClassId;
                // Get Values for Edit from Database for the first time only
                if (!IsPostBack)
                {
                    if (sClassId != "" && sWaitRowId != "")
                    {
                        bool bReturnStatus = false;
                        XElement XmlTemplate = null;
                        string sReturnValue = "";

                        XmlTemplate = GetEditMessageTemplate(sClassId, sWaitRowId);

                        bReturnStatus = CallCWS("AltWaitPeriodsAdaptor.GetRecordAltWait", XmlTemplate, out sReturnValue, false, true);

                        if (!bReturnStatus)
                        {
                            ecErrorControl.errorDom = sReturnValue;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ecErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                bool bReturnStatus = false;
                XElement XmlTemplate = null;
                string sReturnValue = "";

                XmlTemplate = GetSaveMessageTemplate();

                bReturnStatus = CallCWS("AltWaitPeriodsAdaptor.Save", XmlTemplate, out sReturnValue, false, false);

                if (bReturnStatus)
                {
                    string script = "<script>RefreshParent();</script>";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "myscript", script);
                }
                else
                {
                    ecErrorControl.errorDom = sReturnValue;
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ecErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            btnOk_Click(sender, e);
        }

        private XElement GetSaveMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message> ");
            sXml = sXml.Append("<Authorization>409b8705-c38d-4216-adfe-c1f6397196dd</Authorization> ");
            sXml = sXml.Append("<Call><Function>AltWaitPeriodsAdaptor.Save</Function></Call><Document><Document><form name='AltWaitWindow'> ");
            sXml = sXml.Append("<group name='Select'><displaycolumn> ");
            sXml = sXml.Append("<control name='DisWaitRowId' ref='/Instance/Document//control[@name=\"DisWaitRowId\"]' type='id'> ");
            sXml = sXml.Append(DisWaitRowId.Text);
            sXml = sXml.Append("</control> ");
            sXml = sXml.Append("<control firstfield='1' maxlength='50' name='DisWaitPrd' ref='/Instance/Document//control[@name=\"DisWaitPrd\"]' required='yes' title='Quantity' type='text'> ");
            sXml = sXml.Append(DisWaitPrd.Text);
            sXml = sXml.Append("</control></displaycolumn><displaycolumn> ");
            sXml = sXml.Append("<control codeid= '");

            // Get the Code Id for Calendar Work
            sXml = sXml.Append(DisClndrWrkCode.CodeIdValue);
            sXml = sXml.Append("' codetable='CALENDAR_WORK' name='DisClndrWrkCode' ref='/Instance/Document//control[@name=\"DisClndrWrkCode\"]' required='yes' title='Period' type='code'> ");
            sXml = sXml.Append(DisClndrWrkCode.CodeTextValue);
            sXml = sXml.Append(" </control> ");
            sXml = sXml.Append("<control codeid='");
            sXml = sXml.Append(DisPrdType.CodeIdValue);
            sXml = sXml.Append("' codetable='DURATION_TYPE' name='DisPrdType' ref='/Instance/Document//control[@name=\"DisPrdType\"]' required='yes' title='' type='code'> ");
            sXml = sXml.Append(DisPrdType.CodeTextValue);
            sXml = sXml.Append(" </control></displaycolumn><displaycolumn> ");
            sXml = sXml.Append(" <control codeid= '");
            sXml = sXml.Append(DisTypeCode.CodeIdValue);
            sXml = sXml.Append("' codetable='BENEFIT_DIS_TYPE' name='DisTypeCode' ref='/Instance/Document//control[@name=\"DisTypeCode\"]' title='Disability Type' type='code'> ");
            sXml = sXml.Append(DisTypeCode.CodeTextValue);
            sXml = sXml.Append(" </control> ");
            sXml = sXml.Append("<control name='hdnclassid' ref='/Instance/Document//control[@name=\"hdnclassid\"]' type='hidden'>");
            sXml = sXml.Append(hdnclassid.Text);
            sXml = sXml.Append("</control> ");
            sXml = sXml.Append("</displaycolumn></group> ");
            sXml = sXml.Append("<internal name='sys_formidname' type='hidden' value='DisWaitRowId' /> ");
            sXml = sXml.Append("<internal name='sys_formpidname' type='hidden' value='DisWaitRowId' />  ");
            sXml = sXml.Append("<internal name='sys_formpform' type='hidden' value='AltWaitWindow' /> ");
            sXml = sXml.Append("</form></Document></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        private XElement GetEditMessageTemplate(string sClassId, string sWaitRowId)
        {
            StringBuilder sXml = new StringBuilder("<Message> ");
            sXml = sXml.Append("<Authorization>409b8705-c38d-4216-adfe-c1f6397196dd</Authorization> ");
            sXml = sXml.Append("<Call><Function>AltWaitPeriodsAdaptor.GetRecordAltWait</Function></Call><Document><Document><form name='AltWaitWindow'> ");
            sXml = sXml.Append("<group name='Select'><displaycolumn> ");
            sXml = sXml.Append("<control name='DisWaitRowId' ref='/Instance/Document//control[@name=\"DisWaitRowId\"]' type='id'> ");
            sXml = sXml.Append(sWaitRowId);
            sXml = sXml.Append("</control> ");
            sXml = sXml.Append("<control firstfield='1' maxlength='50' name='DisWaitPrd' ref='/Instance/Document//control[@name=\"DisWaitPrd\"]' required='yes' title='Quantity' type='text'> ");
            sXml = sXml.Append("</control></displaycolumn><displaycolumn> ");
            sXml = sXml.Append("<control codeid= '");
            sXml = sXml.Append("' codetable='CALENDAR_WORK' name='DisClndrWrkCode' ref='/Instance/Document//control[@name=\"DisClndrWrkCode\"]' required='yes' title='Period' type='code'> ");
            sXml = sXml.Append(" </control> ");
            sXml = sXml.Append("<control codeid='");
            sXml = sXml.Append("' codetable='DURATION_TYPE' name='DisPrdType' ref='/Instance/Document//control[@name=\"DisPrdType\"]' required='yes' title='' type='code'> ");
            sXml = sXml.Append(" </control></displaycolumn><displaycolumn> ");
            sXml = sXml.Append(" <control codeid= '");
            sXml = sXml.Append("' codetable='BENEFIT_DIS_TYPE' name='DisTypeCode' ref='/Instance/Document//control[@name=\"DisTypeCode\"]' title='Disability Type' type='code'> ");
            sXml = sXml.Append(" </control> ");
            sXml = sXml.Append("<control name='hdnclassid' ref='/Instance/Document//control[@name=\"hdnclassid\"]' type='hidden'>");
            sXml = sXml.Append(sClassId);
            sXml = sXml.Append("</control> ");
            sXml = sXml.Append("</displaycolumn></group> ");
            sXml = sXml.Append("<internal name='sys_formidname' type='hidden' value='DisWaitRowId' /> ");
            sXml = sXml.Append("<internal name='sys_formpidname' type='hidden' value='DisWaitRowId' />  ");
            sXml = sXml.Append("<internal name='sys_formpform' type='hidden' value='AltWaitWindow' /> ");
            sXml = sXml.Append("</form></Document></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
    }
}
