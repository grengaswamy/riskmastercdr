﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.RMXResourceManager;
using Infragistics.WebUI.WebHtmlEditor;
using System.Web.UI.WebControls.WebParts;
using System.Web;
using System.IO; //rkulavil:MITS 37260 

namespace Riskmaster.UI.Comments
{
    /// <summary>
    /// Author: Raman Bhatia
    /// </summary>
    public partial class MainPage : NonFDMBasePageCWS
    {
        XmlDocument oFDMPageDom = null;
        private string PageID = RMXResourceManager.RMXResourceProvider.PageId("MainPage.aspx"); // jira 732 ,ksahu5

        protected void Page_Load(object sender, EventArgs e)
        {
            oFDMPageDom = new XmlDocument();
            this.WebHtmlEditor1.ToolbarClick += new Infragistics.WebUI.WebHtmlEditor.ToolbarClickEventHandler(WebHtmlEditor1_ToolbarClick);
            //Support for adding images in notes..TO DO: discuss deployment and make changes accordingly
            //this.WebHtmlEditor1.UploadedFilesDirectory = "../../Images/ProgressNotesImages";

            //hlv 34136 begin
            string sCulture = AppHelper.GetCulture().ToString();
            string sPath = string.Empty; //rkulavil:MITS 37260 
            if (sCulture != "en-US")
            {
			/*rkulavil:MITS 37260  starts*/
			//this.WebHtmlEditor1.LocalizationFile = Server.MapPath("../../Scripts/Infragistics/localization-" + sCulture + ".xml");
                sPath=Server.MapPath("../../Scripts/Infragistics/localization-" + sCulture + ".xml");
                if (File.Exists(sPath))
                    this.WebHtmlEditor1.LocalizationFile = sPath;
                else
                    this.WebHtmlEditor1.LocalizationFile = Server.MapPath("../../Scripts/Infragistics/localization.xml");

             /*rkulavil:MITS 37260  ends*/
            }

            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("MainPage.aspx"), "MainPageValidations",
                ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "MainPageValidations", sValidationResources, true);
            //hlv 34136 end

            this.WebSpellChecker1.DialogOptions.ShowFinishedMessage = true;
            this.WebSpellChecker1.UserDictionaryFile = Server.MapPath("../../App_Data/C1SP_AE.dct");
            this.WebSpellChecker1.SpellOptions.IncludeUserDictionaryInSuggestions = true;

            if (this.WebHtmlEditor1.Toolbar.Items.Contains(ToolbarItemType.SpellCheck))
            {
                this.WebHtmlEditor1.Toolbar.Items.Remove(ToolbarItemType.SpellCheck);
            }
            if (this.WebHtmlEditor1.Toolbar.Items.Contains(ToolbarItemType.Print))
            {
                this.WebHtmlEditor1.Toolbar.Items.Remove(ToolbarItemType.Print);
            } // if

            this.WebHtmlEditor1.AddToolbarItem(ToolbarItemType.SpellCheck);
            this.WebHtmlEditor1.AddToolbarItem(ToolbarItemType.Print);
            if (!Page.IsPostBack)
            {
                //Firstly parse the querystring and fill hidden variables accordingly
                hdCommentsFlag.Text = AppHelper.GetQueryStringValue("CommentsFlag");
                hdFormName.Text = AppHelper.GetQueryStringValue("SysFormName");
                hdRecordId.Text = AppHelper.GetQueryStringValue("recordid");
                //string sPsid = AppHelper.GetQueryStringValue("psid");
                //string sLoaded = AppHelper.GetQueryStringValue("loaded");
                hdFieldName.Text = AppHelper.GetQueryStringValue("fieldname");
                hdFieldType.Text = AppHelper.GetQueryStringValue("FieldType");//asharma326 JIRA 6422
                //Added:Yukti, Dt:07/17/2014, MITS 34136
                hdSubtitle.Text = AppHelper.GetQueryStringValue("formsubtitle");
                //Ended:Yukti, Dt:07/17/2014, MITS 34136
                hdOpenerPlainMDIId.Text = AppHelper.GetQueryStringValue("openerPlainMDIId");
                hdUseLegacyComments.Text = AppHelper.GetQueryStringValue("useLegacyComments");
                hdRecentAction.Text = "Load";

                InitializeEditor();
                if (oFDMPageDom.SelectSingleNode("//Editor/CommentId") != null)
                  hdCommentId.Text = oFDMPageDom.SelectSingleNode("//Editor/CommentId").InnerText;
                //if (hdFormName.Text == "LookUpClaimPolicy" || hdFormName.Text == "SupervisorApproval") //csingh7 for MITS 20092
                if (hdFormName.Text == "LookUpClaimPolicy")//saurabh singh MITS 26643 Date 11/17/2011 
                    WebHtmlEditor1.Enabled = false;
            }  
        }

        /// <summary>
        /// Handles the click events of toolbar button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void WebHtmlEditor1_ToolbarClick(object sender, ToolbarEventArgs e)
        {
            string sReturn = "";
            if (e.Item.ToString() == ToolbarItemType.Save.ToString())
            {
                //SAVE THE DATA

                //Preparing XML to send to service
                XElement oMessageElement = GetSaveCommentsTemplate();

                //Modify XML 
                XElement oEle = oMessageElement.XPathSelectElement("./Document/Editor/FormName");
                if (oEle != null)
                {
                    oEle.Value = hdFormName.Text;
                }
                oEle = oMessageElement.XPathSelectElement("./Document/Editor/RecordId");
                if (oEle != null)
                {
                    oEle.Value = hdRecordId.Text;
                }
                oEle = oMessageElement.XPathSelectElement("./Document/Editor/ExistingComments/Comments");
                if (oEle != null)
                {
                    oEle.Value = hdComments.Text;
                }
                oEle = oMessageElement.XPathSelectElement("./Document/Editor/ExistingComments/HTMLComments");
                if (oEle != null)
                {
                    oEle.Value = hdHTMLComments.Text;
                }
                oEle = oMessageElement.XPathSelectElement("./Document/Editor/PlainText");
                if (oEle != null)
                {
                    //There is bug in infrigistic if douple space is there then it treat one &nbsp; and other as space
                    oEle.Value = this.WebHtmlEditor1.TextPlain.Replace("&nbsp;"," ");
                }
                oEle = oMessageElement.XPathSelectElement("./Document/Editor/EditorTextHtml");
                if (oEle != null)
                {
                    oEle.Value = this.WebHtmlEditor1.Text;
                }
                oEle = oMessageElement.XPathSelectElement("./Document/Editor/DateTimeStamp");
                if (oEle != null)
                {
                    oEle.Value = txtDtmStamp.Text;
                }
                oEle = oMessageElement.XPathSelectElement("./Document/Editor/CommentId");
                if (oEle != null)
                {
                    oEle.Value = hdCommentId.Text;
                }
                //Raman MITS 19155 : Comments added by one user overwrite other users changes if both users had open the same comments window
                oEle = oMessageElement.XPathSelectElement("./Document/Editor/DttmRcdLastUpd");
                if (oEle != null)
                {
                    oEle.Value = hdDttmRcdLastUpd.Text;
                }
                //Calling Service to get all PreBinded Data 
                sReturn = AppHelper.CallCWSService(oMessageElement.ToString());

                //Binding Error Control
                ErrorControl.errorDom = sReturn;

                //Raman MITS 19155 : Comments added by one user overwrite other users changes if both users had open the same comments window
                if (!ErrorControl.errorFlag)
                {
                    hdDataSaved.Text = "true";

                    InitializeEditor();
                }
            }
        }

        //Mits: 18496
        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            string sReturn = "";
            XElement oMessageElement = GetSaveCommentsTemplate();

            //Modify XML 
            XElement oEle = oMessageElement.XPathSelectElement("./Document/Editor/FormName");
            if (oEle != null)
            {
                oEle.Value = hdFormName.Text;
            }
            oEle = oMessageElement.XPathSelectElement("./Document/Editor/RecordId");
            if (oEle != null)
            {
                oEle.Value = hdRecordId.Text;
            }
            oEle = oMessageElement.XPathSelectElement("./Document/Editor/ExistingComments/Comments");
            if (oEle != null)
            {
                oEle.Value = hdComments.Text;
            }
            oEle = oMessageElement.XPathSelectElement("./Document/Editor/ExistingComments/HTMLComments");
            if (oEle != null)
            {
                oEle.Value = hdHTMLComments.Text;
            }
            oEle = oMessageElement.XPathSelectElement("./Document/Editor/PlainText");
            if (oEle != null)
            {
                //There is bug in infrigistic if douple space is there then it treat one &nbsp; and other as space
                oEle.Value = this.WebHtmlEditor1.TextPlain.Replace("&nbsp;", " ");
            }
            oEle = oMessageElement.XPathSelectElement("./Document/Editor/EditorTextHtml");
            if (oEle != null)
            {
                oEle.Value = this.WebHtmlEditor1.Text;
            }
            oEle = oMessageElement.XPathSelectElement("./Document/Editor/DateTimeStamp");
            if (oEle != null)
            {
                oEle.Value = txtDtmStamp.Text;
            }
            oEle = oMessageElement.XPathSelectElement("./Document/Editor/CommentId");
            if (oEle != null)
            {
                oEle.Value = hdCommentId.Text;
            }
            //Raman MITS 19155 : Comments added by one user overwrite other users changes if both users had open the same comments window
            oEle = oMessageElement.XPathSelectElement("./Document/Editor/DttmRcdLastUpd");
            if (oEle != null)
            {
                oEle.Value = hdDttmRcdLastUpd.Text;
            }
            //Calling Service to get all PreBinded Data 
            sReturn = AppHelper.CallCWSService(oMessageElement.ToString());

            //Binding Error Control
            ErrorControl.errorDom = sReturn;

            //Raman MITS 19155 : Comments added by one user overwrite other users changes if both users had open the same comments window
            if (!ErrorControl.errorFlag)
            {
                hdDataSaved.Text = "true";

                InitializeEditor();
                //aaggarwal29: Merge start bgupta22 MITS 35310 start
                string str = "<script>self.close();</script>"; //window.opener.document.forms[0].txtSubmit.value = \"SUBMIT\";window.opener.document.forms[0].submit();//Parijat: EnhancedMDI
                RegisterClientScriptBlock("", str);
                //aaggarwal29: Merge end bgupta22 MITS 35310 end
            }
        }
        /// <summary>
        /// CWS request message template for GetExistingComments
        /// </summary>
        /// <returns></returns>
        private XElement GetExistingCommentsMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                 <Message>
                    <Authorization>884a6370-ea57-4714-b954-010d86051af6</Authorization> 
                    <Call>
                         <Function>EditorAdaptor.GetExistingComments</Function> 
                    </Call>
                    <Document>
                         <Editor>
                            <FormName></FormName> 
                            <RecordId></RecordId> 
                            <CommentId></CommentId>
                         </Editor>
                    </Document>
                  </Message>");
            return oTemplate;
        }

        /// <summary>
        /// CWS request message template for GetMemoAttributes
        /// </summary>
        /// <returns></returns>
        private XElement GetMemoAttributesMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                 <Message>
                     <Authorization>884a6370-ea57-4714-b954-010d86051af6</Authorization> 
                     <Call>
                          <Function>EditorAdaptor.GetMemoAttributes</Function> 
                      </Call>
                     <Document>
                          <Editor /> 
                     </Document>
                 </Message>");
            return oTemplate;
        }

        /// <summary>
        /// CWS request for save message template
        /// </summary>
        /// <returns></returns>
        private XElement GetSaveCommentsTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                 <Message>
                      <Authorization>9bb7435e-f812-4b55-b2ce-23646444f6aa</Authorization> 
                     <Call>
                      <Function>EditorAdaptor.SaveComments</Function> 
                      </Call>
                     <Document>
                        <Editor>
                          <CommentsFlag>true</CommentsFlag> 
                          <FormName></FormName> 
                          <RecordId></RecordId> 
                          <CommentId></CommentId> 
                          <UserName></UserName> 
                          <DateTimeStamp></DateTimeStamp>
                          <ExistingComments>
                            <Comments></Comments> 
                            <HTMLComments></HTMLComments> 
                          </ExistingComments>
                          <PlainText></PlainText> 
                          <EditorTextHtml></EditorTextHtml> 
                          <DttmRcdLastUpd></DttmRcdLastUpd>
                          <Moved /> 
                        </Editor>
                      </Document>
                 </Message>");
            return oTemplate;
        }

        private void InitializeEditor()
        {
            bool bReturnStatus = false;
            TextBox hdFreezeContentFlag = null;
            XElement oMessageElement = null;

            //Now call appropriate webservice based on Comments Flag
            if (hdCommentsFlag.Text.ToLower() == "true")
            {
                //Preparing XML to send to service
                oMessageElement = GetExistingCommentsMessageTemplate();

                //Modify XML 
                XElement oFormName = oMessageElement.XPathSelectElement("./Document/Editor/FormName");
                if (oFormName != null)
                {
                    oFormName.Value = hdFormName.Text;
                }

                XElement oRecordId = oMessageElement.XPathSelectElement("./Document/Editor/RecordId");
                if (oRecordId != null)
                {
                    oRecordId.Value = hdRecordId.Text;
                }
                XElement oCommentId = oMessageElement.XPathSelectElement("./Document/Editor/CommentId");
                if (oCommentId != null)
                {
                    oCommentId.Value = hdCommentId.Text;
                }

                //Call Service and Bind Data to control
                bReturnStatus = CallCWSFunction("EditorAdaptor.GetExistingComments", oMessageElement);
                oFDMPageDom = Data;
                if (oFDMPageDom.SelectSingleNode("//Editor/CommentId") != null)
                    hdCommentId.Text = oFDMPageDom.SelectSingleNode("//Editor/CommentId").InnerText;
            }
            else
            {
                //Preparing XML to send to service
                oMessageElement = GetMemoAttributesMessageTemplate();

                //Call Service and Bind Data to control
                bReturnStatus = CallCWSFunction("EditorAdaptor.GetMemoAttributes", oMessageElement);
                oFDMPageDom = Data;
            }

            //Updating dynamic table
            //Added:Yukti, Dt:07/17/2014, MITS 34136
            //if (AppHelper.GetValue("formsubtitle") != String.Empty)
            if (HttpUtility.ParseQueryString(Request.RawUrl).Get("commentsTitle") != String.Empty)
            {
                //XmlNodeList oNodeList = oFDMPageDom.SelectNodes("//Editor/Comments");
                //foreach (XmlNode currentNode in oNodeList)
                //{
                //Added:Yukti, Dt:07/17/2014, MITS :34136
                string sCommTitle = string.Empty;
                string sFormSubTitle = string.Empty;
                    TableRow tr = new TableRow();
                    tr.Attributes["bgcolor"] = "WhiteSmoke"; //currentNode.Attributes["bgcolor"].Value;
                    //Added:Yukti, Dt:07/17/2014, MITS 34136
                    sCommTitle = HttpUtility.ParseQueryString(Request.RawUrl).Get("commentsTitle");
                    sFormSubTitle = sCommTitle + " " + hdSubtitle.Text;
                    //Ended:Yukti, Dt:07/17/2014, MITS :34136
                    TableCell tc = new TableCell();
                    tc.ID = "Caption";
                    tc.Attributes["align"] = "center";
                    tc.Text = "<b>"; //+ currentNode.Attributes["Header"].Value;
                    //if (currentNode.Attributes["HeaderText"].Value != string.Empty)
                    //{
                    //Added:Yukti, Dt:07/17/2014, MITS :34136
                    //tc.Text += AppHelper.GetValue("formsubtitle").Replace("[ampersand]", "&").Replace("[plus]", "+"); //currentNode.Attributes["HeaderText"].Value; MITS 29585 hlv 9/11/12
                    //this.hdHeader.Text = AppHelper.GetValue("formsubtitle").Replace("[ampersand]", "&").Replace("[plus]", "+"); //MITS 29585 hlv 9/11/12
                    tc.Text += sFormSubTitle.Replace("[ampersand]", "&").Replace("[plus]", "+"); //currentNode.Attributes["HeaderText"].Value; MITS 29585 hlv 9/11/12
                    this.hdHeader.Text = sFormSubTitle.Replace("[ampersand]", "&").Replace("[plus]", "+"); //MITS 29585 hlv 9/11/12
                        //this.Title = AppHelper.GetValue("formsubtitle");
                    //}
                    tc.Text += "</b>";
                    tr.Cells.Add(tc);
                    tblDynamic.Rows.Add(tr);
                //}
            }
            else
            {
                tblDynamic.Enabled = false;
            }

            this.Title = this.hdHeader.Text;

            //Updating Existing Comments Header
            if (hdCommentsFlag.Text.ToLower() != "true")
            {
                if (hdFieldName.Text.ToLower().EndsWith("eventdescription"))
                {
                    tdExtComments1.InnerText = "Existing Event Description:";

                    hdFreezeContentFlag = hdFreezeEventDesc;
                }
                else
                {
                    tdExtComments1.InnerText = "Existing Event Location Description:";

                    hdFreezeContentFlag = hdFreezeEventLocDesc;
                }
            }
            else
            {
               // tdExtComments1.InnerText = "Existing Comments:";jira 732 ksahu5
                tdExtComments1.InnerText = AppHelper.GetResourceValue(this.PageID, "lblExistingComments", "0");//jira 732 ksahu5

                hdFreezeContentFlag = hdFreezeTextFlag;
            }

            if (hdFreezeContentFlag.Text.ToLower() == "true")
            {
                WebHtmlEditor1.Height = Unit.Percentage(60);
            }
            else
            {
                WebHtmlEditor1.Height = Unit.Percentage(85);
            }
        }
    }
}
