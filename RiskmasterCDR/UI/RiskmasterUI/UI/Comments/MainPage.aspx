﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MainPage.aspx.cs" Inherits="Riskmaster.UI.Comments.MainPage" ValidateRequest="false" %>

<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc2" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" TagPrefix="uc" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Riskmaster Editor</title>
    <link href= "../../Content/rmnet.css" rel="stylesheet" type="text/css" />
    <link href="../../App_Themes/RMX_Default/rmnet.css" rel="stylesheet" type="text/css" /><%-- Mits:32492 --%>
    <script language="javascript" type="text/javascript" src="../../Scripts/form.js"></script>
    <script language="JavaScript" type="text/javascript" src="../../Scripts/WaitDialog.js">{var i;}</script>
    <script type="text/javascript" id="igClientScript">
        //MITS 27964 hlv 4/10/2012 begin
        if (window.opener != null) {
            window.opener.window.parent.parent.iwintype = document.title;
        }

        window.onunload = function () {
            if (window.opener != null) {
                window.opener.window.parent.parent.iwintype = "";
            }
        }
        //MITS 27964 hlv 4/10/2012 end
        //Parijat : Mits 18496
        function SaveAndClose() {
            var oEditor = iged_getById("WebHtmlEditor1");
            m_IsSaved = true;
            pleaseWait1(); //Parijat: 16416
            document.getElementById('hdTextHtml').value = oEditor.getText();
            //Save the Comments as Plain Text
            document.getElementById('hdPlainText').value = DecodeFckEditor(document.getElementById('hdTextHtml').value);//msampathkuma RMA-10685
            //MITS 16416
            //		window.opener.setDataChanged(true); csingh7 r6 claim comment enhancement

            if (m_bCommentsFlag == 'false')	//For Event/Loc Desc
            {
                self.parent.opener.document.OnEditMemoSave();
                self.close();
                return false;
            }
            else 
            {
                return true;
            }            
        }
        

        function WebHtmlEditor1_BeforeAction(oEditor, actID, oEvent, p4, p5, p6, p7, p8, act) {
            //document.getElementById('hdRecentAction').value = actID; Commented by Manika
            if (actID == 'Save') {
                document.getElementById('hdRecentAction').value = actID; //Added by Manika
                m_IsSaved = true;
                pleaseWait1(); //Parijat: 16416
                document.getElementById('hdTextHtml').value = oEditor.getText();
                //Save the Comments as Plain Text
                document.getElementById('hdPlainText').value = DecodeFckEditor(document.getElementById('hdTextHtml').value);

                //MITS 16416
                //		window.opener.setDataChanged(true); csingh7 r6 claim comment enhancement

                if (m_bCommentsFlag == 'false')	//For Event/Loc Desc
                {
                    self.parent.opener.document.OnEditMemoSave();
                    oEvent.needPostBack = false;
                    self.close();
                }
                else {
                    oEvent.needPostBack = true;
                }
            }
            if (actID == 'Print') {
                document.getElementById('hdRecentAction').value = actID;//Added by Manika
                var formsubtitle = document.getElementById('formsubtitle');
                document.getElementById('OldText').value = oEditor.getText();
                //Start by Shivendu for MITS 15107
                var sExistingCommentsPrint = ""
                if (m_bFreezeExisting == 'true') {
                    var sRetVal = "";
                    //MITS 34136 hlv begin
                    //sRetVal = confirm('Do you want to print the existing comments also?');
                    sRetVal = confirm(MainPageValidations.ConfirmPrint);
                    //MITS 34136 hlv begin
                    if (sRetVal) {
                        var oExistingComments = null;
                        oExistingComments = eval('document.frames("iFrameExistingComments").document.all.objExisting');
                        if (oExistingComments != null) {
                            sExistingCommentsPrint = oExistingComments.innerHTML;
                        }
                    }

                }
                //End by Shivendu for MITS 15107
                if (document.getElementById("Caption") != null) {
                    //Concatenation of sExistingCommentsPrint added by Shivendu for MITS 15107
                    strText = "<TABLE WIDTH=100%><TBODY><TR><TD align=CENTER colSpan=3><B>" +
        document.getElementById("Caption").innerHTML + "</B></TD></TR><TR>" +
        "<TD colSpan=3><HR></TD></TR></TBODY></TABLE>" + sExistingCommentsPrint + oEditor.getText();
                }
                else if (formsubtitle != null) {
                    //Concatenation of sExistingCommentsPrint added by Shivendu for MITS 15107
                    strText = "<TABLE WIDTH=100%><TBODY><TR><TD align=CENTER colSpan=3><B>" + formsubtitle.value + "</B></TD></TR><TR>" +
        "<TD colSpan=3><HR></TD></TR></TBODY></TABLE>" + sExistingCommentsPrint + oEditor.getText();
                }
                else {
                    //Concatenation of sExistingCommentsPrint added by Shivendu for MITS 15107
                    strText = sExistingCommentsPrint + oEditor.getText();
                }                
                oEditor.setText(strText);
            }
        }
function WebHtmlEditor1_AfterAction(oEditor, actID, oEvent, p4, p5, p6, p7, p8, act){
	if (actID == 'Print') {
      oEditor.setText(document.getElementById('OldText').value);
	}
}
function WebHtmlEditor1_KeyDown(oEditor, keyCode, oEvent) { // csingh7 r6 claim comment enhancement
    setDataChanged(true);    
}

function StripHTMLTags(InString)
{
	//alert(InString);
	var cleanedbuffer=""
	var PatternString ="\\<[^<>]+\\>";
	var re = new RegExp(PatternString, "gim");
	cleanedbuffer = InString.replace(re, '');

	//replace special characters
	//Commented for Mits:17110-Ampersand,Less than and Greater Than problem
	//cleanedbuffer = cleanedbuffer.replace(/(&nbsp;)|(&quot;)|(&ldquo;)|(&rdquo;)|(&lsquo;)|(&rsquo;)|(&amp;)|(&gt;)|(&lt;)/gim, ' ');
	cleanedbuffer = cleanedbuffer.replace(/(&nbsp;)|(&quot;)|(&ldquo;)|(&rdquo;)|(&lsquo;)|(&rsquo;)/gim, ' ');
	//Commented for Mits:17110-Ampersand,Less Than and Greater Than problem
	//alert(cleanedbuffer);
	return cleanedbuffer;			
}
function OnClosePopUp() //csingh7 for MITS 20092
{
    //reverted code sprint 8
    //mkaran2 - mits36801 - start
    //if (document.getElementById("hdRecentAction") != null && document.getElementById("hdRecentAction").value == "Load") {
    //    if (document.getElementById("hdUseLegacyComments") != null && document.getElementById("hdOpenerPlainMDIId") != null) {
    //        if (String(true) == document.getElementById("hdUseLegacyComments").value.toLowerCase()) {
    //            self.opener.parent.MDIForceRefreshByScreenId(document.getElementById("hdOpenerPlainMDIId").value);
    //        }
    //    }
    //    document.getElementById("hdRecentAction").value = "UnLoad";
    //}
    //mkaran2 - mits36801 - end
    if (document.getElementById("hdFormName") != null) {
        var sFrmName = document.getElementById("hdFormName").value;
        if (sFrmName != "LookUpClaimPolicy" && sFrmName != "SupervisorApproval")
            RMXOnClosePromptForPopUp();
    }
}
    </script>
</head>

<body style="display: inline; height: 95%;width: 95%" onload="if(parent.MDIScreenLoaded != null) parent.MDIScreenLoaded();updateRecentAction();checkForAdjusterText();" onbeforeunload="OnClosePopUp();"> <!-- Modified by csingh7 for MITS 20092 -->
    <script language="javascript" type="text/javascript">
        var m_bCommentsFlag = 'false';
		var m_bFreezeExisting = 'false';
		var m_bDateStamp = 'false';
		var m_bFreezeDateStamp = 'false';
		var m_FieldName = "";
		var m_isClaimComments = 'false';
		var m_sExistingComments = "";
		var m_IsSaved = false;		

		function pleaseWait1()//16416
		{
		    pleaseWait.Show();
		}

		function getTextFlags() 
		{		    
			var oCtrl = document.getElementById("hdCommentsFlag");
			if( oCtrl != null )
			m_bCommentsFlag = oCtrl.value;

			if( m_bCommentsFlag == 'false' )	//For Event/Loc Desc
			{

            // this check has been moved to the if else clause below. -- aaggarwal29 MITS 28066 
			    //oCtrl = document.getElementById("hdDateTimeStampEvtLocDesc");
			    //			    if( oCtrl != null )
			    //			        m_bDateStamp = oCtrl.value;

			    oCtrl = document.getElementById("hdFieldName");
			    if (oCtrl != null) {   // aaggarwal29 MITS 28066 -- start
			        m_FieldName = oCtrl.value;
			        if (m_FieldName == 'txtcomment') {   // based on the screen type, determined using hdFieldName, the date time value is set and displayed
			            oCtrl = document.getElementById("hdDateStampFlag");
			        }
			        else {
			            oCtrl = document.getElementById("hdDateTimeStampEvtLocDesc");
			        }
			        if (oCtrl != null)
			            m_bDateStamp = oCtrl.value;
			    }
			    // aaggarwal29 MITS 28066 -- end


			    //Parijat: Mits 10778
			    oCtrl = document.getElementById("hdFreezeDateFlag");
			    if( oCtrl != null )
			        m_bFreezeDateStamp = oCtrl.value;

			    //From claim, the field name could be ev_eventdescription
			    if( m_FieldName.indexOf("eventdescription") >= 0 )
			    {
			        oCtrl = document.getElementById("hdFreezeEventDesc");
			        if( oCtrl != null )
			            m_bFreezeExisting = oCtrl.value;
			    }
			    else if( m_FieldName.indexOf("locationareadesc") >= 0 )
			    {
			        oCtrl = document.getElementById("hdFreezeEventLocDesc");
			        if( oCtrl != null )
			            m_bFreezeExisting = oCtrl.value;
			    }
			        //asharma326 MITS 28368 Starts
			    else if (m_FieldName.indexOf("txtcomment") >= 0) {
			        oCtrl = document.getElementById("hdFreezeTextFlag");
			        if (oCtrl != null)
			            m_bFreezeExisting = oCtrl.value;
			    }
			    //asharma326 MITS 28368 Ends
			}
			else
			{
			    oCtrl = document.getElementById("hdDateStampFlag");
			    if( oCtrl != null )
			        m_bDateStamp = oCtrl.value;

			    oCtrl = document.getElementById("hdFreezeDateFlag");
			    if( oCtrl != null )
			        m_bFreezeDateStamp = oCtrl.value;

			    oCtrl = document.getElementById("hdFreezeTextFlag");
			    if( oCtrl != null )
			        m_bFreezeExisting = oCtrl.value;

			    oCtrl = document.getElementById("hdComments");
			    m_FieldName = "hdHTMLComments";

			}	
        }

	function updateRecentAction() {
		if (document.getElementById("hdRecentAction").value == "Save") {
			if (String(true) == document.getElementById("hdUseLegacyComments").value.toLowerCase()) {
				self.opener.parent.MDIForceRefreshByScreenId(document.getElementById("hdOpenerPlainMDIId").value);
			}
		}
		document.getElementById("hdRecentAction").value = "Load";
	}
			
		function RMXOnClosePromptForPopUp()
		{
            
            //Mona :12510-08/19/2008:Start-In case of "Use Adjuster Text For Claim Comments", Confirm Save pop up coming 
            //while navigating from existing comments screen to Adjuster dated text screen
            bUseAdjTxt = '';
            bCommentsFlag = 'false';
            if (bUseAdjTxt == 'True')
                if (bCommentsFlag == 'true')
                if (document.title == '') {
                    return;
                }
                
            //Mona :12510-End

            //var sMsg = "You are about to DISCARD UNSAVED CHANGES!";
            //var sMsg = "You are DISCARDING your changes and closing Comments!"; //MITS 27964 hlv 4/10/2012
            var sMsg = MainPageValidations.ValLeavePage; //MITS 34136 hlv 11/27/2012
            if (m_DataChanged && !m_IsSaved) {
                if (window.event != null) {
                    window.event.returnValue = sMsg;
                }
            }
        }
			
		function checkForAdjusterText()
		{
		    var oExistingComments = null;		   
			InitializeEditor();

			if (document.getElementById("hdFormName") != null) {
			    /*var sFormName = document.getElementById("hdFormName").value;
			    if (sFormName == "claimgc" || sFormName == "claimwc" || sFormName == "claimdi" || sFormName == "claimva" || sFormName == "event")
			        parent.MDIScreenLoaded();
			    */
			
			    switch (document.getElementById("hdFormName").value)
			    {
			        case "claimgc":
			        case "claimwc":
			        case "claimdi":
			        case "claimva":
			        //smahajan6 11/26/09 MITS:18230 :Start
			        case "claimpc":
			        //smahajan6 11/26/09 MITS:18230 :End
			            m_isClaimComments = 'true';
			            bCommentsFlag = document.getElementById('hdCommentsFlag').value;
			            bUseAdjTxt = document.getElementById('hdUseAdjTxt').value;
			            var claimid = document.getElementById("hdRecordId").value;
//			            var claimid = window.opener.document.forms[0].claimid.value; csingh7
			            //Mjain8 08/08/06:Added for MITS 7619 for passing adjuster row id
			            var adjrowid = document.getElementById('hdadjrowid').value;

			            if (bCommentsFlag == 'true') {
			                var iAdjRowID = -1;
			                if (!isNaN(adjrowid))
			                    iAdjRowID = parseInt(adjrowid);

			                if (iAdjRowID > 0) {
			                    if (bUseAdjTxt == 'True') {
			                        var sExternalParam = "<SysExternalParam><AdjRowId>" + adjrowid + "</AdjRowId></SysExternalParam>";
			                        //url of adjusterdatedtext.aspx changed by Nitin for Mits 16592 on 15-May-2009
			                        var sLinkto = "/RiskmasterUI/UI/FDM/adjusterdatedtext.aspx?SysCmd=1&SysFormId=" + claimid + "&SysExternalParam=" + sExternalParam;
			                        window.location.href = sLinkto;
			                    }
			                } //mjain8 MITS 7619 end
			                else {
			                    if (bUseAdjTxt == 'True') {
                                    //hlv MITS 34136 begin
                                    //alert("There is not a current adjuster set for this claim.The adjuster text form cannot be opened until there is a current adjuster specified.");
                                    alert(MainPageValidations.ValUseAdjTxt);
                                    //hlv MITS 34136 end
			                        window.close();
			                    }
			                }
			            }
			            break;
					default:			
				}
            }
			//try to load existing comments into upper frame 
			//if the existing text should be freezed.
			if( m_bFreezeExisting == 'true' )
			{
				var oHTMLComments = null;
				//Jira id: 7677 - Start
				//if( window.frames.length > 0 )
				//{
				    
				    //oExistingComments = eval('document.frames("iFrameExistingComments").document.all.objExisting');
				    var browser = get_browser();
				    if (browser.trim().toLowerCase().indexOf("ie") > -1) {
				        if (document.frames.length > 0)
				            oExistingComments = eval('document.frames["iFrameExistingComments"].document.all.objExisting');
				    }
				    else {
				        if (window.frames.length > 0)
				            oExistingComments = eval('window.frames["iFrameExistingComments"].document.all.objExisting');
				    }
				     
					//oExistingComments = eval('document.all.objExisting');
			    //}
				//Jira id: 7677 - End
				if ( oExistingComments != null )
				{
					oHTMLComments = eval('document.all.hdHTMLComments');
					if( oHTMLComments != null )
					{
					    var cleantext = DecodeFckEditor(oHTMLComments.value); //RMA-10685 msampathkuma
					    oExistingComments.innerHTML = cleantext;
					}
				}
            }					

			if((document.getElementById('hdComments').value == "") || (m_bFreezeExisting == 'false'))
			{			    
				if( document.getElementById("tr_extComments1") != null )
				{
                    //igupta3 Mits# 33301
				    document.getElementById('tr_extComments1').style.visibility = 'hidden';
				    document.getElementById('tr_extComments1').style.display = 'none';
				    //document.getElementById('frmWizardData').all.tr_extComments1.style.visibility = 'hidden';
				    //document.getElementById('frmWizardData').all.tr_extComments1.style.display = 'none';
				    //document.frmWizardData.all.tr_extComments1.style.visibility = 'hidden'; //nnithiyanand MITS 34698 -Commented for javascript error.
				    //document.frmWizardData.all.tr_extComments1.style.display="none"; //nnithiyanand MITS 34698 -Commented for javascript error.
				}				
				if( document.getElementById("tr_extComments2") != null )
				{
				    //igupta3 Mits# 33301
				    document.getElementById('tr_extComments2').style.display = "none";
				    document.getElementById('tr_extComments2').style.visibility = 'hidden';
				    //document.getElementById('frmWizardData').all.tr_extComments2.style.visibility = 'hidden';
				    //document.getElementById('frmWizardData').all.tr_extComments2.style.display = "none";
				    //document.frmWizardData.all.tr_extComments2.style.visibility = 'hidden'; //nnithiyanand MITS 34698 -Commented for javascript error.
				    //document.frmWizardData.all.tr_extComments2.style.display="none"; //nnithiyanand MITS 34698 -Commented for javascript error.
					
					var table = document.getElementById("WebHtmlEditor1");
                    table.style.height = "85%";
				}
            }

			if( (m_bDateStamp == 'false') || (m_bFreezeExisting == 'false') )
			{
				if( document.getElementById("tr_DateStamp") != null )
				{
				    //igupta3 Mits# 33301
				    document.getElementById('tr_DateStamp').style.visibility = 'hidden';
				    document.getElementById('tr_DateStamp').style.display = 'none';
				    //document.getElementById('frmWizardData').all.tr_DateStamp.style.visibility = 'hidden';
				    //document.getElementById('frmWizardData').all.tr_DateStamp.style.display = 'none';
				    //document.frmWizardData.all.tr_DateStamp.style.visibility = 'hidden'; //nnithiyanand MITS 34698 -Commented for javascript error.
				    //document.frmWizardData.all.tr_DateStamp.style.display = 'none'; //nnithiyanand MITS 34698 -Commented for javascript error.
				}
				
				if( document.getElementById("txtDtmStamp") != null )
				{
					document.getElementById("txtDtmStamp").value = '';
				}
			}				

			if( m_bFreezeDateStamp == 'true' )
			{
				if( document.getElementById("tr_DateStamp") != null )
				{
				    //igupta3 Mits# 33301
				    document.getElementById('txtDtmStamp').readOnly = "true";
				    //document.getElementById('frmWizardData').all.txtDtmStamp.readOnly = "true";
				    //document.frmWizardData.all.txtDtmStamp.readOnly="true"; //nnithiyanand MITS 34698 -Commented for javascript error.
				}
			}

			adjusterSize();
            
            //nnorouzi; Jan. 23, 2010
            //Avoiding unwanted horizontal scroll bar in the editor
			var webEditorDiv = document.getElementById("WebHtmlEditor1_tw");
			if (webEditorDiv != null)
			{
				webEditorDiv.style.wordWrap = "break-word";
			    //tmalhotra2 mits 25041
			    webEditorDiv.style.width = "95%";  //rkulavil - MITS 32412 - changed width from 100% to 95%

			}
			
			//Start by Shivendu for MITS 16854
			if (m_bFreezeExisting == 'false') {
			    var scrollBarPosition = document.getElementById("scrollBarPosition");
			    if (scrollBarPosition != null)
			        scrollBarPosition.scrollIntoView(false);

			    //Raman MITS 19155 : Comments added by one user overwrite other users changes if both users had open the same comments window
			    if (!(document.getElementById('ErrorControl_lblError') != null && document.getElementById('ErrorControl_lblError').innerHTML != "")) {
			        var oEditor = iged_getById("WebHtmlEditor1");
			        oEditor.setText(m_sExistingComments);    
			    }
			    else
			    {
			        var oEditor = iged_getById("WebHtmlEditor1");			        
			        oEditor.setText(oEditor.getText());   
			    }
			}
			//End by Shivendu for MITS 16854
			
			//nnorouzi; Jan. 20, 2010
			//Scroll to the bottom of the existing comments
			if (oExistingComments != null) {
				iFrameExistingComments.window.scrollTo(0,oExistingComments.scrollHeight);
			}			
            //hlv MITS 20339
			SetEditorHeightAuto();
        }
			
        //hlv MITS 20339
        function SetEditorHeightAuto() {
            var webEditorDiv = document.getElementById("WebHtmlEditor1_tw");
            var webEditorDivParent = webEditorDiv.parentElement;

            webEditorDiv.style.height = "auto";

            var tmpDiv = document.createElement("div");
            tmpDiv.style.clear = "both";

            webEditorDivParent.appendChild(tmpDiv);
        }

        function InitializeEditor() 
        {            
                    document.title = document.getElementById("hdHeader").value;
            if(eval(document.getElementById('txtDtmStamp'))!= null)
		    {
			    var Today = new Date();
			    var CurrentMonth = parseInt(Today.getMonth())+1 ;
			    var strDate = CurrentMonth + "/" + Today.getDate() + "/" + Today.getFullYear();
			    // commented By Nitesh //Nitesh 15 Jan 2006 Bug corrected Tr no.1330
			    //var strTime = Today.getHours() + ":" + Today.getMinutes() + ":" + Today.getSeconds();
			    var strTime = formatTime(Today,'hh:mm:ss a');//Nitesh 15 Jan 2006 Bug corrected Tr no.1330
			    var strUid = document.getElementById('hdUid').value;
			    document.getElementById('txtDtmStamp').value = strDate + " " + strTime + " (" + strUid + ") ";//MITS 11894 - abansal23
		    }
			
		    //var oFCKeditor;
		    getTextFlags();
            //Load event/loc desc
		    if(m_bCommentsFlag == 'false')
		    {
			    self.opener.document.OnEditMemoLoaded();
		    }
			
		    var sExistingComments = '';
		    var oExistingComments = null;
		    if( m_bCommentsFlag == 'true' )
			    oExistingComments = document.getElementById(m_FieldName);
		    else
		    {
			    oExistingComments = self.opener.document.getElementById(m_FieldName + "_HTML");
			    if( oExistingComments == null )
				    oExistingComments = self.opener.document.getElementById(m_FieldName);
		    }
            	    if (document.getElementById("hdFormName").value == "LookUpClaimPolicy" || document.getElementById("hdFormName").value == "SupervisorApproval")  //Added by csingh7 for Mits 20092
            	    {
                	    m_bFreezeExisting = 'false';      
            	    }
		    if( (oExistingComments != null) && (m_bFreezeExisting == 'false') )
		    {
			    sExistingComments = oExistingComments.value;
		        //bkuzhanthaim : RMA-7541 : A potentially dangerous Request.Form
		        sExistingComments = DecodeFckEditor(sExistingComments);
			    if ((m_bDateStamp == 'true') && (document.getElementById('txtDtmStamp') != null)) 
			    {
			        if (sExistingComments != "")
			            sExistingComments += '<BR>' + '<BR>';			        
			        //asharma326 JIRA 6422
			        if (document.getElementById('hdFieldType') != null && document.getElementById('hdFieldType').value == "supphtmlfield") {
			            if (document.getElementById('hdDateTimeStampHTMLText') != null && document.getElementById('hdDateTimeStampHTMLText').value == 'true')
			                sExistingComments += document.getElementById('txtDtmStamp').value;
			        }
			        else {
			            if (document.getElementById("hdFormName") != null) //csingh7 MITS 20092 :Start
			            {
			                if (document.getElementById("hdFormName").value != "LookUpClaimPolicy" && document.getElementById("hdFormName").value != "SupervisorApproval")  //Added by csingh7 for Mits 20092
			                {
			                    //MGaba2: MITS 20935:Cursor should be 1 space after the dttm/Adjuster stamp 
			                    // sExistingComments += "<DIV>" + document.getElementById('txtDtmStamp').value + "</DIV><DIV>"; //18829 :Parijat
			                    sExistingComments += document.getElementById('txtDtmStamp').value; //18829 :Parijat
			                }
			            }
			            else {
			                //MGaba2: MITS 20935:Cursor should be 1 space after the dttm/Adjuster stamp 
			                //sExistingComments += "<DIV>" + document.getElementById('txtDtmStamp').value + "</DIV><DIV>"; //18829 :Parijat
			                sExistingComments += document.getElementById('txtDtmStamp').value; //18829 :Parijat
			            }
			        }                                        //csingh7 MITS 20092 :End
			    }
			    if( m_bCommentsFlag == 'true' )
			    {
				    oExistingComments.value = "";
				    oExistingComments = document.getElementById("hdComments");
				    if( oExistingComments != null )
					    oExistingComments.value = "";
			    }
			    //Added by Shivendu for MITS 16854
			     m_sExistingComments = sExistingComments;
	             sExistingComments += '<p id=scrollBarPosition></p>';
		    }
          
	         //Raman MITS 19155 : Comments added by one user overwrite other users changes if both users had open the same comments window
	         if (!(document.getElementById('ErrorControl_lblError') != null && document.getElementById('ErrorControl_lblError').innerHTML != "")) {
	             var oEditor = iged_getById("WebHtmlEditor1");
	             oEditor.setText(sExistingComments);
	             oEditor.focus(); //Added by Shivendu for MITS 16853
	         }
	         else {
	             var oEditor = iged_getById("WebHtmlEditor1");
	             oEditor.setText(oEditor.getText() + '<B id=scrollBarPosition></B>');
	             oEditor.focus();
	         }
		    
					
//						oFCKeditor = new FCKeditor( 'FCKeditor1',0, '100%', '', sExistingComments);
//							
//						oFCKeditor.BasePath	= "FCKeditor/" ;
//						oFCKeditor.Create() ;
		}
			
			//Nitesh 15 Jan 2006 Bug corrected Tr no.1330
		function formatTime(date, format) 
		{
			format=format+"";
			var result="";
			var i_format=0;
			var c="";
			var token="";

			var H=date.getHours();
			var m=date.getMinutes();
			var s=date.getSeconds();
			var yyyy,yy,MMM,MM,dd,hh,h,mm,ss,ampm,HH,H,KK,K,kk,k;
			// Convert real date parts into formatted versions
			var value=new Object();
			value["H"]=H;

			if (("0"+H ).length==3)
				value["HH"]=("0"+H ).substr(1,2);
			else
				value["HH"]="0"+H ;

			if (H==0){value["h"]=12;}
			//else if (H<12){value["h"]=H-12;} 
			  else if (H>12){value["h"]=H-12;} // akashyap3 26-Feb-2009 MITS:14295
			else {value["h"]=H;}

			if (("0"+value["h"]).length==3)
				value["hh"]=("0"+value["h"]).substr(1,2);
			else
				value["hh"]="0"+value["h"];
			if (H>11){value["K"]=H-12;} else {value["K"]=H;}
			value["k"]=H+1;
			if (("0"+value["K"]).length==3)
				value["KK"]=("0"+value["K"]).substr(1,2);
			else
				value["KK"]="0"+value["K"];

			if (("0"+value["k"]).length==3)
				value["kk"]=("0"+value["k"]).substr(1,2);
			else
				value["kk"]="0"+value["k"]
			if (H > 11) { value["a"]="PM"; }
			else { value["a"]="AM"; }
			value["m"]=m;

			if (("0"+m).length==3)
				value["mm"]=("0"+ m ).substr(1,2);
			else
				value["mm"]="0"+m;
			value["s"]=s;

			if (("0"+s).length==3)
				value["ss"]=("0"+s).substr(1,2);
			else
				value["ss"]="0"+s;

			while (i_format < format.length) {
				c=format.charAt(i_format);
				token="";
				while ((format.charAt(i_format)==c) && (i_format < format.length)) {
					token += format.charAt(i_format++);
					}
				if (value[token] != null) { result=result + value[token]; }
				else { result=result + token; }
				}
			return result;
		}
		//Nitesh 15 Jan 2006 Bug corrected Tr no.1330
			
		//resize the textbox when the window resized
		function adjusterSize()
		{
			var bShowDataStamp = true;
			var bShowExistingComments = true;
			var objExtComments = null;
			var objFCKEditor = null;
            //Jira id : 7677 - Starts
			var browsers = get_browser();

			if (browsers.trim().toLowerCase().indexOf("ie") > -1) {
			    var iHeight = document.body.offsetHeight;//document.body.clientHeight;
			    var iWidth = document.body.offsetWidth;//document.body.clientWidth;
			}
			else {
			    var iHeight = document.body.scrollHeight;
			    var iWidth = document.body.scrollWidth;		    
			    
			}
		    //Jira id : 7677 - End

			if( document.getElementById("tr_DateStamp") != null )
			{
			    //igupta3 Mits# 33301
			    if(document.getElementById('tr_DateStamp').style.visibility == 'hidden')
			    //if(document.getElementById('frmWizardData').all.tr_DateStamp.style.visibility == 'hidden')
			        bShowDataStamp = false;
			    //if(document.frmWizardData.all.tr_DateStamp.style.visibility == 'hidden') //nnithiyanand MITS 34698 -Commented for javascript error.
			    //	bShowDataStamp = false; //nnithiyanand MITS 34698 -Commented for javascript error.
			}
			
			//because claim comments has to display claim number, so the textbox should be a little bit short
			if( m_isClaimComments == 'true')
			{
				iHeight = iHeight - 15;
			}			
			if( document.getElementById("tr_extComments2") != null )
			{
				
			    objExtComments = document.getElementById("iFrameExistingComments");
			    
			    //if(document.frmWizardData.all.tr_extComments2.style.visibility == 'hidden') //nnithiyanand MITS 34698 -Commented for javascript error.
			    //	bShowExistingComments = false; //nnithiyanand MITS 34698 -Commented for javascript error.
			    //if (document.getElementById('frmWizardData').all.tr_extComments2.style.visibility == 'hidden')
			    if (document.getElementById('tr_extComments2').style.visibility == 'hidden') //igupta3 Mits# 33301
			    {
			        bShowExistingComments = false;
			    }
			    else
			    {
			        if (bShowDataStamp == true)
			            objExtComments.height = iHeight * 0.35;
			        else
			            objExtComments.height = iHeight * 0.37;
			    }
				
			}
					
			objFCKEditor = document.getElementById("tr_FCKEditor");
			var oExistingComments = document.getElementById("hdComments");
			var sExistingComments = "";
			if( oExistingComments != null )
				sExistingComments = oExistingComments.value;
				
			if( objFCKEditor != null )
			{
				if((bShowExistingComments == true) && (sExistingComments != ""))
				{
					if( bShowDataStamp == true)
						objFCKEditor.height = iHeight * 0.42;
					else
						objFCKEditor.height = iHeight * 0.44;
				}
				else
				{
					if( bShowDataStamp == true)
						objFCKEditor.height = iHeight * 0.80;
					else
						objFCKEditor.height = iHeight * 0.84;
				}
			}
		}
		
		window.onresize = adjusterSize;
    </script>
    <form id="frmWizardData" runat="server">
    
    <div>
         <uc1:ErrorControl ID="ErrorControl" runat="server" />	
        	<asp:Table ID="tblDynamic" runat="server">
        </asp:Table>
        
		<table id="tblData" width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr runat="server" id="tr_DateStamp">
				<td id="Td1" runat="server" class="caption"><asp:Label ID="lblCaption" runat="server" Text="<%$ Resources:lblCaption %>"></asp:Label></td>
				<td id="Td2" runat="server"><asp:TextBox runat="server" RMXRef="/Instance/Document/Editor/DateTimeStamp" id="txtDtmStamp" size="40"/>
				    <asp:textbox runat="server" style="display:none" id="hdComments" RMXRef="/Instance/Document/Editor/Comments" />
				    <asp:textbox runat="server" style="display:none" id="hdDttmRcdLastUpd" RMXRef="/Instance/Document/Editor/DttmRcdLastUpd" />
				    <asp:textbox runat="server" style="display:none" id="hdHTMLComments" RMXRef="/Instance/Document/Editor/HTMLComments" />
				    <asp:textbox runat="server" style="display:none" id="hdPlainText" RMXRef="/Instance/Document/Editor/PlainText" />
				    <asp:textbox runat="server" style="display:none" id="hdTextHtml" RMXRef="/Instance/Document/Editor/EditorTextHtml" />
				    <asp:textbox runat="server" style="display:none" id="hdTextEncodedHtml" RMXRef="/Instance/Document/Editor/EditorTextEncodedHtml" />
				    <!-- Hidden fields required for Memo -->
				    <asp:textbox runat="server" style="display:none" id="hdCommentsFlag" />
				    <asp:textbox runat="server" style="display:none" id="hdFieldName"  />
                    <asp:textbox runat="server" style="display:none" id="hdSubtitle"  />
				    <!--Shruti for 10364-->
				    <asp:textbox runat="server" style="display:none" id="hdFormName"  />
				    <asp:textbox runat="server" style="display:none" id="hdRecordId"  />
				    <asp:textbox runat="server" style="display:none" id="hdOpenerPlainMDIId"  />
				    <asp:textbox runat="server" style="display:none" id="hdUseLegacyComments"  />
				    <asp:textbox runat="server" style="display:none" id="hdRecentAction"  />
				    <asp:textbox runat="server" style="display:none" id="hdMoved" RMXRef="/Instance/Document/Editor/Moved" />
				    <!--Shruti for 10364 ends-->
				    <asp:textbox runat="server" style="display:none" id="hdCallback"/>
    				
				    <asp:textbox runat="server" style="display:none" id="hdUid" RMXRef="/Instance/Document/Editor/RecordDetails/@UserName" />
    				
				    <asp:textbox runat="server" style="display:none" id="hdFreezeTextFlag" RMXRef="/Instance/Document/Editor/Comments/@FreezeTextFlag"></asp:textbox>
				    <asp:textbox runat="server" style="display:none" id="hdDateStampFlag" RMXRef="/Instance/Document/Editor/Comments/@DateStampFlag"></asp:textbox>
    				
				    <asp:textbox runat="server" style="display:none" id="hdFreezeDateFlag" RMXRef="/Instance/Document/Editor/Comments/@FreezeDateFlag"></asp:textbox>
				    <asp:textbox runat="server" style="display:none" id="hdDateTimeStampEvtLocDesc" RMXRef="/Instance/Document/Editor/Comments/@DateTimeStampEvtLocDesc"></asp:textbox>
				    <asp:textbox runat="server" style="display:none" id="hdFreezeEventDesc" RMXRef="/Instance/Document/Editor/Comments/@FreezeEventDesc"></asp:textbox>
				    <asp:textbox runat="server" style="display:none" id="hdFreezeEventLocDesc" RMXRef="/Instance/Document/Editor/Comments/@FreezeEventLocDesc"></asp:textbox>
    				
				    <asp:textbox runat="server" style="display:none" id="hdUseAdjTxt" RMXRef="/Instance/Document/Editor/RecordDetails/@UseAdjTxt"></asp:textbox>
				    <asp:textbox runat="server" style="display:none" id="hdadjrowid" RMXRef="/Instance/Document/Editor/Comments/@AdjusterRowID"></asp:textbox>
				    <asp:textbox runat="server" style="display:none" id="hdHeader" RMXRef="/Instance/Document/Editor/Comments/@Header"></asp:textbox>
				    <asp:textbox runat="server" style="display:none" id="hdDataSaved"></asp:textbox>
				    <asp:textbox runat="server" style="display:none" id="hdCommentId" RMXRef="/Instance/Document/Editor/CommentId"></asp:textbox>
                    <asp:TextBox runat="server" Style="display: none" ID="hdFieldType" />
                    <asp:TextBox runat="server" Style="display: none" ID="hdDateTimeStampHTMLText" RMXRef="/Instance/Document/Editor/Comments/@DateTimeStampHTMLText"></asp:TextBox>
				</td>
			</tr>
			<tr id="tr_extComments1">
				<td runat="server" id="tdExtComments1" class="caption" colspan="2">
					
				</td>
			</tr>
			
			<tr id="tr_extComments2">
				<td colspan="2">
					<div style='background-color:white; padding:5;'>
						<iframe id="iFrameExistingComments" name="iFrameExistingComments" runat="server" src="ExistingComments.htm" width="100%"></iframe>
					</div>
				</td>
			</tr>
			<tr id="tr_FCKEditor">
				<td colspan="2">
            
                    <ig_spell:WebSpellChecker ID="WebSpellChecker1" runat="server">
                     <%--aanandpraka2-Changes start for MITS 23104-Spell check ignoring upper case words--%>
                <DialogOptions Modal="True" ShowNoErrorsMessage="True" />  <%--mbahl3 Mits:27297--%>
                       <SpellOptions>
                    <PerformanceOptions AllowCapitalizedWords="false"/>
                    </SpellOptions>
                    <%--aanandpraka2-Changes end for MITS 23104-Spell check ignoring upper case words--%>
                    </ig_spell:WebSpellChecker>
                    <ighedit:WebHtmlEditor ID="WebHtmlEditor1" runat="server" Width="100%" Height="60%" SpellCheckerID="WebSpellChecker1" UploadedFilesDirectory="/RiskmasterUI/UploaderTemp" UseLineBreak="false" CssClass="pclass"><%--Changed line break to false: Manika : Mits 24920--%> <%-- Mits:32492 --%>
                        <ClientSideEvents BeforeAction="WebHtmlEditor1_BeforeAction" AfterAction="WebHtmlEditor1_AfterAction" KeyDown="WebHtmlEditor1_KeyDown"/>
                    </ighedit:WebHtmlEditor>
			    </td>
		    </tr>
		</table>
        <asp:Button CssClass="button" ID="btnSaveClose" Text="<%$ Resources:btnSaveClose %>" runat="server" OnClientClick="return SaveAndClose();" onclick="btnSaveClose_Click" /><%--Parijat : Mits 18496--%>
    
		<input type="hidden" id="OldText" />
		<input type="hidden" id="formsubtitle" runat="server"/>
		<uc2:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="_" />		
    </div>
    </form>
</body>
</html>
