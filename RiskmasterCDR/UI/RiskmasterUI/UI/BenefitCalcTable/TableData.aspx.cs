﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.BenefitCalcTable
{
    public partial class TableData : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                bool bReturnStatus = false;
                XElement XmlTemplate = null;
                XmlDocument XmlDoc = new XmlDocument();
                string sreturnValue = "";
                DataRow objRow;
                int iClassId = 0;
                if (!IsPostBack)
                {
                    gvBenefitCalGrid.HeaderStyle.CssClass = "msgheader";
                    gvBenefitCalGrid.RowStyle.CssClass = "datatd1";
                    gvBenefitCalGrid.AlternatingRowStyle.CssClass = "datatd";

                    // Associate the Grid Buttons with the Attributes.
                    // We need to do this at run time as onmouseover and onmouseout attributes are not exposed for Imagebutton.
                    gvBenefitCalGrid_New.Attributes.Add("onmouseover", "this.src='../../Images/new2.gif'");
                    gvBenefitCalGrid_New.Attributes.Add("onmouseout", "this.src='../../Images/new.gif'");

                    // After the javascriopt function is called, we are returning false from here.
                    // as we do not want a postback here.

                    gvBenefitCalGrid_New.Attributes.Add("onclick", "openGridAddEditWindow('BenefitCalGrid','add','500','400');return false;");

                    gvBenefitCalGrid_Edit.Attributes.Add("onmouseover", "this.src='../../Images/edittoolbar2.gif'");
                    gvBenefitCalGrid_Edit.Attributes.Add("onmouseout", "this.src='../../Images/edittoolbar.gif'");
                    gvBenefitCalGrid_Edit.Attributes.Add("onclick", "openGridAddEditWindow('BenefitCalGrid','edit','500','400');return false;");

                    gvBenefitCalGrid_Delete.Attributes.Add("onmouseover", "this.src='../../Images/delete2.gif'");
                    gvBenefitCalGrid_Delete.Attributes.Add("onmouseout", "this.src='../../Images/delete.gif'");
                    gvBenefitCalGrid_Delete.Attributes.Add("onclick", "validateGridForDeletion('BenefitCalGrid');");

                    // Read the Values from Querystring
                    string sClassId = AppHelper.GetQueryStringValue("ClassRowId");


                    if (sClassId != "")
                    {
                        iClassId = Int32.Parse(sClassId);
                        hdnclassid.Text = sClassId;
                    }
                }
                else
                {
                    iClassId = Int32.Parse(hdnclassid.Text);

                }

                XmlTemplate = GetMessageTemplate(iClassId);
                bReturnStatus = CallCWSFunction("DisClassTdManagerAdaptor.OnLoad", out sreturnValue, XmlTemplate);

                if (bReturnStatus)
                {
                    XmlDoc.LoadXml(sreturnValue);

                    DataTable dtGridData = new DataTable();

                    dtGridData.Columns.Add("TdRowId");
                    dtGridData.Columns.Add("Earnings");
                    dtGridData.Columns.Add("Benefits");
                    dtGridData.Columns.Add("Supplement");

                    XmlNodeList tableData = XmlDoc.SelectNodes("//DisClassTdData/DisClassTd");

                    foreach (XmlNode objNodes in tableData)
                    {
                        objRow = dtGridData.NewRow();
                        objRow["TdRowId"] = objNodes.ChildNodes[0].InnerText;
                        objRow["Earnings"] = objNodes.ChildNodes[1].InnerText;
                        objRow["Benefits"] = objNodes.ChildNodes[5].InnerText;
                        objRow["Supplement"] = objNodes.ChildNodes[6].InnerText;

                        dtGridData.Rows.Add(objRow);
                    }

                    objRow = dtGridData.NewRow();
                    dtGridData.Rows.Add(objRow);

                    gvBenefitCalGrid.DataSource = dtGridData;

                    gvBenefitCalGrid.DataBind();

                }
                else
                {
                    ecErrorControl.errorDom = sreturnValue;
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ecErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            
            
        }

        protected void gvBenefitCalGrid_DataBound(object sender, EventArgs e)
        {
            gvBenefitCalGrid.Columns[1].HeaderStyle.CssClass = "hiderowcol";
            gvBenefitCalGrid.Columns[1].ItemStyle.CssClass = "hiderowcol";

            gvBenefitCalGrid.Rows[gvBenefitCalGrid.Rows.Count - 1].CssClass = "hiderowcol";
        }

        protected void gvBenefitCalGrid_Delete_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                bool bReturnStatus = false;
                XElement XmlTemplate = null;
                string sReturnValue = "";

                XmlTemplate = GetDeleteMessageTemplate();
                bReturnStatus = CallCWS("DisClassTdManagerAdaptor.DeleteRowData", XmlTemplate, out sReturnValue, false, false);

                if (bReturnStatus)
                {
                    string script = "<script>Refresh();</script>";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "myscript", script);
                }
                else
                {
                    ecErrorControl.errorDom = sReturnValue;
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ecErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XElement GetMessageTemplate(int iClassId)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>409b8705-c38d-4216-adfe-c1f6397196dd</Authorization>");
            sXml = sXml.Append("<Call><Function>DisClassTdManagerAdaptor.OnLoad</Function></Call><Document><BenefitCalcTable><Parameters><ClassId>");
            sXml = sXml.Append(iClassId);
            sXml = sXml.Append("</ClassId></Parameters></BenefitCalcTable></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        private XElement GetDeleteMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>409b8705-c38d-4216-adfe-c1f6397196dd</Authorization>");
            sXml = sXml.Append("<Call><Function>DisClassTdManagerAdaptor.DeleteRowData</Function></Call><Document><InputXml><BenefitCalTable>");
            sXml = sXml.Append("<rowid>");
            sXml = sXml.Append(hdnTdRowId.Text);
            sXml = sXml.Append("</rowid>");
            sXml = sXml.Append("</BenefitCalTable></InputXml></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
    }
}
