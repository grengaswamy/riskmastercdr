﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TableDataDetail.aspx.cs" Inherits="Riskmaster.UI.BenefitCalcTable.TableDataDetail" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Table Driven Benefits Entry</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
    function BenefitCalGrid_OK()
    {
        var bResult = false;

        // commented for MITS No 14166 as the alert is needed in every case except when all fields are filled
        //   if(getDataChanged()==false)
        //   {
        //	    window.close();
        //	    return true;
        //   }
        if (BenefitCalGrid_Validate()) {
            return true;
        }
        else {
            return false;
        }
    }
    
    function BenefitCalGrid_Validate()
    {	
	    if(trim(document.getElementById('wages_from').value) == '' ||
		   trim(document.getElementById('wages_to').value) == '' ||
		   trim(document.getElementById('week_benefits').value) == '' ||
               trim(document.getElementById('supplement').value) == '') {
                //**ksahu5-ML-MITS34114 Start **   
                //alert('Wages From, Wages To, Weekly Benefits, Supplement are all required fields.');
                alert(TableDetailDataValidations.WagesRequiredFields);
                //**ksahu5-ML-MITS34114 End **  
                return false;
	    }
		else
		{
	        return true;
	    }
    }
    
    function BenefitCalGrid_onCancel()
    {
//	    var retval = false;
//        
//	    if(getDataChanged()==true)
//	    {
//		    if(ConfirmSave()==true)
//		    {
//		        if(BenefitCalGrid_Validate())
//		        {
//			        retval = true;
//			    }
//			    else
//			    {
//			        retval = false;
//			    }
//    			
//		    }
//		    else
//		    {
//		         window.close();
//		        retval = false;
//		    }
//    		
//	    }
//	    else
//	    {
	        window.close();
		    return false;
//	    }
//	    return retval;
    }
    function RefreshParent()
    {
        window.opener.document.forms[0].submit();
        window.close()
    }

    </script>
</head>
<body>
<!-- pmittal5 Mits 16116 05/20/09 - Changed type from "numeric" to "currency"-->
    <form id="frmData" runat="server">
    <table width="100%" cellspacing="0" cellpadding="0">
         <tr>
                
            <td colspan="2">
            <uc1:ErrorControl ID="ecErrorControl" runat="server" />
                <br/>
            </td>
        </tr>
		<tr>
			<td colspan="2" class="ctrlgroup">
             <!--**ksahu5-ML-MITS34114 Start **-->   
              <%--Table Driven Benefits Entry--%>
              <asp:Label ID="lblTableDrivenBenefitsEntry" runat="server" Text="<%$ Resources:lblTableDrivenBenefitsEntry %>"></asp:Label>
             <!--**ksahu5-ML-MITS34114 End **-->  
            </td>
		</tr>
		<tr>
			<td>
             <!--**ksahu5-ML-MITS34114 Start **--> 
                <%--<b><u>Wages From:</u></b>--%>
                
                 <asp:Label ID="lblWagesFrom" runat="server" Text="<%$ Resources:lblWagesFrom %>" Font-Underline="True" Font-Bold="true" ></asp:Label>
                
                <!--**ksahu5-ML-MITS34114 End **-->  
            </td>
            <td>
                <asp:TextBox id="wages_from" runat="server" TabIndex = "1" RMXType="currency" RMXRef="/Instance/Document/DisClassTdData/DisClassTd/WAGES_FROM" onchange="setDataChanged(true);currencyLostFocus(this);" />
            </td>
        </tr>
        <tr>
			<td>
            <!--**ksahu5-ML-MITS34114 Start **--> 
                <%--<u><b>Wages To: </u></b>  --%>  
                
              <asp:Label ID="lblWagesTo" runat="server" Text="<%$ Resources:lblWagesTo %>" Font-Underline="True" Font-Bold="true" ></asp:Label>         
               
              <!--**ksahu5-ML-MITS34114 End **-->    
            </td>
            <td>
                <asp:TextBox id="wages_to" runat="server" TabIndex = "2" RMXType="currency" RMXRef="/Instance/Document/DisClassTdData/DisClassTd/WAGES_TO" onchange="setDataChanged(true);currencyLostFocus(this);" />
            </td>
        </tr>
        <tr>
			<td>
             <!--**ksahu5-ML-MITS34114 Start **--> 
            <%--    <b><u>Weekly Benefits:</u></b>--%>
           
             <asp:Label ID="lblWeeklyBenefits" runat="server" Text="<%$ Resources:lblWeeklyBenefits %>" Font-Underline="True" Font-Bold="true" ></asp:Label>  
             
             <!--**ksahu5-ML-MITS34114 End **-->  
            </td>
            <td>
                <asp:TextBox id="week_benefits" runat="server" TabIndex = "3" RMXType="currency" RMXRef="/Instance/Document/DisClassTdData/DisClassTd/WEEKLY_BENEFIT" onchange="setDataChanged(true);currencyLostFocus(this);" />
            </td>
        </tr>
        <tr>
			<td>
              <!--**ksahu5-ML-MITS34114 Start **--> 
              <%--  <b><u>Supplement:</u></b>--%>
             
              <asp:Label ID="lblSupplement" runat="server" Text="<%$ Resources:lblSupplement %>" Font-Underline="True" Font-Bold="true" ></asp:Label>  
             
                <!--**ksahu5-ML-MITS34114 End **--> 
            </td>
            <td>
                <asp:TextBox id="supplement" runat="server" TabIndex = "4" RMXType="currency" RMXRef="/Instance/Document/DisClassTdData/DisClassTd/SUPPLEMENT" onchange="setDataChanged(true);currencyLostFocus(this);" />
            </td>
        </tr>
        <tr>
			    <td colspan="2"><br/>
			    </td>
			</tr>
			<tr>
			    <td colspan="2">
                  <!--**ksahu5-ML-MITS34114 Start **--> 
				   <%-- <asp:button ID="btnSubmit" runat="server" Text="OK" CssClass="button" 
                        onClientClick="return BenefitCalGrid_OK();"  style="width:60px" tabindex="5" 
                        onclick="btnSubmit_Click"/>
					&#160;
					<asp:button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" 
                        onClientClick="return BenefitCalGrid_onCancel();"  style="width:60px" tabindex="6" 
                        onclick="btnCancel_Click"/>--%>
                         <asp:button ID="btnSubmit" runat="server" Text="<%$ Resources:btnOk %>" CssClass="button" 
                        onClientClick="return BenefitCalGrid_OK();"  style="width:60px" tabindex="5" 
                        onclick="btnSubmit_Click"/>
					&#160;
					<asp:button ID="btnCancel" runat="server" Text="<%$ Resources:btnCancel %>" CssClass="button" 
                        onClientClick="return BenefitCalGrid_onCancel();"  style="width:60px" tabindex="6" 
                        onclick="btnCancel_Click"/>
                       <!--**ksahu5-ML-MITS34114 End **--> 
				</td>
			</tr>
        </table>
	    <asp:TextBox id="ClassId" style="display:none" runat="server" RMXType="id" RMXRef="/Instance/Document/DisClassTdData/DisClassTd/CLASS_ID"/>
	    <asp:TextBox id="RowId" style="display:none" runat="server" RMXType="id" RMXRef="/Instance/Document/DisClassTdData/DisClassTd/TD_ROW_ID"/>
	    <asp:TextBox id="FormMode" style="display:none" runat="server" RMXType="id" RMXRef="/Instance/Document/BenefitCalTable/value"/>
	    <asp:TextBox ID="hdnSelectedId" runat="server" style="display:none" RMXType="id"></asp:TextBox>
	    
    </form>
</body>
</html>
