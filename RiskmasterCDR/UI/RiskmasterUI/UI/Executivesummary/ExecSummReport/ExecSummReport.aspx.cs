﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Riskmaster.UI.ExecSummReport
{
    public partial class ExecSummReport : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            string sreturnValue = "";
            //DataSet usersRecordsSet = null;
            //XmlDocument usersXDoc = new XmlDocument();
            if (!IsPostBack)
            {
            TextBox txtSelectedUser = (TextBox)this.Form.FindControl("hiddenSelectedUser");
            if (txtSelectedUser != null && txtSelectedUser.Text != "")
            {

                TextBox txtUserId = (TextBox)this.Form.FindControl("UserId");
                if (txtUserId != null)
                    txtUserId.Text = txtSelectedUser.Text;
            }
                TextBox txtIsAdmin = (TextBox)this.Form.FindControl("IsAdminUser");
                if (txtIsAdmin != null && txtIsAdmin.Text == "")
                    txtIsAdmin.Text = "False";
                bReturnStatus = CallCWSFunctionBind("ExecutiveSummaryAdaptor.GetUserPreference", out sreturnValue);
                //if (bReturnStatus)
                //{
                //    BindpageControls(sreturnValue);
                    //usersXDoc.LoadXml(sreturnValue);
                    //usersRecordsSet = ConvertXmlDocToDataSet(usersXDoc);
                    ////if (stateRecordsSet.Tables[5] != null)
                    ////{
                    ////    grdStateMaint.DataSource = stateRecordsSet.Tables[5];
                    ////    grdStateMaint.DataBind();
                    ////}
                    //DropDownList lstAllUsersGroup = (DropDownList)this.Form.FindControl("cboAllUsersGroups");
                    //lstAllUsersGroup.DataSource = usersRecordsSet.Tables[80].DefaultView;
                    //lstAllUsersGroup.DataTextField = "Name";
                    //lstAllUsersGroup.DataValueField = "UserID";
                    //lstAllUsersGroup.DataBind();
                    //usersRecordsSet.Dispose();
                //}
            }

        }
        protected void AddUserGroup(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            string sreturnValue = "";
            TextBox txtSelectedUser = (TextBox)this.Form.FindControl("hiddenSelectedUser");
            if (txtSelectedUser != null && txtSelectedUser.Text != "")
            {

                TextBox txtUserId = (TextBox)this.Form.FindControl("UserId");
                if (txtUserId != null)
                    txtUserId.Text = txtSelectedUser.Text;
            }
            TextBox txtIsAdmin = (TextBox)this.Form.FindControl("IsAdminUser");
            if (txtIsAdmin != null && txtIsAdmin.Text == "")
                txtIsAdmin.Text = "False";
             bReturnStatus = CallCWSFunctionBind("ExecutiveSummaryAdaptor.GetUserPreference", out sreturnValue);
             //if (bReturnStatus)
             //{
             //    BindpageControls(sreturnValue);

             //}
            //bReturnStatus = CallCWSFunction("ExecutiveSummaryAdaptor.GetUserPreference");
            //if (bReturnStatus)
            //    bReturnStatus = CallCWSFunction("SupplementalGridParameterSetupAdaptor.Get");

        }
        protected void DeleteUserGroup(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            string sreturnValue = "";
            TextBox txtSelectedUser = (TextBox)this.Form.FindControl("hiddenSelectedUser");
            if (txtSelectedUser != null && txtSelectedUser.Text != "")
            {

                TextBox txtUserId = (TextBox)this.Form.FindControl("UserId");
                if (txtUserId != null)
                    txtUserId.Text = txtSelectedUser.Text;
            }
            TextBox txtIsAdmin = (TextBox)this.Form.FindControl("IsAdminUser");
            if (txtIsAdmin != null && txtIsAdmin.Text == "")
                txtIsAdmin.Text = "False";
            bReturnStatus = CallCWSFunctionBind("ExecutiveSummaryAdaptor.GetUserPreference", out sreturnValue);
            //if (bReturnStatus)
            //{
            //    BindpageControls(sreturnValue);

            //}
            //bReturnStatus = CallCWSFunction("ExecutiveSummaryAdaptor.GetUserPreference");
            //if (bReturnStatus)
            //    bReturnStatus = CallCWSFunction("SupplementalGridParameterSetupAdaptor.Get");

        }
        protected void Save(object sender, EventArgs e)
        {
            string sreturnValue = "";
            bool bReturnStatus = false;
            TextBox txtSelectedUser = (TextBox)this.Form.FindControl("hiddenSelectedUser");
            if (txtSelectedUser != null && txtSelectedUser.Text != "")
            {

                TextBox txtUserId = (TextBox)this.Form.FindControl("UserIDs");
                if (txtUserId != null)
                    txtUserId.Text = txtSelectedUser.Text;
            }
            TextBox txtIsAdmin = (TextBox)this.Form.FindControl("IsAdminUser");
            if (txtIsAdmin != null && txtIsAdmin.Text == "")
                txtIsAdmin.Text = "False";
            bReturnStatus = CallCWSFunction("ExecutiveSummaryAdaptor.SaveUserPreference", out sreturnValue);
            //if (bReturnStatus)
            //    bReturnStatus = CallCWSFunction("ExecutiveSummaryAdaptor.GetUserPreference");
            //if (bReturnStatus)
            //{
            //    BindpageControls(sreturnValue);
            //}
        }
        //private void BindpageControls(string sreturnValue)
        //{
        //    DataSet usersRecordsSet = null;
        //    XmlDocument usersXDoc = new XmlDocument();
        //    usersXDoc.LoadXml(sreturnValue);
        //    usersRecordsSet = ConvertXmlDocToDataSet(usersXDoc);
        //    //if (stateRecordsSet.Tables[5] != null)
        //    //{
        //    //    grdStateMaint.DataSource = stateRecordsSet.Tables[5];
        //    //    grdStateMaint.DataBind();
        //    //}
        //    DropDownList lstAllUsersGroup = (DropDownList)this.Form.FindControl("cboAllUsersGroups");
        //    lstAllUsersGroup.DataSource = usersRecordsSet.Tables[80].DefaultView;
        //    lstAllUsersGroup.DataTextField = "Name";
        //    lstAllUsersGroup.DataValueField = "UserID";
        //    lstAllUsersGroup.DataBind();
        //    usersRecordsSet.Dispose();

        //}
    }
}
