﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Riskmaster.BusinessHelpers;
using Newtonsoft.Json;
using System.Xml.Linq;
using Riskmaster.RMXResourceManager;
using System.Xml.XPath;
using Riskmaster.Common.Extensions;
using System.Dynamic;
using System.Web.Script.Services;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.XPath;

namespace Riskmaster.UI.UI.ClaimActivityLog
{
    public partial class AsyncClaimActLog : System.Web.UI.Page
    {
        private string PageId = RMXResourceProvider.PageId("AsyncClaimActLog.aspx");
        
        RMXMessageTemplateHelper oMessageTemplate = new RMXMessageTemplateHelper();
        string Const_Width = "150";
        XElement resultDoc = null;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request.IsAjaxRequest())
            {
                if (Request.QueryString["call"] != null)
                {
                    string sFormName;
                     string sClaimId;
                    switch (Request.QueryString["call"].ToLower())
                    {
                          
                        case "getdata":
                          sFormName =  Request.QueryString["SysFormName"];
                          sClaimId =  Request.QueryString["ClaimId"];
                          GetData(sFormName, sClaimId);
                            break;
                        case "savepreferences":
                          SavePreferences();
                            break;
                    }
                }
            }
            try
            {

                if (!IsPostBack)
                {
                    // ViewState["FormName"] = AppHelper.GetQueryStringValue("SysFormName");
                    // ViewState["ClaimId"] = AppHelper.GetQueryStringValue("ClaimId");
                    // BindGridWithRecords("");
                    //GetData();
                    hdnFormName.Value = AppHelper.GetQueryStringValue("SysFormName");
                    hdnClaimId.Value = AppHelper.GetQueryStringValue("ClaimId");
                }
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }
        //old code
        //private DataSet ConvertXmlDocToDataSet(XmlDocument resultDoc)
        //{
        //    DataSet dSet = null;
        //    try
        //    {
        //        dSet = new DataSet();
        //        dSet.ReadXml(new XmlNodeReader(resultDoc));

        //        return dSet;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new ApplicationException(ex.Message);
        //    }
        //}

        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization></Authorization> 
                    <Call>
                        <Function>ClaimActLogAdaptor.GetClaimActivityLog</Function> 
                    </Call>
                    <Document>
                        <ClaimActLogList>
                            <ClaimId></ClaimId>
                            <FormName></FormName>
                            <LangCode></LangCode>
                            <SortOrder></SortOrder>
                            <SortColumn></SortColumn>
                        </ClaimActLogList>
                    </Document>
            </Message>
            ");

            return oTemplate;
        }

        /// <summary>
        /// Dev Signature      : vchouhan6  
        /// Date               : 08/02/2014
        /// Description        : Ajax Method to get data for Claim Activity Log grid
        /// <returns>JSON Object</returns>
        /// </summary>
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetData(string sFormName, string sClaimId)
        {
            ServiceResponseHelper objResponse = null;
            XDocument messageElement = null;
            string sReturn = String.Empty;
            try
            {
                string oMessageElement = string.Empty;
                string strSortOrder = String.Empty;
                string strSortColumn = String.Empty;
                string strCategory = String.Empty;
                XmlDocument oTemplate = new XmlDocument();
                // JavaScriptSerializer reqJson = new JavaScriptSerializer();

                if (Request.Form["sortField"] != null && !String.IsNullOrEmpty(Convert.ToString(Request.Form["sortField"])))
                    strSortColumn = Convert.ToString(Request.Form["sortField"]);
                if (Request.Form["sortDirection"] != null && !String.IsNullOrEmpty(Convert.ToString(Request.Form["sortDirection"])))
                    strSortOrder = Convert.ToString(Request.Form["sortDirection"]);
                messageElement = oMessageTemplate.GetUserPrefMessageTemplateCAL();
                sReturn = AppHelper.CallCWSService(messageElement.ToString()); //return user preference xml (saved setting) for the search.

                if (sReturn != "")
                {
                    resultDoc = XElement.Parse(sReturn);

                }
                //if (AppHelper.GetQueryStringValue("Title") == "Claim Activity Log")
                strCategory = "ClaimActivityLog";

                oMessageElement = GetMessageTemplate().ToString();
                oTemplate.LoadXml(oMessageElement);
                XElement xUserPrefNode = resultDoc.XPathSelectElement("//UserPref/" + strCategory);
                if (xUserPrefNode != null && String.IsNullOrEmpty(strSortColumn))
                {
                    if (xUserPrefNode.Attribute("SortType") != null)
                        strSortOrder = xUserPrefNode.Attribute("SortType").Value;
                    if (xUserPrefNode.Attribute("SortColumn") != null)
                        strSortColumn = xUserPrefNode.Attribute("SortColumn").Value;
                }
                oTemplate.SelectSingleNode("//SortColumn").InnerText = strSortColumn;
                oTemplate.SelectSingleNode("//SortOrder").InnerText = strSortOrder;
                if (sClaimId != null)
                {
                   // oTemplate.SelectSingleNode("//ClaimId").InnerText = ViewState["ClaimId"].ToString();
                    oTemplate.SelectSingleNode("//ClaimId").InnerText = Convert.ToString(sClaimId);
                }
                if (sFormName != null)
                {
                  //  oTemplate.SelectSingleNode("//FormName").InnerText = ViewState["FormName"].ToString();
                    oTemplate.SelectSingleNode("//FormName").InnerText = Convert.ToString(sFormName);
                }
                oTemplate.SelectSingleNode("//LangCode").InnerText = AppHelper.GetLanguageCode();
               
                objResponse = BindGridWithRecords(oTemplate);
            }
            catch (Exception ex)
            {
                objResponse = new ServiceResponseHelper(false, ex.Message, null);
            }
            var json = JsonConvert.SerializeObject(objResponse, Newtonsoft.Json.Formatting.Indented);
            Response.Write(json);
            try
            {
                Response.Flush();
                Response.End();
            }
            catch (System.Threading.ThreadAbortException threadEx) { }
        }

        private ServiceResponseHelper BindGridWithRecords(XmlDocument p_objResultXml)
        {
            string sReturn = string.Empty;
            XmlDocument oTemplate = p_objResultXml;
            List<ColumnDefHelper> lstColDef = new List<ColumnDefHelper>();
            List<IDictionary<String, object>> lstResponse = new List<IDictionary<String, object>>();
            XmlDocument usersXDoc = new XmlDocument(); 
            ServiceResponseHelper objResponse;
            GridDataHelper objGridData = new GridDataHelper();
            IEnumerable<XElement> xUserPrefNodeList = null;
            // JavaScriptSerializer reqJson = new JavaScriptSerializer();
            XmlElement xmlIn = null;
            int colIndex = 0;
            string colDisplayName = String.Empty;
            string colField = String.Empty;
            string strSortDirection = String.Empty;
            string strSortField = String.Empty;
            string strPageSize = String.Empty;
            string strCategory = String.Empty;

            bool IsColVisible = true;

            sReturn = AppHelper.CallCWSService(oTemplate.InnerXml);
            if (sReturn != string.Empty)
            {
                usersXDoc.LoadXml(sReturn);
                List<UserPreferences> lstUserPreference = new List<UserPreferences>();
                if (resultDoc != null)
                {
                    //if (AppHelper.GetQueryStringValue("Title") == "Claim Activity Log")
                        strCategory = "ClaimActivityLog";
                    //Userpref Code
                        XElement xUserPrefNode = resultDoc.XPathSelectElement("//UserPref/" + strCategory);
                       
                    if (xUserPrefNode != null)
                    {

                        if (xUserPrefNode.Attribute("SortType") != null)
                            strSortDirection = xUserPrefNode.Attribute("SortType").Value;
                        if (xUserPrefNode.Attribute("SortColumn") != null)
                            strSortField = xUserPrefNode.Attribute("SortColumn").Value;
                        if (String.IsNullOrEmpty(strPageSize) && xUserPrefNode.Attribute("PageSize") != null)
                            strPageSize = xUserPrefNode.Attribute("PageSize").Value;
                        xUserPrefNodeList = resultDoc.XPathSelectElements("//UserPref/" + strCategory + "/Column");
                        foreach (XElement PrefNode in xUserPrefNodeList)
                        {
                            lstUserPreference.Add(new UserPreferences
                            {
                                Name = Convert.ToString(PrefNode.Attribute("Name").Value),
                                ColOrder = Convert.ToString(PrefNode.Attribute("ColOrder").Value) == String.Empty ? 0 : Convert.ToInt32(PrefNode.Attribute("ColOrder").Value),
                                ColWidth = Convert.ToString(PrefNode.Attribute("ColWidth").Value)
                            });
                        }
                    }

                }
                xmlIn = (XmlElement)usersXDoc.SelectSingleNode("//ResultMessage//Document//ClaimActLogList//ClaimActivity");
                if (xmlIn != null)
                {
                    string sAttributeName;
                    foreach (XmlAttribute objChildElement in xmlIn.Attributes)
                    {
                        // for (int i = 0; i < objChildElement.Attributes.Count; i++)
                        // {
                        sAttributeName = objChildElement.Name.ToLower();

                        switch (sAttributeName)
                        {
                            case "dttm_rcd_added":
                                colDisplayName = "Date/Time";
                                //   colField = Common.Conversion.ReplaceSpecialCharacterToWords(objChildElement.Attributes[sAttributeName].Value);
                                break;
                            case "added_by_user":
                                colDisplayName = "User";
                                //  colField = Common.Conversion.ReplaceSpecialCharacterToWords(objChildElement.Attributes[sAttributeName].Value);
                                break;
                            case "type":
                                colDisplayName = "Activity";
                                //  colField = Common.Conversion.ReplaceSpecialCharacterToWords(objChildElement.Attributes[sAttributeName].Value);
                                break;
                            case "log_text":
                                colDisplayName = "Description";
                                //  colField = Common.Conversion.ReplaceSpecialCharacterToWords(objChildElement.Attributes[sAttributeName].Value);
                                break;
                            default:
                                colDisplayName = sAttributeName;
                                //   colField = Common.Conversion.ReplaceSpecialCharacterToWords(objChildElement.Attributes[sAttributeName].Value);
                                break;
                        }

                        var PrefColumn = lstUserPreference.Where(X => X.Name.ToUpper() == colDisplayName.ToUpper());
                        if (PrefColumn != null && PrefColumn.FirstOrDefault() != null)
                        {
                            Const_Width = PrefColumn.FirstOrDefault().ColWidth;
                            colIndex = PrefColumn.FirstOrDefault().ColOrder;
                        }
                        else
                        {
                            Const_Width = "150";
                            colIndex = 0;
                        }
                        if (objChildElement["hidden"] != null && objChildElement["hidden"].Value == "true")
                            IsColVisible = false;
                        else
                            IsColVisible = true;
                        lstColDef.Add(new ColumnDefHelper
                        {
                            field = Common.Conversion.ReplaceSpecialCharacterToWords(objChildElement.Name),
                            displayName = colDisplayName,
                            visible = IsColVisible,
                            width = Const_Width,
                            Position = colIndex,
                            headerCellTemplate = "<div class=\"ngHeaderSortColumn {{col.headerClass}}\" ng-style=\"{'cursor': col.cursor}\" ng-class=\"{ 'ngSorted': !noSortVisible }\">" +
                            "<div ng-click=\"col.sort($event)\"ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div><div class=\"ngSortButtonDown\" ng-show=\"col.showSortButtonDown()\"></div>" +
                            "<div class=\"ngSortButtonUp\" ng-show=\"col.showSortButtonUp()\"></div><div class=\"ngSortPriority\">{{col.sortPriority}}</div><div ng-class=\"{ ngPinnedIcon: col.pinned, ngUnPinnedIcon: !col.pinned }\" ng-click=\"togglePin(col)\" ng-show=\"col.pinnable\"></div></div>" +
                                //"<input type=\"text\" ng-click=\"stopClickProp($event)\" placeholder=\"Filter...\" ng-model=\"col.filterText\" ng-style=\"{ 'width' : col.width - 24 + 'px' }\" style=\"position: absolute; top: 30px; bottom: 30px; left: 0; bottom:0;\"/>"+
                                //"<a class=\"k-grid-filter\" href=\"#\" tabindex=\"-1\" ng-click=\"filterData(col)\"><span class=\"k-icon k-filter\"></span></a>"+
                            "<input type=\"text\" ng-click=\"stopClickProp($event)\" placeholder=\"Filter...\" ng-model=\"col.filterText\" ng-style=\"{ 'width' : col.width - 14 + 'px' }\" style=\"position: absolute; top: 30px; bottom: 30px; left: 0; bottom:0;\"/>" +
                            "<div ng-show=\"col.resizable\" class=\"ngHeaderGrip\" ng-click=\"col.gripClick($event)\" ng-mousedown=\"col.gripOnMouseDown($event)\"></div>"
                            //Position = colIndex,
                            //Key = isKey
                        });
                        // }


                    }

                    XmlNodeList xClaimActLogData = usersXDoc.SelectNodes("//ResultMessage//Document//ClaimActLogList/ClaimActivity");
                    foreach (XmlNode objNodes in xClaimActLogData)
                    {
                        int iCount = 0;
                        dynamic dynamicExpando = new ExpandoObject();
                        var objResponseJson = dynamicExpando as IDictionary<String, object>;
                        for (int i = 0; i < objNodes.Attributes.Count; i++)
                        {
                            objResponseJson[lstColDef[iCount].field] = objNodes.Attributes[i].Value.Trim();
                            iCount++;
                        }
                        lstResponse.Add(objResponseJson);
                    }

                    objGridData.Data = lstResponse;
                    objGridData.ColumnPosition = lstColDef;
                    lstColDef = lstColDef.OrderBy(X => X.Position).ToList<ColumnDefHelper>();
                    objGridData.ColumnDef = lstColDef;
                    objGridData.TotalCount = lstResponse.Count;
                    objGridData.SortDirection = strSortDirection == "descending" ? "desc" : "asc"; ;
                    objGridData.SortField = strSortField;
                    if (String.IsNullOrEmpty(strPageSize.Trim()))
                        strPageSize = Convert.ToString(lstResponse.Count());

                    objGridData.PageSize = Convert.ToInt32(strPageSize);
                }
                else
                {
                    objGridData.Data = null;
                    objGridData.ColumnPosition = null;
                    objGridData.Data = null;
                    objGridData.ColumnDef = null;
                    objGridData.TotalCount = 0;
                    objGridData.PageSize = 0;
                }
            }
            objResponse = new ServiceResponseHelper(true, null, objGridData);
            return objResponse;

            #region Old Code
            //to comment this
            //resultDataSet = ConvertXmlDocToDataSet(resultDoc);
            //if (resultDataSet != null)
            //{
            //    if (resultDataSet.Tables.Count > 2)
            //    {
            //        objView = new DataView(resultDataSet.Tables[3]);
            //        objtable = resultDataSet.Tables[3].DefaultView.ToTable();
            //        // GetDict(objtable);
            //    }

            //if (resultDoc != null)
            //{
            //   // objGridData.Data = "";
            //    objGridData.TotalCount = objtable.Rows.Count;
            //    //objGridData.SortDirection = strSortDirection == "descending" ? "desc" : "asc"; ;


            //}
            // else
            //{   //St:Yukti ML Changes MITS 34069
            //throw new ApplicationException("Error occured while fetching activity log");
            // throw new ApplicationException(AppHelper.GetResourceValue(PageId, "ErrorFetchingActivityLog", "3"));
            //objGridData.Data = null;
            //objGridData.ColumnPosition = null;
            //objGridData.Data = null;
            //objGridData.ColumnDef = null;
            //objGridData.TotalCount = 0;
            //objGridData.PageSize = 0;
            //  }

            //}
            // }
            //    objResponse = new ServiceResponseHelper(true, null, objtable);
            //return objResponse;
            #endregion
        }

        #region Save Preference
         /// <summary>
        /// Dev Signature      : achouhan3  
        /// Date               : 01/06/2014
        /// Description        : Ajax Method to get save user preference in XML
        /// <returns>JSON Object</returns>
        /// </summary>
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void SavePreferences()
        {
            ServiceResponseHelper objResponse = null;
            try
            {
                XDocument messageElement = null;
                XmlElement objUserPrefElement = null;
                XElement objXmlCriteria = null;
                XElement xmlIn;
                string strCategory = String.Empty;
                string strPageSize = String.Empty;
                string strSortDireection = String.Empty;
                string strSortFieldIndex = String.Empty;
                string strSortField = String.Empty;
                if (Request.Form["grdPageSize"] != null && !String.IsNullOrEmpty(Request.Form["grdPageSize"]))
                    strPageSize = Request.Form["grdPageSize"];
                if (Request.Form["sortDirection"] != null && !String.IsNullOrEmpty(Request.Form["sortDirection"]))
                    strSortDireection = Convert.ToString(Request.Form["sortDirection"]);
                if (Request.Form["sortField"] != null && !String.IsNullOrEmpty(Request.Form["sortField"]))
                    strSortField = Request.Form["sortField"];
                if (Request.Form["sortFieldIndex"] != null && !String.IsNullOrEmpty(Request.Form["sortFieldIndex"]))
                    strSortFieldIndex = Request.Form["sortFieldIndex"];
                System.Web.Script.Serialization.JavaScriptSerializer reqJson = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<UserPreferences> gridColumn = new List<UserPreferences>();
                List<object> gridSortColumn = new List<object>();
                int iCount = 1;
                XElement objTempElement = null;
                XDocument xmlUserPref = null;

             //   if (AppHelper.GetQueryStringValue("Title") == "Claim Activity Log")
                    strCategory = "ClaimActivityLog";
               
                objTempElement = new XElement(strCategory);
                objTempElement.SetAttributeValue("PageSize", strPageSize);
                objTempElement.SetAttributeValue("SortType", strSortDireection);
                objTempElement.SetAttributeValue("SortColumn", strSortField);

                xmlUserPref = new XDocument(objTempElement);
                if (Request.Form["columnObject"] != null && !String.IsNullOrEmpty(Request.Form["columnObject"]))
                    gridColumn = reqJson.Deserialize<List<UserPreferences>>(Request.Form["columnObject"]);

                foreach (var objColumn in gridColumn)
                {
                    objTempElement = XElement.Parse("<Column/>");
                    objTempElement.SetAttributeValue("Name", objColumn.colDef.displayName);
                    if (!String.IsNullOrEmpty(objColumn.colDef.cellTemplate))
                    {
                        objTempElement.SetAttributeValue("Key", "True");
                    }
                    objTempElement.SetAttributeValue("ColOrder", iCount);
                    objTempElement.SetAttributeValue("ColWidth", objColumn.width);
                    xmlUserPref.Element(strCategory).Add(objTempElement);
                    iCount++;
                }

                XElement tempElement = null;
                string sReturn = String.Empty;

                messageElement =oMessageTemplate.SetUserPrefMessageTemplateCAL(xmlUserPref.ToString());

                sReturn = AppHelper.CallCWSService(messageElement.ToString()); //return bool.

                //Get User Pref XML to store and communicate later on
                messageElement = oMessageTemplate.GetUserPrefMessageTemplateCAL();
                sReturn = AppHelper.CallCWSService(messageElement.ToString()); //return user preference xml (saved setting) for the search.

                if (sReturn != "")
                {
                    resultDoc = XElement.Parse(sReturn);

                }
                Dictionary<string, string> objUserPrefDict = new Dictionary<string, string>();
                string strUserPref = Server.UrlEncode(Server.HtmlEncode(resultDoc.ToString()));
                objUserPrefDict.Add("userPrefXML", strUserPref);
                objResponse = new ServiceResponseHelper(true, null, objUserPrefDict);
            }

            catch (Exception ex)
            {
                objResponse = new ServiceResponseHelper(false, ex.Message, null);
            }
            var json = JsonConvert.SerializeObject(objResponse);
            Response.Write(json);
            try
            {
                Response.Flush();
                Response.End();
            }
            catch (System.Threading.ThreadAbortException threadex) { }
        }
     
        #endregion
    }

}