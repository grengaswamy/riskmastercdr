﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RecentClaims.aspx.cs" Inherits="Riskmaster.UI.UI.RecentRecords.RecentClaims" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Recent Saved Claims </title>
</head>
<body onload="parent.MDIScreenLoaded();">
    <form id="frmRecentClaims" runat="server">
    <div>
        <uc1:ErrorControl ID="ErrorControl" runat="server" />
        <table width ="100%">
            <tr>
			    <td class="msgheader" nowrap="" colspan="">
				   <asp:Label ID="lblDisplay" runat="server" Text="<%$ Resources:lblDisplay %>"/> 
			    </td>
		    </tr>
		    <tr>
		        <td>
		            <asp:GridView ID="grdRecentClaims" runat="server" CellPadding="4" 
                        AutoGenerateColumns="False" DataKeyNames="claim_id" GridLines="None" 
                        onrowdatabound="grdRecentClaims_RowDataBound">
                    <HeaderStyle CssClass="colheader6" ForeColor="White"/>
                    <AlternatingRowStyle CssClass="data2" /> 
                    <Columns>
                        <asp:BoundField HeaderText="Claim Number" HeaderStyle-CssClass="headerlink2" ItemStyle-CssClass="data" DataField="claim_number"/>
                        <asp:BoundField HeaderText="Line of Business" HeaderStyle-CssClass="headerlink2" ItemStyle-CssClass="data" DataField="claim_lob"/>
                        <asp:BoundField HeaderText="Event Number" HeaderStyle-CssClass="headerlink2" ItemStyle-CssClass="data" DataField="event_number"/>
                        <asp:BoundField HeaderText="Date of Event" HeaderStyle-CssClass="headerlink2" ItemStyle-CssClass="data" DataField="date_of_event"/>
                        <asp:BoundField HeaderText="Date of Claim" HeaderStyle-CssClass="headerlink2" ItemStyle-CssClass="data" DataField="date_of_claim"/>
                        <asp:BoundField HeaderText="Claimant" HeaderStyle-CssClass="headerlink2" ItemStyle-CssClass="data" DataField="claimant"/>
                        <asp:BoundField HeaderText="Claim Type" HeaderStyle-CssClass="headerlink2" ItemStyle-CssClass="data" DataField="claim_type"/>
                        <asp:BoundField HeaderText="Claim Status" HeaderStyle-CssClass="headerlink2" ItemStyle-CssClass="data" DataField="claim_status"/>
                    </Columns>  
                    </asp:GridView>
		        </td>
		    </tr>
		    <tr>
		        <td>
		            <asp:Button ID="btnRefresh" runat="server" Text="<%$ Resources:btnRefresh %>"
                        CssClass="button" onclick="btnRefresh_Click"/>
		        </td>
		    </tr>
	    </table>
    </div>
    </form>
</body>
</html>
