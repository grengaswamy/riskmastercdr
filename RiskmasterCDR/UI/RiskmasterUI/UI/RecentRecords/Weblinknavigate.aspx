﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Weblinknavigate.aspx.cs"
    Inherits="Riskmaster.UI.UI.RecentRecords.Weblinknavigate" %>

<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc1" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="dg" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" >
    <title></title>
    <script type="text/javascript" src="../../Scripts/WaitDialog.js"></script>
    <script type="text/javascript">
        function GetScreenInfo() {
       
            var selectedscreenId = window.opener.document.getElementById("selectedScreenId");
            //PSARIN2 START: Handle Policy Module SYMBOL on claim
            if (selectedscreenId != null) {
                var selectedScreen = window.opener.document.getElementById(selectedscreenId.value);
                var iFrameDoc = (selectedScreen.contentDocument) ?
                 selectedScreen.contentDocument : selectedScreen.contentWindow.document;
                if (iFrameDoc.getElementById("multipolicyid") != null) {
                    var iDx = iFrameDoc.getElementById("multipolicyid").selectedIndex;
                    if (iDx != -1) {
                        var opt = iFrameDoc.getElementById("multipolicyid").options;
                        document.getElementById('txtPolicyId').value = opt[iDx].value;
                    }

                    //Handle for Policy Module SYMBOL on other screens.Need handling on business layer for other cases
                    else if (iFrameDoc.getElementById("policyid") != null) {
                        document.getElementById('txtPolicyId').value = iFrameDoc.getElementById("policyid").value;
                    }
                }
                //PSARIN2 END: Handle Policy Module SYMBOL
                var screeninfo = document.getElementById("tbScreenInfo");
                screeninfo.value = selectedscreenId.value.toString();

                if (document.getElementById("hdnDoRedirect").value == "true") {
                    window.location.href = document.getElementById("WebAddr").value;
                }
                else if (document.getElementById("hdnDoPostBack").value == "true") {
                    document.forms[0].submit();
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }

        }

        

    </script>
</head>
<body onload="pleaseWait.Show();GetScreenInfo();">
    <form id="form1" runat="server">
    <div>
        <dg:ErrorControl ID="ErrorControl1" runat="server" />
        <asp:TextBox Style="display: none" runat="server" ID="rowid" Text="" rmxref="Instance/Document/form/group/WebLink/rowId"
            rmxtype="hidden" />
        <asp:TextBox Style="display: none" runat="server" ID="WebAddr" Text="" rmxref="Instance/Document/form/group/WebLink/URL"
            rmxtype="hidden" />
            
        <asp:TextBox Style="display: none" runat="server" ID="tbScreenInfo" Text="" rmxtype="hidden" />
        <asp:HiddenField ID="hdnDoPostBack" runat="server" value="true"/>
          <asp:HiddenField ID="hdnDoRedirect" runat="server" value="false"/>
        <asp:TextBox Style="display: none" runat="server" ID="tbRecordId" Text="" rmxtype="hidden"  rmxref="Instance/Document/form/group/WebLink/RecordId" />
        <asp:TextBox Style="display: none" runat="server" ID="tbFormName" Text="" rmxtype="hidden"  rmxref="Instance/Document/form/group/WebLink/FormName" />
        <asp:TextBox Style="display: none" runat="server" ID="txtPolicyId" Text="" rmxtype="hidden"  rmxref="Instance/Document/form/group/WebLink/Policy" />
        <uc1:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    </div>
    </form>
</body>
</html>
