﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RecentEvents.aspx.cs" Inherits="Riskmaster.UI.UI.RecentRecords.RecentRecords" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Recent Saved Events</title>
</head>
<body onload="parent.MDIScreenLoaded();">
    <form id="frmRecentEvents" runat="server">
        <uc1:ErrorControl ID="ErrorControl" runat="server" />
        <table width ="100%">
        <tr>
			<td class="msgheader" nowrap="" colspan="">
				 <asp:Label ID="lblDisplaytext" runat="server" Text="<%$ Resources:lblDisplaytext %>"/> 
			</td>
		</tr>
		<tr>
		    <td>
	            <asp:GridView ID="grdRecentEvents" runat="server" CellPadding="4" 
                    AutoGenerateColumns="False" DataKeyNames="event_id" GridLines="None" 
                    onrowdatabound="grdRecentEvents_RowDataBound">
                    <HeaderStyle CssClass="colheader6" ForeColor="White"/>
                    <AlternatingRowStyle CssClass="data2" /> 
                    <Columns>
                       
                        <asp:BoundField HeaderText="<%$ Resources:gvHdrEventNumber %>" HeaderStyle-CssClass="headerlink2" ItemStyle-CssClass="data" DataField="event_number"/>
                        <asp:BoundField HeaderText="<%$ Resources:gvHdrDateofEvent %>" HeaderStyle-CssClass="headerlink2" ItemStyle-CssClass="data" DataField="date_of_event"/>
                        <asp:BoundField HeaderText="<%$ Resources:gvHdrDateReported %>" HeaderStyle-CssClass="headerlink2" ItemStyle-CssClass="data" DataField="date_reported"/>
                        <asp:BoundField HeaderText="<%$ Resources:gvHdrEventStatus %>" HeaderStyle-CssClass="headerlink2" ItemStyle-CssClass="data" DataField="event_status"/>
                        <asp:BoundField HeaderText="<%$ Resources:gvHdrEventType %>" HeaderStyle-CssClass="headerlink2" ItemStyle-CssClass="data" DataField="event_type"/>
                        <asp:BoundField HeaderText="<%$ Resources:gvHdrEventIndicator %>" HeaderStyle-CssClass="headerlink2" ItemStyle-CssClass="data" DataField="ev_indicator_code"/>
                        <asp:BoundField HeaderText="<%$ Resources:gvHdrDepartment %>" HeaderStyle-CssClass="headerlink2" ItemStyle-CssClass="data" DataField="department"/>
                    </Columns>  
                </asp:GridView>	        
		    </td>
		</tr>
		<tr>
	        <td align="left">           
	            <asp:Button ID="btnRefresh" runat="server" Text="<%$ Resources:btnRefresh %>" 
                    CssClass="button" onclick="btnRefresh_Click"/>
	        </td>
		</tr>
    </table>
    
    </form>
</body>
</html>
