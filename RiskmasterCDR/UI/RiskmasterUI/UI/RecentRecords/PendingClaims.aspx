﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PendingClaims.aspx.cs" Inherits="Riskmaster.UI.UI.RecentRecords.PendingClaims"  ValidateRequest="false" EnableViewState="true" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>

<!DOCTYPE html>

<html xmlns:ng="http://angularjs.org">
<head id="Head1" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
       <%-- <meta charset="utf-8">--%>
    <link rel="stylesheet" type="text/css" href="../../Content/ng-grid.css" />
    <script src="../../Scripts/jquery/jquery-1.8.0.min.js"></script>
    <script src="../../Scripts/jquery/jquery-ui-1.9.2.min.js"></script>
    <script src="../../Scripts/angularjs/angular.min.js"></script>
     <script type="text/javascript" src="../../Scripts/angularjs/Directives/rma-ng-grid.js"></script>
    <script type="text/javascript" src="../../Scripts/angularjs/lib/ng-grid.debug.js" ="../../Scripts/angularjs/ng-grid.debug.js"></script>
    <script type="text/javascript" src="../../Scripts/angularjs/ng-grid-layout.js"></script>    
    <script type="text/javascript" src="../../Scripts/angularjs/Controllers/PendingClaimsController.js"></script>   
    <script type="text/javascript" src="../../Scripts/angularjs/Directives/NumericInputDirective.js"></script>
        <script type="text/javascript" src="../../Scripts/angularjs/lib/ngGridExportPlugin.js"></script>
        
    <style>
        
.gridStyle {
    border: 1px solid rgb(212,212,212);
    width:99.9%;
    height: 310px!important;
    margin-top:10px;
}
.ngCanvas {
    min-height: 1px !important;
}

.k-grid-filter {
    float: right;
    /*margin: 1.8em -0.1em -0.4em;*/
    padding: 0.3em 0.2em 0.4em;
    position: relative;
    z-index: 1;
}

.k-icon {
    position: static;
    vertical-align: middle;
    background-image: url("../../images/Change-Sync.png");
    border-color: transparent;
    opacity: 0.9;
    background-color: transparent;
    background-repeat: no-repeat;
    display: inline-block;
    font-size: 0;
    height: 16px;
    line-height: 0;
    overflow: hidden;
    text-align: center;
    width: 16px;
}

.k-filter {
    /* background-position: -32px -80px;*/
}

    </style>
    <script>
        function DisbaleWaitDialog()
        {
            if ($("#pleaseWaitFrame", parent.document.body)[0].style.cssText == 'DISPLAY: block;' || 'display: block;')
                $("#pleaseWaitFrame", parent.document.body)[0].style.cssText = 'display: none;'
        }

    </script>
</head>

<body>
    <form runat="server"> 
        <div>
            <uc1:ErrorControl ID="ErrorControl" runat="server" />
        </div>
    </form>

 <asp:Label ID="lblErrors" class ="warntext "  runat="server"></asp:Label>   
 <div id="Browsercheck" runat="server">
     <div ng-app="rmaApp" id="ng-app">
        <div ng-controller="PendingClaimsController">
           <div class="msgheader"><asp:Label ID="lblPendingClaims" runat="server" Text="<%$ Resources:lblPendingClaims %>"> </asp:Label> {{totalServerItems}}</div>
           <div style="clear: both"></div>
           <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tbldata">
            <tr></tr>
                <tr>
                <td style="width:4%">
                    <span id="Span1" runat="server" width="100%"></span>
                </td>
                <td>
                    <uc1:errorcontrol id="ErrorControl1" runat="server" /> 
                </td>
            </tr>
            <tr>
                <td style="width:12%">
                   <asp:Label ID="lblShowAllClaims" runat="server" Text="<%$ Resources:lblShowAllClaims %>"> </asp:Label>
                <input type="checkbox" ng-model="check" ng-change="ShowAllCheckBox(check)" runat="server" id="chkShowAll">
                </td>
                <td>
                    <asp:Label ID="lblShowPendingFor" runat="server" Text="<%$ Resources:lblShowPendingFor %>"> </asp:Label>
                    <select ng-options="option.userName for option in ddSelectOptions " ng-model="ddlSelected" ng-change="ddlChangeEvent()"></select>
                </td>
            </tr>

                    
            <tr></tr>
            <tr  Width="100%" class="msgheader">
              <td colspan="2" >
                   <span id="lblcaption" Width="100%"></span>
              </td>
            </tr>
        </table>
            <div style="clear: both"></div>

            <!-- GRID Template-->
            
             <input type="hidden" id="hdndropdownData" runat="server" />
             <input type="hidden" id="hdnJsonData" runat="server" />
            <input type="hidden" id="hdnJsonUserPref" runat="server" />
            <input type="hidden" id="hdnJsonAdditionalData" runat="server" />
            <rma-grid id="gridPenClaims"  name="PendingClaims" multiSelect="false" showselectioncheckbox="false" ></rma-grid>
            <!-- GRID Template-->
             <div style="text-align:center;" >
             <label style="display: none;" id="lblEmptyData" />
             </div>
            <!-- End CUSTOMIZATION-->
            <div style="clear: both"></div>
            <table width="100%" border="0" cellspacing="0" cellpadding="4" id="tblback">
			    <tr>
				   <td>
		              </td>
				</tr>
			</table>
            <div class="footer">
                <iframe id="csvDownloadFrame" style="display:none" />
            </div>
      </div>
    </div>
     </div>
 
</body>
</html>
