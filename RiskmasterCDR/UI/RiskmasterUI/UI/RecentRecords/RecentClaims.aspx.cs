﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Data;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.UI.RecentRecords
{
    public partial class RecentClaims : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    BindGridWithRecentRecords();
                }
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            BindGridWithRecentRecords();
        }

        #region Private Methods

        private void BindGridWithRecentRecords()
        {
            string oMessageElement = string.Empty;
            string sReturn = string.Empty;
            XmlDocument resultDoc = null;
            DataSet resultDataSet = null;

            try
            {
                oMessageElement = GetMessageTemplate().ToString();
                sReturn = AppHelper.CallCWSService(oMessageElement);

                if (sReturn != string.Empty)
                {
                    resultDoc = new XmlDocument();
                    resultDoc.LoadXml(sReturn);

                    resultDataSet = ConvertXmlDocToDataSet(resultDoc);

                    if (resultDataSet != null)
                    {
                        if (resultDataSet.Tables.Count >= 3)
                        {
                            //Neha Setting the viesate of the Grid MITS 23633
                            SetViewState(resultDataSet.Tables[2]);
                            grdRecentClaims.DataSource = resultDataSet.Tables[3];
                            grdRecentClaims.DataBind();
                        }
                    }
                }
                else
                {
                    throw new ApplicationException("Error occured while getting Recent Claims");
                }
                
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        
        private DataSet ConvertXmlDocToDataSet(XmlDocument resultDoc)
        {
            DataSet dSet = null;
            try
            {
                dSet = new DataSet();
                dSet.ReadXml(new XmlNodeReader(resultDoc));

                return dSet;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }
        
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization></Authorization> 
                    <Call>
                        <Function>RecentRecordsAdaptor.GetRecentSavedRecords</Function> 
                    </Call>
                    <Document>
                        <recordtype>claim</recordtype>
                    </Document>
            </Message>
            ");

            return oTemplate;
        }

        protected void grdRecentClaims_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            // Neha Setting the header of the Grid MITS 23633 05-16-2011
            if (e.Row.RowType == DataControlRowType.Header)
            {
                SetHeaderCofig(e.Row);
            }//end Neha
            if(e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[0].Text = "<a href='#'>" + e.Row.Cells[0].Text + "</a>";
                e.Row.Cells[0].Attributes.Add("onclick", "parent.MDIShowScreen('" + DataBinder.Eval(e.Row.DataItem, "claim_id").ToString() + "','claim');return false;");
                e.Row.Cells[3].Text = AppHelper.GetDate(e.Row.Cells[3].Text);
                e.Row.Cells[4].Text = AppHelper.GetDate(e.Row.Cells[4].Text);
            }
        }

        
        /// <summary>
        ///Neha Setting the header of the Grid MITS 23633
        /// </summary>
        /// <param name="grdRow"></param>
        private void SetHeaderCofig(GridViewRow grdRow)
        {
            //setting visibility of columns on the basis of configure settings
            if (ViewState["chkClaimNumber"].ToString() == "1")
            {
                grdRecentClaims.Columns[0].Visible = true;
            }
            else
            {
                grdRecentClaims.Columns[0].Visible = false;
            }

            if (ViewState["chkLob"].ToString() == "1")
            {
                grdRecentClaims.Columns[1].Visible = true;
            }
            else
            {
                grdRecentClaims.Columns[1].Visible = false;
            }

            if (ViewState["chkEventNumber"].ToString() == "1")
            {
                grdRecentClaims.Columns[2].Visible = true;
            }
            else
            {
                grdRecentClaims.Columns[2].Visible = false;
            }

            if (ViewState["chkDateOfEvent"].ToString() == "1")
            {
                grdRecentClaims.Columns[3].Visible = true;
            }
            else
            {
                grdRecentClaims.Columns[3].Visible = false;
            }

            if (ViewState["chkDateOfClaim"].ToString() == "1")
            {
                grdRecentClaims.Columns[4].Visible = true;
            }
            else
            {
                grdRecentClaims.Columns[4].Visible = false;
            }

            if (ViewState["chkClaimant"].ToString() == "1")
            {
                grdRecentClaims.Columns[5].Visible = true;
            }
            else
            {
                grdRecentClaims.Columns[5].Visible = false;
            }

            if (ViewState["chkClaimType"].ToString() == "1")
            {
                grdRecentClaims.Columns[6].Visible = true;
            }
            else
            {
                grdRecentClaims.Columns[6].Visible = false;
            }

            if (ViewState["chkClaimStatus"].ToString() == "1")
            {
                grdRecentClaims.Columns[7].Visible = true;
            }
            else
            {
                grdRecentClaims.Columns[7].Visible = false;
            }


          
            //setting header texts on basis of configure settings
            grdRow.Cells[0].Text = ViewState["ClaimNumberHeader"].ToString();

            grdRow.Cells[1].Text = ViewState["LobHeader"].ToString();

            grdRow.Cells[2].Text = ViewState["EventNumberHeader"].ToString();

            grdRow.Cells[3].Text = ViewState["DateOfEventHeader"].ToString();

            grdRow.Cells[4].Text = ViewState["DateOfClaimHeader"].ToString();

            grdRow.Cells[5].Text = ViewState["ClaimantHeader"].ToString();

            grdRow.Cells[6].Text = ViewState["ClaimTypeHeader"].ToString();

            grdRow.Cells[7].Text = ViewState["ClaimStatusHeader"].ToString();
  
            //((LinkButton)grdRow.Cells[4].Controls[0]).Text = ViewState["noheaderselected"].ToString();


            if (ViewState["NoHeaderSelected"].ToString() == "True")
            {

                grdRecentClaims.Visible = false;

            }
            else
            {
                grdRecentClaims.Visible = true;


            }
        }
        /// <summary>
        /// Neha Setting the header of the Grid MITS 23633
        /// </summary>
        /// <param name="dTable"></param>
        private void SetViewState(DataTable dTable)
        {
            DataRow dRow = null;
            dRow = dTable.Rows[0];

            //added Neha Header Config setttings in GridView starts
            try
            {
                ViewState["chkClaimNumber"] = dRow["ClaimNumber"].ToString();
            }
            catch
            {
                ViewState["chkClaimNumber"] = "";
            }

            try
            {
                ViewState["chkLob"] = dRow["Lob"].ToString();
            }
            catch
            {
                ViewState["chkLob"] = "";
            }

            try
            {
                ViewState["chkEventNumber"] = dRow["EventNumber"].ToString();
            }
            catch
            {
                ViewState["chkEventNumber"] = "";
            }

            try
            {
                ViewState["chkDateOfEvent"] = dRow["DateOfEvent"].ToString();
            }
            catch
            {
                ViewState["chkDateOfEvent"] = "";
            }

            try
            {
                ViewState["chkDateOfClaim"] = dRow["DateOfClaim"].ToString();
            }
            catch
            {
                ViewState["chkDateOfClaim"] = "";
            }

            try
            {
                ViewState["chkClaimant"] = dRow["Claimant"].ToString();
            }
            catch
            {
                ViewState["chkClaimant"] = "";
            }
            try
            {
                ViewState["chkClaimType"] = dRow["ClaimType"].ToString();
            }
            catch
            {
                ViewState["chkClaimType"] = "";
            }
            try
            {
                ViewState["chkClaimStatus"] = dRow["ClaimStatus"].ToString();
            }
            catch
            {
                ViewState["chkClaimStatus"] = "";
            }

            try
            {
                ViewState["ClaimNumberHeader"] = dRow["ClaimNumberHeader"].ToString();
            }
            catch
            {
                ViewState["ClaimNumberHeader"] = "";
            }

            try
            {
                ViewState["LobHeader"] = dRow["LobHeader"].ToString();
            }
            catch
            {
                ViewState["LobHeader"] = "";
            }

            try
            {
                ViewState["EventNumberHeader"] = dRow["EventNumberHeader"].ToString();
            }
            catch
            {
                ViewState["EventNumberHeader"] = "";
            }

            try
            {
                ViewState["DateOfEventHeader"] = dRow["DateOfEventHeader"].ToString();
            }
            catch
            {
                ViewState["DateOfEventHeader"] = "";
            }

            try
            {
                ViewState["DateOfClaimHeader"] = dRow["DateOfClaimHeader"].ToString();
            }
            catch
            {
                ViewState["DateOfClaimHeader"] = "";
            }

            try
            {
                ViewState["ClaimantHeader"] = dRow["ClaimantHeader"].ToString();
            }
            catch
            {
                ViewState["ClaimantHeader"] = "";
            }

            try
            {
                ViewState["ClaimTypeHeader"] = dRow["ClaimTypeHeader"].ToString();
            }
            catch
            {
                ViewState["ClaimTypeHeader"] = "";
            }

            try
            {
                ViewState["NoHeaderSelected"] = dRow["NoHeaderSelected"].ToString();
            }
            catch
            {
                ViewState["NoHeaderSelected"] = "";
            }

            try
            {
                ViewState["ClaimStatusHeader"] = dRow["ClaimStatusHeader"].ToString();
            }
            catch
            {
                ViewState["ClaimStatusHeader"] = "";
            }

            //added by nitin for R6 implementation fo Header Config setttings in GridView ends

        }
        #endregion
    }
}
