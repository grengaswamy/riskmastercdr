﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Xml;
using Riskmaster.Common;
using Telerik.Web.UI;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.ClaimLookups
{
    public partial class LookUpClaimPolicy : NonFDMBasePageCWS
    {
        private string pageID = RMXResourceProvider.PageId("LookUpClaimPolicy.aspx"); 
        protected void Page_Load(object sender, EventArgs e)
        {
         
            XElement XmlTemplate = null;
           
            if (!IsPostBack)
            {
                gvPolicyList.HeaderStyle.CssClass = "msgheader";
                //gvPolicyList.RowStyle.CssClass = "datatd1";
                //gvPolicyList.AlternatingRowStyle.CssClass = "datatd";
                this.gvPolicyList.PagerStyle.PagerTextFormat = Server.HtmlDecode("{4} <strong>{3}</strong> "+AppHelper.GetResourceValue(pageID,"lblItems","0") +" <strong>{1}</strong> "+ AppHelper.GetResourceValue(pageID,"lblPages","0"));
                // Read the Values from Querystring
                string sClaimType = AppHelper.GetQueryStringValue("claimtype");
                string sDateofEvent = AppHelper.GetQueryStringValue("evdate");
                string sDateofClaim = AppHelper.GetQueryStringValue("claimdate");
                string sDeptID = AppHelper.GetQueryStringValue("deptid");
                
                    
                int iLOB = 0;
                if (AppHelper.GetQueryStringValue("LOB") != "")
                    iLOB = Int32.Parse(AppHelper.GetQueryStringValue("LOB"));
                //changed by Gagan for Policy Tracking Safeway Retrofit : start
                int iJurisdictionId = 0;
                if (AppHelper.GetQueryStringValue("JurisdictionId") != "")
                    iJurisdictionId = Int32.Parse(AppHelper.GetQueryStringValue("JurisdictionId"));
                //changed by Gagan for Policy Tracking Safeway Retrofit : end

                int iEventState = 0;
                if (AppHelper.GetQueryStringValue("EventState") != "")
                    iEventState = Int32.Parse(AppHelper.GetQueryStringValue("EventState"));
                //Added by Amitosh for Mits 25163 for Policy interface
                string sCurrencyType = AppHelper.GetQueryStringValue("CurrencyType");

                int iTaggedPolicyId = 0;
                if (AppHelper.GetQueryStringValue("TaggedPolicyId") != "")
                    iTaggedPolicyId = Int32.Parse(AppHelper.GetQueryStringValue("TaggedPolicyId"));

                    ViewState["ClaimType"] = sClaimType;
                    ViewState["DateofClaim"] = sDateofClaim;
                    ViewState["DateofEvent"] = sDateofEvent;
                    ViewState["DeptID"] = sDeptID;
                    ViewState["LOB"]=iLOB;
                    ViewState["JurisdictionId"] = iJurisdictionId;
                    ViewState["TaggedPolicyId"] = iTaggedPolicyId;
                    ViewState["CurrencyType"]= sCurrencyType;
                    ViewState["EventState"] = iEventState;

                //End Amitosh
                if (sClaimType != "" && sDateofClaim != "" && sDateofEvent != "" && sDeptID != "" && iLOB != 0)
                {
                        //changed by Gagan for Policy Tracking Safeway Retrofit : start
                    //Deb  ML changes
                    sDateofClaim = AppHelper.GetDateInenUS(sDateofClaim);
                    sDateofEvent = AppHelper.GetDateInenUS(sDateofEvent);
                    //Deb  ML changes
                    XmlTemplate = GetMessageTemplate(sClaimType, sDateofClaim, sDateofEvent, sDeptID, iLOB, iJurisdictionId, iTaggedPolicyId, "1", sCurrencyType,iEventState);//changed by rkaur7- added new field 'iJurisdictionId' on 05/26/2009- MITS 16668
                        //changed by Gagan for Policy Tracking Safeway Retrofit : end
                    CreateServiceCall(XmlTemplate);
                }
                 else
                {
                    gvPolicyList.Visible = false;
                    btnCancel.Visible = false;
                    btnNoPolicy.Visible = false;
                    //Commented by Amitosh for not calling javascript
                    //lblMessage.Text = "<p class='gen' align='center'><br><br><br><h3 align='center'>&nbsp;&nbsp;&nbsp;&nbsp;No policies could be found for this claim!</h3><script> window.onclose=NoPolicy()</script></p>";
                    lblMessage.Text = "<p class='gen' align='center'><br><br><br><h3 align='center'>&nbsp;&nbsp;&nbsp;&nbsp;" + AppHelper.GetResourceValue(pageID, "lblNoPoliciesForClaim", "0") + "</h3></p>";
                }
            }
        }

        private void CreateServiceCall(XElement XmlTemplate)
        {
            bool bReturnStatus = false;
            XmlDocument XmlDoc = new XmlDocument();
            string sreturnValue = "";
            int iTaggedPolicyId = 0;
            int iPageCount = 0;
                  bReturnStatus = CallCWSFunction("ClaimPolicyAdaptor.Get", out sreturnValue, XmlTemplate);
                    
                 iTaggedPolicyId =  Convert.ToInt32(ViewState["TaggedPolicyId"]);

                    if (bReturnStatus)
                    {
                        XmlDoc.LoadXml(sreturnValue);

                        XmlNodeList policyRecords = XmlDoc.SelectNodes("//policies/policy");
                        XmlNode policies = XmlDoc.SelectSingleNode("//policies");
                        AttachMultiplePolicy.Value = policies.Attributes["AttachMultiplePolicy"].Value;
                        string sUsePolicyInterface = policies.Attributes["UsePolicyInterface"].Value;
                        ViewState["sUsePolicyInterface"] = sUsePolicyInterface;
                    
                        ViewState["PageNumber"] = policies.Attributes["PageNumber"].Value;
                        gvPolicyList.VirtualItemCount =Convert.ToInt32(policies.Attributes["RecordCount"].Value);

                        
                        //rsushilaggar MITS 28648 Date 06/13/2012
                        iPageCount = gvPolicyList.VirtualItemCount / gvPolicyList.PageSize;
                        if (gvPolicyList.VirtualItemCount % gvPolicyList.PageSize > 0)
                            iPageCount = iPageCount + 1;
                        ViewState["lastpage"] = iPageCount;

                        if (policyRecords.Count != 0)
                        {
                            DataTable dtGridData = new DataTable();
                            DataColumn dcGridColumn;
                            XmlNode xNode;
                            DataRow objRow;
                            
                            dtGridData.Columns.Add("Policy Id");
                            dtGridData.Columns.Add("Policy Number");
                            dtGridData.Columns.Add("Policy Name");
                            dtGridData.Columns.Add("Insurer");
                            dtGridData.Columns.Add("Effective Date");
                            dtGridData.Columns.Add("Expiration Date");
                            dtGridData.Columns.Add("Limit/Occur.");
                            dtGridData.Columns.Add("Applied %");
                            dtGridData.Columns.Add("Comments");
                            dtGridData.Columns.Add("PolicySystem");
                            
                           // // Column to be associated with Datasource of grid
                          // dcGridColumn = new DataColumn();

                    //       dcGridColumn.ColumnName = "Policy Id";

                           // // Add Column to Data Table
                         //  dtGridData.Columns.Add(dcGridColumn);

                           // // Column to be associated with the Grid
                           // //bfGridColumn = new BoundField();
                           // bfGridColumn = new GridBoundColumn();

                           // // Associate the Grid Bound Column with the Column of the Datasource
                           // bfGridColumn.DataField = dcGridColumn.ColumnName;

                           // // Assign the HeaderText
                           // bfGridColumn.HeaderText = dcGridColumn.ColumnName;
                           // bfGridColumn.Visible = false;
                           // // Add the newly created bound field to the GridView. 
                           // gvPolicyList.Columns.Add(bfGridColumn);

                           // // Policy Number

                           // // Column to be associated with Datasource of grid
                           // dcGridColumn = new DataColumn();

                           // dcGridColumn.ColumnName = "Policy Number";

                           // // Add Column to Data Table
                           // dtGridData.Columns.Add(dcGridColumn);

                           // // Column to be associated with the Grid
                           // //bfGridColumn = new BoundField();
                           // bfGridColumn = new GridBoundColumn();

                           // // Associate the Grid Bound Column with the Column of the Datasource
                           // bfGridColumn.DataField = dcGridColumn.ColumnName;

                           // // Assign the HeaderText
                           // bfGridColumn.HeaderText = dcGridColumn.ColumnName;

                           // // Add the newly created bound field to the GridView. 
                           // gvPolicyList.Columns.Add(bfGridColumn);


                           // // Column to be associated with Datasource of grid
                           // dcGridColumn = new DataColumn();

                           // // Assign the Column Name from XmlNode
                           // dcGridColumn.ColumnName = "Policy Name";

                           // // Add Column to Data Table
                           // dtGridData.Columns.Add(dcGridColumn);

                            
                           //// butGridLinkColumn = new ButtonField();
                           // butGridLinkColumn = new GridButtonColumn();

                           // // Associate the Grid Bound Column with the Column of the Datasource
                           // butGridLinkColumn.DataTextField = dcGridColumn.ColumnName;
                           // butGridLinkColumn.da = dcGridColumn.ColumnName;

                           // // Assign the HeaderText
                           // butGridLinkColumn.HeaderText = dcGridColumn.ColumnName;
                            
                           // // Create the hyperlink for the column
                           // butGridLinkColumn.ButtonType = Telerik.Web.UI.GridButtonColumnType.LinkButton; 

                           // gvPolicyList.Columns.Add(butGridLinkColumn);

                           // // Column to be associated with Datasource of grid
                           // dcGridColumn = new DataColumn();

                           // dcGridColumn.ColumnName = "Insurer";

                           // // Add Column to Data Table
                           // dtGridData.Columns.Add(dcGridColumn);

                           // // Column to be associated with the Grid
                           // //bfGridColumn = new BoundField();
                           // bfGridColumn = new GridBoundColumn();

                           // // Associate the Grid Bound Column with the Column of the Datasource
                           // bfGridColumn.DataField = dcGridColumn.ColumnName;

                           // // Assign the HeaderText
                           // bfGridColumn.HeaderText = dcGridColumn.ColumnName;

                           // // Add the newly created bound field to the GridView. 
                           // gvPolicyList.Columns.Add(bfGridColumn);

                           // // Column to be associated with Datasource of grid
                           // dcGridColumn = new DataColumn();

                           // dcGridColumn.ColumnName = "Eff. Date (From & To)";


                           // // Add Column to Data Table
                           // dtGridData.Columns.Add(dcGridColumn);

                           // // Column to be associated with the Grid
                           // //bfGridColumn = new BoundField();
                           // bfGridColumn = new GridBoundColumn();

                           // // Associate the Grid Bound Column with the Column of the Datasource
                           // bfGridColumn.DataField = dcGridColumn.ColumnName;
                            
                           // // Assign the HeaderText
                           // bfGridColumn.HeaderText = dcGridColumn.ColumnName;

                           // // Add the newly created bound field to the GridView. 
                           // gvPolicyList.Columns.Add(bfGridColumn);
                            
                           // // Column to be associated with Datasource of grid
                           // dcGridColumn = new DataColumn();

                           // dcGridColumn.ColumnName = "Limit/Occur.";

                           // // Add Column to Data Table
                           // dtGridData.Columns.Add(dcGridColumn);

                           // // Column to be associated with the Grid
                           // //bfGridColumn = new BoundField();
                           // bfGridColumn = new GridBoundColumn();

                           // // Associate the Grid Bound Column with the Column of the Datasource
                           // bfGridColumn.DataField = dcGridColumn.ColumnName;

                           // // Assign the HeaderText
                           // bfGridColumn.HeaderText = dcGridColumn.ColumnName;

                           // // Add the newly created bound field to the GridView. 
                           // gvPolicyList.Columns.Add(bfGridColumn);
                            
                           // // Column to be associated with Datasource of grid
                           // dcGridColumn = new DataColumn();

                           // dcGridColumn.ColumnName = "Applied %";

                           // // Add Column to Data Table
                           // dtGridData.Columns.Add(dcGridColumn);

                           // // Column to be associated with the Grid
                           // //bfGridColumn = new BoundField();
                           // bfGridColumn = new GridBoundColumn();

                           // // Associate the Grid Bound Column with the Column of the Datasource
                           // bfGridColumn.DataField = dcGridColumn.ColumnName;

                           // // Assign the HeaderText
                           // bfGridColumn.HeaderText = dcGridColumn.ColumnName;

                           // // Add the newly created bound field to the GridView. 
                           // gvPolicyList.Columns.Add(bfGridColumn);

                           // // Policy Comments          //csingh7 R6PS1 ENH

                           // // Column to be associated with Datasource of grid
                           // dcGridColumn = new DataColumn();

                           // dcGridColumn.ColumnName = "Comments";

                           // // Add Column to Data Table
                           // dtGridData.Columns.Add(dcGridColumn);

                           // // Column to be associated with the Grid
                           // // butGridLinkColumn = new ButtonField();
                           // butGridLinkColumn = new GridButtonColumn();

                           // // Associate the Grid Bound Column with the Column of the Datasource
                           // butGridLinkColumn.DataTextField = "Comments";

                           // // Assign the HeaderText
                           // butGridLinkColumn.HeaderText = dcGridColumn.ColumnName;

                           // // Create the hyperlink for the column
                           // butGridLinkColumn.ButtonType = Telerik.Web.UI.GridButtonColumnType.LinkButton; 

                           // gvPolicyList.Columns.Add(butGridLinkColumn);

                           // //csingh7 R6PS1 ENH
                           for (int i = 0; i < policyRecords.Count; i++)
                           {
                               xNode = policyRecords[i];
                               objRow = dtGridData.NewRow();
                               objRow["Policy Id"] = xNode.Attributes["policyid"].Value;
                               objRow["Policy Number"] = xNode.Attributes["policynumber"].Value;
                               objRow["Policy Name"] = xNode.Attributes["policyname"].Value;
                               objRow["Insurer"] = xNode.Attributes["insurer"].Value;
                               objRow["Effective Date"] = xNode.Attributes["effectivedate"].Value;
                               objRow["Expiration Date"]  = xNode.Attributes["expirationdate"].Value;
                               objRow["Limit/Occur."] = xNode.Attributes["occurencelimit"].Value;
                               objRow["Applied %"] = xNode.Attributes["app_proc"].Value;
                               if (xNode.Attributes["comments"].Value == "true") //csingh7 MITS 20092
                                   objRow["Comments"] = "Comments";
                               else
                                   objRow["Comments"] = "";
                               dtGridData.Rows.Add(objRow);
                               if(xNode.Attributes["policysystem"] !=null)
                               objRow["PolicySystem"] = xNode.Attributes["policysystem"].Value;
                               
                           }
                           gvPolicyList.DataSource = dtGridData;
                           gvPolicyList.DataBind();
                           ViewState["dtGridData"] = dtGridData;
                        }
                        else
                        {
                            if (iTaggedPolicyId == 0)
                            {
                                gvPolicyList.Visible = false;
                                btnCancel.Visible = false;
                                btnNoPolicy.Visible = false;
                                //Commented by Amitosh for not calling javascript
                                //lblMessage.Text = "<p class='gen' align='center'><br><br><br><h3 align='center'>&nbsp;&nbsp;&nbsp;&nbsp;No policies could be found for this claim!</h3><script> window.onclose=NoPolicy()</script></p>";
                                lblMessage.Text = "<p class='gen' align='center'><br><br><br><h3 align='center'>&nbsp;&nbsp;&nbsp;&nbsp;"+ AppHelper.GetResourceValue(pageID, "lblNoPoliciesForClaim", "0") +"</h3></p>";
                            }
                            else
                            {
                                gvPolicyList.Visible = false;
                                btnCancel.Visible = false;
                                btnNoPolicy.Visible = false;
                                //Commented by Amitosh for not calling javascript
                                //lblMessage.Text = "<p class='gen' align='center'><br><br><br><h3 align='center'>&nbsp;&nbsp;&nbsp;&nbsp;Policy cannot be tagged to  this claim!</h3><script> window.onclose=NoPolicy()</script></p>";
                                lblMessage.Text = "<p class='gen' align='center'><br><br><br><h3 align='center'>&nbsp;&nbsp;&nbsp;&nbsp;" + AppHelper.GetResourceValue(pageID, "lblPolicyTag", "0") + "</h3></p>";
                            }
                        }

                    }
                }
               
           

        protected void gvPolicyList_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridPagerItem)
            {
                GridPagerItem pager = (GridPagerItem)e.Item;
                Label lbl = (Label)pager.FindControl("ChangePageSizeLabel");
                lbl.Visible = false;

                RadComboBox combo = (RadComboBox)pager.FindControl("PageSizeComboBox");
                combo.Visible = false;
            }
            if (e.Item is GridHeaderItem)
            {
                if (ViewState["sUsePolicyInterface"].ToString() == "False")
                {
                    gvPolicyList.Columns[9].Visible = false;
                }
            }
            if (e.Item is GridDataItem)
            {
                    //string txtPolicyId = string.Empty;
                    //string txtPolicyNumber = string.Empty;
                    //string txtPolicyName = string.Empty;
                    //string txtInsurer = string.Empty;
                    //string txtEffDate = string.Empty;
                    //string txtLimit = string.Empty;
                    //string txtApplied = string.Empty;
                    //string txtComments = string.Empty;
                    //string txtPolicySystem = string.Empty;
                    

                    Label gridLblPolicyId = null;
                    Label gridPolicyNumber = null;
                    LinkButton gridPolicyName = null;
                    Label gridInsurer = null;
                    Label gridEffDate= null;
                    Label gridExpDate = null;
                    Label gridLimit = null;
                    Label gridApplied = null;
                    LinkButton gridComments = null;
                    Label gridPolicySystem = null;

                    gridLblPolicyId = (Label)e.Item.Cells[0].FindControl("lblPolicyId");
                    try
                    {
                        gridLblPolicyId.Text = DataBinder.Eval(e.Item.DataItem, "Policy Id").ToString();
                    }
                    catch (System.Web.HttpException)
                    {
                        gridLblPolicyId.Text = "";
                    }

                    gridPolicyNumber = (Label)e.Item.Cells[1].FindControl("lblPolicyNumber");

                    try
                    {
                        gridPolicyNumber.Text = DataBinder.Eval(e.Item.DataItem, "Policy Number").ToString();
                    }
                    catch (System.Web.HttpException)
                    {
                        gridPolicyNumber.Text = "";
                    }
                    gridEffDate = (Label)e.Item.Cells[4].FindControl("lblEffDate");

                    try
                    {
                        gridEffDate.Text = DataBinder.Eval(e.Item.DataItem, "Effective Date").ToString();
                    }
                    catch (System.Web.HttpException)
                    {
                        gridEffDate.Text = "";
                    }

                    gridExpDate = (Label)e.Item.Cells[4].FindControl("lblExpDate");

                    try
                    {
                        gridExpDate.Text = DataBinder.Eval(e.Item.DataItem, "Expiration Date").ToString();
                    }
                    catch (System.Web.HttpException)
                    {
                        gridExpDate.Text = "";
                    }
                      gridPolicyName = (LinkButton)e.Item.Cells[2].FindControl("lblPolicyName");

                    try
                    {
                        gridPolicyName.Text = DataBinder.Eval(e.Item.DataItem, "Policy Name").ToString();

                         gridPolicyName.Attributes.Add("href", "#");

                         gridPolicyName.Attributes.Add("onclick", "selectPolicy(" + DataBinder.Eval(e.Item.DataItem, "Policy Id").ToString() + ",'" + escapeStrings(gridPolicyName.Text) + "');");

                    }
                    catch (System.Web.HttpException)
                    {
                        gridPolicyName.Text = "";
                    }
                    
                     gridInsurer = (Label)e.Item.Cells[3].FindControl("lblInsurer");

                    try
                    {
                        gridInsurer.Text = DataBinder.Eval(e.Item.DataItem, "Insurer").ToString();
                    }
                    catch (System.Web.HttpException)
                    {
                        gridInsurer.Text = "";
                    }



                    //gridEffDate = (Label)e.Item.Cells[4].FindControl("lblEffDate");

                    //try
                    //{
                    //    gridEffDate.Text = DataBinder.Eval(e.Item.DataItem, "Effective Date").ToString();
                    //}
                    //catch (System.Web.HttpException)
                    //{
                    //    gridEffDate.Text = "";
                    //}

                    //gridExpDate = (Label)e.Item.Cells[4].FindControl("lblExpDate");

                    //try
                    //{
                    //    gridExpDate.Text = DataBinder.Eval(e.Item.DataItem, "Expiration Date").ToString();
                    //}
                    //catch (System.Web.HttpException)
                    //{
                    //    gridExpDate.Text = "";
                    //}

                    gridLimit = (Label)e.Item.Cells[5].FindControl("lblLimit");

                    try
                    {
                        gridLimit.Text = DataBinder.Eval(e.Item.DataItem, "Limit/Occur.").ToString();
                    }
                    catch (System.Web.HttpException)
                    {
                        gridLimit.Text = "";
                    }


                    gridApplied = (Label)e.Item.Cells[6].FindControl("lblApplied");

                    try
                    {
                        gridApplied.Text = DataBinder.Eval(e.Item.DataItem, "Applied %").ToString();
                    }
                    catch (System.Web.HttpException)
                    {
                        gridApplied.Text = "";
                    }

                    gridComments = (LinkButton)e.Item.Cells[7].FindControl("lblComments");

                    try
                    {
                        gridComments.Text = DataBinder.Eval(e.Item.DataItem, "comments").ToString();
                         if (gridComments.Text == "Comments")
                        {
                    gridComments.Attributes.Add("href", "#");
                    gridComments.Attributes.Add("onclick", "return LookupComments(" + e.Item.Cells[0].Text + ");");
                          }

                    }
                    catch (System.Web.HttpException)
                    {
                        gridComments.Text = "";
                    }

                     gridPolicySystem = (Label)e.Item.Cells[8].FindControl("lblPolicySystem");

                    try
                    {
                        gridPolicySystem.Text = DataBinder.Eval(e.Item.DataItem, "PolicySystem").ToString();
                    }
                    catch (System.Web.HttpException)
                    {
                        gridPolicySystem.Text = "";
                    }


            }
        }

        private XElement GetMessageTemplate(string sClaimType, string sDateofClaim, string sDateofEvent, string sDeptID, int iLOB,int iJurisdiction,int iTaggedPolicy,string sPageNumber,string sCurrencyType,int iEventState) //added by rkaur7 on 05/26/2009 - new parameter iJurisdiction -MITS 16668
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>409b8705-c38d-4216-adfe-c1f6397196dd</Authorization>");
            sXml = sXml.Append("<Call><Function>ClaimPolicyAdaptor.Get</Function></Call><Document><Document><ClaimTypeCode>");
            sXml = sXml.Append(sClaimType);
            sXml = sXml.Append("</ClaimTypeCode><EventDate>");
            sXml = sXml.Append(sDateofEvent);
            sXml = sXml.Append("</EventDate><ClaimDate >");
            sXml = sXml.Append(sDateofClaim);
            sXml = sXml.Append("</ClaimDate><DeptEID>");
            sXml = sXml.Append(sDeptID);
            sXml = sXml.Append("</DeptEID><LOB>");
            sXml = sXml.Append(iLOB);
            sXml = sXml.Append("</LOB>");                
            //changed by Gagan for Policy Tracking Safeway Retrofit : start
            sXml = sXml.Append("<Jurisdiction>");
            sXml = sXml.Append(iJurisdiction);
            sXml = sXml.Append("</Jurisdiction>");
            sXml = sXml.Append("<TaggedPolicyId>");
            sXml = sXml.Append(iTaggedPolicy);
            sXml = sXml.Append("</TaggedPolicyId>");
            sXml = sXml.Append("<CurrencyType>");
            sXml = sXml.Append(sCurrencyType);
            sXml = sXml.Append("</CurrencyType>");
            sXml = sXml.Append("<EventState>");
            sXml = sXml.Append(iEventState);
            sXml = sXml.Append("</EventState>");
            sXml = sXml.Append("<PageNumber>");
            sXml = sXml.Append(sPageNumber);
            sXml = sXml.Append("</PageNumber>");
            sXml = sXml.Append("</Document></Document></Message>");
            //changed by Gagan for Policy Tracking Safeway Retrofit : start

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

    
        protected void gvPolicyList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                DataRowView drvGridRow = (DataRowView)e.Row.DataItem;
                TableCell tcGridCell;
                LinkButton lbLinkColumn;

                    tcGridCell = e.Row.Cells[2];
                    lbLinkColumn = (LinkButton)tcGridCell.Controls[0];

                    // href is assigned the value # to stop the postback on click.
                    lbLinkColumn.Attributes.Add("href", "#");

                    lbLinkColumn.Attributes.Add("onclick", "selectPolicy(" + e.Row.Cells[0].Text + ",'" + escapeStrings(lbLinkColumn.Text) + "');");

                    tcGridCell = e.Row.Cells[7];        //csingh7 for MITS 20092
                    lbLinkColumn = (LinkButton)tcGridCell.Controls[0];
                    if (lbLinkColumn.Text == "Comments")
                    {
                        lbLinkColumn.Attributes.Add("href", "#");
                        lbLinkColumn.Attributes.Add("onclick", "return LookupComments(" + e.Row.Cells[0].Text + ");");
                    }
            }
        }
        
        protected void gvPolicyList_DataBound(object sender, EventArgs e)
        {
            gvPolicyList.Columns[0].HeaderStyle.CssClass = "hiderowcol";
            gvPolicyList.Columns[0].ItemStyle.CssClass = "hiderowcol";
        }
        //MITS 14182:ybhaskar: Esc-char function added so that policy with apostrophe in name can be attached
        protected string escapeStrings(string sCode)
        {

            sCode = sCode.Replace("'", "\\'");
            return sCode;

        }

        protected void gvPolicyList_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {


            if (ViewState["dtGridData"] != null)
            {
                try
                {
                    DataTable dtGridData = (DataTable)ViewState["dtGridData"];

                    gvPolicyList.DataSource = dtGridData;
                }
                catch
                {
                    gvPolicyList.Visible = false;
                  //  tblNoRecords.Visible = true;
                    // tblTxtArea.Visible = false;
                }
            }

        }

        protected void gvPolicyList_PreRender(object sender, EventArgs e)
        {
            GridFilterMenu menu = gvPolicyList.FilterMenu;
            int i = 0;
            while (i < menu.Items.Count)
            {
                if (menu.Items[i].Text == "Between" || menu.Items[i].Text == "NotBetween" || menu.Items[i].Text == "IsNull" || menu.Items[i].Text == "NotIsNull")
                {
                    menu.Items.RemoveAt(i);
                }
                else
                {
                    i = (i + 1);
                }
            }
        }

        protected void gvPolicyList_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            string sCommandArg = null;
            string sPageNumber = string.Empty;
            XElement XMLTemplate = null;

           
                try
                {
                    sCommandArg = ((System.Web.UI.WebControls.LinkButton)(((Telerik.Web.UI.GridCommandEventArgs)(e)).CommandSource)).CommandArgument;
                }
                catch
                {
                    sCommandArg = ((System.Web.UI.WebControls.Button)(((Telerik.Web.UI.GridCommandEventArgs)(e)).CommandSource)).CommandArgument;
                }

                
                switch (sCommandArg)
                {
                    case "Next":
                        sPageNumber = (Convert.ToInt32(ViewState["PageNumber"]) + 1).ToString();//rsushilaggar MITS 28648 Date 06/13/2012
                        break;
                    case "Previous":
                        sPageNumber = (Convert.ToInt32(ViewState["PageNumber"]) - 1).ToString();//rsushilaggar MITS 28648 Date 06/13/2012
                        break;
                    case "First":
                        sPageNumber = "1";
                        break;
                    case "Last":
                        sPageNumber = ViewState["lastpage"].ToString();
                        break;
                    default:
                        int iPageIndex = e.NewPageIndex + 1;
                        sPageNumber = iPageIndex.ToString();
                        break;
                }
                //rsushilaggar MITS 28648 Date 06/13/2012
                ViewState["PageNumber"] = sPageNumber;

                XMLTemplate = GetMessageTemplate(ViewState["ClaimType"].ToString(), ViewState["DateofClaim"].ToString(), ViewState["DateofEvent"].ToString(), ViewState["DeptID"].ToString(), Convert.ToInt32(ViewState["LOB"]), Convert.ToInt32(ViewState["JurisdictionId"]), Convert.ToInt32(ViewState["TaggedPolicyId"]), ViewState["PageNumber"].ToString(), ViewState["CurrencyType"].ToString(), Convert.ToInt32(ViewState["EventState"]));
               CreateServiceCall(XMLTemplate); 

            }
        public string ClaimDateCheck { get; set; }           
        }

    }

