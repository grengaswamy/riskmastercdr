﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LookUpClaimPlan.aspx.cs" Inherits="Riskmaster.UI.ClaimLookups.LookUpClaimPlan.LookUpClaimPlan" enableEventValidation="false"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">    
    <title>Plans</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
    function handleUnload()
	{
	    if(window.opener!=null)
			window.opener.onCodeClose();
	}
	
	function selectPlan(lPlanId, sPlanName,sClassNames,sDttmRcdLastUpd)
	{
	    if(window.opener!=null)
		    window.opener.SetClaimPolicy(lPlanId,sPlanName,sClassNames,sDttmRcdLastUpd);
	}
	function handleOnload()
	{
    	window.opener.setFieldName();    	
 	}	
	</script>
</head>
<body class="10pt" onload="handleOnload();" onunload="handleOnload();">
    <form id="frmData" runat="server" >
        <table>
            <tr>
                <td>
                    <asp:gridview ID="gvPlanList" runat="server" 
                        onrowdatabound="gvPlanList_RowDataBound" AutoGenerateColumns="False" 
                        ondatabound="gvPlanList_DataBound" CssClass="singleborder" Width="525px">
                        <HeaderStyle HorizontalAlign="Left" />
                        </asp:gridview>
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </td>
            </tr>
            <tr align="center">
                <td>
                    <asp:TextBox  runat="server" id="hdnClaimTypeCode" rmxref="Instance/Document/ClaimPlan/ClaimTypeCode" style="display:none"></asp:TextBox>
                    <asp:TextBox runat="server" id="hdnEventDate" rmxref="Instance/Document/ClaimPlan/EventDate" style="display:none"></asp:TextBox>
                    <asp:TextBox runat="server" id="hdnClaimDate" rmxref="Instance/Document/ClaimPlan/ClaimDate" style="display:none"></asp:TextBox>
                    <asp:TextBox runat="server" id="hdnDeptEID" rmxref="Instance/Document/ClaimPlan/DeptEID" style="display:none"></asp:TextBox>                  
                </td>
            </tr>
        </table>
    </form>
</body>


</html>
