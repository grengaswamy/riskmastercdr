﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.RMXResourceManager; //MITS 34141 - ksahu5

namespace Riskmaster.UI.ClaimLookups.LookUpClaimPlan
{
    public partial class LookUpClaimPlan : NonFDMBasePageCWS
    {        
        private string pageID = RMXResourceProvider.PageId("LookUpClaimPlan.aspx"); //MITS 34141 - ksahu5
        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturnStatus = false;            
            XmlDocument XmlDoc = new XmlDocument();
            string sreturnValue = "";
            if (!IsPostBack)
            {
                gvPlanList.HeaderStyle.CssClass = "msgheader";
                gvPlanList.RowStyle.CssClass = "datatd1";
                gvPlanList.AlternatingRowStyle.CssClass = "datatd";
                
                // Read the Values from Querystring
                hdnClaimTypeCode.Text = AppHelper.GetQueryStringValue("claimtype");
                hdnEventDate.Text = AppHelper.GetQueryStringValue("evdate");
                hdnClaimDate.Text = AppHelper.GetQueryStringValue("claimdate");
                hdnDeptEID.Text = AppHelper.GetQueryStringValue("deptid");
          
                if (hdnClaimTypeCode.Text != "" && hdnEventDate.Text != "" && hdnClaimDate.Text != "" && hdnDeptEID.Text != "")
                {
                    bReturnStatus = CallCWSFunction("ClaimPlanAdaptor.Get", out sreturnValue);

                    if (bReturnStatus)
                    {
                        XmlDoc.LoadXml(sreturnValue);

                        XmlNodeList planRecords = XmlDoc.SelectNodes("//plans/plan");
                        if (planRecords.Count != 0)
                        {
                            DataTable dtGridData = new DataTable();
                            DataColumn dcGridColumn;
                            BoundField bfGridColumn;
                            ButtonField butGridLinkColumn;
                            XmlNode xNode;
                            DataRow objRow;

                            // Plan Id

                            // Column to be associated with Datasource of grid
                            dcGridColumn = new DataColumn();

                            dcGridColumn.ColumnName = "Plan Id";

                            // Add Column to Data Table
                            dtGridData.Columns.Add(dcGridColumn);

                            // Column to be associated with the Grid
                            bfGridColumn = new BoundField();

                            // Associate the Grid Bound Column with the Column of the Datasource
                            bfGridColumn.DataField = dcGridColumn.ColumnName;

                            // Assign the HeaderText
                            bfGridColumn.HeaderText = dcGridColumn.ColumnName;

                            // Add the newly created bound field to the GridView. 
                            gvPlanList.Columns.Add(bfGridColumn);

                            // Plan Number

                            // Column to be associated with Datasource of grid
                            dcGridColumn = new DataColumn();

                            //dcGridColumn.ColumnName = "Plan Number";//MITS 34141 - ksahu5
                            dcGridColumn.ColumnName = AppHelper.GetResourceValue(this.pageID, "gvPlanNumber", "0");//MITS 34141 - ksahu5

                            // Add Column to Data Table
                            dtGridData.Columns.Add(dcGridColumn);

                            // Column to be associated with the Grid
                            bfGridColumn = new BoundField();

                            // Associate the Grid Bound Column with the Column of the Datasource
                            bfGridColumn.DataField = dcGridColumn.ColumnName;

                            // Assign the HeaderText
                            bfGridColumn.HeaderText = dcGridColumn.ColumnName;

                            // Add the newly created bound field to the GridView. 
                            gvPlanList.Columns.Add(bfGridColumn);


                            // Column to be associated with Datasource of grid
                            dcGridColumn = new DataColumn();

                            // Assign the Column Name from XmlNode
                            // dcGridColumn.ColumnName = "Plan Name"; //MITS 34141 - ksahu5
                            dcGridColumn.ColumnName = AppHelper.GetResourceValue(this.pageID, "gvPlanName", "0");//MITS 34141 - ksahu5
                            // Add Column to Data Table
                            dtGridData.Columns.Add(dcGridColumn);

                            butGridLinkColumn = new ButtonField();

                            // Associate the Grid Bound Column with the Column of the Datasource
                            butGridLinkColumn.DataTextField = dcGridColumn.ColumnName;

                            // Assign the HeaderText
                            butGridLinkColumn.HeaderText = dcGridColumn.ColumnName;

                            // Create the hyperlink for the column
                            butGridLinkColumn.ButtonType = ButtonType.Link;

                            gvPlanList.Columns.Add(butGridLinkColumn);

                            // Column to be associated with Datasource of grid
                            dcGridColumn = new DataColumn();

                            // dcGridColumn.ColumnName = "Eff. Date (From & To)"; //MITS 34141 - ksahu5
                            dcGridColumn.ColumnName = AppHelper.GetResourceValue(this.pageID, "gvEffDateFromTo", "0");//MITS 34141 - ksahu5


                            // Add Column to Data Table
                            dtGridData.Columns.Add(dcGridColumn);

                            // Column to be associated with the Grid
                            bfGridColumn = new BoundField();

                            // Associate the Grid Bound Column with the Column of the Datasource
                            bfGridColumn.DataField = dcGridColumn.ColumnName;

                            // Assign the HeaderText
                            bfGridColumn.HeaderText = dcGridColumn.ColumnName;

                            // Add the newly created bound field to the GridView. 
                            gvPlanList.Columns.Add(bfGridColumn);

                            // Column to be associated with Datasource of grid
                            dcGridColumn = new DataColumn();

                            dcGridColumn.ColumnName = "ClassNames";

                            // Add Column to Data Table
                            dtGridData.Columns.Add(dcGridColumn);

                            // Column to be associated with the Grid
                            bfGridColumn = new BoundField();

                            // Associate the Grid Bound Column with the Column of the Datasource
                            bfGridColumn.DataField = dcGridColumn.ColumnName;

                            // Assign the HeaderText
                            bfGridColumn.HeaderText = dcGridColumn.ColumnName;

                            // Add the newly created bound field to the GridView. 
                            gvPlanList.Columns.Add(bfGridColumn);

                            // Column to be associated with Datasource of grid
                            dcGridColumn = new DataColumn();

                            dcGridColumn.ColumnName = "DttmRcdLastUpd";


                            // Add Column to Data Table
                            dtGridData.Columns.Add(dcGridColumn);

                            // Column to be associated with the Grid
                            bfGridColumn = new BoundField();

                            // Associate the Grid Bound Column with the Column of the Datasource
                            bfGridColumn.DataField = dcGridColumn.ColumnName;

                            // Assign the HeaderText
                            bfGridColumn.HeaderText = dcGridColumn.ColumnName;

                            // Add the newly created bound field to the GridView. 
                            gvPlanList.Columns.Add(bfGridColumn);
                          
                            for (int i = 0; i < planRecords.Count; i++)
                            {
                                xNode = planRecords[i];
                                objRow = dtGridData.NewRow();
                                objRow["Plan Id"] = xNode.Attributes["planid"].Value;
                                //MITS 34141 - ksahu5 start
                                //objRow["Plan Number"] = xNode.Attributes["plannumber"].Value;
                                //objRow["Plan Name"] = xNode.Attributes["planname"].Value;
                                //objRow["Eff. Date (From & To)"] = xNode.Attributes["effectivedate"].Value + "-" + xNode.Attributes["expirationdate"].Value;
                                objRow[AppHelper.GetResourceValue(this.pageID, "gvPlanNumber", "0")] = xNode.Attributes["plannumber"].Value;
                                objRow[AppHelper.GetResourceValue(this.pageID, "gvPlanName", "0")] = xNode.Attributes["planname"].Value;
                                objRow[AppHelper.GetResourceValue(this.pageID, "gvEffDateFromTo", "0")] = AppHelper.GetDate(xNode.Attributes["effectivedate"].Value) + "-" + AppHelper.GetDate(xNode.Attributes["expirationdate"].Value);
                                //MITS 34141 - ksahu5 End
                                if (xNode.Attributes["classnames"] != null)  //csingh7 : mits 14176
                                    objRow["ClassNames"] = xNode.Attributes["classnames"].Value;

                                objRow["DttmRcdLastUpd"] = xNode.Attributes["dttmrcdlastupd"].Value;


                                dtGridData.Rows.Add(objRow);
                            }
                            gvPlanList.DataSource = dtGridData;
                            gvPlanList.DataBind();
                        }
                        else
                        {
                            gvPlanList.Visible = false;
                            // lblMessage.Text = "<p align='center'><br><br><br><h3 align='center'>&nbsp;&nbsp;&nbsp;&nbsp;No plans could be found for this claim!</h3></p>";//MITS 34141 - ksahu5
                            lblMessage.Text = "<p align='center'><br><br><br><h3 align='center'>&nbsp;&nbsp;&nbsp;&nbsp;" + AppHelper.GetResourceValue(this.pageID, "lblNoPlans", "0") + "</h3></p>";//MITS 34141 - ksahu5
                        }

                    }
                }
                else
                {
                    gvPlanList.Visible = false;
                    //lblMessage.Text = "<p align='center'><br><br><br><h3 align='center'>&nbsp;&nbsp;&nbsp;&nbsp;No plans could be found for this claim!</h3></p>";//MITS 34141 - ksahu5
                    lblMessage.Text = "<p align='center'><br><br><br><h3 align='center'>&nbsp;&nbsp;&nbsp;&nbsp;" + AppHelper.GetResourceValue(this.pageID, "lblNoPlans", "0") + "</h3></p>";//MITS 34141 - ksahu5
                }
            }

        }
        

        protected void gvPlanList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                DataRowView drvGridRow = (DataRowView)e.Row.DataItem;
                TableCell tcGridCell;
                LinkButton lbLinkColumn;

                tcGridCell = e.Row.Cells[2];
                lbLinkColumn = (LinkButton)tcGridCell.Controls[0];

                // href is assigned the value # to stop the postback on click.
                lbLinkColumn.Attributes.Add("href", "#");
                //Added Escape Strings function: MITS 22231: Yatharth
                lbLinkColumn.Attributes.Add("onclick", "selectPlan(" + e.Row.Cells[0].Text + ",'" + escapeStrings(lbLinkColumn.Text) + "','" + e.Row.Cells[4].Text + "','" + e.Row.Cells[5].Text + "');");

            }
        }

        protected void gvPlanList_DataBound(object sender, EventArgs e)
        {
            gvPlanList.Columns[0].HeaderStyle.CssClass = "hiderowcol";
            gvPlanList.Columns[0].ItemStyle.CssClass = "hiderowcol";
            gvPlanList.Columns[4].HeaderStyle.CssClass = "hiderowcol";
            gvPlanList.Columns[4].ItemStyle.CssClass = "hiderowcol";
            gvPlanList.Columns[5].HeaderStyle.CssClass = "hiderowcol";
            gvPlanList.Columns[5].ItemStyle.CssClass = "hiderowcol";
        }

        //Added Escape Strings function: MITS 22231: Yatharth
        protected string escapeStrings(string sCode)
        {

            sCode = sCode.Replace("'", "\\'");
            return sCode;

        }
    }
}
