﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="about.aspx.cs" Inherits="Riskmaster.UI.UI.Help.about" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="../../App_Themes/RMX_Default/rmnet.css" type="text/css"/>
	<script language="javascript" src="../../Scripts/form.js" type="text/javascript"></script>    	
</head>
<body onload="tryMDIScreenLoaded()">
    <form id="form1" runat="server">
    <div id="aboutRMXLogo">
        <asp:Image ID="imgRMX" ImageUrl="~/Images/RMX_GillSansMt_72.gif" runat="server"/>
    </div>
    <div id="aboutVersion">
        <asp:Label ID="lblVersion" Text="Version: " CssClass="aboutTitle" runat="server"></asp:Label>
        <!--<asp:Image ID="imgGap" ImageUrl="~/Images/aboutGap.gif" runat="server"/> -->
        <asp:Label ID="lblBuildInfo" CssClass="aboutText" runat="server"></asp:Label>
    </div>
    
    <div id="aboutSupportLogo">
        <asp:Image ID="imgSupport" ImageUrl="~/Images/SUPPORT_GillSansMt_14.gif" runat="server"/>
    </div>
    
    <div id="aboutContact">

            <asp:Label ID="lblWebsiteTitle" Text="Website: " CssClass="aboutTitle" runat="server"></asp:Label>
            <asp:Label ID="lblWebsiteText" Text="www.riskmaster.com" CssClass="aboutText" runat="server"></asp:Label>
        <br/>
        
            <asp:Label ID="lblEmeilTitle" Text="Email: " CssClass="aboutTitle" runat="server"></asp:Label>
            <asp:Label ID="lblEmailText" Text="risksupp@csc.com" CssClass="aboutText" runat="server"></asp:Label>
        <br/>
        
            <asp:Label ID="lblPhoneTitle" Text="Phone: " CssClass="aboutTitle" runat="server"></asp:Label>
            <asp:Label ID="lblPhoneText" Text="1 877 275 3676" CssClass="aboutText" runat="server"></asp:Label>
        
        <br/>          
            <asp:Label ID="lblAddressTitle1" Text="Mail: " CssClass="aboutTitle" runat="server"></asp:Label>            
            <asp:Label ID="lblAddressText1" Text="CSC, Suite L60" CssClass="aboutText" runat="server"></asp:Label> 
        <br/>                      
            <asp:Label ID="lblAddressTitle2" Text="Mail: " CssClass="aboutTitleWhite" runat="server"></asp:Label>            
            <asp:Label ID="lblAddressText2" Text="25800 Northwestern Hwy" CssClass="aboutText" runat="server"></asp:Label>
        <br/>                      
            <asp:Label ID="lblAddressTitle3" Text="Mail: " CssClass="aboutTitleWhite" runat="server"></asp:Label>            
            <asp:Label ID="lblAddressText3" Text="Southfield, MI 48075" CssClass="aboutText" runat="server"></asp:Label>
                                
    </div>
    
    <div id="aboutCopyright">
        <asp:Label ID="lblCopyright1" Text="© " runat="server"></asp:Label>
        <asp:Label ID="lblCopyrightEndingYear" runat="server"></asp:Label>
        <asp:Label ID="lblCopyright3" Text=" Computer Sciences Corporation, All Rights Reserved." runat="server"></asp:Label>
    </div>
    </form>
</body>
</html>
