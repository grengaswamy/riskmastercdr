﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PS_Driver.aspx.cs" Inherits="Riskmaster.UI.UI.PolicyInterface.PS_Driver" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl" TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Driver Details</title>
</head>
<body>
    <form id="form1" runat="server" method="post">
     <table>
        <tr>
            <td colspan="2">
                <uc1:errorcontrol id="ErrorControl1" runat="server" />
            </td>
        </tr>
    </table>
    <div>
     <div class="msgheader" id="formtitle">
        Driver Details
        </div>
     <table width="98%" border="0" cellpadding="4" cellspacing="8">
    <colgroup>
    <col width="25%" class="required" />
    <col width="25%" />
    <col width="25%" class="required" />
    <col width="25%"  />
    </colgroup>
    <tr>
    <td><asp:Label Text="Driver" runat="server"></asp:Label></td>
    <td><asp:Label ID="lblDriver" runat="server"></asp:Label></td>
    <td><asp:Label Text="Status" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblStatus"></asp:Label></td>
    </tr>
    <tr>
    <td><asp:Label Text="Driver Name" runat="server"></asp:Label></td>
    <td colspan="3"><asp:Label runat="server" ID="lblFirstNm"></asp:Label>
    &nbsp; &nbsp; <asp:Label runat="server" ID="lblMiddleNm"></asp:Label>
    &nbsp; &nbsp; <asp:Label runat="server" ID="lblLastNm"></asp:Label> &nbsp; &nbsp; <asp:Label runat="server" ID="lblSuffix"></asp:Label> </td>
    </tr>
    <tr>
    <td><asp:Label Text="Date Of Birth" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblDOB"></asp:Label></td>
    <td><asp:Label Text="Gender" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblGender"></asp:Label></td>
    </tr>
    <tr>
    <td><asp:Label Text="Marital Status" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblMarital"></asp:Label></td>
    <td><asp:Label Text="Relation to Insured" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblRelatn"></asp:Label></td>
    </tr>
    <tr>
    <td><asp:Label Text="License State" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblLicenseState"></asp:Label></td>
    <td><asp:Label Text="License Number" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblLicNo"></asp:Label></td>
    </tr>
    <tr>
    <td><asp:Label Text="Date Licensed" runat="server"></asp:Label></td>
    <td><asp:Label ID="lblDtLicensed" runat="server"></asp:Label></td>
    <td><asp:Label Text="MVR Indicator" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblMVRInd"></asp:Label></td>
    </tr>
    <tr>
    <td><asp:Label Text="Driver Status" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblDriverStatus"></asp:Label></td>
    <td><asp:Label Text="SR-22" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblSR"></asp:Label></td>
    </tr>
    <tr>
    <td><asp:Label Text="Age" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblAge"></asp:Label></td>
    <td><asp:Label Text="Driver Class" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblDriverClass"></asp:Label></td>
    </tr>
    <tr>
    <td><asp:Label Text="Principal Operator" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblPO1"></asp:Label> &nbsp; &nbsp;<asp:Label runat="server" ID="lblPO2"></asp:Label> &nbsp; &nbsp;<asp:Label runat="server" ID="lblPO3"></asp:Label> </td>
    <td><asp:Label Text="Part-Time Operator" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblPTO1"></asp:Label> &nbsp; &nbsp; <asp:Label runat="server" ID="lblPTO2"></asp:Label> &nbsp; &nbsp; <asp:Label runat="server" ID="lblPTO3"></asp:Label></td>
    </tr>
    </table>
    </div>
    </form>
</body>
</html>
