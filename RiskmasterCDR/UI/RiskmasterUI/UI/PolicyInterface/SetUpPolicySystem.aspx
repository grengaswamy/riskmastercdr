﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SetUpPolicySystem.aspx.cs" Inherits="Riskmaster.UI.PolicyInterface.SetUpPolicySystem" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl" TagPrefix="uc1" %>
<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Policy System Web Settings </title>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/grid.js">        { var i; }</script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js">        { var i; }</script>
    <script type="text/javascript">
        function validateData() {
            if (trim(document.getElementById("PolicySysName").value) == "")  {
                alert("Please Enter Policy System Name");
                return false;
            }
            
            if (document.getElementById("MappingTablePrefix") != null && trim(document.getElementById("MappingTablePrefix").value) == "") {
                alert("Please Enter Mapping Table Prefix");
                return false;
            }
            
            //rupal:start
            var PolSysTypeId = document.forms[0].PolicySystemType_codelookup_cid.value;
            if (trim(PolSysTypeId) == "" || trim(PolSysTypeId) == "0") {
                alert("Please Enter Policy System Type.");
                document.forms[0].PolicySystemType_codelookup.focus();
                return false;
            }           
            if (document.getElementById("Version") != null && trim(document.getElementById("Version").value) == "0") {
                alert("Please select a Version");
                return false;
            }
            if (trim(document.getElementById("hdnPolicySystemType").value) == "INTEGRAL") {
                //sanoopsharma - MITS 35932 start
                if (document.getElementById("WSSearchURL") != null) {
                    if (trim(document.getElementById("WSSearchURL").value) == "") {
                        alert("Please Enter WS Search URL");
                return false;
            }
                }
                if (document.getElementById("WSInquiryURL") != null) {
                    if (trim(document.getElementById("WSInquiryURL").value) == "") {
                        alert("Please Enter WS Inquiry URL");
                    return false;
                }
            }
                if (document.getElementById("UseCAS") != null) {
                    if (document.getElementById("UseCAS").checked) {
                        if (document.getElementById("CASServerURL") != null) {
                            if (trim(document.getElementById("CASServerURL").value) == "") {
                                alert("Please Enter CAS Server");
                return false;
            }
                        }
                        if (document.getElementById("CASServiceURL") != null) {
                            if (trim(document.getElementById("CASServiceURL").value) == "") {
                                alert("Please Enter CAS Service ");
                return false;
                            }
                        }
                
            }
                }

                if (document.getElementById("FinancialUpdate") != null) {
                    if (document.getElementById("FinancialUpdate").checked) {
                        if (document.getElementById("UploadServiceUrl")!= null)
                            if (trim(document.getElementById("UploadServiceUrl").value) == "") {
                            alert("Please Enter Upload Service URL");
                            return false;
                        }
                    }
                }
                //end sanoopsharma
            }
            //Anu Tennyson Added for staging Implementation Starts
           else if (trim(document.getElementById("hdnPolicySystemType").value) == "STAGING") {
                if (document.getElementById("txtStagingConn") != null) {
                    if (trim(document.getElementById("txtStagingConn").value) == "") {
                        alert("Please Enter Staging Database Information.");
                        return false;
                    }
                }
            }
                //Anu Tennyson Ends 
            else
            {
                if (document.getElementById("SystemTableNm") != null && trim(document.getElementById("SystemTableNm").value) == "") {
                    alert("Please Enter System Table Name");
                    return false;
                }
                if ((document.getElementById("cbUseRMAForCFW").checked == false) && trim(document.getElementById("txtCFWUserNm").value) == "") {
                    alert("Please enter CFW user name");
                    return false;
                }
                if (document.getElementById("TargetDatabase") != null) {
                    if (trim(document.getElementById("TargetDatabase").value) == "") {
                        alert("Please Enter Target Database");
                        return false;
                    }
                }
                if (document.getElementById("URLParam") != null && trim(document.getElementById("URLParam").value) == "") {
                    alert("Please Enter URL Parameters");
                    return false;
                }
                if (trim(document.getElementById("RowId").value) == "") {

                if ((trim(document.getElementById("Password").value) != "") || (trim(document.getElementById("ConfirmPassword").value) != "")) {
                    if ((document.getElementById("Password").value != document.getElementById("ConfirmPassword").value)) {
                        alert("Password and Confirm Password doesnot match");
                        return false;
                        }
                    }
                }

            }
                //sanoopsharma 
                if (document.getElementById("FinancialUpdate") != null) {
                    if (document.getElementById("FinancialUpdate").checked) {
                        if (document.getElementById("UploadServiceUrl")!= null)
                            if (trim(document.getElementById("UploadServiceUrl").value) == "") {
                            alert("Please Enter Upload Service URL");
                            return false;
                        }
                    }
                }
                //end sanoopsharma
        }
        function validatePassword() {

            if ((trim(document.getElementById("oldPwd").value) != "") || (trim(document.getElementById("NewPwd").value) != "")) {
                if ((document.getElementById("NewPwd").value != document.getElementById("ConfirmPwd").value)) {
                    alert("New Password and Confirm Password doesnot match");
                    return false;
                }
            }
            if (document.getElementById("tempPassword").value != document.getElementById("oldPwd").value) {
                alert("Old Password Doesnot match");
                return false;
            }


        }
        function showalert() {
            alert("Password has been successfully changed");
           
                  }
        function ModifyControls() {

            document.getElementById("div1").style.display = "none";
            document.getElementById("div2").style.display = "";
            return false;

        }
        function Cancel2Clicked() {

            document.getElementById("div1").style.display = "";
            document.getElementById("div2").style.display = "none";
            return false;

            }
            function CancelClicked() {
               
                window.close();
            }
            function CheckChange() {
                var ctrl = document.getElementById("cbUseRMAForCFW");
                var ctrlTxtBox;
                ctrlTxtBox = document.getElementById("txtCFWUserNm");
                if (ctrl != null && ctrlTxtBox != null && ctrl.checked == true) {
                    ctrlTxtBox.readOnly = true;
                }
                else if (ctrl != null && ctrlTxtBox != null && ctrl.checked == false) {
                    ctrlTxtBox.readOnly = false;
                }
        }
        //bsharma33 DSN configuration for policy upload.
        var sOriginalClientDSN;
        function CopyDataDSNToClientDSN() {

            var cBox = document.getElementById("cbCopyDataDSN");
            if (cBox.checked == true) {

                sOriginalClientDSN = document.getElementById("txtClientDSNConnStr").value;
                document.getElementById("txtClientDSNConnStr").value = document.getElementById("txtDataDSNConnStr").value;
                document.getElementById("btnAddClientDSN").disabled = "true";
                document.getElementById("ClearClientDSN").disabled = "true";
                
            }
            else {

                document.getElementById("txtClientDSNConnStr").value = sOriginalClientDSN;
                document.getElementById("btnAddClientDSN").disabled = "";
                document.getElementById("ClearClientDSN").disabled = "";
            }
        }
        function AddDSNDetails(p_PolicySystemId, p_Mode,dsntype) {

            window.open('AddDSNDetails.aspx?PolicySystemId=' + p_PolicySystemId + '&Mode=' + p_Mode + '&DSNType=' + dsntype, 'AddEditDSN', 'Width=650,Height=400,status=yes,top=' + (screen.availHeight - 400) / 2 + ',left=' + (screen.availWidth - 650) / 2 + '');
            return false;
        }
        function ClearDSNText(dsnmode) {

            if (dsnmode == "DataDSN") {
                var cBox = document.getElementById("cbCopyDataDSN");
                if (cBox.checked == true) {

                    document.getElementById("txtDataDSNConnStr").value = "";
                    document.getElementById("txtClientDSNConnStr").value = "";
                }
                else {

                    document.getElementById("txtDataDSNConnStr").value = "";
                }

            }
            else if (dsnmode == "ClientDSN") {

                document.getElementById("txtClientDSNConnStr").value = "";
                sOriginalClientDSN = "";
            }
            //Anu Tennyson : Staging Implementation Starts
            else if (dsnmode == "StagingConn") {
                document.getElementById("txtStagingConn").value = "";
            }
            //Anu Tennyson : Staging Implementation Ends
           }
        //End DSN configuration for policy upload.
        //dbisht6 : 35926
        function PIJSetting()
        {           
            var version = document.getElementById("Version");
            var radiobuttonON=document.getElementById("rbon");
            var radiobuttonOFF = document.getElementById("rboff");
            var uploadCheckBox = document.getElementById("chkUpdatePolicyEntities");
            if (version.options[version.selectedIndex].value == "PIJ") {
                radiobuttonON.checked = true;
                radiobuttonOFF.disabled = true;
                uploadCheckBox.checked = false;
                uploadCheckBox.disabled = false;
                 
            }
            else {
                radiobuttonON.checked = true;
                radiobuttonOFF.disabled = false;
                
            }
        }
        //End 35926
        //mits 36536 dbisht6
        function setUploadPolicyEntity(obj)
        {
            
            if (document.getElementById("checkrbState").value == obj.value) {
                return;
            }
            else {
                document.getElementById("checkrbState").value = obj.value;
                var uploadCheckBox = document.getElementById("chkUpdatePolicyEntities");
                if (obj.value == 1) {
                    uploadCheckBox.disabled = false;
                    uploadCheckBox.checked = false;

                }
                else {
                    uploadCheckBox.checked = true;
                    uploadCheckBox.disabled = true;
                }
            }

            //}
        }
        //mits 36536

        //Payal:RMA-6233 starts
        function RealTimeUploadPolicy() {
            if (document.getElementById("FinancialUpdate").checked) {

                document.getElementById("RealTimeDiv").style.display = "block";

                if (trim(document.getElementById("hdnPolicySystemType").value) == "INTEGRAL") {
                    document.getElementById("rdo_RealTime").checked = true;
                }
                else {
                    document.getElementById("rdo_RealTime").disabled = true;
                    document.getElementById("rdo_Batch").checked = true;
                }

            }
            else {
                document.getElementById("RealTimeDiv").style.display = "none";
            }
        }
        //Payal:RMA-6233 Ends
        //smahajan22 mits 36315 start
        function openPopUp()
        {
           window.open('AddPolicyConfig.aspx', 'codeWndparent',
                   'width=500,height=350' + ',top=' + (screen.availHeight) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
            return false;
        }
        //smahajan22 mits 36315 end

 //sanoopsharma - MITS 35932  - Display and hide Cas authentication details 
        function CheckChangeCAS() {          
            var div = document.getElementById("divCASDetails");
            var Check = document.getElementById("UseCAS");
            var chkrmuser = document.getElementById("cbUseRMAForCFW");
            var usernam = document.getElementById("txtCFWUserNm");
            var password = document.getElementById("txtCFWUserPassword");

            var lblchkrmuser = document.getElementById("lblRMAUser");
            var lblusernam = document.getElementById("lblCFWUserName");
            var lblpassword = document.getElementById("lblCFWUserPassword");

            if (Check != null && div != null && Check.checked == true) {
                div.style.display = 'block';
                chkrmuser.style.display = 'block'
                usernam.style.display = 'block';
                password.style.display = 'block';
                lblchkrmuser.style.display = 'block'
                lblusernam.style.display = 'block';
                lblpassword.style.display = 'block';
                //  usernam.readOnly = true;  //Payal:RMA-6233
            }
            else if (Check != null && div != null && Check.checked == false) {
                div.style.display = 'none';
                chkrmuser.style.display = 'none'
                usernam.style.display = 'none';
                password.style.display = 'none';
                lblchkrmuser.style.display = 'none'
                lblusernam.style.display = 'none';
                lblpassword.style.display = 'none';
                //bram4 - RMA-3744 - start                
                document.getElementById("CASServerURL").value = "";
                document.getElementById("CASServiceURL").value = "";
                //bram4 - RMA-3744 - end
            }

        }
        //sanoopsharma - MITS 35932 end
    </script>
    <style type="text/css">
        .auto-style1 {
            width: 38%;
        }
    </style>
</head>
<body onload="CheckChangeCAS()">
    <form id="frmData" runat="server" method="post">
    <table>
        <tr>
            <td colspan="2">
                <uc1:errorcontrol id="ErrorControl1" runat="server" />
            </td>
        </tr>
    </table>
    
    <div id ="div1" runat="server">
        <div class="msgheader" id="formtitle">
                Policy System Web Settings
            </div>
            
            <table width="600">
                <col width="185"/>
                <col width="300"/>
            <tr>
                <td class="required">
                    Policy System Name
                </td>
                <td>
                    <asp:TextBox runat="server" ID="PolicySysName" RMXref="/Instance/Document/PolicySys/PolicySysName" />
                </td>
            </tr>
            <tr>
            <td class="required">
              <asp:Label runat="server" ID="lblMappingTablePrefix" Text ="Mapping Table Prefix"></asp:Label>
            </td>
            <td>
            <asp:TextBox runat="server" ID="MappingTablePrefix" RMXref ="/Instance/Document/PolicySys/MappingTablePrefix" MaxLength="8" ></asp:TextBox>
            <asp:Label runat="server" ID="lblMaxLength" Text="(Max 8 characters)" Font-Size="Smaller"></asp:Label>
            </td>
            </tr>
            <tr>
                <td class="required">
                  <asp:Label runat="server" ID="lbPolicySystemType" Text="Policy System Type" />  
                </td>
                <td>
                 <uc:CodeLookUp runat="server" CodeTable="POLICY_SYSTEM_TYPE" ControlName="PolicySystemType" ID="PolicySystemType" type="code"  RMXref="/Instance/Document/PolicySys/PolicySystemType"/>
                </td>
            </tr>
 <%--sanoopsharma - MITS 35932 Start--%>
                <tr id="CASFields" runat="server">
                    <td colspan="2">
                        <table >
                            <col width="225"/>
                            <col width="300"/>
                            <tr>
                                <td  class="required">
                                    <asp:label runat="server" id="lblWSSearchURL" text="WS Search URL" />
                                </td>
                                <td >
                                    <asp:textbox runat="server" id="WSSearchURL" rmxref="/Instance/Document/PolicySys/WSSearchURL" />
                                </td>
                            </tr>
                            <tr>
                                <td class="required" >
                                    <asp:label runat="server" id="lblWSInquiryURL" text="WS Inquiry URL" />
                                </td>
                                <td >
                                    <asp:textbox runat="server" id="WSInquiryURL" rmxref="/Instance/Document/PolicySys/WSInquiryURL" />
                                </td>
                            </tr>
                             <!-- Payal:RMA-6233 Starts -->
                             <tr>
                                <td>
                                    <asp:label runat="server" id="lblUploadServiceUrl" Text="<%$ Resources:lblUploadServiceUrl %>"/>
                                </td>
                                <td >
                                    <asp:textbox runat="server" id="UploadServiceUrl" rmxref="/Instance/Document/PolicySys/UploadServiceUrl" />
                                </td>
                            </tr>
                              <!-- Payal:RMA-6233 ends -->
                        </table>
                    </td>
                </tr>
                <tr id="trCASCheck" runat="server">
                    <td>
                        <asp:label runat="server" id="lblUseCAS" text="Central Authentication Service(CAS)"></asp:label>
                    </td>
                    <td>
                        <asp:checkbox runat="server" id="UseCAS"
                            rmxref="/Instance/Document/PolicySys/UseCAS" checked="false" onclick="CheckChangeCAS();"  />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div id="divCASDetails" style="display: none;">
                            <table >
                                <col width="225"/>
                                <col width="300"/>
                                <tr>
                                    <td class="required">
                                        <asp:label runat="server" id="lblCASServerURL" text="CAS Server URL" />
                                    </td>
                                    <td>
                                        <asp:textbox runat="server" id="CASServerURL" rmxref="/Instance/Document/PolicySys/CASServerURL" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="required">
                                        <asp:label runat="server" id="lblCASServiceURL" text="CAS Service URL" />
                                    </td>
                                    <td>
                                        <asp:textbox runat="server" id="CASServiceURL" rmxref="/Instance/Document/PolicySys/CASServiceURL" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr id="DefaultPointfields" runat="server">
                    <td colspan="2">
                        <table >
                            <%--sanoopsharma - MITS 35932 end --%>
                            <col width="225"/>
                            <col width="300"/>
            <tr>
                                <td>Domain
                </td>
                <td>
                    <asp:TextBox runat="server" ID="Domain" RMXref="/Instance/Document/PolicySys/Domain" MaxLength="10"/>
                </td>
            </tr>
            <tr>
                <td >
                    License
                </td>
                <td>
                    <asp:TextBox runat="server" ID="License" RMXref="/Instance/Document/PolicySys/License" />
                </td>
            </tr>
            <tr>
                <td>
                    Proxy
                </td>
                <td>
                    <asp:TextBox runat="server" ID="Proxy" RMXref="/Instance/Document/PolicySys/Proxy" />
                </td>
            </tr>
            <tr>
                <td >
                    User Name
                </td>
                <td>
                    <asp:TextBox runat="server" ID="UserName" RMXref="/Instance/Document/PolicySys/UserName" />
                </td>
            </tr>
            <tr>
                <td >
                         <asp:Label runat="server" ID="lbPassword" Text="Password" />  
                </td>
                <td>
                    <asp:TextBox runat="server" ID="Password" RMXref="/Instance/Document/PolicySys/Password" TextMode="Password" autocomplete="off"/>
                </td>
            </tr>
            <tr>
                <td>
                  <asp:Label runat="server" ID="lbconfirmPassword" Text="Confirm Password" />  
                </td>
                <td>
                    <asp:TextBox runat="server" ID="ConfirmPassword" TextMode="Password" autocomplete="off" />
                </td>
            </tr>
            <tr>
                <td class="required">
                    CFW URL 
                </td>
                <td>
                    <asp:TextBox runat="server" ID="URLParam" RMXref="/Instance/Document/PolicySys/URLParam" />
                </td>
            </tr>
 <%--sanoopsharma - MITS 35932 start --%>
                        </table>
                    </td>
                </tr>
                <%--sanoopsharma - MITS 35932 end --%>

                <tr>
                    <td class="required">
                        <asp:Label runat="server" ID="lblRMAUser" Text="Use RMA User for CFW"></asp:Label>
                        <!--bram4: MITS 35932 -added ID -->
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="cbUseRMAForCFW"
                            RMXref="/Instance/Document/PolicySys/UseRMAUserForCFW" Checked="false" onclick="CheckChange();" />
                    </td>

            </tr>
            <tr>
                    <!--Start bram4: MITS 35932 -added ID and required class -->
            <td>
            <asp:Label runat="server" id="lblCFWUserName" Text="CFW User Name"></asp:Label>
            </td>
            <td>
            <asp:TextBox runat="server" ID="txtCFWUserNm" RMXref="/Instance/Document/PolicySys/CFWUserName" ></asp:TextBox>
            </td>
            </tr>
            <tr>
            <td>
            <asp:Label  runat="server" id="lblCFWUserPassword" Text="CFW User Password"></asp:Label>
            </td>
            <td>
            <asp:TextBox runat="server" ID="txtCFWUserPassword" autocomplete="off" TextMode="Password" RMXref="/Instance/Document/PolicySys/CFWUserPassword" ></asp:TextBox>
            </td>
            </tr>
            <tr>
            <td>
                        <%--bram4: MITS 35932 <asp:Label ID="lblPointURL"  runat="server" Text="Point URL"></asp:Label>--%>
                        <asp:label id="lblPointURL" runat="server" text="Policy System URL"></asp:label>
            </td>
            <td>
            <asp:TextBox runat="server" ID="txtPointURL" RMXref="/Instance/Document/PolicySys/PointURL" ></asp:TextBox>
            </td>
            </tr>
                <tr>
                    <td class="required">
                        <asp:label runat="server" id="lblVersion" text="Version"></asp:label>
                    </td>
                    <td>
                        <asp:dropdownlist id="Version" runat="server" rmxref="/Instance/Document/PolicySys/Version">
          <asp:ListItem Text="Select" Value="0"></asp:ListItem>
          </asp:dropdownlist>
                    </td>
                </tr>
            </table>
            <%-- bram4: MITS 35932 - Point specific fields added in the table --%>
            <table id="tblPoint" runat="server" width="600">
                  <col width="185"/>
                <col width="300"/>
                <tr>
                    <td class="required">
                        <asp:label runat="server" id="lblTargetDB" text="Upload Target DSN" visible="false" />
                    </td>
                    <td>
                        <asp:textbox runat="server" id="TargetDatabase" rmxref="/Instance/Document/PolicySys/TargetDatabase" visible="false" />
                    </td>
                </tr>
                <tr>
                    <td class="required">
                        <asp:label runat="server" id="lblClientFileSetting" text="Client File Setting"></asp:label>
                    </td>
                    <td>
                        <input type="radio" id="rbOn" runat="server" rmxref="/Instance/Document/PolicySys/ClientFileFlag" value="1" onclick="javascript: setUploadPolicyEntity(this);" name="FileSetting" /><span>ON</span>
                        <input type="radio" id="rbOff" runat="server" rmxref="/Instance/Document/PolicySys/ClientFileFlag" value="0" onclick="javascript: setUploadPolicyEntity(this);" name="FileSetting" /><span>OFF</span>
                        <asp:TextBox ID="checkrbState" runat="server" Style="display: none" value=""></asp:TextBox>
                    </td>
                </tr>
             <tr>
            <td>
            <asp:Label ID="lblLocationCmpny" runat="server" Text="Location Company"></asp:Label>
            </td>
            <td>
            <asp:TextBox runat="server" ID="txtLocCmpny" RMXref="/Instance/Document/PolicySys/LocCmpny" MaxLength="2" ></asp:TextBox>
            </td>
            </tr>
                         <tr>
            <td>
            <asp:Label ID="lblMasterCompany" runat="server" Text="Master Company"></asp:Label>
            </td>
            <td>
            <asp:TextBox runat="server" ID="txtMasterCompany" RMXref="/Instance/Document/PolicySys/MasterCompany" MaxLength="2" ></asp:TextBox>
            </td>
            </tr>
            <tr>
            <td>
            <asp:Label ID="lbBusinessEntities" runat="server" Text="Business Entities Code"></asp:Label>
            </td>
            <td>
            <asp:TextBox runat="server" ID="txtBusinessEntities" RMXref="/Instance/Document/PolicySys/BusinessEntities" TextMode="MultiLine" ></asp:TextBox>
             <asp:Label runat="server" ID="lbBusinessEntitiesComment" Text="(Enter comma  separated values)" Font-Size="Smaller"></asp:Label>
            </td>
            </tr>
            <tr>
            <td>
            <asp:Label ID="lbMajorPerils" runat="server" Text="Exclude Major Peril"></asp:Label>
            </td>
            <td>
            <asp:TextBox runat="server" ID="txtMajorPerils" RMXref="/Instance/Document/PolicySys/MajorPerils"  TextMode="MultiLine"></asp:TextBox>
             <asp:Label runat="server" ID="lbMajorPerilsComment" Text="(Enter comma  separated values)" Font-Size="Smaller"></asp:Label>
            </td>
            </tr>
            <tr>
            <td>
            <asp:Label ID="lbIntertestList" runat="server" Text="Exclude Additional Interests"></asp:Label>
            </td>
            <td>
            <asp:TextBox runat="server" ID="txtIntertestList" RMXref="/Instance/Document/PolicySys/IntertestList"  TextMode="MultiLine"></asp:TextBox>
             <asp:Label runat="server" ID="lbIntertestListComment" Text="(Enter comma  separated values)" Font-Size="Smaller"></asp:Label>
            </td>
            </tr>
                <tr>
                    <td>Data DSN
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtDataDSNConnStr"  RMXref="/Instance/Document/PolicySys/DataConnstr"  />
                     <asp:Button id="btnAddDataDSN" runat="server" class="button" Text="Add" />
                    <input type="button" id="ClearDataDSN"  class="button" value="Clear" onclick="ClearDSNText('DataDSN');" />
                </td>
            </tr>
             <tr>
                <td>
                    Copy Data DSN to Client DSN:
                </td>
                <td>
                    <asp:CheckBox runat="server" Checked="false" ID="cbCopyDataDSN" onclick="CopyDataDSNToClientDSN();" />
                </td>
            </tr>
            <tr>
                <td>
                    Client DSN
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtClientDSNConnStr"  RMXref="/Instance/Document/PolicySys/ClientConnstr"   />
                    <asp:Button id="btnAddClientDSN" runat="server" class="button" Text="Add" />
                    <input type="button" id="ClearClientDSN"  class="button" value="Clear" onclick="ClearDSNText('ClientDSN');" />
                </td>
            </tr>
            </table>


        <table id="tblStgConnection" runat ="server"  width="600" >
            <col width="185"/>
                <col width="300"/>
             <tr >
                <td width="36%" >Staging Database Connection</td>
                  <%--  <asp:Label  runat="server" id="lblStagingConnection" Text="Staging Database Connection"></asp:Label>--%>
                <td>                    
                    &nbsp;<asp:TextBox runat="server" ID="txtStagingConn"  RMXref="/Instance/Document/PolicySys/StagingConn"  ></asp:TextBox>
                     <asp:Button id="btnStagingConn" runat="server" class="button" Text="Add" />
                    <input type="button" runat="server"   id="ClearStagingConn"  class="button" value="Clear" onclick="ClearDSNText('StagingConn');" />
                </td>
            </tr>
         </table> 
            <table id="tblChk" runat ="server" width="600" >
                  <col width="185"/>
                <col width="300"/>
				<!--RMA-10039-->
                <tr id="financialUpload">
                    <td class="auto-style1">Financial Upload:
                    </td>
                    <td>
                        <asp:CheckBox runat="server" Checked="false" rmxref="/Instance/Document/PolicySys/FinancialUpdate"
                            ID="FinancialUpdate" onclick="RealTimeUploadPolicy();" />
                    </td>
                </tr>
                <!--Payal:RMA-6233 starts -->
				<!--RMA-10039-->
                <tr id="realTime"> 
                    <td class="auto-style1"></td>
                    <td id="RealTimeDiv" style="display: none;" runat="server">
                        <input type="radio" id="rdo_RealTime" value="-1" name="RealTimeUpload" runat="server" rmxref="/Instance/Document/PolicySys/RealTimeUpload" />
                        <asp:Label runat="server" ID="lblRealTime" runat="server" Text="<%$ Resources:lblRealTime %>"></asp:Label>
                        <input type="radio" id="rdo_Batch" value="0" name="RealTimeUpload" runat="server" rmxref="/Instance/Document/PolicySys/RealTimeUpload" />
                        <asp:Label runat="server" ID="lblBatch" runat="server" Text="<%$ Resources:lblBatch %>"></asp:Label>
                    </td>
                </tr>
                <!--Payal:RMA-6233 ends -->
                <tr>
                    <td class="auto-style1">Default Policy System:
                    </td>
                    <td>
                        <asp:checkbox runat="server" checked="false" rmxref="/Instance/Document/PolicySys/DefaultPolicy"
                            id="DefaultPolicy" />
                    </td>
                </tr>
				<!--RMA-10039-->
                <tr id="updatePolicyEntities">
                    <td class="auto-style1">Update Policy Entities in Upload
                    </td>
                    <td>
                        <input type="checkbox" runat="server" id="chkUpdatePolicyEntities" enableviewstate="false" />
                    </td>
                    <td></td>
                </tr>

            </table>

            <table>
                <tr>
                    <td>
                        <asp:Button ID="btnOk" runat="server" class="button" OnClientClick="return validateData();"
                            Text="Ok" onclick="btnOk_Click"  />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" class="button" Text="Cancel" OnClientClick="CancelClicked();"/>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                    <asp:Button ID="btnChangePwd" runat="server" class="button" Text="Change Password" OnClientClick="return ModifyControls();" />
                    </td>
                <%--smahajan22 mits 36315 start--%>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                    <asp:Button ID="btnUploadConfig" runat="server" class="button" Text="Upload Config" OnClientClick="openPopUp();" />
                                       
                    </td>
                <%--smahajan22 mits 36315  end --%>
            </tr>
        </table>
            </div>
            <div id="div2" runat="server"  style="display:none;">
            <div class="msgheader" id="Div3">
       Change Password</div>
            <table>
            <tr>
                <td>
                  Old Password
                </td>
                <td>
                    <asp:TextBox runat="server" ID="oldPwd" TextMode="Password" autocomplete="off" />
                </td>
            </tr>
            <tr>
                <td>
                    New Password
                </td>
                <td>
                    <asp:TextBox runat="server" ID="NewPwd" TextMode="Password" autocomplete="off" />
                </td>
            </tr>
            <tr>
            <td>
            Confirm Password
            </td>
            <td>
             <asp:TextBox runat="server" ID="ConfirmPwd" TextMode="Password" autocomplete="off" />
            </td></tr>
            <tr>
          
                    <td>
                        <asp:Button ID="btnOk2" runat="server" class="button" OnClientClick="return validatePassword();" onclick="btnOk2_Click"  Text="Ok"  />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Button ID="btnCancel2" runat="server" class="button" Text="Cancel" OnClientClick="return Cancel2Clicked();"/>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                 
            </tr>
            </table>
            </div>
            <asp:TextBox Style="display: none" runat="server" ID="RowId" RMXRef="/Instance/Document/PolicySys/policysystemid" />
            <asp:TextBox Style="display: none" runat="server" ID="tempPassword"/>
    <asp:TextBox Style="display: none" runat="server" ID="selectedrowposition"></asp:TextBox>
    <asp:TextBox Style="display: none" runat="server" ID="gridname"></asp:TextBox>
    <asp:TextBox Style="display: none" runat="server" ID="txtPostBack"></asp:TextBox>
    <asp:TextBox Style="display: none" runat="server" ID="validate"></asp:TextBox>
    <asp:TextBox Style="display: none" runat="server" ID="mode"></asp:TextBox>
    <asp:TextBox Style="display: none" runat="server" ID="txtData"></asp:TextBox>
    <asp:TextBox Style="display: none" runat="server" ID="Action"></asp:TextBox>
    <asp:TextBox Style="display: none" runat="server" ID="FormMode"></asp:TextBox>
    <asp:TextBox Style="display: none" runat="server" ID="FunctionToCall"></asp:TextBox>
        <asp:hiddenfield runat="server" id="hdnPolicySystemType"></asp:hiddenfield>
    <asp:TextBox Style="display: none" runat="server" ID="txtAddConfig"></asp:TextBox> 

    </form>
</body>
</html>
