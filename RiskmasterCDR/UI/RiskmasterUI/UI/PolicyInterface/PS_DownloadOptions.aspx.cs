﻿/***************************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            
 ***************************************************************************************************
 * 05/05/2014 | 33574   | pgupta93   | Changes for Continuous Trigger and Policy Search functionality.
 ***************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.IO;
using Riskmaster.BusinessHelpers;
using Riskmaster.BusinessAdaptor.Common;
using System.ServiceModel;
using Riskmaster.UI.PolicyInterfaceService;
using System.Data;
using System.Collections;
using Riskmaster.Common;
using System.Xml;

namespace Riskmaster.UI.UI.PolicyInterface
{
    # region GridViewItemTemplate
    public class GridViewItemTemplate : ITemplate
    {
        private string _cbControlId;
        private string _hdnControlId;
        public GridViewItemTemplate(string cbControlId,string hdnControlId)
        {
            _cbControlId = cbControlId;
            _hdnControlId = hdnControlId;
        }

        public void InstantiateIn(System.Web.UI.Control container)
        {
            CheckBox oCheckBox = new CheckBox();
            oCheckBox.ID = _cbControlId;

            HiddenField oHiddenField = new HiddenField();
            oHiddenField.ID = _hdnControlId;
         //   oHiddenField.Value = "Eval(\"SequenceNumber\")";
           
            container.Controls.Add(oCheckBox);
            container.Controls.Add(oHiddenField);
        }
    }
    # endregion

    public partial class PS_DownloadOptions : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(AppHelper.GetQueryStringValue("currentmode")))
                {
                    DeleteOldFiles(AppHelper.GetQueryStringValue("currentmode"));
                }
                
                
                
                //hdMDIClaimId.Value = AppHelper.GetQueryStringValue("MDIClaimId");
                //hdnMDIEventId.Value = AppHelper.GetQueryStringValue("MDIEventId");
                if (!IsPostBack)
                {
                    txtPolicyId.Text = AppHelper.GetQueryStringValue("PolicyId");
                    txtLossDate.Text = AppHelper.GetQueryStringValue("LossDate");
                    txtPolicyFlag.Text = AppHelper.GetQueryStringValue("DownloadPolicyWithoutClaim");//MITS:33574
                    txtLoBCode.Text = AppHelper.GetQueryStringValue("LOBCode");//MITS:33574
                    GetDownloadOptions();
                }
            }
            catch (FaultException<RMException> ee)
            {
                btnSave.Enabled = false;
                hfException.Value = "true";
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;
            
                

            }
            catch (Exception ee)
            {
                btnSave.Enabled = false;
                hfException.Value = "true";
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
               
            }
        }
    
        private void DeleteOldFiles(string p_smode)
        {
            DisplayMode objMode = null;
            PolicyInterfaceBusinessHelper objHelper = new PolicyInterfaceBusinessHelper();
            try
            {
                objMode = new DisplayMode();
                objMode.Mode = p_smode;
                objHelper.DeleteOldFiles(objMode);
            }
            catch (FaultException<RMException> ee)
            {
                btnSave.Enabled = false;
                hfException.Value = "true";
                throw ee;
                
            }
            catch (Exception ee)
            {
                btnSave.Enabled = false;
                hfException.Value = "true";
                throw ee;
                
            }
            finally
            {
              
                objMode = null;
            }
        }

        private void AddDataToHashTable(ref  Dictionary<string,string> objTable)
        {
            if (string.Equals(txtMode.Text, "Unit", StringComparison.InvariantCultureIgnoreCase))
            {
               
                objTable.Add("StatUnitNo", "Unit Number");
                objTable.Add("UnitType", "Type Of Unit");
                objTable.Add("Status", "Status");
                objTable.Add("Desc", "Description/Address");
                objTable.Add("Vin", "Vin");
                objTable.Add("City", "City");
                objTable.Add("State", "State");
                
            }
            else if (string.Equals(txtMode.Text, "ENTITY", StringComparison.InvariantCultureIgnoreCase))
            {
               
                objTable.Add("TaxID", "Tax ID");
                objTable.Add("FirstName", "First Name");
                objTable.Add("MiddleName", "Middle Name");
                objTable.Add("LastName", "Last Name");
                objTable.Add("RoleCd", "Role");
                // Pradyumna GAP 16 WWIG - start
                int iPolicyStagingId = 0;
                bool bSuccess = false;
                iPolicyStagingId = Conversion.CastToType<int>(txtPolicyStagingId.Text, out bSuccess);
                if (iPolicyStagingId > 0)
                {
                    objTable.Add("TableId", "TableId");
                } 
                // Pradyumna GAP 16 WWIG - Ends                
            }
            else if (string.Equals(txtMode.Text, "Driver", StringComparison.InvariantCultureIgnoreCase))
            {
                
                objTable.Add("TaxID", "Tax ID");
                objTable.Add("FirstName", "First Name");
                objTable.Add("MiddleName", "Middle Name");
                objTable.Add("LastName", "Last Name");
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            PolicyInterfaceBusinessHelper objHelper = new PolicyInterfaceBusinessHelper();
            DisplayFileData oFileData = null;
            DataSet oDS = new DataSet();
            DataRow objRow;
          //  Hashtable objSelectedValues = null;
            XmlDocument objRetunDoc = null;
            //string[] arrSelectedValue = null;
            SaveDownloadOptions oSaveDownloadOptions = null;
            DataView objView = null;
            try
            {

                oSaveDownloadOptions = new SaveDownloadOptions();
                oSaveDownloadOptions.Mode = txtMode.Text;
                oSaveDownloadOptions.SelectedValue = txtSelectedValues.Text;
                //objSelectedValues = new Hashtable();
                //arrSelectedValue = txtSelectedValues.Text.Split(',');

                //foreach (string sSelectedValue in arrSelectedValue)
                //{
                //    objSelectedValues.Add(sSelectedValue, sSelectedValue);
                //}
                //ViewState["SelectedValues"] = objSelectedValues;
                //ViewState["SelectedValues"] = txtSelectedValues.Text;
              //  oFileData = objHelper.GetDuplicateOptions(oSaveDownloadOptions);
                //if (oFileData != null && !string.IsNullOrEmpty(oFileData.ResponseXML))
                //{
                //    objRetunDoc = new XmlDocument();
                //    objRetunDoc.LoadXml(oFileData.ResponseXML);
                //    oDS = ConvertXmlDocToDataSet(objRetunDoc);
                //    ViewState["DbRecord"] = oDS;
                //    if (oDS.Tables.Count > 0)
                //    {
                //        AddColumnsToGrid(true);
                //      //  BindConflictingData();

                //    }
                //    else
                //    {
                        SaveData();
                        //if (txtMode.Text == "unit")
                        //{
                        //    ClientScript.RegisterStartupScript(this.GetType(), "script", "showUnitListing();", true);
                        
                        //}
                //    }
                //}

            }
            catch (FaultException<RMException> ee)
            {
                btnSave.Enabled = false;
                ErrorControl1.Visible = true;
                hfException.Value = "true";
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;
                
            }
            catch (Exception ee)
            {
                btnSave.Enabled = false;
                ErrorControl1.Visible = true;
                hfException.Value = "true";
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                
            }
            finally
            {
                if (objHelper != null)
                    objHelper = null;
                if (oDS != null)
                    oDS = null;
                if (oFileData != null)
                    oFileData = null;
                objRetunDoc = null;
            }

        }

        private void GetDownloadOptions()
        {
            PolicyInterfaceBusinessHelper objHelper = new PolicyInterfaceBusinessHelper();
            DisplayFileData oFileData = null;
            DataSet oDS = new DataSet();
            DataRow objRow;
            XmlDocument objRetunDoc = null;

            try
            {
                oFileData = objHelper.GetXml();
                    txtMode.Text = oFileData.Mode;
                    //if (!string.Equals(txtMode.Text, "unitinterest", StringComparison.InvariantCultureIgnoreCase))
                    //{
                    string sMode = txtMode.Text.ToLower();
                    switch (sMode)
                    {
                        case "completed":
                            lblMode.Visible = false;
                          //  div2.Style.Value = "display:none;";
                            DataGrid.Style.Value = "display:none;";
                            ClientScript.RegisterStartupScript(this.GetType(), "script", "showPolicySuccess();isSuccess=true;", true);
                            break;
                        case "unitinterest":
                            //redirect to Unit interest Page
                             lblMode.Visible = false;
                            DataGrid.Style.Value = "display:none;";
                            ClientScript.RegisterStartupScript(this.GetType(), "script", "OpenUnitInterest();", true);
                            break;
                        case "coveragelist":
                            //redirect to coverage list page
                            lblMode.Visible = false;
                            DataGrid.Style.Value = "display:none;";
                            ClientScript.RegisterStartupScript(this.GetType(), "script", "OpenCoverageList();", true);
                            break;
                        case "unit":
                            lblMode.Visible = false;
                            DataGrid.Style.Value = "display:none;";
                        
                        

                            ClientScript.RegisterStartupScript(this.GetType(), "script", "showUnitListing();", true);
                            break;
                         default:
                            // any other case
                    ViewState["EntityTypelist"] = oFileData.EntityTypelist;
                   // if (oFileData != null && !string.IsNullOrEmpty(oFileData.ResponseXML) && !string.Equals(oFileData.Mode, "completed", StringComparison.InvariantCultureIgnoreCase) && !string.Equals(txtMode.Text, "unit", StringComparison.InvariantCultureIgnoreCase))
                    //{
                        objRetunDoc = new XmlDocument();
                        objRetunDoc.LoadXml(oFileData.ResponseXML);
                        oDS = ConvertXmlDocToDataSet(objRetunDoc);
                        AddColumnsToGrid();
                        if (oDS != null && oDS.Tables.Count > 0)
                        {
                            gvDownloadItems.DataSource = oDS.Tables[1];
                            gvDownloadItems.DataBind();
                            DataGrid.Style.Value = "";
                        }
                        else
                        {
                            btnSave.Enabled = false;
                        }
                        //  div2.Style.Value = "display:none;";
                    //}
                    //else
                    //{
                    //    lblMode.Visible = false;
                    //    //  div2.Style.Value = "display:none;";
                    //    DataGrid.Style.Value = "display:none;";
                    //    ClientScript.RegisterStartupScript(this.GetType(), "script", "showUnitListing();", true);
                    //} 
                    break;
                 }
               //}
               // else
               // {
               //     lblMode.Visible = false;
               //     //  div2.Style.Value = "display:none;";
               //     DataGrid.Style.Value = "display:none;";
               //     ClientScript.RegisterStartupScript(this.GetType(), "script", "showUnitListing();", true);
               // }
            }
            catch (FaultException<RMException> ee)
            {
                btnSave.Enabled = false;
                hfException.Value = "true";
                throw ee;


            }
            catch (Exception ee)
            {
                btnSave.Enabled = false;
                hfException.Value = "true";
                throw ee;
            }
            finally
            {
                if (objHelper != null)
                    objHelper = null;
                if (oDS != null)
                    oDS = null;
                if (oFileData != null)
                    oFileData = null;
                objRetunDoc = null;
            }
            
        }
        
        private void SaveData()
        {
            PolicyInterfaceBusinessHelper objHelper = new PolicyInterfaceBusinessHelper();
            DisplayFileData oFileData = null;
            DataSet oDS = new DataSet();

            SaveDownloadOptions oSaveDownloadOptions = null;
            DataRow objRow;

            XmlDocument objRetunDoc = null;
            bool bResult = false;
            //Hashtable objSelectedValues = null;
            string sSelectedValues = string.Empty;
            bool bSuccess= false;
            try
            {
                oSaveDownloadOptions = new SaveDownloadOptions();
                
                //objSelectedValues =(Hashtable) ViewState["SelectedValues"];

                foreach (string objEntry in txtSelectedValues.Text.Split(','))
                {
                    if(string.IsNullOrEmpty(sSelectedValues))
                    {
                        sSelectedValues = objEntry.Replace("'","");
                    }
                    else
                    {
                        sSelectedValues = sSelectedValues + "," +objEntry.Replace("'", "");
                    }

                }
                oSaveDownloadOptions.AddEntityAs = txtAddEntityAs.Text;
                oSaveDownloadOptions.SelectedValue = sSelectedValues;
                oSaveDownloadOptions.Mode = txtMode.Text;
                oSaveDownloadOptions.ClaimDateReported = hdnClaimReportedDate.Text; 
                oSaveDownloadOptions.PolicyId = Conversion.CastToType<int>(txtPolicyId.Text,out bSuccess);
                bResult = objHelper.SaveOptions(ref oSaveDownloadOptions);
                
                if (bResult)
                {
                   // ClientScript.RegisterStartupScript(this.GetType(), "UpdateIdsScript", "PreserveDownloadedRecordsIds('" + txtMode.Text + "', '" + oSaveDownloadOptions.UpdatedIds + "');", true);
                    GetDownloadOptions();
                }

              
            }
            catch (FaultException<RMException> ee)
            {
                btnSave.Enabled = false;
                hfException.Value = "true";
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;

            }
            catch (Exception ee)
            {
                btnSave.Enabled = false;
                hfException.Value = "true";
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                if (objHelper != null)
                    objHelper = null;
                if (oDS != null)
                    oDS = null;
                if (oFileData != null)
                    oFileData = null;
                objRetunDoc = null;
            }
        }

        //protected void btnAddNew_Click(object sender, EventArgs e)
        //{
        //    RemoveFromDataSet();
        //     DataSet p_objSet = (DataSet)ViewState["DbRecord"];
        //     if (p_objSet.Tables[0].Rows.Count > 0)
        //     {
        //         BindConflictingData();
        //     }
        //     else
        //         SaveData();
           
        //}

        //protected void btnUpdate_Click(object sender, EventArgs e)
        //{
            
        //    Hashtable objSelectedValue = (Hashtable)ViewState["SelectedValues"];
        //    DataSet p_objSet = (DataSet)ViewState["DbRecord"];
        //    objSelectedValue[ViewState["ConflictingValue"]] = objSelectedValue[ViewState["ConflictingValue"]]+ "|"+ txtUpdateWith.Text;
        //    RemoveFromDataSet();
        //    if (p_objSet.Tables[0].Rows.Count > 0)
        //    {
        //        BindConflictingData();
        //    }
        //    else
        //        SaveData();
       
        //}
     
        private void RemoveFromDataSet()
        {
            DataSet p_objSet = (DataSet)ViewState["DbRecord"];

            DataRow[] arrRow =null;
               if(string.Equals(txtMode.Text,"Unit",StringComparison.InvariantCultureIgnoreCase))
                arrRow= p_objSet.Tables[0].Select("UnitNo =" + ViewState["ConflictingValue"]);
            //    else if(string.Equals(txtMode.Text,"property",StringComparison.InvariantCultureIgnoreCase))
            //arrRow = p_objSet.Tables[0].Select("Pin =" + ViewState["ConflictingValue"]);
                else if(string.Equals(txtMode.Text,"entity",StringComparison.InvariantCultureIgnoreCase))
                   arrRow = p_objSet.Tables[0].Select("TaxId =" + ViewState["ConflictingValue"]);
               else if (string.Equals(txtMode.Text, "driver", StringComparison.InvariantCultureIgnoreCase))
                   arrRow = p_objSet.Tables[0].Select("TaxId =" + ViewState["ConflictingValue"]);

            foreach (DataRow objrow in arrRow)
            {
                p_objSet.Tables[0].Rows.Remove(objrow);
            }
            
            ViewState["DbRecord"] = p_objSet;
            
        }

        private void AddColumnsToGrid()
        {
            Dictionary<string, string> objPropertyHeader = null;
            Dictionary<string, string> objEntityHeader = null;
            //Dictionary<string,string> objVehicleHeader = null;
            //Dictionary<string, string> objDriverHeader = null;
            Dictionary<string, string> objUnitHeader = null;

            BoundField bfGridColumn;
            int i = 1;

            // Pradyumna GAP16 WWIG - Start
            int iPolicyStagingId = 0;
            bool bSuccess = false;
            iPolicyStagingId = Conversion.CastToType<int>(txtPolicyStagingId.Text, out bSuccess);
            // Pradyumna GAP16 WWIG - End

            if (string.Equals(txtMode.Text, "Unit", StringComparison.InvariantCultureIgnoreCase))
            {
                //while (i < gvdConflictingData.Columns.Count)
                //{
                //    gvdConflictingData.Columns.RemoveAt(i);
                //}
                gvDownloadItems.Columns.Clear();
                AddCheckboxColumnField();
                
                objUnitHeader = new   Dictionary<string,string>();
                AddDataToHashTable(ref objUnitHeader);

                foreach (string skey in objUnitHeader.Keys)
                {
                    bfGridColumn = new BoundField();
                    bfGridColumn.DataField = skey;
                    bfGridColumn.HeaderText = Conversion.ConvertObjToStr(objUnitHeader[skey]);
                    bfGridColumn.Visible = true;
                    //if (bSelectDuplicate)
                    //    gvdConflictingData.Columns.Add(bfGridColumn);
                    //else
                        gvDownloadItems.Columns.Add(bfGridColumn);
                }
                lblMode.Text = "Download Units";

            }

            else if (string.Equals(txtMode.Text, "ENTITY", StringComparison.InvariantCultureIgnoreCase))
            {
                AddCheckboxColumnField();
                objEntityHeader = new Dictionary<string, string>();                
                AddDataToHashTable(ref objEntityHeader);
                foreach (string skey in objEntityHeader.Keys)
                {
                    bfGridColumn = new BoundField();
                    bfGridColumn.DataField = skey;
                    bfGridColumn.HeaderText = Conversion.ConvertObjToStr(objEntityHeader[skey]);
                    //tanwar2 - Policy download from staging - start
                    //bfGridColumn.Visible = true;
                    if (iPolicyStagingId > 0 && string.Compare(skey, "TableId", true) == 0)
                    {
                        bfGridColumn.Visible = false;
                    }
                    else
                    {
                        bfGridColumn.Visible = true;
                    }
                    //tanwar2 - end
                    //if (bSelectDuplicate)
                    //    gvdConflictingData.Columns.Add(bfGridColumn);
                    //else
                        gvDownloadItems.Columns.Add(bfGridColumn);
                    lblMode.Text = "Download Entities";
                }
            }
            else if (string.Equals(txtMode.Text, "DRIVER", StringComparison.InvariantCultureIgnoreCase))
            {
                //while (i < gvdConflictingData.Columns.Count)
                //{
                //    gvdConflictingData.Columns.RemoveAt(i);
                //}
                gvDownloadItems.Columns.Clear();

                AddCheckboxColumnField();
                objEntityHeader = new Dictionary<string, string>();
                AddDataToHashTable(ref objEntityHeader);
                foreach (string skey in objEntityHeader.Keys)
                {
                    bfGridColumn = new BoundField();
                    bfGridColumn.DataField = skey;
                    bfGridColumn.HeaderText = Conversion.ConvertObjToStr(objEntityHeader[skey]);
                    bfGridColumn.Visible = true;
                    //if (bSelectDuplicate)
                    //    gvdConflictingData.Columns.Add(bfGridColumn);
                    //else
                        gvDownloadItems.Columns.Add(bfGridColumn);
                    lblMode.Text = "Download Drivers";
                }
            }
            //else if (string.Equals(txtMode.Text, "Property", StringComparison.InvariantCultureIgnoreCase))
            //{
            //    while (i < gvdConflictingData.Columns.Count)
            //    {
            //        gvdConflictingData.Columns.RemoveAt(i);
            //    }
            //    gvDownloadItems.Columns.Clear();
            //    AddCheckboxColumnField();

            //    objPropertyHeader = new Dictionary<string, string>();
            //    AddDataToHashTable(ref objPropertyHeader);
            //    foreach (string skey in objPropertyHeader.Keys)
            //    {
            //        bfGridColumn = new BoundField();
            //        bfGridColumn.DataField = skey;
            //        bfGridColumn.HeaderText = Conversion.ConvertObjToStr(objPropertyHeader[skey]);
            //        bfGridColumn.Visible = true;
            //        if (bSelectDuplicate)
            //            gvdConflictingData.Columns.Add(bfGridColumn);
            //        else
            //            gvDownloadItems.Columns.Add(bfGridColumn);
            //    }                
            //    lblMode.Text = "Download Properties";
            //}
        }
        //private void BindConflictingData()
        //{
        //    Hashtable objSelectedValue = (Hashtable)ViewState["SelectedValues"];
        //    DataView objView = null;
        //    DataSet p_objSet = (DataSet)ViewState["DbRecord"];
        //    foreach (DictionaryEntry objRow in objSelectedValue)
        //    {
        //        string sKey = Conversion.ConvertObjToStr(objRow.Key);
        //        objView = p_objSet.Tables[0].DefaultView;
        //        if(string.Equals(txtMode.Text,"Unit",StringComparison.InvariantCultureIgnoreCase))
        //        objView.RowFilter = "UnitNo=" + sKey;
        //        //else if(string.Equals(txtMode.Text,"property",StringComparison.InvariantCultureIgnoreCase))
        //        //objView.RowFilter = "Pin=" + sKey;
        //        else if(string.Equals(txtMode.Text,"entity",StringComparison.InvariantCultureIgnoreCase))
        //        objView.RowFilter = "Taxid=" + sKey;
        //        else if (string.Equals(txtMode.Text, "driver", StringComparison.InvariantCultureIgnoreCase))
        //            objView.RowFilter = "Taxid=" + sKey;

        //        if (objView.Count > 0)
        //        {
        //            ViewState["ConflictingValue"] = sKey;
        //            lblConflictingOption.Text = Conversion.ConvertObjToStr(objSelectedValue[sKey]) + " has followig Duplicate Values.Do you want to update the previous record or add a new record?";
       
        //            gvdConflictingData.DataSource = objView;
        //            gvdConflictingData.DataBind();
        //            DataGrid.Style.Value = "display:none;";
        //            div2.Style.Value = "";
        //            break;

        //        }
        //                objView = null;
        //    }
        //}

        protected void gvDownloadItemsData_OnRowBound(object sender, GridViewRowEventArgs e)
        {
            if (string.Equals(txtMode.Text, "entity", StringComparison.InvariantCultureIgnoreCase))
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataSet oDS = new DataSet();
                    DropDownList gridDDDownloadItemAs = null;
                    gridDDDownloadItemAs = (DropDownList)e.Row.Cells[1].FindControl("ddDownloadItemAs");
                    oDS.ReadXml(new StringReader(ViewState["EntityTypelist"].ToString()));
                    gridDDDownloadItemAs.DataSource = oDS.Tables[0];
                    gridDDDownloadItemAs.DataValueField = "option_text";
                    gridDDDownloadItemAs.DataTextField = "value";
                    gridDDDownloadItemAs.DataBind();

                    //tanawr2 - Defaulting dropdown for policy Download from staging - start
                    DataRowView rowView = e.Row.DataItem as DataRowView;
                    if (rowView.Row.Table.Columns.Contains("TableId"))
                    {
                        if (rowView["TableId"] != null)
                        {
                            gridDDDownloadItemAs.SelectedValue = Convert.ToString(rowView["TableId"]);
                        }
                    }
                    //tanwar2 - end
                }
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {


                HiddenField gridhdnSequenceNumber = (HiddenField)e.Row.Cells[0].FindControl("SequenceNumber");

                if (gridhdnSequenceNumber != null)
                {
                    try
                    {
                        gridhdnSequenceNumber.Value = DataBinder.Eval(e.Row.DataItem, "SequenceNumber").ToString();
                    }
                    catch
                    {
                        gridhdnSequenceNumber.Value = "";
                    }
                }
            }
        }

        private void AddCheckboxColumnField()
        {
            TemplateField oTemplateField = new TemplateField();
            oTemplateField.ItemTemplate = new GridViewItemTemplate("cbDownloadItemsGrid","SequenceNumber");
          
            gvDownloadItems.Columns.Insert(0, oTemplateField);

           
        }
    }
}