﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PS_PolicyUnitDetail.aspx.cs"
    Inherits="Riskmaster.UI.UI.PolicyInterface.PS_PolicyUnitDetail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Policy Unit Details</title>
    <%--tanwar2 - Policy Download from staging - start--%>
    <style type="text/css">
    .hide
    {
        display:none;
    }
    .td{
    display:inline-block;
    *display:inline; /* for IE7*/
    *zoom:1;/* for IE7*/
    width:24%;
    margin-top:5px;
    vertical-align:middle;
    font-family: Tahoma, Arial, Helvetica, sans-serif;
    }

    </style>
    <script src="../../Scripts/jquery/jquery-1.8.0.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        $('document').ready(function () {
            if ($('#hdnRemoveEmtpyFields').val() === 'True') {
                //hide empty tds
                $('#dvProperty table tbody tr td, #dvVehicle table tbody tr td, #dvWorker table tbody tr td, #dvCPP table tbody tr td').each(function () {
                    if ($(this).children('span:empty()').length === $(this).children('span').length) {
                        $(this).addClass('hide');
                        $(this).prev('td').addClass('hide');
                    }
                });

                //mark trs with all empty tds
                $('#dvProperty table tbody tr, #dvVehicle table tbody tr, #dvWorker table tbody tr, #dvCPP table tbody tr').each(function () {
                    if ($(this).children('td.hide').length === $(this).children('td').length) {
                        $(this).addClass('hide');
                    }
                });

                //Remove tr td having class hide
                $('.hide').remove();

                //Change all tr td to divs
                if ($('#dvProperty table tbody').html()) {
                    $('#dvProperty').html($('#dvProperty table tbody').html()
                    .replace(/<tr(.*)>/gi, '')
                    .replace(/<\/tr>/gi, '')
                    .replace(/<td/gi, '<div class="td"')
                    .replace(/<\/td/gi, '</div')
                    );
                }
                if ($('#dvVehicle table tbody').html()) {
                    $('#dvVehicle').html($('#dvVehicle table tbody').html()
                    .replace(/<tr(.*)>/gi, '')
                    .replace(/<\/tr>/gi, '')
                    .replace(/<td/gi, '<div class="td"')
                    .replace(/<\/td/gi, '</div')
                    );
                }
                if ($('#dvWorker table tbody').html()) {
                    $('#dvWorker').html($('#dvWorker table tbody').html()
                    .replace(/<tr(.*)>/gi, '')
                    .replace(/<\/tr>/gi, '')
                    .replace(/<td/gi, '<div class="td"')
                    .replace(/<\/td/gi, '</div')
                    );
                }
                if ($('#dvCPP table tbody').html()) {
                    $('#dvCPP').html($('#dvCPP table tbody').html()
                    .replace(/<tr(.*)>/gi, '')
                    .replace(/<\/tr>/gi, '')
                    .replace(/<td/gi, '<div class="td"')
                    .replace(/<\/td/gi, '</div')
                    );
                }

                $('.td:even').addClass('required');
            }
        });
    </script>
    <%--tanwar2 - Policy Download from staging - end--%>
</head>
<body>
    <form id="form1" runat="server">
    <table>
        <tr>
            <td colspan="2">
                <uc1:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
    </table>
    <div class="msgheader" id="formtitle">
        Policy Unit Details</div>
    <div runat="server" id="dvProperty" visible="false">
    <table width="98%" border="0" cellspacing="8" celpadding="4" id="tbProperty" runat="server">
    <colgroup>
                <col width="25%" class="required" />
                <col width="25%" align="left" />
                <col width="25%" align="left" class="required" />
                <col width="25%" align="left" />
    </colgroup>
    <tr>
    <td>
                    <asp:Label Text="Insurance Line" runat="server" class="required"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblPropInsLn"></asp:Label>
                </td>
                 <td>
                    <asp:Label runat="server" Text="Unit" class="required" ></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblPropUnit"></asp:Label>
                </td>
    </tr>
    <tr>
                <td>
                    <asp:Label  runat="server"  Text="Status" class="required"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblPropStatus"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" Text="Product" class="required"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblPropProduct"></asp:Label>
                </td>
            </tr>
            <tr>
            <td><asp:Label Text="Ratebook" runat="server" class="required"></asp:Label> </td>
            <td><asp:Label runat="server" ID="lblRatebook"></asp:Label> </td>
            <td> <asp:Label runat="server" Text="Premium" class="required"></asp:Label></td>
            <td> <asp:Label runat="server" ID="lblPropPrem"></asp:Label></td>
            </tr>
            <tr>
            <td> <asp:Label runat="server" Text="Location Address" class="required"></asp:Label></td>
            <td> <asp:Label runat="server" ID="lblLocAddr"></asp:Label></td>
            <td> <asp:Label runat="server" Text="Location City" class="required"></asp:Label></td>
            <td> <asp:Label runat="server" ID="lblLocCity"></asp:Label></td>
            </tr>
            <tr>
            <td> <asp:Label runat="server" Text="State" class="required"></asp:Label></td>
            <td> <asp:Label runat="server" ID="lblLocState"></asp:Label></td>
            <td><asp:Label runat="server" Text="Territory" class="required"></asp:Label></td>
            <td><asp:Label runat="server" id="lblPropTerr"></asp:Label></td>
            </tr>
            <tr>
            <td><asp:Label runat="server" Text="Postal Code" class="required"></asp:Label></td>
            <td><asp:Label runat="server" ID="lblPropPostalCd"></asp:Label> </td>
            <td> <asp:Label runat="server" Text="Protection Class" class="required"></asp:Label></td>
            <td> <asp:Label runat="server" ID="lblProtectnClas"></asp:Label></td>
            </tr>
            <tr>
            <td> <asp:Label Text="Deductible" runat="server" class="required"></asp:Label></td>
            <td> <asp:Label runat="server" ID="lblDeduc"></asp:Label></td>
            <td><asp:Label Text="Number Of Families" runat="server" class="required"></asp:Label></td>
            <td><asp:Label runat="server" ID="lblNoFamilies"></asp:Label></td>
            </tr>
            <tr>
            <td><asp:Label Text="Type of Construction" runat="server" class="required"></asp:Label></td>
            <td><asp:Label runat="server" ID="lblCons"></asp:Label></td>
            <td><asp:Label Text="Year Of Construction" runat="server" class="required"></asp:Label></td>
            <td><asp:Label runat="server" ID="lblYrCons"></asp:Label></td>
            </tr>
            <tr>
            <td><asp:Label Text="Inside City" runat="server" class="required"></asp:Label></td>
            <td><asp:Label runat="server" ID="lblInsideCity"></asp:Label></td>
            <td><asp:Label Text="Occupancy" runat="server" class="required"></asp:Label> </td>
            <td><asp:Label runat="server" ID="lblOccupancy"></asp:Label></td>
            </tr>
           <tr><td colspan="4"></td></tr>
           <tr><td colspan="4"></td></tr>
            <tr runat="server" id="trCovInfoHeading" visible="false">
            <td colspan="4" align="center"><asp:Label Text="Coverage Information:" runat="server" class="required"></asp:Label></td>
            </tr>
    </table>
    </div>
    <div runat="server" id="dvVehicle" visible="false">
        <table width="98%" border="0" cellspacing="8" celpadding="4" >
            <colgroup>
                <col width="25%" class="required" />
                <col width="25%" align="left" />
                <col width="25%" align="left" class="required" />
                <col width="25%" align="left" />
            </colgroup>
            <tr>
                <td>
                    <asp:Label Text="Type Vehicle" runat="server">
                    </asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblTypeVeh"></asp:Label>
                </td>
                <td>
                    <asp:Label Text="Insurance Line" CssClass="required" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblInsLn"></asp:Label>
                </td>
            </tr>
           
            <tr>
                <td>
                    <asp:Label runat="server" CssClass="required" Text="Status"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblStatus"></asp:Label>
                </td>
                <td>
                    <asp:Label CssClass="required" runat="server" Text="Product"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblProduct"></asp:Label>
                </td>
            </tr>
            
            <tr>
                <td>
                    <asp:Label runat="server" Text="Unit" CssClass="required"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblUnitNum"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" Text="Unit Premium">
                    </asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblUnitPrem"></asp:Label>
                </td>
            </tr>
           
            <tr>
                <td>
                    <asp:Label runat="server" Text="Rate State">
                    </asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblRateState">
                    </asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" Text="Territory"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblTerritory"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label Text="Vehicle Year" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblVehicleYr"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" Text="Make/Model"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblModel"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" Text="Vehicle Symbol"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblVehSymbol"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" Text="Purchase Date">
                    </asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblPurchaseDt"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" Text="Cost New"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblCostNm">
                    </asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" Text="Serial VIN"></asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblSerial"></asp:Label>
                </td>
            </tr>
            <tr>
            <td>
            <asp:Label runat="server" Text="Primary Class"></asp:Label>
            </td>
            <td>
            <asp:Label runat="server" ID="lblPrimaryClas"></asp:Label>
            </td>
            <td><asp:Label runat="server" Text="Secondary Class"></asp:Label> </td>
            <td> <asp:Label runat="server" ID="lblSecClass"></asp:Label> </td>
            </tr>
            <tr>
            <td> <asp:Label runat="server" Text="Airbag Discount"></asp:Label>  </td>
            <td><asp:Label runat="server" ID="lblAirBagDsc"></asp:Label> </td>
            <td><asp:Label runat="server" Text="Anti-Lock Brakes"></asp:Label>  </td>
            <td> <asp:Label runat="server" ID="lblBrakes"></asp:Label> </td>
            </tr>
            <tr>
            <td> <asp:Label runat="server" Text="Anti-Theft Indicator"></asp:Label> </td>
            <td> <asp:Label runat="server" ID="lblTheft"></asp:Label> </td>
            <td> <asp:Label runat="server" Text="Body Type"></asp:Label> </td>
            <td> <asp:Label runat="server" ID="lblBodyType"></asp:Label> </td>
            </tr>
            <tr> 
            <td> <asp:Label Text="Use of Vehicle" runat="server"></asp:Label> </td>
            <td> <asp:Label runat="server" ID="lblUseOfVeh"></asp:Label>  </td>
            </tr>
        </table>
    </div>
    <div runat="server" id="dvWorker" visible="false">
    <table width="98%"  border="0" cellspacing="8" celpadding="4" >
     <colgroup>
                <col width="25%" class="required" />
                <col width="25%" align="left" />
                <col width="25%" align="left" class="required" />
                <col width="25%" align="left" />
            </colgroup>
    <tr>
    <td> <asp:Label runat="server" Text="Site"></asp:Label></td>
    <td> <asp:Label runat="server" ID="lblSite"></asp:Label> </td>
    <td> <asp:Label runat="server" Text="Status"></asp:Label></td>
    <td> <asp:Label runat="server" ID="lblWCVStatus"></asp:Label></td>
    </tr>
    <tr>
    <td> <asp:Label runat="server" Text="Name"></asp:Label> </td>
    <td> <asp:Label runat="server" ID="lblNm"></asp:Label></td>
    <td> <asp:Label runat="server" Text="Optional"></asp:Label> </td>
    <td> <asp:Label runat="server" ID="lblOptional"></asp:Label> </td>
    </tr>
    <tr>
    <td> <asp:Label runat="server" Text="Address"></asp:Label>    </td>
    <td> <asp:Label runat="server" ID="lblAddr"></asp:Label> </td>
    <td> <asp:Label runat="server" Text="City"></asp:Label></td>
    <td> <asp:Label runat="server" ID="lblCity"></asp:Label> </td>
    </tr>
    <tr>
    <td> <asp:Label runat="server" Text="State"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblState"></asp:Label> </td>
    <td> <asp:Label runat="server" Text="Postal Code"></asp:Label></td>
    <td> <asp:Label runat="server" ID="lblPostalCd"></asp:Label></td>
    </tr>
    <tr>
    <td><asp:Label runat="server" Text="County Code"></asp:Label> </td>
    <td> <asp:Label runat="server" ID="lblCountyCd"></asp:Label> </td>
    <td> <asp:Label runat="server" Text="Phone Number"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblPhnNo"></asp:Label> </td>
    </tr>
    <tr>
    <td><asp:Label runat="server" Text="Contact"></asp:Label> </td>
    <td><asp:Label runat="server" ID="lblContact"></asp:Label> </td>
    <td><asp:Label runat="server" Text="Tax Location"></asp:Label> </td>
    <td><asp:Label runat="server" ID="lblTaxLoc"></asp:Label> </td>
    </tr>
    <tr>
    <td> <asp:Label runat="server"  Text="Audit Basis"></asp:Label> </td>
    <td> <asp:Label runat="server" ID="lblAuditBasis"></asp:Label></td>
    <td> <asp:Label runat="server" Text="Audit Type"></asp:Label></td>
    <td> <asp:Label runat="server" ID="lblAduitType"></asp:Label> </td>
    </tr>
    <tr>
    <td> <asp:Label runat="server" Text="Auditor (Interim Audit)"></asp:Label></td>
    <td colspan="3"> <asp:Label runat="server" ID="lblIAud1"></asp:Label>
    &nbsp; &nbsp;
    <asp:Label runat="server" ID="lblIAud2"></asp:Label>
    </td>
    
    </tr>
    <tr>
    <td> <asp:Label  runat="server" Text="Auditor (Check Audit)"></asp:Label></td>
    <td colspan="3"> <asp:Label runat="server" ID="lblCAudit1"></asp:Label>
    &nbsp; &nbsp;
    <asp:Label runat="server" ID="lblCAudit2"></asp:Label>
    </td>
    
    </tr>
     <tr>
    <td> <asp:Label  runat="server" Text="Auditor (Final Audit)"></asp:Label></td>
    <td colspan="3"> <asp:Label runat="server" ID="lblFAudit1"></asp:Label>
    &nbsp; &nbsp;
    <asp:Label runat="server" ID="lblFAudit2"></asp:Label>
    </td>
    
    </tr>
    <tr>
    <td><asp:Label runat="server" Text="Unemployment Number"></asp:Label> </td>
    <td><asp:Label runat="server" ID="lblUnemployNum"></asp:Label> </td>
    <td><asp:Label runat="server" Text="Number Of Employees"></asp:Label> </td>
    <td> <asp:Label runat="server" ID="lblEmpCt"></asp:Label> </td>
    </tr>
    <tr>
    <td><asp:Label runat="server" Text="Federal Employer Identification Number (FEIN)"></asp:Label> </td>
    <td> <asp:Label runat="server" ID="lblFEIN"></asp:Label></td>
    <td> <asp:Label runat="server" Text="Standard Industrial Classification CODE (SIC)"></asp:Label></td>
    <td> <asp:Label runat="server" ID="lblSIC"></asp:Label></td>
    </tr>
    <tr>
    <td><asp:Label runat="server" Text="Governing Class Code"></asp:Label> </td>
    <td> <asp:Label runat="server" ID="lblGCC"></asp:Label></td>
   <td> <asp:Label runat="server" Text="Sequence No."></asp:Label></td>
   <td> <asp:Label runat="server" ID="lblSeqNo"></asp:Label></td>
    </tr>
    <tr>
     <td> <asp:Label ID="Label1" runat="server" Text="Governing Class Description"></asp:Label></td>
    <td colspan="3"> <asp:Label runat="server" ID="lblGCD"></asp:Label> </td>
    </tr>
    </table>
    </div>
    <div runat="server" id="dvManual" visible="false">
        <table width="98%" id="tableM" runat="server" border="0" cellspacing="8" celpadding="4" >
        </table>
    </div>
    <div id="dvCPP" runat="server" visible="false">
    <table width="98%" border="0" cellpadding="4" cellspacing="8" >
    <colgroup >
    <col width="25%" class="required" />
    <col width="25%" />
    <col width="25%" class="required" />
    <col width="25%" />
    </colgroup>
    <tr>
    <td><asp:Label Text="Insurance Line" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblCPPInsLn"></asp:Label></td>
    <td><asp:Label Text="Product" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblCPPProd"></asp:Label></td>
    </tr>
    <tr>
    <td><asp:Label Text="Status" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblCPPStatus"></asp:Label></td>
    <td><asp:Label Text="Ratebook" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblCPPRatebook"></asp:Label></td>
    </tr>
    <tr>
    <td><asp:Label Text="Premium" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblCPPprem"></asp:Label></td>
    <td><asp:Label Text="State" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblCPpState"></asp:Label></td>
    </tr>
    <tr>
    <td><asp:Label Text="Class Code" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblCPPCd1" ></asp:Label> &nbsp; &nbsp; <asp:Label runat="server" ID="lblCPPCd2"></asp:Label></td>
    <td><asp:Label Text="Description" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblCPpDesc"></asp:Label></td>
    </tr>
    <tr runat="server" visible="false" id="trVeh1">
    <td><asp:Label Text="Unit" runat="server"></asp:Label></td> 
    <td><asp:Label runat="server" ID="lblCPPUnit"></asp:Label></td>
    <td><asp:Label Text="Model Year" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblCppModelYr"></asp:Label></td>
    </tr>
    <tr runat="server" visible="false" id="trVeh2">
    <td><asp:Label Text="Unit Stated Amount" runat="server"></asp:Label></td>
    <td><asp:Label ID="lblUnitStatedAmt" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" Text="Vehicle Identification Number"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblVIN"></asp:Label></td>
    </tr>
    <tr runat="server" visible="false" id="trProp1">
    <td><asp:Label Text="Location" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblCppLoc"></asp:Label></td>
    <td><asp:Label Text="Building" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblBuilding"></asp:Label></td>
    </tr>
  <%--  <tr runat="server" visible="false" id="trProp2">
    <td><asp:Label Text="AddrCity" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblCppAddr"></asp:Label></td>
    <td><asp:Label Text="County Code" runat="server"></asp:Label></td>
    <td><asp:Label runat="server" ID="lblCppCountyCd"></asp:Label></td>
    </tr>--%>
    </table>
    </div>
    <asp:HiddenField ID="hfStatUnitNumber" Value="" runat="server" />
    <%--tanwar2 - Policy Download from staging - start--%>
    <asp:HiddenField ID="hdnRemoveEmtpyFields" runat="server" Value="" />
    <%--tanwar2 - Policy Download from staging - end--%>
    </form>
</body>
</html>
