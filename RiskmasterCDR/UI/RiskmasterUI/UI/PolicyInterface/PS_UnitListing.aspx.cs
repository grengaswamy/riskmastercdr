﻿/***************************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            
 ***************************************************************************************************
 * 05/05/2014 | 33574   | pgupta93   | Changes for Continuous Trigger and Policy Search functionality.
 ***************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.IO;
using Riskmaster.BusinessHelpers;
using Riskmaster.BusinessAdaptor.Common;
using System.ServiceModel;
using Riskmaster.UI.PolicyInterfaceService;
using System.Data;
using System.Collections;
using Riskmaster.Common;
using System.Xml;
using Telerik.Web.UI;
using System.Xml.Linq;
using System.Text;

namespace Riskmaster.UI.UI.PolicyInterface
{

   
      public partial class PS_UnitListing :  NonFDMBasePageCWS
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                bool bSuccess = false;
                string sReturnValue = string.Empty;
                XmlDocument xmlDoc = new XmlDocument();
                XmlNode xmlNode = null;

                if (!IsPostBack)
                {
                    Div1.Style.Value = "display:none;";
                    txtPolicyId.Text = AppHelper.GetQueryStringValue("PolicyId");
                    txtLossDate.Text = AppHelper.GetQueryStringValue("LossDate");
                    txtPolicyFlag.Text = AppHelper.GetQueryStringValue("DownloadPolicyWithoutClaim");//MITS:33574
                    txtLoBCode.Text = AppHelper.GetQueryStringValue("LOBCode");//MITS:33574
                    hdnClaimDateReported.Text = AppHelper.GetQueryStringValue("ClaimDateReported");////Ashish Ahuja: Claims Made Jira 1342
                     // GetDownloadOptions();

                   
                }
            }
            catch (FaultException<RMException> ee)
            {
                btnSave.Enabled = false;
                hfException.Value = "true";
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;
            
                

            }
            catch (Exception ee)
            {
                btnSave.Enabled = false;
                hfException.Value = "true";
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
               
            }
        }
     
        protected void gvDownloadItems_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            
            //GetDownloadUnits((Telerik.Web.UI.RadGrid)source);
            bool bSuccess = false;
             GetDownloadUnits((Telerik.Web.UI.RadGrid)source);
           
        }
       
        protected void gvDownloadItems_PreRender(object sender, EventArgs e)
        {
            GridFilterMenu menu = gvDownloadItems.FilterMenu;
            int i = 0;
            while (i < menu.Items.Count)
            {
                if (menu.Items[i].Text == "Between" || menu.Items[i].Text == "NotBetween" || menu.Items[i].Text == "IsNull" || menu.Items[i].Text == "NotIsNull" || menu.Items[i].Text == "GreaterThan" || menu.Items[i].Text == "LessThan" || menu.Items[i].Text == "GreaterThanOrEqualTo" || menu.Items[i].Text == "LessThanOrEqualTo" || menu.Items[i].Text == "IsEmpty" || menu.Items[i].Text == "NotIsEmpty")
                {
                    menu.Items.RemoveAt(i);
                }
                else
                {
                    i = (i + 1);
                }
            }
        }
        
        protected void btnSave_Click(object sender, EventArgs e)
        {
            DataSet oDS = new DataSet();
            DataRow objRow;
            XmlDocument objRetunDoc = null;
            DataView objView = null;
            bool bSuccess = false;
            try
            {

                        SaveData();
                        if (txtMode.Text == "unit")
                        {
	              
                            ClientScript.RegisterStartupScript(this.GetType(), "script", "showUnitInterestListing();", true);

                        }
               
            }
            catch (FaultException<RMException> ee)
            {
                btnSave.Enabled = false;
                hfException.Value = "true";
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;
                
            }
            catch (Exception ee)
            {
                btnSave.Enabled = false;
                hfException.Value = "true";
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                
            }
            finally
            {
                
                if (oDS != null)
                    oDS = null;
                 objRetunDoc = null;
            }

        }

        private void GetDownloadUnits(Telerik.Web.UI.RadGrid gvUnitGrid)
        {
            PolicyInterfaceBusinessHelper objHelper = new PolicyInterfaceBusinessHelper();
            DisplayFileData oFileData = null;
            DataSet oDS = new DataSet();
            DataRow objRow;
            XmlDocument objRetunDoc = null;

            try
            {
                    oFileData = objHelper.GetUnitXml();
                    txtMode.Text = oFileData.Mode;

                    if (oFileData != null && !string.IsNullOrEmpty(oFileData.ResponseXML))
                    {
                        objRetunDoc = new XmlDocument();
                        objRetunDoc.LoadXml(oFileData.ResponseXML);
                        oDS = ConvertXmlDocToDataSet(objRetunDoc);
                        if (oDS != null && oDS.Tables.Count > 0)
                        {
                            gvUnitGrid.VirtualItemCount = oDS.Tables[1].Rows.Count;
                            gvUnitGrid.DataSource = oDS.Tables[1];
                            ViewState["SearchedUnitData"] = oDS.Tables[1];
                        //    gvDownloadItems.DataBind();
                        }
                        else
                        {
                            btnSave.Enabled = false;
                        }

                        SearchData.Style.Value = "display:none;";
                        DataGrid.Style.Value = "";
                      //  div2.Style.Value = "display:none;";
                    }
                    else if (ViewState["SearchedUnitData"] != null)
                    {
                        DataTable objData = (DataTable)ViewState["SearchedUnitData"];
                        gvUnitGrid.VirtualItemCount = objData.Rows.Count;
                        gvUnitGrid.DataSource = objData;

                    }
                    else if (oFileData != null && oFileData.ShowUnitSearch)
                    {
                        DataGrid.Style.Value = "display:none;";
                        SearchData.Style.Value = "display:block;";
                    }
                    else
                    {

                        lblMode.Visible = false;
                        //  div2.Style.Value = "display:none;";
                        ClientScript.RegisterStartupScript(this.GetType(), "script", "showPolicySuccess();", true);
                    }
            }
            catch (FaultException<RMException> ee)
            {
                btnSave.Enabled = false;
                hfException.Value = "true";
                throw ee;


            }
            catch (Exception ee)
            {
                btnSave.Enabled = false;
                hfException.Value = "true";
                throw ee;
            }
            finally
            {
                if (objHelper != null)
                    objHelper = null;
                if (oDS != null)
                    oDS = null;
                if (oFileData != null)
                    oFileData = null;
                objRetunDoc = null;
            }
            
        }
  
        private void SaveData()
        {
            PolicyInterfaceBusinessHelper objHelper = new PolicyInterfaceBusinessHelper();
            DisplayFileData oFileData = null;
            DataSet oDS = new DataSet();

            SaveDownloadOptions oSaveDownloadOptions = null;
            DataRow objRow;

            XmlDocument objRetunDoc = null;
            bool bResult = false;
            //Hashtable objSelectedValues = null;
            string sSelectedValues = string.Empty;
            string sSelelectedUnits = string.Empty;
            bool bSuccess= false;
            try
            {
                oSaveDownloadOptions = new SaveDownloadOptions();
                txtMode.Text = "unit";
                //objSelectedValues =(Hashtable) ViewState["SelectedValues"];
                if (string.IsNullOrEmpty(txtSelectedValues.Text) == false)
                {
                    sSelelectedUnits = txtSelectedValues.Text;
                }
                else
                {
                    sSelelectedUnits = txtChkBoxSelected.Text;
                }
                foreach (string objEntry in sSelelectedUnits.Split(','))
                {
                    if (string.IsNullOrEmpty(sSelectedValues))
                    {
                        sSelectedValues = objEntry.Replace("'", "");
                    }
                    else
                    {
                        sSelectedValues = sSelectedValues + "," + objEntry.Replace("'", "");
                    }

                }
                oSaveDownloadOptions.SelectedValue = sSelectedValues;
                oSaveDownloadOptions.Mode = txtMode.Text;
                oSaveDownloadOptions.LossDate = txtLossDate.Text;
                oSaveDownloadOptions.ClaimDateReported = hdnClaimDateReported.Text;////Ashish Ahuja: Claims Made Jira 1342
                oSaveDownloadOptions.PolicyId = Conversion.CastToType<int>(txtPolicyId.Text, out bSuccess);
               
                bResult = objHelper.SaveOptions(ref oSaveDownloadOptions);

            }
            catch (FaultException<RMException> ee)
            {
                btnSave.Enabled = false;
                hfException.Value = "true";
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;
                throw ee;

            }
            catch (Exception ee)
            {
                btnSave.Enabled = false;
                hfException.Value = "true";
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                if (objHelper != null)
                    objHelper = null;
                if (oDS != null)
                    oDS = null;
                if (oFileData != null)
                    oFileData = null;
                objRetunDoc = null;
            }
        }
          
        protected void gvDownloadItems_ItemDataBound(object sender, GridItemEventArgs e)
        {

                if (e.Item is GridPagerItem)
                {
                    GridPagerItem pager = (GridPagerItem)e.Item;
                    Label lbl = (Label)pager.FindControl("ChangePageSizeLabel");
                    lbl.Visible = false;

                    RadComboBox combo = (RadComboBox)pager.FindControl("PageSizeComboBox");
                    combo.Visible = false;
                }
                if (e.Item is GridDataItem)
                {

                    DataRowView dv = (DataRowView)e.Item.DataItem;
                    HiddenField gridhdnSequenceNumber = (HiddenField)e.Item.Cells[0].FindControl("SequenceNumber");
                    HiddenField gridhdnUnitNumber = (HiddenField)e.Item.Cells[0].FindControl("UnitNumber");
                    CheckBox chkBox = (CheckBox)e.Item.Cells[0].FindControl("chkBox");

                    if (gridhdnSequenceNumber != null)
                    {
                        try
                        {
                            if (txtChkBoxSelected.Text.Contains("'" + Conversion.ConvertObjToStr(dv["SequenceNumber"]) + "'"))
                            {
                                chkBox.Checked = true;

                            }
                            gridhdnSequenceNumber.Value = DataBinder.Eval(e.Item.DataItem, "SequenceNumber").ToString();
                            gridhdnUnitNumber.Value = DataBinder.Eval(e.Item.DataItem, "UnitNo").ToString();
                        }
                        catch
                        {
                            gridhdnSequenceNumber.Value = "";
                        }
                    }
                }
        }

        protected void gvConfirmSaveList_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridPagerItem)
            {
                GridPagerItem pager = (GridPagerItem)e.Item;
                Label lbl = (Label)pager.FindControl("ChangePageSizeLabel");
                lbl.Visible = false;

                RadComboBox combo = (RadComboBox)pager.FindControl("PageSizeComboBox");
                combo.Visible = false;
            }
            if (e.Item is GridDataItem)
            {

                DataRowView dv = (DataRowView)e.Item.DataItem;
                HiddenField gridhdnSequenceNumber = (HiddenField)e.Item.Cells[0].FindControl("SequenceNumber1");
                HiddenField gridhdnUnitNumber = (HiddenField)e.Item.Cells[0].FindControl("UnitNumber1");
                CheckBox chkBox = (CheckBox)e.Item.Cells[0].FindControl("chkBox1");

                if (gridhdnSequenceNumber != null)
                {
                    try
                    {
                        //if (txtChkBoxSelected.Text.Contains("'" + Conversion.ConvertObjToStr(dv["SequenceNumber"]) + "'"))
                        if (txtSelectedValues.Text.Contains("'" + Conversion.ConvertObjToStr(dv["SequenceNumber"]) + "'"))
                        {
                            chkBox.Checked = true;

                        }
                        gridhdnSequenceNumber.Value = DataBinder.Eval(e.Item.DataItem, "SequenceNumber").ToString();
                        gridhdnUnitNumber.Value = DataBinder.Eval(e.Item.DataItem, "UnitNo").ToString();
                    }
                    catch
                    {
                        gridhdnSequenceNumber.Value = "";
                    }
                }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            //UnitSearch objSearch = null;
            //PolicyInterfaceBusinessHelper objHelper = null;
           // UnitListing objUnitListing = null;
            DataTable objDataTable = null;


            try
            {
                //objSearch = new UnitSearch();

                //objSearch.City = tbCity.Text;
                //objSearch.State = tbState.Text;
                //objSearch.Status = tbStatus.Text;
                //objSearch.UnitDescription = tbDesc.Text;
                //objSearch.UnitNumber = tbUnitNo.Text;
                //objSearch.UnitType = tbUnitType.Text;
                //objSearch.VehicleID = tbVin.Text;

                //objHelper = new PolicyInterfaceBusinessHelper();

               // objUnitListing = objHelper.SearchPolicyUnits(objSearch);
                objDataTable = GetUnitListing(string.Empty);
           //     objDataSet = ConvertXmlDocToDataSet(ToXmlDocument(objUnitListing.UnitList));
                //objUnitListing.UnitList.CopyTo(
                //if(objDataSet.Tables.Count>0)
                //gvDownloadItems.DataSource = objDataSet.Tables[1];
                //if (objUnitListing.UnitList.Count() != 0)
                ViewState["SearchedUnitData"] = null;
                txtChkBoxSelected.Text = string.Empty;
                if ((objDataTable!=null) && ( objDataTable.Rows.Count !=0))
                {
                    //objDataTable = ToDataTable(objUnitListing.UnitList);
                    gvDownloadItems.DataSource = objDataTable;
                    gvDownloadItems.DataBind();
                    ViewState["SearchedUnitData"] = objDataTable;
                    DataGrid.Style.Value = "";
                    SearchData.Style.Value = "";
                    NoRecords.Style.Value = "display:none";
                }
                else
                {
                    NoRecords.Style.Value = "display:block";
                    DataGrid.Style.Value = "display:none";
                }



            }
            catch (Exception ee)
            {
                btnSave.Enabled = false;
                hfException.Value = "true";
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
               // objSearch = null;
              //  objHelper = null;
                objDataTable = null;
            }
           
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            DataTable objDataTable = null;
           // PolicyInterfaceBusinessHelper objHelper = null;
           // UnitListing objUnitListing = null;
            //UnitSearch objSearch = null;
            string sSelectedValues = string.Empty;
            try
            {
                //foreach (string objEntry in txtChkBoxSelected.Text.Split(','))
                //{
                //    if (string.IsNullOrEmpty(sSelectedValues))
                //    {
                //        sSelectedValues = objEntry.Replace("'", "");
                //    }
                //    else
                //    {
                //        sSelectedValues = sSelectedValues + "," + objEntry.Replace("'", "");
                //    }
                //}

                //objSearch = new UnitSearch();
                //objHelper = new PolicyInterfaceBusinessHelper();
                //objSearch.SelectedValue = sSelectedValues;
                bool bSkipConfirm = true;
                if (ViewState["SearchedUnitData"] != null && ((DataTable)ViewState["SearchedUnitData"]).Rows.Count > gvDownloadItems.PageSize)
                {
                    bSkipConfirm = false;
                }
                //objUnitListing = objHelper.SearchPolicyUnits(objSearch);
               // objDataTable = GetUnitListing(sSelectedValues);
                //objDataTable = ToDataTable(objUnitListing.UnitList);
                txtSelectedValues.Text = txtChkBoxSelected.Text;
                objDataTable = ConfirmationGrid();
                ViewState["SearchedUnitData"] = objDataTable;
                gvConfirmSaveList.DataSource = objDataTable;
                gvConfirmSaveList.DataBind();
                DataGrid.Style.Value = "display:none;";
                SearchData.Style.Value = "display:none;";
                NoRecords.Style.Value = "display:none";
               // ViewState["SearchedUnitData"] = objDataTable;
               // gvConfirmSaveList.DataSource = objDataTable;
               // gvConfirmSaveList.DataBind();
                //if (gvDownloadItems.PageCount <= 1)
                if (bSkipConfirm)
                {
                   btnSave_Click(gvConfirmSaveList, new EventArgs());
                }
                else
                {
                    Div1.Style.Value = "";
                    lblMode.Text = "Confirm Selected Units";
                }

            }
            catch (Exception ee)
            {
                btnSave.Enabled = false;
                hfException.Value = "true";
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
               // objHelper = null;
               // objSearch = null;
                
            }

        }
        
        private DataTable ToDataTable(XElement element)
        {
            DataSet ds = new DataSet();
            string rawXml = element.ToString();
            ds.ReadXml(new StringReader(rawXml));
            return ds.Tables[0];
        }

        DataTable ToDataTable(IEnumerable<XElement> elements)
        {
            return ToDataTable(new XElement("Root", elements));
        }

        protected void gvConfirmSaveList_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            DataTable objDataTable = null;
            objDataTable = ConfirmationGrid();
            //ViewState["SearchedUnitData"] = objDataTable;
            gvConfirmSaveList.VirtualItemCount = objDataTable.Rows.Count;
            gvConfirmSaveList.DataSource = objDataTable;
        }

        private DataTable ConfirmationGrid()
        {
            string sSelectedValues = string.Empty;
            DataTable objDataTable = null;
            foreach (string objEntry in txtChkBoxSelected.Text.Split(','))
            {
                if (string.IsNullOrEmpty(sSelectedValues))
                {
                    sSelectedValues = objEntry.Replace("'", "");
                }
                else
                {
                    sSelectedValues = sSelectedValues + "," + objEntry.Replace("'", "");
                }
            }
            objDataTable = GetUnitListing(sSelectedValues);
            return objDataTable;

        }

        private DataTable GetUnitListing(string sSelectedValues)
        {
            UnitSearch objSearch = null;
            PolicyInterfaceBusinessHelper objHelper = null;
            UnitListing objUnitListing = null;
            DataTable objDataTable = null;
            StringBuilder sResponseXML = null;
            DataSet objDataSet = null;
            try
            {
                objSearch = new UnitSearch();
                if (string.IsNullOrEmpty(sSelectedValues))
                {   //Search button clicked
                    objSearch.City = tbCity.Text;
                    objSearch.State = tbState.Text;
                    objSearch.Status = tbStatus.Text;
                    objSearch.UnitDescription = tbDesc.Text;
                    char [] sDescToArray = objSearch.UnitDescription.ToCharArray();
                    if (sDescToArray.Length > 0)
                    {
                        if (sDescToArray[0] == '*')
                        {
                            sDescToArray[0] = '%';
                        }
                        if (sDescToArray[sDescToArray.Length - 1] == '*')
                        {
                            sDescToArray[sDescToArray.Length - 1] = '%';
                        }
                        objSearch.UnitDescription = new String(sDescToArray);
                    }
                    objSearch.UnitDescription = objSearch.UnitDescription.Replace("*", "");//apart from frist and last occurence of '*' others will be removed.
                    objSearch.UnitNumber = tbUnitNo.Text;
                    objSearch.UnitType = tbUnitType.Text;
                    objSearch.VehicleID = tbVin.Text;

                    
                }
                else
                {   //Binding confirmation grid
                    objSearch.SelectedValue = sSelectedValues;

                    
                }
                objHelper = new PolicyInterfaceBusinessHelper();

                objUnitListing = objHelper.SearchPolicyUnits(objSearch);


                if (objUnitListing.UnitList.Count() != 0)
                {
                    sResponseXML = new StringBuilder();
                    sResponseXML.Append("<RootNode>");
                    foreach (XElement objEle in objUnitListing.UnitList)
                    {
                        sResponseXML.Append(objEle.ToString());
                    }

                    sResponseXML.Append("</RootNode>");

                    objDataTable = new DataTable();
                    objDataSet = new DataSet();

                    objDataSet.ReadXml(new StringReader(sResponseXML.ToString()));

                   objDataTable = objDataSet.Tables[0];
                  

                }

            }
            finally
            {
                objSearch = null;
                objHelper = null;
                objDataSet = null;
                sResponseXML = null;
            }
            return objDataTable;
           
        }

        

        // Pradyumna 03142014 - MITS# 35296 - Start
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization></Authorization> 
             <Call>
              <Function></Function> 
             </Call>
             <Document>
                <MultiUnitDownload></MultiUnitDownload>
             </Document>
            </Message>
            ");
            return oTemplate;
        }
        // Pradyumna 03142014 - MITS# 35296 - End
    }
}