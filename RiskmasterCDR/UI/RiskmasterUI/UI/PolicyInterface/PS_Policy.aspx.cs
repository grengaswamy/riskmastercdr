﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.UI.PolicyInterfaceService;
using Riskmaster.Common;
using System.Xml;
using Riskmaster.BusinessHelpers;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Collections.Generic;
using System.ServiceModel;
using Riskmaster.BusinessAdaptor.Common;
using System.Data;
using System.Text;
using System.Configuration;
using Telerik.Web.UI; //Added by Pradyumna MITS#35296 03032014

namespace Riskmaster.UI.UI.PolicyInterface
{
    public partial class PS_Policy : System.Web.UI.Page
    {
        // Pradyumna MITS#35296 03032014 - Start
        private const string GRID_DATA = "dtGridData";
        private DataTable GridDataSource
        {
            get
            {
                return (DataTable)ViewState[GRID_DATA];
            }
            set
            {
                ViewState[GRID_DATA] = value;
            }
        }
        // Pradyumna MITS#35296 03032014 - End

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    //tanwar2 - Policy Download from staging - start
                    if (ConfigurationManager.AppSettings["RemoveEmptyFieldsOnPolicyEnquiry"] != null)
                    {
                        hdnRemoveEmtpyFields.Value = ConfigurationManager.AppSettings["RemoveEmptyFieldsOnPolicyEnquiry"];
                    }
                    //tanwar2 - Policy Download from staging - end
                    LoadDetails();
                }
                else // Pradyumna 03052014 MITS#35296 - Start
                {
                    hdnClickUnitInfo.Value = "True";
                } // Pradyumna 03052014 MITS#35296 - End
            }
            catch (FaultException<RMException> ee)
            {
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;
                dvParent.Visible = false;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                dvParent.Visible = false;
            }
        }

        /// <summary>
        /// Populates the various tabs with information from XMLs
        /// </summary>
        private void LoadDetails()
        {
            PolicyInterfaceBusinessHelper objHelper = null;
            PolicyEnquiry oEnquiryRequest = null;
            PolicyEnquiry oEnquiryResponse = null;
            string sIssueCd = string.Empty;
            bool bInSuccess = false;
            int iTemp = 0;
            try
            {
                oEnquiryRequest = new PolicyEnquiry();

                oEnquiryRequest.PolicyIdentfier = AppHelper.GetQueryStringValue("policyNumber");
                oEnquiryRequest.PolicyNumber = AppHelper.GetQueryStringValue("policyNumber");
                oEnquiryRequest.PolicySymbol = AppHelper.GetQueryStringValue("symbol");
                oEnquiryRequest.LOB = AppHelper.GetQueryStringValue("lob");
                oEnquiryRequest.MasterCompany = AppHelper.GetQueryStringValue("mastercompany");
                hdMasterCompny.Value = oEnquiryRequest.MasterCompany;
                oEnquiryRequest.Module = AppHelper.GetQueryStringValue("module");
                oEnquiryRequest.Location = AppHelper.GetQueryStringValue("location");
                hdLocatnCompny.Value = oEnquiryRequest.Location;
                oEnquiryRequest.PolLossDt = AppHelper.GetQueryStringValue("losdt");
                oEnquiryRequest.ClaimDateReported = AppHelper.GetQueryStringValue("ClaimReportedDate");//Ashish Ahuja : Claims Made Jira 1342
                hdLossDt.Value = AppHelper.GetQueryStringValue("losdt");
                iTemp = Conversion.CastToType<int>(AppHelper.GetQueryStringValue("policysystemid"), out bInSuccess);
                if (bInSuccess)
                {
                    oEnquiryRequest.PolicySystemId = iTemp;
                }

                hdPolicySystemID.Value = oEnquiryRequest.PolicySystemId.ToString();
                //tanwar2 - Changes for Policy download from staging area - start
                //oEnquiryRequest.StagingPolicyId = Conversion.CastToType<int>(AppHelper.GetQueryStringValue("policyStagingId"), out bInSuccess);
                //hdnStagingPolicyId.Value = AppHelper.GetQueryStringValue("policyStagingId");
                hdnClaimReportedDate.Value = oEnquiryRequest.ClaimDateReported;//Ashish Ahuja : Claims Made Jira 1342
                //tanwar2 - Changes for staging - end 
                //TODO: Comment later
                //if (CheckIfDriver(oEnquiryRequest.LOB))
                //{
                //    oEnquiryRequest.IsDriverRequest = true;
                //}
                //else
                //{
                //    oEnquiryRequest.IsDriverRequest = false;
                //}
                //end
                objHelper = new PolicyInterfaceBusinessHelper();
                oEnquiryResponse = objHelper.GetPolicyEnquiryResult(oEnquiryRequest);
              
                if (!HasErrorOccured(oEnquiryResponse.PolicyDataAcordXML))
                {
                    PopulatePolicyDetails(oEnquiryResponse.PolicyDataAcordXML);
                    sIssueCd = GetIssueCode(oEnquiryResponse.PolicyDataAcordXML);
                }
            //    PopulateUnitDetails(oEnquiryResponse.UnitListAcordXML, sIssueCd, oEnquiryRequest.LOB);
                //skhare7 Policy Interface
                if (!string.IsNullOrEmpty(oEnquiryResponse.BaseLOBLine))
                {
                   // hdPolBaseLOB.Value = oEnquiryResponse.BaseLOBLine;
                    PopulateUnitDetails(oEnquiryResponse.UnitListAcordXML, sIssueCd, oEnquiryResponse.BaseLOBLine);
                }
                    PopulateAdditionalPolicyDetails(oEnquiryResponse.PolicyInterestListAcordXML);
                
                PopulateDriverDetails(oEnquiryResponse.DriverListAcordXML);
                }
            
            catch (FaultException<RMException> ee)
            {
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;
                dvParent.Visible = false;
            }
            catch (Exception e)
            {
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(e, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                dvParent.Visible = false;
            }
            finally
            {
                oEnquiryRequest = null;
                oEnquiryResponse = null;
            }
            
        }

        private bool HasErrorOccured(string pResponseXML)
        {
            bool bResult = false;
            IEnumerable<XElement> oElements;
            XElement objDoc, oErrorEle;
            string sErrorText = string.Empty;
            
            objDoc = XElement.Parse(pResponseXML);
            oElements = objDoc.XPathSelectElements("./ClaimsSvcRs/com.csc_ErrorList/com.csc_Error");
            if (oElements != null)
            {
                foreach (XElement oElement in oElements)
                {
                    oErrorEle = oElement.XPathSelectElement("./ERROR");
                    sErrorText = string.Concat(sErrorText, oErrorEle.Value);
                    bResult = true;
                }
            }
            if (bResult)
            {
                ErrorControl1.DisplayError(sErrorText);
                ErrorControl1.Visible = true;
                dvParent.Visible = false;
            }
            
            return bResult;

        }
        /// <summary>
        /// populates the policy details on the corresponding tab
        /// </summary>
        /// <param name="pResponseXML">represetns the response XMl which has the data.
        /// this XML is being traversed to extract the data and bind to UI controls</param>
        private void PopulatePolicyDetails(string pResponseXML)
        {
            XmlNode objNode = null, oNode, oSubNode;
            XmlDocument objResponse = null;
            XmlElement oTempElements = null;
            XmlNodeList objList;
            objResponse = new XmlDocument();
            objResponse.LoadXml(pResponseXML);
            

            //status
            objNode = objResponse.SelectSingleNode("ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/PolicySummaryInfo/PolicyStatusCd");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                policyStatus.Text = objNode.InnerText;
            }

            #region Cancelled Dt
            //check and see if date and status is cancelled or not
            objNode = objResponse.SelectSingleNode("ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ActivityDescCd");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                policyCancelledStatus.Text = objNode.InnerText;
            }
            objNode = objResponse.SelectSingleNode("ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_CancellationDt");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
               policyCancelledDt.Text = objNode.InnerText;
            }

            if (!string.IsNullOrEmpty(policyCancelledDt.Text) && !string.IsNullOrEmpty(policyCancelledStatus.Text))
            {
                policyCancelledStatus.Visible = true;
                policyCancelledDt.Visible = true;
            }
            #endregion

            //Policy Number, 3 labels( symbol +policy number +module)
            objList = objResponse.SelectNodes("ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier");
            foreach (XmlNode xe in objList)
            {
                oNode = xe.SelectSingleNode("./OtherIdTypeCd");
                oSubNode = xe.SelectSingleNode("./OtherId");
                if (oNode != null && oNode.InnerText == "com.csc_Module")
                {
                    policyModule.Text = oSubNode.InnerText;
                }
                if (oNode != null && oNode.InnerText == "com.csc_PolCompany")
                {
                    hdPolCmpny.Value = oSubNode.InnerText;
                }
                if (oNode != null && oNode.InnerText == "com.csc_LocCompany")
                {
                    lblLocCmpnyValue.Text = oSubNode.InnerText;
                }
                if (oNode != null && oNode.InnerText == "com.csc_MasterCompany")
                {
                    lblMasterCmpnyValue.Text = oSubNode.InnerText;
                }
            }
            objNode = objResponse.SelectSingleNode("ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/PolicySummaryInfo/PolicyNumber");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                policyNumber.Text = objNode.InnerText;
            }
            objNode = objResponse.SelectSingleNode("ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/CompanyProductCd");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                policySymbol.Text = objNode.InnerText;
            }


            //effective Date
            objNode = objResponse.SelectSingleNode("ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ContractTerm/EffectiveDt");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                policyEffDate.Text = objNode.InnerText;
            }

            //expiry Date
            objNode = objResponse.SelectSingleNode("ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ContractTerm/ExpirationDt");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                policyExpDate.Text = objNode.InnerText;
            }

            //agency number/name
            objNode = objResponse.SelectSingleNode("ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/ContractNumber");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                policyAgencyNumber.Text = objNode.InnerText;
            }
            objNode = objResponse.SelectSingleNode("ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_AgentName");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                policyAgencyName.Text = objNode.InnerText;
            }
           
            //policy state  : mapped with state value of address ; setting teh actual value in hidden field
            objNode = objResponse.SelectSingleNode("ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/Addr/StateProvCd");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                policyState.Text = objNode.InnerText; 
            }
            objNode = objResponse.SelectSingleNode("ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/StateProvCd");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                hdPolState.Value = objNode.InnerText;
            }
            
            //line of business
            objNode = objResponse.SelectSingleNode("ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/PolicySummaryInfo/LOBCd");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                policyLOB.Text = objNode.InnerText; 
            }

            //issue description
            objNode = objResponse.SelectSingleNode("ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_IssueCodeDesc");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                policyIssueDesc.Text = objNode.InnerText;
            }

            //Insured name
            string sTemp = string.Empty;
            objNode = objResponse.SelectSingleNode("ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/CommlName/CommercialName");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                policyInsured.Text = objNode.InnerText;
            }

            //sort name
            objNode = objResponse.SelectSingleNode("ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/PersonName/NickName");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                policySortname.Text = objNode.InnerText;
            }

            //addrr
            sTemp = string.Empty;
            objNode = objResponse.SelectSingleNode("ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/Addr/Addr1");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                sTemp = string.Concat(objNode.InnerText , " ") ;
            }
            objNode = objResponse.SelectSingleNode("ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/Addr/Addr2");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                sTemp = string.Concat(sTemp, objNode.InnerText);
            }
            objNode = objResponse.SelectSingleNode("ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/Addr/Addr3");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                sTemp = string.Concat(sTemp, objNode.InnerText);
            }
            objNode = objResponse.SelectSingleNode("ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/Addr/Addr4");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                sTemp = string.Concat(sTemp, objNode.InnerText);
            }
            policyAddr.Text = sTemp;

            //city
            objNode = objResponse.SelectSingleNode("ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/Addr/City");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                policyAddrCity.Text = objNode.InnerText;
            }

            //state
            objNode = objResponse.SelectSingleNode("ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/Addr/StateProvCd");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                policyAddrState.Text = objNode.InnerText; 
            } 

            //postal code
            objNode = objResponse.SelectSingleNode("ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/Addr/PostalCode");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                policyAddrZipCode.Text = objNode.InnerText;
                //tkatsarski: 11/17/14 - RMA-2426: The field needs to add the dash if the Postal Code is 9 digits long
                if (policyAddrZipCode.Text != null)
                {
                    policyAddrZipCode.Text = policyAddrZipCode.Text.Trim();
                    if ((policyAddrZipCode.Text.Length == 9) && (policyAddrZipCode.Text.IndexOf('-') == -1))
                    {
                        policyAddrZipCode.Text = policyAddrZipCode.Text.Substring(0, 5) + '-' + policyAddrZipCode.Text.Substring(5, 4);
                    }
                }
            }

            //premium
            objNode = objResponse.SelectSingleNode("ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_AgentPremium");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                policyPremium.Text = objNode.InnerText; 
            }

            //profit centre
            objNode = objResponse.SelectSingleNode("ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ProfitCenter");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                policyProfitCenter.Text = objNode.InnerText; 
            }
            
            //branch
            objNode = objResponse.SelectSingleNode("ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_BranchCd");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                policyBranch.Text = objNode.InnerText; 
            }
            
            //customer number
            objNode = objResponse.SelectSingleNode("ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/ItemIdInfo/CustPermId");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                policyCustomerNumber.Text = objNode.InnerText;
            }

            //optional name/address
            objNode = objResponse.SelectSingleNode("ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/PersonName/OtherGivenName");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                policyOptionalNameAddr.Text = objNode.InnerText;
            }


        }
        /// <summary>
        /// this method is used to find out the value of issue code node
        /// from the response XML and return the same
        /// </summary>
        /// <param name="sResponseXML">reprsents the XML which is being traversed to find out Issue code node</param>
        /// <returns>Issue code value</returns>
        private string GetIssueCode(string sResponseXML)
        {
            XmlDocument objResponse = new XmlDocument();
            XmlNode objNode;
            string sIssueCd = string.Empty;
            objResponse.LoadXml(sResponseXML);
            objNode = objResponse.SelectSingleNode("ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_IssueCd");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                sIssueCd =objNode.InnerText;
                hdIssueCd.Value = sIssueCd;
            }
            return sIssueCd;
        }
        /// <summary>
        /// Populates Unit details tab with vehicle/property/site/manual units.
        /// </summary>
        /// <param name="pResponseXML">reprsents the XML from having data to be shown on UI</param>
        /// <param name="pIssueCd">tells Whether policy is mannual rated or computer rated</param>
        /// <param name="pLOB">tells the Line of business value</param>
        private void PopulateUnitDetails(string pResponseXML,string pIssueCd, string pLOB)
        {
            XmlNode objIssueCdNode;
            XmlDocument objUnitListDetail = null;

            try
            {
                objUnitListDetail = new XmlDocument();
                objUnitListDetail.LoadXml(pResponseXML);
            }
            catch(Exception e)
            {
                ErrorControl1.DisplayError(e.Message);
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(e);
                dvParent.Visible = false;
                return;
            }
           
            pIssueCd = pIssueCd.ToUpper();


            DataTable dtData = new DataTable();
            dtData.Columns.Add("UnitType", typeof(string));
            dtData.Columns.Add("UnitNum", typeof(string));
            dtData.Columns.Add("Status", typeof(string));
            dtData.Columns.Add("Prem", typeof(string));
            dtData.Columns.Add("Desc", typeof(string));
            dtData.Columns.Add("VIN", typeof(string));
            dtData.Columns.Add("MakeYear", typeof(string));
            dtData.Columns.Add("Addr", typeof(string));
            dtData.Columns.Add("val", typeof(string));
            dtData.Columns.Add("InsLine", typeof(string));
            //check if this policy is a mannual rated or not.
            if (string.Compare(pIssueCd, "M") == 0)
            {
                //PopulateMannualRatedSelfInfo(ref multiunitid, objUnitListDetail);
                PopulateMannualRatedSelfInfo(ref dtData, objUnitListDetail);
            }
            else 
            {
                switch (pLOB.Trim())
                {
                 //   case "MHO"://skhare7 Policy interface
                 //   case "HP":
                    case "PL":
                            //PopulatePropertyInfo(ref multiunitid, objUnitListDetail);
                        PopulateMannualRatedSelfInfo(ref dtData, objUnitListDetail);
                        PopulatePropertyInfo(ref dtData, objUnitListDetail, false);
                        break;
                  //  case "ACV":
                    case "AL":
                            //PopulateVehicleInfo(ref multiunitid, objUnitListDetail);
                        PopulateMannualRatedSelfInfo(ref dtData, objUnitListDetail);
                        PopulateVehicleInfo(ref dtData, objUnitListDetail, false);
                        break;
                   // case "WC":
                    //case "WCA":
                    case "WL":
                           // PopulateWorkerInfo(ref multiunitid, objUnitListDetail);
                        PopulateMannualRatedSelfInfo(ref dtData, objUnitListDetail);
                        PopulateWorkerInfo(ref dtData, objUnitListDetail);
                        break;
                 //   case "BOP":
                    case "CL":
                            //PopulateVehicleInfo(ref multiunitid, objUnitListDetail);
                            //PopulatePropertyInfo(ref multiunitid, objUnitListDetail);
                        PopulateMannualRatedSelfInfo(ref dtData, objUnitListDetail);
                        PopulateVehicleInfo(ref dtData, objUnitListDetail,true);
                        PopulatePropertyInfo(ref dtData, objUnitListDetail, true);
                        
                        break;
                    default: ErrorHelper.Write("The LOB value does not matched. Value= "+pLOB);
                        break;  
                }
            }

            // Modified - Pradyumna MITS#35296 03032014 - Start
            GridDataSource = dtData;
            //gvData.DataSource = dtData;
            //gvData.DataBind();
            //ViewState["Data"] = dtData;
            // Modified - Pradyumna MITS#35296 03032014 - End

            if (dtData.Rows.Count == 0)   //if (multiunitid.Items.Count == 0)
            {
                btnUnitDetails.Enabled = false;
                btnUnitCov.Enabled = false;
                btnFormDataUnit.Enabled = false;
                btnUnitInterest.Enabled = false;
            }
        }


        /// <summary>
        /// Populates List box with property information type nodes
        /// </summary>
        /// <param name="oControl">gives the reference to listbox to be populated</param>
        /// <param name="oXmlDoc">gives the xml document</param>
        /// protected void PopulatePropertyInfo(ref ListBox oControl, XmlDocument oXmlDoc)
        protected void PopulatePropertyInfo(ref DataTable dt, XmlDocument oXmlDoc, bool bIncludeProduct)
        {
            XmlNode objTypeCd, objValue, objDescData,objYr, objUnitStatus, objPrem, objAddress, objCity, objState, objZipcode;
            string sVal, sLoc, sSubLoc, sUnitNo, sInsLn, sRateState, sToolTip, sProduct, sPrem, sStatUnit, sAddress=string.Empty;
            ListItem liData;
            DataRow drRow;
            foreach (XmlNode objNode in oXmlDoc.SelectNodes("ACORD/ClaimsSvcRs/com.csc_PolicyUnitListRs/PropertyLossInfo"))
            {
                sVal = string.Empty;
                sStatUnit = string.Empty;
                sLoc = string.Empty;
                sSubLoc = string.Empty;
                sInsLn = string.Empty;
                sUnitNo = string.Empty;
                sRateState = string.Empty;
                sToolTip = string.Empty;
                sProduct = string.Empty;
                sPrem = string.Empty;
                drRow = dt.NewRow();
                foreach (XmlNode oNode in objNode.SelectNodes("ItemIdInfo/OtherIdentifier"))
                {
                    objTypeCd = oNode.SelectSingleNode("OtherIdTypeCd");
                    objValue = oNode.SelectSingleNode("OtherId");

                    if (string.Compare(objTypeCd.InnerText, "com.csc_RiskLoc", true) == 0)
                    {
                        sLoc = objValue.InnerText;
                    }
                    if (string.Compare(objTypeCd.InnerText, "com.csc_RiskSubLoc", true) == 0)
                    {
                        sSubLoc = objValue.InnerText;
                    }
                    if (string.Compare(objTypeCd.InnerText, "com.csc_UnitNumber", true) == 0)
                    {
                        sUnitNo = objValue.InnerText;
                    }
                    if (string.Compare(objTypeCd.InnerText, "com.csc_InsLineCd", true) == 0)
                    {
                        sInsLn = objValue.InnerText;
                    }
                    if (string.Compare(objTypeCd.InnerText, "com.csc_RateState", true) == 0)
                    {
                        sRateState = objValue.InnerText;
                    }
                    if (string.Compare(objTypeCd.InnerText, "com.csc_Product", true) == 0)
                    {
                        sProduct = objValue.InnerText;
                    }
                    if (string.Compare(objTypeCd.InnerText, "com.csc_StatUnitNumber", true) == 0)
                    {
                        sStatUnit = objValue.InnerText;
                    }
                }
                objUnitStatus = objNode.SelectSingleNode("com.csc_UnitStatus");
                objYr = objNode.SelectSingleNode("com.csc_YearBuilt");
                sToolTip = "Rate State: " + sRateState + " Year Built: " + objYr.InnerText;
                sVal = sUnitNo + "|" + sInsLn + "|" + sLoc + "|" + sSubLoc + "|" + sRateState + "|" + sProduct + "|" + objUnitStatus.InnerText +
                    "|" + policyNumber.Text + "|" + policySymbol.Text + "|" + hdPolState.Value  //policyState.Text
                    + "|" + policyLOB.Text + "|" + hdIssueCd.Value + "|" + hdLocatnCompny.Value
                    + "|" + hdMasterCompny.Value + "|" + policyModule.Text + "|" + policyEffDate.Text + "|" + hdPolCmpny.Value + "|" + hdPolicySystemID.Value //+ "|" + hdPolBaseLOB.Value;//skhare7 Policy interface
                    + "|" + sStatUnit + "|P|"+hdLossDt.Value.Trim();

                objDescData = objNode.SelectSingleNode("com.csc_UnitLossData/com.csc_UnitDescription/com.csc_UnitDesc");
                objAddress = objNode.SelectSingleNode("com.csc_UnitLossData/com.csc_UnitDescription/com.csc_UnitAddress");
                objCity = objNode.SelectSingleNode("com.csc_UnitLossData/com.csc_UnitDescription/com.csc_UnitCity");
                objState = objNode.SelectSingleNode("com.csc_UnitLossData/com.csc_UnitDescription/com.csc_UnitState");
                objZipcode = objNode.SelectSingleNode("com.csc_UnitLossData/com.csc_UnitDescription/com.csc_UnitZipCode");
                if(objAddress!=null)
                    sAddress = objAddress.InnerText.Trim();
                if(objCity!=null)
                    sAddress = sAddress + (string.IsNullOrEmpty(sAddress) ? objCity.InnerText.Trim() : ", " + objCity.InnerText.Trim());
                if(objCity!=null && objState!=null)
                    sAddress = sAddress + (string.IsNullOrEmpty(objCity.InnerText.Trim()) ? objState.InnerText.Trim() : ", " + objState.InnerText.Trim());
                if(objZipcode!=null && objState!=null)
                    sAddress = sAddress + (string.IsNullOrEmpty(objState.InnerText.Trim()) ? objZipcode.InnerText.Trim() : ", " + objZipcode.InnerText.Trim());

                objPrem = objNode.SelectSingleNode("PremiumInfo/PremiumAmt");
                if (objPrem != null)
                    sPrem = objPrem.InnerText;
                drRow[0] = "Property";
                drRow[1] = sStatUnit;
                drRow[2] = objUnitStatus.InnerText;
                drRow[3] = sPrem;
                if (bIncludeProduct)
                {
                    drRow[4] = sProduct + " : " + objDescData.InnerText; //desc
                }
                else
                {
                    drRow[4] = objDescData.InnerText; //desc
                }
                    
                drRow[5] = string.Empty; //VIN
                drRow[6] = objYr.InnerText;
                drRow[7] = sAddress;
                drRow[8] = sVal;
                drRow[9] = sInsLn;

                //if (!string.IsNullOrEmpty(objDescData.InnerText))
                //{
                //    liData = new ListItem("Property Unit: " + objDescData.InnerText, sVal);
                //    oControl.Items.Add(liData);
                //    liData.Attributes.Add("title", sToolTip);
                //}
                dt.Rows.Add(drRow);
            }
        }
        /// <summary>
        /// Populates List box with vehicle detail type nodes
        /// </summary>
        /// <param name="oControl">gives the reference to listbox to be populated</param>
        /// <param name="oXmlDoc">gives the xml document</param>
        /// protected void PopulateVehicleInfo(ref ListBox oControl, XmlDocument oXmlDoc)
        protected void PopulateVehicleInfo(ref DataTable dtData , XmlDocument oXmlDoc, bool bIncludeProduct)
        {
            XmlNode objTypeCd, objValue, objListData, objModel, objYr, objUnitStatus, objPrem;
            string sVal, sLoc, sSubLoc, sUnitNo, sInsLn, sRateState, sProduct, sPrem, sStatUnit;
            ListItem liData;
            DataRow drRow ;//= new DataRow();
                foreach (XmlNode objNode in oXmlDoc.SelectNodes("ACORD/ClaimsSvcRs/com.csc_PolicyUnitListRs/AutoLossInfo"))
                {
                    sVal = string.Empty;
                    sLoc = string.Empty;
                    sSubLoc = string.Empty;
                    sInsLn = string.Empty;
                    sUnitNo = string.Empty;
                    sRateState = string.Empty;
                    sProduct = string.Empty;
                    sPrem = string.Empty;
                    sStatUnit = string.Empty;
                    drRow = dtData.NewRow();
                        foreach (XmlNode oNode in objNode.SelectNodes("VehInfo/ItemIdInfo/OtherIdentifier"))
                        {
                            objTypeCd = oNode.SelectSingleNode("OtherIdTypeCd");
                            objValue = oNode.SelectSingleNode("OtherId");

                            if (string.Compare(objTypeCd.InnerText, "com.csc_RiskLoc", true) == 0)
                            {
                                sLoc = objValue.InnerText;
                            }
                            if (string.Compare(objTypeCd.InnerText, "com.csc_RiskSubLoc", true) == 0)
                            {
                                sSubLoc = objValue.InnerText;
                            }
                            if (string.Compare(objTypeCd.InnerText, "com.csc_UnitNumber", true) == 0)
                            {
                                sUnitNo = objValue.InnerText;
                            }
                            if (string.Compare(objTypeCd.InnerText, "com.csc_InsLineCd", true) == 0)
                            {
                                sInsLn = objValue.InnerText;
                            }
                            if (string.Compare(objTypeCd.InnerText, "com.csc_RateState", true) == 0)
                            {
                                sRateState = objValue.InnerText;
                            }
                            if (string.Compare(objTypeCd.InnerText, "com.csc_Product", true) == 0)
                            {
                                sProduct = objValue.InnerText;
                            }
                            if (string.Compare(objTypeCd.InnerText, "com.csc_StatUnitNumber", true) == 0)
                            {
                                sStatUnit = objValue.InnerText;
                            }
                        }

                        objUnitStatus = objNode.SelectSingleNode("com.csc_UnitLossData/com.csc_UnitStatus");
                        sVal = sUnitNo + "|" + sInsLn + "|" + sLoc + "|" + sSubLoc + "|" + sRateState + "|" + sProduct + "|" + objUnitStatus.InnerText +
                           "|" + policyNumber.Text + "|" + policySymbol.Text + "|" + hdPolState.Value +   // policyState.Text + 
                           "|" + policyLOB.Text + "|" + hdIssueCd.Value + "|" + hdLocatnCompny.Value
                           + "|" + hdMasterCompny.Value + "|" + policyModule.Text + "|" + policyEffDate.Text + "|" + hdPolCmpny.Value + "|" + hdPolicySystemID.Value// +"|" + hdPolBaseLOB.Value;//skhare7 Policy interface
                           + "|" + sStatUnit + "|V|"+hdLossDt.Value.Trim();
                        objListData = objNode.SelectSingleNode("com.csc_VehicleIdentificationNumber");
                        objYr = objNode.SelectSingleNode("VehInfo/ModelYear");
                        objModel = objNode.SelectSingleNode("VehInfo/Manufacturer");
                        objPrem = objNode.SelectSingleNode("PremiumInfo/PremiumAmt");
                        if (objPrem != null)
                            sPrem = objPrem.InnerText;


                        drRow[0] = "Vehicle";
                        drRow[1] = sStatUnit;
                        drRow[2] = objUnitStatus.InnerText;
                        drRow[3] = sPrem;//prem
                        if (bIncludeProduct)
                        {
                            drRow[4] =sProduct+" : "+ objModel.InnerText; //desc
                        }
                        else
                        {
                            drRow[4] = objModel.InnerText; //desc
                        }

                        drRow[5] = objListData.InnerText;
                        drRow[6] = objYr.InnerText;
                        drRow[7] = string.Empty; //addr
                        drRow[8] = sVal;
                        drRow[9] = sInsLn;
                        //if (!string.IsNullOrEmpty(objListData.InnerText))
                        //{
                        //    liData = new ListItem("Vehicle Unit: " + objListData.InnerText, sVal);
                        //    oControl.Items.Add(liData);
                        //    liData.Attributes.Add("title", "Model Year:"+objYr.InnerText+" Model:"+objModel.InnerText);
                        //}
                        dtData.Rows.Add(drRow);
                    }
            
            
        }
        /// <summary>
        /// Populates List box with worker detail type nodes
        /// </summary>
        /// <param name="oControl">gives the reference to listbox to be populated</param>
        /// <param name="oXmlDoc">gives the xml document</param>
       /// protected void PopulateWorkerInfo(ref ListBox oControl, XmlDocument oXmlDoc)
        protected void PopulateWorkerInfo(ref DataTable dtData , XmlDocument oXmlDoc)
        {
            XmlNode objTypeCd, objValue, objListData, objAddr2, objAddr1,objAddr3, objAddr4, objCity, objState, objPostalCode; 
            string sVal, sLoc, sSubLoc, sUnitNo, sInsLn, sToolTip, sAddr1, sAddr2, sCity, sRateState, sProduct, sStatUnit;
            ListItem liData;
            DataRow drRow;
                foreach (XmlNode objNode in oXmlDoc.SelectNodes("ACORD/ClaimsSvcRs/com.csc_PolicyUnitListRs/WorkCompLocInfo"))   
                {
                    sVal = string.Empty;
                    sLoc = string.Empty;
                    sSubLoc = string.Empty;
                    sInsLn = string.Empty;
                    sUnitNo = string.Empty;
                    sRateState = string.Empty;
                    sProduct = string.Empty;
                    sStatUnit = string.Empty;
                    drRow = dtData.NewRow();
                    foreach (XmlNode oNode in objNode.SelectNodes("Location/ItemIdInfo/OtherIdentifier"))
                    {
                        objTypeCd = oNode.SelectSingleNode("OtherIdTypeCd");
                        objValue = oNode.SelectSingleNode("OtherId");

                        if (string.Compare(objTypeCd.InnerText, "com.csc_RiskLoc", true) == 0)
                        {
                            sLoc = objValue.InnerText;
                        }
                        if (string.Compare(objTypeCd.InnerText, "com.csc_RiskSubLoc", true) == 0)
                        {
                            sSubLoc = objValue.InnerText;
                        }
                        if (string.Compare(objTypeCd.InnerText, "com.csc_UnitNumber", true) == 0)
                        {
                            sUnitNo = objValue.InnerText;
                        }
                        if (string.Compare(objTypeCd.InnerText, "com.csc_InsLineCd", true) == 0)
                        {
                            sInsLn = objValue.InnerText;
                        }
                        //if (string.Compare(objTypeCd.InnerText, "com.csc_RateState", true) == 0)
                        //{
                        //    sRateState = objValue.InnerText;
                        //}
                        if (string.Compare(objTypeCd.InnerText, "com.csc_Product", true) == 0)
                        {
                            sProduct = objValue.InnerText;
                        }
                        if (string.Compare(objTypeCd.InnerText, "com.csc_StatUnitNumber", true) == 0)
                        {
                            sStatUnit = objValue.InnerText;
                        }
                    }
                    objCity = objNode.SelectSingleNode("Location/Addr/City");
                    objAddr1 = objNode.SelectSingleNode("Location/Addr/Addr1");
                    objAddr2 = objNode.SelectSingleNode("Location/Addr/Addr2");
                    objAddr3 = objNode.SelectSingleNode("Location/Addr/Addr3");
                    objAddr4 = objNode.SelectSingleNode("Location/Addr/Addr4");
                    objPostalCode = objNode.SelectSingleNode("Location/Addr/PostalCode");

                    objState = objNode.SelectSingleNode("Location/Addr/StateProvCd");
                    sRateState = objState.InnerText;

                    sToolTip = "Addr: " + objAddr1.InnerText + " " + objAddr2.InnerText + (objAddr3 == null ? "" : " " + objAddr3.InnerText) + (objAddr4 == null ? "" : " " + objAddr4.InnerText) + " City: " + objCity.InnerText + " State: " + sRateState + " Postal Code: " + objPostalCode.InnerText;

                    
                   
                   // objUnitStatus = objNode.SelectSingleNode("com.csc_UnitStatus");

                    sVal = sUnitNo + "|" + sInsLn + "|" + sLoc + "|" + sSubLoc + "|" + sRateState + "|" + sProduct + "|" + "" +// objUnitStatus.InnerText +
                          "|" + policyNumber.Text + "|" + policySymbol.Text + "|" + hdPolState.Value +// policyState.Text +
                          "|" + policyLOB.Text + "|" + hdIssueCd.Value + "|" + hdLocatnCompny.Value
                          + "|" + hdMasterCompny.Value + "|" + policyModule.Text + "|" + policyEffDate.Text + "|" + hdPolCmpny.Value + "|" + hdPolicySystemID.Value// +"|" + hdPolBaseLOB.Value;//skhare7 Policy interface
                          + "|" + sStatUnit + "|S|"+hdLossDt.Value.Trim();
                    objListData = objNode.SelectSingleNode("Location/LocationName");

                    drRow[0] = "Site";
                    drRow[1] = sStatUnit;
                    drRow[2] = string.Empty; //status
                    drRow[3] = string.Empty; //prem
                    drRow[4] = objListData.InnerText;
                    drRow[5] = string.Empty; //VIN
                    drRow[6] = string.Empty; //make year
                    drRow[7] = sToolTip;
                    drRow[8] = sVal;
                    drRow[9] = sInsLn;
                    //if (!string.IsNullOrEmpty(objListData.InnerText))
                    //{
                    //    liData = new ListItem("Site Unit: " + objListData.InnerText, sVal);
                    //    oControl.Items.Add(liData);
                    //    liData.Attributes.Add("title", sToolTip);

                    //}
                    dtData.Rows.Add(drRow);
                }
           
        }
        /// <summary>
        /// Populates List box with self unit type nodes
        /// </summary>
        /// <param name="oControl">gives the reference to listbox to be populated</param>
        /// <param name="oXmlDoc">gives the xml document</param>
        /// protected void PopulateMannualRatedSelfInfo(ref ListBox oControl, XmlDocument oXmlDoc)
        protected void PopulateMannualRatedSelfInfo(ref DataTable dt,  XmlDocument oXmlDoc)
        {
            XmlNode objTypeCd, objValue, objListData, objUnitStatus, objPrem;
            string sVal, sLoc, sSubLoc, sUnitNo, sInsLn, sProduct, sRateState, sStatus, sPrem, sDesc, sStatUnit, sDescUse1;
            ListItem liData;

            
            DataRow drRow;

            foreach (XmlNode objNode in oXmlDoc.SelectNodes("ACORD/ClaimsSvcRs/com.csc_PolicyUnitListRs/InsuredOrPrincipal"))    
            {
                sVal = string.Empty;
                sStatUnit=string.Empty;
                sLoc = string.Empty;
                sSubLoc = string.Empty;
                sInsLn = string.Empty;
                sUnitNo = string.Empty;
                sRateState = string.Empty;
                sProduct = string.Empty;
                sStatus = string.Empty;
                sPrem = string.Empty;
                sDesc = string.Empty;
                drRow = dt.NewRow();
                sDescUse1 = string.Empty;

                foreach (XmlNode oNode in objNode.SelectNodes("ItemIdInfo/OtherIdentifier"))
                {
                    objTypeCd = oNode.SelectSingleNode("OtherIdTypeCd");
                    objValue = oNode.SelectSingleNode("OtherId");

                    if (string.Compare(objTypeCd.InnerText, "com.csc_RiskLoc", true) == 0)
                    {
                        sLoc = objValue.InnerText;
                    }
                    if (string.Compare(objTypeCd.InnerText, "com.csc_RiskSubLoc", true) == 0)
                    {
                        sSubLoc = objValue.InnerText;
                    }
                    if (string.Compare(objTypeCd.InnerText, "com.csc_UnitNumber", true) == 0)
                    {
                        sUnitNo = objValue.InnerText;
                    }
                    if (string.Compare(objTypeCd.InnerText, "com.csc_InsLineCd", true) == 0)
                    {
                        sInsLn = objValue.InnerText;
                    }
                    if (string.Compare(objTypeCd.InnerText, "com.csc_RateState", true) == 0)
                    {
                        sRateState = objValue.InnerText;
                    }
                    if (string.Compare(objTypeCd.InnerText, "com.csc_Product", true) == 0)
                    {
                        sProduct = objValue.InnerText;
                    }
                    if (string.Compare(objTypeCd.InnerText, "com.csc_StatUnitNumber", true) == 0)
                    {
                        sStatUnit = objValue.InnerText;
                    }
                }
                
                //objUnitStatus = objNode.SelectSingleNode("com.csc_UnitStatus");
                //sStatus = objUnitStatus.InnerText;
                objPrem = objNode.SelectSingleNode("PremiumInfo/PremiumAmt");
                sPrem = objPrem.InnerText;
                sVal = sUnitNo + "|" + sInsLn + "|" + sLoc + "|" + sSubLoc + "|" + sRateState + "|" + sProduct + "|" + "" + // objUnitStatus.InnerText +
                         "|" + policyNumber.Text + "|" + policySymbol.Text + "|" + hdPolState.Value+//policyState.Text +
                         "|" + policyLOB.Text + "|" + hdIssueCd.Value + "|" + hdLocatnCompny.Value
                         + "|" + hdMasterCompny.Value + "|" + policyModule.Text + "|" + policyEffDate.Text + "|" + hdPolCmpny.Value + "|" + hdPolicySystemID.Value
                         + "|" + sStatUnit + "|SU|" +hdLossDt.Value.Trim();
                objListData = objNode.SelectSingleNode("GeneralPartyInfo/NameInfo/CommlName/CommercialName");
                sDesc = objListData.InnerText;
                //dbisht6 start merging mits 35655
                sDescUse1 = objNode.SelectSingleNode("com.csc_InsuredNameIdInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='DESCUSE1']/OtherId").InnerText;
                //dbisht6 end
                
                drRow[0] = "Stat";
                drRow[1] = sStatUnit;
                drRow[2] = sStatus;
                drRow[3] = sPrem;
                drRow[4] = sDesc;
               //dbisht6 start merging for mits 35655
                drRow[5] = sDescUse1; //VIN
                //dbisht6 end
                drRow[6] = string.Empty; //MakeYear
                drRow[7] = string.Empty; //addr, chk from response
                drRow[8] = sVal;
                drRow[9] = sInsLn;
                //if (!string.IsNullOrEmpty(objListData.InnerText))
                //{
                //    liData = new ListItem("Stat Unit: " + objListData.InnerText, sVal);
                //    multiunitid.Items.Add(liData);
                //    liData.Attributes.Add("title", objListData.InnerText);
                //}
                dt.Rows.Add(drRow);
            }
           
        }
        /// <summary>
        /// populates the Additional policy tab 
        /// </summary>
        /// <param name="pResponseXML">represents the response XML which has 
        /// the data to be populated on the UI</param>
        private void PopulateAdditionalPolicyDetails(string pResponseXML)
        {
            IEnumerable<XElement> oElements = null;
            XElement oDocElement, oSubEle1, oSubEle2, oEleAddr1,oEleCity, oTemp;
            ListItem liData;
            string sToolTip, sVal, sLocationNum, sSeqNum;
            
            oDocElement = XElement.Parse(pResponseXML);
            
            oElements = oDocElement.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyInterestListRs/InsuredOrPrincipal");

            foreach (XElement oElement in oElements)
            {
                oSubEle2 = null;
                oSubEle1 = null;
                sToolTip = string.Empty; sLocationNum = string.Empty; sSeqNum = string.Empty;
                oSubEle1 = oElement.XPathSelectElement("./GeneralPartyInfo/NameInfo/CommlName/CommercialName");
                oSubEle2 = oElement.XPathSelectElement("./InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd");
                oEleAddr1 = oElement.XPathSelectElement("./GeneralPartyInfo/Addr/Addr1");
                oEleCity = oElement.XPathSelectElement("./GeneralPartyInfo/Addr/City");
                oTemp = oElement.XPathSelectElement("ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_SeqNum']/OtherId");
                sSeqNum = oTemp.Value;
                oTemp = oElement.XPathSelectElement("ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Location']/OtherId");
                sLocationNum = oTemp.Value;

                sToolTip = "Addr: " + oEleAddr1.Value + " City: " + oEleCity.Value;
                sVal = policyNumber.Text + "|" + policySymbol.Text + "|" + policyModule.Text + "|" + hdLocatnCompny.Value + "|" +
                    hdMasterCompny.Value + "|" + oSubEle2.Value + "|" + hdPolicySystemID.Value + "|" + sLocationNum + "|" + sSeqNum + "|" + policyLOB.Text;
                if (oSubEle1 != null && oSubEle2 != null && !string.IsNullOrEmpty(oSubEle1.Value) && !string.IsNullOrEmpty(oSubEle2.Value))
                {
                    liData = new ListItem(oSubEle1.Value + " : " + oSubEle2.Value, sVal);
                    lbAdditionalInfo.Items.Add(liData);
                    liData.Attributes.Add("title", sToolTip);
                }
            }
            if (lbAdditionalInfo.Items.Count == 0)
            {
                lblAdditionalPolicyMsg.Visible = true;
                btnAdditionalPolicyDetails.Enabled = false;
            }
        }
        /// <summary>
        /// Populates driver infor tab if there is any data corresponding to it 
        /// </summary>
        /// <param name="pResponseXML">represents the response XML which has 
        /// the data to be populated on the UI</param>
        private void PopulateDriverDetails(string pResponseXML)
        {
            IEnumerable<XElement> oElements = null, oItemElements;
            XElement oDocElement, oSubEle1, oSubEle2, oIdType,oIdVal,oSubEle;
            ListItem liData;
            string sDriverType, sStateCd, sToolTip, sVal, sDriverID, sDriverStatus, sDriverProduct, sLoc, sSubLoc;
            try
            {
                if (string.IsNullOrEmpty(pResponseXML))
                    return;
                oDocElement = XElement.Parse(pResponseXML);
            }
            catch (Exception e)
            {
                ErrorHelper.logErrors(e);
                return;
            }

            oElements = oDocElement.XPathSelectElements("./ClaimsSvcRs/com.csc_DriverListRs/DriverInfo"); 
            if (oElements != null )
            {
                foreach (XElement oElement in oElements)
                {
                    //oSubEle1 = null;
                    //oSubEle2 = null;
                    sToolTip = string.Empty;
                    sStateCd = string.Empty;
                    sDriverType = string.Empty;
                    sVal = string.Empty; sDriverStatus = string.Empty; sDriverID = string.Empty;
                    sDriverProduct = string.Empty; sLoc = string.Empty; sSubLoc = string.Empty;

                    oItemElements = oElement.XPathSelectElements("./ItemIdInfo/OtherIdentifier");
                    foreach (XElement oEle in oItemElements)
                    {
                        oIdType = oEle.XPathSelectElement("./OtherIdTypeCd");
                        oIdVal = oEle.XPathSelectElement("./OtherId");
                        if (string.Compare(oIdType.Value, "com.csc_DriverType", true) == 0)
                            sDriverType = oIdVal.Value;
                        if (string.Compare(oIdType.Value, "com.csc_DriverProduct", true) == 0)
                            sDriverProduct=oIdVal.Value;
                        if (string.Compare(oIdType.Value, "com.csc_DriverId", true) == 0)
                            sDriverID = oIdVal.Value;
                        if (string.Compare(oIdType.Value, "com.csc_RiskLoc", true) == 0)
                            sLoc = oIdVal.Value;
                        if (string.Compare(oIdType.Value, "com.csc_RiskSubLoc", true) == 0)
                            sSubLoc = oIdVal.Value;
                    }
                    
                    oSubEle1 = oElement.XPathSelectElement("./License/LicensePermitNumber");
                    oSubEle2 = oElement.XPathSelectElement("./PersonInfo/NameInfo/CommlName/CommercialName");
                    oSubEle = oElement.XPathSelectElement("./License/StateProvCd");
                    sStateCd = oSubEle.Value;
                    oSubEle = oElement.XPathSelectElement("com.csc_DriverStatus");
                    sDriverStatus = oSubEle.Value;

                    sToolTip = "Driver Type:" + sDriverType + "   State: " + sStateCd;

                    sVal = policyNumber.Text + "|" + policySymbol.Text + "|" + policyModule.Text + "|" + hdLocatnCompny.Value + "|" +
                    hdMasterCompny.Value + "|" + policyLOB.Text + "|" + sStateCd + "|" + sDriverType + "|" + sDriverProduct + "|" +
                    sDriverStatus +"|"+ hdPolicySystemID.Value + "|"+hdPolCmpny.Value + "|"+sDriverID +"|"+sLoc+"|"+sSubLoc;

                    if (oSubEle1 != null && oSubEle2 != null && !string.IsNullOrEmpty(oSubEle1.Value) && !string.IsNullOrEmpty(oSubEle2.Value))
                    {
                        liData = new ListItem(oSubEle2.Value + " : " + oSubEle1.Value, sVal);
                        lbDriverInfo.Items.Add(liData);
                        liData.Attributes.Add("title", sToolTip);
                    }
                }
            }
            //if (oElements == null || lbDriverInfo.Items.Count == 0)
            //{
            //    TABSdriverinfo.Attributes.Add("style", "display:none");
            //    FORMTABdriverinfo.Attributes.Add("style", "display:none");
            //}
            if (oElements != null && lbDriverInfo.Items.Count > 0)
            {
                TABSdriverinfo.Attributes.Add("style", "display:' '");
                TBSPdriverinfo.Attributes.Add("style", "display:' '");
            }
        }
        //TODO: comment later
        /// <summary>
        /// Checks if we need to set the driver request flag 
        /// or not based on LOB
        /// </summary>
        /// <param name="pLOB">The LOB value based on which driver request is checked</param>
        /// <returns>true if we need to set driver info else false</returns>
        //private bool CheckIfDriver(string pLOB)
        //{
        //    pLOB = pLOB.ToUpper();

        //    if (string.Compare(pLOB, "APV") == 0 || string.Compare(pLOB, "CPP") == 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}
        //TODO: end

        #region GridEvents
        //protected void gvData_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
        //{
        //    gvData.PageIndex = e.NewPageIndex;
        //    if (ViewState["Data"] != null)
        //    {
        //        gvData.DataSource = (DataTable)ViewState["Data"];
        //        gvData.DataBind();
        //        this.ClientScript.RegisterStartupScript(this.GetType(), "script", "tabChange('unitinfo');", true);
        //    }
        //}

        // Pradyumna MITS#35296 03032014 - Start
        protected void gvUnitList_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            if (GridDataSource != null)
            {
                try
                {
                    gvUnitList.DataSource = GridDataSource;
                }
                catch
                {
                    SetGridDisplay(false);
                }
            }
        }

        protected void gvUnitList_PreRender(object sender, EventArgs e)
        {
            GridFilterMenu menu = gvUnitList.FilterMenu;
            int i = 0;
            while (i < menu.Items.Count)
            {
                if (menu.Items[i].Text == "Between" || menu.Items[i].Text == "NotBetween" || menu.Items[i].Text == "IsNull" || menu.Items[i].Text == "NotIsNull")
                {
                    menu.Items.RemoveAt(i);
                }
                else
                {
                    i = (i + 1);
                }
            }
        }

        private void SetGridDisplay(bool Isdisplay)
        {
            if (Isdisplay)
            {
                gvUnitList.Visible = true;
                //ltNoResult.Visible = false;
                //tblToolbar.Visible = true;
                this.ClientScript.RegisterStartupScript(this.GetType(), "setScroll", "window.scrollTo(0, document.body.scrollHeight)", true);
            }
            else
            {
                gvUnitList.Visible = false;
            }
        }

        protected void gvUnitList_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                RadioButton grdRadio = null;
                GridDataItem item = e.Item as GridDataItem;
                Control radioControl = item["grdRadio"];
                if (radioControl != null)
                {
                    grdRadio = radioControl.FindControl("gdRadio") as RadioButton;
                    if (grdRadio != null)
                    {
                        grdRadio.Attributes.Add("OnClick", "selectSingleRadio(" + grdRadio.ClientID + ", " + "'gvUnitList'" + ") ");
                    }
                }
            }
        }
        // Pradyumna MITS#35296 03032014 - End
        #endregion

        private void PopulatePolicyStagingDetails(string p_xResponseXML)
        {
            XmlNode objNode = null, oNode, oSubNode;
            XmlDocument objResponse = null;
            XmlElement oTempElements = null;
            XmlNodeList objList;
            objResponse = new XmlDocument();
            objResponse.LoadXml(p_xResponseXML);


            //status
            objNode = objResponse.SelectSingleNode("PolicyStaging/PolicySummaryInfo/PolicyStatusCd");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                policyStatus.Text = objNode.InnerText;
            }

            policyCancelledStatus.Visible = true;
            policyCancelledDt.Visible = true;

            objNode = objResponse.SelectSingleNode("PolicyStaging/PolicySummaryInfo/PolicyNumber");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                policyNumber.Text = objNode.InnerText;
            }

            //effective Date
            objNode = objResponse.SelectSingleNode("PolicyStaging/PolicySummaryInfo/EffectiveDt");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                policyEffDate.Text = objNode.InnerText;
            }

            //expiry Date
            objNode = objResponse.SelectSingleNode("PolicyStaging/PolicySummaryInfo/ExpirationDt");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                policyExpDate.Text = objNode.InnerText;
            }

            // Pradyumna 1/14/2014 : WWIG GAP 16 Check for Cancellation Date and Agent - Start
            objNode = objResponse.SelectSingleNode("PolicyStaging/PolicySummaryInfo/CancelledDate");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                policyCancelledDt.Text = objNode.InnerText;
                policyCancelledStatus.Text = policyStatus.Text;
            }
            if (!string.IsNullOrEmpty(policyCancelledDt.Text) && !string.IsNullOrEmpty(policyCancelledStatus.Text))
            {
                policyCancelledStatus.Visible = true;
                policyCancelledDt.Visible = true;
            }

            //Agency Number
            objNode = objResponse.SelectSingleNode("PolicyStaging/PolicySummaryInfo/AgencyNum");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                policyAgencyNumber.Text = objNode.InnerText;
            }

            //Agency Name
            objNode = objResponse.SelectSingleNode("PolicyStaging/PolicySummaryInfo/AgencyName");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                policyAgencyName.Text = objNode.InnerText;
            }
            // Pradyumna 1/14/2014 : WWIG GAP 16 Check for Cancellation Date and Agent - Start

            //policy state  : mapped with state value of address ; setting teh actual value in hidden field
            objNode = objResponse.SelectSingleNode("PolicyStaging/InsuredInfo/StateProvCd");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                policyState.Text = objNode.InnerText;
            }
            objNode = objResponse.SelectSingleNode("PolicyStaging/InsuredInfo/StateProvCd");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                hdPolState.Value = objNode.InnerText;
            }

            //line of business
            objNode = objResponse.SelectSingleNode("PolicyStaging/PolicySummaryInfo/LOBCd");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                policyLOB.Text = objNode.InnerText;
            }

            //Insured name
            string sTemp = string.Empty;
            objNode = objResponse.SelectSingleNode("PolicyStaging/InsuredInfo/CommercialName");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                policyInsured.Text = objNode.InnerText;
            }

            //sort name
            objNode = objResponse.SelectSingleNode("PolicyStaging/InsuredInfo/CommercialName");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                policySortname.Text = objNode.InnerText;
            }

            //addrr
            sTemp = string.Empty;
            objNode = objResponse.SelectSingleNode("PolicyStaging/InsuredInfo/Addr1");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                sTemp = string.Concat(objNode.InnerText, " ");
            }
            objNode = objResponse.SelectSingleNode("PolicyStaging/InsuredInfo/Addr2");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                sTemp = string.Concat(sTemp, objNode.InnerText);
            }
            policyAddr.Text = sTemp;

            //city
            objNode = objResponse.SelectSingleNode("PolicyStaging/InsuredInfo/City");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                policyAddrCity.Text = objNode.InnerText;
            }

            //state
            objNode = objResponse.SelectSingleNode("PolicyStaging/InsuredInfo/StateProvCd");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                policyAddrState.Text = objNode.InnerText;
            }

            //postal code
            objNode = objResponse.SelectSingleNode("PolicyStaging/InsuredInfo/PostalCode");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                policyAddrZipCode.Text = objNode.InnerText;
            }

            //premium
            objNode = objResponse.SelectSingleNode("PolicyStaging/PolicySummaryInfo/csc_AgentPremium");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                policyPremium.Text = objNode.InnerText;
            }

            //optional name/address
            objNode = objResponse.SelectSingleNode("PolicyStaging/InsuredInfo/OtherGivenName");
            if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
            {
                policyOptionalNameAddr.Text = objNode.InnerText;
            }
        }

        private void PopulateUnitStagingDetails(string pResponseXML)
        {
            XmlNode objIssueCdNode;
            XmlDocument objUnitListDetail = null;

            try
            {
                objUnitListDetail = new XmlDocument();
                objUnitListDetail.LoadXml(pResponseXML);
            }
            catch (Exception e)
            {
                ErrorControl1.DisplayError(e.Message);
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(e);
                dvParent.Visible = false;
                return;
            }

            DataRow drRow = null;

            DataTable dtData = new DataTable();
            dtData.Columns.Add("UnitType", typeof(string));
            dtData.Columns.Add("UnitNum", typeof(string));
            dtData.Columns.Add("Status", typeof(string));
            dtData.Columns.Add("Prem", typeof(string));
            dtData.Columns.Add("Desc", typeof(string));
            dtData.Columns.Add("VIN", typeof(string));
            dtData.Columns.Add("MakeYear", typeof(string));
            dtData.Columns.Add("Addr", typeof(string));
            dtData.Columns.Add("val", typeof(string));
            dtData.Columns.Add("InsLine", typeof(string));

            foreach (XmlNode xNode in objUnitListDetail.SelectNodes("Units/Unit/Vehicle"))
            {
                drRow = dtData.NewRow();

                drRow[0] = "Vehicle";
                
                if (xNode.SelectSingleNode("csc_UnitStatus") != null)
                {
                    drRow[2] = xNode.SelectSingleNode("csc_UnitStatus").InnerText.Trim();
                }
                if (xNode.SelectSingleNode("Manufacturer") != null)
                {
                    drRow[4] = xNode.SelectSingleNode("Manufacturer").InnerText.Trim();
                }
                if (xNode.SelectSingleNode("csc_VehicleIdentificationNumber") != null)
                {
                    drRow[5] = xNode.SelectSingleNode("csc_VehicleIdentificationNumber").InnerText.Trim();
                }
                if (xNode.SelectSingleNode("ModelYear") != null)
                {
                    drRow[6] = xNode.SelectSingleNode("ModelYear").InnerText.Trim();
                }
                if (xNode.ParentNode != null)
                {
                    if (xNode.ParentNode.SelectSingleNode("./QueryString") != null)
                    {
                        drRow[8] = xNode.ParentNode.SelectSingleNode("./QueryString").InnerText.Trim();
                    }

                    if (xNode.ParentNode.SelectSingleNode("./csc_UnitNumber") != null)
                    {
                        drRow[1] = xNode.ParentNode.SelectSingleNode("./csc_UnitNumber").InnerText.Trim();
                    }
                    
                }

                dtData.Rows.Add(drRow);
            }

            foreach (XmlNode xNode in objUnitListDetail.SelectNodes("Units/Unit/Property"))
            {
                drRow = dtData.NewRow();

                drRow[0] = "Property";
                if (xNode.SelectSingleNode("csc_UnitStatus") != null)
                {
                    drRow[2] = xNode.SelectSingleNode("csc_UnitStatus").InnerText.Trim();
                }
                if (xNode.SelectSingleNode("csc_UnitDesc") != null)
                {
                    drRow[4] = xNode.SelectSingleNode("csc_UnitDesc").InnerText.Trim();
                }
                if (xNode.SelectSingleNode("csc_YearBuilt") != null)
                {
                    drRow[6] = xNode.SelectSingleNode("csc_YearBuilt").InnerText.Trim();
                }
                if (xNode.SelectSingleNode("csc_UnitAddress") != null)
                {
                    drRow[7] = xNode.SelectSingleNode("csc_UnitAddress").InnerText.Trim();
                }
                if (xNode.ParentNode != null)
                {
                    if (xNode.ParentNode.SelectSingleNode("./QueryString") != null)
                    {
                        drRow[8] = xNode.ParentNode.SelectSingleNode("./QueryString").InnerText.Trim();
                    }

                    if (xNode.ParentNode.SelectSingleNode("./csc_UnitNumber") != null)
                    {
                        drRow[1] = xNode.ParentNode.SelectSingleNode("./csc_UnitNumber").InnerText.Trim();
                    }

                }

                dtData.Rows.Add(drRow);
            }

            foreach (XmlNode xNode in objUnitListDetail.SelectNodes("Units/Unit/Site"))
            {
                drRow = dtData.NewRow();

                drRow[0] = "Site";
                if (xNode.SelectSingleNode("csc_UnitStatus") != null)
                {
                    drRow[2] = xNode.SelectSingleNode("csc_UnitStatus").InnerText.Trim();
                }
                if (xNode.SelectSingleNode("csc_UnitDesc") != null)
                {
                    drRow[4] = xNode.SelectSingleNode("csc_UnitDesc").InnerText.Trim();
                }
                if (xNode.SelectSingleNode("csc_UnitAddress") != null)
                {
                    drRow[7] = xNode.SelectSingleNode("csc_UnitAddress").InnerText.Trim();
                }
                if (xNode.ParentNode != null)
                {
                    if (xNode.ParentNode.SelectSingleNode("./QueryString") != null)
                    {
                        drRow[8] = xNode.ParentNode.SelectSingleNode("./QueryString").InnerText.Trim();
                    }

                    if (xNode.ParentNode.SelectSingleNode("./csc_UnitNumber") != null)
                    {
                        drRow[1] = xNode.ParentNode.SelectSingleNode("./csc_UnitNumber").InnerText.Trim();
                    }

                }

                dtData.Rows.Add(drRow);
            }

            // Modified - Pradyumna MITS#35296 03032014 - Start
            GridDataSource = dtData;
            //gvData.DataSource = dtData;
            //gvData.DataBind();
            //ViewState["Data"] = dtData;
            // Modified - Pradyumna MITS#35296 03032014 - End
            
            if (dtData.Rows.Count == 0)   //if (multiunitid.Items.Count == 0)
            {
                btnUnitDetails.Enabled = false;
                btnUnitCov.Enabled = false;
                btnFormDataUnit.Enabled = false;
                btnUnitInterest.Enabled = false;
            }
        }

        /// <summary>
        /// Populates the Policy Additional Data on screen with data from staging tables
        /// </summary>
        /// <param name="pResponseXML"></param>
        /// Added by Pradyumna 1/6/2013 - WWIG GAP 16
        private void PopulatePolicyStgngAddtnlDetails(string pResponseXML)
        {
            IEnumerable<XElement> oElements = null;
            XElement oDocElement, oSubEleLN, oSubEleFN, oSubEleMN, oEleAddr1, oEleAddr2, oEleCity,oEleRole, oTemp;
            ListItem liData;
            StringBuilder sbToolTip;
            StringBuilder sbListData;
            string sVal;
            string sEntityId = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(pResponseXML))
                    return;
                oDocElement = XElement.Parse(pResponseXML);
                sbToolTip = new StringBuilder();
                sbListData = new StringBuilder();
                oElements = oDocElement.XPathSelectElements("InterestList");

                if (oElements != null)
                {
                    foreach (XElement oElement in oElements)
                    {
                        oSubEleLN = null;
                        oSubEleFN = null;
                        oSubEleMN = null;
                        oEleAddr1 = null;
                        oEleAddr2 = null;
                        oEleRole = null;
                        oTemp = null;
                        sbToolTip.Length = 0;
                        sbListData.Length = 0;
                        oSubEleLN = oElement.XPathSelectElement("Lastname");
                        oSubEleFN = oElement.XPathSelectElement("Firstname");
                        oSubEleMN = oElement.XPathSelectElement("Middlename");
                        oEleAddr1 = oElement.XPathSelectElement("Addr1");
                        oEleAddr2 = oElement.XPathSelectElement("Addr2");
                        oEleCity = oElement.XPathSelectElement("City");
                        oEleRole = oElement.XPathSelectElement("Role");
                        oTemp = oElement.XPathSelectElement("EntityId");
                        sEntityId = oTemp.Value;

                        if (oEleAddr1 != null && !string.IsNullOrEmpty(oEleAddr1.Value))
                        {
                            if (sbToolTip.Length <= 0)
                                sbToolTip.Append("Addr: ");
                            sbToolTip.Append(oEleAddr1.Value + " ");
                        }
                        if (oEleAddr2 != null && !string.IsNullOrEmpty(oEleAddr2.Value))
                        {
                            if (sbToolTip.Length <= 0)
                                sbToolTip.Append("Addr: ");
                            sbToolTip.Append(oEleAddr2.Value + " ");
                        }
                        if (oEleCity != null && !string.IsNullOrEmpty(oEleCity.Value))
                        {
                            sbToolTip.Append(" City: " + oEleCity.Value);
                        }

                        sVal = sEntityId + "|" + hdPolicySystemID.Value;

                        if (oSubEleFN != null && !string.IsNullOrEmpty(oSubEleFN.Value))
                        {
                            sbListData.Append(oSubEleFN.Value + " ");
                        }
                        if (oSubEleMN != null && !string.IsNullOrEmpty(oSubEleMN.Value))
                        {
                            sbListData.Append(oSubEleMN.Value + " ");
                        }
                        if (oSubEleLN != null && !string.IsNullOrEmpty(oSubEleLN.Value))
                        {
                            sbListData.Append(oSubEleLN.Value + " ");
                            //liData = new ListItem(oSubEle1.Value + " : " + oSubEle2.Value, sVal);
                        }
                        if (oEleRole != null && !string.IsNullOrEmpty(oEleRole.Value))
                        {
                            if (sbListData.Length > 0)
                                sbListData.Append(": " + oEleRole.Value);
                        }

                        liData = new ListItem(sbListData.ToString(), sVal);
                        lbAdditionalInfo.Items.Add(liData);

                        liData.Attributes.Add("title", sbToolTip.ToString());
                    }
                }
                if (lbAdditionalInfo.Items.Count == 0)
                {
                    lblAdditionalPolicyMsg.Visible = true;
                    btnAdditionalPolicyDetails.Enabled = false;
                }
            }
            catch (Exception e)
            {
                ErrorControl1.DisplayError(e.Message);
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(e);
                dvParent.Visible = false;
                return;
            }
            finally
            {
                oElements = null;
                sbToolTip = null;
                sbListData = null;
            }
        }

        /// <summary>
        /// Populates the Policy Driver List Data on screen with data from staging tables
        /// </summary>
        /// <param name="pResponseXML"></param>
        /// Added by Pradyumna 1/8/2013 - WWIG GAP 16
        private void PopulateDriverStgngDetails(string pResponseXML)
        {
            IEnumerable<XElement> oElements = null;
            XElement oDocElement, oSubEleLN, oSubEleFN, oSubEleMN, oEleState, oEleDrvType, oEleLicNum, oTemp;
            ListItem liData;
            StringBuilder sbToolTip;
            StringBuilder sbListData;
            string sVal;

            try
            {
                if (string.IsNullOrEmpty(pResponseXML))
                    return;
                oDocElement = XElement.Parse(pResponseXML);
                sbToolTip = new StringBuilder();
                sbListData = new StringBuilder();
                oElements = oDocElement.XPathSelectElements("DriverList");

                if (oElements != null)
                {
                    foreach (XElement oElement in oElements)
                    {
                        oSubEleLN = null;
                        oSubEleFN = null;
                        oSubEleMN = null;
                        oEleDrvType = null;
                        oEleState = null;
                        oEleLicNum = null;
                        oTemp = null;
                        sbToolTip.Length = 0;
                        sbListData.Length = 0;
                        oSubEleLN = oElement.XPathSelectElement("Lastname");
                        oSubEleFN = oElement.XPathSelectElement("Firstname");
                        oSubEleMN = oElement.XPathSelectElement("Middlename");
                        oEleState = oElement.XPathSelectElement("State");
                        oEleDrvType = oElement.XPathSelectElement("DriverType");
                        oEleLicNum = oElement.XPathSelectElement("LicenceNum");
                        oTemp = oElement.XPathSelectElement("EntityId");
                        sVal = oTemp.Value + "|" + hdPolicySystemID.Value;

                        if (oEleDrvType != null && !string.IsNullOrEmpty(oEleDrvType.Value))
                        {
                            sbToolTip.Append("Driver Type: " + oEleDrvType.Value + " ");
                        }
                        if (oEleState != null && !string.IsNullOrEmpty(oEleState.Value))
                        {
                            sbToolTip.Append("State: " + oEleState.Value);
                        }

                        if (oSubEleFN != null && !string.IsNullOrEmpty(oSubEleFN.Value))
                        {
                            sbListData.Append(oSubEleFN.Value + " ");
                        }
                        if (oSubEleMN != null && !string.IsNullOrEmpty(oSubEleMN.Value))
                        {
                            sbListData.Append(oSubEleMN.Value + " ");
                        }
                        if (oSubEleLN != null && !string.IsNullOrEmpty(oSubEleLN.Value))
                        {
                            sbListData.Append(oSubEleLN.Value + " ");
                        }
                        if (oEleLicNum != null && !string.IsNullOrEmpty(oEleLicNum.Value))
                        {
                            if (sbListData.Length > 0)
                                sbListData.Append(": " + oEleLicNum.Value);
                        }

                        liData = new ListItem(sbListData.ToString(), sVal);
                        lbDriverInfo.Items.Add(liData);

                        liData.Attributes.Add("title", sbToolTip.ToString());
                    }
                }
                if (oElements != null && lbDriverInfo.Items.Count > 0)
                {
                    TABSdriverinfo.Attributes.Add("style", "display:' '");
                    TBSPdriverinfo.Attributes.Add("style", "display:' '");
                }

            }
            catch (Exception e)
            {
                ErrorControl1.DisplayError(e.Message);
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(e);
                dvParent.Visible = false;
                return;
            }
            finally
            {
                oElements = null;
                sbToolTip = null;
                sbListData = null;
            }
        }
    }
}
