﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.UI.PolicyInterfaceService;
using Riskmaster.Common;
using Riskmaster.BusinessHelpers;
using Riskmaster.BusinessAdaptor.Common;
using System.ServiceModel;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Collections;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Configuration;


namespace Riskmaster.UI.UI.PolicyInterface
{
    public partial class PS_PolicyUnitDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PolicyInterfaceBusinessHelper oHelper;
            PolicyEnquiry oRequest, oResponse ;
            string sValues;
            string[] sCollection;
            bool bInSuccess;
            if (!Page.IsPostBack)
            {
                try
                {
                    //tanwar2 - Policy Download from staging - start
                    if (ConfigurationManager.AppSettings["RemoveEmptyFieldsOnPolicyEnquiry"] != null)
                    {
                        hdnRemoveEmtpyFields.Value = ConfigurationManager.AppSettings["RemoveEmptyFieldsOnPolicyEnquiry"];
                    }
                    //tanwar2 - Policy Download from staging - end

                    sValues = AppHelper.GetQueryStringValue("val");
                    sCollection = sValues.Split('|');
                    oRequest = new PolicyEnquiry();

                    
                    oRequest.UnitNumber = sCollection[0];
                    oRequest.InsLine = sCollection[1];
                    oRequest.UnitRiskLocation = sCollection[2];
                    oRequest.UnitSubRiskLocation = sCollection[3];
                    oRequest.UnitState = sCollection[4];
                    oRequest.Product = sCollection[5];
                    oRequest.UnitStatus = sCollection[6];
                    oRequest.PolicyNumber = sCollection[7];
                    oRequest.PolicySymbol = sCollection[8];
                    oRequest.State = sCollection[9];
                    oRequest.LOB = sCollection[10];
                    oRequest.IssueCode = sCollection[11];
                    oRequest.Location = sCollection[12];
                    oRequest.MasterCompany = sCollection[13];
                    oRequest.Module = sCollection[14];
                    oRequest.EffDate = sCollection[15];
                    oRequest.PolCompany = sCollection[16];
                    oRequest.PolicyIdentfier = sCollection[7];
                    oRequest.PolicySystemId = Conversion.CastToType<int>(sCollection[17], out bInSuccess);
                  //  oRequest.BaseLOBLine = sCollection[18];//skhare7 POlicy Interface

                    hfStatUnitNumber.Value = sCollection[18];
                    oRequest.UnitTypeInd = sCollection[19];
                   
                    oHelper = new PolicyInterfaceBusinessHelper();
                    oResponse = oHelper.GetUnitDetailResult(oRequest);

                    if (!HasErrorOccured(oResponse.ResponseAcordXML))
                    {
                        PopulateDetails(oResponse.ResponseAcordXML, oResponse.BaseLOBLine, sCollection[11], sCollection[19]);
                    }
                   
                }
                catch (FaultException<RMException> ee)
                {
                    ErrorControl1.Visible = true;
                    ErrorHelper.logErrors(ee);
                    ErrorControl1.errorDom = ee.Detail.Errors;
                }
                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                }
                
            }

        }

        private void PopulateDetails(string pResponseXML, string pLOB, string pIssueCd, string pStatFlag)
        {
            pLOB = pLOB.Trim().ToUpper();
            pIssueCd = pIssueCd.Trim().ToUpper();
            pStatFlag = pStatFlag.Trim().ToUpper();

            if (pIssueCd == "M" | pStatFlag.Equals("SU"))
            {
                PopulateMannualRated(pResponseXML);
            }
            else
            {
                switch (pLOB)
                {
                    //case "HP":
                   // case "MHO":
                    case "PL": PopulateProperty(pResponseXML);
                    break;

                   // case "ACV":
                    case "AL": PopulateVehicle(pResponseXML);
                        break;

                  //  case "WC":
                 //   case "WCA":
                    case "WL": PopulateWorker(pResponseXML);
                        break;

                 //   case "BOP":
                    case "CL": PopulateCPP(pResponseXML);
                        break;

                    default: throw new Exception(string.Format("Policy LOB value {0} not found.",pLOB));
                }
            }
        }
        private void PopulateProperty(string pXML)
        {
            dvProperty.Visible = true;
            XElement objDoc, oSubEle1, oSubEle2,oTemp, oSubEle3;
            IEnumerable<XElement> oElements, oInfoElements, oCovElements;
            Label lb1, lb2, lb3, lb4;
            HtmlTableRow trNew;
            HtmlTableCell tcNew1, tcNew2, tcNew3,tcNew4;
            objDoc = XElement.Parse(pXML);

            oElements = objDoc.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/PropertyLossInfo");
            foreach(XElement oElement in oElements)
            {
                oInfoElements = oElement.XPathSelectElements("./ItemIdInfo/OtherIdentifier");
                oTemp = oElement.XPathSelectElement("com.csc_UnitStatus");
                lblPropStatus.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("com.csc_RateBook");
                lblRatebook.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("PremiumInfo/PremiumAmt");
                lblPropPrem.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("com.csc_UnitLossData/com.csc_UnitDescription/com.csc_UnitDesc");
                lblLocAddr.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("com.csc_UnitLossData/com.csc_UnitDescription/com.csc_UnitCity");
                lblLocCity.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("com.csc_UnitLossData/com.csc_UnitDescription/com.csc_Territory");
                lblPropTerr.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("com.csc_UnitLossData/com.csc_UnitDescription/com.csc_Zipcode");
                lblPropPostalCd.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("com.csc_ProtectionClass");
                lblProtectnClas.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("com.csc_ConstructionCd");
                lblCons.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("com.csc_YearBuilt");
                lblYrCons.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("com.csc_UnitLossData/com.csc_UnitDescription/com.csc_NumberFamilies");
                lblNoFamilies.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("com.csc_UnitLossData/com.csc_UnitDescription/com.csc_InsideCity");
                lblInsideCity.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("com.csc_UnitLossData/com.csc_UnitDescription/com.csc_OccupancyCd");
                lblOccupancy.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("com.csc_CoverageLossInfo/Deductible/DeductibleAmt");
                lblDeduc.Text = oTemp.Value;

                oCovElements = oElement.XPathSelectElements("com.csc_CoverageLossInfo/Coverage");
                //if (oCovElements.Count() > 0)
                //{
                //    trCovInfoHeading.Visible = true;
                //}
                //foreach (XElement oCovElement in oCovElements)
                //{
                //    lb1 = new Label(); lb2 = new Label(); lb3 = new Label(); lb4 = new Label();
                //    tcNew1 = new HtmlTableCell(); tcNew2 = new HtmlTableCell();
                //    tcNew3 = new HtmlTableCell(); tcNew4 = new HtmlTableCell();
                //    trNew = new HtmlTableRow();

                //    oSubEle1 = oCovElement.XPathSelectElement("Limit/LimitAmt");
                //    oSubEle2 = oCovElement.XPathSelectElement("CurrentTermAmt");
                //    oSubEle3 = oCovElement.XPathSelectElement("CoverageCd");

                //    lb1.Text = oSubEle3.Value+" Limit";
                //    lb1.CssClass = "required";
                //    lb2.Text = oSubEle1.Value;
                //    lb3.Text = "Premium";
                //    lb3.CssClass = "required";
                //    lb4.Text = oSubEle2.Value;

                //    tcNew1.Controls.Add(lb1);
                //    tcNew2.Controls.Add(lb2);
                //    tcNew3.Controls.Add(lb3);
                //    tcNew4.Controls.Add(lb4);

                //    trNew.Cells.Add(tcNew1);
                //    trNew.Cells.Add(tcNew2);
                //    trNew.Cells.Add(tcNew3);
                //    trNew.Cells.Add(tcNew4);

                //    tbProperty.Rows.Add(trNew);

                //}

                
                foreach (XElement oInfoElement in oInfoElements)
                {
                    oSubEle1 = oInfoElement.XPathSelectElement("./OtherIdTypeCd");
                    oSubEle2 = oInfoElement.XPathSelectElement("./OtherId ");
                    //if (string.Compare(oSubEle1.Value, "com.csc_UnitNumber", true) == 0)
                    //    lblPropUnit.Text = oSubEle2.Value;
                    //if (string.Compare(oSubEle1.Value, "com.csc_StatUnitNumber", true) == 0)
                        //lblPropUnit.Text = oSubEle2.Value;
                        lblPropUnit.Text = hfStatUnitNumber.Value;
                    if (string.Compare(oSubEle1.Value, "com.csc_InsLineCd", true) == 0)
                        lblPropInsLn.Text = oSubEle2.Value;
                    if (string.Compare(oSubEle1.Value, "com.csc_Product", true) == 0)
                        lblPropProduct.Text = oSubEle2.Value;
                    if (string.Compare(oSubEle1.Value, "com.csc_RateState", true) == 0)
                        lblLocState.Text = oSubEle2.Value;
                }
            }
        }
        private void PopulateVehicle(string pXML)
        {
            dvVehicle.Visible = true;
            IEnumerable<XElement> oElements, oInfoElements;
            XElement oDoc, oSubEle1, oSubEle2,oTemp;
            string sTemp;
            oDoc = XElement.Parse(pXML);

            oElements = oDoc.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/AutoLossInfo");
            foreach (XElement oElement in oElements)
            {
                oTemp = oElement.XPathSelectElement("./com.csc_UnitLossData/com.csc_UnitStatus");
                lblStatus.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("./PremiumInfo/PremiumAmt");
                lblUnitPrem.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("./VehInfo/Model");
                lblModel.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("./VehInfo/ModelYear");
                lblVehicleYr.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("com.csc_VehicleIdentificationNumber");
                lblSerial.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("com.csc_VehicleTypeCd");
                lblTypeVeh.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("./com.csc_UnitLossData/com.csc_UnitDescription/com.csc_Territory");
                lblTerritory.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("./com.csc_UnitLossData/com.csc_UnitDescription/com.csc_VehicleSymbol");
                lblVehSymbol.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("./com.csc_CostNewAmt/Amt");
                lblCostNm.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("./com.csc_PurchaseDt");
                lblPurchaseDt.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("./com.csc_UnitLossData/com.csc_AirbagDiscount");
                lblAirBagDsc.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("./com.csc_UnitLossData/com.csc_AntiTheftInd");
                lblTheft.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("./com.csc_UnitLossData/com.csc_AntiLockBrakes");
                lblBrakes.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("com.csc_BodyType");
                lblBodyType.Text = oTemp.Value;

                oTemp = oElement.XPathSelectElement("com.csc_VehicleUse/ActionCd");
                sTemp = oTemp.Value;
                oTemp = oElement.XPathSelectElement("com.csc_VehicleUse/com.csc_VehicleUseDesc");
                lblUseOfVeh.Text = sTemp.Trim() + "  " + oTemp.Value;

                oTemp = oElement.XPathSelectElement("./com.csc_PrimaryClassCd");
                lblPrimaryClas.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("./com.csc_SecondaryClassCd");
                lblSecClass.Text = oTemp.Value;
                    
                oInfoElements = oElement.XPathSelectElements("./VehInfo/ItemIdInfo/OtherIdentifier");
                foreach (XElement oInfoElement in oInfoElements)
                {
                    oSubEle1 = oInfoElement.XPathSelectElement("./OtherIdTypeCd");
                    oSubEle2 = oInfoElement.XPathSelectElement("./OtherId ");
                    //if(string.Compare(oSubEle1.Value,"com.csc_UnitNumber",true)==0)
                        //lblUnitNum.Text = oSubEle2.Value;
                    lblUnitNum.Text = hfStatUnitNumber.Value;
                    if (string.Compare(oSubEle1.Value, "com.csc_Product", true) == 0)
                        lblProduct.Text = oSubEle2.Value;
                    if (string.Compare(oSubEle1.Value, "com.csc_InsLineCd", true) == 0)
                        lblInsLn.Text = oSubEle2.Value;
                    if (string.Compare(oSubEle1.Value, "com.csc_RateState", true) == 0)
                        lblRateState.Text = oSubEle2.Value;

                }
            }
        }
        private void PopulateWorker(string pXML)
        {
            dvWorker.Visible = true;
            IEnumerable<XElement> oElements, oInfoElements, oTaxElements;
            XElement oDoc, oSubEle1, oSubEle2, oTemp;
            oDoc = XElement.Parse(pXML);
            string sTemp = string.Empty;

            oElements = oDoc.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/WorkCompLocInfo");
            foreach (XElement oElement in oElements)
            {
                oTemp = oElement.XPathSelectElement("com.csc_UnitStatusDesc");
                lblWCVStatus.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("Location/LocationName");
                lblNm.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("Location/com.csc_OptionalName"); //check node
                lblOptional.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("Location/Addr/Addr1");
                sTemp = oTemp.Value;
                oTemp = oElement.XPathSelectElement("Location/Addr/Addr2");
                lblAddr.Text = sTemp + " " + oTemp.Value;
                oTemp = oElement.XPathSelectElement("Location/Addr/City");
                lblCity.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("Location/Addr/StateProvCd");
                lblState.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("Location/Addr/PostalCode");
                lblPostalCd.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("Location/Addr/County"); 
                lblCountyCd.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("Location/Communications/PhoneInfo");
                lblPhnNo.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("Location/Communications/EmailInfo");
                lblContact.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("Location/TaxCodeInfo/TaxCd");
                lblTaxLoc.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("com.csc_AuditorsInfo/com.csc_Audit_Basis");
                lblAuditBasis.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("com.csc_AuditorsInfo/com.csc_AuditType");
                lblAduitType.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("SICCd");
                lblSIC.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("NumEmployees");
                lblEmpCt.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("com.csc_AuditorsInfo/com.csc_SiteAuditorName");
                lblFAudit2.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("com.csc_AuditorsInfo/com.csc_SiteAuditor");
                lblFAudit1.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("com.csc_AuditorsInfo/com_csc_SiteAuditor_I");
                lblIAud1.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("com.csc_AuditorsInfo/com_csc_SiteAuditorName_I");
                lblIAud2.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("com.csc_AuditorsInfo/com_csc_SiteAuditor_C");
                lblCAudit1.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("com.csc_AuditorsInfo/com_csc_SiteAuditorName_C");
                lblCAudit2.Text = oTemp.Value;

                oTaxElements = oElement.XPathSelectElements("Location/TaxCodeInfo/TaxIdentity");
                foreach (XElement oTaxElement in oTaxElements)
                {
                    oSubEle1 = oTaxElement.XPathSelectElement("TaxIdTypeCd");
                    oSubEle2 = oTaxElement.XPathSelectElement("TaxId");
                    if (string.Compare(oSubEle1.Value, "Unemployment", true) == 0)
                    {
                        lblUnemployNum.Text = oSubEle2.Value;
                    }
                    if (string.Compare(oSubEle1.Value, "FEIN", true) == 0)
                    {
                        lblFEIN.Text = oSubEle2.Value;
                    }
                }
                
                oTemp = oElement.XPathSelectElement("com.csc_PrimaryClassCd");
                lblGCC.Text = oTemp.Value;
                oTemp = oElement.XPathSelectElement("com.csc_PrimaryClassDesc");
                lblGCD.Text = oTemp.Value;

                oInfoElements = oElement.XPathSelectElements("./Location/ItemIdInfo/OtherIdentifier");
                foreach (XElement oInfoElement in oInfoElements)
                {
                    oSubEle1 = oInfoElement.XPathSelectElement("./OtherIdTypeCd");
                    oSubEle2 = oInfoElement.XPathSelectElement("./OtherId ");
                    //if (string.Compare(oSubEle1.Value, "com.csc_UnitNumber", true) == 0)
                    //if (string.Compare(oSubEle1.Value, "com.csc_StatUnitNumber", true) == 0)
                    //{
                        //lblSite.Text = oSubEle2.Value;
                    lblSite.Text = hfStatUnitNumber.Value;
                    //}
                    if (string.Compare(oSubEle1.Value, "com.csc_SiteSequenceNum", true) == 0)
                    {
                        lblSeqNo.Text = oSubEle2.Value;
                    }
                    
                }
            }
        }
        private void PopulateMannualRated(string pXML)
        {
            dvManual.Visible = true;
            XElement oDoc, oSubEle1,oSubEle2;
            IEnumerable<XElement> oElements, oInfoElements;
            Label lblName, lblVal;
            oDoc = XElement.Parse(pXML);
            HtmlTableRow trNew;
            HtmlTableCell tcNew1, tcNew2;
            oElements = oDoc.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/InsuredOrPrincipal");
            foreach (XElement oElement in oElements)
            {
                oInfoElements = oElement.XPathSelectElements("./com.csc_InsuredNameIdInfo/ItemIdInfo/OtherIdentifier");
                foreach (XElement oInfoElement in oInfoElements)
                {
                    oSubEle1 = oInfoElement.XPathSelectElement("./OtherIdTypeCd");
                    oSubEle2 = oInfoElement.XPathSelectElement("./OtherId");
                    lblName = new Label();
                    lblName.Text = oSubEle1.Value;
                    lblName.Style.Add("font-weight", "bold");
                    tcNew1 = new HtmlTableCell();
                    tcNew1.Controls.Add(lblName);
                    tcNew1.Width = "25%";

                    lblVal = new Label();
                    lblVal.Text = oSubEle2.Value;
                    tcNew2 = new HtmlTableCell();
                    tcNew2.Controls.Add(lblVal);
                    tcNew2.Width = "75%";

                    trNew = new HtmlTableRow();
                    trNew.Cells.Add(tcNew1);
                    trNew.Cells.Add(tcNew2);
                    
                    tableM.Rows.Add(trNew);
                }
            }

        }
        private bool HasErrorOccured(string pResponseXML)
        {
            bool bResult = false;
            IEnumerable<XElement> oElements;
            XElement objDoc, oErrorEle;
            string sErrorText = string.Empty;

            objDoc = XElement.Parse(pResponseXML);
            oElements = objDoc.XPathSelectElements("./ClaimsSvcRs/com.csc_ErrorList/com.csc_Error");
            if (oElements != null)
            {
                foreach (XElement oElement in oElements)
                {
                    oErrorEle = oElement.XPathSelectElement("./ERROR");
                    sErrorText = string.Concat(sErrorText, oErrorEle.Value);
                    bResult = true;
                }
            }
            if (bResult)
            {
                ErrorControl1.DisplayError(sErrorText);
                ErrorControl1.Visible = true;
            }

            return bResult;

        }
        private void PopulateCPP(string pXML)
        {
            dvCPP.Visible = true;

            XElement objDoc, oSubEle1,oSubEle2, oTemp;
            IEnumerable<XElement> oPropElements, oVehElements, oInfoEle;
            objDoc = XElement.Parse(pXML);
            
            oPropElements = objDoc.XPathSelectElements("ClaimsSvcRs/com.csc_PolicyUnitFetchRs/PropertyLossInfo");
            oVehElements = objDoc.XPathSelectElements("ClaimsSvcRs/com.csc_PolicyUnitFetchRs/AutoLossInfo");
            if (oPropElements.Count() != 0)
            {
                trProp1.Visible = true;
                //trProp2.Visible = true;
                foreach (XElement oElement in oPropElements)
                {

                    oTemp = oElement.XPathSelectElement("com.csc_UnitStatus");
                    lblCPPStatus.Text = oTemp.Value;
                    oTemp = oElement.XPathSelectElement("com.csc_RateBook");
                    lblCPPRatebook.Text = oTemp.Value;
                    oTemp = oElement.XPathSelectElement("PremiumInfo/PremiumAmt");
                    lblCPPprem.Text = oTemp.Value;
                    oTemp = oElement.XPathSelectElement("com.csc_PrimaryClassCd");
                    lblCPPCd1.Text = oTemp.Value;
                    oTemp = oElement.XPathSelectElement("com.csc_PrimaryClassDesc");
                    //tkatsarski: 11/20/14 - RMA-4280 Changed csc_PrimaryClassDesc to be shown in lblCPpDesc instead of lblCPPCd2
                    lblCPpDesc.Text = oTemp.Value;

                    oInfoEle = oElement.XPathSelectElements("ItemIdInfo/OtherIdentifier");
                    foreach (XElement oInfo in oInfoEle)
                    {
                        oSubEle1 = oInfo.XPathSelectElement("OtherIdTypeCd");
                        oSubEle2 = oInfo.XPathSelectElement("OtherId");
                        if (string.Compare(oSubEle1.Value, "com.csc_InsLineCd", true) == 0)
                            lblCPPInsLn.Text = oSubEle2.Value;
                        if (string.Compare(oSubEle1.Value, "com.csc_Product", true) == 0)
                            lblCPPProd.Text = oSubEle2.Value;
                        if (string.Compare(oSubEle1.Value, "com.csc_RateState", true) == 0)
                            lblCPpState.Text = oSubEle2.Value;
                        if (string.Compare(oSubEle1.Value, "com.csc_RiskLoc", true) == 0)
                            lblCppLoc.Text = oSubEle2.Value;
                        if (string.Compare(oSubEle1.Value, "com.csc_RiskSubLoc", true) == 0)
                            lblBuilding.Text = oSubEle2.Value;
                    }
                }
            }
            else if (oVehElements.Count() != 0)
            {
                trVeh1.Visible = true;
                trVeh2.Visible = true;

                foreach (XElement oElement in oVehElements)
                {

                    oTemp = oElement.XPathSelectElement("com.csc_UnitLossData/com.csc_UnitStatus");
                    lblCPPStatus.Text = oTemp.Value;
                    oTemp = oElement.XPathSelectElement("com.csc_RateBook");
                    lblCPPRatebook.Text = oTemp.Value;
                    oTemp = oElement.XPathSelectElement("PremiumInfo/PremiumAmt");
                    lblCPPprem.Text = oTemp.Value;
                    oTemp = oElement.XPathSelectElement("com.csc_PrimaryClassCd");
                    lblCPPCd1.Text = oTemp.Value;
                    oTemp = oElement.XPathSelectElement("com.csc_PrimaryClassDesc");
                    lblCPPCd2.Text = oTemp.Value;
                    oTemp = oElement.XPathSelectElement("com.csc_StatedAmt/Amt");
                    lblUnitStatedAmt.Text = oTemp.Value;
                    oTemp = oElement.XPathSelectElement("com.csc_VehicleIdentificationNumber");
                    lblVIN.Text = oTemp.Value;
                    oTemp = oElement.XPathSelectElement("VehInfo/Manufacturer");
                    lblCPpDesc.Text = oTemp.Value;
                    oTemp = oElement.XPathSelectElement("VehInfo/ModelYear");
                    lblCppModelYr.Text = oTemp.Value;


                    oInfoEle = oElement.XPathSelectElements("VehInfo/ItemIdInfo/OtherIdentifier");
                    foreach (XElement oInfo in oInfoEle)
                    {
                        oSubEle1 = oInfo.XPathSelectElement("OtherIdTypeCd");
                        oSubEle2 = oInfo.XPathSelectElement("OtherId");
                        if (string.Compare(oSubEle1.Value, "com.csc_InsLineCd", true) == 0)
                            lblCPPInsLn.Text = oSubEle2.Value;
                        if (string.Compare(oSubEle1.Value, "com.csc_Product", true) == 0)
                            lblCPPProd.Text = oSubEle2.Value;
                        if (string.Compare(oSubEle1.Value, "com.csc_RateState", true) == 0)
                            lblCPpState.Text = oSubEle2.Value;
                        //if (string.Compare(oSubEle1.Value, "com.csc_UnitNumber", true) == 0)
                        //if (string.Compare(oSubEle1.Value, "com.csc_StatUnitNumber", true) == 0)
                            //lblCPPUnit.Text = oSubEle2.Value;
                        lblCPPUnit.Text = hfStatUnitNumber.Value;
                    }
                }
            }
        }

        private void PopulateStagingUnits(string p_sXml)
        {
            XmlDocument xDoc = null;
            XmlNode xTempNode = null;
            string sUnitType = string.Empty;

            try
            {
                xDoc = new XmlDocument();
                xDoc.LoadXml(p_sXml);
                xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/UnitType");
                if (xTempNode!=null)
                {
                    sUnitType = xTempNode.InnerText;
                }

                switch (sUnitType.Trim())
                {
                    case "v":
                    case "V":
                       dvVehicle.Visible = true;

                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/Model");
                        if (xTempNode != null)
                        {
                            lblModel.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/VehicleYear");
                        if (xTempNode != null)
                        {
                            lblVehicleYr.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/Vin");
                        if (xTempNode != null)
                        {
                            lblSerial.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/VehicleType");
                        if (xTempNode != null)
                        {
                            lblTypeVeh.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/LeaseAmt");
                        if (xTempNode != null)
                        {
                            lblCostNm.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/UnitNo");
                        if (xTempNode != null)
                        {
                            lblUnitNum.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/InsLine");
                        if (xTempNode != null)
                        {
                            lblInsLn.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/Product");
                        if (xTempNode != null)
                        {
                            lblProduct.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/State");
                        if (xTempNode != null)
                        {
                            lblRateState.Text = xTempNode.InnerText;
                        }

                        //Added by swati
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/Status");
                        if (xTempNode != null)
                        {
                            lblStatus.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/Territory");
                        if (xTempNode != null)
                        {
                            lblTerritory.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/VehicleSym");
                        if (xTempNode != null)
                        {
                            lblVehSymbol.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/PurchDate");
                        if (xTempNode != null)
                        {
                            lblPurchaseDt.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/NewCost");
                        if (xTempNode != null)
                        {
                            lblCostNm.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/PrimaryCls");
                        if (xTempNode != null)
                        {
                            lblPrimaryClas.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/SecondaryCls");
                        if (xTempNode != null)
                        {
                            lblSecClass.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/AirbagDscnt");
                        if (xTempNode != null)
                        {
                            lblAirBagDsc.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/AntiLockBrakes");
                        if (xTempNode != null)
                        {
                            lblBrakes.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/AntiTheftInd");
                        if (xTempNode != null)
                        {
                            lblTheft.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/BodyType");
                        if (xTempNode != null)
                        {
                            lblBodyType.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/VehicleUse");
                        if (xTempNode != null)
                        {
                            lblUseOfVeh.Text = xTempNode.InnerText;
                        }
                        //change end here by swati
                        break;
                    case "p":
                    case "P":
                        dvProperty.Visible = true;

                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/Addr");
                        if (xTempNode != null)
                        {
                            lblLocAddr.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/City");
                        if (xTempNode != null)
                        {
                            lblLocCity.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/ZipCode");
                        if (xTempNode != null)
                        {
                            lblPropPostalCd.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/YearBuilt");
                        if (xTempNode != null)
                        {
                            lblYrCons.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/InsLine");
                        if (xTempNode != null)
                        {
                            lblPropInsLn.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/Product");
                        if (xTempNode != null)
                        {
                            lblPropProduct.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/State");
                        if (xTempNode != null)
                        {
                            lblLocState.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/StatUnitNo");
                        if (xTempNode != null)
                        {
                            lblPropUnit.Text = xTempNode.InnerText;
                        }
                        //added by swati
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/Status");
                        if (xTempNode != null)
                        {
                            lblPropStatus.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/Ratebook");
                        if (xTempNode != null)
                        {
                            lblRatebook.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/Premium");
                        if (xTempNode != null)
                        {
                            lblPropPrem.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/Territory");
                        if (xTempNode != null)
                        {
                            lblPropTerr.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/ProtectionCls");
                        if (xTempNode != null)
                        {
                            lblProtectnClas.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/Deductible");
                        if (xTempNode != null)
                        {
                            lblDeduc.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/NumOfFamilies");
                        if (xTempNode != null)
                        {
                            lblNoFamilies.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/ConstrtnType");
                        if (xTempNode != null)
                        {
                            lblCons.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/InsideCity");
                        if (xTempNode != null)
                        {
                            lblInsideCity.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/Occupance");
                        if (xTempNode != null)
                        {
                            lblOccupancy.Text = xTempNode.InnerText;
                        }
                        //change end here by swati
                        break;
                    case "s":
                    case "S":
                        dvWorker.Visible = true;

                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/Optional");
                        if (xTempNode != null)
                        {
                            lblOptional.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/Addr");
                        if (xTempNode != null)
                        {
                            lblAddr.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/City");
                        if (xTempNode != null)
                        {
                            lblCity.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/State");
                        if (xTempNode != null)
                        {
                            lblState.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/PostalCode");
                        if (xTempNode != null)
                        {
                            lblPostalCd.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/Country");
                        if (xTempNode != null)
                        {
                            lblCountyCd.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/Phone");
                        if (xTempNode != null)
                        {
                            lblPhnNo.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/Contact");
                        if (xTempNode != null)
                        {
                            lblContact.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/Sic");
                        if (xTempNode != null)
                        {
                            lblSIC.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/Fein");
                        if (xTempNode != null)
                        {
                            lblFEIN.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/StatUnitNo");
                        if (xTempNode != null)
                        {
                            lblSite.Text = xTempNode.InnerText;
                        }

                        //Added by Swati
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/Status");
                        if (xTempNode != null)
                        {
                            lblWCVStatus.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/TaxLocation");
                        if (xTempNode != null)
                        {
                            lblTaxLoc.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/AuditBasis");
                        if (xTempNode != null)
                        {
                            lblAuditBasis.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/AuditType");
                        if (xTempNode != null)
                        {
                            lblAduitType.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/InterimAuditor");
                        if (xTempNode != null)
                        {
                            lblIAud1.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/CheckAuditor");
                        if (xTempNode != null)
                        {
                            lblCAudit1.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/FinalAuditor");
                        if (xTempNode != null)
                        {
                            lblFAudit1.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/UnemplymntNum");
                        if (xTempNode != null)
                        {
                            lblUnemployNum.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/NumOfEmp");
                        if (xTempNode != null)
                        {
                            lblEmpCt.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/GvrngClsCode");
                        if (xTempNode != null)
                        {
                            lblGCC.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/SeqNum");
                        if (xTempNode != null)
                        {
                            lblSeqNo.Text = xTempNode.InnerText;
                        }
                        xTempNode = xDoc.SelectSingleNode("//Units/Instance/Unit/GvrngClsDesc");
                        if (xTempNode != null)
                        {
                            lblGCD.Text = xTempNode.InnerText;
                        }
                        //change end here by swati
                        break;
                    default:
                        break;
                }
                
            }
            catch (Exception)
            {
                
                throw;
            }
        }
    }
}