﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.BusinessHelpers;
using Riskmaster.UI.PolicyInterfaceService;
using Riskmaster.Common;
using System.ServiceModel;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.Linq;

using System.Xml.XPath;

namespace Riskmaster.UI.UI.PolicyInterface
{
    public partial class PS_Driver : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PolicyInterfaceBusinessHelper oHelper;
            PolicyEnquiry oRequest, oResponse;
            string sValues;
            string[] sCollection;
            bool bInSuccess;
            if (!Page.IsPostBack)
            {
                try
                {
                    sValues = AppHelper.GetQueryStringValue("val");
                    sCollection = sValues.Split('|');
                    oRequest = new PolicyEnquiry();

                   
                    oRequest.PolicyNumber = sCollection[0];
                    oRequest.PolicySymbol = sCollection[1];
                    oRequest.Module = sCollection[2];
                    oRequest.Location = sCollection[3];
                    oRequest.MasterCompany = sCollection[4];
                    oRequest.LOB = sCollection[5];
                    oRequest.UnitState = sCollection[6];
                    oRequest.InterestTypeCode = sCollection[7];
                    oRequest.Product = sCollection[8];
                    oRequest.UnitStatus = sCollection[9];
                    oRequest.PolicySystemId = Conversion.CastToType<int>(sCollection[10], out bInSuccess);
                    oRequest.PolicyIdentfier = sCollection[0];
                    oRequest.PolCompany = sCollection[11];
                    oRequest.UnitNumber = sCollection[12];
                    oRequest.UnitRiskLocation = sCollection[13];
                    oRequest.UnitSubRiskLocation = sCollection[14];
                    oHelper = new PolicyInterfaceBusinessHelper();
                    oResponse = oHelper.GetPolicyDriverDetails(oRequest);
                    PopulateDetails(oResponse.ResponseAcordXML);                   
                }
                catch (FaultException<RMException> ee)
                {
                    ErrorControl1.Visible = true;
                    ErrorHelper.logErrors(ee);
                    ErrorControl1.errorDom = ee.Detail.Errors;
                }
                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                }

            }
        }
        protected void PopulateDetails(string pXML)
        {
            XElement oDoc, oNode;

            oDoc = XElement.Parse(pXML);

            oNode = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_DriverDetailRs/DriverInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_DriverId']/OtherId");
            lblDriver.Text = oNode.Value;

            oNode = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_DriverDetailRs/DriverInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_DriverType']/OtherId");

            oNode = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_DriverDetailRs/DriverInfo/com.csc_DriverStatus");
            lblStatus.Text = oNode.Value;

            oNode = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_DriverDetailRs/DriverInfo/PersonInfo/NameInfo/PersonName/GivenName");
            lblFirstNm.Text = oNode.Value;

            oNode = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_DriverDetailRs/DriverInfo/PersonInfo/NameInfo/PersonName/Surname");
            lblLastNm.Text = oNode.Value;

            oNode = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_DriverDetailRs/DriverInfo/PersonInfo/NameInfo/PersonName/NameSuffix");
            lblSuffix.Text = oNode.Value; 

            oNode = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_DriverDetailRs/DriverInfo/PersonInfo/NameInfo/PersonName/OtherGivenName");
            lblMiddleNm.Text = oNode.Value;

            oNode = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_DriverDetailRs/DriverInfo/PersonInfo/BirthDt");
            lblDOB.Text = oNode.Value;

            oNode = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_DriverDetailRs/DriverInfo/PersonInfo/GenderCd");
            lblGender.Text = oNode.Value;

            oNode = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_DriverDetailRs/DriverInfo/PersonInfo/MaritalStatusCd");
            lblMarital.Text = oNode.Value;

            oNode = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_DriverDetailRs/DriverInfo/com.csc_RelationToInsured");
            lblRelatn.Text = oNode.Value;

            oNode = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_DriverDetailRs/DriverInfo/License/StateProvCd");
            lblLicenseState.Text = oNode.Value;

            oNode = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_DriverDetailRs/DriverInfo/License/LicensePermitNumber");
            lblLicNo.Text = oNode.Value;

            oNode = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_DriverDetailRs/DriverInfo/License/LicensedDt");
            lblDtLicensed.Text = oNode.Value;

            oNode = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_DriverDetailRs/DriverInfo/com.csc.MVRInd");
            lblMVRInd.Text = oNode.Value;

            oNode = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_DriverDetailRs/DriverInfo/com_csc_ExcludedDriverInd");
            lblDriverStatus.Text = oNode.Value;

            oNode = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_DriverDetailRs/DriverInfo/com.csc_UsrInd_6");
            lblSR.Text = oNode.Value;

            oNode = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_DriverDetailRs/DriverInfo/PersonInfo/Age");
            lblAge.Text = oNode.Value;

            oNode = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_DriverDetailRs/DriverInfo/com.csc.DriverClass");
            lblDriverClass.Text = oNode.Value;

            oNode = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_DriverDetailRs/DriverInfo/com.csc_PrincipalOperator_VH_1");
            lblPO1.Text = oNode.Value;

            oNode = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_DriverDetailRs/DriverInfo/com.csc_PrincipalOperator_VH_2");
            lblPO2.Text = oNode.Value;

            oNode = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_DriverDetailRs/DriverInfo/com.csc_PrincipalOperator_VH_3");
            lblPO3.Text = oNode.Value;

            oNode = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_DriverDetailRs/DriverInfo/com.csc_ParttimeOperator_VH_1");
            lblPTO1.Text = oNode.Value;

            oNode = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_DriverDetailRs/DriverInfo/com.csc_ParttimeOperator_VH_2");
            lblPTO2.Text = oNode.Value;

            oNode = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_DriverDetailRs/DriverInfo/com.csc_ParttimeOperator_VH_3");
            lblPTO3.Text = oNode.Value;
        }

        /// <summary>
        /// Populates Driver Screen fields data with Staging Data
        /// </summary>
        /// <param name="pXML"></param>
        /// Pradyumna 1/10/2013 - WWIG GAP 16 - Ends
        protected void PopulateStagingDetails(string pXML)
        {
            XElement oDoc, oNode, oElements;

            oElements = XElement.Parse(pXML);
             //= oDoc.XPathSelectElement("DriverDetail");

            oNode = oElements.XPathSelectElement("DriverType");
            if(oNode != null && !string.IsNullOrEmpty(oNode.Value))
                lblDriver.Text = oNode.Value;

            oNode = oElements.XPathSelectElement("Status");
            if (oNode != null && !string.IsNullOrEmpty(oNode.Value))
                lblStatus.Text = oNode.Value;

            oNode = oElements.XPathSelectElement("FirstName");
            if (oNode != null && !string.IsNullOrEmpty(oNode.Value))
                lblFirstNm.Text = oNode.Value;

            oNode = oElements.XPathSelectElement("LastName");
            if (oNode != null && !string.IsNullOrEmpty(oNode.Value))
                lblLastNm.Text = oNode.Value;
            
            oNode = oElements.XPathSelectElement("MiddleName");
            if (oNode != null && !string.IsNullOrEmpty(oNode.Value))
                lblMiddleNm.Text = oNode.Value;

            oNode = oElements.XPathSelectElement("Suffix");
            if (oNode != null && !string.IsNullOrEmpty(oNode.Value))
                lblSuffix.Text = oNode.Value;

            oNode = oElements.XPathSelectElement("DOB");
            if (oNode != null && !string.IsNullOrEmpty(oNode.Value))
                lblDOB.Text = oNode.Value;

            oNode = oElements.XPathSelectElement("SexCode");
            if (oNode != null && !string.IsNullOrEmpty(oNode.Value))
                lblGender.Text = oNode.Value;

            oNode = oElements.XPathSelectElement("MaritalStatus");
            if (oNode != null && !string.IsNullOrEmpty(oNode.Value))
                lblMarital.Text = oNode.Value;

            oNode = oElements.XPathSelectElement("Relation");
            if (oNode != null && !string.IsNullOrEmpty(oNode.Value))
                lblRelatn.Text = oNode.Value;

            oNode = oElements.XPathSelectElement("LicState");
            if (oNode != null && !string.IsNullOrEmpty(oNode.Value))
                lblLicenseState.Text = oNode.Value;

            oNode = oElements.XPathSelectElement("LicNo");
            if (oNode != null && !string.IsNullOrEmpty(oNode.Value))
                lblLicNo.Text = oNode.Value;

            oNode = oElements.XPathSelectElement("LicDate");
            if (oNode != null && !string.IsNullOrEmpty(oNode.Value))
                lblDtLicensed.Text = oNode.Value;

            oNode = oElements.XPathSelectElement("MVRInd");
            if (oNode != null && !string.IsNullOrEmpty(oNode.Value))
                lblMVRInd.Text = oNode.Value;

            oNode = oElements.XPathSelectElement("DriverStatus");
            if (oNode != null && !string.IsNullOrEmpty(oNode.Value))
                lblDriverStatus.Text = oNode.Value;

            oNode = oElements.XPathSelectElement("SR");
            if (oNode != null && !string.IsNullOrEmpty(oNode.Value))
                lblSR.Text = oNode.Value;

            oNode = oElements.XPathSelectElement("Age");
            if (oNode != null && !string.IsNullOrEmpty(oNode.Value))
                lblAge.Text = oNode.Value;

            oNode = oElements.XPathSelectElement("DriverCls");
            if (oNode != null && !string.IsNullOrEmpty(oNode.Value))
                lblDriverClass.Text = oNode.Value;

            oNode = oElements.XPathSelectElement("PO1");
            if (oNode != null && !string.IsNullOrEmpty(oNode.Value))
                lblPO1.Text = oNode.Value;

            oNode = oElements.XPathSelectElement("PO2");
            if (oNode != null && !string.IsNullOrEmpty(oNode.Value))
                lblPO2.Text = oNode.Value;

            oNode = oElements.XPathSelectElement("PO3");
            if (oNode != null && !string.IsNullOrEmpty(oNode.Value))
                lblPO3.Text = oNode.Value;

            oNode = oElements.XPathSelectElement("PTO1");
            if (oNode != null && !string.IsNullOrEmpty(oNode.Value))
                lblPTO1.Text = oNode.Value;

            oNode = oElements.XPathSelectElement("PTO2");
            if (oNode != null && !string.IsNullOrEmpty(oNode.Value))
                lblPTO2.Text = oNode.Value;

            oNode = oElements.XPathSelectElement("PTO3");
            if (oNode != null && !string.IsNullOrEmpty(oNode.Value))
                lblPTO3.Text = oNode.Value;
        }
    }
}