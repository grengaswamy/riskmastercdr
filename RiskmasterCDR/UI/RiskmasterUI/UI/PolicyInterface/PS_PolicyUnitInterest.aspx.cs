﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.UI.PolicyInterfaceService;
using Riskmaster.Common;
using System.Xml;
using Riskmaster.BusinessHelpers;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Collections.Generic;
using System.ServiceModel;
using Riskmaster.BusinessAdaptor.Common;
using System.Data;

namespace Riskmaster.UI.UI.PolicyInterface
{
    public partial class PS_PolicyUnitInterest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    LoadDetails();
                }
            }
            catch (FaultException<RMException> ee)
            {
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private void LoadDetails()
        {
            PolicyInterfaceBusinessHelper objHelper = null;
            PolicyEnquiry oEnquiryRequest = null;
            PolicyEnquiry oEnquiryResponse = null;
            string sValues;
            string[] sCollection;
            bool bInSuccess;
            string sUnitRowId = string.Empty;
            try
            {
                oEnquiryRequest = new PolicyEnquiry();
                objHelper = new PolicyInterfaceBusinessHelper();
                sUnitRowId = AppHelper.GetQueryStringValue("UnitRowId");
             
                    oEnquiryRequest.UnitRowId = Conversion.CastToType<int>(sUnitRowId, out bInSuccess);
                    oEnquiryResponse = objHelper.GetPolicyUnitInterestList(oEnquiryRequest);


                    PopulateAdditionalInterestList(oEnquiryResponse.ResponseAcordXML, oEnquiryRequest);
                
               

                 
            }
            catch (FaultException<RMException> ee)
            {
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;
            }
            catch (Exception e)
            {
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(e, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                oEnquiryRequest = null;
                oEnquiryResponse = null;
            }
        }

        /// <summary>
        /// populates the Additional policy tab 
        /// </summary>
        /// <param name="pResponseXML">represents the response XML which has 
        /// the data to be populated on the UI</param>
        private void PopulateAdditionalInterestList(string pResponseXML, PolicyEnquiry oEnquiryRequest)
        {
            IEnumerable<XElement> oElements = null;
            XElement oDocElement, oSubEle1, oSubEle2, oEleAddr1, oEleCity, oTemp;
            string sName=string.Empty;
            string sRole=string.Empty;
            int iInterestId;

            ListItem liData;
            string sToolTip, sVal, sLocationNum, sSeqNum;

            oDocElement = XElement.Parse(pResponseXML);

            oElements = oDocElement.XPathSelectElements("./option");

            foreach (XElement oElement in oElements)
            {

               
                sToolTip = string.Empty; sLocationNum = string.Empty; sSeqNum = string.Empty;
                sName = oElement.Value;
              int iValue=Conversion.ConvertStrToInteger( oElement.Attribute("value").Value);


                sToolTip = sName;
                hdnPolicySystemId.Value = oElement.Attribute("PSID").Value;
                //  if (oSubEle1 != null && oSubEle2 != null && !string.IsNullOrEmpty(oSubEle1.Value) && !string.IsNullOrEmpty(oSubEle2.Value))

                if (!string.IsNullOrEmpty(sName))
                    {
                        liData = new ListItem(sName, iValue.ToString());
                        lbAdditionalInfo.Items.Add(liData);
                        liData.Attributes.Add("title", sToolTip);
                    }
                    
                   
                
            }
            if (lbAdditionalInfo.Items.Count == 0)
            {
                lblAdditionalPolicyMsg.Visible = true;
                btnAdditionalInterestDetails.Enabled = false;
            }
        }

        private bool HasErrorOccured(string pResponseXML)
        {
            bool bResult = false;
            IEnumerable<XElement> oElements;
            XElement objDoc, oErrorEle;
            string sErrorText = string.Empty;

            objDoc = XElement.Parse(pResponseXML);
            oElements = objDoc.XPathSelectElements("./ClaimsSvcRs/com.csc_ErrorList/com.csc_Error");
            if (oElements != null)
            {
                foreach (XElement oElement in oElements)
                {
                    oErrorEle = oElement.XPathSelectElement("./ERROR");
                    sErrorText = string.Concat(sErrorText, oErrorEle.Value);
                    bResult = true;
                }
            }
            if (bResult)
            {
                ErrorControl1.DisplayError(sErrorText);
                ErrorControl1.Visible = true;
            }

            return bResult;

        }
    }
}
