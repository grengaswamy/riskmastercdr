﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddPolicyConfig.aspx.cs" Inherits="Riskmaster.UI.PolicyInterface.AddPolicyConfig" %>
<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <title>Upload configuration settings</title>
   
    <script type="text/javascript" src="../../Scripts/form.js">{var j;}</script>
    <script src="../../Scripts/jquery/jquery-1.8.0.js" type="text/javascript"></script>
    <script type="text/javascript">
        function Save()
        {
            
            var DefaultClmOffice = $('#rbtnAddDefaultClmOffice input:checked').val();
            var DefaultExaminer = $('#rbtnAddDefaultExaminer input:checked').val();
            var DefaultExaminerCD = $('#rbtnAddDefaultExaminerCD input:checked').val();
            var DefaultAdjustor = $('#rbtnAddDefaultAdjustor input:checked').val();
            var EntryInPMSP0000ForAL = $('#rbtnAddEntryInPMSP0000ForAL input:checked').val();
            var Adjustor = document.getElementById('txtAdjustor').value;
            var ClmOffice = document.getElementById('txtClmOffice').value;
            var examiner = document.getElementById('txtExaminer').value;
            var ExaminerCD = document.getElementById('txtExaminerCD').value;
            //var DefaultAdj = $('#rbtnAddDefault input:checked').val();
            var ExemptClaimId = document.getElementById('txtExemptClaimId').value;
            //start - added by Nikhil.Upload voucher amount setting
            var UploadVoucher = $('#rbtnUpldVchr input:checked').val();
            //End - added by Nikhil.Upload voucher amount setting
            //aaggarwal29 : RMA-7701 start
            var Payee2inPMSPAP00 = $('#rdPMSPAP00PayeeData input:checked').val();
            //aaggarwal29 : RMA-7701 end
            if (DefaultClmOffice == 'YES')
                DefaultClmOffice= -1;
            else
                DefaultClmOffice = 0;

            if (DefaultExaminer == 'YES')
                DefaultExaminer = -1;
            else
                DefaultExaminer = 0;

            if (DefaultExaminerCD == 'YES')
                DefaultExaminerCD = -1;
            else
                DefaultExaminerCD = 0;

            if (DefaultAdjustor == 'YES')
                DefaultAdjustor = -1;
            else
                DefaultAdjustor = 0;

            if (EntryInPMSP0000ForAL == 'YES')
                EntryInPMSP0000ForAL = -1;
            else
                EntryInPMSP0000ForAL = 0;
           
            //if (DefaultAdj == 'YES')
            //    DefaultAdj = 1;
            //else
            //    DefaultAdj = 0;
            //start - added by Nikhil.Upload voucher amount setting
            if (UploadVoucher == 'YES')
                UploadVoucher = -1;
            else
                UploadVoucher = 0;
            //end - added by Nikhil.Upload voucher amount setting
            if (Payee2inPMSPAP00 == 'YES')
                Payee2inPMSPAP00 = -1;
            else
                Payee2inPMSPAP00 = 0;
            if (!isNum(ExemptClaimId))
            {
                //alert('Exempt ClaimId should be numeric.Please enter a valid number.');
                alert(AddPolicyConfigValidations.ValidNumber);
                document.getElementById('txtExemptClaimId').value = "";
                document.getElementById('txtExemptClaimId').focus();
                return false;
            }
           
            if (DefaultClmOffice == "-1" && ClmOffice == "")
            {
                //alert('Clm Office cannot be blank.');
                alert(AddPolicyConfigValidations.BlankClmOffice);
                document.getElementById('txtClmOffice').focus();
                return false;
            }

            if (DefaultExaminer == "-1" && examiner == "") {
                //alert('Clm Examiner cannot be blank.');
                alert(AddPolicyConfigValidations.BlankClmExaminer);
                document.getElementById('txtExaminer').focus();
                return false;
            }
            if (DefaultExaminerCD == "-1" && ExaminerCD == "") {
                //alert('Clm Examiner CD cannot be blank.');
                alert(AddPolicyConfigValidations.BlankClmExaminerCD);
                document.getElementById('txtExaminerCD').focus();
                return false;
            }
            if (DefaultAdjustor == "-1" && Adjustor == "") {
                //alert('Ajustor cannot be blank.');
                alert(AddPolicyConfigValidations.BlankAdjustor);
                document.getElementById('txtAdjustor').focus();
                return false;
            }
            //window.opener.document.forms[0].txtAddConfig.value = DefaultClmOffice + '|' + DefaultExaminer + '|' + DefaultExaminerCD + '|' + DefaultAdjustor + '|' + EntryInPMSP0000ForAL + '|' + Adjustor + '|' + ClmOffice + '|' + examiner + '|' + ExaminerCD + '|' + DefaultAdj + '|' + ExemptClaimId;
            //start - changed by Nikhil.Upload voucher amount setting
            //window.opener.document.forms[0].txtAddConfig.value = DefaultClmOffice + '|' + DefaultExaminer + '|' + DefaultExaminerCD + '|' + DefaultAdjustor + '|' + EntryInPMSP0000ForAL + '|' + Adjustor + '|' + ClmOffice + '|' + examiner + '|' + ExaminerCD + '|'  + ExemptClaimId;
            window.opener.document.forms[0].txtAddConfig.value = DefaultClmOffice + '|' + DefaultExaminer + '|' + DefaultExaminerCD + '|' + DefaultAdjustor + '|' + EntryInPMSP0000ForAL + '|' + Adjustor + '|' + ClmOffice + '|' + examiner + '|' + ExaminerCD + '|' + ExemptClaimId + '|' + UploadVoucher + '|' + Payee2inPMSPAP00;
            //end - changed by Nikhil.Upload voucher amount setting
            window.close();
        }
        
        function EnableDisable(liText, txtID)
        {
            if (liText == 'YES') {
                document.getElementById(txtID).disabled = false;
                return false;
            }
            else {
                document.getElementById(txtID).disabled = true;
                return false;
            }
        }

        function Populate() {
           
            var arr = null;
            var AddConfigValues = window.opener.document.getElementById('txtAddConfig').value;
            if (AddConfigValues != null && AddConfigValues != "") {
                arr = AddConfigValues.split('|');
                if (arr[0] != "" && arr[0] == "-1") {
                    $('#rbtnAddDefaultClmOffice').find("input[value='YES']").attr("checked", "checked");
                    $("#txtClmOffice").prop('disabled', false);
                }
                else {
                    $('#rbtnAddDefaultClmOffice').find("input[value='NO']").attr("checked", "checked");
                    $("#txtClmOffice").prop('disabled', true);
                }
                if (arr[1] != "") {
                    $('#txtClmOffice').val(arr[1]);
                }

                if (arr[2] != "" && arr[2] == "-1") {
                    $('#rbtnAddDefaultExaminer').find("input[value='YES']").attr("checked", "checked");
                    $("#txtExaminer").prop('disabled', false);
                }
                else {
                    $('#rbtnAddDefaultExaminer').find("input[value='NO']").attr("checked", "checked");
                    $("#txtExaminer").prop('disabled', true);
                }
                if (arr[3] != "") {
                    $('#txtExaminer').val(arr[3]);
                }

                if (arr[4] != "" && arr[4] == "-1") {
                    $('#rbtnAddDefaultExaminerCD').find("input[value='YES']").attr("checked", "checked");
                    $("#txtExaminerCD").prop('disabled', false);
                }
                else {
                    $('#rbtnAddDefaultExaminerCD').find("input[value='NO']").attr("checked", "checked");
                    $("#txtExaminerCD").prop('disabled', true);
                }
                if (arr[5] != "") {
                    $('#txtExaminerCD').val(arr[5]);
                }

                if (arr[6] != "" && arr[6] == "-1") {
                    $('#rbtnAddDefaultAdjustor').find("input[value='YES']").attr("checked", "checked");
                    $("#txtAdjustor").prop('disabled', false);
                }
                else {
                    $('#rbtnAddDefaultAdjustor').find("input[value='NO']").attr("checked", "checked");
                    $("#txtAdjustor").prop('disabled', true);
                }
                if (arr[7] != "") {
                    $('#txtAdjustor').val(arr[7]);
                }

                if (arr[8] != "" && arr[8] == "-1") {
                    $('#rbtnAddEntryInPMSP0000ForAL').find("input[value='YES']").attr("checked", "checked");
                }
                else {
                    $('#rbtnAddEntryInPMSP0000ForAL').find("input[value='NO']").attr("checked", "checked");
                }
                if (arr[9] != "") {
                    $('#txtExemptClaimId').val(arr[9]);
                }
                //start - added by Nikhil.Upload voucher amount setting
                if (arr[10] != "" && arr[10] == "-1") {
                    $('#rbtnUpldVchr').find("input[value='YES']").attr("checked", "checked");
                }
                else {
                    $('#rbtnUpldVchr').find("input[value='NO']").attr("checked", "checked");
                }
                //End -  added by Nikhil.Upload voucher amount setting
                //aaggarwal29 : RMA-7701 start
                if (arr[11] != "" && arr[11] == "-1") {
                    $('#rdPMSPAP00PayeeData').find("input[value='YES']").attr("checked", "checked");
                }
                else {
                    $('#rdPMSPAP00PayeeData').find("input[value='NO']").attr("checked", "checked");
                }
                //aaggarwal29 : RMA-7701 end
            }
        }

    </script>
</head>
<body onload="Populate();">
    <form id="form1" runat="server">
    <div class="msgheader"><asp:label class="msgheader" runat="server" id="lblUploadPolicySetting" text="<%$ Resources:lblUploadPolicySetting %>"></asp:label></div>
    <div>
        <table>
            <tr>
               <td>
                  <asp:label runat="server" id="lblDefaultClmOffice" text="<%$ Resources:lblDefaultClmOffice %>"></asp:label>
               </td>
               <td>
                   <asp:radiobuttonlist id ="rbtnAddDefaultClmOffice" runat="server" RepeatDirection="Horizontal">
                       <asp:ListItem value="<%$ Resources:rbtnliYes %>" onclick="EnableDisable('YES','txtClmOffice');"></asp:ListItem>
                       <asp:ListItem selected="true" value ="<%$ Resources:rbtnliNo %>" onclick="EnableDisable('NO','txtClmOffice');"></asp:ListItem>
                   </asp:radiobuttonlist>
                  <%-- <uc:CodeLookUp runat="server" CodeTable="YES_NO" ControlName="AddDefaultClmOffice" ID="AddDefaultClmOffice" type="code" />--%>                
               </td>
            </tr>  
             <tr>
              <td>
                 <asp:label runat="server" id="lblClmOffice" text="<%$ Resources:lblClmOffice %>"></asp:label>
              </td>
              <td>
                  <asp:textbox runat="server" id ="txtClmOffice" disabled="true" MaxLength="3" ></asp:textbox>
              </td>
            </tr> 
            <tr>
               <td>
                  <asp:label runat="server" id="lblDefaultExaminer" text="<%$ Resources:lblDefaultExaminer %>"></asp:label>
               </td>
               <td>
                   <asp:radiobuttonlist id ="rbtnAddDefaultExaminer" runat="server" RepeatDirection="Horizontal" >
                       <asp:ListItem value="<%$ Resources:rbtnliYes %>" onclick="EnableDisable('YES','txtExaminer');"></asp:ListItem>
                       <asp:ListItem selected="true" value ="<%$ Resources:rbtnliNo %>" onclick="EnableDisable('NO','txtExaminer');"></asp:ListItem>
                   </asp:radiobuttonlist>
                   <%--<uc:CodeLookUp runat="server" CodeTable="YES_NO" ControlName="AddDefaultExaminer" ID="AddDefaultExaminer" type="code" />                --%>
                </td>
            </tr> 
            <tr>
              <td>
                 <asp:label runat="server" id="lblExaminer" text="<%$ Resources:lblExaminer %>"></asp:label>
              </td>
              <td>
                  <asp:textbox runat="server" id ="txtExaminer" disabled="true" MaxLength="10"></asp:textbox>
              </td>
            </tr>
            <tr>
               <td>
                  <asp:label runat="server" id="lblDefaultExaminerCD" text="<%$ Resources:lblDefaultExaminerCD %>" ></asp:label>
               </td>
               <td>
                   <asp:radiobuttonlist id ="rbtnAddDefaultExaminerCD" runat="server" RepeatDirection="Horizontal" >
                       <asp:ListItem value="<%$ Resources:rbtnliYes %>" onclick="EnableDisable('YES','txtExaminerCD');"></asp:ListItem>
                       <asp:ListItem selected="true" value ="<%$ Resources:rbtnliNo %>" onclick="EnableDisable('NO','txtExaminerCD');"></asp:ListItem>
                   </asp:radiobuttonlist>
                   <%--<uc:CodeLookUp runat="server" CodeTable="YES_NO" ControlName="AddDefaultExaminerCD" ID="AddDefaultExaminerCD" type="code" />   --%>
                </td>
            </tr> 
            <tr>
              <td>
                 <asp:label runat="server" id="lblExaminerCD" text="<%$ Resources:lblExaminerCD %>" ></asp:label>
              </td>
              <td>
                  <asp:textbox runat="server" id ="txtExaminerCD" disabled="true" MaxLength="2"></asp:textbox>
              </td>
            </tr>
            <tr>
               <td>
                  <asp:label runat="server" id="lblDefaultAdjustor" text="<%$ Resources:lblDefaultAdjustor %>"></asp:label>
               </td>
               <td>
                   <asp:radiobuttonlist id ="rbtnAddDefaultAdjustor" runat="server" RepeatDirection="Horizontal" >
                       <asp:ListItem value="<%$ Resources:rbtnliYes %>" onclick="EnableDisable('YES','txtAdjustor');"></asp:ListItem>
                       <asp:ListItem selected="true" value ="<%$ Resources:rbtnliNo %>" onclick="EnableDisable('NO','txtAdjustor');"></asp:ListItem>
                   </asp:radiobuttonlist>
                   <%--<uc:CodeLookUp runat="server" CodeTable="YES_NO" ControlName="AddDefaultAdjustor" ID="AddDefaultAdjustor" type="code" /> --%>
                </td>
            </tr> 
             <tr>
              <td>
                 <asp:label runat="server" id="lblAdjuster" text="<%$ Resources:lblAdjuster %>"></asp:label>
              </td>
              <td>
                  <asp:textbox runat="server" id ="txtAdjustor" disabled="true" MaxLength="6"></asp:textbox>
              </td>
            </tr>    
            <tr>
               <td>
                  <asp:label runat="server" id="lblEntryInPMS" text="<%$ Resources:lblEntryInPMS %>"></asp:label>
               </td>
               <td>
                   <asp:radiobuttonlist id ="rbtnAddEntryInPMSP0000ForAL" runat="server" RepeatDirection="Horizontal" >
                       <asp:ListItem value="<%$ Resources:rbtnliYes %>"></asp:ListItem>
                       <asp:ListItem selected="true" value ="<%$ Resources:rbtnliNo %>"></asp:ListItem>
                   </asp:radiobuttonlist>
                    <%--<uc:CodeLookUp runat="server" CodeTable="YES_NO" ControlName="AddEntryInPMSP0000ForAL" ID="AddEntryInPMSP0000ForAL" type="code" />--%>
                </td>
            </tr>                                      
           <%--<tr>
               <td>
                  <asp:label runat="server" text="Add Default"></asp:label>
               </td>
               <td>
                   <asp:radiobuttonlist id ="rbtnAddDefault" runat="server" RepeatDirection="Horizontal" >
                       <asp:ListItem value="YES"></asp:ListItem>
                       <asp:ListItem selected="true" value ="NO"></asp:ListItem>
                   </asp:radiobuttonlist>
                   <uc:CodeLookUp runat="server" CodeTable="YES_NO" ControlName="AddDefault" ID="AddDefault" type="code" />
                </td>
            </tr>--%>
          
            <tr>
              <td>
                 <asp:label runat="server" id="lblExemptClaimId" text="<%$ Resources:lblExemptClaimId %>"></asp:label>
              </td>
              <td>
                  <asp:textbox runat="server" id ="txtExemptClaimId" text="0" MaxLength="4"></asp:textbox>
              </td>
            </tr>
            <%-- //start - Added by Nikhil.Upload voucher amount setting --%>
              <tr>
               <td>
                  <asp:label runat="server" id="lblUpldVchr" text="<%$ Resources:lblUpldVchr %>"></asp:label>
               </td>
               <td>
                   <asp:radiobuttonlist id ="rbtnUpldVchr" runat="server" RepeatDirection="Horizontal" >
                       <asp:ListItem value="<%$ Resources:rbtnliYes %>"></asp:ListItem>
                       <asp:ListItem selected="true" value ="<%$ Resources:rbtnliNo %>"></asp:ListItem>
                   </asp:radiobuttonlist>
                    
                </td>
            </tr>   
             <%-- //end - Added by Nikhil.Upload voucher amount setting --%>   
            <tr>
                  <td>
                  <asp:label runat="server" id="lblPMSPAP00PayeeData" text="<%$ Resources:lblPayee2inPMSPAP00 %>"></asp:label>
               </td>
               <td>
                   <asp:radiobuttonlist id ="rdPMSPAP00PayeeData" runat="server" RepeatDirection="Horizontal" >
                       <asp:ListItem value="<%$ Resources:rbtnliYes %>"></asp:ListItem>
                       <asp:ListItem selected="true" value ="<%$ Resources:rbtnliNo %>"></asp:ListItem>
                   </asp:radiobuttonlist>
                    
                </td>
            </tr>  
            <tr>
              <td>
                  <asp:button runat="server" id="btnOK" text="<%$ Resources:btnOK %>" OnClientClick="Save(); return false;" />
              </td>
           </tr>
                 
        </table>
    </div>
    </form>
</body>
</html>
