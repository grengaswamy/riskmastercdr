﻿/**********************************************************************************************
*   Date     |  MITS   | Programmer | Description                                            *
**********************************************************************************************
* 11/04/2014 | 33574   | pgupta93   | Changes for Continuous Trigger and Policy Search functionality
**********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Riskmaster.BusinessAdaptor.Common;
using System.ServiceModel;
using Riskmaster.Common;
using Riskmaster.UI.PolicyInterfaceService;
using Riskmaster.BusinessHelpers;
using System.Xml;
using System.IO;

namespace Riskmaster.UI.UI.PolicyInterface
{
    public partial class CoverageListSelection : NonFDMBasePageCWS
    {
        string s_LastUsedName = string.Empty;
        //protected void Page_Load(object sender, EventArgs e)
        //{

        //}
        public string fnTesting(string sTest)
        {
            //Label lbl = (Label) dlTest.FindControl("lblTestLable");
            //lbl.Text = sTest;
            string output = string.Empty;
            if (sTest != s_LastUsedName)
            {
                s_LastUsedName = sTest;
                output = "<b>" + sTest + "</b>";
            }
            return output;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // delete old unit interest files if any, since on cancel button click they will still persist..
                //if (AppHelper.GetQueryStringValue("delFile").Trim() != string.Empty)
                //{
                    DeleteOldFiles("unitinterest");
                //}
                // end..
                if (!IsPostBack)
                {
                    txtPolicyId.Text = AppHelper.GetQueryStringValue("PolicyId");
                    txtLossDate.Text = AppHelper.GetQueryStringValue("LossDate");
                    txtPolicyFlag.Text = AppHelper.GetQueryStringValue("DownloadPolicyWithoutClaim");//MITS:33574
                    txtLoBCode.Text = AppHelper.GetQueryStringValue("LOBCode");//MITS:33574
                    hdnClaimReportedDate.Text = AppHelper.GetQueryStringValue("ClaimDateReported");////Ashish Ahuja: Claims Made Jira 1342
                    GetDownloadOptions();
                }
            }
            catch (FaultException<RMException> ee)
            {
                btnSave.Enabled = false;
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;



            }
            catch (Exception ee)
            {
                btnSave.Enabled = false;
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
        private void DeleteOldFiles(string p_smode)
        {
            DisplayMode objMode = null;
            PolicyInterfaceBusinessHelper objHelper = new PolicyInterfaceBusinessHelper();
            try
            {
                objMode = new DisplayMode();
                objMode.Mode = p_smode;
                objHelper.DeleteOldFiles(objMode);
            }
            catch (FaultException<RMException> ee)
            {
                btnSave.Enabled = false;
                throw ee;

            }
            catch (Exception ee)
            {
                btnSave.Enabled = false;
                throw ee;

            }
            finally
            {

                objMode = null;
            }
        }

        //private void AddDataToHashTable(ref  Dictionary<string, string> objTable)
        //{


        //    objTable.Add("TaxID", "Tax ID");
        //    objTable.Add("FirstName", "First Name");
        //    objTable.Add("MiddleName", "Middle Name");
        //    objTable.Add("LastName", "Last Name");
        //    objTable.Add("RoleCd", "Role");
        //    objTable.Add("StatUnitNumber", "UnitNo");





        //}
        protected void btnSave_Click(object sender, EventArgs e)
        {
            PolicyInterfaceBusinessHelper objHelper = new PolicyInterfaceBusinessHelper();
            DisplayFileData oFileData = null;
            DataSet oDS = new DataSet();
            DataRow objRow;
            //  Hashtable objSelectedValues = null;
            XmlDocument objRetunDoc = null;
            //string[] arrSelectedValue = null;
            SaveDownloadOptions oSaveDownloadOptions = null;
            DataView objView = null;
            try
            {

                oSaveDownloadOptions = new SaveDownloadOptions();
                oSaveDownloadOptions.Mode = txtMode.Text;
                oSaveDownloadOptions.SelectedValue = txtSelectedValues.Text;
                oSaveDownloadOptions.LossDate = txtLossDate.Text.Trim();//rupal:mits 33913
                SaveData();


            }
            catch (FaultException<RMException> ee)
            {
                btnSave.Enabled = false;
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;

            }
            catch (Exception ee)
            {
                btnSave.Enabled = false;
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
            finally
            {
                if (objHelper != null)
                    objHelper = null;
                if (oDS != null)
                    oDS = null;
                if (oFileData != null)
                    oFileData = null;
                objRetunDoc = null;
            }

        }

        private void GetDownloadOptions()
        {
            PolicyInterfaceBusinessHelper objHelper = new PolicyInterfaceBusinessHelper();
            DisplayFileData oFileData = null;
            DataSet oDS = new DataSet();
            DataRow objRow;
            XmlDocument objRetunDoc = null;

            try
            {
                oFileData = objHelper.GetXml();
                txtMode.Text = oFileData.Mode;
                ViewState["EntityTypelist"] = oFileData.EntityTypelist;
                if (oFileData != null && !string.IsNullOrEmpty(oFileData.ResponseXML) && !string.Equals(oFileData.Mode, "completed", StringComparison.InvariantCultureIgnoreCase) )
                {

                    DataSet dtTest = new DataSet();
                    dtTest.ReadXml(new StringReader(oFileData.ResponseXML));
                    dlTest.DataSource = dtTest.Tables[1];
                    dlTest.DataBind();
                    lblMode.Text = "Download Coverages";
                    
                    if(dtTest!=null &&  dtTest.Tables.Count >0)
                    {
                        DataGrid.Style.Value = "";
                    }
                    else
                    {
                        btnSave.Enabled = false;
                    }
                    
                }
                else
                {
                    lblMode.Visible = false;
                    DataGrid.Style.Value = "display:none;";
                    ClientScript.RegisterStartupScript(this.GetType(), "script", "showPolicySuccess();", true);
                    //ClientScript.RegisterStartupScript(this.GetType(), "script", "onCancelClick();", true);
                     
                }
            }
            catch (FaultException<RMException> ee)
            {
                btnSave.Enabled = false;
                throw ee;


            }
            catch (Exception ee)
            {
                btnSave.Enabled = false;
                throw ee;
            }
            finally
            {
                if (objHelper != null)
                    objHelper = null;
                if (oDS != null)
                    oDS = null;
                if (oFileData != null)
                    oFileData = null;
                objRetunDoc = null;
            }

        }

        private void SaveData()
        {
            PolicyInterfaceBusinessHelper objHelper = new PolicyInterfaceBusinessHelper();
            DisplayFileData oFileData = null;
            DataSet oDS = new DataSet();

            SaveDownloadOptions oSaveDownloadOptions = null;
            DataRow objRow;

            XmlDocument objRetunDoc = null;
            bool bResult = false;
            //Hashtable objSelectedValues = null;
            string sSelectedValues = string.Empty;
            bool bSuccess = false;
            try
            {
                oSaveDownloadOptions = new SaveDownloadOptions();

                //objSelectedValues =(Hashtable) ViewState["SelectedValues"];

                foreach (string objEntry in txtSelectedValues.Text.Split(','))
                {
                    if (string.IsNullOrEmpty(sSelectedValues))
                    {
                        sSelectedValues = objEntry.Replace("'", "");
                    }
                    else
                    {
                        sSelectedValues = sSelectedValues + "," + objEntry.Replace("'", "");
                    }

                }
                oSaveDownloadOptions.AddEntityAs = txtAddEntityAs.Text;
                oSaveDownloadOptions.SelectedValue = sSelectedValues;
                oSaveDownloadOptions.Mode = txtMode.Text;
                oSaveDownloadOptions.PolicyId = Conversion.CastToType<int>(txtPolicyId.Text, out bSuccess);
                oSaveDownloadOptions.LossDate = txtLossDate.Text.Trim();//rupal:mits 33913
                bResult = objHelper.SaveOptions(ref oSaveDownloadOptions);

                if (bResult)
                {
                    // ClientScript.RegisterStartupScript(this.GetType(), "UpdateIdsScript", "PreserveDownloadedRecordsIds('" + txtMode.Text + "', '" + oSaveDownloadOptions.UpdatedIds + "');", true);
                    //   GetDownloadOptions();
                    lblMode.Visible = false;
                    //  div2.Style.Value = "display:none;";
                    DataGrid.Style.Value = "display:none;";
                   ClientScript.RegisterStartupScript(this.GetType(), "script", "showPolicySuccess();", true);
                    // ClientScript.RegisterStartupScript(this.GetType(), "script", "onCancelClick();", true);
                }
            }
            catch (FaultException<RMException> ee)
            {
                btnSave.Enabled = false;
                hdException.Value = "true";
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;

            }
            catch (Exception ee)
            {
                btnSave.Enabled = false;
                hdException.Value = "true";
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                if (objHelper != null)
                    objHelper = null;
                if (oDS != null)
                    oDS = null;
                if (oFileData != null)
                    oFileData = null;
                objRetunDoc = null;
            }
        }



        //private void RemoveFromDataSet()
        //{
        //    DataSet p_objSet = (DataSet)ViewState["DbRecord"];

        //    DataRow[] arrRow = null;

        //    //if (string.Equals(txtMode.Text, "entity", StringComparison.InvariantCultureIgnoreCase))
        //    arrRow = p_objSet.Tables[0].Select("TaxId =" + ViewState["ConflictingValue"]);

        //    foreach (DataRow objrow in arrRow)
        //    {
        //        p_objSet.Tables[0].Rows.Remove(objrow);
        //    }

        //    ViewState["DbRecord"] = p_objSet;

        //}

        //private void AddColumnsToGrid()
        //{
        //    Dictionary<string, string> objPropertyHeader = null;
        //    Dictionary<string, string> objEntityHeader = null;
        //    //Dictionary<string,string> objVehicleHeader = null;
        //    //Dictionary<string, string> objDriverHeader = null;
        //    Dictionary<string, string> objUnitHeader = null;

        //    BoundField bfGridColumn;
        //    int i = 1;



        //    // if (string.Equals(txtMode.Text, "ENTITY", StringComparison.InvariantCultureIgnoreCase))
        //    // {
        //    AddCheckboxColumnField();
        //    objEntityHeader = new Dictionary<string, string>();
        //    AddDataToHashTable(ref objEntityHeader);
        //    foreach (string skey in objEntityHeader.Keys)
        //    {
        //        bfGridColumn = new BoundField();
        //        bfGridColumn.DataField = skey;
        //        bfGridColumn.HeaderText = Conversion.ConvertObjToStr(objEntityHeader[skey]);
        //        bfGridColumn.Visible = true;
        //        //if (bSelectDuplicate)
        //        //    gvdConflictingData.Columns.Add(bfGridColumn);
        //        //else
        //        //gvDownloadItems.Columns.Add(bfGridColumn);
        //        lblMode.Text = "Download Coverages";
        //    }
        //    // }


        //}
        //protected void gvDownloadItemsData_OnRowBound(object sender, GridViewRowEventArgs e)
        //{
        //    //if (string.Equals(txtMode.Text, "entity", StringComparison.InvariantCultureIgnoreCase))
        //    //{
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        DataSet oDS = new DataSet();
        //        DropDownList gridDDDownloadItemAs = null;
        //        gridDDDownloadItemAs = (DropDownList)e.Row.Cells[1].FindControl("ddDownloadItemAs");
        //        oDS.ReadXml(new StringReader(ViewState["EntityTypelist"].ToString()));
        //        gridDDDownloadItemAs.DataSource = oDS.Tables[0];
        //        gridDDDownloadItemAs.DataValueField = "option_text";
        //        gridDDDownloadItemAs.DataTextField = "value";
        //        gridDDDownloadItemAs.DataBind();

        //    }


        //    //  }
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {


        //        HiddenField gridhdnSequenceNumber = (HiddenField)e.Row.Cells[0].FindControl("SequenceNumber");

        //        if (gridhdnSequenceNumber != null)
        //        {
        //            try
        //            {
        //                gridhdnSequenceNumber.Value = DataBinder.Eval(e.Row.DataItem, "SequenceNumber").ToString();
        //            }
        //            catch
        //            {
        //                gridhdnSequenceNumber.Value = "";
        //            }
        //        }
        //    }
        //}

        //private void AddCheckboxColumnField()
        //{
        //    TemplateField oTemplateField = new TemplateField();
        //    oTemplateField.ItemTemplate = new GridViewItemTemplate("cbDownloadItemsGrid", "SequenceNumber");

        //    //gvDownloadItems.Columns.Insert(0, oTemplateField);


        //}
    }
}