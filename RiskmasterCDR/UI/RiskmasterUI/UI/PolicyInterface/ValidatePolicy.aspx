﻿<%--**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            
 **********************************************************************************************
 * 05/05/2014 | 33574   | pgupta93   | Changes for Continuous Trigger and Policy Search functionality.
 **********************************************************************************************--%>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ValidatePolicy.aspx.cs"
    Inherits="Riskmaster.UI.UI.PolicyInterface.ValidatePolicy" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src="../../Scripts/MDIBase.js"></script>
    <%--MITS:33574--%>
    <script type="text/javascript">
        function AttachPolicy() {
            if (document.getElementById('hdnPolicySrchSys') != null && document.getElementById('hdnPolicySrchSys').value.toLowerCase() == "true") {
                window.close();    // RMA-14822 pmanoharan5
                window.opener.SetClaimPolicyList(document.getElementById("txtPolicyId").value, document.getElementById("txtPolicyNumber").value, document.getElementById("txtPolicySystemId").value, '', '', "1");
                //window.close();
            }
            else {
                window.close();      // RMA-14822 pmanoharan5
                window.opener.SetClaimPolicyList(document.getElementById("txtPolicyId").value, document.getElementById("txtPolicyNumber").value, document.getElementById("txtPolicySystemId").value, '', '');
                //window.close();
            }
            //window.close(); RMA-10609 msampathkuma
        }
        //MITS:33574 START
        function RedirectCliamPage(LoBParentcode, PolicyNumber, DateOfLoss, LOB, LOBID, ClaimstatusId, ClaimReportedDate) {
            window.close();
            if (LoBParentcode != '') {
                if (LoBParentcode == 'WL')
                    window.opener.parent.MDIShowScreenFromPolicy('-1', 'claimwc', document.getElementById("txtPolicyId").value, document.getElementById("txtPolicySystemId").value, PolicyNumber, DateOfLoss, LOB, LOBID, ClaimstatusId, ClaimReportedDate);
                else {
                    window.opener.parent.MDIShowScreenFromPolicy('-1', 'claimgc', document.getElementById("txtPolicyId").value, document.getElementById("txtPolicySystemId").value, PolicyNumber, DateOfLoss, LOB, LOBID, ClaimstatusId, ClaimReportedDate);
                }
            }
        }
        //MITS:33574 END
        function ShowClaimScreen() {

            if (document.getElementById('hdMDIClaimId').value != "" && document.getElementById('hdMDIClaimId').value != "0" && document.getElementById('hdnMDIEventId').value != "" && document.getElementById('hdnMDIEventId').value != "0")
                window.opener.parent.ShowExistingScreenByMDIId(unescape(document.getElementById('hdMDIClaimId').value), unescape(document.getElementById('hdnMDIEventId').value));
        }

        function ClearPolicyRelatedData() {
            var policyId = document.getElementById("txtPolicyId").value;
            var DownloadedVehicleIds;
            var DownloadedPropertyIds;
            var DownloadedEntitiesIds;
            if (policyId != "") {
                DownloadedVehicleIds = window.opener.document.getElementById("DownloadedVehicleIds");
                DownloadedPropertyIds = window.opener.document.getElementById("DownloadedPropertyIds");
                DownloadedEntitiesIds = window.opener.document.getElementById("DownloadedEntitiesIds");

                if (DownloadedVehicleIds != null) {
                    DownloadedVehicleIds.value = UpdateStoredIds(DownloadedVehicleIds.value, policyId);
                }

                if (DownloadedPropertyIds != null) {
                    DownloadedPropertyIds.value = UpdateStoredIds(DownloadedPropertyIds.value, policyId);
                }

                if (DownloadedEntitiesIds != null) {
                    DownloadedEntitiesIds.value = UpdateStoredIds(DownloadedEntitiesIds.value, policyId);
                }
            }
        }

        function UpdateStoredIds(strObjIds, policyId) {
            var strPolicyObjIds = "";
            var arrObjIds = strObjIds.split(";");

            for (var i = 0; i < arrObjIds.length; i++) {
                strPolicyObjIds = arrObjIds[i];
                if (strPolicyObjIds.substr(0, strPolicyObjIds.indexOf("|")) == policyId) {
                    strObjIds = strObjIds.replace((strPolicyObjIds + ";"), "");
                    strObjIds = strObjIds.replace((strPolicyObjIds), "");
                }
            }

            return strObjIds;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:label id="lblValidationFailed" runat="server" />
        <asp:textbox id="txtPolicyId" runat="server" style="display: none"></asp:textbox>
        <asp:textbox id="txtPolicyNumber" runat="server" style="display: none"></asp:textbox>
        <asp:textbox id="txtPolicySystemId" runat="server" style="display: none"></asp:textbox>
        <%-- <asp:HiddenField id="hdMDIClaimId"  value="" runat="server"/>
           <asp:HiddenField id="hdnMDIEventId"  value="" runat="server"/>--%>
        <%--Added by swati for WWIg gap 16 mits # 33414--%>
        <asp:hiddenfield id="hdnPolicySrchSys" value="" runat="server" />
        <asp:textbox id="hdnClaimReportedDate" runat="server" style="display: none"></asp:textbox>
    </div>
    </form>
</body>
</html>
