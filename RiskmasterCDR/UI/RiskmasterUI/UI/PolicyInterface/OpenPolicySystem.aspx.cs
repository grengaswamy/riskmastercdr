﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.AppHelpers;
using System.Xml;
using Riskmaster.UI.Shared.Controls;
using Riskmaster.Common;
using Riskmaster.UI.PolicyInterfaceService;
using System.ServiceModel;
using Riskmaster.BusinessHelpers;

namespace Riskmaster.UI.UI.PolicyInterface
{
    public partial class OpenPolicySystem : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PolicyInterfaceBusinessHelper oHelper;
            FetchPolicyDetails oRequest,oResponse;
            if (!IsPostBack)
            {
              if(Request.QueryString["PolicyID"] != null)
              {
                oHelper = new PolicyInterfaceBusinessHelper();
                oRequest = new FetchPolicyDetails();
                oRequest.PolicyID = Conversion.ConvertStrToInteger(AppHelper.GetQueryStringValue("PolicyID"));
                oResponse = oHelper.GetPolicyDetails(oRequest);
                if(oResponse != null)
                 {
                     Symbol.Text = oResponse.Symbol;
                     Module.Text = oResponse.Module;
                     PolNum.Text = oResponse.PolicyNumber;
                     LOC.Text = oResponse.LocationCompany;
                     MCO.Text = oResponse.MasterCompany;
                     srcLoc.Value = "RMA";
                     pntUser.Value = oResponse.PolSysUserName;
                     pntPwd.Value = oResponse.PolSysPassword;
                     //ajohari2 JIRA-5272: Start
                     if (oResponse.PointURL != null)
                         pntURL.Value = oResponse.PointURL.Trim();
                     //ajohari2 JIRA-5272: End
                     PolAction.Value = "IN";
                     //ajohari2 JIRA-5272: Start
                     if (oResponse.PointURL != null)
                         frmData.Action = oResponse.PointURL.Trim();
                     //ajohari2 JIRA-5272: End
                     frmData.Method = "POST";
                 }
              }
            }
        }
     }
}