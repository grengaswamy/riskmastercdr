﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdditionalData.aspx.cs" Inherits="Riskmaster.UI.UI.PolicyInterface.AdditionalData" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl" TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove"
xmlns:uc="remove" xmlns:dg="remove" xmlns:cul="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
<head runat="server">
    <title>Additional Data</title>
</head>
<body>
    <form id="form1" runat="server">
     <table>
        <tr>
            <td colspan="2">
                <uc1:errorcontrol id="ErrorControl1" runat="server" />
            </td>
        </tr>
    </table>
   <span runat="server" id="spData"></span>
    </form>
</body>
</html>
