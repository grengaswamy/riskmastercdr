﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Riskmaster.UI.UI.IntegralPolicyInterface {
    
    
    public partial class IntegralPolicySysDownloadResults {
        
        /// <summary>
        /// Head1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlHead Head1;
        
        /// <summary>
        /// frmData control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm frmData;
        
        /// <summary>
        /// ErrorControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Riskmaster.UI.Shared.Controls.ErrorControl ErrorControl;
        
        /// <summary>
        protected global::System.Web.UI.WebControls.ImageButton imgbtnSave;
        protected global::System.Web.UI.WebControls.ImageButton imgbtnInquire;
        protected global::System.Web.UI.WebControls.Label lblDateOfLoss;
        protected global::System.Web.UI.HtmlControls.HtmlSelect dlLossDate;
        protected global::System.Web.UI.HtmlControls.HtmlInputText txtLossDate;
        protected global::System.Web.UI.HtmlControls.HtmlInputButton btnLossDate;
        /// ScriptManager1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadScriptManager ScriptManager1;
        
        /// <summary>
        /// RadStyleSheetManager1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadStyleSheetManager RadStyleSheetManager1;
        
        /// <summary>
        /// radCodeBlock control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadCodeBlock radCodeBlock;
        
        /// <summary>
        /// RadAjaxManager1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadAjaxManager RadAjaxManager1;
        
        /// <summary>
        /// dvResults control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvResults;
        
        /// <summary>
        protected global::System.Web.UI.WebControls.Label lblSearchResults;
        /// ltNoResult control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ltNoResult;
        
        /// <summary>
        /// tblToolbar control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTable tblToolbar;
        
        /// <summary>
        /// imgSave control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton imgSave;
        
        /// <summary>
        /// imgEnquire control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton imgEnquire;
        
        /// <summary>
        /// gvPolicyList control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadGrid gvPolicyList;
        protected global::System.Web.UI.WebControls.HiddenField hdnpEffectiveDate;
        protected global::System.Web.UI.WebControls.HiddenField hdnpExpireDate;
        protected global::System.Web.UI.WebControls.HiddenField hdnClaimType;
        
        /// <summary>
        /// hdSelectedPolicyNumber control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hdSelectedPolicyNumber;
        
        /// <summary>
        /// hdSelectedModule control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hdSelectedModule;
        
        /// <summary>
        /// hdSelectedPolicySymbol control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hdSelectedPolicySymbol;
        
        /// <summary>
        /// hdnSystemName control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hdnSystemName;
        
        /// <summary>
        /// hdnClaimId control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hdnClaimId;
        
        /// <summary>
        /// hdnPolicySymbol control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hdnPolicySymbol;
        
        /// <summary>
        /// hdnPolicyLobCode control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hdnPolicyLobCode;
        
        /// <summary>
        /// hdnPolicyLobId control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hdnPolicyLobId;
        
        /// <summary>
        /// hdnPolicyLossDate control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hdnPolicyLossDate;
        
        /// <summary>
        /// PleaseWaitDialog1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Riskmaster.UI.Shared.Controls.PleaseWaitDialog PleaseWaitDialog1;
        
        /// <summary>
        /// hdnPolicySysName control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hdnPolicySysName;
        protected global::System.Web.UI.WebControls.HiddenField hdnPolicySrchSys;
        protected global::System.Web.UI.WebControls.HiddenField hdnIntegralClaimEventSetting;
        protected global::System.Web.UI.WebControls.HiddenField hdSelectedMasterCompany;
        protected global::System.Web.UI.WebControls.HiddenField hdSelectedLOB;
        protected global::System.Web.UI.WebControls.HiddenField hfInsurerNm;
        protected global::System.Web.UI.WebControls.HiddenField hdnTaxId;
        protected global::System.Web.UI.WebControls.HiddenField hdnClientSeqNo;
        protected global::System.Web.UI.WebControls.HiddenField hdnAddressSeqNo;
        protected global::System.Web.UI.WebControls.HiddenField hdnBirthDt;
        protected global::System.Web.UI.WebControls.HiddenField hfInsurerAddr1;
        protected global::System.Web.UI.WebControls.HiddenField hfInsurerCity;
        protected global::System.Web.UI.WebControls.HiddenField hfInsurerPostalCd;
        protected global::System.Web.UI.WebControls.HiddenField hdnNameType;
        protected global::System.Web.UI.WebControls.HiddenField hdnPolicyLocation;
        protected global::System.Web.UI.WebControls.HiddenField hdStagingpolicyINode;
        protected global::System.Web.UI.WebControls.HiddenField hdnrow;
        protected global::System.Web.UI.WebControls.HiddenField hdnPolicyStatus;
    }
}
