﻿using System;
using System.Web.UI;
using Riskmaster.UI.PolicyInterfaceService;
using Riskmaster.BusinessHelpers;
using System.ServiceModel;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.Linq;
using Riskmaster.Common;
using System.Xml.XPath;
using System.Linq;
using System.Xml;
using System.Configuration;

namespace Riskmaster.UI.UI.PolicyInterface
{
    public partial class PS_PolicyCoverageDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PolicyEnquiry oRequest, oResponse;
            PolicyInterfaceBusinessHelper oHelper;
            string sValues;
            string[] sCollection;
            bool bInSuccess;
            if (!Page.IsPostBack)
            {
                try
                {
                    //tanwar2 - Policy Download from staging - start
                    if (ConfigurationManager.AppSettings["RemoveEmptyFieldsOnPolicyEnquiry"] != null)
                    {
                        hdnRemoveEmtpyFields.Value = ConfigurationManager.AppSettings["RemoveEmptyFieldsOnPolicyEnquiry"];
                    }
                    //tanwar2 - Policy Download from staging - end

                    sValues = AppHelper.GetQueryStringValue("val");
                    sCollection = sValues.Split('|');
                    oRequest = new PolicyEnquiry();

                   

                    oRequest.PolicyNumber = sCollection[0];
                    oRequest.PolicySymbol = sCollection[1];
                    oRequest.LOB = sCollection[2];
                    oRequest.IssueCode = sCollection[3];
                    oRequest.Location = sCollection[4];
                    oRequest.MasterCompany = sCollection[5];
                    oRequest.Module = sCollection[6];
                    oRequest.InsLine = sCollection[7];  // unit -- currently from cov list, check
                    oRequest.UnitNumber = sCollection[8]; // fm unit
                    oRequest.Product = sCollection[9];// fm unit
                    oRequest.UnitRiskLocation = sCollection[10]; // fm cov list
                    oRequest.UnitSubRiskLocation = sCollection[11]; // fm cov list
                    oRequest.UnitState = sCollection[12]; // fm cov list
                    oRequest.CovCode = sCollection[13]; // fm cov list
                    oRequest.CovSeqNo = sCollection[14]; // fm cov list
                    oRequest.TransSeq = sCollection[15];  // // fm cov list
                    oRequest.CovStatus = sCollection[16]; // fm cov list
                    oRequest.PolicyIdentfier = sCollection[0];
                    oRequest.PolicySystemId = Conversion.CastToType<int>(sCollection[17], out bInSuccess); ;
                    oRequest.PolCompany = sCollection[18];
                    //  oRequest.BaseLOBLine = sCollection[18];
                    //skhare7 Policy Interface
                    //setting hidden fields
                   // hfUnitNumber.Value = sCollection[8];
                   //hfBuilding.Value = sCollection[11];
                   // hfLocation.Value = sCollection[10];

                    hfStatUnitNum.Value = sCollection[19];
                    hdBaseLob.Value = sCollection[20];
                    if (hdBaseLob.Value != "PL")
                    {
                        lblLimitCovA.Visible = false;
                        lblLimitCovB.Visible = false;
                        lblLimitCovC.Visible = false;
                        lblLimitCovD.Visible = false;
                        lblLimitCovE.Visible = false;
                        lblLimitCovF.Visible = false;
                    }
                    if(hdBaseLob.Value != "WL")
                    {
                        lblWcDedAmt.Visible = false;
                        lblWcDedAggr.Visible = false;
                    }
                    

                    oHelper = new PolicyInterfaceBusinessHelper();
                    oResponse = oHelper.GetCoverageDetail(oRequest);
                    //hfInsLine.Value = oResponse.InsLine;
                    if (!string.IsNullOrEmpty(oResponse.ResponseAcordXML))
                    {
                        PopulateCoverageDetail(oResponse.ResponseAcordXML);
                        
                    }
                }
                catch (FaultException<RMException> ee)
                {
                    ErrorControl1.Visible = true;
                    ErrorHelper.logErrors(ee);
                    ErrorControl1.errorDom = ee.Detail.Errors;
                }
                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                }
            }
        }

        //protected void PopulateDetails(string pXML)
        //{
            //if (pIssueCd == "M")
            //{
            //if (pXML != null)
            //    PopulateCoverageDetail(pXML);
            //}
            //else
            //{
            //    switch (pLOB.Trim())
            //    {
            //        //case "ACV":
            //        case "AL": PopulateVehicle(pXML); break;

            //        // case "HP":
            //        //  case "MHO":
            //        case "PL": PopulateProperty(pXML); break;

            //        // case "BOP":
            //        case "CL": PopulateCPP(pXML); break;

            //        // case "WC":
            //        //  case "WCA":
            //        case "WL": PopulateWorker(pXML); break;
            //    }
            //}

        //}
        //protected void PopulateProperty(string pXML)
        //{
        //    dvProp.Visible = true;
        //    XElement oDoc, oElement;

        //    oDoc = XElement.Parse(pXML);

        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/com.csc_RecordStatus");
        //    lblPropStatus.Text = oElement.Value;

        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/com.csc_RateBook");
        //    lblPropRatebook.Text = oElement.Value;

        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/Coverage/CoverageCd");
        //    lblPropCovCd.Text = oElement.Value;

        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/Coverage/CoverageDesc");
        //    lblPropCovDesc.Text = oElement.Value;

        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_CoverageSeq']/OtherId");
        //    lblPropSeq.Text = oElement.Value;

        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/Coverage/CurrentTermAmt");
        //    lblPropPrem.Text = oElement.Value;

        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNum']/OtherId");
        //    lblPropUnitNum.Text = oElement.Value;

        //    //unit desc
        //}
        //protected void PopulateVehicle(string pXML)
        //{
        //    dvVehicle.Visible = true;
        //    XElement oDoc, oElement;
        //    string sTemp = string.Empty;

        //    oDoc = XElement.Parse(pXML);

        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/com.csc_RecordStatus");
        //    lblVehStatus.Text = oElement.Value;

        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNum']/OtherId");
        //    lblVehUnitNum.Text = oElement.Value;

        //    //unit desc

        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/Coverage/CoverageCd");
        //    lblVehCovCd.Text = oElement.Value;

        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/Coverage/CoverageDes");
        //    lblVehCovDesc.Text = oElement.Value;

        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/Coverage/Limit/FormatCurrencyAmt/Amt");
        //    lblVehLimit.Text = oElement.Value;

        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/Coverage/Deductible/FormatCurrencyAmt/Amt");
        //    if (oElement.Value != string.Empty)
        //        sTemp = oElement.Value;
        //    else
        //        sTemp = "0.0";
        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/Coverage/com.csc_Exposure");
        //    if (oElement.Value != string.Empty)
        //        lblVehDeduc.Text = sTemp + "/" + oElement.Value;
        //    else
        //        lblVehDeduc.Text = sTemp + "/" + "0.0";

        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/com.csc_RateBook");
        //    lblVehRatebook.Text = oElement.Value;

        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/Coverage/CurrentTermAmt");
        //    lblVehPrem.Text = oElement.Value;
        //}
        //protected void PopulateCPP(string pXML)
        //{
        //    dvCPP.Visible = true;
        //    XElement oDoc, oElement;

        //    oDoc = XElement.Parse(pXML);

        //    lblCPPInsLn.Text = hfInsLine.Value;

        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Product']/OtherId");
        //    lblCPPProdct.Text = oElement.Value;

        //    //location
        //    //building
        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/Coverage/CoverageCd");
        //    lblCPPCov.Text = oElement.Value;

        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_CoverageSeq']/OtherId");
        //    lblCPPSeq.Text = oElement.Value;

        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/com.csc_RateBook");
        //    lblCPPRatebook.Text = oElement.Value;

        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/Coverage/CurrentTermAmt");
        //    lblCPPPrem.Text = oElement.Value;

        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RateState']/OtherId");
        //    lblCPPState.Text = oElement.Value;

        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/com.csc_RecordStatus");
        //    lblCPPStatus.Text = oElement.Value;

        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/com.csc_ClassCd");
        //    lblCPPClassCd.Text = oElement.Value;

        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/com.csc_ClassDesc");
        //    lblCPPClassDesc.Text = oElement.Value;

        //    //oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/Coverage/Limit/FormatCurrencyAmt/Amt");
        //    //lblCPPLimit.Text = oElement.Value;

        //    //blanket type

        //    //oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/Coverage/Deductible/FormatCurrencyAmt/Amt");
        //    //lblCPPdeduc.Text = oElement.Value;

        //    //coinsurance is percent of coinsurance
        //    //cause of loss is bureau cause of loss
        //    //theft exclusion
        //    //valuation
        //    //agreed value
        //    //inflation guard
        //    //earthquake cov
        //}
        //protected void PopulateWorker(string pXML)
        //{
        //    dvWCV.Visible = true;
        //    XElement oDoc, oElement;

        //    oDoc = XElement.Parse(pXML);

        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_SiteName']/OtherId");
        //    lblSiteNm.Text = oElement.Value;

        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNum']/OtherId");
        //    lblSiteNum.Text = oElement.Value;

        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/com.csc_RecordStatus");
        //    lblWCVStatus.Text = oElement.Value;

        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/Coverage/CoverageCd");
        //    lblClassCd.Text = oElement.Value;

        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/Coverage/CoverageDesc");
        //    lblClassDesc.Text = oElement.Value;

        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_ClassSeq']/OtherId");
        //    lblClassSeq.Text = oElement.Value;

        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/com.csc_VoluntaryCompInd");
        //    lblVoluntary.Text = oElement.Value;

        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/Coverage/com.csc_EstimatedPayroll");
        //    lblEstdPayroll.Text = oElement.Value;

        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/Coverage/CurrentTermAmt");
        //    lblManualPrem.Text = oElement.Value;

        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/com.csc_RateOverideInd");
        //    lblRate.Text = oElement.Value;

        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/com.csc_SplitRateOverideInd");
        //    lblSplitRate.Text = oElement.Value;

        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/com.csc_Rate");
        //    lblWCVRate.Text = oElement.Value;

        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/com.csc_SplitRate");
        //    lblWCVSplitRate.Text = oElement.Value;

        //    oElement = oDoc.XPathSelectElement("ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo/Coverage/com.csc_ExpCovCd");
        //    lblWCVCovCd.Text = oElement.Value;

        //}
        protected void PopulateCoverageDetail(string pXML)
        {
            XElement oDoc, oElement;
            oDoc = XElement.Parse(pXML);

            //oElement = oDoc.XPathSelectElement("//ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNum']/OtherId");
            //lblMRUnitNum.Text = oElement.Value;
            lblMRUnitNum.Text = hfStatUnitNum.Value;

            oElement = oDoc.XPathSelectElement("//CoverageCd");
            lblMRCovCd.Text = oElement.Value;

            oElement = oDoc.XPathSelectElement("//CoverageDesc");
            lblMRCovDesc.Text = oElement.Value;

            oElement = oDoc.XPathSelectElement("//com.csc_RecordStatus");
            lblMRCovStatus.Text = oElement.Value;

            oElement = oDoc.XPathSelectElement("//com.csc_Subline");
            lblMRSublineCd.Text = oElement.Value;

            oElement = oDoc.XPathSelectElement("//com.csc_SublineDesc");
            lblMRSubline.Text = oElement.Value;

            oElement = oDoc.XPathSelectElement("//com.csc_ClassCd");
            lblMRClassCd.Text = oElement.Value;

            oElement = oDoc.XPathSelectElement("//com.csc_ClassDesc");
            lblMRClassDesc.Text = oElement.Value;

            oElement = oDoc.XPathSelectElement("//ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId");
            lblMRInsLine.Text = oElement.Value;

            oElement = oDoc.XPathSelectElement("//ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RateState']/OtherId");
            lblMRState.Text = oElement.Value;

            oElement = oDoc.XPathSelectElement("//ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_CoverageSeq']/OtherId");
            lblMRCovSeqNo.Text = oElement.Value;

            oElement = oDoc.XPathSelectElement("//ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_TrxnSeq']/OtherId");
            lblMRTransSeqNo.Text = oElement.Value;
            
            oElement = oDoc.XPathSelectElement("//EffectiveDt");
            lblMRCovEffDt.Text = oElement.Value;

            oElement = oDoc.XPathSelectElement("//ExpirationDt");
            lblMRCovExpDt.Text = oElement.Value;

            oElement = oDoc.XPathSelectElement("//com.csc_ExtendDt");
            lblMRExtendDt.Text = oElement.Value;

            oElement = oDoc.XPathSelectElement("//com.csc_TrxnDt");
            lblMRTranDt.Text = oElement.Value;

            oElement = oDoc.XPathSelectElement("//com.csc_EntryDt");
            lblMREntryDt.Text = oElement.Value;

            oElement = oDoc.XPathSelectElement("//com.csc_RetroDt");
            lblMRRetroDt.Text = oElement.Value;

            oElement = oDoc.XPathSelectElement("//com.csc_AcctDt");
            lblMRAcctngDt.Text = oElement.Value;

            oElement = oDoc.XPathSelectElement("//ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Product']/OtherId");
            lblMRProdLn.Text = oElement.Value;

            oElement = oDoc.XPathSelectElement("//com.csc_Exposure");
            lblMRExposure.Text = oElement.Value;

            oElement = oDoc.XPathSelectElement("//Limit[LimitAppliesToCd='OccurenceLimit']/FormatCurrencyAmt/Amt");
            lblMROccurence.Text = oElement.Value;

            oElement = oDoc.XPathSelectElement("//Limit[LimitAppliesToCd='AggregateLimit']/FormatCurrencyAmt/Amt");
            lblMRAggregate.Text = oElement.Value;

            oElement = oDoc.XPathSelectElement("//Deductible/FormatCurrencyAmt/Amt");
            lblMRDeductible.Text = oElement.Value;

            oElement = oDoc.XPathSelectElement("//com.csc_OriginalPremium");
            lblMROrigPrem.Text = oElement.Value;

            oElement = oDoc.XPathSelectElement("//com.csc_WrittenPremium");
            lblMRWrittenPrem.Text = oElement.Value;

            oElement = oDoc.XPathSelectElement("//CurrentTermAmt");
            lblMRFullTermPrem.Text = oElement.Value;

            oElement = oDoc.XPathSelectElement("//com.csc_TotalPremium");
            lblMRTotalPrem.Text = oElement.Value;

            oElement = oDoc.XPathSelectElement("//com.csc_ProductLine");
            lblProductLine.Text = oElement.Value;

            oElement = oDoc.XPathSelectElement("//com.csc_AnnualStatement");
            lblAnnualStmt.Text = oElement.Value;

            if (hdBaseLob.Value == "PL")
            {
                oElement = oDoc.XPathSelectElement("//Limit[LimitAppliesToCd='LIMIT-COVA']/FormatCurrencyAmt/Amt");
                if (oElement != null)
                    txtLimitCovA.Text = oElement.Value;

                oElement = oDoc.XPathSelectElement("//Limit[LimitAppliesToCd='LIMIT-COVB']/FormatCurrencyAmt/Amt");
                if (oElement != null)
                    txtLimitCovB.Text = oElement.Value;

                oElement = oDoc.XPathSelectElement("//Limit[LimitAppliesToCd='LIMIT-COVC']/FormatCurrencyAmt/Amt");
                if (oElement != null)
                    txtLimitCovC.Text = oElement.Value;

                oElement = oDoc.XPathSelectElement("//Limit[LimitAppliesToCd='LIMIT-COVD']/FormatCurrencyAmt/Amt");
                if (oElement != null)
                    txtLimitCovD.Text = oElement.Value;

                oElement = oDoc.XPathSelectElement("//Limit[LimitAppliesToCd='LIMIT-COVE']/FormatCurrencyAmt/Amt");
                if (oElement != null)
                    txtLimitCovE.Text = oElement.Value;

                oElement = oDoc.XPathSelectElement("//Limit[LimitAppliesToCd='LIMIT-COVF']/FormatCurrencyAmt/Amt");
                if (oElement != null)
                    txtLimitCovF.Text = oElement.Value;
            }
            if (hdBaseLob.Value == "WL")
            {
                oElement = oDoc.XPathSelectElement("//Coverage/ExcessWorkCompDeductible/FormatCurrencyAmt/Amt");
                if (oElement != null)
                    txtWcDedAmt.Text = oElement.Value;

                oElement = oDoc.XPathSelectElement("//Coverage/ExcessWorkCompDeductible/com.csc_AggrAmt");
                if (oElement != null)
                    txtWcDedAggr.Text = oElement.Value;
            }

        }

        private void PopulateStagingCoverageDetail(string p_sXml)
        {
            XmlDocument xDoc = null;
            XmlNode xNode = null;

            try
            {
                xDoc = new XmlDocument();
                xDoc.LoadXml(p_sXml);

                xNode = xDoc.SelectSingleNode("//CoverageCd");
                if (xNode != null)
                {
                    lblMRCovCd.Text = xNode.InnerText;
                }
                xNode = xDoc.SelectSingleNode("//CvgDescription");
                if (xNode != null)
                {
                    lblMRCovDesc.Text = xNode.InnerText;
                }
                xNode = xDoc.SelectSingleNode("//csc_CoverageSeq");
                if (xNode != null)
                {
                    lblMRCovSeqNo.Text = xNode.InnerText;
                }
                xNode = xDoc.SelectSingleNode("//csc_TrxnSeq");
                if (xNode != null)
                {
                    lblMRTransSeqNo.Text = xNode.InnerText;
                }
                xNode = xDoc.SelectSingleNode("//EffectiveDt");
                if (xNode != null)
                {
                    lblMRCovEffDt.Text = xNode.InnerText;
                }
                xNode = xDoc.SelectSingleNode("//ExpirationDt");
                if (xNode != null)
                {
                    lblMRCovExpDt.Text = xNode.InnerText;
                }
                xNode = xDoc.SelectSingleNode("//Limit");
                if (xNode != null)
                {
                    lblMRAggregate.Text = xNode.InnerText;
                }
                xNode = xDoc.SelectSingleNode("//csc_OriginalPremium");
                if (xNode != null)
                {
                    lblMROrigPrem.Text = xNode.InnerText;
                }
                xNode = xDoc.SelectSingleNode("//csc_WrittenPremium");
                if (xNode != null)
                {
                    lblMRWrittenPrem.Text = xNode.InnerText;
                }
                xNode = xDoc.SelectSingleNode("//csc_TotalPremium");
                if (xNode != null)
                {
                    lblMRTotalPrem.Text = xNode.InnerText;
                }
                xNode = xDoc.SelectSingleNode("//csc_FullTermPremium");
                if (xNode != null)
                {
                    lblMRFullTermPrem.Text = xNode.InnerText;
                }
                xNode = xDoc.SelectSingleNode("//csc_Exposure");
                if (xNode != null)
                {
                    lblMRExposure.Text = xNode.InnerText;
                }
                xNode = xDoc.SelectSingleNode("//csc_OccurenceLimit");
                if (xNode != null)
                {
                    lblMROccurence.Text = xNode.InnerText;
                }
                xNode = xDoc.SelectSingleNode("//csc_Deductible");
                if (xNode != null)
                {
                    lblMRDeductible.Text = xNode.InnerText;
                }
                xNode = xDoc.SelectSingleNode("//InsLine");
                if (xNode != null)
                {
                    lblMRInsLine.Text = xNode.InnerText;
                }
                xNode = xDoc.SelectSingleNode("//ProdLine");
                if (xNode != null)
                {
                    lblProductLine.Text = xNode.InnerText;
                }
                xNode = xDoc.SelectSingleNode("//StatUnitNumber");
                if (xNode != null)
                {
                    lblMRUnitNum.Text = xNode.InnerText;
                }
                xNode = xDoc.SelectSingleNode("//CovStatus");
                if (xNode != null)
                {
                    lblMRCovStatus.Text = xNode.InnerText;
                }
                xNode = xDoc.SelectSingleNode("//csc_Subline");
                if (xNode != null)
                {
                    lblMRSublineCd.Text = xNode.Value;
                }
                xNode = xDoc.SelectSingleNode("//csc_SublineDesc");
                if (xNode != null)
                {
                    lblMRSubline.Text = xNode.Value;
                }
                xNode = xDoc.SelectSingleNode("//csc_ClassCd");
                if (xNode != null)
                {
                    lblMRClassCd.Text = xNode.Value;
                }
                xNode = xDoc.SelectSingleNode("//csc_ClassDesc");
                if (xNode != null)
                {
                    lblMRClassDesc.Text = xNode.Value;
                }
                xNode = xDoc.SelectSingleNode("//State");
                if (xNode != null)
                {
                    lblMRState.Text = xNode.Value;
                }
                xNode = xDoc.SelectSingleNode("//ExtendDt");
                if (xNode != null)
                    lblMRExtendDt.Text = xNode.Value;

                xNode = xDoc.SelectSingleNode("//TrxnDt");
                if (xNode != null)
                    lblMRTranDt.Text = xNode.Value;

                xNode = xDoc.SelectSingleNode("//EntryDt");
                if (xNode != null)
                    lblMREntryDt.Text = xNode.Value;

                xNode = xDoc.SelectSingleNode("//RetroDt");
                if (xNode != null)
                    lblMRRetroDt.Text = xNode.Value;

                xNode = xDoc.SelectSingleNode("//AcctDt");
                if (xNode != null)
                    lblMRAcctngDt.Text = xNode.Value;

                xNode = xDoc.SelectSingleNode("//MRProdLine");
                if (xNode != null)
                    lblMRProdLn.Text = xNode.Value;

                xNode = xDoc.SelectSingleNode("//AnnualStatement");
                if (xNode != null)
                    lblAnnualStmt.Text = xNode.Value;

                xNode = xDoc.SelectSingleNode("//LimitCovA");
                if (xNode != null)
                    txtLimitCovA.Text = xNode.Value;

                xNode = xDoc.SelectSingleNode("//LimitCovB");
                if (xNode != null)
                    txtLimitCovB.Text = xNode.Value;

                xNode = xDoc.SelectSingleNode("//LimitCovC");
                if (xNode != null)
                    txtLimitCovC.Text = xNode.Value;

                xNode = xDoc.SelectSingleNode("//LimitCovD");
                if (xNode != null)
                    txtLimitCovD.Text = xNode.Value;

                xNode = xDoc.SelectSingleNode("//LimitCovE");
                if (xNode != null)
                    txtLimitCovE.Text = xNode.Value;

                xNode = xDoc.SelectSingleNode("//LimitCovF");
                if (xNode != null)
                    txtLimitCovF.Text = xNode.Value;

                xNode = xDoc.SelectSingleNode("//DedAmt");
                if (xNode != null)
                    txtWcDedAmt.Text = xNode.Value;

                xNode = xDoc.SelectSingleNode("//WcDedAggr");
                if (xNode != null)
                    txtWcDedAggr.Text = xNode.Value;
            }
            catch(Exception ex)
            {
            }
        }
    }
}