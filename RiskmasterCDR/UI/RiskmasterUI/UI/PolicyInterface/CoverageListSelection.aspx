﻿<%--**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            
 **********************************************************************************************
 * 05/05/2014 | 33574   | pgupta93   | Changes for Continuous Trigger and Policy Search functionality.
 **********************************************************************************************--%>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CoverageListSelection.aspx.cs" Inherits="Riskmaster.UI.UI.PolicyInterface.CoverageListSelection" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
   <%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog" TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Policy Download Options</title>
    <script language="javaScript" src="../../Scripts/form.js" type="text/javascript"></script>
     <script language="JavaScript" src="../../Scripts/WaitDialog.js" type="text/javascript">         { var i; }
    </script>
    <script type="text/JavaScript" src="../../../Scripts/Utilities.js"></script>
    <script type="text/javascript">
        function ValidateSelection() {
            var sSelectedValue = "";
            var ctrlCb, ctrlHf, iLength;
            var addEntityAs = "";
            var ctrlList = document.getElementById("dlTest");
            var Ct = ctrlList.children.length;
            var iHfValue;
            for (var rIndex = 0; rIndex < Ct; rIndex++) {
                iLength = ctrlList.children[rIndex].children.length;
                if (iLength == 8) {
                    ctrlCb = ctrlList.children[rIndex].children[2];
                    ctrlHf = ctrlList.children[rIndex].children[3];
                }
                else if (iLength == 7) {
                    ctrlCb = ctrlList.children[rIndex].children[1];
                    ctrlHf = ctrlList.children[rIndex].children[2];
                }
                if (ctrlCb.checked == true) {
                //iHfValue = ctrlHf.value;
                if (sSelectedValue == "")
                    sSelectedValue = "'" + ctrlHf.value + "'";
                        else
                            sSelectedValue = sSelectedValue + "," + "'" + ctrlHf.value + "'";
                }
                               
            }
//            var gvDownloadItems = document.getElementById("gvDownloadItems");
//            var rCount = gvDownloadItems.rows.length;
//            var sSelectedValue = "";
//            var addEntityAs = "";

//            for (var rowIdx = 0; rowIdx < rCount; rowIdx++) {
//                var rowElement = gvDownloadItems.rows[rowIdx];
//                var chkBox = rowElement.cells[0].firstChild;
//                if (chkBox.checked) {
//                    var uniqueId;
//                    if (document.getElementById("txtMode").value == "entity" || document.getElementById("txtMode").value == "unitinterest") {


//                        uniqueId = rowElement.cells[0].children[1];
//                        if (sSelectedValue == "")
//                            sSelectedValue = "'" + uniqueId.value + "'";
//                        else
//                            sSelectedValue = sSelectedValue + "," + "'" + uniqueId.value + "'";

//                        if (addEntityAs == "")
//                            addEntityAs = rowElement.cells[1].firstChild.value;
//                        else
//                            addEntityAs = addEntityAs + "," + rowElement.cells[1].firstChild.value;
//                    }
//                    else {
//                        uniqueId = rowElement.cells[0].children[1];
//                        if (sSelectedValue == "")
//                            sSelectedValue = "'" + uniqueId.value + "'";
//                        else
//                            sSelectedValue = sSelectedValue + "," + "'" + uniqueId.value + "'";
//                    }
//                }

                //            }
            pleaseWait.Show();
            if (sSelectedValue == "") {
                alert("Please select atleast one coverage");

                return false;
            }
            else {
                document.getElementById("txtSelectedValues").value = sSelectedValue;
                document.getElementById("txtAddEntityAs").value = addEntityAs;
            }
            isClose = true;
            return true;
         
        }
        function getSelectedRow(obj) {
            document.getElementById("txtUpdateWith").value = obj.value;

        }
        function onUpdateClick() {
            if (document.getElementById("txtUpdateWith").value == "") {
                alert("Select any record to update");
                return false;
            }
            return true;
        }

        var isClose = false;
        var isSuccess = false;
        //this code will handle the F5 or Ctrl+F5 key Collapse | Copy Code//need to handle more cases like ctrl+R whose codes are not listed here
        document.onkeydown = checkKeycode;
        function checkKeycode(e) {
            var keycode;
            if (window.event)
                keycode = window.event.keyCode;
            else if (e)
                keycode = e.which;
            if (keycode == 116) {
                isClose = true;
            }
            if (event.altKey && keycode == 115) {
                isClose = false;
            }
        }

        function mouseclicked() {

            isClose = true;
        }
        function doUnload() {
            var isException = document.getElementById("hdException");
            if (isException != null && isException.value == "") {
                if (!isClose && !isSuccess) {
                    showPolicySuccess();
                    //window.opener.OpenPolicyPages(document.getElementById("txtmode").value, document.getElementById("txtPolicyId").value);
                }
            }
        }

        function showPolicySuccess() {
            
            
//            window.opener.OpenPolicyPages('CoverageList', document.getElementById("txtPolicyId").value);
//            window.close();

            //alert("Policy Data successfully downloaded"); msampathkuma RMA-10609
            //        if (document.getElementById("hdMDIClaimId").value != "" && document.getElementById("hdMDIClaimId").value != "0" && document.getElementById("hdnMDIEventId").value != "" && document.getElementById("hdnMDIEventId").value != "0") {
            //            window.opener.parent.closeScreen(window.opener.parent.document.getElementById('selectedScreenId').value, false);
            //            window.opener.ValidatePolicy(document.getElementById("txtPolicyId").value, document.getElementById("hdMDIClaimId").value, document.getElementById("hdnMDIEventId").value);
            //        }
                     try {
                            isSuccess = true;
                         //MITS:33574 START
                         //window.opener.ValidatePolicy(document.getElementById("txtPolicyId").value);
                            if (document.getElementById("txtPolicyFlag").value != '')
                                window.opener.ValidatePolicyWithoutClaim(document.getElementById("txtPolicyId").value, document.getElementById("txtLoBCode").value, document.getElementById("hdnClaimReportedDate").value);
                            else
                                window.opener.ValidatePolicy(document.getElementById("txtPolicyId").value);
                         //MITS:33574 END
                        }
                        catch (e) { };

                        window.close();
                        return false;
        }

        function onCancelClick() {

            isClose = false;
            window.close();
        }

        //      function PreserveDownloadedRecordsIds(mode, sUpdatedIds) {
        //          var policyId = document.getElementById("txtPolicyId").value;
        //          if (policyId != "") {
        //              sUpdatedIds = policyId + "|" + sUpdatedIds;
        //              if (mode == 'Unit') {
        //                  var DownloadedUnitIds = window.opener.document.getElementById("DownloadedUnitIds");
        //                  if (DownloadedUnitIds != null) {
        //                      if (DownloadedUnitIds.value == "0" || DownloadedUnitIds.value == "")
        //                          DownloadedUnitIds.value = sUpdatedIds;
        //                      else
        //                          DownloadedUnitIds.value = DownloadedUnitIds.value + ";" + sUpdatedIds;
        //                  }
        //              }
        ////              else if (mode == 'property') {
        ////                  var DownloadedPropertyIds = window.opener.document.getElementById("DownloadedPropertyIds");
        ////                  if (DownloadedPropertyIds != null) {
        ////                      if (DownloadedPropertyIds.value == "0" || DownloadedPropertyIds.value == "")
        ////                          DownloadedPropertyIds.value = sUpdatedIds;
        ////                      else
        ////                          DownloadedPropertyIds.value = DownloadedPropertyIds.value + ";" + sUpdatedIds;
        ////                  }
        ////              }
        //              else if (mode == 'entity') {
        //                  var downloadedEntitiesIds = window.opener.document.getElementById("DownloadedEntitiesIds");
        //                  if (downloadedEntitiesIds != null) {
        //                      if (downloadedEntitiesIds.value == "0" || downloadedEntitiesIds.value == "")
        //                          downloadedEntitiesIds.value = sUpdatedIds;
        //                      else
        //                          downloadedEntitiesIds.value = downloadedEntitiesIds.value + ";" + sUpdatedIds;
        //                  }
        //              }

        //              else if (mode == 'driver') {
        //                  var downloadedEntitiesIds = window.opener.document.getElementById("DownloadedDriverIds");
        //                  if (downloadedEntitiesIds != null) {
        //                      if (downloadedEntitiesIds.value == "0" || downloadedEntitiesIds.value == "")
        //                          downloadedEntitiesIds.value = sUpdatedIds;
        //                      else
        //                          downloadedEntitiesIds.value = downloadedEntitiesIds.value + ";" + sUpdatedIds;
        //                  }
        //              }

        ////               var PolicyDownloaded = window.opener.document.getElementById("PolicyDownloaded");
        ////               if (PolicyDownloaded != null) {
        ////                   PolicyDownloaded.value = "true";
        //               }
        //          }
        //      }
    </script>
</head>
<body  onbeforeunload="doUnload();"  onmousedown="mouseclicked()">
    <form id="frmData" name="frmData" method="post" runat="server">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <div class="msgheader" id="formtitle">
        <asp:Label ID="lblMode" runat="server" /></div>
    <div id="DataGrid" runat="server">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td colspan="4">
                    <table>
                        <%--<tr>
                            <td width="80%">
                                <div>
                                    <asp:GridView ID="gvDownloadItems" runat="server" AutoGenerateColumns="false" Font-Names="Tahoma"
                                        Font-Size="Smaller" CssClass="singleborder" Width="98%" Height="" CellPadding="4"
                                        ForeColor="#333333" GridLines="None" HorizontalAlign="Left"  OnRowDataBound="gvDownloadItemsData_OnRowBound" >
                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                        <Columns>
                                       
                                             <asp:TemplateField HeaderText="Add As">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="ddDownloadItemAs" name="ddDownloadItemAs" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#D1DDF1"  Font-Bold="True" ForeColor="#333333" />
                                        <HeaderStyle CssClass="colheader3" />
                                        <EditRowStyle BackColor="#2461BF" />
                                        <AlternatingRowStyle BackColor="White" />
                                    </asp:GridView>
                                </div>
                            </td>
                        </tr>--%>
                        <tr>
                            <td>
                                <asp:TextBox ID="hdnCbId" runat="server" Style="display: none" RMXType="id"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                        <td>
                         <asp:DataList ID="dlTest" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal" >
                               <ItemTemplate>
                               <%# fnTesting((string)DataBinder.Eval(Container.DataItem, "UnitDesc"))%>
                               <br />
                               <asp:CheckBox ID="cbSelect" runat="server"  /> &nbsp;
                               <asp:HiddenField runat="server" ID="hfValue"  Value='<%#Eval("SequenceNumber") %>' />
                               <%#Eval("CovCode")%> &nbsp;
                               <%#Eval("CovDesc")%> &nbsp;
                               <%#Eval("EffectiveDate")%> &nbsp;
                               <%#Eval("ExpirationDate")%> &nbsp;
                               <br style="line-height:2px" />
                               &nbsp;
                               <asp:Label runat="server" ID="lblLeft" Text="< " ></asp:Label>
                               <%#Eval("ClassData")%>
                               <asp:Label runat="server" ID="lblRight" Text=" >" ></asp:Label>
                               <br />
                               </ItemTemplate>
                        </asp:DataList>
                        </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td>
                    <asp:Button ID="btnSave" Text="Save" OnClientClick="return ValidateSelection();" 
                        runat="server" onclick="btnSave_Click" />
                </td>
                <td>
                    <input type="button" value="Cancel" id="btnCancel" onclick="onCancelClick();" />
                    <%-- onclick="onCancelClick()" />--%>
                </td>
            </tr>
        </table>
    </div>
 <%--    <div id="div2" runat="server" style="display:none;">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td colspan="4">
                    <table>
                    <tr>
                    <td>
                    <asp:Label ID="lblConflictingOption" runat="server" />
                    </td>
                    </tr>
                        <tr>
                            <td width="80%">
                                <div>
                                    <asp:GridView ID="gvdConflictingData" runat="server" AutoGenerateColumns="false" Font-Names="Tahoma"
                                        Font-Size="Smaller" CssClass="singleborder" Width="98%" Height="" CellPadding="4" 
                                        ForeColor="#333333" GridLines="None" HorizontalAlign="Left" >
                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <input id="cbDownloadItemsGrid" name="DownloadItemsGrid" type="radio" value='<%# Eval("RowId")%>' onclick="getSelectedRow(this)"/>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                           
                                        </Columns>
                                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                        <HeaderStyle CssClass="colheader3" />
                                        <EditRowStyle BackColor="#2461BF" />
                                        <AlternatingRowStyle BackColor="White" />
                                    </asp:GridView>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox1" runat="server" Style="display: none" RMXType="id"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td>
                    <asp:Button ID="btnAddNew" Text="Add New Record" runat="server" onclick="btnAddNew_Click" />
                </td>
                <td>
                   <asp:Button ID="btnUpdate" Text="Update Record"  runat="server" onclick="btnUpdate_Click" OnClientClick="return onUpdateClick()" />
                </td>
                <td>
                  <input type="button" value="Cancel" id="btnCancel1" onclick="onCancelClick()" />
                </td>
            </tr>
        </table>
    </div>--%>
    <asp:TextBox ID="txtMode" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSelectedValues" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtUpdateWith" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtPolicyId" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtAddEntityAs" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtLossDate" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtPolicyFlag" runat="server" Style="display: none"></asp:TextBox><%--MITS:33574--%>
    <asp:TextBox ID="txtLoBCode" runat="server" Style="display: none"></asp:TextBox><%--MITS:33574--%>
    <asp:HiddenField runat="server" ID="hdException" Value="" />
     <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
 <%--   <asp:HiddenField id="hdMDIClaimId"  value="" runat="server"/>
     <asp:HiddenField id="hdnMDIEventId"  value="" runat="server"/>--%>
        <asp:TextBox ID="hdnClaimReportedDate" runat="server" Style="display: none"></asp:TextBox>
    </form>
</body>
</html>