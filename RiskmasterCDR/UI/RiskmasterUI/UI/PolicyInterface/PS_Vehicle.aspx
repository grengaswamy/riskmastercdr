<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PS_Vehicle.aspx.cs" Inherits="Riskmaster.UI.UI.PolicyInterface.PS_Vehicle" ValidateRequest ="false" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/UserControlDataGrid.ascx" tagname="UserControlDataGrid" tagprefix="dg" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/SystemUsers.ascx" tagname="SystemUsers" tagprefix="cul" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove" xmlns:cul="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
  <head id="Head1" runat="server">
    <title>Vehicle</title>
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js">        { var i; }
</script>
    <script language="JavaScript" src="../../Scripts/drift.js">        { var i; }
</script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js">        { var i; }
  </script>

    <script type="text/javascript" src="../../Scripts/dhtml-div.js">        { var i; }
  </script>
    <script type="text/javascript" src="../../Scripts/dhtml-help-setup.js">        { var i; }
  </script>
    <%--<script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">        { var i; }
	</script>--%>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
  </head>
  <body class="10pt" >
    <form name="frmData" id="frmData" runat="server">
      <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />
     <%-- <asp:HiddenField runat="server" ID="wsrp_rewrite_action_1" value="" />--%>
      <asp:TextBox style="display:none" runat="server" name="hTabName" id="hTabName" />
      <asp:ScriptManager ID="SMgr" runat="server" />      
      <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" Text="Vehicle" />
        <asp:label id="formsubtitle" runat="server" Text="" />
      </div>
      <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" Text="" />
      </div>
      <div class="tabGroup" id="TabsDivGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSvehicleinfo" id="TABSvehicleinfo">
          <a class="Selected" HREF="#" runat="server" onclick="tabChange(this.name);return false;" RMXRef="" name="vehicleinfo" id="LINKTABSvehicleinfo">Vehicle Info</a>
        </div>
        <div  runat="server" id="TBSPvehicleinfo">
          <nbsp />
          <nbsp />
        </div>
       </div>
      <div width="100%"  runat="server"    name="FORMTABvehicleinfo" id="FORMTABvehicleinfo">
        <table width="100%" border="0" cellspacing="4" celpadding="4" padding="0">
          <tr>
            <td width="100%">
              <asp:hiddenfield runat="server" id="hdvehicleinfo" />
            </td>
          </tr>
          <tr>
            <td width="100%">
              <div runat="server" class="half" id="div_vin" xmlns="" width="100%">
                <asp:label runat="server" class="required" id="lbl_vin" Text="Vehicle ID" />
                <span class="formw">
                  <asp:label runat="server" id="vin" RMXRef="/Instance/Vehicle/Vin" RMXType="text" />
                </span>
              </div>
           </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_vehiclemake" xmlns="">
                <asp:label runat="server" class="required" id="lbl_vehiclemake" Text="Vehicle Make" />
                <span class="formw">
                  <asp:label runat="server" id="vehiclemake" RMXRef="/Instance/Vehicle/VehicleMake" RMXType="text" />
                </span>
              </div>
             
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_vehiclemodel" xmlns="">
                <asp:label runat="server" class="required" id="lbl_vehiclemodel" Text="Vehicle Model" />
                <span class="formw">
                  <asp:label runat="server" id="vehiclemodel" RMXRef="/Instance/Vehicle/VehicleModel" RMXType="text" />
                </span>
              </div>
           </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_vehicleyear" xmlns="">
                <asp:label runat="server" class="required" id="lbl_vehicleyear" Text="Vehicle Year" />
                <span class="formw">
                  <asp:label runat="server" id="vehicleyear" RMXRef="/Instance/Vehicle/VehicleYear" RMXType="numeric" />
                </span>
              </div>
             
            </td>
          </tr>
          <tr>
            <td>
            <div runat="server" class="completerow" id="div_unittype" xmlns="">
                <asp:label runat="server" class="required" id="lbl_unittype" Text="Unit Type" />
                <span class="formw">
                  <asp:label runat="server" id="unittype" RMXRef="/Instance/Vehicle/UnitType" RMXType="text" />
                </span>
              </div>
            </td>
          </tr>
        </table>
      </div>      
     

      <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    </form>
  </body>
</html>
