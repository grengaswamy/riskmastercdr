﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.Text;
using Riskmaster.BusinessHelpers;
using Riskmaster.UI.PolicyInterfaceService;
using Riskmaster.Common;
using System.ServiceModel;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.UI.PolicyInterface
{
    public partial class AdditionalData : System.Web.UI.Page
    {
        private string PageID = RMXResourceManager.RMXResourceProvider.PageId("AdditionalData.aspx");
        protected void Page_Load(object sender, EventArgs e)
        {
            PolicyInterfaceBusinessHelper oHelper;
            PolicyDownload oRequest, oResponse;
            string sTableNm,sRowID,sPolID;
            bool bInSuccess;
            
            oHelper = new PolicyInterfaceBusinessHelper();
            oRequest = new PolicyDownload();
            try
                {
                    oRequest.TableName = AppHelper.GetQueryStringValue("TableNm");
                    sRowID = AppHelper.GetQueryStringValue("RowID");
                    oRequest.TableRowID = Conversion.CastToType<int>(sRowID,out bInSuccess);
                    oRequest.UnitTypeCode = AppHelper.GetQueryStringValue("TypeCd");
                    oRequest.PolicyID = Conversion.CastToType<int>(AppHelper.GetQueryStringValue("PolID"),out bInSuccess);
                    oRequest.PolicySystemId = Conversion.CastToType<int>(AppHelper.GetQueryStringValue("PolicySystemId"), out bInSuccess);
                    sTableNm = oRequest.TableName;
           
                    oResponse = oHelper.GetDownLoadXMLData(oRequest);
           
                    if (oResponse.AcordResponse == null || oResponse.AcordResponse == string.Empty)
                    {
                        Label lblNoData = new Label();
                    //lblNoData.Text = "No Data to Display for this record";
                    lblNoData.Text = AppHelper.GetResourceValue(this.PageID, "lblNoDataToDiplay", "0");//vkumar258 - RMA-5361
                        spData.Controls.Add(lblNoData);
                        return;
                    }
                    if (oResponse.XSLTPath == null || oResponse.XSLTPath == string.Empty)
                    {
                        Label lblNoData = new Label();
                    //lblNoData.Text = "XSL Transformation file path not found";
                     lblNoData.Text = AppHelper.GetResourceValue(this.PageID, "lblXMLFilePathNotFound", "0");//vkumar258 - RMA-5361
                        spData.Controls.Add(lblNoData);
                        return;
                    }
                Transform(oResponse.AcordResponse, oResponse.XSLTPath, oResponse.PolicySystemType);  //bram4 - MITS:35932 - Integral Inetrface
                }
                catch (FaultException<RMException> ee)
                {
                    ErrorControl1.Visible = true;
                    ErrorHelper.logErrors(ee);
                    ErrorControl1.errorDom = ee.Detail.Errors;
                }
                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                }
            finally
            {
        }
        }
        private void Transform(string pInputXML, string pXSLTPath, string sPolicySystemType)
        {
            
            XmlDocument oDoc = new XmlDocument();
            XslTransform xslDoc;
            
            oDoc.LoadXml(pInputXML);
            if (CommonFunctions.GetPolicySystemTypeIndicator(sPolicySystemType.Trim()) == Constants.POLICY_SYSTEM_TYPE.POINT)
            {
            if (oDoc.SelectSingleNode("ACORD") == null)
            {
                Label lblNoData = new Label();
                    //lblNoData.Text = "No ACORD response found for this record.";
                    lblNoData.Text = AppHelper.GetResourceValue(this.PageID, "lblNoDataResponse", "0");
                spData.Controls.Add(lblNoData);
                return;
                }
            }
            xslDoc = new XslTransform();
            xslDoc.Load(pXSLTPath);
            xslDoc.Transform(oDoc, null, Response.Output);
        }
    }
}