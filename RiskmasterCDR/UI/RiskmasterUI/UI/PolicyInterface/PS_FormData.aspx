﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PS_FormData.aspx.cs" Inherits="Riskmaster.UI.UI.PolicyInterface.PS_FormData" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl" TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Form Data</title>
</head>

<body>
<form runat="server" id="frm1" method="post">
    <table>
        <tr>
            <td colspan="2">
                <uc1:errorcontrol id="ErrorControl1" runat="server" />
            </td>
        </tr>
    </table>
    <table width="100%" cellspacing="0" cellpadding="2" border="0">
        <tr bgcolor="#0d4c7d">
            <td style="color: white">
                Form Data
            </td>
        </tr>
        </table>
    <div  >
        <asp:GridView ID="gvFormData" runat="server" AutoGenerateColumns="false" AllowPaging="false" AllowSorting="false" GridLines="both" CellPadding="8" Width="100%" CellSpacing="2" EmptyDataText="No Form Data Found" >
            <AlternatingRowStyle CssClass="rowdark2" />
            <HeaderStyle ForeColor="White"   />
            <Columns>
                <asp:BoundField DataField="Stat" HeaderText="Stat" ReadOnly="true" ItemStyle-Wrap="true"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="GridHeader"
                    FooterStyle-CssClass="GridHeader" />
                <asp:BoundField DataField="INS_LINE" HeaderText="Ins Line" ReadOnly="true" ItemStyle-Wrap="true"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="GridHeader"
                    FooterStyle-CssClass="GridHeader"  />
                <asp:BoundField DataField="Loc" HeaderText="Loc" ReadOnly="true" ItemStyle-Wrap="true"   ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="GridHeader"
                    FooterStyle-CssClass="GridHeader" />
                <asp:BoundField DataField="Bldg" HeaderText="Bldg" ReadOnly="true" ItemStyle-Wrap="true"   ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="GridHeader"
                    FooterStyle-CssClass="GridHeader" />
                <asp:BoundField DataField="Unit" HeaderText="Unit" ReadOnly="true" ItemStyle-Wrap="true"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="GridHeader"
                    FooterStyle-CssClass="GridHeader" />
                <asp:BoundField DataField="FORM_NUMBER" HeaderText="Form Number" ReadOnly="true" ItemStyle-Wrap="true"   ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="GridHeader"
                    FooterStyle-CssClass="GridHeader"/>
                <asp:BoundField DataField="EDITIONDATE" HeaderText="Form Date" ReadOnly="true" ItemStyle-Wrap="true"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="GridHeader"
                    FooterStyle-CssClass="GridHeader" />
                <asp:BoundField DataField="FORM_DESCRIPTION" HeaderText="Form Description" ReadOnly="true" ItemStyle-Wrap="true"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="GridHeader"
                    FooterStyle-CssClass="GridHeader" />
                <asp:BoundField DataField="FORM_ACTION" HeaderText="Form Action" ReadOnly="true" ItemStyle-Wrap="true"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="GridHeader"
                    FooterStyle-CssClass="GridHeader" />
                <asp:BoundField DataField="EZ_SCM" HeaderText="Ez Scm" ReadOnly="true" ItemStyle-Wrap="true"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="GridHeader"
                    FooterStyle-CssClass="GridHeader" />
                <asp:BoundField DataField="Data" HeaderText="Data" ReadOnly="true" ItemStyle-Wrap="true"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="GridHeader"
                    FooterStyle-CssClass="GridHeader" />
                <asp:BoundField DataField="Iterative" HeaderText="Iterative" ReadOnly="true" ItemStyle-Wrap="true"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="GridHeader"
                    FooterStyle-CssClass="GridHeader" />
                <asp:BoundField DataField="RATEOP" HeaderText="RATE OP" ReadOnly="true" ItemStyle-Wrap="true"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="GridHeader"
                    FooterStyle-CssClass="GridHeader" />
                <asp:BoundField DataField="ENTRYDTE" HeaderText="Entry Date" ReadOnly="true" ItemStyle-Wrap="true"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="GridHeader"
                    FooterStyle-CssClass="GridHeader" />              
            </Columns>
        </asp:GridView>
    </div>
    </form>
</body>
</html>
