<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PS_VehicleInspections.aspx.cs" Inherits="Riskmaster.UI.UI.PolicyInterface.PS_VehicleInspections" ValidateRequest="false" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/UserControlDataGrid.ascx" tagname="UserControlDataGrid" tagprefix="dg" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/SystemUsers.ascx" tagname="SystemUsers" tagprefix="cul" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove" xmlns:cul="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
  <head id="Head1" runat="server">
    <title>Unit Inspection</title>
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js">        { var i; }
</script>
    <script language="JavaScript" src="../../Scripts/drift.js">        { var i; }
</script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js">        { var i; }
  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">        { var i; }
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../Scripts/dhtml-div.js">        { var i; }
  </script>
    <script type="text/javascript" src="../../Scripts/dhtml-help-setup.js">        { var i; }
  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">        { var i; }
	</script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
  </head>
  <body class="10pt" onload="pageLoaded();">
    <form name="frmData" id="frmData" runat="server">
      <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />
      <asp:HiddenField runat="server" ID="wsrp_rewrite_action_1" value="" />
      <asp:TextBox style="display:none" runat="server" name="hTabName" id="hTabName" />
      <asp:ScriptManager ID="SMgr" runat="server" />
      <div id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">                
        <div class="toolBarButton" runat="server" id="div_movefirst" xmlns="">
          <asp:ImageButton runat="server" OnClick="NavigateFirst" src="../../Images/tb_first_active.png" width="28" height="28" border="0" id="movefirst" AlternateText="Move First" onMouseOver="this.src='../../Images/tb_first_mo.png';" onMouseOut="this.src='../../Images/tb_first_active.png';" OnClientClick="" />
        </div>
        <div class="toolBarButton" runat="server" id="div_moveprevious" xmlns="">
          <asp:ImageButton runat="server" OnClick="NavigatePrev" src="../../Images/tb_previous_active.png" width="28" height="28" border="0" id="moveprevious" AlternateText="Move Previous" onMouseOver="this.src='../../Images/tb_previous_mo.png';" onMouseOut="this.src='../../Images/tb_previous_active.png';" OnClientClick="" />
        </div>
        <div class="toolBarButton" runat="server" id="div_movenext" xmlns="">
          <asp:ImageButton runat="server" OnClick="NavigateNext" src="../../Images/tb_next_active.png" width="28" height="28" border="0" id="movenext" AlternateText="Move Next" onMouseOver="this.src='../../Images/tb_next_mo.png';" onMouseOut="this.src='../../Images/tb_next_active.png';" OnClientClick="" />
        </div>
        <div class="toolBarButton" runat="server" id="div_movelast" xmlns="">
          <asp:ImageButton runat="server" OnClick="NavigateLast" src="../../Images/tb_last_active.png" width="28" height="28" border="0" id="movelast" AlternateText="Move Last" onMouseOver="this.src='../../Images/tb_last_mo.png';" onMouseOut="this.src='../../Images/tb_last_active.png';" OnClientClick="" />
        </div>                        
      </div>
      <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" Text="Unit Inspection" />
        <asp:label id="formsubtitle" runat="server" Text="" />
      </div>
      <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" Text="" />
      </div>
      <div class="tabGroup" id="TabsDivGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSinspection" id="TABSinspection">
          <a class="Selected" HREF="#" runat="server" onclick="tabChange(this.name);return false;" RMXRef="" name="inspection" id="LINKTABSinspection">Inspection Data</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPinspection">
          <nbsp />
          <nbsp />
        </div>
      </div>
      <div class="singletopborder" runat="server" name="FORMTABinspection" id="FORMTABinspection">
        <table width="98%" border="0" cellspacing="0" celpadding="0" padding="0">
          <tr>
            <td>
              <asp:hiddenfield runat="server" id="hdinspection" />
            </td>
          </tr>
          <tr>
            <td>
              <asp:TextBox style="display:none" runat="server" id="unitid" RMXRef="/Instance/VehicleXInspct/UnitId|/Instance/UI/FormVariables/SysExData/UnitId" RMXType="id" />
              <asp:TextBox style="display:none" runat="server" id="unitinsprowid" RMXRef="/Instance/VehicleXInspct/UnitInspRowId" RMXType="id" />
              <div runat="server" class="half" id="div_inspectiondate" xmlns="">
                <asp:label runat="server" class="required" id="lbl_inspectiondate" Text="Inspection Date" />
                <span class="formw">
                  <asp:label runat="server" id="inspectiondate" RMXRef="/Instance/VehicleXInspct/InspectionDate" RMXType="date" />                  
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="completerow" id="div_inspectionresult" xmlns="">
                <asp:label runat="server" class="required" id="lbl_inspectionresult" Text="Inspection Result" />
                <span class="formw">
                  <asp:TextBox runat="Server" id="inspectionresult" RMXRef="/Instance/VehicleXInspct/InspectionResult" RMXType="memo" readonly="true" TextMode="MultiLine" Columns="30" rows="5" />                  
                </span>
              </div>
            </td>
          </tr>
        </table>
      </div>
      <div id="Div2" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnBack">
          <asp:Button class="button" runat="Server" id="BackToParent" Text="Back To Vehicle" OnClientClick="if(!( XFormHandler('','1','back')))return false;" PostBackUrl="?" />
        </div>
      </div>
      <asp:TextBox style="display:none" runat="server" id="SysCmd" RMXRef="Instance/UI/FormVariables/SysCmd" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdConfirmSave" RMXRef="Instance/UI/FormVariables/SysCmdConfirmSave" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdQueue" RMXRef="Instance/UI/FormVariables/SysCmdQueue" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysCmdText" RMXRef="Instance/UI/FormVariables/SysCmdText" RMXType="hidden" Text="Navigate" />
      <asp:TextBox style="display:none" runat="server" id="SysClassName" RMXRef="Instance/UI/FormVariables/SysClassName" RMXType="hidden" Text="VehicleXInspct" />
      <asp:TextBox style="display:none" runat="server" id="SysSerializationConfig" RMXRef="Instance/UI/FormVariables/SysSerializationConfig" RMXType="hidden" Text="&lt;VehicleXInspct&gt;&lt;/VehicleXInspct&gt;" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPForm" RMXRef="Instance/UI/FormVariables/SysFormPForm" RMXType="hidden" Text="vehicle" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPIdName" RMXRef="Instance/UI/FormVariables/SysFormPIdName" RMXType="hidden" Text="unitid" />
      <asp:TextBox style="display:none" runat="server" id="SysFormPId" RMXRef="Instance/UI/FormVariables/SysFormPId" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysPSid" RMXRef="Instance/UI/FormVariables/SysPSid" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysEx" RMXRef="Instance/UI/FormVariables/SysEx" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysFormName" RMXRef="Instance/UI/FormVariables/SysFormName" RMXType="hidden" Text="vehicleinspections" />
      <asp:TextBox style="display:none" runat="server" id="SysFormIdName" RMXRef="Instance/UI/FormVariables/SysFormIdName" RMXType="hidden" Text="unitinsprowid" />
      <asp:TextBox style="display:none" runat="server" id="SysFormId" RMXRef="Instance/UI/FormVariables/SysFormId" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="SysSid" RMXRef="Instance/UI/FormVariables/SysSid" RMXType="hidden" Text="21200" />
      <asp:TextBox style="display:none" runat="server" id="SysViewType" RMXRef="Instance/UI/FormVariables/SysViewType" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" id="TabNameList" RMXRef="" RMXType="hidden" Text="TABSinspection" />
      <asp:TextBox style="display:none" runat="server" id="GroupAssocFieldList" RMXRef="" RMXType="hidden" Text="" />
      <asp:TextBox style="display:none" runat="server" name="formname" Text="vehicleinspections" />
      <asp:TextBox style="display:none" runat="server" name="SysRequired" id="SysRequired" Text="inspectiondate|inspectionresult|" />
      <asp:TextBox style="display:none" runat="server" name="SysBindingRequiredFields" id="SysBindingRequiredFields" Text="" />
      <asp:TextBox style="display:none" runat="server" name="SysFocusFields" Text="inspectiondate|" />
      <asp:TextBox style="display:none" runat="server" name="SysReadonlyFields" id="SysReadonlyFields" Text="" />
      <asp:TextBox style="display:none" runat="server" name="SysIsServiceError" id="SysIsServiceError" />
      <input type="hidden" id="hdSaveButtonClicked" />
      <asp:TextBox style="display:none" runat="server" name="SysIsFormSubmitted" id="SysIsFormSubmitted" />
      <asp:TextBox runat="server" id="txtScreenFlowStack" style="display:none" />
      <asp:TextBox runat="server" id="SysPageDataChanged" style="display:none" />
      <input type="hidden" value="rmx-widget-handle-2" id="SysWindowId" />
      <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    </form>
  </body>
</html>
