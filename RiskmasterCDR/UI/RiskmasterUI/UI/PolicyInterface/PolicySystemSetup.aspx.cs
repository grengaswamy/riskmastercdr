﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;

namespace Riskmaster.UI.PolicyInterface
{
    public partial class PolicySystemSetup : NonFDMBasePageCWS
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            string sCWSresponse = string.Empty;
            XElement XmlTemplate = null;
            if (PolicySystemGrid_RowDeletedFlag.Text.ToLower() == "true")
            {
                HdnSelectedIdForDeletion.Text = PolicySystemGridSelectedId.Text;
                HdnListNameForDeletion.Text = "PolicySystemList";
                XmlTemplate = GetMessageTemplateForDeletion();
                CallCWS("SetUpPolicySystemAdaptor.Delete", XmlTemplate, out sCWSresponse, false, false);
                PolicySystemGrid_RowDeletedFlag.Text = "false";
            }
            if (HdnActionSave.Text != "Save")
            {
                NonFDMCWSPageLoad("SetUpPolicySystemAdaptor.Get");
            }
        }

        private XElement GetMessageTemplateForDeletion()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("SetUpPolicySystemAdaptor.Delete");
            sXml = sXml.Append("</Function></Call><Document><Document><FunctionToCall/><DeletionListName>");
            sXml = sXml.Append(HdnListNameForDeletion.Text);
            sXml = sXml.Append("</DeletionListName><DeletionId>");
            sXml = sXml.Append(HdnSelectedIdForDeletion.Text);
            sXml = sXml.Append("</DeletionId>");
            sXml = sXml.Append("</Document></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        //rupal:start
        protected void btnValidatePolSystem_Click(object sender, EventArgs e)
        {
            XElement XmlTemplate = null;
            XmlTemplate = GetMessageTemplateForRefreshingPolicySystems();
            string sCWSresponse = string.Empty;
            CallCWS("SetUpPolicySystemAdaptor.RefreshPolicySystems", XmlTemplate, out sCWSresponse, false, false);                    
        }

        private XElement GetMessageTemplateForRefreshingPolicySystems()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("SetUpPolicySystemAdaptor.RefreshPolicySystems");
            sXml = sXml.Append("</Function></Call><Document><Document><FunctionToCall/>");
            sXml = sXml.Append("</Document></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
        //rupal:end
      
    }
}