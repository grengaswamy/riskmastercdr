<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IntegralPolicyData.aspx.cs" Inherits="Riskmaster.UI.UI.IntegralPolicyInterface.IntegralPolicyData"
    ValidateRequest="false" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove"
xmlns:uc="remove" xmlns:dg="remove" xmlns:cul="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
<head id="Head1" runat="server">
    <title>Policy Data</title>
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script language="javaScript" src="../../Scripts/form.js" type="text/javascript"></script>
    <script language="JavaScript" src="../../Scripts/drift.js" type="text/javascript">        { var i; }
    </script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js" type="text/javascript">        { var i; }
    </script>

    <script type="text/javascript" language="javascript">

        function ToggleDivs(divIDShow, divIDHide) {
            if (divIDShow == "FORMTABUnitInterestList" && document.getElementById("hdnIsUnitInterestListEmpty").value == "True") {
                if (divIDHide == "FORMTABunitinfo") {
                    divIDShow = "FORMTABCoverageDetail";
                }
                else if (divIDHide == "FORMTABCoverageDetail") {
                    divIDShow = "FORMTABunitinfo";
                }
            }

            document.getElementById(divIDShow).style.display = 'block';
            document.getElementById(divIDHide).style.display = 'none';
        }

        //function BackButtonClicked() {
        //    var claimid = document.getElementById("hdnClaimId");
        //    if (claimid == "") {// close the pop up in case of policy inquiry
        //        window.self.close();
        //    }
        //    else {// go back to search criteria screen in case of policy download
        //        var claimtype = document.getElementById("hdnClaimType").value;
        //        var sPolicyLobCode = document.getElementById("hdnPolicyLobCode").value;
        //        var sDate = document.getElementById("hdnPolicyLossDate").value;
        //        var sPolicyLobId = document.getElementById("hdnPolicyLobId").value;

        //        window.location.href = "/RiskmasterUI/UI/PolicyInterface/PolicySysDownload.aspx?ClaimType=" + claimtype + "&claimId=" + claimid + '&PolicyLobCode=' + sPolicyLobCode + '&PolicyDateCriteria=' + sDate + '&PolicyLobId=' + sPolicyLobId;
        //    }
        //}

        function showPolicySuccess() {
            if (pleaseWait != null) {
                pleaseWait.pleaseWait('stop');//vkumar258
            }
            if (document.getElementById("hdnPolSaveResponse").value == "True") {
                //bram4 -  ML changes
                alert(IntegralPolicyDataValidation.PolicyDownloadSuccess);
                //alert("Policy Data successfully downloaded");

                try {
                    if (document.getElementById("hdnPolicyFlag").value != '')
                        window.opener.ValidatePolicyWithoutClaim(document.getElementById("hdnPolicyID").value, document.getElementById("hdnPolicyLobCode").value);
                    else
                        window.opener.opener.ValidatePolicy(document.getElementById("hdnPolicyID").value);
                    window.opener.close();
                    window.close();
                    // window.opener.opener.SetClaimPolicyList(document.getElementById("hdnPolicyID").value, document.getElementById("hdnPolicyNumber").value, document.getElementById("hdPolicySystemID").value, '', '');
                    //window.opener.close();
                    // window.close();
                }
                catch (e) {
                }
                return true;
            }
        }
        //vkumar258 -start - for please wait dialogue after policy download

        function OnSavedClick(obj) {
            pleaseWait.Show();
            obj.disabled = true;
        }
        // vkumar258 - end
    </script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body class="10pt" id="bdTag">
    <form name="frmData" id="frmData" runat="server">
        <telerik:radscriptmanager id="ScriptManager1" enablepagemethods="true" runat="server" />
        <telerik:radstylesheetmanager id="RadStyleSheetManager1" runat="server" />
        <telerik:radcodeblock runat="server" id="radCodeBlock">
            <script type="text/javascript">
                function clientAjaxRequest(arg) {
                    var ajaxManagerObject = $find("<%= RadAjaxManager1.ClientID %>");
                    ajaxManagerObject.ajaxRequest(arg);
                }
                function TogglePolicyInterestSelection(checkbox) {
                    clientAjaxRequest("ToggleSelectPolicyInterest");
                }
                //function ToggleUnitSelection(checkbox, index) {
                //    if (checkbox.checked)
                //        clientAjaxRequest("SelectUnit|" + index);
                //    else
                //        clientAjaxRequest("UnselectUnit|" + index);
                //}
                function ToggleUnitInterestSelection(checkbox, index) {
                    if (checkbox.checked)
                        clientAjaxRequest("SelectUnitInterest|" + index);
                    else
                        clientAjaxRequest("UnselectUnitInterest|" + index);
                }
                function gvUnitRowClick(sender, eventArgs)
                {
                    //Payal : RMA:9943 --Starts
                    var grid = $find("<%=gvUnitList.ClientID %>");
                    var MasterTable = grid.get_masterTableView();
                    var selectedRows = MasterTable.get_selectedItems();
                    if (selectedRows.length > 0) {
                        var grid = $find("<%=gvUnitList.ClientID %>");
                           grid.clearSelectedItems();

                       }
                    //Payal : RMA:9943 --Ends
                    var selected = eventArgs.get_gridDataItem();
                    var index = eventArgs.get_itemIndexHierarchical();
                    selected.set_selected(true); //Payal : RMA:9943
                    if (selected.get_selected()) {
                        document.getElementById("btnNextDiv2").style.display = "";
                        clientAjaxRequest("SelectUnit|" + index);                        
                    }                 
                }
                function gvRowCreating(sender, eventArgs)
                {//required dummy funtion - do not remove - sanoopsharma
                }

            </script>
        </telerik:radcodeblock>
        <telerik:radajaxmanager id="RadAjaxManager1" runat="server" onajaxrequest="RadAjaxManager1_AjaxRequest" UpdatePanelsRenderMode="Inline">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="txtAdditionalInfo" />
                        <telerik:AjaxUpdatedControl ControlID="btnNextDiv2" />
                        <telerik:AjaxUpdatedControl ControlID="gvUnitList" />
                        <telerik:AjaxUpdatedControl ControlID="gvUnitInterest" />
                        <telerik:AjaxUpdatedControl ControlID="gvCoverages" />
                        <telerik:AjaxUpdatedControl ControlID="hdnIsUnitInterestListEmpty" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:radajaxmanager>
        <asp:hiddenfield runat="server" id="wsrp_rewrite_action_1" value="" />
        <asp:textbox style="display: none" runat="server" name="hTabName" id="hTabName" />
        <div>
            <uc1:errorcontrol id="ErrorControl1" runat="server" />
        </div>
        <div id="dvParent" runat="server">

            <div id="FORMTABfullpolicy">
                <div runat="server" name="FORMTABpolicyinfo" id="FORMTABpolicyinfo">
                    <table width="100%" border="0" cellspacing="8" cellpadding="4" padding="0">
                        <colgroup>
                            <col width="25%" />
                            <col width="25%" />
                            <col width="25%" />
                            <col width="25%" />
                        </colgroup>
                        <tr>
                            <td colspan="4">
                                <asp:hiddenfield runat="server" id="hdpolicyinfo" />
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" colspan="4">
                                <div class="msgheader" id="divPolicyInfo" runat="server">
                                    <asp:label id="lblPolicyInfo" runat="server" text="<%$ Resources:lblPolicyInfo %>" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:label runat="server" id="lblmasterCmpny" text="<%$ Resources:lblmasterCmpny %>" class="required"></asp:label>
                            </td>
                            <td>
                                <asp:label runat="server" id="lblMasterCmpnyValue"></asp:label>
                            </td>
                            <td>
                                <asp:label runat="server" id="lblPolicyType" text="<%$ Resources:lblPolicyType %>" class="required"></asp:label>
                            </td>
                            <td>
                                <asp:label runat="server" id="lblPolicyTypeValue"></asp:label>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <asp:label runat="server" class="required" id="lblpolicynumber" text="<%$ Resources:lblpolicynumber %>" />
                            </td>
                            <td align="left">
                                <asp:label runat="server" id="policyNumber" rmxtype="text" />
                            </td>
                            <td>
                                <asp:label runat="server" class="required" id="lblStatus" text="<%$ Resources:lblStatus %>" />
                            </td>
                            <td align="left">
                                <asp:label runat="server" id="policyStatus" rmxtype="text" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:label runat="server" class="required" id="lblPolicyeffDate" text="<%$ Resources:lblPolicyeffDate %>" />
                            </td>
                            <td align="left" valign="top">
                                <asp:label runat="server" id="policyEffDate" rmxtype="text" />
                            </td>
                            <td>
                                <asp:label runat="server" class="required" id="lblPolicyExpDate" text="<%$ Resources:lblPolicyExpDate %>" />
                            </td>
                            <td align="left" valign="top">
                                <asp:label runat="server" id="policyExpDate" rmxtype="text" />
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <asp:label runat="server" class="required" id="lblRenewalType" text="<%$ Resources:lblRenewalType %>" />
                            </td>
                            <td align="left">
                                <asp:label runat="server" id="policyRenewalType" rmxtype="text" />

                            </td>
                            <td>
                                <asp:label runat="server" class="required" id="lblRenewalNo" text="<%$ Resources:lblRenewalNo %>" />
                            </td>
                            <td>
                                <asp:label runat="server" id="policyRenewalNo" rmxtype="text" />
                            </td>
                        </tr>
                        <%--<tr>
                            <td>
                                <asp:label runat="server" class="required" id="lblEndorsementNo" text="Endorsement Number" />
                            </td>
                            <td align="left">
                                <asp:label runat="server" id="policyEndorsementNo" rmxtype="text" />
                            </td>
                           <td>
                                <asp:label runat="server" class="required" id="lblPremiumData" text="Premium Data" />
                            </td>
                            <td align="left">
                                <asp:label runat="server" id="policyPremiumData" rmxtype="text" />
                            </td>

                            <td>
                                <asp:label runat="server" class="required" id="lblPaymentPlan" text="Payment Plan" />
                            </td>
                            <td align="left">
                                <asp:label runat="server" id="policyPaymentPlan" rmxtype="text" />
                            </td>
                        </tr>--%>
                        <tr>
                           <%-- <td>
                                <asp:label runat="server" class="required" id="lblPaymentMode" text="Payment Mode" />
                            </td>
                            <td>
                                <asp:label runat="server" id="policyPaymentMode" rmxtype="text" />
                            </td>--%>
                            <%-- <td>
                                <asp:label runat="server" class="required" id="lblRenewalNumber" text="Renewal Number" />
                            </td>
                            <td>
                                <asp:label runat="server" id="policyRenewalNumber" rmxtype="text" />
                            </td>--%>
                            <td>
                                <asp:label runat="server" class="required" id="lblCurrency" text="<%$ Resources:lblCurrency %>" />
                            </td>
                            <td align="left">
                                <asp:label runat="server" id="policyCurrency" rmxtype="text" />
                            </td>
                            <td>
                                <asp:label runat="server" class="required" id="lblNextBillDate" text="<%$ Resources:lblNextBillDate %>" />
                            </td>
                            <td>
                                <asp:label runat="server" id="policyNextBillDate" rmxtype="text" />
                            </td>
                        </tr>
                        <%--<tr>
                            
                            <td>
                                <asp:label runat="server" class="required" id="lblCurrencyRate" text="Currency Rate" />
                            </td>
                            <td align="left">
                                <asp:label runat="server" id="policyCurrencyRate" rmxtype="text" />
                            </td>
                        </tr>--%>
                        <tr>
                            <%--<td>
                                <asp:label runat="server" class="required" id="lblBranch" text="Branch" />
                            </td>
                            <td align="left">
                                <asp:label runat="server" id="policyBranch" rmxtype="text" />
                            </td>--%>
                           <%-- <td>
                                <asp:label runat="server" class="required" id="lblStateCode" text="StateCode" />
                            </td>
                            <td align="left">
                                <asp:label runat="server" id="policyStateCode" rmxtype="text" />
                            </td>--%>
                            <td valign="top">
                                <asp:label runat="server" class="required" id="lblCountry" text="<%$ Resources:lblCountry %>" />
                            </td>
                            <td align="left">
                                <asp:label runat="server" id="policyCountry" rmxtype="text" />
                            </td>

                        </tr>
                        <%--sanoopsharma--%>
                         <tr>                            
                            <td>
                                <asp:label runat="server" class="required" id="lblGrossPremium" text="<%$ Resources:lblGrossPremium %>" />
                            </td>
                            <td align="left">
                                <asp:label runat="server" id="policyGrossPremium" rmxtype="text" />
                            </td>
                            <td valign="top">
                                <asp:label runat="server" class="required" id="lblNetPremium" text="<%$ Resources:lblNetPremium %>" />
                            </td>
                            <td align="left">
                                <asp:label runat="server" id="policyNetPremium" rmxtype="text" />
                            </td>

                        </tr>

                        <tr>                            
                            <td>
                                <asp:label runat="server" class="required" id="lblTotalSumInsured" text="<%$ Resources:lblTotalSumInsured %>" />
                            </td>
                            <td align="left">
                                <asp:label runat="server" id="policyTotalSumInsured" rmxtype="text" />
                            </td>    
                            
                              <td>
                                <asp:label runat="server" class="required" id="lblPaymentPlan" text="<%$ Resources:lblPaymentPlan %>" />
                            </td>
                            <td align="left">
                                <asp:label runat="server" id="policyPaymentPlan" rmxtype="text" />
                            </td>                       

                        </tr>
                        <%--sanoopsharma--%>

                        <tr>
                            <td valign="top" colspan="4">
                                <div class="msgheader" id="div_InterestList" runat="server">
                                    <asp:label id="lblInterestList" runat="server" text="<%$ Resources:lblInterestList %>" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" colspan="4">
                                <asp:gridview id="gvPolicyInterestList" runat="server" autogeneratecolumns="false"
                                    allowsorting="false" gridlines="both" cellpadding="8" width="100%" cellspacing="2"
                                    emptydatatext="<%$ Resources:lblNoPolInterestList %>" pagesize="20" onrowdatabound="gvInterestList_RowDataBound">  
                                        <AlternatingRowStyle CssClass="rowdark2" />
                                        <HeaderStyle ForeColor="White"   />
                                        <Columns>
                                             <asp:TemplateField HeaderStyle-CssClass="Selected">
                                                <ItemTemplate>
                                                    <input type="checkbox" id="chkPolInterest" runat="server" onclick="TogglePolicyInterestSelection();"/>  
                                                    </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="<%$ Resources:gvHdrAddAs %>" HeaderStyle-CssClass="Selected">
                                                <ItemTemplate>                                                     
                                                    <asp:textbox runat="server" id="txtRole" readonly="true" text='<%# Bind("PS_Role_Desc") %>' width="90%"/>
                                                    <asp:DropDownList ID="ddSelectRole" name="ddSelectRole" runat="server" width="93%" />                                           
                                                </ItemTemplate>
                                                <ItemStyle Width="16%"/>
                                            </asp:TemplateField> 
                                            <%--Number column should stay at this position to ensure cell[2] index applies to it--%>
                                             <asp:BoundField DataField="Number" HeaderText="<%$ Resources:gvHdrNumber %>" ReadOnly="true" ItemStyle-Wrap="true"  ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="Selected"
                                            FooterStyle-CssClass="Selected" />                                              
                                            <asp:BoundField DataField="TaxID" HeaderText="<%$ Resources:gvHdrTaxID %>" ReadOnly="true" ItemStyle-Wrap="true"  ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="Selected"
                                            FooterStyle-CssClass="Selected" />
                                           <asp:BoundField DataField="Name" HeaderText="<%$ Resources:gvHdrName %>" ReadOnly="true" ItemStyle-Wrap="true"  ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="Selected"
                                            FooterStyle-CssClass="Selected" />                                            
                                            <asp:BoundField DataField="Birth_Date" HeaderText="<%$ Resources:gvHdrBirthDate %>" ReadOnly="true" ItemStyle-Wrap="true"  ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="Selected"
                                            FooterStyle-CssClass="Selected" />
                                            <asp:BoundField DataField="Sex" HeaderText="<%$ Resources:gvHdrSex %>" ReadOnly="true" ItemStyle-Wrap="true"  ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="Selected"
                                            FooterStyle-CssClass="Selected" />
                                            <asp:BoundField DataField="Email_Address" HeaderText="<%$ Resources:gvHdrEmailAddr %>" ReadOnly="true" ItemStyle-Wrap="true"  ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="Selected"
                                            FooterStyle-CssClass="Selected" />
                                            <asp:BoundField DataField="PS_Role_Text" HeaderText="<%$ Resources:gvHdrPSRoleText %>" ReadOnly="true" ItemStyle-Wrap="true"  ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="Selected"
                                            FooterStyle-CssClass="Selected" />
                                            <asp:BoundField  DataField="Parent_Key" Visible="false" HeaderText="<%$ Resources:gvHdrUnitNumber %>" />                                              
                                            <asp:BoundField DataField="PS_Role_Code" Visible="false"/>
                                            <asp:BoundField DataField="PS_Role_Desc" Visible="false"/>  
                                        </Columns>                     
                                </asp:gridview>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:button id="btnBackDiv1" text="<%$ Resources:btnBack %>" runat="server" onclientclick="window.self.close(); window.opener.pleaseWait.pleaseWait('stop'); return false" enabled="True" />
                                <asp:button id="btnNextDiv1" runat="server" text="<%$ Resources:btnNext %>" onclientclick="ToggleDivs('FORMTABunitinfo','FORMTABpolicyinfo');return false;" />
                            </td>
                            <td width="25%"></td>
                            <td width="25%"></td>
                            <td></td>
                        </tr>

                    </table>
                </div>
                <div id="FORMTABunitinfo" runat="server" style="display: none">

                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <asp:textbox id="txtChkBoxSelected" runat="server" style="display: none"></asp:textbox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <table width="100%">
                                    <tr>
                                        <td valign="top" colspan="4">
                                            <div class="msgheader" id="divUnitInfo" runat="server">
                                                <asp:label id="lblUnitInfo" runat="server" text="<%$ Resources:lblUnitInfo %>" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%">
                                            <div>
                                                <telerik:radgrid runat="server" id="gvUnitList" autogeneratecolumns="false" pagesize="10" allowsorting="True"
                                                    allowpaging="True" AllowMultiRowSelection="false" allowfilteringbycolumn="True" gridlines="None" onneeddatasource="gvUnitList_NeedDataSource"
                                                    skin="Office2007"  onprerender="gvUnitList_PreRender"> <%-- Removed databound event --%>
                                                    <SelectedItemStyle CssClass="SelectedItem" />
                                                    <ClientSettings>
                                                        <Selecting AllowRowSelect ="True" />
                                                        <ClientEvents OnRowClick ="gvUnitRowClick" onRowCreating ="gvRowCreating"/>                                                        
                                                    </ClientSettings>
                                                    <PagerStyle Mode="NextPrevAndNumeric" Position="Top" AlwaysVisible="true" />
                                                    <MasterTableView>
                                                        <Columns>
                                                            <%--<telerik:GridTemplateColumn UniqueName="Select" AllowFiltering="false">
                                                                <ItemTemplate>
                                                                    <input type="checkbox" id="chkSelectUnit" runat="server" />
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>--%>
                                                            <telerik:GridBoundColumn DataField="Unit_Number" HeaderText="<%$ Resources:gvHdrUnitNo %>" FilterControlWidth="50px"
                                                                UniqueName="Unit_Number" SortExpression="Unit_Number">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="Unit_Type" FilterControlWidth="50px" HeaderText="<%$ Resources:gvHdrUnitType %>"
                                                                UniqueName="Unit_Type" SortExpression="Unit_Type">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="Desc" HeaderText="<%$ Resources:gvHdrDesc %>" UniqueName="Desc"
                                                                FilterControlWidth="50px" SortExpression="Desc">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="Registration_No" HeaderText="<%$ Resources:gvHdrRegNo %>" UniqueName="Registration_No" FilterControlWidth="70px"
                                                                SortExpression="Registration_No">
                                                            </telerik:GridBoundColumn> 
                                                            <telerik:GridBoundColumn DataField="Chasis_No" HeaderText="<%$ Resources:gvHdrChasisNo %>" FilterControlWidth="70px"
                                                                UniqueName="Chasis_No" SortExpression="Chasis_No">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="Engine_No" HeaderText="<%$ Resources:gvHdrEngineNo %>" FilterControlWidth="70px"
                                                                UniqueName="Engine_No" SortExpression="Engine_No">
                                                            </telerik:GridBoundColumn> 
                                                             <telerik:GridBoundColumn DataField="Sum_Insured" HeaderText="<%$ Resources:gvHdrSumInsured %>" UniqueName="Sum_Insured" FilterControlWidth="50px"
                                                                SortExpression="Sum_Insured">
                                                            </telerik:GridBoundColumn>                                                          
                                                            <telerik:GridBoundColumn DataField="Region" HeaderText="<%$ Resources:gvHdrRegion %>" FilterControlWidth="70px"
                                                                UniqueName="Region" SortExpression="Region">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="Geo_Location" HeaderText="<%$ Resources:gvHdrGeoLocation %>" FilterControlWidth="70px"
                                                                UniqueName="Geo_Location" SortExpression="Geo_Location">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="Effective_Date" HeaderText="<%$ Resources:gvHdrEffectiveDate %>" FilterControlWidth="70px"
                                                                UniqueName="Effective_Date" SortExpression="Effective_Date">
                                                            </telerik:GridBoundColumn>
                                                            
                                                        </Columns>
                                                        <ItemStyle CssClass="datatd1" />
                                                        <AlternatingItemStyle CssClass="datatd" />
                                                        <HeaderStyle CssClass="msgheader" />
                                                    </MasterTableView>
                                                </telerik:radgrid>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:textbox id="hdnCbId" runat="server" style="display: none" rmxtype="id"></asp:textbox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:button id="btnBackDiv2" runat="server" text="<%$ Resources:btnBackUnit %>" onclientclick="ToggleDivs('FORMTABpolicyinfo','FORMTABunitinfo');return false;" />
                                <asp:button id="btnNextDiv2" runat="server" text="<%$ Resources:btnNextUnit %>" onclientclick="ToggleDivs('FORMTABUnitInterestList','FORMTABunitinfo');return false;" />
                            </td>
                            <td width="25%"></td>
                            <td width="25%"></td>
                            <td></td>
                        </tr>

                    </table>
                </div>
                <div runat="server" style="display: none;" name="FORMTABUnitInterestList"
                    id="FORMTABUnitInterestList">
                    <table width="100%" border="0" cellspacing="8" cellpadding="4">

                        <tr>
                            <td valign="top" colspan="4">
                                <div class="msgheader" id="divUnitInterestList" runat="server">
                                    <asp:label id="lblUnitInterestList" runat="server" text="<%$ Resources:lblUnitInterestList %>" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" colspan="4">
                                <telerik:radgrid runat="server" id="gvUnitInterest" autogeneratecolumns="false" gridlines="None"
                                    skin="Office2007" onneeddatasource="gvUnitInterestList_NeedDataSource"
                                    onitemdatabound="gvUnitInterest_ItemDataBound" enablelinqexpressions="false">
                                    <SelectedItemStyle CssClass="SelectedItem" />
                                    <MasterTableView>
                                        <Columns>
                                            <telerik:GridTemplateColumn UniqueName="Select" AllowFiltering="false">
                                                <ItemTemplate>
                                                    <input type="checkbox" id="chkSelectInterest" runat="server" />
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn UniqueName="SelectRole" HeaderText="<%$ Resources:gvHdrAddAsUnit %>">
                                                <ItemTemplate>
                                                    <asp:dropdownlist id="ddSelectRole" name="ddSelectRole" runat="server" width="93%" />
                                                    </select>
                                                </ItemTemplate>
                                                <ItemStyle Width="18%" />
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridBoundColumn DataField="Parent_Key" HeaderText="<%$ Resources:gvHdrUnitNumber %>" FilterControlWidth="50px"
                                                UniqueName="Parent_Key">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Number" HeaderText="<%$ Resources:gvHdrNumberUnit %>" FilterControlWidth="50px"
                                                UniqueName="Number">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="TaxID" HeaderText="<%$ Resources:gvHdrTaxIDUnit %>" FilterControlWidth="50px"
                                                UniqueName="TaxID">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Name" HeaderText="<%$ Resources:gvHdrNameUnit %>" FilterControlWidth="50px"
                                                UniqueName="Name">
                                            </telerik:GridBoundColumn>
                                           
                                            <telerik:GridBoundColumn DataField="Birth_Date" HeaderText="<%$ Resources:gvHdrBirthDateUnit %>" FilterControlWidth="50px"
                                                UniqueName="Birth_Date">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Sex" HeaderText="<%$ Resources:gvHdrSexUnit %>" FilterControlWidth="50px"
                                                UniqueName="Sex">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Email_Address" HeaderText="<%$ Resources:gvHdrEmailAddrUnit %>" FilterControlWidth="50px"
                                                UniqueName="Email_Address">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="PS_Role_Text" HeaderText="<%$ Resources:gvHdrPSRoleTextUnit %>" FilterControlWidth="50px"
                                                UniqueName="PS_Role_Text">
                                            </telerik:GridBoundColumn>
                                        </Columns>
                                        <ItemStyle CssClass="datatd1" />
                                        <AlternatingItemStyle CssClass="datatd" />
                                        <HeaderStyle CssClass="msgheader" />
                                    </MasterTableView>
                                </telerik:radgrid>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:button id="btnBackDiv3" runat="server" text="<%$ Resources:btnBackUnitInterest %>" onclientclick="ToggleDivs('FORMTABunitinfo','FORMTABUnitInterestList');return false;" />
                                <asp:button id="btnNextDiv3" runat="server" text="<%$ Resources:btnNextUnitInterest %>" onclientclick="ToggleDivs('FORMTABCoverageDetail','FORMTABUnitInterestList');return false;" />
                            </td>
                            <td width="25%"></td>
                            <td width="25%"></td>
                            <td></td>
                        </tr>
                    </table>
                </div>
                <div runat="server" style="display: none;" name="FORMTABCoverageDetail"
                    id="FORMTABCoverageDetail">
                    <table width="100%" border="0" cellspacing="8" cellpadding="4">

                        <tr>
                            <td valign="top" colspan="4">
                                <div class="msgheader" id="divCoverageDetail" runat="server">
                                    <asp:label id="lblCoverageDetail" runat="server" text="<%$ Resources:lblCoverageDetail %>" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" colspan="4">
                                <telerik:radgrid runat="server" id="gvCoverages" autogeneratecolumns="false" gridlines="None"
                                    skin="Office2007" onneeddatasource="gvCoverages_NeedDataSource" enablelinqexpressions="false">
                                    <MasterTableView>
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="CoverageSeqNo" UniqueName="CoverageSeqNo" Visible="false">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Unit_Number" HeaderText="<%$ Resources:gvHdrUnitNumberCovg %>" FilterControlWidth="50px"
                                                UniqueName="Unit_Number">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="UnitDesc" HeaderText="<%$ Resources:gvHdrUnitDescCovg %>" FilterControlWidth="50px"
                                                UniqueName="UnitDesc">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="CovCode" HeaderText="<%$ Resources:gvHdrCovCode %>" FilterControlWidth="50px"
                                                UniqueName="CovCode">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="CovDesc" HeaderText="<%$ Resources:gvHdrCovDesc %>" FilterControlWidth="50px"
                                                UniqueName="CovDesc">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Premium" HeaderText="<%$ Resources:gvHdrPremium %>" FilterControlWidth="50px"
                                                UniqueName="Premium">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Class" HeaderText="<%$ Resources:gvHdrClass %>" FilterControlWidth="50px"
                                                UniqueName="Class">
                                            </telerik:GridBoundColumn>
                                            <%--<telerik:GridBoundColumn DataField="CovStatus" HeaderText="Status" FilterControlWidth="50px"
                                                UniqueName="CovStatus">
                                            </telerik:GridBoundColumn>--%>
                                            <%--<telerik:GridBoundColumn DataField="EffectiveDate" HeaderText="Effective Date" FilterControlWidth="50px"
                                                UniqueName="EffectiveDate">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ExpiryDate" HeaderText="Expiry Date" FilterControlWidth="50px"
                                                UniqueName="ExpiryDate">
                                            </telerik:GridBoundColumn>--%>
                                            <telerik:GridBoundColumn DataField="TotalSumInsured" HeaderText="<%$ Resources:gvHdrTotalSumInsuredCovg %>" FilterControlWidth="50px"
                                                UniqueName="TotalSumInsured">
                                            </telerik:GridBoundColumn>
                                        </Columns>
                                    </MasterTableView>
                                </telerik:radgrid>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:button id="btnBackDiv4" runat="server" text="<%$ Resources:btnBackUnitCovg %>" onclientclick="ToggleDivs('FORMTABUnitInterestList','FORMTABCoverageDetail');return false;" />                              
                                <asp:button id="btnDownPolicy" runat="server" text="<%$ Resources:btnDownPolicy %>" onclientclick="OnSavedClick(this);" usesubmitbehavior="false" onclick="SaveClicked" />
                            </td>
                            <td width="25%"></td>
                            <td width="25%"></td>
                            <td></td>
                        </tr>
                    </table>
                </div>

            </div>
            <div runat="server" style="display: none;" name="FORMTABadditionalpolicyinfo"
                id="FORMTABadditionalpolicyinfo">
                <table width="100%" border="0" cellspacing="8" cellpadding="4">
                    <tr>
                        <td>
                            <asp:hiddenfield runat="server" id="hdadditionalpolicyinfo" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="4">
                            <div class="msgheader" id="divResponseXML" runat="server">
                                <asp:label id="lblResponseXML" runat="server" text="Response XML" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div runat="server" class="half" id="div2" xmlns="">
                                <span>
                                    <asp:textbox textmode="Multiline" runat="server" id="txtAdditionalInfo" readonly="true" width="100%" rows="25" />
                                </span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div runat="server" class="completerow" id="div5" xmlns="">
                                <span class="formw">
                                    <br />
                                </span>
                            </div>

                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <div runat="server" class="completerow" id="div7" xmlns="">
                                <span class="formw">
                                    <br />
                                </span>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>

        </div>
        <asp:hiddenfield runat="server" id="hdLocatnCompny" value="" />
        <asp:hiddenfield runat="server" id="hdMasterCompny" value="" />
        <asp:hiddenfield runat="server" id="hdPolicySystemID" value="" />
        <asp:hiddenfield runat="server" id="hdIssueCd" value="" />
        <asp:hiddenfield runat="server" id="hdPolCmpny" value="" />
        <asp:hiddenfield runat="server" id="hdPolState" value="" />
        <asp:hiddenfield runat="server" id="hdLossDt" value="" />
        <asp:hiddenfield id="hdnClaimType" value="" runat="server" />
        <asp:hiddenfield id="hdnClaimId" value="" runat="server" />
        <asp:hiddenfield id="hdnPolicyLobCode" value="" runat="server" />
        <asp:hiddenfield id="hdnPolicyLobId" value="" runat="server" />
        <asp:hiddenfield id="hdnPolicyLossDate" value="" runat="server" />
        <asp:hiddenfield id="hdnSystemId" value="" runat="server" />
        <asp:hiddenfield id="hdnPolSaveResponse" runat="server" value="" />
        <asp:hiddenfield id="hdnPolicyID" runat="server" value="" />
        <asp:hiddenfield id="hdnPolicyNumber" runat="server" value="" />
        <asp:hiddenfield id="hdnEnquireOnly" runat="server" value="" />
        <asp:hiddenfield id="hdnIsUnitInterestListEmpty" runat="server" value="" />
        <asp:hiddenfield id="hdnPolicyFlag" runat="server" value="" />
        <asp:hiddenfield id="hdnPolicySrchSys" runat="server" value="" />
        <asp:hiddenfield id="hdnStagingPolicyId" runat="server" value="" />
        <uc:pleasewaitdialog id="PleaseWaitDialog1" runat="server" custommessage="Loading" />
    </form>
</body>
</html>
