<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PS_Policy.aspx.cs" Inherits="Riskmaster.UI.UI.PolicyInterface.PS_Policy" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc" %>
<%@ Register Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" TagName="UserControlDataGrid"
    TagPrefix="dg" %>
<%@ Register Src="~/UI/Shared/Controls/MultiCodeLookup.ascx" TagName="MultiCode"
    TagPrefix="uc" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<%@ Register Src="~/UI/Shared/Controls/SystemUsers.ascx" TagName="SystemUsers" TagPrefix="cul" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove"
xmlns:uc="remove" xmlns:dg="remove" xmlns:cul="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
<head id="Head1" runat="server">
    <title>Policy Inquiry</title>
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script language="javaScript" src="../../Scripts/form.js" type="text/javascript"></script>
    <script language="JavaScript" src="../../Scripts/drift.js" type="text/javascript">        { var i; }
    </script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js" type="text/javascript">        { var i; }
    </script>
    <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js" >        { var i; }
    </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js" >        { var i; }
    </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js" >        { var i; }
    </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid.js" >        { var i; }
    </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">        { var i; }
    </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">        { var i; }
    </script>
    <script type="text/javascript" src="../../Scripts/dhtml-div.js">        { var i; }
    </script>
    <script type="text/javascript" src="../../Scripts/dhtml-help-setup.js">        { var i; }
    </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">        { var i; }
    </script>

    <%--tanwar2 - Policy Download from staging - start--%>
    <style type="text/css">
    .hide
    {
        display:none;
    }
    .left
    {
        margin-top:5px;
        display:block;
    }
    .td{
    display:inline-block;
    *display:inline; /* for IE7*/
    *zoom:1;/* for IE7*/
    width:24%;
    margin-top:5px;
    vertical-align:middle;
    font-family: Tahoma, Arial, Helvetica, sans-serif;
    }

    </style>
    <script src="../../Scripts/jquery/jquery-1.8.0.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        var selectedUnitVal = '';
        $('document').ready(function () {
            if ($('#hdnRemoveEmtpyFields').val() === 'True') {
                //hide empty tds
                $('#FORMTABpolicyinfo table tbody tr td').each(function () {
                    if ($(this).children('input[type=submit]').length === 0) {
                        if ($(this).children('span:empty()').length === $(this).children('span').length) {
                            $(this).addClass('hide');
                            if ($(this).prev('td').children('span.required').length > 0) {
                                $(this).prev('td').addClass('hide');
                            }
                        }
                    }
                });

                //mark trs with all empty tds
                $('#FORMTABpolicyinfo table tbody tr').each(function () {
                    if ($(this).children('td.hide').length == $(this).children('td').length) {
                        $(this).addClass('hide');
                    }
                });
            }

            // Pradyumna 02252014 MITS#35296 - Start 
            // Select Unit Info tab on Postback event
            if ($('#hdnClickUnitInfo').val() === 'True') {
                tabChange("unitinfo");
            }
            // Select Row when Radio Button is Clicked
            $('input[type=radio]').change(function () {
                $('input:checked').closest('tr').trigger('click');
            });

            // Select Radio Button when Row is Clicked
            $('tr').click(function () {
                $('input[type=radio]').prop('checked', false);
                $('tr.SelectedItem').find('input[type=radio]').prop('checked', true);
            });
            // Pradyumna 02252014 MITS#35296 - End
        });

        ///Code from:
        ///http ://www.telerik.com/community/code-library/aspnet-ajax/grid/single-radiobutton-check-at-a-time-with-row-selection.aspx
        function selectSingleRadio(objRadioButton, grdName) {
            var i, obj, pageElements;

            if (navigator.userAgent.indexOf("MSIE") != -1) {
                //IE browser  
                pageElements = document.all;
            }
            else if (navigator.userAgent.indexOf("Mozilla") != -1 || navigator.userAgent.indexOf("Opera") != -1) {
                //FireFox/Opera browser  
                pageElements = document.documentElement.getElementsByTagName("input");
            }
            for (i = 0; i < pageElements.length; i++) {
                obj = pageElements[i];

                if (obj.type == "radio") {
                    if (objRadioButton.id.substr(0, grdName.length) == grdName) {
                        if (objRadioButton.id == obj.id) {
                            obj.checked = true;
                        }
                        else {
                            obj.checked = false;
                        }
                    }
                }
            }
        }
    </script>
    <%--tanwar2 - Policy Download from staging - end--%>

    <script type="text/javascript" language="javascript">
        function SetValues() {
            var ctrl1;
            var Nm, Addr1, City, PostalCd, TaxId, BirtDate, NmType;
            ctrl1 = window.opener.document.getElementById("hfInsurerNm");
            if (ctrl1 != null) {              
                Nm = ctrl1.value;
            }

            ctrl1 = window.opener.document.getElementById("hfInsurerAddr1");
            if (ctrl1 != null) {
                Addr1 = ctrl1.value;
            }

            ctrl1 = window.opener.document.getElementById("hfInsurerCity");
            if (ctrl1 != null) {
                City = ctrl1.value;
            }

            ctrl1 = window.opener.document.getElementById("hfInsurerPostalCd");
            if (ctrl1 != null) {
                PostalCd = ctrl1.value;
            }
            ctrl1 = window.opener.document.getElementById("hdnTaxId");
            if (ctrl1 != null) {
                TaxId = ctrl1.value;
            }
            ctrl1 = window.opener.document.getElementById("hdnBirthDt");
            if (ctrl1 != null) {
                BirtDate = ctrl1.value;
            }

            ctrl1 = window.opener.document.getElementById("hdnNameType");
            if (ctrl1 != null) {
                NmType = ctrl1.value;
            }
            document.getElementById("lblInsurerData").innerText = Nm + " " + Addr1 + " " + City + " " + PostalCd; // + " " + InsurerTaxId;
            document.getElementById("InsuredTaxId").innerText = TaxId;
            document.getElementById("InsuredBirthDate").innerText = BirtDate;
            document.getElementById("policyNameType").innerText = NmType;

            //tanwar2 - Policy download from staging - start

            if ($('#hdnRemoveEmtpyFields').val() === 'True') {
                //Remove tr td having class hide
                $('.hide').remove();

                //Change all tr td to divs
                $('#FORMTABpolicyinfo').html($('#FORMTABpolicyinfo table tbody').html()
                                    .replace(/<tr(.*)>/gi, '')
                                    .replace(/<\/tr>/gi, '')
                                    .replace(/<td/gi, '<span class="td"')
                                    .replace(/<\/td/gi, '</span')
                                    );

                $('input[type=submit]').closest('.td').removeClass('td').addClass('left');
                $('.td:even').addClass('required');
            }
            //Policy download from staging - end
        }

        function OpenUnit() {
            // Pradyumna 03032014 MITS#35296 - Start 
            var m_window;
            //var CtrlList, val, m_window;
            //val = "";
            //CtrlList = document.getElementsByTagName("input");
            
//            for (i = 0; i < CtrlList.length; i++) {
//                if (CtrlList[i].type == "radio" && CtrlList[i].checked == true) {
//                    val = CtrlList[i].value;
//                    break;
//                }
//            }
            //if (val == "") {
            if (selectedUnitVal == "") {
                alert("Please select a record");
            } // Pradyumna 03032014 MITS#35296 - End
            else {
                // m_window = window.open("/RiskmasterUI/UI/PolicyInterface/PS_PolicyCoverageDetail.aspx?val=" + val, "CoverageDetail", "width=730,height=430,top=" + (screen.availHeight - 650) / 2 + ",left=" + (screen.availWidth - 900) / 2 + ",resizable=yes,scrollbars=yes");
                // Pradyumna 03032014 MITS#35296 - Start 
                //m_unitWindow = window.open("/RiskmasterUI/UI/PolicyInterface/PS_PolicyUnitDetail.aspx?val=" + val, "Unit", "width=730,height=430,top=" + (screen.availHeight - 650) / 2 + ",left=" + (screen.availWidth - 900) / 2 + ",resizable=yes,scrollbars=yes");
                m_unitWindow = window.open("/RiskmasterUI/UI/PolicyInterface/PS_PolicyUnitDetail.aspx?val=" + selectedUnitVal, "Unit", "width=730,height=430,top=" + (screen.availHeight - 650) / 2 + ",left=" + (screen.availWidth - 900) / 2 + ",resizable=yes,scrollbars=yes");
                // Pradyumna 03032014 MITS#35296 - End
            }
            return false;



//            var vals;
//            ele = document.getElementById('multiunitid');
//            if (ele.selectedIndex == -1) {
//                alert('Please select a unit record to continue');
//                return false;
//            }
//            else {
//                vals = ele.value;
//            }
//            
//            m_unitWindow = window.open("/RiskmasterUI/UI/PolicyInterface/PS_PolicyUnitDetail.aspx?val="+vals, "Unit", "width=730,height=430,top=" + (screen.availHeight - 650) / 2 + ",left=" + (screen.availWidth - 900) / 2 + ",resizable=yes,scrollbars=yes");
//            return false;
        }
        function OpenCoverage() {
            // Pradyumna 03042014 MITS#35296 - Start
            var m_window;
//            var CtrlList, val, m_window;
//            val = "";
//            CtrlList = document.getElementsByTagName("input");
//            for (i = 0; i < CtrlList.length; i++) {
//                if (CtrlList[i].type == "radio" && CtrlList[i].checked == true) {
//                    val = CtrlList[i].value;
//                    break;
//                }
//            }
//            if (val == "") {
//                alert("Please select a record");
//            }
            ////Ashish Ahuja : Claims Made Jira 1342
            var sClaimDateReported = document.getElementById("hdnClaimReportedDate").value;
            if (selectedUnitVal == "") {
                alert("Please select a record");
            }
            else {

                //m_unitWindow = window.open("/RiskmasterUI/UI/PolicyInterface/PS_PolicyCoverageList.aspx?val=" + val, "Coverage", "width=725,height=400,top=" + (screen.availHeight - 650) / 2 + ",left=" + (screen.availWidth - 900) / 2 + ",resizable=yes,scrollbars=yes");
                m_unitWindow = window.open("/RiskmasterUI/UI/PolicyInterface/PS_PolicyCoverageList.aspx?val=" + selectedUnitVal + "&ClaimDateReported=" + sClaimDateReported, "Coverage", "width=725,height=400,top=" + (screen.availHeight - 650) / 2 + ",left=" + (screen.availWidth - 900) / 2 + ",resizable=yes,scrollbars=yes");//Ashish Ahuja : Claims Made Jira 1342
            }
            // Pradyumna 03042014 MITS#35296 - End 
            return false;


//            var vals;
//            ele = document.getElementById('multiunitid');
//            if (ele.selectedIndex == -1) {
//                alert('Please select a unit record to continue');
//                return false;
//            }
//            else {
//                vals = ele.value;
//            }
//            m_unitWindow = window.open("/RiskmasterUI/UI/PolicyInterface/PS_PolicyCoverageList.aspx?val="+vals, "Coverage", "width=725,height=400,top=" + (screen.availHeight - 650) / 2 + ",left=" + (screen.availWidth - 900) / 2 + ",resizable=yes,scrollbars=yes");
//            return false;
        }

        function OpenFormDataScreen() {
            var Policynum, Symbol, LocCmpny, MasterCmpny, module, PolSystemID, loc, subloc, unitno, polLob;

            if (document.getElementById("policyNumber") != null)
                Policynum = document.getElementById("policyNumber").innerText;
            if (document.getElementById("policySymbol") != null)
                symbol = document.getElementById("policySymbol").innerText;
            if (document.getElementById("hdLocatnCompny") != null)
                LocCmpny = document.getElementById("hdLocatnCompny").value;
            if (document.getElementById("hdMasterCompny") != null)
                MasterCmpny = document.getElementById("hdMasterCompny").value;
            if (document.getElementById("policyModule") != null)
                module = document.getElementById("policyModule").innerText;
            if (document.getElementById("hdPolicySystemID") != null)
                PolSystemID = document.getElementById("hdPolicySystemID").value;
            if (document.getElementById("policyLOB") != null)
                polLob = document.getElementById("policyLOB").innerText;
            //Added by swati for staging policy
            var StgngPolicyId;
            if (document.getElementById("hdnStagingPolicyId") != null)
                StgngPolicyId = document.getElementById("hdnStagingPolicyId").value;
            //change end here by swati
            loc = "";
            subloc = "";
            unitno = "";
           var val="";
           //window.open("PS_FormData.aspx", null, 'toolbar=0,titlebar=0,width=770,height=495,resizable=yes,scrollbars=yes');
           // StgngPolicyId added by swati
           //m_unitWindow = window.open("/RiskmasterUI/UI/PolicyInterface/PS_FormData.aspx?Policynum=" + Policynum + "&val="+val+"&symbol=" + symbol + "&LocationCmpny=" + LocCmpny + "&Master=" + MasterCmpny + "&module=" + module + "&PolSysID=" + PolSystemID + "&InsLn=*AL&UnitNo=" + unitno + "&RskLoc=" + loc + "&SubRskLoc=" + subloc + "&Mode=Enquiry&LOB=" + polLob, "Test", "width =720,height=400,resizable=yes,scrollbars=yes");
           m_unitWindow = window.open("/RiskmasterUI/UI/PolicyInterface/PS_FormData.aspx?Policynum=" + Policynum + "&val=" + val + "&symbol=" + symbol + "&LocationCmpny=" + LocCmpny + "&Master=" + MasterCmpny + "&module=" + module + "&PolSysID=" + PolSystemID + "&InsLn=*AL&UnitNo=" + unitno + "&RskLoc=" + loc + "&SubRskLoc=" + subloc + "&Mode=Enquiry&LOB=" + polLob + "&policyStagingId=" + StgngPolicyId, "Test", "width =720,height=400,resizable=yes,scrollbars=yes");
            return false;
        }
        function OpenUnitFormData() {
            // Pradyumna 03042014 MITS#35296 - Start 
            var m_window
            //var CtrlList, val, m_window;
//            val = "";
//            CtrlList = document.getElementsByTagName("input");
//            for (i = 0; i < CtrlList.length; i++) {
//                if (CtrlList[i].type == "radio" && CtrlList[i].checked == true) {
//                    val = CtrlList[i].value;
//                    break;
//                }
//            }
            // if (val == "") {
            if (selectedUnitVal == "") {
                alert("Please select a record");
            }
            else {
                //m_unitWindow = window.open("/RiskmasterUI/UI/PolicyInterface/PS_FormData.aspx?val=" + val + "&Mode=Enquiry", "Test", "width =720,height=400,resizable=yes,scrollbars=yes");
                m_unitWindow = window.open("/RiskmasterUI/UI/PolicyInterface/PS_FormData.aspx?val=" + selectedUnitVal + "&Mode=Enquiry", "Test", "width =720,height=400,resizable=yes,scrollbars=yes");
            }
            // Pradyumna 03042014 MITS#35296 - End
            return false;


//            var Policynum, Symbol, LocCmpny, MasterCmpny, module, PolSystemID, temp, splitval, loc, subloc, unitno, insln, polLob;
//            ele = document.getElementById('multiunitid');
//            if (ele.selectedIndex == -1) {
//                alert('Please select a unit record to continue');
//                return false;
//            }
//            if (ele.selectedindex != -1) {
//                temp = document.getElementById('multiunitid').value;
////                splitval  = temp.split("|");
////                
////                if (document.getElementById("policyNumber") != null)
////                    Policynum = document.getElementById("policyNumber").innerText;
////                if (document.getElementById("policySymbol") != null)
////                    symbol = document.getElementById("policySymbol").innerText;
////                if (document.getElementById("hdLocatnCompny") != null)
////                    LocCmpny = document.getElementById("hdLocatnCompny").value;
////                if (document.getElementById("hdMasterCompny") != null)
////                    MasterCmpny = document.getElementById("hdMasterCompny").value;
////                if (document.getElementById("policyModule") != null)
////                    module = document.getElementById("policyModule").innerText;
////                if (document.getElementById("hdPolicySystemID") != null)
////                    PolSystemID = document.getElementById("hdPolicySystemID").value;
////                if (document.getElementById("policyLOB") != null)
////                    polLob = document.getElementById("policyLOB").innerText;
////                    unitno = splitval[0];
////                    loc = "";
////                    subloc = "";
////                    insln = splitval[1];
//                // m_unitWindow = window.open("/RiskmasterUI/UI/PolicyInterface/PS_FormData.aspx?Policynum=" + Policynum + "&symbol=" + symbol + "&LocationCmpny=" + LocCmpny + "&Master=" + MasterCmpny + "&module=" + module + "&PolSysID=" + PolSystemID + "&InsLn=" + insln + "&UnitNo=" + unitno + "&RskLoc=" + loc + "&SubRskLoc=" + subloc+"&lob="+polLob, "Test", "width =720,height=400,resizable=yes,scrollbars=yes");
//                m_unitWindow = window.open("/RiskmasterUI/UI/PolicyInterface/PS_FormData.aspx?val="+temp+"&Mode=Enquiry", "Test", "width =720,height=400,resizable=yes,scrollbars=yes");
//                    return false;
//            }

            }
            function OpenAdditionalInterest() {
                var vTemp;
                ele = document.getElementById("lbAdditionalInfo");
                if (ele.selectedIndex == -1) {
                    alert("Please select a record to continue");
                    return false;
                }
                else {
                    vTemp = ele.value;
                }
                m_unitWindow = window.open("/RiskmasterUI/UI/PolicyInterface/PS_PolicyInterest.aspx?val="+vTemp, "PolicyInterest", "width =720,height=400,resizable=yes,scrollbars=yes");
                return false;
            }
            function OpenDriverDetails() {
                var item, vData;
                item = document.getElementById("lbDriverInfo");
                if (item.selectedIndex == -1) {
                    alert("Please select a record to continue");
                    return false;
                }
                else {
                    vData = item.value;
                }
                m_unitWindow = window.open("/RiskmasterUI/UI/PolicyInterface/PS_Driver.aspx?val="+vData, "Driver", "width=700,height=400,resizable=yes,scrollbars=yes");
                return false;
            }
            function OpenUnitAdditionalInterest() {
                // Pradyumna 03042014 MITS#35296 - Start
                // dbisht6 jira rma-369
                var m_window, lossdate;
                lossdate = document.getElementById("hdLossDt");
                //jira rma-369 end
//                var CtrlList, val, m_window;
//                val = "";
//                CtrlList = document.getElementsByTagName("input");
//                for (i = 0; i < CtrlList.length; i++) {
//                    if (CtrlList[i].type == "radio" && CtrlList[i].checked == true) {
//                        val = CtrlList[i].value;
//                        break;
//                    }
//                }
//                if (val == "") {
                if (selectedUnitVal == "") {
                    alert("Please select a record");
                }
                else {
                    //m_unitWindow = window.open("/RiskmasterUI/UI/PolicyInterface/PS_UnitInterestList.aspx?val=" + val, "UnitInterest", "width=725,height=400,top=" + (screen.availHeight - 650) / 2 + ",left=" + (screen.availWidth - 900) / 2 + ",resizable=yes,scrollbars=yes");
                    m_unitWindow = window.open("/RiskmasterUI/UI/PolicyInterface/PS_UnitInterestList.aspx?val=" + selectedUnitVal + "&lossdate=" + lossdate.value, "UnitInterest", "width=725,height=400,top=" + (screen.availHeight - 650) / 2 + ",left=" + (screen.availWidth - 900) / 2 + ",resizable=yes,scrollbars=yes");
                }
                // Pradyumna 03042014 MITS#35296 - End
                return false;
            }

    </script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body class="10pt"  onload="SetValues();"  id="bdTag" >
    <form name="frmData" id="frmData" runat="server">
 
    <asp:hiddenfield runat="server" id="wsrp_rewrite_action_1" value="" />
    <asp:textbox style="display: none" runat="server" name="hTabName" id="hTabName" />
    <asp:scriptmanager id="SMgr" runat="server" />
    <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" text="Policy Tracking" />
        <asp:label id="formsubtitle" runat="server" text="" />
    </div>
 
    <div class="completerow"></div>
    <div class="completerow"></div>
    <div>
    <uc1:errorcontrol id="ErrorControl1" runat="server"  />
    </div>
    <div id="dvParent" runat="server">
    <div class="tabGroup" id="TabsDivGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSpolicyinfo" id="TABSpolicyinfo">
            <a class="Selected" href="#" runat="server" onclick="tabChange(this.name);return false;"
                rmxref="" name="policyinfo" id="LINKTABSpolicyinfo">Policy Information</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPpolicyinfo">
            <nbsp />
            <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSunitinfo" id="TABSunitinfo">
            <a class="NotSelected1" href="#" runat="server" onclick="tabChange(this.name);return false;"
                rmxref="" name="unitinfo" id="LINKTABSunitinfo">Unit Information</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPunitinfo">
            <nbsp />
            <nbsp />
        </div>
        <div class="NotSelected" nowrap="true" runat="server" name="TABSadditionalpolicyinfo" id="TABSadditionalpolicyinfo">
            <a class="NotSelected1" href="#" runat="server" onclick="tabChange(this.name);return false;"
                rmxref="" name="additionalpolicyinfo" id="LINKTABSadditionalpolicyinfo">Policy Interest List</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPadditionalpolicyinfo">
            <nbsp />
            <nbsp />
        </div>
        <div class="NotSelected" id="TABSdriverinfo" nowrap="true" runat="server" name="TABSdriverinfo" style="display:none" >
        <a class="NotSelected1" href="#" runat="server" onclick="tabChange(this.name);return false;"
         rmxref="" name="driverinfo" id="LINKTABSdriverinfo">Driver Information</a>
        </div>
        <div class="tabSpace" id="TBSPdriverinfo" runat="server" style="display:none" >
        <nbsp />
        <nbsp />
        </div>
    </div>
    <div class="singletopborder" runat="server" name="FORMTABpolicyinfo" id="FORMTABpolicyinfo">
        <table width="98%" border="0" cellspacing="8" celpadding="4" padding="0">
            <colgroup>
                <col width="25%" />
                <col width="25%" />
                <col width="25%" />
                <col width="25%" />
            </colgroup>
            <tr>
                <td colspan="4">
                    <asp:hiddenfield runat="server" id="hdpolicyinfo" />
                </td>
            </tr>
            <tr>
            <td>
            <asp:label runat="server" id ="lblmasterCmpny" text="Master Company" class="required"></asp:label>
            </td>
            <td>
            <asp:label runat="server" id ="lblMasterCmpnyValue" ></asp:label>
            </td>
            <td>
            <asp:label runat="server" id="lblLocCmpny" text="Location Company" class="required"></asp:label>
            </td>
            <td>
            <asp:label runat="server" id="lblLocCmpnyValue"></asp:label>
            </td>
            </tr>
                <tr>
            <td>
            <asp:label runat="server" class="required" id="lblInsurer" text="Insurer Details" ></asp:label>
            </td>
            <td align="left" colspan="3">
            <asp:label runat="server" id="lblInsurerData" ></asp:label>
            </td>
            </tr>
            <tr>
                <td>
                    <asp:label runat="server" class="required" id="lblStatus" text="Status" />
                </td>
                <td align="left">
                    <asp:label runat="server" id="policyStatus" rmxtype="text" />
                </td>
                <td>
                    <asp:label runat="server" class="required" id="policyCancelledStatus" visible="False"
                        forecolor="Red" />
                </td>
                <td align="left">
                    <asp:label runat="server" id="policyCancelledDt" class="required" visible="false"
                        forecolor="Red" />
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <asp:label runat="server" class="required" id="lblpolicynumber" text="Policy Number" />
                </td>
                <td align="left">
                    <asp:label runat="server" id="policySymbol" rmxtype="text" />
                    &nbsp;
                    <asp:label runat="server" id="policyNumber" rmxtype="text" />
                    &nbsp;
                    <asp:label runat="server" id="policyModule" rmxtype="text" />
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:label runat="server" class="required" id="lblPolicyeffDate" text="Policy Effective Date" />
                </td>
                <td align="left" valign="top">
                    <asp:label runat="server" id="policyEffDate" rmxtype="text" />
                </td>
                <td>
                    <asp:label runat="server" class="required" id="lblPolicyExpDate" text="Policy Expiration Date" />
                </td>
                <td align="left" valign="top">
                    <asp:label runat="server" id="policyExpDate" rmxtype="text" />
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <asp:label runat="server" class="required" id="lblAgencyNumber" text="Agency Number/Name" />
                </td>
                <td align="left">
                    <asp:label runat="server" id="policyAgencyNumber" rmxtype="text" />
                    <asp:label runat="server" id="policyAgencyName" rmxtype="text" />
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:label runat="server" class="required" id="lblLOB" text="Line Of Business" />
                </td>
                <td align="left">
                    <asp:label runat="server" id="policyLOB" rmxtype="text" />
                </td>
                <td>
                    <asp:label runat="server" class="required" id="lblCustomerNumber" text="Customer Number" />
                </td>
                <td align="left">
                    <asp:label runat="server" id="policyCustomerNumber" rmxtype="text" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:label runat="server" class="required" id="lblIssueDesc" text="Issue Description" />
                </td>
                <td>
                    <asp:label runat="server" id="policyIssueDesc" rmxtype="text" />
                </td>
                <td>
                    <asp:label runat="server" class="required" id="lblPolicyState" text="Policy State" />
                </td>
                <td>
                    <asp:label runat="server" id="policyState" rmxtype="text" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:label runat="server" class="required" id="lblInsured" text="Insured" />
                </td>
                <td align="left">
                    <asp:label runat="server" id="policyInsured" rmxtype="text" />
                </td>
                <td>
                    <asp:label runat="server" class="required" id="lblSortName" text="Sort Name" />
                </td>
                <td align="left">
                    <asp:label runat="server" id="policySortname" rmxtype="text" />
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <asp:label runat="server" class="required" id="lblAddr" text="Address" />
                </td>
                <td align="left">
                    <asp:label runat="server" id="policyAddr" rmxtype="text" />
                </td>
                <td>
                    <asp:label runat="server" class="required" id="lblPremium" text="Premium" />
                </td>
                <td align="left">
                    <asp:label runat="server" id="policyPremium" rmxtype="text" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:label runat="server" class="required" id="lblCity" text="City" />
                </td>
                <td align="left">
                    <asp:label runat="server" id="policyAddrCity" rmxtype="text" />
                </td>
                <td>
                    <asp:label runat="server" class="required" id="lblState" text="State" />
                </td>
                <td align="left">
                    <asp:label runat="server" id="policyAddrState" rmxtype="text" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:label runat="server" class="required" id="lblZipCode" text="Postal Code" />
                </td>
                <td align="left">
                    <asp:label runat="server" id="policyAddrZipCode" rmxtype="text" />
                </td>
                <td>
                <asp:label runat="server" class="required" id="lblInsuredTaxId" text="Insured Tax Id"></asp:label>
                </td>
                <td align="left">
                <asp:label runat="server" id="InsuredTaxId" rmxtype="text"></asp:label>
                </td>
            </tr>
                        <tr>
                <td valign="top">
                         <asp:label runat="server" class="required" id="lblInsuredBirthDate" text="Insured Birth Date" />
                </td>
                <td align="left">
                     <asp:label runat="server" id="InsuredBirthDate" rmxtype="text"></asp:label>
                </td>
                <td valign="top">
                <asp:label runat="server" class="required" id="lblNameType" text="Name Type"></asp:label>
                </td>
                <td>
                <asp:label runat="server" id="policyNameType" rmxtype="text" ></asp:label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:label runat="server" class="required" id="lblBranch" text="Branch" />
                </td>
                <td align="left">
                    <asp:label runat="server" id="policyBranch" rmxtype="text" />
                </td>
                <td>
                    <asp:label runat="server" class="required" id="lblProfitCenter" text="Profit Center" />
                </td>
                <td align="left">
                    <asp:label runat="server" id="policyProfitCenter" rmxtype="text" />
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <asp:label runat="server" class="required" id="lblOptional" text="Optional Name/Address" />
                </td>
                <td align="left">
                    <asp:label runat="server" id="policyOptionalNameAddr" rmxtype="text" />
                </td>
                <td valign="top">
                </td>
                <td>
                </td>
            </tr>
            <tr>
            <td>
            <asp:button runat="server" id="btnFormDataPolicy" text="Form Data" class="button" onclientclick="return OpenFormDataScreen();" />
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            </tr>
        </table>
    </div>
    <div class="singletopborder" runat="server" style="display: none; width: 100%;" name="FORMTABunitinfo"
        id="FORMTABunitinfo">
        <table width="98%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td>
                     <asp:hiddenfield runat="server" id="hdunitinfo" />
                </td>
            </tr>
<%--            <tr>
                <td>
                 <asp:GridView id="gvData" runat="server" AutoGenerateColumns="false" 
             AllowPaging="true" AllowSorting="false" GridLines="both" CellPadding="8" 
             Width="100%" CellSpacing="2" EmptyDataText="No Unit List Found" PageSize="20" onpageindexchanging="gvData_PageIndexChanging" >
             <PagerSettings Mode="NumericFirstLast" PageButtonCount="1"  FirstPageText="First" LastPageText="Last"/>
             <PagerStyle CssClass="Selected" /> 
         <AlternatingRowStyle CssClass="rowdark2" />
         <HeaderStyle ForeColor="White"   />
           <Columns>
           <asp:TemplateField >
           <ItemTemplate >
           <input type="radio" name="rdButton" value='<%# Eval("val") %>' />
           </ItemTemplate>
           </asp:TemplateField>
                    <asp:BoundField DataField="UnitType" HeaderText="Unit Type" ReadOnly="true" ItemStyle-Wrap="true"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="Selected"
                    FooterStyle-CssClass="Selected" />
                    <asp:BoundField DataField="UnitNum" HeaderText="Unit Number" ReadOnly="true" ItemStyle-Wrap="true"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="Selected"
                    FooterStyle-CssClass="Selected" />
                    <asp:BoundField DataField="Status" HeaderText="Status" ReadOnly="true" ItemStyle-Wrap="true"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="Selected"
                    FooterStyle-CssClass="Selected" />
                    <asp:BoundField DataField="Prem" HeaderText="Premium" ReadOnly="true" ItemStyle-Wrap="true"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="Selected"
                    FooterStyle-CssClass="Selected" />
                    <asp:BoundField DataField="Desc" HeaderText="Description" ReadOnly="true" ItemStyle-Wrap="true"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="Selected"
                    FooterStyle-CssClass="Selected" />
                    <asp:BoundField DataField="InsLine" HeaderText="Ins Line" ReadOnly="true" ItemStyle-wrap="true" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="Selected"
                    FooterStyle-CssClass="Selected" />
                    <asp:BoundField DataField="VIN" HeaderText="VIN" ReadOnly="true" ItemStyle-Wrap="true"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="Selected"
                    FooterStyle-CssClass="Selected" />
                    <asp:BoundField DataField="MakeYear" HeaderText="Make Year" ReadOnly="true" ItemStyle-Wrap="true"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="Selected"
                    FooterStyle-CssClass="Selected" />
                    <asp:BoundField DataField="Addr" HeaderText="Address" ReadOnly="true" ItemStyle-Wrap="true"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="Selected"
                    FooterStyle-CssClass="Selected" />
                    <asp:BoundField DataField="val" Visible="false" />
           </Columns>
                     
        </asp:GridView>--%>
                   <%-- <div runat="server" class="half" id="div_multiunitid" xmlns="">
                     
                        <asp:label runat="server" class="required" id="lbl_multiunitid" text="Unit Name" />
                        <span class="formw">
                            <asp:listbox runat="server" id="multiunitid" readonly="true" width="250px" />
                        </span>
                    </div> 
                </td>
            </tr> --%>
            <!-- Pradyumna 02252014 MITS#35296 - Start-->
            <tr>
                <td>
                <span>
                <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server" />
                    <telerik:RadCodeBlock runat="server" ID="radCodeBlock">
                         <script type="text/javascript">
                             function GetSelectedRows() {
                                 masterTable = $find("<%=gvUnitList.ClientID %>").get_masterTableView();
                                 var selectedRows = masterTable.get_selectedItems();
                                 if (selectedRows != null && selectedRows != undefined) {
                                     return selectedRows[0];
                                 }
                             }
                             function RowSelected(sender, args) {
                                 selectedUnitVal = args.getDataKeyValue("val"); 
                             }
                         </script>
                    </telerik:RadCodeBlock>
                     <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                        
                    </telerik:RadAjaxManager>

                    <telerik:RadGrid runat="server" ID="gvUnitList" AllowPaging="true" PageSize="20" GridLines="None"
                    AllowFilteringByColumn="true" AllowSorting="true" AutoGenerateColumns="false" Skin="Office2007" 
                    OnNeedDataSource="gvUnitList_NeedDataSource" OnItemDataBound="gvUnitList_ItemDataBound">
                    <SelectedItemStyle CssClass="SelectedItem" />
                    <ClientSettings AllowKeyboardNavigation="true">
                        <ClientEvents OnRowSelected="RowSelected"/>                    
                        <Scrolling SaveScrollPosition="true" AllowScroll="false" />
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>

                    <MasterTableView Width="100%" AllowCustomSorting="false" ClientDataKeyNames="val">
                         <PagerStyle AlwaysVisible="true" Position="Top" mode="NextPrevAndNumeric"/>
                            <RowIndicatorColumn>
                                <HeaderStyle Width="20px" />
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn>
                                <HeaderStyle Width="20px" />
                            </ExpandCollapseColumn>
                             <NoRecordsTemplate>
                              <div>
                                There are no records to display.
                              </div>
                            </NoRecordsTemplate>
                        <Columns>
                            <telerik:GridTemplateColumn UniqueName="grdRadio" ItemStyle-Width="5%" AllowFiltering="false">
                                <ItemTemplate>
                                    <asp:RadioButton ID="gdRadio" runat="server" AutoPostBack="false"/>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataField="UnitType" ItemStyle-Width="10%" FilterControlWidth="80%" HeaderText="Unit Type" UniqueName="UnitType">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="UnitNum" ItemStyle-Width="10%" FilterControlWidth="80%" HeaderText="Unit Number" UniqueName="UnitNum">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Status" ItemStyle-Width="10%" FilterControlWidth="50%" HeaderText="Status" UniqueName="Status">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Prem" ItemStyle-Width="10%" FilterControlWidth="50%" HeaderText="Premium" UniqueName="Prem">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Desc" ItemStyle-Width="15%" FilterControlWidth="80%" HeaderText="Description" UniqueName="Desc">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="InsLine" ItemStyle-Width="10%" FilterControlWidth="50%" HeaderText="Ins Line" UniqueName="InsLine">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="VIN" ItemStyle-Width="10%" FilterControlWidth="80%" HeaderText="VIN" UniqueName="VIN">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="MakeYear" ItemStyle-Width="10%" FilterControlWidth="50%" HeaderText="Make Year" UniqueName="MakeYear">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Addr" ItemStyle-Width="15%" FilterControlWidth="80%" HeaderText="Address" UniqueName="Addr">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="val" HeaderText="val" UniqueName="val" Visible="false">
                            </telerik:GridBoundColumn>          
                        </Columns>
                        <ItemStyle CssClass="datatd1" />
                        <AlternatingItemStyle CssClass="datatd" />
                        <HeaderStyle CssClass="msgheader" />
                    </MasterTableView>
                </telerik:RadGrid>
                    </span>                 
                </td>
            </tr>
            <!-- Pradyumna 02252014 MITS#35296 - End-->
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div_linebreak1" xmlns="">
                        <span class="formw">
                            <br />
                        </span>
                    </div>
                    <div runat="server" class="completerow" id="div_linebreak2" xmlns="">
                        <span class="formw">
                            <br />
                        </span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div_linebreak3" xmlns="">
                        <span class="formw">
                            <br />
                        </span>
                    </div>
                    <div runat="server" class="completerow" id="div_linebreak4" xmlns="">
                        <span class="formw">
                            <br />
                        </span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" id="div_singlerow1" xmlns="">
                        <span class="formw">
                               <asp:button class="button" runat="server" id="btnUnitDetails" text="Unit Details"
                                    onclientclick="return OpenUnit();" />
                        </span>
                        <span class="formw">
                            <asp:button class="button" runat="Server" id="btnUnitCov" text="Unit Coverages" onclientclick="return OpenCoverage();" />
                        </span>
                        <span class="formw">
                            <asp:button class="button" runat="Server" id="btnUnitInterest" text="Unit Additional Interest" onclientclick="return OpenUnitAdditionalInterest();"  />
                        </span>
                        <span class="formw">
                            <asp:button class="button" runat="Server" id="btnFormDataUnit" text="Form Data" onclientclick="return OpenUnitFormData();"  />
                        </span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div_linebreak5" xmlns="">
                        <span class="formw">
                            <br />
                        </span>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="singletopborder" runat="server" style="display: none;" name="FORMTABadditionalpolicyinfo"
        id="FORMTABadditionalpolicyinfo">
        <table width="98%" border="0" cellspacing="0" celpadding="0">
            <tr>
                <td>
                    <asp:hiddenfield runat="server" id="hdadditionalpolicyinfo" />
                </td>
            </tr>
          
            <tr>
                <td>
                    <div runat="server" class="half" id="div2" xmlns="">
                        <asp:label runat="server" class="required" id="lbl_AdditionalInfo" text="Additional Details" />
                        <span class="formw">
                            <asp:listbox runat="server" id="lbAdditionalInfo" readonly="true" width="250px" />
                        </span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div3" xmlns="">
                        <span class="formw">
                            <br />
                        </span>
                    </div>
                    <div runat="server" class="completerow" id="div4" xmlns="">
                        <span class="formw">
                            <br />
                        </span>
                    </div>
                </td>
            </tr>
            <tr>
            <td>
            <asp:label runat="server" id="lblAdditionalPolicyMsg" style="font-weight:bold" text="This policy has no interest list information." visible="false"></asp:label>  
            </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div5" xmlns="">
                        <span class="formw">
                            <br />
                        </span>
                    </div>
                    <div runat="server" class="completerow" id="div6" xmlns="">
                        <span class="formw">
                            <br />
                        </span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                        <div runat="server" class="onecontrol" id="div9">
                            <span class="formw">
                                <asp:button class="button" runat="server" id="btnAdditionalPolicyDetails" text="Interest Details" onclientclick="return OpenAdditionalInterest()" />
                            </span>
                        </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div7" xmlns="">
                        <span class="formw">
                            <br />
                        </span>
                    </div>
                </td>
            </tr>
         </table>
    </div>
    <div class="singletopborder" id="FORMTABdriverinfo" runat="server" name="FORMTABdriverinfo" style="display: none;" >
        <table width="98%" border="0" cellspacing="0" celpadding="0" >
            <tr>
                <td>
                    <asp:hiddenfield runat="server" id="hddriverinfo" />
                </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="half" id="div8" xmlns="">
                        <asp:label runat="server" class="required" id="lblDriverInfo" text="Driver Details" />
                        <span class="formw">
                            <asp:listbox runat="server" id="lbDriverInfo" readonly="true"  width="250px" />
                        </span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div10" xmlns="">
                        <span class="formw">
                            <br />
                        </span>
                    </div>
                    <div runat="server" class="completerow" id="div11" xmlns="">
                        <span class="formw">
                            <br />
                        </span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div12" xmlns="">
                        <span class="formw">
                            <br />
                        </span>
                    </div>
                    <div runat="server" class="completerow" id="div13" xmlns="">
                        <span class="formw">
                            <br />
                        </span>
                    </div>
                </td>
            </tr>
           <tr>
                <td>
                        <div runat="server" class="onecontrol" id="div14">
                            <span class="formw">
                                <asp:button class="button" runat="server" id="btnDriverDetails" text="Driver Details" onclientclick="return OpenDriverDetails()" />
                            </span>
                        </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div15" xmlns="">
                        <span class="formw">
                            <br />
                        </span>
                    </div>
                </td>
            </tr>
         
        </table>
    </div>
    </div>
    <asp:hiddenfield runat="server" id="hdLocatnCompny" value="" />
    <asp:hiddenfield runat="server" id="hdMasterCompny" value="" /> 
    <asp:hiddenfield runat="server" id="hdPolicySystemID" value="" />
    <asp:hiddenfield runat="server" id="hdIssueCd" value="" />
    <asp:hiddenfield runat="server" id="hdPolCmpny" value="" />
    <asp:hiddenfield runat="server" id="hdPolState" value="" />
    <asp:hiddenfield runat="server" id="hdLossDt" value="" />
     <!-- Swati: WWIG GAP16 MITS 33414 01/09/2013 - Start -->     
     <asp:HiddenField ID="hdnStagingPolicyId" runat="server" Value="" />
     <asp:HiddenField ID="hdnRemoveEmtpyFields" runat="server" Value="" />
     <!-- Swati: WWIG GAP16 MITS 33414 01/09/2013 - End -->
     <!-- Pradyumna 03052014 MITS#35296 - Start-->
     <asp:HiddenField ID="hdnClickUnitInfo" runat="server" Value="" />
     <asp:HiddenField ID="hdnClaimReportedDate" runat="server" Value="" />
     <!-- Pradyumna 03052014 MITS#35296 - End-->
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    </form>
</body>
</html>
