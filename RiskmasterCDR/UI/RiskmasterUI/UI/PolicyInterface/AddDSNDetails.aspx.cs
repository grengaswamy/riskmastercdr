﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml;
using System.Text;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.UI.PolicyInterface
{
    public partial class AddDSNDetails : NonFDMBasePageCWS
    {
        public XElement oResponse = null;
        string sReturn = string.Empty;
        private XmlDocument oFDMPageDom = null;
       protected void Page_Load(object sender, EventArgs e)
        {

            if (DataSourceSelectedIndex != -1)
            {
                ViewState["DataSourceSelectedIndex"] = DataSourceSelectedIndex;
            }
            if (txtDataDSNPwd.Text != "")
            {
                ViewState["DataDSNPwd"] = txtDataDSNPwd.Text;
                if (!String.IsNullOrEmpty(txtDataDSNPwd.Attributes["value"]))
                {
                    txtDataDSNPwd.Attributes["value"] = ViewState["DataDSNPwd"].ToString();
                }
                else
                {
                    txtDataDSNPwd.Attributes.Add("value", ViewState["DataDSNPwd"].ToString());
                }
            }
            else
            {
                if (!String.IsNullOrEmpty(txtDataDSNPwd.Attributes["value"]))
                {
                    txtDataDSNPwd.Text = txtDataDSNPwd.Attributes["value"].ToString();
                }
            }
            if (!Page.IsPostBack)
            {    
                //Preparing XML to send to service
                XElement oMessageElement = GetDataSourceTemplate();
                //Call service to get initial data
                sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
                ErrorControl1.errorDom = sReturn;

                hdnDSNType.Value = AppHelper.GetQueryStringValue("DSNType");
                if (!string.IsNullOrEmpty(AppHelper.GetQueryStringValue("Mode")) && AppHelper.GetQueryStringValue("Mode").ToLower() == "edit")
                {
                    BindControls(sReturn);
                }
                WizardStep1_BindGridToXml(sReturn, Wizardstep1_chkDSN.Checked);
                Wizardstep1_SetHeader();
            }

        }

        /// <summary>
        /// CWS request message template for fetching child nodes
        /// </summary>
        /// <returns></returns>


        private int DataSourceSelectedIndex
        {
            get
            {
                if (string.IsNullOrEmpty(Request.Form["DataSourceGroup"]))
                    return -1;
                else
                    return Convert.ToInt32(Request.Form["DataSourceGroup"]);
            }
        }

        protected void grdLookUp_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                int iDataSourceSelectedIndex = DataSourceSelectedIndex;
                if (iDataSourceSelectedIndex == -1 && ViewState["DataSourceSelectedIndex"] != null)
                {
                    iDataSourceSelectedIndex = Convert.ToInt32(ViewState["DataSourceSelectedIndex"]);
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Cells[e.Row.Cells.Count - 1].Visible = false;

                    if (((e.Row.RowIndex + 1) % 2) == 0)
                    {
                        e.Row.CssClass = "rowdark2";
                    }
                    // Grab a reference to the Literal control
                    Literal output = (Literal)e.Row.FindControl("RadioButtonMarkup");
                    // Output the markup except for the "checked" attribute 
                    output.Text = string.Format(@"<input type='radio' onclick='enableNext();' name='DataSourceGroup' " + @"id='RowSelector{0}' value='{0}' ", e.Row.RowIndex);

                    // See if we need to add the "checked" attribute
                    if (iDataSourceSelectedIndex == e.Row.RowIndex)
                    {
                        output.Text += @" checked='checked'";
                        Button btn = (Button)DataSourceWizard.FindControl("StartNavigationTemplateContainerID").FindControl("StartNextButton");
                        btn.Enabled = true;

                    }

                    // Add the closing tag 
                    output.Text += " />";
                }

                if (e.Row.RowType == DataControlRowType.Header)
                {
                    e.Row.Cells[e.Row.Cells.Count - 1].Visible = false;
                    e.Row.CssClass = "colheader3";
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }

        }
        protected void RefreshStep1(object sender, EventArgs e)
        {
            try
            {
                ViewState["DataSourceSelectedIndex"] = "-1";
                Button btn = (Button)DataSourceWizard.FindControl("StartNavigationTemplateContainerID").FindControl("StartNextButton");
                btn.Enabled = false;
                if (Wizardstep1_chkDSN.Checked)
                {
                    Wizardstep1_hdAction.Value = "PredefinedDSN";
                }
                else
                    Wizardstep1_hdAction.Value = "ODBCDrivers";

                //Preparing XML to send to service
                XElement oMessageElement = GetDataSourceTemplate();

                XElement oFormNameElement = oMessageElement.XPathSelectElement("./Document/form");
                if (oFormNameElement != null && oFormNameElement.Attribute("name") != null)
                {
                    oFormNameElement.Attribute("name").Value = Wizardstep1_hdAction.Value;
                }


                //Call service to get initial data
                sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
                ErrorControl1.errorDom = sReturn;

                //Verify if this is the first step
                if (DataSourceWizard.ActiveStepIndex == 0)
                {
                    WizardStep1_BindGridToXml(sReturn, Wizardstep1_chkDSN.Checked);

                    oResponse = XElement.Parse(sReturn);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
        private void WizardStep1_BindGridToXml(string sReturnValue, bool p_bPreDefinedDSN)
        {
            DataRow drLookUpRow = null;
            DataTable dtLookUp = new DataTable();

            XElement objDataSource = XElement.Parse(sReturnValue);

            if (p_bPreDefinedDSN)
            {
                dtLookUp.Columns.Add(new DataColumn("Name"));
                dtLookUp.Columns.Add(new DataColumn("Driver"));
                dtLookUp.Columns.Add("pid");
            }
            else
            {
                dtLookUp.Columns.Add(new DataColumn("Name"));
                dtLookUp.Columns.Add(new DataColumn("Version"));
                dtLookUp.Columns.Add(new DataColumn("Company"));
                dtLookUp.Columns.Add("pid");
            }

            var objRowXml = from rows in objDataSource.XPathSelectElement("./Document/form/groupDB").Descendants("optionDB")

                            select rows;

            string[] arrPid = new string[1];

            string sDescendant = string.Empty;

            if (p_bPreDefinedDSN)
            {
                foreach (XElement row in objRowXml)
                {
                    arrPid[0] = "pid";
                    grdLookUp.DataKeyNames = arrPid;

                    drLookUpRow = dtLookUp.NewRow();
                    drLookUpRow["Name"] = row.Attribute("name").Value;
                    drLookUpRow["Driver"] = row.Attribute("company").Value;
                    drLookUpRow["pid"] = row.Attribute("value").Value;
                    dtLookUp.Rows.Add(drLookUpRow);
                }
                ViewState["DSNXml"] = objDataSource.XPathSelectElement("./Document/form/groupDB/displaycolumn/control[@name='optDSNs']").ToString();
            }
            else
            {
                foreach (XElement row in objRowXml)
                {
                    arrPid[0] = "pid";
                    grdLookUp.DataKeyNames = arrPid;

                    drLookUpRow = dtLookUp.NewRow();
                    drLookUpRow["Name"] = row.Attribute("name").Value;
                    drLookUpRow["Version"] = row.Attribute("version").Value;
                    drLookUpRow["Company"] = row.Attribute("company").Value;
                    drLookUpRow["pid"] = row.Attribute("value").Value;
                    dtLookUp.Rows.Add(drLookUpRow);
                }
                ViewState["DSNXml"] = objDataSource.XPathSelectElement("./Document/form/groupDB/displaycolumn/control[@name='optservers']").ToString();

            }

            // Bind DataTable to GridView.
            grdLookUp.Visible = true;
            grdLookUp.DataSource = dtLookUp;
            grdLookUp.DataBind();

        }

        private void Wizardstep1_SetHeader()
        {

            if (Wizardstep1_chkDSN.Checked)
                Wizardstep1_Header.Text = "Select the ODBC Data Source to Use to Connect to the Database:";
            else
                Wizardstep1_Header.Text = "Select the ODBC Driver for the Database you want to connect to: For Microsoft Access clients, you need to use predefined ODBC Datasources.";
        }
        protected void onNextClick(object sender, EventArgs e)
        {
            try
            {
                switch (DataSourceWizard.ActiveStepIndex)
                {
                    case 0:
                        if (Wizardstep1_chkDSN.Checked)
                        {
                            div_inputServerName.Visible = false;
                            div_inputDatabaseName.Visible = false;
                            // div_chkAddParams.Visible = false;
                            // inputLogin.Focus();
                        }
                        else
                        {
                            div_inputServerName.Visible = true;
                            div_inputDatabaseName.Visible = true;
                            txtDataDSNServerNm.Focus();
                        }
                        //Changing Image
                        ((HtmlImage)DataSourceWizard.FindControl("SideBarContainer").FindControl("dsnimage")).Src = "../../images/dsn1.gif";
                        break;

                    case 1:
                        //Changing Image
                        ((HtmlImage)DataSourceWizard.FindControl("SideBarContainer").FindControl("dsnimage")).Src = "../../images/dsn1.gif";
                        break;


                    case 2:
                        //Preparing XML to send to service
                        XElement oMessageElement = GetDataSourceValidationTemplate();

                        //Modify XML
                        FillDataSourceValidationTemplate(oMessageElement);

                        //Call service to get initial data
                        sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
                        ErrorControl1.errorDom = sReturn;

                        XElement oResponse = XElement.Parse(sReturn);
                        XElement oEle = oResponse.XPathSelectElement("./Document/form/dsnmessage");
                        if (oEle != null && oEle.Attribute("iserror") != null)
                        {
                            wizardstep4_iserror.Value = oEle.Attribute("iserror").Value;
                            wizardstep4_message.Text = ErrorHelper.UpdateErrorMessage(oEle.Attribute("message").Value);
                            if (wizardstep4_iserror.Value.ToLower() == "yes")
                            {
                                //Changing Image
                                ((HtmlImage)DataSourceWizard.FindControl("SideBarContainer").FindControl("dsnimage")).Src = "../../images/dsn1.gif";
                                wizardstep4_message.CssClass = "errtext1";
                            }
                            else
                            {
                                wizardstep4_message.CssClass = "";
                                //Changing Image
                                ((HtmlImage)DataSourceWizard.FindControl("SideBarContainer").FindControl("dsnimage")).Src = "../../images/dsn2.gif";

                                //No error:store connection string in ViewState
                                oEle = oResponse.XPathSelectElement("./Document/form/keys/resultkeys/connkey");
                                if (oEle != null)
                                {
                                    ViewState["connectionstring"] = oEle.Value;
                                    connStr.Value = Riskmaster.Security.Encryption.RMCryptography.EncryptString(oEle.Value);
                                }

                            }
                        }

                        break;

                    case 3:
                        Button btn = (Button)DataSourceWizard.FindControl("StepNavigationTemplateContainerID").FindControl("StepNextButton");
                        btn.Enabled = true;
                        break;
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
        protected void onPreviousClick(object sender, EventArgs e)
        {
            if (DataSourceWizard.ActiveStepIndex <= 3)
            {
                //Changing Image
                ((HtmlImage)DataSourceWizard.FindControl("SideBarContainer").FindControl("dsnimage")).Src = "../../images/dsn1.gif";
            }
        }
        private void BindControls(string sReturnValue)
        {
            XElement XmlTemplate = null;
            string sCWSresponse = "";
            XmlTemplate = GetMessageTemplate();
            CallCWS("SetUpPolicySystemAdaptor.GetById", XmlTemplate, out sCWSresponse, false, false);
            XmlDocument p_objDoc = new XmlDocument(); 
            p_objDoc.LoadXml(sReturnValue);
            
           
            oFDMPageDom = new XmlDocument();
            oFDMPageDom = Data;
            XmlElement objPolicySysXMLEle = null;
            objPolicySysXMLEle = (XmlElement)oFDMPageDom.SelectSingleNode("//PolicySys");
            //hdnDataDSN.Value = Request.Form["txtDataDSN"].ToString();
            //hdnDataDSNUser.Value = Request.Form["txtDataDSNUser"].ToString();
            //hdnDataDSNPwd.Value = Request.Form["txtDataDSNPwd"].ToString();
            //hdnDataDSNDriverNm.Value = Request.Form["txtdataDSNDriverNm"].ToString();
            //hdnPolicySystemId.Value = Request.Form["RowId"].ToString();
           hdnPolicySystemId.Value = AppHelper.GetQueryStringValue("PolicySystemId") == string.Empty ? "0" : AppHelper.GetQueryStringValue("PolicySystemId");
           if (objPolicySysXMLEle.GetElementsByTagName("DataDSN").Count != 0)
                txtDataDSN.Text = objPolicySysXMLEle.GetElementsByTagName("DataDSN").Item(0).InnerText;

            if (objPolicySysXMLEle.GetElementsByTagName("DataDSNUser").Count != 0)
                txtDataDSNUser.Text = objPolicySysXMLEle.GetElementsByTagName("DataDSNUser").Item(0).InnerText;

            if (objPolicySysXMLEle.GetElementsByTagName("DataDSNPwd").Count != 0)
            {
                txtDataDSNPwd.Attributes.Add("value", "##########");
            }

            //if (objPolicySysXMLEle.GetElementsByTagName("SupportDSN").Count != 0)
            //    txtSupportDSN.Text = objPolicySysXMLEle.GetElementsByTagName("SupportDSN").Item(0).InnerText;

            //if (objPolicySysXMLEle.GetElementsByTagName("SupportDSNUser").Count != 0)
            //    txtSupportDSNUser.Text = objPolicySysXMLEle.GetElementsByTagName("SupportDSNUser").Item(0).InnerText;

            //if (objPolicySysXMLEle.GetElementsByTagName("SupportDSNPwd").Count != 0)
            //    txtSupportDSNPwd.Attributes.Add("value", "##########");

            //if (objPolicySysXMLEle.GetElementsByTagName("DataDriverNm").Count != 0)
            //    hdnDataDSNDriverNm.Text = objPolicySysXMLEle.GetElementsByTagName("DataDriverNm").Item(0).InnerText;

            if (objPolicySysXMLEle.GetElementsByTagName("DataServerNm").Count != 0)
                txtDataDSNServerNm.Text = objPolicySysXMLEle.GetElementsByTagName("DataServerNm").Item(0).InnerText;

            if (objPolicySysXMLEle.GetElementsByTagName("DataLibraryList").Count != 0)
                txtDataDSNLibLst.Text = objPolicySysXMLEle.GetElementsByTagName("DataLibraryList").Item(0).InnerText;

            //if (objPolicySysXMLEle.GetElementsByTagName("DataDSNConnStr").Count != 0)
            //    txtDataDSNConnstr.Text = objPolicySysXMLEle.GetElementsByTagName("DataDSNConnStr").Item(0).InnerText;
            //txtDataDSN.Text = Request.Form["txtDataDSN"].ToString();
            //txtDataDSNUser.Text = Request.Form["txtDataDSNUser"].ToString();
            //txtDataDSNPwd.Text = "##########";
            //txtDataDSNPwd.Attributes.Add("value", "##########");
            //txtDataDSNServerNm.Text = Request.Form["txtDataDSNServerNm"].ToString();
            //txtDataDSNLibLst.Text = Request.Form["txtDataDSNLibLst"].ToString();

            if (objPolicySysXMLEle.GetElementsByTagName("DataDriverNm").Item(0) != null && !string.IsNullOrEmpty(objPolicySysXMLEle.GetElementsByTagName("DataDriverNm").Item(0).InnerText))
            {
                XmlElement objTemp1 = (XmlElement)p_objDoc.SelectSingleNode("//control[@name='optservers']");
                XmlElement objTemp2 = (XmlElement)p_objDoc.SelectSingleNode("//optionDB[@name='" + objPolicySysXMLEle.GetElementsByTagName("DataDriverNm").Item(0).InnerText + "']");
                if (objTemp2 != null && objTemp1 != null)
                {
                    objTemp1.Attributes["value"].Value = objTemp2.Attributes["value"].Value;
                    ViewState["DataSourceSelectedIndex"] = Conversion.ConvertStrToInteger(objTemp2.Attributes["value"].Value) - 1;
                }
                //Change Wizard Title
                DataSourceWizard.HeaderText = "Edit the DataSource";
            }
        }

        private void FillDataSourceValidationTemplate(XElement oMessageElement)
        {
            XElement oElement = null;
            string sDsn = string.Empty;
            oElement = oMessageElement.XPathSelectElement("./Document/form/keys/loginscreenkeys/inputservername");
            if (oElement != null)
            {
                oElement.Value = txtDataDSNServerNm.Text;
            }
            oElement = oMessageElement.XPathSelectElement("./Document/form/keys/loginscreenkeys/dsntype");
            if (oElement != null)
            {
                oElement.Value = AppHelper.GetQueryStringValue("DSNType");
            }
            oElement = oMessageElement.XPathSelectElement("./Document/form/keys/loginscreenkeys/inputdatabasename");
            if (oElement != null)
            {
                oElement.Value = txtDataDSN.Text;
            }
            oElement = oMessageElement.XPathSelectElement("./Document/form/keys/loginscreenkeys/inputlibrarylist");
            if (oElement != null)
            {
                oElement.Value = txtDataDSNLibLst.Text;
            }
            oElement = oMessageElement.XPathSelectElement("./Document/form/keys/loginscreenkeys/inputloginuser");
            if (oElement != null)
            {
                oElement.Value = txtDataDSNUser.Text;
            }
            oElement = oMessageElement.XPathSelectElement("./Document/form/keys/loginscreenkeys/inputpassword");
            if (oElement != null)
            {
                oElement.Value = txtDataDSNPwd.Text;
            }
            oElement = oMessageElement.XPathSelectElement("./Document/form/keys/loginscreenkeys/IsDsn");
            if (oElement != null)
            {
                if (Wizardstep1_chkDSN.Checked)
                {
                    oElement.Value = "Yes";
                }
                else
                {
                    oElement.Value = "No";
                }

            }
            oElement = oMessageElement.XPathSelectElement("./Document/form/keys/DBkey/displaycolumn");
            if (oElement != null)
            {
                XElement child = XElement.Parse(ViewState["DSNXml"].ToString());
                oElement.Add(child);
            }
            oElement = oMessageElement.XPathSelectElement("./Document/form/keys/DBkey/displaycolumn/control[@name='optservers']/optionDB[@value='" + (Convert.ToInt32(ViewState["DataSourceSelectedIndex"].ToString()) + 1).ToString() + "']");
            if (oElement != null)
            {
                sDsn = oElement.Value;
            }
            oElement = oMessageElement.XPathSelectElement("./Document/form/keys/loginscreenkeys/inputdrivername");
            if (oElement != null)
            {
                oElement.Value = sDsn;
            }
            if (Wizardstep1_chkDSN.Checked)
            {
                oElement = oMessageElement.XPathSelectElement("./Document/form/keys/DBkey/displaycolumn/control[@name='optDSNs']");
            }
            else
            {
                oElement = oMessageElement.XPathSelectElement("./Document/form/keys/DBkey/displaycolumn/control[@name='optservers']");
            }
            if (oElement != null && oElement.Attribute("value") != null)
            {
                oElement.Attribute("value").Value = (Convert.ToInt32(ViewState["DataSourceSelectedIndex"].ToString()) + 1).ToString();
            }

            oElement = oMessageElement.XPathSelectElement("./Document/form/keys/DBkey/displaycolumn/control[@name='hdnPolicySystemId']");
            if (oElement != null)
            {
                oElement.Attribute("value").Value = hdnPolicySystemId.Value;
            }

        }
        /// <summary>
        /// CWS request message template for validating connection.
        /// </summary>
        /// <returns></returns>
        private XElement GetDataSourceValidationTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization>45990435-011c-4789-ad28-8a2c95bae261</Authorization> 
                <Call>
                    <Function>SetUpPolicySystemAdaptor.ValidatePolicyDSN</Function> 
                </Call>
                <Document>
                    <form class='10pt' name='dsnwizardprocesslogin' title='DSN Wizard'>
                    <dsnmessage iserror='' message='' mode=''>
                        <group name='UserSettings' title='Please Enter Necessary Data to Connect to Database'>
                        </group>
                    </dsnmessage>
                    <keys>
                        <DBkey>
                            <displaycolumn>
                                  <control name='hdnPolicySystemId' type='hidden' value='' /> 
                          </displaycolumn>
                        </DBkey>
                        <loginkey /> 
                        <loginscreenkeys>
                              <inputdrivername></inputdrivername>
                              <inputservername></inputservername> 
                              <inputdatabasename></inputdatabasename> 
                              <inputlibrarylist></inputlibrarylist>
                              <inputloginuser></inputloginuser> 
                              <inputpassword></inputpassword> 
                              <dsntype></dsntype>
                        </loginscreenkeys>
                        <resultkeys>
                            <connkey /> 
                        </resultkeys>
                    </keys>
                </form>
            </Document>
        </Message>

        
            ");
            return oTemplate;
        }

        /// <summary>
        /// CWS request for fetching database driver's information.
        /// </summary>
        /// <returns></returns>
        private XElement GetDataSourceTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization>1f5c22e4-6909-49bb-b541-061e650a5d79</Authorization> 
              <Call>
                  <Function>SecurityAdaptor.GetXMLData</Function> 
              </Call>
              <Document>
                  <form class='10pt' name='ODBCDrivers' title='ODBCDrivers'>
                      <groupDB name='dsnwizard' title='Select the ODBC Driver for the Database you want to connect to: For Microsoft Access clients, you need to use predefined ODBC Datasources.'>
                          <displaycolumn>
                              <control checked='' name='optservers' title='' type='radioDB' value='' page='PolicySystemAddDSN' /> 
                              <control checked='' name='optDSNs' type='radioDB' value='' />
                              <control name='chkDSN' onclick='chkDSNWizard()' title='' type='checkbox' /> 
                              <control name='txtLabel' title='Use Secure DSN Login' type='textlabel'>Use Predefined ODBC Datasource to Connect to Database</control> 
                              <control name='hdAction' type='hidden' value='' /> 
                              <control name='hdView' type='hidden' value='' /> 
                              <control name='hdInvokedFrom' type='hidden' value='' /> 
                              <control name='hdEditDsnId' type='hidden' value='' /> 
                          </displaycolumn>
                      </groupDB>
                      <EditDsnKeys>
                          <control name='inputServerName' title='Server Name' type='text' /> 
                          <control name='inputDatabaseName' title='Database Name' type='text' /> 
                          <control name='inputLogin' title='Login User Name' type='text' /> 
                          <control name='inputPassword' title='Login Password' type='text' /> 
                          <control name='inputDatasourcename' title='Login Password' type='text' /> 
                          <control name='chkAddParams' title='Login Password' type='text' /> 
                          <control name='txtAreaAddParams' title='Login Password' type='text' value='' /> 
                      </EditDsnKeys>
                 </form>
              </Document>
             </Message>
        
            ");
            return oTemplate;
        }

        private XElement GetMessageTemplate()
        {
            XElement oTemplate = null;
            if (!string.IsNullOrEmpty(AppHelper.GetQueryStringValue("PolicySystemId")))
            {
                StringBuilder sXml = new StringBuilder("<Message>");
                sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
                sXml = sXml.Append("<Call><Function>");
                sXml = sXml.Append("SetUpPolicySystemAdaptor.GetById");
                sXml = sXml.Append("</Function></Call><Document><PolicyInterface><FunctionToCall/><PolicySys>");
                sXml = sXml.Append("<PolicySysName></PolicySysName>");
                sXml = sXml.Append("<MappingTablePrefix></MappingTablePrefix>");
                sXml = sXml.Append("<Domain></Domain>");
                sXml = sXml.Append("<License></License>");
                sXml = sXml.Append("<Proxy></Proxy>");
                sXml = sXml.Append("<UserName></UserName>");
                sXml = sXml.Append("<Password></Password>");
                sXml = sXml.Append("<URLParam></URLParam>");
                sXml = sXml.Append("<FinancialUpdate></FinancialUpdate>");
                sXml = sXml.Append("<RealTimeUpload></RealTimeUpload>"); //Payal :RMA:7506
                sXml = sXml.Append("<DefaultPolicy></DefaultPolicy>");
                sXml = sXml.Append("<policysystemid>" + AppHelper.GetQueryStringValue("PolicySystemId") + "</policysystemid>");
                sXml = sXml.Append("<PolicySystemType codeid='' ></PolicySystemType>");
                sXml = sXml.Append("<TargetDatabase></TargetDatabase>");
                sXml = sXml.Append("<ClientFileFlag></ClientFileFlag>");
                sXml = sXml.Append("<Version> </Version>");
                sXml = sXml.Append("<Versions Type=''></Versions>");
                sXml = sXml.Append("<UseRMAUserForCFW></UseRMAUserForCFW>");
                sXml = sXml.Append("<CFWUserName></CFWUserName>");
                sXml = sXml.Append("<CFWUserPassword></CFWUserPassword>");
                sXml = sXml.Append("<PointURL></PointURL>");
                sXml = sXml.Append("<LocCompny></LocCompny>");
                sXml = sXml.Append("<MasterCompany></MasterCompany>");
                sXml = sXml.Append("<BusinessEntities></BusinessEntities>");
                sXml = sXml.Append("<MajorPerils></MajorPerils>");
                sXml = sXml.Append("<IntertestList></IntertestList>");
                sXml = sXml.Append("<DataDSNConnStr></DataDSNConnStr>");
                sXml = sXml.Append("<ClientDSNConnStr></ClientDSNConnStr>");
                sXml = sXml.Append("<StagingConn></StagingConn>");
                sXml = sXml.Append("<DSNType>" + AppHelper.GetQueryStringValue("DSNType") + "</DSNType>");
                sXml = sXml.Append("<DataDriverNm></DataDriverNm>");
                sXml = sXml.Append("<DataServerNm></DataServerNm>");
                sXml = sXml.Append("<DataLibraryList></DataLibraryList>");
                sXml = sXml.Append("<DataDSN></DataDSN>");
                sXml = sXml.Append("<DataDSNUser></DataDSNUser>");
                sXml = sXml.Append("<DataDSNPwd></DataDSNPwd>");
                sXml = sXml.Append("</PolicySys></PolicyInterface></Document></Message>");
                oTemplate = XElement.Parse(sXml.ToString());
                return oTemplate;
            }
            return oTemplate;

        }
    }
}