﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IntegralPolicySysDownloadResults.aspx.cs"
    Inherits="Riskmaster.UI.UI.IntegralPolicyInterface.IntegralPolicySysDownloadResults" ValidateRequest="false" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <script language="javaScript" src="../../Scripts/form.js" type="text/javascript"></script>
    <script language="javaScript" src="../../Scripts/search.js" type="text/javascript"></script>
    <script language="javaScript" type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js"></script>
    <script language="javaScript" type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js"></script>
    <script language="javaScript" type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js" type="text/javascript">        { var i; }    </script>
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <link href="~/App_Themes/RMX_Default/rmnet.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">

        function BackButtonClicked() {

            var claimid = document.getElementById("hdnClaimId");
            var sPolicyLobCode = document.getElementById("hdnPolicyLobCode");
            var sDate = document.getElementById("hdnPolicyLossDate");
            var sPolicyLobId = document.getElementById("hdnPolicyLobId");
            //sanoopsharma start adding system name for UI customization 
            var sSystemName = document.getElementById("hdnPolicySysName");
            window.location.href = "/RiskmasterUI/UI/PolicyInterface/PolicySysDownload.aspx?claimId=" + claimid.value + '&PolicyLobCode=' + sPolicyLobCode.value + '&PolicyDateCriteria=' + sDate.value + '&PolicyLobId=' + sPolicyLobId.value
            + '&SystemName=' + sSystemName.value;;
            //sanoopsharma end 
        }
        //vkumar258 - start
        function OnDownloadButtonClicked(enquireOnly) {
            var sRetVal;
            var polNum = document.getElementById("hdSelectedPolicyNumber").value;
            //  var selectedRow = GetSelectedRows();
            if (polNum != "") {
                var claimId = document.getElementById("hdnClaimId").value;
                var policySystemId = document.getElementById("hdnSystemName").value;
                var policyLossdate = document.getElementById("hdnPolicyLossDate").value;
                var policyLobCode = document.getElementById("hdnPolicyLobCode").value;
                var policyLobCode_ID = document.getElementById("hdnPolicyLobId").value; //sanoopsharma - added in the query string
                var polEffecitveDate = document.getElementById("hdnpEffectiveDate").value;
                var polExpireDate = document.getElementById("hdnpExpireDate").value;
                var objPolicyClaimEventSetting = eval("document.forms[0].hdnIntegralClaimEventSetting");
                var objClaimType = eval("document.forms[0].hdnClaimType");
                var objDateOfLoss = eval("document.forms[0].txtLossDate");
                var objLobCode = eval("document.forms[0].hdnPolicyLobCode");
                if (window.opener != null && window.opener != 'undefined') {
                    var objDateofEvent = eval("window.opener.document.forms[0].ev_dateofevent");
                    var objDateofClaim = eval("window.opener.document.forms[0].dateofclaim");
                }
                polEffecitveDate = polEffecitveDate.replace(/-/g, '/'); //msampathkuma- RMA-14595
                polExpireDate = polExpireDate.replace(/-/g, '/'); //msampathkuma- RMA - 14595
                polEffecitveDate = new Date(polEffecitveDate);
                polExpireDate = new Date(polExpireDate);

                if (document.getElementById("hdnClaimType").value == '') {

                    var LossDate = document.getElementById('txtLossDate').value;
                    if (LossDate != '') {
                        LossDate = new Date(LossDate);
                    }
                }
                else {

                    var LossDate = document.getElementById("hdnPolicyLossDate").value
                    if (LossDate != '') {
                        LossDate = new Date(LossDate);
                    }
                }
                if (enquireOnly == "False") {
                    if (LossDate == '') {
                        alert(IntegralPolicySysDownloadResultsValidation.DateOfLossRequired);
                        //alert("Date Of Loss is required for download the policy");
                        return false;
                    }
                    if (LossDate != '') {
                        if ((LossDate >= polEffecitveDate) && (LossDate <= polExpireDate)) {

                        }
                        else {
                            alert(IntegralPolicySysDownloadResultsValidation.DateInvalid);
                           // alert("Policy selection for entered loss date is invalid");
                            return false;
                        }
                    }
                }
                if (enquireOnly == "False")
                    sRetVal = confirm(IntegralPolicySysDownloadResultsValidation.ConformDownload);
                        //confirm("Are you sure you want to download the selected policy?");
                else
                    sRetVal = 1;
                //MITS:33574 END
                if (!sRetVal)
                    return false;
                else {
                    
                    if (objPolicyClaimEventSetting != null) {

                        if (objClaimType.value == '') {
                            var DownloadPolicyWithoutClaim = "true";
                            if (objDateOfLoss.value != "") {
                                sDate = objDateOfLoss.value
                                window.open("/RiskmasterUI/UI/PolicyInterface/IntegralPolicyData.aspx?claimid=" + claimId + "&policysystemid=" + policySystemId + "&policyNumber=" + polNum + '&&DownloadPolicyWithoutClaim=' + DownloadPolicyWithoutClaim + "&policyLossdate=" + sDate + "&policyLobCode=" + policyLobCode + "&policyLobCode_ID=" + policyLobCode_ID + "&enquireOnly=" + enquireOnly, null, 'toolbar=0,titlebar=0,width=770,height=545,resizable=yes,scrollbars=yes');
                            }
                            else
                                window.open("/RiskmasterUI/UI/PolicyInterface/IntegralPolicyData.aspx?claimid=" + claimId + "&policysystemid=" + policySystemId + "&policyNumber=" + polNum + '&&DownloadPolicyWithoutClaim=' + DownloadPolicyWithoutClaim + "&policyLobCode=" + policyLobCode + "&policyLobCode_ID=" + policyLobCode_ID + "&enquireOnly=" + enquireOnly, null, 'toolbar=0,titlebar=0,width=770,height=545,resizable=yes,scrollbars=yes');
                        }
                        else {
                            if ((objPolicyClaimEventSetting.value == 'true') && (objDateofClaim != null))
                                sDate = objDateofClaim.value;
                            else if ((objPolicyClaimEventSetting.value == 'false') && (objDateofEvent != null))
                                sDate = objDateofEvent.value;
                            window.open("/RiskmasterUI/UI/PolicyInterface/IntegralPolicyData.aspx?claimid=" + claimId + "&policysystemid=" + policySystemId + "&policyNumber=" + polNum + "&policyLossdate=" + policyLossdate + "&policyLobCode=" + policyLobCode + "&policyLobCode_ID=" + policyLobCode_ID + "&enquireOnly=" + enquireOnly, null, 'toolbar=0,titlebar=0,width=770,height=545,resizable=yes,scrollbars=yes');
                        }
                        return false;
                        // }
                        //else {
                        //  alert("Please select policy record");
                        //}
                        //return false;
                    }
                    return true;
                }
            }
            //End****
        }
        //vkumar258 - end

    </script>
    <title>Search</title>
</head>
<body onload="javascript:if(parent.MDIScreenLoaded != null) parent.MDIScreenLoaded();">
    <form id="frmData" runat="server">
        <uc1:errorcontrol id="ErrorControl" runat="server" />
        <table>
            <tr>
                <td>
                    <asp:imagebutton runat="server" onclientclick="return OnDownloadButtonClicked('False');"
                        src="../../Images/tb_save_active.png" width="28" height="28"
                        border="0" id="imgbtnSave" alternatetext="Save Policy" onmouseover="this.src='../../Images/tb_save_mo.png';"
                        onmouseout="this.src='../../Images/tb_save_active.png';" />
                    <asp:imagebutton runat="server" onclientclick="return OnDownloadButtonClicked('True');"
                        src="../../Images/tb_search_active.png" width="28" height="28" border="0" id="imgbtnInquire"
                        alternatetext="Inquire Policy" onmouseover="this.src='../../Images/tb_search_mo.png';"
                        onmouseout="this.src='../../Images/tb_search_active.png';" />
                    <%--MITS:33574 START--%>
                    <asp:label runat="server" font-bold="true" id="lblDateOfLoss" text="Date of Loss:"></asp:label>

                    <select name="dlLossDate" id="dlLossDate" runat="server" tabindex="500" onchange="return BetweenOption('date','dlLossDate');"
                        style='display: none;'>
                        <option value="=">=</option>
                        <option value="&lt;>">&lt;&gt;</option>
                        <option value=">">&gt;</option>
                        <option value="">=">&gt;=</option>
                        <option value="&lt;">&lt;</option>
                        <option value="&lt;=">&lt;=</option>
                    </select>

                    <input name="txtLossDate" type="text" id="txtLossDate" tabindex="14" rmxref="" rmxtype="date" size="30"
                        onchange="datachanged('true')" onblur="dateLostFocus(this.id);" runat="server" />
                    <input type="button" name="btnIssueDateStart" value="" id="btnLossDate" tabindex="15"
                        class="DateLookupControl" runat="server" />
                    <script type="text/javascript">
                        Zapatec.Calendar.setup(
                        {
                            inputField: "txtLossDate",
                            ifFormat: "%m/%d/%Y",
                            button: "btnLossDate"
                        }
                        );
                    </script>
                    <%--MITS:33574 END--%>
                </td>
            </tr>
        </table>
        <telerik:radscriptmanager id="ScriptManager1" enablepagemethods="true" runat="server" />
        <telerik:radstylesheetmanager id="RadStyleSheetManager1" runat="server" />
        <telerik:radcodeblock runat="server" id="radCodeBlock">
            <script type="text/javascript">
                function GetSelectedRow() {
                    var masterTable = $find("<%=gvPolicyList.ClientID %>").get_masterTableView();
                    var SelectedRow = masterTable.get_dataItems()[eventArgs.get_itemIndexHierarchical()];
                    return SelectedRow;
                }
                function OnRowSelected(sender, eventArgs) {
                    // Show / hide download or Inquiry Next button 
                    var claimID = document.getElementById("hdnClaimId").value;
                    // Set hidden fields
                    var MasterTable = sender.get_masterTableView();
                    var row = MasterTable.get_dataItems()[eventArgs.get_itemIndexHierarchical()];
                    var polNum = MasterTable.getCellByColumnUniqueName(row, "PolicyNumber");
                    var polSymbol = MasterTable.getCellByColumnUniqueName(row, "PolicySymbol");
                    var polModule = MasterTable.getCellByColumnUniqueName(row, "Module");
                    //  vkumar258-Start
                    var polEffecitveDate = MasterTable.getCellByColumnUniqueName(row, "EffectiveDate");
                    var polExpireDate = MasterTable.getCellByColumnUniqueName(row, "ExpiryDate");

                    //vkumar258-end
                    document.getElementById("hdSelectedPolicyNumber").value = polNum.innerHTML;
                    document.getElementById("hdSelectedPolicySymbol").value = polSymbol.innerHTML;
                    document.getElementById("hdSelectedModule").value = polModule.innerHTML;
                    document.getElementById("hdnpEffectiveDate").value = polEffecitveDate.innerHTML;
                    document.getElementById("hdnpExpireDate").value = polExpireDate.innerHTML;

                }

            </script>
        </telerik:radcodeblock>
        <telerik:radajaxmanager id="RadAjaxManager1" runat="server">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="gvPolicyList" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="gvPolicyList">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="gvPolicyList" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:radajaxmanager>
        <div id="dvResults" runat="server">
            <table border="0" cellspacing="0" cellpadding="2" width="99.5%">
                <tr>
                    <td class="ctrlgroup">
                        <asp:label id="lblSearchResults" runat="server" text="<%$ Resources:lblSearchResults %>"></asp:label>
                    </td>
                </tr>
                <tr>
                    <td align="center" width="100%">
                        <asp:literal id="ltNoResult" runat="server" visible="false" />
                    </td>
                </tr>
            </table>
            <table id="tblToolbar" border="0" cellspacing="0" cellpadding="0" align="center"
                runat="server" width="100%">
                <tr>
                    <td>
                        <asp:imagebutton runat="server" onclientclick="return OnDownloadButtonClicked('False');"
                            src="../../Images/tb_save_active.png" width="28" height="28" style="display: none"
                            border="0" id="imgSave" alternatetext="Save Policy" onmouseover="this.src='../../Images/tb_save_mo.png';"
                            onmouseout="this.src='../../Images/tb_save_active.png';" />
                        <asp:imagebutton runat="server" onclientclick="return OnDownloadButtonClicked('True');" style="display: none"
                            src="../../Images/tb_search_active.png" width="28" height="28" border="0" id="imgEnquire"
                            alternatetext="Inquire Policy" onmouseover="this.src='../../Images/tb_search_mo.png';"
                            onmouseout="this.src='../../Images/tb_search_active.png';" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <telerik:radgrid runat="server" id="gvPolicyList" autogeneratecolumns="false"
                            onneeddatasource="gvPolicyList_NeedDataSource" allowsorting="True" allowfilteringbycolumn="True"
                            gridlines="None" skin="Office2007" onprerender="gvPolicyList_PreRender" height="100%" width="100%" headerstyle-cssclass="msgheader"
                            allowpaging="true" pagesize="10" enablelinqexpressions="false">
                            <SelectedItemStyle CssClass="SelectedItem" />
                            <ClientSettings AllowKeyboardNavigation="true">
                                <ClientEvents OnRowSelected="OnRowSelected" />
                                <Scrolling SaveScrollPosition="true" AllowScroll="false" />
                                <Selecting AllowRowSelect="true" />
                            </ClientSettings>
                            <MasterTableView Width="100%">
                                <RowIndicatorColumn>
                                    <HeaderStyle Width="20px" />
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn>
                                    <HeaderStyle Width="20px" />
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridBoundColumn HeaderText="<%$ Resources:lblNum %>" UniqueName="Number" DataField="Number" SortExpression="Number"
                                        Visible="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn HeaderText="<%$ Resources:lblStat %>"
                                        ItemStyle-Wrap="true" UniqueName="PolicyStatus" DataField="PolicyStatus" SortExpression="PolicyStatus">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn HeaderText="<%$ Resources:lblLOB %>"
                                        ItemStyle-Wrap="true" UniqueName="PolicySymbol" DataField="PolicySymbol" SortExpression="PolicySymbol">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn ItemStyle-CssClass="data" ItemStyle-Wrap="false" DataField="PolicyNumber" HeaderText="<%$ Resources:lblPolNum %>"
                                        SortExpression="PolicyNumber" UniqueName="PolicyNumber">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn ItemStyle-CssClass="data" ItemStyle-Wrap="false" DataField="Module" HeaderText="<%$ Resources:lblModule %>"
                                        SortExpression="Module" UniqueName="Module" Display="False">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn HeaderText="<%$ Resources:lblInsuredName %>" DataField="InsuredFullName" SortExpression="InsuredFullName"
                                        ItemStyle-Wrap="true" UniqueName="InsuredFullName">
                                    </telerik:GridBoundColumn>
                                     <telerik:GridBoundColumn HeaderText="<%$ Resources:lblInsTaxID %>" DataField="InsuredTaxID" SortExpression="InsuredTaxID"
                                        ItemStyle-Wrap="true" UniqueName="InsuredTaxID">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn HeaderText="<%$ Resources:lblEffDate %>" UniqueName="EffectiveDate" DataField="EffectiveDate" SortExpression="EffectiveDate"
                                        ItemStyle-Wrap="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn HeaderText="<%$ Resources:lblExpDate %>" UniqueName="ExpiryDate" DataField="ExpiryDate" SortExpression="ExpiryDate"
                                        ItemStyle-Wrap="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn HeaderText="<%$ Resources:lblMastComp %>" UniqueName="MasterCompany" DataField="MasterCompany" SortExpression="MasterCompany"
                                        ItemStyle-Wrap="false">
                                    </telerik:GridBoundColumn>
                                       <telerik:GridBoundColumn HeaderText="<%$ Resources:lblBranch %>" UniqueName="LocationCompany" DataField="LocationCompany" SortExpression="LocationCompany"
                                        ItemStyle-Wrap="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn HeaderText="<%$ Resources:lblAgenNam %>" UniqueName="AgentName" DataField="AgentName" SortExpression="AgentName"
                                        ItemStyle-Wrap="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn HeaderText="<%$ Resources:lblAgenNum %>" UniqueName="AgentNumber" DataField="AgentNumber" SortExpression="AgentNumber"
                                        ItemStyle-Wrap="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn HeaderText="<%$ Resources:lblIncDate %>" UniqueName="InceptionDate"
                                        DataField="InceptionDate" SortExpression="InceptionDate">
                                    </telerik:GridBoundColumn>
                                     <telerik:GridTemplateColumn DataField="IntegralClaimEventSetting" UniqueName="IntegralClaimEventSetting" Display="false">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblIntegralClaimEventSetting" Text=""></asp:Label>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn> 
                                </Columns>
                                <ItemStyle CssClass="datatd1" />
                                <AlternatingItemStyle CssClass="datatd" />
                                <HeaderStyle CssClass="msgheader" />
                            </MasterTableView>
                            <PagerStyle Mode="NextPrevAndNumeric" Position="Top" AlwaysVisible="true" />
                            <FilterMenu EnableTheming="True">
                                <CollapseAnimation Duration="200" Type="OutQuint" />
                            </FilterMenu>
                        </telerik:radgrid>
                    </td>
                </tr>
            </table>
            <table border="0" cellspacing="0" cellpadding="2" width="99.5%">
                <tr>
                    <td width="50px">
                        <input type="button" name="btnBack" value="Back" onclick="BackButtonClicked()" id="btnBack"
                            tabindex="500" class="button" />
                    </td>
                    <td></td>
                </tr>
            </table>

            <asp:hiddenfield id="hdnpEffectiveDate" value="" runat="server" />
            <asp:hiddenfield id="hdnpExpireDate" value="" runat="server" />
            <asp:hiddenfield id="hdnClaimType" value="" runat="server" />
            <asp:hiddenfield id="hdSelectedPolicyNumber" value="" runat="server" />
            <asp:hiddenfield id="hdSelectedModule" value="" runat="server" />
            <asp:hiddenfield id="hdSelectedPolicySymbol" value="" runat="server" />
            <asp:hiddenfield id="hdnSystemName" value="" runat="server" />
            <asp:hiddenfield id="hdnClaimId" value="" runat="server" />
            <asp:hiddenfield id="hdnPolicySymbol" value="" runat="server" />
            <asp:hiddenfield id="hdnPolicyLobCode" value="" runat="server" />
            <asp:hiddenfield id="hdnPolicyLobId" value="" runat="server" />
            <asp:hiddenfield id="hdnPolicyLossDate" value="" runat="server" />
            <uc:pleasewaitdialog id="PleaseWaitDialog1" runat="server" custommessage="Loading" />
            <!--Sanoopsharma-->
            <asp:hiddenfield id="hdnPolicySysName" runat="server" value="" />
            <asp:hiddenfield id="hdnPolicySrchSys" runat="server" value="" />
            <asp:hiddenfield id="hdnIntegralClaimEventSetting" runat="server" value="" />

            <!--Sanoopsharma-->
            <!-- vkumar258-->
            <asp:hiddenfield id="hdSelectedMasterCompany" runat="server" value="" />
            <asp:hiddenfield id="hdSelectedLOB" runat="server" value="" />
            <asp:hiddenfield id="hfInsurerNm" runat="server" value="" />
            <asp:hiddenfield id="hdnTaxId" runat="server" value="" />
            <asp:hiddenfield id="hdnClientSeqNo" runat="server" value="" />
            <asp:hiddenfield id="hdnAddressSeqNo" runat="server" value="" />
            <asp:hiddenfield id="hdnBirthDt" runat="server" value="" />
            <asp:hiddenfield id="hfInsurerAddr1" runat="server" value="" />
            <asp:hiddenfield id="hfInsurerCity" runat="server" value="" />
            <asp:hiddenfield id="hfInsurerPostalCd" runat="server" value="" />
            <asp:hiddenfield id="hdnNameType" runat="server" value="" />
            <asp:hiddenfield id="hdnPolicyLocation" runat="server" value="" />
            <asp:hiddenfield id="hdStagingpolicyINode" runat="server" value="" />
            <asp:hiddenfield id="hdnrow" runat="server" value="" />
            <asp:hiddenfield id="hdnPolicyStatus" runat="server" value="" />
            <!-- vkumar258-->
        </div>
    </form>
</body>
</html>
