﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PS_UnitInterestList.aspx.cs" Inherits="Riskmaster.UI.UI.PolicyInterface.PS_UnitInterestList" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl" TagPrefix="uc1" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog" TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Unit Interest List</title>
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script language="javaScript" src="../../Scripts/form.js" type="text/javascript"></script>
    <script language="JavaScript" src="../../Scripts/drift.js" type="text/javascript">        { var i; }
    </script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js" type="text/javascript">        { var i; }
    </script>
    <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js" >        { var i; }
    </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js" >        { var i; }
    </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js" >        { var i; }
    </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid.js" >        { var i; }
    </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">        { var i; }
    </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">        { var i; }
    </script>
    <script type="text/javascript" src="../../Scripts/dhtml-div.js">        { var i; }
    </script>
    <script type="text/javascript" src="../../Scripts/dhtml-help-setup.js">        { var i; }
    </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">        { var i; }
    </script>
    
    <script type="text/javascript" language="javascript">
        function OpenAdditionalInterest() {
            var vTemp;
            ele = document.getElementById("lbAdditionalInfo");
            if (ele.selectedIndex == -1) {
                alert("Please select a record to continue");
                return false;
            }
            else {
                vTemp = ele.value;
            }
            m_unitWindow = window.open("/RiskmasterUI/UI/PolicyInterface/PS_PolicyInterest.aspx?val=" + vTemp, "UnitInterest", "width =720,height=400,resizable=yes,scrollbars=yes");
            return false;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <uc1:errorcontrol id="ErrorControl1" runat="server"  />
    </div>
    <div class="singletopborder" runat="server" id="dvUnitInterestList">
        <table width="98%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td >
                    <asp:hiddenfield runat="server" id="hdadditionalpolicyinfo" />
                </td>
            </tr>
            <tr>
                <td >
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td valign="top">
                                <asp:Label runat="server" class="required" ID="lbl_AdditionalInfo" Text="Additional Interests"
                                    Width="150px" />
                            </td>
                            <td>
                                <span class="formw">
                                    <asp:ListBox runat="server" ID="lbAdditionalInfo" readonly="true" Width="250px" />
                                </span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div3" xmlns="">
                        <span class="formw">
                            <br />
                        </span>
                    </div>
                    <div runat="server" class="completerow" id="div4" xmlns="">
                        <span class="formw">
                            <br />
                        </span>
                    </div>
                </td>
            </tr>
            <tr>
            <td>
            <asp:label runat="server" id="lblAdditionalPolicyMsg" style="font-weight:bold" text="This unit has no interest list information." visible="false"></asp:label>  
            </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div5" xmlns="">
                        <span class="formw">
                            <br />
                        </span>
                    </div>
                    <div runat="server" class="completerow" id="div6" xmlns="">
                        <span class="formw">
                            <br />
                        </span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                        <div runat="server" class="onecontrol" id="div9">
                            <span class="formw">
                                <asp:button class="button" runat="server" id="btnAdditionalInterestDetails" text="Interest Details" onclientclick="return OpenAdditionalInterest()" />
                            </span>
                        </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div runat="server" class="completerow" id="div7" xmlns="">
                        <span class="formw">
                            <br />
                        </span>
                    </div>
                </td>
            </tr>
         </table>
    </div>
    <asp:HiddenField runat="server" ID="hdPolicyNum" Value="" />
    <asp:HiddenField runat="server" ID="hdCmpnyProductCd" />
    <asp:HiddenField runat="server" ID="hdIssueCd" />
    <asp:HiddenField runat="server" ID="hdModule" />
    <asp:HiddenField runat="server" ID="hdUnitNum" />
    <asp:HiddenField runat="server" ID="hdLobCd" />
    <asp:hiddenfield runat="server" id="hdLocatnCompny" value="" />
    <asp:hiddenfield runat="server" id="hdMasterCompny" value="" /> 
    <asp:hiddenfield runat="server" id="hdPolicySystemID" value="" />    
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    </form>
</body>
</html>
