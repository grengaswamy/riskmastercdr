﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PS_PolicyCoverageDetail.aspx.cs" Inherits="Riskmaster.UI.UI.PolicyInterface.PS_PolicyCoverageDetail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl" TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Coverage Detail</title>
    <%--tanwar2 - Policy Download from staging - start--%>
    <style type="text/css">
    .hide
    {
        display:none;
    }
    .td{
    display:inline-block;
    *display:inline; /* for IE7*/
    *zoom:1;/* for IE7*/
    width:24%;
    margin-top:5px;
    vertical-align:middle;
    font-family: Tahoma, Arial, Helvetica, sans-serif;
    }

    </style>
    <script src="../../Scripts/jquery/jquery-1.8.0.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        $('document').ready(function () {
            if ($('#hdnRemoveEmtpyFields').val() === 'True') {
                //hide empty tds
                $('#dvManual table tbody tr td').each(function () {
                    if ($(this).children('span:empty()').length === $(this).children('span').length) {
                        $(this).addClass('hide');
                        $(this).prev('td').addClass('hide');
                    }
                });

                //mark trs with all empty tds
                $('#dvManual table tbody tr').each(function () {
                    if ($(this).children('td.hide').length == $(this).children('td').length) {
                        $(this).addClass('hide');
                    }
                });

                //Remove tr td having class hide
                $('.hide').remove();

                //Change all tr td to divs
                $('#dvManual').html($('#dvManual table tbody').html()
                    .replace(/<tr(.*)>/gi, '')
                    .replace(/<\/tr>/gi, '')
                    .replace(/<td/gi, '<div class="td"')
                    .replace(/<\/td/gi, '</div')
                    );

                $('.td:even').addClass('required');
            }
        });
        <%--tanwar2 - Policy Download from staging - end--%>
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <table>
        <tr>
            <td colspan="2">
                <uc1:errorcontrol id="ErrorControl1" runat="server" />
            </td>
        </tr>
    </table>
     <div class="msgheader" id="formtitle">
        Coverage Detail
        </div>
        <!--<div runat="server" id="dvProp" visible="false">
        <table width="98%" cellpadding="4" cellspacing="8" border="0" >
        <colgroup>
        <col width="25%" class="required" />
        <col width="25%" />
        <col width="25%" class="required" />
        <col width="25%" />
        </colgroup>
        <tr>
        <td><asp:Label runat="server" Text="Status" ></asp:Label></td>
        <td><asp:Label runat="server" ID="lblPropStatus"></asp:Label></td>
        <td><asp:Label runat="server" Text="Ratebook"></asp:Label></td>
        <td><asp:Label runat="server" ID="lblPropRatebook"></asp:Label></td>
        </tr>
        <tr>
        <td><asp:Label runat="server" Text="Coverage"></asp:Label></td>
        <td><asp:Label runat="server" ID="lblPropCovCd"></asp:Label> &nbsp; &nbsp; <asp:Label runat="server" ID="lblPropCovDesc"></asp:Label></td>
        <td><asp:Label runat="server" Text="Unit"></asp:Label></td>
        <td><asp:Label runat="server" ID="lblPropUnitNum"></asp:Label> &nbsp; &nbsp; <asp:Label runat="server" ID="lblPropUnitDesc"></asp:Label></td>
        </tr>
        <tr>
        <td><asp:Label runat="server" Text="Premium"></asp:Label></td>
        <td><asp:Label runat="server" ID="lblPropPrem"></asp:Label></td>
        <td><asp:Label Text="Sequence" runat="server"></asp:Label></td>
        <td><asp:Label runat="server" ID="lblPropSeq"></asp:Label></td>
        </tr>
        </table>
        </div>
        <div runat="server" id="dvWCV" visible="false">
        <table width="98%" cellpadding="4" cellspacing="8" border="0" >
        <colgroup>
        <col width="25%" class="required" />
        <col width="25%" />
        <col width="25%" class="required" />
        <col width="25%" />
        </colgroup>
        <tr>
        <td><asp:Label Text="Site Name/Number" runat="server"></asp:Label></td>
        <td><asp:Label runat="server" ID="lblSiteNm"></asp:Label> &nbsp; &nbsp; <asp:Label runat="server" ID="lblSiteNum"></asp:Label></td>
        <td><asp:Label runat="server" Text="Status"></asp:Label></td>
        <td><asp:Label runat="server" ID="lblWCVStatus"></asp:Label></td>
        </tr>
        <tr>
        <td><asp:Label Text="Class Code" runat="server"></asp:Label></td>
        <td><asp:Label ID="lblClassCd" runat="server"></asp:Label> &nbsp; &nbsp; <asp:Label runat="server" ID="lblClassDesc"></asp:Label></td>
        <td><asp:Label Text="Class Sequence" runat="server"></asp:Label></td>
        <td><asp:Label runat="server" ID="lblClassSeq"></asp:Label></td>
        </tr>
        <tr>
        <td><asp:Label Text="Voluntary Compensation" runat="server"></asp:Label></td>
        <td><asp:Label runat="server" ID="lblVoluntary"></asp:Label></td>
        <td><asp:Label ID="Label1" runat="server" Text="Coverage Code"></asp:Label></td>
        <td><asp:Label runat="server" ID ="lblWCVCovCd"></asp:Label></td>
        </tr>
        <tr>
        <td><asp:Label Text="Estimated Payroll" runat="server"></asp:Label></td>
        <td><asp:Label runat="server" ID="lblEstdPayroll"></asp:Label></td>
        <td><asp:Label Text="Manual Premium" runat="server"></asp:Label></td>
        <td><asp:Label runat="server" ID="lblManualPrem"></asp:Label></td>
        </tr>
        <tr>
        <td><asp:Label runat="server" Text="Rate Override"></asp:Label></td>
        <td><asp:Label runat="server" ID="lblRate"></asp:Label></td>
        <td><asp:Label runat="server" Text="Split Rate Override"></asp:Label></td>
        <td><asp:Label runat="server" ID="lblSplitRate"></asp:Label></td>
        </tr>
        <tr>
        <td><asp:Label Text="Rate" runat="server"></asp:Label></td>
        <td><asp:Label runat="server" ID="lblWCVRate"></asp:Label></td>
        <td><asp:Label runat="server" Text="Split Rate"></asp:Label></td>
        <td><asp:Label runat="server" ID="lblWCVSplitRate"></asp:Label></td>
        </tr>
        </table>
        </div>
        <div runat="server" id="dvVehicle" visible="false">
        <table width="98%" cellpadding="8" cellspacing="4" >
        <colgroup>
        <col width="25%" class="required" />
        <col width="25%" />
        <col width="25%" class="required" />
        <col width="25%" />
        </colgroup>
        <tr>
        <td><asp:Label Text="Status" runat="server"></asp:Label></td>
        <td><asp:Label runat="server" ID="lblVehStatus"></asp:Label></td>
        <td><asp:Label Text="Unit" runat="server"></asp:Label></td>
        <td><asp:Label runat="server" ID="lblVehUnitNum"></asp:Label> &nbsp; &nbsp; <asp:Label runat="server" ID="lblVehUnitDesc"></asp:Label></td>
        </tr>
        <tr>
        <td><asp:Label Text="Coverage" runat="server"></asp:Label></td>
        <td><asp:Label runat="server" ID="lblVehCovCd"></asp:Label> &nbsp; &nbsp; <asp:Label runat="server" ID="lblVehCovDesc"></asp:Label></td>
        <td><asp:Label runat="server" Text="Limit"></asp:Label></td>
        <td><asp:Label runat="server" ID="lblVehLimit"></asp:Label></td>
        </tr>
        <tr>
        <td><asp:Label Text="Deductible/Exposure" runat="server"></asp:Label></td>
        <td><asp:Label runat="server" ID="lblVehDeduc"></asp:Label></td>
        <td><asp:Label Text="Ratebook" runat="server"></asp:Label></td>
        <td><asp:Label runat="server" ID="lblVehRatebook"></asp:Label></td>
        </tr>
        <tr>
        <td><asp:Label Text="Premium" runat="server"></asp:Label></td>
        <td><asp:Label ID="lblVehPrem" runat="server"></asp:Label></td>
        </tr>
        </table>
        </div>-->
        <div runat="server" id="dvManual">
            <table width="98%" cellpadding="4" cellspacing="8">
                <colgroup>
                    <col width="25%" class="required" />
                    <col width="25%" />
                    <col width="25%" class="required" />
                    <col width="25%" />
                </colgroup>
                <tr>
                    <td>
                        <asp:Label Text="Unit" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblMRUnitNum"></asp:Label>
                        &nbsp; &nbsp;
                        <asp:Label runat="server" ID="lblMRUnitDesc"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label Text="Coverage" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblMRCovCd"></asp:Label>
                        &nbsp; &nbsp;
                        <asp:Label runat="server" ID="lblMRCovDesc"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" Text="Coverage Status"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblMRCovStatus"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label Text="Class" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblMRClassCd"></asp:Label>
                        &nbsp; &nbsp;
                        <asp:Label runat="server" ID="lblMRClassDesc"></asp:Label>
                    </td>
                    <td>
                        <asp:Label Text="State" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblMRState"></asp:Label>
                    </td>
                </tr>
                <tr>
                     <td>
                        <asp:Label Text="SubLine" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblMRSublineCd"></asp:Label>
                        &nbsp; &nbsp;
                        <asp:Label runat="server" ID="lblMRSubline"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" Text="Ins Line"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblMRInsLine"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label runat="server" Text="Coverage Seq No"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblMRCovSeqNo"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" Text="Trans Seq No"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblMRTransSeqNo"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label Text="Coverage Effective Date" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblMRCovEffDt"></asp:Label>
                    </td>
                    <td>
                        <%-- tkatsarski: 11/14/14 RMA-2235: Changed label. --%>
                        <asp:Label Text="Coverage Expiration Date" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblMRCovExpDt"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label runat="server" Text="Transaction Date"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblMRTranDt"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" Text="Entry Date"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblMREntryDt"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label runat="server" Text="Retro Date"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblMRRetroDt"></asp:Label>
                    </td>
                    <td>
                        <asp:Label Text="Accounting Date" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblMRAcctngDt"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label runat="server" Text="Extend Date"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblMRExtendDt"></asp:Label>
                    </td>
                    <td>
                        <asp:Label Text="Product" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblMRProdLn"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label Text="Exposure" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblMRExposure"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" Text="Occurence Limit"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblMROccurence"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label runat="server" Text="Aggregate Limit"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblMRAggregate"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" Text="Deductible"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblMRDeductible"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label Text="Original Premium" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblMROrigPrem"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" Text="Written Premium"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblMRWrittenPrem"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label runat="server" Text="Full Term Premium"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblMRFullTermPrem"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" Text="Total Premium"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblMRTotalPrem"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label runat="server" Text="Product Line" ></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblProductLine"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" Text="Annual Statement"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblAnnualStmt"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblWcDedAmt" runat="server" Text="WC Deductible Amount"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="txtWcDedAmt"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblWcDedAggr" runat="server" Text="WC Deductible Aggregate"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="txtWcDedAggr"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblLimitCovA" runat="server" Text="Limit-CovA"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="txtLimitCovA"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblLimitCovB" runat="server" Text="Limit-CovB"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="txtLimitCovB"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblLimitCovC" runat="server" Text="Limit-CovC"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="txtLimitCovC"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblLimitCovD" runat="server" Text="Limit-CovD"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="txtLimitCovD"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblLimitCovE" runat="server" Text="Limit-CovE"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="txtLimitCovE"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblLimitCovF" runat="server" Text="Limit-CovF"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="txtLimitCovF"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <!--<div runat="server" id="dvCPP" visible="false">
        <table width="98%" cellpadding="4" cellspacing="8">
        <colgroup>
        <col width="25%" class="required" />
        <col width="25%" />
        <col width="25%" class="required" />
        <col width="25%" />
        </colgroup>
        <tr>
        <td><asp:Label runat="server" Text="Insurance Line"></asp:Label></td>
        <td><asp:Label runat="server" ID="lblCPPInsLn"></asp:Label></td>
        <td><asp:Label Text="Product" runat="server"></asp:Label></td>
        <td><asp:Label runat="server" ID="lblCPPProdct"></asp:Label></td>
        </tr>
        <tr>
        <td><asp:Label runat="server" Text="Location"></asp:Label></td>
        <td><asp:Label runat="server" ID="lblCPPLocatn"></asp:Label></td>
        <td><asp:Label runat="server" Text="Building"></asp:Label></td>
        <td><asp:Label runat="server" ID="lblCppBuildng"></asp:Label></td>
        </tr>
        <tr>
        <td><asp:Label Text="Coverage" runat="server"></asp:Label></td>
        <td><asp:Label runat="server" ID="lblCPPCov"></asp:Label></td>
        <td><asp:Label Text="Sequence" runat="server"></asp:Label></td>
        <td><asp:Label runat="server" ID="lblCPPSeq"></asp:Label></td>
        </tr>
        <tr>
        <td><asp:Label Text="Ratebook" runat="server"></asp:Label></td>
        <td><asp:Label runat="server" ID="lblCPPRatebook"></asp:Label></td>
        <td><asp:Label Text="Premium" runat="server"></asp:Label></td>
        <td><asp:Label runat="server" ID="lblCPPPrem"></asp:Label></td>
        </tr>
        <tr>
        <td><asp:Label Text="State" runat="server"></asp:Label></td>
        <td><asp:Label runat="server" ID="lblCPPState"></asp:Label></td>
        <td><asp:Label runat="server" Text="Status"></asp:Label></td>
        <td><asp:Label runat="server" ID="lblCPPStatus"></asp:Label></td>
        </tr>
        <tr>
        <td><asp:Label Text="Class Code" runat="server"></asp:Label></td>
        <td colspan="3"><asp:Label runat="server"  ID="lblCPPClassCd"></asp:Label> &nbsp; &nbsp; <asp:Label runat="server" ID="lblCPPClassDesc"></asp:Label></td>
        </tr>
        </table>
        </div>-->
        <asp:HiddenField ID="hfInsLine" runat="server" />
        <asp:HiddenField ID = "hdCoveragesXML" runat="server" />
        <asp:HiddenField ID="hfStatUnitNum" runat="server" />
        <asp:HiddenField ID="hdBaseLob" runat="server" />
        <%--tanwar2 - Policy Download from staging - start--%>
        <asp:HiddenField ID="hdnRemoveEmtpyFields" runat="server" Value="" />
        <%--tanwar2 - Policy Download from staging - end--%>
    </form>
</body>
</html>
