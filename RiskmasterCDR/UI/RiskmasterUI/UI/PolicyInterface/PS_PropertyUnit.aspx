<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PS_PropertyUnit.aspx.cs" Inherits="Riskmaster.UI.UI.PolicyInterface.PS_PropertyUnit" ValidateRequest="false" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/UserControlDataGrid.ascx" tagname="UserControlDataGrid" tagprefix="dg" %>
<%@ Register src="~/UI/Shared/Controls/MultiCodeLookup.ascx" tagname="MultiCode" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/SystemUsers.ascx" tagname="SystemUsers" tagprefix="cul" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:asp="remove" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove" xmlns:cul="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
  <head id="Head1" runat="server">
    <title>Property</title>
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script language="JavaScript" src="../../Scripts/form.js">        { var i; }
</script>
    <script language="JavaScript" src="../../Scripts/drift.js">        { var i; }
</script>
    <script language="JavaScript" src="../../Scripts/WaitDialog.js">        { var i; }
  </script>
    <%--<script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">        { var i; }
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }
</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">        { var i; }
	</script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">        { var i; }
	</script>--%>
    <script type="text/javascript" src="../../Scripts/dhtml-div.js">        { var i; }
  </script>
    <script type="text/javascript" src="../../Scripts/dhtml-help-setup.js">        { var i; }
  </script>
   <%-- <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">        { var i; }
	</script>--%>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
  </head>
  <body class="10pt" >
    <form name="frmData" id="frmData" runat="server">
      <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />
      <asp:HiddenField runat="server" ID="wsrp_rewrite_action_1" value="" />
      <asp:TextBox style="display:none" runat="server" name="hTabName" id="hTabName" />
      <asp:ScriptManager ID="SMgr" runat="server" />     
      <div class="msgheader" id="div_formtitle" runat="server">
        <asp:label id="formtitle" runat="server" Text="Property" />
        <asp:label id="formsubtitle" runat="server" Text="" />
      </div>
      <div id="Div1" class="errtextheader" runat="server">
        <asp:label id="formdemotitle" runat="server" Text="" />
      </div>
      <div class="tabGroup" id="TabsDivGroup" runat="server">
        <div class="Selected" nowrap="true" runat="server" name="TABSpropertyinfo" id="TABSpropertyinfo">
          <a class="Selected" HREF="#" runat="server" onclick="tabChange(this.name);return false;" RMXRef="" name="propertyinfo" id="LINKTABSpropertyinfo">Property Info</a>
        </div>
        <div class="tabSpace" runat="server" id="TBSPpropertyinfo">
          <nbsp />
          <nbsp />
        </div>
      </div>
      <div class="singletopborder"   runat="server" name="FORMTABpropertyinfo" id="FORMTABpropertyinfo">
        <table width="100%" border="0" cellspacing="4" celpadding="4" padding="0">
          <tr>
            <td>
              <asp:hiddenfield runat="server" id="hdpropertyinfo" />
            </td>
          </tr>
          <tr>
            <td>
    
              <div runat="server" class="half" id="div_pin" xmlns="">
                <asp:label runat="server" class="required" id="lbl_pin" Text="Property ID" />
                <span class="formw">
                  <asp:label runat="server" id="pin" RMXRef="/Instance/PropertyUnit/Pin" RMXType="text"  />
                </span>
              </div>
             
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_addr" xmlns="">
                <asp:label runat="server" class="required" id="lbl_addr" Text="Address" />
                <span class="formw">
                  <asp:label runat="server" id="addr" RMXRef="/Instance/PropertyUnit/Addr" RMXType="text" />
                </span>
              </div>
            </td>
          </tr>
      
          <tr>
            <td>
              <div runat="server" class="half" id="div_classofconstruction" xmlns="">
                <asp:label runat="server" class="required" id="lbl_classofconstruction" Text="Class of Construction" />
                <span class="formw">                  
                  <asp:label runat="server" id="ClassOfConstruction" RMXRef="/Instance/PropertyUnit/ClassOfConstruction" RMXType="text" />
                </span>
              </div>
             
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_yearofconstruction" xmlns="">
                <asp:label runat="server" class="required" id="lbl_yearofconstruction" Text="Year of Construction" />
                <span class="formw">
                  <asp:label runat="server" id="YearOfConstruction" RMXRef="/Instance/PropertyUnit/YearOfConstruction" RMXType="numeric" />
                </span>
              </div>
             
            </td>
          </tr>
          <tr>
            <td>
              
              <div runat="server" class="half" id="div_firedistrict" xmlns="">
                <asp:label runat="server" class="required" id="lbl_firedistrict" Text="Fire District" />
                <span class="formw">                  
                  <asp:label runat="server" id="FireDistrict" RMXRef="/Instance/PropertyUnit/FireDistrict" RMXType="text" />
                </span>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div runat="server" class="half" id="div_noofstories" xmlns="">
                <asp:label runat="server" class="required" id="lbl_noofstories" Text="No. of Stories" />
                <span class="formw">
                  <asp:label runat="server" id="NoOfStories" RMXRef="/Instance/PropertyUnit/NoOfStories" RMXType="numeric" />
                </span>
              </div>
            </td>
          </tr>
        </table>
      </div>
      <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Loading" />
    </form>
  </body>
</html>
