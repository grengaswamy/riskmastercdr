﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using Riskmaster.BusinessAdaptor.Common;
using System.Web.UI.HtmlControls;
using System.ServiceModel;
using Riskmaster.UI.IntegralInterfaceService;
using Riskmaster.Common;
using System.Xml;
using Riskmaster.BusinessHelpers;
using Telerik.Web.UI;
using System.Text;
using System.Configuration;
using System.Collections.Specialized;
using Riskmaster.Common; //sanoopsharma
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.UI.IntegralPolicyInterface
{
    public partial class IntegralPolicySysDownloadResults : System.Web.UI.Page
    {
        private const string GRID_DATA = "dtGridData";
        private const string pageID = "261"; //sanoopsharma

        private List<SearchRow> GridDataSource
        {
            get
            {
                return (List<SearchRow>)ViewState[GRID_DATA];
            }
            set
            {
                ViewState[GRID_DATA] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ErrorControl.Visible = false;

            string[] sValues;
            string sPolicyLOB = string.Empty;
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("IntegralPolicySysDownloadResults.aspx"), "IntegralPolicySysDownloadResultsValidation", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "IntegralPolicySysDownloadResultsValidation", sValidationResources, true);
            if (!Page.IsPostBack)
            {
                hdnPolicySysName.Value = Request.Form["dlSystemName"].ToString(); //sanoopsharma
                sValues = Request.Form["dlSystemName"].ToString().Split('|');
                hdnSystemName.Value = sValues[0];
                if(Request.Form["hdnClaimId"] != null)
                    hdnClaimId.Value = Request.Form["hdnClaimId"].ToString();

                sPolicyLOB = Request.Form["txtIntegralPolicyLOB"].ToString();
                if (sPolicyLOB != "")
                    hdnPolicyLobCode.Value = sPolicyLOB.Split(' ')[0];
                hdnPolicyLobId.Value = Request.Form["txtIntegralPolicyLOB_cid"].ToString();
                hdnPolicyLossDate.Value = Request.Form["txtIntegralLossDate"].ToString();
                hdnClaimType.Value = Request.Form["hdnClaimType"].ToString();
                hdnPolicySysName.Value = Request.Form["dlSystemName"].ToString(); //sanoopsharma
                if (string.IsNullOrEmpty(hdnClaimType.Value))
                {
                    string sDateOfLoss = string.Empty;
                    sDateOfLoss = Request.QueryString["PolicyDateCriteria"];
                    if (sDateOfLoss != "")
                    {
                        txtLossDate.Value = sDateOfLoss;
                    }
                }
                else
                {
                    txtLossDate.Style.Add("display", "none");
                    btnLossDate.Style.Add("display", "none");
                    lblDateOfLoss.Visible = false;
                }

                GetPolicySearchResults();
            }
        }

        private void GetPolicySearchResults()
        {
            DataSet oSearchRecords = null;
            PolicySearch oPolicySearchResponse = null;
            IntegralInterfaceBusinessHelper objHelper = null;

            try
            {
                oSearchRecords = new DataSet();
                objHelper = new IntegralInterfaceBusinessHelper();
                
                oPolicySearchResponse = objHelper.GetPolicySearchResults(GetSearchFilterParameters());
               
                hdnIntegralClaimEventSetting.Value = oPolicySearchResponse.IntegralClaimEventSetting;
                if (oPolicySearchResponse.SearchRowList.Length > 0)
                    GridDataSource = oPolicySearchResponse.SearchRowList.ToList<SearchRow>();
                else
                {
                    GridDataSource = null;
                    if (!string.IsNullOrEmpty(oPolicySearchResponse.ResponseError))
                    {
                        ErrorControl.Visible = true;
                        ErrorControl.DisplayError(oPolicySearchResponse.ResponseError);
                    }
                    else
                        CallNoResult();
                }
            }
            catch (FaultException<RMException> ee)
            {
                ErrorControl.Visible = true;
                ErrorHelper.logErrors(ee);
                ErrorControl.errorDom = ee.Detail.Errors;

            }
            catch (Exception ex)
            {
                GridDataSource = null;
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                if (oSearchRecords != null)
                    oSearchRecords = null;
                if (oPolicySearchResponse != null)
                    oPolicySearchResponse = null;
                if (objHelper != null)
                    objHelper = null;
            }
        }

        private PolicySearch GetSearchFilterParameters()
        {
            bool blnSuccess;
            string[] sValues;
            PolicySearch oPolicySearchRequest = new PolicySearch();
            oPolicySearchRequest.objSearchFilters = new SearchFilters();

            sValues = Request.Form["dlSystemName"].ToString().Split('|');
            oPolicySearchRequest.objSearchFilters.PolicySystemId = Conversion.CastToType<int>(sValues[0], out blnSuccess);

            //oPolicySearchRequest.objSearchFilters.LOB = "APV";

            if (!string.IsNullOrEmpty(Request.Form["txtIntegralPolicyLOB"].ToString()))
                oPolicySearchRequest.objSearchFilters.LOB = Conversion.CastToType<int>(Request.Form["txtIntegralPolicyLOB_cid"].ToString(), out blnSuccess);

            if (!string.IsNullOrEmpty(Request.Form["txtIntegralLossDate"].ToString()))
                oPolicySearchRequest.objSearchFilters.LossDate = (Request.Form["txtIntegralLossDate"].ToString().Trim());

            if (!string.IsNullOrEmpty(Request.Form["txtIntegralPolicyNumber"].ToString()))
                oPolicySearchRequest.objSearchFilters.PolicyNumber = Request.Form["txtIntegralPolicyNumber"].ToString().Trim();

            if (!string.IsNullOrEmpty(Request.Form["txtIntegralAgentLastName"].ToString()))
                oPolicySearchRequest.objSearchFilters.AgentLastName = Request.Form["txtIntegralAgentLastName"].ToString().Trim();

            if (!string.IsNullOrEmpty(Request.Form["txtIntegralAgentFirstName"].ToString()))
                oPolicySearchRequest.objSearchFilters.AgentFirstName = Request.Form["txtIntegralAgentFirstName"].ToString().Trim();

            if (!string.IsNullOrEmpty(Request.Form["txtIntegralAgentNumber"].ToString()))
                oPolicySearchRequest.objSearchFilters.AgentNumber = Request.Form["txtIntegralAgentNumber"].ToString().Trim();

            if (string.IsNullOrEmpty(Request.Form["txtIntegralInsuredLastName"].ToString()))
                oPolicySearchRequest.objSearchFilters.InsuredLastName = "";
            else
                oPolicySearchRequest.objSearchFilters.InsuredLastName = Request.Form["txtIntegralInsuredLastName"].ToString().Trim();

            if (string.IsNullOrEmpty(Request.Form["txtIntegralInsuredFirstName"].ToString()))
                oPolicySearchRequest.objSearchFilters.InsuredFirstName = "";
            else
                oPolicySearchRequest.objSearchFilters.InsuredFirstName = Request.Form["txtIntegralInsuredFirstName"].ToString().Trim();

            if (!string.IsNullOrEmpty(Request.Form["txtIntegralVehicleRegistrationNumber"].ToString()))
                oPolicySearchRequest.objSearchFilters.VehicleRegistrationNumber = Request.Form["txtIntegralVehicleRegistrationNumber"].ToString().Trim();

            if (!string.IsNullOrEmpty(Request.Form["txtIntegralVehicleChasisNumber"].ToString()))
                oPolicySearchRequest.objSearchFilters.VehicleChasisNumber = Request.Form["txtIntegralVehicleChasisNumber"].ToString().Trim();

            if (!string.IsNullOrEmpty(Request.Form["txtIntegralVehicleEngineNumber"].ToString()))
                oPolicySearchRequest.objSearchFilters.VehicleEngineNumber = Request.Form["txtIntegralVehicleEngineNumber"].ToString().Trim();

            return oPolicySearchRequest;
        }

        private void CallNoResult()
        {
            //string sNoResult = "<span xhtml:class='msgheader1' align='center'>Your search produced no results.<br/>Please refine your criteria selections.<br/></span>";
            string sNoResult = "<span xhtml:class='msgheader1' align='center'>"+
                                 AppHelper.GetResourceValue(pageID, "AlertNoResult", "1")            
                                +"<br/>" 
                                + AppHelper.GetResourceValue(pageID, "AlertRefineSearch", "1")
                                +"<br/></span>"; //sanoopsharma
            SetGridDisplay(false);
            ltNoResult.Text = sNoResult;
        }

        private void SetGridDisplay(bool Isdisplay)
        {
            if (Isdisplay)
            {
                gvPolicyList.Visible = true;
                ltNoResult.Visible = false;
                tblToolbar.Visible = true;
                this.ClientScript.RegisterStartupScript(this.GetType(), "setScroll", "window.scrollTo(0, document.body.scrollHeight)", true);
            }
            else
            {
                gvPolicyList.Visible = false;
                tblToolbar.Visible = false;
                ltNoResult.Visible = true;
            }
        }

        #region GridEvents
        protected void gvPolicyList_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            if (GridDataSource != null)
            {
                try
                {
                gvPolicyList.DataSource = GridDataSource;
        }
                catch
                {
                    SetGridDisplay(false);
                }
            }
            else
            {
                dvResults.Visible = false;
            }
        }
        protected void gvPolicyList_PreRender(object sender, EventArgs e)
        {
                GridFilterMenu menu = gvPolicyList.FilterMenu;
                int i = 0;
                while (i < menu.Items.Count)
                {
                    if (menu.Items[i].Text == "Between" || menu.Items[i].Text == "NotBetween" || menu.Items[i].Text == "IsNull" || menu.Items[i].Text == "NotIsNull")
                        menu.Items.RemoveAt(i);
                    else
                        i = (i + 1);

                }
            }

        #endregion
    }
}
