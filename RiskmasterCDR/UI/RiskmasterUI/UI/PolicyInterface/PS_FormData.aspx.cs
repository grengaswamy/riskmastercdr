﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Data;
using System.Xml.XPath;
using Riskmaster.UI.PolicyInterfaceService;
using Riskmaster.BusinessHelpers;
using Riskmaster.Common;
using System.ServiceModel;
using Riskmaster.BusinessAdaptor.Common;


namespace Riskmaster.UI.UI.PolicyInterface
{
    public partial class PS_FormData : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PolicyInterfaceBusinessHelper objHelper = null;
            PolicyEnquiry oRequest, oResponse;
            PolicyDownload oTrackingRequest, oTrackingResponse;
            bool bInSuccess;
            int iTemp;
            string sValues, sMode, sRowID;
            string[] sCollection;
            oRequest = new PolicyEnquiry();

            try
            {
                sMode = AppHelper.GetQueryStringValue("Mode");
                if (sMode == "Enquiry")
                {
                    
                    sValues = AppHelper.GetQueryStringValue("val");
                    if (sValues.Trim() == string.Empty)
                    {
                        oRequest.PolicyNumber = AppHelper.GetQueryStringValue("Policynum");
                        oRequest.PolicySymbol = AppHelper.GetQueryStringValue("symbol");
                        oRequest.Module = AppHelper.GetQueryStringValue("module");
                        oRequest.Location = AppHelper.GetQueryStringValue("LocationCmpny");
                        oRequest.UnitRiskLocation = AppHelper.GetQueryStringValue("RskLoc");
                        oRequest.UnitSubRiskLocation = AppHelper.GetQueryStringValue("SubRskLoc");
                        oRequest.MasterCompany = AppHelper.GetQueryStringValue("Master");
                        oRequest.PolicyIdentfier = AppHelper.GetQueryStringValue("Policynum");
                        oRequest.InsLine = AppHelper.GetQueryStringValue("InsLn");
                        oRequest.UnitNumber = AppHelper.GetQueryStringValue("UnitNo");
                        oRequest.LOB = AppHelper.GetQueryStringValue("LOB");//skhare7 Policy interface
                        
                    }
                    else
                    {
                        sCollection = sValues.Split('|');
                            oRequest.UnitNumber = sCollection[0];
                            oRequest.InsLine = sCollection[1];
                            oRequest.UnitRiskLocation = sCollection[2];
                            oRequest.UnitSubRiskLocation = sCollection[3];
                            oRequest.UnitState = sCollection[4];  //Contains unit state's state id in corresponding point system. "sCollection[4]" contains State's ShortCode in RMX
                            //oRequest.Product = sCollection[5];
                            //oRequest.UnitStatus = sCollection[6];
                            oRequest.PolicyNumber = sCollection[7];
                            oRequest.PolicySymbol = sCollection[8];
                            //oRequest.State = sCollection[9];
                            oRequest.LOB = sCollection[10];
                            //oRequest.IssueCode = sCollection[11];
                            oRequest.Location = sCollection[12];
                            oRequest.MasterCompany = sCollection[13];
                            oRequest.Module = sCollection[14];
                            //oRequest.EffDate = sCollection[15];
                            //oRequest.PolCompany = sCollection[16];
                            oRequest.PolicyIdentfier = sCollection[7];
                            oRequest.PolicySystemId = Conversion.CastToType<int>(sCollection[17], out bInSuccess);
                        
                    }

                    iTemp = Conversion.CastToType<int>(AppHelper.GetQueryStringValue("PolSysID"), out bInSuccess);
                    if (bInSuccess)
                    {
                        oRequest.PolicySystemId = iTemp;
                    }

                    objHelper = new PolicyInterfaceBusinessHelper();
                    oResponse = objHelper.GetEndorsementData(oRequest);
                    //SetTableDataSource(oResponse.ResponseAcordXML);
                    SetTableDataSource(oResponse.ResponseAcordXML);
                   
                }
                else if (sMode == "Tracking")
                {
                    oTrackingRequest = new PolicyDownload();
                    oTrackingRequest.TableName = AppHelper.GetQueryStringValue("TableNm");
                     sRowID = AppHelper.GetQueryStringValue("RowID");
                     oTrackingRequest.TableRowID = Conversion.CastToType<int>(sRowID, out bInSuccess);
                     oTrackingRequest.PolicyID = Conversion.CastToType<int>(AppHelper.GetQueryStringValue("PolID"), out bInSuccess);
                     oTrackingRequest.UnitTypeCode = AppHelper.GetQueryStringValue("TypeCd");
                     objHelper = new PolicyInterfaceBusinessHelper();
                     oTrackingResponse = objHelper.GetEndorsementDataForTracking(oTrackingRequest);
                     gvFormData.DataSource = oTrackingResponse.EndorsementData;
                     gvFormData.DataBind();
                }
            }
            catch (FaultException<RMException> excp)
            {
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(excp);
                ErrorControl1.errorDom = excp.Detail.Errors;
            }
            catch (Exception excp)
            {
                ErrorHelper.logErrors(excp);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(excp, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
     
        private void SetTableDataSource(string pResponse)
        {
            DataTable dtGridData = new DataTable();
            IEnumerable<XElement> oElements, oInfoElements;
            DataRow drRowData;
            XElement objDoc,oSubElement;
            XElement oInfoCd, oInfoVal;
            string sInsertedFormNo = string.Empty;

            try
            {
                objDoc = XElement.Parse(pResponse);
            }
            catch (Exception e)
            {
                ErrorHelper.logErrors(e);
                return;
            }
            
            //add columns to data table
            dtGridData.Columns.Add("Stat",typeof(string));
            dtGridData.Columns.Add("INS_LINE", typeof(string));
            dtGridData.Columns.Add("Loc", typeof(string));
            dtGridData.Columns.Add("Bldg", typeof(string));
            dtGridData.Columns.Add("Unit", typeof(string));
            dtGridData.Columns.Add("FORM_NUMBER", typeof(string));
            dtGridData.Columns.Add("EDITIONDATE", typeof(string));
            dtGridData.Columns.Add("FORM_DESCRIPTION", typeof(string));
            dtGridData.Columns.Add("FORM_ACTION", typeof(string));
            dtGridData.Columns.Add("EZ_SCM", typeof(string));
            dtGridData.Columns.Add("Data", typeof(string));
            dtGridData.Columns.Add("Iterative", typeof(string));
            dtGridData.Columns.Add("RateOp", typeof(string));
            dtGridData.Columns.Add("EntryDte", typeof(string));
            drRowData = dtGridData.NewRow();

            oElements = objDoc.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyFormDataRs/form");

            foreach (XElement oElement in oElements)
            {
                oSubElement = oElement.XPathSelectElement("./FormNumber");//Not to show duplicate rows in Form Data.
                drRowData["FORM_NUMBER"] = oSubElement.Value;
                if (sInsertedFormNo.Contains(oSubElement.Value + ","))
                {
                    continue;//dublicate form data to be skipped.
                }
                else
                {
                    sInsertedFormNo = sInsertedFormNo + oSubElement.Value + ",";
                }
                oInfoElements = oElement.XPathSelectElements("./com.csc_ItemIdInfo/OtherIdentifier");
                foreach (XElement oInfoElement in oInfoElements)
                {
                    oInfoCd = oInfoElement.XPathSelectElement("./OtherIdTypeCd");
                    oInfoVal = oInfoElement.XPathSelectElement("./OtherId ");
                    if (string.Compare(oInfoCd.Value, "com.csc_Stat", true) == 0)
                    {
                        drRowData["Stat"] = oInfoVal.Value;
                    }
                    else if (string.Compare(oInfoCd.Value, "com.csc_InsLineCd", true) == 0)
                    {
                        drRowData["INS_LINE"] = oInfoVal.Value;
                    }
                    else if (string.Compare(oInfoCd.Value, "com.csc_Loc", true) == 0)
                    {
                        drRowData["Loc"] = oInfoVal.Value;
                    }
                    else if (string.Compare(oInfoCd.Value, "com.csc_Bldg", true) == 0)
                    {
                        drRowData["Bldg"] = oInfoVal.Value;
                    }
                    else if (string.Compare(oInfoCd.Value, "com.csc_Unit", true) == 0)
                    {
                        drRowData["Unit"] = oInfoVal.Value;
                    }
                    else if (string.Compare(oInfoCd.Value, "com.csc_EZScrn", true) == 0)
                    {
                        drRowData["EZ_SCM"] = oInfoVal.Value;
                    }
                    else if (string.Compare(oInfoCd.Value, "com.csc_RATEOP", true) == 0)
                    {
                        drRowData["RATEOP"] = oInfoVal.Value;
                    }
                    else if (string.Compare(oInfoCd.Value, "com.csc_ENTRYDTE", true) == 0)
                    {
                        drRowData["ENTRYDTE"] = oInfoVal.Value;
                    }
                }

                oSubElement = oElement.XPathSelectElement("./EditionDt");
                drRowData["EDITIONDATE"] = oSubElement.Value;

                oSubElement = oElement.XPathSelectElement("./FormDesc");
                drRowData["FORM_DESCRIPTION"] = oSubElement.Value;

                oSubElement = oElement.XPathSelectElement("./FormTextContent");
                drRowData["FORM_ACTION"] = oSubElement.Value;

                oSubElement = oElement.XPathSelectElement("./FormDataArea");
                drRowData["Data"] = oSubElement.Value;

                oSubElement = oElement.XPathSelectElement("./IterationNumber");
                drRowData["Iterative"] = oSubElement.Value;
                
                dtGridData.Rows.Add(drRowData);

                drRowData = dtGridData.NewRow();
            }
            gvFormData.DataSource = dtGridData;
            gvFormData.DataBind();
        }

        //added by swati
        private void SetStgngTableDataSource(string pResponse)
        {
            DataTable dtGridData = new DataTable();
            IEnumerable<XElement> oElements, oInfoElements;
            DataRow drRowData;
            XElement objDoc, oSubElement;
            XElement oInfoCd, oInfoVal;
            string sInsertedFormNo = string.Empty;

            try
            {
                objDoc = XElement.Parse(pResponse);
            }
            catch (Exception e)
            {
                ErrorHelper.logErrors(e);
                return;
            }

            //add columns to data table
            dtGridData.Columns.Add("Stat", typeof(string));
            dtGridData.Columns.Add("INS_LINE", typeof(string));
            dtGridData.Columns.Add("Loc", typeof(string));
            dtGridData.Columns.Add("Bldg", typeof(string));
            dtGridData.Columns.Add("Unit", typeof(string));
            dtGridData.Columns.Add("FORM_NUMBER", typeof(string));
            dtGridData.Columns.Add("EDITIONDATE", typeof(string));
            dtGridData.Columns.Add("FORM_DESCRIPTION", typeof(string));
            dtGridData.Columns.Add("FORM_ACTION", typeof(string));
            dtGridData.Columns.Add("EZ_SCM", typeof(string));
            dtGridData.Columns.Add("Data", typeof(string));
            dtGridData.Columns.Add("Iterative", typeof(string));
            dtGridData.Columns.Add("RateOp", typeof(string));
            dtGridData.Columns.Add("EntryDte", typeof(string));
            drRowData = dtGridData.NewRow();

            oElements = objDoc.XPathSelectElements("FormData");

            foreach (XElement oElement in oElements)
            {
                oSubElement = oElement.XPathSelectElement("FormNumber");//Not to show duplicate rows in Form Data.
                drRowData["FORM_NUMBER"] = oSubElement.Value;
                if (sInsertedFormNo.Contains(oSubElement.Value + ","))
                {
                    continue;//dublicate form data to be skipped.
                }
                else
                {
                    sInsertedFormNo = sInsertedFormNo + oSubElement.Value + ",";
                }

                oSubElement = oElement.XPathSelectElement("Stat");
                drRowData["Stat"] = oSubElement.Value;

                oSubElement = oElement.XPathSelectElement("InsLine");
                drRowData["INS_LINE"] = oSubElement.Value;

                oSubElement = oElement.XPathSelectElement("Loc");
                drRowData["Loc"] = oSubElement.Value;

                oSubElement = oElement.XPathSelectElement("Bldg");
                drRowData["Bldg"] = oSubElement.Value;

                oSubElement = oElement.XPathSelectElement("Unit");
                drRowData["Unit"] = oSubElement.Value;

                oSubElement = oElement.XPathSelectElement("EditionDate");
                drRowData["EDITIONDATE"] = oSubElement.Value;

                oSubElement = oElement.XPathSelectElement("FormDescription");
                drRowData["FORM_DESCRIPTION"] = oSubElement.Value;

                oSubElement = oElement.XPathSelectElement("FormAction");
                drRowData["FORM_ACTION"] = oSubElement.Value;

                oSubElement = oElement.XPathSelectElement("EzScm");
                drRowData["EZ_SCM"] = oSubElement.Value;                       

                oSubElement = oElement.XPathSelectElement("Data");
                drRowData["Data"] = oSubElement.Value;

                oSubElement = oElement.XPathSelectElement("Iterative");
                drRowData["Iterative"] = oSubElement.Value;

                oSubElement = oElement.XPathSelectElement("RateOp");
                drRowData["RateOp"] = oSubElement.Value;

                oSubElement = oElement.XPathSelectElement("EntryDte");
                drRowData["EntryDte"] = oSubElement.Value;       

                dtGridData.Rows.Add(drRowData);

                drRowData = dtGridData.NewRow();
            }
            gvFormData.DataSource = dtGridData;
            gvFormData.DataBind();
        }
        //change end here by swati
    }
}