﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OpenPolicySystem.aspx.cs" Inherits="Riskmaster.UI.UI.PolicyInterface.OpenPolicySystem" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">
        function ValidateAndSubmit()
        {
            var pntPwd = document.getElementById("pntPwd");
            var pointURL = document.getElementById("pntURL");
            if (pntPwd != null && pointURL != null && pntPwd.value != "" && pointURL.value !="") {
                document.forms[0].submit();
            }
            else {
                alert("Configuration issue in Policy system.");
                window.close();
            }

        }

    </script>
</head>
<body onload="return ValidateAndSubmit();">
    <form id="frmData" runat="server">
    <div>
     <table>
            <tr>
                <td><%--Policy Number : --%></td>
                <td colspan="3">
                    <asp:TextBox ID="Symbol" Text="WCV"  runat="server" style="display:none;"></asp:TextBox>
                    <asp:TextBox ID="PolNum"  Text=""  runat="server" style="display:none;"></asp:TextBox>
                    <asp:TextBox ID="Module"  Text="00"  runat="server" style="display:none;"></asp:TextBox>
                    <asp:TextBox ID="LOC"  Text="00"  runat="server" style="display:none;"></asp:TextBox>
                    <asp:TextBox ID="MCO"  Text="05"  runat="server" style="display:none;"></asp:TextBox>
               <%--     <input type="text" name="Symbol" value="WCV"/>
                    <input type="text" name="PolNum" value="0000011"/>
                    <input type="text" name="Module" value="00"/>--%></td>
            </tr>
            <tr>
                <td><%--Location & MCO:--%> </td>
                <td colspan="3">
                  <%--  <input type="text" name="LOC" value="00"/>
                    <input type="text" name="MCO" value="05"/>--%>
                    <asp:HiddenField ID="srcLoc"  Value="RMA" runat="server" />
                    <asp:HiddenField ID="pntUser"  value=""  runat="server" />
                    <asp:HiddenField ID="pntPwd"  value=""  runat="server" />
                    <asp:HiddenField ID="PolAction"  Value="IN"  runat="server" />
                    <asp:HiddenField ID="pntURL"  Value=""  runat="server" />
                    
               <%-- <input type="hidden" name="srcLoc" value="RMA" />
                    <input type="hidden" name="pntUser" value="LAB07"/>
                    <input type="hidden" name="pntPwd" value="sangeet#4"/>
                    <input type="hidden" name="PolAction" value="IN"/>--%>
                </td>
            </tr>

            <tr>
                <td colspan="3">
                   <%-- <input type="SUBMIT" value='Open Policy Inq' tabindex="15" />--%>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
