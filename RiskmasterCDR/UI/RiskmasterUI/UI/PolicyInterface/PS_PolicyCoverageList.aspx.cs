﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Data;
using Riskmaster.BusinessAdaptor.Common;
using System.ServiceModel;
using Riskmaster.UI.PolicyInterfaceService;
using Riskmaster.Common;
using Riskmaster.BusinessHelpers;
using System.Xml.Linq;
using System.Xml.XPath;

namespace Riskmaster.UI.UI.PolicyInterface
{
    public partial class PS_PolicyCoverageList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PolicyEnquiry oRequest, oResponse;
            PolicyInterfaceBusinessHelper oHelper;
            string sValues;
            string[] sCollection;
            bool bInSuccess;
            if (!Page.IsPostBack)
            {
                try
                {
                    sValues = AppHelper.GetQueryStringValue("val");
                    sCollection = sValues.Split('|');
                    oRequest = new PolicyEnquiry();
                    oRequest.UnitNumber = sCollection[18];// Assigning StatUnitNumber to fetch coverage list.
                    oRequest.InsLine = sCollection[1];
                    oRequest.UnitRiskLocation = sCollection[2];
                    oRequest.UnitSubRiskLocation = sCollection[3];
                    oRequest.UnitState = sCollection[4];
                    oRequest.Product = sCollection[5];
                    //oRequest.UnitStatus = sCollection[6];
                    oRequest.PolicyNumber = sCollection[7];
                    oRequest.PolicySymbol = sCollection[8];
                    //oRequest.State = sCollection[9];
                    oRequest.LOB = sCollection[10];
                    oRequest.IssueCode = sCollection[11];
                    oRequest.Location = sCollection[12];
                    oRequest.MasterCompany = sCollection[13];
                    oRequest.Module = sCollection[14];
                    //oRequest.EffDate = sCollection[15];
                    //oRequest.PolCompany = sCollection[16];
                    oRequest.PolicyIdentfier = sCollection[7];
                    oRequest.PolicySystemId = Conversion.CastToType<int>(sCollection[17], out bInSuccess);
                    oRequest.PolLossDt = sCollection[20];
                    oRequest.ClaimDateReported = AppHelper.GetQueryStringValue("ClaimDateReported");//Ashish Ahuja Claims Made Jira 1342

                    //set hidden parameters
                    hfUnitNum.Value = sCollection[0];
                    //hfProduct.Value = sCollection[8];
                    hfPolicyNum.Value = sCollection[7];
                    hfLobCd.Value = sCollection[10];
                    hfIssueCd.Value = sCollection[11];
                    hfLocCmpy.Value = sCollection[12];
                    hfMasterCmpny.Value = sCollection[13];
                    hfModule.Value = sCollection[14];
                    hfCmpnyProductCd.Value = sCollection[8];
                    hfPolicySystemIdentifier.Value = sCollection[17];
                    hfPolCompnay.Value = sCollection[16];
                    hfStatUnitNum.Value = sCollection[18];
                    oHelper = new PolicyInterfaceBusinessHelper();
                    oResponse = oHelper.GetUnitCoverageListResult(oRequest);

                        PopulateDataSet(oResponse.ResponseAcordXML, oResponse.BaseLOBLine, oRequest.IssueCode);
                    
                    if (gvData.Rows.Count == 0)
                    {
                        btnCovDetail.Enabled = false;
                    }
                }
                catch (FaultException<RMException> ee)
                {
                    ErrorControl1.Visible = true;
                    ErrorHelper.logErrors(ee);
                    ErrorControl1.errorDom = ee.Detail.Errors;
                }
                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                }

            }
        }

        private void PopulateDataSet(string pXML, string pLOB, string pIssueCd)
        {
            DataTable dtData = new DataTable();
            string sVal = string.Empty, sInsLineCd, sUnitNum, sLoc, sSubLoc, sRateState, sCovCd, sCovSeqNo, sCovStatus, sTransSeq, sProduct;
            dtData.Columns.Add("CovCode", typeof(string));
            dtData.Columns.Add("Desc", typeof(string));
            dtData.Columns.Add("Status", typeof(string));
            dtData.Columns.Add("Prem", typeof(string));
            dtData.Columns.Add("Unit", typeof(string));
            dtData.Columns.Add("Deduc", typeof(string));
            dtData.Columns.Add("val", typeof(string));
            XElement oDoc, oTemp;
            oDoc = XElement.Parse(pXML);
            IEnumerable<XElement> oElements;
            DataRow dr;
            oElements = oDoc.XPathSelectElements("ClaimsSvcRs/com.csc_UnitCoverageListRs/com.csc_CoverageLossInfo");
            foreach (XElement oElement in oElements)
            {
                sVal = string.Empty;
                sUnitNum = string.Empty; sInsLineCd = string.Empty;
                sLoc = string.Empty; sSubLoc = string.Empty;
                sRateState = string.Empty; sCovCd = string.Empty;
                sCovSeqNo = string.Empty; sCovStatus = string.Empty;
                sTransSeq = string.Empty; sProduct = string.Empty;
                dr = dtData.NewRow();
                oTemp = oElement.XPathSelectElement("Coverage/CoverageCd");
                if (oTemp != null)
                {
                    dr[0] = oTemp.Value;
                    sCovCd = oTemp.Value;
                }

                oTemp = oElement.XPathSelectElement("Coverage/CoverageDesc");
                if (oTemp != null)
                    dr[1] = oTemp.Value;

                oTemp = oElement.XPathSelectElement("com.csc_RecordStatus");
                if (oTemp != null)
                {
                    dr[2] = oTemp.Value;
                    sCovStatus = dr[2].ToString();
                }

                oTemp = oElement.XPathSelectElement("Coverage/CurrentTermAmt");
                if (oTemp != null)
                    dr[3] = oTemp.Value;

                //if (pLOB == "WL")//skhare7 Policy interface
                //{
                //    oTemp = oElement.XPathSelectElement("ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_ClassSeq']/OtherId");
                //    if (oTemp != null)
                //        dr[4] = oTemp.Value;
                //    sCovSeqNo = dr[4].ToString();
                //}
                //else
                //{
                oTemp = oElement.XPathSelectElement("ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_CoverageSeq']/OtherId");
                if (oTemp != null)
                    dr[4] = oTemp.Value;
                sCovSeqNo = dr[4].ToString();
                //}

                oTemp = oElement.XPathSelectElement("Coverage/com.csc_Exposure");
                if (oTemp != null)
                    dr[5] = oTemp.Value;

                oTemp = oElement.XPathSelectElement("Coverage/Deductible/FormatCurrencyAmt/Amt");
                if (oTemp != null && oTemp.Value!=string.Empty)
                    dr[5] = dr[5].ToString()+"/"+ oTemp.Value;

                oTemp = oElement.XPathSelectElement("ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId");
                if (oTemp != null )
                    sInsLineCd = oTemp.Value;
                
                oTemp = oElement.XPathSelectElement("ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNum']/OtherId");
                if (oTemp != null)
                    sUnitNum = oTemp.Value;

                oTemp = oElement.XPathSelectElement("ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskSubLoc']/OtherId");
                if (oTemp != null)
                    sSubLoc = oTemp.Value;

                oTemp = oElement.XPathSelectElement("ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskLoc']/OtherId");
                if (oTemp != null)
                    sLoc = oTemp.Value;

                oTemp = oElement.XPathSelectElement("ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RateState']/OtherId");
                if (oTemp != null)
                    sRateState = oTemp.Value;

                //if (pLOB == "WCV" || pLOB=="WC" || pLOB=="WCA")
                //{
                //    oTemp = oElement.XPathSelectElement("Coverage/com.csc_ExpCovCd");
                //    if (oTemp != null)
                //        sTransSeq = oTemp.Value;
                //}
                //if(pIssueCd=="M")
                //{
                oTemp = oElement.XPathSelectElement("ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_TrxnSeq']/OtherId");
                if (oTemp != null)
                    sTransSeq = oTemp.Value;
                //}

                oTemp = oElement.XPathSelectElement("ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Product']/OtherId");
                if (oTemp != null)
                    sProduct = oTemp.Value;

                dr[6] = hfPolicyNum.Value + "|" + hfCmpnyProductCd.Value + "|" + hfLobCd.Value + "|" + hfIssueCd.Value + "|" + hfLocCmpy.Value
                    + "|" + hfMasterCmpny.Value + "|" + hfModule.Value + "|" + sInsLineCd + "|" + hfUnitNum.Value + "|" + sProduct + "|" + sLoc + "|" + sSubLoc
                    + "|" + sRateState + "|" + sCovCd + "|" + sCovSeqNo + "|" + sTransSeq + "|" + sCovStatus + "|" + hfPolicySystemIdentifier.Value + "|" + hfPolCompnay.Value
                    + "|" + hfStatUnitNum.Value + "|" + pLOB;
               
                dtData.Rows.Add(dr);
            }
            gvData.DataSource = dtData;
            gvData.DataBind();
          
        }

        private void PopulateStagingDataSet(string p_sXML)
        {
            DataTable dtData = new DataTable();
            string sVal = string.Empty, sInsLineCd, sUnitNum, sLoc, sSubLoc, sRateState, sCovCd, sCovSeqNo, sCovStatus, sTransSeq, sProduct;
            dtData.Columns.Add("CovCode", typeof(string));
            dtData.Columns.Add("Desc", typeof(string));
            dtData.Columns.Add("Status", typeof(string));
            dtData.Columns.Add("Prem", typeof(string));
            dtData.Columns.Add("Unit", typeof(string));
            dtData.Columns.Add("Deduc", typeof(string));
            dtData.Columns.Add("val", typeof(string));
            XElement oDoc, oTemp;
            oDoc = XElement.Parse(p_sXML);
            DataRow dr;

            try
            {
                foreach (XElement xElement in oDoc.XPathSelectElements("Coverage"))
                {
                    dr = dtData.NewRow();

                    oTemp = xElement.XPathSelectElement("CoverageCd");
                    if (oTemp!=null)
                    {
                        dr["CovCode"] = oTemp.Value;
                    }

                    oTemp = xElement.XPathSelectElement("Description");
                    if (oTemp != null)
                    {
                        dr["Desc"] = oTemp.Value;
                    }

                    oTemp = xElement.XPathSelectElement("Premium");
                    if (oTemp != null)
                    {
                        dr["Prem"] = oTemp.Value;
                    }

                    oTemp = xElement.XPathSelectElement("CvgSeqNum");
                    if (oTemp != null)
                    {
                        dr["Unit"] = oTemp.Value;
                    }

                    oTemp = xElement.XPathSelectElement("Deductible");
                    if (oTemp != null)
                    {
                        dr["Deduc"] = oTemp.Value;
                    }
                    oTemp = xElement.XPathSelectElement("PolCvgRowId");
                    if (oTemp != null)
                    {
                        dr["val"] = oTemp.Value;
                    }

                    dtData.Rows.Add(dr);
                }

                gvData.DataSource = dtData;
                gvData.DataBind();
            }
            catch (Exception)
            {
                
                throw;
            }
        }

    }
 }
