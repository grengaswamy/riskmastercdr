﻿/***************************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            
 ***************************************************************************************************
 * 05/05/2014 | 33574   | pgupta93   | Changes for Continuous Trigger and Policy Search functionality.
 ***************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.IO;
using Riskmaster.BusinessHelpers;
using Riskmaster.BusinessAdaptor.Common;
using System.ServiceModel;
using Riskmaster.UI.PolicyInterfaceService;
using System.Data;
using System.Collections;
using Riskmaster.Common;
using System.Xml;

namespace Riskmaster.UI.UI.PolicyInterface
{

    public partial class PS_UnitInterestDownload : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                //if (!string.IsNullOrEmpty(AppHelper.GetQueryStringValue("currentmode")))
                //{
                //    DeleteOldFiles(AppHelper.GetQueryStringValue("currentmode"));
                //}
                //hdMDIClaimId.Value = AppHelper.GetQueryStringValue("MDIClaimId");
                //hdnMDIEventId.Value = AppHelper.GetQueryStringValue("MDIEventId");
                if (!IsPostBack)
                {
					
                    txtPolicyId.Text = AppHelper.GetQueryStringValue("PolicyId");
                    txtLossDate.Text = AppHelper.GetQueryStringValue("LossDate");
                    txtPolicyFlag.Text = AppHelper.GetQueryStringValue("DownloadPolicyWithoutClaim");//MITS:33574
                    txtLoBCode.Text = AppHelper.GetQueryStringValue("LOBCode");//MITS:33574
                    hdnClaimReportedDate.Text = AppHelper.GetQueryStringValue("ClaimDateReported");////Ashish Ahuja: Claims Made Jira 1342
                    GetDownloadOptions();
                }
            }
            catch (FaultException<RMException> ee)
            {
                btnSave.Enabled = false;
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;



            }
            catch (Exception ee)
            {
                btnSave.Enabled = false;
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
        private void DeleteOldFiles(string p_smode)
        {
            DisplayMode objMode = null;
            PolicyInterfaceBusinessHelper objHelper = new PolicyInterfaceBusinessHelper();
            try
            {
                objMode = new DisplayMode();
                objMode.Mode = p_smode;
                objHelper.DeleteOldFiles(objMode);
            }
            catch (FaultException<RMException> ee)
            {
                btnSave.Enabled = false;
                throw ee;

            }
            catch (Exception ee)
            {
                btnSave.Enabled = false;
                throw ee;

            }
            finally
            {

                objMode = null;
            }
        }

        private void AddDataToHashTable(ref  Dictionary<string, string> objTable)
        {
           
         
                objTable.Add("TaxID", "Tax ID");
                objTable.Add("FirstName", "First Name");
                objTable.Add("MiddleName", "Middle Name");
                objTable.Add("LastName", "Last Name");
                objTable.Add("RoleCd", "Role");
                objTable.Add("StatUnitNumber", "UnitNo");
                // Pradyumna GAP 16 WWIG - start
                int iPolicyStagingId = 0;
                bool bSuccess = false;
                iPolicyStagingId = Conversion.CastToType<int>(txtPolicyStagingId.Text, out bSuccess);
                if (iPolicyStagingId > 0)
                {
                    objTable.Add("TableId", "TableId");
                }
                // Pradyumna GAP 16 WWIG - Ends    
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            PolicyInterfaceBusinessHelper objHelper = new PolicyInterfaceBusinessHelper();
            DisplayFileData oFileData = null;
            DataSet oDS = new DataSet();
            DataRow objRow;
            //  Hashtable objSelectedValues = null;
            XmlDocument objRetunDoc = null;
            //string[] arrSelectedValue = null;
            SaveDownloadOptions oSaveDownloadOptions = null;
            DataView objView = null;
            try
            {

                oSaveDownloadOptions = new SaveDownloadOptions();
                oSaveDownloadOptions.Mode = txtMode.Text;
                oSaveDownloadOptions.SelectedValue = txtSelectedValues.Text;
                
                SaveData();
                

            }
            catch (FaultException<RMException> ee)
            {
                btnSave.Enabled = false;
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;

            }
            catch (Exception ee)
            {
                btnSave.Enabled = false;
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
            finally
            {
                if (objHelper != null)
                    objHelper = null;
                if (oDS != null)
                    oDS = null;
                if (oFileData != null)
                    oFileData = null;
                objRetunDoc = null;
            }

        }

        private void GetDownloadOptions()
        {
            PolicyInterfaceBusinessHelper objHelper = new PolicyInterfaceBusinessHelper();
            DisplayFileData oFileData = null;
            DataSet oDS = new DataSet();
            DataRow objRow;
            XmlDocument objRetunDoc = null;

            try
            {
                oFileData = objHelper.GetXml();
                
                txtMode.Text = oFileData.Mode;
                string sMode = txtMode.Text.ToLower();
                switch (sMode)
                {
                    case "coveragelist":
                        //redirect to coverage list page
                        lblMode.Visible = false;
                        DataGrid.Style.Value = "display:none;";
                        ClientScript.RegisterStartupScript(this.GetType(), "script", "OpenCoverageList();", true);
                        break;
                    case "completed":
                        lblMode.Visible = false;
                        //  div2.Style.Value = "display:none;";
                        DataGrid.Style.Value = "display:none;";
                        //caggarwal4 Merged for RMA-9771
                        ClientScript.RegisterStartupScript(this.GetType(), "script", "isSuccess=true;showPolicySuccess();", true);
                        break;
                    default:
                        
                        ViewState["EntityTypelist"] = oFileData.EntityTypelist;
                        if (oFileData != null && !string.IsNullOrEmpty(oFileData.ResponseXML))
                        {
                            objRetunDoc = new XmlDocument();
                            objRetunDoc.LoadXml(oFileData.ResponseXML);
                            oDS = ConvertXmlDocToDataSet(objRetunDoc);
                            AddColumnsToGrid();
                            if (oDS != null && oDS.Tables.Count > 0)
                            {
                                gvDownloadItems.DataSource = oDS.Tables[1];
                                gvDownloadItems.DataBind();
                                DataGrid.Style.Value = "";
                            }
                            else
                            {
                                btnSave.Enabled = false;
                            }
                        }
                        break;


                }
                //ViewState["EntityTypelist"] = oFileData.EntityTypelist;
                //if (oFileData != null && !string.IsNullOrEmpty(oFileData.ResponseXML) && !string.Equals(oFileData.Mode, "completed", StringComparison.InvariantCultureIgnoreCase) && !string.Equals(oFileData.Mode, "CoverageList", StringComparison.InvariantCultureIgnoreCase))
                //{
                //    objRetunDoc = new XmlDocument();
                //    objRetunDoc.LoadXml(oFileData.ResponseXML);
                //    oDS = ConvertXmlDocToDataSet(objRetunDoc);
                //    AddColumnsToGrid();
                //    if (oDS != null && oDS.Tables.Count > 0)
                //    {
                //        gvDownloadItems.DataSource = oDS.Tables[1];
                //        gvDownloadItems.DataBind();
                //        DataGrid.Style.Value = "";
                //    }
                //    else
                //    {
                //        btnSave.Enabled = false;
                //    }
                //    //  div2.Style.Value = "display:none;";
                //}
                //else
                //{
                //    lblMode.Visible = false;
                //    //  div2.Style.Value = "display:none;";
                //    DataGrid.Style.Value = "display:none;";
                //    ClientScript.RegisterStartupScript(this.GetType(), "script", "showPolicySuccess();", true);
                //}
            }
            catch (FaultException<RMException> ee)
            {
                btnSave.Enabled = false;
                throw ee;


            }
            catch (Exception ee)
            {
                btnSave.Enabled = false;
                throw ee;
            }
            finally
            {
                if (objHelper != null)
                    objHelper = null;
                if (oDS != null)
                    oDS = null;
                if (oFileData != null)
                    oFileData = null;
                objRetunDoc = null;
            }

        }

        private void SaveData()
        {
            PolicyInterfaceBusinessHelper objHelper = new PolicyInterfaceBusinessHelper();
            DisplayFileData oFileData = null;
            DataSet oDS = new DataSet();

            SaveDownloadOptions oSaveDownloadOptions = null;
            DataRow objRow;

            XmlDocument objRetunDoc = null;
            bool bResult = false;
            //Hashtable objSelectedValues = null;
            string sSelectedValues = string.Empty;
            bool bSuccess = false;
            try
            {
                oSaveDownloadOptions = new SaveDownloadOptions();

                //objSelectedValues =(Hashtable) ViewState["SelectedValues"];

                foreach (string objEntry in txtSelectedValues.Text.Split(','))
                {
                    if (string.IsNullOrEmpty(sSelectedValues))
                    {
                        sSelectedValues = objEntry.Replace("'", "");
                    }
                    else
                    {
                        sSelectedValues = sSelectedValues + "," + objEntry.Replace("'", "");
                    }

                }
                oSaveDownloadOptions.AddEntityAs = txtAddEntityAs.Text;
                oSaveDownloadOptions.SelectedValue = sSelectedValues;
                oSaveDownloadOptions.Mode = txtMode.Text;
                oSaveDownloadOptions.PolicyId = Conversion.CastToType<int>(txtPolicyId.Text, out bSuccess);
                // Pradyumna Mits 36013 - Start
				bResult = objHelper.SaveOptions(ref oSaveDownloadOptions);
               
                if (bResult)
                {
                    GetDownloadOptions();


                 //   // ClientScript.RegisterStartupScript(this.GetType(), "UpdateIdsScript", "PreserveDownloadedRecordsIds('" + txtMode.Text + "', '" + oSaveDownloadOptions.UpdatedIds + "');", true);
                 ////   GetDownloadOptions();
                 //   lblMode.Visible = false;
                 //   //  div2.Style.Value = "display:none;";
                 //   DataGrid.Style.Value = "display:none;";
                 //   ClientScript.RegisterStartupScript(this.GetType(), "script", "showPolicySuccess();", true);
                }


            }
            catch (FaultException<RMException> ee)
            {
                btnSave.Enabled = false;
                hfException.Value = "true";
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;

            }
            catch (Exception ee)
            {
                btnSave.Enabled = false;
                hfException.Value = "true";
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            finally
            {
                if (objHelper != null)
                    objHelper = null;
                if (oDS != null)
                    oDS = null;
                if (oFileData != null)
                    oFileData = null;
                objRetunDoc = null;
            }
        }

     

        private void RemoveFromDataSet()
        {
            DataSet p_objSet = (DataSet)ViewState["DbRecord"];

            DataRow[] arrRow = null;
         
//if (string.Equals(txtMode.Text, "entity", StringComparison.InvariantCultureIgnoreCase))
                arrRow = p_objSet.Tables[0].Select("TaxId =" + ViewState["ConflictingValue"]);
           
            foreach (DataRow objrow in arrRow)
            {
                p_objSet.Tables[0].Rows.Remove(objrow);
            }

            ViewState["DbRecord"] = p_objSet;

        }

        private void AddColumnsToGrid()
        {
            Dictionary<string, string> objPropertyHeader = null;
            Dictionary<string, string> objEntityHeader = null;
            //Dictionary<string,string> objVehicleHeader = null;
            //Dictionary<string, string> objDriverHeader = null;
            Dictionary<string, string> objUnitHeader = null;

            BoundField bfGridColumn;
            int i = 1;

            // Pradyumna GAP16 WWIG - Start
            int iPolicyStagingId = 0;
            bool bSuccess = false;
            iPolicyStagingId = Conversion.CastToType<int>(txtPolicyStagingId.Text, out bSuccess);
            // Pradyumna GAP16 WWIG - End

          // if (string.Equals(txtMode.Text, "ENTITY", StringComparison.InvariantCultureIgnoreCase))
           // {
                AddCheckboxColumnField();
                objEntityHeader = new Dictionary<string, string>();
                AddDataToHashTable(ref objEntityHeader);
                foreach (string skey in objEntityHeader.Keys)
                {
                    bfGridColumn = new BoundField();
                    bfGridColumn.DataField = skey;
                    bfGridColumn.HeaderText = Conversion.ConvertObjToStr(objEntityHeader[skey]);
                    
                    if (iPolicyStagingId > 0 && string.Compare(skey, "TableId", true) == 0)
                    {
                        bfGridColumn.Visible = false;
                    }
                    else
                    {
                        bfGridColumn.Visible = true;
                    }
                    //if (bSelectDuplicate)
                    //    gvdConflictingData.Columns.Add(bfGridColumn);
                    //else
                    gvDownloadItems.Columns.Add(bfGridColumn);
                    lblMode.Text = "Download Unit Interest";
                }
           // }
           
           
        }
        protected void gvDownloadItemsData_OnRowBound(object sender, GridViewRowEventArgs e)
        {
//if (string.Equals(txtMode.Text, "entity", StringComparison.InvariantCultureIgnoreCase))
//{
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataSet oDS = new DataSet();
                    DropDownList gridDDDownloadItemAs = null;
                    gridDDDownloadItemAs = (DropDownList)e.Row.Cells[1].FindControl("ddDownloadItemAs");
                    oDS.ReadXml(new StringReader(ViewState["EntityTypelist"].ToString()));
                    gridDDDownloadItemAs.DataSource = oDS.Tables[0];
                    gridDDDownloadItemAs.DataValueField = "option_text";
                    gridDDDownloadItemAs.DataTextField = "value";
                    gridDDDownloadItemAs.DataBind();

                    //tanawr2 - Defaulting dropdown for policy Download from staging - start
                    DataRowView rowView = e.Row.DataItem as DataRowView;
                    if (rowView.Row.Table.Columns.Contains("TableId"))
                    {
                        if (rowView["TableId"] != null)
                        {
                            gridDDDownloadItemAs.SelectedValue = Convert.ToString(rowView["TableId"]);
                        }
                    }
                    //tanwar2 - end
                }


          //  }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {


                HiddenField gridhdnSequenceNumber = (HiddenField)e.Row.Cells[0].FindControl("SequenceNumber");

                if (gridhdnSequenceNumber != null)
                {
                    try
                    {
                        gridhdnSequenceNumber.Value = DataBinder.Eval(e.Row.DataItem, "SequenceNumber").ToString();
                    }
                    catch
                    {
                        gridhdnSequenceNumber.Value = "";
                    }
                }
            }
        }

        private void AddCheckboxColumnField()
        {
            TemplateField oTemplateField = new TemplateField();
            oTemplateField.ItemTemplate = new GridViewItemTemplate("cbDownloadItemsGrid", "SequenceNumber");

            gvDownloadItems.Columns.Insert(0, oTemplateField);


        }

    }
}