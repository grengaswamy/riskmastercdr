﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.Common;
using System.Xml;
using Riskmaster.BusinessHelpers;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Collections.Generic;
using System.ServiceModel;
using Riskmaster.BusinessAdaptor.Common;
using System.Data;
using Riskmaster.UI.IntegralInterfaceService;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using System.Web.SessionState;
using Telerik.Web.UI;
using System.Xml.Serialization;
using Riskmaster.RMXResourceManager;


namespace Riskmaster.UI.UI.IntegralPolicyInterface
{
    public partial class IntegralPolicyData : System.Web.UI.Page
    {
        private DataTable UnitDataSource
        {// Viewstate in place to support radgrid filtering / sorting/ paging functions
            get
            {
                return (DataTable)ViewState["dtUnitData"];
            }
            set
            {
                ViewState["dtUnitData"] = value;
            }
        }
        private List<string> SelectedUnits
        {
            get
            {
                if (ViewState["lstSelectedUnits"] == null)
                    ViewState["lstSelectedUnits"] = new List<string>();

                return (List<string>)(this.ViewState["lstSelectedUnits"]);
            }
            set
            {
                ViewState["lstSelectedUnits"] = value;
            }
        }

        private DataTable UnitInterestDataSource
        {// Viewstate in place to support radgrid filtering based on unit selection
            get
            {
                return (DataTable)ViewState["dtUnitInterestData"];
            }
            set
            {
                ViewState["dtUnitInterestData"] = value;
            }
        }
        private DataTable CoverageDataSource
        {// Viewstate in place to support radgrid filtering based on unit selection
            get
            {
                return (DataTable)ViewState["dtCoveragesData"];
            }
            set
            {
                ViewState["dtCoveragesData"] = value;
            }
        }

        private string InterestListRoles
        {// Viewstate in place to load multiple Add As combos in interest list grid
            get
            {
                return (string)ViewState["InterestListRoles"];
            }
            set
            {
                ViewState["InterestListRoles"] = value;
            }
        }

        private ExternalPolicyData InquiryResponseObject
        {
            get
            {
                return (ExternalPolicyData)ViewState["oInquiryResponseObject"];
            }
            set
            {
                ViewState["oInquiryResponseObject"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //bram4 - ML changes
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("IntegralPolicyData.aspx"), "IntegralPolicyDataValidation", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "IntegralPolicyDataValidation", sValidationResources, true);

                // This page is accessed for both Integral Policy Inquiry and Download 
                if (!Page.IsPostBack)
                {
                    string sPolNumber = ""; string sPolModule = "";
                    string sPolSymbol = ""; string sPolSystem = "";

                    hdnEnquireOnly.Value = AppHelper.GetQueryStringValue("enquireOnly");
                    if (hdnEnquireOnly.Value == "True")
                        btnDownPolicy.Visible = false;
                    else
                        //bram4 - hiding this button even if it is not enquire only as it is not in current sprint scope - make it true later
                        btnDownPolicy.Visible = true;
                   
                    hdnClaimId.Value = AppHelper.GetQueryStringValue("claimid");
                        policyNumber.Text = AppHelper.GetQueryStringValue("policyNumber");                       
                        hdPolicySystemID.Value = AppHelper.GetQueryStringValue("policysystemid");
                        hdnPolicyLossDate.Value = AppHelper.GetQueryStringValue("policyLossDate");
                    hdnPolicyLobCode.Value = AppHelper.GetQueryStringValue("policyLobCode");
                    hdnPolicyLobId.Value = AppHelper.GetQueryStringValue("policyLobCode_ID");
                    hdnPolicyFlag.Value = AppHelper.GetQueryStringValue("DownloadPolicyWithoutClaim");
                    ProcessExternalPolicyData();
                }
            }
            catch (FaultException<RMException> ee)
            {
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                //ErrorControl1.errorDom = ee.Detail.Errors;
                ErrorControl1.DisplayError(ee.Detail.Errors);
                dvParent.Visible = true;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                dvParent.Visible = true;
            }
        }

        /// <summary>
        /// Inquire and Load all Policy data
        /// </summary>
        private void ProcessExternalPolicyData()
        {
            IntegralInterfaceBusinessHelper objHelper = null;
            PolicyEnquiry oEnquiryRequest = null;
            ExternalPolicyData oInquiryResponse = null;          
            XmlDocument serializedResponseDoc = new XmlDocument();

            string sIssueCd = string.Empty;
            bool bInSuccess = false;
            bool bIsError = false;
            int iTemp = 0;
            try
            {
                oEnquiryRequest = new PolicyEnquiry();
                oEnquiryRequest.PolicyNumber = policyNumber.Text;
                //send LOB for saving in policy table
                if (hdnPolicyLobId.Value != "")
                    oEnquiryRequest.LOB = hdnPolicyLobId.Value;
                //loss date is required for inquiry 
                if (hdnPolicyLossDate.Value != "")
                    oEnquiryRequest.PolLossDt = hdnPolicyLossDate.Value;
                iTemp = Conversion.CastToType<int>(hdPolicySystemID.Value, out bInSuccess);
                if (bInSuccess)
                    oEnquiryRequest.PolicySystemId = iTemp;

                objHelper = new IntegralInterfaceBusinessHelper();
                oInquiryResponse = objHelper.GetPolicyEnquiryResult(oEnquiryRequest);

                InquiryResponseObject = oInquiryResponse;                

                if (oInquiryResponse != null)
                {

                    // Awaiting Integral webservice to define schema for error details
                    bIsError = HasErrorOccured(oInquiryResponse.ResponseError);
                    if (!bIsError)
                    {
                        ViewState["InterestListRoles"] = oInquiryResponse.InterestListRoles;
                        LoadDetails(oInquiryResponse);
                    }
                }

            }
            catch (FaultException<RMException> ee)
            {
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                //ErrorControl1.errorDom = ee.Detail.Errors;
                ErrorControl1.DisplayError(ee.Detail.Errors);
                dvParent.Visible = false;
            }
            catch (Exception e)
            {
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(e, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                dvParent.Visible = false;
            }
            finally
            {
                oEnquiryRequest = null;
                oInquiryResponse = null;
                objHelper = null;
                serializedResponseDoc = null;
            }

        }

        private bool HasErrorOccured(string sErrorText)
        {
            bool bResult = false;

            if (!string.IsNullOrEmpty(sErrorText))
            {
                ErrorControl1.DisplayError(sErrorText);
                ErrorControl1.Visible = true;
                dvParent.Visible = false;
                bResult = true;
            }
            return bResult;
        }

        private void LoadDetails(ExternalPolicyData oInquiryResponse)
        {
            try
            {               

                // Map details of Policy and related objects (units, interest list, coverages, drivers, etc)
                PopulatePolicyDetails(oInquiryResponse);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
        /// <summary>
        /// populates the policy details 
        /// </summary>
        /// <param name="sResponse">represetns the response XMl which has the data.
        /// this XML is being traversed to extract the data and bind to UI controls</param>
        private void PopulatePolicyDetails(ExternalPolicyData oInquiryResponse)
        {
            

            try
            {
                XmlDocument objResponse = null;
                XmlNode xmlPolicy = null;
                XmlNode xmlUnits = null;
                XmlNode objNode = null;
                objResponse = new XmlDocument();


                //Map Policy Details from Policy Inquiry serialized xml               
                if (!string.IsNullOrEmpty(oInquiryResponse.PolicyData.Type))
                    lblPolicyTypeValue.Text = oInquiryResponse.PolicyData.Type;

                if (!string.IsNullOrEmpty(oInquiryResponse.PolicyData.PolicyNumber))
                    policyNumber.Text = oInquiryResponse.PolicyData.PolicyNumber;

                if (!string.IsNullOrEmpty(oInquiryResponse.PolicyData.StatusCode))
                    policyStatus.Text = oInquiryResponse.PolicyData.StatusCode;

                //sanoopsharma start
                //if (!string.IsNullOrEmpty(oInquiryResponse.PolicyData.ServiceBranch))
                //    policyBranch.Text = oInquiryResponse.PolicyData.ServiceBranch;
                //sanoopsharma end

                if (!string.IsNullOrEmpty(oInquiryResponse.PolicyData.EffectiveDate))
                    policyEffDate.Text = oInquiryResponse.PolicyData.EffectiveDate;

                if (!string.IsNullOrEmpty(oInquiryResponse.PolicyData.ExpiryDate))
                    policyExpDate.Text = oInquiryResponse.PolicyData.ExpiryDate;

                if (!string.IsNullOrEmpty(oInquiryResponse.PolicyData.RenewalType))
                    policyRenewalType.Text = oInquiryResponse.PolicyData.RenewalType;

                if (!string.IsNullOrEmpty(oInquiryResponse.PolicyData.RenewalNumber))
                    policyRenewalNo.Text = oInquiryResponse.PolicyData.RenewalNumber;

                //sanoopsharma start
                //if (!string.IsNullOrEmpty(oInquiryResponse.PolicyData.EndorsementNumber))
                //    policyEndorsementNo.Text = oInquiryResponse.PolicyData.EndorsementNumber;
                //sanoopsharma end

                if (!string.IsNullOrEmpty(oInquiryResponse.PolicyData.PaymentPlan))
                    policyPaymentPlan.Text = oInquiryResponse.PolicyData.PaymentPlan;

                //sanoopsharma start
                //if (!string.IsNullOrEmpty(oInquiryResponse.PolicyData.PaymentMode))
                //    policyPaymentMode.Text = oInquiryResponse.PolicyData.PaymentMode;
                //sanoopsharma end

                if (!string.IsNullOrEmpty(oInquiryResponse.PolicyData.NextBillDate))
                    policyNextBillDate.Text = oInquiryResponse.PolicyData.NextBillDate;

                if (!string.IsNullOrEmpty(oInquiryResponse.PolicyData.Currency))
                    policyCurrency.Text = oInquiryResponse.PolicyData.Currency;

                //if (!string.IsNullOrEmpty(oInquiryResponse.PolicyData.CurrencyRate))
                //    policyCurrencyRate.Text = oInquiryResponse.PolicyData.CurrencyRate;

                if (!string.IsNullOrEmpty(oInquiryResponse.PolicyData.MasterCompany))
                    lblMasterCmpnyValue.Text = oInquiryResponse.PolicyData.MasterCompany;

                if (!string.IsNullOrEmpty(oInquiryResponse.PolicyData.Country))
                    policyCountry.Text = oInquiryResponse.PolicyData.Country;

                if (!string.IsNullOrEmpty(oInquiryResponse.PolicyData.PrimiumData.TotalSumInsured))
                    policyTotalSumInsured.Text = oInquiryResponse.PolicyData.PrimiumData.TotalSumInsured;

                if (!string.IsNullOrEmpty(oInquiryResponse.PolicyData.PrimiumData.GrossPremium))
                    policyGrossPremium.Text = oInquiryResponse.PolicyData.PrimiumData.GrossPremium;

                if (!string.IsNullOrEmpty(oInquiryResponse.PolicyData.PrimiumData.NetPremium))
                    policyNetPremium.Text = oInquiryResponse.PolicyData.PrimiumData.NetPremium;

                // Ash: Ensure all policy detail fields are mapped.

                // Map policy interests list - including Insured, Insurer, Agent, and Drivers
                PopulateInterestList(oInquiryResponse.PolicyData.Entities);

                // Map policy Units list and related Unit objects (Unit interest list, coverages)               
                PopulateUnitList(oInquiryResponse.PolicyData.Units);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                XmlDocument objResponse = null;
                XmlNode xmlPolicy = null;
                XmlNode xmlUnits = null;
                XmlNode objNode = null;
                objResponse = new XmlDocument();
            }

        }
        /// <summary>
        /// populates the Policy's and Unit's Interest list - including Insured, Insurer, Agent, and Drivers
        /// </summary>
        private void PopulateInterestList(EntityData[] oEntityList, string sUnitNumber="")
        {
            string sPartyType = ""; string sPartyName = "";
            string sPolicyRole = ""; string sPolicyRoleCode = "";
            string sParentKey = "";
            XmlNode xmlEntities = null;
            DataTable dtInterestList = new DataTable();

            dtInterestList.Columns.Add("Parent_Key", typeof(string));
            dtInterestList.Columns.Add("Name", typeof(string));
            dtInterestList.Columns.Add("Number", typeof(string));
            dtInterestList.Columns.Add("TaxID", typeof(string));
          
            dtInterestList.Columns.Add("Birth_Date", typeof(string));
            dtInterestList.Columns.Add("Sex", typeof(string));
            dtInterestList.Columns.Add("Email_Address", typeof(string));
            dtInterestList.Columns.Add("PS_Role_Code", typeof(string));
            dtInterestList.Columns.Add("PS_Role_Desc", typeof(string));
            dtInterestList.Columns.Add("PS_Role_Text", typeof(string));

            try
            {
                // Check whether entity parent is Unit or Policy
                bool bPolicyParty = false;
                if (sUnitNumber == "")
                    bPolicyParty = true;

                if (bPolicyParty)                   
                    sParentKey = "";
                else    
                    sParentKey = sUnitNumber;

                foreach (EntityData xPartynode in oEntityList)
                {
                    DataRow drParty = dtInterestList.NewRow();

                    drParty["Parent_Key"] = sParentKey;
                    drParty["TaxID"] = xPartynode.TaxID;
                    drParty["Number"] = xPartynode.ClientNumber; //sanoopsharma field removed, not required for integral
                    sPartyType = xPartynode.TypeCode; // sanoopsharma field renamed for integral
                    if (sPartyType == "P")
                        drParty["Name"] = xPartynode.FirstName + " "                           
                            + xPartynode.LastName;
                    else
                        drParty["Name"] = xPartynode.BusinessName;
                   
                    drParty["Birth_Date"] = xPartynode.BirthDate;
                    drParty["Sex"] = xPartynode.Sex;
                    drParty["Email_Address"] = xPartynode.EmailAddress;
                    if (xPartynode.Role != null)
                    {
                        sPolicyRoleCode = xPartynode.RoleCode;
                        sPolicyRole = xPartynode.Role;

                        drParty["PS_Role_Code"] = sPolicyRoleCode;
                        drParty["PS_Role_Desc"] = sPolicyRole;
                        drParty["PS_Role_Text"] = sPolicyRole;
                    }
                    dtInterestList.Rows.Add(drParty);
                }


                if (bPolicyParty)
                {
                    // Map Non-driver parties to Policy Interest list grid
                    gvPolicyInterestList.DataSource = dtInterestList;
                    //gvPolicyInterestList.DataSource = dtInterestList.Select("PS_Role_Desc <> 'Driver'");
                    gvPolicyInterestList.DataBind();

                    // Map driver parties to Drivers grid
                    //gvDriverList.DataSource = dtInterestList.Select("PS_Role_Desc = 'Driver'");
                    //gvDriverList.DataBind();
                }
                else
                {
                    // Merge Unit interests list of multiple Units in view state
                    if (UnitInterestDataSource == null)
                        UnitInterestDataSource = dtInterestList;
                    else
                        UnitInterestDataSource.Merge(dtInterestList);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// populates the Unit list of selected Policy
        /// </summary>
        private void PopulateUnitList(UnitData[] oUnitList)
        {

            string sUnitType = string.Empty;
            string sPartyName = string.Empty;
            string sDesc = string.Empty;
            DataTable dtUnitDetails = new DataTable();

            dtUnitDetails.Columns.Add("Unit_Number", typeof(string));
            dtUnitDetails.Columns.Add("Unit_Type", typeof(string));
            dtUnitDetails.Columns.Add("Desc", typeof(string));
            dtUnitDetails.Columns.Add("Registration_No", typeof(string));
            dtUnitDetails.Columns.Add("Chasis_No", typeof(string));
            dtUnitDetails.Columns.Add("Engine_No", typeof(string));
            dtUnitDetails.Columns.Add("Sum_Insured", typeof(string));
            dtUnitDetails.Columns.Add("Region", typeof(string));
            dtUnitDetails.Columns.Add("Geo_Location", typeof(string));
            dtUnitDetails.Columns.Add("Effective_Date", typeof(string));
            dtUnitDetails.Columns.Add("Expiry_Date", typeof(string));

            try
            {
                foreach (UnitData oUnitData in oUnitList)
                {
                    DataRow drUnit = dtUnitDetails.NewRow();
                    drUnit["Unit_Number"] = oUnitData.UnitNumber;
                    drUnit["Unit_Type"] = oUnitData.Type;

                    sUnitType = oUnitData.UnitTypeInd;
                    if (CommonFunctions.GetUnitTypeIndicator(sUnitType) == Constants.UNIT_TYPE_INDICATOR.VEHICLE)
                    {
                        drUnit["Desc"] = oUnitData.VehicleMake + " "
                            + oUnitData.Model + " "
                            + oUnitData.Year;
                        drUnit["Geo_Location"] = oUnitData.GeoLocation;
                        drUnit["Region"] = oUnitData.Region;
                    }
                    else
                    {
                        sDesc = string.IsNullOrEmpty(oUnitData.OccupiedAs) ? oUnitData.RiskAccumulationLocality : oUnitData.RiskAccumulationLocality + "(" + oUnitData.OccupiedAs + ")";
                        //sanoopsharma - added for premisis node addition - start
                        if (!string.IsNullOrEmpty(oUnitData.Premises))
                            sDesc = oUnitData.Premises + " - " + sDesc;
                        else if (!string.IsNullOrEmpty(oUnitData.Situation))
                            sDesc = oUnitData.Situation + " - " + sDesc;

                        if (!string.IsNullOrEmpty(oUnitData.Business))
                            sDesc = oUnitData.Business + " - " + sDesc;
                        else if (!string.IsNullOrEmpty(oUnitData.RiskRating))
                            sDesc = oUnitData.RiskRating + " - " + sDesc;
                            drUnit["Desc"] = sDesc;
                        //sanoopsharma - end 
                        drUnit["Geo_Location"] = oUnitData.RiskAccumulationState;
                    }

                    drUnit["Registration_No"] = oUnitData.RegistrationNumber;
                    drUnit["Chasis_No"] = oUnitData.ChasisNumber;
                    drUnit["Engine_No"] = oUnitData.EngineNumber;
                    drUnit["Sum_Insured"] = oUnitData.PrimiumData.TotalSumInsured; ;
                    drUnit["Effective_Date"] = oUnitData.EffectiveDate;
                    drUnit["Region"] = oUnitData.Region;
                    drUnit["Expiry_Date"] = oUnitData.ExpiryDate;
                    dtUnitDetails.Rows.Add(drUnit);

                    // Map interest list for each Unit
                    PopulateInterestList(oUnitData.Entities, oUnitData.UnitNumber);

                    // Map Coverage list for each Unit
                    PopulateCoverages(oUnitData);
                }

                UnitDataSource = dtUnitDetails;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dtUnitDetails.Dispose();
                sUnitType = null;
                sPartyName =  null;
                sDesc = null;
            }

        }

        /// <summary>
        /// populates the Coverage list for each Unit
        /// </summary>
        private void PopulateCoverages(UnitData oUnitData)
        {
            string sUnitNumber = oUnitData.UnitNumber;
            string sUnitDesc = oUnitData.Type;
            //string sEffectiveDate = oUnitData.EffectiveDate; //sanoopsharma removed not required
            //string sExpiryDate = oUnitData.ExpiryDate; //sanoopsharma removed not required
            XmlNode xmlCoverages = null;
            DataTable dtCoverages = new DataTable();
            dtCoverages.Columns.Add("CoverageSeqNo", typeof(string));
            dtCoverages.Columns.Add("Unit_Number", typeof(string));
            dtCoverages.Columns.Add("UnitDesc", typeof(string));
            dtCoverages.Columns.Add("CovCode", typeof(string));
            dtCoverages.Columns.Add("CovDesc", typeof(string));
            dtCoverages.Columns.Add("Premium", typeof(string));
            dtCoverages.Columns.Add("Class", typeof(string));
            dtCoverages.Columns.Add("TotalSumInsured", typeof(string));    //sanoopsharma added premium total sum insured        
            //dtCoverages.Columns.Add("CovStatus", typeof(string));//sanoopsharma removed not required
            //dtCoverages.Columns.Add("EffectiveDate", typeof(string)); //sanoopsharma removed not required
            //dtCoverages.Columns.Add("ExpiryDate", typeof(string));

            try
            {
                sUnitNumber = oUnitData.UnitNumber;                

                foreach (CoverageData oCoverageData in oUnitData.Coverages)
                {
                    DataRow drCov = dtCoverages.NewRow();
                    drCov["Unit_Number"] = sUnitNumber;
                    drCov["UnitDesc"] = sUnitDesc;

                    drCov["CoverageSeqNo"] = oCoverageData.Number;
                    drCov["CovCode"] = oCoverageData.CoverageType;
                    drCov["CovDesc"] = oCoverageData.CoverageDesc;
                    drCov["Premium"] = oCoverageData.PrimiumData.GrossPremium; //sanoopsharma field renamed for integral
                    drCov["Class"] = oCoverageData.PrimiumData.PremiumClass;  //sanoopsharma field mapped 
                    drCov["TotalSumInsured"] = oCoverageData.PrimiumData.TotalSumInsured;  //sanoopsharma added premium total sum insured        
                    //drCov["CovStatus"] = "" ; //sanoopsharma removed not required
                    //drCov["EffectiveDate"] = sEffectiveDate;//sanoopsharma removed not required
                    //drCov["ExpiryDate"] = sExpiryDate;//sanoopsharma removed not required

                    dtCoverages.Rows.Add(drCov);
                }

                // Merge Unit interests list of multiple Units in view state
                if (CoverageDataSource == null)
                    CoverageDataSource = dtCoverages;
                else
                    CoverageDataSource.Merge(dtCoverages);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        
        protected void SaveClicked(object sender, EventArgs e)
        {
            IntegralInterfaceBusinessHelper objHelper = null;
            PolicySaveRequest objRes = null;
            ExternalPolicyData oSaveRequest = new ExternalPolicyData();
            bool blnSuccess = false;
            try
            {
                oSaveRequest = InquiryResponseObject;
                objHelper = new IntegralInterfaceBusinessHelper();
                oSaveRequest.ClaimID = string.IsNullOrWhiteSpace(hdnClaimId.Value) ? 0 : Convert.ToInt32(hdnClaimId.Value);
                oSaveRequest.PolicySystemID = Conversion.CastToType<int>(hdPolicySystemID.Value, out blnSuccess);
                objRes = objHelper.SavePolicyAllData(oSaveRequest);
                hdnPolSaveResponse.Value = Convert.ToString(objRes.Result);
                hdnPolicyID.Value = Convert.ToString(objRes.AddedPolicyId);
                hdnPolicyNumber.Value = objRes.PolicyName;
                hdPolicySystemID.Value = Convert.ToString(objRes.PolicySystemId);
                hdnPolicyLobCode.Value = AppHelper.GetQueryStringValue("policyLobCode");//vkumar258
                hdnPolicyFlag.Value = AppHelper.GetQueryStringValue("DownloadPolicyWithoutClaim");//vkumar258 
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>showPolicySuccess();</script>", false);
            }
            catch (FaultException<RMException> ee)
            {
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                ErrorControl1.DisplayError(ee.Detail.Errors);
                dvParent.Visible = true;
            }
            finally
            {
                objHelper = null;
                objRes = null;
                oSaveRequest = null;
            }
        }

        #region Grid Events

        protected void gvInterestList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string sRMRoleCode = "";
            GridViewRow rowInterest = e.Row;

            if (rowInterest.RowType == DataControlRowType.DataRow)
            {
                DropDownList ddSelectRole = (DropDownList)rowInterest.FindControl("ddSelectRole");
                TextBox txtRole = (TextBox)rowInterest.FindControl("txtRole");
                HtmlInputCheckBox chkSave = (HtmlInputCheckBox)rowInterest.FindControl("chkPolInterest");

                
                if (rowInterest.RowIndex < 3)
                {
                    ddSelectRole.Visible = false;
                    txtRole.Visible = true;
                    chkSave.Checked = true;
                    chkSave.Disabled = true;
                }                
               
                else
                {
                    // Load roles combo from viewstate                
                    DataSet dsRoles = new DataSet();
                    dsRoles.ReadXml(new StringReader(ViewState["InterestListRoles"].ToString()));

                    ddSelectRole.DataSource = dsRoles.Tables[0];
                    ddSelectRole.DataValueField = "option_text";
                    ddSelectRole.DataTextField = "value";
                    ddSelectRole.DataBind();
                    //if (rowInterest.Cells[8].Text == "Driver")
                    if (Constants.listDriver.Contains(rowInterest.Cells[8].Text.ToLower()))
                    {
                        ddSelectRole.SelectedIndex = 8;
                        ddSelectRole.Enabled = false;
                    }
                    ddSelectRole.Visible = true;
                    txtRole.Visible = false;
                }
            }

        }

        protected void gvUnitList_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            if (UnitDataSource != null && UnitDataSource.Rows.Count > 0)
                gvUnitList.DataSource = UnitDataSource;
        }

        //protected void gvUnitList_ItemDataBound(object sender, GridItemEventArgs e)
        //{
        //    if (e.Item is GridDataItem)
        //    {
                //GridDataItem item = (GridDataItem)e.Item;
                //HtmlInputCheckBox chkSel = (HtmlInputCheckBox)item.FindControl("chkSelectUnit");
                //chkSel.Attributes.Add("onclick", "ToggleUnitSelection(this,'" + item.ItemIndex + "')");
        //    }
        //}

        protected void gvUnitList_PreRender(object sender, EventArgs e)
        {
            //bool bSelected = false;
            //if (SelectedUnits != null && SelectedUnits.Count > 0)
            //{
            //    foreach (string sUnitNumber in SelectedUnits)
            //    {
            //        foreach (GridDataItem item in gvUnitList.MasterTableView.Items)
            //        {
            //            if (string.Equals(item["Unit_Number"].Text.ToString(), sUnitNumber))
            //            {
            //                //HtmlInputCheckBox chkSel = (HtmlInputCheckBox)item.FindControl("chkSelectUnit");
            //                CheckBox chkSel = (CheckBox)item["chkSelectUnit"].Controls[0];
            //                chkSel.Checked = true;
            //                bSelected = true;
            //            }
            //        }
            //    }

            // Show / Hide Next button on Unit grid list screen
            //if (bSelected)
            if (SelectedUnits != null && SelectedUnits.Count > 0)
                btnNextDiv2.Style.Add("display", "inline");
            else
                btnNextDiv2.Style.Add("display", "none");
        }

        protected void gvUnitInterestList_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {

            if (UnitInterestDataSource != null && UnitInterestDataSource.Rows.Count > 0)
            {

                gvUnitInterest.DataSource = UnitInterestDataSource;

                if (SelectedUnits != null && SelectedUnits.Count > 0)
                {// Apply filter if units are selected

                    string sFilterExpression = "";
                    foreach (string sUnitNumber in SelectedUnits)
                    {
                        if (sFilterExpression == "")
                            sFilterExpression = "[Parent_Key] ='" + sUnitNumber + "'";
                        else
                            sFilterExpression += " OR [Parent_Key] ='" + sUnitNumber + "'";
                    }


                    gvUnitInterest.MasterTableView.FilterExpression = "(" + sFilterExpression + ")";
                }
            }

        }

        protected void gvUnitInterest_ItemDataBound(object sender, GridItemEventArgs e)
        {
            DataRowView rowInterest = (DataRowView)e.Item.DataItem;
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                HtmlInputCheckBox chkSel = (HtmlInputCheckBox)item.FindControl("chkSelectInterest");
                chkSel.Attributes.Add("onclick", "ToggleUnitInterestSelection(this,'" + item.ItemIndex + "')");

                // Load roles combo from viewstate          
                DropDownList ddSelectRole = (DropDownList)item.FindControl("ddSelectRole");
                DataSet dsRoles = new DataSet();
                dsRoles.ReadXml(new StringReader(ViewState["InterestListRoles"].ToString()));

                ddSelectRole.DataSource = dsRoles.Tables[0];
                ddSelectRole.DataValueField = "option_text";
                ddSelectRole.DataTextField = "value";
                ddSelectRole.DataBind();
                if (Constants.listDriver.Contains(rowInterest.Row.ItemArray[9].ToString().ToLower()))
                {                    
                    ddSelectRole.SelectedIndex = 8;
                    ddSelectRole.Enabled = false;
                    chkSel.Checked = true;
                    chkSel.Disabled = true;
                }
            }
        }
        protected void gvCoverages_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            if (CoverageDataSource != null && CoverageDataSource.Rows.Count > 0)
            {
                gvCoverages.DataSource = CoverageDataSource;

                if (SelectedUnits != null && SelectedUnits.Count > 0)
                {// Apply filter if units are selected

                    string sFilterExpression = "";
                    foreach (string sUnitNumber in SelectedUnits)
                    {
                        if (sFilterExpression == "")
                            sFilterExpression = "[Unit_Number] ='" + sUnitNumber + "'";
                        else
                            sFilterExpression += " OR [Unit_Number] ='" + sUnitNumber + "'";
                    }

                    // test filter expression
                    gvCoverages.MasterTableView.FilterExpression = "(" + sFilterExpression + ")";
                }
            }
        }
        #endregion
        #region Ajax Manager Functions
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            try
            {
                string ajaxFunction = ""; string ajaxKey = "";
                string sPartyNumber = ""; string sPartyAddAs = "";
                string sUnitNumber = ""; string sFilterExpression = "";
                string sRole = "";

                string ajaxArgument = e.Argument;

                if (ajaxArgument.Split('|').Length > 1)
                {
                    ajaxFunction = ajaxArgument.Split('|').GetValue(0).ToString();
                    ajaxKey = ajaxArgument.Split('|').GetValue(1).ToString();
                }
                else
                    ajaxFunction = ajaxArgument;

                switch (ajaxFunction)
                {
                    case "ToggleSelectPolicyInterest":

                        foreach (GridViewRow row in gvPolicyInterestList.Rows)
                        {// Do not process for first 3 policy interests,Insured, Insurer, and Agent, which are always saved
                            if (row.RowIndex > 2)
                            {
                                HtmlInputCheckBox chkSelect = (HtmlInputCheckBox)row.FindControl("chkPolInterest");
                                DropDownList ddSelectRole = (DropDownList)row.FindControl("ddSelectRole");

                                if (chkSelect.Checked)
                                {
                                    sPartyNumber = row.Cells[2].Text.ToString();
                                    sPartyAddAs = ddSelectRole.SelectedValue;
                                    sRole = row.Cells[8].Text.ToString();
                                    UpdateResponseObject("SelectPolicyInterest", sPartyNumber, sRole, sPartyAddAs);
                                }
                                else
                                {
                                    sPartyNumber = row.Cells[2].Text.ToString();
                                    UpdateResponseObject("UnselectPolicyInterest", sPartyNumber, sRole);
                                }
                            }
                        }
                        break;

                    case "SelectUnit":
                        GridDataItem gdSelectedUnit = (GridDataItem)gvUnitList.Items[ajaxKey];
                        foreach(GridDataItem gd in gvUnitList.Items)
                        {
                            UpdateResponseObject("UnselectUnit", gd["Unit_Number"].Text, "");
                            SelectedUnits.Remove(gd["Unit_Number"].Text);
                        }
                        sUnitNumber = gdSelectedUnit["Unit_Number"].Text;
                        UpdateResponseObject("SelectUnit", sUnitNumber, "");

                        // Update Selected Units Collection
                        SelectedUnits.Add(sUnitNumber.ToString());

                        // Rebind grids to apply filter expression as per selected Units
                        gvUnitInterest.Rebind();
                        gvCoverages.Rebind();

                        if (gvUnitInterest.Items.Count == 0)
                            hdnIsUnitInterestListEmpty.Value = "True";
                        else
                            hdnIsUnitInterestListEmpty.Value = "False";

                        break;

                    case "UnselectUnit":
                        GridDataItem gdUnselectedUnit = (GridDataItem)gvUnitList.Items[ajaxKey];
                        sUnitNumber = (string)gdUnselectedUnit["Unit_Number"].Text;
                        UpdateResponseObject("UnselectUnit", sUnitNumber, "");

                        // Update Selected Units Collection
                        SelectedUnits.Remove(sUnitNumber.ToString());

                        // Rebind grids to apply filter expression as per selected Units
                        gvUnitInterest.Rebind();
                        gvCoverages.Rebind();

                        if (gvUnitInterest.Items.Count == 0)
                            hdnIsUnitInterestListEmpty.Value = "True";
                        else
                            hdnIsUnitInterestListEmpty.Value = "False";

                        break;
                    case "SelectUnitInterest":
                        GridDataItem gdSelectedInterest = (GridDataItem)gvUnitInterest.Items[ajaxKey];
                        sUnitNumber = (string)gdSelectedInterest["Parent_Key"].Text;
                        sPartyNumber = (string)gdSelectedInterest["Number"].Text;

                        DropDownList ddSelRole = (DropDownList)gdSelectedInterest.FindControl("ddSelectRole");
                        sPartyAddAs = ddSelRole.SelectedValue;
                        UpdateResponseObject("SelectUnitInterest", sUnitNumber + "|" + sPartyNumber, "", sPartyAddAs);

                        break;
                    case "UnselectUnitInterest":
                        GridDataItem gdUnSelectedInterest = (GridDataItem)gvUnitInterest.Items[ajaxKey];
                        sUnitNumber = (string)gdUnSelectedInterest["Parent_Key"].Text;
                        sPartyNumber = (string)gdUnSelectedInterest["Number"].Text;
                        UpdateResponseObject("UnselectUnitInterest", sUnitNumber + "|" + sPartyNumber, "");
                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void UpdateResponseObject(string action, string matchkey, string sRole)
        {
            UpdateResponseObject(action, matchkey, sRole, "");
        }
        protected void UpdateResponseObject(string action, string matchkey, string sRole, string sAdditionalData)
        {
            XmlDocument objResponseXML = new XmlDocument();
            StringBuilder sb = new StringBuilder();
            XmlWriterSettings settings = new XmlWriterSettings();
            XmlWriter writer = null;
            
            string sUnitNumber = ""; string sPartyNumber = "";
            
            switch (action)
            {
                    
                case "SelectPolicyInterest":
                    
                    //find the matching entity object by looping through the entity list                    
                    foreach (EntityData oEntityData in InquiryResponseObject.PolicyData.Entities)
                    {
                        if (oEntityData.ClientNumber == matchkey && oEntityData.Role.Trim() == sRole.Trim()) //sanoopsharma field renamed for integral
                        {
                            oEntityData.Selected = "True";
                            if (sAdditionalData != "")
                            {
                                oEntityData.TableID = sAdditionalData;
                            }
                            break;
                        }                            
                    }                    

                   //if (objResponseXML.SelectSingleNode("//PolicyData/Entities/EntityData[Number=" + matchkey + "]") != null)
                   // {
                   //    // xParty = objResponseXML.SelectSingleNode("//Policy/Party[Number=" + matchkey + "]");
                   //     xParty = objResponseXML.SelectSingleNode("//PolicyData/Entities/EntityData[Number=" + matchkey + "]");
                   //     xKey.Value = "True";
                   //     xParty.SelectSingleNode("./Selected").InnerText = xKey.Value;

                   //     // Add AddAs xml node to Party node.
                   //     // Add RMA role code and description to AddAs xml node
                   //     if (sAdditionalData != "")
                   //     {
                   //         if (xParty.SelectSingleNode("./AddAs") != null)
                   //             xmlAddAs = xParty.SelectSingleNode("./AddAs");
                   //         else
                   //             xParty.PrependChild(xmlAddAs);

                   //         xTableID.Value = sAdditionalData;
                   //         xmlAddAs.InnerText = xTableID.Value;
                   //     }
                   // }
                    
                    break;
                case "UnselectPolicyInterest":
                    //update xpath as above
                    //if (objResponseXML.SelectSingleNode("//PolicyData/Entities/EntityData[Number=" + matchkey + "]") != null)
                    //{
                    //    //update xpath as above
                    //    xParty = objResponseXML.SelectSingleNode("//PolicyData/Entities/EntityData[Number=" + matchkey + "]");
                    //    xKey.Value = "False";
                    //    xParty.SelectSingleNode("./Selected").InnerText = xKey.Value;
                    //}
                    
                    foreach (EntityData oEntityData in InquiryResponseObject.PolicyData.Entities)
                    {
                        if (oEntityData.ClientNumber == matchkey && oEntityData.Role.Trim() == sRole.Trim()) //sanoopsharma field renamed for integral
                        {
                            oEntityData.Selected = "False";
                            break;
                        }                            
                    }
                    break;

                case "SelectUnit":
                    //update xpath as above
                    foreach (UnitData oUnitData in InquiryResponseObject.PolicyData.Units)
                    {
                        if (oUnitData.UnitNumber == matchkey)
                        {
                            oUnitData.Selected = "True";
                            break;
                        }
                    }
                    //if (objResponseXML.SelectSingleNode("//PolicyData/Units/UnitData[UnitNumber=" + matchkey + "]") != null)
                    //{
                    //    //update xpath as above
                    //    xUnit = objResponseXML.SelectSingleNode("//PolicyData/Units/UnitData[UnitNumber=" + matchkey + "]");
                    //    xKey.Value = "True";
                    //    xUnit.SelectSingleNode("./Selected").InnerText = xKey.Value;
                    //}
                    break;

                case "UnselectUnit":
                    //update xpath as above
                    foreach (UnitData oUnitData in InquiryResponseObject.PolicyData.Units)
                    {
                        if (oUnitData.UnitNumber == matchkey)
                        {
                            oUnitData.Selected = "False";
                            break;
                        }
                    }
                    //if (objResponseXML.SelectSingleNode("//PolicyData/Units/UnitData[UnitNumber=" + matchkey + "]") != null)
                    //{
                    //    //update xpath as above
                    //    xUnit = objResponseXML.SelectSingleNode("//PolicyData/Units/UnitData[UnitNumber=" + matchkey + "]");
                    //    xKey.Value = "False";
                    //    xUnit.SelectSingleNode("./Selected").InnerText = xKey.Value;
                    //}
                    break;
                case "SelectUnitInterest":

                    // vkumar258: JIRA 2309
                    if (matchkey != null)
                    {
                        sUnitNumber = matchkey.Split('|').GetValue(0).ToString();
                        sPartyNumber = matchkey.Split('|').GetValue(1).ToString();
                    }
                        //update xpath as above

                    foreach (UnitData oUnitData in InquiryResponseObject.PolicyData.Units)
                    {
                        if (oUnitData.UnitNumber == sUnitNumber)
                        {
                            foreach (EntityData oEntityData in oUnitData.Entities)
                            {
                                if (oEntityData.ClientNumber == sPartyNumber) //sanoopsharma field renamed for integral
                                {
                                    oEntityData.Selected = "True";
                                    if (sAdditionalData != "")
                                    {
                                        oEntityData.TableID = sAdditionalData;
                                    }
                                }
                                break;
                            }
                        }                        
                    }
                    //if (objResponseXML.SelectSingleNode("//PolicyData/Units/UnitData[UnitNumber=" + sUnitNumber + "]/Entities/EntityData[Number=" + sPartyNumber + "]") != null)
                    //{
                    //    //update xpath as above
                    //    xParty = objResponseXML.SelectSingleNode("//PolicyData/Units/UnitData[UnitNumber=" + sUnitNumber + "]/Entities/EntityData[Number=" + sPartyNumber + "]");
                    //    xKey.Value = "True";
                    //    xParty.SelectSingleNode("./Selected").InnerText = xKey.Value;
                    //}

                    //if (sAdditionalData != "")
                    //{
                    //    if (xParty.SelectSingleNode("./AddAs") != null)
                    //        xmlAddAs = xParty.SelectSingleNode("./AddAs");
                    //    else
                    //        xParty.PrependChild(xmlAddAs);

                    //    xTableID.Value = sAdditionalData;
                    //    xmlAddAs.InnerText = xTableID.Value;
                    //}
                    break;

                case "UnselectUnitInterest":

                    // vkumar258: JIRA 2309
                    if (matchkey != null)
                    {
                        sUnitNumber = matchkey.Split('|').GetValue(0).ToString();
                        sPartyNumber = matchkey.Split('|').GetValue(1).ToString();
                    }
                    foreach (UnitData oUnitData in InquiryResponseObject.PolicyData.Units)
                    {
                        if (oUnitData.UnitNumber == sUnitNumber)
                        {
                            foreach (EntityData oEntityData in oUnitData.Entities)
                            {
                                if (oEntityData.ClientNumber == sPartyNumber) //sanoopsharma field renamed for integral
                                {
                                    oEntityData.Selected = "False";                                    
                                }
                                break;
                            }
                        }
                    }

                    //update xpath as above
                    //if (objResponseXML.SelectSingleNode("//PolicyData/Units/UnitData[UnitNumber=" + sUnitNumber + "]/Entities/EntityData[Number=" + sPartyNumber + "]") != null)
                    //{
                    //    //update xpath as above
                    //    xParty = objResponseXML.SelectSingleNode("//PolicyData/Units/UnitData[UnitNumber=" + sUnitNumber + "]/Entities/EntityData[Number=" + sPartyNumber + "]");
                    //    xKey.Value = "False";
                    //    xParty.SelectSingleNode("./Selected").InnerText = xKey.Value;
                    //}
                    break;
            }

        }
        #endregion

    }
}
