﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.AppHelpers;
using System.Xml;
using Riskmaster.UI.Shared.Controls;
using Riskmaster.Common;
using Riskmaster.UI.PolicyInterfaceService;
using System.ServiceModel;
using System.Xml.XPath;//sas

namespace Riskmaster.UI.PolicyInterface
{
    public partial class SetUpPolicySystem : NonFDMBasePageCWS
    {
        private XmlDocument oFDMPageDom = null;
        XElement XmlTemplate = null;
        string sCWSresponse = "";
        XmlDocument XmlDoc = new XmlDocument();
        string[] PolSystemType;  //bram4: MITS 35932
        private const string pageID = "262";
        Constants.POLICY_SYSTEM_TYPE? PolicySystemName = null; //Payal: RMA-6233

        protected void Page_Load(object sender, EventArgs e)
        {
            string PolicySysType;//bram4: MITS 35932
            try
            {
                if (!IsPostBack)
                {
                    btnAddDataDSN.OnClientClick = string.Format("return AddDSNDetails('{0}','{1}','{2}');", AppHelper.GetQueryStringValue("selectedid"), AppHelper.GetQueryStringValue("mode"), "DataDSN");
                    btnAddClientDSN.OnClientClick = string.Format("return AddDSNDetails('{0}','{1}','{2}');", AppHelper.GetQueryStringValue("selectedid"), AppHelper.GetQueryStringValue("mode"), "ClientDSN");
                    //Anu Tennyson Staging Implementation Start
                    btnStagingConn.OnClientClick = string.Format("return AddDSNDetails('{0}','{1}','{2}');", AppHelper.GetQueryStringValue("selectedid"), AppHelper.GetQueryStringValue("mode"), "StagingConn");
                    txtStagingConn.Attributes.Add("readOnly", "true");
                    //Anu Tennyson Staging Implementation Ends
                    txtDataDSNConnStr.Attributes.Add("readOnly","true");
                    txtClientDSNConnStr.Attributes.Add("readOnly","true");
                    //Domain.Attributes.Add("onchange", "javascript:checkNumLostFocus(this);");
                    Session["RowCount"] = -1;
                    gridname.Text = AppHelper.GetQueryStringValue("gridname");
                    mode.Text = AppHelper.GetQueryStringValue("mode");
                    selectedrowposition.Text = AppHelper.GetQueryStringValue("selectedrowposition");
                    lblRealTime.Text = AppHelper.GetResourceValue(pageID, "lblRealTime", "0"); //Payal: RMA-6233
                    lblBatch.Text = AppHelper.GetResourceValue(pageID, "lblBatch", "0");  //Payal: RMA-6233
                    if (mode.Text.ToLower() != "add")
                    {
                        XmlTemplate = GetMessageTemplate();
                        CallCWS("SetUpPolicySystemAdaptor.GetById", XmlTemplate, out sCWSresponse, false, false);
                        BindControls();
                        //bram4: MITS 35932 - Change the UI based on policy system type  
                        PolSystemType = (PolicySystemType.CodeTextValue).Trim().Split(' ');
                        // Payal: RMA-6233 Starts
                        PolicySystemName = CommonFunctions.GetPolicySystemTypeIndicator(PolSystemType[0].Trim());
                        //  if (PolSystemType[0].Trim() == "INTEGRAL")  
                        if (PolicySystemName == Common.Constants.POLICY_SYSTEM_TYPE.INTEGRAL)
                        // Payal: RMA-6233 Ends
                        {
                            Version.Attributes.Remove("onchange");
                            tblPoint.Visible = false;
                            //sanoopsharma - MITS 35932 start                            
                            trCASCheck.Visible = true;
                            CASFields.Visible = true;
                            DefaultPointfields.Visible = false;
                            btnUploadConfig.Visible = false;
                            btnChangePwd.Visible = false;
                            tblChk.Visible = true;
                            tblStgConnection.Visible = false;
                            //lblCFWUserName.Text = "CAS User Name";
                            lblCFWUserName.Text = AppHelper.GetResourceValue(pageID, "lblCASUsrNam", "0");
                            //lblCFWUserPassword.Text = "CAS User Password";
                            lblCFWUserPassword.Text = AppHelper.GetResourceValue(pageID, "lblCASUsrPwd", "0");
                            //sanoopsharma - MITS 35932 end
                            lblRMAUser.CssClass = lblRMAUser.CssClass.Replace("required", "");
                            lblCFWUserName.CssClass = lblCFWUserName.CssClass.Replace("required", "");
                            lblRMAUser.Text = AppHelper.GetResourceValue(pageID, "lblCFWUsrCAS", "0");
                            lblUploadServiceUrl.Text = AppHelper.GetResourceValue(pageID, "lblUploadServiceUrl", "0");  //Payal: RMA-6233
                        }
                        else if (PolicySystemName == Common.Constants.POLICY_SYSTEM_TYPE.POINT)
                        {     //dbisht : 35926
                            Version.Attributes.Add("onchange", "javascript:PIJSetting();");
                            //End : 35926            
                            tblPoint.Visible = true;
                            lblRMAUser.CssClass = "required";
                            lblCFWUserName.CssClass = "required";
                            tblStgConnection.Visible = false;
                            //sanoopsharma - MITS 35932 start                            
                            trCASCheck.Visible = false;
                            CASFields.Visible = false;
                            DefaultPointfields.Visible = true;
                            //lblCFWUserName.Text = "CFW User Name";
                            lblCFWUserName.Text = AppHelper.GetResourceValue(pageID, "lblCFWUsrNam", "0");
                            //lblCFWUserPassword.Text = "CFW User Password";
                            lblCFWUserPassword.Text = AppHelper.GetResourceValue(pageID, "lblCFWUsrPwd", "0");
                            //lblRMAUser.Text = "Use RMA User for CFW";
                            lblRMAUser.Text = AppHelper.GetResourceValue(pageID, "lblCFWUsrCFW", "0");
                            //sanoopsharma - MITS 35932 end
                        }
					//Start RMA-10039
                        else if (PolicySystemName == Common.Constants.POLICY_SYSTEM_TYPE.STAGING)
                        {                            
                            tblChk.Visible = true;
                            updatePolicyEntities.Visible = false;
                            realTime.Visible = false;
                            financialUpload.Visible = false;

                            Version.Attributes.Remove("onchange");
                            tblPoint.Visible = false;
                            lblRMAUser.CssClass = lblRMAUser.CssClass.Replace("required", "");
                            lblCFWUserName.CssClass = lblCFWUserName.CssClass.Replace("required", "");
                            cbUseRMAForCFW.Visible = false;
                            lblRMAUser.Visible = false;
                            tblPoint.Visible = false;
                            trCASCheck.Visible = false;
                            CASFields.Visible = false;
                            DefaultPointfields.Visible = false;
                            lblCFWUserName.Text = "CAS User Name";
                            lblCFWUserPassword.Text = "CAS User Password";
                            lblRMAUser.Text = "Use RMA User for CAS";
                            btnUploadConfig.Visible = false;
                            lblUseCAS.Visible = false;
                            UseCAS.Visible = false;
                            lblPointURL.Visible = false;
                            txtPointURL.Visible = false;
                            lblVersion.Visible = false;
                            Version.Visible = false;
                            lblCFWUserName.Visible = false;
                            lblCFWUserPassword.Visible = false;
                            txtCFWUserNm.Visible = false;
                            txtCFWUserPassword.Visible = false;

                        } //end RMA-10039
			else
                        {
                            Version.Attributes.Remove("onchange");
                            tblPoint.Visible = false;
                            lblRMAUser.CssClass = lblRMAUser.CssClass.Replace("required", "");
                            lblCFWUserName.CssClass = lblCFWUserName.CssClass.Replace("required", "");
                            //sanoopsharma - MITS 35932 start  
                            cbUseRMAForCFW.Visible = false;
                            lblRMAUser.Visible = false;
                            tblPoint.Visible = false;
                            trCASCheck.Visible = false;
                            CASFields.Visible = false;
                            DefaultPointfields.Visible = false;
                            lblCFWUserName.Text = "CAS User Name";
                            lblCFWUserPassword.Text = "CAS User Password";
                            lblRMAUser.Text = "Use RMA User for CAS";
                            btnUploadConfig.Visible = false;
                            lblUseCAS.Visible = false;
                            UseCAS.Visible = false;
                            lblPointURL.Visible = false;
                            txtPointURL.Visible = false;
                            lblVersion.Visible = false;
                            Version.Visible = false;
                            lblCFWUserName.Visible = false;
                            lblCFWUserPassword.Visible = false;
                            txtCFWUserNm.Visible = false;
                            txtCFWUserPassword.Visible = false;
                            tblChk.Visible = false;
                        }
                        
                        //bram4: MITS 35932 - END
                        //dbisht6 mits 35926
                        if (Version.SelectedValue == "PIJ")
                        {
                           // rbOff.Enabled = false;
                            rbOff.Disabled = true;
                        }
                        else
                        {
                            //rbOff.Enabled = true;
                            rbOff.Disabled = false;
                        }
                        //dbisht6 end
                    }
                    if (mode.Text.ToLower() == "add")
                    {
                        Version.Attributes.Add("onchange", "javascript:PIJSetting();");
                        btnChangePwd.Visible = false;
                        tblPoint.Visible = true;
                        lblRMAUser.CssClass = "required";
                        lblCFWUserName.CssClass = "required";
                        //sanoopsharma - MITS 35932 start                        
                        trCASCheck.Visible = false;
                        CASFields.Visible = false;
                        DefaultPointfields.Visible = true;
                        //lblCFWUserName.Text = "CFW User Name";
                        lblCFWUserName.Text = AppHelper.GetResourceValue(pageID, "lblCFWUsrNam", "0");
                        //lblCFWUserPassword.Text = "CFW User Password";
                        lblCFWUserPassword.Text = AppHelper.GetResourceValue(pageID, "lblCFWUsrPwd", "0");
                        //lblRMAUser.Text = "Use RMA User for CFW";
                        lblRMAUser.Text = AppHelper.GetResourceValue(pageID, "lblCFWUsrCFW", "0");
                        //sanoopsharma - MITS 35932 end
                        //Anu Tennyson Staging Implementation Starts
                       // lblStagingConnection.Text = AppHelper.GetResourceValue(pageID, "lblStagingConn", "0");
                        //Anu Tennyson Ends
                    }
                }

                else
                {
                    TextBox txtPostBackParent = (TextBox)this.FindControl("txtPostBack");
                    txtPostBackParent.Text = "Done";

                    Control c = DatabindingHelper.GetPostBackControl(this.Page);
                    mode.Text = AppHelper.GetQueryStringValue("mode");
                    
                    XmlTemplate = GetMessageTemplate();
                    PolSystemType = (PolicySystemType.CodeTextValue).Trim().Split(' ');
                    // Payal:RMA-6233 Starts
                    PolicySystemName = CommonFunctions.GetPolicySystemTypeIndicator(PolSystemType[0].Trim());
                    //  if (PolSystemType[0].Trim() == "INTEGRAL") 
                    if (PolicySystemName == Common.Constants.POLICY_SYSTEM_TYPE.INTEGRAL)
                    // Payal:RMA-6233 Ends
                    {
                    if (c == null)
                    {
                        XmlTemplate = GetMessageTemplate();
                        if (FunctionToCall.Text != "" ) 
                        {
                            if (FunctionToCall.Text == "SetUpPolicySystemAdaptor.GetVersionInfo")
                            {
                                XmlTemplate = GetVersionTemplate();
                            }
                            CallCWS(FunctionToCall.Text, XmlTemplate, out sCWSresponse, false, false);
                        }
                        else
                        {
                            CallCWS("SetUpPolicySystemAdaptor.GetById", XmlTemplate, out sCWSresponse, false, false);
                        }
                        BindControls();
                    }
                    Version.Attributes.Remove("onchange");
                    tblPoint.Visible = false;
                    tblStgConnection.Visible = false;
                    lblRMAUser.CssClass = lblRMAUser.CssClass.Replace("required", "");
                    lblCFWUserName.CssClass = lblCFWUserName.CssClass.Replace("required", "");
                    tblChk.Visible = true;
                    //sanoopsharma - MITS 35932 start                        
                    trCASCheck.Visible = true;
                    CASFields.Visible = true;
                    DefaultPointfields.Visible = false;
                    lblCFWUserName.Text = "CAS User Name";
                    lblCFWUserPassword.Text = "CAS User Password";
                    lblRMAUser.Text = "Use RMA User for CAS";
                    btnUploadConfig.Visible = false;
                        ////sanoopsharma - MITS 35932 end
                    //Version.Attributes.Remove("onchange");
                    //tblPoint.Visible = false;
                    //tblChk.Visible = true;
                    ////sanoopsharma - MITS 35932 start                            
                    //trCASCheck.Visible = true;
                    //CASFields.Visible = true;
                    //DefaultPointfields.Visible = false;
                    //btnUploadConfig.Visible = false;
                    //btnChangePwd.Visible = false;
                    //tblChk.Visible = true;
                    //tblStgConnection.Visible = false;
                    ////lblCFWUserName.Text = "CAS User Name";
                    //lblCFWUserName.Text = AppHelper.GetResourceValue(pageID, "lblCASUsrNam", "0");
                    ////lblCFWUserPassword.Text = "CAS User Password";
                    //lblCFWUserPassword.Text = AppHelper.GetResourceValue(pageID, "lblCASUsrPwd", "0");
                    ////sanoopsharma - MITS 35932 end
                    //lblRMAUser.CssClass = lblRMAUser.CssClass.Replace("required", "");
                    //lblCFWUserName.CssClass = lblCFWUserName.CssClass.Replace("required", "");
                    //lblRMAUser.Text = AppHelper.GetResourceValue(pageID, "lblCFWUsrCAS", "0");
                    //lblUploadServiceUrl.Text = AppHelper.GetResourceValue(pageID, "lblUploadServiceUrl", "0");
                    }

                    else if (PolicySystemName == Common.Constants.POLICY_SYSTEM_TYPE.POINT)
                    {

                        tblPoint.Visible = true;
                        lblRMAUser.CssClass = "required";
                        lblCFWUserName.CssClass = "required";
                        //sanoopsharma - MITS 35932 start                        
                        trCASCheck.Visible = false;
                        CASFields.Visible = false;
                        DefaultPointfields.Visible = true;
                        tblStgConnection.Visible = false;
                        tblChk.Visible = true;
                        //lblCFWUserName.Text = "CFW User Name";
                        lblCFWUserName.Text = AppHelper.GetResourceValue(pageID, "lblCFWUsrNam", "0");
                        //lblCFWUserPassword.Text = "CFW User Password";
                        lblCFWUserPassword.Text = AppHelper.GetResourceValue(pageID, "lblCFWUsrPwd", "0");
                        //lblRMAUser.Text = "Use RMA User for CFW";
                        lblRMAUser.Text = AppHelper.GetResourceValue(pageID, "lblCFWUsrCFW", "0");
                        //sanoopsharma - MITS 35932 end
                        if (c == null)
                        {
                            //bram4: MITS 35932 - END                            
                            if (FunctionToCall.Text != "")
                            {
                                if (FunctionToCall.Text == "SetUpPolicySystemAdaptor.GetVersionInfo")
                                {
                                    XmlTemplate = GetVersionTemplate();
                                }
                                CallCWS(FunctionToCall.Text, XmlTemplate, out sCWSresponse, false, false);
                            }
                            else
                            {
                                CallCWS("SetUpPolicySystemAdaptor.GetById", XmlTemplate, out sCWSresponse, false, false);
                            }
                            BindControls();
                        }
                    }

                    else
                    {
                        Version.Attributes.Remove("onchange");
                        tblStgConnection.Visible = true;
                        lblRMAUser.CssClass = lblRMAUser.CssClass.Replace("required", "");
                        lblCFWUserName.CssClass = lblCFWUserName.CssClass.Replace("required", "");
                        //sanoopsharma - MITS 35932 start  
                        btnStagingConn.OnClientClick = string.Format("return AddDSNDetails('{0}','{1}','{2}');", AppHelper.GetQueryStringValue("selectedid"), AppHelper.GetQueryStringValue("mode"), "StagingConn");
                        //lblStagingConnection.Text = AppHelper.GetResourceValue(pageID, "lblStagingConn", "0");
                        cbUseRMAForCFW.Visible = false;
                        lblRMAUser.Visible = false; 
                        tblPoint.Visible = false; 
                        trCASCheck.Visible = false;
                        CASFields.Visible = false;
                        DefaultPointfields.Visible = false;
                        lblCFWUserName.Text = "CAS User Name";
                        lblCFWUserPassword.Text = "CAS User Password";
                        lblRMAUser.Text = "Use RMA User for CAS";
                        btnUploadConfig.Visible = false;
                        lblUseCAS.Visible = false;
                        UseCAS.Visible = false;
                        lblPointURL.Visible = false;
                        txtPointURL.Visible = false;
                        lblVersion.Visible = false;
                        Version.Visible = false;
                        lblCFWUserName.Visible = false;
                        lblCFWUserPassword.Visible = false;
                        txtCFWUserNm.Visible = false;
                        txtCFWUserPassword.Visible = false;

                        //Start RMA-10039
                        tblChk.Visible = true;
                        updatePolicyEntities.Visible = false;
                        realTime.Visible = false;
                        financialUpload.Visible = false;
                        //End RMA-10039
                                          
                    }
                    //Start bram4: MITS 35932 - To set the value of policy system type in hidden field               
                }
                PolSystemType = (PolicySystemType.CodeTextValue).Trim().Split(' ');
                // Payal:RMA-6233 Starts
                // hdnPolicySystemType.Value = PolSystemType[0]; 
                PolicySystemName = CommonFunctions.GetPolicySystemTypeIndicator(PolSystemType[0].Trim());
                hdnPolicySystemType.Value = PolicySystemName.ToString();
                // Payal:RMA-6233 Ends
                //End bram4: MITS 35932 - To set the value of policy system type in hidden field               
            }
            catch (FaultException<RMException> ee)
            {
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XElement GetMessageTemplate()
        {
            string[] AddConfig = null;//smahajan22 mits 36315
            string sPolSysTypeCode = "0";
            if (mode.Text.ToLower() == "edit")
            {
                string strRowId = AppHelper.GetQueryStringValue("selectedid");
                RowId.Text = strRowId.Substring(strRowId.LastIndexOf(',') + 1);
            }
            if (PolicySystemType.CodeIdValue != string.Empty)
            {
                sPolSysTypeCode = PolicySystemType.CodeIdValue;
            }
            //smahajan22 mits 36315 start 
            if (txtAddConfig.Text != string.Empty)
            {
                 AddConfig = txtAddConfig.Text.Split('|');

            }
            //smahajan22 mits 36315 end 
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("SetUpPolicySystemAdaptor.GetById");
            sXml = sXml.Append("</Function></Call><Document><PolicyInterface><FunctionToCall/><PolicySys>");
            sXml = sXml.Append("<PolicySysName>" + PolicySysName.Text + "</PolicySysName>");
            sXml = sXml.Append("<MappingTablePrefix>" + MappingTablePrefix.Text + "</MappingTablePrefix>");// aaggarwal29: Code mapping change
            sXml = sXml.Append("<Domain>" + Domain.Text + "</Domain>");
            sXml = sXml.Append("<License>" + License.Text + "</License>");
            sXml = sXml.Append("<Proxy>" + Proxy.Text + "</Proxy>");
            sXml = sXml.Append("<UserName>" + UserName.Text + "</UserName>");
            sXml = sXml.Append("<Password>" + Password.Text + "</Password>");
            sXml = sXml.Append("<URLParam>" + URLParam.Text + "</URLParam>");
            sXml = sXml.Append("<FinancialUpdate>" + FinancialUpdate.Checked.ToString() + "</FinancialUpdate>");
            sXml = sXml.Append("<DefaultPolicy>" + DefaultPolicy.Checked.ToString() + "</DefaultPolicy>");
            sXml = sXml.Append("<policysystemid>" + RowId.Text + "</policysystemid>");
            sXml = sXml.Append("<PolicySystemType codeid='" + sPolSysTypeCode + "' >" + PolicySystemType.CodeText + "</PolicySystemType>");
            sXml = sXml.Append("<TargetDatabase>" + TargetDatabase.Text + "</TargetDatabase>");
            if (rbOn.Checked)
            {
                sXml = sXml.Append("<ClientFileFlag> 1 </ClientFileFlag>");
            }
            else 
            {
                sXml = sXml.Append("<ClientFileFlag> 0 </ClientFileFlag>");
            }
            sXml = sXml.Append("<Version>"+ Version.SelectedValue.Trim()+ "</Version>");
            sXml = sXml.Append("<Versions Type='" + PolicySystemType.CodeText + "'></Versions>");
            sXml = sXml.Append("<UseRMAUserForCFW>" + cbUseRMAForCFW .Checked.ToString()+ "</UseRMAUserForCFW>");
            sXml = sXml.Append("<CFWUserName>" + txtCFWUserNm .Text+ "</CFWUserName>");
            sXml = sXml.Append("<CFWUserPassword>" + txtCFWUserPassword.Text + "</CFWUserPassword>");
            sXml = sXml.Append("<PointURL>" + txtPointURL.Text + "</PointURL>");
            sXml = sXml.Append("<LocCompny>" + txtLocCmpny.Text + "</LocCompny>");
            sXml = sXml.Append("<MasterCompany>" + txtMasterCompany.Text + "</MasterCompany>");
            sXml = sXml.Append("<BusinessEntities>" + txtBusinessEntities.Text + "</BusinessEntities>");
            sXml = sXml.Append("<MajorPerils>" + txtMajorPerils.Text + "</MajorPerils>");
            sXml = sXml.Append("<IntertestList>" + txtIntertestList.Text + "</IntertestList>");
          //  sXml = sXml.Append("<ClVehicleUnitMapping>" + txtClVehicleUnitMapping.Text + "</ClVehicleUnitMapping>");
          //  sXml = sXml.Append("<ClPropertyUnitMapping>" + txtClPropertyUnitMapping.Text + "</ClPropertyUnitMapping>");
            sXml = sXml.Append("<DataDSNConnStr>" + txtDataDSNConnStr.Text + "</DataDSNConnStr>");
            sXml = sXml.Append("<ClientDSNConnStr>" + txtClientDSNConnStr.Text + "</ClientDSNConnStr>");
            //Anu Tennyson Staging Implementation Starts
            sXml = sXml.Append("<StagingConn>" + txtStagingConn.Text + "</StagingConn>");
            //Anu Tennyson Staging Implementation Ends
            sXml = sXml.Append("<DataDriverNm></DataDriverNm>");
            sXml = sXml.Append("<DataServerNm></DataServerNm>");
            sXml = sXml.Append("<DataLibraryList></DataLibraryList>");
            sXml = sXml.Append("<DataDSN></DataDSN>");
            sXml = sXml.Append("<DataDSNUser></DataDSNUser>");
            sXml = sXml.Append("<DataDSNPwd></DataDSNPwd>");
            //sanoopsharma - MITS 35932 start
            sXml = sXml.Append("<WSSearchURL>" + WSSearchURL.Text + "</WSSearchURL>");
            sXml = sXml.Append("<WSInquiryURL>" + WSInquiryURL.Text + "</WSInquiryURL>");
            sXml = sXml.Append("<CASServerURL>" + CASServerURL.Text + "</CASServerURL>");
            sXml = sXml.Append("<CASServiceURL>" + CASServiceURL.Text + "</CASServiceURL>");
            //sanoopsharma - MITS 35932 end
            //smahajan22 mits 36315 start 
          
		    sXml = sXml.Append("<UpdatePolicyEntities>" + chkUpdatePolicyEntities.Checked.ToString() + "</UpdatePolicyEntities>");
           // build issue 6/16/2014 dbisht6  
            if (txtAddConfig.Text != string.Empty)
            {
                if (string.IsNullOrEmpty(AddConfig[0]))
                    sXml = sXml.Append("<DefaultClmOffice>0</DefaultClmOffice>");
                else
                    sXml = sXml.Append("<DefaultClmOffice>" + AddConfig[0] + "</DefaultClmOffice>");
                if (string.IsNullOrEmpty(AddConfig[1]))
                    sXml = sXml.Append("<DefaultExaminer>0</DefaultExaminer>");
                else
                    sXml = sXml.Append("<DefaultExaminer>" + AddConfig[1] + "</DefaultExaminer>");
                if (string.IsNullOrEmpty(AddConfig[2]))
                    sXml = sXml.Append("<DefaultExaminerCD>0</DefaultExaminerCD>");
                else
                    sXml = sXml.Append("<DefaultExaminerCD>" + AddConfig[2] + "</DefaultExaminerCD>");
                if (string.IsNullOrEmpty(AddConfig[3]))
                    sXml = sXml.Append("<DefaultAdjustor>0</DefaultAdjustor>");
                else
                    sXml = sXml.Append("<DefaultAdjustor>" + AddConfig[3] + "</DefaultAdjustor>");
                if (string.IsNullOrEmpty(AddConfig[4]))
                    sXml = sXml.Append("<EntryInPMSP0000For>0</EntryInPMSP0000For>");
                else
                    sXml = sXml.Append("<EntryInPMSP0000For>" + AddConfig[4] + "</EntryInPMSP0000For>");
                sXml = sXml.Append("<Adjustor>" + AddConfig[5] + "</Adjustor>");
                sXml = sXml.Append("<ClmOffice>" + AddConfig[6] + "</ClmOffice>");
                sXml = sXml.Append("<Examiner>" + AddConfig[7] + "</Examiner>");
                sXml = sXml.Append("<ExaminerCD>" + AddConfig[8] + "</ExaminerCD>");
                //sXml = sXml.Append("<DefaultAdj>" + Convert.ToInt32(AddConfig[9]) + "</DefaultAdj>");
                if (string.IsNullOrEmpty(AddConfig[9]))
                    sXml = sXml.Append("<ExemptClaimId>0</ExemptClaimId>");
                else
                    sXml = sXml.Append("<ExemptClaimId>" + AddConfig[9] + "</ExemptClaimId>");
                //start - added by Nikhil.Upload voucher amount setting
                if (string.IsNullOrEmpty(AddConfig[10]))
                    sXml = sXml.Append("<UploadVoucher>0</UploadVoucher>");
                else
                    sXml = sXml.Append("<UploadVoucher>" + AddConfig[10] + "</UploadVoucher>");
                //End - added by Nikhil.Upload voucher amount setting
                //aaggarwal29 -RMA-7701 start
                if (string.IsNullOrEmpty(AddConfig[11]))
                    sXml = sXml.Append("<Payee2inPMSPAP00>0</Payee2inPMSPAP00>");
                else
                    sXml = sXml.Append("<Payee2inPMSPAP00>" + AddConfig[11] + "</Payee2inPMSPAP00>");
                //aaggarwal29 -RMA-7701 end
            }
			//build issue 6/16/2014 end
            else
            {
                sXml = sXml.Append("<DefaultClmOffice></DefaultClmOffice>");
                sXml = sXml.Append("<DefaultExaminer></DefaultExaminer>");
                sXml = sXml.Append("<DefaultExaminerCD></DefaultExaminerCD>");
                sXml = sXml.Append("<DefaultAdjustor></DefaultAdjustor>");
                sXml = sXml.Append("<EntryInPMSP0000For></EntryInPMSP0000For>");
                sXml = sXml.Append("<Adjustor></Adjustor>");
                sXml = sXml.Append("<ClmOffice></ClmOffice>");
                sXml = sXml.Append("<Examiner></Examiner>");
                sXml = sXml.Append("<ExaminerCD></ExaminerCD>");
               // sXml = sXml.Append("<DefaultAdj></DefaultAdj>");
                sXml = sXml.Append("<ExemptClaimId></ExemptClaimId>");
                //added by Nikhil.Upload voucher amount setting
                sXml = sXml.Append("<UploadVoucher></UploadVoucher>");
                //end -added by Nikhil.Upload voucher amount setting
                sXml = sXml.Append("<Payee2inPMSPAP00></Payee2inPMSPAP00>");//aaggarwal29 : new node for RMA-7701
            }
            //smahajan22 mits 36315 end 
            //Payal:RMA-6233 --starts

            if (rdo_RealTime.Checked)
            {
                sXml = sXml.Append("<RealTimeUpload> -1 </RealTimeUpload>");
            }
            else
            {
                sXml = sXml.Append("<RealTimeUpload> 0 </RealTimeUpload>");
            }
            sXml = sXml.Append("<UploadServiceUrl>" + UploadServiceUrl.Text + "</UploadServiceUrl>");
            //Payal:RMA-6233 --Ends
            sXml = sXml.Append("</PolicySys></PolicyInterface></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;

        }
        private XElement GetVersionTemplate()
        {
            string sPolSysTypeCode = "0";
            if (PolicySystemType.CodeIdValue != string.Empty)
            {
                sPolSysTypeCode = PolicySystemType.CodeIdValue;
            }
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("SetUpPolicySystemAdaptor.GetVersionInfo");
            sXml = sXml.Append("</Function></Call><Document><PolicyInterface><FunctionToCall/><PolicySys>");
            sXml = sXml.Append("<Versions Type='" + sPolSysTypeCode + "'></Versions>");
            sXml = sXml.Append("</PolicySys></PolicyInterface></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
 
        /// <summary>
        /// Binds data to the controls
        /// </summary>
        internal void BindControls()
        {
            bool bSuccess = false;
            oFDMPageDom = new XmlDocument();
            TextBox mode = new TextBox(); //bram4: MITS 35932 
            XmlNode xmlNode = null;
            XmlNodeList RealTimeUpload = null; //Payal:RMA-6233
            oFDMPageDom = Data;

            XmlElement objPolicySysXMLEle = null;
            StringBuilder stxtAddConfig = new StringBuilder();
            objPolicySysXMLEle = (XmlElement)oFDMPageDom.SelectSingleNode("//PolicySys");

            if(objPolicySysXMLEle.GetElementsByTagName("PolicySysName").Count!=0)
                PolicySysName.Text = objPolicySysXMLEle.GetElementsByTagName("PolicySysName").Item(0).InnerText;

            // aaggarwal29: Code mapping changes start
            if (objPolicySysXMLEle.GetElementsByTagName("MappingTablePrefix").Count != 0)
                MappingTablePrefix.Text = objPolicySysXMLEle.GetElementsByTagName("MappingTablePrefix").Item(0).InnerText;
            //aaggarwal29: Code mapping changes end
            if (objPolicySysXMLEle.GetElementsByTagName("PolicySystemType").Count != 0)
            {
                PolicySystemType.CodeId = ((XmlElement)(objPolicySysXMLEle.GetElementsByTagName("PolicySystemType").Item(0))).GetAttribute("codeid");
                PolicySystemType.CodeText = objPolicySysXMLEle.GetElementsByTagName("PolicySystemType").Item(0).InnerText;
            }
         
            if (objPolicySysXMLEle.GetElementsByTagName("Domain").Count!=0)
                Domain.Text = objPolicySysXMLEle.GetElementsByTagName("Domain").Item(0).InnerText;


            if (objPolicySysXMLEle.GetElementsByTagName("License").Count!=0)
                License.Text = objPolicySysXMLEle.GetElementsByTagName("License").Item(0).InnerText;

            if(objPolicySysXMLEle.GetElementsByTagName("Proxy").Count!=0)
                Proxy.Text = objPolicySysXMLEle.GetElementsByTagName("Proxy").Item(0).InnerText;

            xmlNode = objPolicySysXMLEle.GetElementsByTagName("UserName").Item(0);
            if (xmlNode != null && xmlNode.InnerText.Trim() != string.Empty)
                UserName.Text = xmlNode.InnerText;

            xmlNode = objPolicySysXMLEle.GetElementsByTagName("Password").Item(0);
            if (xmlNode != null && xmlNode.InnerText.Trim() != string.Empty)
                tempPassword.Text = xmlNode.InnerText;

            if (objPolicySysXMLEle.GetElementsByTagName("URLParam").Count!=0)
                URLParam.Text = objPolicySysXMLEle.GetElementsByTagName("URLParam").Item(0).InnerText;

            PolSystemType = (PolicySystemType.CodeTextValue).Trim().Split(' ');
            //Payal:RMA-6233 Starts
            PolicySystemName = CommonFunctions.GetPolicySystemTypeIndicator(PolSystemType[0].Trim());
            //  if (PolSystemType[0].Trim() == "INTEGRAL")  
            if (PolicySystemName == Common.Constants.POLICY_SYSTEM_TYPE.INTEGRAL)
            //Payal:RMA-6233 Ends
            {
                if (objPolicySysXMLEle.GetElementsByTagName("policysystemid").Count != 0)
                    RowId.Text = objPolicySysXMLEle.GetElementsByTagName("policysystemid").Item(0).InnerText;

                if (Conversion.CastToType<int>(RowId.Text, out bSuccess) > 0)
                {
                    Password.Visible = false;
                    lbPassword.Visible = false;
                    ConfirmPassword.Visible = false;
                    lbconfirmPassword.Visible = false;
                    if (DefaultPolicy.Checked)
                        DefaultPolicy.Enabled = false;
                    else
                        DefaultPolicy.Enabled = true;
                }

                //sanoopsharma - MITS 35932 start
                if (objPolicySysXMLEle.GetElementsByTagName("WSSearchURL").Count != 0)
                    WSSearchURL.Text = objPolicySysXMLEle.GetElementsByTagName("WSSearchURL").Item(0).InnerText;
                if (objPolicySysXMLEle.GetElementsByTagName("WSInquiryURL").Count != 0)
                    WSInquiryURL.Text = objPolicySysXMLEle.GetElementsByTagName("WSInquiryURL").Item(0).InnerText;
                //bram4 - RMA-3744 - start
                if (objPolicySysXMLEle.GetElementsByTagName("CASServerURL").Count != 0 && objPolicySysXMLEle.GetElementsByTagName("CASServerURL").Item(0).InnerText != "")
                {
                    CASServerURL.Text = objPolicySysXMLEle.GetElementsByTagName("CASServerURL").Item(0).InnerText;
                    UseCAS.Checked = true;
                }
                if (objPolicySysXMLEle.GetElementsByTagName("CASServiceURL").Count != 0 && objPolicySysXMLEle.GetElementsByTagName("CASServiceURL").Item(0).InnerText != "")
                    CASServiceURL.Text = objPolicySysXMLEle.GetElementsByTagName("CASServiceURL").Item(0).InnerText;
                //bram4 - RMA-3744 - end
                //sanoopsharma - MITS 35932 end
                //Payal:RMA-6233 starts
                if (objPolicySysXMLEle.GetElementsByTagName("UploadServiceUrl").Count != 0 && objPolicySysXMLEle.GetElementsByTagName("UploadServiceUrl").Item(0).InnerText != "")
                    UploadServiceUrl.Text = objPolicySysXMLEle.GetElementsByTagName("UploadServiceUrl").Item(0).InnerText;
                //Payal:RMA-6233 ends

            }
                //Anu Tennyson : Staging Implementation Starts
            else if (PolicySystemName == Common.Constants.POLICY_SYSTEM_TYPE.STAGING)
            {
                if (objPolicySysXMLEle.GetElementsByTagName("StagingConn").Count != 0)
                {
                    txtStagingConn.Text = objPolicySysXMLEle.GetElementsByTagName("StagingConn").Item(0).InnerText;
                }
                // rma-10039 - policy App & staging integration
                if (DefaultPolicy.Checked)
                    DefaultPolicy.Enabled = false;
                else
                    DefaultPolicy.Enabled = true;

                if (objPolicySysXMLEle.GetElementsByTagName("DefaultPolicy").Count != 0)
                {
                    if (Conversion.ConvertStrToBool(objPolicySysXMLEle.GetElementsByTagName("DefaultPolicy").Item(0).InnerText))
                    {
                        DefaultPolicy.Checked = true;
                    }
                    else
                    {
                        DefaultPolicy.Checked = false;
                    }
                }
            }
                //Anu Tennyson Ends
            else  //bram4: MITS 35932 - POINT specific control binding
            {
            if(objPolicySysXMLEle.GetElementsByTagName("TargetDatabase").Count!=0)
                TargetDatabase.Text = objPolicySysXMLEle.GetElementsByTagName("TargetDatabase").Item(0).InnerText;

            if (objPolicySysXMLEle.GetElementsByTagName("LocCompny").Count != 0)
                txtLocCmpny.Text = objPolicySysXMLEle.GetElementsByTagName("LocCompny").Item(0).InnerText;

            if (objPolicySysXMLEle.GetElementsByTagName("MasterCompany").Count != 0)
                txtMasterCompany.Text = objPolicySysXMLEle.GetElementsByTagName("MasterCompany").Item(0).InnerText;

            if (objPolicySysXMLEle.GetElementsByTagName("BusinessEntities").Count != 0)
                txtBusinessEntities.Text = objPolicySysXMLEle.GetElementsByTagName("BusinessEntities").Item(0).InnerText;


            if (objPolicySysXMLEle.GetElementsByTagName("MajorPerils").Count != 0)
                txtMajorPerils.Text = objPolicySysXMLEle.GetElementsByTagName("MajorPerils").Item(0).InnerText;


            if (objPolicySysXMLEle.GetElementsByTagName("IntertestList").Count != 0)
                txtIntertestList.Text = objPolicySysXMLEle.GetElementsByTagName("IntertestList").Item(0).InnerText;

            //if (objPolicySysXMLEle.GetElementsByTagName("ClVehicleUnitMapping").Count != 0)
            //    txtClVehicleUnitMapping.Text = objPolicySysXMLEle.GetElementsByTagName("ClVehicleUnitMapping").Item(0).InnerText;

            //if (objPolicySysXMLEle.GetElementsByTagName("ClPropertyUnitMapping").Count != 0)
            //    txtClPropertyUnitMapping.Text = objPolicySysXMLEle.GetElementsByTagName("ClPropertyUnitMapping").Item(0).InnerText;
                /*
                //bram4: Start  MITS 35932 code moved 
            if (objPolicySysXMLEle.GetElementsByTagName("FinancialUpdate").Count != 0)
            {
                if (Conversion.ConvertStrToBool(objPolicySysXMLEle.GetElementsByTagName("FinancialUpdate").Item(0).InnerText))
                {
                    FinancialUpdate.Checked = true;
                }
                else
                {
                    FinancialUpdate.Checked = false;
                }
            }

            if (objPolicySysXMLEle.GetElementsByTagName("DefaultPolicy").Count != 0)
            {
                if (Conversion.ConvertStrToBool(objPolicySysXMLEle.GetElementsByTagName("DefaultPolicy").Item(0).InnerText))
                {
                    DefaultPolicy.Checked = true;
                }
                else
                {
                    DefaultPolicy.Checked = false;
                }
                }*/
                //bram4: End MITS 35932
            if(objPolicySysXMLEle.GetElementsByTagName("policysystemid").Count!=0)
                RowId.Text = objPolicySysXMLEle.GetElementsByTagName("policysystemid").Item(0).InnerText;

            if (Conversion.CastToType<int>(RowId.Text, out bSuccess) > 0)
            {
                Password.Visible = false;
                lbPassword.Visible = false;
                ConfirmPassword.Visible = false;
                lbconfirmPassword.Visible = false;
                if (DefaultPolicy.Checked)
                    DefaultPolicy.Enabled = false;
                else
                    DefaultPolicy.Enabled = true;
            }
                //bram4: Start  MITS 35932 code moved 
                /*
            //rupal:start
                if (objPolicySysXMLEle.GetElementsByTagName("PolicySystemType").Count != 0)
                {
                    PolicySystemType.CodeId = ((XmlElement)(objPolicySysXMLEle.GetElementsByTagName("PolicySystemType").Item(0))).GetAttribute("codeid");
                    PolicySystemType.CodeText = objPolicySysXMLEle.GetElementsByTagName("PolicySystemType").Item(0).InnerText;
                }
                //rupal:end
        */
                //bram4: End  MITS 35932
                if (objPolicySysXMLEle.GetElementsByTagName("ClientFileFlag").Count != 0)
                {
                    if (objPolicySysXMLEle.GetElementsByTagName("ClientFileFlag").Item(0).InnerText == "0")
                    {
                        rbOff.Checked = true;
                        chkUpdatePolicyEntities.Disabled = true;
                    }
                    else if (objPolicySysXMLEle.GetElementsByTagName("ClientFileFlag").Item(0).InnerText == "1")
                    {
                        rbOn.Checked = true;
                        chkUpdatePolicyEntities.Disabled = false;
                    }
                }
                if (objPolicySysXMLEle.GetElementsByTagName("ClientFileFlag").Item(0) != null)
                {
                    checkrbState.Text = objPolicySysXMLEle.GetElementsByTagName("ClientFileFlag").Item(0).InnerText;
                }
                //TextBox mode = new TextBox();//bram4: MITS 35932
            } //bram4: MITS 35932 - point specific control binding end



            //sanoopsharma - MITS 35932 start


            if (objPolicySysXMLEle.GetElementsByTagName("CFWUserName").Count != 0)
            {
                txtCFWUserNm.Text = objPolicySysXMLEle.GetElementsByTagName("CFWUserName").Item(0).InnerText;
            }
            xmlNode = objPolicySysXMLEle.GetElementsByTagName("CFWUserPassword").Item(0);
            if (xmlNode != null && xmlNode.InnerText.Trim() != string.Empty)
            {
                txtCFWUserPassword.Attributes.Add("value", "##########");
                //txtCFWUserPassword.Text = objPolicySysXMLEle.GetElementsByTagName("CFWUserPassword").Item(0).InnerText;
                xmlNode = null;
            }
            if (objPolicySysXMLEle.GetElementsByTagName("PointURL").Count != 0)
            {
                txtPointURL.Text = objPolicySysXMLEle.GetElementsByTagName("PointURL").Item(0).InnerText;
            }
            if (objPolicySysXMLEle.GetElementsByTagName("UseRMAUserForCFW").Count != 0)
            {
                if (Conversion.ConvertStrToBool(objPolicySysXMLEle.GetElementsByTagName("UseRMAUserForCFW").Item(0).InnerText))
                {
                    cbUseRMAForCFW.Checked = true;
                   // txtCFWUserNm.Attributes.Add("readOnly", "true");
                }
                else 
                {
                    cbUseRMAForCFW.Checked = false;
                }
            }
            /*
                        if (objPolicySysXMLEle.GetElementsByTagName("ClientFileFlag").Count != 0)
                        {
                            if (objPolicySysXMLEle.GetElementsByTagName("ClientFileFlag").Item(0).InnerText == "0")
                            {
                                rbOff.Checked = true;
                                chkUpdatePolicyEntities.Disabled = true; 
                            }
                            else if (objPolicySysXMLEle.GetElementsByTagName("ClientFileFlag").Item(0).InnerText == "1")
                            {
                                rbOn.Checked = true;
                                chkUpdatePolicyEntities.Disabled = false; 
                            }
                        }
                        if (objPolicySysXMLEle.GetElementsByTagName("ClientFileFlag").Item(0) != null)
                        {
                            checkrbState.Text = objPolicySysXMLEle.GetElementsByTagName("ClientFileFlag").Item(0).InnerText;
                        } */
            //TextBox mode = new TextBox();

            //tanwar2 - Added if condition for stagign
            mode.Text = AppHelper.GetQueryStringValue("mode");
            if (objPolicySysXMLEle.GetElementsByTagName("Versions")!= null
                && objPolicySysXMLEle.GetElementsByTagName("Version") != null
                && objPolicySysXMLEle.GetElementsByTagName("Version").Item(0) != null)
            {
                BindVersionDropDown(mode.Text, objPolicySysXMLEle.GetElementsByTagName("Versions").Item(0), objPolicySysXMLEle.GetElementsByTagName("Version").Item(0).InnerText);
            }
            
            //bram4: MITS 35932 - Start
            //if (objPolicySysXMLEle.GetElementsByTagName("ClVehicleUnitMapping").Count != 0)
            //    txtClVehicleUnitMapping.Text = objPolicySysXMLEle.GetElementsByTagName("ClVehicleUnitMapping").Item(0).InnerText;

            //if (objPolicySysXMLEle.GetElementsByTagName("ClPropertyUnitMapping").Count != 0)
            //    txtClPropertyUnitMapping.Text = objPolicySysXMLEle.GetElementsByTagName("ClPropertyUnitMapping").Item(0).InnerText;
            if (objPolicySysXMLEle.GetElementsByTagName("FinancialUpdate").Count != 0)
            {
                if (Conversion.ConvertStrToBool(objPolicySysXMLEle.GetElementsByTagName("FinancialUpdate").Item(0).InnerText))
                {
                    FinancialUpdate.Checked = true;
                }
                else
                {
                    FinancialUpdate.Checked = false;
                }
            }
            //payal:RMA-6233 starts
            if (FinancialUpdate.Checked)
            {
                RealTimeDiv.Style["display"] = "block";

                RealTimeUpload = objPolicySysXMLEle.GetElementsByTagName("RealTimeUpload");
                if (RealTimeUpload.Count != 0)
                {
                    if (RealTimeUpload.Item(0) != null)
                    {
                        if (RealTimeUpload.Item(0).InnerText == "-1")
                        {
                            rdo_RealTime.Checked = true;
                        }
                        if (RealTimeUpload.Item(0).InnerText == "0")
                        {
                            rdo_Batch.Checked = true;
                        }

                    }
                    if (PolicySystemName == Common.Constants.POLICY_SYSTEM_TYPE.INTEGRAL)
                    {
                        rdo_RealTime.Disabled = false;
                    }
                    else
                    {
                        rdo_RealTime.Disabled = true;
                    }
                }

            }
            //payal:RMA-6233 ends
            if (objPolicySysXMLEle.GetElementsByTagName("DefaultPolicy").Count != 0)
            {
                if (Conversion.ConvertStrToBool(objPolicySysXMLEle.GetElementsByTagName("DefaultPolicy").Item(0).InnerText))
                {
                    DefaultPolicy.Checked = true;
                }
                else
                {
                    DefaultPolicy.Checked = false;
                }
            }
            if (mode.Text.ToUpper() == "EDIT")
            {
                PolicySystemType.Enabled = false;
                MappingTablePrefix.Enabled = false; // aaggarwal29: Code mapping change
                }
            if (objPolicySysXMLEle.GetElementsByTagName("DataDSNConnStr").Count != 0)
            { 
                 txtDataDSNConnStr.Text = objPolicySysXMLEle.GetElementsByTagName("DataDSNConnStr").Item(0).InnerText;       
            }
            if (objPolicySysXMLEle.GetElementsByTagName("ClientDSNConnStr").Count != 0)
            {
                txtClientDSNConnStr.Text = objPolicySysXMLEle.GetElementsByTagName("ClientDSNConnStr").Item(0).InnerText;
            }
            //build issue 6/16/2014 dbisht6 
            //smahajan22 mits 36315 start
            if (objPolicySysXMLEle.GetElementsByTagName("DefaultClmOffice").Item(0)!=null)
                stxtAddConfig.Append(objPolicySysXMLEle.GetElementsByTagName("DefaultClmOffice").Item(0).InnerText);
            stxtAddConfig.Append("|");
            if (objPolicySysXMLEle.GetElementsByTagName("ClmOffice").Item(0)!=null)
                stxtAddConfig.Append(objPolicySysXMLEle.GetElementsByTagName("ClmOffice").Item(0).InnerText);
            stxtAddConfig.Append("|");
            if (objPolicySysXMLEle.GetElementsByTagName("DefaultExaminer").Item(0) != null)
                stxtAddConfig.Append(objPolicySysXMLEle.GetElementsByTagName("DefaultExaminer").Item(0).InnerText);
            stxtAddConfig.Append("|");
            if (objPolicySysXMLEle.GetElementsByTagName("Examiner").Item(0)!=null)
                stxtAddConfig.Append(objPolicySysXMLEle.GetElementsByTagName("Examiner").Item(0).InnerText);
            stxtAddConfig.Append("|");
            if (objPolicySysXMLEle.GetElementsByTagName("DefaultExaminerCD").Item(0)!=null)
                stxtAddConfig.Append(objPolicySysXMLEle.GetElementsByTagName("DefaultExaminerCD").Item(0).InnerText);
            stxtAddConfig.Append("|");
            if (objPolicySysXMLEle.GetElementsByTagName("ExaminerCD").Item(0)!= null)
                stxtAddConfig.Append(objPolicySysXMLEle.GetElementsByTagName("ExaminerCD").Item(0).InnerText);
            stxtAddConfig.Append("|");
            if (objPolicySysXMLEle.GetElementsByTagName("DefaultAdjustor").Item(0)!= null)
                stxtAddConfig.Append(objPolicySysXMLEle.GetElementsByTagName("DefaultAdjustor").Item(0).InnerText);
            stxtAddConfig.Append("|");
            if (objPolicySysXMLEle.GetElementsByTagName("Adjustor").Item(0)!= null)
                stxtAddConfig.Append(objPolicySysXMLEle.GetElementsByTagName("Adjustor").Item(0).InnerText);
            stxtAddConfig.Append("|");
            if (objPolicySysXMLEle.GetElementsByTagName("EntryInPMSP0000For").Item(0)!= null)
                stxtAddConfig.Append(objPolicySysXMLEle.GetElementsByTagName("EntryInPMSP0000For").Item(0).InnerText);
            stxtAddConfig.Append("|");
            if (objPolicySysXMLEle.GetElementsByTagName("ExemptClaimId").Item(0)!= null)
                stxtAddConfig.Append(objPolicySysXMLEle.GetElementsByTagName("ExemptClaimId").Item(0).InnerText);
            //added by Nikhil.Upload voucher amount setting
            stxtAddConfig.Append("|");
            if (objPolicySysXMLEle.GetElementsByTagName("UploadVoucher").Item(0) != null)
                stxtAddConfig.Append(objPolicySysXMLEle.GetElementsByTagName("UploadVoucher").Item(0).InnerText);
            //End -  added by Nikhil.Upload voucher amount setting
            //aaggarwal29 -RMA-7701 start
            stxtAddConfig.Append("|");
            if (objPolicySysXMLEle.GetElementsByTagName("Payee2inPMSPAP00").Item(0) != null)
                stxtAddConfig.Append(objPolicySysXMLEle.GetElementsByTagName("Payee2inPMSPAP00").Item(0).InnerText);
            //aaggarwal29 -RMA-7701 end
            txtAddConfig.Text = stxtAddConfig.ToString();
            // dbisht6 mits 36536
            if (objPolicySysXMLEle.GetElementsByTagName("UpdatePolicyEntities").Item(0) != null)
            {
                    if (objPolicySysXMLEle.GetElementsByTagName("UpdatePolicyEntities").Item(0).InnerText == "-1")
                    {
                        chkUpdatePolicyEntities.Checked = true;
                    }
                    if (objPolicySysXMLEle.GetElementsByTagName("UpdatePolicyEntities").Item(0).InnerText == "0")
                    {
                        chkUpdatePolicyEntities.Checked = false;
                    }
               
            }
            // mits 36536 end
            //txtAddConfig.Text = objPolicySysXMLEle.GetElementsByTagName("DefaultClmOffice").Item(0).InnerText 
            //    + "|" + objPolicySysXMLEle.GetElementsByTagName("ClmOffice").Item(0).InnerText +
            //              "|" + objPolicySysXMLEle.GetElementsByTagName("DefaultExaminer").Item(0).InnerText + "|" + objPolicySysXMLEle.GetElementsByTagName("Examiner").Item(0).InnerText +
            //              "|" + objPolicySysXMLEle.GetElementsByTagName("DefaultExaminerCD").Item(0).InnerText + "|" + objPolicySysXMLEle.GetElementsByTagName("ExaminerCD").Item(0).InnerText +
            //              "|" + objPolicySysXMLEle.GetElementsByTagName("DefaultAdjustor").Item(0).InnerText + "|" + objPolicySysXMLEle.GetElementsByTagName("Adjustor").Item(0).InnerText +
            //              "|" + objPolicySysXMLEle.GetElementsByTagName("EntryInPMSP0000For").Item(0).InnerText + "|" + objPolicySysXMLEle.GetElementsByTagName("ExemptClaimId").Item(0).InnerText;
            ////smahajan22 mits 36315 end
        }
        protected void BindVersionDropDown(string pMode, XmlNode pDoc, string pSelectedVersion)
        {
            ListItem li;
            Version.Items.Clear();
            li = new ListItem("Select", "0");
            Version.Items.Add(li);
            foreach (XmlNode oNode in pDoc.SelectNodes("Version"))
            {
                li = new ListItem(oNode.InnerText, oNode.InnerText);
                Version.Items.Add(li);
            }
            pMode = pMode.Trim().ToUpper();
            if (pMode == "ADD")
            {
                Version.SelectedIndex = 0;
            }
            else if (pMode == "EDIT")
            {
                Version.SelectedValue = pSelectedVersion;
            }
        }
        protected void btnOk_Click(object sender, EventArgs e)
        {
             try
             {
                 XmlTemplate = GetMessageTemplate();
                 CallCWS("SetUpPolicySystemAdaptor.Save", XmlTemplate, out sCWSresponse, false, false);
                 XmlDoc.LoadXml(sCWSresponse);
                 string sMsgStatus = XmlDoc.SelectSingleNode("//MsgStatusCd").InnerText;
                 if (sMsgStatus == "Success")
                 {
                     Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Page.ClientScript", "fnRefreshParentAndClosePopup();", true);
                 }
				 //smahajan22 mits 36315  start
                 else if (sMsgStatus == "Error")
                 {
                     txtAddConfig.Text = string.Empty;
                 }
				 //smahajan22 mits 36315 end
                 //sanoopsharma - schedule a task 
                 if (rdo_RealTime.Checked == true)
                 {
                     ScheduleTask();
                 }
                 else if (FinancialUpdate.Checked == false || rdo_Batch.Checked == true)
                 {
                     DeleteTask();
                 }
                 //sanoopsharma

             }
             catch (FaultException<RMException> ee)
             {
                 ErrorControl1.Visible = true;
                 ErrorHelper.logErrors(ee);
                 ErrorControl1.errorDom = ee.Detail.Errors;
             }
             catch (Exception ee)
             {
                 ErrorHelper.logErrors(ee);
                 BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                 err.Add(ee, BusinessAdaptorErrorType.SystemError);
                 ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
             }
        }

        //sanoopsharma

        public override void ModifyXml(ref XElement Xelement)
        {
        }

        private void ScheduleTask()
        {
         
            string sCWSresponse = string.Empty;
            XElement XmlMessage = null;
            XmlMessage = GetMessageTemplateForScheduling();
            if (XmlMessage!=null)
            CallCWS("SetUpPolicySystemAdaptor.ScheduleTask", XmlMessage, out sCWSresponse, false, false);
        }

        private void DeleteTask()
        {

            string sCWSresponse = string.Empty;
            XElement XmlMessage = null;
            XmlMessage = GetMessageTemplateForDeleting();
            if (XmlMessage != null)
                CallCWS("SetUpPolicySystemAdaptor.DeleteTask", XmlMessage, out sCWSresponse, false, false);
        }
       
        private XElement GetMessageTemplateForScheduling()
        {

            string strRowId = AppHelper.GetQueryStringValue("selectedid");
                RowId.Text = strRowId.Substring(strRowId.LastIndexOf(',') + 1);

            XElement oTemplate = XElement.Parse(@"            
                <Message>
                <Authorization></Authorization>
                <Call>
                    <Function>TaskManagementAdaptor.GetIntervalType</Function>
                </Call>
                <Document>
                    <Details>
					    <IntervalType /> 
                        <IntervalTypeId />
                        <Interval /> 
                        <TaskTypeText></TaskTypeText> 
                        <ScheduleTypeText></ScheduleTypeText> 
                        <TaskType></TaskType> 
                        <ScheduleType></ScheduleType> 
                        <Time /> 
                        <Date /> 
                        <bParams /> 
                        <Arguments /> 
                        <UserArguments /> 
                        <ZeroBasedFinHist /> 
                        <ClaimBasedFinHist /> 
                        <EventBasedFinHist /> 
                        <RecreateFinHist /> 
                        <BillingOption /> 
                        <PolicySystems>" + RowId.Text + @"</PolicySystems>
                        <dtActivity /> 
                    </Details>
                </Document>
            </Message>        
            ");
            return oTemplate;
        }

        private XElement GetMessageTemplateForDeleting()
        {

            string strRowId = AppHelper.GetQueryStringValue("selectedid");
            RowId.Text = strRowId.Substring(strRowId.LastIndexOf(',') + 1);

            XElement oTemplate = XElement.Parse(@"            
                <Message>
                <Authorization></Authorization>
                <Call>
                    <Function>TaskManagementAdaptor.GetIntervalType</Function>
                </Call>
                <Document>
                    <Details>					     
                        <PolicySystems>" + RowId.Text + @"</PolicySystems>                        
                    </Details>
                </Document>
            </Message>        
            ");
            return oTemplate;
        }


        //sanoopsharma
        private XElement GetXMLForPasswordUpdate()
        {
            if (mode.Text.ToLower() == "edit")
            {
                string strRowId = AppHelper.GetQueryStringValue("selectedid");
                RowId.Text = strRowId.Substring(strRowId.LastIndexOf(',') + 1);
            }
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("SetUpPolicySystemAdaptor.UpdatePassword");
            sXml = sXml.Append("</Function></Call><Document><PolicyInterface><FunctionToCall/><PolicySys><ChangePassword>");
            sXml = sXml.Append("<NewPassword>" + NewPwd.Text + "</NewPassword>");
            sXml = sXml.Append("<policysystemid>" + RowId.Text + "</policysystemid>");

            sXml = sXml.Append("</ChangePassword></PolicySys></PolicyInterface></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;

        }

        protected void btnOk2_Click(object sender, EventArgs e)
        {
            try
            {
                XmlTemplate = GetXMLForPasswordUpdate();
                CallCWS("SetUpPolicySystemAdaptor.UpdatePassword", XmlTemplate, out sCWSresponse, false, false);
                XmlDoc.LoadXml(sCWSresponse);
                string sMsgStatus = XmlDoc.SelectSingleNode("//MsgStatusCd").InnerText;
                if (sMsgStatus == "Success")
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "dd", "showalert();", true);
                }

            }
            catch (FaultException<RMException> ee)
            {
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                ErrorControl1.errorDom = ee.Detail.Errors;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

       
        
    }
}