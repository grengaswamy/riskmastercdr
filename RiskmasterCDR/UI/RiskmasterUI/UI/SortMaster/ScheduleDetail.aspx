﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ScheduleDetail.aspx.cs" Inherits="Riskmaster.UI.SortMaster.ScheduleDetail" %>

<!DOCTYPE html>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl" TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>
    <script type="JavaScript" src="../../Scripts/form.js"></script>
    <script type="JavaScript" src="../../Scripts/oshareportdata.js"></script>
    <script type="text/javascript" >

//        function Validation()
//        {
//            var i = 0;

//            if (document.forms[0].datemethod[0].checked == false &&
//                document.forms[0].datemethod[1].checked == true &&
//                document.forms[0].datemethod[2].checked == false)
//            {
//                if (document.getElementById("begindate").value == "")
//                {
//                    frmData.innerHTML = "<b><font color=red>There were errors in the submitted report criteria. Please hit the Back button on your browser to correct the mistakes and submit again.<br /><br />" +
//                                         "</font></b>" +
//                                         "<font size=2><ul>" +
//                                         "<li>COSHAManager.COSHAManager.ValidateCriteria : Invalid BeginDate Specified.</li>" +
//                                         "<li>IReport_Engine_CriteriaXMLDom.CriteriaXMLDom [Property Set] : Failed Criteria Check. (See previous error.)</li>" +
//                                         "</ul></font>"
//                    i = 1;
//                }
//                else if (document.getElementById("enddate").value == "")
//                {
//                    frmData.innerHTML = "<b><font color=red>There were errors in the submitted report criteria. Please hit the Back button on your browser to correct the mistakes and submit again.<br /><br />" +
//                                         "</font></b>" +
//                                         "<font size=2><ul>" +
//                                         "<li>COSHAManager.COSHAManager.ValidateCriteria : Invalid EndDate Specified.</li>" +
//                                         "<li>IReport_Engine_CriteriaXMLDom.CriteriaXMLDom [Property Set] : Failed Criteria Check. (See previous error.)</li>" +
//                                         "</ul></font>"
//                    i = 1;
//                }
//                else if (document.getElementById("begindate").value != "" && document.getElementById("enddate").value != "") {
//                    if (document.getElementById("begindate").value.split("/")[2] != document.getElementById("enddate").value.split("/")[2]) {
//                        frmData.innerHTML = "<b><font color=red>There were errors in the submitted report criteria. Please hit the Back button on your browser to correct the mistakes and submit again.<br /><br />" +
//                                         "</font></b>" +
//                                         "<font size=2><ul>" +
//                                         "<li>COSHAManager.COSHAManager.ValidateCriteria : Date Range Spans multiple years (not allowed)</li>" +
//                                         "<li>IReport_Engine_CriteriaXMLDom.CriteriaXMLDom [Property Set] : Failed Criteria Check. (See previous error.)</li>" +
//                                         "</ul></font>"
//                        i = 1;
//                    }
//                }
//            }
//            else if (document.forms[0].datemethod[0].checked == false &&
//                     document.forms[0].datemethod[1].checked == false &&
//                     document.forms[0].datemethod[2].checked == true)
//            {
//                if (document.getElementById("timescalar").value == "")
//                {
//                    frmData.innerHTML = "<b><font color=red>There were errors in the submitted report criteria. Please hit the Back button on your browser to correct the mistakes and submit again.<br /><br />" +
//                                         "</font></b>" +
//                                         "<font size=2><ul>" +
//                                         "<li>COSHAManager.COSHAManager.ValidateCriteria : Previous Value not Specified for Relative Reporting Date.</li>" +
//                                         "<li>IReport_Engine_CriteriaXMLDom.CriteriaXMLDom [Property Set] : Failed Criteria Check. (See previous error.)</li>" +
//                                         "</ul></font>"
//                    i = 1;
//                }
//            }

//            if (document.forms[0].allentitiesflag[0].checked == false &&
//                document.forms[0].allentitiesflag[1].checked == true &&
//                i == 0)
//            {
//                if (document.getElementById("selectedentities").innerText == "")
//                {
//                    frmData.innerHTML = "<b><font color=red>There were errors in the submitted report criteria. Please hit the Back button on your browser to correct the mistakes and submit again.<br /><br />" +
//                                         "</font></b>" +
//                                         "<font size=2><ul>" +
//                                         "<li>COSHAManager.COSHAManager.ValidateCriteria : No entities selected for reporting.</li>" +
//                                         "<li>IReport_Engine_CriteriaXMLDom.CriteriaXMLDom [Property Set] : Failed Criteria Check. (See previous error.)</li>" +
//                                         "</ul></font>"
//                    i = 1;
//                }
//            }
//            
//            if (document.getElementById("timeunit")[document.getElementById("timeunit").selectedIndex].text == "Years") {
//                frmData.innerHTML = "<b><font color=red>There were errors in the submitted report criteria. Please hit the Back button on your browser to correct the mistakes and submit again.<br /><br />" +
//                                         "</font></b>" +
//                                         "<font size=2><ul>" +
//                                         "<li>COSHAManager.COSHAManager.ValidateCriteria : Date Range Spans multiple years (not allowed)</li>" +
//                                         "<li>IReport_Engine_CriteriaXMLDom.CriteriaXMLDom [Property Set] : Failed Criteria Check. (See previous error.)</li>" +
//                                         "</ul></font>"
//                i = 1;
//            }
//            return i;
//        }

//        function SaveData() {
//            
//            var j = -1;

//            j = Validation();

//            if (j == 0)
//            {
//                if (document.forms[0].byoshaestablishmentflag[0] != null &&
//                    document.forms[0].byoshaestablishmentflag[1] != null &&
//                    document.getElementById("usereportlevel") != null &&
//                    document.getElementById("reportlevel") != null &&
//                    document.getElementById("establishmentnameprefix") != null &&
//                    document.forms[0].datemethod[0] != null &&
//                    document.getElementById("yearofreport") != null &&
//                    document.forms[0].datemethod[1] != null &&
//                    document.getElementById("begindate") != null &&
//                    document.getElementById("enddate") != null &&
//                    document.forms[0].datemethod[2] != null &&
//                    document.getElementById("timescalar") != null &&
//                    document.getElementById("timeunit") != null &&
//                    document.getElementById("asofdate") != null &&
//                    document.getElementById("enforce180dayrule") != null &&
//                    document.getElementById("printsofterrlog") != null &&
//                    document.getElementById("eventbasedflag") != null &&
//                    document.getElementById("printoshadescflag") != null &&
//                    document.getElementById("columnesource") != null &&
//                    document.getElementById("columnfsource") != null &&
//                    document.getElementById("sortorder") != null &&
//                    document.forms[0].allentitiesflag[0] != null &&
//                    document.forms[0].allentitiesflag[1] != null &&
//                    document.getElementById("selectedentities") != null) {
//                    document.getElementById("Hidden1").value = document.forms[0].byoshaestablishmentflag[0].checked + ";" +
//                                                               document.forms[0].byoshaestablishmentflag[1].checked + ";" +
//                                                               document.getElementById("usereportlevel")[document.getElementById("usereportlevel").selectedIndex].text + ";" +
//                                                               document.getElementById("reportlevel")[document.getElementById("reportlevel").selectedIndex].text + ";" +
//                                                               document.getElementById("establishmentnameprefix")[document.getElementById("establishmentnameprefix").selectedIndex].text + ";" +
//                                                               document.forms[0].datemethod[0].checked + ";" +
//                                                               document.getElementById("yearofreport")[document.getElementById("yearofreport").selectedIndex].text + ";" +
//                                                               document.forms[0].datemethod[1].checked + ";" +
//                                                               document.getElementById("begindate").value + ";" +
//                                                               document.getElementById("enddate").value + ";" +
//                                                               document.forms[0].datemethod[2].checked + ";" +
//                                                               document.getElementById("timescalar").value + ";" +
//                                                               document.getElementById("timeunit")[document.getElementById("timeunit").selectedIndex].text + ";" +
//                                                               document.getElementById("asofdate").value + ";" +
//                                                               document.getElementById("enforce180dayrule").checked + ";" +
//                                                               document.getElementById("printsofterrlog").checked + ";" +
//                                                               document.getElementById("eventbasedflag").checked + ";" +
//                                                               document.getElementById("printoshadescflag").checked + ";" +
//                                                               document.getElementById("columnesource")[document.getElementById("columnesource").selectedIndex].text + ";" +
//                                                               document.getElementById("columnfsource")[document.getElementById("columnfsource").selectedIndex].text + ";" +
//                                                               document.getElementById("sortorder")[document.getElementById("sortorder").selectedIndex].text + ";" +
//                                                               document.forms[0].allentitiesflag[0].checked + ";" +
//                                                               document.forms[0].allentitiesflag[1].checked + ";" +
//                                                               document.getElementById("selectedentities").value + ";" +
//                                                               document.getElementById("timeunit")[document.getElementById("timeunit").selectedIndex].text;
//                }

//                else if (document.getElementById("preparertitle") != null &&
//                    document.getElementById("preparername") != null &&
//                    document.getElementById("preparerphone") != null &&
//                    document.getElementById("preparersignaturedate") != null &&
//                    document.forms[0].byoshaestablishmentflag[0] != null &&
//                    document.forms[0].byoshaestablishmentflag[1] != null &&
//                    document.getElementById("usereportlevel") != null &&
//                    document.getElementById("reportlevel") != null &&
//                    document.getElementById("establishmentnameprefix") != null &&
//                    document.getElementById("yearofreport") != null &&
//                    document.forms[0].datemethod[0] != null &&
//                    document.forms[0].datemethod[1] != null &&
//                    document.getElementById("begindate") != null &&
//                    document.getElementById("enddate") != null &&
//                    document.forms[0].datemethod[2] != null &&
//                    document.getElementById("timescalar") != null &&
//                    document.getElementById("timeunit") != null &&
//                    document.getElementById("asofdate") != null &&
//                    document.getElementById("enforce180dayrule") != null &&
//                    document.getElementById("printsofterrlog") != null &&
//                    document.getElementById("eventbasedflag") != null &&
//                    document.getElementById("printoshadescflag") != null &&
//                    document.getElementById("primarylocationflag") != null &&
//                    document.forms[0].allentitiesflag[0] != null &&
//                    document.forms[0].allentitiesflag[1] != null &&
//                    document.getElementById("selectedentities") != null) {
//                    document.getElementById("Hidden1").value = document.getElementById("preparertitle").value + ";" +
//                                                               document.getElementById("preparername").value + ";" +
//                                                               document.getElementById("preparerphone").value + ";" +
//                                                               document.getElementById("preparersignaturedate").value + ";" +
//                                                               document.forms[0].byoshaestablishmentflag[0].checked + ";" +
//                                                               document.forms[0].byoshaestablishmentflag[1].checked + ";" +
//                                                               document.getElementById("usereportlevel")[document.getElementById("usereportlevel").selectedIndex].text + ";" +
//                                                               document.getElementById("reportlevel")[document.getElementById("reportlevel").selectedIndex].text + ";" +
//                                                               document.getElementById("establishmentnameprefix")[document.getElementById("establishmentnameprefix").selectedIndex].text + ";" +
//                                                               document.getElementById("yearofreport")[document.getElementById("yearofreport").selectedIndex].text + ";" +
//                                                               document.forms[0].datemethod[0].checked + ";" +
//                                                               document.forms[0].datemethod[1].checked + ";" +
//                                                               document.getElementById("begindate").value + ";" +
//                                                               document.getElementById("enddate").value + ";" +
//                                                               document.forms[0].datemethod[2].checked + ";" +
//                                                               document.getElementById("timescalar").value + ";" +
//                                                               document.getElementById("timeunit")[document.getElementById("timeunit").selectedIndex].text + ";" +
//                                                               document.getElementById("asofdate").value + ";" +
//                                                               document.getElementById("enforce180dayrule").checked + ";" +
//                                                               document.getElementById("printsofterrlog").checked + ";" +
//                                                               document.getElementById("eventbasedflag").checked + ";" +
//                                                               document.getElementById("printoshadescflag").checked + ";" +
//                                                               document.getElementById("primarylocationflag").checked + ";" +
//                                                               document.forms[0].allentitiesflag[0].checked + ";" +
//                                                               document.forms[0].allentitiesflag[1].checked + ";" +
//                                                               document.getElementById("selectedentities").value + ";" +
//                                                               document.getElementById("timeunit")[document.getElementById("timeunit").selectedIndex].text;
//                }
//                else if (document.getElementById("preparertitle") != null &&
//                    document.getElementById("preparername") != null &&
//                    document.getElementById("preparerphone") != null &&
//                    document.forms[0].byoshaestablishmentflag[0] != null &&
//                    document.forms[0].byoshaestablishmentflag[1] != null &&
//                    document.getElementById("usereportlevel") != null &&
//                    document.getElementById("reportlevel") != null &&
//                    document.getElementById("yearofreport") != null &&
//                    document.forms[0].datemethod[0] != null &&
//                    document.forms[0].datemethod[1] != null &&
//                    document.getElementById("begindate") != null &&
//                    document.getElementById("enddate") != null &&
//                    document.forms[0].datemethod[2] != null &&
//                    document.getElementById("timescalar") != null &&
//                    document.getElementById("timeunit") != null &&
//                    document.getElementById("printsofterrlog") != null &&
//                    document.getElementById("eventbasedflag") != null &&
//                    document.getElementById("printoshadescflag") != null &&
//                    document.getElementById("primarylocationflag") != null &&
//                    document.forms[0].allentitiesflag[0] != null &&
//                    document.forms[0].allentitiesflag[1] != null &&
//                    document.getElementById("selectedentities") != null) {
//                    document.getElementById("Hidden1").value = document.getElementById("preparertitle").value + ";" +
//                                                               document.getElementById("preparername").value + ";" +
//                                                               document.getElementById("preparerphone").value + ";" +
//                                                               document.forms[0].byoshaestablishmentflag[0].checked + ";" +
//                                                               document.forms[0].byoshaestablishmentflag[1].checked + ";" +
//                                                               document.getElementById("usereportlevel")[document.getElementById("usereportlevel").selectedIndex].text + ";" +
//                                                               document.getElementById("reportlevel")[document.getElementById("reportlevel").selectedIndex].text + ";" +
//                                                               document.getElementById("yearofreport")[document.getElementById("yearofreport").selectedIndex].text + ";" +
//                                                               document.forms[0].datemethod[0].checked + ";" +
//                                                               document.forms[0].datemethod[1].checked + ";" +
//                                                               document.getElementById("begindate").value + ";" +
//                                                               document.getElementById("enddate").value + ";" +
//                                                               document.forms[0].datemethod[2].checked + ";" +
//                                                               document.getElementById("timescalar").value + ";" +
//                                                               document.getElementById("timeunit")[document.getElementById("timeunit").selectedIndex].text + ";" +
//                                                               document.getElementById("printsofterrlog").checked + ";" +
//                                                               document.getElementById("eventbasedflag").checked + ";" +
//                                                               document.getElementById("printoshadescflag").checked + ";" +
//                                                               document.getElementById("primarylocationflag").checked + ";" +
//                                                               document.forms[0].allentitiesflag[0].checked + ";" +
//                                                               document.forms[0].allentitiesflag[1].checked + ";" +
//                                                               document.getElementById("selectedentities").value + ";" +
//                                                               document.getElementById("timeunit")[document.getElementById("timeunit").selectedIndex].text;
//                }
//                else if (document.forms[0].byoshaestablishmentflag[0] != null &&
//                    document.forms[0].byoshaestablishmentflag[1] != null &&
//                    document.getElementById("usereportlevel") != null &&
//                    document.getElementById("reportlevel") != null &&
//                    document.getElementById("establishmentnameprefix") != null &&
//                    document.getElementById("yearofreport") != null &&
//                    document.forms[0].datemethod[0] != null &&
//                    document.forms[0].datemethod[1] != null &&
//                    document.getElementById("begindate") != null &&
//                    document.getElementById("enddate") != null &&
//                    document.forms[0].datemethod[2] != null &&
//                    document.getElementById("timescalar") != null &&
//                    document.getElementById("timeunit") != null &&
//                    document.getElementById("printsofterrlog") != null &&
//                    document.getElementById("eventbasedflag") != null &&
//                    document.getElementById("primarylocationflag") != null &&
//                    document.getElementById("useemployeenames") != null &&
//                    document.forms[0].allentitiesflag[0] != null &&
//                    document.forms[0].allentitiesflag[1] != null &&
//                    document.getElementById("selectedentities") != null) {
//                    document.getElementById("Hidden1").value = document.forms[0].byoshaestablishmentflag[0].checked + ";" +
//                                                               document.forms[0].byoshaestablishmentflag[1].checked + ";" +
//                                                               document.getElementById("usereportlevel")[document.getElementById("usereportlevel").selectedIndex].text + ";" +
//                                                               document.getElementById("reportlevel")[document.getElementById("reportlevel").selectedIndex].text + ";" +
//                                                               document.getElementById("establishmentnameprefix")[document.getElementById("establishmentnameprefix").selectedIndex].text + ";" +
//                                                               document.getElementById("yearofreport")[document.getElementById("yearofreport").selectedIndex].text + ";" +
//                                                               document.forms[0].datemethod[0].checked + ";" +
//                                                               document.forms[0].datemethod[1].checked + ";" +
//                                                               document.getElementById("begindate").value + ";" +
//                                                               document.getElementById("enddate").value + ";" +
//                                                               document.forms[0].datemethod[2].checked + ";" +
//                                                               document.getElementById("timescalar").value + ";" +
//                                                               document.getElementById("timeunit")[document.getElementById("timeunit").selectedIndex].text + ";" +
//                                                               document.getElementById("printsofterrlog").checked + ";" +
//                                                               document.getElementById("eventbasedflag").checked + ";" +
//                                                               document.getElementById("primarylocationflag").checked + ";" +
//                                                               document.getElementById("useemployeenames").checked + ";" +
//                                                               document.forms[0].allentitiesflag[0].checked + ";" +
//                                                               document.forms[0].allentitiesflag[1].checked + ";" +
//                                                               document.getElementById("selectedentities").value + ";" +
//                                                               document.getElementById("timeunit")[document.getElementById("timeunit").selectedIndex].text;
//                }

//                if (document.getElementById("selectedentities") != null) {
//                    var i;
//                    var selectedentities = "";
//                    for (i = 0; i < document.getElementById("selectedentities").length; i++) {
//                        if (i != document.getElementById("selectedentities").length - 1) {
//                            selectedentities = selectedentities + document.getElementById("selectedentities")[i].text + ":";
//                            selectedentities = selectedentities + document.getElementById("selectedentities")[i].value + ";";
//                        }
//                        else {
//                            selectedentities = selectedentities + document.getElementById("selectedentities")[i].text + ":";
//                            selectedentities = selectedentities + document.getElementById("selectedentities")[i].value;
//                        }
//                    }
//                    document.getElementById("Hidden2").value = selectedentities;
//                }
//                parent.MDISetUnDirty(null, "0", false);
//                return true;
//            }
//        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="div1" runat="server">
        <table>
            <tr>
                <td colspan="2">
                    <uc1:errorcontrol id="ErrorControl1" runat="server" />
                </td>
            </tr>
        </table>
        <h3> Scheduled Report Details </h3>

        <table border="0" cellpadding="4" cellspacing="0" id="Table1">
            <tr>
                <td class="ctrlgroup2">
                    <asp:Label ID="lblscheduletype" runat="server" Text="Schedule Type:"></asp:Label>
                </td>
                <td class="datatd1">
                    <asp:DropDownList ID="ddlscheduletype" runat="server" OnSelectedIndexChanged="ddlscheduletype_SelectedIndexChanged" AutoPostBack="true">
                        <asp:ListItem Value="daily"> Daily </asp:ListItem>
                        <asp:ListItem Value="monthly"> Monthly </asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="ctrlgroup2">
                    <asp:Label ID="lblreportname" runat="server" Text="Report Name:"></asp:Label>
                </td>
                <td class="datatd">
                    <asp:Label ID="lblrptname" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="ctrlgroup2">
                    <asp:Label ID="lblnextrun" runat="server" Text="Next Run:"></asp:Label>
                </td>
                <td class="datatd1">
                    <asp:TextBox ID="txtnextrun" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="ctrlgroup2">
                    <asp:Label ID="lblnexttimerun" runat="server" Text="Next Time Run:"></asp:Label>
                </td>
                <td class="datatd">
                    <asp:TextBox ID="txtnexttimerun" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="ctrlgroup2">
                    <asp:Label ID="lblreportruns" runat="server" Text="Report runs every:"></asp:Label>
                </td>
                <td class="datatd">
                    <asp:CheckBox ID="chkmon" runat="server" Text="Monday" Visible="false"/>
                    <asp:CheckBox ID="chktue" runat="server" Text="Tuesday" Visible="false"/>
                    <asp:CheckBox ID="chkwed" runat="server" Text="Wednesday" Visible="false"/>
                    <asp:CheckBox ID="chkthu" runat="server" Text="Thursday" Visible="false"/>
                    <asp:CheckBox ID="chkfri" runat="server" Text="Friday" Visible="false"/>
                    <asp:CheckBox ID="chksat" runat="server" Text="Saturday" Visible="false"/>
                    <asp:CheckBox ID="chksun" runat="server" Text="Sunday" Visible="false"/>

                    <asp:CheckBox ID="chkjan" runat="server" Text="January" Visible="false"/>
                    <asp:CheckBox ID="chkfeb" runat="server" Text="February" Visible="false"/>
                    <asp:CheckBox ID="chkmar" runat="server" Text="March" Visible="false"/>
                    <asp:CheckBox ID="chkapr" runat="server" Text="April" Visible="false"/>
                    <asp:CheckBox ID="chkmay" runat="server" Text="May" Visible="false"/>
                    <asp:CheckBox ID="chkjun" runat="server" Text="June" Visible="false"/>
                    <asp:CheckBox ID="chkjul" runat="server" Text="July" Visible="false"/>
                    <asp:CheckBox ID="chkaug" runat="server" Text="August" Visible="false"/>
                    <asp:CheckBox ID="chksep" runat="server" Text="September" Visible="false"/>
                    <asp:CheckBox ID="chkoct" runat="server" Text="October" Visible="false"/>
                    <asp:CheckBox ID="chknov" runat="server" Text="November" Visible="false"/>
                    <asp:CheckBox ID="chkdec" runat="server" Text="December" Visible="false"/>
                </td>
            </tr>
            <tr>
                <td class="ctrlgroup2">
                    <asp:Label ID="lbloutputtype" runat="server" Text="Output Type:"></asp:Label>
                </td>
                <td class="datatd">
                    <asp:DropDownList ID="ddloutputtype" runat="server">
                        <asp:ListItem Value="pdf"> PDF </asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="ctrlgroup2">
                    <asp:Label ID="lblnotification" runat="server" Text="Notification Type:"></asp:Label>
                </td>
                <td class="datatd">
                    <asp:DropDownList ID="ddlnotification" runat="server">
                        <asp:ListItem Value="notifylink"> Send Email with Link to Report Output </asp:ListItem>
                        <asp:ListItem Value="notifyembed"> Send Email with Report Output Attached </asp:ListItem>
                        <asp:ListItem Value="notifyonly"> Send Email Only(no report output attached or linked) </asp:ListItem>
                        <asp:ListItem Value="none"> None </asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="ctrlgroup2">
                    <asp:Label ID="lblemail" runat="server" Text="E-Mail To:"></asp:Label>
                </td>
                <td class="datatd">
                    <asp:TextBox ID="txtemail" runat="server" TextMode="MultiLine"></asp:TextBox><br />
                    <i>(Note: separate multiple email addresses with semicolons)</i>
                </td>
            </tr>
        </table>
    </div>
    <div id="div2" runat="server">
    
    </div>
    <div id="div3" runat="server">
        <asp:Button ID="btnsave" runat="server" Text="  Save  " Class="button" OnClick="btnsave_Click"/>&nbsp;&nbsp;
        <asp:Button ID="btnback" runat="server" Text="  Back  " Class="button" onclick="btnback_Click"/>
        <input id="Hidden1" type="hidden"  runat="server"/>
        <input id="Hidden2" type="hidden"  runat="server"/>
    </div>
    </form>
</body>
</html>
