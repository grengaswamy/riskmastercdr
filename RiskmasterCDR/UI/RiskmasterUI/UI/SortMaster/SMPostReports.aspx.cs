﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.AppHelpers;

namespace Riskmaster.UI.SortMaster
{
    public partial class SMPostReports : NonFDMBasePageCWS
    {

        bool bReturnStatus = false;
        XElement XmlTemplate = null;
        XmlDocument XmlDoc = new XmlDocument();
        string sCWSresponse = string.Empty;
        private XmlDocument Model = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            string sReturn = string.Empty;

            try
            {
                if (!IsPostBack)
                {
                    String pageName = AppHelper.GetQueryStringValue("smpage");
                    if (pageName != string.Empty)
                    {
                        bReturnStatus = CallCWS("SortMasterAdaptor.GetUsers", XmlTemplate, out sReturn, true, true);
                        Model = new XmlDocument();
                        Model.LoadXml(sReturn);
                        if (Model.SelectSingleNode("//Document/UserDetails/SMPath") != null)
                        {
                            hdnSMPath.Value = Model.SelectSingleNode("//Document/UserDetails/SMPath").InnerText;
                        }
                        switch (pageName)
                        {
                            case "postosha300":
                                txtReprtName.Text = "OSHA 300 Report";
                                hdnReportXMLName.Value = "/oshareport.xml";
                                ViewState["Type"]="1";
                                break;
                            case "postosha301":
                                txtReprtName.Text = "OSHA 301 Report";
                                hdnReportXMLName.Value = "/osha301report.xml";
                                ViewState["Type"] = "4";
                                break;
                            case "postosha300A":
                                txtReprtName.Text = "OSHA 300A Report";
                                hdnReportXMLName.Value = "/osha300Areport.xml";
                                ViewState["Type"] = "3";
                                break;
                            case "postoshasharps":
                                txtReprtName.Text = "OSHA Sharps Log";
                                hdnReportXMLName.Value = "/oshasharpslog.xml";
                                ViewState["Type"] = "5";
                                break;
                            default:
                                break;
                        }
                    }
                    txtReprtDesc.Text = "Not Specified";
                }
                if (hdnReportXMLName.Value != string.Empty)
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "DisableControl", "<script type=\"text/JavaScript\" language=\"javascript\"> $('#tdReprtFileNameID').hide();$('#tdReprtFileName').hide();</script>");
                }
                else
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "EnableControl", "<script type=\"text/JavaScript\" language=\"javascript\"> $('#tdReprtFileNameID').show();$('#tdReprtFileName').show();</script>");
                }
               
                divReport.Attributes.Add("style", "display:''");
                pnlReprtPostSuccessfully.Visible = false;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                XmlTemplate = GetXMLForSave();
                CallCWS("SortMasterAdaptor.PostReports", XmlTemplate, out sCWSresponse, false, false);
                XmlDoc.LoadXml(sCWSresponse);
                string sMsgStatus = XmlDoc.SelectSingleNode("//MsgStatusCd").InnerText;
                if (sMsgStatus == "Success")
                {
                    divReport.Attributes.Add("style", "display:none");
                    pnlReprtPostSuccessfully.Visible = true;
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XElement GetXMLForSave()
        {
            string PrivacyCase = string.Empty;
            string XmlDetails = string.Empty;
            string path = string.Empty;
            string selectedItemValue = string.Empty;

            if (chkPrivacyCase.Checked)
                PrivacyCase = "1";
            else
                PrivacyCase = "0";

            XmlDocument objXML = new XmlDocument();

            if (hdnSMPath.Value != string.Empty && hdnReportXMLName.Value != string.Empty)
            {
                path = hdnSMPath.Value + hdnReportXMLName.Value;
            }
            else
            {
                path = UploadFile.PostedFile.FileName;
            }
            if (path != string.Empty)
            {
                objXML.Load(path);
                XmlNode objXmlnode = objXML.SelectSingleNode("/report");
                if (objXmlnode != null && objXmlnode.Attributes["type"] != null)
                {
                    objXmlnode.Attributes["type"].Value = ViewState["Type"].ToString();
                }
                XmlAttribute xKey = objXML.CreateAttribute("PrivacyCaseFlag");
                xKey.Value = PrivacyCase;
                objXmlnode.Attributes.Append(xKey);
                XmlDetails = objXmlnode.OuterXml;
            }

            if (lstUsers.Items.Count > 0)
            {
                for (int i = 0; i < lstUsers.Items.Count; i++)
                {
                    if (lstUsers.Items[i].Selected)
                    {
                        if (selectedItemValue != string.Empty)
                        {
                            selectedItemValue = selectedItemValue + "," + lstUsers.Items[i].Value;
                        }
                        else
                        {
                            selectedItemValue = lstUsers.Items[i].Value;
                        }
                    }
                }
            }

            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("</Function></Call><Document><Report>");
            sXml = sXml.Append("<ReportName>" + txtReprtName.Text + "</ReportName>");
            sXml = sXml.Append("<ReportDesc>" + txtReprtDesc.Text + "</ReportDesc>");
            sXml = sXml.Append("<ReportXML>" + XmlDetails + "</ReportXML>");
            sXml = sXml.Append("<PrivacyCase>" + PrivacyCase + "</PrivacyCase>");
            sXml = sXml.Append("<UserIDs>" + selectedItemValue + "</UserIDs>");
            sXml = sXml.Append("</Report></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
    }
}