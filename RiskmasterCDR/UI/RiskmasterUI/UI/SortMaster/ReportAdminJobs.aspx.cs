﻿using System;
using System.Drawing;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.SortMaster
{
    public partial class ReportAdminJobs : System.Web.UI.Page
    {
        string sMenuRequestType = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            string sReturn = string.Empty;
            if (Request.QueryString["smpage"] != null)
            {
                sMenuRequestType = Request.QueryString["smpage"];
            }
            try
            {
                if (!IsPostBack)
                {
                    XmlDocument oFDMPageDom = new XmlDocument();
                    string strReturn = string.Empty;
                    XElement oMessageElement = LoadTemplate();
                    CallService(oMessageElement, ref oFDMPageDom, ref strReturn, false);
                    XmlNode objNode = oFDMPageDom.SelectSingleNode("//JobDetails");

                    if (objNode != null)
                    {
                        XmlDataSource source = new XmlDataSource();
                        source.ID = "XmlDataSource1";
                        source.EnableCaching = false;
                        source.Data = objNode.OuterXml;
                        source.XPath = "JobDetails/Job";
                        source.DataBind();
                        RptrReport.DataSource = source;
                        RptrReport.DataBind();
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XElement LoadTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("SortMasterAdaptor.GetJobDetails</Function></Call>");
            sXml = sXml.Append("<Document><ParmaList><JobId></JobId><JobName></JobName><JobDesc></JobDesc>");
            sXml = sXml.Append("<OutputPathURL></OutputPathURL><StartDTTM></StartDTTM><Assigned></Assigned>");
            sXml = sXml.Append("<AssignedDTTM></AssignedDTTM><Complete></Complete><CompleteDTTM></CompleteDTTM>");
            sXml = sXml.Append("<ErrorFlag></ErrorFlag>");
            sXml = sXml.Append("<MenuRequestType>" + sMenuRequestType + "</MenuRequestType>");
            sXml = sXml.Append("</ParmaList></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        protected void RptrReport_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                XPathNavigator nav = ((IXPathNavigable)e.Item.DataItem).CreateNavigator();
                HyperLink hypLnkJobName = e.Item.FindControl("hypLnkJobName") as HyperLink;
                Label lblJobStatus = e.Item.FindControl("lblJobStatus") as Label;
                Label lblJobStartDTTM = e.Item.FindControl("lblJobStartDTTM") as Label;

                if (nav != null)
                {
                    XmlElement xmlElement = nav.UnderlyingObject as XmlElement;

                    string Complete = xmlElement.GetAttribute("Complete");
                    string ErrorFlag = xmlElement.GetAttribute("ErrorFlag");
                    string AssignedDTTM = xmlElement.GetAttribute("AssignedDTTM");
                    string Assigned = xmlElement.GetAttribute("Assigned");
                    string CompleteDTTM = xmlElement.GetAttribute("CompleteDTTM");
                    string StartDTTM = xmlElement.GetAttribute("StartDTTM");
                
                    if (StartDTTM != string.Empty)
                    {
                        lblJobStartDTTM.Text = "Run @ " + StartDTTM;
                    }
                    else 
                    {
                        lblJobStartDTTM.Text = "Run Immediately";
                    }

                    if (Complete.Equals("-1"))
                    {
                        hypLnkJobName.NavigateUrl = xmlElement.GetAttribute("OutputPathURL");
                    }
                    else
                    {
                        hypLnkJobName.ForeColor = Color.Black;
                    }

                    if (Assigned.Equals("0"))
                    {
                        lblJobStatus.Text = "Waiting";
                        lblJobStatus.ForeColor = Color.Gray;
                    }
                    else if (Complete.Equals("0"))
                    {
                        lblJobStatus.Text = string.Format("<b>Running </b>(Started @ {0} {1})", AssignedDTTM, xmlElement.GetAttribute("ProgressMsg"));
                        lblJobStatus.ForeColor = Color.Blue;
                    }
                    else if (ErrorFlag.Equals("-1"))
                    {

                        lblJobStatus.Text = string.Format("<b> Error occured </b> {0})", xmlElement.GetAttribute("ErrorMsg"));
                        lblJobStatus.ForeColor = Color.Red;
                    }
                    else
                    {
                        lblJobStatus.Text = string.Format("<b>Complete</b> (Started @ {0} and Completed @ {1})", AssignedDTTM, CompleteDTTM);
                        lblJobStatus.ForeColor = Color.Green;
                    }
                }
            }
        }

        /// <summary>
        /// This function would call the webservice to get output and return ouput string
        /// </summary>
        /// <param name="oMessageElement"></param>
        /// <param name="oFDMPageDom"></param>
        protected void CallService(XElement oMessageElement, ref XmlDocument oXMLOut, ref string p_sReturn, bool deleteRec)
        {
            string sMsgStatus = string.Empty;
            p_sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
            oXMLOut.LoadXml(p_sReturn);
            XmlNode oInstanceNode = oXMLOut.SelectSingleNode("/ResultMessage/Document");
            XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
            sMsgStatus = oXMLOut.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText;

            if ((sMsgStatus == "Success") && deleteRec)
            {
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string strReturn = string.Empty;
            string JobIds = string.Empty;
            XmlDocument oFDMPageDom = new XmlDocument();
            try
            {
                foreach (RepeaterItem item in RptrReport.Items)
                {
                    CheckBox chkselected = item.FindControl("chkJobName") as CheckBox;
                    HiddenField hdnJobId = item.FindControl("hdnJobId") as HiddenField;
                    if (chkselected.Checked)
                    {
                        if (JobIds == string.Empty)
                            JobIds = hdnJobId.Value;
                        else
                            JobIds = JobIds + "," + hdnJobId.Value;
                    }
                }

                XmlDocument objXML = new XmlDocument();
                StringBuilder sXml = new StringBuilder("<Message>");
                sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
                sXml = sXml.Append("<Call><Function>SortMasterAdaptor.DeleteJobs</Function></Call>");
                sXml = sXml.Append("<Document><Job>");
                sXml = sXml.Append("<JobIds>" + JobIds + "</JobIds>");
                sXml = sXml.Append("</Job></Document></Message>");
                XElement oTemplate = XElement.Parse(sXml.ToString());

                CallService(oTemplate, ref oFDMPageDom, ref strReturn, true);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnEmail_Click(object sender, EventArgs e)
        {
            string JobIds = string.Empty;
            try
            {
                foreach (RepeaterItem item in RptrReport.Items)
                {
                    CheckBox chkselected = item.FindControl("chkJobName") as CheckBox;
                    HiddenField hdnJobId = item.FindControl("hdnJobId") as HiddenField;
                    if (chkselected.Checked)
                    {
                        if (JobIds == string.Empty)
                            JobIds = hdnJobId.Value;
                        else
                            JobIds = JobIds + "," + hdnJobId.Value;
                    }
                }
                Response.Redirect("EmailJob.aspx?JobIds=" + JobIds);

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnArchive_Click(object sender, EventArgs e)
        {
            string JobIds = string.Empty;
            try
            {
                foreach (RepeaterItem item in RptrReport.Items)
                {
                    CheckBox chkselected = item.FindControl("chkJobName") as CheckBox;
                    HiddenField hdnJobId = item.FindControl("hdnJobId") as HiddenField;
                    if (chkselected.Checked)
                    {
                        if (JobIds == string.Empty)
                            JobIds = hdnJobId.Value;
                        else
                            JobIds = JobIds + "," + hdnJobId.Value;
                    }
                }
                Response.Redirect("ArchiveJob.aspx?JobIds=" + JobIds);

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
    }
}
