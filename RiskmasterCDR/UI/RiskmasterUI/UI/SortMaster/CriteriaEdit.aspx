﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CriteriaEdit.aspx.cs" Inherits="Riskmaster.UI.SortMaster.CriteriaEdit" %>

<!DOCTYPE html>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl" TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script type="text/javascript" >

        function Validation() {
            var i = 0;
            var errorMessage = "";
            var headerErrorMessage = "<b><font color=red>There were errors in the submitted report criteria. Please hit the Back button on your browser to correct the mistakes and submit again.<br /><br />" +
                                         "</font></b><font size=2><ul>";
            var footerMessage ="<li>IReport_Engine_CriteriaXMLDom.CriteriaXMLDom [Property Set] : Failed Criteria Check. (See previous error.)</li>" +
                                         "</ul></font>";
           
            //if (document.forms[0].datemethod[0].checked == false &&
            //    document.forms[0].datemethod[1].checked == true &&
            //    document.forms[0].datemethod[2].checked == false) 
            if (document.forms[0].datemethod[0].checked == false &&
                document.forms[0].datemethod[1].checked == true)
            {
                if (document.forms[0].begindate.value == "") {
                    errorMessage += "<li>COSHAManager.COSHAManager.ValidateCriteria : Invalid BeginDate Specified.</li>"
                    i = 1;
                }
                else if (document.forms[0].enddate.value == "") {
                    errorMessage +="<li>COSHAManager.COSHAManager.ValidateCriteria : Invalid EndDate Specified.</li>" 
                    i = 1;
                }
                else if (document.forms[0].begindate.value != "" && document.forms[0].enddate.value != "") {
                    if (document.forms[0].begindate.value.split("/")[2] != document.forms[0].enddate.value.split("/")[2]) {
                        errorMessage += "<li>COSHAManager.COSHAManager.ValidateCriteria : Date Range Spans multiple years (not allowed)</li>"
                        i = 1;
                    }
                }
            }
            else if (document.forms[0].datemethod[0].checked == false &&
                     document.forms[0].datemethod[1].checked == false &&
                     document.forms[0].datemethod[2].checked == true) {
                if (document.getElementById("timescalar").value == "") {
                    errorMessage +="<li>COSHAManager.COSHAManager.ValidateCriteria : Previous Value not Specified for Relative Reporting Date.</li>" 
                    i = 1;
                }
            }
            if (document.forms[0].allentitiesflag[0] != null && document.forms[0].allentitiesflag[1] != null) {
                if (document.forms[0].allentitiesflag[0].checked == false &&
                   document.forms[0].allentitiesflag[1].checked == true) {
                    if (document.forms[0].selectedentities[0].text == "") {
                        errorMessage +="<li>COSHAManager.COSHAManager.ValidateCriteria : No entities selected for reporting.</li>"
                        i = 1;
                    }
                }
            }

            if (document.getElementById("timeunit") != null) {
                if (document.getElementById("timeunit")[document.getElementById("timeunit").selectedIndex].text == "Years") {
                    errorMessage +="<li>COSHAManager.COSHAManager.ValidateCriteria : Date Range Spans multiple years (not allowed)</li>"
                    i = 1;
                }
            }

            if (i == 1 && errorMessage != "") {
                frmData.innerHTML = headerErrorMessage + errorMessage + footerMessage;
            }
            return i;
        }

        function SaveData() {
            var j = -1;

            j = Validation();

            if (j == 0) 
            {
                if (document.forms[0].selectedentities != null) {
                    var i;
                    var selectedentities = "";
                    for (i = 0; i < document.forms[0].selectedentities.length; i++) {
                        if (i != document.forms[0].selectedentities.length - 1) {
                            //rkulavil :RMA-12898 starts 
                            //selectedentities = selectedentities + document.forms[0].selectedentities[i].text + ",";
                            selectedentities = selectedentities + document.forms[0].selectedentities[i].text + "|^|";
                            //selectedentities = selectedentities + document.forms[0].selectedentities[i].value + ";";
                            selectedentities = selectedentities + document.forms[0].selectedentities[i].value + "|~|";
                            //rkulavil :RMA-12898 ends 
                        }
                        else {
                            //rkulavil :RMA-12898 starts 
                            //selectedentities = selectedentities + document.forms[0].selectedentities[i].text + ",";
                            selectedentities = selectedentities + document.forms[0].selectedentities[i].text + "|^|";
                            //rkulavil :RMA-12898 ends 
                            selectedentities = selectedentities + document.forms[0].selectedentities[i].value;
                        }
                    }
                    document.forms[0].Hidden1.value = selectedentities;
                }
                parent.MDISetUnDirty(null, "0", false);
                return true;
            }
        }
    </script>
</head>
<body>
    <form id="frmData" runat="server">
        <div id="div1" runat="server">
            <table>
                <tr>
                    <td colspan="2">
                        <uc1:errorcontrol id="ErrorControl1" runat="server" />
                    </td>
                </tr>
            </table>
            <table>                
                <tr>
                    <td colspan="2" class="ctrlgroup" width="1200">
                        <asp:Label ID="lblReportName" runat="server" Text="Report Name"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <Center> <asp:Button ID="btnsave1" runat="server" Text="Run Report" Class="button" OnClientClick="SaveData()" OnClick="btnsave_Click"/> </Center>
                    </td>
                </tr>
            </table>
        </div>
        <div id="div2" runat="server">
    
        </div>
        <div id="div3" runat="server">
            <Center> <asp:Button ID="btnsave2" runat="server" Text="Run Report" Class="button" OnClientClick="SaveData()" OnClick="btnsave_Click"/> </Center>
            <input id="Hidden1" type="hidden"  runat="server"/>
        </div>
    </form>
</body>
</html>
