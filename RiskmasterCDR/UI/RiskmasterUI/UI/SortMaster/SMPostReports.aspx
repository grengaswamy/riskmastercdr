﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SMPostReports.aspx.cs" Inherits="Riskmaster.UI.SortMaster.SMPostReports" %>

<!DOCTYPE html>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl" TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title></title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
     <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">         { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/WaitDialog.js" ></script> 
    <script type="text/javascript">
        $(document).ready(function () {
            $('input:text:first').focus();
        });
    </script>
    <script type="text/javascript">
        var m_FormSubmitted = false;

        function UserSubmit() {
            if (m_FormSubmitted) {
                alert("You already submitted this form. Please wait for server to respond.");
                return false;
            }

            if ($("#txtReprtName").val() == "") {
                alert("Please enter report name.");
                return false;
            }

            if ($(tdReprtFileNameID).is(":visible")) {
                if (document.forms[0].UploadFile != null) {
                    filePath = document.forms[0].UploadFile.value;
                }
                if (filePath.indexOf('.') == -1) {
                    alert("Please select report file to upload.");
                    return false;
                }
            }

            if ($('#lstUsers :selected').val() == undefined) {
                alert("Please select at least one user allowed to use this report.");
                return false;
            }

            m_FormSubmitted = true;
            return true;
        }

        function ReportSuccess() {
            $('#pnlReprtPostSuccessfully').hide();
            $('#divReport').show();
            return false;
        }
    </script>
</head>
<body onload="parent.pleaseWait('stop');">
    <form id="form1" name="frmData" runat="server">
    <div>
      <table>
        <tr>
            <td colspan="2">
                <uc1:errorcontrol id="ErrorControl1" runat="server" />
            </td>
        </tr>
    </table>
    <asp:Panel ID="pnlReprtPostSuccessfully" runat="server" HorizontalAlign="Center" Visible="false">
		<b><asp:Label ID="lblReprtPostSuccessfully" runat="server" Text="Report Posted Successfully!" Font-Size="Medium" ></asp:Label></b>
       <br /><br />
        <asp:Button ID="btnOK" runat="server" BorderStyle="Solid" Font-Bold="true" Text="    Ok    " OnClientClick="return ReportSuccess();" />
    </asp:Panel>
    <div id="divReport" runat="server">
    <table width="100%">
        <tr><td colspan="2" class="msgheader"><asp:Label ID="lblRprtAdmin" runat="server" Text="Reports Administration" Font-Bold="true"></asp:Label></td></tr>
	    <tr><td colspan="2" class="ctrlgroup"><asp:Label ID="lblPostRprt" runat="server" Text="Post New Report" Font-Bold="true"></asp:Label></td></tr>
        <tr><td colspan="2" class="errortext"><asp:Label ID="lblError" runat="server" Text="" Visible="false"></asp:Label></td></tr>
        <tr>
            <td nowrap="" class="datatd"><b><asp:Label ID="lblReprtName" runat="server" Text="Report Name:" Font-Bold="true"></asp:Label></b></td>
		    <td class="datatd"><asp:TextBox ID="txtReprtName" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td nowrap="" class="datatd"><asp:Label ID="lblReprtDesc" runat="server" Text="Report Description:" Font-Bold="true"></asp:Label></td>
		    <td class="datatd"><asp:TextBox ID="txtReprtDesc" runat="server"></asp:TextBox></td>
	    </tr>
	    <tr>
		    <td nowrap="" class="datatd" runat="server" id="tdReprtFileName">
            <asp:Label ID="lblReprtFileName" runat="server" Text="Report File Name:" Font-Bold="true"></asp:Label>
            
            </td>
		    <td class="datatd" runat="server" id="tdReprtFileNameID">
                <asp:FileUpload ID="UploadFile" runat="server" />
		    </td>
	    </tr>
        <tr>
		    <td nowrap="" class="datatd"><asp:Label ID="lblUsers" runat="server" Text="Users:" Font-Bold="true"></asp:Label></td>
		    <td class="datatd">
                <asp:Label ID="lblNote" runat="server" Text="Note: To select multiple users, hold down Ctrl key while clicking on user." Font-Italic="true"></asp:Label><br />
                <asp:ListBox ID="lstUsers" runat="server" rmxref="/Instance/Document/UserDetails/User" ItemSetRef="/Instance/Document/UserDetails/User" SelectionMode="Multiple"></asp:ListBox>
		    </td>
	    </tr>
        <tr>
		    <td nowrap="" class="datatd"><asp:Label ID="lblPrivacyCase" runat="server" Text="Privacy case:" Font-Bold="true"></asp:Label></td>
		    <td class="datatd"><asp:CheckBox ID="chkPrivacyCase" runat="server" /></td>
	    </tr>
    </table>
    <p class="small">
        Use this page to add a new report to the Available Reports list.        Be sure to give access to at least one user in the "Users" list.
        <br>
        *** For information on extended reporting features and report troubleshooting please see the <a href="SMFAQs.aspx">Frequently Asked Questions (FAQ)</a>.
    </p>
        <asp:Button ID="btnSave" runat="server" Text="Save" BorderStyle="Solid" Font-Bold="true" OnClick="btnSave_Click" OnClientClick="return UserSubmit();" />
    </div>
    </div >
        <asp:HiddenField ID="hdnReportXMLName" runat="server" />
        <asp:HiddenField ID="hdnSMPath" runat="server" />
    </form>
</body>
</html>
