﻿using System;
using System.Collections;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using Riskmaster.Common;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Xsl;
using System.IO;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.SortMaster
{
    public partial class CriteriaEdit : Page
    {
        static XmlDocument xmlRep = new XmlDocument();
        static String ReportValue = String.Empty;
        static String ScheduleType = String.Empty;
        static String TaskTypeText = String.Empty;
        static String TaskType = String.Empty;
        static String UserID = String.Empty;
        static String WizardPath = String.Empty;
        string sMenuRequestType = string.Empty;

        private XElement GetMessageTemplate(String ReportValue)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization></Authorization>");
            sXml = sXml.Append("<Call><Function>SortMasterAdaptor.GetXMLByReportID</Function></Call>");
            sXml = sXml.Append("<Document><ParmaList><ReportID>" + ReportValue + "</ReportID><MenuRequestType>" + sMenuRequestType + "</MenuRequestType></ParmaList></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            XElement XmlTemplate = null;
            String sReturn = String.Empty;
            StringBuilder sHTML = new StringBuilder();
            XmlNodeList pList;
            XmlElement pRoot = null;
            String ReportXML = String.Empty;
            String sTemp;

            if (Request.QueryString["smpage"] != null)
            {
                sMenuRequestType = Request.QueryString["smpage"];
            }
            if (!(IsPostBack))
            {
                try
                {
                    if (Request.QueryString["ReportId"] != null)
                    {
                        ReportValue = Request.QueryString["ReportId"];
                    }
                    if (Request.QueryString["ReportName"] != null)
                    {
                        lblReportName.Text = Request.QueryString["ReportName"];
                    }

                    XmlTemplate = GetMessageTemplate(ReportValue);
                    // bReturnStatus = CallCWS("SortMasterAdaptor.GetXMLByReportID", XmlTemplate, out sReturn, true, true);
                    sReturn = AppHelper.CallCWSService(XmlTemplate.ToString());
                    sReturn = sReturn.Replace("&lt;", "<");
                    sReturn = sReturn.Replace("&gt;", ">");
                    sReturn = sReturn.Replace('\"', '"');

                    xmlRep.LoadXml(sReturn);
                    pRoot = xmlRep.DocumentElement;

                    WizardPath = xmlRep.SelectSingleNode("/ResultMessage/Document/ReportXML/SMPath").InnerText;                

                    sHTML = sHTML.Append("<link rel=stylesheet href=../../Content/rmnet.css type=text/css />");
                    sHTML = sHTML.Append("<script language=JavaScript SRC=../../Scripts/form.js></script>");
                    sHTML = sHTML.Append("<script language=JavaScript src=../../Scripts/oshareportdata.js></script>");

                    sHTML = sHTML.Append("<table border=0 cellspacing=4 cellpadding=2>");
                    sHTML = sHTML.Append("<tr><td colspan=2 class=ctrlgroup width=1200>Report Info</td></tr>");
                    pList = xmlRep.SelectNodes("/report/reporttitle");

                    sHTML = sHTML.Append("<tr>");
                    sHTML = sHTML.Append("<td>Report Title</td>");
                    sHTML = sHTML.Append("<td><input name=reporttitle type=text size=80 value='");
                    if (pList.Count > 0)
                        sHTML = sHTML.Append(pList.Item(0).InnerText);
                    sHTML = sHTML.Append("'></input></td>");
                    sHTML = sHTML.Append("</tr>");

                    pList = xmlRep.SelectNodes("/report/reporttitle/subtitle");
                    sHTML = sHTML.Append("<tr>");
                    sHTML = sHTML.Append("<td>Report Subtitle</td>");
                    sHTML = sHTML.Append("<td><input name=reportsubtitle type=text size=80 value='");
                    if (pList.Count > 0)
                        sHTML = sHTML.Append(pList.Item(0).InnerText);
                    sHTML = sHTML.Append("'></input></td>");
                    sHTML = sHTML.Append("</tr>");

                    sTemp = pRoot.GetAttribute("evaldate");

                    if (sTemp != "")
                    {
                        DateTime TempDate = DateTime.ParseExact(sTemp, "yyyyMMdd", null);
                        sHTML = sHTML.Append("<tr>");
                        sHTML = sHTML.Append("<td>Report As-Of Date</td>");
                        sHTML = sHTML.Append("<td><input name=asofdate type=text size=15 value='");
                        sHTML = sHTML.Append(TempDate.ToShortDateString());
                        sHTML = sHTML.Append("'></input></td>");
                        sHTML = sHTML.Append("</tr>");
                    }
                    sHTML = sHTML.Append("<tr>");
                    sHTML = sHTML.Append("<td valign='top'>Optional Output Name</td>");
                    sHTML = sHTML.Append("<td><input name=outputfilename type=text size=20 value=''></input><br /><i>(Note: OutputFileName=[OutputName]+[UniqueId]+[.Extension])</i></td>");
                    sHTML = sHTML.Append("</tr>");

                    sHTML = sHTML.Append("<tr>");
                    sHTML = sHTML.Append("<td>Output Type</td>");
                    sHTML = sHTML.Append("<td>");
                    sHTML = sHTML.Append("<select name=outtype height=1>");

                    if (xmlRep.SelectSingleNode("/ResultMessage/Document/ReportXML/RptXML/report").Attributes[0].Value == "1")
                        sHTML = sHTML.Append("<option value=pdf>PDF</option>");
                    else
                    {
                        sHTML = sHTML.Append("<option value=pdf>PDF</option>");
                        sHTML = sHTML.Append("<option value=html>HTML</option>");
                        sHTML = sHTML.Append("<option value=xml>XML</option>");
                        sHTML = sHTML.Append("<option value=csv>CSV</option>");
                        sHTML = sHTML.Append("<option value=xls>XLS</option>");
                    }
                    sHTML = sHTML.Append("<option></option>");
                    sHTML = sHTML.Append("</select>");
                    sHTML = sHTML.Append("</td>");
                    sHTML = sHTML.Append("</tr>");

                    sHTML = sHTML.Append("<tr>");
                    sHTML = sHTML.Append("<td>Run Report</td>");
                    sHTML = sHTML.Append("<td>");
                    sHTML = sHTML.Append("<select name=execreport height=1>");
                    sHTML = sHTML.Append("<option value=1>Immediately</option>");
                    sHTML = sHTML.Append("<option value=2>At Specific Date/Time -></option>");
                    sHTML = sHTML.Append("</select>");
                    sHTML = sHTML.Append("&nbsp; Start Date:");
                    sHTML = sHTML.Append("<input type=text name=startdate value='' onblur=dateLostFocus(this.name) size=10/>");
                    sHTML = sHTML.Append("&nbsp; Start Time:");
                    sHTML = sHTML.Append("<input type=text name=starttime value='' onblur=timeLostFocus(this.name) size=10/>");
                    sHTML = sHTML.Append("</td>");
                    sHTML = sHTML.Append("</tr>");

                    sHTML = sHTML.Append("<tr>");
                    sHTML = sHTML.Append("<td>Notification Type</td>");
                    sHTML = sHTML.Append("<td>");
                    sHTML = sHTML.Append("<select name=notifytype height=1>");
                    sHTML = sHTML.Append("<option value=notifylink>Send Email with Link to Report Output</option>");
                    sHTML = sHTML.Append("<option value=notifyembed>Send Email with Report Output Attached</option>");
                    sHTML = sHTML.Append("<option value=notifyonly>Send Email Only(no report output attached or linked)</option>");
                    sHTML = sHTML.Append("<option value=none>None</option>");
                    sHTML = sHTML.Append("</select>");
                    sHTML = sHTML.Append("</td>");
                    sHTML = sHTML.Append("</tr>");

                    sHTML = sHTML.Append("<tr>");
                    sHTML = sHTML.Append("<td>Email To</td>");
                    sHTML = sHTML.Append("<td><textarea cols=30 rows=2 name=notifyemail>" + xmlRep.SelectSingleNode("/ResultMessage/Document/ReportXML/EmailID").InnerText + "</textarea>&nbsp;&nbsp;<i>(Note: separate multiple email addresses with semicolons)</i>");
                    sHTML = sHTML.Append("</td>");
                    sHTML = sHTML.Append("</tr>");

                    sHTML = sHTML.Append("<tr>");
                    sHTML = sHTML.Append("<td>Optional Email Message</td>");
                    sHTML = sHTML.Append("<td><textarea cols=30 rows=2 name=notifymsg></textarea>");
                    sHTML = sHTML.Append("</td>");
                    sHTML = sHTML.Append("</tr");

                    sHTML = sHTML.Append("</table><br />");


                    String GenerateHTML = GenerateCriteriaHTML(sReturn);
                    sHTML = sHTML.Append(GenerateHTML);

                    sHTML = sHTML.Replace("<?xml version=\"1.0\" encoding=\"utf-8\"?>", "");
                    sHTML = sHTML.Replace("<script language=\"JavaScript\" SRC=\"oshareport.js\" />", "");

                    div2.InnerHtml = sHTML.ToString();
                }
                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                }
            }
        }

        protected String GenerateCriteriaHTML(String sReturn)
        {
            String GenerateOSHAHTML = String.Empty;
            String lType = String.Empty;

            if (Request.QueryString["sSubTaskValue"] != null)
            {
                ReportValue = Request.QueryString["sSubTaskValue"];
            }
            lType = GetReportType(ReportValue, sReturn);

            switch (lType)
            {
                case "1":
                    GenerateOSHAHTML = GenerateOSHACriteriaHTML(lType);
                    break;
                case "3":
                    GenerateOSHAHTML = GenerateOSHACriteriaHTML(lType);
                    break;
                case "4":
                    GenerateOSHAHTML = GenerateOSHACriteriaHTML(lType);
                    break;
                case "5":
                    GenerateOSHAHTML = GenerateOSHACriteriaHTML(lType);
                    break;
                default:
                    GenerateOSHAHTML = "Your report Criteria HTML generating function (call) here";
                    break;
            }
            return GenerateOSHAHTML;
        }

        public String GetReportType(String ReportValue, String sReturn)
        {
            XmlDocument xmlobj = new XmlDocument();
            XmlElement objNode;
            String Type = String.Empty;

            xmlobj.LoadXml(sReturn);

            objNode = (XmlElement)xmlobj.SelectSingleNode("/ResultMessage/Document/ReportXML/RptXML/report");

            if (objNode != null)
                Type = objNode.GetAttribute("type");

            return Type;
        }

        protected String GenerateOSHACriteriaHTML(string lType)
        {
            XmlDocument objXml = new XmlDocument();
            XmlDocument objResult = new XmlDocument();
            XmlElement objopt;
            XmlElement objNode;
            String OSHAXML = String.Empty;
            String XmlData = String.Empty;
            long iSelected = 0;
            long CurrentYear = Convert.ToInt64(DateTime.Now.ToString("yyyy"));
            int i;

            XmlData = xmlRep.SelectSingleNode("/ResultMessage/Document/ReportXML/RptXML").InnerXml;

            XslCompiledTransform transform = new XslCompiledTransform();
            transform.Load(WizardPath + "\\oshareportdata.xsl");
            StringWriter results = new StringWriter();
            using (XmlReader reader = XmlReader.Create(new StringReader(XmlData)))
            {
                transform.Transform(reader, null, results);
            }
            objResult.LoadXml(results.ToString());

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'byoshaestablishmentflag'][@value='false']");
            if (objNode != null)
                objNode.SetAttribute("onClick", "byEstablishmentFlagChanged()");

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'byoshaestablishmentflag'][@value='true']");
            if (objNode != null)
                objNode.SetAttribute("onClick", "byEstablishmentFlagChanged()");

            objNode = (XmlElement)objResult.SelectSingleNode("//select[@name = 'reportlevel']");
            if (objNode != null)
                objNode.SetAttribute("onClick", "reportlevelChanged()");

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'selectedentitiesbtn']");
            if (objNode != null)
                objNode.SetAttribute("onClick", "selectReportTarget()");

            objNode = (XmlElement)objResult.SelectSingleNode("//select[@name = 'yearofreport']");
            if (objNode != null)
            {
                i = 2002;

                if (objNode.GetAttribute("codeid") != "")
                    iSelected = Convert.ToInt64(objNode.GetAttribute("codeid"));

                if (iSelected == 0 || i > iSelected)
                    iSelected = CurrentYear;

                for (i = 2002; i <= CurrentYear; i++)
                {
                    objopt = objResult.CreateElement("option");
                    objopt.SetAttribute("value", i.ToString());

                    if (i == iSelected)
                        objopt.SetAttribute("selected", "");
                    objopt.InnerText = i.ToString();
                    objNode.AppendChild(objopt);
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'byoshaestablishmentflag'][@value='false']");
            if (objNode != null)
                objNode.SetAttribute("checked", "");

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'reporttype']");
            if (objNode != null)
                objNode.SetAttribute("value", ReportValue);

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'allentitiesflag'][@value = 'true']");
            if (objNode != null)
                objNode.SetAttribute("checked", "");

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'datemethod'][@value = 'useyear']");
            if (objNode != null)
                objNode.SetAttribute("checked", "");

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'printsofterrlog']");
            if (objNode != null)
                objNode.SetAttribute("checked", "");

            switch (lType)
            {
                case "3":
                case "4":
                    objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'printoshadescflag']");
                    if (objNode != null)
                        objNode.SetAttribute("checked", "");

                    objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'primarylocationflag']");
                    if (objNode != null)
                        objNode.SetAttribute("checked", "");
                    break;
                case "5":
                    objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'primarylocationflag']");
                    if (objNode != null)
                        objNode.SetAttribute("checked", "");
                    break;
                default:
                    break;
            }

            OSHAXML = objResult.InnerXml;

            return OSHAXML;
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {            
            try
            {
                SavingData();
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void SavingData()
        {
            StringBuilder SaveXml = new StringBuilder();
            String OutputPathURL;
            String OutputPath;
            DateTime DateTime = new DateTime();
            String ReturnXml = String.Empty;
            String OSHAType = String.Empty;
            XmlDocument oXMLOut = null;
            string sMsgStatus = string.Empty;

            // akaushik5 CHanged for MITS 38389 Starts
            //OutputPathURL = xmlRep.SelectSingleNode("/ResultMessage/Document/ReportXML/SMNetServer").InnerText + "/RiskmasterUI/UI/SortMaster/SMRepView.aspx?DPT=0";
            OutputPathURL = "/RiskmasterUI/UI/SortMaster/SMRepView.aspx?DPT=0";
            // akaushik5 Changed for MITS 38389 Ends
            OutputPath = xmlRep.SelectSingleNode("/ResultMessage/Document/ReportXML/SMDataPath").InnerText;
            OSHAType = xmlRep.SelectSingleNode("/ResultMessage/Document/ReportXML/RptXML/report").Attributes[0].Value;

            if (OSHAType == "1" || OSHAType == "3" || OSHAType == "4" || OSHAType == "5")
                PostReports();

            if (Request.Form["startdate"] != "")
                DateTime = Convert.ToDateTime(Request.Form["startdate"]);

            SaveXml.Append("<Message>");
            SaveXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            SaveXml.Append("<Call>");
            SaveXml.Append("<Function>SortMasterAdaptor.RunReport</Function>");
            SaveXml.Append("</Call>");
            SaveXml.Append("<Document>");
            SaveXml.Append("<Details>");
            SaveXml.Append("<JobPriority>" + 0 + "</JobPriority>");

            if (Request.Form["execreport"] == "1")
            {
                SaveXml.Append("<Date>"+ DateTime.ToShortDateString() +"</Date>");
                SaveXml.Append("<Time></Time>");
            }
            else if (Request.Form["execreport"] == "2")
            {
                SaveXml.Append("<Date>" + DateTime.ToShortDateString() + "</Date>");
                SaveXml.Append("<Time>" + Request.Form["starttime"] + "</Time>");
            }

            SaveXml.Append("<ReportName>" + lblReportName.Text + "</ReportName>");
            SaveXml.Append("<OutputType>" + Request.Form["outtype"] + "</OutputType>");
            SaveXml.Append("<OutputPath>" + OutputPath + "</OutputPath>");
            SaveXml.Append("<OutputPathURL>" + OutputPathURL + "</OutputPathURL>");
            SaveXml.Append("<OutputOptions></OutputOptions>");
            SaveXml.Append("<ReportXML>" + xmlRep.SelectSingleNode("/ResultMessage/Document/ReportXML/RptXML").InnerXml + "</ReportXML>");
            SaveXml.Append("<NotificationType>" + Request.Form["notifytype"] + "</NotificationType>");
            SaveXml.Append("<NotifyEmail>" + Request.Form["notifyemail"] + "</NotifyEmail>");
            SaveXml.Append("<NotifyMsg>" + Request.Form["notifymsg"] + "</NotifyMsg>");

            SaveXml.Append("</Details>");
            SaveXml.Append("</Document>");
            SaveXml.Append("</Message>");

            ReturnXml = AppHelper.CallCWSService(SaveXml.ToString());
            oXMLOut = new XmlDocument();
            oXMLOut.LoadXml(ReturnXml);
            XmlNode oInstanceNode = oXMLOut.SelectSingleNode("/ResultMessage/Document");
            XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
            sMsgStatus = oXMLOut.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText;

            if (sMsgStatus == "Success")
            {
                Response.Redirect("~/UI/SortMaster/JobQueue.aspx");
            }
        }

        protected void PostReports()
        {
            XmlNodeList objNodeList1 = null;
            XmlNodeList objNodeList2 = null;
            XmlNodeList objNodeList3 = null;
            XmlElement objElement = null;
            XmlElement objElement1 = null;
            XmlNode objNode = null;
            String sType = String.Empty;
            String sName = String.Empty;
            String sValue = String.Empty;
            String sCodeID = String.Empty;
            String[] SaveData1 = new string[0];
            //rkulavil :RMA-12898 starts 
            string[] sSeperator = { "|^|" };
            string[] sSplitSeperator = { "|~|" };
            //rkulavil :RMA-12898 ends 
            if(Hidden1.Value != "")
                //rkulavil :RMA-12898 starts 
                //SaveData1 = Hidden1.Value.Split(';');
                SaveData1 = Hidden1.Value.Split(sSplitSeperator,StringSplitOptions.RemoveEmptyEntries);
                //rkulavil :RMA-12898 ends 

            objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ReportXML/RptXML/report");
            if (objElement != null)
                objElement.SetAttribute("outputfilename", Request.Form["outputfilename"]);

            if (xmlRep.SelectSingleNode("/ResultMessage/Document/ReportXML/RptXML/report/form").HasChildNodes)
            {
                objNodeList1 = xmlRep.SelectSingleNode("/ResultMessage/Document/ReportXML/RptXML/report/form").ChildNodes;

                //foreach (XmlElement objElement1 in objNodeList1)
                for (int i = 0; i < objNodeList1.Count; i++)
                {
                    if (objNodeList1[i].NodeType == XmlNodeType.Element)
                    {
                        objElement1 = (XmlElement)objNodeList1[i];

                        if (objElement1.HasChildNodes)
                        {
                            objNodeList2 = objElement1.ChildNodes;
                            foreach (XmlElement objElement2 in objNodeList2)
                            {
                                sType = objElement2.GetAttribute("type");
                                sName = objElement2.GetAttribute("name");
                                sValue = objElement2.GetAttribute("value");

                                for (int k = 0; k < objElement2.Attributes.Count; k++)
                                {
                                    if (objElement2.Attributes[k].Name == "checked")
                                        objElement2.RemoveAttribute("checked");
                                }

                                switch (sType)
                                {
                                    case "radio":
                                        if (Request.Form[sName] == sValue)
                                            objElement2.SetAttribute("checked", "");
                                        break;
                                    case "checkbox":
                                        if (Request.Form[sName] != null)
                                            objElement2.SetAttribute("checked", "");
                                        break;
                                    case "combobox":
                                        objNodeList3 = objElement2.ChildNodes;
                                        if (objNodeList3.Count != 0)
                                        {
                                            foreach (XmlElement objElement3 in objNodeList3)
                                            {
                                                if (objElement3.InnerXml == Request.Form[sName])
                                                {
                                                    sCodeID = objElement3.GetAttribute("value");
                                                    objElement2.SetAttribute("codeid", sCodeID);
                                                    objElement3.SetAttribute("selected", "");
                                                    break;
                                                }
                                            }
                                        }
                                        else
                                            objElement2.SetAttribute("codeid", Request.Form[sName]);
                                        break;
                                    case "date":
                                        if (Request.Form[sName] != "")
                                        {
                                            DateTime dt = Convert.ToDateTime(Request.Form[sName]);
                                            objElement2.SetAttribute("value", dt.ToString("yyyyMMdd"));
                                        }
                                        break;
                                    case "entitylist":
                                        if (SaveData1 != null)
                                        {
                                            for (int j = 0; j < SaveData1.Length; j++)
                                            {
                                                if (SaveData1[j] != "")
                                                {
                                                    objElement = xmlRep.CreateElement("option");
                                                    //rkulavil :RMA-12898 starts 
                                                    //objElement.SetAttribute("value", SaveData1[j].Split(',')[1]);
                                                    objElement.SetAttribute("value", SaveData1[j].Split(sSeperator, StringSplitOptions.RemoveEmptyEntries)[1]);
                                                    //objElement.InnerText = SaveData1[j].Split(',')[0];
                                                    objElement.InnerText = SaveData1[j].Split(sSeperator, StringSplitOptions.RemoveEmptyEntries)[0];
                                                    //rkulavil :RMA-12898 ends 
                                                    objNode = objElement2;
                                                    objNode.AppendChild(objElement);
                                                }
                                            }
                                        }
                                        break;
                                    case "numeric":
									case "text":
                                        objElement2.SetAttribute("value", Request.Form[sName]);
                                        break;
                                }
                            }
                        }
                    }
                }
            }
        }        
    }
}