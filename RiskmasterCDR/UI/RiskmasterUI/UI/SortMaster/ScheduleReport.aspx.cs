﻿using System;
using System.Collections;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using Riskmaster.Common;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Xsl;
using System.IO;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.UI.SortMaster
{
    public partial class ScheduleReport : Page
    {
        static XmlDocument xmlRep = new XmlDocument();
        static String ReportValue = String.Empty;
        static String ScheduleType = String.Empty;
        static String TaskTypeText = String.Empty;
        static String TaskType = String.Empty;
        static String UserID = String.Empty;
        static String WizardPath = String.Empty;

        private XElement GetMessageTemplate(String ReportValue)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization></Authorization>");
            sXml = sXml.Append("<Call><Function>SortMasterAdaptor.GetXMLByReportID</Function></Call>");
            sXml = sXml.Append("<Document><ReportID>" + ReportValue + "</ReportID></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            XElement XmlTemplate = null;
            String sReturn = String.Empty;
            StringBuilder sHTML = new StringBuilder();
            XmlNodeList pList;
            XmlElement pRoot = null;
            String ReportXML = String.Empty;
            String sTemp;

            if (!(IsPostBack))
            {
                try
                {
                    if (Request.QueryString["ScheduleTypetxt"] != null)
                    {
                        lblSchdlTyp.Text = Request.QueryString["ScheduleTypetxt"];
                    }
                    if (Request.QueryString["sSubTaskNameText"] != null)
                    {
                        lblReprtName.Text = Request.QueryString["sSubTaskNameText"];
                    }
                    if (Request.QueryString["sSubTaskValue"] != null)
                    {
                        ReportValue = Request.QueryString["sSubTaskValue"];
                    }
                    if (Request.QueryString["ScheduleType"] != null)
                    {
                        ScheduleType = Request.QueryString["ScheduleType"];
                    }
                    if (Request.QueryString["TaskTypetxt"] != null)
                    {
                        TaskTypeText = Request.QueryString["TaskTypetxt"];
                    }
                    if (Request.QueryString["TaskType"] != null)
                    {
                        TaskType = Request.QueryString["TaskType"];
                    }
                    XmlTemplate = GetMessageTemplate(ReportValue);
                    // bReturnStatus = CallCWS("SortMasterAdaptor.GetXMLByReportID", XmlTemplate, out sReturn, true, true);
                    sReturn = AppHelper.CallCWSService(XmlTemplate.ToString());
                    sReturn = sReturn.Replace("&lt;", "<");
                    sReturn = sReturn.Replace("&gt;", ">");
                    sReturn = sReturn.Replace('\"', '"');

                    xmlRep.LoadXml(sReturn);
                    pRoot = xmlRep.DocumentElement;

                    WizardPath = xmlRep.SelectSingleNode("/ResultMessage/Document/ReportXML/SMPath").InnerText;

                    sHTML = sHTML.Append("<link rel=stylesheet href=../../Content/rmnet.css type=text/css />");
                    sHTML = sHTML.Append("<script language=JavaScript SRC=../../Scripts/form.js></script>");
                    sHTML = sHTML.Append("<script language=JavaScript src=../../Scripts/oshareportdata.js></script>");

                    sHTML = sHTML.Append("<table border=0 cellspacing=4 cellpadding=2>");
                    sHTML = sHTML.Append("<tr><td class=ctrlgroup colspan=2 width=1200>Enter Start Date and Time:</td></tr>");

                    sHTML = sHTML.Append("<tr>");
                    sHTML = sHTML.Append("<td>Start Date:</td>");
                    sHTML = sHTML.Append("<td><input type=text name=startdate value='' onblur=dateLostFocus(this.name) size=10/></td>");
                    sHTML = sHTML.Append("</tr>");

                    sHTML = sHTML.Append("<tr>");
                    sHTML = sHTML.Append("<td><b>Start Time:</b></td>");
                    sHTML = sHTML.Append("<td><input type=text name=starttime value='' onblur=timeLostFocus(this.name) size=10/></td>");
                    sHTML = sHTML.Append("</tr>");

                    if (ScheduleType == "7")
                    {
                        sHTML = sHTML.Append("<tr>");
                        sHTML = sHTML.Append("<td class=ctrlgroup colspan=2 width=1200>Select the days you would like to Run Report(s) on:</td>");
                        sHTML = sHTML.Append("</tr>");

                        sHTML = sHTML.Append("<tr>");
                        sHTML = sHTML.Append("<td/>");
                        sHTML = sHTML.Append("<td><input type=checkbox name=mon_run checked=1 value=1 /> Monday<br />");
                        sHTML = sHTML.Append("<input type=checkbox name=tue_run checked=1 value=1 /> Tuesday<br />");
                        sHTML = sHTML.Append("<input type=checkbox name=wed_run checked=1 value=1 /> Wednesday<br />");
                        sHTML = sHTML.Append("<input type=checkbox name=thu_run checked=1 value=1 /> Thursday<br />");
                        sHTML = sHTML.Append("<input type=checkbox name=fri_run checked=1 value=1 /> Friday<br />");
                        sHTML = sHTML.Append("<input type=checkbox name=sat_run value=1 /> Saturday<br />");
                        sHTML = sHTML.Append("<input type=checkbox name=sun_run value=1 /> Sunday<br />");
                        sHTML = sHTML.Append("</td>");
                        sHTML = sHTML.Append("</tr>");
                    }
                    else
                    {
                        sHTML = sHTML.Append("<tr>");
                        sHTML = sHTML.Append("<td><b>Run on Day:</b></td>");
                        sHTML = sHTML.Append("<td><input type=text name=startday value=1 onblur=numLostFocus(this) size=2 maxlength=2/></td>");
                        sHTML = sHTML.Append("</tr>");

                        sHTML = sHTML.Append("<tr>");
                        sHTML = sHTML.Append("<td class=ctrlgroup colspan=2 width=1200>Select the Month(s) you would like to Run Report(s) on:</td>");
                        sHTML = sHTML.Append("</tr>");

                        sHTML = sHTML.Append("<tr>");
                        sHTML = sHTML.Append("<td/>");
                        sHTML = sHTML.Append("<td><input type=checkbox name=jan_run checked=1 value=1 /> January<br />");
                        sHTML = sHTML.Append("<input type=checkbox name=feb_run checked=1 value=1 /> February<br />");
                        sHTML = sHTML.Append("<input type=checkbox name=mar_run checked=1 value=1 /> March<br />");
                        sHTML = sHTML.Append("<input type=checkbox name=apr_run checked=1 value=1 /> April<br />");
                        sHTML = sHTML.Append("<input type=checkbox name=may_run checked=1 value=1 /> May<br />");
                        sHTML = sHTML.Append("<input type=checkbox name=jun_run checked=1 value=1 /> June<br />");
                        sHTML = sHTML.Append("<input type=checkbox name=jul_run checked=1 value=1 /> July<br />");
                        sHTML = sHTML.Append("<input type=checkbox name=aug_run checked=1 value=1 /> August<br />");
                        sHTML = sHTML.Append("<input type=checkbox name=sep_run checked=1 value=1 /> September<br />");
                        sHTML = sHTML.Append("<input type=checkbox name=oct_run checked=1 value=1 /> October<br />");
                        sHTML = sHTML.Append("<input type=checkbox name=nov_run checked=1 value=1 /> November<br />");
                        sHTML = sHTML.Append("<input type=checkbox name=dec_run checked=1 value=1 /> December<br />");
                        sHTML = sHTML.Append("</td>");
                        sHTML = sHTML.Append("</tr>");
                    }

                    sHTML = sHTML.Append("<tr><td colspan=2 class=ctrlgroup width=1200>Report Info</td></tr>");
                    pList = xmlRep.SelectNodes("/report/reporttitle");

                    sHTML = sHTML.Append("<tr>");
                    sHTML = sHTML.Append("<td>Report Title</td>");
                    sHTML = sHTML.Append("<td><input name=reporttitle type=text size=80 value='");
                    if (pList.Count > 0)
                        sHTML = sHTML.Append(pList.Item(0).InnerText);
                    sHTML = sHTML.Append("'></input></td>");
                    sHTML = sHTML.Append("</tr>");

                    pList = xmlRep.SelectNodes("/report/reporttitle/subtitle");
                    sHTML = sHTML.Append("<tr>");
                    sHTML = sHTML.Append("<td>Report Subtitle</td>");
                    sHTML = sHTML.Append("<td><input name=reportsubtitle type=text size=80 value='");
                    if (pList.Count > 0)
                        sHTML = sHTML.Append(pList.Item(0).InnerText);
                    sHTML = sHTML.Append("'></input></td>");
                    sHTML = sHTML.Append("</tr>");

                    sTemp = pRoot.GetAttribute("evaldate");

                    if (sTemp != "")
                    {
                        DateTime TempDate = DateTime.ParseExact(sTemp, "yyyyMMdd", null);
                        sHTML = sHTML.Append("<tr>");
                        sHTML = sHTML.Append("<td>Report As-Of Date</td>");
                        sHTML = sHTML.Append("<td><input name=asofdate type=text size=15 value='");
                        sHTML = sHTML.Append(TempDate.ToShortDateString());
                        sHTML = sHTML.Append("'></input></td>");
                        sHTML = sHTML.Append("</tr>");
                    }
                    sHTML = sHTML.Append("<tr>");
                    sHTML = sHTML.Append("<td valign='top'>Optional Output Name</td>");
                    sHTML = sHTML.Append("<td><input name=outputfilename type=text size=20 value=''></input><br /><i>(Note: OutputFileName=[OutputName]+[UniqueId]+[.Extension])</i></td>");
                    sHTML = sHTML.Append("</tr>");

                    sHTML = sHTML.Append("<tr>");
                    sHTML = sHTML.Append("<td>Output Type</td>");
                    sHTML = sHTML.Append("<td>");
                    sHTML = sHTML.Append("<select name=outtype height=1>");

                    if (xmlRep.SelectSingleNode("/ResultMessage/Document/ReportXML/RptXML/report").Attributes[0].Value == "1")
                        sHTML = sHTML.Append("<option value=pdf>PDF</option>");
                    else
                    {
                        sHTML = sHTML.Append("<option value=pdf>PDF</option>");
                        sHTML = sHTML.Append("<option value=html>HTML</option>");
                        sHTML = sHTML.Append("<option value=xml>XML</option>");
                        sHTML = sHTML.Append("<option value=csv>CSV</option>");
                        sHTML = sHTML.Append("<option value=xls>XLS</option>");
                    }
                    sHTML = sHTML.Append("<option></option>");
                    sHTML = sHTML.Append("</select>");
                    sHTML = sHTML.Append("</td>");
                    sHTML = sHTML.Append("</tr>");

                    sHTML = sHTML.Append("<tr>");
                    sHTML = sHTML.Append("<td>Notification Type</td>");
                    sHTML = sHTML.Append("<td>");
                    sHTML = sHTML.Append("<select name=notifytype height=1>");
                    sHTML = sHTML.Append("<option value=notifylink>Send Email with Link to Report Output</option>");
                    sHTML = sHTML.Append("<option value=notifyembed>Send Email with Report Output Attached</option>");
                    sHTML = sHTML.Append("<option value=notifyonly>Send Email Only(no report output attached or linked)</option>");
                    sHTML = sHTML.Append("<option value=none>None</option>");
                    sHTML = sHTML.Append("</select>");
                    sHTML = sHTML.Append("</td>");
                    sHTML = sHTML.Append("</tr>");

                    sHTML = sHTML.Append("<tr>");
                    sHTML = sHTML.Append("<td>Email To</td>");
                    sHTML = sHTML.Append("<td><textarea cols=30 rows=2 name=notifyemail>" + xmlRep.SelectSingleNode("/ResultMessage/Document/ReportXML/EmailID").InnerText + "</textarea>&nbsp;&nbsp;<i>(Note: separate multiple email addresses with semicolons)</i>");
                    sHTML = sHTML.Append("</td>");
                    sHTML = sHTML.Append("</tr>");

                    sHTML = sHTML.Append("<tr>");
                    sHTML = sHTML.Append("<td>Optional Email Message</td>");
                    sHTML = sHTML.Append("<td><textarea cols=30 rows=2 name=notifymsg></textarea>");
                    sHTML = sHTML.Append("</td>");
                    sHTML = sHTML.Append("</tr");

                    sHTML = sHTML.Append("</table><br />");


                    String GenerateHTML = GenerateCriteriaHTML(sReturn);
                    sHTML = sHTML.Append(GenerateHTML);

                    sHTML = sHTML.Replace("<?xml version=\"1.0\" encoding=\"utf-8\"?>", "");
                    sHTML = sHTML.Replace("<script language=\"JavaScript\" SRC=\"oshareport.js\" />", "");

                    div2.InnerHtml = sHTML.ToString();
                }
                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                }
            }
        }

        protected String GenerateCriteriaHTML(String sReturn)
        {
            String GenerateOSHAHTML = String.Empty;
            String lType = String.Empty;

            if (Request.QueryString["sSubTaskValue"] != null)
            {
                ReportValue = Request.QueryString["sSubTaskValue"];
            }
            lType = GetReportType(ReportValue, sReturn);

            switch (lType)
            {
                case "1":
                    GenerateOSHAHTML = GenerateOSHACriteriaHTML(lType);
                    break;
                case "3":
                    GenerateOSHAHTML = GenerateOSHACriteriaHTML(lType);
                    break;
                case "4":
                    GenerateOSHAHTML = GenerateOSHACriteriaHTML(lType);
                    break;
                case "5":
                    GenerateOSHAHTML = GenerateOSHACriteriaHTML(lType);
                    break;
                default:
                    GenerateOSHAHTML = "Your report Criteria HTML generating function (call) here";
                    break;
            }
            return GenerateOSHAHTML;
        }

        public String GetReportType(String ReportValue, String sReturn)
        {
            XmlDocument xmlobj = new XmlDocument();
            XmlElement objNode;
            String Type = String.Empty;

            xmlobj.LoadXml(sReturn);

            objNode = (XmlElement)xmlobj.SelectSingleNode("/ResultMessage/Document/ReportXML/RptXML/report");

            if (objNode != null)
                Type = objNode.GetAttribute("type");

            return Type;
        }

        protected String GenerateOSHACriteriaHTML(string lType)
        {
            XmlDocument objXml = new XmlDocument();
            XmlDocument objResult = new XmlDocument();
            XmlElement objopt;
            XmlElement objNode;
            String OSHAXML = String.Empty;
            String XmlData = String.Empty;
            long iSelected = 0;
            long CurrentYear = Convert.ToInt64(DateTime.Now.ToString("yyyy"));
            int i;

            XmlData = xmlRep.SelectSingleNode("/ResultMessage/Document/ReportXML/RptXML").InnerXml;

            XslCompiledTransform transform = new XslCompiledTransform();
            transform.Load(WizardPath + "\\oshareportdata.xsl");
            StringWriter results = new StringWriter();
            using (XmlReader reader = XmlReader.Create(new StringReader(XmlData)))
            {
                transform.Transform(reader, null, results);
            }
            objResult.LoadXml(results.ToString());

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'byoshaestablishmentflag'][@value='false']");
            if (objNode != null)
                objNode.SetAttribute("onClick", "byEstablishmentFlagChanged()");

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'byoshaestablishmentflag'][@value='true']");
            if (objNode != null)
                objNode.SetAttribute("onClick", "byEstablishmentFlagChanged()");

            objNode = (XmlElement)objResult.SelectSingleNode("//select[@name = 'reportlevel']");
            if (objNode != null)
                objNode.SetAttribute("onClick", "reportlevelChanged()");

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'selectedentitiesbtn']");
            if (objNode != null)
                objNode.SetAttribute("onClick", "selectReportTarget()");

            objNode = (XmlElement)objResult.SelectSingleNode("//select[@name = 'yearofreport']");
            if (objNode != null)
            {
                i = 2002;

                if (objNode.GetAttribute("codeid") != "")
                    iSelected = Convert.ToInt64(objNode.GetAttribute("codeid"));

                if (iSelected == 0 || i > iSelected)
                    iSelected = CurrentYear;

                for (i = 2002; i <= CurrentYear; i++)
                {
                    objopt = objResult.CreateElement("option");
                    objopt.SetAttribute("value", i.ToString());

                    if (i == iSelected)
                        objopt.SetAttribute("selected", "");
                    objopt.InnerText = i.ToString();
                    objNode.AppendChild(objopt);
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'byoshaestablishmentflag'][@value='false']");
            if (objNode != null)
                objNode.SetAttribute("checked", "");

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'reporttype']");
            if (objNode != null)
                objNode.SetAttribute("value", ReportValue);

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'allentitiesflag'][@value = 'true']");
            if (objNode != null)
                objNode.SetAttribute("checked", "");

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'datemethod'][@value = 'useyear']");
            if (objNode != null)
                objNode.SetAttribute("checked", "");

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'printsofterrlog']");
            if (objNode != null)
                objNode.SetAttribute("checked", "");

            switch (lType)
            {
                case "3":
                    objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'printoshadescflag']");
                    if (objNode != null)
                        objNode.SetAttribute("checked", "");

                    objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'primarylocationflag']");
                    if (objNode != null)
                        objNode.SetAttribute("checked", "");
                    break;
                case "4":

                    objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'printoshadescflag']");
                    if (objNode != null)
                        objNode.SetAttribute("checked", "");

                    objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'primarylocationflag']");
                    if (objNode != null)
                        objNode.SetAttribute("checked", "");
                    break;
                case "5":
                    objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'primarylocationflag']");
                    if (objNode != null)
                        objNode.SetAttribute("checked", "");
                    break;
                default:
                    break;
            }

            OSHAXML = objResult.InnerXml;

            return OSHAXML;
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                if (Request.Form["starttime"] != "")
                {
                    if (lblSchdlTyp.Text == "Monthly")
                    {
                        if (Request.Form["startday"] != "")
                        {
                            SavingData();
                        }
                        else
                        {
                            Response.Write("<script>alert('Please Enter Run on Day.')</script>");
                        }
                    }

                    else if (lblSchdlTyp.Text == "Daily")
                    {
                        SavingData();
                    }
                }
                else
                {
                    Response.Write("<script>alert('Please Enter Start Time.')</script>");
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void SavingData()
        {
            string sMsgStatus = string.Empty;
            XmlDocument oXMLOut = null;
            StringBuilder SaveXml = new StringBuilder();
            String OutputPathURL;
            String OutputPath;
            DateTime DateTime = new DateTime();
            String ReturnXml = String.Empty;
            String OSHAType = String.Empty;

            // akaushik5 CHanged for MITS 38389 Starts
            //OutputPathURL = xmlRep.SelectSingleNode("/ResultMessage/Document/ReportXML/SMNetServer").InnerText + "/RiskmasterUI/UI/SortMaster/SMRepView.aspx?DPT=0";
            OutputPathURL = "/RiskmasterUI/UI/SortMaster/SMRepView.aspx?DPT=0";
            // akaushik5 CHanged for MITS 38389 Ends
            OutputPath = xmlRep.SelectSingleNode("/ResultMessage/Document/ReportXML/SMDataPath").InnerText;
            OSHAType = xmlRep.SelectSingleNode("/ResultMessage/Document/ReportXML/RptXML/report").Attributes[0].Value;

            if (OSHAType == "1" || OSHAType == "3" || OSHAType == "4" || OSHAType == "5")
                PostReports();

            if (Request.Form["startdate"] != "")
                DateTime = Convert.ToDateTime(Request.Form["startdate"]);
            else
                DateTime = DateTime.Now;

            SaveXml.Append("<Message>");
            SaveXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            SaveXml.Append("<Call>");
            SaveXml.Append("<Function>SortMasterAdaptor.ScheduleReports</Function>");
            SaveXml.Append("</Call>");
            SaveXml.Append("<Document>");
            SaveXml.Append("<Details>");
            SaveXml.Append("<TaskTypeText>" + TaskTypeText.Split(',')[0] + "</TaskTypeText>");
            SaveXml.Append("<TaskNameLabel>" + TaskTypeText.Split(',')[0] + "</TaskNameLabel>");
            SaveXml.Append("<ScheduleTypeId></ScheduleTypeId>");
            SaveXml.Append("<ScheduleTypeText></ScheduleTypeText>");
            SaveXml.Append("<Date>" + DateTime.ToShortDateString() + "</Date>");
            SaveXml.Append("<Month>" + DateTime.ToString("MM") + "</Month>");

            if (Request.Form["startday"] != "")
                SaveXml.Append("<DayOfMonth>" + Request.Form["startday"] + "</DayOfMonth>");
            else
                SaveXml.Append("<DayOfMonth></DayOfMonth>");

            SaveXml.Append("<Time>" + Request.Form["starttime"] + "</Time>");
            SaveXml.Append("<bParams>false</bParams>");
            SaveXml.Append("<Arguments></Arguments>");
            SaveXml.Append("<ZeroBasedFinHist>false</ZeroBasedFinHist>");
            SaveXml.Append("<ClaimBasedFinHist></ClaimBasedFinHist>");
            SaveXml.Append("<EventBasedFinHist></EventBasedFinHist>");
            SaveXml.Append("<RecreateFinHist>false</RecreateFinHist>");
            SaveXml.Append("<CreateLog>false</CreateLog>");
            SaveXml.Append("<BillingOption>Installment</BillingOption>");
            SaveXml.Append("<OverDueDays></OverDueDays>");
            SaveXml.Append("<CreateSysDiary>false</CreateSysDiary>");
            SaveXml.Append("<SendEmailNotify>false</SendEmailNotify>");
            SaveXml.Append("<BothDiaryAndEmail>false</BothDiaryAndEmail>");
            SaveXml.Append("<PolicySystemList>");
            SaveXml.Append("<PolicySystems></PolicySystems>");
            SaveXml.Append("</PolicySystemList>");
            SaveXml.Append("<AdminLogin></AdminLogin>");
            SaveXml.Append("<AdminPassword></AdminPassword>");
            SaveXml.Append("<IsFroiBatch>false</IsFroiBatch>");
            SaveXml.Append("<IsAcordBatch>false</IsAcordBatch>");
            SaveXml.Append("<ScheduleId>0</ScheduleId>");
            SaveXml.Append("<saved></saved>");
            SaveXml.Append("<TaskType>" + TaskType + "</TaskType>");
            SaveXml.Append("<UserArguments></UserArguments>");
            SaveXml.Append("<TaskName>" + TaskTypeText.Split(',')[0] + "</TaskName>");
            SaveXml.Append("<IsDataIntegratorTask>0</IsDataIntegratorTask>");
            SaveXml.Append("<OptionsetId></OptionsetId>");
            SaveXml.Append("<SystemModuleName>OSHA</SystemModuleName>");

            SaveXml.Append("<ScheduleIdSM>0</ScheduleIdSM>");
            SaveXml.Append("<ReportId>" + ReportValue + "</ReportId>");
            SaveXml.Append("<LastRunDTTM></LastRunDTTM>");
            SaveXml.Append("<Time></Time>");

            if (lblSchdlTyp.Text == "Daily")
                SaveXml.Append("<ScheduleTypeIdSM>1</ScheduleTypeIdSM>");
            else if (lblSchdlTyp.Text == "Monthly")
                SaveXml.Append("<ScheduleTypeIdSM>2</ScheduleTypeIdSM>");

            if (lblSchdlTyp.Text == "Daily")
            {
                if (Request.Form["mon_run"] == "1")
                    SaveXml.Append("<Mon_Run>true</Mon_Run>");
                else
                    SaveXml.Append("<Mon_Run>false</Mon_Run>");
                if (Request.Form["tue_run"] == "1")
                    SaveXml.Append("<Tue_Run>true</Tue_Run>");
                else
                    SaveXml.Append("<Tue_Run>false</Tue_Run>");
                if (Request.Form["wed_run"] == "1")
                    SaveXml.Append("<Wed_Run>true</Wed_Run>");
                else
                    SaveXml.Append("<Wed_Run>false</Wed_Run>");
                if (Request.Form["thu_run"] == "1")
                    SaveXml.Append("<Thu_Run>true</Thu_Run>");
                else
                    SaveXml.Append("<Thu_Run>false</Thu_Run>");
                if (Request.Form["fri_run"] == "1")
                    SaveXml.Append("<Fri_Run>true</Fri_Run>");
                else
                    SaveXml.Append("<Fri_Run>false</Fri_Run>");
                if (Request.Form["sat_run"] == "1")
                    SaveXml.Append("<Sat_Run>true</Sat_Run>");
                else
                    SaveXml.Append("<Sat_Run>false</Sat_Run>");
                if (Request.Form["sun_run"] == "1")
                    SaveXml.Append("<Sun_Run>true</Sun_Run>");
                else
                    SaveXml.Append("<Sun_Run>false</Sun_Run>");

                SaveXml.Append("<January>false</January>");
                SaveXml.Append("<February>false</February>");
                SaveXml.Append("<March>false</March>");
                SaveXml.Append("<April>false</April>");
                SaveXml.Append("<May>false</May>");
                SaveXml.Append("<June>false</June>");
                SaveXml.Append("<July>false</July>");
                SaveXml.Append("<August>false</August>");
                SaveXml.Append("<September>false</September>");
                SaveXml.Append("<October>false</October>");
                SaveXml.Append("<November>false</November>");
                SaveXml.Append("<December>false</December>");
                SaveXml.Append("<Month>false</Month>");
            }
            else if (lblSchdlTyp.Text == "Monthly")
            {
                SaveXml.Append("<Mon_Run>false</Mon_Run>");
                SaveXml.Append("<Tue_Run>false</Tue_Run>");
                SaveXml.Append("<Wed_Run>false</Wed_Run>");
                SaveXml.Append("<Thu_Run>false</Thu_Run>");
                SaveXml.Append("<Fri_Run>false</Fri_Run>");
                SaveXml.Append("<Sat_Run>false</Sat_Run>");
                SaveXml.Append("<Sun_Run>false</Sun_Run>");

                if (Request.Form["startday"] != "")
                    SaveXml.Append("<Month>" + Request.Form["startday"] +"</Month>");

                if (Request.Form["jan_run"] == "1")
                    SaveXml.Append("<January>true</January>");
                else
                    SaveXml.Append("<January>false</January>");
                if (Request.Form["feb_run"] == "1")
                    SaveXml.Append("<February>true</February>");
                else
                    SaveXml.Append("<February>false</February>");
                if (Request.Form["mar_run"] == "1")
                    SaveXml.Append("<March>true</March>");
                else
                    SaveXml.Append("<March>false</March>");
                if (Request.Form["apr_run"] == "1")
                    SaveXml.Append("<April>true</April>");
                else
                    SaveXml.Append("<April>false</April>");
                if (Request.Form["may_run"] == "1")
                    SaveXml.Append("<May>true</May>");
                else
                    SaveXml.Append("<May>false</May>");
                if (Request.Form["jun_run"] == "1")
                    SaveXml.Append("<June>true</June>");
                else
                    SaveXml.Append("<June>false</June>");
                if (Request.Form["jul_run"] == "1")
                    SaveXml.Append("<July>true</July>");
                else
                    SaveXml.Append("<July>false</July>");
                if (Request.Form["aug_run"] == "1")
                    SaveXml.Append("<August>true</August>");
                else
                    SaveXml.Append("<August>false</August>");
                if (Request.Form["sep_run"] == "1")
                    SaveXml.Append("<September>true</September>");
                else
                    SaveXml.Append("<September>false</September>");
                if (Request.Form["oct_run"] == "1")
                    SaveXml.Append("<October>true</October>");
                else
                    SaveXml.Append("<October>false</October>");
                if (Request.Form["nov_run"] == "1")
                    SaveXml.Append("<November>true</November>");
                else
                    SaveXml.Append("<November>false</November>");
                if (Request.Form["dec_run"] == "1")
                    SaveXml.Append("<December>true</December>");
                else
                    SaveXml.Append("<December>false</December>");
            }
            SaveXml.Append("<ReportName>" + lblReprtName.Text + "</ReportName>");
            SaveXml.Append("<OutputType>" + Request.Form["outtype"] + "</OutputType>");
            SaveXml.Append("<OutputPath>" + OutputPath + "</OutputPath>");
            SaveXml.Append("<OutputPathURL>" + OutputPathURL + "</OutputPathURL>");
            SaveXml.Append("<OutputOptions></OutputOptions>");
            SaveXml.Append("<ReportXML>" + xmlRep.SelectSingleNode("/ResultMessage/Document/ReportXML/RptXML").InnerXml + "</ReportXML>");
            SaveXml.Append("<NotificationType>" + Request.Form["notifytype"] + "</NotificationType>");
            SaveXml.Append("<NotifyEmail>" + Request.Form["notifyemail"] + "</NotifyEmail>");
            SaveXml.Append("<NotifyMsg>" + Request.Form["notifymsg"] + "</NotifyMsg>");

            SaveXml.Append("</Details>");
            SaveXml.Append("</Document>");
            SaveXml.Append("</Message>");

            ReturnXml = AppHelper.CallCWSService(SaveXml.ToString());
            oXMLOut = new XmlDocument();
            oXMLOut.LoadXml(ReturnXml);
            XmlNode oInstanceNode = oXMLOut.SelectSingleNode("/ResultMessage/Document");
            XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
            sMsgStatus = oXMLOut.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText;

            if (sMsgStatus == "Success")
            {
                Response.Redirect("~/UI/SortMaster/ScheduledView.aspx");
            }
        }

        protected void PostReports()
        {
            XmlNodeList objNodeList1 = null;
            XmlNodeList objNodeList2 = null;
            XmlNodeList objNodeList3 = null;
            XmlElement objElement = null;
            XmlElement objElement1 = null;
            XmlNode objNode = null;
            String sType = String.Empty;
            String sName = String.Empty;
            String sValue = String.Empty;
            String sCodeID = String.Empty;
            String[] SaveData1 = new string[0];
            //rkulavil :RMA-12898 starts 
            string[] sSeperator = { "|^|" };
            string[] sSplitSeperator = { "|~|" };
            //rkulavil :RMA-12898 ends

            if (Hidden1.Value != "")
                //rkulavil :RMA-12898 starts 
                //SaveData1 = Hidden1.Value.Split(';');
                SaveData1 = Hidden1.Value.Split(sSplitSeperator,StringSplitOptions.RemoveEmptyEntries);
                //rkulavil :RMA-12898 ends

            objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ReportXML/RptXML/report");
            if (objElement != null)
                objElement.SetAttribute("outputfilename", Request.Form["outputfilename"]);

            if (xmlRep.SelectSingleNode("/ResultMessage/Document/ReportXML/RptXML/report/form").HasChildNodes)
            {
                objNodeList1 = xmlRep.SelectSingleNode("/ResultMessage/Document/ReportXML/RptXML/report/form").ChildNodes;

                //foreach (XmlElement objElement1 in objNodeList1)
                for (int i = 0; i < objNodeList1.Count; i++)
                {
                    if (objNodeList1[i].NodeType == XmlNodeType.Element)
                    {
                        objElement1 = (XmlElement)objNodeList1[i];

                        if (objElement1.HasChildNodes)
                        {
                            objNodeList2 = objElement1.ChildNodes;
                            foreach (XmlElement objElement2 in objNodeList2)
                            {
                                sType = objElement2.GetAttribute("type");
                                sName = objElement2.GetAttribute("name");
                                sValue = objElement2.GetAttribute("value");

                                for (int k = 0; k < objElement2.Attributes.Count; k++)
                                {
                                    if (objElement2.Attributes[k].Name == "checked")
                                        objElement2.RemoveAttribute("checked");
                                }

                                switch (sType)
                                {
                                    case "radio":
                                        if (Request.Form[sName] == sValue)
                                            objElement2.SetAttribute("checked", "");
                                        break;
                                    case "checkbox":
                                        if (Request.Form[sName] != null)
                                            objElement2.SetAttribute("checked", "");
                                        break;
                                    case "combobox":
                                        objNodeList3 = objElement2.ChildNodes;
                                        if (objNodeList3.Count != 0)
                                        {
                                            foreach (XmlElement objElement3 in objNodeList3)
                                            {
                                                if (objElement3.InnerXml == Request.Form[sName])
                                                {
                                                    sCodeID = objElement3.GetAttribute("value");
                                                    objElement2.SetAttribute("codeid", sCodeID);
                                                    objElement3.SetAttribute("selected", "");
                                                    break;
                                                }
                                            }
                                        }
                                        else
                                            objElement2.SetAttribute("codeid", Request.Form[sName]);
                                        break;
                                    case "date":
                                        if (Request.Form[sName] != "")
                                        {
                                            DateTime dt = Convert.ToDateTime(Request.Form[sName]);
                                            objElement2.SetAttribute("value", dt.ToString("yyyyMMdd"));
                                        }
                                        break;
                                    case "entitylist":
                                        if (SaveData1 != null)
                                        {
                                            for (int j = 0; j < SaveData1.Length; j++)
                                            {
                                                if (SaveData1[j] != "")
                                                {
                                                    objElement = xmlRep.CreateElement("option");
                                                    //rkulavil :RMA-12898 starts 
                                                    //objElement.SetAttribute("value", SaveData1[j].Split(',')[1]);
                                                    objElement.SetAttribute("value", SaveData1[j].Split(sSeperator, StringSplitOptions.RemoveEmptyEntries)[1]);
                                                    //objElement.InnerText = SaveData1[j].Split(',')[0];
                                                    objElement.InnerText = SaveData1[j].Split(sSeperator, StringSplitOptions.RemoveEmptyEntries)[0];
                                                    //rkulavil :RMA-12898 ends 
                                                    objNode = objElement2;
                                                    objNode.AppendChild(objElement);
                                                }
                                            }
                                        }
                                        break;
                                    case "numeric":
									case "text":
                                        objElement2.SetAttribute("value", Request.Form[sName]);
                                        break;
                                }
                            }
                        }
                    }
                }
            }
        }
        
    }
}