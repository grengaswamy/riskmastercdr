﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.AppHelpers;
using System.ServiceModel;
using System.Configuration;
using System.Data;
using System.IO;

namespace Riskmaster.UI.SortMaster
{
    public partial class DeleteReports : System.Web.UI.Page
    {
        bool bReturnStatus = false;
        XElement XmlTemplate = null;
        XmlDocument XmlDoc = new XmlDocument();
        string sCWSresponse = string.Empty;
        private XmlDocument Model = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            string sReturn = string.Empty;
            try
            {
                if (!IsPostBack)
                {
                    XmlDocument oFDMPageDom = new XmlDocument();
                    string strReturn = string.Empty;
                    XElement oMessageElement = LoadSearchWizardTemplate();
                    CallService(oMessageElement, ref oFDMPageDom, ref strReturn, false);

                    if (oFDMPageDom.SelectSingleNode("//ReportDetails") != null)
                    {
                        DataSet aDataSet = new DataSet();
                        aDataSet.ReadXml(new StringReader(oFDMPageDom.GetElementsByTagName("ReportDetails")[0].OuterXml));
                        RptrReport.DataSource = aDataSet.Tables["Report"];
                        ViewState["oFDMPageDom"] = oFDMPageDom.GetElementsByTagName("ReportDetails")[0].OuterXml;
                        RptrReport.DataBind();

                    }
                    chkDeleteAll.Checked = true; // mkaran2 : MITS 33046 : OSHA Issue : On opening "Delete Report" screen by default it should checked
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        /// <summary>
        /// This function would call the webservice to get output and return ouput string
        /// </summary>
        /// <param name="oMessageElement"></param>
        /// <param name="oFDMPageDom"></param>
        protected void CallService(XElement oMessageElement, ref XmlDocument oXMLOut, ref string p_sReturn, bool deletesave)
        {
            string sMsgStatus = string.Empty;

            p_sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
            oXMLOut.LoadXml(p_sReturn);
            XmlNode oInstanceNode = oXMLOut.SelectSingleNode("/ResultMessage/Document");
            XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
            sMsgStatus = oXMLOut.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText;

            if ((sMsgStatus == "Success") && deletesave)
            {
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
            }
        }

        private XElement LoadSearchWizardTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                 <Message>
                     <Authorization>cdb73334bf7e4750ada07b96c582eab5</Authorization> 
                     <Call>
                      <Function>SortMasterAdaptor.GetReportDetails</Function> 
                      </Call>
                      <Document>
                        <ReportName></ReportName>
                        <ReportDesc></ReportDesc>
                        <ReportId></ReportId>
                      </Document>
                </Message>


            ");
            return oTemplate;
        }

        protected void btndelete_Click(object sender, EventArgs e)
        {
            XmlDocument oFDMPageDom = new XmlDocument();
            string strReturn = string.Empty;
            string DeleteAll = string.Empty;
            string ReportIds = string.Empty;
            XmlDocument oXMLOut = null;

            try
            {

                foreach (RepeaterItem item in RptrReport.Items)
                {
                    CheckBox chkselected = item.FindControl("chkReportName") as CheckBox;
                    HiddenField hdnReportId = item.FindControl("hdnReportId") as HiddenField;
                    if (chkselected.Checked)
                    {
                        if (ReportIds == string.Empty)
                            ReportIds = hdnReportId.Value;
                        else
                            ReportIds = ReportIds + "," + hdnReportId.Value;
                    }
                }

                if (chkDeleteAll.Checked)
                    DeleteAll = "1";
                else
                    DeleteAll = "0";

                XmlDocument objXML = new XmlDocument();
                StringBuilder sXml = new StringBuilder("<Message>");
                sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
                sXml = sXml.Append("<Call><Function>SortMasterAdaptor.DeleteReports</Function></Call>");
                sXml = sXml.Append("<Document><Report>");
                sXml = sXml.Append("<DeleteAllReport>" + DeleteAll + "</DeleteAllReport>");
                sXml = sXml.Append("<ReportIDs>" + ReportIds + "</ReportIDs>");
                sXml = sXml.Append("</Report></Document></Message>");
                XElement oTemplate = XElement.Parse(sXml.ToString());

                CallService(oTemplate, ref oFDMPageDom, ref strReturn, true);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

            finally
            {
                oXMLOut = null;
            }

        }
    }
}