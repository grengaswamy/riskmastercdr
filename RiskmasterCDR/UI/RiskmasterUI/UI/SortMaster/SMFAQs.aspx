﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SMFAQs.aspx.cs" Inherits="Riskmaster.UI.SortMaster.SMFAQs" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Sortmaster - FAQ</title>
    <H3>Sortmaster - FAQ</H3>
    <script lang="JavaScript1.2">
        var lBackCount = -1;
</script>
</head>
<body>
    <form id="Form1" runat="server">
    <asp:Label ID="lblQA" runat="server" Text="Questions and Answers" Font-Bold="true"></asp:Label>
        <p class="small"><a href="#tips" onclick="lBackCount--;">Skip to Tricks</a></p>
    <p>
        <asp:Label ID="lblQ1" runat="server" Text="Q.) How do I run an available report?" Font-Bold="true"></asp:Label><br />
        <asp:Label ID="lblA1" runat="server" Text="To begin running an available report, click on the 'Available Reports' menu item from the left hand menu, then click on the desired report name."></asp:Label>
    </p>
        <asp:Label ID="lblQ2" runat="server" Text="Q.) How do I make a report 'available'?" Font-Bold="true"></asp:Label><br />
        <asp:Label ID="lblA2" runat="server" Text="To make a report show up in the available reports list, use the 'Post New Report' menu item."></asp:Label>
    <p>
        <asp:Label ID="lblQ3" runat="server" Text="Q.) What does it mean to 'Post' a report?" Font-Bold="true"></asp:Label><br />
        <asp:Label ID="lblA3" runat="server" Text="This refers to the process of making a saved Sortmaster report available for internet users to run directly through this web site.  "></asp:Label>
    </p>
    <p>
        <asp:Label ID="lblQ4" runat="server" Text="Q.) The 'Post New Report' page is asking for me to find a file.  Why?" Font-Bold="true"></asp:Label><br />
        <asp:Label ID="lblA4" runat="server" Text="Currently the main way to create Sortmaster reports is the desktop product.  These reports are saved to a '.smr' file.  The page is expecting you to find the .smr file 
        containing the report you want to post."></asp:Label> 

    </p>

    <p>
        <asp:Label ID="lblQ5" runat="server" Text="Q.) What is a 'Job' and how is it different from a 'Report'?" Font-Bold="true"></asp:Label> <br />
        <asp:Label ID="lblA5" runat="server" Text="A 'Report' is the set of stored rules for retrieving data along with notes about what additional criteria you may specifiy.  Once you 
        have requested that this 'Report' be run and specified all criteria required,then a copy of the 'Report' is made which includes all your specified information.  
        This copy is called a 'Job'.  Jobs are automatically submitted to the reporting server(s) 'Job Queue' to be run.  You will find the list of 'Jobs' by clicking the 'Job Queue' menu item to the left. "></asp:Label>
    </p>

    <p>
        <asp:Label ID="lblQ6" runat="server" Text="Q.) What is the 'Job Queue'?" Font-Bold="true"></asp:Label> <br />
        <asp:Label ID="lblA6" runat="server" Text="The 'Job Queue' is a list of reports that have become 'Jobs'.This screen shows several things of interest."></asp:Label>
       
            <asp:BulletedList ID="BulletedList1" runat="server" BulletStyle="Numbered">
             <asp:ListItem>The activities of the reporting server.</asp:ListItem>
             <asp:ListItem>The status of the currently running 'Job'.</asp:ListItem>
             <asp:ListItem>The list of 'Jobs' that are waiting to run.</asp:ListItem>
             <asp:ListItem>The list of 'Jobs' that are completed and links to the results.</asp:ListItem>
         </asp:BulletedList>
       
           
        <asp:Label ID="lblA6prev" runat="server" Text="See the previous question & answer to understand the distinction between 'Jobs' and 'Reports'."></asp:Label>   
     
        
    </p>

    <p>
        <asp:Label ID="lblQ7" runat="server" Text="Q.) What does it mean to 'Schedule a Report'?" Font-Bold="true"></asp:Label><br />
        <asp:Label ID="lblA7" runat="server" Text="A report can be set up in the system to run automatically on a recurring basis.
        For example, once each week on Sunday nights it might be good to have a 
        particular financial or status report run so that it can be reviewed on Monday.
        This can be accomplished by clicking the 'Schedule Reports' menu item at the left."></asp:Label>
    </p>

    <p>
        <asp:Label ID="lblQ8" runat="server" Text="Q.) When I follow the link from an emailed report notification I don't see my report results.  Why?" Font-Bold="true"></asp:Label><br />
        <asp:Label ID="lblA8" runat="server" Text="A report can be set to run with a notification sent at completion.  Based on your selections, 
        this notice can include the report as an attachment, a link, or not at all.
        The linked option provides a link directly to the report output page.  However, 
        if you are not logged in when it is clicked the system will return access denied or 
        force you back to the login screen.  This is done by design to protect the report data 
        from anyone who could illicitly get a copy of the link.  
        What you can do is login, then return to your email program and click the link again.  
        It will now open directly."></asp:Label>
    </p>

    <p>
        <asp:Label ID="lblQ9" runat="server" Text="Q.) Where is my Report Output actually stored?" Font-Bold="true"></asp:Label><br />
        <asp:Label ID="lblA9" runat="server" Text="Reports are generated in a temporary area on the Reporting Server.  They 
        are then moved automatically into the document path where all your other 
        files are stored."></asp:Label>
    </p>

    <p>
        <asp:Label ID="lblQ10" runat="server" Text="Q.) What does it mean to 'Archive' report output from the 'Job Queue'?" Font-Bold="true"></asp:Label><br />
        <asp:Label ID="lblA10" runat="server" Text="Report output can take up disk space and make the 'Job Queue' exceptionally long. 
        'Archiving' report output removes the 'Job' entry from the 'Job Queue' and moves the
        report output into the folder of your choice under the 'Files' section of RMNet.  
        You can get to the 'Files' section of RMNet by clicking the blue 'Files' button above this page.
        It is recommended that you 'Archive' the output you want to keep and delete any that 
        you no longer need.  This will keep the 'Job Queue' shorter."></asp:Label>
    </p>
   
    <a id="tips"></a>
    <asp:Label ID="lblTricks" runat="server" Text="Tips and Tricks" Font-Bold="true"></asp:Label>
    <asp:BulletedList ID="BulletedList2" runat="server" BulletStyle="Disc" >
        <asp:ListItem>The 'Job Queue' refreshes every few seconds automatically.  If you do not feel like waiting you can click the refresh button on the page.  
            This will immediately update just this frame rather than using the browser refresh button which will navigate away from the "Report Queue" page.
        </asp:ListItem>
        <asp:ListItem>An Optional Output Name can be given to any report output.  This can be usefull to give meaningfull names to the output files. 
            The provided name will be at the beginning of the report output file.  A UniqueId is used by the system to make sure that output from one 
            report does not over-write output from any other report.  The file extension is used by the system to track the file format.Thus the resulting 
            file names will be constructed as follows:
            OutputFileName = [OutputName]+[UniqueId]+[.Extension]
        </asp:ListItem>
        <asp:ListItem>SortMaster.Net Reports support email notification and delivery! The reporting server
    can be configured to allow the sending of email.  If this has been done
    on your installation of SortMaster.Net, then you can have the system send notification of report completion, 
    a link to the results or an attachment containing the results.  
    This mail can also include a small message of your own and may be sent to 
    as many people as you want.  You can even deliver to non-SortMasterNet users!</asp:ListItem>
    </asp:BulletedList>
   
    <div style="text-align:center">
     <asp:Button ID="btnBack" runat="server" Text="Back" OnClientClick="parent.history.go(lBackCount);return false;" Font-Bold="true"></asp:Button>
    </div>
    </form>
    
</body>
</html>

