﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestSM.aspx.cs" Inherits="Riskmaster.UI.SortMaster.TestSM" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
          <a href="javascript:window.open('SM-Redirect.aspx?smpage=rptfields')">SM Designer</a>
           <a href="javascript:window.open('SM-Redirect.aspx?smpage=openreport')">SM Draft Report</a>
            <a href="javascript:window.open('SM-Redirect.aspx?smpage=postreport')">Post SM Draft Report</a>
            <a href="javascript:window.open('SM-Redirect.aspx?smpage=smpostreport')">Post SM/World Report</a>
            <br />
            <br />
            <a href="javascript:window.open('SM-Redirect.aspx?smpage=smreportsel')">Available Reports</a>
            <a href="javascript:window.open('SM-Redirect.aspx?smpage=smrepqueue')">Job Queue</a>
            <a href="javascript:window.open('SM-Redirect.aspx?smpage=smreportdelete')">Delete Report</a>
            <a href="javascript:window.open('SM-Redirect.aspx?smpage=schedulereport')">Schedule Reports</a>
            <a href="javascript:window.open('SM-Redirect.aspx?smpage=schedulelist')">View Scheduled Reports</a>
            
            <br />
            <br />
            <a href="javascript:window.open('SM-Redirect.aspx?smpage=postosha300')">Post OSHA 300 Report</a>
            <a href="javascript:window.open('SM-Redirect.aspx?smpage=postosha301')">Post OSHA 301 Report</a>
            <a href="javascript:window.open('SM-Redirect.aspx?smpage=postosha300A')">Post OSHA 300A Report</a>
            <a href="javascript:window.open('SM-Redirect.aspx?smpage=postoshasharps')">Post OSHA Sharps Log Report</a>
            <br />
            <br />
            <a href="javascript:window.open('SM-Redirect.aspx?smpage=adminreportqueue')">View All Reports in Queue</a>
            <a href="javascript:window.open('SM-Redirect.aspx?smpage=adminschedulelist')">View All Scheduled Reports</a>
    </div>
    </form>
</body>
</html>
