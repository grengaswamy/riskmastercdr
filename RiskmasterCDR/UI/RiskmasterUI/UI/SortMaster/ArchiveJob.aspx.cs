﻿using System;
using System.Collections;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using Riskmaster.Common;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Xsl;
using System.IO;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.SortMaster
{
    public partial class ArchiveJob : NonFDMBasePageCWS
    {
        bool bReturnStatus = false;
        XElement XmlTemplate = null;
        XmlDocument XmlDoc = new XmlDocument();
        string sCWSresponse = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            string sReturn = string.Empty;

            if (!(IsPostBack))
            {
                try
                {
                    bReturnStatus = CallCWS("SortMasterAdaptor.GetFoldersForArchive", XmlTemplate, out sReturn, true, true);
                    ViewState["JobID"] = Request.QueryString["JobIds"]; 
                }
                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                }
            }
        }

        protected void btnArchive_Click(object sender, EventArgs e)
        {
            try
            {
                XmlTemplate = GetXMLForSave();
                string p_sReturn = string.Empty;
                XmlDocument oXMLOut = new XmlDocument();

                bReturnStatus = CallCWS("SortMasterAdaptor.ArchiveJob", XmlTemplate, out p_sReturn, false, false);

                string sMsgStatus = string.Empty;
                oXMLOut.LoadXml(p_sReturn);
                XmlNode oInstanceNode = oXMLOut.SelectSingleNode("/ResultMessage/Document");
                XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
                sMsgStatus = oXMLOut.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText;

                if (sMsgStatus == "Success")
                {
                    Response.Redirect("JobQueue.aspx");
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XElement GetXMLForSave()
        {
           
            string XmlDetails = string.Empty;

            //StringBuilder sXml = new StringBuilder("<Message>");
            //sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            //sXml = sXml.Append("<Call><Function>SortMasterAdaptor.UpdateJobData");
            //sXml = sXml.Append("</Function></Call><Document><Job>");
            //sXml = sXml.Append("<JobIDs>" + ViewState["JobID"] + "</JobIDs>");
            //sXml = sXml.Append("<FolderIDs>" + lstfolders.SelectedItem.Text + "</FolderIDs>");
            //sXml = sXml.Append("<FolderName>" + lstfolders.SelectedValue + "</FolderName>");
            //sXml = sXml.Append("</Job></Document></Message>");
            //XElement oTemplate = XElement.Parse(sXml.ToString());
            //return oTemplate;


            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>SortMasterAdaptor.ArchiveJob");
            sXml = sXml.Append("</Function></Call><Document><Job>");
            sXml = sXml.Append("<JobIDs>" + ViewState["JobID"] + "</JobIDs>");
            sXml = sXml.Append("<FolderIDs>" + lstfolders.SelectedValue  + "</FolderIDs>");
            sXml = sXml.Append("<FolderName>" + lstfolders.SelectedItem.Text + "</FolderName>");
            sXml = sXml.Append("</Job></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
    }
}
