﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;
using System.Xml.Linq;
using System.Data;
using System.IO;

namespace Riskmaster.UI.SortMaster
{
    public partial class EmailJob : NonFDMBasePageCWS
    {
        // string JobIds = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                bool bReturnStatus = false;
                XElement XmlTemplate = null;
                string sReturn = string.Empty;
                XmlDocument oFDMPageDom = new XmlDocument();
                string strReturn = string.Empty;
                if (!IsPostBack)
                {
                    ViewState["JobIds"] = Request.QueryString["JobIds"]; ;
                    //JobIds = Request.QueryString["JobIds"];
                    XmlTemplate = GetMessageTemplate();
                    bReturnStatus = CallCWS("SortMasterAdaptor.GetUsersForMail", XmlTemplate, out sReturn, true, true);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization></Authorization>");
            sXml = sXml.Append("<Call><Function>SortMasterAdaptor.GetUsersForMail</Function></Call><Document><UserID/><UserID />");
            sXml = sXml.Append("</Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("JobQueue.aspx");
        }

        protected void btnEmail_Click(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            XElement XmlTemplate = null;
            string sReturn = string.Empty;
            string sMsgStatus = string.Empty;
            XmlDocument oXMLOut = null;
            string selectedItemValue = string.Empty;
            if (lstUsers.Items.Count > 0)
            {
                for (int i = 0; i < lstUsers.Items.Count; i++)
                {
                    if (lstUsers.Items[i].Selected)
                    {
                        if (selectedItemValue != string.Empty)
                        {
                            selectedItemValue = selectedItemValue + "," + lstUsers.Items[i].Value;
                        }
                        else
                        {
                            selectedItemValue = lstUsers.Items[i].Value;
                        }
                    }
                }
            }

            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization></Authorization>");
            sXml = sXml.Append("<Call><Function>SortMasterAdaptor.sendMail</Function></Call><Document><Mail>");
            sXml = sXml.Append("<EmailCC>" + txtEmailTo.Text + "</EmailCC>");
            sXml = sXml.Append("<OptionalMessage>" + txtMessage.Text + "</OptionalMessage>");
            sXml = sXml.Append("<UserID>" + selectedItemValue + "</UserID>");
            sXml = sXml.Append("<JobIds>" + ViewState["JobIds"] + "</JobIds>");
            sXml = sXml.Append("</Mail></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            bReturnStatus = CallCWS("SortMasterAdaptor.sendMail", oTemplate, out sReturn, true, true);
            oXMLOut = new XmlDocument();
            oXMLOut.LoadXml(sReturn);
            XmlNode oInstanceNode = oXMLOut.SelectSingleNode("/ResultMessage/Document");
            XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
            sMsgStatus = oXMLOut.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText;

            if (sMsgStatus == "Success")
            {
                Page.Response.Redirect("JobQueue.aspx", true);
            }
        }
    }
}