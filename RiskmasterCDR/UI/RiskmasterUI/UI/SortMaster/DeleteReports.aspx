﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DeleteReports.aspx.cs" Inherits="Riskmaster.UI.SortMaster.DeleteReports" %>

<!DOCTYPE html>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl" TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <script type="text/javascript">
        function DeleteReport() {
            var pageElements = document.forms[0].elements;
            var oneChecked = false;
            var reportAvailable = false;
            for (var i = 0; i < pageElements.length; i++) {
                if (pageElements[i].type == "checkbox" && pageElements[i].name.indexOf('chkDeleteAll') == -1)
                {
                    reportAvailable = true;
                    if (pageElements[i].checked == true)
                    {
                        oneChecked = true;
                    }
                }
            }
            if (reportAvailable == false)
            {
                alert("No reports available.");
                return false;
            }
            if (oneChecked==false) {
                alert("Please select report to delete.");
                return false;
            }
            if (!self.confirm("Report(s) will be permanently deleted. Are you sure you want to delete selected Report(s)?"))
                return false;
        }
    </script>
</head>
<body onload="parent.pleaseWait('stop');">
    <form id="form1" runat="server">
    <div>
        <table>
        <tr>
            <td colspan="2">
                <uc1:errorcontrol id="ErrorControl1" runat="server" />
            </td>
        </tr>
    </table>
    <asp:Repeater ID="RptrReport" runat="server">
            <HeaderTemplate>
                <table width="100%" border="1" cellspacing="0" cellpadding="1">
                    <tr><td class="ctrlgroup2" colspan="8">
                        <asp:Label ID="lblAvailableReports" runat="server" Text="Delete Available Reports"></asp:Label></td></tr>
                    <tr>
                    <td class="colheader3" width="30%" align="center">&nbsp;<asp:Label ID="lblReportName" runat="server" Text="Report Name"></asp:Label></td>
                    <td class="colheader3" width="70%" align="center">&nbsp;<asp:Label ID="lblReportDesc" runat="server" Text="Report Description"></asp:Label></td>
                    </tr>
                </table>
            </HeaderTemplate>
            <ItemTemplate>
                <table  width="100%" height="0" bgcolor="white" border="1" cellspacing="0" cellpadding="10" class="rowlight1">
                    <tr>
                        <td width="30%">
                            <asp:CheckBox ID="chkReportName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ReportName")%>' />
                            <asp:HiddenField ID="hdnReportId" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "ReportId")%>' />
                        </td>
                        <td width="70%">
                            <asp:Label ID="lblReprtDesc" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ReportDesc")%>'></asp:Label>
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <table width="100%" height="0" bgcolor="white" border="1" cellspacing="0" cellpadding="10" class="rowdark1">
                    <tr>
                        <td width="30%">
                            <asp:CheckBox ID="chkReportName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ReportName")%>' />
                            <asp:HiddenField ID="hdnReportId" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "ReportId")%>' />
                        </td>
                        <td width="70%">
                            <asp:Label ID="lblReprtDesc" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ReportDesc")%>'></asp:Label>
                        </td>
                    </tr>
                </table>
            </AlternatingItemTemplate>
        </asp:Repeater>
        <table> 
            <tr>
                <td>
                     <asp:CheckBox ID="chkDeleteAll" runat="server" Text="Delete for all users" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     <asp:Button ID="btndelete" runat="server" Text="Delete Selected Report(s)" Font-Bold="true" OnClientClick="return DeleteReport();" OnClick="btndelete_Click" />
                </td>
            </tr>
        </table>
       
    </div>
    This list shows the reports that are available for deletion.<br />
    To delete one or more available reports, place a checkmark next to the report and click the "Delete Selected Report(s)" button.
    <br>Note: This will not affect other users who also have access to a report.
    <br>
    *** For information on extended reporting features and report troubleshooting please see the <a href="SMFAQs.aspx">Frequently Asked Questions (FAQ)</a>.
    </form>
</body>
</html>
