﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportAdminScheduleList.aspx.cs" Inherits="Riskmaster.UI.UI.SortMaster.ReportAdminScheduleList" %>

<!DOCTYPE html>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl" TagPrefix="uc3" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <script type="text/javascript">
        function DeleteReport() {
            var pageElements = document.forms[0].elements;
            var oneChecked = false;
            for (var i = 0; i < pageElements.length; i++) {
                if (pageElements[i].type == "checkbox") {
                    if (pageElements[i].checked == true) {
                        oneChecked = true;
                    }
                }
            }
            if (oneChecked == false) {
                alert("Please select the schedule you would like to delete.");
                return false;
            }
            if (!self.confirm("Are you sure you want to delete selected schedule(s)??"))
                return false;
        }
    </script>
</head>
<body onload="parent.pleaseWait('stop');">
    <form id="form1" runat="server">
    <div>
    <table cellspacing="0" cellpadding="0" border="0" width="50%">
        <tr>
                <td colspan="2"><uc3:ErrorControl ID="ErrorControl1" runat="server" /></td>
        </tr>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="2">
        <tr><td class="msgheader" colspan="8">
            <asp:Label ID="lblRepAdmin" runat="server" Text="Reports Administration" ForeColor="Black"></asp:Label></td></tr>
        <tr><td class="ctrlgroup" colspan="8"><asp:Label ID="lblScheduleRep" runat="server" Text="Scheduled Reports"></asp:Label></td></tr>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="2">
        <asp:GridView width="100%" HeaderStyle-ForeColor="White" HeaderStyle-Font-Size="11" EmptyDataText="There are no Reports scheduled to run" EmptyDataRowStyle-HorizontalAlign="Center"
            RowStyle-CssClass="rowlight1" AlternatingRowStyle-CssClass="rowdark1" HeaderStyle-BackColor="Gray"  HeaderStyle-HorizontalAlign="Left" GridLines="None"
            cellspacing="0" cellpadding="2" ID="gvScheduledReports" RowStyle-BorderColor="White" 
            runat="server" AutoGenerateColumns="False" OnRowDataBound="gvScheduledReports_RowDataBound">
            <Columns>
                <asp:TemplateField HeaderText="Report Name" SortExpression="report_name">
                    <ItemTemplate>
                        <asp:CheckBox ID="chkReportName" runat="server"></asp:CheckBox>
                        <asp:HyperLink ID="hyplnkReportName" runat="server" Text='<%# Eval("ReportName")%>' NavigateUrl='<%# String.Format("~/UI/SortMaster/ScheduleDetail.aspx?ReportId={0}&ReportName={1}&ScheduleId={2}", Eval("ReportId"),Eval("ReportName"), Eval("ScheduleId")) %>'></asp:HyperLink>
                        <asp:HiddenField ID="hdnScheduleId" runat="server" Value='<%# Eval("ScheduleId")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText="Schedule Type" DataField="ScheduleType" SortExpression="schedule_type" ItemStyle-HorizontalAlign="Left"/>
                <asp:BoundField HeaderText="Next Run" DataField="NextRunDT" SortExpression="next_run_date,start_time" ItemStyle-HorizontalAlign="Left"/>
                <asp:BoundField HeaderText="Last Run" DataField="LastRunDTTM" SortExpression="last_run_dttm" ItemStyle-HorizontalAlign="Left"/>
            </Columns>
        </asp:GridView>
        </tr>
        <tr><td>
            <asp:Button ID="btnDelete" runat="server" Text="Delete" OnClientClick="return DeleteReport();" OnClick="btnDelete_Click" /></td></tr>
    </table>
    </div>
    <div class="small">
        This list shows the reports that are scheduled to run on a recurring basis.  
          
        <br />
        To edit the schedule for a report, click on the report name.<br />To delete one or more scheduled reports, place a checkmark next to the report and click the "Delete" button.
        <br>
        *** For information on extended reporting features and report troubleshooting please see the <a href="SMFAQs.aspx">Frequently Asked Questions (FAQ)</a>.
    </div>
    </form>
</body>
</html>

