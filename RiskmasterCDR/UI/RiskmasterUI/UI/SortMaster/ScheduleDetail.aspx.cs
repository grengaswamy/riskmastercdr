﻿using System;
using System.Collections;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using Riskmaster.Common;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Xsl;
using System.IO;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.SortMaster
{
    public partial class ScheduleDetail : Page
    {
        static XmlDocument xmlRep = new XmlDocument();
        static XmlDocument objResult = new XmlDocument();
        static String ReportValue = String.Empty;
        static String ScheduleType = String.Empty;
        static String TaskTypeText = String.Empty;
        static String TaskType = String.Empty;
        static String UserID = String.Empty;
        static String WizardPath = String.Empty;
        static String ScheduleIdSM = String.Empty;
        static String LastRunDTTM = String.Empty;

        private XElement GetMessageTemplate(String ScheduleID)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization></Authorization>");
            sXml = sXml.Append("<Call><Function>SortMasterAdaptor.GetScheduleDetailsSM</Function></Call>");
            sXml = sXml.Append("<Document><ScheduleID>" + ScheduleID + "</ScheduleID></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            XElement XmlTemplate = null;
            String sReturn = String.Empty;
            StringBuilder sHTML = new StringBuilder();
            XmlNodeList pList;
            XmlElement pRoot = null;
            String ReportXML = String.Empty;
            String GenerateHTML = String.Empty;            

            if (!(IsPostBack))
            {
                try
                {
                    ViewState["PrevPage"] = Request.UrlReferrer.ToString();
                    if (Request.QueryString["ReportId"] != null)
                    {
                        ReportValue = Request.QueryString["ReportId"];
                    }
                    if (Request.QueryString["ReportName"] != null)
                    {
                        lblrptname.Text = Request.QueryString["ReportName"];
                    }
                    if (Request.QueryString["ScheduleId"] != null)
                    {
                        ScheduleIdSM = Request.QueryString["ScheduleId"];
                    }

                    XmlTemplate = GetMessageTemplate(ScheduleIdSM);
                    sReturn = AppHelper.CallCWSService(XmlTemplate.ToString());

                    sReturn = sReturn.Replace("&lt;", "<");
                    sReturn = sReturn.Replace("&gt;", ">");
                    sReturn = sReturn.Replace('\"', '"');

                    xmlRep.LoadXml(sReturn);
                    pRoot = xmlRep.DocumentElement;

                    WizardPath = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/SMPath").InnerText;

                    //GenerateHTML = GenerateCriteriaHTML(sReturn);

                    sHTML = sHTML.Append("<link rel=stylesheet href=../../Content/rmnet.css type=text/css />");
                    sHTML = sHTML.Append("<script language=JavaScript SRC=../../Scripts/form.js></script>");
                    sHTML = sHTML.Append("<script language=JavaScript src=../../Scripts/oshareportdata.js></script>");

                    //sHTML = sHTML.Append(GenerateHTML);

                    sHTML = sHTML.Replace("<?xml version=\"1.0\" encoding=\"utf-8\"?>", "");
                    sHTML = sHTML.Replace("<script language=\"JavaScript\" SRC=\"oshareport.js\" />", "");

                    div2.InnerHtml = sHTML.ToString();

                    ddlscheduletype.SelectedIndex = Convert.ToInt32(xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ScheduleType").InnerText) - 1;

                    LastRunDTTM = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/LastRunDTTM").InnerText;

                    String nextRun = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/NextRunDate").InnerText;
                    DateTime dt = DateTime.ParseExact(nextRun, "yyyyMMdd", null);

                    txtnextrun.Text = dt.ToShortDateString();

                    String nextRunTime = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/NextRunTime").InnerText;
                    DateTime tm = DateTime.ParseExact(nextRunTime, "HHmmss", null);

                    txtnexttimerun.Text = tm.ToLongTimeString();

                    if (ddlscheduletype.SelectedIndex == 0)
                    {
                        chkmon.Visible = true;
                        chktue.Visible = true;
                        chkwed.Visible = true;
                        chkthu.Visible = true;
                        chkfri.Visible = true;
                        chksat.Visible = true;
                        chksun.Visible = true;

                        if (xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/MonRun").InnerText == "-1" ||
                           xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/MonRun").InnerText == "1")
                        {
                            chkmon.Checked = true;
                        }
                        if (xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/TueRun").InnerText == "-1" ||
                           xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/TueRun").InnerText == "1")
                        {
                            chktue.Checked = true;
                        }
                        if (xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/WedRun").InnerText == "-1" ||
                           xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/WedRun").InnerText == "1")
                        {
                            chkwed.Checked = true;
                        }
                        if (xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ThuRun").InnerText == "-1" ||
                           xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ThuRun").InnerText == "1")
                        {
                            chkthu.Checked = true;
                        }
                        if (xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/FriRun").InnerText == "-1" ||
                           xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/FriRun").InnerText == "1")
                        {
                            chkfri.Checked = true;
                        }
                        if (xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/SatRun").InnerText == "-1" ||
                           xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/SatRun").InnerText == "1")
                        {
                            chksat.Checked = true;
                        }
                        if (xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/SunRun").InnerText == "-1" ||
                           xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/SunRun").InnerText == "1")
                        {
                            chksun.Checked = true;
                        }
                    }
                    else if (ddlscheduletype.SelectedIndex == 1)
                    {
                        chkjan.Visible = true;
                        chkfeb.Visible = true;
                        chkmar.Visible = true;
                        chkapr.Visible = true;
                        chkmay.Visible = true;
                        chkjun.Visible = true;
                        chkjul.Visible = true;
                        chkaug.Visible = true;
                        chksep.Visible = true;
                        chkoct.Visible = true;
                        chknov.Visible = true;
                        chkdec.Visible = true;

                        if (xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/JanRun").InnerText == "-1" ||
                           xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/JanRun").InnerText == "1")
                        {
                            chkjan.Checked = true;
                        }
                        if (xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/FebRun").InnerText == "-1" ||
                           xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/FebRun").InnerText == "1")
                        {
                            chkfeb.Checked = true;
                        }
                        if (xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/MarRun").InnerText == "-1" ||
                           xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/MarRun").InnerText == "1")
                        {
                            chkmar.Checked = true;
                        }
                        if (xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/AprRun").InnerText == "-1" ||
                           xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/AprRun").InnerText == "1")
                        {
                            chkapr.Checked = true;
                        }
                        if (xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/MayRun").InnerText == "-1" ||
                           xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/MayRun").InnerText == "1")
                        {
                            chkmay.Checked = true;
                        }
                        if (xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/JunRun").InnerText == "-1" ||
                           xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/JunRun").InnerText == "1")
                        {
                            chkjun.Checked = true;
                        }
                        if (xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/JulRun").InnerText == "-1" ||
                           xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/JulRun").InnerText == "1")
                        {
                            chkjul.Checked = true;
                        }
                        if (xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/AugRun").InnerText == "-1" ||
                           xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/AugRun").InnerText == "1")
                        {
                            chkaug.Checked = true;
                        }
                        if (xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/SepRun").InnerText == "-1" ||
                           xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/SepRun").InnerText == "1")
                        {
                            chksep.Checked = true;
                        }
                        if (xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/OctRun").InnerText == "-1" ||
                           xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/OctRun").InnerText == "1")
                        {
                            chkoct.Checked = true;
                        }
                        if (xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/NovRun").InnerText == "-1" ||
                           xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/NovRun").InnerText == "1")
                        {
                            chknov.Checked = true;
                        }
                        if (xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/DecRun").InnerText == "-1" ||
                           xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/DecRun").InnerText == "1")
                        {
                            chkdec.Checked = true;
                        }
                    }

                    ddloutputtype.SelectedValue = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/OutputType").InnerText;
                    ddlnotification.SelectedValue = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/NotificationType").InnerText;
                    txtemail.Text = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/NotifyEmail").InnerText;                    
                }
                catch (Exception ee)
                {
                    ErrorHelper.logErrors(ee);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ee, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                }
            }
        }

        protected String GenerateCriteriaHTML(String sReturn)
        {
            String GenerateOSHAHTML = String.Empty;
            String lType = String.Empty;

            lType = GetReportType(ReportValue, sReturn);

            switch (lType)
            {
                case "1":
                case "3":
                case "4":
                case "5":
                    GenerateOSHAHTML = GenerateOSHACriteriaHTML();
                    break;
                default:
                    GenerateOSHAHTML = "Your report Criteria HTML generating function (call) here";
                    break;
            }
            return GenerateOSHAHTML;
        }

        public String GetReportType(String ReportValue, String sReturn)
        {
            XmlDocument xmlobj = new XmlDocument();
            XmlElement objNode;
            String Type = String.Empty;

            xmlobj.LoadXml(sReturn);

            objNode = (XmlElement)xmlobj.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report");

            if (objNode != null)
                Type = objNode.GetAttribute("type");

            return Type;
        }

        protected String GenerateOSHACriteriaHTML()
        {            
            XmlElement objNode = null;
            String OSHAXML = String.Empty;
            String XmlData = String.Empty;
            String OSHAType = String.Empty;

            XmlData = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML").InnerXml;
            OSHAType = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report").Attributes[0].Value;

            XslCompiledTransform transform = new XslCompiledTransform();
            transform.Load(WizardPath + "\\oshareportdata.xsl");
            StringWriter results = new StringWriter();
            using (XmlReader reader = XmlReader.Create(new StringReader(XmlData)))
            {
                transform.Transform(reader, null, results);
            }
            objResult.LoadXml(results.ToString());

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'byoshaestablishmentflag'][@value='false']");
            if (objNode != null)
                objNode.SetAttribute("onClick", "byEstablishmentFlagChanged()");

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'byoshaestablishmentflag'][@value='true']");
            if (objNode != null)
                objNode.SetAttribute("onClick", "byEstablishmentFlagChanged()");

            objNode = (XmlElement)objResult.SelectSingleNode("//select[@name = 'reportlevel']");
            objNode.SetAttribute("onClick", "reportlevelChanged()");

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'selectedentitiesbtn']");
            objNode.SetAttribute("onClick", "selectReportTarget()");

            if (OSHAType == "1")
                OSHAXML = LoadOSHA300Report();
            else if (OSHAType == "3")
                OSHAXML = LoadOSHA300AReport();
            else if (OSHAType == "4")
                OSHAXML = LoadOSHA301Report();
            else if (OSHAType == "5")
                OSHAXML = LoadOSHASharpsReport();

            return OSHAXML;
        }        

        protected String LoadOSHA300Report()
        {
            XmlNodeList objNodeList = null;
            XmlNode objElementNode = null;
            XmlAttributeCollection objAttributeCollection = null;
            XmlElement objopt = null;
            XmlElement objNode = null;
            XmlElement objInnerNode = null;
            String XML = String.Empty;
            long iSelected = 0;
            long CurrentYear = Convert.ToInt64(DateTime.Now.ToString("yyyy"));
            int i;
            int k;

            objNode = (XmlElement)objResult.SelectSingleNode("//select[@name = 'yearofreport']");
            i = 2002;

            if (objNode.GetAttribute("codeid") != "")
                iSelected = Convert.ToInt64(objNode.GetAttribute("codeid"));

            if (iSelected == 0 || i > iSelected)
                iSelected = CurrentYear;

            for (i = 2002; i <= CurrentYear; i++)
            {
                objopt = objResult.CreateElement("option");
                objopt.SetAttribute("value", i.ToString());

                if (i == Convert.ToInt32(xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[2].ChildNodes[1].Attributes[3].InnerText))
                    objopt.SetAttribute("selected", "");
                objopt.InnerText = i.ToString();
                objNode.AppendChild(objopt);
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'byoshaestablishmentflag'][@value='true']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[0].ChildNodes[0];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'byoshaestablishmentflag'][@value='false']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[1].ChildNodes[0];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            for (int j = 1; j <= 3; j++)
            {
                if (j == 1)
                    objNode = (XmlElement)objResult.SelectSingleNode("//select[@name = 'usereportlevel']");
                else if (j == 2)
                    objNode = (XmlElement)objResult.SelectSingleNode("//select[@name = 'reportlevel']");
                else if (j == 3)
                    objNode = (XmlElement)objResult.SelectSingleNode("//select[@name = 'establishmentnameprefix']");

                objNodeList = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[1].ChildNodes[j].ChildNodes;

                k = 0;
                foreach (XmlElement objNodeData in objNodeList)
                {
                    objAttributeCollection = objNodeData.Attributes;

                    foreach (XmlAttribute objAttribute in objAttributeCollection)
                    {
                        if (objAttribute != null && objAttribute.Name == "selected")
                        {
                            if (j == 1)
                            {
                                objInnerNode = (XmlElement)objNode.SelectNodes("//select[@name = 'usereportlevel']//option")[k];
                                objInnerNode.SetAttribute("selected", "true");
                            }
                            else if (j == 2)
                            {
                                objInnerNode = (XmlElement)objNode.SelectNodes("//select[@name = 'reportlevel']//option")[k];
                                objInnerNode.SetAttribute("selected", "true");
                            }
                            else if (j == 3)
                            {
                                objInnerNode = (XmlElement)objNode.SelectNodes("//select[@name = 'establishmentnameprefix']//option")[k];
                                objInnerNode.SetAttribute("selected", "true");
                            }
                            break;
                        }
                    }
                    k++;
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'datemethod'][@value = 'useyear']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[2].ChildNodes[0];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'datemethod'][@value = 'userange']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[2].ChildNodes[2];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'begindate']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[2].ChildNodes[3];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "value" && objAttribute.Value != "")
                {
                    DateTime dt = DateTime.ParseExact(objAttribute.Value, "yyyyMMdd", null);
                    objNode.SetAttribute("value", dt.ToShortDateString());
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'enddate']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[2].ChildNodes[4];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "value" && objAttribute.Value != "")
                {
                    DateTime dt = DateTime.ParseExact(objAttribute.Value, "yyyyMMdd", null);
                    objNode.SetAttribute("value", dt.ToShortDateString());
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'datemethod'][@value = 'userelative']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[3].ChildNodes[0];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'timescalar']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[3].ChildNodes[1];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "value")
                {
                    objNode.SetAttribute("value", objAttribute.Value);
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//select[@name = 'timeunit']");
            objNodeList = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[3].ChildNodes[2].ChildNodes;

            k = 0;
            foreach (XmlElement objNodeData in objNodeList)
            {
                objAttributeCollection = objNodeData.Attributes;

                foreach (XmlAttribute objAttribute in objAttributeCollection)
                {
                    if (objAttribute != null && objAttribute.Name == "selected")
                    {
                        objInnerNode = (XmlElement)objNode.SelectNodes("//select[@name = 'timeunit']//option")[k];
                        objInnerNode.SetAttribute("selected", "true");
                        break;
                    }
                }
                k++;
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'asofdate']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[0];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "value" && objAttribute.Value != "")
                {
                    DateTime dt = DateTime.ParseExact(objAttribute.Value, "yyyyMMdd", null);
                    objNode.SetAttribute("value", dt.ToShortDateString());
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'enforce180dayrule']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[1];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'printsofterrlog']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[2];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'eventbasedflag']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[3];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'printoshadescflag']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[4];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//select[@name = 'columnesource']");
            objNodeList = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[5].ChildNodes;

            k = 0;
            foreach (XmlElement objNodeData in objNodeList)
            {
                objAttributeCollection = objNodeData.Attributes;

                foreach (XmlAttribute objAttribute in objAttributeCollection)
                {
                    if (objAttribute != null && objAttribute.Name == "selected")
                    {
                        objInnerNode = (XmlElement)objNode.SelectNodes("//select[@name = 'columnesource']//option")[k];
                        objInnerNode.SetAttribute("selected", "true");
                        break;
                    }
                }
                k++;
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//select[@name = 'columnfsource']");
            objNodeList = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[6].ChildNodes;

            k = 0;
            foreach (XmlElement objNodeData in objNodeList)
            {
                objAttributeCollection = objNodeData.Attributes;

                foreach (XmlAttribute objAttribute in objAttributeCollection)
                {
                    if (objAttribute != null && objAttribute.Name == "selected")
                    {
                        objInnerNode = (XmlElement)objNode.SelectNodes("//select[@name = 'columnfsource']//option")[k];
                        objInnerNode.SetAttribute("selected", "true");
                        break;
                    }
                }
                k++;
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//select[@name = 'sortorder']");
            objNodeList = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[7].ChildNodes;

            k = 0;
            foreach (XmlElement objNodeData in objNodeList)
            {
                objAttributeCollection = objNodeData.Attributes;

                foreach (XmlAttribute objAttribute in objAttributeCollection)
                {
                    if (objAttribute != null && objAttribute.Name == "selected")
                    {
                        objInnerNode = (XmlElement)objNode.SelectNodes("//select[@name = 'sortorder']//option")[k];
                        objInnerNode.SetAttribute("selected", "true");
                        break;
                    }
                }
                k++;
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'allentitiesflag'][@value = 'true']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[8];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'allentitiesflag'][@value = 'false']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[9];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            //objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[10];
            //objNodeList = objElementNode.ChildNodes;

            //foreach (XmlElement objNodeData in objNodeList)
            //{
            //    if (objNodeData != null && objNodeData.InnerXml != "")
            //    {
            //        objInnerNode = objResult.CreateElement("option");
            //        objInnerNode.SetAttribute("value", objNodeData.Attributes[0].Value);
            //        objInnerNode.InnerXml = objNodeData.InnerXml;
            //        objNode = (XmlElement)objResult.SelectSingleNode("//select[@name = 'selectedentities']");
            //        objNode.AppendChild(objInnerNode);
            //    }
            //}

            return XML = objResult.InnerXml;
        }

        protected String LoadOSHA300AReport()
        {
            XmlNodeList objNodeList = null;
            XmlNode objElementNode = null;
            XmlAttributeCollection objAttributeCollection = null;
            XmlElement objopt = null;
            XmlElement objNode = null;
            XmlElement objInnerNode = null;
            String XML = String.Empty;
            long iSelected = 0;
            long CurrentYear = Convert.ToInt64(DateTime.Now.ToString("yyyy"));
            int i;
            int k;

            objNode = (XmlElement)objResult.SelectSingleNode("//select[@name = 'yearofreport']");
            i = 2002;

            if (objNode.GetAttribute("codeid") != "")
                iSelected = Convert.ToInt64(objNode.GetAttribute("codeid"));

            if (iSelected == 0 || i > iSelected)
                iSelected = CurrentYear;

            for (i = 2002; i <= CurrentYear; i++)
            {
                objopt = objResult.CreateElement("option");
                objopt.SetAttribute("value", i.ToString());

                if (i == Convert.ToInt32(xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[3].ChildNodes[0].Attributes[3].InnerText))
                    objopt.SetAttribute("selected", "");
                objopt.InnerText = i.ToString();
                objNode.AppendChild(objopt);
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'preparertitle']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[0].ChildNodes[0];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "value")
                {
                    objNode.SetAttribute("value", objAttribute.Value);
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'preparername']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[0].ChildNodes[1];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "value")
                {
                    objNode.SetAttribute("value", objAttribute.Value);
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'preparerphone']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[0].ChildNodes[2];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "value")
                {
                    objNode.SetAttribute("value", objAttribute.Value);
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'preparersignaturedate']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[0].ChildNodes[3];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "value" && objAttribute.Value != "")
                {
                    DateTime dt = DateTime.ParseExact(objAttribute.Value, "yyyyMMdd", null);
                    objNode.SetAttribute("value", dt.ToShortDateString());
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'byoshaestablishmentflag'][@value='true']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[1].ChildNodes[0];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'byoshaestablishmentflag'][@value='false']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[2].ChildNodes[0];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            for (int j = 1; j <= 3; j++)
            {
                if (j == 1)
                    objNode = (XmlElement)objResult.SelectSingleNode("//select[@name = 'usereportlevel']");
                else if (j == 2)
                    objNode = (XmlElement)objResult.SelectSingleNode("//select[@name = 'reportlevel']");
                else if (j == 3)
                    objNode = (XmlElement)objResult.SelectSingleNode("//select[@name = 'establishmentnameprefix']");

                objNodeList = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[2].ChildNodes[j].ChildNodes;

                k = 0;
                foreach (XmlElement objNodeData in objNodeList)
                {
                    objAttributeCollection = objNodeData.Attributes;

                    foreach (XmlAttribute objAttribute in objAttributeCollection)
                    {
                        if (objAttribute != null && objAttribute.Name == "selected")
                        {
                            if (j == 1)
                            {
                                objInnerNode = (XmlElement)objNode.SelectNodes("//select[@name = 'usereportlevel']//option")[k];
                                objInnerNode.SetAttribute("selected", "true");
                            }
                            else if (j == 2)
                            {
                                objInnerNode = (XmlElement)objNode.SelectNodes("//select[@name = 'reportlevel']//option")[k];
                                objInnerNode.SetAttribute("selected", "true");
                            }
                            else if (j == 3)
                            {
                                objInnerNode = (XmlElement)objNode.SelectNodes("//select[@name = 'establishmentnameprefix']//option")[k];
                                objInnerNode.SetAttribute("selected", "true");
                            }
                            break;
                        }
                    }
                    k++;
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'datemethod'][@value = 'useyear']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[3].ChildNodes[1];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'datemethod'][@value = 'userange']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[3].ChildNodes[2];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'begindate']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[3].ChildNodes[3];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "value" && objAttribute.Value != "")
                {
                    DateTime dt = DateTime.ParseExact(objAttribute.Value, "yyyyMMdd", null);
                    objNode.SetAttribute("value", dt.ToShortDateString());
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'enddate']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[3].ChildNodes[4];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "value" && objAttribute.Value != "")
                {
                    DateTime dt = DateTime.ParseExact(objAttribute.Value, "yyyyMMdd", null);
                    objNode.SetAttribute("value", dt.ToShortDateString());
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'datemethod'][@value = 'userelative']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[0];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'timescalar']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[1];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "value")
                {
                    objNode.SetAttribute("value", objAttribute.Value);
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//select[@name = 'timeunit']");
            objNodeList = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[2].ChildNodes;

            k = 0;
            foreach (XmlElement objNodeData in objNodeList)
            {
                objAttributeCollection = objNodeData.Attributes;

                foreach (XmlAttribute objAttribute in objAttributeCollection)
                {
                    if (objAttribute != null && objAttribute.Name == "selected")
                    {
                        objInnerNode = (XmlElement)objNode.SelectNodes("//select[@name = 'timeunit']//option")[k];
                        objInnerNode.SetAttribute("selected", "true");
                        break;
                    }
                }
                k++;
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'asofdate']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[5].ChildNodes[0];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "value" && objAttribute.Value != "")
                {
                    DateTime dt = DateTime.ParseExact(objAttribute.Value, "yyyyMMdd", null);
                    objNode.SetAttribute("value", dt.ToShortDateString());
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'enforce180dayrule']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[5].ChildNodes[1];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'printsofterrlog']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[5].ChildNodes[2];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'eventbasedflag']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[5].ChildNodes[3];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'printoshadescflag']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[5].ChildNodes[4];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'primarylocationflag']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[5].ChildNodes[5];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'allentitiesflag'][@value = 'true']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[5].ChildNodes[6];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'allentitiesflag'][@value = 'false']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[5].ChildNodes[7];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            //objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[5].ChildNodes[8];
            //objNodeList = objElementNode.ChildNodes;

            //foreach (XmlElement objNodeData in objNodeList)
            //{
            //    if (objNodeData != null && objNodeData.InnerXml != "")
            //    {
            //        objInnerNode = objResult.CreateElement("option");
            //        objInnerNode.SetAttribute("value", objNodeData.Attributes[0].Value);
            //        objInnerNode.InnerXml = objNodeData.InnerXml;
            //        objNode = (XmlElement)objResult.SelectSingleNode("//select[@name = 'selectedentities']");
            //        objNode.AppendChild(objInnerNode);
            //    }
            //}

            return XML = objResult.InnerXml;
        }

        protected String LoadOSHA301Report()
        {
            XmlNodeList objNodeList = null;
            XmlNode objElementNode = null;
            XmlAttributeCollection objAttributeCollection = null;
            XmlElement objopt = null;
            XmlElement objNode = null;
            XmlElement objInnerNode = null;
            String XML = String.Empty;
            long iSelected = 0;
            long CurrentYear = Convert.ToInt64(DateTime.Now.ToString("yyyy"));
            int i;
            int k;

            objNode = (XmlElement)objResult.SelectSingleNode("//select[@name = 'yearofreport']");
            i = 2002;

            if (objNode.GetAttribute("codeid") != "")
                iSelected = Convert.ToInt64(objNode.GetAttribute("codeid"));

            if (iSelected == 0 || i > iSelected)
                iSelected = CurrentYear;

            for (i = 2002; i <= CurrentYear; i++)
            {
                objopt = objResult.CreateElement("option");
                objopt.SetAttribute("value", i.ToString());

                if (i == Convert.ToInt32(xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[3].ChildNodes[0].Attributes[3].InnerText))
                    objopt.SetAttribute("selected", "");
                objopt.InnerText = i.ToString();
                objNode.AppendChild(objopt);
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'preparertitle']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[0].ChildNodes[0];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "value")
                {
                    objNode.SetAttribute("value", objAttribute.Value);
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'preparername']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[0].ChildNodes[1];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "value")
                {
                    objNode.SetAttribute("value", objAttribute.Value);
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'preparerphone']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[0].ChildNodes[2];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "value")
                {
                    objNode.SetAttribute("value", objAttribute.Value);
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'byoshaestablishmentflag'][@value='true']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[1].ChildNodes[0];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'byoshaestablishmentflag'][@value='false']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[2].ChildNodes[0];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            for (int j = 1; j <= 2; j++)
            {
                if (j == 1)
                    objNode = (XmlElement)objResult.SelectSingleNode("//select[@name = 'usereportlevel']");
                else if (j == 2)
                    objNode = (XmlElement)objResult.SelectSingleNode("//select[@name = 'reportlevel']");

                objNodeList = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[2].ChildNodes[j].ChildNodes;

                k = 0;
                foreach (XmlElement objNodeData in objNodeList)
                {
                    objAttributeCollection = objNodeData.Attributes;

                    foreach (XmlAttribute objAttribute in objAttributeCollection)
                    {
                        if (objAttribute != null && objAttribute.Name == "selected")
                        {
                            if (j == 1)
                            {
                                objInnerNode = (XmlElement)objNode.SelectNodes("//select[@name = 'usereportlevel']//option")[k];
                                objInnerNode.SetAttribute("selected", "true");
                            }
                            else if (j == 2)
                            {
                                objInnerNode = (XmlElement)objNode.SelectNodes("//select[@name = 'reportlevel']//option")[k];
                                objInnerNode.SetAttribute("selected", "true");
                            }
                            break;
                        }
                    }
                    k++;
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'datemethod'][@value = 'useyear']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[3].ChildNodes[1];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'datemethod'][@value = 'userange']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[3].ChildNodes[2];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'begindate']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[3].ChildNodes[3];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "value" && objAttribute.Value != "")
                {
                    DateTime dt = DateTime.ParseExact(objAttribute.Value, "yyyyMMdd", null);
                    objNode.SetAttribute("value", dt.ToShortDateString());
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'enddate']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[3].ChildNodes[4];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "value" && objAttribute.Value != "")
                {
                    DateTime dt = DateTime.ParseExact(objAttribute.Value, "yyyyMMdd", null);
                    objNode.SetAttribute("value", dt.ToShortDateString());
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'datemethod'][@value = 'userelative']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[0];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'timescalar']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[1];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "value")
                {
                    objNode.SetAttribute("value", objAttribute.Value);
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//select[@name = 'timeunit']");
            objNodeList = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[2].ChildNodes;

            k = 0;
            foreach (XmlElement objNodeData in objNodeList)
            {
                objAttributeCollection = objNodeData.Attributes;

                foreach (XmlAttribute objAttribute in objAttributeCollection)
                {
                    if (objAttribute != null && objAttribute.Name == "selected")
                    {
                        objInnerNode = (XmlElement)objNode.SelectNodes("//select[@name = 'timeunit']//option")[k];
                        objInnerNode.SetAttribute("selected", "true");
                        break;
                    }
                }
                k++;
            }            

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'printsofterrlog']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[5].ChildNodes[0];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'eventbasedflag']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[5].ChildNodes[1];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'printoshadescflag']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[5].ChildNodes[2];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'primarylocationflag']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[5].ChildNodes[3];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'allentitiesflag'][@value = 'true']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[5].ChildNodes[4];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'allentitiesflag'][@value = 'false']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[5].ChildNodes[5];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            //objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[5].ChildNodes[6];
            //objNodeList = objElementNode.ChildNodes;

            //foreach (XmlElement objNodeData in objNodeList)
            //{
            //    if (objNodeData != null && objNodeData.InnerXml != "")
            //    {
            //        objInnerNode = objResult.CreateElement("option");
            //        objInnerNode.SetAttribute("value", objNodeData.Attributes[0].Value);
            //        objInnerNode.InnerXml = objNodeData.InnerXml;
            //        objNode = (XmlElement)objResult.SelectSingleNode("//select[@name = 'selectedentities']");
            //        objNode.AppendChild(objInnerNode);
            //    }
            //}

            return XML = objResult.InnerXml;
        }

        protected String LoadOSHASharpsReport()
        {
            XmlNodeList objNodeList = null;
            XmlNode objElementNode = null;
            XmlAttributeCollection objAttributeCollection = null;
            XmlElement objopt = null;
            XmlElement objNode = null;
            XmlElement objInnerNode = null;
            String XML = String.Empty;
            long iSelected = 0;
            long CurrentYear = Convert.ToInt64(DateTime.Now.ToString("yyyy"));
            int i;
            int k;

            objNode = (XmlElement)objResult.SelectSingleNode("//select[@name = 'yearofreport']");
            i = 2002;

            if (objNode.GetAttribute("codeid") != "")
                iSelected = Convert.ToInt64(objNode.GetAttribute("codeid"));

            if (iSelected == 0 || i > iSelected)
                iSelected = CurrentYear;

            for (i = 2002; i <= CurrentYear; i++)
            {
                objopt = objResult.CreateElement("option");
                objopt.SetAttribute("value", i.ToString());

                if (i == Convert.ToInt32(xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[2].ChildNodes[0].Attributes[3].InnerText))
                    objopt.SetAttribute("selected", "");
                objopt.InnerText = i.ToString();
                objNode.AppendChild(objopt);
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'byoshaestablishmentflag'][@value='true']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[0].ChildNodes[0];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'byoshaestablishmentflag'][@value='false']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[1].ChildNodes[0];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            for (int j = 1; j <= 3; j++)
            {
                if (j == 1)
                    objNode = (XmlElement)objResult.SelectSingleNode("//select[@name = 'usereportlevel']");
                else if (j == 2)
                    objNode = (XmlElement)objResult.SelectSingleNode("//select[@name = 'reportlevel']");
                else if (j == 3)
                    objNode = (XmlElement)objResult.SelectSingleNode("//select[@name = 'establishmentnameprefix']");

                objNodeList = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[1].ChildNodes[j].ChildNodes;

                k = 0;
                foreach (XmlElement objNodeData in objNodeList)
                {
                    objAttributeCollection = objNodeData.Attributes;

                    foreach (XmlAttribute objAttribute in objAttributeCollection)
                    {
                        if (objAttribute != null && objAttribute.Name == "selected")
                        {
                            if (j == 1)
                            {
                                objInnerNode = (XmlElement)objNode.SelectNodes("//select[@name = 'usereportlevel']//option")[k];
                                objInnerNode.SetAttribute("selected", "true");
                            }
                            else if (j == 2)
                            {
                                objInnerNode = (XmlElement)objNode.SelectNodes("//select[@name = 'reportlevel']//option")[k];
                                objInnerNode.SetAttribute("selected", "true");
                            }
                            else if (j == 3)
                            {
                                objInnerNode = (XmlElement)objNode.SelectNodes("//select[@name = 'establishmentnameprefix']//option")[k];
                                objInnerNode.SetAttribute("selected", "true");
                            }
                            break;
                        }
                    }
                    k++;
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'datemethod'][@value = 'useyear']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[2].ChildNodes[1];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'datemethod'][@value = 'userange']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[2].ChildNodes[2];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'begindate']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[2].ChildNodes[3];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "value" && objAttribute.Value != "")
                {
                    DateTime dt = DateTime.ParseExact(objAttribute.Value, "yyyyMMdd", null);
                    objNode.SetAttribute("value", dt.ToShortDateString());
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'enddate']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[2].ChildNodes[4];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "value" && objAttribute.Value != "")
                {
                    DateTime dt = DateTime.ParseExact(objAttribute.Value, "yyyyMMdd", null);
                    objNode.SetAttribute("value", dt.ToShortDateString());
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'datemethod'][@value = 'userelative']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[3].ChildNodes[0];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'timescalar']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[3].ChildNodes[1];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "value")
                {
                    objNode.SetAttribute("value", objAttribute.Value);
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//select[@name = 'timeunit']");
            objNodeList = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[3].ChildNodes[2].ChildNodes;

            k = 0;
            foreach (XmlElement objNodeData in objNodeList)
            {
                objAttributeCollection = objNodeData.Attributes;

                foreach (XmlAttribute objAttribute in objAttributeCollection)
                {
                    if (objAttribute != null && objAttribute.Name == "selected")
                    {
                        objInnerNode = (XmlElement)objNode.SelectNodes("//select[@name = 'timeunit']//option")[k];
                        objInnerNode.SetAttribute("selected", "true");
                        break;
                    }
                }
                k++;
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'printsofterrlog']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[0];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'eventbasedflag']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[1];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'primarylocationflag']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[2];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'useemployeenames']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[3];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'allentitiesflag'][@value = 'true']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[4];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            objNode = (XmlElement)objResult.SelectSingleNode("//input[@name = 'allentitiesflag'][@value = 'false']");
            objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[5];
            objAttributeCollection = objElementNode.Attributes;

            foreach (XmlAttribute objAttribute in objAttributeCollection)
            {
                if (objAttribute != null && objAttribute.Name == "checked")
                {
                    objNode.SetAttribute("checked", "true");
                }
            }

            //objElementNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[6];
            //objNodeList = objElementNode.ChildNodes;

            //foreach (XmlElement objNodeData in objNodeList)
            //{
            //    if (objNodeData != null && objNodeData.InnerXml != "")
            //    {
            //        objInnerNode = objResult.CreateElement("option");
            //        objInnerNode.SetAttribute("value", objNodeData.Attributes[0].Value);
            //        objInnerNode.InnerXml = objNodeData.InnerXml;
            //        objNode = (XmlElement)objResult.SelectSingleNode("//select[@name = 'selectedentities']");
            //        objNode.AppendChild(objInnerNode);
            //    }
            //}

            return XML = objResult.InnerXml;
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                SavingData();
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void SavingData()
        {
            String[] SaveData1;
            String[] SaveData2;
            StringBuilder SaveXml = new StringBuilder();
            String OutputPathURL;
            String OutputPath;
            DateTime DateTime = new DateTime();
            String ReturnXml = String.Empty;
            String OSHAType = String.Empty;
            String ScheduleID = String.Empty;

            // akaushik5 CHanged for MITS 38389 Starts
            //OutputPathURL = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/SMNetServer").InnerText + "/RiskmasterUI/UI/SortMaster/SMRepView.aspx?DPT=0";
            OutputPathURL = "/RiskmasterUI/UI/SortMaster/SMRepView.aspx?DPT=0";
            // akaushik5 CHanged for MITS 38389 Ends
            OutputPath = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/SMDataPath").InnerText;
            OSHAType = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report").Attributes[0].Value;
            ScheduleID = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/TMScheduleID").InnerText;

            SaveData1 = Hidden1.Value.Split(';');
            SaveData2 = Hidden2.Value.Split(';');

            //if (OSHAType == "1")
            //    PostOSHA300Report(SaveData1, SaveData2);
            //else if (OSHAType == "3")
            //    PostOSHA300AReport(SaveData1, SaveData2);
            //else if (OSHAType == "4")
            //    PostOSHA301Report(SaveData1, SaveData2);
            //else if (OSHAType == "5")
            //    PostOSHASharpsReport(SaveData1, SaveData2);


            if (txtnextrun.Text != "")
                DateTime = Convert.ToDateTime(txtnextrun.Text);
            else
                DateTime = DateTime.Now;

            SaveXml.Append("<Message>");
            SaveXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            SaveXml.Append("<Call>");
            SaveXml.Append("<Function>SortMasterAdaptor.ScheduleReports</Function>");
            SaveXml.Append("</Call>");
            SaveXml.Append("<Document>");
            SaveXml.Append("<Details>");
            if (TaskTypeText != "")
            {
                SaveXml.Append("<TaskTypeText>" + TaskTypeText.Split(',')[0] + "</TaskTypeText>");
                SaveXml.Append("<TaskNameLabel>" + TaskTypeText.Split(',')[0] + "</TaskNameLabel>");
            }
            else
            {
                SaveXml.Append("<TaskTypeText></TaskTypeText>");
                SaveXml.Append("<TaskNameLabel></TaskNameLabel>");
            }
            SaveXml.Append("<ScheduleTypeId></ScheduleTypeId>");
            SaveXml.Append("<ScheduleTypeText></ScheduleTypeText>");
            SaveXml.Append("<Date>" + DateTime.ToShortDateString() + "</Date>");
            SaveXml.Append("<Month>" + DateTime.ToString("MM") + "</Month>");

            if (ddlscheduletype.Text == "daily")
                SaveXml.Append("<DayOfMonth></DayOfMonth>");
            else if (ddlscheduletype.Text == "monthly")
                SaveXml.Append("<DayOfMonth>" + DateTime.ToString("dd") + "</DayOfMonth>");
            SaveXml.Append("<Time>" + txtnexttimerun.Text + "</Time>");
            SaveXml.Append("<bParams>false</bParams>");
            SaveXml.Append("<Arguments></Arguments>");
            SaveXml.Append("<ZeroBasedFinHist>false</ZeroBasedFinHist>");
            SaveXml.Append("<ClaimBasedFinHist></ClaimBasedFinHist>");
            SaveXml.Append("<EventBasedFinHist></EventBasedFinHist>");
            SaveXml.Append("<RecreateFinHist>false</RecreateFinHist>");
            SaveXml.Append("<CreateLog>false</CreateLog>");
            SaveXml.Append("<BillingOption>Installment</BillingOption>");
            SaveXml.Append("<OverDueDays></OverDueDays>");
            SaveXml.Append("<CreateSysDiary>false</CreateSysDiary>");
            SaveXml.Append("<SendEmailNotify>false</SendEmailNotify>");
            SaveXml.Append("<BothDiaryAndEmail>false</BothDiaryAndEmail>");
            SaveXml.Append("<PolicySystemList>");
            SaveXml.Append("<PolicySystems></PolicySystems>");
            SaveXml.Append("</PolicySystemList>");
            SaveXml.Append("<AdminLogin></AdminLogin>");
            SaveXml.Append("<AdminPassword></AdminPassword>");
            SaveXml.Append("<IsFroiBatch>false</IsFroiBatch>");
            SaveXml.Append("<IsAcordBatch>false</IsAcordBatch>");
            SaveXml.Append("<ScheduleId>" + ScheduleID + "</ScheduleId>");
            SaveXml.Append("<saved></saved>");

            if(TaskType != "")
                SaveXml.Append("<TaskType>" + TaskType + "</TaskType>");
            else
                SaveXml.Append("<TaskType></TaskType>");

            SaveXml.Append("<UserArguments></UserArguments>");
            if (TaskTypeText != "")
                SaveXml.Append("<TaskName>" + TaskTypeText.Split(',')[0] + "</TaskName>");
            else
                SaveXml.Append("<TaskName></TaskName>");

            SaveXml.Append("<IsDataIntegratorTask>0</IsDataIntegratorTask>");
            SaveXml.Append("<OptionsetId></OptionsetId>");
            SaveXml.Append("<SystemModuleName>NoSystemModuleName</SystemModuleName>");

            SaveXml.Append("<ScheduleIdSM>" + ScheduleIdSM + "</ScheduleIdSM>");
            SaveXml.Append("<ReportId>" + ReportValue + "</ReportId>");
            SaveXml.Append("<LastRunDTTM>" + LastRunDTTM + "</LastRunDTTM>");
            SaveXml.Append("<Time></Time>");

            if (ddlscheduletype.Text == "daily")
                SaveXml.Append("<ScheduleTypeIdSM>1</ScheduleTypeIdSM>");
            else if (ddlscheduletype.Text == "monthly")
                SaveXml.Append("<ScheduleTypeIdSM>2</ScheduleTypeIdSM>");

            if (ddlscheduletype.Text == "daily")
            {
                SaveXml.Append("<Mon_Run>" + chkmon.Checked + "</Mon_Run>");
                SaveXml.Append("<Tue_Run>" + chktue.Checked + "</Tue_Run>");
                SaveXml.Append("<Wed_Run>" + chkwed.Checked + "</Wed_Run>");
                SaveXml.Append("<Thu_Run>" + chkthu.Checked + "</Thu_Run>");
                SaveXml.Append("<Fri_Run>" + chkfri.Checked + "</Fri_Run>");
                SaveXml.Append("<Sat_Run>" + chksat.Checked + "</Sat_Run>");
                SaveXml.Append("<Sun_Run>" + chksun.Checked + "</Sun_Run>");

                SaveXml.Append("<January>false</January>");
                SaveXml.Append("<February>false</February>");
                SaveXml.Append("<March>false</March>");
                SaveXml.Append("<April>false</April>");
                SaveXml.Append("<May>false</May>");
                SaveXml.Append("<June>false</June>");
                SaveXml.Append("<July>false</July>");
                SaveXml.Append("<August>false</August>");
                SaveXml.Append("<September>false</September>");
                SaveXml.Append("<October>false</October>");
                SaveXml.Append("<November>false</November>");
                SaveXml.Append("<December>false</December>");
                SaveXml.Append("<Month>false</Month>");
            }
            else if (ddlscheduletype.Text == "monthly")
            {
                SaveXml.Append("<Mon_Run>false</Mon_Run>");
                SaveXml.Append("<Tue_Run>false</Tue_Run>");
                SaveXml.Append("<Wed_Run>false</Wed_Run>");
                SaveXml.Append("<Thu_Run>false</Thu_Run>");
                SaveXml.Append("<Fri_Run>false</Fri_Run>");
                SaveXml.Append("<Sat_Run>false</Sat_Run>");
                SaveXml.Append("<Sun_Run>false</Sun_Run>");

                SaveXml.Append("<Month></Month>");

                SaveXml.Append("<January>" + chkjan.Checked + "</January>");
                SaveXml.Append("<February>" + chkfeb.Checked + "</February>");
                SaveXml.Append("<March>" + chkmar.Checked + "</March>");
                SaveXml.Append("<April>" + chkapr.Checked + "</April>");
                SaveXml.Append("<May>" + chkmay.Checked + "</May>");
                SaveXml.Append("<June>" + chkjun.Checked + "</June>");
                SaveXml.Append("<July>" + chkjul.Checked + "</July>");
                SaveXml.Append("<August>" + chkaug.Checked + "</August>");
                SaveXml.Append("<September>" + chksep.Checked + "</September>");
                SaveXml.Append("<October>" + chkoct.Checked + "</October>");
                SaveXml.Append("<November>" + chknov.Checked + "</November>");
                SaveXml.Append("<December>" + chkdec.Checked + "</December>");
            }
            SaveXml.Append("<ReportName>" + lblrptname.Text + "</ReportName>");
            SaveXml.Append("<OutputType>" + ddloutputtype.Text + "</OutputType>");
            SaveXml.Append("<OutputPath>" + OutputPath + "</OutputPath>");
            SaveXml.Append("<OutputPathURL>" + OutputPathURL + "</OutputPathURL>");
            SaveXml.Append("<OutputOptions></OutputOptions>");
            SaveXml.Append("<ReportXML>" + xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML").InnerXml + "</ReportXML>");
            SaveXml.Append("<NotificationType>" +ddlnotification.Text + "</NotificationType>");
            SaveXml.Append("<NotifyEmail>" + txtemail.Text + "</NotifyEmail>");
            SaveXml.Append("<NotifyMsg></NotifyMsg>");

            SaveXml.Append("</Details>");
            SaveXml.Append("</Document>");
            SaveXml.Append("</Message>");

            ReturnXml = AppHelper.CallCWSService(SaveXml.ToString());

            Response.Redirect("~/UI/SortMaster/ScheduledView.aspx");
        }

        protected void PostOSHA300Report(String[] SaveData1, String[] SaveData2)
        {
            XmlNodeList objNodeList = null;
            XmlElement objElement = null;
            XmlNode objNode = null;

            if (SaveData1 != null)
            {
                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[0].ChildNodes[0];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[0] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[1].ChildNodes[0];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[1] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objNodeList = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[1].ChildNodes[1].ChildNodes;
                foreach (XmlElement NodeList in objNodeList)
                {
                    if (NodeList.Attributes.Count > 1)
                    {
                        NodeList.RemoveAttribute("selected");
                    }
                }
                foreach (XmlElement NodeList in objNodeList)
                {
                    if (NodeList.InnerXml == SaveData1[2])
                    {
                        NodeList.SetAttribute("selected", "");
                        break;
                    }
                }

                objNodeList = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[1].ChildNodes[2].ChildNodes;
                foreach (XmlElement NodeList in objNodeList)
                {
                    if (NodeList.Attributes.Count > 1)
                    {
                        NodeList.RemoveAttribute("selected");
                    }
                }
                foreach (XmlElement NodeList in objNodeList)
                {
                    if (NodeList.InnerXml == SaveData1[3])
                    {
                        NodeList.SetAttribute("selected", "");
                        break;
                    }
                }

                objNodeList = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[1].ChildNodes[3].ChildNodes;
                foreach (XmlElement NodeList in objNodeList)
                {
                    if (NodeList.Attributes.Count > 1)
                    {
                        NodeList.RemoveAttribute("selected");
                    }
                }
                foreach (XmlElement NodeList in objNodeList)
                {
                    if (NodeList.InnerXml == SaveData1[4])
                    {
                        NodeList.SetAttribute("selected", "");
                        break;
                    }
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[2].ChildNodes[0];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[5] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[2].ChildNodes[1];
                if (SaveData1[6] != "")
                {
                    objElement.SetAttribute("codeid", SaveData1[6]);
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[2].ChildNodes[2];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[7] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[2].ChildNodes[3];
                if (SaveData1[8] != "")
                {
                    DateTime dt = Convert.ToDateTime(SaveData1[8]);
                    objElement.SetAttribute("value", dt.ToString("yyyyMMdd"));
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[2].ChildNodes[4];
                if (SaveData1[9] != "")
                {
                    DateTime dt = Convert.ToDateTime(SaveData1[9]);
                    objElement.SetAttribute("value", dt.ToString("yyyyMMdd"));
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[3].ChildNodes[0];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[10] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[3].ChildNodes[1];
                objElement.SetAttribute("value", SaveData1[11]);

                //Rakhel - code for adding combox box value
                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[3].ChildNodes[2];
                string CodeID = string.Empty;
                if (SaveData1[24] == "Days")
                {
                    CodeID = "d";
                }
                else if (SaveData1[24] == "Months")
                {
                    CodeID = "m";
                }
                else if (SaveData1[24] == "Years")
                {
                    CodeID = "";
                }
                objElement.SetAttribute("codeid", CodeID);
                //Rakhel - code for adding combox box value

                objNodeList = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[3].ChildNodes[2].ChildNodes;
                foreach (XmlElement NodeList in objNodeList)
                {
                    if (NodeList.Attributes.Count > 1)
                    {
                        NodeList.RemoveAttribute("selected");
                    }
                }
                foreach (XmlElement NodeList in objNodeList)
                {
                    if (NodeList.InnerXml == SaveData1[12])
                    {
                        NodeList.SetAttribute("selected", "");
                        break;
                    }
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[0];
                if (SaveData1[13] != "")
                {
                    DateTime dt = Convert.ToDateTime(SaveData1[13]);
                    objElement.SetAttribute("value", dt.ToString("yyyyMMdd"));
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[1];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[14] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[2];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[15] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[3];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[16] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[4];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[17] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objNodeList = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[5].ChildNodes;
                foreach (XmlElement NodeList in objNodeList)
                {
                    if (NodeList.Attributes.Count > 1)
                    {
                        NodeList.RemoveAttribute("selected");
                    }
                }
                foreach (XmlElement NodeList in objNodeList)
                {
                    if (NodeList.InnerXml == SaveData1[18])
                    {
                        NodeList.SetAttribute("selected", "");
                        break;
                    }
                }

                objNodeList = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[6].ChildNodes;
                foreach (XmlElement NodeList in objNodeList)
                {
                    if (NodeList.Attributes.Count > 1)
                    {
                        NodeList.RemoveAttribute("selected");
                    }
                }
                foreach (XmlElement NodeList in objNodeList)
                {
                    if (NodeList.InnerXml == SaveData1[19])
                    {
                        NodeList.SetAttribute("selected", "");
                        break;
                    }
                }

                objNodeList = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[7].ChildNodes;
                foreach (XmlElement NodeList in objNodeList)
                {
                    if (NodeList.Attributes.Count > 1)
                    {
                        NodeList.RemoveAttribute("selected");
                    }
                }
                foreach (XmlElement NodeList in objNodeList)
                {
                    if (NodeList.InnerXml == SaveData1[20])
                    {
                        NodeList.SetAttribute("selected", "");
                        break;
                    }
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[8];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[21] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[9];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[22] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                if (SaveData2 != null)
                {
                    objNodeList = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[10].ChildNodes;
                    for (int i = objNodeList.Count - 1; i >= 0; i--)
                    {
                        objNodeList[i].ParentNode.RemoveChild(objNodeList[i]);
                    }
                    // Commented below because already loading value and xml in below code
                    /*for (int i = 0; i < SaveData2.Length; i++)
                    {
                        if (SaveData2[i] != "")
                        {
                            objElement = xmlRep.CreateElement("option");
                            objElement.SetAttribute("value", SaveData2[i].Split(':')[1]);
                            objNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[10];
                            objNode.AppendChild(objElement);
                        }
                    }*/
                    for (int i = 0; i < SaveData2.Length; i++)
                    {
                        if (SaveData2[i] != "")
                        {
                            objElement = xmlRep.CreateElement("option");
                            objElement.SetAttribute("value", SaveData2[i].Split(':')[1]);
                            objElement.InnerText = SaveData2[i].Split(':')[0];
                            objNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[10];
                            objNode.AppendChild(objElement);
                        }
                    }
                }
            }
        }

        protected void PostOSHA300AReport(String[] SaveData1, String[] SaveData2)
        {
            XmlNodeList objNodeList = null;
            XmlElement objElement = null;
            XmlNode objNode = null;

            if (SaveData1 != null)
            {
                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[0].ChildNodes[0];
                if (SaveData1[0] != "")
                {
                    objElement.SetAttribute("value", SaveData1[0]);
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[0].ChildNodes[1];
                if (SaveData1[1] != "")
                {
                    objElement.SetAttribute("value", SaveData1[1]);
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[0].ChildNodes[2];
                if (SaveData1[2] != "")
                {
                    objElement.SetAttribute("value", SaveData1[2]);
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[0].ChildNodes[3];
                if (SaveData1[3] != "")
                {
                    DateTime dt = Convert.ToDateTime(SaveData1[3]);
                    objElement.SetAttribute("value", dt.ToString("yyyyMMdd"));
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[1].ChildNodes[0];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[4] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[2].ChildNodes[0];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[5] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objNodeList = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[2].ChildNodes[1].ChildNodes;
                foreach (XmlElement NodeList in objNodeList)
                {
                    if (NodeList.Attributes.Count > 1)
                    {
                        NodeList.RemoveAttribute("selected");
                    }
                }
                foreach (XmlElement NodeList in objNodeList)
                {
                    if (NodeList.InnerXml == SaveData1[6])
                    {
                        NodeList.SetAttribute("selected", "");
                        break;
                    }
                }

                objNodeList = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[2].ChildNodes[2].ChildNodes;
                foreach (XmlElement NodeList in objNodeList)
                {
                    if (NodeList.Attributes.Count > 1)
                    {
                        NodeList.RemoveAttribute("selected");
                    }
                }
                foreach (XmlElement NodeList in objNodeList)
                {
                    if (NodeList.InnerXml == SaveData1[7])
                    {
                        NodeList.SetAttribute("selected", "");
                        break;
                    }
                }

                objNodeList = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[2].ChildNodes[3].ChildNodes;
                foreach (XmlElement NodeList in objNodeList)
                {
                    if (NodeList.Attributes.Count > 1)
                    {
                        NodeList.RemoveAttribute("selected");
                    }
                }
                foreach (XmlElement NodeList in objNodeList)
                {
                    if (NodeList.InnerXml == SaveData1[8])
                    {
                        NodeList.SetAttribute("selected", "");
                        break;
                    }
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[3].ChildNodes[0];
                if (SaveData1[9] != "")
                {
                    objElement.SetAttribute("codeid", SaveData1[9]);
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[3].ChildNodes[1];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[10] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[3].ChildNodes[2];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[11] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[3].ChildNodes[3];
                if (SaveData1[12] != "")
                {
                    DateTime dt = Convert.ToDateTime(SaveData1[12]);
                    objElement.SetAttribute("value", dt.ToString("yyyyMMdd"));
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[3].ChildNodes[4];
                if (SaveData1[13] != "")
                {
                    DateTime dt = Convert.ToDateTime(SaveData1[13]);
                    objElement.SetAttribute("value", dt.ToString("yyyyMMdd"));
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[0];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[14] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[1];
                objElement.SetAttribute("value", SaveData1[15]);

                //Rakhel - code for adding combox box value
                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[2];

                string CodeID = string.Empty;
                if (SaveData1[26] == "Days")
                {
                    CodeID = "d";
                }
                else if (SaveData1[26] == "Months")
                {
                    CodeID = "m";
                }
                else if (SaveData1[26] == "Years")
                {
                    CodeID = "";
                }
                objElement.SetAttribute("codeid", CodeID);
                //Rakhel - code for adding combox box value

                objNodeList = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[2].ChildNodes;
                foreach (XmlElement NodeList in objNodeList)
                {
                    if (NodeList.Attributes.Count > 1)
                    {
                        NodeList.RemoveAttribute("selected");
                    }
                }
                foreach (XmlElement NodeList in objNodeList)
                {
                    if (NodeList.InnerXml == SaveData1[16])
                    {
                        NodeList.SetAttribute("selected", "");
                        break;
                    }
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[5].ChildNodes[0];
                if (SaveData1[17] != "")
                {
                    DateTime dt = Convert.ToDateTime(SaveData1[17]);
                    objElement.SetAttribute("value", dt.ToString("yyyyMMdd"));
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[5].ChildNodes[1];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[18] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[5].ChildNodes[2];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[19] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[5].ChildNodes[3];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[20] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[5].ChildNodes[4];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[21] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[5].ChildNodes[5];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[22] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[5].ChildNodes[6];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[23] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[5].ChildNodes[7];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[24] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                if (SaveData2 != null)
                {
                    objNodeList = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[5].ChildNodes[8].ChildNodes;
                    for (int i = objNodeList.Count - 1; i >= 0; i--)
                    {
                        objNodeList[i].ParentNode.RemoveChild(objNodeList[i]);
                    }
                    // Commented below because already loading value and xml in below code
                    /*for (int i = 0; i < SaveData2.Length; i++)
                    {
                        if (SaveData2[i] != "")
                        {
                            objElement = xmlRep.CreateElement("option");
                            objElement.SetAttribute("value", SaveData2[i].Split(':')[1]);
                            objNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[5].ChildNodes[8];
                            objNode.AppendChild(objElement);
                        }
                    }*/
                    for (int i = 0; i < SaveData2.Length; i++)
                    {
                        if (SaveData2[i] != "")
                        {
                            objElement = xmlRep.CreateElement("option");
                            objElement.SetAttribute("value", SaveData2[i].Split(':')[1]);
                            objElement.InnerText = SaveData2[i].Split(':')[0];
                            objNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[5].ChildNodes[8];
                            objNode.AppendChild(objElement);
                        }
                    }
                }
            }
        }

        protected void PostOSHA301Report(String[] SaveData1, String[] SaveData2)
        {
            XmlNodeList objNodeList = null;
            XmlElement objElement = null;
            XmlNode objNode = null;

            if (SaveData1 != null)
            {
                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[0].ChildNodes[0];
                if (SaveData1[0] != "")
                {
                    objElement.SetAttribute("value", SaveData1[0]);
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[0].ChildNodes[1];
                if (SaveData1[1] != "")
                {
                    objElement.SetAttribute("value", SaveData1[1]);
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[0].ChildNodes[2];
                if (SaveData1[2] != "")
                {
                    objElement.SetAttribute("value", SaveData1[2]);
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[1].ChildNodes[0];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[3] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[2].ChildNodes[0];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[4] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objNodeList = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[2].ChildNodes[1].ChildNodes;
                foreach (XmlElement NodeList in objNodeList)
                {
                    if (NodeList.Attributes.Count > 1)
                    {
                        NodeList.RemoveAttribute("selected");
                    }
                }
                foreach (XmlElement NodeList in objNodeList)
                {
                    if (NodeList.InnerXml == SaveData1[5])
                    {
                        NodeList.SetAttribute("selected", "");
                        break;
                    }
                }

                objNodeList = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[2].ChildNodes[2].ChildNodes;
                foreach (XmlElement NodeList in objNodeList)
                {
                    if (NodeList.Attributes.Count > 1)
                    {
                        NodeList.RemoveAttribute("selected");
                    }
                }
                foreach (XmlElement NodeList in objNodeList)
                {
                    if (NodeList.InnerXml == SaveData1[6])
                    {
                        NodeList.SetAttribute("selected", "");
                        break;
                    }
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[3].ChildNodes[0];
                if (SaveData1[7] != "")
                {
                    objElement.SetAttribute("codeid", SaveData1[7]);
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[3].ChildNodes[1];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[8] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[3].ChildNodes[2];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[9] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[3].ChildNodes[3];
                if (SaveData1[10] != "")
                {
                    DateTime dt = Convert.ToDateTime(SaveData1[10]);
                    objElement.SetAttribute("value", dt.ToString("yyyyMMdd"));
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[3].ChildNodes[4];
                if (SaveData1[11] != "")
                {
                    DateTime dt = Convert.ToDateTime(SaveData1[11]);
                    objElement.SetAttribute("value", dt.ToString("yyyyMMdd"));
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[0];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[12] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[1];
                objElement.SetAttribute("value", SaveData1[13]);

                //Rakhel - code for adding combox box value
                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[2];

                string CodeID = string.Empty;
                if (SaveData1[22] == "Days")
                {
                    CodeID = "d";
                }
                else if (SaveData1[22] == "Months")
                {
                    CodeID = "m";
                }
                else if (SaveData1[22] == "Years")
                {
                    CodeID = "";
                }
                objElement.SetAttribute("codeid", CodeID);
                //Rakhel - code for adding combox box value

                objNodeList = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[2].ChildNodes;
                foreach (XmlElement NodeList in objNodeList)
                {
                    if (NodeList.Attributes.Count > 1)
                    {
                        NodeList.RemoveAttribute("selected");
                    }
                }
                foreach (XmlElement NodeList in objNodeList)
                {
                    if (NodeList.InnerXml == SaveData1[14])
                    {
                        NodeList.SetAttribute("selected", "");
                        break;
                    }
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[5].ChildNodes[0];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[15] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[5].ChildNodes[1];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[16] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[5].ChildNodes[2];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[17] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[5].ChildNodes[3];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[18] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[5].ChildNodes[4];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[19] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[5].ChildNodes[5];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[20] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                if (SaveData2 != null)
                {
                    objNodeList = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[5].ChildNodes[6].ChildNodes;
                    for (int i = objNodeList.Count - 1; i >= 0; i--)
                    {
                        objNodeList[i].ParentNode.RemoveChild(objNodeList[i]);
                    }
                    // Commented below because already loading value and xml in below code
                    /*for (int i = 0; i < SaveData2.Length; i++)
                    {
                        if (SaveData2[i] != "")
                        {
                            objElement = xmlRep.CreateElement("option");
                            objElement.SetAttribute("value", SaveData2[i].Split(':')[1]);
                            objNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[5].ChildNodes[6];
                            objNode.AppendChild(objElement);
                        }
                    }*/
                    for (int i = 0; i < SaveData2.Length; i++)
                    {
                        if (SaveData2[i] != "")
                        {
                            objElement = xmlRep.CreateElement("option");
                            objElement.SetAttribute("value", SaveData2[i].Split(':')[1]);
                            objElement.InnerText = SaveData2[i].Split(':')[0];
                            objNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[5].ChildNodes[6];
                            objNode.AppendChild(objElement);
                        }
                    }
                }
            }
        }

        protected void PostOSHASharpsReport(String[] SaveData1, String[] SaveData2)
        {
            XmlNodeList objNodeList = null;
            XmlElement objElement = null;
            XmlNode objNode = null;

            if (SaveData1 != null)
            {
                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[0].ChildNodes[0];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[0] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[1].ChildNodes[0];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[1] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objNodeList = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[1].ChildNodes[1].ChildNodes;
                foreach (XmlElement NodeList in objNodeList)
                {
                    if (NodeList.Attributes.Count > 1)
                    {
                        NodeList.RemoveAttribute("selected");
                    }
                }
                foreach (XmlElement NodeList in objNodeList)
                {
                    if (NodeList.InnerXml == SaveData1[2])
                    {
                        NodeList.SetAttribute("selected", "");
                        break;
                    }
                }

                objNodeList = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[1].ChildNodes[2].ChildNodes;
                foreach (XmlElement NodeList in objNodeList)
                {
                    if (NodeList.Attributes.Count > 1)
                    {
                        NodeList.RemoveAttribute("selected");
                    }
                }
                foreach (XmlElement NodeList in objNodeList)
                {
                    if (NodeList.InnerXml == SaveData1[3])
                    {
                        NodeList.SetAttribute("selected", "");
                        break;
                    }
                }

                objNodeList = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[1].ChildNodes[3].ChildNodes;
                foreach (XmlElement NodeList in objNodeList)
                {
                    if (NodeList.Attributes.Count > 1)
                    {
                        NodeList.RemoveAttribute("selected");
                    }
                }
                foreach (XmlElement NodeList in objNodeList)
                {
                    if (NodeList.InnerXml == SaveData1[4])
                    {
                        NodeList.SetAttribute("selected", "");
                        break;
                    }
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[2].ChildNodes[0];
                if (SaveData1[5] != "")
                {
                    objElement.SetAttribute("codeid", SaveData1[5]);
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[2].ChildNodes[1];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[6] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[2].ChildNodes[2];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[7] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[2].ChildNodes[3];
                if (SaveData1[8] != "")
                {
                    DateTime dt = Convert.ToDateTime(SaveData1[8]);
                    objElement.SetAttribute("value", dt.ToString("yyyyMMdd"));
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[2].ChildNodes[4];
                if (SaveData1[9] != "")
                {
                    DateTime dt = Convert.ToDateTime(SaveData1[9]);
                    objElement.SetAttribute("value", dt.ToString("yyyyMMdd"));
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[3].ChildNodes[0];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[10] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[3].ChildNodes[1];
                objElement.SetAttribute("value", SaveData1[11]);

                //Rakhel - code for adding combox box value
                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[3].ChildNodes[2];

                string CodeID = string.Empty;
                if (SaveData1[20] == "Days")
                {
                    CodeID = "d";
                }
                else if (SaveData1[20] == "Months")
                {
                    CodeID = "m";
                }
                else if (SaveData1[20] == "Years")
                {
                    CodeID = "";
                }
                objElement.SetAttribute("codeid", CodeID);
                //Rakhel - code for adding combox box value

                objNodeList = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[3].ChildNodes[2].ChildNodes;
                foreach (XmlElement NodeList in objNodeList)
                {
                    if (NodeList.Attributes.Count > 1)
                    {
                        NodeList.RemoveAttribute("selected");
                    }
                }
                foreach (XmlElement NodeList in objNodeList)
                {
                    if (NodeList.InnerXml == SaveData1[12])
                    {
                        NodeList.SetAttribute("selected", "");
                        break;
                    }
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[0];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[13] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[1];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[14] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[2];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[15] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[3];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[16] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[4];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[17] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                objElement = (XmlElement)xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[5];
                for (int i = 0; i < objElement.Attributes.Count; i++)
                {
                    if (objElement.Attributes[i].Name == "checked")
                        objElement.RemoveAttribute("checked");
                }
                if (SaveData1[18] == "true")
                {
                    objElement.SetAttribute("checked", "");
                }

                if (SaveData2 != null)
                {
                    objNodeList = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[6].ChildNodes;
                    for (int i = objNodeList.Count - 1; i >= 0; i--)
                    {
                        objNodeList[i].ParentNode.RemoveChild(objNodeList[i]);
                    }
                    // Commented below because already loading value and xml in below code
                    /*for (int i = 0; i < SaveData2.Length; i++)
                    {
                        if (SaveData2[i] != "")
                        {
                            objElement = xmlRep.CreateElement("option");
                            objElement.SetAttribute("value", SaveData2[i].Split(':')[1]);
                            objNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[6];
                            objNode.AppendChild(objElement);
                        }
                    }*/
                    for (int i = 0; i < SaveData2.Length; i++)
                    {
                        if (SaveData2[i] != "")
                        {
                            objElement = xmlRep.CreateElement("option");
                            objElement.SetAttribute("value", SaveData2[i].Split(':')[1]);
                            objElement.InnerText = SaveData2[i].Split(':')[0];
                            objNode = xmlRep.SelectSingleNode("/ResultMessage/Document/ScheduleDetails/ReportXML/report/form").ChildNodes[4].ChildNodes[6];
                            objNode.AppendChild(objElement);
                        }
                    }
                }
            }
        }

        protected void ddlscheduletype_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlscheduletype.SelectedIndex == 0)
                {
                    chkmon.Visible = true;
                    chktue.Visible = true;
                    chkwed.Visible = true;
                    chkthu.Visible = true;
                    chkfri.Visible = true;
                    chksat.Visible = true;
                    chksun.Visible = true;

                    chkjan.Visible = false;
                    chkfeb.Visible = false;
                    chkmar.Visible = false;
                    chkapr.Visible = false;
                    chkmay.Visible = false;
                    chkjun.Visible = false;
                    chkjul.Visible = false;
                    chkaug.Visible = false;
                    chksep.Visible = false;
                    chkoct.Visible = false;
                    chknov.Visible = false;
                    chkdec.Visible = false;
                }
                else if (ddlscheduletype.SelectedIndex == 1)
                {
                    chkjan.Visible = true;
                    chkfeb.Visible = true;
                    chkmar.Visible = true;
                    chkapr.Visible = true;
                    chkmay.Visible = true;
                    chkjun.Visible = true;
                    chkjul.Visible = true;
                    chkaug.Visible = true;
                    chksep.Visible = true;
                    chkoct.Visible = true;
                    chknov.Visible = true;
                    chkdec.Visible = true;

                    chkmon.Visible = false;
                    chktue.Visible = false;
                    chkwed.Visible = false;
                    chkthu.Visible = false;
                    chkfri.Visible = false;
                    chksat.Visible = false;
                    chksun.Visible = false;
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnback_Click(object sender, EventArgs e)
        {
            if (ViewState["PrevPage"].ToString().Contains("adminschedulelist"))
            {
                Response.Redirect("~/UI/SortMaster/ReportAdminScheduleList.aspx?smpage=adminschedulelist");
            }
            else 
            {
                Response.Redirect("~/UI/SortMaster/ScheduledView.aspx");
            }
        }
    }
}
