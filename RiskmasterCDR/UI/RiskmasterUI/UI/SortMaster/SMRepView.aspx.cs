﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Text;


namespace Riskmaster.UI.SortMaster
{
    public partial class SMRepView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string JobID = string.Empty;
            XElement XmlTemplate = null;
            XmlDocument oFDMPageDom = new XmlDocument();
            string strReturn = string.Empty;
            MemoryStream objMemoryStream = null;
            FileStream objFileStream = null;
            BinaryReader objBinaryReader = null;
            Byte[] arrByte = null;
            XmlDocument oXMLOut = null;
            string filename = string.Empty;

            if (!IsPostBack)
            {
                JobID = Request.QueryString["JobID"];
                XmlTemplate = GetMessageTemplate(JobID);
                strReturn = AppHelper.CallCWSService(XmlTemplate.ToString());
                oXMLOut = new XmlDocument();
                oXMLOut.LoadXml(strReturn);
                XmlNode objXMLNode = oXMLOut.SelectSingleNode("/ResultMessage/Document/OutputDetails");
                filename = objXMLNode.InnerText;
                if (filename != "")
                {
                    if (!File.Exists(filename))
                        throw new FileNotFoundException("Can not get DocPath. Check record for JOB_ID=" +JobID+ " in TM_REP_JOBS table.<br/>");
                    objFileStream = new FileStream(filename, FileMode.Open, FileAccess.Read);
                    objMemoryStream = new MemoryStream((int)objFileStream.Length);
                    objBinaryReader = new BinaryReader(objFileStream);
                    arrByte = objBinaryReader.ReadBytes((int)objFileStream.Length);
                    objMemoryStream.Write(arrByte, 0, (int)objFileStream.Length);
                    Header.Title = filename;
                    string sFileContent = Convert.ToBase64String(objMemoryStream.ToArray());
                    Response.ClearContent();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-Disposition", "inline;filename=" + filename);
                    Response.BinaryWrite(Convert.FromBase64String(sFileContent));
                    Response.Flush();
                    Response.Close();
                }
            }
        }

        private XElement GetMessageTemplate(string sJobId)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>c9243784-54ff-4f04-b7cd-cba4c910ce2a</Authorization>");
            sXml = sXml.Append("<Call><Function>SortMasterAdaptor.GetOutputPathByJobID</Function></Call><Document>");
            sXml = sXml.Append("<JobID>" + sJobId);
            sXml = sXml.Append("</JobID>");
            sXml = sXml.Append("</Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
    }
}
