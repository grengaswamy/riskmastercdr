﻿using System;
using System.Drawing;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;
namespace Riskmaster.UI.SortMaster
{
    public partial class JobQueue : System.Web.UI.Page
    {
        //Add by kuladeep
        string sMenuRequestType = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            string sReturn = string.Empty;
            if (Request.QueryString["smpage"]!=null)
            {
                sMenuRequestType = Request.QueryString["smpage"];
            }
            try
            {
                if (!IsPostBack)
                {
                    XmlDocument oFDMPageDom = new XmlDocument();
                    string strReturn = string.Empty;
                    XElement oMessageElement = LoadTemplate();
                    CallService(oMessageElement, ref oFDMPageDom, ref strReturn, false);
                    // akaushik5 Changed for MITS 33666 Starts
                    //if (oFDMPageDom.SelectSingleNode("//JobDetails") != null)
                    //{
                    //    DataSet aDataSet = new DataSet();
                    //    aDataSet.ReadXml(new StringReader(oFDMPageDom.GetElementsByTagName("JobDetails")[0].OuterXml));
                    //    RptrReport.DataSource = aDataSet.Tables["Job"];
                    //    ViewState["oFDMPageDom"] = oFDMPageDom.GetElementsByTagName("JobDetails")[0].OuterXml;
                    XmlNode objNode = oFDMPageDom.SelectSingleNode("//JobDetails");

                    if (objNode != null)
                    {
                        XmlDataSource source = new XmlDataSource();
                        source.ID = "XmlSource1";
                        source.EnableCaching = false;
                        source.Data = objNode.OuterXml;
                        source.DataBind();
                        RptrReport.DataSource = source;
                        // akaushik5 Changed for MITS 33666 Ends
                        RptrReport.DataBind();
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        /// <summary>
        /// This function would call the webservice to get output and return ouput string
        /// </summary>
        /// <param name="oMessageElement"></param>
        /// <param name="oFDMPageDom"></param>
        protected void CallService(XElement oMessageElement, ref XmlDocument oXMLOut, ref string p_sReturn, bool deleteRec)
        {
            string sMsgStatus = string.Empty;
            p_sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
            oXMLOut.LoadXml(p_sReturn);
            XmlNode oInstanceNode = oXMLOut.SelectSingleNode("/ResultMessage/Document");
            XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
            sMsgStatus = oXMLOut.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText;

            if ((sMsgStatus == "Success") && deleteRec)
            {
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
            }
        }

        private XElement LoadTemplate()
        {
//            XElement oTemplate = XElement.Parse(@"
//                 <Message>
//                     <Authorization>cdb73334bf7e4750ada07b96c582eab5</Authorization> 
//                     <Call>
//                      <Function>SortMasterAdaptor.GetJobDetails</Function> 
//                      </Call>
//                      <Document>
//                            <JobId></JobId>
//                            <JobName></JobName>
//                            <JobDesc></JobDesc>
//                            <OutputPathURL></OutputPathURL>
//                            <StartDTTM></StartDTTM>
//                            <Assigned></Assigned>
//                            <AssignedDTTM></AssignedDTTM>
//                            <Complete></Complete>
//                            <CompleteDTTM></CompleteDTTM>
//                            <ErrorFlag></ErrorFlag>
//                      </Document>
//                </Message>
//
//            ");
//            return oTemplate;

            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("SortMasterAdaptor.GetJobDetails</Function></Call>");
            sXml = sXml.Append("<Document><ParmaList><JobId></JobId><JobName></JobName><JobDesc></JobDesc>");
            sXml = sXml.Append("<OutputPathURL></OutputPathURL><StartDTTM></StartDTTM><Assigned></Assigned>");
            sXml = sXml.Append("<AssignedDTTM></AssignedDTTM><Complete></Complete><CompleteDTTM></CompleteDTTM>");
            sXml = sXml.Append("<ErrorFlag></ErrorFlag>");
            sXml = sXml.Append("<MenuRequestType>" + sMenuRequestType + "</MenuRequestType>");
            sXml = sXml.Append("</ParmaList></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
        // akaushik5 Changed for MITS 33666 Starts
        //protected void RptrReport_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    //string JobId = string.Empty;
        //    //string JobName = string.Empty;
        //    //string JobDesc = string.Empty;
        //    //string OutputPathURL = string.Empty;
        //    //string StartDTTM = string.Empty;
        //    //string Assigned = string.Empty;
        //    //string AssignedDTTM = string.Empty;
        //    //string Complete = string.Empty;
        //    //string CompleteDTTM = string.Empty;
        //    //string ErrorFlag = string.Empty;
        //    string strReturn = string.Empty;
        //    XmlDocument oXMLOut = new XmlDocument();

        //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //    {
        //        HyperLink hypLnkJobName = (HyperLink)e.Item.FindControl("hypLnkJobName");
        //        Label lblJobStatus = (Label)e.Item.FindControl("lblJobStatus");
                
        //        DataRowView drv = e.Item.DataItem as DataRowView;
        //        if (drv != null)
        //        {
        //            //JobId = drv.Row[0].ToString();
        //            //JobName = drv.Row[1].ToString();
        //            //JobDesc = drv.Row[2].ToString();
        //            //OutputPathURL = drv.Row[3].ToString();
        //            //StartDTTM = drv.Row[4].ToString();
        //            //Assigned = drv.Row[5].ToString();
        //            //AssignedDTTM = drv.Row[6].ToString();
        //            //Complete = drv.Row[7].ToString();
        //            //CompleteDTTM = drv.Row[8].ToString();
        //            //ErrorFlag = drv.Row[9].ToString();
        //        }
        //        if (drv.Row[7].ToString() == "-1" && drv.Row[9].ToString() == "0")
        //        {
        //            hypLnkJobName.NavigateUrl = drv.Row[3].ToString();
        //        }
        //        else
        //        {
        //            hypLnkJobName.ForeColor = Color.Black;
        //        }

        //        if (drv.Row[5].ToString() == "0")
        //        {
        //            lblJobStatus.Text = "Waiting";
        //            lblJobStatus.ForeColor = Color.Gray;
        //        }
        //        else if (drv.Row[7].ToString() == "0")
        //        {
                    
        //            //XElement oMessageElement = LoadTemplate("progress",JobId);
        //            //strReturn = AppHelper.CallCWSService(oMessageElement.ToString());//chnge
        //            //oXMLOut.LoadXml(strReturn);
        //            //XmlNode oInstanceNode = oXMLOut.SelectSingleNode("/ResultMessage/Document");
        //            //XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
        //            lblJobStatus.Text = "<b>Running </b>(Started @ " + drv.Row[6].ToString();
        //            //lblJobStatus.Text = lblJobStatus.Text + oMessageRespElement.Value;
        //            lblJobStatus.Text = lblJobStatus.Text +  drv.Row[10].ToString();
        //            lblJobStatus.ForeColor = Color.Blue;
        //        }
        //        else if (drv.Row[9].ToString() == "-1")
        //        {
                   
        //            //XElement oMessageElement = LoadTemplate("error", JobId);
        //            //strReturn = AppHelper.CallCWSService(oMessageElement.ToString());//chnge
        //            //oXMLOut.LoadXml(strReturn);
        //            //XmlNode oInstanceNode = oXMLOut.SelectSingleNode("/ResultMessage/Document");
        //            //XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
        //            //lblJobStatus.Text = "<b> Error occured </b>" + oMessageRespElement.Value;
        //            lblJobStatus.Text = "<b> Error occured </b>" + drv.Row[11].ToString();
        //            lblJobStatus.ForeColor = Color.Red ;
        //        }
        //        else
        //        {
        //            lblJobStatus.Text = "<b>Complete</b> (Started @ " + drv.Row[6].ToString() + " and Completed @ " + drv.Row[8].ToString() + ")";
        //            lblJobStatus.ForeColor = Color.Green;
        //        }
        //    }
        //}

        /// <summary>
        /// Handles the ItemDataBound event of the RptrReport control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
        protected void RptrReport_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                XPathNavigator nav = ((IXPathNavigable)e.Item.DataItem).CreateNavigator();
                HyperLink hypLnkJobName = e.Item.FindControl("hypLnkJobName") as HyperLink;
                Label lblJobStatus = e.Item.FindControl("lblJobStatus") as Label;

                if (nav != null)
                {
                    XmlElement xmlElement = nav.UnderlyingObject as XmlElement;
                    string Complete = xmlElement.GetAttribute("Complete");
                    string ErrorFlag = xmlElement.GetAttribute("ErrorFlag");
                    string AssignedDTTM = xmlElement.GetAttribute("AssignedDTTM");
                    string Assigned = xmlElement.GetAttribute("Assigned");
                    string CompleteDTTM = xmlElement.GetAttribute("CompleteDTTM");
                    string ProgressMsg = xmlElement.GetAttribute("ProgressMsg");
                    string ErrorMsg = xmlElement.GetAttribute("ErrorMsg");

                    if (Complete.Equals("-1") && ErrorFlag == "0")
                    {
                        // akaushik5 Changed for MITS 38389 Starts
                        //hypLnkJobName.NavigateUrl = xmlElement.GetAttribute("OutputPathURL");
                        if (!string.IsNullOrEmpty(xmlElement.GetAttribute("OutputPathURL")))
                        {
                            hypLnkJobName.NavigateUrl = (xmlElement.GetAttribute("OutputPathURL").StartsWith("https://") || xmlElement.GetAttribute("OutputPathURL").StartsWith("http://")) && xmlElement.GetAttribute("OutputPathURL").IndexOf("/RiskmasterUI") > -1 ?
                                xmlElement.GetAttribute("OutputPathURL").Substring(xmlElement.GetAttribute("OutputPathURL").IndexOf("/RiskmasterUI"))
                                : xmlElement.GetAttribute("OutputPathURL");
                        }
                        // akaushik5 Changed for MITS 38389 Ends
                    }
                    else
                    {
                        hypLnkJobName.ForeColor = Color.Black;
                    }

                    if (Assigned.Equals("0"))
                    {
                        lblJobStatus.Text = "Waiting";
                        lblJobStatus.ForeColor = Color.Gray;
                    }
                    else if (Complete.Equals("0"))
                    {
                        lblJobStatus.Text = string.Format("<b>Running </b>(Started @ {0} {1})", AssignedDTTM, ProgressMsg);
                        lblJobStatus.ForeColor = Color.Blue;
                    }
                    else if (ErrorFlag.Equals("-1"))
                    {
                        lblJobStatus.Text = string.Format("<b> Error occured </b> {0})", ErrorMsg);
                        lblJobStatus.ForeColor = Color.Red;
                    }
                    else
                    {
                        lblJobStatus.Text = string.Format("<b>Complete</b> (Started @ {0} and Completed @ {1})", AssignedDTTM, CompleteDTTM);
                        lblJobStatus.ForeColor = Color.Green;
                    }
                }
            }
        }
        // akaushik5 Changed for MITS 33666 Ends

        //private XElement LoadTemplate(string MessageType, string JobId)
        //{
        //    StringBuilder sXml = new StringBuilder("<Message>");
        //    sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
        //    sXml = sXml.Append("<Call><Function>");
        //    sXml = sXml.Append("SortMasterAdaptor.GetJobMessage</Function></Call><Document><ParamList>");
        //    sXml = sXml.Append("<JobID>" + JobId + "</JobID>");
        //    sXml = sXml.Append("<MessageType>" + MessageType + "</MessageType>");
        //    sXml = sXml.Append("</ParamList></Document></Message>");
        //    XElement oTemplate = XElement.Parse(sXml.ToString());
        //    return oTemplate;
        //}

        //protected void CallService(XElement oMessageElement, ref XmlDocument oXMLOut, ref string p_sReturn)
        //{
        //    string sMsgStatus = string.Empty;

        //    p_sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
        //    oXMLOut.LoadXml(p_sReturn);
        //    XmlNode oInstanceNode = oXMLOut.SelectSingleNode("/ResultMessage/Document");
        //    XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
        //    sMsgStatus = oXMLOut.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText;

        //    if (sMsgStatus == "Success")
        //    {
        //        Page.Response.Redirect(Page.Request.Url.ToString(), true);
        //    }
        //}

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string strReturn = string.Empty;
            string JobIds = string.Empty;
            XmlDocument oFDMPageDom = new XmlDocument();
            try
            {
                foreach (RepeaterItem item in RptrReport.Items)
                {
                    CheckBox chkselected = item.FindControl("chkJobName") as CheckBox;
                    HiddenField hdnJobId = item.FindControl("hdnJobId") as HiddenField;
                    if (chkselected.Checked)
                    {
                        if (JobIds == string.Empty)
                            JobIds = hdnJobId.Value;
                        else
                            JobIds = JobIds + "," + hdnJobId.Value;
                    }
                }

                XmlDocument objXML = new XmlDocument();
                StringBuilder sXml = new StringBuilder("<Message>");
                sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
                sXml = sXml.Append("<Call><Function>SortMasterAdaptor.DeleteJobs</Function></Call>");
                sXml = sXml.Append("<Document><Job>");
                sXml = sXml.Append("<JobIds>" + JobIds + "</JobIds>");
                sXml = sXml.Append("</Job></Document></Message>");
                XElement oTemplate = XElement.Parse(sXml.ToString());

                CallService(oTemplate, ref oFDMPageDom, ref strReturn, true);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnEmail_Click(object sender, EventArgs e)
        {
            string JobIds = string.Empty;
            try
            {
                foreach (RepeaterItem item in RptrReport.Items)
                {
                    CheckBox chkselected = item.FindControl("chkJobName") as CheckBox;
                    HiddenField hdnJobId = item.FindControl("hdnJobId") as HiddenField;
                    if (chkselected.Checked)
                    {
                        if (JobIds == string.Empty)
                            JobIds = hdnJobId.Value;
                        else
                            JobIds = JobIds + "," + hdnJobId.Value;
                    }
                }
                Response.Redirect("EmailJob.aspx?JobIds=" + JobIds);

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnArchive_Click(object sender, EventArgs e)
        {
            string JobIds = string.Empty;
            try
            {
                foreach (RepeaterItem item in RptrReport.Items)
                {
                    CheckBox chkselected = item.FindControl("chkJobName") as CheckBox;
                    HiddenField hdnJobId = item.FindControl("hdnJobId") as HiddenField;
                    if (chkselected.Checked)
                    {
                        if (JobIds == string.Empty)
                            JobIds = hdnJobId.Value;
                        else
                            JobIds = JobIds + "," + hdnJobId.Value;
                    }
                }
                Response.Redirect("ArchiveJob.aspx?JobIds=" + JobIds);

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
    }
}