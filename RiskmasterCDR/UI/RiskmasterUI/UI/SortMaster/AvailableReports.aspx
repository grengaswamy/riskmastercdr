﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AvailableReports.aspx.cs"
    Inherits="Riskmaster.UI.SortMaster.AvailableReports" %>

<!DOCTYPE html>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <script type="text/javascript">
        function ViewReport(Id) {
            var obj = document.getElementById('reportid');
            var vMenuType = document.getElementById('hdnMenuRequestType');
            if (obj == null) {
                alert("No reports available.");
                return false;
            }
            obj.value = Id;
            if (obj.value != "-1") {
                window.location.href = "AvailableReports.aspx?IsDownload=1&ReportId=" + Id + "&smpage=" + vMenuType.value;
            }
            return true;
        }
        function onPageLoaded() {
            var obj = document.getElementById('hdnhyperRprt');
            if (obj != null) {
                if (obj.value == "true") {
                    var ar = document.getElementsByName('hyperRprt');
                    if (ar != null) {
                        for (var i = 0; i < ar.length; i++) {
                            ar[i].style.display = "none";
                        }
                    }
                }
            }
        }
    </script>
</head>
<body onload="parent.pleaseWait('stop');onPageLoaded();">
    <form id="frmData" runat="server">
    <div>
        <table>
            <tr>
                <td colspan="2">
                    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
                </td>
            </tr>
        </table>
        <asp:Repeater ID="RptrReport" runat="server" OnItemDataBound="RptrReport_ItemDataBound">
            <HeaderTemplate>
                <table width="100%" border="1" cellspacing="0" cellpadding="1">
                    <tr>
                        <td class="ctrlgroup2" colspan="8">
                            <asp:Label ID="lblAvailableReports" runat="server" Text="Available Reports"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="colheader3" width="30%">
                            &nbsp;<asp:Label ID="lblReportName" runat="server" Text="Report Name"></asp:Label>
                        </td>
                        <td class="colheader3" width="70%">
                            &nbsp;<asp:Label ID="lblReportDesc" runat="server" Text="Report Description"></asp:Label>
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <ItemTemplate>
                <table width="100%" height="0" bgcolor="white" border="1" cellspacing="0" cellpadding="10"
                    class="rowlight1">
                    <tr>
                        <td width="30%">
                            <a id="hyperRprt" href="#" style="display: none" onclick="ViewReport(<%# Eval("ReportId")%>)"><%# DataBinder.Eval(Container.DataItem, "ReportName")%></a> <%--Jira id: 7489 --%>
                            <asp:HyperLink ID="HypLnkReprtName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ReportName")%>' Font-Underline="true" NavigateUrl='<%# String.Format("~/UI/SortMaster/CriteriaEdit.aspx?ReportId={0}&ReportName={1}&PrivacyCase={2}&smpage={3}", Eval("ReportId"),Eval("ReportName"),Eval("PrivacyCase"), hdnMenuRequestType.Value) %>' ></asp:HyperLink>
                            <%--<asp:HyperLink ID="HypLnkReprtName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ReportName")%>' Font-Underline="true" NavigateUrl='<%# String.Format("~/UI/SortMaster/AvailableReports.aspx?ReportId={0}&IsDownload=1", Eval("ReportId")) %>'></asp:HyperLink>--%>
                        </td>
                        <td width="70%">
                            <asp:Label ID="lblReprtDesc" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ReportDesc")%>'></asp:Label>
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <table width="100%" height="0" bgcolor="white" border="1" cellspacing="0" cellpadding="10"
                    class="rowdark1">
                    <tr>
                        <td width="30%">
                          <a id="hyperRprt" href="#" style="display: none" onclick="ViewReport(<%# Eval("ReportId")%>)"><%#DataBinder.Eval(Container.DataItem, "ReportName")%></a> <%--Jira id: 7489 --%>
                          <asp:HyperLink ID="HypLnkReprtName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ReportName")%>' Font-Underline="true" NavigateUrl='<%# String.Format("~/UI/SortMaster/CriteriaEdit.aspx?ReportId={0}&ReportName={1}&PrivacyCase={2}&smpage={3}", Eval("ReportId"),Eval("ReportName"),Eval("PrivacyCase"),hdnMenuRequestType.Value) %>' ></asp:HyperLink>
                          <%--<asp:HyperLink ID="HypLnkReprtName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ReportName")%>' Font-Underline="true" NavigateUrl='<%# String.Format("~/UI/SortMaster/AvailableReports.aspx?ReportId={0}&IsDownload=1", Eval("ReportId")) %>'></asp:HyperLink>--%>
                        </td>
                        <td width="70%">
                            <asp:Label ID="lblReprtDesc" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ReportDesc")%>'></asp:Label>
                        </td>
                    </tr>
                </table>
            </AlternatingItemTemplate>
        </asp:repeater>
        </div>
        <input type="hidden" name="reportid" value="-1" runat="server" id="reportid" />
        <input type="hidden" name="hdnhyperRprt" value="false" runat="server" id="hdnhyperRprt" />
        <input type="hidden" name="hdnMenuRequestType" value="false" runat="server" id="hdnMenuRequestType" /> <%--RMACLOUD-126--%>
        <font class="small">You have access to the following reports which have been posted
        to the system.
        <br />
        To begin running an available report, click on the desired report name.
        <br>
        *** For information on extended reporting features and report troubleshooting please
        see the <a href="SMFAQs.aspx">Frequently Asked Questions (FAQ)</a>. </font>
    </form>
</body>
</html>
