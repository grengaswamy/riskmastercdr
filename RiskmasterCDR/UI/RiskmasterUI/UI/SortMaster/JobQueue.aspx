﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="JobQueue.aspx.cs" Inherits="Riskmaster.UI.SortMaster.JobQueue" %>

<!DOCTYPE html>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl" TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Job Queue</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript">
        function Refresh()
        {
            // window.location = window.location.href;
            location.reload(true); 
        }
        function SelectAll()
        {
            $('input:checkbox').attr('checked', 'checked');   
        }
        function Validate(text)
        {
            var pageElements = document.forms[0].elements;
            var oneChecked = false;
            var reportAvailable = false;
            for (var i = 0; i < pageElements.length; i++)
            {
                if (pageElements[i].type == "checkbox" && pageElements[i].name.indexOf('chkDeleteAll') == -1)
                {
                    reportAvailable = true;
                    if (pageElements[i].checked == true)
                    {
                        oneChecked = true;
                    }
                }
            }
            if (oneChecked == false)
            {
                alert("Please select report jobs you would like to " + text+ ".");
                return false;
            }
            
        }
    </script>
</head>
<body  onload="parent.pleaseWait('stop');">
    <form id="form1" runat="server">
    <div class="divScroll">
        <table>
            <tr>
                <td colspan="2">
                    <uc1:errorcontrol id="ErrorControl1" runat="server" />
                </td>
            </tr>
        </table>
         <asp:Repeater ID="RptrReport" runat="server" OnItemDataBound="RptrReport_ItemDataBound">
            <HeaderTemplate>
                <table width="100%" border="0" cellspacing="0" cellpadding="1">
                    <tr><td class="ctrlgroup2" colspan="8"><asp:Label ID="lblJobQueue" runat="server" Text="Job Queue"></asp:Label></td></tr>
                    <tr>
                        <td class="colheader3" width="25%"><asp:Label ID="lblJobName" runat="server" Text="Job Name"></asp:Label></td>
                        <td class="colheader3" width="30%"><asp:Label ID="lblJobDescription" runat="server" Text="Job Description"></asp:Label></td>
                        <td class="colheader3" width="25%"><asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label></td>
                    </tr>
                </table>
            </HeaderTemplate>
            <ItemTemplate>
                <table id="rptrItm" bgcolor="white" border="1" width="100%" height="0"  class="rowlight1" cellspacing="0" cellpadding="1">
                    <tr>
                        <td class="td8" width="25%">
                            <asp:CheckBox ID="chkJobName" runat="server"/>
                            <asp:HyperLink ID="hypLnkJobName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "JobName")%>'></asp:HyperLink>
                            <asp:HiddenField ID="hdnJobId" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "JobId")%>' />
                        </td>
                        <td class="td8" width="25%">
                            <asp:Label ID="lblJobDesc" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "JobDesc")%>'></asp:Label>
                        </td>
                        <td class="td8" width="50%">
                            <asp:Label ID="lblJobStatus" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <table id="rptrAltItm" bgcolor="white" border="1" width="100%" height="0" "rowdark1" cellspacing="0" cellpadding="1">
                    <tr>
                       <td class="td8" width="25%">
                            <asp:CheckBox ID="chkJobName" runat="server"/>
                            <asp:HyperLink ID="hypLnkJobName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "JobName")%>'></asp:HyperLink>
                           <asp:HiddenField ID="hdnJobId" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "JobId")%>' />
                        </td>
                        <td class="td8" width="25%">
                            <asp:Label ID="lblJobDesc" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "JobDesc")%>'></asp:Label>
                        </td>
                        <td class="td8" width="50%">
                            <asp:Label ID="lblJobStatus" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </AlternatingItemTemplate>
        </asp:Repeater>
    </div>
    <font class="small">This page shows current reporting activity and allows you to manage report output.
    <br />To view report results, click on a job name.
    <br />To Archive, Email or Delete one or more jobs, place a checkmark next to the target jobs and click the desired button.
    <br />*** For information on extended reporting features and report troubleshooting please see the <a href="SMFAQs.aspx">Frequently Asked Questions (FAQ)</a>.
    </font>
    <table border="0">
        <tr>
            <td>
                <asp:Button ID="btnSelectAll" runat="server" Text="Select All" OnClientClick="SelectAll()"/>
            </td>
            <td>
                <asp:Button ID="btnArchive" runat="server" Text="   Archive   " OnClientClick="return Validate('archive')" OnClick="btnArchive_Click" />
            </td>
            <td>
                <asp:Button ID="btnEmail" runat="server" Text="  E-Mail  " OnClientClick="return Validate('e-mail')" OnClick="btnEmail_Click" />
            </td>
            <td>
                <asp:Button ID="btnDelete" runat="server" Text="  Delete  " OnClientClick="return Validate('delete')" OnClick="btnDelete_Click" />
            </td>
            <td>
                <asp:Button ID="btnRefresh" runat="server" Text="  Refresh  " OnClientClick="Refresh();" />
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
