﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.AppHelpers;
using System.ServiceModel;
using System.Configuration;
using System.Data;
using System.IO;

namespace Riskmaster.UI.UI.SortMaster
{
    public partial class ReportAdminScheduleList : System.Web.UI.Page
    {
        string sMenuRequestType = string.Empty;//Add by kualdeep

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (Request.QueryString["smpage"] != null)
                {
                    sMenuRequestType = Request.QueryString["smpage"];
                }

                if (!IsPostBack)
                {
                    XmlDocument oFDMPageDom = new XmlDocument();
                    string strReturn = string.Empty;
                    XElement oMessageElement = LoadTemplate();
                    CallService(oMessageElement, ref oFDMPageDom, ref strReturn, false);
                    if (oFDMPageDom.SelectSingleNode("//ScheduleDetails") != null)
                    {
                        DataSet aDataSet = new DataSet();
                        aDataSet.ReadXml(new StringReader(oFDMPageDom.GetElementsByTagName("ScheduleDetails")[0].OuterXml));
                        gvScheduledReports.DataSource = aDataSet.Tables["Schedule"];
                        gvScheduledReports.DataBind();
                        if (gvScheduledReports.Rows.Count == 0) 
                        {
                            btnDelete.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        /// <summary>
        /// This function would call the webservice to get output and return ouput string
        /// </summary>
        /// <param name="oMessageElement"></param>
        /// <param name="oFDMPageDom"></param>
        protected void CallService(XElement oMessageElement, ref XmlDocument oXMLOut, ref string p_sReturn, bool deleteRec)
        {
            string sMsgStatus = string.Empty;
            p_sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
            oXMLOut = new XmlDocument();
            oXMLOut.LoadXml(p_sReturn);
            XmlNode oInstanceNode = oXMLOut.SelectSingleNode("/ResultMessage/Document");
            XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
            sMsgStatus = oXMLOut.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText;

            if ((sMsgStatus == "Success") && deleteRec)
            {
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
            }
        }

        private XElement LoadTemplate()
        {
            //            XElement oTemplate = XElement.Parse(@"
            //                 <Message>
            //                     <Authorization>cdb73334bf7e4750ada07b96c582eab5</Authorization> 
            //                     <Call>
            //                      <Function>SortMasterAdaptor.GetScheduleDetails</Function> 
            //                      </Call>
            //                      <Document>
            //                        <OrderBy></OrderBy>
            //                        <ScheduleId></ScheduleId>
            //                        <ReportId></ReportId>
            //                        <ReportName></ReportName>
            //                        <ScheduleType></ScheduleType>
            //                        <LastRunDTTM></LastRunDTTM>
            //                        <NextRunDT></NextRunDT>
            //                      </Document>
            //                </Message>
            //
            //
            //            ");
            //            return oTemplate;


            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
            sXml = sXml.Append("<Call><Function>");
            sXml = sXml.Append("SortMasterAdaptor.GetScheduleDetails</Function></Call>");
            sXml = sXml.Append("<Document><ParmaList><JobId></JobId><JobName></JobName><JobDesc></JobDesc>");
            sXml = sXml.Append("<OutputPathURL></OutputPathURL><StartDTTM></StartDTTM><Assigned></Assigned>");
            sXml = sXml.Append("<AssignedDTTM></AssignedDTTM><Complete></Complete><CompleteDTTM></CompleteDTTM>");
            sXml = sXml.Append("<ErrorFlag></ErrorFlag>");
            sXml = sXml.Append("<MenuRequestType>" + sMenuRequestType + "</MenuRequestType>");
            sXml = sXml.Append("</ParmaList></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }

        protected void gvScheduledReports_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[1].Text.Contains("1"))
                {
                    e.Row.Cells[1].Text = "Daily";
                }
                if (e.Row.Cells[1].Text.Contains("2"))
                {
                    e.Row.Cells[1].Text = "Monthly";
                }
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string strReturn = string.Empty;
            string ScheduleIds = string.Empty;
            XmlDocument oFDMPageDom = new XmlDocument();
            try
            {
                for (int i = 0; i <= gvScheduledReports.Rows.Count - 1; i++)
                {
                    GridViewRow row = gvScheduledReports.Rows[i];
                    CheckBox Ckbox = (CheckBox)row.FindControl("chkReportName");
                    HiddenField hdnScheduleId = (HiddenField)row.FindControl("hdnScheduleId");
                    if (Ckbox.Checked)
                    {
                        if (ScheduleIds == string.Empty)
                            ScheduleIds = hdnScheduleId.Value;
                        else
                            ScheduleIds = ScheduleIds + "," + hdnScheduleId.Value;

                    }
                }

                XmlDocument objXML = new XmlDocument();
                StringBuilder sXml = new StringBuilder("<Message>");
                sXml = sXml.Append("<Authorization>772f8091-0f7e-4360-9477-c2ebdd98baae</Authorization>");
                sXml = sXml.Append("<Call><Function>SortMasterAdaptor.DeleteScheduledReports</Function></Call>");
                sXml = sXml.Append("<Document><Schedule>");
                sXml = sXml.Append("<ScheduleIds>" + ScheduleIds + "</ScheduleIds>");
                sXml = sXml.Append("</Schedule></Document></Message>");
                XElement oTemplate = XElement.Parse(sXml.ToString());

                CallService(oTemplate, ref oFDMPageDom, ref strReturn, true);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
    }
}
