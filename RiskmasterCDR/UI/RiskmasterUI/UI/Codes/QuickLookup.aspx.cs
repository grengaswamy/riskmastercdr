﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using Riskmaster.UI.CodesService;
using Riskmaster.UI.Shared;
using Riskmaster.BusinessHelpers;
using System.Web.UI.WebControls;
using Riskmaster.BusinessAdaptor.Common;
using System.ServiceModel;
using Riskmaster.Common;
using System.Xml.Linq;
using System.Text;
using Riskmaster.Models;

namespace Riskmaster.UI.Codes
{
    public partial class QuickLookup : System.Web.UI.Page
    {
        protected Int32 iCount =0;
        protected List<Riskmaster.Models.CodeType> codes;
        protected CodeListType objList;
        string sCodetype = String.Empty;
        string sLOB = String.Empty;  
        string sLookupstring = String.Empty;
        string sDescSearch = String.Empty;
        string sFilter = String.Empty;
        string sSessionlob = String.Empty;
        string sDeptEid = String.Empty;
        string sFormname = String.Empty;
        string sTriggerDate = String.Empty;
        string sEventdate = String.Empty;
        string sTitle = String.Empty;
        string sSessionClaimId = String.Empty;
        string sOrgLevel = String.Empty;
        string sParentId = String.Empty;//abansal23: MITS 14847
        string sInsuredEid = String.Empty;
        string sParentCodeid = String.Empty;//declare variable for the parent reserve type code id--stara 05/18/09
        string sEventId = string.Empty;
        string sPolicyId = string.Empty;
        string sCovTypeCodeId = string.Empty;
        string sLossTypeCodeId = string.Empty;      //Ankit Start : Work for JIRA - 908 (Adding LossType for Where condition)
        string sTransId = string.Empty;
        string sRsvStatusParent = string.Empty;
        string sClaimantEid = string.Empty;
        protected bool bNotExactMatch = true;//Added by Shivendu for MITS 15343
        protected Int32 iCurrentPageNumber = 1;
        private const string sPageLoad = "PageLoad";
        private const string sFirst = "First";
        private const string sNext = "Next";
        private const string sPrev = "Prev";
        private const string sLast = "Last";
        //rupal:start, r8 first & final enhancement, 4 july 2011
        string sIsFirstFinal = string.Empty;
        string sPolCovgRowId = string.Empty;
        //rupal:end
        string sPolUnitRowId = string.Empty;

        string sPolicyLOB = string.Empty;
        string sCovgSeqNum = string.Empty;//rupal:policy system interface
        string sTransSeqNum = string.Empty;
        string sCoverageKey = string.Empty;     //Ankit Start : Worked on MITS - 34297
		string sFieldName = string.Empty; //igupta3: Ensuring field is supp or not
		string sJurisdiction = string.Empty;    //MITS#36929 added by swati
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                //srajindersin MITS 26940 dt 04/23/2012
                pageaction.Value = Request.RawUrl;
                hdPageNumber.Value = "1";
                //END srajindersin MITS 26940 dt 04/23/2012

                //changes for pen testing by atavaragiri (htmlcustomencode) :mits 27825
                //codetype.Value = AppHelper.GetQueryStringValue("codetype");
                //lookupstring.Value = AppHelper.GetQueryStringValue("lookupstring");
                //descSearch.Value = AppHelper.GetQueryStringValue("descSearch");
                //filter.Value = AppHelper.GetQueryStringValue("filter");
                //lob.Value = AppHelper.GetQueryStringValue("lob");
                //orgeid.Value = AppHelper.GetQueryStringValue("orgeid");
                //formname.Value = AppHelper.GetQueryStringValue("formname");
                //triggerdate.Value = AppHelper.GetQueryStringValue("triggerdate");
                //eventdate.Value = AppHelper.GetQueryStringValue("eventdate");
                //sessionlob.Value = AppHelper.GetQueryStringValue("sessionlob");
                //Title.Value = AppHelper.GetQueryStringValue("Title");
                //sessionclaimid.Value = AppHelper.GetQueryStringValue("sessionclaimid");
                //orgLevel.Value = AppHelper.GetQueryStringValue("orgLevel");
                //insuredeid.Value = AppHelper.GetQueryStringValue("insuredeid"); //PJS MITS # 15220
                //parentcodeid.Value = AppHelper.GetQueryStringValue("parentcodeid");//stara Mits 16667 05/18/09
                //eventid.Value = AppHelper.GetQueryStringValue("eventid");
                //policyid.Value = AppHelper.GetQueryStringValue("PolicyId");
                //covtypecodeid.Value = AppHelper.GetQueryStringValue("CovCodeId");
                //transid.Value = AppHelper.GetQueryStringValue("TransId");
                //claimanteid.Value = AppHelper.GetQueryStringValue("sClaimantEntId");

                codetype.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("codetype"));
                lookupstring.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("lookupstring"));
                descSearch.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("descSearch"));
                filter.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("filter"));
                lob.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("lob"));
                orgeid.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("orgeid"));
                formname.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("formname"));
                triggerdate.Value =AppHelper.HTMLCustomEncode (AppHelper.GetQueryStringValue("triggerdate"));
                eventdate.Value =AppHelper.HTMLCustomEncode( AppHelper.GetQueryStringValue("eventdate"));
                sessionlob.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("sessionlob"));
                Title.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("Title"));
                sessionclaimid.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("sessionclaimid"));
                orgLevel.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("orgLevel"));
                insuredeid.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("insuredeid")); //PJS MITS # 15220
                parentcodeid.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("parentcodeid"));//stara Mits 16667 05/18/09
                eventid.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("eventid"));
                policyid.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("PolicyId"));
                covtypecodeid.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("CovCodeId"));
                losstypecodeid.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("LossCodeId"));     //Ankit Start : Work for JIRA - 908 (Adding LossType for Where condition)
                transid.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("TransId"));
                claimanteid.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("sClaimantEntId"));
                RsvStatusParent.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("RsvStatusParent"));
                // END :changes for pen testing by atavaragiri (htmlcustomencode) :mits 27825

                //rupal:start First & Final Payment, 4 july 2011
                //to get coverage_type, need to first identify if the payment is first & final. 

                //changes for pen testing by atavaragiri (htmlcustomencode) :mits 27825
                //IsFirstFinal.Value = AppHelper.GetQueryStringValue("IsFirstFinal");
                IsFirstFinal.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("IsFirstFinal"));
                //END: changes for pen testing by atavaragiri (htmlcustomencode) :mits 27825
                //rupal:end for first & final payment
                //added by swati for AIC GAp 7 MITS # 36929
                jurisdiction.Value = AppHelper.GetQueryStringValue("jurisdiction");
                //change end here

                //changes for pen testing by atavaragiri (htmlcustomencode) :mits 27825
                //sPolUnitRowId = AppHelper.GetQueryStringValue("PolUnitRowId");
                sPolUnitRowId = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("PolUnitRowId"));
                //END :changes for pen testing by atavaragiri (htmlcustomencode) :mits 27825
                PolicyLOB.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("policyLOB"));
                CovgSeqNum.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("CovgSeqNum"));//rupal:policy system interface
                TransSeqNum.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("TransSeqNum"));//smishra54: MITS 33996
                CoverageKey.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("CoverageKey"));       //Ankit Start : Worked on MITS - 34297
				FieldName.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("FieldName")); ////igupta3: Ensuring field is supp or not : MITS 36575 
                OnPageLoad(sPageLoad);
            }
        }

        protected void lnkFirst_Click(object sender, EventArgs e)
        {
            OnPageLoad(sFirst);
        }
        protected void lnkNext_Click(object sender, EventArgs e)
        {
            OnPageLoad(sNext);
        }
        protected void lnkPrev_Click(object sender, EventArgs e)
        {
            OnPageLoad(sPrev);
        }
        protected void lnkLast_Click(object sender, EventArgs e)
        {
            OnPageLoad(sLast);
        }

        public void OnPageLoad(string sCallee)
        {
            CodesBusinessHelper cb = new CodesBusinessHelper();
            try
            {

                sCodetype = codetype.Value;
                sLookupstring = lookupstring.Value;
                sDescSearch = descSearch.Value;
                sFilter = filter.Value;
                sLOB = lob.Value;
                sDeptEid = orgeid.Value;
                sFormname = formname.Value;
                sTriggerDate = triggerdate.Value;
                sEventdate = eventdate.Value;
                sSessionlob = sessionlob.Value;
                sTitle = Title.Value;
                sSessionClaimId = sessionclaimid.Value;
                sOrgLevel = orgLevel.Value;
                sInsuredEid = insuredeid.Value;
                sParentCodeid = parentcodeid.Value;
                sEventId = eventid.Value;
                sPolicyId = policyid.Value;
                sCovTypeCodeId = covtypecodeid.Value;
                sLossTypeCodeId = losstypecodeid.Value;     //Ankit Start : Work for JIRA - 908 (Adding LossType for Where condition)
                sTransId = transid.Value;
                sClaimantEid = claimanteid.Value;
                //rupal:start, r8 first & final payment enh
                sIsFirstFinal = IsFirstFinal.Value;                
                //rupal:end
                //added by swati MITS# 36929
                sJurisdiction = jurisdiction.Value;
                //change end here by swati
                sPolicyLOB = PolicyLOB.Value;
                sCovgSeqNum = CovgSeqNum.Value;//rupal:policy system interface
                sRsvStatusParent = RsvStatusParent.Value;
                sTransSeqNum = TransSeqNum.Value;
                sCoverageKey = CoverageKey.Value;       //Ankit Start : Worked on MITS - 34297
		 		sFieldName = FieldName.Value; ////igupta3: Ensuring field is supp or not
                switch (sCallee)
                {
                    case sPageLoad:
                        iCurrentPageNumber = 1;
                        break;
                    case sFirst:
                        iCurrentPageNumber = 1;
                        break;
                    case sNext:
                        if (Convert.ToInt32(hdPageNumber.Value) < Convert.ToInt32(hdTotalNumberOfPages.Value))
                        {
                            hdPageNumber.Value = Convert.ToString(Convert.ToInt32(hdPageNumber.Value) + 1);
                        }
                        iCurrentPageNumber = Convert.ToInt32(hdPageNumber.Value);
                        break;
                    case sPrev:
                        if (Convert.ToInt32(hdPageNumber.Value) > 1)
                        {
                            hdPageNumber.Value = Convert.ToString(Convert.ToInt32(hdPageNumber.Value) - 1);
                        }
                        iCurrentPageNumber = Convert.ToInt32(hdPageNumber.Value);
                        break;
                    case sLast:
                        hdPageNumber.Value = hdTotalNumberOfPages.Value;
                        iCurrentPageNumber = Convert.ToInt32(hdPageNumber.Value);
                        break;
                    default:
                        break;

                }
                //Ankit Start : Work for JIRA - 908 (Adding LossType for Where condition)
                objList = cb.QuickLookup(sCodetype, sLookupstring, sDescSearch, sFilter, sLOB, sJurisdiction, sSessionlob, sDeptEid, sFormname, sTriggerDate, sEventdate, sTitle, 
                                        sSessionClaimId, sOrgLevel, sInsuredEid, sEventId, sParentCodeid, iCurrentPageNumber, sPolicyId, sCovTypeCodeId, sTransId,
                                        sClaimantEid, sIsFirstFinal, sPolUnitRowId, sPolicyLOB, sCovgSeqNum, sRsvStatusParent, sTransSeqNum, sFieldName, sCoverageKey, sLossTypeCodeId); 
                codes = objList.Codes.ToList();
                iCount = codes.Count;
                hdPageNumber.Value = objList.ThisPage;
                hdTotalNumberOfPages.Value = objList.PageCount;
                //abansal23 on 05/13/2009 MITS 14847 Starts
                //if (sCodetype == "code.orgh" && ((sOrgLevel.ToLower() == "department")||(sOrgLevel=="undefined")))//abansal23 MITS 16751 on 06/16/2009
                //if (sCodetype == "code.orgh")//abansal23 MITS 16751 on 06/16/2009:Commented for Mits 18733
                if (sCodetype == "code.orgh" && ((sOrgLevel.ToLower().IndexOf("department") != -1) || (sOrgLevel == "undefined")))//Mits 18733:should execute only for department(department involved) search.
                {
                    for (int i = 0; i < iCount; i++)
                    {
                        codes[i].parentText = "";
                        for (int j = 0; j < 7; j++)
                        {
                            sParentId = codes[i].Desc.Substring(0, codes[i].Desc.IndexOf(" "));
                            codes[i].parentText = codes[i].parentText + sParentId + "//";
                            //codes[i].Desc = codes[i].Desc.Remove(0, codes[i].Desc.IndexOf(" ") + 1);
                        }                        
                        //codes[i].Desc = codes[i].Desc.Remove(0, codes[i].Desc.IndexOf("-") + 1); //Amandeep MITS 33054
                    }

                }                
                //abansal23 on 05/13/2009 MITS 14847 Ends
                if (iCount == 1 && Request.QueryString["creatable"] != "1")   //pmittal5 Mits 15308 05/07/09 - Handling Add New functionality in case of Entities
                {
                    resulttext.Value = codes[0].Desc.Trim();  //pmittal5 Mits 15308 05/07/09 - Remove white spaces
                    //zmohammad JIRA 6284 : Fix for Policy lookups
                    if (codes[0].CoverageKey != null)
                    hdnCovgKey.Value = codes[0].CoverageKey;

                    if (codes[0].CovgSeqNum != null)
                    hdnCovSeqNum.Value = codes[0].CovgSeqNum;

                    if (codes[0].TransSeqNum != null)
                    hdnTransSeqNum.Value = codes[0].TransSeqNum;
                }
                //changes for pen testing by atavaragiri (htmlcustomencode) :mits 27825

                /*else 
                 {
                  resulttext.Value = AppHelper.HTMLCustomEncode(Request.QueryString["lookupstring"]);
                } */

                else if (!String.IsNullOrEmpty(Request.QueryString["lookupstring"]))
                {
                    //changes for pen testing by atavaragiri (htmlcustomencode) :mits 27825
                    //resulttext.Value = Request.QueryString["lookupstring"]; 
                    resulttext.Value = AppHelper.HTMLCustomEncode(Request.QueryString["lookupstring"]);

                    //End :changes for pen testing by atavaragiri (htmlcustomencode) :mits 27825
                }
                //Start by Shivendu for MITS 15343
                if (iCount == 1 && string.Compare(sCodetype.ToLower().Trim(), "employee") == 0)
                {
                    string[] sEmpDetails = codes[0].Desc.Trim().Split(' ');
                    if (sEmpDetails.Length >= 1 && string.Compare(sEmpDetails[0].Trim(), sLookupstring.Trim()) == 0)
                    {
                        bNotExactMatch = false;
                    }
                }
                //End by Shivendu for MITS 15343
                hdnIsEntityRoleEnabled.Value = objList.IsEntityRoleEnabled.ToString().ToLower();//RMA-6871
                EnableDisablePageLinks(objList);
            }
            catch (FaultException<RMException> ee)
            {
                ErrorHelper.logErrors(ee);


            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        /// <summary>
        /// Enables and disables Page links and shows current page number and total number of
        /// pages
        /// </summary>
        /// <param name="objProgressNote"></param>
        public void EnableDisablePageLinks(CodeListType objList)
        {
            StringBuilder sPageDetails = new StringBuilder();
            hdTotalNumberOfPages.Value = objList.PageCount.ToString();
            hdPageNumber.Value = objList.ThisPage.ToString();
            sPageDetails.Append("Page ");
            sPageDetails.Append(hdPageNumber.Value);
            sPageDetails.Append(" Of ");
            sPageDetails.Append(hdTotalNumberOfPages.Value);
            lblPageDetails.Text = sPageDetails.ToString();
            lnkFirst.Enabled = true;
            lnkNext.Enabled = true;
            lnkPrev.Enabled = true;
            lnkLast.Enabled = true;

            if (string.Compare(hdTotalNumberOfPages.Value, "1") == 0)
            {
                lnkFirst.Enabled = false;
                lnkNext.Enabled = false;
                lnkPrev.Enabled = false;
                lnkLast.Enabled = false;
            }

            if (string.Compare(hdPageNumber.Value, "1") == 0)
            {
                lnkFirst.Enabled = false;
                lnkPrev.Enabled = false;
            }

            if (string.Compare(hdPageNumber.Value, hdTotalNumberOfPages.Value) == 0)
            {
                lnkLast.Enabled = false;
                lnkNext.Enabled = false;
            }
        }
    }
}
