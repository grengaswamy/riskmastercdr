<%--/**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            *
 **********************************************************************************************
 * 03/14/2014 | 35039   | pgupta93   | Changes req for tabing in IE
 **********************************************************************************************/--%>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CodesList.aspx.cs" Inherits="Riskmaster.UI.Codes.CodesList" %>
<%@ OutputCache CacheProfile="RMXCodeCacheProfile" %>

<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc1" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<html>
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Code Selection</title>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
    <script type="text/javascript" language="JavaScript" src="../../Scripts/getcode.js"></script>
    <!-- Start: rsushilaggar Modified the code to fix an issue in the Check Stub Text MITS 21600 -->
    <script language='javascript' type="text/javascript">
        function CheckAllclicked(chkAll) {
            var bCheckAll = document.getElementById(chkAll);
            //msampathkuma RMA-11406 start
            var table = document.getElementById('dgCodes');
            var bCheckBoxes = table.getElementsByTagName('input');
            //msampathkuma RMA-11406 end
            //var bCheckBoxes = self.document.all.tags("input");
            //prashbiharis MITS 35141 start
            var GVList1Id = 'dgCodes_chkId_';
            //var GVList1Row = 2; 
            var j = 0;
             for (var i = 0; i < bCheckBoxes.length - 1; i++) {
                 //GVList1Id = 'dgCodes_chkId_';
                 //if (GVList1Row < 10) {
                 //    GVList1Id = GVList1Id + '0';
                 //}
                 //GVList1Id = GVList1Id + GVList1Row;
                 //if (bCheckBoxes[i].id == GVList1Id + '_chkId') {

                 //    if (bCheckAll.checked == true) {

                 //        bCheckBoxes[i].checked = true;
                 //    }
                 //    else {
                 //        bCheckBoxes[i].checked = false;
                 //    }
                 //    GVList1Row = GVList1Row + 1;
                 //}
                 if (bCheckBoxes[i].id == (GVList1Id + j)) {

                     if (bCheckAll.checked == true) {

                         bCheckBoxes[i].checked = true;
                     }
                     else {
                         bCheckBoxes[i].checked = false;
                     }
                     j++;
                 }
             }
            //prashbiharis MITS 35141 End
         }



        function set() {
            //RMA-10579  msampathkuma - start
            var table = document.getElementById('dgCodes');
            var bCheckBoxes = table.getElementsByTagName('input'); //self.document.all.tags("input");
            //RMA-10579  msampathkuma - end
            //prashbiharis MITS 35141 start
            var GVList1Id = 'dgCodes_chkId_';
            //var GVList1Row = 2;
            var j = 0;
            for (var i = 0; i < bCheckBoxes.length - 1; i++) {
                //GVList1Id = 'dgCodes_ctl';
                //if (GVList1Row < 10) {
                //    GVList1Id = GVList1Id + '0';
                //}
                //GVList1Id = GVList1Id + GVList1Row;
                //if (bCheckBoxes[i].id == GVList1Id + '_chkId') {
                //    if (bCheckBoxes[i].checked == true) {
                //        var hdnField = document.getElementById(bCheckBoxes[i].id + "_hidden");
                //        if (hdnField != null) {
                //            var variables = new Array();
                //            variables = hdnField.value.split(',');
                //            selCode(variables[0], variables[1], variables[2],'true');
                //        }
                //    }
                //    GVList1Row = GVList1Row + 1;
                //}
                if (bCheckBoxes[i].id == (GVList1Id + j)) {

                    if (bCheckBoxes[i].checked == true) {
                        var hdnField = document.getElementById(GVList1Id + "hidden_"+ j);
                        if (hdnField != null) {
                            var variables = new Array();
                            variables = hdnField.value.split(',');
                            selCode(variables[0], variables[1], variables[2], 'true');
                        }
                    }
                    j++;
                }
            }
            //prashbiharis MITS 35141 End
            window.close();
        }
    </script>
    <!-- End rsushilaggar-->
</head>
<body onload="handleUnload">
    <form id="frmData" runat="server">
    <br />
    <asp:HiddenField ID ="pagecount" runat ="server" />
    <asp:HiddenField ID ="pagenumber" runat ="server" />
    <asp:HiddenField ID ="nextpage" runat ="server" />
    <asp:HiddenField ID="displayname" runat ="server" /> 
    
    
    <table width="95%" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td>
                <uc3:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="errortext"></td>
        </tr>
    </table>
    <table width="95%" cellspacing="0" cellpadding="0">
        <%if (displayname.Value == "") displayname.Value = " No Record Found";%>
        <tr>
            <td class="ctrlgroup"><%=displayname.Value%></td>
        </tr>
    </table>
    <br/>
    <!-- Start: rsushilaggar Modified the code to fix an issue in the Check Stub Text MITS 21600 --> 
     <table>
     <tr>
     <td>
     <asp:Button ID="btnOk2" runat="server" class="button" Text="  OK  " OnClientClick="set();"/>
     </td>
     </tr>
     </table>
    <!-- Start: rsushilaggar Modified the code to fix an issue in the Check Stub Text MITS 21600 -->     
    <%if (Convert.ToInt32(pagecount.Value)>1)   %>
    <%{ %>
    <table width="95%" cellspacing="0" cellpadding="1" border="0">
        <tr>
            <td colspan="4" class="headertext2">Page <%=Convert.ToInt32(pagenumber.Value)%> of <%=Convert.ToInt32(pagecount.Value)%>
            </td>
            <td nowrap="" colspan="2" align="right" class="headertext2">
      								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;
      									<%if (Convert.ToInt32(pagenumber.Value) > 1) %>
      										<%{
                                                 nextpage.Value = (Convert.ToInt32(pagenumber.Value) - 1).ToString();  
                                            %>   
      										<a class="headerlink2" href="#" onclick="getPage(1)">First &gt;</a> 
      										&nbsp;&nbsp;|&nbsp;
      										<a class="headerlink2" href="#" onclick="getPage('<%=nextpage.Value%>')">&lt; Prev</a>
       										<%}%>
       										<%else %>
       										<%{%>
       										<a class="headerlink2" style="FONT-WEIGHT: bold;" disabled>First
       										&nbsp;&nbsp;|&nbsp;
      										&lt; Prev&nbsp;&nbsp;</a>
       										<%}%> 
      									
      									<%if (Convert.ToInt32(pagenumber.Value) < Convert.ToInt32(pagecount.Value)) %>
      										<%{
                                                 nextpage.Value = (Convert.ToInt32(pagenumber.Value) + 1).ToString();
      										%>   
      										<%--bkumar33:cosmetic change--%>
      								        <a class="headerlink2" href="#" onclick="getPage('<%=nextpage.Value%>')">  |  Next &gt;</a>
      									    &nbsp;&nbsp;|&nbsp;&nbsp;
      									    <%nextpage.Value =pagecount.Value; %>
      								        <a class="headerlink2" href="#" onclick="getPage('<%=nextpage.Value%>')">Last</a>
      									    &nbsp;
      									    <%}%>
       										<%else %>
       										<%{%>
       										<a class="headerlink2" style="FONT-WEIGHT: bold;" disabled>| Next &gt;
       										&nbsp;&nbsp;|&nbsp;&nbsp;
       										Last
       										&nbsp;|</a>
       										<%}%>
      							
     </td>
        </tr>
     </table>  
     <%} %>  
        <!-- Start: rsushilaggar Modified the code to fix an issue in the Check Stub Text MITS 21600 -->
    <asp:GridView ID="dgCodes" AutoGenerateColumns="False" runat="server" 
        Font-Bold="True" CellPadding="0"  PagerSettings-Mode="NumericFirstLast"
        GridLines="None" CellSpacing="3" Width="95%" OnRowDataBound ="dgCodes_RowDataBound"
        onprerender="dgCodes_PreRender" OnDataBound="dgCodes_DataBound">
        <%--OnSorting="grdView_OnSorting" OnPageIndexChanging="grdView_PageIndexChanging" AllowSorting="True" AllowPaging="True" OnRowCreated="GridView_RowCreated" --%>
        <PagerStyle CssClass="headertext2" ForeColor="White" />
        <HeaderStyle CssClass="msgheader"/>
        <PagerSettings Mode="NumericFirstLast"></PagerSettings>
        <FooterStyle CssClass="headertext2" />
        <RowStyle CssClass="Bold2" />
        <Columns>
            <asp:TemplateField  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="msgheader" HeaderStyle-Width="4%" >
                <HeaderTemplate>
                    <asp:CheckBox ID="chkAll" runat="server" AutoPostBack="true"  />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="chkId" runat="server" value='<%# Eval("ShortCode")%>' />
                    <asp:HiddenField ID="chkId_hidden" runat="server"  />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField SortExpression="1" HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="msgheader" >
                <HeaderTemplate><!--  MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17-->
                     <a style="color:White;" href="#" tabindex="-1" onclick="getSorting('1')">Code</a>
                </HeaderTemplate>
                <ItemTemplate>
                    <%# Eval("ShortCode")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField SortExpression="2" HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                <HeaderTemplate><!--  MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17-->
                     <a style="color:White;" href="#" tabindex="-1" onclick="getSorting('2')">Description</a>
                </HeaderTemplate>
                <ItemTemplate>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField SortExpression="3" HeaderStyle-HorizontalAlign="Left">
                 <HeaderTemplate><!--  MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17-->
                     <a style="color:White;" href="#" tabindex="-1"  onclick="getSorting('3')">Parent Code</a>
                </HeaderTemplate>
                <ItemTemplate>
                    <%# Eval("ParentCode")%>
                    <%# Eval("parentDesc")%>
                </ItemTemplate>
                <ControlStyle CssClass="msgheader" />
            </asp:TemplateField><%--skhare7 27920--%>
             <asp:TemplateField  HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="msgheader" >
                <ItemTemplate>
                    <%# Eval("resstatus")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField  HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="msgheader" >
                <ItemTemplate>
                    <%# Eval("CovgSeqNum")%>
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField  HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="msgheader" >
                <ItemTemplate>
                    <%# Eval("TransSeqNum")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField  HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="msgheader" >
                <ItemTemplate>
                    <%# Eval("CoverageKey")%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
        <table cellspacing="3" width="95%">
            <tr >
                <td class="msgheader"></td>
				<td class="msgheader">Code</td>
				<td class="msgheader">Description</td>
				<td class="msgheader">Parent Code</td>
			</tr>
        </table>
        </EmptyDataTemplate> 
        <HeaderStyle BackColor="#B8CCE4" Font-Bold="True" ForeColor="White" />
        <FooterStyle BackColor="#B8CCE4" Font-Bold="True" ForeColor="White" />
    </asp:GridView>
    <%if (Convert.ToInt32(pagecount.Value)>1)   %>
    <%{ %>
    <table width="95%" cellspacing="0" cellpadding="1" border="0">
        <tr>
            <td colspan="4" class="headertext2">Page <%=Convert.ToInt32(pagenumber.Value)%> of <%=Convert.ToInt32(pagecount.Value)%>
            </td>
            <td nowrap="" colspan="2" align="right" class="headertext2">
      								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;
      									<%if (Convert.ToInt32(pagenumber.Value) > 1) %>
      										<%{
                                                 nextpage.Value = (Convert.ToInt32(pagenumber.Value) - 1).ToString();  
                                            %>   
      										<a class="headerlink2" href="#" onclick="getPage(1)">First &gt;</a> 
      										&nbsp;&nbsp;|&nbsp;
      										<a class="headerlink2" href="#" onclick="getPage('<%=nextpage.Value%>')">&lt; Prev</a>
       										<%}%>
       										<%else %>
       										<%{%>
       										<a class="headerlink2" style="FONT-WEIGHT: bold;" disabled>First &gt;
       										&nbsp;&nbsp;|&nbsp;
      										&lt; Prev&nbsp;&nbsp;|</a>
       										<%}%> 
      									
      									<%if (Convert.ToInt32(pagenumber.Value) < Convert.ToInt32(pagecount.Value)) %>
      										<%{
                                                 nextpage.Value = (Convert.ToInt32(pagenumber.Value) + 1).ToString();
      										%>   
      								        <a class="headerlink2" href="#" onclick="getPage('<%=nextpage.Value%>')">Next &gt;</a> 
      									    &nbsp;&nbsp;|&nbsp;&nbsp;
      									    <%nextpage.Value =pagecount.Value; %>
      								        <a class="headerlink2" href="#" onclick="getPage('<%=nextpage.Value%>')">Last</a>
      									    &nbsp;
      									    <%}%>
       										<%else %>
       										<%{%>
       										<a class="headerlink2" style="FONT-WEIGHT: bold;" disabled>| Next &gt;
       										&nbsp;&nbsp;|&nbsp;&nbsp;
       										Last</a>
       										&nbsp;
       										<%}%>
      							
     </td>
        </tr>
     </table>  
     <%} %>
      
      <!-- Start: rsushilaggar Modified the code to fix an issue in the Check Stub Text MITS 21600 --> 
     <table>
     <tr>
     <td>
     <asp:Button ID="btnOk" runat="server" class="button" Text="  OK  " OnClientClick="set();"/>
     </td>
     </tr>
     </table>
    <!-- Start: rsushilaggar Modified the code to fix an issue in the Check Stub Text MITS 21600 -->     
    <uc2:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="Please wait while we process your request.." />
    <asp:HiddenField ID="hdnSortExpression" runat="server" Value="1" />
    <asp:HiddenField ID="hdSortOrder" runat="server"/><!--MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17-->
    <asp:HiddenField ID="hdSortColumn" runat="server"/><!--MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17-->
    <asp:HiddenField ID ="pageaction" runat ="server" /><!--MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17-->
    
    </form>
</body>
</html>
