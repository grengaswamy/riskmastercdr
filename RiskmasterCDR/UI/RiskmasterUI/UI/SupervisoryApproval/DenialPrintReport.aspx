﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DenialPrintReport.aspx.cs" Inherits="Riskmaster.UI.SupervisoryApproval.DenialPrintReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frmData" runat="server">
    <rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />
    <div>
    <font size="4" color="red"><asp:Label ID="lblNoreport" Visible=false Text="No report to print." runat=server></asp:Label></font>
    </div>
    </form>
</body>
</html>
