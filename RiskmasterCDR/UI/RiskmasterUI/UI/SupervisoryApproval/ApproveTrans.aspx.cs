﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using Newtonsoft.Json;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;
using System.Xml.XPath;
using System.Text;


namespace Riskmaster.UI.UI.SupervisoryApproval
{
    public partial class ApproveTrans : NonFDMBasePageCWS
    {
        string Const_Width = "150";
        XElement resultDoc = null;
        public XmlDocument Model = null;
        public int selvalue = 0;
        //static ErrorControl objErrorCtl;
        static string sSuccess = "Success";
        static string sPageName = "ApproveTrans.aspx";
        private static string strSearchMessageTempalte = "<Message><Authorization>34880aa1-ee69-4766-8c95-2afd5fe434d2</Authorization><Call>" +
         "<Function>SupervisorApprovalAdaptor.ListFundsTransactions</Function></Call><Document><SearchParam><PayeeLastNameComparison>=</PayeeLastNameComparison><PayeeLastName></PayeeLastName><PayeeFirstNameComparison>=</PayeeFirstNameComparison><PayeeFirstName></PayeeFirstName><PrimaryClaimantLastNameComparison>=</PrimaryClaimantLastNameComparison><PrimaryClaimantLastName></PrimaryClaimantLastName><PrimaryClaimantFirstNameComparison>=</PrimaryClaimantFirstNameComparison><PrimaryClaimantFirstName></PrimaryClaimantFirstName><FromDateComparison>between</FromDateComparison><FromDate></FromDate><ToDate></ToDate><PayeeStatusComparison>=</PayeeStatusComparison><PayeeStatus codeid=\"\"></PayeeStatus><ShowAllItems>False</ShowAllItems><ShowPayeeOnHold/><SearchEnabled></SearchEnabled><LangCode></LangCode><ContentType>JSON</ContentType><GridId/><PageName/></SearchParam></Document></Message>";
        private string m_sEnableOffSet = "true";  //mcapps2 MITS 30236
        private string m_sEntityApprovalFlag = "true";
        private string m_sLssInvoice = "true";
        private string m_sEnableVoidReason = "true";
        private static string strPageName = "ApproveTrans.aspx";
        private static string strGridID = "gridPaymentApproval|gridCollectionApproval|gridDenialPayment|gridMyPendingTransaction";
        protected void Page_Load(object sender, EventArgs e)
        {
            //RMA-13502     achouhan3   Added fot print funcationality Starts
            btnPrint.Attributes.Add("onclick", "return WindowOpen();");
            btnDenialPrint.Attributes.Add("onclick", "return WindowOpenDenial();");
            btnColPrint.Attributes.Add("onclick", "return WindowOpenCol();");
            //RMA-13502     achouhan3   Added fot print funcationality Ends
            TransDateop.Attributes.Add("onchange", "return BetweenOption('date','TransDate');");
            string NgGridToolTips = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("rmANgGrid.aspx".ToUpper()), "NgGridToolTips", ((int)RMXResourceProvider.RessoureType.TOOLTIP).ToString());
            ClientScript.RegisterClientScriptBlock(this.GetType(), "NgGridToolTips", NgGridToolTips, true);

            string NgGridLabels = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("rmANgGrid.aspx".ToUpper()), "NgGridLabels", ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            ClientScript.RegisterClientScriptBlock(this.GetType(), "NgGridLabels", NgGridLabels, true);

            string NgGridAlertMessages = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("rmANgGrid.aspx".ToUpper()), "NgGridAlertMessages", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterClientScriptBlock(this.GetType(), "NgGridAlertMessages", NgGridAlertMessages, true);

            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("ApproveTrans.aspx"), "PaymentValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "PaymentValidations", sValidationResources, true);


            Model = new XmlDocument();
            try
            {
                SetControlText();
                hdnLangCode.Text = AppHelper.GetLanguageCode();
                txtContent.Text = "JSON";
                txtGridID.Text = strGridID;
                txtPageName.Text = strPageName;
                if (!Page.IsPostBack)
                {
                    if (!string.IsNullOrEmpty(AppHelper.GetValue("MyTrans")) && AppHelper.GetValue("MyTrans") == "true")
                    {
                        if (!string.IsNullOrEmpty(AppHelper.GetValue("Parent")) && AppHelper.GetValue("Parent") == "MyWork")
                            divCheckItems.Visible = false;
                        else
                            divCheckItems.Visible = true;
                        hdnParent.Text = AppHelper.GetValue("MyTrans");
                        divSearch.Visible = false;
                        divShowItems.Visible = false; 
                        secColButton.Visible = false;
                        tddeny.Visible = false;
                        tdapprove.Visible = false;
                        tdvoid.Visible = false;
                        secVoidReason.Visible = false;
                        secOperation.Visible = false;
                        reason.Visible = false;
                        spnReason.Visible = false;
                        

                    }
                    else
                    {
                        divCheckItems.Visible = true;
                        divSearch.Visible = true;
                        hdnParent.Text = String.Empty;
                        divShowItems.Visible = true;
                        secColButton.Visible = true;
                        tddeny.Visible = true;
                        tdapprove.Visible = true;
                        tdvoid.Visible = true;
                        secVoidReason.Visible = true;
                        secOperation.Visible = true;
                        reason.Visible = true;
                        spnReason.Visible = true;
                    }
                    NonFDMCWSPageLoad("SupervisorApprovalAdaptor.ListFundsTransactions");
                    Model = new XmlDocument();
                    Model = Data;
                    BindpageControls(Model.InnerXml);
                }

            } // try
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            } // catch
        }

        private void BindpageControls(string sCWSresponse)
        {
            XmlDocument xResponse = new XmlDocument();
            XmlElement xDataElement = null;
            
            try
            {
                xResponse.LoadXml(sCWSresponse);
                //pkandhari JIRA 8889 starts
                //Permission checks for void,deny,approve Starts
                if (String.IsNullOrEmpty(AppHelper.GetValue("MyTrans")) || AppHelper.GetValue("MyTrans") != "true")
                {
                    XmlNode NodeApprove = xResponse.SelectSingleNode("//Document/SupervisorApproval/ScreenUISettings/ApprovePermission");
                    if (NodeApprove != null && NodeApprove.InnerText == Boolean.FalseString)
                    {
                        tdapprove.Visible = false;
                        lbl_overrideamount.Visible = false;
                        tboverrideamount.Visible = false;
                        lbl_applyoverride.Visible = false;
                        applyoverride.Visible = false;
                    }
                    XmlNode NodeDeny = xResponse.SelectSingleNode("//Document/SupervisorApproval/ScreenUISettings/DenyPermission");
                    if (NodeDeny != null && NodeDeny.InnerText == Boolean.FalseString)
                        tddeny.Visible = false;
                    XmlNode NodeVoid = xResponse.SelectSingleNode("//Document/SupervisorApproval/ScreenUISettings/VoidPermission");
                    if (NodeVoid != null && NodeVoid.InnerText == Boolean.FalseString)
                        tdvoid.Visible = false;
                    //Permission checks for void,deny,approve Ends
                    //pkandhari JIRA 8889 starts

                    XmlNode Node = xResponse.SelectSingleNode("//Document/SupervisorApproval/ScreenUISettings/UseEntityPaymentApproval");
                    if (Node != null && Node.InnerText == "false")
                    {
                        m_sEntityApprovalFlag = Node.InnerText;
                    }
                    Node = xResponse.SelectSingleNode("//Document/SupervisorApproval/ScreenUISettings/ShowLSSLink");
                    if (Node != null && Node.InnerText == "false")
                    {
                        m_sLssInvoice = Node.InnerText;
                    }


                    //Rahul Aggarwal- Void Payment Reason Start- 05/20/2010 MITS 19970
                    Node = xResponse.SelectSingleNode("//Document/SupervisorApproval/ScreenUISettings/EnableVoidReason");
                    if (Node != null)
                        m_sEnableVoidReason = Node.InnerText;

                    if (m_sEnableVoidReason == "true")
                    {
                        if (NodeVoid != null && NodeVoid.InnerText == Boolean.FalseString)
                        {
                            voidreason.Visible = false;
                            voidreasonbtnMemo.Visible = false;
                            lbl_txtVoidReason.Visible = false;
                            reason.Visible = false;
                            spnReason.Visible = false;
                            voidreason_HTML.Text = string.Empty;
                            //hdnVoidreasonFlag.Value = "false";
                        }
                        else
                        {
                            voidreason.Visible = true;
                            voidreasonbtnMemo.Visible = true;
                            lbl_txtVoidReason.Visible = true;
                            //lblReason.Visible = false;
                            reason.Visible = false;
                            spnReason.Visible = false;
                            voidreason_HTML.Text = string.Empty;
                            //hdnVoidreasonFlag.Value = "true";//averma62 - MITS 29130
                        }
                    }
                    else
                    {
                        voidreason.Visible = false;
                        voidreasonbtnMemo.Visible = false;
                        lbl_txtVoidReason.Visible = false;
                        //lblReason.Visible = true;
                        reason.Visible = true;
                        spnReason.Visible = true;
                        //hdnVoidreasonFlag.Value = "false";//averma62 - MITS 29130
                    }
                    hdnEntityApprovalFlag.Value = m_sEntityApprovalFlag;
                    //Rahul Aggarwal- Void Payment Reason-End
                    //mcapps2 MITS 30236 Start
                    Node = xResponse.SelectSingleNode("//Document/SupervisorApproval/ScreenUISettings/EnableOffset");
                    if (Node != null)
                    {
                        m_sEnableOffSet = Node.InnerText.ToLower();
                    }
                    if (m_sEnableOffSet == "true")
                    {
                        if (NodeApprove != null && NodeApprove.InnerText == Boolean.FalseString)
                        {
                            applyoffset.Visible = false;
                            lbl_applyoffset.Visible = false;
                        }
                        else
                        {
                            applyoffset.Visible = true;
                            lbl_applyoffset.Visible = true;
                        }
                    }
                    else
                    {
                        applyoffset.Visible = false;
                        lbl_applyoffset.Visible = false;
                    }
                }
                
                
                xDataElement = (XmlElement)xResponse.SelectSingleNode("//SupervisorApproval//AllTransactions/PaymentTransaction/Data");
                if (xDataElement != null)
                    hdnJsonData_gridPaymentApproval.Value = xDataElement.InnerText;

                xDataElement = (XmlElement)xResponse.SelectSingleNode("//SupervisorApproval//AllTransactions/PaymentTransaction/UserPref");
                if (xDataElement != null)
                    hdnJsonUserPref_gridPaymentApproval.Value = xDataElement.InnerText;

                xDataElement = (XmlElement)xResponse.SelectSingleNode("//SupervisorApproval//AllTransactions/PaymentTransaction/AdditionalData");
                if (xDataElement != null)
                    hdnJsonAdditionalData_gridPaymentApproval.Value = xDataElement.InnerText;

                xDataElement = (XmlElement)xResponse.SelectSingleNode("//SupervisorApproval//AllTransactions/CollectionTransaction/Data");
                if (xDataElement != null)
                    hdnJsonData_gridCollectionApproval.Value = xDataElement.InnerText;
                xDataElement = (XmlElement)xResponse.SelectSingleNode("//SupervisorApproval//AllTransactions/CollectionTransaction/UserPref");
                if (xDataElement != null)
                    hdnJsonUserPref_gridCollectionApproval.Value = xDataElement.InnerText;

                xDataElement = (XmlElement)xResponse.SelectSingleNode("//SupervisorApproval//AllTransactions/CollectionTransaction/AdditionalData");
                if (xDataElement != null)
                    hdnJsonAdditionalData_gridCollectionApproval.Value = xDataElement.InnerText;

                xDataElement = (XmlElement)xResponse.SelectSingleNode("//SupervisorApproval//DenyTransactions//Data");
                if (xDataElement != null)
                    hdnJsonData_gridDenialPayment.Value = xDataElement.InnerText;

                xDataElement = (XmlElement)xResponse.SelectSingleNode("//SupervisorApproval//DenyTransactions//UserPref");
                if (xDataElement != null)
                    hdnJsonUserPref_gridDenialPayment.Value = xDataElement.InnerText;

                xDataElement = (XmlElement)xResponse.SelectSingleNode("//SupervisorApproval//DenyTransactions//AdditionalData");
                if (xDataElement != null)
                    hdnJsonAdditionalData_gridDenialPayment.Value = xDataElement.InnerText;

                if (!string.IsNullOrEmpty(hdnJsonUserPref_gridDenialPayment.Value))
                {
                    if (!string.IsNullOrEmpty(AppHelper.GetValue("MyTrans")) && AppHelper.GetValue("MyTrans") == "true")
                    {
                        if (!string.IsNullOrEmpty(AppHelper.GetValue("Parent")) && AppHelper.GetValue("Parent") == "MyWork")
                            divCheckItems.Visible = false;
                        else
                            divCheckItems.Visible = true;
                    }
                    else
                        divCheckItems.Visible = false;
                }
            }
            finally
            {
                xResponse = null;
                xDataElement = null;
            }

        }

        /// <summary>
        /// Author: Rahul Aggarwal
        /// This function is used to search the transaction based upon the search criteria
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                NonFDMCWSPageLoad("SupervisorApprovalAdaptor.ListFundsTransactions");
                Model = new XmlDocument();
                Model = Data;
                BindpageControls(Model.InnerXml);
            } // try
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            } // catch
        }

        private static XElement GetFundsTransactionMessageTemplate()        //ajohari2 RMA 6404 and RMA 9875 ADDED PayeeStatus node
        {
            string strMessageTemplate = "<Message><Authorization>34880aa1-ee69-4766-8c95-2afd5fe434d2</Authorization><Call>" +
          "<Function>SupervisorApprovalAdaptor.ListFundsTransactions</Function></Call><Document><SupervisorApproval><ShowAllItems/> <ShowPayeeOnHold/> <AllTransactions>Y</AllTransactions><DenyTransactions>Y" +
          "</DenyTransactions><GridId/><PageName/><ContentType>JSON</ContentType><ShowMyTrans/><LangCode>1033</LangCode></SupervisorApproval></Document></Message>";
            XElement xFundsMessageTemplate = XElement.Parse(strMessageTemplate);
            return xFundsMessageTemplate;
        }

        [System.Web.Services.WebMethod]
        public static string BindDataToGrid(string showAll, string showMyTrans,string payeeStatus)
        {


            XmlDocument resultDoc = new XmlDocument();
            XmlElement xmlIn = null;
            Dictionary<string, string> dicResponseData = new Dictionary<string, string>(); ;
            Dictionary<string, string> dicInputData;
            XElement messageElement;
            string sCWSresponse = "";

            try
            { 
                dicInputData = new Dictionary<string, string>();

                messageElement = GetFundsTransactionMessageTemplate();//GetMessageTemplate(userId, Convert.ToBoolean(showAll));
                XElement xNodeShowAllItems = messageElement.XPathSelectElement("//SupervisorApproval//ShowAllItems");
                if (xNodeShowAllItems != null && !String.IsNullOrEmpty(showAll) && showAll.Trim().ToLower()=="true")
                    xNodeShowAllItems.Value = showAll;
                else if (xNodeShowAllItems!=null)
                    xNodeShowAllItems.Value = Boolean.FalseString;
                //ajohari2 RMA 6404 and RMA 9875
                XElement xNodePayeeStatus = messageElement.XPathSelectElement("//SupervisorApproval//ShowPayeeOnHold");
                if (xNodePayeeStatus != null && !String.IsNullOrEmpty(payeeStatus) && payeeStatus.Trim().ToLower() == "true")
                    xNodePayeeStatus.Value = payeeStatus;
                //ajohari2 RMA 6404 and RMA 9875 END

                XElement xNodeShowMyTrans = messageElement.XPathSelectElement("//SupervisorApproval//ShowMyTrans");
                if (xNodeShowMyTrans != null && !String.IsNullOrEmpty(showMyTrans))
                    xNodeShowMyTrans.Value = showMyTrans;
                XElement xNodeGridID = messageElement.XPathSelectElement("//SupervisorApproval//GridId");
                if (xNodeGridID != null)
                    xNodeGridID.Value = strGridID;
                XElement xNodePageName = messageElement.XPathSelectElement("//SupervisorApproval//PageName");
                if (xNodePageName != null)
                    xNodePageName.Value = "ApproveTrans.aspx";
                sCWSresponse = AppHelper.CallCWSService(messageElement.ToString());
                resultDoc.LoadXml(sCWSresponse);

                resultDoc.LoadXml(sCWSresponse);
                if (resultDoc.SelectSingleNode("//MsgStatus/MsgStatusCd").InnerText == sSuccess)
                {
                    dicResponseData.Add("response", sSuccess);
                }
                else if (resultDoc.SelectSingleNode("//MsgStatus/MsgStatusCd").InnerText=="Error")
                {
                    dicResponseData.Add("response", ErrorHelper.UpdateErrorMessage(resultDoc.SelectSingleNode("//ExtendedStatusDesc").InnerText));
                }
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//SupervisorApproval//AllTransactions//PaymentTransaction//Data");
                if (xmlIn != null)
                {
                    dicResponseData.Add("PaymentData", xmlIn.InnerText);
                }
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//SupervisorApproval//AllTransactions//PaymentTransaction//UserPref");
                if (xmlIn != null)
                {
                    dicResponseData.Add("PaymentUserPref", xmlIn.InnerText);
                }
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//SupervisorApproval//AllTransactions//PaymentTransaction//AdditionalData");
                if (xmlIn != null)
                {
                    dicResponseData.Add("PaymentAdditionalData", xmlIn.InnerText);
                }
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//SupervisorApproval//AllTransactions//CollectionTransaction//Data");
                if (xmlIn != null)
                {
                    dicResponseData.Add("CollectionData", xmlIn.InnerText);
                }
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//SupervisorApproval//AllTransactions//CollectionTransaction//UserPref");
                if (xmlIn != null)
                {
                    dicResponseData.Add("CollectionUserPref", xmlIn.InnerText);
                }
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//SupervisorApproval//AllTransactions//CollectionTransaction//AdditionalData");
                if (xmlIn != null)
                {
                    dicResponseData.Add("CollectionAdditionalData", xmlIn.InnerText);
                }

                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//SupervisorApproval//DenyTransactions//Data");
                if (xmlIn != null)
                {
                    dicResponseData.Add("DenialData", xmlIn.InnerText);
                }
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//SupervisorApproval//DenyTransactions//UserPref");
                if (xmlIn != null)
                {
                    dicResponseData.Add("DenialUserPref", xmlIn.InnerText);
                }
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//SupervisorApproval//DenyTransactions//AdditionalData");
                if (xmlIn != null)
                {
                    dicResponseData.Add("DenialAdditionalData", xmlIn.InnerText);
                }
                return JsonConvert.SerializeObject(dicResponseData);
            }
            catch (Exception ex)
            {  
                if (!dicResponseData.ContainsKey("response"))
                    dicResponseData.Add("response", ex.Message);
                return JsonConvert.SerializeObject(dicResponseData);
            }
            finally
            {
                resultDoc = null;
                xmlIn = null;
                dicResponseData = null;
                dicInputData = null;
                messageElement = null;
            }
            
        }

        /// <summary>
        /// this function i sused to save preferences for the grid in the database
        /// </summary>
        /// <param name="gridPreference"></param>
        /// <param name="gridId"></param>
        /// <returns></returns>
        [System.Web.Services.WebMethod]
        public static string SavePreferences(string gridPreference, string gridId)
        {

            StringBuilder sbMessageTemplate = new StringBuilder();
            string sReturnResponse = "";
            Dictionary<string, string> dicResponse = new Dictionary<string, string>();
            try
            {
                gridPreference = AppHelper.HtmlEncodeString(gridPreference);
                sbMessageTemplate.Append("<Message>");
                sbMessageTemplate.Append("<Authorization></Authorization>");
                sbMessageTemplate.Append("<Call>");
                sbMessageTemplate.Append("<Function>GridPreferenceAdaptor.SaveUserHeaderAndPreference</Function>");
                sbMessageTemplate.Append("</Call>");
                sbMessageTemplate.Append("<Document>");
                sbMessageTemplate.Append("<FinancialDetailHistory>");
                sbMessageTemplate.Append("<GridId>");
                sbMessageTemplate.Append(gridId);
                sbMessageTemplate.Append("</GridId>");
                sbMessageTemplate.Append("<PageName>ApproveTrans.aspx</PageName>");
                sbMessageTemplate.Append("<UserPref>" + gridPreference + "</UserPref>");
                sbMessageTemplate.Append("</FinancialDetailHistory>");
                sbMessageTemplate.Append("</Document>");
                sbMessageTemplate.Append("</Message>");
                string sReturn = AppHelper.CallCWSService(sbMessageTemplate.ToString()); //return bool.   
                XmlDocument objXmlDoc = new XmlDocument();
                objXmlDoc.LoadXml(sReturn);

                if (objXmlDoc.SelectSingleNode("//MsgStatus/MsgStatusCd").InnerText == "Error")
                {
                    dicResponse.Add("error", "true");
                    dicResponse.Add("errorMessage", ErrorHelper.UpdateErrorMessage(objXmlDoc.SelectSingleNode("//ExtendedStatusDesc").InnerText));
                    sReturnResponse = Newtonsoft.Json.JsonConvert.SerializeObject(dicResponse);
                }
                //else                    
                //    dicResponse.Add("response", sSuccess);                
                sReturnResponse = Newtonsoft.Json.JsonConvert.SerializeObject(dicResponse);
            }
            catch (Exception ex)
            {
                dicResponse.Add("error", "true");
                dicResponse.Add("errorMessage", ex.Message);
                sReturnResponse = Newtonsoft.Json.JsonConvert.SerializeObject(dicResponse);
            }
            finally
            {
                sbMessageTemplate = null;
                dicResponse = null;
            }
            return sReturnResponse;

        }

        /// <summary>
        /// this function is used to restore default values for the grid.
        /// </summary>
        /// <param name="inputData"></param>
        /// <returns></returns>
        [System.Web.Services.WebMethod]
        public static string RestoreDefault(string inputData)
        {
            StringBuilder sbMessageTemplate = new StringBuilder();
            XmlDocument objXmlDoc = new XmlDocument();
            Dictionary<string, string> dicInputData;
            string sResponseData = "";
            XmlElement xmlIn = null;
            Dictionary<string, string> dicResponseData = new Dictionary<string, string>();
            try
            {
                dicInputData = new Dictionary<string, string>();
                dicResponseData = new Dictionary<string, string>();
                JsonConvert.PopulateObject(inputData, dicInputData);
                sbMessageTemplate.Append("<Message>");
                sbMessageTemplate.Append("<Authorization></Authorization>");
                sbMessageTemplate.Append("<Call>");
                sbMessageTemplate.Append("<Function>SupervisoryApprovalAdaptor.RestoreDefaults</Function>");
                sbMessageTemplate.Append(" </Call>");
                sbMessageTemplate.Append("<Document>");
                sbMessageTemplate.Append("<RestoreDefault>");
                sbMessageTemplate.Append("<GridId>");
                sbMessageTemplate.Append(dicInputData["GridId"]);
                sbMessageTemplate.Append("</GridId>");
                sbMessageTemplate.Append("<PageName>ApproveTrans.aspx</PageName>");
                sbMessageTemplate.Append("</RestoreDefault>");
                sbMessageTemplate.Append("</Document>");
                sbMessageTemplate.Append("</Message>");
                string sReturn = AppHelper.CallCWSService(sbMessageTemplate.ToString());
                objXmlDoc.LoadXml(sReturn);
                if (objXmlDoc.SelectSingleNode("//MsgStatus/MsgStatusCd").InnerText == sSuccess)
                {
                    xmlIn = (XmlElement)objXmlDoc.SelectSingleNode("//UserPref");
                    if (xmlIn != null)
                    {
                        sResponseData = xmlIn.InnerText;
                    }
                }
                else
                {//error handling
                    dicResponseData.Add("error", "true");
                    dicResponseData.Add("errorMessage", ErrorHelper.UpdateErrorMessage(objXmlDoc.SelectSingleNode("//ExtendedStatusDesc").InnerText));
                    sResponseData = Newtonsoft.Json.JsonConvert.SerializeObject(dicResponseData);
                }
            }
            catch (Exception ex)
            {
                dicResponseData.Add("error", "true");
                dicResponseData.Add("errorMessage", ex.Message);
                sResponseData = Newtonsoft.Json.JsonConvert.SerializeObject(dicResponseData);
            }

            finally
            {
                sbMessageTemplate = null;
                objXmlDoc = null;
                dicInputData = null;
                xmlIn = null;
                dicResponseData = null;
            }
            return sResponseData;
        }

        [System.Web.Services.WebMethod]
        public static string ActionMethod(string postData, string searchInput)
        {
            XmlDocument resultDoc = new XmlDocument();
            XmlElement xmlIn = null;
            Dictionary<string, string> dicResponseData  = new Dictionary<string, string>();;
            XElement messageElement;
            string sCWSresponse = "";
            string strMessageTempalte = String.Empty; ;
            Dictionary<string, string> dictPostData;
            Dictionary<string, string> dictSearchModel;
            System.Text.StringBuilder sError = new StringBuilder(); //RMA-13778 achouhan3   Added to handle multiple messsages
            try
            {
               
                dictPostData = new Dictionary<string, string>();
                JsonConvert.PopulateObject(postData, dictPostData);
                if (dictPostData["CallingAction"] == "Void")
                {
                    strMessageTempalte = "<Message><Authorization>34880aa1-ee69-4766-8c95-2afd5fe434d2</Authorization><Call>" +
         "<Function>SupervisorApprovalAdaptor.VoidSuperChecks</Function></Call><Document><SupervisorApproval><TransIds><TransId/></TransIds><Reason/><VoidReason/><VoidReason_HTMLComments/></SupervisorApproval></Document></Message>"; ;
                }
                else if (dictPostData["CallingAction"] == "Approve")
                {
                    strMessageTempalte = "<Message><Authorization>34880aa1-ee69-4766-8c95-2afd5fe434d2</Authorization><Call>" +
         "<Function>SupervisorApprovalAdaptor.ApproveSuperChecks</Function></Call><Document><SupervisorApproval><TransIds><TransId/><ClaimId></ClaimId><OverRideAmount></OverRideAmount><ApplyOverRide></ApplyOverRide><ApplyOffSet></ApplyOffSet></TransIds><Reason/></SupervisorApproval></Document></Message>";   //mcapps2 MITS 30236
                }
                else if (dictPostData["CallingAction"] == "Deny")
                {
                    strMessageTempalte = "<Message><Authorization>34880aa1-ee69-4766-8c95-2afd5fe434d2</Authorization><Call>" +
         "<Function>SupervisorApprovalAdaptor.DenySuperChecks</Function></Call><Document><SupervisorApproval><TransIds/><TransId/><Reason/></SupervisorApproval></Document></Message>";
                }

                if (dictPostData["CallingAction"] == "Deny" || dictPostData["CallingAction"] == "Approve" || dictPostData["CallingAction"] == "Void")
                {
                    strMessageTempalte = AppHelper.ChangeMessageValue(strMessageTempalte, "//TransId", dictPostData["TransId"].TrimEnd(','));
                    //rsushilaggar: Void check Reason mits 19970
                    if (dictPostData["CallingAction"] == "Void")
                    {
                        strMessageTempalte = AppHelper.ChangeMessageValue(strMessageTempalte, "//VoidReason", dictPostData["VoidReason"]);
                        strMessageTempalte = AppHelper.ChangeMessageValue(strMessageTempalte, "//VoidReason_HTMLComments", dictPostData["VoidReason"]);
                    }
                    else if (dictPostData["CallingAction"] == "Deny" || dictPostData["CallingAction"] == "Approve")
                        strMessageTempalte = AppHelper.ChangeMessageValue(strMessageTempalte, "//Reason", dictPostData["Comments"]);
                    //End: rsushilaggar
                    strMessageTempalte = AppHelper.ChangeMessageValue(strMessageTempalte, "//ApplyOverRide", dictPostData["IsOverride"]);
                    strMessageTempalte = AppHelper.ChangeMessageValue(strMessageTempalte, "//OverRideAmount", dictPostData["OverrideAmount"]);
                    strMessageTempalte = AppHelper.ChangeMessageValue(strMessageTempalte, "//ApplyOffSet", dictPostData["IsApplyOffset"]);  //mcapps2 MITS 30236
                }

                if (dictPostData["CallingAction"] == "DenialDeny")
                {
                    strMessageTempalte = "<Message><Authorization>34880aa1-ee69-4766-8c95-2afd5fe434d2</Authorization><Call>" +
         "<Function>SupervisorApprovalAdaptor.DenySuperChecks</Function></Call><Document><SupervisorApproval><TransIds/><TransId/><Reason/></SupervisorApproval></Document></Message>";
                    strMessageTempalte = AppHelper.ChangeMessageValue(strMessageTempalte, "//TransId", dictPostData["TransId"].TrimEnd(','));
                    strMessageTempalte = AppHelper.ChangeMessageValue(strMessageTempalte, "//Reason", dictPostData["Comments"]);
                } // if
                string sReturn = AppHelper.CallCWSService(strMessageTempalte.ToString());
                resultDoc.LoadXml(sReturn);
                if (resultDoc.SelectSingleNode("//MsgStatus/MsgStatusCd").InnerText == sSuccess)
                {
                    dicResponseData.Add("response", sSuccess);
                }
                else if (resultDoc.SelectSingleNode("//MsgStatus/MsgStatusCd").InnerText == "Error")
                {
                    //RMA-13778 achouhan3   Added to handle multiple messsages
                    sError.Append(ErrorHelper.UpdateErrorMessage(resultDoc.SelectSingleNode("//ExtendedStatusDesc").InnerText));
                }
                strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//LangCode", AppHelper.GetLanguageCode());
                strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//PageName", strPageName);
                strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//GridId", strGridID);

                //RMA-13630     achouhan3   Modified to remove extra added records
                if (dictPostData["isShowAll"] != null && dictPostData["isShowAll"].ToLower()=="true")
                    strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//ShowAllItems", dictPostData["isShowAll"]);
                else
                    strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//ShowAllItems", Boolean.FalseString);
                if (dictPostData.ContainsKey("isHoldPayeeStatus") && dictPostData["isHoldPayeeStatus"] != null && dictPostData["isHoldPayeeStatus"].ToLower() == "true" )
                    strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//ShowPayeeOnHold", dictPostData["isHoldPayeeStatus"]);
                dictSearchModel = new Dictionary<string, string>();
                JsonConvert.PopulateObject(searchInput, dictSearchModel);
                if (dictSearchModel != null)
                {
                    strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//SearchEnabled",Convert.ToString(dictSearchModel["IsSearchEnabled"]).ToLower());
                    strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//SearchParam//PayeeLastNameComparison", dictSearchModel["PayeeLastNameComparison"]);
                    strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//SearchParam//PayeeLastName", dictSearchModel["PayeeLastName"]);
                    strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//SearchParam//PayeeFirstNameComparison", dictSearchModel["PayeeFirstNameComparison"]);
                    strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//SearchParam//PayeeFirstName", dictSearchModel["PayeeFirstName"]);
                    strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//SearchParam//PrimaryClaimantLastNameComparison", dictSearchModel["PrimaryClaimantLastNameComparison"]);
                    strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//SearchParam//PrimaryClaimantLastName", dictSearchModel["PrimaryClaimantLastName"]);
                    strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//SearchParam//PrimaryClaimantFirstNameComparison", dictSearchModel["PrimaryClaimantFirstNameComparison"]);
                    strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//SearchParam//PrimaryClaimantFirstName", dictSearchModel["PrimaryClaimantFirstName"]);
                    strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//SearchParam//FromDateComparison", dictSearchModel["TransDateComparison"]);
                    strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//SearchParam//FromDate", dictSearchModel["TransFromDate"]);
                    strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//SearchParam//ToDate", dictSearchModel["TransToDate"]);
                    strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//SearchParam//PayeeStatusComparison", dictSearchModel["PayeeStatusComparison"]);
                    strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//SearchParam//PayeeStatus", dictSearchModel["PayeeStatus"]);
                }
                sCWSresponse = AppHelper.CallCWSService(strSearchMessageTempalte.ToString());
                resultDoc.LoadXml(sCWSresponse);
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//SupervisorApproval//AllTransactions//PaymentTransaction//Data");
                if (xmlIn != null)
                {
                    dicResponseData.Add("PaymentData", xmlIn.InnerText);
                }
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//SupervisorApproval//AllTransactions//PaymentTransaction//UserPref");
                if (xmlIn != null)
                {
                    dicResponseData.Add("PaymentUserPref", xmlIn.InnerText);
                }
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//SupervisorApproval//AllTransactions//PaymentTransaction//AdditionalData");
                if (xmlIn != null)
                {
                    dicResponseData.Add("PaymentAdditionalData", xmlIn.InnerText);
                }
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//SupervisorApproval//AllTransactions//CollectionTransaction//Data");
                if (xmlIn != null)
                {
                    dicResponseData.Add("CollectionData", xmlIn.InnerText);
                }
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//SupervisorApproval//AllTransactions//CollectionTransaction//UserPref");
                if (xmlIn != null)
                {
                    dicResponseData.Add("CollectionUserPref", xmlIn.InnerText);
                }
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//SupervisorApproval//AllTransactions//CollectionTransaction//AdditionalData");
                if (xmlIn != null)
                {
                    dicResponseData.Add("CollectionAdditionalData", xmlIn.InnerText);
                }

                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//SupervisorApproval//DenyTransactions//Data");
                if (xmlIn != null)
                {
                    dicResponseData.Add("DenialData", xmlIn.InnerText);
                }
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//SupervisorApproval//DenyTransactions//UserPref");
                if (xmlIn != null)
                {
                    dicResponseData.Add("DenialUserPref", xmlIn.InnerText);
                }
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//SupervisorApproval//DenyTransactions//AdditionalData");
                if (xmlIn != null)
                {
                    dicResponseData.Add("DenialAdditionalData", xmlIn.InnerText);
                }
               //RMA-13778 achouhan3    Added to handle Error Messages Starts
                if (resultDoc.SelectSingleNode("//ErrorTransactions")!=null)
                {
                    foreach (System.Xml.XmlNode node in resultDoc.SelectNodes("//ErrorTransactions"))
                    {
                        sError.Append(node.InnerText);
                    }
                }
                if (resultDoc.SelectSingleNode("//WarningTransactions") != null)
                {
                    foreach (System.Xml.XmlNode node in resultDoc.SelectNodes("//WarningTransactions"))
                    {
                        sError.Append(node.InnerText);
                    }
                }
                dicResponseData.Add("error", ErrorHelper.UpdateErrorMessage(sError.ToString()));
                //RMA-13778 achouhan3    Added to handle Error Messages Ends
                return JsonConvert.SerializeObject(dicResponseData);
            }
            catch (Exception ex)
            {
                if (!dicResponseData.ContainsKey("error"))
                    dicResponseData.Add("error", ex.Message);
                return JsonConvert.SerializeObject(dicResponseData);
            }
            finally
            {
                resultDoc = null;
                xmlIn = null;
                dicResponseData = null;
                dictPostData = null;
                messageElement = null;
            }
            
        }


        [System.Web.Services.WebMethod]
        public static string CollectionMethod(string postData, string searchInput)
        {
            XmlDocument resultDoc = new XmlDocument();
            XmlElement xmlIn = null;
            Dictionary<string, string> dicResponseData = new Dictionary<string, string>(); ;
            XElement messageElement;
            string sCWSresponse = "";
            string strMessageTempalte = String.Empty; ;
            Dictionary<string, string> dictPostData;
            Dictionary<string, string> dictSearchModel;
            System.Text.StringBuilder sError = new StringBuilder(); //RMA-13778 achouhan3   Added to handle multiple messsages
            try
            {
                dicResponseData = new Dictionary<string, string>();
                dictPostData = new Dictionary<string, string>();
                JsonConvert.PopulateObject(postData, dictPostData);
                if (dictPostData["CallingAction"] == "Void")
                {
                    strMessageTempalte = "<Message><Authorization>34880aa1-ee69-4766-8c95-2afd5fe434d2</Authorization><Call>" +
         "<Function>SupervisorApprovalAdaptor.VoidSuperChecks</Function></Call><Document><SupervisorApproval><TransIds><TransId/></TransIds><Reason/><VoidReason/><VoidReason_HTMLComments/></SupervisorApproval></Document></Message>"; ;
                }
                else if (dictPostData["CallingAction"] == "Approve")
                {
                    strMessageTempalte = "<Message><Authorization>34880aa1-ee69-4766-8c95-2afd5fe434d2</Authorization><Call>" +
         "<Function>SupervisorApprovalAdaptor.ApproveSuperChecks</Function></Call><Document><SupervisorApproval><TransIds><TransId/><ClaimId></ClaimId><OverRideAmount></OverRideAmount><ApplyOverRide></ApplyOverRide><ApplyOffSet></ApplyOffSet></TransIds><Reason/></SupervisorApproval></Document></Message>";   //mcapps2 MITS 30236
                }
                else if (dictPostData["CallingAction"] == "Deny")
                {
                    strMessageTempalte = "<Message><Authorization>34880aa1-ee69-4766-8c95-2afd5fe434d2</Authorization><Call>" +
		 "<Function>SupervisorApprovalAdaptor.DenySuperChecks</Function></Call><Document><SupervisorApproval><TransIds/><TransId/><Reason/></SupervisorApproval></Document></Message>";
                }

                if (dictPostData["CallingAction"] == "Deny" || dictPostData["CallingAction"] == "Approve" || dictPostData["CallingAction"] == "Void")
                {
                    strMessageTempalte = AppHelper.ChangeMessageValue(strMessageTempalte, "//TransId", dictPostData["TransId"].TrimEnd(','));
                    if (dictPostData["CallingAction"] == "Deny" || dictPostData["CallingAction"] == "Approve")
                        strMessageTempalte = AppHelper.ChangeMessageValue(strMessageTempalte, "//Reason", dictPostData["Comments"]);
                }                //End: rsushilaggar 
                string sReturn = AppHelper.CallCWSService(strMessageTempalte.ToString());
                resultDoc.LoadXml(sReturn);
                if (resultDoc.SelectSingleNode("//MsgStatus/MsgStatusCd").InnerText == sSuccess)
                {
                    dicResponseData.Add("response", sSuccess);
                }
                else if (resultDoc.SelectSingleNode("//MsgStatus/MsgStatusCd").InnerText == "Error")
                {
                    //RMA-13778 achouhan3   Added to handle multiple messsages
                    sError.Append(ErrorHelper.UpdateErrorMessage(resultDoc.SelectSingleNode("//ExtendedStatusDesc").InnerText));
                }
                strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//LangCode", AppHelper.GetLanguageCode());
                strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//PageName", strPageName);
                strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//GridId", strGridID);
                //RMA-13630     achouhan3   Modified to remove extra added records
                if (dictPostData["isShowAll"] != null && dictPostData["isShowAll"].ToLower() == "true")
                    strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//ShowAllItems", dictPostData["isShowAll"]);
                else
                    strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//ShowAllItems", Boolean.FalseString);
                dictSearchModel = new Dictionary<string, string>();
                JsonConvert.PopulateObject(searchInput, dictSearchModel);
                if (dictSearchModel != null)
                {
                    strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//SearchEnabled", Convert.ToString(dictSearchModel["IsSearchEnabled"]).ToLower());
                    strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//SearchParam//PayeeLastNameComparison", dictSearchModel["PayeeLastNameComparison"]);
                    strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//SearchParam//PayeeLastName", dictSearchModel["PayeeLastName"]);
                    strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//SearchParam//PayeeFirstNameComparison", dictSearchModel["PayeeFirstNameComparison"]);
                    strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//SearchParam//PayeeFirstName", dictSearchModel["PayeeFirstName"]);
                    strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//SearchParam//PrimaryClaimantLastNameComparison", dictSearchModel["PrimaryClaimantLastNameComparison"]);
                    strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//SearchParam//PrimaryClaimantLastName", dictSearchModel["PrimaryClaimantLastName"]);
                    strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//SearchParam//PrimaryClaimantFirstNameComparison", dictSearchModel["PrimaryClaimantFirstNameComparison"]);
                    strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//SearchParam//PrimaryClaimantFirstName", dictSearchModel["PrimaryClaimantFirstName"]);
                    strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//SearchParam//FromDateComparison", dictSearchModel["TransDateComparison"]);
                    strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//SearchParam//FromDate", dictSearchModel["TransFromDate"]);
                    strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//SearchParam//ToDate", dictSearchModel["TransToDate"]);
                    strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//SearchParam//PayeeStatusComparison", dictSearchModel["PayeeStatusComparison"]);
                    strSearchMessageTempalte = AppHelper.ChangeMessageValue(strSearchMessageTempalte, "//SearchParam//PayeeStatus", dictSearchModel["PayeeStatus"]);
                }
                sCWSresponse = AppHelper.CallCWSService(strSearchMessageTempalte.ToString());
                resultDoc.LoadXml(sCWSresponse);
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//SupervisorApproval//AllTransactions//PaymentTransaction//Data");
                if (xmlIn != null)
                {
                    dicResponseData.Add("PaymentData", xmlIn.InnerText);
                }
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//SupervisorApproval//AllTransactions//PaymentTransaction//UserPref");
                if (xmlIn != null)
                {
                    dicResponseData.Add("PaymentUserPref", xmlIn.InnerText);
                }
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//SupervisorApproval//AllTransactions//PaymentTransaction//AdditionalData");
                if (xmlIn != null)
                {
                    dicResponseData.Add("PaymentAdditionalData", xmlIn.InnerText);
                }
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//SupervisorApproval//AllTransactions//CollectionTransaction//Data");
                if (xmlIn != null)
                {
                    dicResponseData.Add("CollectionData", xmlIn.InnerText);
                }
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//SupervisorApproval//AllTransactions//CollectionTransaction//UserPref");
                if (xmlIn != null)
                {
                    dicResponseData.Add("CollectionUserPref", xmlIn.InnerText);
                }
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//SupervisorApproval//AllTransactions//CollectionTransaction//AdditionalData");
                if (xmlIn != null)
                {
                    dicResponseData.Add("CollectionAdditionalData", xmlIn.InnerText);
                }

                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//SupervisorApproval//DenyTransactions//Data");
                if (xmlIn != null)
                {
                    dicResponseData.Add("DenialData", xmlIn.InnerText);
                }
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//SupervisorApproval//DenyTransactions//UserPref");
                if (xmlIn != null)
                {
                    dicResponseData.Add("DenialUserPref", xmlIn.InnerText);
                }
                xmlIn = (XmlElement)resultDoc.SelectSingleNode("//SupervisorApproval//DenyTransactions//AdditionalData");
                if (xmlIn != null)
                {
                    dicResponseData.Add("DenialAdditionalData", xmlIn.InnerText);
                }
                //RMA-13778 achouhan3    Added to handle Error Messages Starts
                if (resultDoc.SelectSingleNode("//ErrorTransactions") != null)
                {
                    foreach (System.Xml.XmlNode node in resultDoc.SelectNodes("//ErrorTransactions"))
                    {
                        sError.Append(node.InnerText);
                    }
                }
                if (resultDoc.SelectSingleNode("//WarningTransactions") != null)
                {
                    foreach (System.Xml.XmlNode node in resultDoc.SelectNodes("//WarningTransactions"))
                    {
                        sError.Append(node.InnerText);
                    }
                }
                dicResponseData.Add("error", ErrorHelper.UpdateErrorMessage(sError.ToString()));
                //RMA-13778 achouhan3    Added to handle Error Messages Ends
                return JsonConvert.SerializeObject(dicResponseData);
            }
            catch (Exception ex)
            {
                if (!dicResponseData.ContainsKey("error"))
                    dicResponseData.Add("error", ex.Message);
                return JsonConvert.SerializeObject(dicResponseData);
            }
            finally
            {
                resultDoc = null;
                xmlIn = null;
                dicResponseData = null;
                dictPostData = null;
                messageElement = null;
            }
        }

        private void SetControlText()
        {
            spnHeaderPayment.InnerText = RMXResourceProvider.GetSpecificObject("lblHeaderSearchFunds", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            spnPayeePayorLName.InnerText = RMXResourceProvider.GetSpecificObject("lblSearchPayeePayorLastName", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            spnPayeeLastName.InnerText = RMXResourceProvider.GetSpecificObject("lblSearchPayeeLastName", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString()) ;
            spnPayeePayorFName.InnerText = RMXResourceProvider.GetSpecificObject("lblSearchPayeePayorFirstName", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            spnPayeeFName.InnerText = RMXResourceProvider.GetSpecificObject("lblSearchPayeeFirstName", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString()) ;
            spnPrimaryClmntLName.InnerText = RMXResourceProvider.GetSpecificObject("lblSearchClaimantLastName", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            spnPrimaryClmntFName.InnerText = RMXResourceProvider.GetSpecificObject("lblSearchClaimantFirstName", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString()) ;
            spnTransactionDate.InnerText = RMXResourceProvider.GetSpecificObject("lblTransactionDate", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString()) ;
            spnPayeeApprovalSts.InnerText = RMXResourceProvider.GetSpecificObject("lblPayeeApprovalStatus", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            btnSearch.Text = RMXResourceProvider.GetSpecificObject("btnSubmitQuery", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.BUTTON).ToString()); 
            reset.Text = RMXResourceProvider.GetSpecificObject("btnClear", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.BUTTON).ToString()); 
            LINKTABSPayments.InnerText = RMXResourceProvider.GetSpecificObject("lblHeaderPayment", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString()); 
            LINKTABSCollectionsfromLSS.InnerText = RMXResourceProvider.GetSpecificObject("lblHeaderLSSCollection", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString()); 
            spnShowAllItem.InnerText = RMXResourceProvider.GetSpecificObject("lblShowAllTrans", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString()); 
            lblPendingItems.InnerText = RMXResourceProvider.GetSpecificObject("lblShowMyPendingTrans", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString()); 
            spnMsgHeader.InnerText = RMXResourceProvider.GetSpecificObject("lblPaymentMsgHeader", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            spnFundsTransPayment.InnerText = RMXResourceProvider.GetSpecificObject("lblFundsTransPayment", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            spnHeaderCollection.InnerText = RMXResourceProvider.GetSpecificObject("lblPaymentMsgHeader", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString()); 
            spnCollectionText.InnerText = RMXResourceProvider.GetSpecificObject("lblFundsTransCollection", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString()); 
            lbl_overrideamount.Text = RMXResourceProvider.GetSpecificObject("lblOverrideAmount", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString()) + ":";
            lbl_applyoverride.Text = RMXResourceProvider.GetSpecificObject("lblApplyOverride", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString()) + ":";
            lbl_applyoffset.Text = RMXResourceProvider.GetSpecificObject("lblApplyOffSet", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString()) + ":";
            lbl_txtVoidReason.Text = RMXResourceProvider.GetSpecificObject("lblVoidCheckReason", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString()) + ":";
            btnDeny.Value = RMXResourceProvider.GetSpecificObject("lblDenyButton", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.BUTTON).ToString());
            btnApprove.Value = RMXResourceProvider.GetSpecificObject("lblApproveButton", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.BUTTON).ToString()); 
            btnVoid.Value = RMXResourceProvider.GetSpecificObject("lblVoidButton", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.BUTTON).ToString()); 
            btnPrint.Value = RMXResourceProvider.GetSpecificObject("lblPrintButton", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.BUTTON).ToString()); 
            btnColDeny.Value = RMXResourceProvider.GetSpecificObject("lblDenyButton", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.BUTTON).ToString());
            btnColApprove.Value = RMXResourceProvider.GetSpecificObject("lblApproveButton", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.BUTTON).ToString()); 
            btnColVoid.Value = RMXResourceProvider.GetSpecificObject("lblVoidButton", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.BUTTON).ToString()); 
            btnColPrint.Value = RMXResourceProvider.GetSpecificObject("lblPrintButton", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.BUTTON).ToString()); 
            if (!string.IsNullOrEmpty(AppHelper.GetValue("MyTrans")) && AppHelper.GetValue("MyTrans") == "true")
                spnDenialHeader.InnerText = RMXResourceProvider.GetSpecificObject("lblMyPendingFundsTrx", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString()); 
            else
                spnDenialHeader.InnerText = RMXResourceProvider.GetSpecificObject("lblFundsTransDenial", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString()); 
            btnDenialDeny.Value = RMXResourceProvider.GetSpecificObject("lblDenyButton", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.BUTTON).ToString()); 
            btnDenialPrint.Value = RMXResourceProvider.GetSpecificObject("lblPrintButton", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.BUTTON).ToString()); 
            spnReason.InnerText = RMXResourceProvider.GetSpecificObject("lblReason", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.BUTTON).ToString());
            spnSearchCriteria.InnerText = RMXResourceProvider.GetSpecificObject("lblSearchCriteria", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            spnDisplayStatus.InnerText = RMXResourceProvider.GetSpecificObject("lblDisplayHoldPayee", RMXResourceProvider.PageId("ApproveTrans.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString()); 
        }

    }
}