﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using System.Data;
using System.Xml.Linq;
using Riskmaster.Common;

namespace Riskmaster.UI.SupervisoryApproval
{
	public partial class trans : NonFDMBasePageCWS
	{
		public XmlDocument Model = null;
		private string ListFundsTransactionsTemplate = "<Message><Authorization>34880aa1-ee69-4766-8c95-2afd5fe434d2</Authorization><Call>" +
         "<Function>SupervisorApprovalAdaptor.ListFundsTransactions</Function></Call><Document><SupervisorApproval><ShowAllItems/> <AllTransactions>Y</AllTransactions><DenyTransactions>Y" +
         "</DenyTransactions><LangCode></LangCode></SupervisorApproval></Document></Message>";
		private string DenySuperChecksMessageTemplate = "<Message><Authorization>34880aa1-ee69-4766-8c95-2afd5fe434d2</Authorization><Call>" +
		 "<Function>SupervisorApprovalAdaptor.DenySuperChecks</Function></Call><Document><SupervisorApproval><TransIds/><TransId/><Reason/></SupervisorApproval></Document></Message>";
		private string ApproveSuperChecksMessageTemplate = "<Message><Authorization>34880aa1-ee69-4766-8c95-2afd5fe434d2</Authorization><Call>" +
         "<Function>SupervisorApprovalAdaptor.ApproveSuperChecks</Function></Call><Document><SupervisorApproval><TransIds><TransId/><ClaimId></ClaimId><OverRideAmount></OverRideAmount><ApplyOverRide></ApplyOverRide><ApplyOffSet></ApplyOffSet></TransIds><Reason/></SupervisorApproval></Document></Message>";   //mcapps2 MITS 30236
		//Start rsushilaggar MITS 20606,19970 05/21/2010
		private string VoidSuperChecksMessageTemplate = "<Message><Authorization>34880aa1-ee69-4766-8c95-2afd5fe434d2</Authorization><Call>" +
		 "<Function>SupervisorApprovalAdaptor.VoidSuperChecks</Function></Call><Document><SupervisorApproval><TransIds><TransId/></TransIds><Reason/><VoidReason/><VoidReason_HTMLComments/></SupervisorApproval></Document></Message>";


        private string m_sEnableOffSet = "true";  //mcapps2 MITS 30236
        private string m_sEntityApprovalFlag = "true";
		private string m_sLssInvoice = "true";
		private string m_sEnableVoidReason = "true";
		//End rsushilaggar 
		protected void Page_Load(object sender, EventArgs e)
		{

			btnDeny.Attributes.Add("onclick", "return SelectChecksOk('Deny');");
			btnApprove.Attributes.Add("onclick", "return SelectChecksOk('Approve');");
			btnVoid.Attributes.Add("onclick", "return SelectChecksOk('Void');");
			btnPrint.Attributes.Add("onclick", "return WindowOpen();");
			btnDenialDeny.Attributes.Add("onclick", "return SelectChecksOk('DenialDeny');");
			btnDenialPrint.Attributes.Add("onclick", "return WindowOpenDenial();");
			//start rsushilaggar 06/02/2010 MITS 20938/19970
			btnColDeny.Attributes.Add("onclick", "return SelectColChecksOk('Deny');");
			btnColApprove.Attributes.Add("onclick", "return SelectColChecksOk('Approve');");
			btnColVoid.Attributes.Add("onclick", "return SelectColChecksOk('Void');");
			btnColPrint.Attributes.Add("onclick", "return WindowOpenCol();");

			TransDateop.Attributes.Add("onchange", "return BetweenOption('date','TransDate');");
			//end rsushilaggar            

			Model = new XmlDocument();
			try
			{
                hdnLangCode.Text = AppHelper.GetLanguageCode();
				if (!Page.IsPostBack)
				{
					NonFDMCWSPageLoad("SupervisorApprovalAdaptor.ListFundsTransactions");
					Model = new XmlDocument();
					Model = Data;
					BindpageControls(Model.InnerXml,"");
				}
               

			} // try
			catch (Exception ex)
			{
				ErrorHelper.logErrors(ex);
				BusinessAdaptorErrors err = new BusinessAdaptorErrors();
				err.Add(ex, BusinessAdaptorErrorType.SystemError);
				ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
			} // catch

		}

		private void SetValuesFromService()
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Author: Rahul Aggarwal
		/// MITS 20938 06/02/2010
		/// This function is used to create the xml for collection transaction
		/// </summary>
		/// <param name="sMessageTemplate">Input XML</param>
		/// <param name="sAction">Action Applied</param>
		/// <returns>Input Xml</returns>
		private string CreateServiceCallforCollection(string sMessageTemplate, string sAction)
		{
			if (sAction == "Deny" || sAction == "Approve" || sAction == "Void")
			{
				string sTransId = AppHelper.GetFormValue("hdnTransIdToUpper");
				sMessageTemplate = AppHelper.ChangeMessageValue(sMessageTemplate, "//TransId", sTransId);
				return sMessageTemplate;
			} // if
			if (sAction == "DenialDeny")
			{
				string sTransId = AppHelper.GetFormValue("hdnTransIdToLower");
				sMessageTemplate = AppHelper.ChangeMessageValue(sMessageTemplate, "//TransId", sTransId);

				return sMessageTemplate;
			} // if

			return "";
		}

		/// <summary>
		/// This function is used to create the xml for payment transaction
		/// </summary>
		/// <param name="sMessageTemplate"></param>
		/// <param name="sAction"></param>
		/// <returns>Input Xml</returns>
		private string CreateServiceCall(string sMessageTemplate, string sAction)
		{
			if (sAction == "Deny" || sAction == "Approve" || sAction == "Void")
			{
				string sTransId = AppHelper.GetFormValue("hdnTransIdToUpper");
				sMessageTemplate = AppHelper.ChangeMessageValue(sMessageTemplate, "//TransId", sTransId);
				//rsushilaggar: Void check Reason mits 19970
				if (sAction == "Void")
				{
					voidreason.ReadOnly = false;
					sMessageTemplate = AppHelper.ChangeMessageValue(sMessageTemplate, "//VoidReason", HttpContext.Current.Request.Form["voidreason"]);
					sMessageTemplate = AppHelper.ChangeMessageValue(sMessageTemplate, "//VoidReason_HTMLComments", voidreason_HTML.Text);
					voidreason.ReadOnly = true;
				}
				//End: rsushilaggar

                sMessageTemplate = AppHelper.ChangeMessageValue(sMessageTemplate, "//ApplyOverRide", applyoverride.Checked.ToString());
                sMessageTemplate = AppHelper.ChangeMessageValue(sMessageTemplate, "//OverRideAmount", AppHelper.GetFormValue("tboverrideamount"));
                sMessageTemplate = AppHelper.ChangeMessageValue(sMessageTemplate, "//ApplyOffSet", applyoffset.Checked.ToString());  //mcapps2 MITS 30236
         
				return sMessageTemplate;
			} // if
			if (sAction == "DenialDeny")
			{
				string sTransId = AppHelper.GetFormValue("hdnTransIdToLower");
				sMessageTemplate = AppHelper.ChangeMessageValue(sMessageTemplate, "//TransId", sTransId);

				return sMessageTemplate;
			} // if

			return "";
		}

		/// <summary>
		/// Author: Rahul Aggarwal
		/// This function is used to bind the data with the grid.
		/// </summary>
		/// <param name="sCWSresponse"></param>
		/// <param name="sortExpression"></param>
		private void BindpageControls(string sCWSresponse,string sortExpression)
		{
			DataSet usersRecordsSet = null;
			//DataSet usersRecordsSetNotApproved = null;  //added by neha goel for retrieving nor approved records
			XmlDocument usersXDoc = new XmlDocument();
			XmlElement xmlIn = null;
			XmlNodeList xmlNodeList = null;            //added by neha goel for retrieving nor approved records

			usersXDoc.LoadXml(sCWSresponse);

			XmlDocument usersXDocAlltransaction = new XmlDocument();
			usersXDocAlltransaction.LoadXml("<Document></Document>");
			XmlNode root = usersXDocAlltransaction.DocumentElement;

            //pkandhari JIRA 8889 starts
            //Permission checks for void,deny,approve Starts
            XmlNode NodeApprove = usersXDoc.SelectSingleNode("//Document/SupervisorApproval/ScreenUISettings/ApprovePermission");
            if (NodeApprove != null && NodeApprove.InnerText == Boolean.FalseString)
            {
                tdapprove.Visible = false;
                lbl_overrideamount.Visible = false;
                tboverrideamount.Visible = false;
                lbl_applyoverride.Visible = false;
                applyoverride.Visible = false;
            }
            XmlNode NodeDeny = usersXDoc.SelectSingleNode("//Document/SupervisorApproval/ScreenUISettings/DenyPermission");
            if (NodeDeny != null && NodeDeny.InnerText == Boolean.FalseString)
                tddeny.Visible = false;
            XmlNode NodeVoid = usersXDoc.SelectSingleNode("//Document/SupervisorApproval/ScreenUISettings/VoidPermission");
            if (NodeVoid != null && NodeVoid.InnerText == Boolean.FalseString)
                tdvoid.Visible = false;
            //Permission checks for void,deny,approve Ends
            //pkandhari JIRA 8889 starts

			XmlNode Node = usersXDoc.SelectSingleNode("//Document/SupervisorApproval/ScreenUISettings/UseEntityPaymentApproval");
			if (Node != null && Node.InnerText == "false")
			{
				m_sEntityApprovalFlag = Node.InnerText;
			}
			Node = usersXDoc.SelectSingleNode("//Document/SupervisorApproval/ScreenUISettings/ShowLSSLink");
			if (Node != null && Node.InnerText == "false")
			{
				m_sLssInvoice = Node.InnerText;
			}

			//added by neha goel:for binding grid with non approved records:start
			xmlNodeList = (XmlNodeList)usersXDoc.SelectNodes("//Document/SupervisorApproval/AllTransactions/Transaction[@PaymentType = 'P']");
			if (xmlNodeList != null && xmlNodeList.Count > 0)
			{
				foreach (XmlNode xnode in xmlNodeList)
				{
					XmlNode node = usersXDocAlltransaction.ImportNode(xnode, true);
					root.AppendChild(node);
				}

				usersRecordsSet = ConvertXmlDocToDataSet(usersXDocAlltransaction);
	
				GridView gvListFundsTransactions = (GridView)this.Form.FindControl("gvListFundsTransactions");
				
				DataView view = new DataView(usersRecordsSet.Tables["Transaction"]);
				if (!string.IsNullOrEmpty(sortExpression))
				{
					view.Sort = sortExpression ;
				}
				else
				{
					view = usersRecordsSet.Tables["Transaction"].DefaultView;
				}
				gvListFundsTransactions.DataSource = view;
				gvListFundsTransactions.DataBind();

			}//end
			//start rsushilaggar MITS 20938 06/02/2010
			usersXDocAlltransaction.LoadXml("<Document></Document>");
			root = usersXDocAlltransaction.DocumentElement;
			xmlNodeList = (XmlNodeList)usersXDoc.SelectNodes("//Document/SupervisorApproval/AllTransactions/Transaction[@PaymentType = 'C']");
			if (xmlNodeList != null && xmlNodeList.Count > 0)
			{
				foreach (XmlNode xnode in xmlNodeList)
				{
					XmlNode node = usersXDocAlltransaction.ImportNode(xnode, true);
					root.AppendChild(node);
				}

				usersRecordsSet = ConvertXmlDocToDataSet(usersXDocAlltransaction);

				GridView gvListFundsCollections = (GridView)this.Form.FindControl("gvListFundsCollections");
				DataView view = new DataView(usersRecordsSet.Tables["Transaction"]);
				if (!string.IsNullOrEmpty(sortExpression))
				{
					view.Sort = sortExpression;
				}
				else
				{
					view = usersRecordsSet.Tables["Transaction"].DefaultView;
				}
				gvListFundsCollections.DataSource = view;
				gvListFundsCollections.DataBind();
			}
			//end rsushilaggar
			xmlIn = (XmlElement)usersXDoc.SelectSingleNode("//Document/SupervisorApproval/DenyTransactions");
			if (xmlIn != null)
			{
				usersXDocAlltransaction.LoadXml(xmlIn.OuterXml);
				usersRecordsSet = ConvertXmlDocToDataSet(usersXDocAlltransaction);

				DataView dataView = new DataView(usersRecordsSet.Tables["Transaction"]);
				if (!string.IsNullOrEmpty(sortExpression))
				{
					dataView.Sort = sortExpression;
				}
				else
				{
					dataView = usersRecordsSet.Tables["Transaction"].DefaultView;
				}
				GridView gvDeniedTransactions = (GridView)this.Form.FindControl("gvDeniedTransactions");
				gvDeniedTransactions.DataSource = dataView; 
				gvDeniedTransactions.DataBind();
			}

			//Rahul Aggarwal- Void Payment Reason Start- 05/20/2010 MITS 19970
			Node = usersXDoc.SelectSingleNode("//Document/SupervisorApproval/ScreenUISettings/EnableVoidReason");
			if (Node != null)
				m_sEnableVoidReason = Node.InnerText;

			if (m_sEnableVoidReason == "true")
			{
                if (NodeVoid != null && NodeVoid.InnerText == Boolean.FalseString)
                {
                    voidreason.Visible = false;
                    voidreasonbtnMemo.Visible = false;
                    lbl_txtVoidReason.Visible = false;
                    reason.Visible = false;
                    voidreason_HTML.Text = string.Empty;
                    hdnVoidreasonFlag.Value = "false";
                }
                else
                {
                    voidreason.Visible = true;
                    voidreasonbtnMemo.Visible = true;
                    lbl_txtVoidReason.Visible = true;
                    lblReason.Visible = false;//vkumar258 JIRA-11778
                    reason.Visible = false;
                    voidreason_HTML.Text = string.Empty;
                    hdnVoidreasonFlag.Value = "true";//averma62 - MITS 29130
                }
			}
			else
			{
				voidreason.Visible = false;
				voidreasonbtnMemo.Visible = false;
				lbl_txtVoidReason.Visible = false;
				lblReason.Visible = true;//vkumar258 JIRA-11778
				reason.Visible = true;
                hdnVoidreasonFlag.Value = "false";//averma62 - MITS 29130
			}
			//Rahul Aggarwal- Void Payment Reason-End
            //mcapps2 MITS 30236 Start
            Node = usersXDoc.SelectSingleNode("//Document/SupervisorApproval/ScreenUISettings/EnableOffset");
            if (Node != null)
            {
                m_sEnableOffSet = Node.InnerText.ToLower();
            }
            if (m_sEnableOffSet == "true")
            {
                if (NodeApprove != null && NodeApprove.InnerText == Boolean.FalseString)
                {
                    applyoffset.Visible = false;
                    lbl_applyoffset.Visible = false;
                }
                else
                {
                    applyoffset.Visible = true;
                    lbl_applyoffset.Visible = true;
                }
            }
            else
            {
                applyoffset.Visible = false;
                lbl_applyoffset.Visible = false;
            }
            //mcapps2 MITS 30236 end
		}

		/// <summary>
		/// Author: Rahul Aggarwal
		/// This function is used to deny the collection transaction  
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnDeny_Click(object sender, EventArgs e)
		{
			try
			{
				DenySuperChecksMessageTemplate = CreateServiceCall(DenySuperChecksMessageTemplate, "Deny");

				string sReturn = AppHelper.CallCWSService(DenySuperChecksMessageTemplate.ToString());
				Refresh_Page(sReturn);
			} // try
			catch (Exception ex)
			{
				ErrorHelper.logErrors(ex);
				BusinessAdaptorErrors err = new BusinessAdaptorErrors();
				err.Add(ex, BusinessAdaptorErrorType.SystemError);
				ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
			} // catch
		}

		/// <summary>
		/// This function is used to refresh the data
		/// </summary>
		private void Refresh_Page(string sPreviousData)
		{
			try
			{
				//Start: rsushilaggar Vendor's Supervisor Approval MITS 20606 05/05/2010
				NonFDMCWSPageLoad("SupervisorApprovalAdaptor.ListFundsTransactions");
				Model = new XmlDocument();
				Model = Data;
				BindpageControls(Model.InnerXml, "");
				//End: rsushilaggar Vendor's Supervisor Approval

				XmlDocument xmlReturnErrorTest = new XmlDocument();
				xmlReturnErrorTest.LoadXml(sPreviousData);
				if (xmlReturnErrorTest.SelectSingleNode("//MsgStatus/MsgStatusCd").InnerText == "Error")
				{
					ErrorControl1.errorDom = sPreviousData;
				}
			}
			catch (Exception ex)
			{
				ErrorHelper.logErrors(ex);
				BusinessAdaptorErrors err = new BusinessAdaptorErrors();
				err.Add(ex, BusinessAdaptorErrorType.SystemError);
				ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
			} // catch
		}// method: Refresh_Page

		/// <summary>
		/// Author: Rahul Aggarwal
		/// This function is used to approve the transaction 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnApprove_Click(object sender, EventArgs e)
		{
            string sReturn = string.Empty;
			try
			{
                
                    ApproveSuperChecksMessageTemplate = CreateServiceCall(ApproveSuperChecksMessageTemplate, "Approve");

                    sReturn = AppHelper.CallCWSService(ApproveSuperChecksMessageTemplate.ToString());
                
				Refresh_Page(sReturn);
			} // try
			catch (Exception ex)
			{
				ErrorHelper.logErrors(ex);
				BusinessAdaptorErrors err = new BusinessAdaptorErrors();
				err.Add(ex, BusinessAdaptorErrorType.SystemError);
				ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
			} // catch
		}

		/// <summary>
		/// Author: Rahul Aggarwal
		/// This function is used to void the transaction 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnVoid_Click(object sender, EventArgs e)
		{
			try
			{
				VoidSuperChecksMessageTemplate = CreateServiceCall(VoidSuperChecksMessageTemplate, "Void");
				string sReturn = AppHelper.CallCWSService(VoidSuperChecksMessageTemplate.ToString());
				Refresh_Page(sReturn);
			} // try
			catch (Exception ex)
			{
				ErrorHelper.logErrors(ex);
				BusinessAdaptorErrors err = new BusinessAdaptorErrors();
				err.Add(ex, BusinessAdaptorErrorType.SystemError);
				ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
			} // catch
		}

		/// <summary>
		/// Author: Rahul Aggarwal
		/// This function is used to deny the transaction 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnDenialDeny_Click(object sender, EventArgs e)
		{
			try
			{
				DenySuperChecksMessageTemplate = CreateServiceCall(DenySuperChecksMessageTemplate, "DenialDeny");

				string sReturn = AppHelper.CallCWSService(DenySuperChecksMessageTemplate.ToString());
				Refresh_Page(sReturn);
			} // try
			catch (Exception ex)
			{
				ErrorHelper.logErrors(ex);
				BusinessAdaptorErrors err = new BusinessAdaptorErrors();
				err.Add(ex, BusinessAdaptorErrorType.SystemError);
				ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
			} // catch
		}
		
		/// <summary>
		/// Author: Rahul Aggarwal
		/// This function is used to search the transaction based upon the search criteria
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnSearch_Click(object sender, EventArgs e)
		{
			try
			{
				NonFDMCWSPageLoad("SupervisorApprovalAdaptor.ListFundsTransactions");
				Model = new XmlDocument();
				Model = Data;
				BindpageControls(Model.InnerXml,"");
			} // try
			catch (Exception ex)
			{
				ErrorHelper.logErrors(ex);
				BusinessAdaptorErrors err = new BusinessAdaptorErrors();
				err.Add(ex, BusinessAdaptorErrorType.SystemError);
				ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
			} // catch
		}
		//end

		/// <summary>
		/// This property is used to get/set the GridViewSortDirection
		/// </summary>
		public SortDirection GridViewSortDirection
		{
			get
			{
				if (ViewState["sortDirection"] == null)
					//ViewState["sortDirection"] = SortDirection.Ascending;  Commented by csingh7 : MITS 17576
					ViewState["sortDirection"] = SortDirection.Descending;
				return (SortDirection)ViewState["sortDirection"];
			}
			set { ViewState["sortDirection"] = value; }
		}

		/// <summary>
		/// Author: Rahul Aggarwal
		/// This function checks the payee status and based upon that disabled the check box
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {            
            GridViewRow row = e.Row;
            string strReasonCode = string.Empty;
            if (row != null && row.Cells.Count > 0 && m_sEntityApprovalFlag == "true")
            {
                //Syadav55 Jira-7810 starts
                ////rsushilaggar MITS 21715 11-Aug-2010
                Control ctrlreasoncode = row.FindControl("hdnReasonCode");
                    if (ctrlreasoncode != null)
                    {
                        TextBox TbReasoncode1 = (TextBox)ctrlreasoncode;
                        strReasonCode = TbReasoncode1.Text.Trim();
                    }
                    if (row.Cells[12].Text != "Approved" && (strReasonCode == "PAYEE_NOT_APPRV" || strReasonCode == "PAYEE_REJECT"))
                

                //if (strReasonCode == "PAYEE_NOT_APPRV")
                    {
                        //Syadav55 Jira-7810 ends

                        Control ctrl = row.FindControl("valueupper");
                        if (ctrl != null)
                        {
                            CheckBox chk = (CheckBox)ctrl;
                            chk.Enabled = false;
                        }
                        ctrl = row.FindControl("valuelower");
                        if (ctrl != null)
                        {
                            CheckBox chk = (CheckBox)ctrl;
                            chk.Enabled = false;
                        }

                    }
                //Syadav55 Jira-7810 starts
                if (chkdisplaypayment.Checked != true)
                {
                    
                    if (strReasonCode == "PAYEE_NOT_APPRV" || strReasonCode == "PAYEE_REJECT")
                    {
                        row.Visible = false;
                    }
                }
                //Syadav55 Jira-7810 ends
            }
                      

			if (e.Row.RowType == DataControlRowType.DataRow)
			{

				DataRowView drvGridRow = (DataRowView)e.Row.DataItem;
				TableCell tcGridCell;
				LinkButton lbLinkColumn;

				tcGridCell = e.Row.Cells[13];        //csingh7 for MITS 20092
				lbLinkColumn = (LinkButton)tcGridCell.Controls[0];
				if (lbLinkColumn.Text == "Comments")
				{
					lbLinkColumn.Attributes.Add("href", "#");
					//lbLinkColumn.Attributes.Add("onclick", "return LookupComments('" + e.Row.Cells[1].Text + "');");//rsushilaggar MITS 22075 DATE 09/27/2010
                    //Deb MITS 25610
                    lbLinkColumn.Attributes.Add("onclick", "return LookupComments('" + ((DataRowView)(e.Row.DataItem)).Row.ItemArray[0].ToString() + "');");
                    //Deb MITS 25610
				}
			}
		}

		/// <summary>
		/// Author: Rahul Aggarwal
		/// This function checks the uitlity setting flag and based upon that Hide/Unhide the grid columns
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void GridView_DataBound(object sender, EventArgs e)
		{

			if (m_sEntityApprovalFlag == "false")
			{
				GridView gvListFundsTransactions = (GridView)this.Form.FindControl("gvListFundsTransactions");    
				if (gvListFundsTransactions != null)
				{
					gvListFundsTransactions.Columns[12].HeaderStyle.CssClass = "hiderowcol";
					gvListFundsTransactions.Columns[12].ItemStyle.CssClass = "hiderowcol";
				}
				
				GridView gvDeniedTransactions = (GridView)this.Form.FindControl("gvDeniedTransactions");
				if (gvDeniedTransactions != null)
				{
					gvDeniedTransactions.Columns[12].HeaderStyle.CssClass = "hiderowcol";
					gvDeniedTransactions.Columns[12].ItemStyle.CssClass = "hiderowcol";
				}
			}
			if (m_sLssInvoice == "false")
			{
				GridView gvListFundsTransactions = (GridView)this.Form.FindControl("gvListFundsTransactions");
				if (gvListFundsTransactions != null)
				{
					gvListFundsTransactions.Columns[11].HeaderStyle.CssClass = "hiderowcol";
					gvListFundsTransactions.Columns[11].ItemStyle.CssClass = "hiderowcol";
				}

				GridView gvDeniedTransactions = (GridView)this.Form.FindControl("gvDeniedTransactions");
				if (gvDeniedTransactions != null)
				{
					gvDeniedTransactions.Columns[11].HeaderStyle.CssClass = "hiderowcol";
					gvDeniedTransactions.Columns[11].ItemStyle.CssClass = "hiderowcol";
				}
			}

		}

		/// <summary>
		/// Author: Rahul Aggarwal
		/// This function checks the uitlity setting flag and based upon that Hide/Unhide the grid columns
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void GridViewCollection_DataBound(object sender, EventArgs e)
		{

			
			GridView gvListFundsCollection = (GridView)this.Form.FindControl("gvListFundsCollections");
            if (gvListFundsCollection != null)
            {
                gvListFundsCollection.Columns[12].HeaderStyle.CssClass = "hiderowcol";
                gvListFundsCollection.Columns[12].ItemStyle.CssClass = "hiderowcol";
            }
			
			if (m_sLssInvoice == "false")
			{
				//gvListFundsCollection = (GridView)this.Form.FindControl("gvListFundsCollections");
				if (gvListFundsCollection != null)
				{
					gvListFundsCollection.Columns[11].HeaderStyle.CssClass = "hiderowcol";
					gvListFundsCollection.Columns[11].ItemStyle.CssClass = "hiderowcol";
				}
			}

		}

		/// <summary>
		/// Author: Rahul Aggarwal
		/// This function is used to sort the grid columns.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void GridView_Sorting(Object sender, GridViewSortEventArgs e)
		{
			IsSearchEnabled.Text = "true";
		   

			try
			{
				NonFDMCWSPageLoad("SupervisorApprovalAdaptor.ListFundsTransactions");
				Model = new XmlDocument();
				Model = Data;
				if (!string.IsNullOrEmpty(GridSortExpression.Value) && GridSortExpression.Value == e.SortExpression)
				{
					if (GridSortDirection.Value == "ASC")
						GridSortDirection.Value = "DESC";
					else
						GridSortDirection.Value = "ASC";
				}
				else
				{
					GridSortDirection.Value = "ASC";
				}
				//DataTable GridData = (DataTable)gvListFundsTransactions.DataSource;
				//DataView view = new DataView(GridData);
				//view.Sort = e.SortExpression + " " + GridSortDirection.Value;
				//gvListFundsTransactions.DataSource = view;
				//gvListFundsTransactions.DataBind();
				BindpageControls(Model.InnerXml, e.SortExpression + " " +GridSortDirection.Value);
				GridSortExpression.Value = e.SortExpression;
			}
			catch (Exception ex)
			{
				ErrorHelper.logErrors(ex);
				BusinessAdaptorErrors err = new BusinessAdaptorErrors();
				err.Add(ex, BusinessAdaptorErrorType.SystemError);
				ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
			}
		}

		/// <summary>
		/// Author: Rahul Aggarwal
		/// This function is used to deny the collection transaction  
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnColDeny_Click(object sender, EventArgs e)
		{
			try
			{
				DenySuperChecksMessageTemplate = CreateServiceCallforCollection(DenySuperChecksMessageTemplate, "Deny");

				string sReturn = AppHelper.CallCWSService(DenySuperChecksMessageTemplate.ToString());
				Refresh_Page(sReturn);
			} // try
			catch (Exception ex)
			{
				ErrorHelper.logErrors(ex);
				BusinessAdaptorErrors err = new BusinessAdaptorErrors();
				err.Add(ex, BusinessAdaptorErrorType.SystemError);
				ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
			} // catch
		}

		/// <summary>
		/// Author: Rahul Aggarwal
		/// This function is used to approve the collection transaction 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnColApprove_Click(object sender, EventArgs e)
		{
			try
			{
				ApproveSuperChecksMessageTemplate = CreateServiceCallforCollection(ApproveSuperChecksMessageTemplate, "Approve");

				string sReturn = AppHelper.CallCWSService(ApproveSuperChecksMessageTemplate.ToString());
				Refresh_Page(sReturn);
			} // try
			catch (Exception ex)
			{
				ErrorHelper.logErrors(ex);
				BusinessAdaptorErrors err = new BusinessAdaptorErrors();
				err.Add(ex, BusinessAdaptorErrorType.SystemError);
				ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
			} // catch
		}

		/// <summary>
		/// Author: Rahul Aggarwal
		/// This function is used to void the collection transaction 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnColVoid_Click(object sender, EventArgs e)
		{
			try
			{
				VoidSuperChecksMessageTemplate = CreateServiceCallforCollection(VoidSuperChecksMessageTemplate, "Void");
				string sReturn = AppHelper.CallCWSService(VoidSuperChecksMessageTemplate.ToString());
				Refresh_Page(sReturn);
			} // try
			catch (Exception ex)
			{
				ErrorHelper.logErrors(ex);
				BusinessAdaptorErrors err = new BusinessAdaptorErrors();
				err.Add(ex, BusinessAdaptorErrorType.SystemError);
				ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
			} // catch
		}

		/// <summary>
		/// Author - Rahul Aggarwal
		/// This function is for creating a dataset from Xml document
		/// </summary>
		/// <param name="transDoc"></param>
		/// <returns></returns>
		public new DataSet ConvertXmlDocToDataSet(XmlDocument transDoc)
		{
			DataSet dSet = null;
			bool bSuccess = false;
			try
			{
				dSet = new DataSet();
				DataTable dt = new DataTable("Transaction");

				DataColumn dc = new DataColumn();
				dc.ColumnName = "TransId";
				dc.DataType = System.Type.GetType("System.Int64");
				dt.Columns.Add(dc);

				 dc = new DataColumn();
				dc.ColumnName = "CtlNumber";
				dc.DataType = System.Type.GetType("System.String");
				dt.Columns.Add(dc);


				dc = new DataColumn();
				dc.ColumnName = "TransDate";
				dc.DataType = System.Type.GetType("System.String");
				dt.Columns.Add(dc);

				dc = new DataColumn();
				dc.ColumnName = "PayeeName";
				dc.DataType = System.Type.GetType("System.String");
				dt.Columns.Add(dc);

				dc = new DataColumn();
				dc.ColumnName = "ClaimNumber";
				dc.DataType = System.Type.GetType("System.String");
				dt.Columns.Add(dc);

				dc = new DataColumn();
				dc.ColumnName = "PrimaryClaimantName";
				dc.DataType = System.Type.GetType("System.String");
				dt.Columns.Add(dc);
				
				dc = new DataColumn();
				dc.ColumnName = "PaymentAmount";
				dc.DataType = System.Type.GetType("System.String");
				dt.Columns.Add(dc);

				dc = new DataColumn();
				dc.ColumnName = "PaymentAmountNumber";
				dc.DataType = System.Type.GetType("System.Double");
				dt.Columns.Add(dc);

				dc = new DataColumn();
				dc.ColumnName = "User";
				dc.DataType = System.Type.GetType("System.String");
				dt.Columns.Add(dc); 

				dc = new DataColumn();
				dc.ColumnName = "TransactionType";
				dc.DataType = System.Type.GetType("System.String");
				dt.Columns.Add(dc);

				dc = new DataColumn();
				dc.ColumnName = "SplitAmount";
				dc.DataType = System.Type.GetType("System.String");
				dt.Columns.Add(dc);

				dc = new DataColumn();
				dc.ColumnName = "SplitAmountNumber";
				dc.DataType = System.Type.GetType("System.Double");
				dt.Columns.Add(dc);

				dc = new DataColumn();
				dc.ColumnName = "FromToDate";
				dc.DataType = System.Type.GetType("System.String");
				dt.Columns.Add(dc);

				dc = new DataColumn();
				dc.ColumnName = "LSSID";
				dc.DataType = System.Type.GetType("System.String");
				dt.Columns.Add(dc);
				
				dc = new DataColumn();      //csingh7 for MITS 20092
				dc.ColumnName = "PayeeStatus";
				dc.DataType = System.Type.GetType("System.String");
				dt.Columns.Add(dc);

				dc = new DataColumn();
				dc.ColumnName = "VoidReason";
				dc.DataType = System.Type.GetType("System.String");
				dt.Columns.Add(dc);

				dc = new DataColumn();
				dc.ColumnName = "UtilitySetting";
				dc.DataType = System.Type.GetType("System.String");
				dt.Columns.Add(dc);

				dc = new DataColumn();
				dc.ColumnName = "PaymentType";
				dc.DataType = System.Type.GetType("System.String");
				dt.Columns.Add(dc);

				dc = new DataColumn();
				dc.ColumnName = "EntityApprovalStatus";
				dc.DataType = System.Type.GetType("System.String");
				dt.Columns.Add(dc);

				dc = new DataColumn();      //csingh7 for MITS 20092
				dc.ColumnName = "Comments";
				dc.DataType = System.Type.GetType("System.String");
				dt.Columns.Add(dc);

                //sharishkumar Jira 6417 starts
                dc = new DataColumn();
                dc.ColumnName = "SubmittedTo";
                dc.DataType = System.Type.GetType("System.String");
                dt.Columns.Add(dc);
                //sharishkumar Jira 6417 ends

                //JIRA 7810 Start Snehal
                dc = new DataColumn();
                dc.ColumnName = "HOLD_REASON";
                dc.DataType = System.Type.GetType("System.String");
                dt.Columns.Add(dc);


                dc = new DataColumn();
                dc.ColumnName = "HOLD_REASON_CODE";
                dc.DataType = System.Type.GetType("System.String");
                dt.Columns.Add(dc);
                //JIRA 7810 End

				foreach (XmlNode node in transDoc.SelectNodes("//Transaction"))
				{
					DataRow dr = dt.NewRow();
					dr["TransId"] = Conversion.CastToType<Int64>(node.SelectSingleNode("TransId") != null ? node.SelectSingleNode("TransId").InnerText : string.Empty, out bSuccess);
					dr["CtlNumber"] = node.SelectSingleNode("CtlNumber")!= null? node.SelectSingleNode("CtlNumber").InnerText:string.Empty;
					dr["TransDate"] = node.SelectSingleNode("TransDate") != null ? node.SelectSingleNode("TransDate").InnerText : string.Empty;
					//dr["TransDateSort"] = Conversion.CastToType<int>(node.SelectSingleNode("TransDateSort").InnerText, out bSuccess);
					dr["PayeeName"] = node.SelectSingleNode("PayeeName") != null ? node.SelectSingleNode("PayeeName").InnerText : string.Empty;
					dr["ClaimNumber"] = node.SelectSingleNode("ClaimNumber") != null ? node.SelectSingleNode("ClaimNumber").InnerText : string.Empty;
					dr["PrimaryClaimantName"] = node.SelectSingleNode("PrimaryClaimantName") != null ? node.SelectSingleNode("PrimaryClaimantName").InnerText : string.Empty;
					dr["PaymentAmount"] = node.SelectSingleNode("PaymentAmount") != null ? node.SelectSingleNode("PaymentAmount").InnerText : string.Empty;
					dr["PaymentAmountNumber"] = Conversion.CastToType<double>(node.SelectSingleNode("PaymentAmountNumber")!= null?node.SelectSingleNode("PaymentAmountNumber").InnerText:string.Empty, out bSuccess);
					dr["User"] = node.SelectSingleNode("User") != null ? node.SelectSingleNode("User").InnerText : string.Empty;
					dr["TransactionType"] = node.SelectSingleNode("TransactionType") != null ? node.SelectSingleNode("TransactionType").InnerText : string.Empty;
					dr["SplitAmount"] = node.SelectSingleNode("SplitAmount") != null ? node.SelectSingleNode("SplitAmount").InnerText : string.Empty;
					dr["SplitAmountNumber"] = Conversion.CastToType<double>(node.SelectSingleNode("SplitAmountNumber") != null ? node.SelectSingleNode("SplitAmountNumber").InnerText : string.Empty, out bSuccess);
					dr["FromToDate"] = node.SelectSingleNode("FromToDate") != null ? node.SelectSingleNode("FromToDate").InnerText : string.Empty;
					dr["LSSID"] = node.SelectSingleNode("LSSID") != null ? node.SelectSingleNode("LSSID").InnerText : string.Empty;
					dr["PayeeStatus"] = node.SelectSingleNode("PayeeStatus") != null ? node.SelectSingleNode("PayeeStatus").InnerText : string.Empty;  //csingh7 for MITS 20092
					dr["VoidReason"] = node.SelectSingleNode("VoidReason") != null ? node.SelectSingleNode("VoidReason").InnerText : string.Empty;
					dr["UtilitySetting"] = node.Attributes["UtilitySetting"].Value;
					dr["PaymentType"] = node.Attributes["PaymentType"].Value;
					dr["EntityApprovalStatus"] = node.Attributes["EntityApprovalStatus"].Value;
					dr["Comments"] = node.SelectSingleNode("Comments")!= null?node.SelectSingleNode("Comments").InnerText:"";  //csingh7 for MITS 20092
                    dr["SubmittedTo"] = node.SelectSingleNode("SubmittedTo") != null ? node.SelectSingleNode("SubmittedTo").InnerText : string.Empty;//sharishkumar Jira 6417
                    dr["HOLD_REASON"] = node.SelectSingleNode("HOLD_REASON") != null ? node.SelectSingleNode("HOLD_REASON").InnerText : string.Empty;  //JIRA 7810  Snehal
                    dr["HOLD_REASON_CODE"] = node.SelectSingleNode("HOLD_REASON_CODE") != null ? node.SelectSingleNode("HOLD_REASON_CODE").InnerText : string.Empty;  //JIRA 7810  Snehal
					dt.Rows.Add(dr);
				}
				//dSet.ReadXml(new XmlNodeReader(diaryDoc));
				dSet.Tables.Add(dt);

				return dSet;
			}
			catch (Exception ex)
			{
				throw new ApplicationException(ex.Message);
			}
		}

		//protected void gvListFundsTransactions_RowDataBound(object sender, GridViewRowEventArgs e)
		//{
		//    if (e.Row.RowType == DataControlRowType.DataRow)
		//    {

		//        DataRowView drvGridRow = (DataRowView)e.Row.DataItem;
		//        TableCell tcGridCell;
		//        LinkButton lbLinkColumn;

		//        tcGridCell = e.Row.Cells[10];        //csingh7 for MITS 20092
		//        lbLinkColumn = (LinkButton)tcGridCell.Controls[0];
		//        if (lbLinkColumn.Text == "Comments")
		//        {
		//            lbLinkColumn.Attributes.Add("href", "#");
		//            lbLinkColumn.Attributes.Add("onclick", "return LookupComments(" + e.Row.Cells[1].Text + ");");
		//        }
		//    }
		//}

		//protected void gvDeniedTransactions_RowDataBound(object sender, GridViewRowEventArgs e)
		//{
		//    if (e.Row.RowType == DataControlRowType.DataRow)
		//    {

		//        DataRowView drvGridRow = (DataRowView)e.Row.DataItem;
		//        TableCell tcGridCell;
		//        LinkButton lbLinkColumn;

		//        tcGridCell = e.Row.Cells[10];        //csingh7 for MITS 20092
		//        lbLinkColumn = (LinkButton)tcGridCell.Controls[0];
		//        if (lbLinkColumn.Text == "Comments")
		//        {
		//            lbLinkColumn.Attributes.Add("href", "#");
		//            lbLinkColumn.Attributes.Add("onclick", "return LookupComments(" + e.Row.Cells[1].Text + ");");
		//        }
		//    }
		//}
        protected void chkShowAllItem_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                hdnLangCode.Text = AppHelper.GetLanguageCode();
                ListFundsTransactionsTemplate = AppHelper.ChangeMessageValue(ListFundsTransactionsTemplate, "//ShowAllItems", chkShowAllItem.Checked.ToString());
                ListFundsTransactionsTemplate = AppHelper.ChangeMessageValue(ListFundsTransactionsTemplate, "//LangCode", hdnLangCode.Text);
                string sReturn = AppHelper.CallCWSService(ListFundsTransactionsTemplate.ToString());
                Model.LoadXml(sReturn);
                //SetValuesFromService();
                BindpageControls(sReturn, "" );//Needs to set sort expression
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            } // catch
        }

        //Syadav55 Jira-7810 starts
        protected void chkdisplaypayment_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                    hdnLangCode.Text = AppHelper.GetLanguageCode();               
                    NonFDMCWSPageLoad("SupervisorApprovalAdaptor.ListFundsTransactions");
                    Model = new XmlDocument();
                    Model = Data;
                    BindpageControls(Model.InnerXml, "");               

            } // try
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            } // catch
        }
        //Syadav55 Jira-7810 ends
	}
}
