﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="trans.aspx.cs" Inherits="Riskmaster.UI.SupervisoryApproval.trans" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>

<%@ Import Namespace="System.Xml" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Reports</title>
	<link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
	<link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
	<link rel="stylesheet" href="../../Content/system.css" type="text/css" />
	<link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />

	<script type="text/javascript" language="JavaScript" src="../../Scripts/form.js"></script>
	<script src="../../Scripts/zapatec/utils/zapatec.js" type="text/javascript"></script>

	<script type="text/javascript" language="JavaScript" src="../../Scripts/WaitDialog.js"> { var i; }</script>

	<%--<script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js"></script>

	<script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>

	<script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar-setup.js"></script>--%>

    <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
	<uc4:CommonTasks ID="CommonTasks1" runat="server" />
	<script language='javascript' type="text/javascript">

	    //rsushilaggar MITS 20938 06/03/2010
	    function WindowOpen() {
	        //window.open('home?pg=riskmaster/SupervisoryApproval/PrintReport', 'Print', 'width=1000,height=600' + ',top=' + (screen.availHeight - 290) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
	        window.open('PrintReport.aspx?TransType=P', 'Print', 'width=1000,height=600' + ',top=' + (screen.availHeight - 600) / 2 + ',left=' + (screen.availWidth - 1000) / 2 + ',resizable=yes,scrollbars=yes');
	        return false;
	    }
	    //rsushilaggar MITS 20938 06/03/2010
	    function WindowOpenCol() {
	        //window.open('home?pg=riskmaster/SupervisoryApproval/PrintReport', 'Print', 'width=1000,height=600' + ',top=' + (screen.availHeight - 290) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
	        window.open('PrintReport.aspx?TransType=C', 'Print', 'width=1000,height=600' + ',top=' + (screen.availHeight - 600) / 2 + ',left=' + (screen.availWidth - 1000) / 2 + ',resizable=yes,scrollbars=yes');
	        return false;
	    }
	    function WindowOpenDenial() {
	        window.open('DenialPrintReport.aspx', 'Print', 'width=1000,height=600' + ',top=' + (screen.availHeight - 600) / 2 + ',left=' + (screen.availWidth - 1000) / 2 + ',resizable=yes,scrollbars=yes');
	        return false;
	    }

	    function SelectChecksOk(CalledFrom) {
	        var CalledFrom = CalledFrom;
	        var SelectedTransRow = 0;
	        var SelectTransactionUpper = "";
	        var SelectTransactionLower = "";
	        //var bCheckBoxes = self.document.all.tags("input");
	        //var GVList1Id; var GVList1Row = 2; var GVList2Id; var GVList2Row = 2; var k = 2;
	        //for (var i = 0; i < bCheckBoxes.length - 1; i++) {
	        //	GVList1Id = 'gvListFundsTransactions_ctl';
	        //	if (GVList1Row < 10) {
	        //		GVList1Id = GVList1Id +'0';
	        //	}
	        //	GVList1Id = GVList1Id + GVList1Row;
	        //	if (bCheckBoxes[i].id == GVList1Id + '_valueupper') {
	        //		if (bCheckBoxes[i].checked == true) {
	        //			var id = document.getElementById(GVList1Id + '_hdnId');
	        //			if (SelectTransactionUpper != "")
	        //				SelectTransactionUpper += "," + id.value;
	        //			else
	        //				SelectTransactionUpper += id.value;
	        //			//rsushilaggar MITS 21715 11-Aug-2010
	        //            SelectedTransRow = SelectedTransRow + 1;
	        //		}
	        //		GVList1Row = GVList1Row + 1;
	        //	}
	        //	GVList2Id = 'gvDeniedTransactions_ctl';
	        //	if (GVList2Row < 10) {
	        //		GVList2Id = GVList2Id +'0';
	        //	}
	        //	GVList2Id = GVList2Id + GVList2Row;
	        //	if (bCheckBoxes[i].id == GVList2Id + '_valuelower') {
	        //		if (bCheckBoxes[i].checked == true) {
	        //			var id = document.getElementById(GVList2Id + '_hdnId');
	        //			if (SelectTransactionLower != "")
	        //				SelectTransactionLower += "," + id.value;
	        //			else
	        //				SelectTransactionLower += id.value;
	        //		}
	        //		GVList2Row = GVList2Row + 1;
	        //	}
	        //}


	        //zmohammad MITs 34347 : Removing hardcoded parts from script
	        var grid = document.getElementById('gvListFundsTransactions');
	        if (grid != null) {
	            for (var i = 1; i < grid.rows.length; i++) {
	                var cntls = grid.rows[i].cells[0].getElementsByTagName('input');
	                if (cntls[0].type == "checkbox" && cntls[0].checked) {
	                    if (SelectTransactionUpper != "")
	                        SelectTransactionUpper += "," + cntls[1].value;
	                    else
	                        SelectTransactionUpper += cntls[1].value;
	                    SelectedTransRow = SelectedTransRow + 1;
	                }
	            }
	        }

	        var grid = document.getElementById('gvDeniedTransactions');
	        if (grid != null) {
	            for (var i = 1; i < grid.rows.length; i++) {

	                var cntls = grid.rows[i].cells[0].getElementsByTagName('input');
	                if (cntls[0].type == "checkbox" && cntls[0].checked) {
	                    if (SelectTransactionLower != "")
	                        SelectTransactionLower += "," + cntls[1].value;
	                    else
	                        SelectTransactionLower += cntls[1].value;
	                }
	            }
	        }

	        window.document.forms[0].hdnTransIdToUpper.value = SelectTransactionUpper;
	        window.document.forms[0].hdnTransIdToLower.value = SelectTransactionLower;
	        if ((SelectTransactionUpper == "" && (CalledFrom == "Deny" || CalledFrom == "Void" || CalledFrom == "Approve")) || (SelectTransactionLower == "" && CalledFrom == "DenialDeny")) {
	            alert("Please select a record");
	            return false;
	        }
	        if ((CalledFrom == "Approve") && document.getElementById("applyoverride").checked && (trim(document.getElementById("tboverrideamount").value) == "")) {
	            alert("Please enter Override Amount");
	            return false;
	        }
	        if (CalledFrom == "Void") {
	            if (!HasVoidReasonText())
	                return false;
	        }
	        //rsushilaggar MITS 21715 11-Aug-2010
	        //added by amitosh for mits 24357 (03/16/2011)
	        //averma62 - MITS 29130 - Added one morch check to validate utility check for void reason flag.
	        if (SelectedTransRow > 1 && CalledFrom == "Void" && document.getElementById("hdnVoidreasonFlag").value == "true") {
	            //MITS: 28028 - nsachdeva2 - 04/11/2012
	            var bResult = confirm("You are about to apply the same void reason to multiple checks");
	            if (!bResult)
	                return false;
	        }
	    }

	    //Start rsushilaggar MITS 20938 06/02/2010
	    function SelectColChecksOk(CalledFrom) {
	        var CalledFrom = CalledFrom;
	        var SelectedColRow = 0;
	        var SelectTransactionUpper = "";
	        var SelectTransactionLower = "";
	        //var bCheckBoxes = self.document.all.tags("input");
	        //var bCheckBoxes = document.forms[0].getElementsByTagName("input");
	        //var GVList3Id; var GVList3Row = 2;
	        //for (var i = 0; i < bCheckBoxes.length - 1; i++) {
	        //	GVList3Id = 'gvListFundsCollections_ctl';
	        //	if (GVList3Row < 10) {
	        //		GVList3Id = GVList3Id + '0';
	        //	}
	        //	GVList3Id = GVList3Id + GVList3Row;
	        //	if (bCheckBoxes[i].id == GVList3Id + '_valueupper') {

	        //		if (bCheckBoxes[i].checked == true) {
	        //			var id = document.getElementById(GVList3Id + '_hdnId');
	        //			if (SelectTransactionUpper != "")
	        //				SelectTransactionUpper += "," + id.value;
	        //			else
	        //				SelectTransactionUpper += id.value;
	        //			//rsushilaggar MITS 21715 11-Aug-2010
	        //            SelectedColRow = SelectedColRow + 1;

	        //		}
	        //		GVList3Row = GVList3Row + 1;
	        //	}
	        //}

	        //zmohammad MITs 34347 : Removing hardcoded parts from script
	        var grid = document.getElementById('gvListFundsCollections');
	        if (grid != null) {
	            for (var i = 1; i < grid.rows.length; i++) {
	                var cntls = grid.rows[i].cells[0].getElementsByTagName('input');
	                if (cntls[0].type == "checkbox" && cntls[0].checked) {
	                    if (SelectTransactionUpper != "")
	                        SelectTransactionUpper += "," + cntls[1].value;
	                    else
	                        SelectTransactionUpper += cntls[1].value;
	                    SelectedColRow = SelectedColRow + 1;
	                }
	            }
	        }
	        window.document.forms[0].hdnTransIdToUpper.value = SelectTransactionUpper;
	        window.document.forms[0].hdnTransIdToLower.value = SelectTransactionLower;
	        if ((SelectTransactionUpper == "" && (CalledFrom == "Deny" || CalledFrom == "Void" || CalledFrom == "Approve")) || (SelectTransactionLower == "" && CalledFrom == "DenialDeny")) {
	            alert("Please select a record");
	            return false;
	        }
	        //rsushilaggar MITS 21715 11-Aug-2010
	        //added by amitosh for mits 24357 (03/16/2011)
	        //if (SelectedColRow > 1 && CalledFrom == "Void") {
	        //	alert("You are about to apply the same void reason to multiple payment approval records.");
	        //}
	    }
	    function setDataChange(b) {
	        m_DataChanged = b;
	        return b;
	    }
	    //Start Debabrata Biswas MCIC r6 Retrofit
	    function tabChange(objname) {
	        //Start rsushilaggar MITS 22009 DATE 09/27/2010
	        var tabSelected = document.getElementById("DefaultTabSelected").value;
	        if (tabSelected != "" && objname == "") {
	            objname = tabSelected;
	        }
	        if (tabSelected == "" && objname == "") {
	            objname = 'Payments';
	        }
	        //End rsushilaggar 
	        document.forms[0].hTabName.value = objname;
	        var tmpName = "";
	        var m_TabList = ['Payments', 'CollectionsfromLSS'];

	        //Deselect All
	        for (i = 0; i < m_TabList.length; i++) {
	            tmpName = m_TabList[i];

	            if (eval('document.all.FORMTAB' + tmpName) != null)
	                eval('document.all.FORMTAB' + tmpName + '.style.display="none"');

	            if (eval('document.all.FORMTABBUTTONLIST' + tmpName) != null)
	                eval('document.all.FORMTABBUTTONLIST' + tmpName + '.style.display="none"');

	            if (eval('document.all.FORMTABDENIAL' + tmpName) != null)
	                eval('document.all.FORMTABDENIAL' + tmpName + '.style.display="none"');

	            if (eval('document.all.FORMTABBUTTONLISTDENIAL' + tmpName) != null)
	                eval('document.all.FORMTABBUTTONLISTDENIAL' + tmpName + '.style.display="none"');
	            //rsushilaggar MITS 21399 04-Aug-2010
	            if (eval('document.all.TABS' + tmpName) != null)
	                eval('document.all.TABS' + tmpName + '.className="NotSelected"');
	            if (eval('document.all.LINKTABS' + tmpName) != null)
	                eval('document.all.LINKTABS' + tmpName + '.className="NotSelected1"');
	        }
	        //Select the tab requested
	        if (eval('document.all.FORMTAB' + objname) != null)
	            eval('document.all.FORMTAB' + objname + '.style.display=""');

	        if (eval('document.all.FORMTABBUTTONLIST' + objname) != null)
	            eval('document.all.FORMTABBUTTONLIST' + objname + '.style.display=""');

	        if (eval('document.all.FORMTABDENIAL' + objname) != null)
	            eval('document.all.FORMTABDENIAL' + objname + '.style.display=""');

	        if (eval('document.all.FORMTABBUTTONLISTDENIAL' + objname) != null)
	            eval('document.all.FORMTABBUTTONLISTDENIAL' + objname + '.style.display=""');

	        //rsushilaggar MITS 21399 04-Aug-2010
	        if (eval('document.all.TABS' + objname) != null)
	            eval('document.all.TABS' + objname + '.className="Selected"');
	        if (eval('document.all.LINKTABS' + objname) != null)
	            eval('document.all.LINKTABS' + objname + '.className="Selected"');

	        document.getElementById("DefaultTabSelected").value = objname;

	        return true;
	    }


	    function TabSelected(ctrl) {
	        //Start rsushilaggar MITS 22009 DATE 09/27/2010
	        var tabSelected = document.getElementById("DefaultTabSelected").value;
	        if (tabSelected != "" && ctrl == "") {
	            ctrl = document.getElementById("DefaultTabSelected");
	        }
	        if (tabSelected == "" && ctrl == null) {
	            ctrl = document.getElementById("Payments");
	        }
	        //End rsushilaggar
	        try {
	            if (ctrl.className = "Selected") {
	                document.forms[0].SysTabSelected.value = ctrl.name + "**" + ctrl.className;
	            }
	        } catch (ex) { }
	    }

	    //End Debabrata Biswas MCIC r6 Retrofit

	    //Start: rsushilaggar Void PaymentReason MITS 19970 05/21/2010
	    function SubmitQuery(btnSelected) {
	        document.getElementById("IsSearchEnabled").value = "true";


	    }

	    function HasVoidReasonText() {
	        document.getElementById("IsSearchEnabled").value = "true";
	        var v_voidReason = document.getElementById("voidreason");
	        if (v_voidReason != null) {
	            sVoidReason = document.getElementById("voidreason").value;
	            if (sVoidReason == "") {
	                alert("Enter Void Check Reason");
	                return false;
	            }
	            return true;
	        }
	        return true;
	    }
	    //End: rsushilaggar

	    function LookupComments(sId) {      //csingh7 for MITS 20092
	        var sLink = '../Comments/MainPage.aspx?SysFormName=SupervisorApproval&recordid=' + sId + '&CommentsFlag=true';

	        window.open(sLink, 'qdWnd',
            'width=720,height=450' + ',top=' + (screen.availHeight - 400) / 2 + ',left=' + (screen.availWidth - 720) / 2 + ',resizable=yes,scrollbars=yes');
	        return false;
	    }

	    //PJS:MITS # 21392 - Added Reset function to clear the screen
	    function Reset() {
	        document.forms[0].txtPayeeLastName.value = "";
	        document.forms[0].lstPayeeLastName.value = "=";
	        document.forms[0].txtPayeeFirstName.value = "";
	        document.forms[0].lstPayeeFirstName.value = "=";
	        document.forms[0].txtPrimaryClaimantLastName.value = "";
	        document.forms[0].lstPrimaryClaimantLastName.value = "=";
	        document.forms[0].txtPrimaryClaimantFirstName.value = "";
	        document.forms[0].lstPrimaryClaimantFirstName.value = "=";
	        document.forms[0].TransDatestart.value = "";
	        document.forms[0].TransDateend.value = "";
	        document.forms[0].TransDateop.value = "between";
	        var PayeeStatus_codelookup = document.getElementById('PayeeStatus_codelookup');
	        if (PayeeStatus_codelookup != null)
	            PayeeStatus_codelookup.value = "";
	    }

	</script>

</head>
<body onload="parent.MDIScreenLoaded();tabChange('');TabSelected(null);return false;">
<%-- Start rsushilaggar MITS 22009 DATE 09/27/2010 --%>
	<form id="frmData" name="frmData"  runat="server" >
	<rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />
	<%try
	  {
	%><% if (Model.SelectSingleNode("//WarningTransactions") != null)
		 {%>
	<td align="left" colspan="4">
		<table>
			<tr style="font-size: small">
				<td colspan="2" class="warntextheader">
					Following Warnings were reported:
				</td>
			</tr>
			<tr>
				<td colspan="2" class="warntext">
					<%=Model.SelectSingleNode("//WarningTransactions").InnerText%>
				</td>
			</tr>
		</table>
	</td>
	<%}%>
	<% if (Model.SelectSingleNode("//ErrorTransactions") != null)
	   {%>
	<%foreach (XmlNode node in Model.SelectNodes("//ErrorTransactions"))
	  {
		  int iCount = 0; %>
	<tr>
		<td align="left" colspan="4">
			<font size="4" color="red">
				<%=node.InnerText %>
			</font>
		</td>
	</tr>
	<%iCount++;%>
	<%}%>
	<%         } %>
	<%}
	  catch (Exception ee)
	  {
		  ErrorHelper.logErrors(ee);
		  lblErrors.Text = ErrorHelper.FormatErrorsForUI("Error getting data for Supervisor Approval Of Funds Transactions");
	  }%>
	 <%if ((Model.SelectSingleNode("//ApproveTransactionSearch") != null) && Model.SelectSingleNode("//ApproveTransactionSearch").InnerText == "true")
	   { %>
		<table width="100%" cellspacing="0" cellpadding="2" border="0">
		<tr>
			<td colspan="10" class="msgheader" bgcolor="#D5CDA4">
				Search Funds Transaction Details
			</td>
		</tr>
		<tr>
			<td align="left" width="20%">
			<% if (Model.SelectSingleNode("//PutLSSCollectionOnHold") != null && Model.SelectSingleNode("//PutLSSCollectionOnHold").InnerText == "true")
			   { %>
				Payee/Payor Last Name:
				<% }
			   else
			   {%>
			   Payee Last Name:
				<%}%>
			</td>
			<td align="left" width="10%">
				<asp:DropDownList runat="server" type="combobox" rmxref="/Instance/Document/SearchParam/PayeeLastNameComparison"
					ID="lstPayeeLastName" ItemSetRef="/Instance/Document/SearchResults/Select">
					<asp:ListItem Value="=">=</asp:ListItem>
					<asp:ListItem Value="&lt;&gt;">&lt;&gt;</asp:ListItem>
					<asp:ListItem Value="&gt;">&gt;</asp:ListItem>
					<asp:ListItem Value="&gt;=">&gt;=</asp:ListItem>
					<asp:ListItem Value="&lt;">&lt;</asp:ListItem>
					<asp:ListItem Value="&lt;=">&lt;=</asp:ListItem>
				</asp:DropDownList>
			</td>
			<td>
				<asp:TextBox runat="server" type="text" rmxref="/Instance/Document/SearchParam/PayeeLastName"
					size="30" ID="txtPayeeLastName" />
			</td>
		</tr>
		<tr>
			<td align="left" width="20%">
			   <% if (Model.SelectSingleNode("//PutLSSCollectionOnHold") != null && Model.SelectSingleNode("//PutLSSCollectionOnHold").InnerText == "true")
				  { %>
					Payee/Payor First Name:
				<% }
				  else
				  {%>
						Payee First Name:
				<%} %>
			</td>
			<td align="left" width="5%">
				<asp:DropDownList runat="server" type="combobox" rmxref="/Instance/Document/SearchParam/PayeeFirstNameComparison"
					ID="lstPayeeFirstName" ItemSetRef="/Instance/Document/SearchResults/Select">
					<asp:ListItem Value="=">=</asp:ListItem>
					<asp:ListItem Value="&lt;&gt;">&lt;&gt;</asp:ListItem>
					<asp:ListItem Value="&gt;">&gt;</asp:ListItem>
					<asp:ListItem Value="&gt;=">&gt;=</asp:ListItem>
					<asp:ListItem Value="&lt;">&lt;</asp:ListItem>
					<asp:ListItem Value="&lt;=">&lt;=</asp:ListItem>
				</asp:DropDownList>
			</td>
			<td>
				<asp:TextBox runat="server" type="text" rmxref="/Instance/Document/SearchParam/PayeeFirstName"
					size="30" ID="txtPayeeFirstName" />
			</td>
		</tr>
		<tr>
			<td align="left" width="20%">
				Primary Claimant Last Name:
			</td>
			<td align="left" width="5%">
				<asp:DropDownList runat="server" type="combobox" rmxref="/Instance/Document/SearchParam/PrimaryClaimantLastNameComparison"
					ID="lstPrimaryClaimantLastName" ItemSetRef="/Instance/Document/SearchResults/Select">
					<asp:ListItem Value="=">=</asp:ListItem>
					<asp:ListItem Value="&lt;&gt;">&lt;&gt;</asp:ListItem>
					<asp:ListItem Value="&gt;">&gt;</asp:ListItem>
					<asp:ListItem Value="&gt;=">&gt;=</asp:ListItem>
					<asp:ListItem Value="&lt;">&lt;</asp:ListItem>
					<asp:ListItem Value="&lt;=">&lt;=</asp:ListItem>
				</asp:DropDownList>
			</td>
			<td>
				<asp:TextBox runat="server" type="text" rmxref="/Instance/Document/SearchParam/PrimaryClaimantLastName"
					size="30" ID="txtPrimaryClaimantLastName" />
			</td>
		</tr>
		<tr>
			<td align="left" width="20%">
				Primary Claimant First Name:
			</td>
			<td align="left" width="5%">
				<asp:DropDownList runat="server" type="combobox" rmxref="/Instance/Document/SearchParam/PrimaryClaimantFirstNameComparison"
					ID="lstPrimaryClaimantFirstName" ItemSetRef="/Instance/Document/SearchResults/Select">
					<asp:ListItem Value="=">=</asp:ListItem>
					<asp:ListItem Value="&lt;&gt;">&lt;&gt;</asp:ListItem>
					<asp:ListItem Value="&gt;">&gt;</asp:ListItem>
					<asp:ListItem Value="&gt;=">&gt;=</asp:ListItem>
					<asp:ListItem Value="&lt;">&lt;</asp:ListItem>
					<asp:ListItem Value="&lt;=">&lt;=</asp:ListItem>
				</asp:DropDownList>
			</td>
			<td>
				<asp:TextBox runat="server" type="text" rmxref="/Instance/Document/SearchParam/PrimaryClaimantFirstName"
					size="30" ID="txtPrimaryClaimantFirstName" />
			</td>
		</tr>
		<tr>
			<td align="left">
				Transaction Date:
			</td>
			<td align="left" width="5%">
				<asp:DropDownList runat="server" type="combobox" rmxref="/Instance/Document/SearchParam/FromDateComparison"
					ID="TransDateop" ItemSetRef="/Instance/Document/SearchResults/Select" >
					<asp:ListItem Value="between">Between</asp:ListItem>
					<asp:ListItem Value="=">=</asp:ListItem>
					<asp:ListItem Value="&lt;&gt;">&lt;&gt;</asp:ListItem>
					<asp:ListItem Value="&gt;">&gt;</asp:ListItem>
					<asp:ListItem Value="&gt;=">&gt;=</asp:ListItem>
					<asp:ListItem Value="&lt;">&lt;</asp:ListItem>
					<asp:ListItem Value="&lt;=">&lt;=</asp:ListItem>
				</asp:DropDownList>
			</td>
			<td align="left">
				<asp:TextBox runat="server" type="text" rmxref="/Instance/Document/SearchParam/FromDate"
					value="" onblur="dateLostFocus(this.id);" ID="TransDatestart" size="12" tabIndex="1" />
				<%--<input type="button" class="DateLookupControl" value="..." name="btnoneTransDate" id="btnoneTransDate" />

				<script type="text/javascript">
				    Zapatec.Calendar.setup(
									{
									    inputField: "TransDatestart",
									    ifFormat: "%m/%d/%Y",
									    button: "btnoneTransDate"
									}
									);
				</script>--%>
                <script type="text/javascript">
                    $(function () {
                        $("#TransDatestart").datepicker({
                            showOn: "button",
                            buttonImage: "../../Images/calendar.gif",
                           // buttonImageOnly: true,
                            showOtherMonths: true,
                            selectOtherMonths: true,
                            changeYear: true
                        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "2");//vkumar258 ML Changes
                    });
                    </script>

				<asp:TextBox runat="server" type="text" rmxref="/Instance/Document/SearchParam/ToDate"
					value="" ID="TransDateend" size="12" tabIndex="3" />
				<%--<input type="button" class="DateLookupControl" value="..." name="btntwoTransDate" id="btntwoTransDate" />

				<script type="text/javascript">
				    Zapatec.Calendar.setup(
									{
									    inputField: "TransDateend",
									    ifFormat: "%m/%d/%Y",
									    button: "btntwoTransDate"
									}
									);
				</script>--%>
                <script type="text/javascript">
                    $(function () {
                        $("#TransDateend").datepicker({
                            showOn: "button",
                            buttonImage: "../../Images/calendar.gif",
                            //buttonImageOnly: true,
                            showOtherMonths: true,
                            selectOtherMonths: true,
                            changeYear: true
                        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "4");//vkumar258 ML Changes
                    });
                    </script>

			</td>
		</tr>
		<%if ((Model.SelectSingleNode("//UseEntityPaymentApproval") != null) && Model.SelectSingleNode("//UseEntityPaymentApproval").InnerText == "true")
		  { %>
		<tr>
			<td align="left" width="20%">
				Payee Approval Status:
			</td>
			<td align="left" width="5%">
				<asp:DropDownList runat="server" type="combobox" rmxref="/Instance/Document/SearchParam/PayeeStatusComparison"
					ID="lstPayeeStatus" ItemSetRef="/Instance/Document/SearchResults/Select">
					<asp:ListItem Value="=">=</asp:ListItem>
					<asp:ListItem Value="&lt;&gt;">&lt;&gt;</asp:ListItem>
				</asp:DropDownList>
			</td>
			<td>
				<uc:CodeLookUp runat="server" OnChange="setDataChanged(true);" 
				ID="PayeeStatus" CodeTable="ENTITY_APPRV_REJ" ControlName="PayeeStatus" 
				RMXRef="/Instance/Document/SearchParam/PayeeStatus" RMXType="code" tabindex="20" />    
			</td>
		</tr>
		<%} %>
		<tr>
			<td align="left" colspan="2">
				<asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" OnClientClick="SubmitQuery('search');" Text="Submit Query"
					class="button" />
				<asp:Button runat="server" type="button" ID="reset" Text="Clear" OnClientClick="Reset();return false;" class="button" />
			</td>
		</tr>
	</table>
	<%} %>
	<div class="tabGroup" id="TabsDivGroup" runat="server">
		<asp:HiddenField ID="hTabName" runat="server" Value='' />
		<asp:HiddenField ID="SysTabSelected" runat="server" Value='' />
		<asp:HiddenField ID="DefaultTabSelected" runat="server" Value='' />
		<div class="Selected" nowrap="true" runat="server" name="TABSPayments"
			id="TABSPayments">
			<a class="Selected" href="#" runat="server" onclick="tabChange(this.name);TabSelected(this);return false;"
				rmxref="" name="Payments" id="LINKTABSPayments">Payments</a>
		</div>
		<div class="tabSpace" runat="server" id="TABSPayments_">
			<nbsp />
			<nbsp />
		</div>
		<!-- start rsushilaggar MITS 20938 06/02/2010 -->
		<% if (Model.SelectSingleNode("//PutLSSCollectionOnHold")!= null && Model.SelectSingleNode("//PutLSSCollectionOnHold").InnerText == "true")
		   { %>
		<div class="NotSelected" nowrap="true" runat="server" name="TABSCollectionsfromLSS"
			id="TABSCollectionsfromLSS">
			<a class="NotSelected1" href="#" runat="server" onclick="tabChange(this.name);TabSelected(this);return false;"
				rmxref="" name="CollectionsfromLSS" id="LINKTABSCollectionsfromLSS">Collections from LSS</a>
		</div>
		<div class="tabSpace" runat="server" id="TABSCollectionsfromLSS_">
			<nbsp />
			<nbsp />
		</div>
		<!-- end rsushilaggar MITS 20938 06/02/2010 -->
		<%} %>
	</div>
	<div id="FORMTABPayments">
		<table width="100%" id="FORMTABPayments_" cellspacing="0" cellpadding="2"
			border="0">
			<tr>
				<td class="msgheader" bgcolor="#D5CDA4">
					Supervisor Approval Of Funds Transactions
				</td>
			</tr>
			<% if (Model.SelectSingleNode("//Transaction[@PaymentType = 'P']") != null)
			   {
				   //if(Model.SelectSingleNode("//Transaction").Attributes["UtilitySetting"].Value == "H")
				   //{
				   %>
			   
			<tr class="colheader3">
				<!--ctrlgroup-->
				<td>
					Funds Transactions Available For Approval, Void, Or Denial
				</td>
			</tr>
			<tr>
				<asp:GridView ID="gvListFundsTransactions" runat="server" AutoGenerateColumns="false"
					AllowPaging="false" OnRowDataBound="GridView_RowDataBound" OnSorting="GridView_Sorting"  Width="100%" ShowHeader="true" GridLines="Both"
					AllowSorting="true" OnDataBound="GridView_DataBound" >
					<RowStyle CssClass="datatd1" />
					<AlternatingRowStyle CssClass="datatd" />
					<HeaderStyle CssClass="msgheader" ForeColor="White"/>
					<Columns>
						<asp:TemplateField HeaderStyle-HorizontalAlign="Left" 
							ItemStyle-Width="3%" >
							<ItemTemplate >
								<asp:CheckBox runat="server"  id='valueupper' value='<%# DataBinder.Eval(Container.DataItem,"TransId")%>' />
								<asp:HiddenField ID="hdnId" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"TransId") %>' />
								<asp:HiddenField ID="hdnIsAutoCheck" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"UtilitySetting") %>' />
							</ItemTemplate>
						</asp:TemplateField>
						<asp:BoundField HeaderText="Control #" DataField="CtlNumber" ItemStyle-HorizontalAlign="Left"
							HeaderStyle-HorizontalAlign="Left" 
							 SortExpression="CtlNumber"  ></asp:BoundField>
						<asp:BoundField HeaderText="Trans Date" DataField="TransDate" ItemStyle-HorizontalAlign="Left"
							HeaderStyle-HorizontalAlign="Left" 
							 SortExpression="TransDate"></asp:BoundField>
						<asp:BoundField HeaderText="Payee" DataField="PayeeName" ItemStyle-HorizontalAlign="Left"
							HeaderStyle-HorizontalAlign="Left" 
							 SortExpression="PayeeName"></asp:BoundField>
						<asp:BoundField HeaderText="Claim #" DataField="ClaimNumber" ItemStyle-HorizontalAlign="Left"
							HeaderStyle-HorizontalAlign="Left" 
							 SortExpression="ClaimNumber"></asp:BoundField>
						<asp:BoundField HeaderText="Primary Claimant" DataField="PrimaryClaimantName" ItemStyle-HorizontalAlign="Left"
							HeaderStyle-HorizontalAlign="Left" 
							 SortExpression="PrimaryClaimantName"></asp:BoundField>
						<asp:BoundField HeaderText="Amount" DataField="PaymentAmount" ItemStyle-HorizontalAlign="Left"
							HeaderStyle-HorizontalAlign="Left" 
							 SortExpression="PaymentAmountNumber"></asp:BoundField>
						<asp:BoundField HeaderText="User" DataField="User" ItemStyle-HorizontalAlign="Left"
							HeaderStyle-HorizontalAlign="Left" 
							 SortExpression="User"></asp:BoundField>
						<asp:BoundField HeaderText="Trans Type" DataField="TransactionType" ItemStyle-HorizontalAlign="Left"
							HeaderStyle-HorizontalAlign="Left" 
							 SortExpression="TransactionType"></asp:BoundField>
						<asp:BoundField HeaderText="Split Amount" DataField="SplitAmount" ItemStyle-HorizontalAlign="Left"
							HeaderStyle-HorizontalAlign="Left" 
							 SortExpression="SplitAmountNumber"></asp:BoundField>
						<asp:BoundField HeaderText="From/To Dates" DataField="FromToDate" ItemStyle-HorizontalAlign="Left"
							HeaderStyle-HorizontalAlign="Left" 
							 SortExpression="FromToDate"></asp:BoundField>
						<asp:TemplateField HeaderText="LSS Invoice" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" SortExpression="LSSID" >
						   <ItemTemplate>
							   <asp:HyperLink 
							   runat="server" ID="hplLssId" 
							   Text='<%# DataBinder.Eval(Container.DataItem,"LSSID")%>'
							   NavigateUrl="#" 
							   onclick='<%# "navigateToLSS(" + (char)(39) + "paramLSSInvoiceID=" + DataBinder.Eval(Container.DataItem,"LSSID") + (char)(39) + "); return false;"%>'>
							   </asp:HyperLink>
						   </ItemTemplate>
						</asp:TemplateField>
						<asp:BoundField HeaderText="Payee Status" DataField="PayeeStatus" ItemStyle-HorizontalAlign="Left"
							HeaderStyle-HorizontalAlign="Left" 
							 SortExpression="PayeeStatus"></asp:BoundField>
						<asp:ButtonField  Text="Comments" HeaderText="Comments" ButtonType="Link" ItemStyle-HorizontalAlign="Left"
							HeaderStyle-HorizontalAlign="Left" SortExpression="Comments"></asp:ButtonField>
                          <%--sharishkumar Jra 6417 starts--%>
                        <asp:BoundField HeaderText="Submitted To" DataField="SubmittedTo" ItemStyle-HorizontalAlign="Left"
							HeaderStyle-HorizontalAlign="Left" 
							 SortExpression="SubmittedTo"></asp:BoundField>
                        <%--JIRA 7810 start Snehal--%>
                        <asp:BoundField HeaderText="Hold Reason" DataField="HOLD_REASON" ItemStyle-HorizontalAlign="Left"
							HeaderStyle-HorizontalAlign="Left" 
							 SortExpression="HOLD_REASON"></asp:BoundField>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="3%"  Visible="False">
							<ItemTemplate >								
								<asp:TextBox ID="hdnReasonCode" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"HOLD_REASON_CODE") %>' style="display:none" />								
							</ItemTemplate>
						</asp:TemplateField>                           
                        <%--JIRA 7810  End--%>
                         <%--sharishkumar Jra 6417 ends--%>
					 <%--rsushilaggar MITS 21452 26-jul-2010--%>
					</Columns>
				</asp:GridView>
			</tr>
			<%} %>
		</table>
	</div>
	<!-- start rsushilaggar MITS 20938 06/02/2010 -->
	 <div id="FORMTABCollectionsfromLSS" style="display:none">
		<table width="100%" id="FORMTABCollectionsfromLSS_" cellspacing="0" cellpadding="2"
			border="0">
			<tr>
				<td class="msgheader" bgcolor="#D5CDA4">
					<%--Supervisor Approval Of Funds Transactions--%>
					Supervisor Approval Of Funds Collections
				</td>
			</tr>
			<% if (Model.SelectSingleNode("//Transaction[@PaymentType = 'C']") != null)
			   {
				   //if(Model.SelectSingleNode("//Transaction").Attributes["UtilitySetting"].Value == "H")
				   //{
				   %>
			   
			<tr class="colheader3">
				<!--ctrlgroup-->
				<td>
					<%--Funds Transactions Available For Approval, Void, Or Denial--%>
					Funds Collections Available For Approval, Void, Or Denial
				</td>
			</tr>
			<tr>
				<asp:GridView ID="gvListFundsCollections" runat="server" AutoGenerateColumns="false"
					AllowPaging="false" OnSorting="GridView_Sorting"  Width="100%" ShowHeader="true" GridLines="Both"
					AllowSorting="true" OnDataBound="GridViewCollection_DataBound" OnRowDataBound="GridView_RowDataBound" >
					<RowStyle CssClass="datatd1" />
					<AlternatingRowStyle CssClass="datatd" />
					<HeaderStyle CssClass="msgheader" ForeColor="White"/>
					<Columns>
						<asp:TemplateField HeaderStyle-HorizontalAlign="Left" 
							ItemStyle-Width="3%" >
							<ItemTemplate >
								<asp:CheckBox runat="server"  id='valueupper' value='<%# DataBinder.Eval(Container.DataItem,"TransId")%>' />
								<asp:HiddenField ID="hdnId" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"TransId") %>' />
								<asp:HiddenField ID="hdnIsAutoCheck" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"UtilitySetting") %>' />
							</ItemTemplate>
						</asp:TemplateField>
						<asp:BoundField HeaderText="Control #" DataField="CtlNumber" ItemStyle-HorizontalAlign="Left"
							HeaderStyle-HorizontalAlign="Left" 
							 SortExpression="CtlNumber"  ></asp:BoundField>
						<asp:BoundField HeaderText="Trans Date" DataField="TransDate" ItemStyle-HorizontalAlign="Left"
							HeaderStyle-HorizontalAlign="Left" 
							 SortExpression="TransDate"></asp:BoundField>
						<asp:BoundField HeaderText="Payor" DataField="PayeeName" ItemStyle-HorizontalAlign="Left"
							HeaderStyle-HorizontalAlign="Left" 
							 SortExpression="PayeeName"></asp:BoundField>
						<asp:BoundField HeaderText="Claim #" DataField="ClaimNumber" ItemStyle-HorizontalAlign="Left"
							HeaderStyle-HorizontalAlign="Left" 
							 SortExpression="ClaimNumber"></asp:BoundField>
						<asp:BoundField HeaderText="Primary Claimant" DataField="PrimaryClaimantName" ItemStyle-HorizontalAlign="Left"
							HeaderStyle-HorizontalAlign="Left" 
							 SortExpression="PrimaryClaimantName"></asp:BoundField>
						<asp:BoundField HeaderText="Amount" DataField="PaymentAmount" ItemStyle-HorizontalAlign="Left"
							HeaderStyle-HorizontalAlign="Left" 
							 SortExpression="PaymentAmountNumber"></asp:BoundField>
						<asp:BoundField HeaderText="User" DataField="User" ItemStyle-HorizontalAlign="Left"
							HeaderStyle-HorizontalAlign="Left" 
							 SortExpression="User"></asp:BoundField>
						<asp:BoundField HeaderText="Trans Type" DataField="TransactionType" ItemStyle-HorizontalAlign="Left"
							HeaderStyle-HorizontalAlign="Left" 
							 SortExpression="TransactionType"></asp:BoundField>
						<asp:BoundField HeaderText="Split Amount" DataField="SplitAmount" ItemStyle-HorizontalAlign="Left"
							HeaderStyle-HorizontalAlign="Left" 
							 SortExpression="SplitAmountNumber"></asp:BoundField>
						<asp:BoundField HeaderText="From/To Dates" DataField="FromToDate" ItemStyle-HorizontalAlign="Left"
							HeaderStyle-HorizontalAlign="Left" 
							 SortExpression="FromToDate"></asp:BoundField>
					 <asp:TemplateField HeaderText="LSS Invoice" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" SortExpression="LSSID" >
						<ItemTemplate>
							<asp:HyperLink 
							   runat="server" ID="hplLssId" 
							   Text='<%# DataBinder.Eval(Container.DataItem,"LSSID")%>'
							   NavigateUrl="#" 
							   onclick='<%# "navigateToLSS(" + (char)(39) + "paramLSSInvoiceID=" + DataBinder.Eval(Container.DataItem,"LSSID") + (char)(39) + "); return false;"%>'>
							   </asp:HyperLink>
						</ItemTemplate>
					 </asp:TemplateField>
						<%--<asp:HyperLinkField DataTextField="LSSID"  ></asp:HyperLinkField>--%> 
					<asp:BoundField HeaderText="Payee Status" DataField="PayeeStatus" ItemStyle-HorizontalAlign="Left"
						HeaderStyle-HorizontalAlign="Left" 
						 SortExpression="PayeeStatus"></asp:BoundField>
                    <asp:ButtonField  Text="Comments" HeaderText="Comments" ButtonType="Link" ItemStyle-HorizontalAlign="Left"
							HeaderStyle-HorizontalAlign="Left" SortExpression="Comments"></asp:ButtonField>							
                            <%-- rsushilaggar MITS 22075 DATE 09/27/2010--%>
					</Columns>
				</asp:GridView>
			</tr>
			<%} %>
		</table>
	</div>
	<!-- start rsushilaggar MITS 20938 06/02/2010 -->
	 
   <% if (Model.SelectSingleNode("//ErrorTransactions") == null && Model.SelectSingleNode("//Transaction[@PaymentType = 'P']") != null)
	   {%>
		<div id="FORMTABBUTTONLISTPayments">
		<table width="100%" id="FORMTABBUTTONLISTPayments_" cellspacing="0" cellpadding="2"
			border="0">
			<tr class="ctrlgroup">
				<td colspan="10">
				</td>
				
			</tr>
            <%--Added by Amitosh for R8 enhancement of OverRide Authority--%>
                 <tr>
                 <td>
                 <asp:Label runat="server" ID="lbl_overrideamount" text="Override Amount:"></asp:Label>&nbsp;&nbsp;
                 </td>
                 <td>
                  <asp:TextBox runat="Server" id="tboverrideamount" RMXRef="Instance/Funds/OverRideAmount"   onchange="setDataChanged(true);" MaxLength ="10"  onblur="numLostFocus(this);"/>
                 </td>
                 </tr>
                 <tr>
                 <td>
                 <asp:Label runat="server" ID="lbl_applyoverride" text="Apply Override Amount:"></asp:Label>&nbsp;&nbsp;
                 </td>
                 <td>
                 <asp:CheckBox ID="applyoverride"  RMXRef="Instance/Funds/ApplyOverRideAmount" onchange="setDataChanged(true);" runat ="server"/>
                 </td>
                 </tr>
                 <tr>
                 <td>
                 <asp:Label runat="server" ID="lbl_applyoffset" text="Apply Offset:"></asp:Label>&nbsp;&nbsp;
                 </td>
                 <td>
                 <asp:CheckBox ID="applyoffset"  RMXRef="Instance/Funds/ApplyOffSet" onchange="setDataChanged(true);" runat ="server"/>
                 </td>
               

            </tr>
            <%--End amitosh--%>
			<tr class="datatd">
				<%--<td colspan="5" width="80%">
				</td>--%>
				<td><u><asp:Label runat="server" ID="lbl_txtVoidReason" text="Void Check Reason:"></asp:Label> </u>&nbsp;&nbsp;</td>
				<td>
					<span class="formw">
				  <asp:TextBox runat="Server" id="voidreason" RMXRef="Instance/Funds/VoidReason" RMXType="memo" ReadOnly="true" style="background-color: #F2F2F2;" onchange="setDataChanged(true);" tabindex="29" TextMode="MultiLine" Columns="30" rows="3" />
				  <asp:button runat="server" class="MemoButton" name="voidreasonbtnMemo" id="voidreasonbtnMemo" onclientclick="return EditHTMLMemo('voidreason','yes');" />
				  <asp:TextBox style="display:none" runat="server" RMXRef="Instance/Funds/VoidReason_HTMLComments" id="voidreason_HTML" />
				</span>    
				</td>
				<!-- Commented by Shivendu for MITS 18517. Deny button should always be visible. -->
				<%--<% if (Model.SelectSingleNode("//Transaction/@UtilitySetting").Value == "Q")
			   {%>--%>
				<td id="tddeny" runat="server" width="5%" >
					<asp:Button runat="server" ID="btnDeny" class="button" Style="width: 100px" Text="Deny"
						OnClick="btnDeny_Click" OnClientClick="SubmitQuery('deny');"  />
				</td>
				<%-- <%}%>--%>
				<td id="tdapprove" runat="server" width="5%" >
					<asp:Button runat="server" ID="btnApprove" class="button" Style="width: 100px" Text="Approve"
						OnClick="btnApprove_Click" OnClientClick="SubmitQuery('approve');" />
				</td>
				<td id="tdvoid" runat="server" width="5%" >
					<asp:Button runat="server" ID="btnVoid" class="button" Style="width: 100px" Text="Void"
						OnClick="btnVoid_Click" OnClientClick="SubmitQuery('void');"  />
				</td>
				<td width="5%">
					<asp:Button runat="server" ID="btnPrint" class="button" Style="width: 100px" Text="Print Report" />
				</td>
			</tr>
		</table>
	</div>
	<%} %>
	
	<% if (Model.SelectSingleNode("//ErrorTransactions") == null && Model.SelectSingleNode("//Transaction[@PaymentType = 'C']") != null)
	   {%>
		<div id="FORMTABBUTTONLISTCollectionsfromLSS" style="display:none">
		<table width="100%" id="FORMTABBUTTONLISTCollectionsfromLSS_" cellspacing="0" cellpadding="2"
			border="0">
			<tr class="ctrlgroup">
				<td colspan="10">
				</td>
				
			</tr>
			<tr class="datatd">
				<td colspan="5" width="80%">
				</td>
				<!-- Commented by Shivendu for MITS 18517. Deny button should always be visible. -->
				<%--<% if (Model.SelectSingleNode("//Transaction/@UtilitySetting").Value == "Q")
			   {%>--%>
				<td width="5%">
					<asp:Button runat="server" ID="btnColDeny" class="button" Style="width: 100px" Text="Deny"
						OnClick="btnColDeny_Click" OnClientClick="SubmitQuery('deny');"  />
				</td>
				<%-- <%}%>--%>
				<td width="5%">
					<asp:Button runat="server" ID="btnColApprove" class="button" Style="width: 100px" Text="Approve"
						OnClick="btnColApprove_Click" OnClientClick="SubmitQuery('approve');" />
				</td>
				<td width="5%">
					<asp:Button runat="server" ID="btnColVoid" class="button" Style="width: 100px" Text="Void"
						OnClick="btnColVoid_Click" OnClientClick="SubmitQuery('void');"  />
				</td>
				<td width="5%">
					<asp:Button runat="server" ID="btnColPrint" class="button" Style="width: 100px" Text="Print Report" />
				</td>
			</tr>
		</table>
	</div>
	<%} %>
	
	<%--<%}%>--%>
	<% if (Model.SelectSingleNode("//Transaction") != null && Model.SelectSingleNode("//Transaction").Attributes["UtilitySetting"].Value == "Q")
	   {%>
	<div id="FORMTABDENIALPayments">
		<table width="100%" id="FORMTABDENIALPayments_" cellspacing="0" cellpadding="2"
			border="0">
			<tr class="colheader3">
				<td>
					Funds Transactions Available For Denial Only
				</td>
			</tr>
			<asp:GridView ID="gvDeniedTransactions" runat="server" AutoGenerateColumns="false"
				AllowPaging="false" Width="100%" ShowHeader="true" GridLines="Both" AllowSorting="true"
				 OnRowDataBound="GridView_RowDataBound" OnSorting="GridView_Sorting" OnDataBound="GridView_DataBound">
				<RowStyle CssClass="datatd1" />
				<AlternatingRowStyle CssClass="datatd" />
				<HeaderStyle CssClass="msgheader" ForeColor="White"/>
				<Columns>
					<asp:TemplateField HeaderStyle-HorizontalAlign="Left" 
						ItemStyle-Width="3%">
						<ItemTemplate>
							<asp:CheckBox id='valuelower' runat="server" value='<%# DataBinder.Eval(Container.DataItem,"TransId")%>' />
							<asp:HiddenField ID="hdnId" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"TransId") %>' />
							<asp:HiddenField ID="hdnIsAutoCheck" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"UtilitySetting") %>' />
						</ItemTemplate>
					</asp:TemplateField>
					<asp:BoundField HeaderText="Control #" DataField="CtlNumber" ItemStyle-HorizontalAlign="Left"
						HeaderStyle-HorizontalAlign="Left" 
						ItemStyle-Width="10%" SortExpression="CtlNumber"></asp:BoundField>
					<asp:BoundField HeaderText="Trans Date" DataField="TransDate" ItemStyle-HorizontalAlign="Left"
						HeaderStyle-HorizontalAlign="Left" 
						ItemStyle-Width="8%" SortExpression="TransDate"></asp:BoundField>
					<asp:BoundField HeaderText="Payee" DataField="PayeeName" ItemStyle-HorizontalAlign="Left"
						HeaderStyle-HorizontalAlign="Left" 
						ItemStyle-Width="14%" SortExpression="PayeeName"></asp:BoundField>
					<asp:BoundField HeaderText="Claim #" DataField="ClaimNumber" ItemStyle-HorizontalAlign="Left"
						HeaderStyle-HorizontalAlign="Left" 
						ItemStyle-Width="18%" SortExpression="ClaimNumber"></asp:BoundField>
					<asp:BoundField HeaderText="Primary Claimant" DataField="PrimaryClaimantName" ItemStyle-HorizontalAlign="Left"
						HeaderStyle-HorizontalAlign="Left" 
						ItemStyle-Width="18%" SortExpression="PrimaryClaimantName"></asp:BoundField>
					<asp:BoundField HeaderText="Amount" DataField="PaymentAmount" ItemStyle-HorizontalAlign="Left"
						HeaderStyle-HorizontalAlign="Left" 
						ItemStyle-Width="10%" SortExpression="PaymentAmountNumber"></asp:BoundField>
					<asp:BoundField HeaderText="User" DataField="User" ItemStyle-HorizontalAlign="Left"
						HeaderStyle-HorizontalAlign="Left" 
						ItemStyle-Width="7%" SortExpression="User"></asp:BoundField>
					<asp:BoundField HeaderText="Trans Type" DataField="TransactionType" ItemStyle-HorizontalAlign="Left"
						HeaderStyle-HorizontalAlign="Left" 
						ItemStyle-Width="8%" SortExpression="TransactionType"></asp:BoundField>
					<asp:BoundField HeaderText="Split Amount" DataField="SplitAmount" ItemStyle-HorizontalAlign="Left"
						HeaderStyle-HorizontalAlign="Left" 
						ItemStyle-Width="12%" SortExpression="SplitAmountNumber"></asp:BoundField>
					<asp:BoundField HeaderText="From/To Dates" DataField="FromToDate" ItemStyle-HorizontalAlign="Left"
						HeaderStyle-HorizontalAlign="Left" 
						ItemStyle-Width="20%" SortExpression="FromToDate"></asp:BoundField>
					<asp:BoundField HeaderText="LSS Invoice" DataField="LSSID" ItemStyle-HorizontalAlign="Left"
						HeaderStyle-HorizontalAlign="Left" 
						ItemStyle-Width="20%" SortExpression="LSSID"></asp:BoundField>
					<asp:BoundField HeaderText="Payee Status" DataField="PayeeStatus" ItemStyle-HorizontalAlign="Left"
						HeaderStyle-HorizontalAlign="Left" 
						ItemStyle-Width="20%" SortExpression="PayeeStatus"></asp:BoundField>    
					<asp:ButtonField  Text="Comments" HeaderText="Comments" DataTextField="Comments" ButtonType="Link" ItemStyle-HorizontalAlign="Left"
						HeaderStyle-HorizontalAlign="Left" SortExpression="Comments"></asp:ButtonField>
					<%--rsushilaggar MITS 21452 26-jul-2010--%>
				 <%--<asp:TemplateField HeaderStyle-HorizontalAlign ="Left" HeaderText="Void Reason" ControlStyle-Width="20%"  >
					  
				<ItemTemplate>
					<input type="textarea"  id='txtVoidReasons' value='' />
			   </ItemTemplate>


			 </asp:TemplateField>--%>                          
						
						
						
						
				</Columns>
			</asp:GridView>
		</table>
	</div>
	<div id="FORMTABBUTTONLISTDENIALPayments">
		<table width="100%" id="FORMTABBUTTONLISTDENIALPayments_" cellspacing="0"
			cellpadding="2" border="0">
			<tr class="ctrlgroup">
				<td colspan="10">
				</td>
			</tr>
			<tr class="datatd">
				<td colspan="7" width="90%">
				</td>
				<% if (Model.SelectSingleNode("//Transaction").Attributes["UtilitySetting"].Value == "Q")
				   {%>
				<td width="5%">
					<asp:Button runat="server" ID="btnDenialDeny" class="button" Style="width: 100px"
						Text="Deny" OnClick="btnDenialDeny_Click" />
				</td>
				<%} %>
				<td width="5%">
					<asp:Button runat="server" ID="btnDenialPrint" class="button" Style="width: 100px"
						Text="Print Report" />
				</td>
			</tr>
		</table>
	</div>
	<%} %>
	<% if (Model.SelectSingleNode("//Transaction") != null)
     {%> <%Model = null;%> <%} %>
     
     
          <table>
						<tr>
							<%--<td width="20%" >Reason:    </td>--%>
                            <td width="20%" ><asp:Label id="lblReason" runat="server"  text="Reason:" ></asp:Label> </td>
							<td>
								<asp:TextBox id="reason" onchange="setDataChanged(true);" size="40" maxlength="255" type="text" runat="server"></asp:TextBox>
							</td>
                            <td width="5%" >    </td>
                            <td>
                                <asp:CheckBox ID="chkShowAllItem" runat="server" AutoPostBack="true" rmxref="/Instance/Document/SearchParam/ShowAllItems"  OnCheckedChanged="chkShowAllItem_CheckedChanged" Text="Show All Items" />
                            </td>
						</tr>
                    <tr>
                    <td>
                    <asp:Label runat="server" ID="lblDisplayStatus" text="Display Payment with Unapproved Payee status:"></asp:Label>&nbsp;&nbsp;
                    </td>
                    <td>
                    <asp:CheckBox ID="chkdisplaypayment"  RMXRef="Instance/Funds/Display" onchange="setDataChanged(true);" runat ="server" checked ="false" AutoPostBack="true" OnCheckedChanged="chkdisplaypayment_CheckedChanged"/>
                    </td>
                    </tr>
					</table>
    
	<%--<% Model = null;%>--%>
	
	<asp:HiddenField ID="hdnTransIdToUpper" runat="server" />
	<asp:HiddenField ID="hdnTransIdToLower" runat="server" />
	<asp:Label ID="lblErrors" runat="server"></asp:Label>
	<asp:TextBox ID="IsSearchEnabled" runat="server" type="text" rmxref="/Instance/Document/SearchParam/SearchEnabled" EnableViewState="true" style="display:none" />
	<asp:HiddenField EnableViewState ="true" runat="server" ID="GridSortExpression" />
	<asp:HiddenField EnableViewState ="true" runat="server" ID="GridSortDirection" Value="ASC" />
    <asp:HiddenField ID="hdnVoidreasonFlag" runat="server"/><%--averma62 - MITS 29130--%>
    <asp:TextBox ID="hdnLangCode" runat="server" type="text" rmxref="/Instance/Document/SearchParam/LangCode" style="display:none" />
    
	</form>
</body>
</html>
