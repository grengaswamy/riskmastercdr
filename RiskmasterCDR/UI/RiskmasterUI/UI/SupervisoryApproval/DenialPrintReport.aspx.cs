﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.Linq;
using System.Xml.XPath;

namespace Riskmaster.UI.SupervisoryApproval
{
    public partial class DenialPrintReport : System.Web.UI.Page
    {
        XElement objTempElement = null;
        //public XmlDocument Model = null;
        private string sPrintReportDenyTemplate = "<Message><Authorization>34880aa1-ee69-4766-8c95-2afd5fe434d2</Authorization><Call>" +
         "<Function>SupervisorApprovalAdaptor.CreateReportDenyTransactions</Function></Call><Document><SupervisorApproval><AllTransactions>N</AllTransactions><DenyTransactions>Y" +
         "</DenyTransactions><IsShowMyTrans/></SupervisorApproval></Document></Message>";
        protected void Page_Load(object sender, EventArgs e)
        {
                        try
            {
                if (!Page.IsPostBack)
                {
                    //ajohari2 RMA-13878(RMA 6404 and RMA 9875)
                    string sIsShowMyTrans = Request.QueryString["IsShowMyTrans"];
                    if (!String.IsNullOrEmpty(sIsShowMyTrans))
                        sPrintReportDenyTemplate = AppHelper.ChangeMessageValue(sPrintReportDenyTemplate, "//IsShowMyTrans", sIsShowMyTrans);
                    //ajohari2 RMA-13878(RMA 6404 and RMA 9875) END
                    string sReturn = AppHelper.CallCWSService(sPrintReportDenyTemplate.ToString());
                    //Model.LoadXml(sReturn);
                    objTempElement = XElement.Parse(sReturn);
                    objTempElement = objTempElement.XPathSelectElement("./Document/SupervisorApproval/File");
                    if (objTempElement != null)
                    {
                        if (objTempElement.Value != "")
                        {
                            byte[] pdfbytes = Convert.FromBase64String(objTempElement.Value);
                            if (pdfbytes != null)
                            {
                                Response.Buffer = true;
                                Response.Clear();
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Charset = "";
                                Response.AppendHeader("Content-Encoding", "none;");
                                Response.ContentType = "application/pdf";
                                Response.AddHeader("Content-Disposition", string.Format("inline; filename=REPORT.PDF"));
                                Response.AddHeader("Accept-Ranges", "bytes");
                                Response.BinaryWrite(pdfbytes);
                                Response.Flush();
                                Response.Close();
                            }
                        }
                    }
                        //14179 starts
                    else
                    {
                        lblNoreport.Visible = true;
                    }//14179 ends
                    //SetValuesFromService();
                    //BindpageControls(sReturn);
                } // if
                else
                {
                    lblNoreport.Visible = true;
                }

            } // try
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            } // catch
        }
    }
}
