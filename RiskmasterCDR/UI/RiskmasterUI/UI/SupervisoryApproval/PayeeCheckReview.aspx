﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PayeeCheckReview.aspx.cs" Inherits="Riskmaster.UI.UI.SupervisoryApproval.PayeeCheckReview" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="ajaxtoolkit" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" Tagname="PleaseWaitDialog" tagprefix="uc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Payee Check Review</title>
    <link rel="stylesheet" href="../../App_Themes/RMX_Default/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../App_Themes/RMX_Portal/Portal.css" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/WaitDialog.js"></script>
     <script language='javascript' type="text/javascript">
     function tab1_ClientClick() {
         document.forms[0].tabindex.value = "1";
         document.forms[0].TabButton1.click();
         pleaseWait.Show();
         
     }
     function tab2_ClientClick() {
         document.forms[0].tabindex.value = "2";
         document.forms[0].TabButton2.click();
         pleaseWait.Show();
     }
     function tab3_ClientClick() {
         document.forms[0].tabindex.value = "3";
         document.forms[0].TabButton3.click();
         pleaseWait.Show();
     }
     function lnkFirst_Click(TypeofVar) {
         if (TypeofVar == "Payee") {
             document.forms[0].Tab1PageNumber.value = "1";
             document.forms[0].tabindex.value = "1";
             return true;
         }

     }
     function lnkNext_Click(TypeofVar) {
         var PageNum;
         if (TypeofVar == "Payee") {
             PageNum = parseInt(document.forms[0].Tab1PageNumber.value) + 1;
             document.forms[0].Tab1PageNumber.value = String(PageNum);
             document.forms[0].tabindex.value = "1";
             return true;
         }

     }
     function lnkPrev_Click(TypeofVar) {
         var PageNum;
         if (TypeofVar == "Payee") {
             PageNum = parseInt(document.forms[0].Tab1PageNumber.value) - 1;
             document.forms[0].Tab1PageNumber.value = String(PageNum);
             document.forms[0].tabindex.value = "1";
             return true;
         }

     }
     function lnkLast_Click(TypeofVar) {
         if (TypeofVar == "Payee") {
             document.forms[0].Tab1PageNumber.value = document.forms[0].Tab1LastPage.value;
             document.forms[0].tabindex.value = "1";
             return true;
         }


     }
     function GetByName(TypeOfName) {
         debugger;
         //igupta3 Mits:33301
         var gridElementsRadio = document.getElementsByName(TypeOfName);
         if (gridElementsRadio.length == 0 && !window.ie) {
             var num = 0;
             var gridElementsnew = new Array();
             try {
                 var input = document.getElementsByTagName('input');
                 for (i = 0; i < input.length; i++) {
                     if (input[i].id == TypeOfName) {
                         //gridElements[num] = document.getElementById('MyRadioButton');
                         gridElementsnew[num] = input[i];
                         num++;
                     }
                 }
                 gridElementsRadio = gridElementsnew;
             }
             catch (e) {
             }
         }         
         return gridElementsRadio;
     }
     function SelectAll() {
         var i;

         if (document.forms[0].tabindex.value == "")
             document.forms[0].tabindex.value = "1";
         
         if (document.forms[0].tabindex.value == "1") {             
                 var checkEntityId = GetByName("checkEntityId");
             for (i = 0; i <= checkEntityId.length - 1; i++)
                 checkEntityId[i].checked = true;
                
         }
         if (document.forms[0].tabindex.value == "2") {             
                 var checkCheckId = GetByName("checkCheckId");
             for (i = 0; i <= checkCheckId.length - 1; i++)
                 checkCheckId[i].checked = true;
         }
         if (document.forms[0].tabindex.value == "3") {
                 var checkAutoCheck = GetByName("checkAutoCheck");
             for (i = 0; i <= checkAutoCheck.length - 1; i++)
                 checkAutoCheck[i].checked = true;
         }

         return false;
     }
     function DeSelectAll() {
         var i;

         if (document.forms[0].tabindex.value == "1") {             
                 var checkEntityId = GetByName("checkEntityId");
             for (i = 0; i <= checkEntityId.length - 1; i++)
                 checkEntityId[i].checked = false;
         }
         if (document.forms[0].tabindex.value == "2") {             
                 var checkCheckId = GetByName("checkCheckId");
             for (i = 0; i <= checkCheckId.length - 1; i++)
                 checkCheckId[i].checked = false;
         }
         if (document.forms[0].tabindex.value == "3") {             
                 var checkAutoCheck = GetByName("checkAutoCheck");
             for (i = 0; i <= checkAutoCheck.length - 1; i++)
                 checkAutoCheck[i].checked = false;
         }

         return false;
     }

     function CheckSelection(buttonName, itemType) {
         
         if (buttonName == 'Save') {
             var i;
             var sTmp = "";
             var s = "";

             // obj = document.getElementsByTagName("input");
             if (document.forms[0].tabindex.value == "1")
                 //obj = document.getElementsByName("checkEntityId");
                 obj = GetByName("checkEntityId");

             if (document.forms[0].tabindex.value == "2")
                 //obj = document.getElementsByName("checkCheckId");
                 obj = GetByName("checkCheckId");

             if (document.forms[0].tabindex.value == "3")
                 // obj = document.getElementsByName("checkAutoCheck");
                 obj = GetByName("checkAutoCheck");

             if (obj == null)
                 return false;

             s = new String(eval(obj.length));
             if (s == "undefined") {
                 if (obj.checked == true) {
                     sTmp = obj.value;
                 }
             }
             else {
                 for (i = 0; i < obj.length; i++) {
                     if (obj[i].checked)
                         sTmp = sTmp + "," + obj[i].value;
                 }

                 document.getElementById("selectedvalues").value = sTmp.substr(1, sTmp.length - 1);
             }
             if (sTmp == "") {
                 alert('Please select some ' + itemType + '(s)');
                 return false;
             }
         }
       
         return true;
     }

     function LoadPleaseWait() {
         pleaseWait.Show();
         return true;
     }
          
</script>
</head>
<body onload="parent.MDIScreenLoaded();">

    <form id="frmData" runat="server">
    <%--<asp:Button ID="Button2" runat="server"    style="display:none;"/>
    <asp:Button ID="Button1" runat="server"  style="display:none;"   />--%>
    <%--<uc2:PleaseWaitDialog ID="PleaseWaitDialog4" runat="server"  CustomMessage="ddd" />--%>
    <asp:Button ID="TabButton1" runat="server" OnClick="TabButton1_Click" style="display:none;"/>
    <asp:Button ID="TabButton2" runat="server" OnClick="TabButton2_Click" style="display:none;" />
    <asp:Button ID="TabButton3" runat="server" OnClick="TabButton3_Click" style="display:none;"/>
                    <asp:ImageButton runat="server" ID="btnSave" title="Save"
    onMouseOver="this.src='../../Images/tb_save_mo.png';this.style.zoom='110%'" onMouseOut="this.src='../../Images/tb_save_active.png';this.style.zoom='100%'"  src="../../Images/tb_save_active.png"  style="height:28px;width:28px;border-width:0px;" OnClientClick="return CheckSelection('Save','Record');" OnClick="btnSave_Click" />
    <asp:ImageButton runat="server" ID="btnSelectAll" onMouseOver="this.src='../../Images/tb_grantmodules_mo.png';this.style.zoom='110%'" onMouseOut="this.src='../../Images/tb_grantmodules_active.png';this.style.zoom='100%'" title="Select All" src="../../Images/tb_grantmodules_active.png"  style="height:28px;width:28px;border-width:0px;" OnClientClick="return SelectAll();" />
    <asp:ImageButton runat="server" ID="btnDeselectAll" title="Deselect All"
    onMouseOver="this.src='../../Images/tb_blockmodules_mo.png';this.style.zoom='110%'" onMouseOut="this.src='../../Images/tb_blockmodules_active.png';this.style.zoom='100%'"  src="../../Images/tb_blockmodules_active.png"   style="height:28px;width:28px;border-width:0px;" OnClientClick="return DeSelectAll();" />
                  <br /><br />
                  
                   <asp:ScriptManager ID="ScriptManager2" runat="server">
                   
        </asp:ScriptManager>
        <%--<asp:UpdateProgress ID="UpdateProgress" runat="server" AssociatedUpdatePanelID="PayeeCheckReviewTabs">
            <ProgressTemplate>
                Loading...
            </ProgressTemplate>
        </asp:UpdateProgress>--%>
        <ajaxtoolkit:tabcontainer id="PayeeCheckReviewTabs" CssClass="ajax__tab_xp2" runat="server" activetabindex="0" >
            <ajaxtoolkit:tabpanel runat="server" OnClientClick="tab1_ClientClick" headertext="Release Payee(s)" id="tabReviewPayees"  >
                <headertemplate>
                    Release Payee(s)
                </headertemplate>
                <contenttemplate>
                <asp:UpdatePanel ID="up1" runat="server" UpdateMode="Conditional" >
                <Triggers>
        <asp:AsyncPostBackTrigger ControlID="TabButton1" />
    </Triggers>


<ContentTemplate>

                  <uc1:ErrorControl ID="ErrorControl1" runat="server" />  
                  <uc2:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server"  CustomMessage="Loading..."/>

                    
                    List of Payee(s) on Freeze by Background OFAC check
            <br /><br />
                  
                   <asp:LinkButton ID="lnkFirst" Text = "First |" runat="server" OnClientClick=" return lnkFirst_Click('Payee');" OnClick="TabButton1_Click" ></asp:LinkButton>
          <asp:LinkButton ID="lnkNext" Text = "Next > |" runat="server" OnClientClick="return lnkNext_Click('Payee');" OnClick="TabButton1_Click" ></asp:LinkButton>
          <asp:LinkButton ID="lnkPrev" Text = "< Prev |" runat="server" OnClientClick="return lnkPrev_Click('Payee');" OnClick="TabButton1_Click"></asp:LinkButton>
          <asp:LinkButton ID="lnkLast" Text = "Last" runat="server" OnClientClick="return lnkLast_Click('Payee');" OnClick="TabButton1_Click"></asp:LinkButton>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               
            <asp:Label ID="Label1" Text="Page 1 Of 1" runat="server" ></asp:Label>
            <br />
            <asp:GridView ID="gvPayeeFreezeList" runat="server" AutoGenerateColumns="false"
            AllowPaging="false" Width="100%" ShowHeader="true" GridLines="None">
            <RowStyle CssClass="" />
            <AlternatingRowStyle CssClass="data2" /> 
             <HeaderStyle CssClass="colheader6" ForeColor="White"/>
            <Columns>
                <asp:TemplateField  ItemStyle-CssClass="data"
                     HeaderStyle-CssClass="headerlink2">
                    <ItemTemplate>
                        <input type="checkbox" id='checkEntityId' value='<%#Eval("EntityId")%>' />
                        <%--<asp:HiddenField ID="hdnId" runat="server" Value='' />
                        <asp:HiddenField ID="hdnIsAutoCheck" runat="server" Value='' />--%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText="Type"  DataField="Type" 
                    HeaderStyle-CssClass="headerlink2"
                    ItemStyle-CssClass="data"></asp:BoundField>
                <asp:BoundField HeaderText="SSN/Tax ID" DataField="TaxId" HeaderStyle-CssClass="headerlink2"
                    ItemStyle-CssClass="data"
                    ></asp:BoundField>
                <asp:BoundField HeaderText="Last Name" DataField="LastName"  HeaderStyle-CssClass="headerlink2"
                    ItemStyle-CssClass="data"
                    ></asp:BoundField>
                <asp:BoundField HeaderText="First Name" DataField="FirstName" ItemStyle-CssClass="data"
                     HeaderStyle-CssClass="headerlink2"
                   ></asp:BoundField>
                <%--<asp:TemplateField  ItemStyle-CssClass="data"
                     HeaderStyle-CssClass="headerlink2" HeaderText="Birth Date">
                    <ItemTemplate>
                        <asp:Label ID="lbl" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>       --%>
                <asp:BoundField HeaderText="Birth Date" DataField="BirthDate" ItemStyle-CssClass="data"
                    HeaderStyle-CssClass="headerlink2"
                    ></asp:BoundField>
                <asp:BoundField HeaderText="Office Phone" DataField="OfficePhone" ItemStyle-CssClass="data"
                     HeaderStyle-CssClass="headerlink2"
                    ></asp:BoundField>
                <asp:BoundField HeaderText="Home Phone" DataField="HomePhone" ItemStyle-CssClass="data"
                     HeaderStyle-CssClass="headerlink2"
                    ></asp:BoundField>
                <asp:BoundField HeaderText="Date Time of Freeze" DataField="DttmFreeze" ItemStyle-CssClass="data"
                     HeaderStyle-CssClass="headerlink2"
                    ></asp:BoundField>
                
            </Columns>
        </asp:GridView>
        </ContentTemplate>

</asp:UpdatePanel>


                </contenttemplate>
            </ajaxtoolkit:tabpanel>
            <ajaxtoolkit:tabpanel runat="server" OnClientClick="tab2_ClientClick" headertext="Release Check(s)" id="tabReviewChecks">
                <headertemplate>
                    Release Check(s)
                </headertemplate>
                <contenttemplate>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <Triggers>
        <asp:AsyncPostBackTrigger ControlID="TabButton2" />
    </Triggers>

<ContentTemplate>

                    <uc1:ErrorControl ID="ErrorControl2" runat="server" />
                    <uc2:PleaseWaitDialog ID="PleaseWaitDialog2" runat="server"  CustomMessage="Loading..."/>

                    List of Check(s) on Hold by Background OFAC check
             <br /><br />
            <asp:LinkButton ID="LinkButton1" Text = "First |" runat="server" OnClientClick="return lnkFirst_Click('Check');" OnClick="TabButton2_Click" ></asp:LinkButton>
          <asp:LinkButton ID="LinkButton2" Text = "Next > |" runat="server" OnClientClick="return lnkNext_Click('Check');" OnClick="TabButton2_Click"  ></asp:LinkButton>
          <asp:LinkButton ID="LinkButton3" Text = "< Prev |" runat="server" OnClientClick="return lnkPrev_Click('Check');" OnClick="TabButton2_Click" ></asp:LinkButton>
          <asp:LinkButton ID="LinkButton4" Text = "Last" runat="server" OnClientClick="return lnkLast_Click('Check');" OnClick="TabButton2_Click" ></asp:LinkButton>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               
            <asp:Label ID="Label2" Text="Page 1 Of 1" runat="server" ></asp:Label>
            <br />
            <asp:GridView ID="gvHoldChecksList" runat="server" AutoGenerateColumns="false"
            AllowPaging="false" Width="100%" ShowHeader="true" GridLines="None">
            <RowStyle CssClass="" />
            <AlternatingRowStyle CssClass="data2" /> 
             <HeaderStyle CssClass="colheader6" ForeColor="White"/>
            <Columns>
                <asp:TemplateField ItemStyle-CssClass="data"
                     HeaderStyle-CssClass="headerlink2">
                    <ItemTemplate>
                        <input type="checkbox" id='checkCheckId' value='<%#Eval("TransId")%>' />
                        <%--<asp:HiddenField ID="hdnId" runat="server" Value='' />
                        <asp:HiddenField ID="hdnIsAutoCheck" runat="server" Value='' />--%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText="Control Number"  DataField="CtlNumber" ItemStyle-CssClass="data"
                     HeaderStyle-CssClass="headerlink2"></asp:BoundField>
                <asp:BoundField HeaderText="Claim Number" DataField="ClaimNumber" ItemStyle-CssClass="data"
                     HeaderStyle-CssClass="headerlink2"></asp:BoundField>
                <asp:BoundField HeaderText="Check Date" DataField="DateOfCheck"  ItemStyle-CssClass="data"
                     HeaderStyle-CssClass="headerlink2"></asp:BoundField>
                <asp:BoundField HeaderText="Transaction Date" DataField="TransDate" ItemStyle-CssClass="data"
                     HeaderStyle-CssClass="headerlink2"></asp:BoundField>
                <asp:BoundField HeaderText="Payee Last Name" DataField="LastName" ItemStyle-CssClass="data"
                     HeaderStyle-CssClass="headerlink2"></asp:BoundField>
                <asp:BoundField HeaderText="Payee First Name" DataField="FirstName" ItemStyle-CssClass="data"
                     HeaderStyle-CssClass="headerlink2"></asp:BoundField>
                <asp:BoundField HeaderText="Amount" DataField="Amount" ItemStyle-CssClass="data"
                     HeaderStyle-CssClass="headerlink2"></asp:BoundField>
                <asp:BoundField HeaderText="Date Time of Hold" DataField="DttmHold" ItemStyle-CssClass="data"
                     HeaderStyle-CssClass="headerlink2"></asp:BoundField>
                
            </Columns>
        </asp:GridView>
        </ContentTemplate>
</asp:UpdatePanel>


                </contenttemplate>
            </ajaxtoolkit:tabpanel>
            <ajaxtoolkit:tabpanel runat="server" OnClientClick="tab3_ClientClick"  headertext="Release Check(s)" id="Tabpanel1">
                <headertemplate>
                    Release Batch(es)
                </headertemplate>
                <contenttemplate>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                <Triggers>
        <asp:AsyncPostBackTrigger ControlID="TabButton3" />
    </Triggers>

<ContentTemplate>

                    <uc1:ErrorControl ID="ErrorControl3" runat="server" />
                    <uc2:PleaseWaitDialog ID="PleaseWaitDialog3" runat="server"  CustomMessage="Loading..."/>

                    List of Batch(es)/Auto-Check(s) on Freeze by Background OFAC check
               <br /><br />
              <asp:LinkButton ID="LinkButton5" Text = "First |" runat="server" OnClientClick="return lnkFirst_Click('Auto');"  OnClick="TabButton3_Click"></asp:LinkButton>
          <asp:LinkButton ID="LinkButton6" Text = "Next > |" runat="server" OnClientClick="return lnkNext_Click('Auto');" OnClick="TabButton3_Click"></asp:LinkButton>
          <asp:LinkButton ID="LinkButton7" Text = "< Prev |" runat="server" OnClientClick="return lnkPrev_Click('Auto');" OnClick="TabButton3_Click"></asp:LinkButton>
          <asp:LinkButton ID="LinkButton8" Text = "Last" runat="server" OnClientClick="return lnkLast_Click('Auto');" OnClick="TabButton3_Click"></asp:LinkButton>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               
               
            <asp:Label ID="Label3" Text="Page 1 Of 1" runat="server" ></asp:Label>
            <br />
             <asp:GridView ID="gvHoldBatchesList" runat="server" AutoGenerateColumns="false"
            AllowPaging="false" Width="100%" ShowHeader="true" GridLines="None">
            <RowStyle CssClass="" />
            <AlternatingRowStyle CssClass="data2" /> 
             <HeaderStyle CssClass="colheader6" ForeColor="White"/>
            <Columns>
                <asp:TemplateField ItemStyle-CssClass="data"
                     HeaderStyle-CssClass="headerlink2">
                    <ItemTemplate>
                        <input type="checkbox" id='checkAutoCheck' value='<%#Eval("AutoBatchId")%>' />
                        <%--<asp:HiddenField ID="hdnId" runat="server" Value='' />
                        <asp:HiddenField ID="hdnIsAutoCheck" runat="server" Value='' />--%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText="Claim Number" DataField="ClaimNumber" ItemStyle-CssClass="data"
                     HeaderStyle-CssClass="headerlink2"></asp:BoundField>
                <asp:BoundField HeaderText="Batch ID" DataField="AutoBatchId"  ItemStyle-CssClass="data"
                     HeaderStyle-CssClass="headerlink2"></asp:BoundField>
                <asp:BoundField HeaderText="First Check's Print Date" DataField="PrintDate" ItemStyle-CssClass="data"
                     HeaderStyle-CssClass="headerlink2"></asp:BoundField>
                <asp:BoundField HeaderText="Payee Last Name" DataField="LastName" ItemStyle-CssClass="data"
                     HeaderStyle-CssClass="headerlink2"></asp:BoundField>
                <asp:BoundField HeaderText="Payee First Name" DataField="FirstName" ItemStyle-CssClass="data"
                     HeaderStyle-CssClass="headerlink2"></asp:BoundField>
                <asp:BoundField HeaderText="Amount" DataField="Amount" ItemStyle-CssClass="data"
                     HeaderStyle-CssClass="headerlink2"></asp:BoundField>
                <asp:BoundField HeaderText="Date Time of Freeze" DataField="DttmFreeze" ItemStyle-CssClass="data"
                     HeaderStyle-CssClass="headerlink2"></asp:BoundField>
                
            </Columns>
        </asp:GridView>
        </ContentTemplate>

</asp:UpdatePanel>

                </contenttemplate>
            </ajaxtoolkit:tabpanel>
        </ajaxtoolkit:tabcontainer>
         
         <asp:TextBox runat="server" ID="selectedvalues" style="display:none" />
         <asp:TextBox runat="server" ID="tabindex" style="display:none" />
         <asp:TextBox runat="server" ID="Tab1PageNumber" style="display:none" />
         <asp:TextBox runat="server" ID="Tab2PageNumber" style="display:none" />
         <asp:TextBox runat="server" ID="Tab3PageNumber" style="display:none" />
         <asp:TextBox runat="server" ID="Tab1LastPage" style="display:none" />
         <asp:TextBox runat="server" ID="Tab2LastPage" style="display:none" />
         <asp:TextBox runat="server" ID="Tab3LastPage" style="display:none" />
         
         
     <%--<uc2:PleaseWaitDialog ID="PleaseWaitDialog4" runat="server"  CustomMessage="Loading..."/>--%>
    </form>
</body>
</html>
