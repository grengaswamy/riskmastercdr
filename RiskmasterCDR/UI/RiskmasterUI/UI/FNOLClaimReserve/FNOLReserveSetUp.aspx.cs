﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Text;
using System.IO;
using Riskmaster.AppHelpers;
using Riskmaster.RMXResourceManager; //spahariya for ML
namespace Riskmaster.UI.FNOLClaimReserve
{
    public partial class FNOLReserveSetUp : NonFDMBasePageCWS
    {
        XElement XmlTemplate = null;
        string sCWSresponse = "";
        XmlDocument Model = new XmlDocument();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //spahariya ML Changes
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("FNOLReserveSetUp.aspx"), "FNOLReserveSetUpValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "FNOLReserveSetUpValidations", sValidationResources, true);
                //spahariya ML Changes
                if (string.Compare(txtWinClose.Text, "yes", true) == 0)
                {
                    //HttpRuntime.Cache["RedirectFNOLXML"] = string.Empty;
                    HttpRuntime.Cache["RedirectPageXML"] = string.Empty;
                    txtWinClose.Text = "";
                }
                else
                {
                    string sClaimID = string.Empty;
                    if (Request.QueryString["ClaimId"] != null)
                    {
                        sClaimID = Request.QueryString["ClaimId"];
                        txtClaimId.Text = sClaimID;
                    }
                    else
                    {
                        txtClaimId.Text = AppHelper.GetQueryStringValue("gridname");
                    }
                    if (!IsPostBack)
                    {
                        gridname.Text = AppHelper.GetQueryStringValue("gridname");
                        mode.Text = AppHelper.GetQueryStringValue("mode");
                        selectedrowposition.Text = AppHelper.GetQueryStringValue("selectedrowposition");
                        if (string.Compare(mode.Text.ToLower(), "add", true) == 0)
                        {
                            XmlTemplate = GetResMessageTemplate();
                            CallCWS("FNOLReserveAdaptor.GetResSetUp", XmlTemplate, out sCWSresponse, true, true);
                            Model.LoadXml(sCWSresponse);
                            PopulatePolicy(Model);
                            PopulateClaimant(Model);
                            HttpRuntime.Cache["RedirectPageXML"] = Model;
                            //string sReturn = AppHelper.CallCWSService(MergeMessageTemplate.ToString());
                        }

                    }
                    else
                    {
                        TextBox txtPostBackParent = (TextBox)this.FindControl("txtPostBack");
                        txtPostBackParent.Text = "Done";

                        Control c = DatabindingHelper.GetPostBackControl(this.Page);

                        if (c == null)
                        {
                            XmlTemplate = GetResMessageTemplate();
                            CallCWS("FNOLReserveAdaptor.GetResSetUp", XmlTemplate, out sCWSresponse, false, true);
                            Model.LoadXml(sCWSresponse);
                            //PopulatePolicy(Model);
                        }
                        else
                        {
                            if (HttpRuntime.Cache["RedirectPageXML"] != null && Convert.ToString(HttpRuntime.Cache["RedirectPageXML"]) != "")
                            {
                                Model = (XmlDocument)HttpRuntime.Cache["RedirectPageXML"];
                            }
                        }
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XElement GetResMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><FNOLSetup>");
            sXml = sXml.Append("<ClaimID>");
            sXml = sXml.Append(txtClaimId.Text);
            sXml = sXml.Append("</ClaimID>");
            sXml = sXml.Append("</FNOLSetup></Document>");
            sXml = sXml.Append("</Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;
        }

        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder();
            sXml = sXml.Append("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><FNOLResSetup>");
            sXml = sXml.Append("<control name='ClaimId' type='id'>");
            sXml = sXml.Append(txtClaimId.Text);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<FNOLReserve>");
            sXml = sXml.Append("<control name='Policy' type='code'>");
            sXml = sXml.Append(cmbPolicy.SelectedValue);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name='Unit' type='code'>");
            sXml = sXml.Append(cmbUnit.SelectedValue);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name='Claimant' type='entity'>");
            sXml = sXml.Append(cmbClaimant.SelectedValue);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name='CoverageType' type='code'>");
            sXml = sXml.Append(cmbCoverageType.SelectedValue);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("<control name='LossType' type='code'>");
            sXml = sXml.Append(cmbLossType.SelectedValue);
            sXml = sXml.Append("</control>");
            sXml = sXml.Append("</FNOLReserve>");
            sXml = sXml.Append("</FNOLResSetup></Document>");
            sXml = sXml.Append("</Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;
        }


        private XElement GetMessageTemplateOld()
        {
            string sRowId = string.Empty;
            XmlDocument xmlFNOLRserves = new XmlDocument();
            XmlNode xmlResSetup = null;
            StringBuilder sXml = new StringBuilder();
            XElement oElement = null;
            XmlElement xmlElem = null;
            if (HttpRuntime.Cache["RedirectPageXML"] != null && Convert.ToString(HttpRuntime.Cache["RedirectFNOLXML"]) != "")
            {
                xmlFNOLRserves = (XmlDocument)HttpRuntime.Cache["RedirectFNOLXML"];
                xmlResSetup = xmlFNOLRserves.SelectSingleNode("//FNOLResSetup");
                sXml = sXml.Append("<FNOLReserve>");
                sXml = sXml.Append("<control name='Policy' type='code'>");
                sXml = sXml.Append(cmbPolicy.SelectedValue);
                sXml = sXml.Append("</control>");
                sXml = sXml.Append("<control name='Unit' type='code'>");
                sXml = sXml.Append(cmbUnit.SelectedValue);
                sXml = sXml.Append("</control>");
                sXml = sXml.Append("<control name='Claimant' type='entity'>");
                sXml = sXml.Append(cmbClaimant.SelectedValue);
                sXml = sXml.Append("</control>");
                sXml = sXml.Append("<control name='CoverageType' type='code'>");
                sXml = sXml.Append(cmbCoverageType.SelectedValue);
                sXml = sXml.Append("</control>");
                sXml = sXml.Append("<control name='LossType' type='code'>");
                sXml = sXml.Append(cmbLossType.SelectedValue);
                sXml = sXml.Append("</control>");
                sXml = sXml.Append("</FNOLReserve>");
                oElement = XElement.Parse(sXml.ToString());
                xmlElem = (XmlElement)xmlFNOLRserves.ReadNode(oElement.CreateReader());
                xmlResSetup.AppendChild(xmlElem);
                HttpRuntime.Cache["RedirectFNOLXML"] = xmlFNOLRserves;
                oElement = XElement.Parse(xmlFNOLRserves.InnerXml);
            }
            else
            {
                if (string.Compare(mode.Text.ToLower(), "edit", true) == 0)
                {
                    sRowId = AppHelper.GetQueryStringValue("selectedid");
                }
                sXml = sXml.Append("<Message>");
                sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
                sXml = sXml.Append("<Call><Function></Function></Call><Document><FNOLResSetup>");
                sXml = sXml.Append("<control name='ClaimId' type='id'>");
                sXml = sXml.Append(txtClaimId.Text);
                sXml = sXml.Append("</control>");
                sXml = sXml.Append("<FNOLReserve>");
                sXml = sXml.Append("<control name='RowId' type='id'>");
                sXml = sXml.Append(sRowId);
                sXml = sXml.Append("</control>");
                sXml = sXml.Append("<control name='Policy' type='code'>");
                sXml = sXml.Append(cmbPolicy.SelectedValue);
                sXml = sXml.Append("</control>");
                sXml = sXml.Append("<control name='Unit' type='code'>");
                sXml = sXml.Append(cmbUnit.SelectedValue);
                sXml = sXml.Append("</control>");
                sXml = sXml.Append("<control name='Claimant' type='entity'>");
                sXml = sXml.Append(cmbClaimant.SelectedValue);
                sXml = sXml.Append("</control>");
                sXml = sXml.Append("<control name='CoverageType' type='code'>");
                sXml = sXml.Append(cmbCoverageType.SelectedValue);
                sXml = sXml.Append("</control>");
                sXml = sXml.Append("<control name='LossType' type='code'>");
                sXml = sXml.Append(cmbLossType.SelectedValue);
                sXml = sXml.Append("</control>");
                sXml = sXml.Append("</FNOLReserve>");
                sXml = sXml.Append("</FNOLResSetup></Document>");
                sXml = sXml.Append("</Message>");
                oElement = XElement.Parse(sXml.ToString());
            }
            return oElement;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            XmlDocument xmlDoc = new XmlDocument(); 
            try
            {
                
                XmlTemplate = GetMessageTemplate();
                CallCWS("FNOLReserveAdaptor.SaveResSetup", XmlTemplate, out sCWSresponse, false, false);
                XmlDocument objReturnXml = new XmlDocument();
                objReturnXml.LoadXml(sCWSresponse);
                string sMsgStatus = objReturnXml.SelectSingleNode("//MsgStatusCd").InnerText;
                if (string.Compare(sMsgStatus, "Success", true) == 0)
                {
                    //HttpRuntime.Cache["RedirectFNOLXML"] = string.Empty;
                    HttpRuntime.Cache["RedirectPageXML"] = string.Empty;
                    ClientScript.RegisterStartupScript(this.GetType(), "closescript", "<script type='text/javascript'>window.opener.document.forms[0].submit();window.close();</script>");
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            //HttpRuntime.Cache["RedirectFNOLXML"] = string.Empty;
            HttpRuntime.Cache["RedirectPageXML"] = string.Empty;
            if (string.Compare(txtSaveOnCancel.Text, "save", true) == 0)
            {
                btnSave_Click(sender, e);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "closescript", "<script type='text/javascript'>window.opener.document.forms[0].submit();window.close();</script>");
            }
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {            
            try
            {
                XmlTemplate = GetMessageTemplate();
                CallCWS("FNOLReserveAdaptor.SaveResSetup", XmlTemplate, out sCWSresponse, false, false);
                XmlDocument objReturnXml = new XmlDocument();
                objReturnXml.LoadXml(sCWSresponse);
                string sMsgStatus = objReturnXml.SelectSingleNode("//MsgStatusCd").InnerText;
                if (string.Compare(sMsgStatus, "Success", true) == 0)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "reloadcript", "<script type='text/javascript'>window.location.href=window.location;</script>");
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }
        //protected void btnNext_Click(object sender, EventArgs e)
        //{
        //    StringBuilder sXml = new StringBuilder();
        //    XmlDocument xmlFNOLRserves = new XmlDocument();
        //    XmlDocument xmlDoc = new XmlDocument();
        //    XmlNode xmlNodeDoc = null;
        //    XmlNode xmlResSetup = null;
        //    XElement oElement = null;
        //    XmlElement xmlElem = null;
        //    string sRowId = string.Empty;
        //    //StringWriter sw = new StringWriter();
        //    //XmlTextWriter tx = new XmlTextWriter(sw);
        //    try
        //    {
        //        if (HttpRuntime.Cache["RedirectPageXML"] != null && Convert.ToString(HttpRuntime.Cache["RedirectFNOLXML"]) != "")
        //        {
        //            //xmlFNOLRserves = GetStoredFNOLRserves();
        //            xmlFNOLRserves = (XmlDocument)HttpRuntime.Cache["RedirectFNOLXML"];
        //            xmlResSetup = xmlFNOLRserves.SelectSingleNode("//FNOLResSetup");
        //            sXml = sXml.Append("<FNOLReserve>");
        //            sXml = sXml.Append("<control name='Policy' type='code'>");
        //            sXml = sXml.Append(cmbPolicy.SelectedValue);
        //            sXml = sXml.Append("</control>");
        //            sXml = sXml.Append("<control name='Unit' type='code'>");
        //            sXml = sXml.Append(cmbUnit.SelectedValue);
        //            sXml = sXml.Append("</control>");
        //            sXml = sXml.Append("<control name='Claimant' type='entity'>");
        //            sXml = sXml.Append(cmbClaimant.SelectedValue);
        //            sXml = sXml.Append("</control>");
        //            sXml = sXml.Append("<control name='CoverageType' type='code'>");
        //            sXml = sXml.Append(cmbCoverageType.SelectedValue);
        //            sXml = sXml.Append("</control>");
        //            sXml = sXml.Append("<control name='LossType' type='code'>");
        //            sXml = sXml.Append(cmbLossType.SelectedValue);
        //            sXml = sXml.Append("</control>");
        //            sXml = sXml.Append("</FNOLReserve>");
        //            oElement = XElement.Parse(sXml.ToString());
        //            //xmlDoc.Load(oElement.CreateReader());
        //            xmlElem = (XmlElement)xmlFNOLRserves.ReadNode(oElement.CreateReader());
        //            //xmlNodeDoc = xmlDoc.SelectSingleNode("FNOLReserve");
        //            xmlResSetup.AppendChild(xmlElem);                    
        //            //ViewState["FNOLRserves"] = xmlFNOLRserves;
        //            //xmlFNOLRserves.WriteTo(tx);
        //            //ViewState["FNOLRserves"] = sw.ToString();
        //            HttpRuntime.Cache["RedirectFNOLXML"] = xmlFNOLRserves;
        //            ClientScript.RegisterStartupScript(this.GetType(), "reloadcript", "<script type='text/javascript'>window.location.href=window.location;</script>");
        //        }
        //        else
        //        {
        //            if (mode.Text.ToLower() == "edit")
        //            {
        //                sRowId = AppHelper.GetQueryStringValue("selectedid");
        //            }
        //            sXml = sXml.Append("<Message>");
        //            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
        //            sXml = sXml.Append("<Call><Function></Function></Call><Document><FNOLResSetup>");
        //            sXml = sXml.Append("<control name='ClaimId' type='id'>");
        //            sXml = sXml.Append(txtClaimId.Text);
        //            sXml = sXml.Append("</control>");
        //            sXml = sXml.Append("<FNOLReserve>");
        //            sXml = sXml.Append("<control name='RowId' type='id'>");
        //            sXml = sXml.Append(sRowId);
        //            sXml = sXml.Append("</control>");
        //            sXml = sXml.Append("<control name='Policy' type='code'>");
        //            sXml = sXml.Append(cmbPolicy.SelectedValue);
        //            sXml = sXml.Append("</control>");
        //            sXml = sXml.Append("<control name='Unit' type='code'>");
        //            sXml = sXml.Append(cmbUnit.SelectedValue);
        //            sXml = sXml.Append("</control>");
        //            sXml = sXml.Append("<control name='Claimant' type='entity'>");
        //            sXml = sXml.Append(cmbClaimant.SelectedValue);
        //            sXml = sXml.Append("</control>");
        //            sXml = sXml.Append("<control name='CoverageType' type='code'>");
        //            sXml = sXml.Append(cmbCoverageType.SelectedValue);
        //            sXml = sXml.Append("</control>");
        //            sXml = sXml.Append("<control name='LossType' type='code'>");
        //            sXml = sXml.Append(cmbLossType.SelectedValue);
        //            sXml = sXml.Append("</control>");
        //            sXml = sXml.Append("</FNOLReserve>");
        //            sXml = sXml.Append("</FNOLResSetup></Document>");
        //            sXml = sXml.Append("</Message>");
        //            oElement = XElement.Parse(sXml.ToString());
        //            xmlFNOLRserves.Load(oElement.CreateReader());       
        //            HttpRuntime.Cache["RedirectFNOLXML"] = xmlFNOLRserves;
        //            ClientScript.RegisterStartupScript(this.GetType(), "reloadcript", "<script type='text/javascript'>window.location.href=window.location;</script>");
        //        }
        //    }
        //    catch (Exception ee)
        //    {
        //        ErrorHelper.logErrors(ee);
        //        BusinessAdaptorErrors err = new BusinessAdaptorErrors();
        //        err.Add(ee, BusinessAdaptorErrorType.SystemError);
        //        ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
        //    }

        //}


        private void PopulatePolicy(XmlDocument xmlDoc)
        {
            ListItem item = new ListItem();
            item.Text = "";
            item.Value = "";
            cmbPolicy.Items.Add(item);
            foreach (XmlElement ele in xmlDoc.SelectNodes("//policy"))
            {
                item = new ListItem();
                item.Text = ele.Attributes["name"].Value;
                item.Value = ele.Attributes["value"].Value;
                cmbPolicy.Items.Add(item);
            }
            if (xmlDoc.SelectNodes("//policy").Count == 1)
            {
                cmbPolicy.SelectedIndex = 1;
                cmbUnit.Enabled = true;
                cmbUnit.Items.Clear();
                //XmlNode nodePolicy = Model.SelectSingleNode("//policy[@value='" + cmbPolicy.SelectedValue + "']");
                item = new ListItem();
                item.Text = "";
                item.Value = "";
                cmbUnit.Items.Add(item);
                foreach (XmlElement ele in xmlDoc.SelectNodes("//policy/unit"))
                {
                    item = new ListItem();
                    item.Text = ele.Attributes["name"].Value;
                    item.Value = ele.Attributes["value"].Value;
                    cmbUnit.Items.Add(item);
                }
                cmbUnit.SelectedIndex = 1;

                cmbCoverageType.Enabled = true;
                cmbCoverageType.Items.Clear();
                item = new ListItem();
                item.Text = "";
                item.Value = "";
                cmbCoverageType.Items.Add(item);
                foreach (XmlElement ele in xmlDoc.SelectNodes("//policy[@value='" + cmbPolicy.SelectedValue + "']/unit[@value='" + cmbUnit.SelectedValue + "']/coverage"))
                {
                    item = new ListItem();
                    item.Text = ele.Attributes["name"].Value;
                    item.Value = ele.Attributes["value"].Value;
                    cmbCoverageType.Items.Add(item);
                }
            }
            else if (xmlDoc.SelectNodes("//policy").Count == 0)
            {
                cmbPolicy.Width = 100;
            }
        }

        private void PopulateClaimant(XmlDocument xmlDoc)
        {
            ListItem item = new ListItem();
            item.Text = "";
            item.Value = "";
            cmbClaimant.Items.Add(item);
            foreach (XmlElement elem in xmlDoc.SelectNodes("//Claimants/Claimant"))
            {
                item = new ListItem();
                item.Text = elem.SelectSingleNode("ClaimantName").InnerText;
                item.Value = elem.SelectSingleNode("ClaimantEID").InnerText;
                cmbClaimant.Items.Add(item);
            }
            if (xmlDoc.SelectNodes("//Claimants/Claimant").Count == 1)
            {
                cmbClaimant.SelectedIndex = 1;                
            }
            else if (xmlDoc.SelectNodes("//Claimants/Claimant").Count == 0)
            {
                cmbClaimant.Width = 100;
            }
        }

        protected void cmbPolicy_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmbUnit.Enabled = true;
            cmbUnit.Items.Clear();
            ListItem item = new ListItem();
            item.Text = "";
            item.Value = "";
            cmbUnit.Items.Add(item);
            foreach (XmlElement ele in Model.SelectNodes("//policy[@value='" + cmbPolicy.SelectedValue + "']/unit"))
            {
                item = new ListItem();
                item.Text = ele.Attributes["name"].Value;
                item.Value = ele.Attributes["value"].Value;
                cmbUnit.Items.Add(item);
            }

            if (Model.SelectNodes("//policy[@value='" + cmbPolicy.SelectedValue + "']/unit").Count == 1)
            {
                cmbUnit.SelectedIndex = 1;

                cmbCoverageType.Enabled = true;
                cmbCoverageType.Items.Clear();
                item = new ListItem();
                item.Text = "";
                item.Value = "";
                cmbCoverageType.Items.Add(item);
                foreach (XmlElement ele in Model.SelectNodes("//policy[@value='" + cmbPolicy.SelectedValue + "']/unit[@value='" + cmbUnit.SelectedValue + "']/coverage"))
                {
                    item = new ListItem();
                    item.Text = ele.Attributes["name"].Value;
                    item.Value = ele.Attributes["value"].Value;
                    cmbCoverageType.Items.Add(item);
                }
            }            
        }

        protected void cmbUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmbCoverageType.Enabled = true;
            cmbCoverageType.Items.Clear();
            //XmlNode nodePolicy = Model.SelectSingleNode("//policy[@value='" + cmbPolicy.SelectedValue + "']");
            ListItem item = new ListItem();
            item.Text = "";
            item.Value = "";
            cmbCoverageType.Items.Add(item);
            foreach (XmlElement ele in Model.SelectNodes("//policy[@value='" + cmbPolicy.SelectedValue + "']/unit[@value='" + cmbUnit.SelectedValue + "']/coverage"))
            {
                item = new ListItem();
                item.Text = ele.Attributes["name"].Value;
                item.Value = ele.Attributes["value"].Value;
                cmbCoverageType.Items.Add(item);
            }
            //if (Model.SelectNodes("//policy[@value='" + cmbPolicy.SelectedValue + "']/unit[@value='" + cmbUnit.SelectedValue + "']/coverage").Count == 0)
            //{
            //    cmbCoverageType.Width = 100;
            //}
        }
        protected void cmbCoverageType_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmbLossType.Enabled = true;
            cmbLossType.Items.Clear();
            //XmlNode nodePolicy = Model.SelectSingleNode("//policy[@value='" + cmbPolicy.SelectedValue + "']");
            ListItem item = new ListItem();
            item.Text = "";
            item.Value = "";
            cmbLossType.Items.Add(item);
            foreach (XmlElement ele in Model.SelectNodes("//policy[@value='" + cmbPolicy.SelectedValue + "']/unit[@value='" + cmbUnit.SelectedValue + "']/coverage[@value='" + cmbCoverageType.SelectedValue + "']/loss"))
            {
                item = new ListItem();
                item.Text = ele.Attributes["name"].Value;
                item.Value = ele.Attributes["value"].Value;
                cmbLossType.Items.Add(item);
            }
            //if (Model.SelectNodes("//policy[@value='" + cmbPolicy.SelectedValue + "']/unit[@value='" + cmbUnit.SelectedValue + "']/coverage[@value='" + cmbCoverageType.SelectedValue + "']/loss").Count == 0)
            //{
            //    cmbLossType.Width = 100;
            //}
        }
    }


}