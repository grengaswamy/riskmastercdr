﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FNOLAddReserve.aspx.cs" Inherits="Riskmaster.UI.FNOLClaimReserve.FNOLAddReserve"  ValidateRequest="false" %>
<%@ Register  TagPrefix="dg" TagName="UserControlDataGrid" Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove" xmlns:cul="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
  <head id="Head1" runat="server">
  <title>Claim FNOL Reserve Setup</title>
  <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js">{var i;}
</script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/supportscreens.js">        { var i; }</script>
        <script language="JavaScript" src="../../Scripts/WaitDialog.js">            { var i; }  </script>
  </head>
  <body>
    <form name="frmData" id="frmData" runat="server">
     <div id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
        <div class="toolBarButton" runat="server" id="div_save" xmlns="" xmlns:cul="remove">
            <asp:ImageButton runat="server" src="../../Images/tb_save_active.png" Width="28"
                Height="28" border="0" ID="save" AlternateText="Save" onmouseover="this.src='../../Images/tb_save_mo.png';this.style.zoom='110%'"
                onmouseout="this.src='../../Images/tb_save_active.png';this.style.zoom='100%'" OnClick="btnSave_Click" OnClientClick="FNOLAddRes_Save();" /></div>
    </div>
    <div>
        <uc1:ErrorControl ID="ErrorControl1" runat="server" />
        <div>
          <span>
            <dg:UserControlDataGrid runat="server" ID="FNOLClmResGrid" GridName="FNOLClmResGrid" GridTitle="Add FNOL Reserve" Target="/Document/FNOLResList" Ref="/Instance/Document/form//control[@name='FNOLClmResGrid']" Unique_Id="RowId" ShowRadioButton="true" Width="" Height="60%" hidenodes="|RowId|" HideButtons="Edit" ShowHeader="True" LinkColumn="" PopupWidth="400" PopupHeight="300" Type="GridAndButtons" RowDataParam="listrow"/>
          </span>
        </div>
        <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="_"/>
        <asp:TextBox style="display:none" runat="server" id="FNOLClmResSelectedId"  RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="FNOLClmResGrid_RowDeletedFlag"  RMXType="id" Text="false" />
       <asp:TextBox style="display:none" runat="server" id="FNOLClmResGrid_Action"  RMXType="id" />
       <asp:TextBox style="display:none" runat="server" id="FNOLClmResGrid_RowAddedFlag"  RMXType="id" Text="false"  />
       <asp:TextBox style="display:none" runat="server" id="txtClaimId" />
       <asp:TextBox style="display:none" runat="server" id="txtAction" />
      </div>
              <asp:HiddenField id="hdnClaimant"  value="<%$ Resources:lblClaimantDesc %>" runat="server"/>
    </form>
  </body>
</html>