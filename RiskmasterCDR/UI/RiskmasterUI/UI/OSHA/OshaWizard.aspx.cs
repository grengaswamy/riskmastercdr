﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.OSHA
{
    public partial class OshaWizard : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Acc_Set.Value == "true")
            {
                lblResult.Attributes.Add("Style", "display:block");
                optResult.Attributes.Add("Style", "display:block");
            }
            else
            {
                lblResult.Attributes.Add("Style", "display:none");
                optResult.Attributes.Add("Style", "display:none");
            }
            
            Button btnCancel = OshaWizardlist.FindControl("StartNavigationTemplateContainerID$StepCancelButton") as Button;
            btnCancel.Attributes.Add("OnClick", "CancelClick();return false;");
            Button btnFinish = OshaWizardlist.FindControl("StartNavigationTemplateContainerID$StepFinishButton") as Button;
            btnFinish.Attributes.Add("OnClick", "FinishClick();return false;");
            try
            {
                bool bReturnStatus = false;
                XElement XmlTemplate = null;
                string sreturnValue = "";

                if (!Page.IsPostBack)
                {
                    eventid.Text = Convert.ToString(Request.QueryString["eventid"]);
                    recordableflag.Text = Convert.ToString(Request.QueryString["recordableflag"]);
                    XmlTemplate = GetMessageTemplate(eventid.Text);
                    bReturnStatus = CallCWS("OSHAAdaptor.OshaRecordabilityWizard", XmlTemplate, out sreturnValue, false, false);

                    if (bReturnStatus)
                    {
                        XmlDocument XmlDoc = new XmlDocument();
                        XmlDoc.LoadXml(sreturnValue);
                        if (XmlDoc.SelectSingleNode("//PINames") != null)
                           pinames.Text = XmlDoc.SelectSingleNode("//PINames").InnerText;
                        else
                           pinames.Text = string.Empty;

                        if (XmlDoc.SelectSingleNode("//PIFlags") != null)
                           piflags.Text = XmlDoc.SelectSingleNode("//PIFlags").InnerText;
                         else
                           piflags.Text = string.Empty;

                        if (XmlDoc.SelectSingleNode("//Recommend") != null)
                           recommend.Text = XmlDoc.SelectSingleNode("//Recommend").InnerText;
                        else
                           recommend.Text = string.Empty;

                        if (XmlDoc.SelectSingleNode("//PitTracking") != null)
                           pitracking.Text = XmlDoc.SelectSingleNode("//PitTracking").InnerText;
                        else
                           pitracking.Text = string.Empty;

                        if (XmlDoc.SelectSingleNode("//RecordableFlag") != null)
                           recordableflag.Text = XmlDoc.SelectSingleNode("//RecordableFlag").InnerText;
                        //else //Commented else part for Mits 18621:Event OSHA Recordable flag is not determined by OSHA Recordability wizard
                            //recordableflag.Text = string.Empty;
                   }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            
        }

        public void SkipNextStep(object sender, WizardNavigationEventArgs e)
        {
            if (optAccident.SelectedIndex == 0)
            {
                Acc_Set.Value = "true" ;
            }
            switch (e.CurrentStepIndex)
            {
                case 0:
                    if (!(optAccident.SelectedIndex == 0))
                        OshaWizardlist.ActiveStepIndex = 3;
                    else if (optAccident.SelectedIndex == 0 && !(optResult.SelectedIndex == 2))
                        OshaWizardlist.ActiveStepIndex = 1;
                    else if (!(optPremise.SelectedIndex == 0))
                        OshaWizardlist.ActiveStepIndex = 2;
                    else
                        OshaWizardlist.ActiveStepIndex = 3;
                    break;
                case 1:
                    if (!(optPremise.SelectedIndex == 0))
                        OshaWizardlist.ActiveStepIndex = 2;
                    else
                        OshaWizardlist.ActiveStepIndex = 3;
                    break;
                case 2:
                    OshaWizardlist.ActiveStepIndex = 3;
                    break;
                case 3:
                    OshaWizardlist.ActiveStepIndex = 4;
                    if (!(optTracking.SelectedIndex == 0))
                    { 
                    }
                    break;
            }
            DisplayPanel(OshaWizardlist.ActiveStepIndex);   
        }
        public void SkipPrevStep(object sender, WizardNavigationEventArgs e)
        {

            if (Acc_Set.Value == "true")
            {
                lblResult.Attributes.Add("Style", "display:inline");
                optResult.Attributes.Add("Style", "display:inline");
            }
            else
            {
                lblResult.Attributes.Add("Style", "display:none");
                optResult.Attributes.Add("Style", "display:none");
            }
            switch (e.CurrentStepIndex) { 
                case 1:
                   
                    OshaWizardlist.ActiveStepIndex = 0;
                    break;
                case 2:
                    if(optAccident.SelectedIndex==0 && !(optResult.SelectedIndex==2))
                        OshaWizardlist.ActiveStepIndex = 1;
                    else
                        OshaWizardlist.ActiveStepIndex = 0;
                    break;
                case 3:
                    if(!(optAccident.SelectedIndex ==0))
                        OshaWizardlist.ActiveStepIndex = 0;
                    else if(!(optPremise.SelectedIndex ==0))
                        OshaWizardlist.ActiveStepIndex = 2;
                    else if(!(optAccident.SelectedIndex ==0) && !(optResult.SelectedIndex==2))
                        OshaWizardlist.ActiveStepIndex = 1;
                    else
                        OshaWizardlist.ActiveStepIndex = 0;
                    break;
            }
            DisplayPanel(OshaWizardlist.ActiveStepIndex); 
        }
        public void DisplayPanel( int newPanelID)
        {
            string m_iRecommend = recommend.Text; 
            var m_iPITracking = pitracking.Text;  
            string sLabel = "";
            int iTitle = 0;
            iTitle = newPanelID + 1;     
            if (newPanelID == 4)
                Page.Title = "OSHA Recordability Wizard - Finished";
            else {
                Page.Title = "OSHA Recordability Wizard - Step " + iTitle + " of 4";
            }


            switch (newPanelID) {
                case 0:
                   // document.forms[0].btnBack.disabled = true;
                    break;
                case 3:
                    GetRecommendation();
                    m_iRecommend = recommend.Text;   
                    if (m_iRecommend == "0") {
                        Page.Title  += " - Recordable";
                        sLabel = "<b>The OSHA Recordable Wizard has determined that this event is most " +
											    "likely not OSHA Recordable.<br><br>" +
											    "Press Next to verify the OSHA Recordable flag has not been set on " +
											    "your Riskmaster data.</b>";
                    }
                    else {
                        Page.Title += " - Not Recordable";
                        sLabel = "<b>The OSHA Recordable Wizard has determined that this event is most " +
											    "likely OSHA Recordable.<br><br>" +
											    "Press Next to verify the OSHA Recordable flag has been set on " +
											    "your Riskmaster data.</b>";
                    }
                    span4.Text = sLabel;
                    break;
                case 4:

                    sLabel = "";
                    if (m_iRecommend == "1") {
                        sLabel = "The wizard recommended this event be recordable.<br><br>" +
										    "Press the Finish button to exit the OSHA Recordable Wizard.";
                    }
                    else {
                        sLabel = "The wizard recommended this event is not recordable.<br><br>" +
										    "Press the Finish button to exit the OSHA Recordable Wizard.";
                    }
                    span51.Text = sLabel;
                    sLabel = "";
                    if (m_iPITracking == "") {
                        if (m_iRecommend != recordableflag.Text )
                            sLabel = "<b>The recordable flag for this event is not set according to the " +
											    "recommendations of this wizard.<br><br>" +
											    "If you determine the OSHA recordable flag should be set to the " +
											    "recommendations of this wizard, you are responsible for making the " +
											    "change.</b>";
                        else
                            sLabel = "<b>The recordable flag for this event is already set to the " +
											    "recommendations of this wizard.</b>";
                    }
                    else {
                        string sLastName=string.Empty;
			            string sFirstName=string.Empty;
			            string sPINames=string.Empty;
                        string[] sDelimiter=new string[]{"|^%'"};
                        string sPIFlags=string.Empty;
                        int index=0;
                        int count=0;
                        sPINames = pinames.Text;
                        string[] arPINames = sPINames.Split(sDelimiter, StringSplitOptions.None); 
                        sPIFlags = piflags.Text;
                        string[] arPIFlags = sPIFlags.Split(sDelimiter, StringSplitOptions.None);
                        count = arPINames.Length; 
                        for (index = 0; index < count; index++) {
                            if (m_iRecommend != arPIFlags[index]) {
                                sLabel += "<b>The recordable flag on the person involved record for " + arPINames[index] + " is not " +
												    "set according to the recommendations of this wizard.<br></b>";
                            }
                        }

                        if (sLabel == "") {
                            sLabel = "<b>The recordable flags for all person involved employees are " +
                                                "already set to the recommendations of this wizard.</b>";
                        }
                        else {
                            sLabel += "<br><br><b>If you determine the OSHA recordable flag should be set to the " +
											    "recommendations of this wizard, you are responsible for making the " +
											    "change.</b>";
                        }
                    }
                    span52.Text = sLabel;
                    break;
            }
        }
        public void GetRecommendation()
        {
            recommend.Text = "1";
            if (!(optAccident.SelectedIndex == 0))
                recommend.Text = "0";
            else if (!(optPremise.SelectedIndex == 0)) // not on premises
            {
                if ((!(optWorkRelated.SelectedIndex == 0)) && (!(optPresence.SelectedIndex == 0)) && (!(optTravel.SelectedIndex == 0)))
                    recommend.Text = "0";
            }
            else if (!(optResult.SelectedIndex == 2)) //death is not selected
            {
                if ((!(optMedical.SelectedIndex == 0)) && (!(optConsc.SelectedIndex == 0)) && (!(optRestrict.SelectedIndex == 0)) && (!(optTransfer.SelectedIndex == 0)))
                    recommend.Text = "0";
            }
        }

        private XElement GetMessageTemplate(string sEventID)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><OSHARecordWizard><EventId>");
            sXml = sXml.Append(sEventID);
            sXml = sXml.Append("</EventId></OSHARecordWizard></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
    }
}
