﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="OshaWizard.aspx.cs" Inherits="Riskmaster.UI.OSHA.OshaWizard" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>OSHA Recordability Wizard</title>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
    
</head>
<body>
<script type="text/javascript" language="javascript" >
    var m_winCancel = null;
    function AccidentClick(index) {
        var sDisplay = "";
        if (index == 0)
            sDisplay = "";
        else 
        {
            sDisplay = "none";
            document.forms[0].OshaWizardlist_optResult_0.checked = false;
            document.forms[0].OshaWizardlist_optResult_1.checked = false;
            document.forms[0].OshaWizardlist_optResult_2.checked = false;
        }
        window.OshaWizardlist_lblResult.style.display = sDisplay;
        window.OshaWizardlist_optResult.style.display = sDisplay;
       
    }
    function FinishClick() {
        window.close();
    }
    function CancelClick() {
        var windowWidth = 300;
        var windowHeight = 150;
        var locX = (screen.width - windowWidth) / 2;
        var locY = (screen.height - windowHeight) / 2;
        var windowFeatures = "width=" + windowWidth + ",height=" + windowHeight +
								",screenX=" + locX + ",screenY=" + locY + ",left=" + locX + ",top=" + locY;

        if ((m_winCancel != null) && !m_winCancel.closed) {
            m_winCancel.close();
        }
        m_winCancel = open("", "winConfirm", windowFeatures);
        var sHTML = '<HEAD><TITLE>Cancel Wizard</TITLE>'
								+ '<link rel="stylesheet" href="RMNet.css" type="text/css" /></HEAD>'
								+ '<BODY BGCOLOR="#FFFFFF">'
								+ '<CENTER>'
								+ 'Are you sure you want to cancel the OSHA Recordability Wizard?'
								+ '<FORM NAME="buttonForm">'
								+ '<INPUT TYPE="button" class="button" VALUE=" YES  "'
								+ ' ONCLICK="window.opener.buttonClicked(0);self.close();">'
								+ '&nbsp;&nbsp;&nbsp;'
								+ '<INPUT TYPE="button" class="button" VALUE="  NO  "'
								+ ' ONCLICK="window.opener.buttonClicked(1);self.close();">'
								+ '</FORM></BODY>';

        m_winCancel.document.writeln(sHTML);
    }
    function buttonClicked(buttonChoice) {
        if (buttonChoice == 0)
            window.close();
    }

						
    </script>
    <form id="frmData" runat="server">
    <uc3:ErrorControl ID="ErrorControl1" runat="server" />
    <asp:HiddenField ID="Acc_Set" runat="server" Value ="" /> 
    <asp:TextBox rmxref="/Instance/Document/EventId" ID="eventid" style="display:none" runat="server"/>
	<asp:TextBox rmxref="/Instance/Document/PitTracking" ID="pitracking" style="display:none" runat="server"/>
	<asp:TextBox rmxref="/Instance/Document/Recommend" ID="recommend" style="display:none" runat="server"/>
	<asp:TextBox rmxref="/Instance/Document/RecordableFlag" ID="recordableflag" style="display:none" runat="server"/>
	<asp:TextBox rmxref="/Instance/Document/PanelId" ID="panelid" style="display:none" runat="server"/>
	<asp:TextBox rmxref="/Instance/Document/PiFlags" ID="piflags" style="display:none" runat="server"/>
	<asp:TextBox rmxref="/Instance/Document/PiNames" ID="pinames"  style="display:none" runat="server"/>
	<asp:TextBox rmxref="/Instance/Document/submitflag" ID="submitflag"  style="display:none" runat="server"/>
    <div>
        <asp:Wizard ID="OshaWizardlist" runat="server"  DisplaySideBar="false"  
            OnNextButtonClick="SkipNextStep" OnPreviousButtonClick="SkipPrevStep" 
            ActiveStepIndex="0"  >
          <StartNavigationTemplate>
              <asp:Button class="button" ID="StepCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" OnClientClick="CancelClick();return false;" />
              <asp:Button class="button" ID="StepPreviousButton" disabled="true" runat="server" CausesValidation="True" CommandName="MovePrevious" Text="Previous" /> 
              <asp:Button class="button" ID="StepNextButton" runat="server" CommandName="MoveNext" Text="Next" CausesValidation="True" /> 
              <asp:Button class="button" ID="StepFinishButton" disabled="true" runat="server" CommandName="Finish" Text="Finish" CausesValidation="True" OnClientClick="FinishClick();return false;" /> 
          </StartNavigationTemplate>  
          <StepNavigationTemplate>
              <asp:Button class="button" ID="StepCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" OnClientClick="CancelClick();return false;" />
              <asp:Button class="button" ID="StepPreviousButton" runat="server" CausesValidation="True" CommandName="MovePrevious" Text="Previous" /> 
              <asp:Button class="button" ID="StepNextButton" runat="server" CommandName="MoveNext" Text="Next" CausesValidation="True" /> 
             <asp:Button class="button" ID="StepFinishButton" disabled="true" runat="server" CommandName="Finish" Text="Finish" CausesValidation="True" OnClientClick="FinishClick();return false;"/> 
           </StepNavigationTemplate>
           <FinishNavigationTemplate>
              <asp:Button class="button" ID="StepCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" OnClientClick="CancelClick();return false;" />
              <asp:Button class="button" ID="StepPreviousButton" disabled="true" runat="server" CausesValidation="True" CommandName="MovePrevious" Text="Previous" /> 
              <asp:Button class="button" ID="StepNextButton" disabled="true" runat="server" CommandName="MoveNext" Text="Next" CausesValidation="True" /> 
            <asp:Button class="button" ID="StepFinishButton" runat="server" CommandName="Finish" Text="Finish" CausesValidation="True" OnClientClick="FinishClick();return false;"/> 
           </FinishNavigationTemplate> 
          <WizardSteps>
                <asp:WizardStep ID="WizardStep1" runat="server" Title="OSHA Recordability Wizard -  Step 1"  >
                    <table>
						<tr>
							<td>
								<img src="../../Images/oshaWizard.gif" alt="oshaWizard" />
							</td>
							<td>
								<span>The OSHA recordable wizard will ask you a few questions to help 
								you perform a preliminary assessment on the OSHA recordable status. Please 
								select the appropriate answer and choose Next when you are finished.</span>
								<br />
								<br />
								<span>Note: Users are responsible for reviewing OSHA compliance 
								requirements and should not rely on this wizard as a primary determinate for 
								OSHA recordability.</span>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<hr />
							</td>
						</tr>
						<tr>
							<td colspan="2">
							Do you track the OSHA recordable information based on the event (default) or based on the person involved?
							</td>
						</tr>
						<tr>
							<td colspan="2">
							    <asp:RadioButtonList ID="optTracking"  runat="server" RepeatDirection="Horizontal">
							        <asp:ListItem Value="Event" >Event </asp:ListItem>
							        <asp:ListItem Value="Person Involved" >Person Involved</asp:ListItem>							
						        </asp:RadioButtonList>
							</td>
						</tr>
						<tr>
							<td colspan="2"></td>
						</tr>
						<tr>
							<td colspan="2">
								Did the event or exposure occur on the employer's premises?
							</td>
						</tr>
						<tr>
							<td colspan="2">
							    <asp:RadioButtonList ID="optPremise"  runat="server" RepeatDirection="Horizontal">
							        <asp:ListItem Value="Yes" >Yes&#160;&#160;&#160;</asp:ListItem>
							        <asp:ListItem Value="No" >No</asp:ListItem>							
						        </asp:RadioButtonList>
							</td>
						</tr>
						<tr>
							<td colspan="2"></td>
						</tr>
						<tr>
							<td colspan="2">
							Did the event or exposure result in an injury, illness or death?
							</td>
						</tr>
						<tr>
							<td colspan="2">
							    <asp:RadioButtonList ID="optAccident"  runat="server" RepeatDirection="Horizontal">
							        <asp:ListItem Value="0" onclick="AccidentClick(0);">Yes </asp:ListItem>
							        <asp:ListItem Value="1" onclick="AccidentClick(1);">No</asp:ListItem>							
						        </asp:RadioButtonList>
							</td>
						</tr>
						<tr id="trWhichOne" >
							<td width="2%"></td>
							<td colspan="">
							<asp:Label ID ="lblResult" Text ="	Which One?" runat ="server" style="display:none"/>
							</td>
						</tr>

						<tr id="trWhichOneOpt">
							<td width="2%"></td>
							    <td colspan="">
							        <asp:RadioButtonList ID="optResult"  runat="server"  style="display:none" RepeatDirection="Horizontal">
							                <asp:ListItem Value="Injury" >Injury </asp:ListItem>
							                <asp:ListItem Value="Illness" >Illness </asp:ListItem>	
							                <asp:ListItem Value="Death" >Death </asp:ListItem>						
						            </asp:RadioButtonList>

						        </td>
						</tr>
					</table>
                </asp:WizardStep>
                <asp:WizardStep ID="WizardStep2" runat="server" Title="OSHA Recordability Wizard -  Step 2">
                    <table>
	                    <tr>
		                    <td>
			                    <img src="../../Images/oshaWizard.gif" alt="oshaWizard"/>
		                    </td>
		                    <td>
			                    <span class="tx">Please answer the following questions based on your report that 
			                    an injury or illness occurred. Choose Next when you are finished, or Back to 
			                    change your answers.</span>
		                    </td>
	                    </tr>
	                    <tr>
		                    <td colspan="2"><hr /></td>
	                    </tr>
	                    <tr>
		                    <td colspan="2">
			                    Was medical treatment more than just first aid involved?
		                    </td>
	                    </tr>
	                    <tr>
		                    <td colspan="2" >
		                        <asp:RadioButtonList ID="optMedical"  runat="server" RepeatDirection="Horizontal">
		                            <asp:ListItem Value="Yes" >Yes </asp:ListItem>
		                            <asp:ListItem Value="No" >No</asp:ListItem>	
    	                        </asp:RadioButtonList>
                    		</td>
	                    </tr>
	                    <tr>
		                    <td colspan="2"></td>
	                    </tr>
	                    <tr>
		                    <td colspan="2">
			                    Did the employee lose consciousness?
		                    </td>
	                    </tr>
	                    <tr>
		                    <td colspan="2">
		                        <asp:RadioButtonList ID="optConsc"  runat="server" RepeatDirection="Horizontal">
		                            <asp:ListItem Value="Yes" >Yes </asp:ListItem>
		                            <asp:ListItem Value="No" >No</asp:ListItem>	
    	                        </asp:RadioButtonList>
		                    </td>
	                    </tr>
	                    <tr>
		                    <td colspan="2"></td>
	                    </tr>
	                    <tr>
		                    <td colspan="2">
			                    Does the employee have any restriction limiting job duties?
		                    </td>
	                    </tr>
	                    <tr>
		                    <td colspan="2">
		                        <asp:RadioButtonList ID="optRestrict"  runat="server" RepeatDirection="Horizontal">
		                            <asp:ListItem Value="Yes" >Yes </asp:ListItem>
		                            <asp:ListItem Value="No" >No</asp:ListItem>	
    	                        </asp:RadioButtonList>
                        	</td>
	                    </tr>
	                    <tr>
		                    <td colspan="2"></td>
	                    </tr>
	                    <tr>
		                    <td colspan="2">
			                    Does the injury or illness require transfer to another job?
		                    </td>
	                    </tr>
	                    <tr>
		                    <td colspan="2">
		                        <asp:RadioButtonList ID="optTransfer"  runat="server" RepeatDirection="Horizontal">
		                            <asp:ListItem Value="Yes" >Yes </asp:ListItem>
		                            <asp:ListItem Value="No" >No</asp:ListItem>	
    	                        </asp:RadioButtonList>
                    		</td>
	                    </tr>
	                    </table>
                </asp:WizardStep>
                 <asp:WizardStep ID="WizardStep3" runat="server" Title="OSHA Recordability Wizard -  Step 3">
                     <table>
	                    <tr>
		                    <td>
			                    <img src="../../Images/oshaWizard.gif" alt="oshaWizard"/>
		                    </td>
		                    <td>
			                    <span class="tx">Please answer the following questions based on your report that 
			                    the event or exposure did not occur on premises. Choose Next when you are 
			                    finished, or Back to change your answers.</span>
		                    </td>
	                    </tr>
	                    <tr>
		                    <td colspan="2"><hr /></td>
	                    </tr>
	                    <tr>
		                    <td colspan="2">
			                    Was the employee engaged in a work related activity?
		                    </td>
	                    </tr>
	                    <tr>
		                    <td colspan="2">
		                        <asp:RadioButtonList ID="optWorkRelated"  runat="server" RepeatDirection="Horizontal">
		                            <asp:ListItem Value="Yes" >Yes </asp:ListItem>
		                            <asp:ListItem Value="No" >No</asp:ListItem>	
    	                        </asp:RadioButtonList>
                    		</td>
	                    </tr>
	                    <tr>
		                    <td colspan="2"></td>
	                    </tr>
	                    <tr>
		                    <td colspan="2">
			                    Was the employee presence at the location a condition or required as part of their employment?
		                    </td>
	                    </tr>
	                    <tr>
		                    <td colspan="2">
		                        <asp:RadioButtonList ID="optPresence"  runat="server" RepeatDirection="Horizontal">
		                            <asp:ListItem Value="Yes" >Yes </asp:ListItem>
		                            <asp:ListItem Value="No" >No</asp:ListItem>	
    	                        </asp:RadioButtonList>
                    		</td>
	                    </tr>
	                    <tr>
		                    <td colspan="2"></td>
	                    </tr>
	                    <tr>
		                    <td colspan="2">
			                    Was the employee traveling and engaged in work or work related travel functions?
		                    </td>
	                    </tr>
	                    <tr>
		                    <td colspan="2">
		                        <asp:RadioButtonList ID="optTravel"  runat="server" RepeatDirection="Horizontal">
		                            <asp:ListItem Value="Yes" >Yes </asp:ListItem>
		                            <asp:ListItem Value="No" >No</asp:ListItem>	
    	                        </asp:RadioButtonList>
                    		</td>
	                    </tr>
	                    </table>
                </asp:WizardStep>
                 <asp:WizardStep ID="WizardStep4" runat="server" Title="OSHA Recordability Wizard -  Step 4">
                     <table>
	                    <tr>
		                    <td>
			                    <img src="../../Images/oshaWizard.gif" alt="oshaWizard"/>
		                    </td>
		                    <td>
			                    <span>Please press the Next button to begin the OSHA Recordable 
			                    analysis on your Riskmaster data or press Back to change your answers.</span>
		                    </td>
	                    </tr>
	                    <tr>
		                    <td colspan="2"><hr /></td>
	                    </tr>
	                    <tr>
		                    <td colspan="2">
			                    <asp:Label ID="span4" runat="server" />
		                    </td>
	                    </tr>
                    </table>
                </asp:WizardStep>
                 <asp:WizardStep ID="WizardStep5" runat="server" Title="OSHA Recordability Wizard - Finished">
                     <table>
	                    <tr>
		                    <td>
			                    <img src="../../Images/oshaWizard.gif" alt="oshaWizard"/>
		                    </td>
		                    <td>
		                    
		                    <asp:Label ID="span51" runat="server" />
		                    </td>
	                    </tr>
	                    <tr>
		                    <td colspan="2"><hr /></td>
	                    </tr>
	                    <tr>
		                    <td colspan="2">              
			                    <asp:Label id="span52" runat ="server" />               
		                    </td>
	                    </tr>
                    </table>
                </asp:WizardStep>
            </WizardSteps>
        </asp:Wizard>
    </div>
    </form>
</body>
</html>
