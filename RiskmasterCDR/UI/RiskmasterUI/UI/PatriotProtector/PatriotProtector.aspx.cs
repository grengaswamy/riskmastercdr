﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.PatriotProtector
{
    public partial class PatriotProtector : NonFDMBasePageCWS 
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                bool bReturnStatus = false;
                XElement XmlTemplate = null;
                string sreturnValue = "";
                XmlNode xNode;
                DataTable objTable = new DataTable();
                DataRow objRow;
                AdvertiseOnly.Value = "0";
                MatchedResult.Value = "0";
                NoDBServiceOutput.Value = "0";
                DBServiceOutputValue.Value = "0";


                if (!IsPostBack)
                {
                    lastname.Value = Request.QueryString["lastname"];
                    firstname.Value = Request.QueryString["firstname"];
                    XmlTemplate = GetMessageTemplate(lastname.Value, firstname.Value);
                    bReturnStatus = CallCWS("PatriotProtectorWrapperAdaptor.Lookup", XmlTemplate, out sreturnValue, false, false);
                    if (bReturnStatus)
                    {
                        XmlDocument XmlDoc = new XmlDocument();
                        XmlDoc.LoadXml(sreturnValue);
                        //xNode = XmlDoc.SelectSingleNode("//AdvertiseOnly");
                        if (XmlDoc.SelectSingleNode("//AdvertiseOnly") != null)
                        {
                            AdvertiseOnly.Value = "1";
                        }
                        else
                            if (XmlDoc.SelectSingleNode("//PATRIOTPROTECTOR/NewDataSet/ofac/*") != null)
                            {
                                MatchedResult.Value = "1";
                                XmlNodeList ofaclist = XmlDoc.SelectNodes("//PATRIOTPROTECTOR/NewDataSet/ofac");
                                objTable.Columns.Add("Name");
                                objTable.Columns.Add("Type");
                                objTable.Columns.Add("Alternate");
                                objTable.Columns.Add("AlternateType");
                                objTable.Columns.Add("Address");
                                objTable.Columns.Add("Country");
                                objTable.Columns.Add("City");
                                objTable.Columns.Add("Remarks");
                                for (int i = 0; i < ofaclist.Count; i++)
                                {
                                    xNode = ofaclist[i];
                                    objRow = objTable.NewRow();

                                    foreach (XmlNode childnode in xNode.ChildNodes)
                                    {
                                        switch (childnode.Name)
                                        {
                                            case "IDS_NAME":
                                                objRow["Name"] = childnode.InnerText;
                                                break;
                                            case "IDS_SDN_TYPE":
                                                objRow["Type"] = childnode.InnerText;
                                                break;
                                            case "IDS_ALT":
                                                objRow["Alternate"] = childnode.InnerText;
                                                break;
                                            case "IDS_ALT_TYPE":
                                                objRow["AlternateType"] = childnode.InnerText;
                                                break;
                                            case "IDS_ADDRESS":
                                                objRow["Address"] = childnode.InnerText;
                                                break;
                                            case "IDS_COUNTRY":
                                                objRow["Country"] = childnode.InnerText;
                                                break;
                                            case "IDS_CITY":
                                                objRow["City"] = childnode.InnerText;
                                                break;
                                            case "IDS_REMARKS":
                                                objRow["Remarks"] = childnode.InnerText;
                                                break;
                                        }
                                    }
                                   objTable.Rows.Add(objRow);

                                }
                                objTable.AcceptChanges();
                                GridViewResult.Visible = true;
                                GridViewResult.DataSource = objTable;
                                GridViewResult.DataBind();
                            }
                            else if (XmlDoc.SelectSingleNode("//PATRIOTPROTECTOR/DBServiceOutputInfo") == null)
                            {
                                NoDBServiceOutput.Value = "1";  
                            }
                            else if (XmlDoc.SelectSingleNode("//PATRIOTPROTECTOR/DBServiceOutputInfo/ReturnCode") != null)       
                            {
                                xNode = XmlDoc.SelectSingleNode("//PATRIOTPROTECTOR/DBServiceOutputInfo/ReturnCode");
                                if (xNode.InnerText != "100" && xNode.InnerText != "0")
                                {
                                    DBServiceOutputValue.Value = xNode.InnerText; 
                                }
                                xNode = XmlDoc.SelectSingleNode("//PATRIOTPROTECTOR/DBServiceOutputInfo/ReturnMessage");
                                ReturnMessage.Value = xNode.InnerText;  
                            }
                            else 
                            {
                                DBServiceOutputValue.Value = "1"; 
                            }
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

  

        }
        private XElement GetMessageTemplate(string sLname, string sFname)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><PatriotProtector><LastName>");
            sXml = sXml.Append(sLname);
            sXml = sXml.Append("</LastName><FirstName>");
            sXml = sXml.Append(sFname);
            sXml = sXml.Append("</FirstName></PatriotProtector></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
        
    }
}
