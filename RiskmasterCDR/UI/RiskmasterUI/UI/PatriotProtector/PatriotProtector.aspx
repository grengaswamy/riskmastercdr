﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="PatriotProtector.aspx.cs" Inherits="Riskmaster.UI.PatriotProtector.PatriotProtector" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Specially Designated Nationals List Check</title>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
</head>
<body>
    <form id="form1" runat="server">
     <uc3:ErrorControl ID="ErrorControl1" runat="server" />
        <asp:HiddenField ID ="AdvertiseOnly" runat ="server" />
        <asp:HiddenField ID ="MatchedResult" runat ="server" />
        <asp:HiddenField ID ="firstname" runat ="server" />
        <asp:HiddenField ID="lastname" runat ="server" /> 
        <asp:HiddenField ID="NoDBServiceOutput" runat ="server" /> 
        <asp:HiddenField ID="DBServiceOutputValue" runat ="server" /> 
        <asp:HiddenField ID="ReturnMessage" runat ="server" /> 
        
        <%if (AdvertiseOnly.Value == "1")%>
    <%{ %>
        <p class="formtitle">Specially Designated Nationals List Check</p>
        This add-on electronically checks the payee information for this payment against the United States Treasury's Office of Foreign Assets Control(OFAC) <a class="LightBold" href="http://www.ustreas.gov/offices/eotffc/ofac/sdn/index.html">
							  <u>Specially Designated Nationals(SDN) List</u>
						  </a> to determine if he\she\they may be a Specially Designated National.  <br></br><b>Contact your RISKMASTER account manager today about adding this functionality.</b>  	
					  <br /><br />
    <%} %>
    <%if (MatchedResult.Value == "1")%>
    <%{ %>
    Checking the payee information [<b><%=firstname.Value %>&#160;<%=lastname.Value%></b>] for this payment against the United States Treasury's Office of Foreign Assets Control(OFAC) <a class="LightBold" href="http://www.ustreas.gov/offices/eotffc/ofac/sdn/index.html">
									<u>Specially Designated Nationals(SDN) List</u>
								</a> indicates that  <b><%=firstname.Value%>&#160;<%=lastname.Value%></b> may be a Specially Designated National.  
    <table width="100%" border="0" cellspacing="0" cellpadding="3">
		<tr>
			<td class="ctrlgroup2" colspan="8">SDN List Matches</td>
		</tr>
	</table> 	
    <asp:GridView ID="GridViewResult" AutoGenerateColumns="False" runat="server" Font-Bold="True" 
		CellPadding="0" GridLines="None" CellSpacing="2" Width="100%" HeaderStyle-BackColor="DarkGray">
		<rowstyle CssClass ="rowlight1" HorizontalAlign="Left" Font-Bold="false"  />
        <alternatingrowstyle CssClass="rowdark1" HorizontalAlign="Left" Font-Bold="false" />

        <Columns>
            <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="colheader3" >
                <ItemTemplate>
                   <%# Eval("Name")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Type" HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="colheader3" >
                <ItemTemplate>
                   <%# Eval("Type")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Alternate" HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="colheader3" >
                <ItemTemplate>
                   <%# Eval("Alternate")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Alternate Type" HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="colheader3" >
                <ItemTemplate>
                   <%# Eval("AlternateType")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Address" HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="colheader3" >
                <ItemTemplate>
                   <%# Eval("Address")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Country" HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="colheader3" >
                <ItemTemplate>
                   <%# Eval("Country")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="City" HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="colheader3" >
                <ItemTemplate>
                   <%# Eval("City")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Remarks" HeaderStyle-HorizontalAlign="Left" ControlStyle-CssClass="colheader3" >
                <ItemTemplate>
                   <%# Eval("Remarks")%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns> 
    </asp:GridView>     
   <%} %>
    <%else if (NoDBServiceOutput.Value == "1")%>
    <%{ %>
    <p class="errtext">There has been a technical problem during the OFAC SDN check for <b><%=firstname.Value %>&#160;<%=lastname.Value %></b>.  No results are available.</p>  <br />
									Troubleshooting information follows:<br />
    <%} %>
    <%else if (DBServiceOutputValue.Value != "0" && DBServiceOutputValue.Value != "100")%>
    <%{ %>
    <p class="errtext">There has been a technical problem during the OFAC SDN check for <b><%=firstname.Value %>&#160;<%=lastname.Value %></b>.  No results are available.</p>  <br />
								Troubleshooting information follows:<br />
	 <%=DBServiceOutputValue.Value%>  <%=ReturnMessage.Value%><br />							
    <%} %>
    <%else %>
    <%{ %>
    Payee [<b><%=firstname.Value %>&#160;<%=lastname.Value %></b>] is not listed in the United States Treasury's Office of Foreign Assets Control(OFAC) <a class="LightBold" href="http://www.ustreas.gov/offices/enforcement/ofac/sdn/index.shtml">
									<u>Specially Designated Nationals(SDN) List</u>
								</a>. <br />This indicates that <b><%=firstname.Value %>&#160;<%=lastname.Value %></b> is not known to be a Specially Designated National at this time.  

    <%} %>
        <center>
		    <form>
			    <input class="button" type="button" onclick="window.close();" value="  OK  " />
		    </form>
	    </center>
    </form>
</body>
</html>
