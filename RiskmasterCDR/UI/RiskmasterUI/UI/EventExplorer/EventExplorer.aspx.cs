﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Riskmaster.UI.EventExplorer
{
    public partial class EventExplorer : System.Web.UI.Page
    {
        private string m_Lob = "";
        private string m_ClaimId = "";
        private string m_EventId = "";
        public string Lob
        {
            get
            {
                return m_Lob;
            }
        }
        public string ClaimId
        {
            get
            {
                return m_ClaimId;
            }
        }
        public string EventId
        {
            get
            {
                return m_EventId;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            m_Lob = AppHelper.GetQueryStringValue("Lob");
            m_ClaimId = AppHelper.GetQueryStringValue("ClaimId");
            m_EventId = AppHelper.GetQueryStringValue("EventId");
        }
    }
}
