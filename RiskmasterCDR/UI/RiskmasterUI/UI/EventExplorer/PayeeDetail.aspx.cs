﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.UI.CommonWCFService;
using System.IO;
using Riskmaster.Common;
namespace Riskmaster.UI.EventExplorer
{
    public partial class PayeeDetail : System.Web.UI.Page
    {
        private string m_strSortExp = "";
        private string m_SortDirection = "";
        public string Total = "";
        string GetPayeeDetailMessageTemplate = "<Message><Authorization></Authorization><Call><Function>EventExplorerAdaptor.GetPayeeDetails"+
            "</Function></Call><Document><PayeeDetail><ClaimId></ClaimId><PayeeId></PayeeId><SortCol></SortCol>"+
            "<Ascending></Ascending></PayeeDetail></Document></Message>";
        protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
        {
            // There seems to be a bug in GridView sorting implementation. Value of
            // SortDirection is always set to "Ascending". Now we will have to play
            // little trick here to switch the direction ourselves.
            try
            {
                if (String.Empty != m_strSortExp)
                {
                    if (String.Compare(e.SortExpression, m_strSortExp, true) == 0)
                    {
                        m_SortDirection =
                            (m_SortDirection == "0") ? "1" : "0";
                    }
                }
                ViewState["_Direction_"] = m_SortDirection;
                ViewState["_SortExp_"] = m_strSortExp = e.SortExpression;
                this.BindData();
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }				
        private void BindData()
        {
            GetPayeeDetailMessageTemplate = AppHelper.ChangeMessageValue(GetPayeeDetailMessageTemplate, "//ClaimId", AppHelper.GetQueryStringValue("claimid"));
            GetPayeeDetailMessageTemplate = AppHelper.ChangeMessageValue(GetPayeeDetailMessageTemplate, "//PayeeId", AppHelper.GetQueryStringValue("payeeid"));
            GetPayeeDetailMessageTemplate = AppHelper.ChangeMessageValue(GetPayeeDetailMessageTemplate, "//SortCol", m_strSortExp);
            GetPayeeDetailMessageTemplate = AppHelper.ChangeMessageValue(GetPayeeDetailMessageTemplate, "//Ascending", m_SortDirection);
            string sReturn = AppHelper.CallCWSService(GetPayeeDetailMessageTemplate);
            XmlDocument Model = new XmlDocument();
            Model.LoadXml(sReturn);
            DataTable dt = new DataTable();
            DataColumn dc = new DataColumn();
            dc.ColumnName = "CtlNumber";
            dt.Columns.Add(dc);

            dc = new DataColumn();
            dc.ColumnName = "TransNumber";
            dt.Columns.Add(dc);

            dc = new DataColumn();
            dc.ColumnName = "TransDate";
            dt.Columns.Add(dc);

            dc = new DataColumn();
            dc.ColumnName = "Amount";
            dt.Columns.Add(dc);

            dc = new DataColumn();
            dc.ColumnName = "Date";
            dt.Columns.Add(dc);

            dc = new DataColumn();
            dc.ColumnName = "InvoiceNumber";
            dt.Columns.Add(dc);

            dc = new DataColumn();
            dc.ColumnName = "CodeDesc";
            dt.Columns.Add(dc);

            dc = new DataColumn();
            dc.ColumnName = "SplitAmount";
            dt.Columns.Add(dc);

            foreach (XmlElement el in Model.SelectNodes("//row"))
            {
                DataRow dr = dt.NewRow();
                dr["CtlNumber"] = el.SelectSingleNode("CtlNumber").InnerText;
                dr["TransNumber"] = el.SelectSingleNode("TransNumber").InnerText;
                dr["TransDate"] = el.SelectSingleNode("TransDate").InnerText;
                dr["Amount"] = el.SelectSingleNode("Amount").InnerText;
                dr["Date"] = el.SelectSingleNode("Date").InnerText;
                dr["InvoiceNumber"] = el.SelectSingleNode("InvoiceNumber").InnerText;
                dr["CodeDesc"] = el.SelectSingleNode("CodeDesc").InnerText;
                dr["SplitAmount"] = el.SelectSingleNode("SplitAmount").InnerText;
                dt.Rows.Add(dr);
            }

            Total = Model.SelectSingleNode("//TotalAll").InnerText;
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        protected void OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                if (String.Empty != m_strSortExp)
                {
                    AddSortImage(e.Row);
                }
            }
        }
        private int GetSortColumnIndex(String strCol)
        {
            foreach (DataControlField field in GridView1.Columns)
            {
                if (field.SortExpression == strCol)
                {
                    return GridView1.Columns.IndexOf(field);
                }
            }

            return -1;
        }
        void AddSortImage(GridViewRow headerRow)
        {
            Int32 iCol = GetSortColumnIndex(m_strSortExp);
            if (-1 == iCol)
            {
                return;
            }
            // Create the sorting image based on the sort direction.
            Image sortImage = new Image();
            if ("1" == m_SortDirection)
            {
                sortImage.ImageUrl = "~/Images/arrow_down_white.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "~/Images/arrow_up_white.gif";
                sortImage.AlternateText = "Descending Order";
            }

            // Add the image to the appropriate header cell.
            headerRow.Cells[iCol].Controls.Add(sortImage);
        }
      
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    m_strSortExp = "FUNDS.CTL_NUMBER";
                    m_SortDirection = "0";
                    ViewState["_Direction_"] = m_SortDirection;
                    ViewState["_SortExp_"] = m_strSortExp;

                    BindData();
                 
                 
                }
                if (ViewState["_SortExp_"] != null)
                {
                    m_strSortExp = ViewState["_SortExp_"].ToString();
                    m_SortDirection = ViewState["_Direction_"].ToString();
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
          
          
        }
    }
}
