﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using Riskmaster;
using Riskmaster.BusinessAdaptor.Common;
using System.Collections;
using Riskmaster.UI.CommonWCFService;
using System.IO;
using Riskmaster.Common;

namespace Riskmaster.UI.EventExplorer
{
    public partial class EventExplorerTree : System.Web.UI.Page
    {
        public string Display = "";
        string GetTreeMessageTemplate = "<Message><Authorization></Authorization><Call>"+
            "<Function>EventExplorerAdaptor.GetEventSummaryXml</Function></Call><Document><EventExplorer><EventId>1</EventId>"+
            "<ClaimId>1</ClaimId><Lob>241</Lob></EventExplorer></Document></Message>";
        
        protected void Page_Load(object sender, EventArgs e)
        {
            XslCompiledTransform xslt = null;
            StringWriter sw = new StringWriter();
            try
            {
                GetTreeMessageTemplate = AppHelper.ChangeMessageValue(GetTreeMessageTemplate, "//EventExplorer/EventId", AppHelper.GetQueryStringValue("EventId"));
                GetTreeMessageTemplate = AppHelper.ChangeMessageValue(GetTreeMessageTemplate, "//EventExplorer/Lob", AppHelper.GetQueryStringValue("Lob"));
                GetTreeMessageTemplate = AppHelper.ChangeMessageValue(GetTreeMessageTemplate, "//EventExplorer/ClaimId", AppHelper.GetQueryStringValue("ClaimId"));
                string sReturn = AppHelper.CallCWSService(GetTreeMessageTemplate);
                XmlDocument Model = new XmlDocument();
                Model.LoadXml(sReturn);
                xslt = new XslCompiledTransform();
                xslt.Load(Server.MapPath("./Xsl/Tree.xsl"));
                // Transform the file.
                xslt.Transform(Model, null, sw);
                Display = sw.ToString();
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
    }
}
