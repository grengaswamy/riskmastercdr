﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using Riskmaster;
using Riskmaster.BusinessAdaptor.Common;
using System.Collections;
using Riskmaster.UI.CommonWCFService;
using System.IO;
using Riskmaster.Common;
namespace Riskmaster.UI.EventExplorer
{
    public partial class EventExplorerData : System.Web.UI.Page
    {
        public string Display = "";
        string GetDataMessageTemplate = "<Message><Authorization></Authorization><Call>"+
            "<Function>EventExplorerAdaptor.GetData</Function></Call><Document><EventExplorer><EventId>1</EventId><Key />"+
            "<ClaimId>1</ClaimId></EventExplorer></Document></Message>";
        protected void Page_Load(object sender, EventArgs e)
        {
            XslCompiledTransform xslt = null;
            StringWriter sw = new StringWriter();
            try
            {
                if (AppHelper.GetFormValue("Key") == "")
                {
                    GetDataMessageTemplate = AppHelper.ChangeMessageValue(GetDataMessageTemplate, "//EventExplorer/EventId", AppHelper.GetQueryStringValue("EventId"));
                    GetDataMessageTemplate = AppHelper.ChangeMessageValue(GetDataMessageTemplate, "//EventExplorer/Key", AppHelper.GetQueryStringValue("Key"));
                    GetDataMessageTemplate = AppHelper.ChangeMessageValue(GetDataMessageTemplate, "//EventExplorer/ClaimId", AppHelper.GetQueryStringValue("ClaimId"));
                }
                else
                {
                    GetDataMessageTemplate = AppHelper.ChangeMessageValue(GetDataMessageTemplate, "//EventExplorer/EventId", AppHelper.GetFormValue("EventId"));
                    GetDataMessageTemplate = AppHelper.ChangeMessageValue(GetDataMessageTemplate, "//EventExplorer/Key", AppHelper.GetFormValue("Key"));
                }
                //tmalhotra2 MITS-29787 Re# 5.1.13
                if (AppHelper.GetQueryStringValue("ClaimId") != string.Empty)
                {
                    GetDataMessageTemplate = AppHelper.ChangeMessageValue(GetDataMessageTemplate, "//EventExplorer/ClaimId", AppHelper.GetQueryStringValue("ClaimId"));
                }
                string sNoteKey = (AppHelper.GetFormValue("Key") == "") ? AppHelper.GetQueryStringValue("Key") : AppHelper.GetFormValue("Key");
                string sReturn = AppHelper.CallCWSService(GetDataMessageTemplate);
                XmlDocument Model = new XmlDocument();
                Model.LoadXml(sReturn);
                xslt = new XslCompiledTransform();
                xslt.Load(Server.MapPath("./Xsl/Data.xsl"));
                // Transform the file.
                xslt.Transform(Model, null, sw);
                Display = sw.ToString();
                //mdhamija MITS 28062--Start
                //rsharma220 JIRA 3841
                if (!string.IsNullOrEmpty(sNoteKey))
                {
                    if (sNoteKey.Contains("name=note"))
                        Display = Server.HtmlDecode(Display);
                }
                //Display = Server.HtmlDecode(Display); //rkotak:mits 28930, this line is supposed to be commnted but was uncommented during the merge on 5/3/2012
                //mdhamija MITS 28062
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
    }
}
