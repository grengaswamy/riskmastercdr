﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EventExplorerData.aspx.cs" Inherits="Riskmaster.UI.EventExplorer.EventExplorerData" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register src="~/UI/Shared/Controls/ErrorControl.ascx" Tagname="ErrorControl" tagprefix="uc3" %>
<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" Tagname="CommonTasks" tagprefix="uc4" %>
<uc4:CommonTasks ID="CommonTasks1" runat="server" />
 <uc3:ErrorControl ID="ErrorControl1" runat="server" />
 <!-- mkaran2 - MITS 32179- HTML in Enhanced Notes causes Quick Summary enhanced notes records to run off the page -->
 <!-- <html style="overflow-x:hidden; "> --> 
<html>
			<head runat="server">
				<title>Explorer Data (<xsl:value-of select="data/@title"/>)</title>
				<script language="javascript" src="/RiskmasterUI/UI/EventExplorer/Scripts/eventexplorer.js"> </script>
       


    </head>
    <%= Display%>
    </html>


