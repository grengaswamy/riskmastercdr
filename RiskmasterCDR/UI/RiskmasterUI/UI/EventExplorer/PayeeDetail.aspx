﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PayeeDetail.aspx.cs" Inherits="Riskmaster.UI.EventExplorer.PayeeDetail" %>

<%@ Register src="~/UI/Shared/Controls/ErrorControl.ascx" Tagname="ErrorControl" tagprefix="uc3" %>



<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" Tagname="CommonTasks" tagprefix="uc4" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Payee Detail</title>
</head>
<body>
    <form id="frmData" runat="server">
     <uc3:ErrorControl ID="ErrorControl1" runat="server" />
      <asp:GridView ID="GridView1" runat="server" CellPadding="4" ForeColor="#333333"  AllowSorting="True" OnRowCreated="OnRowCreated"
          GridLines="None" Width="100%" ShowFooter=true AutoGenerateColumns=false onsorting="GridView1_Sorting" >
          <Columns>
          <asp:BoundField DataField="CtlNumber" HeaderText="Control #"  HeaderStyle-HorizontalAlign=Left SortExpression='FUNDS.CTL_NUMBER'/>
<asp:BoundField DataField="TransNumber"  HeaderText="Check #" HeaderStyle-HorizontalAlign=Left SortExpression='FUNDS.TRANS_NUMBER'/>
<asp:BoundField DataField="TransDate" HeaderText="Date" HeaderStyle-HorizontalAlign=Left SortExpression='FUNDS.TRANS_DATE'/>
<asp:BoundField DataField="Amount"  HeaderText="Check Amount" HeaderStyle-HorizontalAlign=Left SortExpression='FUNDS.AMOUNT'/>
<asp:BoundField DataField="Date" HeaderText="From/To Date" HeaderStyle-HorizontalAlign=Left SortExpression='FUNDS_TRANS_SPLIT.FROM_DATE'/>
<asp:BoundField DataField="InvoiceNumber"  HeaderText="Invoice" HeaderStyle-HorizontalAlign=Left SortExpression='FUNDS_TRANS_SPLIT.INVOICE_NUMBER' />
<asp:BoundField DataField="CodeDesc" FooterText="Total" HeaderText="Transaction Type" HeaderStyle-HorizontalAlign=Left SortExpression='CODES_TEXT.CODE_DESC'/>
<asp:TemplateField HeaderText="Split Amount" HeaderStyle-HorizontalAlign=Left SortExpression='FUNDS_TRANS_SPLIT.AMOUNT'>
<ItemTemplate>
  <%# DataBinder.Eval(Container.DataItem, "SplitAmount")%>
</ItemTemplate>
   <FooterTemplate>
             <%# Total %>
</FooterTemplate>
</asp:TemplateField>
</Columns>
          <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
          
          <RowStyle BackColor="#EFF3FB" />
          <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
          <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
          <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
          <EditRowStyle BackColor="#2461BF" />
          <AlternatingRowStyle BackColor="White" />
   
     
      </asp:GridView>
      
    </form>
  
</body>
</html>
