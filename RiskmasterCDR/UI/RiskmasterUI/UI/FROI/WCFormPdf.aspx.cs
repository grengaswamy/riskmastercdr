﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.UI.FROI
{
    public partial class WCFormPdf : System.Web.UI.Page
    {
        private XElement oMessageElement = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            XmlDocument oFDMPageDom = null;
            XElement rootElement = null;
            string sReturn = "";
            try
            {
                if (!Page.IsPostBack)
                {
                    oFDMPageDom = new XmlDocument();
                    //Preparing XML to send to service
                    oMessageElement = GetMessageTemplate();

                    //Modify XML 
                    ModifyTemplate();

                    //Calling Service to get all PreBinded Data 
                    sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
                    ErrorControl1.errorDom = sReturn;
                    oFDMPageDom.LoadXml(sReturn);
                    XmlNode oInstanceNode = oFDMPageDom.SelectSingleNode("/ResultMessage/Document");
                    XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
                    oFDMPageDom.LoadXml(oInstanceNode.OuterXml);

                    rootElement = XElement.Parse(oFDMPageDom.OuterXml);

                    if (!ErrorControl1.errorFlag)
                    {
                        XElement oEle = rootElement.XPathSelectElement("//Froi/File");
                        if (oEle != null)
                        {
                            string sFileContent = oEle.Value;
                            string sFileName = oEle.Attribute("Name").Value;
                            XElement oStorageIDEle = rootElement.XPathSelectElement("//Froi/StorageId");
                            if (oStorageIDEle != null)
                            {
                                StorageId.Value = oStorageIDEle.Value;
                                //Session["WCPDF_StorageId"] = oStorageIDEle.Value; //mkaran2-  WC Forms Session
                            }

                            byte[] byteOrg = Convert.FromBase64String(sFileContent);

                            Response.Clear();
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.fdf";
                            Response.AddHeader("Content-Disposition", "inline;");
                            Response.BinaryWrite(byteOrg);
                            Response.End();
                        }
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }

            
           
        }

        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
            <Authorization>0c3ac4fb-603d-490d-a260-e25e9c3c4907</Authorization> 
             <Call>
                 <Function>FROIAdaptor.InvokeWCForms</Function> 
              </Call>
            <Document>
             <FROI>
                  <ClaimId></ClaimId> 
                  <FormId></FormId> 
                  <State></State> 
                  <OrgHierarchy></OrgHierarchy> 
                  <Name /> 
                  <Title /> 
                  <Phone />
                  <rememberInfo>0</rememberInfo>   
                  <DocumentPath /> 
                  <AttachForm /> 
                  <DocStorageType></DocStorageType> 
                  <RequestHost>http://rbhatia4-2/RiskmasterUI</RequestHost> 
            </FROI>
         </Document>
  </Message>


            ");

            return oTemplate;
        }
        private void ModifyTemplate()
        {
            XElement oElement = oMessageElement.XPathSelectElement("./Document/FROI/ClaimId");
            if (oElement != null)
            {
                oElement.Value = AppHelper.GetQueryStringValue("ClaimId");
            }
            ClaimId.Value = AppHelper.GetQueryStringValue("ClaimId");
            //Session["WCPDF_ClaimId"] = AppHelper.GetQueryStringValue("ClaimId"); //mkaran2-  WC Forms Session

            oElement = oMessageElement.XPathSelectElement("./Document/FROI/State");
            if (oElement != null)
            {
                oElement.Value = AppHelper.GetQueryStringValue("State");
            }
            State.Value = AppHelper.GetQueryStringValue("State");
            //Session["WCPDF_State"] = AppHelper.GetQueryStringValue("State"); //mkaran2-  WC Forms Session

            oElement = oMessageElement.XPathSelectElement("./Document/FROI/FormId");
            if (oElement != null)
            {
                oElement.Value = AppHelper.GetQueryStringValue("FormId");
            }
            FormId.Value = AppHelper.GetQueryStringValue("FormId");
            //Session["WCPDF_FormId"] = AppHelper.GetQueryStringValue("FormId"); //mkaran2-  WC Forms Session

            oElement = oMessageElement.XPathSelectElement("./Document/FROI/OrgHierarchy");
            if (oElement != null)
            {
                oElement.Value = AppHelper.GetQueryStringValue("OrgHierarchy");
            }
            OrgHierarchy.Value = AppHelper.GetQueryStringValue("OrgHierarchy");
            Session["WCPDF_OrgHierarchy"] = AppHelper.GetQueryStringValue("OrgHierarchy");

            oElement = oMessageElement.XPathSelectElement("./Document/FROI/Name");
            if (oElement != null)
            {
                oElement.Value = AppHelper.GetQueryStringValue("Name");
            }
            Name.Value = AppHelper.GetQueryStringValue("Name");

            oElement = oMessageElement.XPathSelectElement("./Document/FROI/Title");
            if (oElement != null)
            {
                oElement.Value = AppHelper.GetQueryStringValue("Title");
            }
            Title.Value = AppHelper.GetQueryStringValue("Title");

            oElement = oMessageElement.XPathSelectElement("./Document/FROI/Phone");
            if (oElement != null)
            {
                oElement.Value = AppHelper.GetQueryStringValue("Phone");
            }
            Phone.Value = AppHelper.GetQueryStringValue("Phone");

            oElement = oMessageElement.XPathSelectElement("./Document/FROI/RequestHost");
            if (oElement != null)
            {
                oElement.Value = Request.Url.AbsoluteUri.Substring(0 , Request.Url.AbsoluteUri.IndexOf("RiskmasterUI") + 12);
            }
            RequestHost.Value = Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.IndexOf("RiskmasterUI") + 12);
        }
    }
}
