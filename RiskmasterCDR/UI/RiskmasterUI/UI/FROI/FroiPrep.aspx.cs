﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.UI.FROI
{
    public partial class FroiPrep : NonFDMBasePageCWS
    {
        private XElement oMessageElement = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            XmlDocument oFDMPageDom = null;
            XElement rootElement = null;
            string sReturn = "";

            try
            {
                if (!Page.IsPostBack)
                {
                    oFDMPageDom = new XmlDocument();
                    //Preparing XML to send to service
                    oMessageElement = GetMessageTemplate();

                    //Modify XML 
                    ModifyTemplate();

                    //Calling Service to get all PreBinded Data 
                    CallCWS("FROIAdaptor.SkipFroiPreparerPrompt" , oMessageElement , out sReturn , false , true);
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization>85999636-bfe4-46ea-882b-9de89bab4c18</Authorization> 
                <Call>
                  <Function>FROIAdaptor.SkipFroiPreparerPrompt</Function> 
                </Call>
                <Document>
                     <FROI>
                          <ClaimId></ClaimId> 
                          <FormId /> 
                          <State /> 
                          <Name /> 
                          <Title /> 
                          <Phone /> 
                          <DocumentPath /> 
                          <AttachForm /> 
                          <DocStorageType /> 
                      </FROI>
                </Document>
            </Message>
            ");

            return oTemplate;
        }
        private void ModifyTemplate()
        {
            XElement oElement = oMessageElement.XPathSelectElement("./Document/FROI/ClaimId");
            if (oElement != null)
            {
                oElement.Value = AppHelper.GetQueryStringValue("claimid");
            }
            

            oElement = oMessageElement.XPathSelectElement("./Document/FROI/State");
            if (oElement != null)
            {
                oElement.Value = AppHelper.GetQueryStringValue("folder");
            }
            
        }

        protected void btnBackToClaimTop_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/UI/FDM/claimwc.aspx?recordID=" + ClaimId.Text + "&SysCmd=0");
        }
    }
}
