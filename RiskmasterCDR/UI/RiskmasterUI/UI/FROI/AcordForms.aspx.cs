﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.UI.FROI
{
    public partial class AcordForms : NonFDMBasePageCWS
    {
        private XElement oMessageElement = null;
        protected IEnumerable result = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            XmlDocument oFDMPageDom = null;
            XElement rootElement = null;
            string sReturn = "";
            try
            {
                if (!Page.IsPostBack)
                {
                    oFDMPageDom = new XmlDocument();
                    //Preparing XML to send to service
                    oMessageElement = GetMessageTemplate();

                    //Modify XML 
                    ModifyTemplate();

                    //Calling Service to get all PreBinded Data 
                    CallCWS("FROIAdaptor.GetACORDForms", oMessageElement, out sReturn, false, true);

                    ErrorControl1.errorDom = sReturn;
                    oFDMPageDom.LoadXml(sReturn);
                    XmlNode oInstanceNode = oFDMPageDom.SelectSingleNode("/ResultMessage/Document");
                    XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
                    oFDMPageDom.LoadXml(oInstanceNode.OuterXml);

                    rootElement = XElement.Parse(oFDMPageDom.OuterXml);
                    result = from c in rootElement.XPathSelectElements("//ACORDForms/Form")
                             select c;
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization>b1b7ae5b-d681-45a8-9709-4cd130b34f29</Authorization> 
                 <Call>
                  <Function>FROIAdaptor.GetACORDForms</Function> 
                 </Call>
                <Document>
                     <ACORD>
                      <ClaimId></ClaimId> 
                      <FormId /> 
                      <State /> 
                      <DocumentPath /> 
                      <AttachForm></AttachForm> 
                      <DocStorageType /> 
                      <Name></Name> 
                      <Title></Title> 
                      <Phone></Phone> 
                      <ClaimNumber></ClaimNumber>                      
                      </ACORD>
                </Document>
             </Message>
            ");

            return oTemplate;
        }

        private void ModifyTemplate()
        {
            XElement oElement = oMessageElement.XPathSelectElement("./Document/ACORD/ClaimId");
            if (oElement != null)
            {
                oElement.Value = AppHelper.GetQueryStringValue("ClaimId");
            }

            oElement = oMessageElement.XPathSelectElement("./Document/ACORD/Name");
            if (oElement != null)
            {
                oElement.Value = AppHelper.GetQueryStringValue("Name");
            }           
            
            oElement = oMessageElement.XPathSelectElement("./Document/ACORD/ClaimNumber");
            if (oElement != null)
            {
                oElement.Value = AppHelper.GetQueryStringValue("claimnumber");
            }

            

        }
    }
}
