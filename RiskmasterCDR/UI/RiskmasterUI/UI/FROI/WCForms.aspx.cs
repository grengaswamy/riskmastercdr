﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.UI.FROI
{
    public partial class WCForms : System.Web.UI.Page
    {
        private XElement oMessageElement = null;
        protected IEnumerable result = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            XmlDocument oFDMPageDom = null;
            XElement rootElement = null;
            string sReturn = "";
            try
            {
                if (!Page.IsPostBack)
                {
                    oFDMPageDom = new XmlDocument();
                    //Preparing XML to send to service
                    oMessageElement = GetMessageTemplate();

                    //Modify XML 
                    ModifyTemplate();

                    //Calling Service to get all PreBinded Data 
                    sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
                    ErrorControl1.errorDom = sReturn;
                    oFDMPageDom.LoadXml(sReturn);
                    XmlNode oInstanceNode = oFDMPageDom.SelectSingleNode("/ResultMessage/Document");
                    XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
                    oFDMPageDom.LoadXml(oInstanceNode.OuterXml);

                    rootElement = XElement.Parse(oFDMPageDom.OuterXml);
                    result = from c in rootElement.XPathSelectElements("//ClaimForms/Form")
                             select c;

                    XElement oEle = rootElement.XPathSelectElement("//ClaimForms/FormCount");
                    if (oEle != null)
                    {
                        hdFormCount.Value = oEle.Value;
                    }



                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }

        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
             <Authorization>0c3ac4fb-603d-490d-a260-e25e9c3c4907</Authorization> 
             <Call>
                <Function>FROIAdaptor.GetWCForms</Function> 
              </Call>
              <Document>
                <FROI>
                  <ClaimId></ClaimId> 
                  <FormId /> 
                  <State></State> 
                  <OrgHierarchy></OrgHierarchy> 
                  <Name /> 
                  <Title /> 
                  <Phone /> 
                  <DocumentPath /> 
                  <AttachForm /> 
                  <DocStorageType /> 
                </FROI>
              </Document>
            </Message>



            ");

            return oTemplate;
        }
        private void ModifyTemplate()
        {
            XElement oElement = oMessageElement.XPathSelectElement("./Document/FROI/FormId");
            if (oElement != null)
            {   // pen testing changes:atavaragiri mits 27914
                //oElement.Value = AppHelper.GetQueryStringValue("FormId");
                oElement.Value =AppHelper.HTMLCustomEncode( AppHelper.GetQueryStringValue("FormId"));
                //END:pen testing
            }
            // pen testing changes:atavaragiri mits 27914
            //FormId.Value = AppHelper.GetQueryStringValue("FormId");
            if(!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("FormId")))
            FormId.Value = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("FormId"));
            //END:pen testing

            oElement = oMessageElement.XPathSelectElement("./Document/FROI/ClaimId");
            if (oElement != null)
            {    //pen testing changes:atavaragiri mits 27914
                //oElement.Value = AppHelper.GetQueryStringValue("claimid");
                if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("claimid")))
                oElement.Value =AppHelper.HTMLCustomEncode( AppHelper.GetQueryStringValue("claimid"));
               
            }

            if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("claimid")))
            //ClaimId.Value = AppHelper.GetQueryStringValue("claimid");
             ClaimId.Value =AppHelper.HTMLCustomEncode( AppHelper.GetQueryStringValue("claimid"));
            // END:pen testing

            oElement = oMessageElement.XPathSelectElement("./Document/FROI/State");
            if (oElement != null)
            {   // pen testing chnages:atavaragiri mits 27914
                //oElement.Value = AppHelper.GetQueryStringValue("folder");
                if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("folder")))
                oElement.Value = AppHelper.GetQueryStringValue("folder");
            }


            //State.Value = AppHelper.GetQueryStringValue("folder");
            if(!String.IsNullOrEmpty( AppHelper.GetQueryStringValue("folder")))
            State.Value =AppHelper.HTMLCustomEncode( AppHelper.GetQueryStringValue("folder"));
            //END:pen testing

            oElement = oMessageElement.XPathSelectElement("./Document/FROI/Name");
            if (oElement != null)
            {
               oElement.Value = Request.Form["Name"]; //Yatharth : Assigned for MITS 20033
            }
           
            Name.Value = Request.Form["Name"];//Yatharth : Assigned for MITS 20033
            oElement = oMessageElement.XPathSelectElement("./Document/FROI/Title");
            if (oElement != null)
            {
               oElement.Value = Request.Form["Title"];//Yatharth : Assigned for MITS 20033
            }
            
            Title.Value = Request.Form["Title"];//Yatharth : Assigned for MITS 20033
            oElement = oMessageElement.XPathSelectElement("./Document/FROI/Phone");
            if (oElement != null)
            {
                oElement.Value = Request.Form["Phone"];//Yatharth : Assigned for MITS 20033
            }
            Phone.Value = Request.Form["Phone"];//Yatharth : Assigned for MITS 20033

            oElement = oMessageElement.XPathSelectElement("./Document/FROI/OrgHierarchy");
            if (oElement != null)
            {    // pen testing changes:atavaragiri mits 27914
                //oElement.Value = AppHelper.GetQueryStringValue("OrgHierarchy");
                if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("OrgHierarchy")))
                    oElement.Value =AppHelper.HTMLCustomEncode( AppHelper.GetQueryStringValue("OrgHierarchy"));
            }
            //OrgHierarchy.Value = AppHelper.GetQueryStringValue("OrgHierarchy");
            if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("OrgHierarchy")))
            OrgHierarchy.Value =AppHelper.HTMLCustomEncode( AppHelper.GetQueryStringValue("OrgHierarchy"));
            //END:pen testing
        }
    }
}
