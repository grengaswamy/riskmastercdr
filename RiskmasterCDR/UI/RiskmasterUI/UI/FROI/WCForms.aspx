﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WCForms.aspx.cs" Inherits="Riskmaster.UI.UI.FROI.WCForms" ValidateRequest="false" EnableViewStateMac="false" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Import Namespace ="System.Xml"%>
<%@ Import Namespace ="System.Xml.Linq"%>
<%@ Import Namespace ="System.Xml.XPath"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>WC Claim Forms</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>
    <script type="text/javascript" src="../../Scripts/FROI.js" language="javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <div id="divScroll" style="overflow:auto;height:100%">
    <table width="100%">
						<tr>
							<td class="msgheader" bgcolor="#D5CDA4">WC PDF Forms</td>
						</tr>
						<tr>
							<%if (hdFormCount.Value != "0")
         {%><td class="Bold1">The Following Claim Forms are available:</td>
                             <%}
         else
         {%>
                                 <td>There are no WC PDF Forms available.</td>
                                 <%} %>
							
						</tr>
					</table>
					<div id="divForms" class="divScroll">
						<table>
							<%int i = 0; foreach (XElement item in result)
         {%>
							
								<tr>
									<td>
										<strong>
											<%=item.Attribute("FormCatDesc").Value%> 
										</strong>
									</td>
									<td>
										
										
										<% 
                                            lnkFrm.ID = "lnkFrm" + i;
                                            lnkFrm.Text = item.Attribute("FormNameAndTitle").Value;
                                            lnkFrm.OnClientClick = "return ShowWCPdf('" + item.Attribute("FormId").Value + "');";
                                            
              %>
              <asp:LinkButton runat="server" id="lnkFrm"></asp:LinkButton> 
											
									</td>
								</tr>
							<%} %>
						</table>
					</div>
					<br/>
					<asp:button runat="server" CssClass="button" id="btnBack" OnClientClick="return Back(document.getElementById('ClaimId').value , 'claimwc');" text="Back to Claim"/>
					<asp:HiddenField id="ClaimId" runat="server"/>
					<asp:HiddenField id="FormId" runat="server" />
					<asp:HiddenField id="State" runat="server" />
					<asp:HiddenField id="OrgHierarchy" runat="server" />
					<asp:HiddenField id="Name" runat="server" />
					<asp:HiddenField id="Title" runat="server" />
					<asp:HiddenField id="Phone" runat="server" />
					<asp:HiddenField ID="hdFormCount" runat="server" />
			</div>
    </form>
</body>
</html>
