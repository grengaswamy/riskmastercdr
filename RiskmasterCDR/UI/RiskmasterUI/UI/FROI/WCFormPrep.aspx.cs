﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.UI.FROI
{
    public partial class WCFormPrep : NonFDMBasePageCWS
    {
        private XElement oMessageElement = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            //Deb: Update the code for MS Security HP not to submit the page directly
            string sSubmitedFrmRsv = AppHelper.GetQueryStringValue("Submit");
            if (!string.IsNullOrEmpty(sSubmitedFrmRsv))
            {
                if (sSubmitedFrmRsv == "WCForms")
                {
                    Server.Transfer("WCForms.aspx?claimid=" + ClaimId.Text + "&folder=" + Folder.Text + "&OrgHierarchy=" + OrgHierarchy.Text);
                }
                else if (sSubmitedFrmRsv == "FroiForms")
                {
                    Server.Transfer("FroiForms.aspx");
                }
            }
            //Deb: Update the code for MS Security HP not to submit the page directly
            XmlDocument oFDMPageDom = null;
            XElement rootElement = null;
            string sReturn = "";

            try
            {
                if (!Page.IsPostBack)
                {
                    //oFDMPageDom = new XmlDocument();
                    //Preparing XML to send to service
                    //oMessageElement = GetMessageTemplate();

                    // pen testing changes :atavaragiri mits 27914
                    //OrgHierarchy.Text = AppHelper.GetQueryStringValue("OrgHierarchy");
                    if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("OrgHierarchy")))
                    OrgHierarchy.Text =AppHelper.HTMLCustomEncode( AppHelper.GetQueryStringValue("OrgHierarchy"));
                    //Folder.Text = AppHelper.GetQueryStringValue("folder");
                    if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("folder")))
                    Folder.Text =AppHelper.HTMLCustomEncode( AppHelper.GetQueryStringValue("folder"));
                    //ClaimId.Text = AppHelper.GetQueryStringValue("claimid");
                    if (!String.IsNullOrEmpty(AppHelper.GetQueryStringValue("claimid")))
                    ClaimId.Text =AppHelper.HTMLCustomEncode( AppHelper.GetQueryStringValue("claimid"));
                    // END:pen testing

                    //Modify XML 
                    //ModifyTemplate();

                    //Calling Service to get all PreBinded Data 
                    //CallCWS("FROIAdaptor.SkipJurisPreparerPrompt", oMessageElement, out sReturn, false, true);
                    NonFDMCWSPageLoad("JurisPreparerInfoAdaptor.Get");
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization>85999636-bfe4-46ea-882b-9de89bab4c18</Authorization> 
                <Call>
                  <Function>FROIAdaptor.SkipJurisPreparerPrompt</Function> 
                </Call>
                <Document>
                     <Juris>
                          <ClaimId></ClaimId> 
                          <FormId /> 
                          <State /> 
                          <Name /> 
                          <Title /> 
                          <Phone /> 
                          <DocumentPath /> 
                          <AttachForm /> 
                          <DocStorageType /> 
                      </Juris>
                </Document>
            </Message>
            ");

            return oTemplate;
        }
        private void ModifyTemplate()
        {
            XElement oElement = oMessageElement.XPathSelectElement("./Document/Juris/ClaimId");
            if (oElement != null)
            {     // pen testing changes:atavaragiri mits 27914
                //oElement.Value = AppHelper.GetQueryStringValue("claimid");
                oElement.Value =AppHelper.HTMLCustomEncode( AppHelper.GetQueryStringValue("claimid"));
                //END :pen testing
            }


            oElement = oMessageElement.XPathSelectElement("./Document/Juris/State");
            if (oElement != null)
            {    // pen testing changes:atavaragiri mits 27914
                //oElement.Value = AppHelper.GetQueryStringValue("folder");
                oElement.Value =AppHelper.HTMLCustomEncode( AppHelper.GetQueryStringValue("folder"));
                // END:pen testing
            }

        }
    }
}
