﻿using System;
using System.Collections;
using System.Linq;
using System.Web.UI;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.UI.FROI
{
    public partial class FroiForms : NonFDMBasePageCWS
    {
        private XElement oMessageElement = null;
        protected IEnumerable result = null;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            XmlDocument oFDMPageDom = null;
            XElement rootElement = null;
            string sReturn = "";
            try
            {
                if (!Page.IsPostBack)
                {
                    oFDMPageDom = new XmlDocument();
                    //Preparing XML to send to service
                    oMessageElement = GetMessageTemplate();

                    //Modify XML 
                    ModifyTemplate();

                    //Calling Service to get all PreBinded Data 
                    CallCWS("FROIAdaptor.GetFROIForms", oMessageElement, out sReturn, false, true);
                    
                    ErrorControl1.errorDom = sReturn;
                    oFDMPageDom.LoadXml(sReturn);
                    XmlNode oInstanceNode = oFDMPageDom.SelectSingleNode("/ResultMessage/Document");
                    XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
                    oFDMPageDom.LoadXml(oInstanceNode.OuterXml);

                    rootElement = XElement.Parse(oFDMPageDom.OuterXml);
                    result = from c in rootElement.XPathSelectElements("//FROIForms/Form")
                             select c;
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization>b1b7ae5b-d681-45a8-9709-4cd130b34f29</Authorization> 
                 <Call>
                  <Function>FROIAdaptor.GetFROIForms</Function> 
                 </Call>
                <Document>
                     <FROI>
                      <ClaimId></ClaimId> 
                      <FormId /> 
                      <State /> 
                      <DocumentPath /> 
                      <AttachForm></AttachForm> 
                      <DocStorageType /> 
                      <Name></Name> 
                      <Title></Title> 
                      <Phone></Phone> 
                      </FROI>
                </Document>
             </Message>
            ");

            return oTemplate;
        }

        private void ModifyTemplate()
        {
            XElement oElement = oMessageElement.XPathSelectElement("./Document/FROI/ClaimId");
            if (oElement != null)
            {
                oElement.Value = Request.Form["ClaimId"];
            }

            oElement = oMessageElement.XPathSelectElement("./Document/FROI/AttachForm");
            if (oElement != null)
            {
                if (Request.Form["AttachForm_chk"] != null)
                {
                    if (Request.Form["AttachForm_chk"] == "on")
                    {
                        oElement.Value = "true";
                    }
                }
                
            }

            oElement = oMessageElement.XPathSelectElement("./Document/FROI/Name");
            if (oElement != null)
            {
                oElement.Value = Request.Form["Name"];
            }

            oElement = oMessageElement.XPathSelectElement("./Document/FROI/Title");
            if (oElement != null)
            {
                oElement.Value = Request.Form["Title"];
            }

            oElement = oMessageElement.XPathSelectElement("./Document/FROI/Phone");
            if (oElement != null)
            {
                oElement.Value = Request.Form["Phone"];
            }
            

        }
        protected void btnBackToClaimTop_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/UI/FDM/claimwc.aspx?recordID=" + ClaimId.Text + "&SysCmd=0");
        }
    }
}
