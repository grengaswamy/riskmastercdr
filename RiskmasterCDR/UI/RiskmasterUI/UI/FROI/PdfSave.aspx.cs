﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.UI.FROI
{
    public partial class PdfSave : System.Web.UI.Page
    {
        private XElement rootElement = null;
        private XElement oMessageElement = null;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            XmlDocument oFDMPageDom = null;
            try
            {
                oFDMPageDom = new XmlDocument();
                string sReturn = "";
                if (!Page.IsPostBack)
                {
                    oFDMPageDom = new XmlDocument();
                    //Preparing XML to send to service
                    oMessageElement = GetMessageTemplate();

                    //Modify XML 
                    ModifyTemplate();

                    //Calling Service to get all PreBinded Data 
                    sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
                    ErrorControl1.errorDom = sReturn;

                    if (!ErrorControl1.errorFlag)
                    {
                        btnClose.Visible = false;
                        string sScript = "window.close();";
                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "script_close", sScript, true);
                    }
                    else
                    {
                        btnClose.Visible = true;
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
            <Authorization>0c3ac4fb-603d-490d-a260-e25e9c3c4907</Authorization> 
             <Call>
                <Function>FormProcessorAdaptor.SavePdfForm</Function> 
             </Call>
            <Document>
                <WCPdfForm>
                  <ClaimId></ClaimId> 
                  <FormId></FormId> 
                  <StorageId></StorageId> 
                  <State></State> 
                  <OrgHierarchy></OrgHierarchy> 
                  <RequestFDFData>
                     <request>
                     <parameters>
                     </parameters>
                    </request>
                  </RequestFDFData>
                </WCPdfForm>
            </Document>
            </Message>


            ");

            return oTemplate;
        }
        private void ModifyTemplate()
        {
            //mkaran2-  WC Forms Session

            //XElement oElement = null;         
            //if (Session["WCPDF_ClaimId"] != null)
            //{
            //    oElement = oMessageElement.XPathSelectElement("./Document/WCPdfForm/ClaimId");
            //    if (oElement != null)
            //    {
            //        oElement.Value = Session["WCPDF_ClaimId"].ToString();
            //    }
            //}

            //if (Session["WCPDF_State"] != null)
            //{
            //    oElement = oMessageElement.XPathSelectElement("./Document/WCPdfForm/State");
            //    if (oElement != null)
            //    {
            //        oElement.Value = Session["WCPDF_State"].ToString();
            //    }
            //}

            //if (Session["WCPDF_FormId"] != null)
            //{
            //    oElement = oMessageElement.XPathSelectElement("./Document/WCPdfForm/FormId");
            //    if (oElement != null)
            //    {
            //        oElement.Value = Session["WCPDF_FormId"].ToString();
            //    }
            //}

            //if (Session["WCPDF_OrgHierarchy"] != null)
            //{
            //    oElement = oMessageElement.XPathSelectElement("./Document/WCPdfForm/OrgHierarchy");
            //    if (oElement != null)
            //    {
            //        oElement.Value = Session["WCPDF_OrgHierarchy"].ToString();
            //    }
            //}

            //if (Session["WCPDF_StorageId"] != null)
            //{
            //    oElement = oMessageElement.XPathSelectElement("./Document/WCPdfForm/StorageId");
            //    if (oElement != null)
            //    {
            //        oElement.Value = Session["WCPDF_StorageId"].ToString();
            //    }
            //}
            //Session.Remove("WCPDF_ClaimId");
            //Session.Remove("WCPDF_State");
            //Session.Remove("WCPDF_FormId");
            //Session.Remove("WCPDF_OrgHierarchy");
            //Session.Remove("WCPDF_StorageId");

            //We need to put all PDF forms data from Request object into XML

            rootElement = oMessageElement.XPathSelectElement("./Document/WCPdfForm/RequestFDFData/request/parameters");
            foreach (string sKey in Request.Form.AllKeys)
            {
                XElement oEle = new XElement("parameter");
                XElement oChildEle = new XElement("name", sKey);
                oEle.Add(oChildEle);
                oChildEle = new XElement("value", Request.Form[sKey].ToString());
                oEle.Add(oChildEle);
                rootElement.Add(oEle);
            }


            
        }
        
    }
}
