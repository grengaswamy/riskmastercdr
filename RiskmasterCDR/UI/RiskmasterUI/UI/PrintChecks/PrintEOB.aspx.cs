﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;

namespace Riskmaster.UI.PrintChecks
{
    public partial class PrintEOB : System.Web.UI.Page
    {
        public XmlDocument Model = null;
        private string PrintEOBBRSReportTemplate = "<Message><Authorization></Authorization><Call>" +
         "<Function>PrintEOBAdaptor.PrintEOBBRSReport</Function></Call><Document><Document><PrintEOBBRSReport><TransId /><IsSilent></IsSilent><ClaimId/></PrintEOBBRSReport></Document></Document></Message>";
        private string FileRequestPostEobMessageTemplate = "<Message><Authorization></Authorization><Call>" +
   "<Function>PrintChecksAdaptor.GetPrintCheckFile</Function></Call><Document><PrintChecks><FileName/><FileType/></PrintChecks></Document></Message>";
        private string DeletePostEobMessageTemplate = "<Message><Authorization></Authorization><Call>" +
"<Function>PrintEOBAdaptor.DeleteEobFiles</Function></Call><Document><PostCheckEOB><FileNames/></PostCheckEOB></Document></Message>";
        protected void Page_Load(object sender, EventArgs e)
        {
            Model = new XmlDocument();
            try
            {
                if (!Page.IsPostBack)
                {
                    StoreQueryStringValues();
                }
                else
                {
                    if (hdnFileRequest.Value == "1")
                    {
                        hdnFileRequest.Value = "0";
                        GetFile();
                    }
                    if (hdnPrintRequest.Value == "1")
                    {
                        hdnPrintRequest.Value = "0";
                        PrintEOBBRSReportTemplate = CreateServiceCall();
                        string sReturn = AppHelper.CallCWSService(PrintEOBBRSReportTemplate.ToString());
                        Model.LoadXml(sReturn);
                        SetValuesFromService();

                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            DataBind();

        }
        private void SetValuesFromService()
        {
            hdnFileNames.Value = AppHelper.GetValueFromDOM(Model, "FileNames");
        }

        private void GetFile()
        {

            FileRequestPostEobMessageTemplate = AppHelper.ChangeMessageValue(FileRequestPostEobMessageTemplate, "//FileName", hdnFileName.Value);
            FileRequestPostEobMessageTemplate = AppHelper.ChangeMessageValue(FileRequestPostEobMessageTemplate, "//FileType", hdnFileType.Value);
            string sReturn = AppHelper.CallCWSService(FileRequestPostEobMessageTemplate.ToString());
            XmlDocument objFile = new XmlDocument();
            objFile.LoadXml(sReturn);

            byte[] pdfbytes = Convert.FromBase64String(objFile.SelectSingleNode("//File").InnerText);

            Response.Buffer = true;
            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Charset = "";
            Response.AppendHeader("Content-Encoding", "none;");
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=report.PDF");
            Response.AddHeader("Accept-Ranges", "bytes");
            Response.BinaryWrite(pdfbytes);
            Response.Flush();
            Response.Close();
        }
        protected void DeleteFile()
        {
            DeletePostEobMessageTemplate = AppHelper.ChangeMessageValue(DeletePostEobMessageTemplate, "//FileNames", hdnFileNames.Value);
            string sReturn = AppHelper.CallCWSService(DeletePostEobMessageTemplate.ToString());
            XmlDocument objFile = new XmlDocument();
            objFile.LoadXml(sReturn);
        }


        private void StoreQueryStringValues()
        {
            hdnTransId.Value = AppHelper.GetQueryStringValue("transid");

            hdnIsSilent.Value = AppHelper.GetQueryStringValue("silent");

            hdnClaimId.Value = AppHelper.GetQueryStringValue("iClaimId");
        }
        private string CreateServiceCall()
        {
            string sTransId = AppHelper.GetFormValue("hdnTransId");
            string sSilent = AppHelper.GetFormValue("hdnIsSilent");
            string sStateId = AppHelper.GetFormValue("hdnClaimId");

            PrintEOBBRSReportTemplate = AppHelper.ChangeMessageValue(PrintEOBBRSReportTemplate, "//TransId", sTransId);

            PrintEOBBRSReportTemplate = AppHelper.ChangeMessageValue(PrintEOBBRSReportTemplate, "//IsSilent", sSilent);

            PrintEOBBRSReportTemplate = AppHelper.ChangeMessageValue(PrintEOBBRSReportTemplate, "//ClaimId", sStateId);

            return PrintEOBBRSReportTemplate;
        }
    }
}
