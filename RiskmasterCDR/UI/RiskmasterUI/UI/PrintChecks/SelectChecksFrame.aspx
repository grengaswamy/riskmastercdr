﻿<%--/**********************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                    *
 **********************************************************************************************
 * 12/09/2014 | RMA-438         | ajohari2   | Underwriters - EFT Payments
 **********************************************************************************************/--%>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SelectChecksFrame.aspx.cs"
    Inherits="Riskmaster.UI.PrintChecks.SelectChecksFrame" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc2" %>
<%--akaushik5 added for MITS 37251 Starts--%>
<%@ Register Assembly="MultiCurrencyCustomControl" Namespace="MultiCurrencyCustomControl" TagPrefix="mc" %>
<%--akaushik5 added for MITS 37251 Ends--%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Select Checks to Print</title>
    <link rel="stylesheet" href="../../App_Themes/RMX_Default/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <%--bgupta22 MITS 34700--%>
    <style type="text/css">
        .Selected {
            float:none !important;
        }
    </style>

    <script language="javaScript" src="../../Scripts/PrintChecks.js" type="text/javascript"></script>
<%--akaushik5 added for MITS 37251 Starts--%>
    <script language="javaScript" src="../../Scripts/jquery/jquery-1.8.0.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(
            function () {
                $("input:checkbox").on("click", GetSelectedCount);
            }
        );
        function GetSelectedCount() {
            var n = $("td>input:checked").length;
            var totalAmount = 0;
            $("#selectedCount").text(n);
            $("td>input:checked").each(function () {
                totalAmount += parseFloat($(this).siblings('.amount').val());
            })
            if (document.getElementById('TotalAmount') != null) {
                document.getElementById('TotalAmount').setAttribute('value', totalAmount);
                document.getElementById('TotalAmount').onblur();
                $("#lblTotalAmount").text(document.getElementById('TotalAmount').getAttribute('value'));//RMA-9665 caggarwal4
            }
        }
        </script>
    <script type="text/javascript">
        $(document).ready(function () {
            {
                // Tooltip only Text
			// npadhy - JIRA 6421 Add the functionality to control the tooltip hover, width and New Line 
            $('.tooltiphover').hover(function () {
                    // Hover over code
                var title = $(this).parent().attr('tooltip');
                $('<p class="tooltip"></p>')
                    .html(title)
                    .appendTo('body')
                    .fadeIn('fast');
                }, function () {
                    // Hover out code
                    $(this).parent().attr('tooltip', $(this).parent().data('tipText'));
                    $('.tooltip').remove();
                }).mousemove(function (e) {
                    var mousex = e.pageX; //Get X coordinates
                    var mousey = e.pageY + 10; //Get Y coordinates
                    var rightpos = $(window).width() - mousex - $(this).outerWidth(true);
                    $('.tooltip')
                    .css({ top: mousey, left: '', right: rightpos, width: $(this).parent().width })
                });
            }
            });
    </script>
    <%-- aravi5 added for RMA-7066 - Chrome: Unable to Print check from Pre-check register Starts--%>
    <script type="text/javascript">
        function HideOverlaydiv() {
            var IEbrowser = false || !!document.documentMode; // At least IE6;
            if (!IEbrowser) {
                if (window.opener.parent.parent.document.getElementById("overlaydiv") != null) {
                    window.opener.parent.parent.document.getElementById("overlaydiv").remove();
                    var returnval = window.returnValue;
                    if (returnval != null && returnval != "") {
                        var arrSelectedChecks = returnval.split("||");
                        window.opener.document.forms[0].selectedchecksids.value = arrSelectedChecks[0];
                        window.opener.document.forms[0].selectedautochecksids.value = arrSelectedChecks[1];
                        //Add by kuladeep for mits:22878 Start---If user select all checks through selection in that case we pass flag only,no check ids
                        window.opener.document.forms[0].hdnAllCheckSelected.value = arrSelectedChecks[2];
                        window.opener.document.getElementById("functiontocall").value = "PrintChecksAdaptor.CheckSetChanged";
                        //window.document.all("action").value = "PageRefresh";
                        window.opener.document.forms[0].submit();
                        //OnFormSubmitPrintCheck();
                    }
                    else {
                        // window.opener.document.forms[0].printpre.disabled = window.opener.document.forms[0].printpre.disabled;
                        window.opener.document.forms[0].choose.disabled = false;
                        window.opener.document.forms[0].functiontocall.value = "";
                    }
                }
            }
        }
    </script> 
    <%-- aravi5 added for RMA-7066 - Chrome: Unable to Print check from Pre-check register Ends--%>
<%--akaushik5 added for MITS 37251 Ends--%>
    <style>
        .tooltip {
            display: none;
            position: fixed;
            border: 1px solid #6AADE4;
            background-color: #6AADE4;
            border-radius: 5px;
            padding: 10px;
            color: #fff;
        }
    </style> 
</head>
<base target="_self">
<%--akaushik5 added for MITS 37251 Starts--%>
<body onpagehide="HideOverlaydiv(); return false;" onload="Javascript :SelectChecksOnLoad(); GetSelectedCount();"> <%-- aravi5: RMA-7066 --%>
<%--akaushik5 added for MITS 37251 Ends--%>
    <form id="frmData" runat="server">
    <rmcontrol:ErrorControl ID = "ErrorControl1" runat="server" />
    <table width="100%" cellspacing="0" cellpadding="2" border="0">
        <tr bgcolor="#0d4c7d">
            <td style="color: white">
                Select Checks To Print
            </td>
        </tr>
        <tr align="right">
            <td class="msgheader">
                Filter By Control Number Starting With:
                <asp:TextBox runat="server" rmxref="/Instance/Document/GetListOfChecks/ControlNumber"
                    ID="ControlNumber" />
                <asp:Button runat="server" Text="Refresh" class="button" ID="Refresh" OnClick="Refresh_Click" />
            </td>
        </tr>
    </table>
    <%--<dg:UserControlDataGrid runat="server" ID="EntityXOperatingAsGrid" Target="/Instance/UI/FormVariables/SysExData/EntityXOperatingAs" Unique_Id="OperatingId" ShowCheckBox="true" Width="680px" Height="150px" HideNodes="New|Edit|Delete" ShowHeader="True" LinkColumn="" PopupWidth="500" PopupHeight="400" />--%>
    <%--<asp:DataGrid runat="server" ID="dgSelectChecksFrame" />--%>
    <div style="overflow: scroll; width: 99%; position: relative; height: 300px">
        <asp:GridView ID="gvSelectChecksFrame" runat="server" AutoGenerateColumns="false"
            AllowPaging="false" AllowSorting="true" Width="100%" ShowHeader="true" GridLines="both"
            OnSorting="gvSelectChecksFrame_Sorting" EmptyDataText="Zero checks meet your selection criteria. There are no checks to display."
            CellPadding="8" EnableModelValidation="True" OnRowDataBound="gvSelectChecksFrame_RowDataBound">
            <RowStyle CssClass="" />
            <AlternatingRowStyle CssClass="rowdark2" />
            <HeaderStyle ForeColor="White" />
            <Columns>
                <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="Selected" ItemStyle-HorizontalAlign="Center">
                    <HeaderTemplate>
                        &nbsp<asp:CheckBox runat="server" ID="ChkAll" onclick="SelectAllChecks(this.id);" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%--hlv MITS 23153--%>                
                        <input type="checkbox" name="<%# DataBinder.Eval(Container.DataItem,"CheckBoxName")%>" id='<%# DataBinder.Eval(Container.DataItem,"CheckBoxName")%>'
                            value='<%# DataBinder.Eval(Container.DataItem,"TransId")%>' onclick="CheckClick()"
                            <%# DataBinder.Eval(Container.DataItem, "IsSelected") %> />
                            <%--hlv MITS 23153--%>                
                        <asp:HiddenField ID="hdnId" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"TransId") %>' />
                        <asp:HiddenField ID="hdnIsAutoCheck" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"AutoCheck") %>' />
<%--akaushik5 added for MITS 37251 Starts--%>
                        <asp:TextBox runat="server" Style="display: none" ID="hdnAmount" class= "amount" Value='<%# DataBinder.Eval(Container.DataItem,"SortAmount") %>' />
<%--akaushik5 added for MITS 37251 Ends--%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField SortExpression="SortCheckDate" HeaderText="Check Date" DataField="CheckDate"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="Selected"
                    FooterStyle-CssClass="Selected"></asp:BoundField>
                <asp:BoundField SortExpression="CtlNumber" HeaderText="Control #" DataField="CtlNumber"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="Selected"
                    FooterStyle-CssClass="Selected"></asp:BoundField>
                <asp:BoundField SortExpression="PayeeName" HeaderText="Payee" DataField="PayeeName"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="Selected"
                    FooterStyle-CssClass="Selected"></asp:BoundField>
                <asp:BoundField SortExpression="ClaimNumber" HeaderText="Claim Number" DataField="ClaimNumber"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="Selected"
                    FooterStyle-CssClass="Selected"></asp:BoundField>
                <asp:BoundField SortExpression="SortAmount" HeaderText="Amount" DataField="Amount"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="Selected"
                    FooterStyle-CssClass="Selected"></asp:BoundField>
                <asp:BoundField SortExpression="AutoCheck" HeaderText="Auto ?" DataField="AutoCheck"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="Selected"
                    FooterStyle-CssClass="Selected"></asp:BoundField>
                    <%--skhare7 R8 Enahnacement--%>
                     <asp:BoundField SortExpression="CombinedPayment" HeaderText="Combined ?" DataField="CombinedPayment"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="Selected"
                    FooterStyle-CssClass="Selected"></asp:BoundField>
                <asp:BoundField SortExpression="SubBankAccount" HeaderText="Sub Bank Account Name"
                    DataField="SubBankAccount" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                    HeaderStyle-CssClass="Selected" FooterStyle-CssClass="Selected"></asp:BoundField>
                <%--Start Debabrata Biswas R6 Batch Printing Retrofit MITS# 19715/20050 Date: 03/17/2010--%>                
                <asp:BoundField SortExpression="OrgHierarchy" HeaderText="OrgHierarchy"
                    DataField="OrgHierarchy" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                    HeaderStyle-CssClass="Selected" FooterStyle-CssClass="Selected"></asp:BoundField>                    
                <%--Start Debabrata Biswas R6 Batch Printing Retrofit MITS# 19715/20050 Date: 03/17/2010--%>
                
                <asp:BoundField SortExpression="PayeeAddress" HeaderText="Payee Address"
                    DataField="PayeeAddress" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                    HeaderStyle-CssClass="Selected" FooterStyle-CssClass="Selected" Visible="false">
                    
                </asp:BoundField>    
                                
            </Columns>
        </asp:GridView>
    </div>
<%--akaushik5 added for MITS 37251 Starts--%>
        <table width ="40%" align="right">
            <tr>
            <td >
                <div align="right">Selected check count: <span id="selectedCount" ></span>
                </div>
                </td>
            <td width="60%">
                
                <div align="right" style ="padding-right:5px;">Total Amount: <span id="lblTotalAmount"></span>
                </div>
                <mc:CurrencyTextbox runat="server" id="TotalAmount" Style="display: none" ReadOnly="true" size = "30" />
                </td>
            </tr>
            </table>
<%--akaushik5 added for MITS 37251 Ends--%>
    <table width="17%">
        <tr>
            <td width="5%">
                <input type="button" value="   OK   " class="button" id="Ok" onclick="javascript: SelectChecksOk(); return false;" style="width:100%"/>
                 <%--<asp:button class="button" runat="server" id="btnOk" RMXRef="" Text="OK" CssClass="button" onClick="btnOk_Click" UseSubmitBehavior="false"  OnClientClick="javascript: AmendPolicyOk();" />--%>
            </td>
            <td width="5%">
                <input type="button" value="Cancel" class="button" id="Cancel" onclick="return SelectChecksCancel()" style="width:100%"/>
            </td>
            <%--				<td width="5%">
					<input type="button" value="View Source" onclick="viewSource()" />
				</td>	--%>
        </tr>
    </table>
    <asp:TextBox runat="server" Style="display: none" ID="accountid" rmxref="/Instance/Document/GetListOfChecks/AccountId" />
    <asp:TextBox runat="server" Style="display: none" ID="fromdate" rmxref="/Instance/Document/GetListOfChecks/FromDate" />
    <asp:TextBox runat="server" Style="display: none" ID="todate" rmxref="/Instance/Document/GetListOfChecks/ToDate" />
    <asp:TextBox runat="server" Style="display: none" ID="fromdateflag" rmxref="/Instance/Document/GetListOfChecks/FromDateFlag" />
    <asp:TextBox runat="server" Style="display: none" ID="todateflag" rmxref="/Instance/Document/GetListOfChecks/ToDateFlag" />
    <%--Start Debabrata Biswas R6 Batch Printing Retrofit MITS# 19715/20050 Date: 03/17/2010--%>
    <asp:TextBox runat="server" Style="display: none" ID="orghierarchypre" rmxref="/Instance/Document/GetListOfChecks/OrgHierarchyPre" />
    <asp:TextBox runat="server" Style="display: none" ID="orghierarchylevelpre" rmxref="/Instance/Document/GetListOfChecks/OrgHierarchyLevelPre" />
    <%--End Debabrata Biswas R6 Batch Printing Retrofit MITS# 19715/20050 Date: 03/17/2010--%>
    <asp:TextBox runat="server" Style="display: none" ID="includeautopayments" rmxref="/Instance/Document/GetListOfChecks/IncludeAutoPayment" />
    <asp:TextBox runat="server" Style="display: none" ID="orderby" rmxref="/Instance/Document/GetListOfChecks/OrderBy" />
    <asp:TextBox runat="server" Style="display: none" ID="orderbydirection" rmxref="/Instance/Document/GetListOfChecks/OrderByDirection" />
    <%--skhare7 R8 enhancement --%>
       <asp:TextBox runat="server" Style="display: none" ID="includecombinedpay" rmxref="/Instance/Document/GetListOfChecks/IncludeCombinedPayment" />
       <%--skhare7 End--%>
     <%--npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
     So introducing Distribution Type and removing out EFTPayment below--%>
    <!--JIRA:438 START: ajohari2-->
    <%--<asp:TextBox runat="server" Style="display: none" ID="printeftpayment" rmxref="/Instance/Document/GetListOfChecks/PrintEFTPayment" />--%>
    <!--JIRA:438 END: ajohari2-->
        <asp:TextBox runat="server" Style="display: none" ID="distributiontype" rmxref="/Instance/Document/GetListOfChecks/DistributionType" />
         <!--Add by kuladeep for mits:22878 -->
    <%--npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
     So introducing Distribution Type and removing out EFTPayment below--%>
    <asp:HiddenField ID="hdnPrtChkLmtId" runat="server" />
    <asp:HiddenField ID="hdnSelectAllCheck" runat="server" />


    </form>
</body>
</html>
