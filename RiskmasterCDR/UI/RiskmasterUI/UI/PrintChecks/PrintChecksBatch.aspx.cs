﻿/**********************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                    *
 **********************************************************************************************
 * 12/09/2014 | RMA-438         | ajohari2   | Underwriters - EFT Payments
 **********************************************************************************************/
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.PrintChecks
{
    public partial class PrintChecksBatch : System.Web.UI.Page
    {
        public XmlDocument Model=null;
        //skhare7 MITS 23664
        string sPrintMode = string.Empty;
          //260e44e7-61ea-4324-ac7c-876a6621ef8b
        //34880aa1-ee69-4766-8c95-2afd5fe434d2
         // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
         // So introducing Distribution Type and removing out EFTPayment below
        //JIRA:438 START: ajohari2
        private string MessageTemplate = "<Message><Authorization></Authorization><Call>" +
            "<Function>PrintChecksAdaptor.PrintChecksBatch</Function></Call><Document><PrintChecks><CheckId /><PrintMode></PrintMode><TransNo></TransNo><RollUpId></RollUpId><BatchNumber></BatchNumber><AccountId></AccountId><IncludeAutoPayment></IncludeAutoPayment><UsingSelection>false</UsingSelection><CheckStockId></CheckStockId><FirstCheckNum></FirstCheckNum><OrderBy></OrderBy><CheckDate></CheckDate><NumAutoChecksToPrint></NumAutoChecksToPrint><IncludeCombinedPayment></IncludeCombinedPayment><DistributionType></DistributionType><IncludeClaimantInPayee /></PrintChecks></Document></Message>";
        //JIRA:438 End: 
        private string FileRequestMessageTemplate = "<Message><Authorization></Authorization><Call>" +
           "<Function>PrintChecksAdaptor.GetPrintCheckFile</Function></Call><Document><PrintChecks><FileName/><FileType/></PrintChecks></Document></Message>";
        //skhare7 MITS 23664 End
        protected void Page_Load(object sender, EventArgs e)
        {
            //tanwar2 - mits 30910 - start
            RMXResourceProvider.GetSpecificObject("lblSingleCheckNotPrinted", RMXResourceProvider.PageId("PrintChecksBatch.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            RMXResourceProvider.GetSpecificObject("lblBatchCheckNotPrinted", RMXResourceProvider.PageId("PrintChecksBatch.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            //tanwar2 - mits 30910 - end
            Model = new XmlDocument();
            try
            {
                if (!Page.IsPostBack)
                {
                    StoreQueryStringValues();
                }
                else
                {
                    if (hdnFileRequest.Value == "1")
                    {
                        hdnFileRequest.Value = "0";
                        GetFile();
                    }
                    if (hdnPrintRequest.Value == "1")
                    {
                        hdnPrintRequest.Value = "0";
                        MessageTemplate = CreateServiceCall();
                        string sReturn = AppHelper.CallCWSService(MessageTemplate.ToString());
                        Model.LoadXml(sReturn);
                        SetValuesFromService();
                        hdnModelXml.Value = Model.OuterXml;                       
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            DataBind();

        }

        private void GetFile()
        {
            if (hdnFileRequest.Value == "1")
            {
                Model.LoadXml(hdnModelXml.Value);
            }

            FileRequestMessageTemplate = AppHelper.ChangeMessageValue(FileRequestMessageTemplate, "//FileName", hdnFileName.Value);
            FileRequestMessageTemplate = AppHelper.ChangeMessageValue(FileRequestMessageTemplate, "//FileType", hdnFileType.Value);
            string sReturn = AppHelper.CallCWSService(FileRequestMessageTemplate.ToString());
            XmlDocument objFile = new XmlDocument();
            objFile.LoadXml(sReturn);

            byte[] pdfbytes = Convert.FromBase64String(objFile.SelectSingleNode("//File").InnerText);
            
            Response.Buffer = true;
            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Charset = "";
            Response.AppendHeader("Content-Encoding", "none;");
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=report.PDF");
            Response.AddHeader("Accept-Ranges", "bytes");
            Response.BinaryWrite(pdfbytes);
            Response.Flush();
            Response.Close();
        }
        protected void btnOk_Click(object sender, EventArgs e)
        {
          
        }
        public string CheckSingleOrBatch()
        {
            if (hdnBatchPrint.Value == "SingleCheck")
            {
                return "Check";
            }
            else
            {
                return "Checks for Batch";
            }
        }
        public string CheckSingleOrCheckBatch()
        {
            if (hdnBatchPrint.Value == "SingleCheck")
            {
                return "Check";
            }
            else
            {
                return "Checks";
            }
        }
        public string ChangeTitle()
        {
            if (hdnBatchPrint.Value == "SingleCheck")
            {
                return "Print Check ";
            }
            else
            {
                return "Print Check Batch";
            }
        }
        private void SetValuesFromService()
        {
            StartNum.Value= AppHelper.GetValueFromDOM(Model,"StartNum");
            EndNum.Value = AppHelper.GetValueFromDOM(Model, "EndNum");
            hdnFileNameAndType.Value = AppHelper.GetValueFromDOM(Model, "@FileNameAndType");
            hdnPrintCheckDetails.Value = AppHelper.GetInnerXmlFromDOM(Model, "PrintCheckDetails");
            //Added by Amitosh for R8 enhancement of EFT Payments
            if (hdnFileNameAndType.Value.Split('~').Length > 1)
            {
                string sFileType = hdnFileNameAndType.Value.Split('~')[1];
                if (sFileType == "eft")
                {
                    if (checkandeob != null)
                    {
                        checkandeob.Visible = false;
                    }
                    if (lbEFT != null)
                    {
                        lbEFT.Visible = true;
                    }
                }
                else
                {
                    if (checkandeob != null)
                    {
                        checkandeob.Visible = true;
                    }
                    if (lbEFT != null)
                    {
                        lbEFT.Visible = false;
                    }
                }
            }
            //End Amitosh
        }
        private void StoreQueryStringValues()
        {
            //skhare7 MITS 23664
             hdnPrintMode.Value = AppHelper.GetQueryStringValue("PrintMode");
             hdnRollUpId.Value = AppHelper.GetQueryStringValue("rollupid");
             //skhare7 MITS 23664 End
            hdnBatchPrint.Value = AppHelper.GetQueryStringValue("IsBatchPrint"); ;

            hdnCheckId.Value = AppHelper.GetQueryStringValue("CheckId");

            hdnBatchId.Value = AppHelper.GetQueryStringValue("BatchId");
      
            hdnAccountId.Value = AppHelper.GetQueryStringValue("AccountId");

            hdnCheckDate.Value = AppHelper.GetQueryStringValue("CheckDate");

            hdnIncludeAutoPayment.Value = AppHelper.GetQueryStringValue("IncludeAutoPayments");

            hdnCheckStockId.Value = AppHelper.GetQueryStringValue("CheckStockId");

            if (hdnPrintMode.Value.CompareTo("PS") == 0)
            {
                hdnFirstCheckNum.Value = AppHelper.GetQueryStringValue("transno");
            }
            else
            {
                hdnFirstCheckNum.Value = AppHelper.GetQueryStringValue("FirstCheck");
            }

            hdnOrderBy.Value = AppHelper.GetQueryStringValue("OrderBy");

            hdnNumAutoChecksToPrint.Value = AppHelper.GetQueryStringValue("NumAutoChecksToPrint");

            hdnUsingSelection.Value = AppHelper.GetQueryStringValue("UsingSelection");

            hdnIncludeClaimantInPayee.Value = AppHelper.GetQueryStringValue("IncludeClaimantInPayee");
           //skhare7 r8 Combined Payment
            hdnIncludeCombPay.Value = AppHelper.GetQueryStringValue("includecombinedpays");
            // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
            // So introducing Distribution Type and removing out EFTPayment below
            //JIRA:438 START: ajohari2
            //if (AppHelper.GetQueryStringValue("EFTPayment").CompareTo(string.Empty) == 0)
            //{
            //    hdnPrintEFTPayment.Value = AppHelper.GetQueryStringValue("PrintEFTPayment");
            //}
            //else
            //{
            //    hdnPrintEFTPayment.Value = AppHelper.GetQueryStringValue("EFTPayment");
            //}
            hdnDistributionType.Value = AppHelper.GetQueryStringValue("DistributionType");
            //JIRA:438 End: 
            // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
            // So introducing Distribution Type and removing out EFTPayment below
        }
        private string  CreateServiceCall()
        {
            string IsBatchPrint = AppHelper.GetFormValue("hdnBatchPrint");
            hdnBatchPrint.Value = IsBatchPrint;
            string CheckId = AppHelper.GetFormValue("hdnCheckId");
            MessageTemplate = AppHelper.ChangeMessageValue(MessageTemplate, "//CheckId", CheckId);
            string CheckBatch = AppHelper.GetFormValue("hdnBatchId");
            MessageTemplate = AppHelper.ChangeMessageValue(MessageTemplate, "//BatchNumber", CheckBatch);
            string BankAccount = AppHelper.GetFormValue("hdnAccountId");
            MessageTemplate = AppHelper.ChangeMessageValue(MessageTemplate, "//AccountId", BankAccount);
            string CheckDate = AppHelper.GetFormValue("hdnCheckDate");
            MessageTemplate = AppHelper.ChangeMessageValue(MessageTemplate, "//CheckDate", CheckDate);
            string IncludeAutoPaymentsFlag = AppHelper.GetFormValue("hdnIncludeAutoPayment");
            MessageTemplate = AppHelper.ChangeMessageValue(MessageTemplate, "//IncludeAutoPayment", IncludeAutoPaymentsFlag);
            string CheckStock = AppHelper.GetFormValue("hdnCheckStockId");
            MessageTemplate = AppHelper.ChangeMessageValue(MessageTemplate, "//CheckStockId", CheckStock);
            string FirstCheck = AppHelper.GetFormValue("hdnFirstCheckNum");
            MessageTemplate = AppHelper.ChangeMessageValue(MessageTemplate, "//FirstCheckNum", FirstCheck);
            string OrderBy = AppHelper.GetFormValue("hdnOrderBy");
            MessageTemplate = AppHelper.ChangeMessageValue(MessageTemplate, "//OrderBy", OrderBy);
            string NumAutoChecksToPrint = AppHelper.GetFormValue("hdnNumAutoChecksToPrint");
            MessageTemplate = AppHelper.ChangeMessageValue(MessageTemplate, "//NumAutoChecksToPrint", NumAutoChecksToPrint);
            string UsingSelection = AppHelper.GetFormValue("hdnUsingSelection");
            MessageTemplate = AppHelper.ChangeMessageValue(MessageTemplate, "//UsingSelection", UsingSelection);
            string IncludeClaimantInPayee = AppHelper.GetFormValue("hdnIncludeClaimantInPayee");
            MessageTemplate = AppHelper.ChangeMessageValue(MessageTemplate, "//IncludeClaimantInPayee", IncludeClaimantInPayee);
            string sPrintMode = AppHelper.GetFormValue("hdnPrintMode");
            MessageTemplate = AppHelper.ChangeMessageValue(MessageTemplate, "//PrintMode", sPrintMode);
            string iRollUpId = AppHelper.GetFormValue("hdnRollUpId");
            MessageTemplate = AppHelper.ChangeMessageValue(MessageTemplate, "//RollUpId", iRollUpId);
            //skhare7 R8 comb Payment
            string IncludeCombPayment = AppHelper.GetFormValue("hdnIncludeCombPay");
            MessageTemplate = AppHelper.ChangeMessageValue(MessageTemplate, "//IncludeCombinedPayment", IncludeCombPayment);
            // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
            // So introducing Distribution Type and removing out EFTPayment below
            //JIRA:438 START: ajohari2
            //string PrintEFTPayment = AppHelper.GetFormValue("hdnPrintEFTPayment");
            //MessageTemplate = AppHelper.ChangeMessageValue(MessageTemplate, "//PrintEFTPayment", PrintEFTPayment);
            //JIRA:438 End: 
            string DistributionType = AppHelper.GetFormValue("hdnDistributionType");
            MessageTemplate = AppHelper.ChangeMessageValue(MessageTemplate, "//DistributionType", DistributionType);
            // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
            // So introducing Distribution Type and removing out EFTPayment below
            return MessageTemplate;
        }
        
        //tanwar2 - mits 30910 - start
        protected string GetZeroCheckMessage()
        {
            string sMessage = string.Empty;
            if (string.Compare(CheckSingleOrCheckBatch(),"SingleCheck",true)==0)
            {
                sMessage = "$0.00 check will not be printed. This payments will be updated to P -  Printed.";
            }
            else
            {
                sMessage = "$0.00 check (s) with control number (ctl_num1, ctl_num2 ) have not printed. Please note that the status has been updated to ‘P - Printed'.";
            }
            return sMessage;
        }
        //tanwar2 - mits 30910 - end
    }
}
