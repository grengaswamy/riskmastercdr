﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Riskmaster.UI.PrintChecks
{
    public partial class ChecksFailedToPrint : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                StoreQueryStringValues();
            }
            lblStartNum.Text = StartNum.Value;
            lblEndNum.Text = EndNum.Value;
            txtFirstFailedCheckNumber.Value = StartNum.Value;
            DataBind();
        }
        private void StoreQueryStringValues()
        {
            StartNum.Value = AppHelper.GetQueryStringValue("StartNum"); ;

            EndNum.Value = AppHelper.GetQueryStringValue("EndNum");                     

        }
    }
}
