﻿/**********************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                    *
 **********************************************************************************************
 * 12/09/2014 | RMA-438         | ajohari2   | Underwriters - EFT Payments
 **********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.PrintChecks
{
    public partial class ShowPreCheck : NonFDMBasePageCWS
    {
        XElement m_objMessageElement = null;
        private string sCWSresponse = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            XElement objTempElement = null;
            bool bReturnStatus = false;
            string sFromDateFlag = string.Empty;
            string sToDateFlag = string.Empty;
            string sIncludeAutoPaymentsFlag = string.Empty;
            string sFromDate = string.Empty;
            string sToDate = string.Empty;
            string sAccountId = string.Empty;
            string sOrderBy = string.Empty;
            string sUsingSelection = string.Empty;
            string sTransIds = string.Empty;
            string sAutoTransIds = string.Empty;
            //npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and removing out EFTPayment below
            string sEFTPayment = string.Empty;//JIRA:438 START: ajohari2
            string sDistributionType = string.Empty;
            //npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and removing out EFTPayment below
            string sOrghierarchyPre = string.Empty;//Debabrata Biswas Batch Printing Retrofit MITS 19715/20050 Date 03/17/2010
            string sOrghierarchyLevelPre = string.Empty;//Debabrata Biswas Batch Printing Retrofit MITS 19715/20050 Date 03/17/2010
            try
            {
                m_objMessageElement = GetMessageTemplate();
                //error.Text = "";
                if (Request.QueryString["fromdateflag"] != null)
                {
                    sFromDateFlag = Request.QueryString["fromdateflag"];
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/FromDateFlag");
                    objTempElement.Value = sFromDateFlag;
                }

                if (Request.QueryString["todateflag"] != null)
                {
                    sToDateFlag = Request.QueryString["todateflag"];
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/ToDateFlag");
                    objTempElement.Value = sToDateFlag;
                }

                if (Request.QueryString["includeautopayments"] != null)
                {
                    sIncludeAutoPaymentsFlag = Request.QueryString["includeautopayments"];
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/IncludeAutoPayment");
                    objTempElement.Value = sIncludeAutoPaymentsFlag;
                }

                if (Request.QueryString["fromdate"] != null)
                {
                    sFromDate = Request.QueryString["fromdate"];
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/FromDate");
                    objTempElement.Value = sFromDate;
                }

                if (Request.QueryString["todate"] != null)
                {
                    sToDate = Request.QueryString["todate"];
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/ToDate");
                    objTempElement.Value = sToDate;
                }

                if (Request.QueryString["accountid"] != null)
                {
                    sAccountId = Request.QueryString["accountid"];
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/AccountId");
                    objTempElement.Value = sAccountId;
                }

                if (Request.QueryString["usingselection"] != null)
                {
                    sUsingSelection = Request.QueryString["usingselection"];
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/UsingSelection");
                    objTempElement.Value = sUsingSelection;
                }

                if (Request.QueryString["selectedchecksids"] != null)
                {
                    sTransIds = Request.QueryString["selectedchecksids"];
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/TransIds");
                    objTempElement.Value = sTransIds;
                }

                if (Request.QueryString["selectedautochecksids"] != null)
                {
                    sAutoTransIds = Request.QueryString["selectedautochecksids"];
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/AutoTransIds");
                    objTempElement.Value = sAutoTransIds;
                }

                if (Request.QueryString["orderby"] != null)
                {
                    sOrderBy = Request.QueryString["orderby"];
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/OrderBy");
                    objTempElement.Value = sOrderBy;
                }

                //Start Debabrata Biswas Batch Printing Retrofit MITS 19715/20050 Date: 03/17/2010
                if (Request.QueryString["orghierarchypre"] != null)
                {
                    sOrghierarchyPre = Request.QueryString["orghierarchypre"];
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/OrghierarchyPre");
                    objTempElement.Value = sOrghierarchyPre;
                }

                if (Request.QueryString["orghierarchylevelpre"] != null)
                {
                    sOrghierarchyLevelPre = Request.QueryString["orghierarchylevelpre"];
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/OrghierarchyLevelPre");
                    objTempElement.Value = sOrghierarchyLevelPre;
                }
                //End Debabrata Biswas Batch Printing Retrofit MITS 19715/20050 Date: 03/17/2010
                //skhare7 r8 combined Payment
                if (Request.QueryString["includecombinedpays"] != null)
                {
                    sOrghierarchyLevelPre = Request.QueryString["includecombinedpays"];
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/IncludeCombinedPayment");
                    objTempElement.Value = sOrghierarchyLevelPre;
                }
                //npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and removing out EFTPayment below
                //JIRA:438 START: ajohari2
                //if (Request["EFTPayment"] != null)
                //{
                //    sEFTPayment = Request["EFTPayment"];
                //    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/PrintEFTPayment");
                //    objTempElement.Value = sEFTPayment;
                //}
                //JIRA:438 End: 

                if (Request.QueryString["DistributionType"] != null)
                {
                    sDistributionType = Request.QueryString["DistributionType"];
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/DistributionType");
                    objTempElement.Value = sDistributionType;
                }
                //npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and removing out EFTPayment below
                //if (HttpContext.Current.Request.Cookies["SessionId"] != null)
                //{
                //    objTempElement = m_objMessageElement.XPathSelectElement("./Authorization");
                //    objTempElement.Value = HttpContext.Current.Request.Cookies["SessionId"].Value;
                //}

                if (!IsPostBack)
                {
                    bReturnStatus = CallCWSFunctionBind("PrintChecksAdaptor.PreCheckDetail", out sCWSresponse, m_objMessageElement);
                }
                objTempElement = XElement.Parse(sCWSresponse);
                objTempElement = objTempElement.XPathSelectElement("./Document/PrintChecks/File");
                //TextBox txtPdfFile = (TextBox)this.Form.FindControl("File");
                if (objTempElement != null)
                {
                    byte[] pdfbytes = Convert.FromBase64String(objTempElement.Value);

                    Response.Buffer = true;
                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Charset = "";
                    Response.AppendHeader("Content-Encoding", "none;");
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-Disposition", string.Format("inline; filename=PRECHECKREGISTER.PDF"));
                    Response.AddHeader("Accept-Ranges", "bytes");
                    Response.BinaryWrite(pdfbytes);
                    Response.Flush();
                    Response.Close();
                } // if
                else
                {
                   // error.Text = "true";
                } // else

            } // try
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                //ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            } // catch

            //DataBind();
        }
        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization></Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>");
            sXml = sXml.Append("<PrintChecks>");
            sXml = sXml.Append("<FromDateFlag></FromDateFlag>");
            sXml = sXml.Append("<ToDateFlag></ToDateFlag>");
            sXml = sXml.Append("<AttachedClaimOnly></AttachedClaimOnly>");
            sXml = sXml.Append("<IncludeAutoPayment></IncludeAutoPayment>");
            //skhare7 r8 combined Payment
            sXml = sXml.Append("<IncludeCombinedPayment></IncludeCombinedPayment>");
            //skhare7 r8
            //npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and removing out EFTPayment below
            //JIRA:438 START: ajohari2
            //sXml = sXml.Append("<PrintEFTPayment></PrintEFTPayment>");
            //JIRA:438 End: 
            sXml = sXml.Append("<DistributionType></DistributionType>");
            //npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and removing out EFTPayment below
            sXml = sXml.Append("<FromDate></FromDate>");
            sXml = sXml.Append("<ToDate></ToDate>");
            sXml = sXml.Append("<AccountId></AccountId>");
            //Start Debabrata Biswas Batch Printing Retrofit Mits 19715/20050 Date: 03/17/2010
            sXml = sXml.Append("<OrghierarchyPre></OrghierarchyPre>");
            sXml = sXml.Append("<OrghierarchyLevelPre></OrghierarchyLevelPre>");
            //End Debabrata Biswas Batch Printing Retrofit Mits 19715/20050 Date: 03/17/2010
            sXml = sXml.Append("<UsingSelection></UsingSelection>");
            sXml = sXml.Append("<TransIds></TransIds>");
            sXml = sXml.Append("<AutoTransIds></AutoTransIds>");
            sXml = sXml.Append("<OrderBy></OrderBy>");
            
            sXml = sXml.Append("</PrintChecks></Document></Message>");


            XElement oTemplate = XElement.Parse(sXml.ToString());

            return oTemplate;
        }
    }
}
