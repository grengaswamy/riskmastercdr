﻿/**********************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                    *
 **********************************************************************************************
 * 12/09/2014 | RMA-438         | ajohari2   | Underwriters - EFT Payments
 **********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Data;
using System.Xml;
using System.Collections;

namespace Riskmaster.UI.PrintChecks
{
    public partial class SelectChecksFrame : NonFDMBasePageCWS
    {
        XElement m_objMessageElement = null;
        private string sCWSresponse = "";
        private string m_strSortExp = "";
        string sDirection = "ASC";  //the default sort direction
        private  bool bIsAddressSelect=false;
		
		// npadhy JIRA 6421 The BL will send the Address 2 and City separated by the separator.
		// We need to replace it with LineBreak
        private string sSeparator = "@^*";
        private string sLineBreak = "<br/>";
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Expires = -1441;
            XElement objTempElement = null;
            bool bReturnStatus = false;
            string sFromDateFlag = string.Empty;
            string sToDateFlag = string.Empty;
            string sIncludeAutoPaymentsFlag = string.Empty;
            string sFromDate = string.Empty;
            string sToDate = string.Empty;
            string sAccountId = string.Empty;
            string sOrderBy = string.Empty;
            string sOrderByDirection = string.Empty;
            //Debabrata Biswas Batch printing R6 Retrofit MITS # 19715/20050 Date: 03/17/2010/20050
            string sOrgHierarchyPre = string.Empty;
            //Debabrata Biswas Batch printing R6 Retrofit MITS # 19715/20050 Date: 03/17/2010/20050
            string sOrgHierarchyLevelPre =string.Empty;
            //skhare7 R8 enhancement
            string sIncludeCombinedPaymentsFlag = string.Empty;
            //skhare7 R8 end Combined Pay
            // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and removing out EFTPayment below
            //JIRA:438 START: ajohari2
            //string sPrintEFTPayment = string.Empty;
            //JIRA:438 End: 
            string sDistributionType = string.Empty;
			// npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and removing out EFTPayment below
            
            try
            {
                m_objMessageElement = GetMessageTemplate();

                if(!IsPostBack)
                {
                    if (Request.QueryString["fromdateflag"] != null)
                    {
                        sFromDateFlag = Request.QueryString["fromdateflag"];
                        objTempElement = m_objMessageElement.XPathSelectElement("./Document/GetListOfChecks/FromDateFlag");
                        objTempElement.Value = sFromDateFlag;
                        this.fromdateflag.Text = sFromDateFlag;
                    }

                    if (Request.QueryString["todateflag"] != null)
                    {
                        sToDateFlag = Request.QueryString["todateflag"];
                        objTempElement = m_objMessageElement.XPathSelectElement("./Document/GetListOfChecks/ToDateFlag");
                        objTempElement.Value = sToDateFlag;
                        this.todateflag.Text = sToDateFlag;
                    }

                    if (Request.QueryString["includeautopayments"] != null)
                    {
                        sIncludeAutoPaymentsFlag = Request.QueryString["includeautopayments"];
                        objTempElement = m_objMessageElement.XPathSelectElement("./Document/GetListOfChecks/IncludeAutoPayment");
                        objTempElement.Value = sIncludeAutoPaymentsFlag;
                        this.includeautopayments.Text = sIncludeAutoPaymentsFlag;
                    }

                    if (Request.QueryString["fromdate"] != null)
                    {
                        sFromDate = Request.QueryString["fromdate"];
                        objTempElement = m_objMessageElement.XPathSelectElement("./Document/GetListOfChecks/FromDate");
                        objTempElement.Value = sFromDate;
                        this.fromdate.Text = sFromDate;
                    }

                    if (Request.QueryString["todate"] != null)
                    {
                        sToDate = Request.QueryString["todate"];
                        objTempElement = m_objMessageElement.XPathSelectElement("./Document/GetListOfChecks/ToDate");
                        objTempElement.Value = sToDate;
                        this.todate.Text = sToDate;
                    }

                    if (Request.QueryString["accountid"] != null)
                    {
                        sAccountId = Request.QueryString["accountid"];
                        objTempElement = m_objMessageElement.XPathSelectElement("./Document/GetListOfChecks/AccountId");
                        objTempElement.Value = sAccountId;
                        this.accountid.Text = sAccountId;
                    }

                    if (Request.QueryString["orderby"] != null)
                    {
                        sOrderBy = Request.QueryString["orderby"];
                        objTempElement = m_objMessageElement.XPathSelectElement("./Document/GetListOfChecks/OrderBy");
                        objTempElement.Value = sOrderBy;
                        this.orderby.Text = sOrderBy;
                    }

                    if (Request.QueryString["orderbydirection"] != null)
                    {
                        sOrderByDirection = Request.QueryString["orderbydirection"];
                        objTempElement = m_objMessageElement.XPathSelectElement("./Document/GetListOfChecks/OrderByDirection");
                        objTempElement.Value = sOrderByDirection;
                        this.orderbydirection.Text = sOrderByDirection;
                    }
                    //Start Debabrata Biswas Batch printing R6 Retrofit MITS # 19715/20050 Date: 03/17/2010
                    if (Request.QueryString["orghierarchypre"] != null)
                    {
                        sOrgHierarchyPre = Request.QueryString["orghierarchypre"];
                        objTempElement = m_objMessageElement.XPathSelectElement("./Document/GetListOfChecks/OrgHierarchyPre");
                        objTempElement.Value = sOrgHierarchyPre;
                        this.orghierarchypre.Text = sOrgHierarchyPre;
                    }
                    if (Request.QueryString["orghierarchylevelpre"] != null)
                    {
                        sOrgHierarchyLevelPre = Request.QueryString["orghierarchylevelpre"];
                        objTempElement = m_objMessageElement.XPathSelectElement("./Document/GetListOfChecks/OrgHierarchyLevelPre");
                        objTempElement.Value = sOrgHierarchyLevelPre;
                        this.orghierarchylevelpre.Text = sOrgHierarchyLevelPre;
                    }
                    //End Debabrata Biswas Batch printing R6 Retrofit MITS # 19715/20050 Date: 03/17/2010
                    //skhare7 R8 enhancement Combined Pay
                    if (Request.QueryString["includecombinedpays"] != null)
                    {
                        sIncludeCombinedPaymentsFlag = Request.QueryString["includecombinedpays"];
                        objTempElement = m_objMessageElement.XPathSelectElement("./Document/GetListOfChecks/IncludeCombinedPayment");
                        objTempElement.Value = sIncludeCombinedPaymentsFlag;
                        includecombinedpay.Text = sIncludeCombinedPaymentsFlag;
                    }
                    //skhare7 R8 end
                    // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and removing out EFTPayment below
                    //JIRA:438 START: ajohari2
                    //if (Request.QueryString["EFTPayment"] != null)
                    //{
                    //    sPrintEFTPayment = Request.QueryString["EFTPayment"];
                    //    objTempElement = m_objMessageElement.XPathSelectElement("./Document/GetListOfChecks/PrintEFTPayment");
                    //    objTempElement.Value = sPrintEFTPayment;
                    //    this.printeftpayment.Text = sPrintEFTPayment;
                    //}
                    //JIRA:438 End: 

                    if (Request.QueryString["DistributionType"] != null)
                    {
                        sDistributionType = Request.QueryString["DistributionType"];
                        objTempElement = m_objMessageElement.XPathSelectElement("./Document/GetListOfChecks/DistributionType");
                        objTempElement.Value = sDistributionType;
                        this.distributiontype.Text = sDistributionType;
                    }
                    // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and removing out EFTPayment below
                    //if (HttpContext.Current.Request.Cookies["SessionId"] != null)
                    //{
                    //    objTempElement = m_objMessageElement.XPathSelectElement("./Authorization");
                    //    objTempElement.Value = HttpContext.Current.Request.Cookies["SessionId"].Value;
                    //}

                    bReturnStatus = CallCWSFunctionBind("PrintChecksAdaptor.GetListOfChecks", out sCWSresponse, m_objMessageElement);

                    BindpageControls(sCWSresponse);
                } // if

               
            } // try
            catch(Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            } // catch

        }

        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization></Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>");
            sXml = sXml.Append("<GetListOfChecks>");
			sXml = sXml.Append("<FromDateFlag></FromDateFlag>");
			sXml = sXml.Append("<ToDateFlag></ToDateFlag>");
			sXml = sXml.Append("<IncludeAutoPayment></IncludeAutoPayment>");
            //skhare7 r8 combined Pay
            sXml = sXml.Append("<IncludeCombinedPayment></IncludeCombinedPayment>");
            //skhare7 r8 combined Pay End
			sXml = sXml.Append("<FromDate></FromDate>");
			sXml = sXml.Append("<ToDate></ToDate>");
			sXml = sXml.Append("<AccountId></AccountId>");
            //Debabrata Biswas Batch printing R6 Retrofit MITS # 19715/20050 Date: 03/17/2010
            sXml = sXml.Append("<OrgHierarchyPre></OrgHierarchyPre>");
            //Debabrata Biswas Batch printing R6 Retrofit MITS # 19715/20050 Date: 03/17/2010
            sXml = sXml.Append("<OrgHierarchyLevelPre></OrgHierarchyLevelPre>");    
			sXml = sXml.Append("<ControlNumber></ControlNumber>");
            // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and removing out EFTPayment below
            //JIRA:438 START: ajohari2
            //sXml = sXml.Append("<PrintEFTPayment></PrintEFTPayment>");
            //JIRA:438 End: 
            sXml = sXml.Append("<DistributionType></DistributionType>");
            // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and removing out EFTPayment below
            sXml = sXml.Append("<OrderBy></OrderBy>");
            sXml = sXml.Append("<OrderByDirection></OrderByDirection>");
            sXml = sXml.Append("</GetListOfChecks></Document></Message>");


            XElement oTemplate = XElement.Parse(sXml.ToString());

            return oTemplate;
        }

        private void BindpageControls(string sreturnValue)
        {
            try
            {



                DataSet usersRecordsSet = null;
                XmlDocument usersXDoc = new XmlDocument();
                usersXDoc.LoadXml(sreturnValue);
                usersRecordsSet = ConvertXmlDocToDataSet(usersXDoc);
                string sHeaderText = string.Empty;
                //Add by kuladeep for mits:22878
                XElement oElement = null;
                XmlNode xPrintCheckLimit = null;

                GridView gvSelectChecksFrame = (GridView)this.Form.FindControl("gvSelectChecksFrame");

                //Add by kuladeep for mits:22878 Start
                if (usersXDoc.SelectSingleNode("/ResultMessage/Document/ListOfChecks/PrintChecks") != null)
                {
                    xPrintCheckLimit = usersXDoc.SelectSingleNode("/ResultMessage/Document/ListOfChecks/PrintChecks");
                    if (xPrintCheckLimit != null)
                    {
                        oElement = XElement.Parse(xPrintCheckLimit.OuterXml);
                        if (oElement != null)
                        {
                            hdnPrtChkLmtId.Value = oElement.Attribute("PrintCheckLimit").Value;
                        }
                    }
                }
                //Add by kuladeep for mits:22878 Start


                //hlv MITS 23153
                CheckBox cbAll = null;
                ArrayList alSelectCheckState = null;
                ArrayList alSelectAutoCheckState = null;

                if (gvSelectChecksFrame.HeaderRow != null)
                {
                    cbAll = gvSelectChecksFrame.HeaderRow.FindControl("ChkAll") as CheckBox;
                }

                if (Request["SelectCheck"] != null && Request["SelectCheck"].Length > 0)
                {
                    alSelectCheckState = ArrayList.Adapter(Request["SelectCheck"].Split(','));
                }

                if (Request["SelectAutoCheck"] != null && Request["SelectAutoCheck"].Length > 0)
                {
                    alSelectAutoCheckState = ArrayList.Adapter(Request["SelectAutoCheck"].Split(','));
                }
                //alCheckboxState.
                //Start Debabrata Biswas Batch Printing Retrofit R6 MITS# 19715/20050 Date: 03/17/2010
                switch(orghierarchylevelpre.Text)
                {
                    case "1005":sHeaderText = "Client Code";break;
                    case "1006":sHeaderText = "Company Code";break;
                    case "1007":sHeaderText = "Operation Code";break;
                    case "1008":sHeaderText = "Region Code";break;
                    case "1009":sHeaderText = "Division Code";break;
                    case "1010":sHeaderText = "Location Code";break;
                    case "1011":sHeaderText = "Facility Code";break;
                    case "1012":sHeaderText = "Department Code";break;
                }
                gvSelectChecksFrame.Columns[9].HeaderText = sHeaderText;
                //End Debabrata Biswas Batch Printing Retrofit R6 MITS# 19715/20050 Date: 03/17/2010
                if(usersRecordsSet.Tables["check"] != null)
                {
                    usersRecordsSet.Tables["check"].Columns.Add("CheckBoxName");
                    usersRecordsSet.Tables["check"].Columns.Add("IsSelected");//hlv MITS 23153
                    foreach (DataRow dr in usersRecordsSet.Tables["check"].Rows)
                    {
                       
                        if (dr["AutoCheck"].ToString().ToUpper() == "TRUE")
                        {
                            dr["CheckBoxName"] = "SelectAutoCheck";
                            //hlv MITS 23153
                            if (alSelectAutoCheckState != null && alSelectAutoCheckState.IndexOf(dr["TransId"]) >= 0)
                            {
                                dr["IsSelected"] = "checked";
                            }
                        } // if
                        else
                        {
                            dr["CheckBoxName"] = "SelectCheck";
                            //hlv MITS 23153
                            if (alSelectCheckState != null && alSelectCheckState.IndexOf(dr["TransId"]) >= 0)
                            {
                                dr["IsSelected"] = "checked";
                            }
                        } // else

                        //hlv MITS 23153
                        if (cbAll != null && cbAll.Checked)
                        {
                            dr["IsSelected"] = "checked";
                        }
                    } // foreach

                    if (m_strSortExp != "")
                        usersRecordsSet.Tables["check"].DefaultView.Sort = m_strSortExp + " " + sDirection;

                    gvSelectChecksFrame.DataSource = usersRecordsSet.Tables["check"].DefaultView;
                    //pkandhari JIRA 6421
                    if (usersXDoc.SelectSingleNode("/ResultMessage/Document/ListOfChecks/Checks/@IsAddressSelect") != null)
                    {
                        bIsAddressSelect = Convert.ToBoolean(usersXDoc.SelectSingleNode("/ResultMessage/Document/ListOfChecks/Checks/@IsAddressSelect").Value);
                        gvSelectChecksFrame.Columns[10].Visible = bIsAddressSelect ? true : false;
                    }
                    //pkandhari JIRA 6421
                    gvSelectChecksFrame.DataBind();
                } // if
                else
                {
                    gvSelectChecksFrame.DataSource = new ArrayList();
                    gvSelectChecksFrame.DataBind();
                } // else
                //hlv MITS 23153
                (gvSelectChecksFrame.HeaderRow.FindControl("ChkAll") as CheckBox).Checked = (cbAll != null && cbAll.Checked);
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                //ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            } // catch

        }

        //pkandhari JIRA 6421
        protected void gvSelectChecksFrame_RowDataBound(object sender, GridViewRowEventArgs  e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow & bIsAddressSelect)
            {
                // npadhy JIRA 6421 If the Text lenght is greater than 30, then we need to show the epllipsis.
                // We wrap the ellipsis within a span and set a style to it. So that we can control the behavior of tooltip using this class

                int separatorLocation = e.Row.Cells[10].Text.IndexOf(sSeparator);

               // Separator location is > 30 then we do not need to add the length of Separator while taking out 30 characters
                if (e.Row.Cells[10].Text.Length > 30 + sSeparator.Length)
                {
                    e.Row.Cells[10].Attributes.Add("tooltip", HttpUtility.HtmlDecode(e.Row.Cells[10].Text.Replace(sSeparator, sLineBreak)));

                    if (separatorLocation >= 30 || separatorLocation < 30 - sSeparator.Length)
                    {
                        e.Row.Cells[10].Text = HttpUtility.HtmlDecode(e.Row.Cells[10].Text.Substring(0, 30 + (separatorLocation >= 30 ? 0 : sSeparator.Length)).Replace(sSeparator, sLineBreak) + "<span class='tooltiphover'>(...)</span>");
                    }
                    else
                        e.Row.Cells[10].Text = HttpUtility.HtmlDecode(e.Row.Cells[10].Text.Substring(0, 30 + (sSeparator.Length - (30 - separatorLocation))).Replace(sSeparator, sLineBreak) + "<span class='tooltiphover'>(...)</span>");
                }
                else
                {
                    e.Row.Cells[10].Text = HttpUtility.HtmlDecode(e.Row.Cells[10].Text.Replace(sSeparator, sLineBreak));

                }
                e.Row.Cells[10].Width = 170;
            }
        }
        //pkandhari JIRA 6421


        protected void gvSelectChecksFrame_Sorting(object sender, GridViewSortEventArgs e)
        {
            m_strSortExp = e.SortExpression;
            bool bReturnStatus = false;

            if (orderby.Text == m_strSortExp) 
            {
                sDirection = (orderbydirection.Text == "ASC" ? "DESC" : "ASC");
            }
            orderbydirection.Text = sDirection;
            orderby.Text = m_strSortExp;
            m_objMessageElement = GetMessageTemplate();
            bReturnStatus = CallCWSFunctionBind("PrintChecksAdaptor.GetListOfChecks", out sCWSresponse, m_objMessageElement);

            BindpageControls(sCWSresponse);
        }

        protected void Refresh_Click(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            orderbydirection.Text = "ascending";
            orderby.Text = "CtlNumber";

            m_objMessageElement = GetMessageTemplate();

            bReturnStatus = CallCWSFunctionBind("PrintChecksAdaptor.GetListOfChecks", out sCWSresponse, m_objMessageElement);

            BindpageControls(sCWSresponse);
        }

    }
}
