﻿/**********************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                    *
 **********************************************************************************************
 * 12/09/2014 | RMA-438         | ajohari2   | Underwriters - EFT Payments
 **********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.PrintChecks
{
    public partial class PreCheckDetailFrame : NonFDMBasePageCWS
    {
        XElement m_objMessageElement = null;
        private string sCWSresponse = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            XElement objTempElement = null;
            bool bReturnStatus = false;
            string sFromDateFlag = string.Empty;
            string sToDateFlag = string.Empty;
            string sIncludeAutoPaymentsFlag = string.Empty;
            string sFromDate = string.Empty;
            string sToDate = string.Empty;
            string sAccountId = string.Empty;
            string sOrderBy = string.Empty;
            string sUsingSelection = string.Empty;
            string sTransIds = string.Empty;
            string sAutoTransIds = string.Empty;
            // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
            // So introducing Distribution Type and removing out EFTPayment
            //string sEFTPayment = string.Empty;//JIRA:438 START: ajohari2
            string sDistributionType = string.Empty;
            // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
            // So introducing Distribution Type and removing out EFTPayment
            string sOrghierarchyPre = string.Empty;//Debabrata Biswas Batch Printing Retrofit MITS 19715/20050 Date 03/17/2010
            string sOrghierarchyLevelPre = string.Empty;//Debabrata Biswas Batch Printing Retrofit MITS 19715/20050 Date 03/17/2010
            
            try
            {
                m_objMessageElement = GetMessageTemplate();
                error.Text = "";
                //if (Request.QueryString["fromdateflag"] != null)
                if (Request["fromdateflag"] != null)   //MITS 24942 hlv 3/28/2012
                {
                    //sFromDateFlag = Request.QueryString["fromdateflag"];
                    sFromDateFlag = Request["fromdateflag"]; //MITS 24942 hlv 3/28/2012
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/FromDateFlag");
                    objTempElement.Value = sFromDateFlag;
                }

                //if (Request.QueryString["todateflag"] != null)
                if (Request["todateflag"] != null) //MITS 24942 hlv 3/28/2012
                {
                    //sToDateFlag = Request.QueryString["todateflag"];
                    sToDateFlag = Request["todateflag"]; //MITS 24942 hlv 3/28/2012
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/ToDateFlag");
                    objTempElement.Value = sToDateFlag;
                }

                //if (Request.QueryString["includeautopayments"] != null)
                if (Request["includeautopayments"] != null)  //MITS 24942 hlv 3/28/2012
                {
                    //sIncludeAutoPaymentsFlag = Request.QueryString["includeautopayments"];
                    sIncludeAutoPaymentsFlag = Request["includeautopayments"]; //MITS 24942 hlv 3/28/2012
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/IncludeAutoPayment");
                    objTempElement.Value = sIncludeAutoPaymentsFlag;
                }

                //if (Request.QueryString["fromdate"] != null)
                if (Request["fromdate"] != null)  //MITS 24942 hlv 3/28/2012
                {
                    //sFromDate = Request.QueryString["fromdate"];
                    sFromDate = Request["fromdate"];  //MITS 24942 hlv 3/28/2012
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/FromDate");
                    objTempElement.Value = sFromDate;
                }

                //if (Request.QueryString["todate"] != null)
                if (Request["todate"] != null)  //MITS 24942 hlv 3/28/2012
                {
                    //sToDate = Request.QueryString["todate"];
                    sToDate = Request["todate"];  //MITS 24942 hlv 3/28/2012
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/ToDate");
                    objTempElement.Value = sToDate;
                }

                //if (Request.QueryString["accountid"] != null)
                if (Request["accountid"] != null)  //MITS 24942 hlv 3/28/2012
                {
                    //sAccountId = Request.QueryString["accountid"];
                    sAccountId = Request["accountid"]; //MITS 24942 hlv 3/28/2012
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/AccountId");
                    objTempElement.Value = sAccountId;
                }

                //if (Request.QueryString["usingselection"] != null)
                if (Request["usingselection"] != null)  //MITS 24942 hlv 3/28/2012
                {
                    //sUsingSelection = Request.QueryString["usingselection"];
                    sUsingSelection = Request["usingselection"]; //MITS 24942 hlv 3/28/2012
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/UsingSelection");
                    objTempElement.Value = sUsingSelection;
                }

                //if (Request.QueryString["selectedchecksids"] != null)
                if (Request["selectedchecksids"] != null)   //MITS 24942 hlv 3/28/2012
                {
                    //sTransIds = Request.QueryString["selectedchecksids"];
                    sTransIds = Request["selectedchecksids"];  //MITS 24942 hlv 3/28/2012
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/TransIds");
                    objTempElement.Value = sTransIds;
                }

                //if (Request.QueryString["selectedautochecksids"] != null)
                if (Request["selectedautochecksids"] != null)   //MITS 24942 hlv 3/28/2012
                {
                    //sAutoTransIds = Request.QueryString["selectedautochecksids"];
                    sAutoTransIds = Request["selectedautochecksids"];    //MITS 24942 hlv 3/28/2012
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/AutoTransIds");
                    objTempElement.Value = sAutoTransIds;
                }

                //if (Request.QueryString["orderby"] != null)
                if (Request["orderby"] != null)  //MITS 24942 hlv 3/28/2012
                {
                    //sOrderBy = Request.QueryString["orderby"];
                    sOrderBy = Request["orderby"];  //MITS 24942 hlv 3/28/2012
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/OrderBy");
                    objTempElement.Value = sOrderBy;
                }

                //Start Debabrata Biswas Batch Printing Retrofit MITS 19715/20050 Date: 03/17/2010
                //if (Request.QueryString["orghierarchypre"] != null)
                if (Request["orghierarchypre"] != null) //MITS 24942 hlv 3/28/2012
                {
                    //sOrghierarchyPre = Request.QueryString["orghierarchypre"];
                    sOrghierarchyPre = Request["orghierarchypre"];//MITS 24942 hlv 3/28/2012
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/OrghierarchyPre");
                    objTempElement.Value = sOrghierarchyPre;
                }

                //if (Request.QueryString["orghierarchylevelpre"] != null)
                if (Request["orghierarchylevelpre"] != null)     //MITS 24942 hlv 3/28/2012
                {
                    //sOrghierarchyLevelPre = Request.QueryString["orghierarchylevelpre"];
                    sOrghierarchyLevelPre = Request["orghierarchylevelpre"];  //MITS 24942 hlv 3/28/2012
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/OrghierarchyLevelPre");
                    objTempElement.Value = sOrghierarchyLevelPre;
                }
                //End Debabrata Biswas Batch Printing Retrofit MITS 19715/20050 Date: 03/17/2010
                //skhare7 r8 combined Payment
                //if (Request.QueryString["includecombinedpays"] != null)
                if (Request["includecombinedpays"] != null)     //MITS 24942 hlv 3/28/2012
                {
                    //sOrghierarchyLevelPre = Request.QueryString["includecombinedpays"];
                    sOrghierarchyLevelPre = Request["includecombinedpays"];  //MITS 24942 hlv 3/28/2012
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/IncludeCombinedPayment");
                    objTempElement.Value = sOrghierarchyLevelPre;
                }

                // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
                // So introducing Distribution Type and removing out EFTPayment
                //JIRA:438 START: ajohari2
                //if (Request["EFTPayment"] != null)    
                //{
                //    sEFTPayment = Request["EFTPayment"];  
                //    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/PrintEFTPayment");
                //    objTempElement.Value = sEFTPayment;
                //}   
                //JIRA:438 End: 
                if (Request["DistributionType"] != null)
                {
                    sDistributionType = Request["DistributionType"];
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/DistributionType");
                    objTempElement.Value = sDistributionType;
                }
                // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
                // So introducing Distribution Type and removing out EFTPayment
                //if (HttpContext.Current.Request.Cookies["SessionId"] != null)
                //{
                //    objTempElement = m_objMessageElement.XPathSelectElement("./Authorization");
                //    objTempElement.Value = HttpContext.Current.Request.Cookies["SessionId"].Value;
                //}

                if (!IsPostBack)
                {
                    bReturnStatus = CallCWSFunctionBind("PrintChecksAdaptor.PreCheckDetail", out sCWSresponse, m_objMessageElement);
                }
                objTempElement = XElement.Parse(sCWSresponse);
                objTempElement = objTempElement.XPathSelectElement("./Document/PrintChecks/File");
                //TextBox txtPdfFile = (TextBox)this.Form.FindControl("File");
                if (objTempElement != null)
                {
                    byte[] pdfbytes = Convert.FromBase64String(objTempElement.Value);

                    //MITS 24942 hlv 3/28/2012
                    Response.Clear();
                    Response.Buffer = true;
                    //Response.ClearContent();
                    //Response.ClearHeaders();
                    //Response.Charset = "";
                    //Response.AppendHeader("Content-Encoding", "none;");
                    Response.AddHeader("Content-Length", pdfbytes.Length.ToString());
                    Response.AddHeader("Content-Disposition", string.Format("inline; filename=PRECHECKREGISTER.PDF"));
                    Response.AddHeader("Pragma", "cache");
                    Response.AddHeader("Cache-Control", "private");
                    Response.AddHeader("Accept-Ranges", "bytes");
                    Response.ContentType = "application/pdf";
                    Response.BinaryWrite(pdfbytes);
                    Response.Flush();
                    Response.Close();
		// akaushik5 Changed for MITS 35846 Starts
                    //Response.End();
                    Response.SuppressContent = true;
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
		// akaushik5 Changed for MITS 35846 Ends
                } // if
                else
                {
                    error.Text = "true";
                } // else

            } // try
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                //ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            } // catch


        }

        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization></Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>");
            sXml = sXml.Append("<PrintChecks>");
            sXml = sXml.Append("<FromDateFlag></FromDateFlag>");
            sXml = sXml.Append("<ToDateFlag></ToDateFlag>");
            sXml = sXml.Append("<AttachedClaimOnly></AttachedClaimOnly>");
            sXml = sXml.Append("<IncludeAutoPayment></IncludeAutoPayment>");
            //skhare7 r8 combined Payment
            sXml = sXml.Append("<IncludeCombinedPayment></IncludeCombinedPayment>");
            //skhare7 r8
            // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
            // So introducing Distribution Type and removing out EFTPayment
            //JIRA:438 START: ajohari2
            //sXml = sXml.Append("<PrintEFTPayment></PrintEFTPayment>");
            //JIRA:438 End: 
            sXml = sXml.Append("<DistributionType></DistributionType>");
            // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
            // So introducing Distribution Type and removing out EFTPayment
            sXml = sXml.Append("<FromDate></FromDate>");
            sXml = sXml.Append("<ToDate></ToDate>");
            sXml = sXml.Append("<AccountId></AccountId>");
            //Start Debabrata Biswas Batch Printing Retrofit Mits 19715/20050 Date: 03/17/2010
            sXml = sXml.Append("<OrghierarchyPre></OrghierarchyPre>");
            sXml = sXml.Append("<OrghierarchyLevelPre></OrghierarchyLevelPre>");
            //End Debabrata Biswas Batch Printing Retrofit Mits 19715/20050 Date: 03/17/2010
            sXml = sXml.Append("<UsingSelection></UsingSelection>");
            sXml = sXml.Append("<TransIds></TransIds>");
            sXml = sXml.Append("<AutoTransIds></AutoTransIds>");
            sXml = sXml.Append("<OrderBy></OrderBy>");
            sXml = sXml.Append("</PrintChecks></Document></Message>");


            XElement oTemplate = XElement.Parse(sXml.ToString());

            return oTemplate;
        }
    }
}
