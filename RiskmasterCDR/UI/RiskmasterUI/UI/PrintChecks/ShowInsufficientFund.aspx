﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShowInsufficientFund.aspx.cs" Inherits="Riskmaster.UI.PrintChecks.ShowInsuffiecientFund"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Insufficient Funds Reports</title>
</head>
<body>
        <form id="frmData" runat="server" style="height:100%">
      
        <%--npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks--%>  
        <iframe  src='InsufficientFundFrame.aspx?fromdate=<%# Request.QueryString["fromdate"]%>&todate=<%# Request.QueryString["todate"]%>&fromdateflag=<%# Request.QueryString["fromdateflag"]%>&todateflag=<%# Request.QueryString["todateflag"]%>&includeautopayments=<%# Request.QueryString["includeautopayments"]%>&accountid=<%# Request.QueryString["accountid"]%>&selectedchecksids=<%# Request.QueryString["selectedchecksids"]%>&selectedautochecksids=<%# Request.QueryString["selectedautochecksids"]%>&usingselection=<%# Request.QueryString["usingselection"]%>&distributiontype=<%# Request.QueryString["distributiontype"]%>' frameborder="0" marginheight="0" marginwidth="0" border="0" width="100%" height="100%" scrolling="auto"  id="InsufficientFund"  ></iframe>		    
		<%--npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks--%>  
   <script type="text/javascript">
function resizeIframe() {
    var height = document.documentElement.clientHeight;
    height -= document.getElementById('InsufficientFund').offsetTop;
    height -= 10;
    document.getElementById('InsufficientFund').style.height = height + "px"
    
}
document.getElementById('InsufficientFund').onload = resizeIframe;

window.onresize = resizeIframe;
</script>
		</form>
</body>
</html>
