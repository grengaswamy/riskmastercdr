﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.PrintChecks
{
    public partial class PostCheckEob : System.Web.UI.Page
    {
        public XmlDocument Model = null;
		// npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
        private string PostEobMessageTemplate = "<Message><Authorization></Authorization><Call>" +
         "<Function>PrintEOBAdaptor.PostCheckEOB</Function></Call><Document><PostCheckEOB><AccountId /><BatchNumber></BatchNumber><PostcheckDate></PostcheckDate><OrderBy></OrderBy><DistributionType></DistributionType></PostCheckEOB></Document></Message>";
		// npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
        private string FileRequestPostEobMessageTemplate = "<Message><Authorization></Authorization><Call>" +
   "<Function>PrintChecksAdaptor.GetPrintCheckFile</Function></Call><Document><PrintChecks><FileName/><FileType/></PrintChecks></Document></Message>";
        private string DeletePostEobMessageTemplate = "<Message><Authorization></Authorization><Call>" +
"<Function>PrintEOBAdaptor.DeleteEobFiles</Function></Call><Document><PostCheckEOB><FileNames/></PostCheckEOB></Document></Message>";
        protected void Page_Load(object sender, EventArgs e)
        {
            Model = new XmlDocument();
            try
            {
                if (!Page.IsPostBack)
                {
                    StoreQueryStringValues();
                }
                else
                {
                    if (hdnFileRequest.Value == "1")
                    {
                        hdnFileRequest.Value = "0";
                        GetFile();
                    }
                    if (hdnPrintRequest.Value == "1")
                    {
                        hdnPrintRequest.Value = "0";
                        PostEobMessageTemplate = CreateServiceCall();
                        string sReturn = AppHelper.CallCWSService(PostEobMessageTemplate.ToString());
                        Model.LoadXml(sReturn);   
                        SetValuesFromService();
                                          
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
            DataBind();

        }
        private void SetValuesFromService()
        {
            hdnFileNames.Value = AppHelper.GetValueFromDOM(Model, "@FileNames");
        }

        private void GetFile()
        {
        
            FileRequestPostEobMessageTemplate = AppHelper.ChangeMessageValue(FileRequestPostEobMessageTemplate, "//FileName", hdnFileName.Value);
            FileRequestPostEobMessageTemplate = AppHelper.ChangeMessageValue(FileRequestPostEobMessageTemplate, "//FileType", hdnFileType.Value);
            string sReturn = AppHelper.CallCWSService(FileRequestPostEobMessageTemplate.ToString());
            XmlDocument objFile = new XmlDocument();
            objFile.LoadXml(sReturn);

            byte[] pdfbytes = Convert.FromBase64String(objFile.SelectSingleNode("//File").InnerText);

            Response.Buffer = true;
            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Charset = "";
            Response.AppendHeader("Content-Encoding", "none;");
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=report.PDF");
            Response.AddHeader("Accept-Ranges", "bytes");
            Response.BinaryWrite(pdfbytes);
            Response.Flush();
            Response.Close();
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            DeletePostEobMessageTemplate = AppHelper.ChangeMessageValue(DeletePostEobMessageTemplate, "//FileNames", hdnFileNames.Value);
            string sReturn = AppHelper.CallCWSService(DeletePostEobMessageTemplate.ToString());
            XmlDocument objFile = new XmlDocument();
            objFile.LoadXml(sReturn);
        }
      
    
        private void StoreQueryStringValues()
        {
            hdnIsBatchPrint.Value = AppHelper.GetQueryStringValue("IsBatchPrint"); ;
            
         
            hdnBatchNumber.Value = AppHelper.GetQueryStringValue("batchnumber");

            hdnAccountId.Value = AppHelper.GetQueryStringValue("accountid");


            hdnFirstTime.Value = "0";
            hdnPostCheckDate.Value = AppHelper.GetQueryStringValue("postcheckdate");

            hdnOrderBy.Value = AppHelper.GetQueryStringValue("orderby");
			// npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
            hdnDistributionType.Value = AppHelper.GetQueryStringValue("distributiontype");
			// npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
            
        }
        private string CreateServiceCall()
        {
            string IsBatchPrint = AppHelper.GetFormValue("hdnBatchPrint");
            hdnIsBatchPrint.Value = IsBatchPrint;
         
            string CheckBatch = AppHelper.GetFormValue("hdnBatchNumber");
            PostEobMessageTemplate = AppHelper.ChangeMessageValue(PostEobMessageTemplate, "//BatchNumber", CheckBatch);

            string BankAccount = AppHelper.GetFormValue("hdnAccountId");
            PostEobMessageTemplate = AppHelper.ChangeMessageValue(PostEobMessageTemplate, "//AccountId", BankAccount);

            string CheckDate = AppHelper.GetFormValue("hdnPostCheckDate");
            PostEobMessageTemplate = AppHelper.ChangeMessageValue(PostEobMessageTemplate, "//PostcheckDate", CheckDate);
         
            string OrderBy = AppHelper.GetFormValue("hdnOrderBy");
            PostEobMessageTemplate = AppHelper.ChangeMessageValue(PostEobMessageTemplate, "//OrderBy", OrderBy);
			
			// npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
            string DistributionType = AppHelper.GetFormValue("hdnDistributionType");
            PostEobMessageTemplate = AppHelper.ChangeMessageValue(PostEobMessageTemplate, "//DistributionType", OrderBy);
			// npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
            return PostEobMessageTemplate;
        }
    }
}
