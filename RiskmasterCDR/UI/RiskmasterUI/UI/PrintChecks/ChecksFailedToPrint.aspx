﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChecksFailedToPrint.aspx.cs" Inherits="Riskmaster.UI.PrintChecks.ChecksFailedToPrint"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" Tagname="CommonTasks" tagprefix="uc4" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" Tagname="PleaseWaitDialog" tagprefix="uc2" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Print Check</title>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
    <script  type="text/javascript" language="javascript" src='<%#Page.ResolveUrl("~/Scripts/PrintChecks.js")%>'></script>
    <script type="text/javascript" language="javascript">
        //START MITS 32641 srajindersin 06/25/2013 - 'X' button will behave as 'Ok' button
        function windowCloseX()
        {
            //tanwar2 - JIRA 5007 - start
            //tanwar2 - Code is added on unload - it will be called everytime window closes.
            //tanwar2 - These changes should be viewed together with mits 32641
            var isCancel = false;
            var IEbrowser = false || !!document.documentMode; //RMA-10491
			var iFirstFailedCheckNumber = window.document.forms[0].txtFirstFailedCheckNumber.value;
            if (document.getElementById('hdnOkCancelClick') != null && document.getElementById('hdnOkCancelClick').value == "1") {
                isCancel = true;
            }
            if (!isCancel) {
                //tanwar2 - JIRA 5007 - end
                var iStartNum = window.document.forms[0].StartNum.value;
                var iEndNum = window.document.forms[0].EndNum.value;
                if (isNaN(iFirstFailedCheckNumber) || (iStartNum > iFirstFailedCheckNumber) || (iFirstFailedCheckNumber > iEndNum)) {
                    alert("Invalid Check Number ");
                    return false;
                }
                if (iFirstFailedCheckNumber == "") iFirstFailedCheckNumber = "0";
                if (IEbrowser)
                    window.returnValue = iFirstFailedCheckNumber;
                else {
                    window.opener.document.forms[0].FirstFailedCheckNumber.value = iFirstFailedCheckNumber;
                }
            }
            // RMA-10491 : bkuzhanthaim :ShowModalDialog Issue in Chrome : PrintSingleCheckOk Method logic
            if (!IEbrowser) {
                //RMA-10491 : Maintenance - Policy Billing (PrintDisbursement)
                if (window.opener.parent != window.opener && window.opener.document.forms[0].action.indexOf("PrintChecks") == -1) {
                    if (window.opener.parent.document.forms[0].Source.value == "Disbursement") {
                        window.opener.parent.document.forms[0].hdnPrintBatchOk.value = "Processed";
                        window.opener.document.forms[0].functiontocall.value = "BillingAdaptor.PostPrintDisb";
                        window.opener.document.forms[0].method = "post";
                        window.opener.document.forms[0].submit();
                    }
                }
                else {
                    //tanwar2 - added if condition
                    var hdnZeroChecksValue = 0;
                    var arrCheckDetail = window.opener.document.forms[0].hdnPrintBatchOk.value.split("||");
                    if (arrCheckDetail.length >= 6) {
                        hdnZeroChecksValue = arrCheckDetail[5];
                    }
                    if (hdnZeroChecksValue == "2") {
                        //RMA-10491 : should not be Funds-> Print Checks (PrintChecksBatch) screen.if (window.opener.document.forms[0].action.indexOf("PrintChecks") < -1)
                         window.opener.close();
                    }
                    else {
                        //window.opener.document.forms[0].hdnRequest.value = "";
                        window.opener.document.forms[0].functiontocall.value = "PrintChecksAdaptor.UpdateStatusForPrintedChecks";
                        window.opener.document.forms[0].method = "post";
                        window.opener.document.forms[0].submit();
                    }
                }
            }
            window.close();
            return false;
        }
        //END MITS 32641 srajindersin 06/25/2013 - 'X' button will behave as 'Ok' button
    </script>
    <%-- aravi5 RMA-10253,RMA-10491 Policy billing - Print - Show modal Starts --%>
    <script type="text/javascript">
        function HideOverlaydiv() {
            var IEbrowser = false || !!document.documentMode; // At least IE6
            if (!IEbrowser) {
                if (window.opener.parent.parent.document.getElementById("overlaydiv") != null) {
                    window.opener.parent.parent.document.getElementById("overlaydiv").remove();
                }
                return false;
            }
        }
     </script>
     <%-- aravi5 RMA-10253 Policy billing - Print - Show modal Ends --%>
</head>
<body onpagehide="HideOverlaydiv(); return false;" onunload="windowCloseX();"><%-- aravi5 RMA-10253 --%>
    <form id="frmData" runat="server">
    <input type="hidden" runat=server  id="StartNum"><input type="hidden"  runat=server  id="EndNum"><table width="100%" cellspacing="0" cellpadding="2" border="0">
    <tr>
     <td>
      								&nbsp;
      							
     </td>
    </tr>
    <tr class="msgheader">
     <td>
      								Checks Failed To Print
      							
     </td>
    </tr>
    <tr>
     <td><BR>The following checks were printed:
      								<asp:Label ID="lblStartNum" runat=server></asp:Label>
      								through
      								<asp:Label ID="lblEndNum" runat=server></asp:Label>
     </td>
    </tr>
    <tr>
     <td><BR>Enter the number of first check printed which failed
      								and press Ok. Choosing Cancel will mark all checks
      								as printed successfully.								
      							
     </td>
    </tr>
    <tr>
     <td><BR> First Failed Check Number: 
      								<input type="text"  runat=server  id="txtFirstFailedCheckNumber" size="20"></td>
    </tr>
   </table>
   <table width="17%">
    <tr>
     <td width="5%"><BR><input type="button"  value="   Ok   " class="button" id="Ok" onclick="return ChecksFailedToPrintOk(); "></td>
     <td width="5%"><BR><input type="button" value="Cancel" class="button" id="Cancel" onclick="return ChecksFailedToPrintCancel(); "></td>
    </tr>
   </table>
        <%--tanwar2 - JIRA 5007 - start--%>
        <input type="hidden" value="" id="hdnOkCancelClick" />
        <%--tanwar2 - JIRA 5007 - End--%>
    </form>
</body>
</html>
