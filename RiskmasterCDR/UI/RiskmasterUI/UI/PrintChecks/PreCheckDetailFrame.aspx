﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PreCheckDetailFrame.aspx.cs" Inherits="Riskmaster.UI.PrintChecks.PreCheckDetailFrame" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>PreRegister Checks</title>
        <script language='javascript' type="text/javascript">

        function CheckError() {
           if(document.forms[0].error.value == "true"){
               alert("There are no pre checks eligible for printing.  If you feel you've received this message in error please check the funds records and ensure that all the required fields are present.");
               window.returnValue = "error";
               window.close();
           }
        }
        </script>
</head>
<body onload="Javascript :CheckError()">
    <form id="frmData" runat="server">
    <div>
    </div>
    <asp:TextBox runat="server" Style="display: none" ID="error" />
    </form>
</body>
</html>
