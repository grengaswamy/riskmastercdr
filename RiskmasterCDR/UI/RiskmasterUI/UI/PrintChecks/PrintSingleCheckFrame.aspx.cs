﻿/**********************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                    *
 **********************************************************************************************
 * 12/09/2014 | RMA-438         | ajohari2   | Underwriters - EFT Payments
 **********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;
using System.Data;

namespace Riskmaster.UI.PrintChecks
{
    public partial class PrintSingleCheckFrame : NonFDMBasePageCWS
    {
        XElement m_objMessageElement = null;
        private string sFunctionToCall = string.Empty;
        private string sTotalCollection = string.Empty;
        private string sCWSresponse = "";
        //skhare7:MITS 23664
        private string sPrintMode = string.Empty;
        //skhare7:MITS 23664 End
        //private string PrintSingleCheckTemplate = "<Message><Authorization>34880aa1-ee69-4766-8c95-2afd5fe434d2</Authorization><Call>" +
        //"<Function>PrintChecksAdaptor.LoadSingleCheck</Function></Call><Document><IncludeClaimantInPayee/><TransId></TransId><Source></Source>" +
        //"<SelDisbursementRowID></SelDisbursementRowID><TotalCollection></TotalCollection><AccountName></AccountName><AccountNumber></AccountNumber>" +
        //"<NextCheckNumber></NextCheckNumber><CheckDate></CheckDate><SelectedStock></SelectedStock><FirstFailedCheckNumber></FirstFailedCheckNumber>" +
        //"<FunctionToCall>PrintChecksAdaptor.LoadSingleCheck</FunctionToCall></Document></Message>";

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                XElement objTempElement = null;
                bool bReturnStatus = false;
                sFunctionToCall = functiontocall.Text;
                if (!Page.IsPostBack)
                {
                    StoreQueryStringValues();
                     //skhare7:MITS 23664
                    if (sPrintMode.CompareTo("PR") == 0)
                    {

                        PrintMode.Text = sPrintMode;


                       
                    }
                    //skhare7:MITS 23664 End
                }
                if (!string.IsNullOrEmpty(sFunctionToCall)) // RMA-9643 : Cannot find adaptor specified in calling envelope in common web service configuration file.
                {
                    m_objMessageElement = GetMessageTemplate();
                    //bReturnStatus = CallCWSFunctionBindNo(sFunctionToCall, out sCWSresponse, m_objMessageElement);
                    bReturnStatus = CallCWS(sFunctionToCall, m_objMessageElement, out sCWSresponse, false, true);
                    //string sReturn = AppHelper.CallCWSService(PrintSingleCheckTemplate.ToString());
                    //m_objMessageElement = XElement.Parse(sReturn.ToString());

                    //SetValuesFromService();

                    //Start - vkumar258-ML Changes
                    string sCulture = AppHelper.GetCulture().ToString();
                    if (sCulture != "en-US")
                    {
                        //Register Date Script
                        AppHelper.CalenderClientScriptNonFDM(sCulture, this, 2);
                        //Register Time Script
                        AppHelper.TimeClientScript(sCulture, this);
                    }
                    //End - vkumar258-ML Changes


                    if (!IsPostBack)
                    {
                        objTempElement = XElement.Parse(sCWSresponse);
                        if (objTempElement.XPathSelectElement("//LoadSingleCheck/Messages/Error") != null)
                            txtError.Text = objTempElement.XPathSelectElement("//LoadSingleCheck/Messages/Error").Value;
                        if (txtError.Text == "")
                        {
                            BindpageControls(sCWSresponse);
                            AllowPostDate.Text = objTempElement.XPathSelectElement("//CheckDate").Attribute("disabled").Value;
                            objTempElement = objTempElement.XPathSelectElement("//IsPrinterSelected");
                            txtIsPrinterSelected.Text = objTempElement.Value;
                        } // if
                    } // if
                } // try
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            } // catch

        }

        private void StoreQueryStringValues()
        {
            TransId.Text = AppHelper.GetQueryStringValue("transid"); ;

            IncludeClaimantInPayee.Text = AppHelper.GetQueryStringValue("IncludeClaimantInPayee");
            // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
            // So introducing Distribution Type and removing out EFTPayment below
            //JIRA:438 START: ajohari2
            //PrintEFTPayment.Text = AppHelper.GetQueryStringValue("PrintEFTPayment");
            //JIRA:438 End: 
            DistributionType.Text = AppHelper.GetQueryStringValue("DistributionType");
            DistributionTypeId.Text = AppHelper.GetQueryStringValue("DistributionTypeId");

            // Hide Distribution Type if we are calling screen from Print Preview
            if (AppHelper.GetQueryStringValue("BindDistributionType") != "true")
            {
                trDistributionType.Style.Add("display", "none");
            }
            else
            {
                trDistributionType.Style.Add("display", "");
            }
            // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
            // So introducing Distribution Type and removing out EFTPayment below
            Account.Text = AppHelper.GetQueryStringValue("accountname");

            sTotalCollection = AppHelper.GetQueryStringValue("totalamount");
            //skhare7:MITS 23664
            sPrintMode = AppHelper.GetQueryStringValue("PrintMode");
                        //skhare7:MITS 23664 End
            //Added by Amitosh for EFT Payment
           // tbIsEFTAccount.Text = AppHelper.GetQueryStringValue("IsEFTPayment");

        }

        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization></Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>");
            sXml = sXml.Append("<PrintChecks>");
            if (sFunctionToCall == "PrintChecksAdaptor.UpdateStatusForPrintedChecks")
            {
                sXml = sXml.Append("<FirstFailedCheckNumber>");
                sXml = sXml.Append(this.FirstFailedCheckNumber.Text);
                sXml = sXml.Append("</FirstFailedCheckNumber>");
                sXml = sXml.Append("<PrintCheckDetails>");
                sXml = sXml.Append(this.PrintCheckDetails.Text);
                sXml = sXml.Append("</PrintCheckDetails>");
                
            }

            else if (sFunctionToCall == "BillingAdaptor.PostPrintDisb")
            {
                sXml = sXml.Append("<Source>");
                sXml = sXml.Append("</Source>");
                sXml = sXml.Append("<TransId>");
                sXml = sXml.Append("</TransId>");
                sXml = sXml.Append("<SelDisbursementRowID>");
                sXml = sXml.Append("</SelDisbursementRowID>");
                sXml = sXml.Append("<FirstFailedCheckNumber>");
                sXml = sXml.Append("</FirstFailedCheckNumber>");
                sXml = sXml.Append("<PrintCheckDetails>");
                sXml = sXml.Append("</PrintCheckDetails>");
            }
            else
            {
                sXml = sXml.Append("<TransId>");
                sXml = sXml.Append(TransId.Text);
                sXml = sXml.Append("</TransId>");
                sXml = sXml.Append("<TotalCollection>");
                sXml = sXml.Append(sTotalCollection);
                sXml = sXml.Append("</TotalCollection>");
                sXml = sXml.Append("<PrintMode>");
                sXml = sXml.Append(sTotalCollection);
                sXml = sXml.Append("</PrintMode>");
                //skhare7:MITS 23664
            } // else
            sXml = sXml.Append("<LangCode>");
            sXml = sXml.Append(AppHelper.GetLanguageCode());
            sXml = sXml.Append("</LangCode>");
            //JIRA:438 START: ajohari2
            // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
            // So introducing Distribution Type and removing out EFTPayment below
            //sXml = sXml.Append("<PrintEFTPayment>");
            //sXml = sXml.Append(this.PrintEFTPayment.Text);
            //sXml = sXml.Append("</PrintEFTPayment>");
            sXml = sXml.Append("<DistributionType>");
            sXml = sXml.Append(this.DistributionTypeId.Text);
            sXml = sXml.Append("</DistributionType>");
            //JIRA:438 End: 
            // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
            // So introducing Distribution Type and removing out EFTPayment below
            sXml = sXml.Append("</PrintChecks></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());

            return oTemplate;
        }

        //private void SetValuesFromService()
        //{
        //    AllowPostDate.Text = objTempElement.XPathSelectElement("//CheckDate").Attribute("disabled").Value;
        //}

        private void BindpageControls(string sReturnValue)
        {
            DataSet usersRecordsSet = null;
            XmlDocument usersXDoc = new XmlDocument();
            usersXDoc.LoadXml(sReturnValue);
            usersRecordsSet = ConvertXmlDocToDataSet(usersXDoc);
            if (usersRecordsSet.Tables["stock"] != null)
            {
                DropDownList lstCboStocks = (DropDownList)this.Form.FindControl("cboStocks");
                lstCboStocks.DataSource = usersRecordsSet.Tables["stock"].DefaultView;
                lstCboStocks.DataTextField = "Name";
                lstCboStocks.DataValueField = "Id";

                lstCboStocks.DataBind();
            }
        }
    }
}
