﻿<%--/**********************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                    *
 **********************************************************************************************
 * 12/09/2014 | RMA-438         | ajohari2   | Underwriters - EFT Payments
 **********************************************************************************************/--%>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShowPreCheck.aspx.cs" Inherits="Riskmaster.UI.PrintChecks.ShowPreCheck"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>PreRegister Checks</title>
</head>
    <body onload="window.close();">
        <form id="frmData" runat="server">
          <%--skhare7 R8 Include combined Payment paramter added--%>
          <%--Debabrata Biswas Added new items query string items : orghierarchylevelpre  orghierarchypre Batch Printing Retrofit MITS 19715/20050 Date: 03/17/2010/20050 --%>
          <!-- MITS 24942 hlv 3/28/2012 <iframe  src='PreCheckDetailFrame.aspx?fromdate=<%# Request.QueryString["fromdate"]%>&todate=<%# Request.QueryString["todate"]%>&fromdateflag=<%# Request.QueryString["fromdateflag"]%>&todateflag=<%# Request.QueryString["todateflag"]%>&includeautopayments=<%# Request.QueryString["includeautopayments"]%>&accountid=<%# Request.QueryString["accountid"]%>&orghierarchypre=<%# Request.QueryString["orghierarchypre"]%>&orghierarchylevelpre=<%# Request.QueryString["orghierarchylevelpre"] %>&selectedchecksids=<%# Request.QueryString["selectedchecksids"]%>&selectedautochecksids=<%# Request.QueryString["selectedautochecksids"]%>&usingselection=<%# Request.QueryString["usingselection"]%>&orderby=<%# Request.QueryString["orderby"]%>&includecombinedpays=<%# Request.QueryString["includecombinedpays"]%>' frameborder="0" marginheight="0" marginwidth="0" border="0" width="100%" height="100%" scrolling="auto"  id="PreCheck"  ></iframe>-->
          
            <asp:TextBox runat="server" Style="display: none" ID="error" />
        </form>
        <!-- MITS 24942 hlv 3/28/2012 begin -->
            <form id="frmDataNew" name="frmDataNew" action="PreCheckDetailFrame.aspx" method="post" target="PreCheck">
                <input type="hidden" id="fromdate" name="fromdate" value="" />
                <input type="hidden" id="todate" name="todate" value="" />
                <input type="hidden" id="fromdateflag" name="fromdateflag" value="" />
                <input type="hidden" id="todateflag" name="todateflag" value="" />
                <input type="hidden" id="includeautopayments" name="includeautopayments" value="" />
                <input type="hidden" id="accountid" name="accountid" value="" />
                <input type="hidden" id="orghierarchypre" name="orghierarchypre" value="" />
                <input type="hidden" id="orghierarchylevelpre" name="orghierarchylevelpre" value="" />
                <input type="hidden" id="selectedchecksids" name="selectedchecksids" value="" />
                <input type="hidden" id="selectedautochecksids" name="selectedautochecksids" value="" />
                <input type="hidden" id="usingselection" name="usingselection" value="" />
                <input type="hidden" id="orderby" name="orderby" value="" />
                <input type="hidden" id="includecombinedpays" name="includecombinedpays" value="" />
                <%--npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and removing out EFTPayment below--%>
                <%--<input type="hidden" id="EFTPayment" name="EFTPayment" value="" /><%--JIRA:438 START: ajohari2--%>--%>
                <input type="hidden" id="DistributionType" name="DistributionType" value="" />
                <%--npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and removing out EFTPayment below--%>

                <%--<iframe id="PreCheck" name="PreCheck" frameborder="0" marginheight="0" marginwidth="0" border="0" width="100%" height="100%" scrolling="auto"   ></iframe>--%>
                <!-- MITS 24942 hlv 3/28/2012 end -->
                <script type="text/javascript">
                    //MITS 24942 hlv 3/28/2012 begin
                    
                    if (window.dialogArguments != null) //tmalhotra3 MITS 36348
                    {
                        var tmparg = window.dialogArguments.split('&');
                        for (var i = 0; i < tmparg.length; i++) {
                            var tmpInput = tmparg[i].split('=');
                            document.getElementById(tmpInput[0]).value = tmpInput[1];
                        }
                    }
                    var tmpfrm = document.getElementById("frmDataNew");
                    tmpfrm.submit();
                    setTimeout("window.close();", 3000); // RMA-10209 Close ShowPreCheck and open PreCheckDetail Window
                    //MITS 24942 hlv 3/28/2012 end
                </script>
            </form>
    </body>
</html>
