﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;

namespace Riskmaster.UI.PrintChecks
{
    public partial class InsufficientFundFrame : NonFDMBasePageCWS
    {
        XElement m_objMessageElement = null;
        private string sCWSresponse = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            XElement objTempElement = null;
            bool bReturnStatus = false;
            string sFromDateFlag = string.Empty;
            string sToDateFlag = string.Empty;
            string sIncludeAutoPaymentsFlag = string.Empty;
            string sFromDate = string.Empty;
            string sToDate = string.Empty;
            string sAccountId = string.Empty;
            string sOrderBy = string.Empty;
            string sUsingSelection = string.Empty;
            string sTransIds = string.Empty;
            string sAutoTransIds = string.Empty;
			// npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
            string sDistributionType = string.Empty;
			// npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
            try
            {

                m_objMessageElement = GetMessageTemplate();

                if (Request.QueryString["fromdateflag"] != null)
                {
                    sFromDateFlag = Request.QueryString["fromdateflag"];
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/FromDateFlag");
                    objTempElement.Value = sFromDateFlag;
                }

                if (Request.QueryString["todateflag"] != null)
                {
                    sToDateFlag = Request.QueryString["todateflag"];
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/ToDateFlag");
                    objTempElement.Value = sToDateFlag;
                }

                if (Request.QueryString["includeautopayments"] != null)
                {
                    sIncludeAutoPaymentsFlag = Request.QueryString["includeautopayments"];
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/IncludeAutoPayment");
                    objTempElement.Value = sIncludeAutoPaymentsFlag;
                }

                if (Request.QueryString["fromdate"] != null)
                {
                    sFromDate = Request.QueryString["fromdate"];
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/FromDate");
                    objTempElement.Value = sFromDate;
                }

                if (Request.QueryString["todate"] != null)
                {
                    sToDate = Request.QueryString["todate"];
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/ToDate");
                    objTempElement.Value = sToDate;
                }

                if (Request.QueryString["accountid"] != null)
                {
                    sAccountId = Request.QueryString["accountid"];
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/AccountId");
                    objTempElement.Value = sAccountId;
                }
                if (Request.QueryString["usingselection"] != null)
                {
                    sUsingSelection = Request.QueryString["usingselection"];
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/UsingSelection");
                    objTempElement.Value = sUsingSelection;
                }

                if (Request.QueryString["selectedchecksids"] != null)
                {
                    sTransIds = Request.QueryString["selectedchecksids"];
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/TransIds");
                    objTempElement.Value = sTransIds;
                }

                if (Request.QueryString["selectedautochecksids"] != null)
                {
                    sAutoTransIds = Request.QueryString["selectedautochecksids"];
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/AutoTransIds");
                    objTempElement.Value = sAutoTransIds;
                }

				// npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
                if (Request.QueryString["distributiontype"] != null)
                {
                    sDistributionType = Request.QueryString["distributiontype"];
                    objTempElement = m_objMessageElement.XPathSelectElement("./Document/PrintChecks/DistributionType");
                    objTempElement.Value = sDistributionType;
                }
				// npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
                //if (HttpContext.Current.Request.Cookies["SessionId"] != null)
                //{
                //    objTempElement = m_objMessageElement.XPathSelectElement("./Authorization");
                //    objTempElement.Value = HttpContext.Current.Request.Cookies["SessionId"].Value;
                //}

                if (!IsPostBack)
                {
                    bReturnStatus = CallCWSFunctionBind("PrintChecksAdaptor.InsufficientFundsPreEdit", out sCWSresponse, m_objMessageElement);
                }
                objTempElement = XElement.Parse(sCWSresponse);
                objTempElement = objTempElement.XPathSelectElement("./Document/PrintChecks/File");
                //TextBox txtPdfFile = (TextBox)this.Form.FindControl("File");

                byte[] pdfbytes = Convert.FromBase64String(objTempElement.Value);

                Response.Buffer = true;
                Response.Clear();
                // aravi5 RMA-11631 Warning message"There are no pre checks eligible for printing...." Starts
                //Response.ClearContent();
                //Response.ClearHeaders();
                //Response.Charset = "";
                //Response.AppendHeader("Content-Encoding", "none;");
                //Response.ContentType = "application/pdf";
                //Response.AddHeader("Content-Disposition", string.Format("inline; filename=INSUFFICIENTFUND.PDF"));
                //Response.AddHeader("Accept-Ranges", "bytes");
                //Response.BinaryWrite(pdfbytes);
                //Response.Flush();
                //Response.Close();
                Response.AddHeader("Content-Length", pdfbytes.Length.ToString());
                Response.AddHeader("Content-Disposition", string.Format("inline; filename=INSUFFICIENTFUND.PDF"));
                Response.AddHeader("Pragma", "cache");
                Response.AddHeader("Cache-Control", "private");
                Response.AddHeader("Accept-Ranges", "bytes");
                Response.ContentType = "application/pdf";
                Response.BinaryWrite(pdfbytes);
                Response.Flush();
                Response.Close();                
                Response.SuppressContent = true;
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                // aravi5 RMA-11631 Warning message"There are no pre checks eligible for printing...." Ends
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                //ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            } // catch

        }

        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization></Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>");
            sXml = sXml.Append("<PrintChecks>");
            sXml = sXml.Append("<FromDateFlag></FromDateFlag>");
            sXml = sXml.Append("<ToDateFlag></ToDateFlag>");
            sXml = sXml.Append("<IncludeAutoPayment></IncludeAutoPayment>");
            sXml = sXml.Append("<FromDate></FromDate>");
            sXml = sXml.Append("<ToDate></ToDate>");
            sXml = sXml.Append("<AccountId></AccountId>");
            sXml = sXml.Append("<UsingSelection></UsingSelection>");
            sXml = sXml.Append("<TransIds></TransIds>");
            sXml = sXml.Append("<AutoTransIds></AutoTransIds>");
			// npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
            sXml = sXml.Append("<DistributionType></DistributionType>");
			// npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
            sXml = sXml.Append("</PrintChecks></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());

            return oTemplate;
        }
    }
}
