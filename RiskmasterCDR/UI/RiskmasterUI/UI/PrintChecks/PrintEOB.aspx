﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintEOB.aspx.cs" Inherits="Riskmaster.UI.PrintChecks.PrintEOB"%>

<%@ Import Namespace="System.Xml" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" Tagname="CommonTasks" tagprefix="uc4" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" Tagname="PleaseWaitDialog" tagprefix="uc2" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

 <base target=_self>
    <title>Explanation of Benefits Reports</title>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
    <script  type="text/javascript" language="javascript" src='<%#Page.ResolveUrl("~/Scripts/trans.js")%>'></script>
    
    <script language='javascript' type="text/javascript">
    
        function checkReadyState() {

            if (document.forms[0].hdnFileRequest.value == "1") {
                //JIRA RMA-9120 - ajohari2 Start

                var strreadyState = null;
                if (document.readyState != null) {
                    strreadyState = document.readyState;
                }
                //while (document.all.file.document.readyState != "complete") {
                while (strreadyState != "complete") {
                    status += "."; // just wait ...
                }
                //JIRA RMA-9120 - ajohari2 End

                pleaseWait.pleaseWait('stop');
            }
        }

    function submitthisPage()
    {
        if (document.forms[0].hdnFirstTime != null) {
            if (document.forms[0].hdnFirstTime.value == "0") {
                document.forms[0].hdnFirstTime.value = "1";
                document.forms[0].hdnPrintRequest.value = "1";
                document.forms[0].submit();
                pleaseWait.Show();

            }
        }
        
     }   
    </script>
</head>
<body onload="javascript:submitthisPage();" onunload="PrintEOBOnClose()">

  
    <form id="frmData" runat="server"  >
   
    <rmcontrol:ErrorControl ID="ErrorControl1" Runat="server" />
        <%try
       {
           %>
       
					<% if (Model.SelectSingleNode("//PrintEOB") != null)
        {%>  
      <table width="100%" cellspacing="0" cellpadding="2" border="0">  
    				    
		<tr class="msgheader">
						<td colspan="2">
								EOB Reports
						</td>
    				</tr>
        <%if (Model.SelectSingleNode("//PrintEOB").Attributes["PrintDirectlyToPrinter"].Value == "false")
          { %>
	            <tr>
					<td>
								EOB reports attached with this payment:
								</td>
							</tr>
<%--							<%foreach (XmlNode node in Model.SelectNodes("//File"))
         {
             int iCount = 0; %>--%>
						<tr>
									<td>
										<div style="position:relative;width:420px;height:70px;overflow:auto" class="divScroll">
											<table>
												<ul>
													<%foreach (XmlNode node in Model.SelectNodes("//File"))
               {
                   int iCount = 0; %>
														<tr>
															<td width="5%">
															</td>
															<td>
																<li>
																
																<a  id='<%=node.Attributes["Name"].Value %>' href="javascript:PrintSelectedReport('<%=node.Attributes["FileName"].Value %>')" >
										                        <%if (node.Attributes["Name"].Value == "StubDetail")
                                    {%> Stub Detail Report<%} %>					
                                                                <%if (node.Attributes["Name"].Value == "PrintEOBBRSReport")
                                                                  {%> EOB Report<%} %>
																</a>
																</li>				
															</td>
															
														</tr>
													<%iCount++;%>
                                                    <%}%>
												</ul>
											</table>
										</div>
									</td>
								</tr>
<%--							<%iCount++;%>
<%         } %>--%>
        <% } %>
        <%else
            { %>
            <%if (Model.SelectSingleNode("//PrintEOB").Attributes["PrintDirectlyToPrinter"].Value == "true")
              { %>
            <%if (Model.SelectSingleNode("//PrintEOB").Attributes["StubDetail"].Value == "true" && Model.SelectSingleNode("//PrintEOB").Attributes["PrintEOBBRSReport"].Value == "true")
              { %>
						<tr>
						<td>
                EOB report(s) and Check Stub are sucessfully printed.
            </td>
        </tr>
        <%} %>
        <%else if (Model.SelectSingleNode("//PrintEOB").Attributes["StubDetail"].Value == "true" && Model.SelectSingleNode("//PrintEOB").Attributes["PrintEOBBRSReport"].Value == "false")
            { %>
                <tr>
            <td>
                Check Stub is sucessfully printed.
            </td>
        </tr>
        <%} %>
                <%else if (Model.SelectSingleNode("//PrintEOB").Attributes["StubDetail"].Value == "false" && Model.SelectSingleNode("//PrintEOB").Attributes["PrintEOBBRSReport"].Value == "true")
            { %>
                <tr>
            <td>
                EOB report(s) is/are sucessfully printed.
            </td>
        </tr>
        <%} %>
                        <%else if (Model.SelectSingleNode("//PrintEOB").Attributes["StubDetail"].Value == "false" && Model.SelectSingleNode("//PrintEOB").Attributes["PrintEOBBRSReport"].Value == "false")
            { %>
                <tr>
            <td>
                There are no reports to print.Please contact your system administrator to check the settings in Utilities.
            </td>
        </tr>
        <%} %>
        <%         } %>
                <%else
            { %>
                            <tr>
            <td>
               Please select proper Printer,EOB bin in Utilites.
            </td>
        </tr>
                    <%         } %>
        <%} %>
        <tr>
            <td>
						<br />
							<asp:Button ID="btnOk" runat="server"  OnClientClick="return PrintEOBOk();"
             Text="   Ok   "  CssClass=button 
              />										
						</td>							
					</tr>
					
					<%         } %>	
				
			<%        Model = null; %>	
					<tr>
						<td>
                         
					   <asp:HiddenField ID="hdnIsSilent" runat="server" />
              
           <asp:HiddenField ID="hdnTransId" runat="server" />
           <asp:HiddenField ID="hdnClaimId" runat="server" />
           <asp:HiddenField ID="hdnAccountId" runat="server" />
       
         <asp:HiddenField ID="hdnOrderBy" runat="server" />
          <asp:HiddenField ID="hdnPostCheckDate" runat="server" />   
         <asp:HiddenField ID="hdnDeleteEOBFiles" value="false" runat="server" />  
         <asp:HiddenField ID="hdnFirstTime" runat="server"  Value="0"/>
         <asp:HiddenField ID="hdnFileNames" runat="server"  Value="0"/>
         <input type='hidden' id="hdnModelXml" runat=server ></input>
          	<input type='hidden' id="hdnFileRequest" runat=server ></input>
						<input type='hidden' id="hdnPrintRequest" runat=server  value="0"></input>
					<input type='hidden' id="hdnFileName" runat=server ></input>
					<input type='hidden' id="hdnFileType" runat=server ></input>        
					<tr>
					<td>
			<iframe frameborder="0" width="0" height="0" src="" id="file"></iframe>
					 <uc2:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server"   CustomMessage="Printing" />
					 </td>
					</tr>
				</table>
				<%}
       catch (Exception ee)
       {
           ErrorHelper.logErrors(ee);
           lblErrors.Text =ErrorHelper.FormatErrorsForUI("Error Printing EOB report.");
       }%>	
        <asp:Label ID="lblErrors" runat=server></asp:Label>		
    </form>
</body>
</html>