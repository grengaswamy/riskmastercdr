<%--/**********************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                    *
 **********************************************************************************************
 * 12/09/2014 | RMA-438         | ajohari2   | Underwriters - EFT Payments
 **********************************************************************************************/--%>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintSingleCheckFrame.aspx.cs"
    Inherits="Riskmaster.UI.PrintChecks.PrintSingleCheckFrame" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%--<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
</body>
</html>--%>
<head runat="server">
    <title>Print Check</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />

    <script language="javaScript" src="../../Scripts/trans.js" type="text/javascript"></script>

    <script language="JavaScript" src="../../Scripts/drift.js" type="text/javascript"></script>

    <script language="javaScript" src="../../Scripts/PrintChecks.js" type="text/javascript"></script>

    <script language="JavaScript" src="../../Scripts/WaitDialog.js" type="text/javascript">        { var i; }
    </script>
     <%--vkumar258 - RMA_6037- Starts--%>
   <%-- <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">        { var i; }
    </script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }
    </script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }
    </script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid.js">        { var i; }
    </script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">        { var i; }
    </script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">        { var i; }
    </script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">        { var i; }
    </script>--%>
    <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
        <%--vkumar258 - RMA-6037 - End --%>
    <script language="JavaScript" src="../../Scripts/rmx-common-ref.js" type="text/javascript">        { var i; }
    </script>
    <%-- aravi5 RMA-7244 starts --%>
    <script type="text/javascript">
        function HideOverlaydiv() {
            var IEbrowser = false || !!document.documentMode; // At least IE6
            if (!IEbrowser) {
                if (window.opener.parent.parent.document.getElementById("overlaydiv") != null) {
                    window.opener.parent.parent.document.getElementById("overlaydiv").remove();
                }
                return false;
            }
        }
    </script>
     <%-- aravi5 RMA-7244 ends --%>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<base target="_self">
<body class="10pt" onpagehide="HideOverlaydiv(); return false;" onload="PrintSingleCheckOnLoad();"> <%-- aravi5 RMA-7244 --%>
    <form id="frmData" name="frmData" method="post" runat="server">
    <rmcontrol:ErrorControl ID="ErrorControl1" Runat="server" />
      <div id="Div1" class="errtextheader" runat="server">
            <asp:Label ID="formdemotitle" runat="server" Text="" />
        </div>
    <table cellspacing="0" cellpadding="2" width="100%" border="0">
        <tbody>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr class="msgheader">
                <td colspan="2">
                    Print Single Check
                </td>
            </tr>
            <tr>
                <td>
                    From Account
                </td>
                <td>
                    <asp:TextBox type="text" runat="server" ID="Account" rmxref="/Instance/Document//AccountName" />
                    <asp:TextBox runat="server" ID="AccountNumber" Style="display: none" rmxref="/Instance/Document//AccountNumber" />
                    <asp:TextBox runat="server" ID="TransId" Style="display: none" rmxref="/Instance/Document//TransId" />
                    <%--<skhare7:MITS 23664>--%>
                     <asp:TextBox runat="server" ID="PrintMode" Style="display: none" />
                       <%--<skhare7:MITS 23664>--%>
                    <asp:TextBox runat="server" ID="AllowPostDate" Style="display: none" />
                    <asp:TextBox runat="server" ID="Source" Style="display: none" rmxref="/Instance/Document//Source" />
                    <asp:TextBox runat="server" ID="txtError" Style="display: none" />
                    <asp:TextBox runat="server" ID="FirstFailedCheckNumber" Style="display: none" rmxref="/Instance/Document//FirstFailedCheckNumber" />
                    <asp:TextBox runat="server" ID="functiontocall" Text="PrintChecksAdaptor.LoadSingleCheck"
                        Style="display: none" rmxref="/Instance/Document//FunctionToCall" />
                </td>
            </tr>
            <tr>
                <td>
                    Check Stock
                </td>
                <td>
                    <%--<select id="cboStocks" size="1" name="$node^26">
                        <option value="5" selected>Stock</option>
                        <option value="6">utest_123</option>
                    </select>--%>
                    <asp:DropDownList runat="server" ID="cboStocks" size="1">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Check Number
                </td>
                <td>
                    <asp:TextBox runat="server" ID="CheckNumber" 
                        rmxref="/Instance/Document//NextCheckNumber" MaxLength="18" />
                </td>
            </tr>
            <tr>
                <td>
                    Check Date
                </td>
                <td>
                <!-- pmittal5 Mits 17988 10/07/09 - validate Check Date against Past Date-->
                    <%--<asp:TextBox runat="server" ID="txtCheckDate" onblur="dateLostFocus(this.id);" rmxref="/Instance/Document//CheckDate" />--%>
                    <asp:TextBox runat="server" ID="txtCheckDate" onblur="dateLostFocus(this.id);" rmxref="/Instance/Document//CheckDate" onchange="validateCheckDate(this.id)"/>
                  <%--vkumar258 - RMA_6037- Starts--%>
                     <%-- <input runat="server" class="button" id="txtCheckDatebtn" type="button" value="..." />

                    <script type="text/javascript">
                        Zapatec.Calendar.setup(
											{
											    inputField: "txtCheckDate",
											    ifFormat: "%m/%d/%Y",
											    button: "txtCheckDatebtn"
											}
											);
                    </script>--%>
                    <script type="text/javascript">
                        $(function () {
                            $("#txtCheckDate").datepicker({
                                showOn: "button",
                                buttonImage: "../../Images/calendar.gif",
                               // buttonImageOnly: true,
                                showOtherMonths: true,
                                selectOtherMonths: true,
                                changeYear: true
                            }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' });
                        });
                    </script>
                     <%--vkumar258 - RMA_6037- End--%>
                </td>
            </tr>
            <%--npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
            So introducing Distribution Type and removing out EFTPayment below--%>
            <tr id="trDistributionType" runat="server">
                <td>
                    Distribution Type
                </td>
                <td>
                    <asp:TextBox runat="server" ID="DistributionType" Enabled="false"
                         MaxLength="18" />
                    <asp:TextBox runat="server" ID="DistributionTypeId" style="display:none" rmxref="/Instance/Document//DistributionType"></asp:TextBox>
                </td>
            </tr>
            <%--npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
            So introducing Distribution Type and removing out EFTPayment below--%>
            <tr>
                <td>
                    <input class="button" id="Ok" onclick="return PrintSingleCheckOk(); " type="submit"
                        value="   Print    " />
                </td>
                <td>
                    <input class="button" id="Cancel" onclick="return PrintSingleCheckCancel(); " type="submit"
                        value="   Cancel   " />
                </td>
            </tr>
            <asp:TextBox runat="server" ID="IncludeClaimantInPayee" Value="true" Style="display: none"
                rmxref="/Instance/Document//IncludeClaimantInPayee" />
                    <asp:TextBox runat="server"  ID="txtIsPrinterSelected"  style="display: none"/>                                                
            <asp:TextBox runat="server" value="" Style="display: none" ID="PrintCheckDetails" />
            <%--npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
            So introducing Distribution Type and removing out EFTPayment below--%>
            <%--<asp:TextBox runat="server" value="" Style="display: none" ID="PrintEFTPayment" /> JIRA:438 START: ajohari2--%>
            <%--npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
            So introducing Distribution Type and removing out EFTPayment below--%>
              <asp:TextBox runat="server" value="" style="display: none" id="tbIsEFTAccount" RMXRef="/Instance/Document//IsEFTAccount"/><%--added by Amitosh for EFT Payment--%>
                <input type='hidden' id="hdnPrintBatchOk" runat=server ></input>
        </tbody>
    </table>
    </form>
</body>
</html>
