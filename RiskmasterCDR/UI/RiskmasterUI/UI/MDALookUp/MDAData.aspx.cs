﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.BusinessHelpers;
namespace Riskmaster.UI.UI.MDALookUp
{
    public partial class MDAData : NonFDMBasePageCWS
    {
        string mdaType = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                bool bReturnStatus = false;
                XElement XmlTemplate = null;
                string sreturnValue = "";
                string sClaimID="";
                XmlDocument XmlDoc = new XmlDocument();
                XmlNode xNode=null;
                XmlNode xDiagcode=null;
                DataRow objRow;
                if (!IsPostBack)
                {
                    txtMedicalCode.Text = AppHelper.GetQueryStringValue("medicalcode");
                    txtJobID.Text = AppHelper.GetQueryStringValue("jobclassid");
                   // XmlTemplate = GetMessageTemplate(txtJobID.Text,txtMedicalCode.Text);
                    //bReturnStatus = CallCWSFunction("MDALookUpAdaptor.GetMDATopicDetails", out sreturnValue, XmlTemplate);
                    mdaType = txtMedicalCode.Text.Split('~').LastOrDefault().ToUpper();
                    this.hdnMDAType.Value = mdaType;
                    MDABusinessHelper helper = new MDABusinessHelper();
                    string retData = helper.GetMDATopics(txtMedicalCode.Text, txtJobID.Text);
                    XmlDoc.LoadXml(retData);
                    SetMDAMessage(XmlDoc);
                    string sothercodes = "";
                    if (XmlDoc.SelectSingleNode("//AdditionalCodesForMonograph/MedicalCode") != null)
                    {
                        foreach (XmlNode node in XmlDoc.SelectNodes("//AdditionalCodesForMonograph/MedicalCode"))
                        {
                            sothercodes += node.SelectSingleNode("MedicalCodeValue").InnerText+",";
                        }
                        if (sothercodes.Length >1)
                        {
                            sothercodes=sothercodes.Substring(0, sothercodes.Length - 1);
                        }
                    }
                     
                    if (XmlDoc.SelectSingleNode("//DisabilityDurations/DisabilityDuration")!=null )
                    {

                        XmlNodeList diaglist = XmlDoc.SelectNodes("//DisabilityDurations/DisabilityDuration");

                        DataTable objTable = new DataTable();
                        //objTable.Columns.Add("ICD9");
                        objTable.Columns.Add(mdaType);
                        objTable.Columns.Add("GuidelineID");
                        objTable.Columns.Add("TopicId");
                        objTable.Columns.Add("Topic");
                        objTable.Columns.Add("Reason");
                        objTable.Columns.Add("Factors");
                        objTable.Columns.Add("OtherCodes");
                        objTable.Columns.Add("Minimum");
                        objTable.Columns.Add("Optimum");
                        objTable.Columns.Add("Maximum");
                        for (int i = 0; i < diaglist.Count; i++)
                        {
                            xDiagcode = diaglist[i];
                            objRow = objTable.NewRow();
                            //objRow["ICD9"] = xDiagcode.SelectSingleNode("MedicalCode").InnerText;
                            objRow[mdaType] = xDiagcode.SelectSingleNode("MedicalCode").InnerText;
                            objRow["GuidelineID"] = "";// xDiagcode.SelectSingleNode("MedicalCodeDescription").InnerText;
                            objRow["TopicId"] = "";//xDiagcode.SelectSingleNode("TopicId").InnerText;
                            objRow["Topic"] = xDiagcode.SelectSingleNode("MedicalCodeDescription").InnerText;
                            objRow["Factors"] = xDiagcode.SelectSingleNode("Factors").InnerText;
                            objRow["OtherCodes"] = sothercodes;//xDiagcode.SelectSingleNode("OtherCodes").InnerText;
                            objRow["Minimum"] = xDiagcode.SelectSingleNode("MinimumDuration").InnerText;
                            objRow["Optimum"] = xDiagcode.SelectSingleNode("OptimumDuration").InnerText;
                            objRow["Maximum"] = xDiagcode.SelectSingleNode("MaximumDuration").InnerText;
                            objTable.Rows.Add(objRow);
                        }
                        objTable.AcceptChanges();
                        MDADataGrid.Visible = true;
                        MDADataGrid.DataSource = objTable;
                        MDADataGrid.DataBind();
                        
                       
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }
        private void SetMDAMessage(XmlDocument doc)
        {
            string message = "";
            if (doc != null)
            {
                if (doc.SelectSingleNode("//TransactionMessage/Message") != null)
                {
                    message = doc.SelectSingleNode("//TransactionMessage/Message").InnerText;
                    if (message.ToLower().Equals("license is current."))
                    {
                        message = "";
                    }
                }
            }
            if (!message.Equals(""))
            {
                ErrorControl1.Text = ErrorHelper.FormatErrorsForUI(message, true);
            }
        }

        private XElement GetMessageTemplate(string sJobID, string sMedicalCode)
        {

            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization></Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><MDA><MDAParameters><JobClassID>");
            sXml = sXml.Append(sJobID);
            sXml = sXml.Append("</JobClassID><MedicalCode>");
            sXml = sXml.Append(sMedicalCode);
            sXml = sXml.Append("</MedicalCode></MDAParameters></MDA></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
       }

        protected void MDADataGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lbLinkColumn = new LinkButton();
                string sTopicId = string.Empty;
                string sTopic = string.Empty;
                string sGuidelineID = string.Empty;
                string sFactors = string.Empty;
                string sMinimum = string.Empty;
                string sOptimum = string.Empty;
                string sMaximum = string.Empty;


                if (DataBinder.Eval(e.Row.DataItem, "TopicId") == null)
                {
                    sTopicId = "";
                }
                else
                {
                    sTopicId = DataBinder.Eval(e.Row.DataItem, "TopicId").ToString();
                }
                if (DataBinder.Eval(e.Row.DataItem, "Topic") == null)
                {
                    sTopic = "";
                }
                else
                {
                    sTopic = DataBinder.Eval(e.Row.DataItem, "Topic").ToString();
                }
                if (DataBinder.Eval(e.Row.DataItem, "GuidelineID") == null)
                {
                    sGuidelineID = "";
                }
                else
                {
                    sGuidelineID = DataBinder.Eval(e.Row.DataItem, "GuidelineID").ToString();
                }
                if (DataBinder.Eval(e.Row.DataItem, "Factors") == null)
                {
                    sFactors = "";
                }
                else
                {
                    sFactors = DataBinder.Eval(e.Row.DataItem, "Factors").ToString();
                }
                if (DataBinder.Eval(e.Row.DataItem, "Minimum") == null)
                {
                    sMinimum = "";
                }
                else
                {
                    sMinimum = DataBinder.Eval(e.Row.DataItem, "Minimum").ToString();
                }
                if (DataBinder.Eval(e.Row.DataItem, "Optimum") == null)
                {
                    sOptimum = "";
                }
                else
                {
                    sOptimum = DataBinder.Eval(e.Row.DataItem, "Optimum").ToString();
                }
                if (DataBinder.Eval(e.Row.DataItem, "Maximum") == null)
                {
                    sMaximum = "";
                }
                else
                {
                    sMaximum = DataBinder.Eval(e.Row.DataItem, "Maximum").ToString();
                }


                lbLinkColumn.Attributes.Add("href", "#");
                //srajindersin - MITS 28731 - 09/25/2012
                //lbLinkColumn.Attributes.Add("onclick", "postMDALookUpData('" + sTopicId + "','" + sTopic + "','" + sGuidelineID + "','" + sFactors + "','" + sMinimum + "','" + sOptimum + "','" + sMaximum + "');return false;");
                lbLinkColumn.Attributes.Add("onclick", "postMDALookUpData(\"" + sTopicId + "\",\"" + sTopic + "\",\"" + sGuidelineID + "\",\"" + sFactors + "\",\"" + sMinimum + "\",\"" + sOptimum + "\",\"" + sMaximum + "\",\"" + mdaType + "\");return false;");
                //lbLinkColumn.Text = DataBinder.Eval(e.Row.DataItem, "ICD9").ToString(); ;
                lbLinkColumn.Text = DataBinder.Eval(e.Row.DataItem, mdaType).ToString(); ;
                e.Row.Cells[0].Controls.Add(lbLinkColumn);
            }
            else if (e.Row.RowType.Equals(DataControlRowType.Header))
            {
                e.Row.Cells[0].Text = string.Format("{0} Code", mdaType);
                e.Row.Cells[3].Text = string.Format("Other {0} Codes in this Topic", mdaType); 
            }
            
        }  


    }
}
