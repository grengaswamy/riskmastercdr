﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="MDAData.aspx.cs" Inherits="Riskmaster.UI.UI.MDALookUp.MDAData" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc2" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MDA Topics</title>
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />

    <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js"></script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js"></script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js"></script>

    <uc4:CommonTasks ID="CommonTasks1" runat="server" />

    <script type="text/javascript" language="javascript">
        function postMDALookUpData(topicid, topic, guidelineid, factors, minimum, optimum, maximum, mdaType) {
            window.opener.document.forms[0].mdatopic.value = topic;
            window.opener.document.forms[0].mdafactor.value = factors;
            window.opener.document.forms[0].mdatopicid.value = topicid;
            window.opener.document.forms[0].mdaguidelineid.value = guidelineid;

            window.opener.document.forms[0].mdaminimumdays1.value = minimum;
            window.opener.document.forms[0].mdaminimumdays2.value = minimum;
            window.opener.document.forms[0].mdaoptimumdays1.value = optimum;
            window.opener.document.forms[0].mdaoptimumdays2.value = optimum;
            window.opener.document.forms[0].mdamaximumdays1.value = maximum;
            window.opener.document.forms[0].mdamaximumdays2.value = maximum;
            document.getElementById("hdnMDAType").value = mdaType;
            switch (mdaType) {
                case "ICD9":
                    window.opener.document.forms[0].hdnmdaminimumdays.value = minimum;
                    window.opener.document.forms[0].hdnmdaoptimumdays.value = optimum;
                    window.opener.document.forms[0].hdnmdamaximumdays.value = maximum;
                    window.opener.document.forms[0].hdnMDATopic.value = topic;
                    window.opener.document.forms[0].hdnMDAFactor.value = factors;

                    switch (window.opener.document.getElementById('hdnicdflag').value) {
                        case '0':
                        case '1':
                            window.opener.document.getElementById('hdnicdflag').value = '1'; break;
                        case '2':
                        case '3':
                        case '4':
                            window.opener.document.getElementById('hdnicdflag').value = '3'; break;
                    }

                    break;
                case "ICD10":
                    window.opener.document.forms[0].hdnMDA10MinDays.value = minimum;
                    window.opener.document.forms[0].hdnMDA10OptDays.value = optimum;
                    window.opener.document.forms[0].hdnMDA10MaxDays.value = maximum;
                    window.opener.document.forms[0].hdnMDA10Topic.value = topic;
                    window.opener.document.forms[0].hdnMDA10Factor.value = factors;

                    switch (window.opener.document.getElementById('hdnicdflag').value) {
                        case '0':
                        case '2':
                            window.opener.document.getElementById('hdnicdflag').value = '2'; break;
                        case '1':
                        case '3':
                        case '4':
                            window.opener.document.getElementById('hdnicdflag').value = '4'; break;
                    }
                    break;
            }
            window.opener.setDataChanged(true);
            window.close();
            return false;
        }
    </script>

</head>
<body>
    <form id="frmData" runat="server">
     <uc3:ErrorControl ID="ErrorControl1" runat="server" />
    <div class="formtitle" id="formtitle">
        MDA Topics</div>
    <table>
        <tr>
            <td colspan="2">
               
            </td>
        </tr>
        <tr class="formsubtitle">
            <td>
                Job Classification:
            </td>
            <td id="JobClass">
            </td>
        </tr>
        <tr class="formsubtitle">
            <td>
                Primary Diagnosis:
            </td>
            <td id="PrimaryDiagnosis">
            </td>
            <td>
                <br></br>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
        </tr>
    </table>
    <br></br>
    <table width="100%" border="0" cellspacing="0" cellpadding="2">
        <asp:GridView ID="MDADataGrid" AutoGenerateColumns="False" runat="server" Font-Bold="True"
            CellPadding="0" GridLines="Both" CellSpacing="0" Width="100%" EmptyDataText="There is no Data available for the supplied combination of Job Classification and ICD9 code."
            OnRowDataBound="MDADataGrid_RowDataBound">
            <HeaderStyle CssClass="colheader5" />
            <RowStyle CssClass="datatd1" HorizontalAlign="Left" Font-Bold="false" />
            <AlternatingRowStyle CssClass="datatd" HorizontalAlign="Left" Font-Bold="false" />
            <Columns>
                <asp:TemplateField HeaderText= "" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="colheader5">
                </asp:TemplateField>
                <asp:BoundField DataField="Topic" HeaderText="Disability Topic" HeaderStyle-HorizontalAlign="Left"
                    HeaderStyle-CssClass="colheader5" />
                <asp:BoundField DataField="Factors" HeaderText="Disability Factor" HeaderStyle-HorizontalAlign="Left"
                    HeaderStyle-CssClass="colheader5" />
                <asp:BoundField DataField="OtherCodes" HeaderText=""
                    HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="colheader5" />
                <asp:BoundField DataField="Minimum" HeaderText="Minimum" HeaderStyle-HorizontalAlign="Left"
                    HeaderStyle-CssClass="colheader5" />
                <asp:BoundField DataField="Optimum" HeaderText="Optimum" HeaderStyle-HorizontalAlign="Left"
                    HeaderStyle-CssClass="colheader5" />
                <asp:BoundField DataField="Maximum" HeaderText="Maximum" HeaderStyle-HorizontalAlign="Left"
                    HeaderStyle-CssClass="colheader5" />
            </Columns>
        </asp:GridView>
    </table>
    <table>
        <tr>
            <td>
                <br></br>
                <input type="button" id="btnClose" value="  Close  " class="button" onclick="javascript: window.close();" />
                <input type="hidden" id ="hdnMDAType" value="" runat="server"/>
                <br></br>
            </td>
        </tr>
    </table>
    <asp:TextBox Style="display: none" ID="txtMedicalCode" runat="server" />
    <asp:TextBox Style="display: none" ID="txtJobID" runat="server" />

    <script type="text/javascript" language="javascript">
        document.all('JobClass').innerText = window.opener.document.forms[0].jobclassification_codelookup.value;
        document.all('PrimaryDiagnosis').innerText = document.getElementById('hdnMDAType')!= null && document.getElementById('hdnMDAType').value=="ICD10"
            ? window.opener.document.forms[0].icd10primarydiagnosis.value
            :window.opener.document.forms[0].icd9primarydiagnosis.value;
    </script>

    </form>
</body>
</html>
