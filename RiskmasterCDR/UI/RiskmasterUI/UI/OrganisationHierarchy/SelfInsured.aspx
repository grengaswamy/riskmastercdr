﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SelfInsured.aspx.cs" Inherits="Riskmaster.UI.UI.OrganisationHierarchy.SelfInsured" ValidateRequest="false"  %>

<%@ Register  TagPrefix="dg" TagName="UserControlDataGrid" Src="~/UI/Shared/Controls/UserControlDataGrid.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove" xmlns:cul="remove" xmlns:uc3="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard">
  <head runat="server">
  <title>Self Insured Details</title>
  <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
  <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js">{var i;}
</script>
 <script type="text/javascript" language="JavaScript" src="../../Scripts/WaitDialog.js">     { var i; }</script>
  </head>
  <body>
    <form name="frmData" id="frmData" runat="server">
    <div>
        <uc1:ErrorControl ID="ErrorControl1" runat="server" />
        <div>
          <span>
            <dg:UserControlDataGrid runat="server" ID="SelfInsuredGrid" GridName="SelfInsuredGrid" GridTitle="Self Insured Info List" Target="/Document/SelfInsuredInfo" Ref="/Instance/Document/OrgHierarchy/target_SI" Unique_Id="SIRowId" ShowRadioButton="true" Width="" Height="" hidenodes="|SIRowId|EntityId|DeletedFlag|LegalEntityId|DttmRcdAdded|DttmRcdLastUpd|UpdatedByUser|AddedByUser|" ShowHeader="True" LinkColumn="" PopupWidth="500" PopupHeight="400" Type="GridAndButtons" RowDataParam="SelfInsured" OnClick="KeepRowForEdit('SelfInsuredGrid');"/>
          </span>
        </div>
        <asp:TextBox id="entityid" style="display:none" RMXType="id" runat="server"/>
        <asp:TextBox style="display:none" runat="server" id="entitytableid"  RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="SelfInsuredSelectedId"  RMXType="id" />
        <asp:TextBox style="display:none" runat="server" id="SelfInsuredGrid_RowDeletedFlag"  RMXType="id" Text="false" />
       <asp:TextBox style="display:none" runat="server" id="SelfInsuredGrid_Action"  RMXType="id" />
       <asp:TextBox style="display:none" runat="server" id="SelfInsuredGrid_RowAddedFlag"  RMXType="id" Text="false"  />
      </div>
      <div id="Div2" class="formButtonGroup" runat="server">
        <div class="formButton" runat="server" id="div_btnBack">
          <asp:button class="button" runat="server" id="btnBack" RMXRef="" Text="Back" onClientClick="return BackToOrgAddEdit();" />
        </div>
      </div>
      <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
  </body>
</html>