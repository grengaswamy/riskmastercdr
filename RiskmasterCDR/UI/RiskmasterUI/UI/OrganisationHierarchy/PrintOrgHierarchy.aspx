﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintOrgHierarchy.aspx.cs" Inherits="Riskmaster.UI.OrganisationHierarchy.PrintOrgHierarchy" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Org Hierarchy Report</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <uc1:ErrorControl ID="ErrorControl" runat="server" />
    </div>
    <asp:TextBox runat="server" ID="chkFilter" style="display:none" />
    </form>
</body>
</html>
