﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.OrganisationHierarchy
{
    public partial class ClaimEntityInstructions : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XElement XmlTemplate = null;
            bool bReturnStatus = false;
            string sCWSresponse = "";
            XmlDocument XmlDoc = new XmlDocument();
                try
                {
                    EId.Text = AppHelper.GetQueryStringValue("EId");
                    XmlTemplate = GetMessageTemplate(EId.Text);
                    bReturnStatus = CallCWSFunctionBind("ClaimEntityInstructionsAdaptor.Get", out sCWSresponse, XmlTemplate);
                    if (bReturnStatus)
                    {
                        XmlDoc.LoadXml(sCWSresponse);
                        ClaimInstructions.Text = "<font face='Verdana,Arial' size='4'><b>" + XmlDoc.SelectSingleNode("//Document/Instructions").InnerText + "<b></font>";
                    }
                }
                catch(Exception ee)
                {
                   ErrorHelper.logErrors(ee);
                   BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                   err.Add(ee, BusinessAdaptorErrorType.SystemError);
                   ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
                }


        }
        private XElement GetMessageTemplate(string sEntityId)
        {
            StringBuilder sXml=new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml=sXml.Append("<Call><Function></Function></Call><Document><Document>");
            sXml = sXml.Append("<EId>");
            sXml = sXml.Append(sEntityId);
            sXml = sXml.Append("</EId>");
            sXml = sXml.Append("</Document></Document>");
            sXml = sXml.Append("</Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;
        }
    }
}
