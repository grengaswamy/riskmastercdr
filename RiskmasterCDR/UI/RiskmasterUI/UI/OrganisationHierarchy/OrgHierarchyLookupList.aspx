﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrgHierarchyLookupList.aspx.cs" Inherits="Riskmaster.UI.OrganisationHierarchy.OrgHierarchyLookupList" EnableEventValidation="false"%>
 <%@ Register TagPrefix="OrgSearch" TagName="OrgSearchCriteriaControl" Src="~/UI/Shared/Controls/OrgSearchCriteria.ascx" %>
 <%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
 <%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>
 
 <%@ Import Namespace="System.Data" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Org Hierarchy</title>
    <link href= "../../Content/rmnet.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript" src="../../Scripts/org.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/form.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/WaitDialog.js">{var i;}
  </script>
    <script language="javascript" type="text/javascript">
        var m_InitialEntityLevel = "";
        function UpdateInitialEntityLevel()
        {
          var oEntityLevel = document.getElementById('OrgSearch_cmb_slevel');
          if(oEntityLevel != null)
          {
            m_InitialEntityLevel = oEntityLevel.value;
          }
        }
        function EntitySelected(oEntity)
        {
            if(document.getElementById('hd_flgMaintScreen').value == "true")
            {
                var sURL = "../FDM/orghierarchymaint.aspx?recordID=" + oEntity.name + "&action=Edit&entitylevel=" + m_InitialEntityLevel;
                window.location = sURL;
                pleaseWait.Show();
            }
            else
            {//abansal23 for MITS 14847
                var txtItem = oEntity.id.substring(0, oEntity.id.lastIndexOf('_') + 1);
                var txtItemId = txtItem + 'txtItemData';
                getOrg(oEntity.name, oEntity.innerText, null, null, txtItemId);
            }
            return false;
        }
        function UpdateSearchCriteria()
        {
            pleaseWait.Show();
            return true;
        }
        function LoadListPage()
        {
            pleaseWait.Show();
            return true;
        }     
    </script>
   
    
</head>
<body onload="javascript:UpdateInitialEntityLevel()">
    <form id="frmData" runat="server">
        <div id="maindiv" style="width:100%;overflow:auto">
        <uc1:ErrorControl ID="ErrorControl" runat="server" />
        <table runat="server" id="tbData" visible="false" style="width:100%" border="0">
				<tr>
		    		<td width="100%" valign="middle" align="center">
						<font face="Verdana, Arial" size="4">
							<b>Record(s) matching the criteria</b>
						</font>
					</td>
				</tr>
		</table>
		<table runat="server" id="tbNoData" visible="false" style="width:100%" border="0">
				<tr>
		    		<td width="100%" valign="middle" align="center">
						<font face="Verdana, Arial" size="4">
							<b>Zero records match the criteria</b>
						</font>
					</td>
				</tr>
		</table>
		<table width="100%">
		    <tr>
		        <td width="60%">
		            <div style="width:100%;height:450px;" class="divScroll" runat="server" id="grpData">
            <asp:DataList id="ItemsList"
               cellpadding="1"
               cellspacing="1"
              
               HeaderStyle-Font-Names="Verdana"
               headerstyle-font-size="12pt"
               headerstyle-horizontalalign="center"
               headerstyle-font-bold="true"
               footerstyle-font-size="9pt"
               footerstyle-font-italic="true"
               runat="server"
               EnableViewState="false"
               ShowHeader="false"
               >
             
            <HeaderTemplate>
                <div runat="server" id="header">Record(s) matching the criteria</div>
             </HeaderTemplate>
            
            <%--<AlternatingItemStyle BackColor="Gainsboro">
             </AlternatingItemStyle>--%>
                            <ItemTemplate>
                                <asp:LinkButton ID="lstItem" name='<% # ((DataRowView)Container.DataItem)["ENTITY_ID"] %>'
                                    runat="server" CommandName="select" OnClientClick="return EntitySelected(this);"
                                    Text='<%# ((DataRowView)Container.DataItem)["ABBREVIATION"] + " - " + 
             ((DataRowView)Container.DataItem)["LAST_NAME"] %> ' />
                                <%--abansal23 on 05/14/2009 for Group Association Starts --%>
                                <%if (m_sInitialEntityLevel == "1012")
                                  {%>
                                <asp:TextBox ID="txtItemData" runat="server" Style="display: none" Text='<%# ((DataRowView)Container.DataItem)["FACILITY_EID"] + "//" + ((DataRowView)Container.DataItem)["LOCATION_EID"] + "//" + ((DataRowView)Container.DataItem)["DIVISION_EID"] + "//" + ((DataRowView)Container.DataItem)["REGION_EID"] + "//" + ((DataRowView)Container.DataItem)["OPERATION_EID"] + "//" + ((DataRowView)Container.DataItem)["COMPANY_EID"] + "//" + ((DataRowView)Container.DataItem)["CLIENT_EID"] + "//"%>'></asp:TextBox>
                                <%} %>
            </ItemTemplate>
      </asp:DataList>
        </div>
		        </td>
		        <td width="40%">
		        <OrgSearch:OrgSearchCriteriaControl ID="OrgSearch" runat="server" />
		        <asp:HiddenField ID="hd_flgMaintScreen" runat="server" />
        <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="_" />
		        </td>
		    </tr>
		</table>
        
        
    
        
        
        </div>
    </form>
</body>
</html>
