﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EntityExposure_RollUp.aspx.cs" Inherits="Riskmaster.UI.OrganisationHierarchy.EntityExposure_RollUp" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>RollUp Report</title>
    <script language="javascript" type="text/javascript">
        function ConfirmPrint()
        {
            if(!confirm(document.getElementById('hdnStatusDesc').value + "\nDo you want to print the Roll-Up log report?"))
            {
                self.close();
            }
            else
            {
                document.getElementById('hdnAction').value = 'print';
                document.forms[0].submit();
            }
        }
        
    </script>
</head>
<body onload="ConfirmPrint()">
    <form id="frmData" runat="server">
    <div>
        <uc1:ErrorControl ID="ErrorControl" runat="server" />
        <asp:HiddenField id="hdnStatusDesc" runat="server" />
        <asp:HiddenField ID="hdnAction" runat="server" />
    </div>
    </form>
</body>
</html>
