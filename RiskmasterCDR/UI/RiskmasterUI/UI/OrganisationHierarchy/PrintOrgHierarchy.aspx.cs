﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.OrganisationHierarchy
{
    public partial class PrintOrgHierarchy : System.Web.UI.Page
    {
        /// <summary>
        /// Contains web service output
        /// </summary>
        XmlDocument oFDMPageDom = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                oFDMPageDom = new XmlDocument();
                string sReturn = "";

                //Added for MITS:16164-Wrong records coming in pdf of Org Hierarchy:Filter by Effective Date not working
                chkFilter.Text = AppHelper.GetQueryStringValue("chkFilter");
                //Added for MITS:16164-Wrong records coming in pdf of Org Hierarchy:Filter by Effective Date not working

                //Preparing XML to send to service
                XElement oMessageElement = GetMessageTemplate();
                
                //Calling Service
                CommonFunctions.CallService(oMessageElement, ref oFDMPageDom, ref sReturn);

                //Binding Error Control
                ErrorControl.errorDom = sReturn;

                XmlElement oEle = (XmlElement)oFDMPageDom.SelectSingleNode("//File");
                if (oEle != null)
                {
                    string sFileContent = oEle.InnerText;
                    byte[] byteOrg = Convert.FromBase64String(sFileContent);

                    Response.Clear();
                    Response.Charset = "";
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-Disposition", "attachment;filename=OrgHierarchyReport.pdf");
                    Response.BinaryWrite(byteOrg);
                    Response.End();
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);

            }

        }
        /// <summary>
        /// This function gets the XML for the webservice call
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            string slevel;
            slevel = AppHelper.GetQueryStringValue("sLevel");//mbahl3 Mit:25694 org hierarchy Print out
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization>1f5c22e4-6909-49bb-b541-061e650a5d79</Authorization> 
            <Call>
              <Function>OrgHierarchyAdaptor.PrintOrg</Function> 
              </Call>
            <Document>
            <InputXml>
                 <form bcolor='white' cid='orgtree' name='frmData' title='' topbuttons='1'>
                 <toolbar>
                  <button title='New' type='new' /> 
                  <button title='Edit' type='edit' /> 
                  </toolbar>
                  <body1 func='windowstatus_new()' req_func='yes' /> 
                 <group method='post' name='table_detail' title='Table Detail'>
                 <control name='orgTree' num_level='8' type='orgTree'>
                  <org child_name='client' ischild='' level='' name='Organization Hierarchy' parent='' /> 
                  </control>
                  <control name='dlevel' type='hidden' userid='2'>"+slevel+@"</control> 
                  </group>
                 <levellist name='cmb_dlevel'>
                  <option value='C'>Client</option> 
                  <option value='CO'>Company</option> 
                  <option value='O'>Operation</option> 
                  <option value='R'>Region</option> 
                  <option value='D'>Division</option> 
                  <option value='L'>Location</option> 
                  <option value='F'>Facility</option> 
                  <option value='DT'>Department</option> 
                  </levellist>
                  <chkFilter>" + chkFilter.Text + @"</chkFilter>
                  </form>
            </InputXml>
              </Document>
           </Message>
            ");
            return oTemplate;
        }
        
    }
}
