﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrgHierarchyLookup.aspx.cs" Inherits="Riskmaster.Views.OrganisationHierarchy.OrgHierarchyLookup" EnableEventValidation="false"%>
<%@ Register tagprefix="OrgTree" Tagname="OrgTreeViewControl" Src="~/UI/Shared/Controls/OrgTree.ascx"%>
<%@ Register TagPrefix="OrgSearch" TagName="OrgSearchCriteriaControl" Src="~/UI/Shared/Controls/OrgSearchCriteria.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Org Hierarchy</title><%--
    <link href= "../../Content/rmnet.css" rel="stylesheet" type="text/css" />--%>
    <script language="javascript" type="text/javascript" src="../../Scripts/org.js"></script>
    <script type="text/javascript" language="javascript" src="../../Scripts/form.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/WaitDialog.js">{var i;}</script>
    <script language="javascript" type="text/javascript">
        function UpdateSearchCriteria()
        {
            document.getElementById("OT_hd_slevel").value = document.getElementById("OrgSearch_cmb_slevel").value;
            document.getElementById("OT_hd_txtsorg").value = document.getElementById("OrgSearch_txtsorg").value;
            document.getElementById("OT_hd_txtcity").value = document.getElementById("OrgSearch_txtcity").value;
            document.getElementById("OT_hd_txtstate").value = document.getElementById("OrgSearch_txtstate_codelookup_cid").value;
            document.getElementById("OT_hd_txtzip").value = document.getElementById("OrgSearch_txtzip").value;            
            pleaseWait.Show();
        }
        function LoadListPage()
        {
            var sLevel = GetLevel(document.getElementById("OrgSearch_cmb_slevel").value);
            var sLOB = document.getElementById("OT_hd_lob").value;
            
            var sSearchText = document.getElementById("OrgSearch_txtsorg").value;
            window.location.href = "OrgHierarchyLookupList.aspx?EntityId=" + sLevel + "&Lookup=" + sSearchText + "&Lob=" + sLOB;
            var sCityText = document.getElementById("OrgSearch_txtcity").value;
            var sStateId = document.getElementById("OrgSearch_txtstate_codelookup_cid").value;
            var sStateText = document.getElementById("OrgSearch_txtstate_codelookup").value;
            var sZipText = document.getElementById("OrgSearch_txtzip").value;
            window.location.href = "OrgHierarchyLookupList.aspx?EntityId=" + sLevel + "&Lookup=" + sSearchText + "&City=" + sCityText + "&State=" + sStateId + "&StateText=" + sStateText + "&Zip=" + sZipText + "&Lob=" + sLOB;
            pleaseWait.Show();
            return false;
        }
        function GetLevel(sCode)
        {
            var entityTable = 1012;
            switch(sCode)
		    {
			    case 'C':entityTable=1005;
			    break;
			    case 'CO':entityTable=1006;
			    break;
			    case 'O':entityTable=1007;
			    break;
			    case 'R':entityTable=1008;
			    break;
			    case 'D':entityTable=1009;
			    break;
			    case 'L':entityTable=1010;
			    break;
			    case 'F':entityTable=1011;
			    break;
			    case 'DT':entityTable=1012;
			    break;
			    default:entityTable=1012;
		    }	
		    return entityTable;
        }
        function UpdateParent(nodeText , nodeValue , currentnodeDepth)
        {
            var oLob = document.getElementById('OT_hd_lob');
            if(oLob != null)
            {
                if(oLob.value == currentnodeDepth || oLob.value == "all" || oLob.value == "ALL") {
                    getOrg(nodeValue, nodeText, null, null, 'hdSelectedNodeHierarchyString');
                }
                
            }
            
        }
    </script>
</head>
<body>
        <form id="frmData" runat="server">
       <uc1:ErrorControl ID="ErrorControl" runat="server" />
       <asp:HiddenField runat="server" ID="hdSelectedNodeHierarchyString"/>
       <table width="100%">
        <tr>
            <td style="width:60%">
                <OrgTree:OrgTreeViewControl id="OT" runat="server"></OrgTree:OrgTreeViewControl>       
            </td>
            <td  style="width:40%">
                <table width="100%" border="0" cellspacing="6" cellpadding="4">
            <tr>
                <td width="100%">
                        <OrgSearch:OrgSearchCriteriaControl ID="OrgSearch" runat="server" />
               
                </td>
            </tr>
            </table>
            </td>
            </tr>
            </table>
       
        
        
       
        
        
    
    </form>
    <p>
        &nbsp;</p>
</body>
</html>
