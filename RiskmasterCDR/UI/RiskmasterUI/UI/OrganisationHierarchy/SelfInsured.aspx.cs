﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;

namespace Riskmaster.UI.UI.OrganisationHierarchy
{
    public partial class SelfInsured : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XElement XmlTemplate = null;
            string sCWSresponse = "";
            XmlDocument XmlDoc = new XmlDocument();
            try
            {
                if (!Page.IsPostBack)
                {
                    entityid.Text = AppHelper.GetQueryStringValue("EntityId");
                    entitytableid.Text = AppHelper.GetQueryStringValue("EntityLevel");
                }
                if (SelfInsuredGrid_RowDeletedFlag.Text.ToLower() == "true")
                {
                    string selectedRowId = SelfInsuredSelectedId.Text;
                    XmlTemplate = GetDeletionTemplate(selectedRowId);
                    CallCWS("OrgHierarchyAdaptor.DeleteEntityXSelfInsuredData", XmlTemplate, out sCWSresponse, false, false);
                    SelfInsuredGrid_RowDeletedFlag.Text = "false";
                }
                XmlTemplate = GetMessageTemplate(entityid.Text);
                CallCWSFunctionBind("OrgHierarchyAdaptor.GetEntityXSelfInsuredData", out sCWSresponse, XmlTemplate);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }
        private XElement GetMessageTemplate(string sEntityId)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><SelfInsuredDetails>");
            sXml = sXml.Append("<OrgHierarchy>");
            sXml = sXml.Append("<EntityId>");
            sXml = sXml.Append(sEntityId);
            sXml = sXml.Append("</EntityId>");
            sXml = sXml.Append("</OrgHierarchy>");
            sXml = sXml.Append("<SelfInsuredInfo>");
            sXml = sXml.Append("<listhead>");
            sXml = sXml.Append("<Jurisdiction>Jurisdiction</Jurisdiction>");
            
            sXml = sXml.Append("<CertificateNumber>Certificate No.</CertificateNumber>");
            sXml = sXml.Append("<EffectiveDate>Effective Date</EffectiveDate>");
            sXml = sXml.Append("<ExpirationDate>Expiration Date</ExpirationDate>");
            sXml = sXml.Append("<Authorization>Authorization</Authorization>");
            sXml = sXml.Append("<Organization>Organization</Organization>");
            //sgoel6 Medicare 05/12/2009
            //sXml = sXml.Append("<LineOfBusCodeSelfInsured>Line Of Business</LineOfBusCodeSelfInsured>");
            sXml = sXml.Append("<LobCode>Line Of Business</LobCode>");
            sXml = sXml.Append("<IsCertDiffFlag>Different Certificate?</IsCertDiffFlag>");
            sXml = sXml.Append("<CertNameEid>Self Insured Certificate Name</CertNameEid>");
            sXml = sXml.Append("</listhead>");
            sXml = sXml.Append("</SelfInsuredInfo></SelfInsuredDetails></Document></Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;

        }
        private XElement GetDeletionTemplate(string selectedRowId)
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document><SelfInsuredDetails>");
            sXml = sXml.Append("<SIRowId>");
            sXml = sXml.Append(selectedRowId);
            sXml = sXml.Append("</SIRowId>");
            sXml = sXml.Append("</SelfInsuredDetails></Document>");
            sXml = sXml.Append("</Message>");
            XElement oElement = XElement.Parse(sXml.ToString());
            return oElement;

        }
    }
}