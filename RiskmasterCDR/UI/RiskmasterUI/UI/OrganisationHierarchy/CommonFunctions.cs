﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;

namespace Riskmaster.UI.OrganisationHierarchy
{
    public class CommonFunctions
    {
        /// <summary>
        /// Client short name
        /// </summary>
        public const string CLIENT = "C";

        /// <summary>
        /// Company short name
        /// </summary>
        public const string COMPANY = "CO";

        /// <summary>
        /// Operation short name
        /// </summary>
        public const string OPERATION = "O";

        /// <summary>
        /// Region short name
        /// </summary>
        public const string REGION = "R";

        /// <summary>
        /// Location short name
        /// </summary>
        public const string LOCATION = "L";

        /// <summary>
        /// Division short name
        /// </summary>
        public const string DIVISION = "D";

        /// <summary>
        /// Facility short name
        /// </summary>
        public const string FACILITY = "F";

        /// <summary>
        /// Department short name
        /// </summary>
        public const string DEPARTMENT = "DT";

        /// <summary>
        /// All levels of Organization Hierarchy
        /// </summary>
        public const string ALL = "A";
        /// <summary>
        /// Various Levels for the Organisation Hierarchy Tree
        /// </summary>

        public enum enumOrgHierarchyLevels : int
        {
            CLIENT = 1,
            COMPANY = 2,
            OPERATION = 3,
            REGION = 4,
            DIVISION = 5,
            LOCATION = 6,
            FACILITY = 7,
            DEPARTMENT = 8
        };
        /// <summary>
        /// This function would call the webservice to get output
        /// </summary>
        /// <param name="oMessageElement"></param>
        /// <param name="oFDMPageDom"></param>
        public static void CallService(XElement oMessageElement, ref XmlDocument oXMLOut)
        {
            string sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
            oXMLOut.LoadXml(sReturn);
            XmlNode oInstanceNode = oXMLOut.SelectSingleNode("/ResultMessage/Document");
            XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
            oXMLOut.LoadXml(oInstanceNode.OuterXml);
            
        }
        /// <summary>
        /// This function would call the webservice to get output and return ouput string
        /// </summary>
        /// <param name="oMessageElement"></param>
        /// <param name="oFDMPageDom"></param>
        public static void CallService(XElement oMessageElement, ref XmlDocument oXMLOut , ref string p_sReturn)
        {
            p_sReturn = AppHelper.CallCWSService(oMessageElement.ToString());
            oXMLOut.LoadXml(p_sReturn);
            XmlNode oInstanceNode = oXMLOut.SelectSingleNode("/ResultMessage/Document");
            XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);
            oXMLOut.LoadXml(oInstanceNode.OuterXml);
            
        }
        /// <summary>
        /// Fetches the Entity level of the Org Hierarchy
        /// </summary>
        /// <param name="p_sNodCat">Level</param>
        /// <returns>Level depending upon the given parameter</returns>
        public static int GetEntityLevel(string p_sNodCat)
        {
            switch (p_sNodCat.ToUpper())
            {
                case CLIENT:
                    return 1005;
                case COMPANY:
                    return 1006;
                case OPERATION:
                    return 1007;
                case REGION:
                    return 1008;
                case DIVISION:
                    return 1009;
                case LOCATION:
                    return 1010;
                case FACILITY:
                    return 1011;
                case DEPARTMENT:
                    return 1012;
                default:
                    return 1012;
            }
        }
        /// <summary>
        /// Fetches the Entity level of the Org Hierarchy
        /// </summary>
        /// <param name="p_iDepth"></param>
        /// <returns></returns>
        public static int GetEntityLevel(int p_iDepth)
        {
            int iLevel = 1012;
            switch (p_iDepth)
            {
                case 1: iLevel = 1005; break;
                case 2: iLevel = 1006; break;
                case 3: iLevel = 1007; break;
                case 4: iLevel = 1008; break;
                case 5: iLevel = 1009; break;
                case 6: iLevel = 1010; break;
                case 7: iLevel = 1011; break;
                case 8: iLevel = 1012; break;
                default: iLevel = 1012; break;

            }
            return iLevel;
        }
        /// <summary>
        /// Get Level category code form level number
        /// </summary>
        /// <param name="p_iLevel"></param>
        /// <returns></returns>
        public static string GetCatFromLevel(long p_iLevel)
        {
            switch (p_iLevel)
            {
                case 1:
                case 1005:
                    return CLIENT;
                case 2:
                case 1006:
                    return COMPANY;
                case 3:
                case 1007:
                    return OPERATION;
                case 4:
                case 1008:
                    return REGION;
                case 5:
                case 1009:
                    return DIVISION;
                case 6:
                case 1010:
                    return LOCATION;
                case 7:
                case 1011:
                    return FACILITY;
                case 8:
                case 1012:
                    return DEPARTMENT;
                default:
                    return CLIENT;
            }
        }
        /// <summary>
        /// Fetches the level of the Org Hierarchy
        /// </summary>
        /// <param name="p_sNodCat">Level</param>
        /// <returns>Level depending upon the given parameter</returns>
        public static int GetLevel(string p_sNodCat)
        {
            switch (p_sNodCat.ToUpper())
            {
                case CLIENT:
                    return 1;
                case COMPANY:
                    return 2;
                case OPERATION:
                    return 3;
                case REGION:
                    return 4;
                case DIVISION:
                    return 5;
                case LOCATION:
                    return 6;
                case FACILITY:
                    return 7;
                case DEPARTMENT:
                    return 8;
                default:
                    return 1;
            }
        }	
    }
}
