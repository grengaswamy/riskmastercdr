﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.AppHelpers;
using Riskmaster.UI.Shared.Controls;

namespace Riskmaster.UI.OrganisationHierarchy
{
    public partial class OrgHierarchyMaintenance : System.Web.UI.Page
    {
        
        /// <summary>
        /// Contains web service output
        /// </summary>
        XmlDocument oFDMPageDom = null;
        TextBox otxtsorg = null;
        TextBox otxtcity = null;
        TextBox otxtstate_codelookup_cid = null;
        CodeLookUp otxtstate = null;
        TextBox otxtzip = null;
        string sLevel = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                oFDMPageDom = new XmlDocument();
                string sReturn = "";
                otxtsorg = (TextBox)OrgSearch.FindControl("txtsorg");
                //MITS 16771- Pankaj 5/29/09
                otxtcity = (TextBox)OrgSearch.FindControl("txtcity");
                otxtstate = (CodeLookUp)OrgSearch.FindControl("txtstate");
                otxtzip = (TextBox)OrgSearch.FindControl("txtzip");
                if (Page.IsPostBack)
                {
                    //Finding out the control which caused the postback
                    Control oControl = DatabindingHelper.GetPostBackControl(Page);

                    //Checking of clone button has been pressed
                    if (oControl != null && oControl.GetType().ToString() == "System.Web.UI.WebControls.Button")
                    {
                        Button oButton = (Button)oControl;
                        if (oButton != null && oButton.ID == "btnClone")
                        {
                            //Preparing XML to send to service
                            XElement oMessageElement = GetMessageTemplate();

                            //Modify XML if needed
                            ModifyTemplate(oMessageElement);

                            //Calling Service to complete cloning
                            CommonFunctions.CallService(oMessageElement, ref oFDMPageDom, ref sReturn);

                            //Binding Error Control
                            ErrorControl.errorDom = sReturn;
                        }
                    }

                }
                if (!Page.IsPostBack)
                {
                    sLevel = HttpContext.Current.Request.QueryString["level"];
                    otxtsorg.Text = HttpContext.Current.Request.QueryString["txtorg"];
                    //MITS 16771- Pankaj 5/29/09
                    otxtcity.Text = HttpContext.Current.Request.QueryString["txtcity"];
                    otxtstate.CodeId = HttpContext.Current.Request.QueryString["txtstate"];
                    otxtstate.CodeText = HttpContext.Current.Request.QueryString["txtstatetext"];
                    otxtzip.Text = HttpContext.Current.Request.QueryString["txtzip"];
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
            
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            //Populating the "Search Selected Level" dropdownlist
            Control oControl = OT.FindControl("cmb_dlevel"); 
            ListItem iListItem = null;
            if (oControl != null)
            {
                Control oSearchControl = OrgSearch.FindControl("cmb_slevel");
                if (((DropDownList)oSearchControl).Items.Count == 0)
                {
                    foreach (ListItem iItem in ((DropDownList)oControl).Items)
                    {
                        //Added for Mits 18705-Start
                        //((DropDownList)oSearchControl).Items.Add(iItem);
                        iListItem = new ListItem(iItem.Text, iItem.Value);
                        ((DropDownList)oSearchControl).Items.Add(iListItem);
                        //Added for Mits 18705-End      
                    }
                    //if(sLevel != null && sLevel != "")
                    if (!string.IsNullOrEmpty(sLevel))
                        ((DropDownList)oSearchControl).SelectedValue = sLevel;
                    else //Added for Mits 18705-Start:To set the search level same as Default Expansion level when the screen opens
                        ((DropDownList)oSearchControl).SelectedValue = ((DropDownList)oControl).SelectedValue;
                    //Added for Mits 18705-End    
                }
                else
                {
                    //DETERMINE IF SEARCH LEVEL IS SET FROM WEBSERVICE THEN SET IT ACCORDINGLY

                    oControl = OT.FindControl("hd_slevel");
                    if (oControl != null && ((HiddenField)oControl).Value != String.Empty)
                    {
                        ((DropDownList)oSearchControl).SelectedValue = ((HiddenField)oControl).Value;
                    }
                    else
                    {
                        ((DropDownList)oSearchControl).SelectedIndex = ((DropDownList)oControl).SelectedIndex;
                    }

                }
                if (cmb_desiredcloninglevel.Items.Count == 0)
                {
                    foreach (ListItem iItem in ((DropDownList)oControl).Items)
                    {
                        ListItem newItem = new ListItem(iItem.Text, iItem.Value);
                        cmb_desiredcloninglevel.Items.Add(newItem);
                    }
                }
                cmb_desiredcloninglevel.SelectedValue = CommonFunctions.CLIENT;
                cmb_desiredcloninglevel.Enabled = false;
                cloninglevelpreference1.Checked = true;
                cloninglevelpreference2.Checked = false;
            }

        }
        
        /// <summary>
        /// This function gets the template for web service call
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            //sgoel6 | MITS 15063 | ParentName node added
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization>1f5c22e4-6909-49bb-b541-061e650a5d79</Authorization> 
            <Call>
              <Function>OrgHierarchyAdaptor.CreateClone</Function> 
              </Call>
            <Document>
                <OrgHierarchy>
                <CloningLevelPreference></CloningLevelPreference> 
                <DesiredCloningLevel></DesiredCloningLevel> 
                <SelectedLevelId></SelectedLevelId>
                <CompanyInfo>
                    <DepartmentInfo>
                  <LastUpdate /> 
                  <BusinessTypeCode codeid='' /> 
                  <County /> 
                  <NatureOfBusiness /> 
                  <SicCode codeid='' /> 
                  <SicCodeDesc /> 
                  <NAICSCode codeid='' /> 
                  <WCFillingNumber /> 
                  <EntityTableId /> 
                  <LastName /> 
                  <LastNameSoundex /> 
                  <FirstName /> 
                  <AlsoKnownAs /> 
                  <Abbreviation /> 
                  <CCCode /> 
                  <Addr1 /> 
                  <Addr2 /> 
                  <Addr3 /> 
                  <Addr4 /> 
                  <City /> 
                  <CountryCode codeid='' /> 
                  <StateId codeid='' /> 
                  <ZipCode /> 
                  <ParentName />
                  <ParentEID /> 
                  <TaxID /> 
                  <Contact /> 
                  <ClaimInstruction /> 
                  <EmailTypeCode codeid='' /> 
                  <EmailAddress /> 
                  <SexCode /> 
                  <BirthDate /> 
                  <Phone1 /> 
                  <Phone2 /> 
                  <FaxNumber /> 
                  <DeletedFlag /> 
                  <EffectiveStartDate /> 
                  <EffectiveEndDate /> 
                  <TriggerDateField /> 
                  <RMUSerID /> 
                  <FreezePayments /> 
                  <Parent1099EID codeid='' /> 
                  <Report1099Flag /> 
                  <MiddleName /> 
                  <Title /> 
                  <EntityID /> 
                  <DateTimeAdded /> 
                  <AddedByUser /> 
                  <EntityApprovalStatusCode codeid='' /> 
                  <RejectReasonText />   
                  <RejectReasonText_HTMLComments />                  
                 <OperationInfo>
                 <listhead>
                  <OperatingID>Operating ID</OperatingID> 
                  <Initial>Initial</Initial> 
                  <OperatingName>Name</OperatingName> 
                  </listhead>
                 <Operating>
                  <OperatingID>0</OperatingID> 
                  <Initial /> 
                  <OperatingName /> 
                  </Operating>
                  </OperationInfo>
                 <ContactInfo>
                 <listhead>
                  <ContactID>Contact ID</ContactID> 
                  <ContactName>Name</ContactName> 
                  <ContactTitle>Title</ContactTitle> 
                  <ContactInitials>Initials</ContactInitials> 
                  <Addr1>Address1</Addr1> 
                  <Addr2>Address2</Addr2> 
                  <Addr3>Address3</Addr3>
                  <Addr4>Address4</Addr4>
                  <ContactCity>City</ContactCity> 
                  <State codeid=''>State</State> 
                  <ZipCode>Zip Code</ZipCode> 
                  <Phone>Phone</Phone> 
                  <FaxNumber>Fax No.</FaxNumber> 
                  <EMailAddress>E Mail</EMailAddress> 
                  </listhead>
                 <Contact>
                  <ContactID /> 
                  <ContactName /> 
                  <ContactTitle /> 
                  <ContactInitials /> 
                  <Addr1 /> 
                  <Addr2 /> 
                  <Addr3 /> 
                  <Addr4 /> 
                  <ContactCity /> 
                  <State codeid='' /> 
                  <ZipCode /> 
                  <Phone /> 
                  <FaxNumber /> 
                  <EMailAddress /> 
                  </Contact>
                  </ContactInfo>
                 <OrgHierarchyInfo>
                  <TableName /> 
                  <TableValue /> 
                  </OrgHierarchyInfo>
                 <ClientLimitsInfo>
                 <listhead>
                  <Client_RowId>Client Row ID</Client_RowId> 
                  <Client_Eid>Entity ID</Client_Eid> 
                  <LOB_CODE>LOB</LOB_CODE> 
                  <RESERVE_TYPE_CODE>Reserve Type</RESERVE_TYPE_CODE> 
                  <MAX_AMOUNT>Max Amount</MAX_AMOUNT> 
                  <PAYMENT_FLAG>Payment Flag</PAYMENT_FLAG> 
                  <Type_Of_Limit>Type Of Limit</Type_Of_Limit> 
                  </listhead>
                 <ClientLimits>
                  <Client_RowId /> 
                  <Client_Eid /> 
                  <LOB_CODE codeid='' /> 
                  <RESERVE_TYPE_CODE codeid='' /> 
                  <MAX_AMOUNT /> 
                  <PAYMENT_FLAG /> 
                  <Type_Of_Limit /> 
                  </ClientLimits>
                  </ClientLimitsInfo>
                  <Supplementals /> 
                  <SuppDef /> 
                 <ExposureInfo>
                  <ExposureRowID /> 
                  <StartDate /> 
                  <EndDate /> 
                  <NoOfEmployees /> 
                  <WorkHrs /> 
                  <PayRollAmt /> 
                  <AssetValue /> 
                  <SquareFootage /> 
                  <VehicleCount /> 
                  <TotalRevenue /> 
                  <OtherBase /> 
                  <RiskMgtOverHead /> 
                  <UserGeneratedFlag /> 
                  </ExposureInfo>
                 <SelfInsuredInfo>
                 <listhead>
                  <SIRowId>SI RowId</SIRowId> 
                  <EntityId>Entity Id</EntityId> 
                  <Jurisdiction>Jurisdiction</Jurisdiction> 
                  <DeletedFlag>Deleted Flag</DeletedFlag> 
                  <LegalEntityId>Legal Entity Id</LegalEntityId> 
                  <CertificateNumber>Certificate No.</CertificateNumber> 
                  <EffectiveDate>Effective Date</EffectiveDate> 
                  <ExpirationDate>Expiration Date</ExpirationDate> 
                  <Authorization>Authorization</Authorization> 
                  <Organization>Organization</Organization> 
                  <DttmRcdAdded>Record Added</DttmRcdAdded> 
                  <DttmRcdLastUpd>Record Updated</DttmRcdLastUpd> 
                  <UpdatedByUser>Updated By User</UpdatedByUser> 
                  <AddedByUser>Added By User</AddedByUser> 
                  </listhead>
                 <SelfInsured>
                  <SIRowId /> 
                  <EntityId /> 
                  <Jurisdiction codeid='' /> 
                  <DeletedFlag /> 
                  <LegalEntityId /> 
                  <CertNameEid codeid='' /> 
                  <CertificateNumber /> 
                  <EffectiveDate /> 
                  <ExpirationDate /> 
                  <Authorization codeid='' /> 
                  <Organization codeid='' /> 
                  <DttmRcdAdded /> 
                  <DttmRcdLastUpd /> 
                  <UpdatedByUser /> 
                  <AddedByUser /> 
                  </SelfInsured>
                  </SelfInsuredInfo>
                 <JurisAndLicenInfo>
                 <listhead>
                  <Jurisdiction>Jurisdiction</Jurisdiction> 
                  <CodeLicenseNumber>Code Or License Number</CodeLicenseNumber> 
                  <EffectiveDate>Effective Date</EffectiveDate> 
                  <ExpirationDate>Expiration Date</ExpirationDate> 
                  <GeneralClaims>General Claims</GeneralClaims> 
                  <ShortTermDisability>Short Term Disability</ShortTermDisability> 
                  <VehicleAccident>Vehicle Accident</VehicleAccident> 
                  <WorkersCompensation>Workers Compensation</WorkersCompensation> 
                  <AllLineOfBusiness>All Line Of Business</AllLineOfBusiness> 
                  </listhead>
                 <JurisAndLicen>
                  <TableRowId /> 
                  <EntityId /> 
                  <Jurisdiction codeid='' /> 
                  <DeletedFlag /> 
                  <CodeLicenseNumber /> 
                  <EffectiveDate /> 
                  <ExpirationDate /> 
                  <DttmRcdAdded /> 
                  <DttmRcdLastUpd /> 
                  <UpdatedByUser /> 
                  <AddedByUser /> 
                  <GeneralClaims codeid='' /> 
                  <ShortTermDisability codeid='' /> 
                  <VehicleAccident codeid='' /> 
                  <WorkersCompensation codeid='' /> 
                  <AllLineOfBusiness codeid='' /> 
                  </JurisAndLicen>
                  </JurisAndLicenInfo>
                  </DepartmentInfo>
            </CompanyInfo>
            </OrgHierarchy>
              </Document>
           </Message>
            ");
            return oTemplate;
        }
        
        /// <summary>
        /// This function would be used to modify the template as and when necessary
        /// </summary>
        /// <param name="oMessageElement"></param>
        private void ModifyTemplate(XElement oMessageElement)
        {
            XElement oElement = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/CloningLevelPreference");
            if (cloninglevelpreference1.Checked)
            {
                oElement.Value = "0";
            }
            else
            {
                oElement.Value = "1";

                oElement = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/DesiredCloningLevel");
                oElement.Value = CommonFunctions.GetEntityLevel(cmb_desiredcloninglevel.SelectedValue).ToString();
            }
            oElement = oMessageElement.XPathSelectElement("./Document/OrgHierarchy/SelectedLevelId");
            oElement.Value = ((HiddenField)OT.FindControl("hd_selectednodevalue")).Value;

        }
        
       
    }
}
