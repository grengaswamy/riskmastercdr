﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.OrganisationHierarchy
{
    public partial class EntityExposure_RollUp : System.Web.UI.Page
    {
        /// <summary>
        /// Contains web service output
        /// </summary>
        XmlDocument oFDMPageDom = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    oFDMPageDom = new XmlDocument();
                    string sReturn = "";

                    //Preparing XML to send to service
                    XElement oMessageElement = GetMessageTemplate();

                    //Calling Service 
                    CommonFunctions.CallService(oMessageElement, ref oFDMPageDom , ref sReturn);

                    //Binding Error Control
                    ErrorControl.errorDom = sReturn;

                    //Storing Status in the hidden field for display on client side
                    XmlElement oEle = (XmlElement)oFDMPageDom.SelectSingleNode("//StatusDesc");
                    if (oEle != null)
                    {
                        hdnStatusDesc.Value = oEle.InnerText;
                    }

                    oEle = (XmlElement)oFDMPageDom.SelectSingleNode("//File");
                    if (oEle != null)
                    {
                        string sFileContent = oEle.InnerText;
                        string sFileName = oEle.GetAttribute("Name");
                        ViewState["FileContent"] = sFileContent;
                        ViewState["FileName"] = sFileName;
                    }
                }
                else
                {
                    if (hdnAction.Value == "print")
                    {
                        string sFileContent = ViewState["FileContent"].ToString();
                        string sFileName = ViewState["FileName"].ToString();
                        byte[] byteOrg = Convert.FromBase64String(sFileContent);
                        Response.Clear();
                        Response.Charset = "";
                        Response.ContentType = "application/pdf";
                        Response.AddHeader("Content-Disposition", ("inline;filename= " + sFileName));
                        Response.BinaryWrite(byteOrg);
                        Response.End();

                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
        /// <summary>
        /// This function gets the XML for the webservice call
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization>1f5c22e4-6909-49bb-b541-061e650a5d79</Authorization> 
              <Call>
                <Function>EntityExposureAdaptor.RollUp</Function> 
              </Call>
              <Document>
                <Document>
                <dummy /> 
                <StatusDesc /> 
                </Document>
              </Document>

           </Message>
            ");
            return oTemplate;
        }
    }
}
