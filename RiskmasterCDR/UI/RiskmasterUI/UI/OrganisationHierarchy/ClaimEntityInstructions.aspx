﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimEntityInstructions.aspx.cs" Inherits="Riskmaster.UI.OrganisationHierarchy.ClaimEntityInstructions" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Claim Entity Instructions</title>
    <link rel="Stylesheet" type="text/css" href="../../Content/rmnet.css" />
    <link rel="stylesheet" type="text/css" href="../../Content/zpcal/themes/system.css" />
</head>
<body class="10pt">
    <form id="frmData" runat="server">
    <uc1:ErrorControl ID="ErrorControl" runat="server" />
    <div id="div_Instructions" runat="server"  >
    <asp:Literal runat="server" ID="ClaimInstructions" ></asp:Literal>
    </div>
  
  <asp:textbox ID="EId" style="display:none" RMXRef="Instance/Document/EId" runat="server" />
    
    </form>
</body>
</html>
