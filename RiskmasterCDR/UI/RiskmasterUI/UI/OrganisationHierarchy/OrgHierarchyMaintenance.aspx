﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrgHierarchyMaintenance.aspx.cs" Inherits="Riskmaster.UI.OrganisationHierarchy.OrgHierarchyMaintenance" EnableEventValidation="false" ValidateRequest="false"%>
<%@ Register tagprefix="OrgTree" Tagname="OrgTreeViewControl" Src="~/UI/Shared/Controls/OrgTree.ascx"%>
<%@ Register TagPrefix="OrgSearch" TagName="OrgSearchCriteriaControl" Src="~/UI/Shared/Controls/OrgSearchCriteria.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Org Hierarchy</title>
     <script type="text/javascript" language="JavaScript" src="../../Scripts/WaitDialog.js">{var i;}
  </script>
  <script type="text/javascript" language="javascript" src="../../Scripts/form.js"></script>
  <script type="text/javascript" language="javascript" src="../../Scripts/org.js"></script>
    <script language="javascript" type="text/javascript">
       
    </script>
</head>
<body onload="scrollNodeIntoView();parent.MDIScreenLoaded();">
    <form id="frmData" runat="server">
       
       <div style="width:100%">
       <uc1:ErrorControl ID="ErrorControl" runat="server" />
       </div>
       <div id="toolbardrift" name="toolbardrift" class="toolbardrift" style="width:100%">
			<div class="image">
			   <asp:ImageButton runat="server" OnClientClick="return checknew();" ID="btnNew" 
                    ImageUrl="../../Images/tb_new_active.png" Width="28" Height="28" BorderStyle="None" 
                    AlternateText="New" ToolTip="New" 
                    onMouseOver="this.src='../../Images/tb_new_mo.png';this.style.zoom='110%'" 
                    onMouseOut="this.src='../../Images/tb_new_active.png';this.style.zoom='100%'" 
                    />
			</div>
			<div class="image">
			  <asp:ImageButton runat="server" OnClientClick="return checkedit();" ID="btnEdit" 
                    AlternateText="Edit" ImageUrl="../../Images/tb_edit_active.png" ToolTip="Edit" 
                    width="28" height="28" border="0" alt="" 
                    onMouseOver="this.src='../../Images/tb_edit_mo.png';this.style.zoom='110%'" 
                    onMouseOut="this.src='../../Images/tb_edit_active.png';this.style.zoom='100%'" 
                    />
			</div>
			<div class="image">
			  <asp:ImageButton runat="server" OnClientClick="return printOrg();" ID="btnPrintOrg" 
                    AlternateText="Print" ImageUrl="../../Images/tb_print_active.png" ToolTip="Print" 
                    width="28" height="28" border="0" alt="" 
                    onMouseOver="this.src='../../Images/tb_print_mo.png';this.style.zoom='110%'" 
                    onMouseOut="this.src='../../Images/tb_print_active.png';this.style.zoom='100%'" 
                    />
			</div>
       </div>
       
       <table width="100%">
        <tr>
            <td style="width:60%">
                <OrgTree:OrgTreeViewControl id="OT" runat="server"></OrgTree:OrgTreeViewControl>         
            </td>
            <td  style="width:40%">
                <table width="100%" border="0" cellspacing="6" cellpadding="4">
            <tr>
                <td width="100%">
                        <OrgSearch:OrgSearchCriteriaControl ID="OrgSearch" runat="server" />
               
                </td>
            </tr>
            <tr>
            <td></td>
            </tr>
            <tr>
            <td></td>
            </tr>
        &nbsp;&nbsp;   
        
        
            <tr class="ctrlgroup">
              <td width="100%" colspan="2">Hierarchy Cloning -- From Selected Level</td>
            </tr>
            <tr>
              <td colspan="2">
                <asp:RadioButton ID="cloninglevelpreference1" runat="server" Text="Clone to Next Level" Checked="true" onclick="if(document.getElementById('cloninglevelpreference1').checked == true) {document.getElementById('cloninglevelpreference2').checked = false;document.getElementById('cmb_desiredcloninglevel').disabled= true;}"/>
              </td>
            </tr>
            <tr>
              <td colspan="2">
                <asp:RadioButton ID="cloninglevelpreference2" runat="server" Text="Clone Through " Checked="false" onclick="if(document.getElementById('cloninglevelpreference2').checked == true) {document.getElementById('cloninglevelpreference1').checked = false;document.getElementById('cmb_desiredcloninglevel').disabled= false;}"/>
                <asp:DropDownList runat="server" ID="cmb_desiredcloninglevel"></asp:DropDownList>
                
            
                Level
              </td>
            </tr>
            <tr>
              <td colspan="2" align="left">
                <asp:Button CssClass="button" Text="Clone" runat="server" ID="btnClone" OnClientClick="return ValidateCloning();"/>
            
              </td>
            </tr>
           
          </table>
        
        
        
        
            </td>
        </tr>
        
       </table>
       
             
        &nbsp;&nbsp;
       
        
        

    </form>
    <p>
        &nbsp;</p>
</body>
</html>
