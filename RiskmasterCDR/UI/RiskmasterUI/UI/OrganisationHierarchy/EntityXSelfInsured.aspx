<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EntityXSelfInsured.aspx.cs"
    Inherits="Riskmaster.UI.UI.OrganisationHierarchy.EntityXSelfInsured" %>

<%@ Register TagPrefix="uc1" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<%@ Register TagPrefix="uc" TagName="CodeLookUp" Src="~/UI/Shared/Controls/CodeLookUp.ascx" %>
<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"
    TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Self Insured Details</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />

    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/supportscreens.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/grid.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/utilities.js">        { var i; }</script>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/WaitDialog.js">        { var i; }</script>
    <%--vkumar258 - RMA_6037- Starts --%>
    <%--<script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">        { var i; }</script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }</script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }</script>--%>
    <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <%--vkumar258 - RMA_6037- End --%>

    <script language='javascript' type="text/javascript">

        function DisableCertName() {
            if(document.getElementById('chkIsCertDiff') != null)
            {
                if(!document.forms[0].chkIsCertDiff.checked)
                {
                    document.getElementById('trCertName').style.display = "none";
                    document.getElementById('CertNameEid').value = "";
                    document.forms[0].CertNameEid_cid.value = "0";
                }
                else
                {
                    document.getElementById('trCertName').style.display = "";
                }
            }
        }
    </script>
</head>
<body onload="CopyGridRowDataToPopup();DisableCertName();">
    <form id="frmData" runat="server">
    <div class="msgheader" id="div_formtitle">
        <asp:Label ID="formtitle" runat="server" Text="Self Insured Details" />
    </div>
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <%--<asp:TextBox Style="display: none" runat="server" ID="RowId" RMXRef="/Instance/Document//ReturnXml/OutputResult/xml/SIRowId"
        rmxignoreset='true' />--%>
    <asp:TextBox Style="display: none" runat="server" ID="selectedid" />
    <asp:TextBox Style="display: none" runat="server" ID="mode" />
    <asp:TextBox Style="display: none" runat="server" ID="selectedrowposition" />
    <asp:TextBox Style="display: none" runat="server" ID="gridname" />
    <asp:TextBox Style="display: none" runat="server" ID="UniqueId" Text="RowId" />
    <asp:TextBox Style="display: none" runat="server" ID="SIRowId" RMXRef="/Instance/Document//ReturnXml/OutputResult/xml/SIRowId"
     rmxignoreset='true'   RMXType="id" />
    <asp:TextBox Style="display: none" runat="server" ID="EntityId" RMXRef="/Instance/Document//ReturnXml/OutputResult/xml/EntityId"
       rmxignoreset='true' RMXType="id" />
    <asp:TextBox Style="display: none" runat="server" ID="DeletedFlag" RMXRef="/Instance/Document//ReturnXml/OutputResult/xml/DeletedFlag"
        RMXType="id" />
    <asp:TextBox Style="display: none" runat="server" ID="LegalEntityId" RMXRef="/Instance/Document//ReturnXml/OutputResult/xml/LegalEntityId"
        RMXType="id" />
    <asp:TextBox Style="display: none" runat="server" ID="DttmRcdAdded" RMXRef="/Instance/Document//ReturnXml/OutputResult/xml/DttmRcdAdded"
        RMXType="id" />
    <asp:TextBox Style="display: none" runat="server" ID="DttmRcdLastUpd" RMXRef="/Instance/Document//ReturnXml/OutputResult/xml/DttmRcdLastUpd"
        RMXType="id" />
    <asp:TextBox Style="display: none" runat="server" ID="UpdatedByUser" RMXRef="/Instance/Document//ReturnXml/OutputResult/xml/UpdatedByUser"
        RMXType="id" />
    <asp:TextBox Style="display: none" runat="server" ID="AddedByUser" RMXRef="/Instance/Document//ReturnXml/OutputResult/xml/AddedByUser"
        RMXType="id" />
    <table cellspacing="0" cellpadding="0" border="0">
        <tbody>
            <%--<tr class="ctrlgroup2">
                <td colspan="2">
                    <asp:Label runat="server" name="BRPanPlan" ID="BRPanPlan" Text="Add/Modify Pay Plan" />
                </td>
            </tr>--%>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="label" ID="lbl_Jurisdiction" Text="Jurisdiction:" />&nbsp;&nbsp;
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="Jurisdiction" CodeTable="states" ControlName="Jurisdiction"
                        RMXRef="/Instance/Document//ReturnXml/OutputResult/xml/Jurisdiction" RMXType="code" TabIndex="1" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="label" ID="lbl_CertificateNumber" Text="Certificate Number:" />&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" onchange="setDataChanged(true);" ID="CertificateNumber"
                        RMXRef="/Instance/Document//ReturnXml/OutputResult/xml/CertificateNumber" RMXType="text" TabIndex="4" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="label" ID="lbl_Authorization" Text="Authorization Type:" />&nbsp;&nbsp;
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="Authorization" CodeTable="SI_AUTH_TYPE" ControlName="Authorization"
                        RMXRef="/Instance/Document//ReturnXml/OutputResult/xml/Authorization" RMXType="code" TabIndex="5" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="label" ID="lbl_Organization" Text="Organization Type:" />&nbsp;&nbsp;
                </td>
                <td>
                    <uc:CodeLookUp runat="server" ID="Organization" CodeTable="SI_ORG_TYPE" ControlName="Organization"
                        RMXRef="/Instance/Document//ReturnXml/OutputResult/xml/Organization" RMXType="code" TabIndex="6" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="label" ID="lbl_EffectiveDate" Text="Effective Date:" />&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" FormatAs="date" ID="EffectiveDate" RMXRef="/Instance/Document//ReturnXml/OutputResult/xml/EffectiveDate"
                        RMXType="date" TabIndex="6" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                        <%--vkumar258 - RMA_6037- Starts --%>
                        <%--<asp:Button class="DateLookupControl" runat="server" ID="EffectiveDatebtn" TabIndex="7" />

                    <script type="text/javascript">
                        Zapatec.Calendar.setup(
					{
					    inputField: "EffectiveDate",
					    ifFormat: "%m/%d/%Y",
					    button: "EffectiveDatebtn"
					}
					);
                    </script>--%>
                        <script type="text/javascript">
                            $(function () {
                                $("#EffectiveDate").datepicker({
                                    showOn: "button",
                                    buttonImage: "../../Images/calendar.gif",
                                    //buttonImageOnly: true,
                                    showOtherMonths: true,
                                    selectOtherMonths: true,
                                    changeYear: true
                                }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "7");
                            });
                        </script>
                        <%--vkumar258 - RMA_6037- Starts --%>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:label runat="server" class="label" id="lbl_ExpirationDate" text="Expiration Date:" />
                        &nbsp;&nbsp;
                    </td>
                    <td>
                        <asp:textbox runat="server" formatas="date" id="ExpirationDate" rmxref="/Instance/Document//ReturnXml/OutputResult/xml/ExpirationDate"
                            rmxtype="date" tabindex="8" onchange="setDataChanged(true);" onblur="dateLostFocus(this.id);" />
                        <%--vkumar258 - RMA_6037- Starts --%>
                        <%-- <asp:Button class="DateLookupControl" runat="server" ID="ExpirationDatebtn" TabIndex="8" />

                    <script type="text/javascript">
                        Zapatec.Calendar.setup(
					{
					    inputField: "ExpirationDate",
					    ifFormat: "%m/%d/%Y",
					    button: "ExpirationDatebtn"
					}
					);
                    </script>--%>
                        <script type="text/javascript">
                            $(function () {
                                $("#ExpirationDate").datepicker({
                                    showOn: "button",
                                    buttonImage: "../../Images/calendar.gif",
                                    //buttonImageOnly: true,
                                    showOtherMonths: true,
                                    selectOtherMonths: true,
                                    changeYear: true
                                }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "9");
                            });
                        </script>
                        <%--vkumar258 - RMA_6037- End --%>
                    </td>
                </tr>
                <!--<tr>
                <td class="required">
                    <asp:Label runat="server" class="label" ID="lblLOB" Text="Line Of Business:" />
                </td>                
                <td>
                    <uc:CodeLookUp runat="server" ID="LineOfBusCodeSelfInsured" CodeTable="LINE_OF_BUSINESS" ControlName="LineOfBusCodeSelfInsured"
                        RMXRef="/Instance/Document//ReturnXml/OutputResult/xml/LineOfBusCodeSelfInsured" RMXType="code" TabIndex="9" />
                </td>
            </tr>   
            -->            
            <tr>
                <td>
                    <asp:Label runat="server" class="required" ID="lblLOBNew" Text="Line Of Business:"></asp:Label>
                </td>
                <td>   
                  <asp:DropDownList runat="server" id="LobCode" tabindex="1" RMXRef="/Instance/Document//ReturnXml/OutputResult/xml/LobCode" RMXType="combobox" onchange="setDataChanged(true);">
                  <asp:ListItem Value="0" Text="" />
                  <asp:ListItem Value="241" Text="GC  General Claims" />
                  <asp:ListItem Value="242" Text="VA  Vehicle Accident Claims" />
                  <asp:ListItem Value="243" Text="WC  Workers' Compensation" />
                  <asp:ListItem Value="-3" Text="All" />
                </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" class="label" ID="lblIsCertDiff" Text="Is certificate name diff. from Org-Hierarchy Name?"></asp:Label>
                </td>
                <td>   
                  <asp:CheckBox runat="server" id="chkIsCertDiff" tabindex="1" RMXRef="/Instance/Document//ReturnXml/OutputResult/xml/IsCertDiffFlag" RMXType="CheckBox" onclick="setDataChanged(true);DisableCertName();">
                </asp:CheckBox>
                </td>
            </tr>
            <tr id = "trCertName">
                <td>
                    <asp:Label runat="server" class="label" ID="lbl_CertNameEid" Text="Certificate Name:" />&nbsp;&nbsp;
                </td>
                <td>
                    <asp:TextBox runat="server" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);"
                        ID="CertNameEid" RMXRef="/Instance/Document//ReturnXml/OutputResult/xml/CertNameEid" RMXType="eidlookup" cancelledvalue=""
                        TabIndex="2" />
                    <input type="button" class="button" value="..." name="CertNameEidbtn" tabindex="3"
                        onclick="return lookupData('CertNameEid','SI_CERTIFICATE',4,'CertNameEid',2)" />
                    <asp:TextBox Style="display: none" runat="server" ID="CertNameEid_cid" RMXref="/Instance/Document//ReturnXml/OutputResult/xml/CertNameEid/@codeid"
                        cancelledvalue="" />
                </td>
            </tr>        
        </tbody>
    </table>
    <asp:TextBox Style="display: none" runat="server" ID="txtData" />
    <asp:TextBox Style="display: none" runat="server" ID="txtPostBack" />
    <div id="div_buttons" class="formButtonGroup">
        <div class="formButton" id="div_btnOk">
            <asp:Button class="button" runat="server" ID="btnOk" Text="OK" Width="75px" OnClientClick="return EntityXSelfInsured_onOk();"
                OnClick="btnOk_Click" />
        </div>
        <div class="formButton" id="div_btnCancel">
            <asp:Button class="button" runat="server" ID="btnCancel" Text="Cancel" Width="75px"
                OnClientClick="return EntityXSelfInsured_onCancel();" OnClick="btnCancel_Click" />
        </div>
    </div>
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    </form>
</body>
</html>
