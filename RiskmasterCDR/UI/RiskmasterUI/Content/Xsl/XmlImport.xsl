<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >
<xsl:template match="Supplementals">
  <Supplementals>
    <xsl:for-each select="*">
      <xsl:copy>
        <xsl:choose>
          <xsl:when test="@codeid">
          <xsl:attribute name="codeid">
            <xsl:value-of select="@codeid"/>
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="value">
            <xsl:value-of select="text()"/>
          </xsl:attribute>
        </xsl:otherwise>
          </xsl:choose>
      </xsl:copy>
    </xsl:for-each>
  </Supplementals>
</xsl:template>
  <!--<xsl:template match="Supplementals">
    <Supplementals>
      <xsl:for-each select="*">
        <xsl:copy>
          <xsl:copy-of select="@*"/>
          <xsl:if test="not(@codeid)">
          <xsl:attribute name="value">
            <xsl:value-of select="text()"/>
          </xsl:attribute>
          </xsl:if>
        </xsl:copy>
        </xsl:for-each>
        </Supplementals>
  </xsl:template>-->
  
 </xsl:stylesheet>

