﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Riskmaster.UI.App_Master
{
    public partial class RMXMain : System.Web.UI.MasterPage
    {
        public HtmlGenericControl BodyControl
        {
            get
            {
                return this.cphHeaderBody;
            }
        } // property MasterBody

        public HtmlGenericControl HTMLControl
        {
            get
            {
                return this.cphHTML;
            }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            string someScript = "";
            someScript = "SetUserNameFocus();";//rsushilaggar MITS 24412 Date 03/2/2011
            Page.ClientScript.RegisterStartupScript(this.GetType(), "onload", someScript,true);

        }

        /// <summary>
        /// Handles the Logout event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Loginstatus1_LoggingOut(object sender, LoginCancelEventArgs e)
        {
            Loginstatus1.LogoutPageUrl = Riskmaster.AppHelpers.ConfigHelper.GetLogoutPageUrl();
            Loginstatus1.LogoutAction = LogoutAction.Redirect;

        }
    }
}
