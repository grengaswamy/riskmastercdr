﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.ServiceModel;
using Riskmaster.Models;


namespace Riskmaster.ServiceHelpers
{
    public class CodesServiceHelper
    {
         public CodeListType GetCodes(CodeTypeRequest type)
        {
            CodeListType returnVal = null;
            try
            {
                type.ClientId = AppHelper.ClientId;
                returnVal = AppHelper.GetResponse<CodeListType>("RMService/Codes/list", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, type);

            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
            }
            return returnVal;
        }
         public CodeListType GetQuickLookUp(QuickLookupRequest type)
         {
             //CodesServiceClient rmservice = null;
             CodeListType returnVal = null;
             try
             {
                 //rmservice = new CodesServiceClient();
                 type.ClientId = AppHelper.ClientId;
                 returnVal = AppHelper.GetResponse<CodeListType>("RMService/Codes/lookup", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, type);
                 //returnVal = rmservice.GetQuickLookUp(type);
             }
             catch (FaultException<RMException> ee)
             {
                 throw ee;
             }
             catch (Exception ee)
             {
                 throw ee;
             }
             finally
             {
                 //rmservice.Close();
             }
             return returnVal;
         }
    }
}
