﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
//using Riskmaster.UI.XMLImport;
//using Riskmaster.UI.DataStreamingService;
using System.ServiceModel;
using System.Collections.Generic;
using System.Xml;
using Riskmaster.Models;

namespace Riskmaster.ServiceHelpers
{
    public class XMLImportServiceHelper
    {
        public bool SaveXML(XMLInput oXMLInput, out XMLOutPut oXMLOutPut)//logFileContent Added for MITS 29711 by bsharma33
        {
            //XMLImportClient rmservice = null;
            bool returnVal = false;
            XMLOutPut objReturn = new XMLOutPut();
            try
            {

                oXMLInput.Token = AppHelper.GetSessionId();
                oXMLInput.ClientId = AppHelper.ClientId;
                oXMLInput.bVal = false;
                //objReturn = AppHelper.GetResponse<XMLOutPut>("RMService/XMLImports/list", AppHelper.HttpVerb.POST, "application/json", oXMLInput);

                oXMLOutPut = AppHelper.GetResponse<XMLOutPut>("RMService/XMLImports/list", AppHelper.HttpVerb.POST, "application/json", oXMLInput);
                //mbahl3 jira [RMACLOUD-158]              
                //rmservice = new XMLImportClient();
                //returnVal = rmservice.ProcessXML(out oXMLOutPut,request, false);//logFileContent Added for MITS 29711 by bsharma33
            }
            
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
               // rmservice.Close();
            }
            //oXMLOutPut = objReturn;
            //return  objReturn.bResult;
            return oXMLOutPut.bResult;
        }


        public XMLTemplate GetSuppTemplate(XMLTemplate oXMLTemplate)
        {
            //XMLImportClient rmservice = null;
            XMLTemplate returnDoc = null;
            try
            {
                XMLTemplate objReturn;
                oXMLTemplate.Token = AppHelper.GetSessionId();
                oXMLTemplate.ClientId = AppHelper.ClientId;
               
                //returnDoc = new XMLTemplate();
               // rmservice = new XMLImportClient();
             //   returnDoc = new XmlDocument();
              //returnDoc=  rmservice.GetSuppTemplate(request);
                returnDoc = AppHelper.GetResponse<XMLTemplate>("RMService/XMLImports/template", AppHelper.HttpVerb.POST, "application/json", oXMLTemplate);
            }
            //catch (FaultException<Riskmaster.UI.XMLImport.RMException> ee)
            //{
            //    throw ee;
            //}
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //rmservice.Close();
            }
            return returnDoc;
        }
    }
}