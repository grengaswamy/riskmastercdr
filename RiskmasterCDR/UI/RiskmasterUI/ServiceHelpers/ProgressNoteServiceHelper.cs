﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.ServiceModel;
//using Riskmaster.UI.ProgressNoteService;
using Riskmaster.Models;

namespace Riskmaster.ServiceHelpers
{
    public class ProgressNoteServiceHelper
    {
        //rsolanki2: Enhc Notes ajax updates
        public ProgressNotesType LoadPartial(ProgressNotesType request)
        {
            //ProgressNoteServiceClient rmservice = null;
            ProgressNotesType returnVal;
            try
            {
                //rmservice = new ProgressNoteServiceClient();

                //returnVal = rmservice.LoadProgressNotesPartial(request);
                returnVal = AppHelper.GetResponse<ProgressNotesType>("RMService/ProgressNotes/loadprogresspartial", AppHelper.HttpVerb.POST, "application/json", request);
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //rmservice.Close();
            }
            return returnVal;
        }
			//mbahl3 enhance notes enhancement mits 30513

        public ProgressNotesTypeChange GetNotesCaption(ProgressNotesTypeChange request)
        {
            //ProgressNoteServiceClient rmservice = null;
            ProgressNotesTypeChange returnVal;
            try
            {
                //rmservice = new ProgressNoteServiceClient();

                //returnVal = rmservice.GetNotesCaption(request);
                returnVal = AppHelper.GetResponse<ProgressNotesTypeChange>("RMService/ProgressNotes/getnotescaption", AppHelper.HttpVerb.POST, "application/json", request);
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //rmservice.Close();
            }
            return returnVal;
        }
	//mbahl3 enhance notes enhancement mits 30513
        public ProgressNotesType Load(ProgressNotesType request)
        {
            //ProgressNoteServiceClient rmservice = null;
            ProgressNotesType returnVal;
            try
            {
                //rmservice = new ProgressNoteServiceClient();
                
                //returnVal = rmservice.LoadProgressNotes(request);
                returnVal = AppHelper.GetResponse<ProgressNotesType>("RMService/ProgressNotes/loadprogress", AppHelper.HttpVerb.POST, "application/json", request);
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //rmservice.Close();
            }
            return returnVal;
        }
        public ProgressNotesType DeleteNote(ProgressNotesType request)
        {
            //ProgressNoteServiceClient rmservice = null;
            ProgressNotesType returnVal;
            try
            {
                //rmservice = new ProgressNoteServiceClient();

                //returnVal = rmservice.DeleteNote(request);
                returnVal = AppHelper.GetResponse<ProgressNotesType>("RMService/ProgressNotes/deletenote", AppHelper.HttpVerb.POST, "application/json", request);
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //rmservice.Close();
            }
            return returnVal;
        }
        public ProgressNotesType PrintProgressNotes(ProgressNotesType request)
        {
            //ProgressNoteServiceClient rmservice = null;
            ProgressNotesType returnVal;
            try
            {
                //rmservice = new ProgressNoteServiceClient();

                //returnVal = rmservice.PrintProgressNotes(request);
                returnVal = AppHelper.GetResponse<ProgressNotesType>("RMService/ProgressNotes/printnote", AppHelper.HttpVerb.POST, "application/json", request);
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //rmservice.Close();
            }
            return returnVal;
        }
        public GetNoteDetailsObject GetNoteDetails(GetNoteDetailsObject request)
        {
            //ProgressNoteServiceClient rmservice = null;
            GetNoteDetailsObject returnVal;
            try
            {
                //rmservice = new ProgressNoteServiceClient();

                //returnVal = rmservice.GetNoteDetails(request);
                returnVal = AppHelper.GetResponse<GetNoteDetailsObject>("RMService/ProgressNotes/getnotedetails", AppHelper.HttpVerb.POST, "application/json", request);
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //rmservice.Close();
            }
            return returnVal;
        }
        public SelectClaimObject SelectClaim(SelectClaimObject request)
        {
            //ProgressNoteServiceClient rmservice = null;
            SelectClaimObject returnVal;
            try
            {
                //rmservice = new ProgressNoteServiceClient();

                //returnVal = rmservice.SelectClaim(request);
                returnVal = AppHelper.GetResponse<SelectClaimObject>("RMService/ProgressNotes/selectclaim", AppHelper.HttpVerb.POST, "application/json", request);
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //rmservice.Close();
            }
            return returnVal;
        }
        //Added by GBINDRA MITS#34104 WWIG Gap15 02052014 START
        public SelectClaimantObject SelectClaimant(SelectClaimantObject request)
        {
            //ProgressNoteServiceClient rmservice = null;
            SelectClaimantObject returnVal;
            try
            {
                //rmservice = new ProgressNoteServiceClient();

                //returnVal = rmservice.SelectClaimant(request);
                returnVal = AppHelper.GetResponse<SelectClaimantObject>("RMService/ProgressNotes/selectclaimant", AppHelper.HttpVerb.POST, "application/json", request);
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //rmservice.Close();
            }
            return returnVal;
        }
        //Added by GBINDRA MITS#34104 WWIG Gap15 02052014 END
        public ProgressNote SaveNotes(ProgressNote request)
        {
            //ProgressNoteServiceClient rmservice = null;
            ProgressNote returnVal;
            try
            {
                //rmservice = new ProgressNoteServiceClient();

                //returnVal = rmservice.SaveNotes(request);
                returnVal = AppHelper.GetResponse<ProgressNote>("RMService/ProgressNotes/savenotes", AppHelper.HttpVerb.POST, "application/json", request);
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //rmservice.Close();
            }
            return returnVal;
        }
        public ProgressNoteTemplates LoadTemplates(ProgressNoteTemplates request)
        {
            //ProgressNoteServiceClient rmservice = null;
            ProgressNoteTemplates returnVal;
            try
            {
                //rmservice = new ProgressNoteServiceClient();

                //returnVal = rmservice.LoadTemplates(request);
                returnVal = AppHelper.GetResponse<ProgressNoteTemplates>("RMService/ProgressNotes/loadtemplates", AppHelper.HttpVerb.POST, "application/json", request);
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //rmservice.Close();
            }
            return returnVal;
        }
        public ProgressNoteTemplates DeleteTemplate(ProgressNoteTemplates request)
        {
            //ProgressNoteServiceClient rmservice = null;
            ProgressNoteTemplates returnVal;
            try
            {
                //rmservice = new ProgressNoteServiceClient();

                //returnVal = rmservice.DeleteTemplate(request);
                returnVal = AppHelper.GetResponse<ProgressNoteTemplates>("RMService/ProgressNotes/deletetemplate", AppHelper.HttpVerb.POST, "application/json", request);
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //rmservice.Close();
            }
            return returnVal;
        }
        public ProgressNoteTemplates SaveTemplates(ProgressNoteTemplates request)
        {
            //ProgressNoteServiceClient rmservice = null;
            ProgressNoteTemplates returnVal;
            try
            {
                //rmservice = new ProgressNoteServiceClient();

                //returnVal = rmservice.SaveTemplates(request);
                returnVal = AppHelper.GetResponse<ProgressNoteTemplates>("RMService/ProgressNotes/savetemplates", AppHelper.HttpVerb.POST, "application/json", request);
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //rmservice.Close();
            }
            return returnVal;
        }
        public ProgressNoteTemplates GetTemplateDetails(ProgressNoteTemplates request)
        {
            //ProgressNoteServiceClient rmservice = null;
            ProgressNoteTemplates returnVal;
            try
            {
                //rmservice = new ProgressNoteServiceClient();

                //returnVal = rmservice.GetTemplateDetails(request);
                returnVal = AppHelper.GetResponse<ProgressNoteTemplates>("RMService/ProgressNotes/gettemplatedetails", AppHelper.HttpVerb.POST, "application/json", request);
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //rmservice.Close();
            }
            return returnVal;
        }
        public ProgressNoteSettings GetNotesHeaderOrder(ProgressNoteSettings request)
        {
            //ProgressNoteServiceClient rmservice = null;
            ProgressNoteSettings returnVal;
            try
            {
                //rmservice = new ProgressNoteServiceClient();

                //returnVal = rmservice.GetNotesHeaderOrder(request);
                returnVal = AppHelper.GetResponse<ProgressNoteSettings>("RMService/ProgressNotes/getnotesheaderorder", AppHelper.HttpVerb.POST, "application/json", request);
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //rmservice.Close();
            }
            return returnVal;
        }
        public ProgressNoteSettings SaveNotesSettings(ProgressNoteSettings request)
        {
            //ProgressNoteServiceClient rmservice = null;
            ProgressNoteSettings returnVal;
            try
            {
                //rmservice = new ProgressNoteServiceClient();

                //returnVal = rmservice.SaveNotesSettings(request);
                returnVal = AppHelper.GetResponse<ProgressNoteSettings>("RMService/ProgressNotes/savenotessettings", AppHelper.HttpVerb.POST, "application/json", request);
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //rmservice.Close();
            }
            return returnVal;
        }
    }
}
