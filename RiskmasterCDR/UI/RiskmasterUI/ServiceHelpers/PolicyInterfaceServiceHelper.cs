﻿using System;
using Riskmaster.UI.PolicyInterfaceService;
using System.Xml.Linq;
using System.Xml;

namespace Riskmaster.ServiceHelpers
{
    public class PolicyInterfaceServiceHelper
    {
        public PolicySystemList GetPolicySystemList(PolicySystemList oRequest)
        {
            PolicyInterfaceServiceClient rmservice = null;
            PolicySystemList oResponse;
            try
            {
                rmservice = new PolicyInterfaceServiceClient();
                oResponse = rmservice.GetPolicySystemList(oRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                rmservice.Close();
            }
            return oResponse;
        }

        public DisplayFileData GetXml(RMServiceType objType)
        {
            PolicyInterfaceServiceClient rmservice = null;
            DisplayFileData returnVal;
            try
            {
                rmservice = new PolicyInterfaceServiceClient();
                returnVal = rmservice.GetXml(objType);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                rmservice.Close();
            }
            return returnVal;
        }
        public DisplayFileData GetUnitXml(RMServiceType objType)
        {
            PolicyInterfaceServiceClient rmservice = null;
            DisplayFileData returnVal;
            try
            {
                rmservice = new PolicyInterfaceServiceClient();
                returnVal = rmservice.GetUnitXml(objType);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                rmservice.Close();
            }
            return returnVal;
        }
        public DisplayFileData GetDuplicateOptions(SaveDownloadOptions oSaveDownloadOptions)
        {
            PolicyInterfaceServiceClient rmservice = null;
            DisplayFileData returnVal;
            try
            {
                rmservice = new PolicyInterfaceServiceClient();
                returnVal = rmservice.GetDuplicateOptions(oSaveDownloadOptions);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                rmservice.Close();
            }
            return returnVal;
        }

        public bool SaveOptions(ref SaveDownloadOptions oSaveDownloadOptions)
        {
            PolicyInterfaceServiceClient rmservice = null;
            bool returnVal= false;
            try
            {
                rmservice = new PolicyInterfaceServiceClient();
                returnVal = rmservice.SaveOptions(ref oSaveDownloadOptions);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                rmservice.Close();
            }
            return returnVal;
        }

       

        public void DeleteOldFiles(DisplayMode oMode)
        {
            PolicyInterfaceServiceClient rmservice = null;
            bool returnVal = false;
            try
            {
                rmservice = new PolicyInterfaceServiceClient();
                 rmservice.DeleteOldFiles(oMode);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                rmservice.Close();
            }
           
        }
        
      
		//abhal3 MITS 36046 start
        public LossCodeMappingOutput ImportMappingFromFile(LossCodeMappingFileContent oRequest)
        {
            PolicyInterfaceServiceClient rmservice = null;
            LossCodeMappingOutput oResponse;
            try
            {
                rmservice = new PolicyInterfaceServiceClient();
                oResponse = rmservice.ImportMappingFromFile(oRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                rmservice.Close();
            }
            return oResponse;
        }
        public LossCodeMappingOutput ReplicateMapping(ReplicateLossCodeMapping oRequest)
        {
            PolicyInterfaceServiceClient rmservice = null;
            LossCodeMappingOutput oResponse;
            try
            {
                rmservice = new PolicyInterfaceServiceClient();
                oResponse = rmservice.ReplicateLossCodeMapping(oRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                rmservice.Close();
            }
            return oResponse;
        }
        public string ExportMapping(ExportLossCodeMapping oRequest)
        {
            PolicyInterfaceServiceClient rmservice = null;
            string sFileContent = string.Empty;
            try
            {
                rmservice = new PolicyInterfaceServiceClient();
                sFileContent = rmservice.ExportMapping(oRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                rmservice.Close();
            }
            return sFileContent;
        }
		//abhal3 MITS 36046 end
        public PolicySearch GetPolicySearchResult(PolicySearch oSearchRequest)
        {
            PolicyInterfaceServiceClient rmservice = null;
            PolicySearch oResponse;
            try
            {
                rmservice = new PolicyInterfaceServiceClient();
                oResponse = rmservice.GetPolicySearchResult(oSearchRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                rmservice.Close();
            }
            return oResponse;
        }

        public PolicyEnquiry GetPolicyEnquiryResult(PolicyEnquiry oEnquiryRequest)
        {
            PolicyInterfaceServiceClient rmservice = null;
            PolicyEnquiry oResponse;
            try
            {
                rmservice = new PolicyInterfaceServiceClient();
                oResponse = rmservice.GetPolicyEnquiryResult(oEnquiryRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                rmservice.Close();
            }
            return oResponse;
        }

        public PolicyEnquiry GetEndorsementData(PolicyEnquiry oEnquiryRequest)
        {
            PolicyInterfaceServiceClient rmservice = null;
            PolicyEnquiry oResponse;
            try
            {
                rmservice = new PolicyInterfaceServiceClient();
                oResponse = rmservice.GetEndorsementData(oEnquiryRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                rmservice.Close();
            }
            return oResponse;
        }

        public PolicyEnquiry GetUnitDetailResult(PolicyEnquiry oEnquiryRequest)
        {
            PolicyInterfaceServiceClient rmservice = null;
            PolicyEnquiry oResponse;
            try
            {
                rmservice = new PolicyInterfaceServiceClient();
                oResponse = rmservice.GetUnitDetailResult(oEnquiryRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                rmservice.Close();
            }
            return oResponse;
        }
        
        //public PolicyEnquiry GetPropertyUnitDetailResult(PolicyEnquiry oEnquiryRequest)
        //{
        //    PolicyInterfaceServiceClient rmservice = null;
        //    PolicyEnquiry oResponse;
        //    try
        //    {
        //        rmservice = new PolicyInterfaceServiceClient();
        //        oResponse = rmservice.GetPropertyUnitDetailResult(oEnquiryRequest);
        //    }
        //    catch (Exception ee)
        //    {
        //        throw ee;
        //    }
        //    finally
        //    {
        //        rmservice.Close();
        //    }
        //    return oResponse;
        //}

        //public PolicyEnquiry GetAutoUnitDetailResult(PolicyEnquiry oEnquiryRequest)
        //{
        //    PolicyInterfaceServiceClient rmservice = null;
        //    PolicyEnquiry oResponse;
        //    try
        //    {
        //        rmservice = new PolicyInterfaceServiceClient();
        //        oResponse = rmservice.GetAutoUnitDetailResult(oEnquiryRequest);
        //    }
        //    catch (Exception ee)
        //    {
        //        throw ee;
        //    }
        //    finally
        //    {
        //        rmservice.Close();
        //    }
        //    return oResponse;
        //}

        //public PolicyEnquiry GetAutoCoverageDetailResult(PolicyEnquiry oEnquiryRequest)
        //{
        //    PolicyInterfaceServiceClient rmservice = null;
        //    PolicyEnquiry oResponse;
        //    try
        //    {
        //        rmservice = new PolicyInterfaceServiceClient();
        //       // oResponse = rmservice.GetAutoUnitCoverageListResult(oEnquiryRequest);
        //    }
        //    catch (Exception ee)
        //    {
        //        throw ee;
        //    }
        //    finally
        //    {
        //        rmservice.Close();
        //    }
        //    return oResponse;
        //}

        //public PolicyEnquiry GetPropertyCoverageDetailResult(PolicyEnquiry oEnquiryRequest)
        //{
        //    PolicyInterfaceServiceClient rmservice = null;
        //    PolicyEnquiry oResponse;
        //    try
        //    {
        //        rmservice = new PolicyInterfaceServiceClient();
        //        oResponse = rmservice.GetUnitCoverageListResult(oEnquiryRequest);
        //    }
        //    catch (Exception ee)
        //    {
        //        throw ee;
        //    }
        //    finally
        //    {
        //        rmservice.Close();
        //    }
        //    return oResponse;
        //}

        public PolicyEnquiry GetPolicyInterestDetailResult(PolicyEnquiry oEnquiryRequest)
        {
            PolicyInterfaceServiceClient rmservice = null;
            PolicyEnquiry oResponse;
            try
            {
                rmservice = new PolicyInterfaceServiceClient();
                oResponse = rmservice.GetPolicyInterestDetailResult(oEnquiryRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                rmservice.Close();
            }
            return oResponse;
        }

        public PolicySaveRequest SaveExternalPolicy(PolicySaveRequest oPolicySaveRequest)
        {
            PolicyInterfaceServiceClient rmservice = null;
            PolicySaveRequest oPolicySaveResponse;
            try
            {
                rmservice = new PolicyInterfaceServiceClient();
                oPolicySaveResponse = rmservice.SaveExternalPolicy(oPolicySaveRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                rmservice.Close();
            }
            return oPolicySaveResponse;
        }

      

        public PolicyValidateResponse PolicyValidation(PolicyValidateInput oPolicyValidateInput)
        {
            PolicyInterfaceServiceClient rmservice = null;
            PolicyValidateResponse oPolicyValidateResponse;
            try
            {
                rmservice = new PolicyInterfaceServiceClient();
                oPolicyValidateResponse = rmservice.PolicyValidation(oPolicyValidateInput);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                rmservice.Close();
            }
           return oPolicyValidateResponse;
        }

       

        public PolicyEnquiry GetPolicyDriverDetailResult(PolicyEnquiry oEnquire)
        {
            PolicyInterfaceServiceClient rmservice = null;
            PolicyEnquiry oResponse;
            try
            {
                rmservice = new PolicyInterfaceServiceClient();
                oResponse = rmservice.GetPolicyDriverDetailResult(oEnquire);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                rmservice.Close();
            }
            return oResponse;
        }

        public PolicyDownload GetDownLoadXMLData(PolicyDownload oRequest)
        {
            PolicyInterfaceServiceClient rmservice = null;
            PolicyDownload oResposne;

            string sResult = string.Empty;
            try
            {
                rmservice = new PolicyInterfaceServiceClient();
                oResposne = rmservice.GetDownLoadXMLData(oRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                rmservice.Close();
            }
            return oResposne;
        }

        public PolicyEnquiry GetUnitCoverageListResult(PolicyEnquiry oRequest)
        {
            PolicyInterfaceServiceClient rmservice = null;
            PolicyEnquiry oResponse;
            try
            {
                rmservice = new PolicyInterfaceServiceClient();
                oResponse = rmservice.GetUnitCoverageListResult(oRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                rmservice.Close();
            }
            return oResponse;
        }

        public PolicyEnquiry GetCoverageDetail(PolicyEnquiry oRequest)
        {
            PolicyInterfaceServiceClient rmservice = null;
            PolicyEnquiry oResponse = null;
            try
            {
                rmservice = new PolicyInterfaceServiceClient();
                oResponse= rmservice.GetCoverageDetailResult(oRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                rmservice.Close();
            }
            return oResponse;
        }

        public PolicyDownload GetEndorsementDataForTracking(PolicyDownload oRequest)
        {
            PolicyInterfaceServiceClient rmservice = null;
            PolicyDownload oResposne;

            string sResult = string.Empty;
            try
            {
                rmservice = new PolicyInterfaceServiceClient();
                oResposne = rmservice.GetEndorsementDataForTracking(oRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                rmservice.Close();
            }
            return oResposne;
        }

        public PolicyEnquiry GetUnitInterestListResult(PolicyEnquiry oEnquiryRequest)
        {
            PolicyInterfaceServiceClient rmservice = null;
            PolicyEnquiry oResponse;
            try
            {
                rmservice = new PolicyInterfaceServiceClient();
                oResponse = rmservice.GetUnitInterestListResult(oEnquiryRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                rmservice.Close();
            }
            return oResponse;
        }
        public PolicyEnquiry GetPolicyUnitInterestList(PolicyEnquiry oEnquiryRequest)
        {
            PolicyInterfaceServiceClient rmservice = null;
            PolicyEnquiry oResponse;
            try
            {
                rmservice = new PolicyInterfaceServiceClient();
                oResponse = rmservice.GetPolicyUnitInterestList(oEnquiryRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                rmservice.Close();
            }
            return oResponse;
        }

        public UnitListing SearchPolicyUnits(UnitSearch oRequest)
        {
            PolicyInterfaceServiceClient rmservice = null;
            UnitListing oResposne;
            try
            {
                rmservice = new PolicyInterfaceServiceClient();
                oResposne = rmservice.SearchPolicyUnits(oRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                rmservice.Close();
            }
            return oResposne;
        }
        /// <summary>
        /// tanwar2 - mits 30910
        /// </summary>
        /// <param name="xDoc"></param>
        /// <param name="oRequest"></param>
        public XElement GetDimEvalDate(XElement xDoc, PolicyEnquiry oRequest)
        {
            PolicyInterfaceServiceClient rmservice = null;
            UnitListing oResposne;
            try
            {
                rmservice = new PolicyInterfaceServiceClient();
                return rmservice.GetDiminishingEvaluationDate(xDoc, oRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                rmservice.Close();
            }
        }
        public FetchPolicyDetails GetPolicyDetails(FetchPolicyDetails oRequest)
        {
            PolicyInterfaceServiceClient rmservice = null;
            FetchPolicyDetails oResposne;

            try
            {

                rmservice = new PolicyInterfaceServiceClient();
                oRequest.Token = AppHelper.GetSessionId();
                oResposne = rmservice.GetPolicyDetails(oRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }

            return oResposne;
        }

        }
}
