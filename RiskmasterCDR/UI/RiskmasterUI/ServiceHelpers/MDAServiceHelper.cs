﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
//using Riskmaster.UI.MDAService; //mbahl3 jira [RMACLOUD-159]
using System.ServiceModel;
using System.Collections.Generic;
using Riskmaster.Models;
namespace Riskmaster.ServiceHelpers
{
    public class MDAServiceHelper
    {
        public string GetMDATopics(string medicalcode, string jobclass)
        {
            //MDAServiceClient rmservice = null; //mbahl3 jira [RMACLOUD-159]
          //mbahl3 jira [RMACLOUD-159]
		   MDATopic  request = null;
           MDATopic objReturn;
		   //mbahl3 jira [RMACLOUD-159]
            string returnVal = "";
            try
            {
			//mbahl3 jira [RMACLOUD-159]
                //rmservice = new MDAServiceClient();
                //returnVal = rmservice.GetMDATopics(medicalcode,jobclass);

                request = new MDATopic();
                request.Token = AppHelper.GetSessionId();
                request.Medicalcode = medicalcode;
                request.Jobclass = jobclass;
                //request.ReturnValue = returntype;
                request.ClientId = AppHelper.ClientId;
                objReturn = AppHelper.GetResponse<MDATopic>("RMService/MDA/Topic", AppHelper.HttpVerb.POST, "application/json", request);
               //mbahl3 jira [RMACLOUD-159]
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
               // rmservice.Close();
            }
            return objReturn.ReturnValue;  //mbahl3 jira [RMACLOUD-159]
        }
    }
}
