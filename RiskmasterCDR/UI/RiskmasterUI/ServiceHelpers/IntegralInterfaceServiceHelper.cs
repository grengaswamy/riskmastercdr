﻿using System;
using Riskmaster.UI.IntegralInterfaceService;


namespace Riskmaster.ServiceHelpers
{
    public class IntegralInterfaceServiceHelper
    {        
        public PolicySearch GetPolicySearchResult(PolicySearch oSearchRequest)
        {
            IntegralInterfaceServiceClient rmservice = null;
            PolicySearch oResponse;
            try
            {
                oSearchRequest.ClientId = AppHelper.ClientId;
                rmservice = new IntegralInterfaceServiceClient();
                oResponse = rmservice.GetPolicySearchResult(oSearchRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                rmservice.Close();
            }
            return oResponse;
        }
        
        public ExternalPolicyData GetPolicyEnquiryResult(PolicyEnquiry oEnquiryRequest)
        {
            IntegralInterfaceServiceClient rmservice = null;
            ExternalPolicyData oResponse;
            try
            {
                oEnquiryRequest.ClientId = AppHelper.ClientId;
                rmservice = new IntegralInterfaceServiceClient();
                oResponse = rmservice.GetPolicyEnquiryResult(oEnquiryRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                rmservice.Close();
            }
            return oResponse;
        }
        public PolicyValidateResponse PolicyValidation(PolicyValidateInput oPolicyValidateInput)
        {
            IntegralInterfaceServiceClient rmservice = null;
            PolicyValidateResponse oPolicyValidateResponse;
            try
            {
				oPolicyValidateInput.ClientId = AppHelper.ClientId;
                rmservice = new IntegralInterfaceServiceClient();
                oPolicyValidateResponse = rmservice.PolicyValidation(oPolicyValidateInput);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                rmservice.Close();
            }
            return oPolicyValidateResponse;
        }
        public PolicySaveRequest SavePolicyAllData(ExternalPolicyData oPolicySaveRequest)
        {
            IntegralInterfaceServiceClient rmservice = null;
            PolicySaveRequest oPolicySaveResponse;
            try
            {
				oPolicySaveRequest.ClientId = AppHelper.ClientId;
                rmservice = new IntegralInterfaceServiceClient();
                oPolicySaveResponse = rmservice.SavePolicyAllData(oPolicySaveRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                rmservice.Close();
            }
            return oPolicySaveResponse;
        }
    }
}
