﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Riskmaster.UI.RMSSO {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="CustomMembershipProvider", Namespace="http://schemas.datacontract.org/2004/07/Riskmaster.Security.Authentication")]
    [System.SerializableAttribute()]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Riskmaster.UI.RMSSO.RiskmasterLDAPMembershipProvider))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Riskmaster.UI.RMSSO.RiskmasterADMembershipProvider))]
    public partial class CustomMembershipProvider : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string AdminPwdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string AdminUserNameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ConnectionStringField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ConnectionTypeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string HostNameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool IsEnabledField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int ProviderIDField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string AdminPwd {
            get {
                return this.AdminPwdField;
            }
            set {
                if ((object.ReferenceEquals(this.AdminPwdField, value) != true)) {
                    this.AdminPwdField = value;
                    this.RaisePropertyChanged("AdminPwd");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string AdminUserName {
            get {
                return this.AdminUserNameField;
            }
            set {
                if ((object.ReferenceEquals(this.AdminUserNameField, value) != true)) {
                    this.AdminUserNameField = value;
                    this.RaisePropertyChanged("AdminUserName");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ConnectionString {
            get {
                return this.ConnectionStringField;
            }
            set {
                if ((object.ReferenceEquals(this.ConnectionStringField, value) != true)) {
                    this.ConnectionStringField = value;
                    this.RaisePropertyChanged("ConnectionString");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ConnectionType {
            get {
                return this.ConnectionTypeField;
            }
            set {
                if ((object.ReferenceEquals(this.ConnectionTypeField, value) != true)) {
                    this.ConnectionTypeField = value;
                    this.RaisePropertyChanged("ConnectionType");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string HostName {
            get {
                return this.HostNameField;
            }
            set {
                if ((object.ReferenceEquals(this.HostNameField, value) != true)) {
                    this.HostNameField = value;
                    this.RaisePropertyChanged("HostName");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool IsEnabled {
            get {
                return this.IsEnabledField;
            }
            set {
                if ((this.IsEnabledField.Equals(value) != true)) {
                    this.IsEnabledField = value;
                    this.RaisePropertyChanged("IsEnabled");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ProviderID {
            get {
                return this.ProviderIDField;
            }
            set {
                if ((this.ProviderIDField.Equals(value) != true)) {
                    this.ProviderIDField = value;
                    this.RaisePropertyChanged("ProviderID");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="RiskmasterLDAPMembershipProvider", Namespace="http://schemas.datacontract.org/2004/07/Riskmaster.Security.Authentication")]
    [System.SerializableAttribute()]
    public partial class RiskmasterLDAPMembershipProvider : Riskmaster.UI.RMSSO.CustomMembershipProvider {
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string BaseDNField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Collections.Generic.Dictionary<string, string> LDAPServerTypeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string UserAttributeField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string BaseDN {
            get {
                return this.BaseDNField;
            }
            set {
                if ((object.ReferenceEquals(this.BaseDNField, value) != true)) {
                    this.BaseDNField = value;
                    this.RaisePropertyChanged("BaseDN");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Collections.Generic.Dictionary<string, string> LDAPServerType {
            get {
                return this.LDAPServerTypeField;
            }
            set {
                if ((object.ReferenceEquals(this.LDAPServerTypeField, value) != true)) {
                    this.LDAPServerTypeField = value;
                    this.RaisePropertyChanged("LDAPServerType");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string UserAttribute {
            get {
                return this.UserAttributeField;
            }
            set {
                if ((object.ReferenceEquals(this.UserAttributeField, value) != true)) {
                    this.UserAttributeField = value;
                    this.RaisePropertyChanged("UserAttribute");
                }
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="RiskmasterADMembershipProvider", Namespace="http://schemas.datacontract.org/2004/07/Riskmaster.Security.Authentication")]
    [System.SerializableAttribute()]
    public partial class RiskmasterADMembershipProvider : Riskmaster.UI.RMSSO.CustomMembershipProvider {
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="RMSSO.ISSOService")]
    public interface ISSOService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISSOService/GetSSLAuthTypes", ReplyAction="http://tempuri.org/ISSOService/GetSSLAuthTypesResponse")]
        System.Collections.Generic.Dictionary<string, string> GetSSLAuthTypes();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISSOService/GetADProvider", ReplyAction="http://tempuri.org/ISSOService/GetADProviderResponse")]
        System.Data.DataSet GetADProvider();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISSOService/GetLDAPProvider", ReplyAction="http://tempuri.org/ISSOService/GetLDAPProviderResponse")]
        System.Data.DataSet GetLDAPProvider();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISSOService/InsertADProvider", ReplyAction="http://tempuri.org/ISSOService/InsertADProviderResponse")]
        void InsertADProvider(Riskmaster.UI.RMSSO.RiskmasterADMembershipProvider objADProvider);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISSOService/InsertLDAPProvider", ReplyAction="http://tempuri.org/ISSOService/InsertLDAPProviderResponse")]
        void InsertLDAPProvider(Riskmaster.UI.RMSSO.RiskmasterLDAPMembershipProvider objLDAPProvider);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISSOService/UpdateADProvider", ReplyAction="http://tempuri.org/ISSOService/UpdateADProviderResponse")]
        void UpdateADProvider(Riskmaster.UI.RMSSO.RiskmasterADMembershipProvider objADProvider);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISSOService/UpdateLDAPProvider", ReplyAction="http://tempuri.org/ISSOService/UpdateLDAPProviderResponse")]
        void UpdateLDAPProvider(Riskmaster.UI.RMSSO.RiskmasterLDAPMembershipProvider objLDAPProvider);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface ISSOServiceChannel : Riskmaster.UI.RMSSO.ISSOService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class SSOServiceClient : System.ServiceModel.ClientBase<Riskmaster.UI.RMSSO.ISSOService>, Riskmaster.UI.RMSSO.ISSOService {
        
        public SSOServiceClient() {
        }
        
        public SSOServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public SSOServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public SSOServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public SSOServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public System.Collections.Generic.Dictionary<string, string> GetSSLAuthTypes() {
            return base.Channel.GetSSLAuthTypes();
        }
        
        public System.Data.DataSet GetADProvider() {
            return base.Channel.GetADProvider();
        }
        
        public System.Data.DataSet GetLDAPProvider() {
            return base.Channel.GetLDAPProvider();
        }
        
        public void InsertADProvider(Riskmaster.UI.RMSSO.RiskmasterADMembershipProvider objADProvider) {
            base.Channel.InsertADProvider(objADProvider);
        }
        
        public void InsertLDAPProvider(Riskmaster.UI.RMSSO.RiskmasterLDAPMembershipProvider objLDAPProvider) {
            base.Channel.InsertLDAPProvider(objLDAPProvider);
        }
        
        public void UpdateADProvider(Riskmaster.UI.RMSSO.RiskmasterADMembershipProvider objADProvider) {
            base.Channel.UpdateADProvider(objADProvider);
        }
        
        public void UpdateLDAPProvider(Riskmaster.UI.RMSSO.RiskmasterLDAPMembershipProvider objLDAPProvider) {
            base.Channel.UpdateLDAPProvider(objLDAPProvider);
        }
    }
}
