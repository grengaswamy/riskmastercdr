﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Riskmaster.BusinessHelpers
{
    public class ServiceResponseHelper
    {
        #region Property Declaration

            public bool Success { get; set; }
            public string Description { get; set; }
            public object ResponseData { get; set; }
            #endregion
      
        #region Construcor Declaration

        public ServiceResponseHelper(bool Success, string Description = null, object ResponseData = null)
        {
            this.Success = Success;
            this.Description = Description;
            this.ResponseData = ResponseData;
        }
        #endregion
    }
}