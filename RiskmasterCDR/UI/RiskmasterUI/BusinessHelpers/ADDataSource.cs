﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.ComponentModel;
using System.Xml;
//using Riskmaster.UI.RMSSO;//Change by kuladeep for Jira-156
using Riskmaster.Models;


[DataObject(true)]
//public class ADDataSource: RiskmasterADMembershipProvider //Change by kuladeep for Jira-156
public class ADDataSource: RiskmasterMembershipProvider
{

    #region Select
    /// <summary>
    /// Gets the Active Directory Membership Provider record
    /// </summary>
    /// <returns>DataSet containing the Active Directory Membership Provider record</returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public DataSet SelectADRecord()
    {

        //Code change by kuladeep for Jira-156 Start---Make Rest service.

        //SSOServiceClient objSvcClient = new SSOServiceClient();
        //objSvcClient.Open();
        ////DataSet dstADRecord = LoadDataSet(objSvcClient.GetADProvider());
        //DataSet dstADRecord = objSvcClient.GetADProvider();
        //objSvcClient.Close();


        RMServiceType oRMServiceType = null;
        oRMServiceType = new RMServiceType();
        oRMServiceType.Token = AppHelper.GetSessionId();
        oRMServiceType.ClientId = AppHelper.ClientId;
        DataSet dstADRecord = AppHelper.GetResponse<DataSet>("RMService/SSO/provider", AppHelper.HttpVerb.POST, "application/json", oRMServiceType);
        //Code change by kuladeep for Jira-156 End

        DataTable dtADRecord = new DataTable();

        dtADRecord = dstADRecord.Tables[0];

        foreach (DataColumn dtColumn in dtADRecord.Columns)
        {
            string strColumnName = dtColumn.ColumnName;
            string strDataType = dtColumn.DataType.ToString();
        }

        return dstADRecord;

    } // method: SelectADRecord 
    #endregion

    #region Insert
    /// <summary>
    /// Inserts the specified Active Directory membership record
    /// </summary>
    /// <param name="objADDataSource"></param>
    [DataObjectMethod(DataObjectMethodType.Insert)]
    public void InsertADRecord(ADDataSource objADDataSource)
    {

        //Code change by kuladeep for Jira-156 Start---Make Rest service.

        //SSOServiceClient objSvcClient = new SSOServiceClient();
        //objSvcClient.Open();
        //RiskmasterADMembershipProvider objADProvider = new RiskmasterADMembershipProvider();

        //objADProvider.ProviderID = objADDataSource.ProviderID;
        //objADProvider.HostName = objADDataSource.HostName;
        //objADProvider.AdminUserName = objADDataSource.AdminUserName;
        //objADProvider.AdminPwd = objADDataSource.AdminPwd;
        //objADProvider.IsEnabled = objADDataSource.IsEnabled;
        //objADProvider.ConnectionType = objADDataSource.ConnectionType;

        ////objSvcClient.UpdateADProvider(objADProvider);
        //objSvcClient.InsertADProvider(objADProvider);       
        //objSvcClient.Close();


        RiskmasterMembershipProvider oRiskmasterMembershipProvider = null;
        oRiskmasterMembershipProvider = new RiskmasterMembershipProvider();
        oRiskmasterMembershipProvider.Token = AppHelper.GetSessionId();
        oRiskmasterMembershipProvider.ClientId = AppHelper.ClientId;
        oRiskmasterMembershipProvider.ProviderID = objADDataSource.ProviderID;
        oRiskmasterMembershipProvider.HostName = objADDataSource.HostName;
        oRiskmasterMembershipProvider.AdminUserName = objADDataSource.AdminUserName;
        oRiskmasterMembershipProvider.AdminPwd = objADDataSource.AdminPwd;
        oRiskmasterMembershipProvider.IsEnabled = objADDataSource.IsEnabled;
        oRiskmasterMembershipProvider.ConnectionType = objADDataSource.ConnectionType;

        AppHelper.GetResponse("RMService/SSO/add", AppHelper.HttpVerb.POST, "application/json", oRiskmasterMembershipProvider);

        //Code change by kuladeep for Jira-156 End---Make Rest service.


    } // method: InsertADRecord 
    #endregion

    #region Update
    /// <summary>
    /// Updates the specified Active Directory Membership record
    /// </summary>
    /// <param name="objADDataSource"></param>
    [DataObjectMethod(DataObjectMethodType.Update)]
    public void UpdateADRecord(ADDataSource objADDataSource)
    {

        //Code change by kuladeep for Jira-156 Start---Make Rest service.

        //SSOServiceClient objSvcClient = new SSOServiceClient();
        //objSvcClient.Open();
        //RiskmasterADMembershipProvider objADProvider = new RiskmasterADMembershipProvider();

        //objADProvider.ProviderID = objADDataSource.ProviderID;
        //objADProvider.HostName = objADDataSource.HostName;
        //objADProvider.AdminUserName = objADDataSource.AdminUserName;
        //objADProvider.AdminPwd = objADDataSource.AdminPwd;
        //objADProvider.IsEnabled = objADDataSource.IsEnabled;
        //objADProvider.ConnectionType = objADDataSource.ConnectionType;

        //objSvcClient.UpdateADProvider(objADProvider);
        //objSvcClient.Close();


        RiskmasterMembershipProvider oRiskmasterMembershipProvider = null;
        oRiskmasterMembershipProvider = new RiskmasterMembershipProvider();
        oRiskmasterMembershipProvider.Token = AppHelper.GetSessionId();
        oRiskmasterMembershipProvider.ClientId = AppHelper.ClientId;
        oRiskmasterMembershipProvider.ProviderID = objADDataSource.ProviderID;
        oRiskmasterMembershipProvider.HostName = objADDataSource.HostName;
        oRiskmasterMembershipProvider.AdminUserName = objADDataSource.AdminUserName;
        oRiskmasterMembershipProvider.AdminPwd = objADDataSource.AdminPwd;
        oRiskmasterMembershipProvider.IsEnabled = objADDataSource.IsEnabled;
        oRiskmasterMembershipProvider.ConnectionType = objADDataSource.ConnectionType;

        AppHelper.GetResponse("RMService/SSO/updateprovider", AppHelper.HttpVerb.POST, "application/json", oRiskmasterMembershipProvider);

        //Code change by kuladeep for Jira-156 End---Make Rest service.

    } // method: UpdateADRecord 
    #endregion

    #region Delete
    /// <summary>
    /// Deletes the specified Active Directory Membership record
    /// </summary>
    /// <param name="objADDataSource"></param>
    [DataObjectMethod(DataObjectMethodType.Delete)]
    public void DeleteADRecord(ADDataSource objADDataSource)
    {
    } // method: DeleteADRecord 
    #endregion

}
