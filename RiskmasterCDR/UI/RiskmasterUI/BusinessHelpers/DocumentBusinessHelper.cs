﻿using System;
using System.Collections.Generic;
using System.Web;
using Riskmaster.ServiceHelpers;
//using Riskmaster.UI.DocumentService;
//using Riskmaster.UI.DataStreamingService; 
using System.IO;
using System.Collections;
using Riskmaster.Common;
using CuteWebUI;
using Riskmaster.Models;
namespace Riskmaster.BusinessHelpers
{
    public class DocumentBusinessHelper
    {
        public void CreateFolder(string Foldername, string ParentId)
        {
            DocumentServiceHelper objModel = null;
            FolderTypeRequest folder = null;
            bool ret = false;
            try
            {
                objModel = new DocumentServiceHelper();
                folder = new FolderTypeRequest();
                folder.FolderName = Foldername;
                folder.ParentId = ParentId;
                folder.Token = AppHelper.GetSessionId();
                //Call the Model Helper
                ret = objModel.CreateFolder(folder);
            }
            finally
            {
                objModel = null;

            }
        }

        //rbhatia4:R8: Use Media View Setting : July 08 2011
        //Restructuring Document Management Code for choosing the selected vendor
        /*
		//Mona:PaperVision Merge: Checking whether PaperVision is ON or OFF
        public string GetPaperVisionSetting()
        {
            string sIsPaperVisionOn = "0";
            DocumentServiceHelper objServiceHelper = null;
            DocumentType objDocReq = null;
            try
            {                
                objDocReq = new DocumentType();
                objDocReq.Token = AppHelper.GetSessionId();

                objServiceHelper = new DocumentServiceHelper();
                sIsPaperVisionOn = objServiceHelper.GetPaperVisionSetting(objDocReq);
            }
            finally
            {
                objServiceHelper=null;
                objDocReq = null;
            }
            return sIsPaperVisionOn;
        }
         */ 

        //rbhatia4:R8: Use Media View Setting : July 05 2011
        //Making  a general helper method to find out which document management  system has been used

        public DocumentVendorDetails GetDocumentManagementVendorDetails(bool bUseMediaView)
        {
            DocumentServiceHelper objServiceHelper = null;
            DocumentVendorDetails objDocReq = null;
            DocumentVendorDetails objResponse = null;
            string sDocumentVendorName = String.Empty;
            try
            {
                objDocReq = new DocumentVendorDetails();
                objDocReq.AssociatedRecordInfo = new AssociatedRecordInfo();
                objDocReq.Token = AppHelper.GetSessionId();

                objServiceHelper = new DocumentServiceHelper();
                objDocReq.OpenMediaView = bUseMediaView;
                objDocReq.AssociatedRecordInfo.TableName = AppHelper.GetValue("AttachTableName").ToString();
                objDocReq.AssociatedRecordInfo.FormName = AppHelper.GetValue("FormName").ToString();
                objDocReq.AssociatedRecordInfo.RecordId = AppHelper.GetValue("AttachRecordId").ToString();

                if (!String.IsNullOrEmpty(AppHelper.GetValue("flag")))
                {
                    objDocReq.AssociatedRecordInfo.TableName = "User";
                    objDocReq.AssociatedRecordInfo.FormName =  "User";
                }

                objResponse = objServiceHelper.GetDocumentManagementVendorDetails(objDocReq);
            }
            finally
            {
                objServiceHelper = null;
                objDocReq = null;
            }
            return objResponse;
        }

        //Document/Download/Id
        public DocumentType UpdateDocumentFile(DocumentType dtype)
        {
            DocumentServiceHelper objModel = null;

            DocumentType documentTemp = null;

            try
            {
                objModel = new DocumentServiceHelper();

                dtype.Token = AppHelper.GetSessionId();
                documentTemp = objModel.UpdateDocumentFile(dtype);

            }

            finally
            {
                objModel = null;
            }
            return documentTemp;
        }

        public void Delete(string folderids, string docids, string psid, string flag, string TableName)
        {
            DocumentServiceHelper objModel = null;
            DocumentsDeleteRequest request = null;
            bool ret = false;
            try
            {
                objModel = new DocumentServiceHelper();
                request = new DocumentsDeleteRequest();
                request.FolderIds = folderids;
                if (request.FolderIds.Length > 0)
                {
                    if (request.FolderIds.IndexOf(",") != -1)
                    {
                        request.FolderIds = request.FolderIds.Substring(0, request.FolderIds.Length - 1);
                    }
                        //skhare7 MITS 23094 start
                    else if (request.FolderIds.IndexOf("|") != -1)
                    { 
                     request.FolderIds= request.FolderIds.Substring(1, request.FolderIds.Length - 2);
                     request.FolderIds = request.FolderIds.Replace("||", ",");
                    }
                    //skhare7 MITS 23094 end
                }
                request.DocumentIds = docids;
                if (request.DocumentIds.Length > 0)
                {
                    if ((request.DocumentIds.IndexOf(",") != -1))
                    {
                        request.DocumentIds = request.DocumentIds.Substring(0, request.DocumentIds.Length - 1);
                    }
                      //  skhare7 MITS 23094 start
                    else if (request.DocumentIds.IndexOf("|") != -1)
                    {
                        request.DocumentIds = request.DocumentIds.Substring(1, request.DocumentIds.Length - 2);
                        request.DocumentIds=request.DocumentIds.Replace("||", ",");
                    
                    }
                    //skhare7 MITS 23094 end
                }
                request.Token = AppHelper.GetSessionId();
                request.Psid = psid;
                request.ScreenFlag = flag;
                if (TableName != "")
                {
                    request.TableName = TableName;
                }
                ret = objModel.Delete(request);
            }
            finally
            {
                objModel = null;
            }
        }
        public void Add()
        {
            DocumentServiceHelper objModel = null;
            DocumentType document = null;
            RMServiceType objResponse = null;
            byte[] file = null;
           
            try
            {
                objModel = new DocumentServiceHelper();
                document = new DocumentType();
                document.Class = new CodeType();
                document.Category = new CodeType();
                document.Type = new CodeType();
                document.Title = AppHelper.GetFormValue("Title");
                document.FolderId = AppHelper.GetFormValue("FolderId").ToString();
                document.PsId = Conversion.ConvertStrToInteger(AppHelper.GetFormValue("Psid").ToString());
                document.FilePath = "";
                document.Subject = AppHelper.GetFormValue("Subject");
                //Start HardCoded need to change values
                document.Token = AppHelper.GetSessionId();
                //End HardCoded need to change values
                document.Type.Id = Conversion.ConvertStrToInteger(AppHelper.GetFormValue("documenttypecode_cid"));
                document.Type.Desc = AppHelper.GetFormValue("documenttypecode");
                document.Class.Id = Conversion.ConvertStrToInteger(AppHelper.GetFormValue("documentclasscode_cid"));
                document.Class.Desc = AppHelper.GetFormValue("documentclasscode");
                document.Category.Id = Conversion.ConvertStrToInteger(AppHelper.GetFormValue("documentcategorycode_cid"));
                document.Category.Desc = AppHelper.GetFormValue("documentcategorycode");
                //HardCoded need to change values
                document.ScreenFlag = AppHelper.GetFormValue("flag").ToString();
                document.Keywords = AppHelper.GetFormValue("Keywords");
                document.Notes = AppHelper.GetFormValue("Notes");
                document.FileName = HttpContext.Current.Request.Files[0].FileName;
                document.AttachTable = AppHelper.GetFormValue("AttachTableName").ToString();
                document.FormName = AppHelper.GetFormValue("FormName").ToString();
                document.AttachRecordId = Conversion.ConvertStrToInteger(AppHelper.GetFormValue("AttachRecordId").ToString());

                HttpFileCollection files = HttpContext.Current.Request.Files;
                if (files.Count > 0)
                {
                    HttpPostedFile uploadFile = files[0];
                    file = new byte[uploadFile.InputStream.Length];
                    int len = Convert.ToInt32(uploadFile.InputStream.Length);
                    uploadFile.InputStream.Read(file, 0, len);
                    document.FileContents = file;
                }
                objResponse = objModel.AddDocument(document);
            }
            finally
            {
                objModel = null;

            }
        }

        public delegate void AsyncMethodCaller(AttachmentItemCollection attachments, DocumentType document, ArrayList p_ArrListFileName, ArrayList p_ArrListDocumentId, string p_sErrorFolderName);

        private void UsingEndInvoke(AttachmentItemCollection attachments, DocumentType document, ArrayList p_ArrListFileName, ArrayList p_ArrListDocumentId,string p_sErrorFolderName)
        {
            // create a delegate of MethodInvoker poiting to our Foo function.
            AsyncMethodCaller simpleDelegate = new AsyncMethodCaller(BeginUploadLargeDocument);
            simpleDelegate.BeginInvoke(attachments,document,p_ArrListFileName,p_ArrListDocumentId,p_sErrorFolderName,new AsyncCallback(EndUploadLargeDocument), null); // callback delegate!                    
        }
        
        /// <summary>
        /// Added by Nitin for Implementing Large document uploads
        /// </summary>
        /// <param name="attachments"></param>
        public void AddMultipleLargeFiles(AttachmentItemCollection attachments,string p_sErrorFolderName)
        {
            DocumentServiceHelper objModel = null;
            DocumentType document = null;
            DocumentType objResponse = null;
            byte[] file = null;
            
            //changed by Nitin for Mits 2116
            ArrayList arrListFileName = null;
            ArrayList arrListDocId = null;

            try
            {
                objModel = new DocumentServiceHelper();
                document = new DocumentType();
                document.Class = new CodeType();
                document.Category = new CodeType();
                document.Type = new CodeType();
                document.Title = AppHelper.GetFormValue("Title");
                document.FolderId = AppHelper.GetFormValue("FolderId").ToString();
                document.PsId = Conversion.ConvertStrToInteger(AppHelper.GetFormValue("Psid").ToString());
                document.FilePath = "";
                document.Subject = AppHelper.GetFormValue("Subject");
                //Start HardCoded need to change values
                document.Token = AppHelper.GetSessionId();
                //End HardCoded need to change values
                document.Type.Id = Conversion.ConvertStrToInteger(AppHelper.GetFormValue("documenttypecode_cid"));
                document.Type.Desc = AppHelper.GetFormValue("documenttypecode");
                document.Class.Id = Conversion.ConvertStrToInteger(AppHelper.GetFormValue("documentclasscode_cid"));
                document.Class.Desc = AppHelper.GetFormValue("documentclasscode");
                document.Category.Id = Conversion.ConvertStrToInteger(AppHelper.GetFormValue("documentcategorycode_cid"));
                document.Category.Desc = AppHelper.GetFormValue("documentcategorycode");
                //HardCoded need to change values
                document.ScreenFlag = AppHelper.GetFormValue("flag").ToString();
                document.Keywords = AppHelper.GetFormValue("Keywords");
                document.Notes = AppHelper.GetFormValue("Notes");

                document.AttachTable = AppHelper.GetFormValue("AttachTableName").ToString();
                document.FormName = AppHelper.GetFormValue("FormName").ToString();
                document.AttachRecordId = Conversion.ConvertStrToInteger(AppHelper.GetFormValue("AttachRecordId").ToString());

                //changed by Nitin for Mits 2116
                arrListDocId = new ArrayList();
                arrListFileName = new ArrayList();

                foreach (AttachmentItem item in attachments)
                {
                    if (item.Checked)
                    {
                        document.FileName = item.FileName;
                        objResponse = objModel.CreateDocumentDetail(document);

                        //changed by Nitin for Mits 2116
                        //changing from stringbuilder to ArrayList
                        arrListFileName.Add(objResponse.FileName);
                        arrListDocId.Add(objResponse.DocumentId.ToString()); 
                    }
                }
                document.ClientId = AppHelper.ClientId;
                //document.BaseUrl = Common.CommonFunctions.RetrieveBaseURL();
                UsingEndInvoke(attachments,document, arrListFileName,arrListDocId, p_sErrorFolderName);
            }
            finally
            {
                objModel = null;
            }
        }
        
        //changed by Nitin for Mits 2116
        private void BeginUploadLargeDocument(AttachmentItemCollection attachments, DocumentType document, ArrayList p_ArrListFileName, ArrayList p_ArrListDocumentId, string p_sErrorFolderName)
        {
            DocumentServiceHelper objModel = null;
            Riskmaster.UI.DataStreamingService.StreamedDocumentType sDocument = null;
            RMServiceType objResponse = null;
            System.IO.Stream data = null;
            string[] arrNewFileName = null;
            string[] arrNewDocumentId = null;
            int count = 0;

            try
            {
                objModel = new DocumentServiceHelper();
                sDocument = new Riskmaster.UI.DataStreamingService.StreamedDocumentType();
                sDocument.Class = new Riskmaster.UI.DataStreamingService.StreamedCodeType();
                sDocument.Category = new Riskmaster.UI.DataStreamingService.StreamedCodeType();
                sDocument.Type = new Riskmaster.UI.DataStreamingService.StreamedCodeType();
                sDocument.Title = document.Title;
                sDocument.FolderId = document.FolderId;
                sDocument.PsId = document.PsId;
                sDocument.FilePath = document.FilePath;
                sDocument.Subject = document.Subject;
                //Start HardCoded need to change values
                sDocument.Token = document.Token;
                //End HardCoded need to change values
                sDocument.Type.Id = document.Type.Id;
                sDocument.Type.Desc = document.Type.Desc;
                sDocument.Class.Id = document.Class.Id;
                sDocument.Class.Desc = document.Class.Desc;
                sDocument.Category.Id = document.Category.Id;
                sDocument.Category.Desc = document.Category.Desc;
                //HardCoded need to change values
                sDocument.ScreenFlag = document.ScreenFlag;
                sDocument.Keywords = document.Keywords;
                sDocument.Notes = document.Notes;

                sDocument.AttachTable = document.AttachTable;
                sDocument.FormName = document.FormName;
                sDocument.AttachRecordId = document.AttachRecordId;
                sDocument.ClientId = document.ClientId;
                //sDocument.BaseUrl = document.BaseUrl;
                foreach (AttachmentItem item in attachments)
                {
                    if (item.Checked)
                    {
                        //changed by Nitin for Mits 2116
                        sDocument.FileSize = item.FileSize.ToString();
                        sDocument.FileName = p_ArrListFileName[count].ToString(); 
                        sDocument.DocumentId = Convert.ToInt32(p_ArrListDocumentId[count]);
                        count += 1;
                        data = item.OpenStream();
                        //sDocument.FileContents = StreamToByte(data);
                        sDocument.fileStream = data;

                        try
                        {
                            objModel.AddLargeDocument(sDocument);
                        }
                        catch
                        {
                            CreateErrorFileWhileLargeUpload(p_ArrListDocumentId, p_sErrorFolderName);
                        }
                        finally
                        {
                            data.Dispose();
                        }
                    }
                }
            }
            catch 
            {
                CreateErrorFileWhileLargeUpload(p_ArrListDocumentId, p_sErrorFolderName);
            }
            finally
            {
                objModel = null;
                this.RemoveTempFiles(attachments);//MITS #32088
            }
        }
        public static byte[] StreamToByte(Stream input)
        {
            //byte[] buffer = new byte[input.Length]; 
            //using (MemoryStream ms = new MemoryStream())
            //{
            //    int read;
            //    while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
            //    {
            //        ms.Write(buffer, 0, read);
            //    }
            //    return ms.ToArray();
            //}
            BinaryReader br = new BinaryReader(input);
            return br.ReadBytes((int)input.Length);
        }
        //MITS #32088 - Start
        private void RemoveTempFiles(AttachmentItemCollection attachments)
        {
            foreach (AttachmentItem item in attachments)
            {
                //if (File.Exists(item.GetTempFilePath()) && CheckFileAccess(item.GetTempFilePath()))
                //{
                //    File.Delete(item.GetTempFilePath());
                //}
                try //  Check File Access function is no longer in use - Code modified as per provided code review suggestions 
                {
                    File.Delete(item.GetTempFilePath());
                }
                catch
                {   
 
                }
            }
        }
        //public static bool CheckFileAccess(string sFilePath)
        //{
        //    try
        //    {
        //        using (FileStream fs = File.Open(sFilePath, FileMode.Open, FileAccess.Read, FileShare.None)) { }
        //    }
        //    catch (System.IO.IOException exp)
        //    {
        //        return false;
        //    }
        //    return true;
        //}
        //MITS #32088 - End

        private void EndUploadLargeDocument(IAsyncResult ar)
        {

        }


        private void CreateErrorFileWhileLargeUpload(ArrayList p_ArrListDocumentId, string errorFileFolder)
        {
            string[] arrDocumentId;
            string errorFileName = string.Empty;
            FileStream errorFileStream = null;

            try
            {
                for (int i = 0; i < p_ArrListDocumentId.Count; i++)
                {
                    errorFileName = errorFileFolder + p_ArrListDocumentId[i] + "_error";
                    if (!File.Exists(errorFileName))
                    {
                        errorFileStream = File.Create(errorFileName);
                    }
                }
            }
            finally
            {
                errorFileStream.Close();
            }
        }


        public void AddMultiple(AttachmentItemCollection attachments)
        {
            DocumentServiceHelper objModel = null;
            DocumentType document = null;
            RMServiceType objResponse = null;
            byte[] file = null;
           
            try
            {
                objModel = new DocumentServiceHelper();
                document = new DocumentType();
                document.Class = new CodeType();
                document.Category = new CodeType();
                document.Type = new CodeType();
                document.Title = AppHelper.GetFormValue("Title");
                document.FolderId = AppHelper.GetFormValue("FolderId").ToString();
                document.PsId = Conversion.ConvertStrToInteger(AppHelper.GetFormValue("Psid").ToString());
                document.FilePath = "";
                document.Subject = AppHelper.GetFormValue("Subject");
                //Start HardCoded need to change values
                document.Token = AppHelper.GetSessionId();
                //End HardCoded need to change values
                document.Type.Id = Conversion.ConvertStrToInteger(AppHelper.GetFormValue("documenttypecode_cid"));
                document.Type.Desc = AppHelper.GetFormValue("documenttypecode");
                document.Class.Id = Conversion.ConvertStrToInteger(AppHelper.GetFormValue("documentclasscode_cid"));
                document.Class.Desc = AppHelper.GetFormValue("documentclasscode");
                document.Category.Id = Conversion.ConvertStrToInteger(AppHelper.GetFormValue("documentcategorycode_cid"));
                document.Category.Desc = AppHelper.GetFormValue("documentcategorycode");
                //HardCoded need to change values
                document.ScreenFlag = AppHelper.GetFormValue("flag").ToString();
                document.Keywords = AppHelper.GetFormValue("Keywords");
                document.Notes = AppHelper.GetFormValue("Notes");

                document.AttachTable = AppHelper.GetFormValue("AttachTableName").ToString();
                document.FormName = AppHelper.GetFormValue("FormName").ToString();
                document.AttachRecordId = Conversion.ConvertStrToInteger(AppHelper.GetFormValue("AttachRecordId").ToString());
                foreach (AttachmentItem item in attachments)
                {
                    if (item.Checked)
                    {
                        document.FileName = item.FileName;
                        System.IO.Stream data = item.OpenStream();
                        file = new byte[item.FileSize];
                        int len = Convert.ToInt32(item.FileSize);
                        data.Read(file, 0, len);
                        document.FileContents = file;
                        objResponse = objModel.AddDocument(document);
                    }
                }

            }
            finally
            {
                objModel = null;

            }
        }
        // /Document/GetUsersList
        public UsersList GetEmailUsersList(string Psid)
        {
            DocumentServiceHelper objModel = null;
            DocumentUserRequest request = null;
            bool ret = false;
            UsersList list = null;

            try
            {
                objModel = new DocumentServiceHelper();
                request = new DocumentUserRequest();
                request.Psid = Psid;
                request.Token = AppHelper.GetSessionId();
                //Call the Model Helper
                list = objModel.GetUsers(request);
            }

            finally
            {
                objModel = null;
            }
            return list;
        }
        public void EmailDocuments()
        {
            DocumentServiceHelper objModel = null;
            EmailDocumentsRequest request = null;
            string selectedUserEmailIds = string.Empty;
            bool ret = false;
            try
            {
                objModel = new DocumentServiceHelper();
                request = new EmailDocumentsRequest();
                //changes by Nitin for Mits 15565 on 20-Apr-2009 starts , to implement CUL 
                selectedUserEmailIds = AppHelper.GetFormValue("UserIdStr");
                selectedUserEmailIds = selectedUserEmailIds.Replace(' ', ','); 
                request.UserEmailIds = selectedUserEmailIds;
                //changes by Nitin for Mits 15565 on 20-Apr-2009 end
                request.OtherEmailIds = AppHelper.GetFormValue("txtEmailAdd");
                request.Documents = AppHelper.GetFormValue("hdocId");
                request.Message = AppHelper.GetFormValue("Message");
                request.Subject = AppHelper.GetFormValue("EmailSubject");
                if (request.Documents.Length > 0)
                {
                    if (request.Documents.IndexOf(",") != -1)
                    {
                        request.Documents = request.Documents.Substring(0, request.Documents.Length - 1);
                    }
                }
                request.TableName = AppHelper.GetFormValue("TableName");
                request.Psid = AppHelper.GetFormValue("Psid");
                request.Token = AppHelper.GetSessionId();
                //Call the Model Helper
                ret = objModel.EmailDocuments(request);
            }
            finally
            {
                objModel = null;
            }

        }
        //
        //RecordId=1&FormName=event&psid=11000&Regarding=Attachment for Event  : dvbdffEV2006000001PP 
        //flag=Files
        //Document/List
        public DocumentList List()
        {
            DocumentServiceHelper objModel = null;
            DocumentListRequest document = null;
            
            DocumentList objList = null;

            try
            {
                objModel = new DocumentServiceHelper();
                document = new DocumentListRequest();
                //Hardcoded
                document.Token = AppHelper.GetSessionId();
                if (AppHelper.GetFormValue("ParentFolderId") == null)
                    document.ParentFolderId = "";
                else
                    document.ParentFolderId = AppHelper.GetFormValue("ParentFolderId");
                if (AppHelper.GetFormValue("FolderId") == null)
                    document.FolderId = "";
                else
                    document.FolderId = AppHelper.GetFormValue("FolderId");
                document.SortOrder = AppHelper.GetFormValue("hdnOrderBy");
                document.PageNumber = "0";// AppHelper.GetFormValue("PageNumber");
                document.ParentFolderName = AppHelper.GetFormValue("ParentFolderName");
                document.Psid = "50";// AppHelper.GetFormValue("Psid");
                document.ScreenFlag = "Files";//AppHelper.GetFormValue("flag");
                document.FolderName = AppHelper.GetFormValue("foldername");
                //Call the Model Helper

                objList = objModel.DocumentsList(document);
                objList.ScreenFlag = "Files";
            }
            finally
            {
                objModel = null;
            }
            return objList;

        }
        //Document/Download/Id
        public DocumentType Download(int docid)
        {
            DocumentServiceHelper objModel = null;
            DocumentType document = null;
            DocumentType documentTemp = null;
            
            try
            {
                objModel = new DocumentServiceHelper();
                document = new DocumentType();
                documentTemp = new DocumentType();
                document.Class = new CodeType();
                document.Class.Desc = "";
                document.Category = new CodeType();
                document.Category.Desc = "";
                document.Type = new CodeType();
                document.Type.Desc = "";

                document.Token = AppHelper.GetSessionId();
                document.DocumentId = docid;
                documentTemp = objModel.Download(document);
                documentTemp.FileName = AppHelper.GetFormValue("FileName");

            }

            finally
            {
                objModel = null;
            }
            return documentTemp;
        }

        public delegate void AsyncDownloadDocument(StreamedDocumentType document, string tempFileName);
        
        public void DownloadLargeFile(int docid,string tempFileName)
        {
            StreamedDocumentType document = null;
            document = new StreamedDocumentType();
            document.Class = new StreamedCodeType();
            document.Category = new StreamedCodeType();
            document.PsId = Conversion.ConvertStrToInteger(AppHelper.GetFormValue("Psid").ToString());
            document.Token = AppHelper.GetSessionId();
            document.ClientId = AppHelper.ClientId;
            document.ScreenFlag = AppHelper.GetFormValue("flag").ToString();
            document.DocumentId = docid;
            document.FormName = AppHelper.GetFormValue("FormName").ToString();//rsushilaggar MITS 23234
            //document.BaseUrl = Common.CommonFunctions.RetrieveBaseURL();
            document.ClientId = AppHelper.ClientId;
            DownloadEndInvoke(document, tempFileName);
        }

        private void DownloadEndInvoke(StreamedDocumentType document, string tempFileName)
        {
            // create a delegate of MethodInvoker poiting to our Foo function.
            AsyncDownloadDocument downloadDelegate = new AsyncDownloadDocument(BeginDownloadLargeDocument);
            downloadDelegate.BeginInvoke(document, tempFileName, new AsyncCallback(EndDownloadLargeDocument), null);
        }

        private void BeginDownloadLargeDocument(StreamedDocumentType document, string tempFileName)
        {
            DocumentServiceHelper objModel = null;
            Stream downloadedData = null;
            
            try
            {
                objModel = new DocumentServiceHelper();
                downloadedData = objModel.DownloadLargeDocument(document);

                CreateTempFile(tempFileName, downloadedData);
                //once temp file created successfully ,rename it as completed
                File.Move(tempFileName, tempFileName + "_completed");
            }
            catch (Exception ee)
            {
                //incase of any error create error file at same path and then thread ends after throwing
                //exception
                CreateErrorFile(tempFileName);
                throw ee;
            }
            finally
            {
                downloadedData = null;
                objModel = null;
            }
        }

        private void CreateTempFile(string tempFileName, Stream downloadedData)
        {
            FileStream targetStream = null;
            byte[] buffernew = null;
            BinaryWriter objBinaryWriter = null;
            const int bufferLen = 32768; //4096;
            byte[] buffer = null;
            int count = 0;

            try
            {
                using (targetStream = new System.IO.FileStream(tempFileName, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None))
                {
                    objBinaryWriter = new BinaryWriter(targetStream);
                    //read from the input stream in 4K chunks
                    //and save to output stream
                    
                    buffer = new byte[bufferLen];
                    while ((count = downloadedData.Read(buffer, 0, bufferLen)) > 0)
                    {
                        objBinaryWriter.Write(buffer, 0, count);
                        objBinaryWriter.Flush();
                    }
                }
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                objBinaryWriter.Close();
                targetStream.Close();
                downloadedData.Close();
            }
        }

        private void CreateErrorFile(string errorFileName)
        {
            FileStream errorFileStream = null;
            try
            {
                //File.Move(tempFileName, tempFileName + "_completed");
                if(File.Exists(errorFileName))
                {
                    File.Move(errorFileName, errorFileName  + "_error");
                    return;
                }

               errorFileStream =  File.Create(errorFileName  + "_error");
            }
            finally
            {
                errorFileStream.Close();
            }
        }

        
        private void EndDownloadLargeDocument(IAsyncResult ar)
        {
        }

        public void MoveDocuments()
        {
            DocumentServiceHelper objModel = null;
            MoveDocumentsRequest request = null;
            bool ret = false;
            try
            {
                objModel = new DocumentServiceHelper();
                request = new MoveDocumentsRequest();
                request.Documents = AppHelper.GetFormValue("hdocId");
                if (request.Documents.Length > 0)
                {
                    if (request.Documents.IndexOf(",") != -1)
                    {
                        request.Documents = request.Documents.Substring(0, request.Documents.Length - 1);
                    }
                }
                request.NewFolderId = AppHelper.GetFormValue("folderslist");
                request.Token = AppHelper.GetSessionId();
                //Call the Model Helper
                ret = objModel.MoveDocuments(request);
            }
            finally
            {
                objModel = null;
            }
        }
        public List<Folder> GetFoldersList(string FolderId)
        {
            DocumentServiceHelper objModel = null;
            Folder request = null;
            bool ret = false;
            List<Folder> objFolders = null;
            try
            {
                objModel = new DocumentServiceHelper();
                request = new Folder();
                request.FolderID = FolderId;
                request.Token = AppHelper.GetSessionId();
                //Call the Model Helper
                objFolders = objModel.GetFoldersList(request);

            }

            finally
            {
                objModel = null;
            }
            return objFolders;
        }
        public void TransferDocuments()
        {
            DocumentServiceHelper objModel = null;
            TransferDocumentsRequest request = null;
            bool ret = false;
            try
            {
                objModel = new DocumentServiceHelper();
                request = new TransferDocumentsRequest();
                request.UserLoginNames = AppHelper.GetFormValue("Userslist");
                request.DocumentIds = AppHelper.GetFormValue("hdocId");
                if (request.DocumentIds.Length > 0)
                {
                    if (request.DocumentIds.IndexOf(",") != -1)
                    {
                        request.DocumentIds = request.DocumentIds.Substring(0, request.DocumentIds.Length - 1);
                    }
                }
                request.ScreenFlag = AppHelper.GetFormValue("flag");
                request.Psid = AppHelper.GetFormValue("Psid");
                request.Token = AppHelper.GetSessionId();
                //Call the Model Helper
                ret = objModel.TransferDocuments(request);
            }
            finally
            {
                objModel = null;
            }

        }
        // /Document/GetFoldersList/GetUsersList
        public UsersList GetUsersList(string Psid)
        {
            DocumentServiceHelper objModel = null;
            DocumentUserRequest request = null;
            bool ret = false;
            UsersList list = null;
            try
            {
                objModel = new DocumentServiceHelper();
                request = new DocumentUserRequest();
                request.Psid = Psid;
                request.Token = AppHelper.GetSessionId();
                //Call the Model Helper
                list = objModel.GetUsers(request);
            }
            finally
            {
                objModel = null;
            }
            return list;
        }
        public DocumentType Edit(int id, string flag, string Psid,string ScreenName,int Upload_Status)
        {
            DocumentServiceHelper objModel = null;
            DocumentType document = null;
            try
            {
                objModel = new DocumentServiceHelper();
                document = new DocumentType();
                document.Class = new CodeType();
                document.Category = new CodeType();
                document.Type = new CodeType();
                document.Token = AppHelper.GetSessionId();
                document.DocumentId = id;
                document.FormName = ScreenName;
                document.PsId = Conversion.ConvertStrToInteger(Psid);
                document.ScreenFlag = flag;
                document.Upload_Status = Upload_Status;
                document = objModel.EditDocument(document);
                document.ScreenFlag = flag;
            }
            finally
            {
                objModel = null;
            }
            return document;
        }
        // /Document/Edit/Id
        public DocumentType EditDocument(int id)
        {
            DocumentServiceHelper objModel = null;
            DocumentType document = null;
            try
            {
                objModel = new DocumentServiceHelper();
                document = new DocumentType();
                document.Class = new CodeType();
                document.Category = new CodeType();
                document.Type = new CodeType();
                document.DocumentId = id;
                document.Title = AppHelper.GetFormValue("Title");
                document.FolderId = AppHelper.GetFormValue("FolderId").ToString();
                document.PsId = Conversion.ConvertStrToInteger(AppHelper.GetFormValue("Psid").ToString());
                document.FilePath = "";
                document.Subject = AppHelper.GetFormValue("Subject");
                document.Token = AppHelper.GetSessionId();

                //Start Code For now commenting till Code Lookup and Quick Lookup gets Completed
                document.Type.Id = Conversion.ConvertStrToInteger(AppHelper.GetFormValue("documenttypecode_cid"));
                document.Type.Desc = AppHelper.GetFormValue("documenttypecode");
                document.Class.Id = Conversion.ConvertStrToInteger(AppHelper.GetFormValue("documentclasscode_cid"));
                document.Class.Desc = AppHelper.GetFormValue("documentclasscode");
                document.Category.Id = Conversion.ConvertStrToInteger(AppHelper.GetFormValue("documentcategorycode_cid"));
                document.Category.Desc = AppHelper.GetFormValue("documentcategorycode");
                //End Code
                document.Keywords = AppHelper.GetFormValue("Keywords");
                document.Notes = AppHelper.GetFormValue("Notes");
                document.FileName = AppHelper.GetFormValue("hdnFileName");
                if (AppHelper.GetFormValue("AttachTable") == "")
                    document.AttachTable = "";
                else
                    document.AttachTable = AppHelper.GetFormValue("AttachTable").ToString();
                if (AppHelper.GetFormValue("FormName") == "")
                    document.FormName = "";
                else
                    document.FormName = AppHelper.GetFormValue("FormName").ToString();
                if (AppHelper.GetFormValue("AttachRecordId") == "")
                    document.AttachRecordId = 0;
                else
                    document.AttachRecordId = Conversion.ConvertStrToInteger(AppHelper.GetFormValue("AttachRecordId").ToString());
                bool bln = objModel.UpdateDocument(document);
            }

            finally
            {

            }
            return document;

        }
        public void CopyDocuments()
        {
            DocumentServiceHelper objModel = null;
            TransferDocumentsRequest request = null;
            bool ret = false;
            try
            {
                objModel = new DocumentServiceHelper();
                request = new TransferDocumentsRequest();
                request.UserLoginNames = AppHelper.GetFormValue("Userslist");
                request.DocumentIds = AppHelper.GetFormValue("hdocId");
                if (request.DocumentIds.Length > 0)
                {
                    if (request.DocumentIds.IndexOf(",") != -1)
                    {
                        request.DocumentIds = request.DocumentIds.Substring(0, request.DocumentIds.Length - 1);
                    }
                }
                request.ScreenFlag = AppHelper.GetFormValue("flag");
                request.Psid = AppHelper.GetFormValue("Psid");
                request.Token = AppHelper.GetSessionId();
                //Call the Model Helper
                ret = objModel.CopyDocuments(request);
            }
            finally
            {
                objModel = null;
            }

        }
        // /Document/List
        //AttachTable=event&AttachRecordId=1&FormName=event&psid=11000&Regarding=Attachment for Event  : dvbdffEV2006000001PP
        //flag=Files
        //Document/List
        //Added:Yukti,DT:11/14/2013,MITS 30990
        //public DocumentList List(string exp, string ScreenFlag, string Psid,string PageNumber)
        public DocumentList List(string exp, string ScreenFlag, string Psid,string PageNumber,bool AddDoc)
        //Ended:Yukti,MITS 30990
        {
            DocumentServiceHelper objModel = null;
            DocumentListRequest document = null;
            
            DocumentList objList = null;

            try
            {
                objModel = new DocumentServiceHelper();
                document = new DocumentListRequest();
                //Hardcoded
                document.Token = AppHelper.GetSessionId();
                if (AppHelper.GetFormValue("ParentFolderId") == null)
                    document.ParentFolderId = "";
                else
                    document.ParentFolderId = AppHelper.GetFormValue("ParentFolderId");
                if (AppHelper.GetFormValue("FolderId") == null)
                    document.FolderId = "";
                else
                    document.FolderId = AppHelper.GetFormValue("FolderId");
                
                ////Added:Yukti,MITS 30990,DT:11/13/2013

                // akaushik5 Commented for MITS 37157 Starts
                //if (AddDoc == true)
                //{
                //    document.SortOrder = "";
                //}
                //else
                //{
                // akaushik5 Commented for MITS 37157 Ends
                document.SortOrder = exp;
                // akaushik5 Commented for MITS 37157 Starts
                //}
                // akaushik5 Commented for MITS 37157 Ends

                //document.SortOrder = exp;         
                //Ended:Yukti,DT:11/13/2013
                document.PageNumber = PageNumber;// AppHelper.GetFormValue("PageNumber");
                document.ParentFolderName = AppHelper.GetFormValue("ParentFolderName");
                document.Psid = Psid;// AppHelper.GetFormValue("Psid");
                document.ScreenFlag = ScreenFlag;//AppHelper.GetFormValue("flag");
                document.FolderName = AppHelper.GetFormValue("foldername");
                //Call the Model Helper
                if (document.ScreenFlag == "")
                {
                    document.TableName = AppHelper.GetValue("AttachTableName").ToString();
                    document.FormName = AppHelper.GetValue("FormName").ToString();
                    document.RecordId = AppHelper.GetValue("AttachRecordId").ToString();
                    document.NonMCMFormName = AppHelper.GetValue("NonMCMFormName").ToString();
                    objList = objModel.GetAttachments(document);
                }
                else
                {
                    objList = objModel.DocumentsList(document);
                }
                //objList.ScreenFlag = "Files";
            }
            finally
            {
                objModel = null;
            }
            return objList;

        }
        public DocumentType GetExecutiveSummaryFileDetails(DocumentType dtype)
        {
            DocumentServiceHelper objModel = null;

            DocumentType documentTemp = null;



            try
            {
                objModel = new DocumentServiceHelper();

                dtype.Token = AppHelper.GetSessionId();
                documentTemp = objModel.GetExecutiveSummaryFileDetails(dtype);

            }

            finally
            {
                objModel = null;
            }
            return documentTemp;
        }
        public bool CheckOutlookSetting(DocumentType dtype)
        {
            DocumentServiceHelper objModel = null;

            bool IsOutlookEnabled = false;



            try
            {
                objModel = new DocumentServiceHelper();

                dtype.Token = AppHelper.GetSessionId();
                IsOutlookEnabled = objModel.CheckOutlookSetting(dtype);

            }

            finally
            {
                objModel = null;
            }
            return IsOutlookEnabled;
        }
        //(oRequest.DocumentType, oRequest.FileName, oRequest.ModuleName, oRequest.RelatedId, oRequest.fileStream, oRequest.Token,oRequest.filePath, oRequest.Errors, oRequest.ClientId);
        public void UploadFile(Riskmaster.UI.DataStreamingService.StreamedUploadDocumentType oInput)
        {
            DocumentServiceHelper objModel = null;
            Riskmaster.UI.DataStreamingService.StreamedUploadDocumentType oRequest = new UI.DataStreamingService.StreamedUploadDocumentType();
            try
            {
                objModel = new DocumentServiceHelper();

                oRequest.ClientId = oInput.ClientId;
                oRequest.DocumentType = oInput.DocumentType;
                oRequest.filePath = oInput.filePath;
                oRequest.Token = oInput.Token;
                //oRequest.fileStream =
                oRequest.FileName = oInput.FileName;
                oRequest.ModuleName = oInput.ModuleName;
                oRequest.RelatedId = oInput.RelatedId;
                oRequest.fileStream = oInput.fileStream;
                objModel.UploadFile(oRequest);
            }

            finally
            {
                objModel = null;
            }
            
        }
    }
}
