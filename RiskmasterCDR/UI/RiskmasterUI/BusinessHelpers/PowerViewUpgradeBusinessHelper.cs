﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.ServiceHelpers;
using System.IO;
using System.ServiceModel;
using Riskmaster.Common;

namespace Riskmaster.BusinessHelpers
{
    public class PowerViewUpgradeBusinessHelper
    {
        /// <summary>
        /// Function for converting any xml tag in form of Aspx controls
        /// created by- Parijat, for dynamic Controls.
        /// </summary>
        /// <param name="bReadOnly">If the converted should be in form of read only.</param>
        /// <param name="bTopDown">If it has to be in form of top down layout.</param>
        /// <param name="viewXml">Xml document which needs to be converted.</param>
        /// <param name="divName">Any name Of div tag which is being converted</param>
        /// <returns>The converted result in the form of ASPX controls.</returns>
        public string  UpgradeXmlTagToAspxForm(bool breadonly, bool bTopDown, string viewXml, string divName)
        {
            PowerViewUpgradeServiceHelper objModel = null;
            string document ="";
            try
            {
                objModel = new PowerViewUpgradeServiceHelper();
                //document.Token = HttpContext.Current.Session["SessionId"].ToString();

                //document.FolderName = AppHelper.GetFormValue("foldername");
                ////Call the Model Helper

                document = objModel.UpgradeXmlTagToAspxForm(breadonly,bTopDown,viewXml,divName);

            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //objModel = null;
            }
            return document;

        }
        /// <summary>
        /// Function for fetching of Jurisdictional data in form of Aspx, stored in the database
        /// created by- Parijat, for dynamic Controls. --Jurisdictional
        /// </summary>
        /// <param name="stateID"></param>
        /// <returns></returns>
        public string FetchJurisdictionalData(int stateID)
        {
            PowerViewUpgradeServiceHelper objModel = null;
            string document = "";
            try
            {
                objModel = new PowerViewUpgradeServiceHelper();
                document = objModel.FetchJurisdictionalData(stateID);

            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //objModel = null;
            }
            return document;

        }
    }
}
