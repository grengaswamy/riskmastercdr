﻿using System;
using Riskmaster.ServiceHelpers;
using Riskmaster.UI.IntegralInterfaceService;
using System.Xml;

namespace Riskmaster.BusinessHelpers
{
    public class IntegralInterfaceBusinessHelper
    {       
        public PolicySearch GetPolicySearchResults(PolicySearch oSearchRequest)
        {
            IntegralInterfaceServiceHelper objModel = null;
            PolicySearch oSearchResponse = null;
            try
            {
                objModel = new IntegralInterfaceServiceHelper();
                oSearchRequest.Token = AppHelper.GetSessionId();

                oSearchResponse = objModel.GetPolicySearchResult(oSearchRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }

            return oSearchResponse;
        }
     
        public ExternalPolicyData GetPolicyEnquiryResult(PolicyEnquiry oEnquiryRequest)
        {
            IntegralInterfaceServiceHelper objModel = null;
            ExternalPolicyData oEnquiryResponse = null;
            try
            {
                objModel = new IntegralInterfaceServiceHelper();
                oEnquiryRequest.Token = AppHelper.GetSessionId();
                oEnquiryResponse = objModel.GetPolicyEnquiryResult(oEnquiryRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            return oEnquiryResponse;
        }
        public PolicyValidateResponse PolicyValidation(PolicyValidateInput oPolicyValidateInput)
        {
            IntegralInterfaceServiceHelper objModel = null;
            PolicyValidateResponse oPolicyValidateResponse = null;
            try
            {
                objModel = new IntegralInterfaceServiceHelper();
                oPolicyValidateInput.Token = AppHelper.GetSessionId();
                oPolicyValidateResponse = objModel.PolicyValidation(oPolicyValidateInput);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            return oPolicyValidateResponse;
        }
        public PolicySaveRequest SavePolicyAllData(ExternalPolicyData oSaveRequest)
        {
            IntegralInterfaceServiceHelper objModel = null;
            PolicySaveRequest oPolicySaveResponse = null;
            try
            {
                objModel = new IntegralInterfaceServiceHelper();
                oSaveRequest.Token = AppHelper.GetSessionId();
                oPolicySaveResponse = objModel.SavePolicyAllData(oSaveRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            return oPolicySaveResponse;
        }
    }
}
