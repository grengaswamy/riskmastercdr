﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Riskmaster.BusinessHelpers
{
    public class UserPreferences
    {

        public string width { get; set; }
        public ColumnDefHelper colDef { get; set; }
        public string sortDirection { get; set; }
        public string Name { get; set; }
        public string Key { get; set; }
        public int ColOrder { get; set; }
        public string ColWidth { get; set; }
        public bool SortOrder { get; set; }
        public string SortType { get; set; }

    }
}