﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.ComponentModel;
//using Riskmaster.UI.RMSSO;//Change by kuladeep for Jira-156
using Riskmaster.Models;

[DataObject(true)]
//public class LDAPDataSource : RiskmasterLDAPMembershipProvider //Change by kuladeep for Jira-156
public class LDAPDataSource : RiskmasterMembershipProvider
{

    #region Select
    /// <summary>
    /// Gets the LDAP Membership Provider record
    /// </summary>
    /// <returns>DataSet containing the LDAP Membership Provider record</returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public DataSet SelectLDAPRecord()
    {
        //Code change by kuladeep for Jira-156 Start---Make Rest service.

        //SSOServiceClient objSvcClient = new SSOServiceClient();
        //objSvcClient.Open();
        ////DataSet dstADRecord = objSvcClient.GetLDAPProvider();
        //DataSet dstLDAPRecord = objSvcClient.GetLDAPProvider();
        //objSvcClient.Close();
        
        RMServiceType oRMServiceType = new RMServiceType();
        oRMServiceType.Token = AppHelper.GetSessionId();
        oRMServiceType.ClientId = AppHelper.ClientId;
        DataSet dstLDAPRecord = AppHelper.GetResponse<DataSet>("RMService/SSO/ldapprovider", AppHelper.HttpVerb.POST, "application/json", oRMServiceType);
        //Code change by kuladeep for Jira-156 End

        //DataTable dtADRecord = new DataTable();
        DataTable dtLDAPRecord = new DataTable();

        //dtADRecord = dstADRecord.Tables[0];
        dtLDAPRecord = dstLDAPRecord.Tables[0];

        //foreach (DataColumn dtColumn in dtADRecord.Columns)
        foreach (DataColumn dtColumn in dtLDAPRecord.Columns)
        {
            string strColumnName = dtColumn.ColumnName;
            string strDataType = dtColumn.DataType.ToString();
        }

        //return dstADRecord;
        return dstLDAPRecord;

    } // method: SelectLDAPRecord 
    #endregion

    #region Insert
    /// <summary>
    /// Inserts the specified LDAP Membership Provider record
    /// </summary>
    /// <param name="objLDAPDataSource"></param>
    [DataObjectMethod(DataObjectMethodType.Insert)]
    public void InsertLDAPRecord(LDAPDataSource objLDAPDataSource)
    {

        //Code change by kuladeep for Jira-156 Start---Make Rest service.

        //SSOServiceClient objSvcClient = new SSOServiceClient();
        //objSvcClient.Open();
        //RiskmasterLDAPMembershipProvider objLDAPProvider = new RiskmasterLDAPMembershipProvider();
        //objLDAPProvider.AdminUserName = objLDAPDataSource.AdminUserName;
        //objLDAPProvider.AdminPwd = objLDAPDataSource.AdminPwd;
        //objLDAPProvider.IsEnabled = objLDAPDataSource.IsEnabled;
        //objLDAPProvider.HostName = objLDAPDataSource.HostName;
        //objLDAPProvider.ProviderID = objLDAPDataSource.ProviderID;
        //objLDAPProvider.ConnectionType = objLDAPDataSource.ConnectionType;

        //// MAC : Added these
        //objLDAPProvider.BaseDN = objLDAPDataSource.BaseDN;
        //objLDAPProvider.HostName = objLDAPDataSource.HostName;
        //objLDAPProvider.UserAttribute = objLDAPDataSource.UserAttribute;
        //objSvcClient.InsertLDAPProvider(objLDAPProvider);  
        //objSvcClient.Close();


        RiskmasterMembershipProvider oRiskmasterMembershipProvider = null;

        oRiskmasterMembershipProvider = new RiskmasterMembershipProvider();
        oRiskmasterMembershipProvider.Token = AppHelper.GetSessionId();
        oRiskmasterMembershipProvider.ClientId = AppHelper.ClientId;

        oRiskmasterMembershipProvider.ProviderID = objLDAPDataSource.ProviderID;
        oRiskmasterMembershipProvider.HostName = objLDAPDataSource.HostName;
        oRiskmasterMembershipProvider.AdminUserName = objLDAPDataSource.AdminUserName;
        oRiskmasterMembershipProvider.AdminPwd = objLDAPDataSource.AdminPwd;
        oRiskmasterMembershipProvider.IsEnabled = objLDAPDataSource.IsEnabled;
        oRiskmasterMembershipProvider.ConnectionType = objLDAPDataSource.ConnectionType;
        // MAC : Added these
        oRiskmasterMembershipProvider.BaseDN = objLDAPDataSource.BaseDN;
        oRiskmasterMembershipProvider.HostName = objLDAPDataSource.HostName;
        oRiskmasterMembershipProvider.UserAttribute = objLDAPDataSource.UserAttribute;

        AppHelper.GetResponse("RMService/SSO/addldap", AppHelper.HttpVerb.POST, "application/json", oRiskmasterMembershipProvider);

        //Code change by kuladeep for Jira-156 End---Make Rest service.

    } // method: InsertLDAPRecord 
    #endregion

    #region Update
    /// <summary>
    /// Updates the specified LDAP Membership Provider record
    /// </summary>
    /// <param name="objLDAPDataSource"></param>
    [DataObjectMethod(DataObjectMethodType.Update)]
    public void UpdateLDAPRecord(LDAPDataSource objLDAPDataSource)
    {
        //Code change by kuladeep for Jira-156 Start---Make Rest service.

        //SSOServiceClient objSvcClient = new SSOServiceClient();

        //objSvcClient.Open();
        //RiskmasterLDAPMembershipProvider objLDAPProvider = new RiskmasterLDAPMembershipProvider();

        //objLDAPProvider.AdminUserName = objLDAPDataSource.AdminUserName;
        //objLDAPProvider.AdminPwd = objLDAPDataSource.AdminPwd;
        //objLDAPProvider.IsEnabled = objLDAPDataSource.IsEnabled;
        //objLDAPProvider.HostName = objLDAPDataSource.HostName;
        //objLDAPProvider.ProviderID = objLDAPDataSource.ProviderID;
        //objLDAPProvider.ConnectionType = objLDAPDataSource.ConnectionType;

        //// MAC : Added these
        //objLDAPProvider.BaseDN = objLDAPDataSource.BaseDN;
        //objLDAPProvider.HostName = objLDAPDataSource.HostName;
        //objLDAPProvider.UserAttribute = objLDAPDataSource.UserAttribute;
        //objSvcClient.UpdateLDAPProvider(objLDAPProvider);
        //objSvcClient.Close();


        RiskmasterMembershipProvider oRiskmasterMembershipProvider = null;
        oRiskmasterMembershipProvider = new RiskmasterMembershipProvider();
        oRiskmasterMembershipProvider.Token = AppHelper.GetSessionId();
        oRiskmasterMembershipProvider.ClientId = AppHelper.ClientId;

        oRiskmasterMembershipProvider.ProviderID = objLDAPDataSource.ProviderID;
        oRiskmasterMembershipProvider.HostName = objLDAPDataSource.HostName;
        oRiskmasterMembershipProvider.AdminUserName = objLDAPDataSource.AdminUserName;
        oRiskmasterMembershipProvider.AdminPwd = objLDAPDataSource.AdminPwd;
        oRiskmasterMembershipProvider.IsEnabled = objLDAPDataSource.IsEnabled;
        oRiskmasterMembershipProvider.ConnectionType = objLDAPDataSource.ConnectionType;
        // MAC : Added these
        oRiskmasterMembershipProvider.BaseDN = objLDAPDataSource.BaseDN;
        oRiskmasterMembershipProvider.HostName = objLDAPDataSource.HostName;
        oRiskmasterMembershipProvider.UserAttribute = objLDAPDataSource.UserAttribute;

        AppHelper.GetResponse("RMService/SSO/updateldapprovider", AppHelper.HttpVerb.POST, "application/json", oRiskmasterMembershipProvider);
        //Code change by kuladeep for Jira-156 End---Make Rest service.

    } // method: UpdateLDAPRecord 
    #endregion

    #region Delete
    /// <summary>
    /// Deletes the specified LDAP Membership Provider record
    /// </summary>
    /// <param name="objLDAPDataSource"></param>
    //[DataObjectMethod(DataObjectMethodType.Delete)]
    public void DeleteLDAPRecord(LDAPDataSource objLDAPDataSource)
    {
    }//method: DeleteLDAPRecord() 
}//class: LDAPDataSource
	#endregion