﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace Riskmaster.BusinessHelpers
{
    public class RMXMessageTemplateHelper
    {
        public string GetReserveApprovalMessageTempplate() // XMl Template Changed By Nitika For JIRA 8253 
        {
            return "<Message><Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization><Call><Function></Function></Call><Document><Reserve><ApproveReasonCom></ApproveReasonCom><ShowAllItem>True</ShowAllItem><ShowMyTrans></ShowMyTrans><ApproveorRejectRsv></ApproveorRejectRsv><SortOrder></SortOrder><SortColumn></SortColumn><IsJson>True</IsJson><ActionType></ActionType></Reserve></Document></Message>";//sharishkumar 6385
        }

        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        public XDocument SetUserPrefMessageTemplate(string strXml)
        {
            string str = "<Message><Authorization></Authorization><Call><Function>ReserveFundsAdaptor.SetUserPref</Function></Call><Document><UserPref>" + strXml + "</UserPref></Document></Message>";

            XElement oTemplate = XElement.Parse(@str);

            return new XDocument(oTemplate);
        }
        public XDocument GetUserPrefMessageTemplate()
        {
            string str = "<Message><Authorization></Authorization><Call><Function>ReserveFundsAdaptor.GetUserPrefXML</Function></Call><Document><UserPref></UserPref></Document></Message>";

            XElement oTemplate = XElement.Parse(@str);

            return new XDocument(oTemplate);
        }

        public string GetReserveWorkSheetMessageTemplate()
        {
            string str = "<Message><Document><valueupper /><valuelower /><test /><RswId>-1</RswId><ReserveApproval><ShowAllItems /><ShowMyTrans /><CallFor /><ReserveApprovals /></ReserveApproval><AppRejReqCom></AppRejReqCom><SortOrder></SortOrder><SortColumn></SortColumn><IsSort>True</IsSort><IsJson>True</IsJson><ActionType></ActionType></Document></Message>";//RMA-8253 pgupta93 //sharishkumar 6385
            return str;
        }

        //JIRA-6405 Start vchouhan6 Templates to store and get user preferences
        /// <summary>
        /// CWS request message template
        /// </summary>
        /// <returns></returns>
        public XDocument SetUserPrefMessageTemplateCAL(string strXml)
        {
            string str = "<Message><Authorization></Authorization><Call><Function>ClaimActLogAdaptor.SetUserPref</Function></Call><Document><UserPref>" + strXml + "</UserPref></Document></Message>";

            XElement oTemplate = XElement.Parse(@str);

            return new XDocument(oTemplate);
        }
        public XDocument GetUserPrefMessageTemplateCAL()
        {
            string str = "<Message><Authorization></Authorization><Call><Function>ClaimActLogAdaptor.GetUserPrefXML</Function></Call><Document><UserPref></UserPref></Document></Message>";

            XElement oTemplate = XElement.Parse(@str);

            return new XDocument(oTemplate);
        }
        //JIRA-6405 End vchouhan6 Templates to store and get user preferences

    }
}
