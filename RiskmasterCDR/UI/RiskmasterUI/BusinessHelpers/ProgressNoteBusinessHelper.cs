﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.ServiceHelpers;
//using Riskmaster.UI.ProgressNoteService;
using System.IO;
using System.ServiceModel;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using System.Text;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using Riskmaster.Models;


namespace Riskmaster.BusinessHelpers
{
    public class ProgressNoteBusinessHelper
    {
        //rsolanki2: Enhc Notes ajax updates
        public ProgressNotesType LoadPartial(ProgressNotesType document)
        {
            ProgressNoteServiceHelper objModel = null;

            try
            {
                objModel = new ProgressNoteServiceHelper();
                document.Token = AppHelper.GetSessionId();
                document.ClientId = AppHelper.ClientId;
                //document.Cli

                //document.FolderName = AppHelper.GetFormValue("foldername");
                ////Call the Model Helper

                document = objModel.LoadPartial(document);

            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //objModel = null;
            }
            return document;

        }
        public ProgressNotesType Load(ProgressNotesType document)
        {
            ProgressNoteServiceHelper objModel = null;

            try
            {
                objModel = new ProgressNoteServiceHelper();
                document.Token = AppHelper.GetSessionId();
                document.ClientId = AppHelper.ClientId;
           
                //document.FolderName = AppHelper.GetFormValue("foldername");
                ////Call the Model Helper

                document = objModel.Load(document);

            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //objModel = null;
            }
            return document;

        }
        public ProgressNotesType DeleteNote(ProgressNotesType document)
        {
            ProgressNoteServiceHelper objModel = null;

            try
            {
                objModel = new ProgressNoteServiceHelper();
                document.Token = AppHelper.GetSessionId();
                document.ClientId = AppHelper.ClientId;
                document = objModel.DeleteNote(document);

            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //objModel = null;
            }
            return document;

        }
	//mbahl3 enhance notes enhancement mits 30513
        public ProgressNotesTypeChange GetNotesCaption(ProgressNotesTypeChange document)
        {
            ProgressNoteServiceHelper objModel = null;
            ProgressNotesTypeChange  outDoc= null;
            try
            {
                objModel = new ProgressNoteServiceHelper();
                //outDoc = new ProgressNotesTypeChange();
                document.Token = AppHelper.GetSessionId();
                document.ClientId = AppHelper.ClientId;
                outDoc = objModel.GetNotesCaption(document);

            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                objModel = null;
            }
            return outDoc;
        }
//mbahl3 enhance notes enhancement mits 30513
        public ProgressNotesType PrintProgressNotes(ProgressNotesType document)
        {
            ProgressNoteServiceHelper objModel = null;

            try
            {
                objModel = new ProgressNoteServiceHelper();
                document.Token = AppHelper.GetSessionId();
                document.ClientId = AppHelper.ClientId;
                document = objModel.PrintProgressNotes(document);

            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //objModel = null;
            }
            return document;

        }
        public GetNoteDetailsObject GetNoteDetails(GetNoteDetailsObject document)
        {
            ProgressNoteServiceHelper objModel = null;
            HtmlDocument objHtmDoc;
            Stream objStream;

            try
            {
                objModel = new ProgressNoteServiceHelper();
                document.Token = AppHelper.GetSessionId();
                document.ClientId = AppHelper.ClientId;
                document = objModel.GetNoteDetails(document);             
                //rsolanki2 : mits  20796 - start
                //rectifying the Html dom structure using the htmlagilitypack
                // if there are HTML tags broken we simply skip on rectyifying the HTML as Htmlagilitypack creates unexpected results
                if (Regex.Matches(document.HTML, "<").Count
                    == Regex.Matches(document.HTML, ">").Count)
                {
                    objHtmDoc = new HtmlDocument();
                    objHtmDoc.OptionFixNestedTags = true;

                    objStream = new MemoryStream
                                (ASCIIEncoding.Default.GetBytes
                                    (Conversion.ConvertObjToStr
                                        (document.HTML)));
                    objHtmDoc.Load(objStream);
                    objStream.Close();
                    document.HTML = objHtmDoc.DocumentNode.InnerHtml;
                }
                // mkaran2 : MITS 32741 : start : code commented to avoid unusal formatting for HTML
                //else
                //{
                //    //rsolanki2: updates for MITS 21258:: when there is broken tags we replace them with the corresponding escape characters.
                //    document.HTML = System.Security.SecurityElement.Escape(document.HTML);                              
                //}
                
                //rsolanki2 : mits  20796 - end

                // mkaran2 : MITS 32741 : end

            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                objHtmDoc = null;
                objModel = null;
                objStream = null;
                //objModel = null;
            }
            return document;

        }
        public SelectClaimObject SelectClaim(SelectClaimObject document)
        {
            ProgressNoteServiceHelper objModel = null;

            try
            {
                objModel = new ProgressNoteServiceHelper();
                document.Token = AppHelper.GetSessionId();
                document.ClientId = AppHelper.ClientId;
                document = objModel.SelectClaim(document);

            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //objModel = null;
            }
            return document;

        }
        //Added by GBINDRA MITS#34104 WWIG Gap15 02052014 START
        public SelectClaimantObject SelectClaimant(SelectClaimantObject document)
        {
            ProgressNoteServiceHelper objModel = null;

            try
            {
                objModel = new ProgressNoteServiceHelper();
                document.Token = AppHelper.GetSessionId();
                document.ClientId = AppHelper.ClientId;
                document = objModel.SelectClaimant(document);

            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //objModel = null;
            }
            return document;

        }
        //Added by GBINDRA MITS#34104 WWIG Gap15 02052014 END
        public ProgressNote SaveNotes(ProgressNote document)
        {
            ProgressNoteServiceHelper objModel = null;

            try
            {
                objModel = new ProgressNoteServiceHelper();
                document.Token = AppHelper.GetSessionId();
                document.ClientId = AppHelper.ClientId;
                document = objModel.SaveNotes(document);

            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //objModel = null;
            }
            return document;

        }
        public ProgressNoteTemplates LoadTemplates(ProgressNoteTemplates document)
        {
            ProgressNoteServiceHelper objModel = null;

            try
            {
                objModel = new ProgressNoteServiceHelper();
                document.Token = AppHelper.GetSessionId();
                document.ClientId = AppHelper.ClientId;
                document = objModel.LoadTemplates(document);

            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //objModel = null;
            }
            return document;

        }
        public ProgressNoteTemplates SaveTemplates(ProgressNoteTemplates document)
        {
            ProgressNoteServiceHelper objModel = null;

            try
            {
                objModel = new ProgressNoteServiceHelper();
                document.Token = AppHelper.GetSessionId();
                document.ClientId = AppHelper.ClientId;
                document = objModel.SaveTemplates(document);

            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //objModel = null;
            }
            return document;

        }
        public ProgressNoteTemplates DeleteTemplate(ProgressNoteTemplates document)
        {
            ProgressNoteServiceHelper objModel = null;

            try
            {
                objModel = new ProgressNoteServiceHelper();
                document.Token = AppHelper.GetSessionId();
                document.ClientId = AppHelper.ClientId;
                document = objModel.DeleteTemplate(document);

            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //objModel = null;
            }
            return document;

        }
        public ProgressNoteTemplates GetTemplateDetails(ProgressNoteTemplates document)
        {
            ProgressNoteServiceHelper objModel = null;

            try
            {
                objModel = new ProgressNoteServiceHelper();
                document.Token = AppHelper.GetSessionId();
                document.ClientId = AppHelper.ClientId;
                document = objModel.GetTemplateDetails(document);

            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //objModel = null;
            }
            return document;

        }
        public ProgressNoteSettings GetNotesHeaderOrder(ProgressNoteSettings document)
        {
            ProgressNoteServiceHelper objModel = null;

            try
            {
                objModel = new ProgressNoteServiceHelper();
                document.Token = AppHelper.GetSessionId();
                document.ClientId = AppHelper.ClientId;
                document = objModel.GetNotesHeaderOrder(document);

            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //objModel = null;
            }
            return document;

        }
        public ProgressNoteSettings SaveNotesSettings(ProgressNoteSettings document)
        {
            ProgressNoteServiceHelper objModel = null;

            try
            {
                objModel = new ProgressNoteServiceHelper();
                document.Token = AppHelper.GetSessionId();
                document.ClientId = AppHelper.ClientId;
                document = objModel.SaveNotesSettings(document);

            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //objModel = null;
            }
            return document;

        }
              
    }
}
