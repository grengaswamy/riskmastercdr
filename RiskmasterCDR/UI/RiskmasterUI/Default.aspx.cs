﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;

namespace Riskmaster.UI
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Title = Riskmaster.AppHelpers.ConfigHelper.GetRMXVersion();
            Response.Redirect(Riskmaster.AppHelpers.ConfigHelper.GetDefaultPageUrl()); 
            
        }
    }
}
