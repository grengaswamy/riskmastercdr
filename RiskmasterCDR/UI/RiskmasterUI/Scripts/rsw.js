// JScript source code

var imgFolderOpen = new Image(16,16); 
var imgFolderClosed = new Image(16,16);
var oldNodId="";
var m_DataChanged=false;
var m_DisableSubmit = false; //smahajan6m
var m_bReserveAmntChanges = false; //Added by Nitin for R6

var m_arrReasonsToBeChecked = new Array();
var m_arrCommentsToBeChecked = new Array();
var m_ReserveSummaryCreated = false;

imgFolderOpen.src = "csc-Theme/riskmaster/img/handle.collapse.more.gif"; 
imgFolderClosed.src = "csc-Theme/riskmaster/img/handle.expand.end.gif";

function ToggleFolder(id) {
var folderopen; 

 document.getElementById('icon-' + id).src = imgFolderClosed.src;

    if (document.getElementById(id).style.display == "inline") {
        folderopen = "false";
    } else {
        folderopen = "true";
    }

    if (folderopen == "false") {
    document.getElementById(id).style.display = "none";
    document.getElementById('icon-' + id).src = imgFolderClosed.src;
    } else {
    document.getElementById(id).style.display = "inline";
    document.getElementById('icon-' + id).src = imgFolderOpen.src;
    }
    return false;
}

//todo this function to be called from MDI tree
// rswTypeToOpen may start with evt, clm, stl
function OpenWorkSheet(id, rswTypeToOpen)
{
	var ctrlStatus = getElementById('hdnRSWStatus');
	var ctlRSWType = getElementById('RSWType').value;
	
	//todo depends upon utility setting
//	var clmstsRelID = self.parent.frames["LeftTree"].document.getElementById('ClmStsRelID').value;
//    var clmstsRel = self.parent.frames["LeftTree"].document.getElementById('ClmStsRel').value;
//    
//    if (clmstsRelID != null)
//    {
//     if (clmstsRelID == '8')
//     {
//       alert("Reserve Worksheet can not be opened/created when the Claim is Closed or Open-Pending.");
//       self.parent.close();
//       return false;
//     }
//    }
    	
	if (ctrlStatus != null)
	{
		if (ctrlStatus.value == "New" || ctrlStatus.value == "Pending")
		{
			var ret = false;
			ret = confirm("A worksheet is already open. You will lose any changes made if another worksheet is opened. Click 'OK' to continue and 'Cancel' to continue working with the current worksheet.");
			if (ret)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	var ctrl = document.getElementById("hyp-"+id);	
	if (ctrl != null)
	{		
		if (ctrl.title.indexOf('Rejected') != -1) // If rejected sheet is being opened
		{
			var ret = false;
			ret = confirm("Do you want to open the Rejected worksheet in 'Edit' Mode? Click 'OK' for Edit Mode and 'Cancel' for Read-Only Mode.");
			if (ret)
			{
				self.parent.frames["MainFrame"].location.href = 'home?pg=riskmaster/Reserves/ReserveWorksheet/MainFrame&editRej=true&rswid='+id;
				ShowPlsWait();
				return false;
			}
		}
	}
	self.parent.frames["MainFrame"].location.href = 'home?pg=riskmaster/Reserves/ReserveWorksheet/MainFrame&rswid='+id;
	
	ShowPlsWait();
	return false;
}



function LoadNew()
{
    m_DisableSubmit=false;//smahajan6m
	var ret = false;
	var ctrl = document.getElementById('hdnRSWStatus');
	
	//todo This is depend on utility settings
	
    //	var clmstsRelID = document.getElementById('ClmStsRelID').value;
    //  var clmstsRel = document.getElementById('ClmStsRel').value;

    //    if (clmstsRelID != null)
    //    {
    //     if (clmstsRelID == '8')
    //     {
    //       alert("Reserve Worksheet can not be opened/created when the Claim is Closed or Open-Pending.");
    //       self.parent.close();
    //       return false;
    //     }
    //    }
	
	if (ctrl != null) // A worksheet is already open
	{
	    
		if (ctrl.value == "New")//New worksheet
		{
			ret = false;
			ret = confirm("A worksheet is already open. You will lose any changes made if a new worksheet is opened. Click 'OK' to continue and 'Cancel' to continue working with the current worksheet.");
			if (!ret)
				return false;
		}

		if (ctrl.value == "Pending")
		{
		    alert("A worksheet with the status 'Pending' has already been created. You may select that worksheet from the left pane and either (a) continue editing it and then submit it for approval; or (b) delete it, and then create a new worksheet.");
		    return false;
		}

		if (ctrl.value == "Pending Approval")
		{
		    alert("A worksheet with status 'Pending Approval' exists on this claim. A new worksheet may not be created until that worksheet has been either approved or rejected.");
		    return false;
		}

		if (ctrl.value == "Rejected") 
		{
		    alert("A worksheet with the status of 'Rejected' is present.  You may select that worksheet from the left pane and either (a) continue editing it and then submit it for approval; or (b) delete it, and then create a new worksheet.");
		    return false;
		}

		if (ctrl.value == "Approved") 
		{
		    var ctrlCloneFromApproved;
		    ctrlCloneFromApproved = document.getElementById('hdnCloneFromApproved');
		    ret = false;
		    ret = confirm("A current 'Approved' worksheet is present. Click 'OK' to create a new worksheet with values from the approved worksheet. Click on 'Cancel' to create a new worksheet from scratch.");
		    if (ret) {
		        if (ctrlCloneFromApproved != null) {
		            ctrlCloneFromApproved.value = "True";
		        }
		    }
		    else {
		        if (ctrlCloneFromApproved != null) {
		            ctrlCloneFromApproved.value = "False";
		        }
		    }
		    pleaseWait.Show();
		    return true;
		}
		
	}

	if (document.getElementById('hdnPenapprovalRWSId').value != '0')
	{
		alert("A worksheet with status 'Pending Approval' exists on this claim. A new worksheet may not be created until that worksheet has been either approved or rejected.");
		return false;
	}
	else if (document.getElementById('hdnPendingRWSId').value != '0')
	{
		alert("A worksheet with the status 'Pending' has already been created. You may select that worksheet from the left pane and either (a) continue editing it and then submit it for approval; or (b) delete it, and then create a new worksheet.");
		return false;
	}
	else if (document.getElementById('hdnRejectRWSId').value != '0')
	{
		alert("A worksheet with the status of 'Rejected' is present.  You may select that worksheet from the left pane and either (a) continue editing it and then submit it for approval; or (b) delete it, and then create a new worksheet.");
		return false;
	}
	else if (document.getElementById('hdnApprovedRWSId').value != '0')
	{
	    var ctrlCloneFromApproved;
	    ctrlCloneFromApproved = document.getElementById('hdnCloneFromApproved');
	    ret = false;
	    ret = confirm("A current 'Approved' worksheet is present. Click 'OK' to create a new worksheet with values from the approved worksheet. Click on 'Cancel' to create a new worksheet from scratch.");
	    if (ret) 
	    {
	        if (ctrlCloneFromApproved != null)
	        {
	            ctrlCloneFromApproved.value = "True";
	        }
	    }
	    else 
	    {
	        if (ctrlCloneFromApproved != null) 
	        {
	            ctrlCloneFromApproved.value = "False";
            }
	    }
		return true;
	}

	pleaseWait.Show();
	return true;
}

function RSW_Radio_Change(ctl)
{	
	var s=ctl.value;

	//Update Date Submitted
	var ctrl=document.getElementById('dtSubmit_'+s);
	if(ctrl!=null)
		document.getElementById('DateSubmitted').innerText='Date Submitted: '+ctrl.value;
	
	//Update Submitted by
	var ctrl=document.getElementById('submitBy_'+s);
	if(ctrl!=null)
		document.getElementById('SubmittedBy').innerText='Submitted By: '+ctrl.value;	
}

function ToggleCheck(obj)
{
	if (obj.id == "statuteLim")
	{
		if (obj.checked == true)
			document.getElementById('openRep').checked = false;
	}
	else if (obj.id == "openRep")
	{
		if (obj.checked == true)
			document.getElementById('statuteLim').checked = false;
	}
	return false;
}

//Added by Nitin for Mits 18882
function SetWipedoutAfterApprove() 
{
    var ctrlRSWStatus = document.getElementById('hdnRSWStatus');
    var hdnApprovedBtnClicked = document.getElementById('hdnApprovedBtnClicked');

    if (hdnApprovedBtnClicked.value == 'true') 
    {
        if (ctrlRSWStatus && ctrlRSWStatus.value == 'Approved')
        {
            parent.MDISetWipedOut();
        }
    }
}


//Changed by Nitin for ReserveWorkSheet on 08-Sept
function RefreshRSWScreen() {    
    var Status = document.getElementById('hdnReqStatus');
    var ctlUnderLimit = document.getElementById('hdnUnderLimit');
    var ctlUnderClaimIncLimit = document.getElementById('hdnUnderClaimIncLimit');//sachin 6385
    var ctlHoldStatus = document.getElementById('hdnHoldStatus');//sachin 7810
	var ctlSMSCheckToUpdate = document.getElementById('hdnSMSCheckToUpdate');
	var ctrlReserveTypeTabs = document.getElementById('hdnReserveTypeTabs'); //hdnReserveTypeTabs
	
	if (ctlUnderLimit != null && ctlUnderLimit.value != "")
	{
	    //checking if there are security permissions to update

	   if (ctlUnderLimit.value == "Yes") 
      {
        parent.MDIRefreshCurrentNode();
        if (ctlSMSCheckToUpdate.value != "No") 
        {
            var ret = false;
            //ret = confirm("Reserves are within your approval authority. Worksheet does not need to be submitted for further approval. \r\nClick 'OK' to Approve the worksheet and apply the new reserves to the Claim or \r\nClick 'Cancel' to close this message and then click 'Approve' to Approve the worksheet and apply the new reserves to the Claim.");
            if (ret) 
            {
                document.getElementById('hdnAproveFromClientSide').value = "true";
                pleaseWait.Show();
                document.forms[0].submit();
                return false;
            }
        }
        else 
        {
            alert("Reserves are within your approval authority. You do not have sufficient security permission to Approve this Worksheet.The worksheet is being sent to your supervisor for approval.");
        }
      }
	  //else if (ctlUnderLimit.value == "No")
	  //{
      //    //sachin 7810 starts
	  //    //alert("Reserves exceed your authority limits. The worksheet is being sent to your supervisor for approval.");
	  //    if (ctlHoldStatus != null) {
	  //        alert("The worksheet is being sent to your supervisor for approval as " + ctlHoldStatus.value + ".");
	  //    }
	  //    //sachin 7810 ends
	  //    parent.MDIRefreshCurrentNode();	    
	  //}
	  else if (ctlUnderLimit.value == "NoSupervisor")
	  {
	    alert("Reserves exceed your authority limits and you don't have any supervisor. The worksheet will only be approved/rejected by your supervisor.");
	  }
	  else if (ctlUnderLimit.value == "Reject") {
	    parent.MDIRefreshCurrentNode();
	    //alert("Reserves are within your approval authority. Worksheet has been Rejected.  After closing this message, You may select this Rejected worksheet from the left pane and either (a) continue editing it and then submit it for approval; or (b) delete it, and then create a new worksheet.");
	  }
	  else if (ctlUnderLimit.value == "NoAuthSubmit")
	  {
	    alert("You are not authorized to submit this worksheet.");
	  }
	  // ctlUnderLimit.value = "";
	  // ctlHoldStatus.value = "";//sachin 7810
	}
    //Snehal 6385 starts


        
	if (ctlUnderLimit.value == "No" || ctlUnderClaimIncLimit=="No")
	{
	    parent.MDIRefreshCurrentNode();
	}
       
	    //{
    //else if (ctlUnderLimit.value == "No")
    //{
    //    //sachin 7810 starts
    //    //alert("Reserves exceed your authority limits. The worksheet is being sent to your supervisor for approval.");
    //    if (ctlHoldStatus != null) {
    //        alert("The worksheet is being sent to your supervisor for approval as " + ctlHoldStatus.value + ".");
    //    }
    //    //sachin 7810 ends
    //    parent.MDIRefreshCurrentNode();	    
    //}
	//if (ctlUnderClaimIncLimit != null && ctlUnderClaimIncLimit.value != "") {

	//    if (ctlUnderClaimIncLimit.value == "No") {
	        //sachin 7810 starts
    //if (ctlHoldStatus != null && ctlHoldStatus.value != "" && ctlHoldStatus.value != "New" && ctlUnderLimit.value != "" && ctlUnderClaimIncLimit.value!="" && ctlUnderLimit.value != "Approve" && ctlUnderClaimIncLimit.value!="Approve")
	if (ctlHoldStatus != null && ctlHoldStatus.value != "" && ctlHoldStatus.value != "New" && (ctlUnderLimit.value == "No" || ctlUnderClaimIncLimit.value == "No") && ctlUnderLimit.value != "Approve" && ctlUnderClaimIncLimit.value != "Approve")
	        {
	            alert("The worksheet is being sent to your supervisor for approval as " + ctlHoldStatus.value + ".");
	        }
	        //sachin 7810 ends
	        //parent.MDIRefreshCurrentNode();

	        //ctlUnderClaimIncLimit.value = "";
	        ctlHoldStatusvalue = "";
	    //}
	//}
    //Snehal 6385 ends
   
    //By Nitin on 06-Nov-2009
    //if there is no any ReserveType for a current claim then show ReserveType Tab
	if (ctrlReserveTypeTabs != null) 
	{
	    if (ctrlReserveTypeTabs.value == '')
	        CreateReserveSummary();
	}
	
	return true;
}

function SaveData() {

    var ret = false;
    var bPreModResToZeroSet = false;
	var bIsValidated = false;
	var ctrlstatus = document.getElementById('hdnRSWStatus'); // Get the status of the worksheet open.
	if (ctrlstatus != null)
     {
	    if (document.getElementById('RswTypeName') != null) {
	        var sRSWType = document.getElementById('RswTypeName').value;
	        if (sRSWType == "Customize")
	         {
	            alert("A Reserve Worksheet with the status '" + ctrlstatus.value + "' can not be changed and saved.");
	            return false;
	        }

	    } 
	}


	if (!m_DataChanged)
	    return false;

	
	if (ctrlstatus != null)
	 {//shobhana
	     
	    bIsValidated = ValidateReserveAmtBeforeSave();
	 //  debugger;
//	    if (sReserveAmount <= 0 && document.forms[0].PrevResModifyzero.value == "True") {
//	        if (!confirm("The Reserve amount should be greater than $0.00. Do you really want to modify this reserve to $0.00?"))
//	            return false;
//	    }
    	if (bIsValidated  == false)
    	{
	        alert("No reserve amounts have been entered or reserves have not changed as compared to existing reserves for all reserve categories. This reserve worksheet cannot be saved.");
	        return false;
	    }
	    else 
	    {
	        if (!ValidateReasonsAndComments()) 
	        {
	            return false;
	        }
	    }
	    //skhare7 Mits 19027
	    if (!ValidatePreModResTOZeroSetting())
         {
	        return false;

	    }

	 

	
		if (ctrlstatus.value == "New" || ctrlstatus.value == "Pending" || ctrlstatus.value == "Rejected")
		{
		    pleaseWait.Show();
		    m_bConfirmSave = false;
		    return true;
		}
		else
		{
			alert("A Reserve Worksheet with the status '" + ctrlstatus.value + "' can not be changed and saved.");
			return false;
		}
	}

	return false;
}

//Added by Nitin for Validation purpose
function ValidateReserveAmtBeforeSave() {
   var hdnCtrlToClear = document.getElementById('hdnCtrlToEdit');
   var bValidated = false;
   var arrCtrls = null;
   var txtCommentsControl = null;
   var hdnCommentsControl = null;

   StoreReserveTypeComments();

   if (hdnCtrlToClear != null) 
   {
        arrCtrls = hdnCtrlToClear.value.split(",");

        for (var count = 0; count < arrCtrls.length; count++)
        {
            if (arrCtrls[count].indexOf("TransTypeIncurredAmount") != -1) 
            {
                transTypeControl = document.getElementById(arrCtrls[count]);
                if (transTypeControl != null) 
                {
                    //if( (transTypeControl.value == '') || transTypeControl.value == '$0.00') 
                    RSW_MultiCurrencyToDecimal(transTypeControl);   //Aman Multi Currency
                    if (transTypeControl.getAttribute("Amount") == "0" || transTypeControl.getAttribute("Amount") == "0.00")//Add one more condition by kuladeep for MITS:27294
                    {
                        bValidated = false;
                    }
                    else
                    {
                        bValidated = true;
                        return bValidated;
                    }
                }
            }
        }
    }
    return bValidated;
}


//skhare7 Mits 19027

function ValidatePreModResTOZeroSetting() {
    var hdnCtrlToClear = document.getElementById('hdnCtrlToClear');
     var key = null;
     var arrReserve = null;
     var arrFlag = null;
     var arrCtrl = null;
    var txtCommentsControl = null;
    var hdnCommentsControl = null;
    var sReserveAmount;
    var bValidated = false;
   
   
  //  debugger;

   
        if (hdnCtrlToClear != null)
         {
              arrCtrl = hdnCtrlToClear.value.split(",");
              for (var i = 0; i < m_TabList.length; i++)
               {
                  key = m_TabList[i].id.substring(4);

                  transTypeControl = document.getElementById('txt_' + key + '_NEWTOTALINCURRED')
                  if (transTypeControl != null)
                   {
                  for (var count = 0; count < arrCtrl.length; count++)
                   {
                      if (transTypeControl.id == arrCtrl[count]) {
                          //Aman Multi Currency
                          RSW_MultiCurrencyToDecimal(transTypeControl);
                          sReserveAmount = transTypeControl.getAttribute("Amount");
                          //sReserveAmount = (transTypeControl.value).replace(/[,\$]/g, "");
                          //if (transTypeControl.value == '') {
                          //    sReserveAmount = 0;
                          //}
                          if (transTypeControl.getAttribute("Amount") == "0") {
                              sReserveAmount = 0;
                          }
                          else {

                              sReserveAmount = parseFloat(sReserveAmount);
                          }
                          
                              if (document.getElementById('hdnPrevModValtoZero') != null)
                               {


                                  if (sReserveAmount <= 0 && document.getElementById('hdnPrevModValtoZero').value == "True")
                                  {
                                      tabChange(key);
                                      if (!confirm("The Reserve amount should be greater than 0. Do you really want to modify this reserve to 0?")) {  //  arrFlag
                                          bValidated = false;
                                          return bValidated;
                                      }
                                  }



                              }


                          }
                      }
                  } 
              }
             
           }
           bValidated = true;
    return bValidated;
}
//end



function ValidateReasonsAndComments() 
{
    var arrCtrls = null;
    var reserveTypeCommentControl = null;
    var rserverSummaryComment = null;
    var bValidated = null;
    
    
    //nnorouzi; MITS 18989 and cleaning up the code start    
    var key = null;
    var controlToBeChecked = null;
    var reasonCommentsAlert = "Please fill in the Reason and/or Comments!"
    var reasonCommentsRequired;


    reasonCommentsRequired = document.getElementById('hdn_ReasonCommentsRequired');
    if (String(true) == reasonCommentsRequired.value.toLowerCase()) {
        for (var i = 0; i < m_TabList.length; i++) {
            key = m_TabList[i].id.substring(4);
            if (m_arrReasonsToBeChecked[key]) {
                controlToBeChecked = document.getElementById('txt_' + key + '_ReserveWorksheetReason_codelookup');
                if (controlToBeChecked && controlToBeChecked.value == '') {
                    tabChange(key);
                    alert(reasonCommentsAlert);
                    return false;
                }
            }
            if (m_arrCommentsToBeChecked[key]) {
                controlToBeChecked = document.getElementById('txt_' + key + '_Comments');
                if (controlToBeChecked && controlToBeChecked.value == '') {
                    tabChange(key);
                    alert(reasonCommentsAlert);
                    return false;
                }
            }
            if (i == m_TabList.length - 1) {
                controlToBeChecked = document.getElementById('txt_RESERVESUMMARY_Comments');
                if (controlToBeChecked && controlToBeChecked.value == '') {
                    if (!m_ReserveSummaryCreated) {
                        CreateReserveSummary();
                    }
                    tabChange(key);
                    alert(reasonCommentsAlert);
                    return false;
                }
            }
        }
    }
    //nnorouzi; MITS 18989 and cleaning up the code end    
    
    return true;
}


//Added by Nitin for R6 inorder to restore Comments value in hiddenfields
function StoreReserveTypeComments() 
{
    var hdnCtrlToClear = document.getElementById('hdnCtrlToClear');
    var arrCtrls = null;
    var txtCommentsControl = null;
    var hdnCommentsControl = null;

    if (hdnCtrlToClear != null) 
    {
        arrCtrls = hdnCtrlToClear.value.split(",");

        for (var count = 0; count < arrCtrls.length; count++) 
        {
            //added in order to populate hidden fields of comments
            if (arrCtrls[count].indexOf("Comments") != -1) 
            {
                txtCommentsControl = document.getElementById(arrCtrls[count]);
                if (txtCommentsControl != null) 
                {
                    if (arrCtrls[count].substring(0, 3) == 'txt') 
                    {
                        hdnCommentsControl = document.getElementById("hdn" + arrCtrls[count].substring(3));
                        if (hdnCommentsControl != null) 
                        {
                            hdnCommentsControl.value = txtCommentsControl.value;
                        }
                    }
                }
                txtCommentsControl = null;
                hdnCommentsControl = null;
            }
        }
    }
}


//Added by Nitin for R6
function CreateReserveSummary() {
    var tab = null;
    var root = null;
    var tbo = null;
    var row = null;
    var cell = null;
    var arrCtrls = null;
    var hdnReserveTypeTabs = null;
    var txtReserveTypePaid = null;
    var sReserveCategoryDesc;
    var txtReserveTypePrevOutStanding = null;
    var hdnReserveTypeNewOutStanding = null;
    var hdnReserveTypeNewIncurred = null;
    var hdnReserveTypeReserveChange = null;
    var txtReserveTypeComments = null;
    var sReserveTypeComments = "";
    var txtReserveSummaryComments = null;

    var dTotalPaidAmt = 0.0;
    var dTotalPrevOutStandingAmt = 0.0;
    var dTotalNewOutStandingAmt = 0.0;
    var dTotalNewIncurredAmt = 0.0;
    var dTotalReserveChangeAmt = 0.0;
    //akaur9 Multi Currency --Start
    var sum = 0;
    var amount = 0;

    root = document.getElementById('divReserveSummary');
    root.innerHTML = "";
    tab = document.createElement('table');
    tab.border = "0";
    tab.cellPadding = "0";
    tab.cellSpacing = "0";
    tab.className = "divScroll";
    tbo = document.createElement('tbody');
    

    //creating header and its cells for Reserve summary table
    row = document.createElement('tr');
    row.className = "colheader6";

    //cell 0
    cell = document.createElement('td');
    cell.style.color = "White";
    cell.appendChild(document.createTextNode(' Reserve Category '));
    row.appendChild(cell);

    //cell1
    cell = document.createElement('td');
    cell.style.color = "White";
    cell.appendChild(document.createTextNode(' Paid '));
    row.appendChild(cell);

    //cell2
    cell = document.createElement('td');
    cell.style.color = "White";
    cell.appendChild(document.createTextNode(' Previous Reserve Balance '));
    row.appendChild(cell);

    //cell3
    cell = document.createElement('td');
    cell.style.color = "White";
    cell.appendChild(document.createTextNode(' New Reserve Balance '));
    row.appendChild(cell);

    //cell4
    cell = document.createElement('td');
    cell.style.color = "White";
    cell.appendChild(document.createTextNode(' New Total Reserve '));
    row.appendChild(cell);

    //cell5
    cell = document.createElement('td');
    cell.style.color = "White";
    cell.appendChild(document.createTextNode('Reserve Change'));
    row.appendChild(cell);

    //adding this header row to table body
    tbo.appendChild(row);
    
    hdnReserveTypeTabs = document.getElementById('hdnReserveTypeTabs');
    if (hdnReserveTypeTabs != null) 
    {
        arrCtrls = hdnReserveTypeTabs.value.split(",");
    }
    else 
    {
        return false;
    }
    
   //creating table data details

    for (var count = 0; count < arrCtrls.length-1; count++) 
    {
        txtReserveTypePaid = document.getElementById('txt_' + arrCtrls[count] + '_Paid');
        txtReserveTypePrevOutStanding = document.getElementById('txt_' + arrCtrls[count] + '_Outstanding');
        hdnReserveTypeNewOutStanding = document.getElementById('hdn_' + arrCtrls[count] + '_NEWOutstanding');
        hdnReserveTypeNewIncurred = document.getElementById('hdn_' + arrCtrls[count] + '_NEWTOTALINCURRED');
        hdnReserveTypeReserveChange = document.getElementById('hdn_' + arrCtrls[count] + '_ReserveChange');
        sReserveCategoryDesc = document.getElementById('TABS' + arrCtrls[count]);

        txtReserveTypeComments = document.getElementById('txt_' + arrCtrls[count] + '_Comments');
        if (txtReserveTypePaid != null) {
            RSW_MultiCurrencyToDecimal(txtReserveTypePaid);
        }
        if (txtReserveTypePrevOutStanding != null) {
            RSW_MultiCurrencyToDecimal(txtReserveTypePrevOutStanding);
        }
       
        amount = txtReserveTypePaid.getAttribute("Amount");
        dTotalPaidAmt = Number(dTotalPaidAmt) + Number(amount);
        dTotalPrevOutStandingAmt = Number(dTotalPrevOutStandingAmt) + Number(txtReserveTypePrevOutStanding.getAttribute("Amount"));
        dTotalNewOutStandingAmt = Number(dTotalNewOutStandingAmt) + Number(hdnReserveTypeNewOutStanding.value);
        dTotalNewIncurredAmt = Number(dTotalNewIncurredAmt) + Number(hdnReserveTypeNewIncurred.value);
        //dTotalPaidAmt += ConvertStrToDbl(txtReserveTypePaid.value);
        //dTotalPrevOutStandingAmt += ConvertStrToDbl(txtReserveTypePrevOutStanding.value);
        //dTotalNewOutStandingAmt += ConvertStrToDbl(hdnReserveTypeNewOutStanding.value);
        //dTotalNewIncurredAmt += ConvertStrToDbl(hdnReserveTypeNewIncurred.value);

        if (hdnReserveTypeReserveChange.value == '') 
        {
            hdnReserveTypeReserveChange.value = 'No Change';
        }
        
        if (hdnReserveTypeReserveChange.value != 'No Change') 
        {
            //dTotalReserveChangeAmt.value += ConvertStrToDbl(hdnReserveTypeReserveChange.value);
            dTotalReserveChangeAmt = Number(dTotalReserveChangeAmt) + Number(hdnReserveTypeReserveChange.value);
        }

        row = document.createElement('tr');
       
        //cell 0
        cell = document.createElement('td');
        cell.className = "data";
        cell.appendChild(document.createTextNode(sReserveCategoryDesc.innerText));
        row.appendChild(cell);
        
        //cell 1
        cell = document.createElement('td');
        cell.className = "data";
        cell.appendChild(document.createTextNode(txtReserveTypePaid.value));
        row.appendChild(cell);

        //cell 2
        cell = document.createElement('td');
        cell.className = "data";
        cell.appendChild(document.createTextNode(txtReserveTypePrevOutStanding.value));
        row.appendChild(cell);

        //cell 3
        cell = document.createElement('td');
        cell.className = "data";
        //Aman -- Multi Currency Control is being passed to get the type of function to call for the hidden controls
        amount = hdnReserveTypeNewOutStanding.value;
        RSW_HiddentoMultiCurrencyType(txtReserveTypePaid, hdnReserveTypeNewOutStanding);
        cell.appendChild(document.createTextNode(hdnReserveTypeNewOutStanding.value));
        row.appendChild(cell);
        hdnReserveTypeNewOutStanding.value = amount;

        //cell 4
        cell = document.createElement('td');
        cell.className = "data";
        //Aman -- Multi Currency Control is being passed to get the type of function to call for the hidden controls
        amount = hdnReserveTypeNewIncurred.value;
        RSW_HiddentoMultiCurrencyType(txtReserveTypePaid, hdnReserveTypeNewIncurred);
        cell.appendChild(document.createTextNode(hdnReserveTypeNewIncurred.value));
        row.appendChild(cell);
        hdnReserveTypeNewIncurred.value = amount;

        //cell 5
        cell = document.createElement('td');
        cell.className = "data";
        //Aman -- Multi Currency Control is being passed to get the type of function to call for the hidden controls
        amount = hdnReserveTypeReserveChange.value;
        RSW_HiddentoMultiCurrencyType(txtReserveTypePaid, hdnReserveTypeReserveChange);
        cell.appendChild(document.createTextNode(hdnReserveTypeReserveChange.value));
        row.appendChild(cell);
        hdnReserveTypeReserveChange.value = amount;
        tbo.appendChild(row);
    }
    
    tab.appendChild(tbo);
    root.appendChild(tab);

    //Creating Row showing totals
    
    row = document.createElement('tr');
    row.className = "colheader6";

    //cell 0
    cell = document.createElement('td');
    cell.style.color = "White";
    cell.appendChild(document.createTextNode('SUMMARY TOTALS'));
    row.appendChild(cell);

    //cell1
    cell = document.createElement('td');
    cell.style.color = "White";
    //cell.appendChild(document.createTextNode(ConvertDblToCurr(dTotalPaidAmt)));
    if (hdnReserveTypeReserveChange != null)//MITS 31574 srajindersin 02/15/2013
    {
    var temp = hdnReserveTypeReserveChange.value;

    hdnReserveTypeReserveChange.value = dTotalPaidAmt;
    RSW_HiddentoMultiCurrencyType(txtReserveTypePaid, hdnReserveTypeReserveChange);
    cell.appendChild(document.createTextNode(hdnReserveTypeReserveChange.value));
    row.appendChild(cell);
    

    //cell2
    cell = document.createElement('td');
    cell.style.color = "White";
    hdnReserveTypeReserveChange.value = dTotalPrevOutStandingAmt;
    RSW_HiddentoMultiCurrencyType(txtReserveTypePaid, hdnReserveTypeReserveChange);
    cell.appendChild(document.createTextNode(hdnReserveTypeReserveChange.value));
    row.appendChild(cell);

    //cell3
    cell = document.createElement('td');
    cell.style.color = "White";
    hdnReserveTypeReserveChange.value = dTotalNewOutStandingAmt;
    RSW_HiddentoMultiCurrencyType(txtReserveTypePaid, hdnReserveTypeReserveChange);
    cell.appendChild(document.createTextNode(hdnReserveTypeReserveChange.value))
    //cell.appendChild(document.createTextNode(ConvertDblToCurr(dTotalNewOutStandingAmt)));
    row.appendChild(cell);

    //cell4
    cell = document.createElement('td');
    cell.style.color = "White";
    hdnReserveTypeReserveChange.value = dTotalNewIncurredAmt;
    RSW_HiddentoMultiCurrencyType(txtReserveTypePaid, hdnReserveTypeReserveChange);
    cell.appendChild(document.createTextNode(hdnReserveTypeReserveChange.value));
    //cell.appendChild(document.createTextNode(ConvertDblToCurr(dTotalNewIncurredAmt)));
    row.appendChild(cell);

    //cell5
    cell = document.createElement('td');
    cell.style.color = "White";
    hdnReserveTypeReserveChange.value = dTotalReserveChangeAmt;
    RSW_HiddentoMultiCurrencyType(txtReserveTypePaid, hdnReserveTypeReserveChange);
    cell.appendChild(document.createTextNode(hdnReserveTypeReserveChange.value));
    //cell.appendChild(document.createTextNode(ConvertDblToCurr(dTotalReserveChangeAmt)));
    row.appendChild(cell);

    //adding this header row to table body
    tbo.appendChild(row);
    root.appendChild(tab);
    m_ReserveSummaryCreated = true;

    hdnReserveTypeReserveChange.value = temp; //Aman ---End
    }
    return false;
}

function GetSelectedRSWid()
{
    inputs = document.getElementsByTagName("input");
	for (i = 0; i < inputs.length; i++)
	{
		if ((inputs[i].type=="radio") && (inputs[i].checked==true))
		{
			inputid=inputs[i].value;
			return inputid;
		}
	}
}


//Changed by Nitin for R6 on 10-Sept-2009
function DeleteWorksheet()
{
    var ret = false;
	var ctrl = document.getElementById('hdnRSWStatus'); // Get the status of the worksheet open.

	if (ctrl != null)
	{//shobhana
	    if (document.getElementById('RswTypeName') != null) 
	    {
	        var sRSWType = document.getElementById('RswTypeName').value;
	        if (sRSWType == "Customize")
	         {
	             alert("A Reserve Worksheet with the status approved can not be changed and saved.");
	            return false;
	        }

	    }

		if (ctrl.value == "New")
		{
			ret = confirm("This worksheet has not been saved yet. Do you want to clear the data?");
			if (ret)
				ClearData();
			return false;
		}	
		else if (ctrl.value == "Pending" || ctrl.value == "Rejected" || ctrl.value == "Rejected (Edit Mode)") // Alert the user and then delete the worksheet
		{
			return DeleteRecord();
		}
		else
		{
			alert("A Reserve Worksheet with the status '" + ctrl.value + "' can not be deleted.");
			return false;
		}
	}

	return false;
}


//Changed by Nitin for R6 on 10th Sept 2009
function ClearData()
{
	var ctrl;
	var ClearControls = document.getElementById('hdnCtrlToClear');
		
	var ctrlStatus = document.getElementById('hdnRSWStatus');
	if (ctrlStatus != null)
	{
		if (ctrlStatus.value == "New" || ctrlStatus.value == "Pending" || ctrlStatus.value == "Rejected")
		{
			var ret = confirm("Are you sure, you want to clear the worksheet?");
			if (!ret)
				return false;
			
			if (ClearControls != null)
			{
				var arrCtrls = ClearControls.value.split(",");
				for (var count = 0; count < arrCtrls.length; count ++)
				{
				    //Clearing codlookup i,e reason control 
				    if (arrCtrls[count].indexOf("Reason") != -1) 
				    {
				        ctrl = document.getElementById(arrCtrls[count] + "_codelookup");
				        if (ctrl != null)
				            ctrl.value = "";
				    }
					
					ctrl = document.getElementById(arrCtrls[count]);
					if (ctrl != null)
					{
						switch (ctrl.type)
						{
							case 'textarea':
							case 'text':
							case 'select-one':
								ctrl.value = "";
								break;
							case 'checkbox':
								ctrl.checked = false;
								break;
							default:
								ctrl.value = "";
								break;
                        } // end of switch case
                    }
                } // end of for loop
			}
		}
		else
		{
			alert("Cannot Clear Data when the Worksheet status is '" + ctrlStatus.value + "'.");
		}
	}	
	return false;
}

//Changed by Nitin on 08-Sept
function SubmitSheet()
{
    var ctrlStatus = document.getElementById('hdnRSWStatus');
    var bIsValidated = false;
    
    //smahajan6m
	if(m_DisableSubmit)
	{
		alert("A Reserve Worksheet has already been submitted.");
		return false;
	}
	//smahajan6m

	if (ctrlStatus != null) 
	{//shobhana

	    if (document.getElementById('RswTypeName') != null)
	     {
	        var sRSWType = document.getElementById('RswTypeName').value;
	        if (sRSWType == "Customize") {
	            alert("A Reserve Worksheet with the status approved can not be changed and saved.");
	            return false;
	        }

	    }

	
		//A pending approval worksheet can be resubmitted by the manager.
		//In this case no validation is required since its already validated and furthermore the fields are readonly.
		//The need is only to re-submit the same to that it goes to his manager further

	    bIsValidated = ValidateReserveAmtBeforeSave();

	    if (bIsValidated == false)
         {
	        alert("No reserve amounts have been entered or reserves have not changed in this worksheet as compared to existing reserves for all reserve categories. This worksheet cannot be submitted for approval.");
	        return false;
	    }
	    else 
	    {
            if (!ValidateReasonsAndComments()) 
	        {
	            return false;
	        }
	    }
	   // debugger;
	    //skhare7 MITS 19027
	    if (!ValidatePreModResTOZeroSetting()) 
        {
            return false;
        
        }

	  
         
        if (document.getElementById('hdnCurrentUser').value == document.getElementById('hdnSubmittedTo').value)
		{
		        m_DisableSubmit = true; //smahajan6m
		        pleaseWait.Show();
			    return true;  //submit
        }
                                             //commented by Nitin for Mits 18482                                   
        if (ctrlStatus.value == "Pending")   //|| ctrlStatus.value == "Pending Approval") // submit only when the status is pending/ || ctrlStatus.value == "Rejected (Edit Mode)"
		{
		    m_DisableSubmit = true; //smahajan6m
		    m_bConfirmSave = false;
		    pleaseWait.Show();
		    return true;
        }
		else
			alert("A Reserve Worksheet with the status '" + ctrlStatus.value + "' can not be submitted.");
	}
    return false;	
}

// Called from the main Reserve Worksheet, rather than the Approval Screen
function Approve() {//MGaba2:R6 Reserve WorkSheet
    var ctrlStatus = document.getElementById('hdnRSWStatus');
    
	if (ctrlStatus != null) 
	{
//shobhana
	    if (document.getElementById('RswTypeName')!= null) {
	        var sRSWType = document.getElementById('RswTypeName').value;
	        if (sRSWType == "Customize") {
	            alert("A Reserve Worksheet with the status approved can not be changed and saved.");
	            return false;
	        }

	    }

		//Submit only when the status is Pending Approval
		
    	bIsValidated = ValidateReserveAmtBeforeSave();
    		    
    	if (bIsValidated  == false)
    	{
	        {
            	alert("No reserve amounts have been entered or reserves have not changed in this worksheet as compared to existing reserves for all reserve categories. This worksheet cannot be approved. Please 'Reject' this worksheet and select this rejected worksheet from the left pane and either (a) continue editing it and then submit it for approval; or (b) delete it, and then create a new worksheet.");
		       	return false;
	        }
	    }
	    else 
	    {
	        if (!ValidateReasonsAndComments()) 
	        {
	            return false;
	        }
	    }
//	    if (!ValidatePreModResTOZeroSetting()) {
//	        return false;

//	    }

	   

		if (ctrlStatus.value == "Pending Approval" || ctrlStatus.value == "Pending") 
		{
		    //document.getElementById('RefreshLeftTree').value = "Y";
		    m_bConfirmSave = false;
		    pleaseWait.Show();
		    //commented by Nitin for Mits 18882
		    //parent.MDISetWipedOut();
		    return true;
		}
		else
		{
		    alert("A Reserve Worksheet with the status '" + ctrlStatus.value + "' can not be approved.");
		    return false;
		}
    }
	
	return false;
}

function trimAll(sString) 
{
	while (sString.substring(0,1) == ' ')
	{
		sString = sString.substring(1, sString.length);
	}
	while (sString.substring(sString.length-1, sString.length) == ' ')
	{
		sString = sString.substring(0,sString.length-1);
	}
	return sString;
}

function Reject()
{//MGaba2:R6:Changed the function
	var ctrlStatus = document.getElementById('hdnRSWStatus');
	if (ctrlStatus != null)
	{
	    if (document.getElementById('RswTypeName') != null)
	     {
	        var sRSWType = document.getElementById('RswTypeName').value;
	        if (sRSWType == "Customize")
	         {
	             alert("A Reserve Worksheet with the status approved can not be changed and saved.");
	            return false;
	        }

	    }

		
		if (ctrlStatus.value == "Pending Approval") // reject only when the status is pending approval
		{
		    //ShowPlsWait();
		    pleaseWait.Show();
			return true;
		}
		else
			alert("A Reserve Worksheet with the status '" + ctrlStatus.value + "' can not be rejected.");
	}
	
	return false;
}

//By Vaibhav for Safeway reserve worksheet
function ApproveFromApprovalScreen()
{
    var bNoRecord= false;
    var bRadio = document.getElementsByTagName("input");
        
        for( var i = 0 ; i < bRadio.length - 1 ; i++ )
          {
              if( bRadio[i].type == "radio" )
              {
	              bNoRecord = true;;
	              break;
              }
         }


	      if (!bNoRecord) 
          {
          //Mgaba2: R8:Supervisory Approval
              var ScreenName = document.getElementById("hdnScreenName");

              alert('Only an existing ' + ScreenName.value + ' can be Approved.');
                return false;
    }
    //By Vaibhav for Safeway MITS # 13938
    return checkCharacterLength();
}

function RejectFromApprovalScreen()
{
    var bNoRecord= false;
    var bRadio = document.getElementsByTagName("input");
        
        for( var i = 0 ; i < bRadio.length - 1 ; i++ )
          {
              if( bRadio[i].type == "radio" )
              {
	              bNoRecord = true;;
	              break;
              }
         }
         
         
    if(!bNoRecord)
     {
        //Mgaba2: R8:Supervisory Approval
        var ScreenName = document.getElementById("hdnScreenName");
        alert('Only an existing ' + ScreenName.value + ' can be Rejected.');
        return false;
    }
    //By Vaibhav for Safeway MITS # 13938
    return checkCharacterLength();
}

//By Vaibhav for Safeway MITS # 13938 : start
function checkCharacterLength()
{
    var maxLength = 255;
    if(document.getElementById("txtAppRejReqCom").value.length > maxLength)
    {
        alert('You cannot enter more than 255 characters in Reason field.');
        return false;
    }
}

function EditWorkSheet()
{

    var iClaimId = "-1";
    var bNoRecord= false;
    var bRadio = document.getElementsByTagName("input");
        
        for( var i = 0 ; i < bRadio.length - 1 ; i++ )
          {
              if( bRadio[i].type == "radio" )
              {
	              bNoRecord = true;;
	              break;
              }
         }
         
         
    if(!bNoRecord)
    {
        alert('Only an existing Reserve Worksheet can be Opened.');
        return false;
    } 
      
    iClaimId = document.getElementById('claimid').value;
    iRSWId = document.getElementById('hdnRswId').value;
	    
    if (iClaimId == "-1")
	    alert("Only an existing Reserve Worksheet can be Opened.");
    else
    {
        if (iRSWId == "-1")
        {
		    m_EditMode = true;
            window.open('home?pg=riskmaster/Reserves/ReserveWorksheet/ReserveWorksheet&amp;RSWType=CRW&amp;claimid='+iClaimId,'ReserveWorksheet',
		    'width=950,height=700'+',top='+(screen.availHeight-700)/2+',left='+(screen.availWidth-950)/2+',resizable=no,scrollbars=yes');
		}
	    else
	    {
			m_EditMode = true;
            window.open('home?pg=riskmaster/Reserves/ReserveWorksheet/ReserveWorksheet&amp;RSWType=CRW&amp;claimid='+iClaimId+'&amp;rswid='+iRSWId,'ReserveWorksheet',
		    'width=950,height=700'+',top='+(screen.availHeight-700)/2+',left='+(screen.availWidth-950)/2+',resizable=no,scrollbars=yes');
	    }    
    }
    
   return false;
}

function CheckParentScreen()
{
    //get the current URL 
    var url = window.location.toString();
    //get the parameters 
    url.match(/\?(.+)$/); 
    var params = RegExp.$1; 
    //split up the query string and store in an associative array 
    var params = params.split("&"); 
    var queryStringList = {};  
    for(var i=0;i<params.length;i++) 
    {     
        var tmp = params[i].split("=");     
        queryStringList[tmp[0]] = unescape(tmp[1]);
        if (unescape(tmp[0]) == "rswid")
        {
            if (unescape(tmp[1]) != "")
                self.parent.frames["MainFrame"].location.href = 'home?pg=riskmaster/Reserves/ReserveWorksheet/MainFrame&rswid='+unescape(tmp[1]);
                
        }
    }  
}

function PrintWorksheet()
{
        var iClaimId = 0;
    var iRSWId = "-1";
    var rswtypename = "";
    //debugger;
//*******To do *****************
//Mgaba2:Commenting it for the time being
    var sRSWCustomXML = "";
//    
//    //By Vaibhav for Reserve worksheet : Added if condition
//    if(document.title== "Reserve Approval")
//    {
//        iRSWId = document.getElementById('hdnRswId').value;
//		    
//	    if (iRSWId == "-1")
//		    alert("Only an existing Reserve Worksheet can be Printed.");
//	    else
//	    {
//		    var sQueryString = "rswid="+ iRSWId;
//		    window.open('/RiskmasterUI/UI/ReserveWorksheet/PrintWorksheet.aspx&amp;' + sQueryString, 'PrintRSW',
//			    'width=650,height=550,top='+(screen.availHeight-550)/2+',left='+(screen.availWidth-650)/2+',resizable=yes,scrollbars=yes');
//	    }
//    }
//    //Vaibhav code end
//    else
//    {
//	    var ctrlStatus = self.parent.frames["MainFrame"].document.getElementById('hdnRSWStatus');	

    //MGaba2:MITS 22225:Print worksheet was not working from Reserve Approval Screen
    //Adding null check as this control is not present on Reserve Approval Screen    
    if (document.getElementById('hdnCustomTransXML') != null) {
        sRSWCustomXML = document.getElementById('hdnCustomTransXML').value;
    }

    //	    {

    //MGaba2:MITS 22225:Print worksheet was not working from Reserve Approval Screen
    //This control is not present on Reserve Approval Screen    
    if (document.getElementById('RswTypeName') != null) {
        rswtypename = document.getElementById('RswTypeName').value;
    }
    else {
        rswtypename = "";
    }

    //MGaba2:MITS 22225:Print worksheet was not working from Reserve Approval Screen
    //if (document.getElementById('hdnRSWid') != null && document.getElementById('RswTypeName') != null)
    //Ashish Ahuja - RMA 9672 & 9409
    if (document.getElementById('hdnRswId') != null)
    {
        iRSWId = document.getElementById('hdnRswId').value;
    }
    else if (document.getElementById('rswid') != null)
    {
        iRSWId = document.getElementById('rswid').value;
    }

    if (document.getElementById('hdnRswId') != null || document.getElementById('rswid') != null)
    //if (document.getElementById('rswid') != null) //RMA-8751 Fixed nshah28(replace 'hdnRswId' with 'rswid')
    {
        //iRSWId = document.getElementById('hdnRswId').value;
        //iRSWId = document.getElementById('rswid').value; //RMA-8751 Fixed nshah28


        //MGaba2:MITS 22225:Value of RswTypeName will be considered only if reserve worksheet is open through claim
		  //rswtypename = document.getElementById('RswTypeName').value;
//		  if (document.getElementById('RswTypeName') != null) 
//		  {
//		      var sRSWType = document.getElementById('RswTypeName').value;
//		      if (sRSWType == "Customize") {
//		         
//		          return false;
//		      }

        //		  }
 	//MGaba2:MITS 22225:ClaimId is sent in case RSW is opened from reserve approval screen
        if (document.getElementById('claimid') != null) {
            iClaimId = document.getElementById('claimid').value;
        }
        //RMA-10171 achouhan3   issue fiz for alert message on multi select while print Starts
        if (iRSWId == "-1")
            //alert("Only an existing Reserve Worksheet can be printed. Please save the Worksheet in order to print it.");
            alert(CreateReserveValidations.ValidReserveWorksheet);
        //RMA-5566  achouhan3   Added to check for only one record to print 
        //RMA-10213  achouhan3   Multiple Print worksheet 
        //else if (isNaN(iRSWId))
        //    alert(CreateReserveValidations.ValidOnlyOneReserveWorksheet);
        else { //MGaba2:MITS 22225:Start
            //In case RSW is opened from Reserve Approval Screen,claimId should be sent through Connnectionstring
            //and rswtypename should be sent as blank
//            var sQueryString = "rswid=" + iRSWId;
            var sUrl = '/RiskmasterUI/UI/ReserveWorksheet/PrintWorksheet.aspx?rswid=' + iRSWId + '&RSWCustomXML=' + sRSWCustomXML + '&RswTypeName=' + rswtypename;
            if (iClaimId != 0) {
                sUrl += '&ClaimId=' + iClaimId;
            }
            window.open(sUrl, 'PrintRSW',
		    'width=650,height=550,top=' + (screen.availHeight - 550) / 2 + ',left=' + (screen.availWidth - 650) / 2 + ',resizable=yes,scrollbars=yes');
//            window.open('/RiskmasterUI/UI/ReserveWorksheet/PrintWorksheet.aspx?' + sQueryString + '&RSWCustomXML=' + sRSWCustomXML + '&RswTypeName=' + rswtypename, 'PrintRSW',
//		    'width=650,height=550,top=' + (screen.availHeight - 550) / 2 + ',left=' + (screen.availWidth - 650) / 2 + ',resizable=yes,scrollbars=yes');
            //MGaba2:MITS 22225:End
        }
	}
  
    return false;
}

function Tree_Load()
{
var scrType="";
var oCtrl = document.getElementById('selectedID');
if(oCtrl!=null)
	oldNodId= oCtrl.value;
else
	oldNodId="";
	
oCtrl = document.getElementById('scr');	
if(oCtrl.value=='ra')
	{
		oCtrl = document.getElementById('RSWType');
		switch(oCtrl.value)
		{
			case "CRW":
				scrType="clm";
				break;
		}
		OpenWorkSheet(oldNodId,scrType);
	}
}

/*function onSaveMedReserve()//Mohit
{
	var NewMedRes = document.getElementById('NewMedResAmt').value;
	var ival = document.getElementById('selectedrowid').value;
	var hdnList = window.opener.document.getElementById('hdnMedList').value;
	var before = hdnList.substring(0, hdnList.indexOf(";" + ival + "|") + 1);
	var after = "";
	var list = "";
	
	var MedInc = window.opener.document.getElementById('hdnMedIncurred').value;
	var BalMedRes = window.opener.document.getElementById('MedBalAmt').value;
	
	var TotNewAmt = 0.0;
	
	if (hdnList.indexOf(';', hdnList.indexOf(";" + ival + "|")+1) != -1)
		after = hdnList.substring(hdnList.indexOf(';', hdnList.indexOf(";" + ival + "|")+1));
		
	list = ival + "|" + ConvertDblToCurr(ConvertStrToDbl(NewMedRes));
	window.opener.document.getElementById('hdnMedList').value = before+list+after;
	ival = parseInt(ival) + 1;
	window.opener.document.getElementById('MedList|'+ ival +'|NewMedResAmt|TD|').innerText = ConvertDblToCurr(ConvertStrToDbl(NewMedRes));

	for (var i = 2; i < 13; i ++)	
	{
		TotNewAmt += ConvertStrToDbl(window.opener.document.getElementById('MedList|'+ i +'|NewMedResAmt|TD|').innerText);
	}
	window.opener.document.getElementById('NewMedInc').innerText = ConvertDblToCurr(ConvertStrToDbl(MedInc) + TotNewAmt);
	window.opener.document.getElementById('NewMedBalAmt').innerText = ConvertDblToCurr(ConvertStrToDbl(BalMedRes) + TotNewAmt);
	// Populate values on 'Reserve Summary' tab
	window.opener.CalcResAna();
	window.close();	
	
	return false;
}*/

/*function onSaveIndReserve()//Mohit
{
	var NewIndRes = document.getElementById('NewIndResAmt').value;
	var IndWeeks = document.getElementById('IndWeeks').value;
	var IndRates = document.getElementById('IndRate').value;
	var ival = document.getElementById('selectedrowid').value;
	var hdnList = window.opener.document.getElementById('hdnIndList').value;
	var before = hdnList.substring(0, hdnList.indexOf(";" + ival + "|") + 1);
	var after = "";
	var list = "";
	
	var IndInc = window.opener.document.getElementById('hdnIndIncurred').value;
	var BalIndRes = window.opener.document.getElementById('IndBalAmt').value;
	
	var TotNewAmt = 0.0;
	
	if (hdnList.indexOf(';', hdnList.indexOf(";" + ival + "|")+1) != -1)
		after = hdnList.substring(hdnList.indexOf(';', hdnList.indexOf(";" + ival + "|")+1));
		
	list = ival + "|" + ConvertDblToCurr(ConvertStrToDbl(NewIndRes));
	window.opener.document.getElementById('hdnIndList').value = before+list+after;
	ival = parseInt(ival) + 1;
	
	if (parseInt(ival) != 9 && parseInt(ival) != 10){
	window.opener.document.getElementById('IndList|'+ ival +'|IndWeeks|TD|').innerText = ConvertStrToDbl(IndWeeks);
	window.opener.document.getElementById('IndList|'+ ival +'|IndRate|TD|').innerText = ConvertDblToCurr(ConvertStrToDbl(IndRates));
	}	
	window.opener.document.getElementById('IndList|'+ ival +'|NewIndResAmt|TD|').innerText = ConvertDblToCurr(ConvertStrToDbl(NewIndRes));

	for (var i = 2; i < 11; i ++)	
	{
		TotNewAmt += ConvertStrToDbl(window.opener.document.getElementById('IndList|'+ i +'|NewIndResAmt|TD|').innerText);
	}
	window.opener.document.getElementById('NewIndInc').innerText = ConvertDblToCurr(ConvertStrToDbl(IndInc) + TotNewAmt);
	window.opener.document.getElementById('NewIndBalAmt').innerText = ConvertDblToCurr(ConvertStrToDbl(BalIndRes) + TotNewAmt);
	// Populate values on 'Reserve Summary' tab
	window.opener.CalcResAna();
	window.close();	
	
	return false;
}*/

/*function onSaveRehabReserve()//Mohit
{
	var NewRehabRes = document.getElementById('NewRehabResAmt').value;
	var RehabWeeks = document.getElementById('RehabWeeks').value;
	var RehabRates = document.getElementById('RehabRate').value;
	var ival = document.getElementById('selectedrowid').value;
	var hdnList = window.opener.document.getElementById('hdnRehabList').value;
	var before = hdnList.substring(0, hdnList.indexOf(";" + ival + "|") + 1);
	var after = "";
	var list = "";
	
	var RehabInc = window.opener.document.getElementById('hdnRehabIncurred').value;
	var BalRehabRes = window.opener.document.getElementById('RehabBalAmt').value;
	
	var TotNewAmt = 0.0;
	
	if (hdnList.indexOf(';', hdnList.indexOf(";" + ival + "|")+1) != -1)
		after = hdnList.substring(hdnList.indexOf(';', hdnList.indexOf(";" + ival + "|")+1));
		
	list = ival + "|" + ConvertDblToCurr(ConvertStrToDbl(NewRehabRes));
	window.opener.document.getElementById('hdnRehabList').value = before+list+after;
	ival = parseInt(ival) + 1;
	if (parseInt(ival) == 2){
	window.opener.document.getElementById('RehabList|'+ ival +'|RehabWeeks|TD|').innerText = ConvertStrToDbl(RehabWeeks);
	window.opener.document.getElementById('RehabList|'+ ival +'|RehabRate|TD|').innerText = ConvertDblToCurr(ConvertStrToDbl(RehabRates));
	}
	window.opener.document.getElementById('RehabList|'+ ival +'|NewRehabResAmt|TD|').innerText = ConvertDblToCurr(ConvertStrToDbl(NewRehabRes));

	for (var i = 2; i < 6; i ++)	
	{
		TotNewAmt += ConvertStrToDbl(window.opener.document.getElementById('RehabList|'+ i +'|NewRehabResAmt|TD|').innerText);
	}
	window.opener.document.getElementById('NewRehabInc').innerText = ConvertDblToCurr(ConvertStrToDbl(RehabInc) + TotNewAmt);
	window.opener.document.getElementById('NewRehabBalAmt').innerText = ConvertDblToCurr(ConvertStrToDbl(BalRehabRes) + TotNewAmt);
	// Populate values on 'Reserve Summary' tab
	window.opener.CalcResAna();
	window.close();	
	
	return false;
}*/

/*function onSaveLegalReserve()//Mohit
{
	var NewLegalRes = document.getElementById('NewLegalResAmt').value;
	var ival = document.getElementById('selectedrowid').value;
	var hdnList = window.opener.document.getElementById('hdnLegalList').value;
	var before = hdnList.substring(0, hdnList.indexOf(";" + ival + "|") + 1);
	var after = "";
	var list = "";
	
	var LegalInc = window.opener.document.getElementById('hdnLegalIncurred').value;
	var BalLegalRes = window.opener.document.getElementById('LegalBalAmt').value;
	
	var TotNewAmt = 0.0;
	
	if (hdnList.indexOf(';', hdnList.indexOf(";" + ival + "|")+1) != -1)
		after = hdnList.substring(hdnList.indexOf(';', hdnList.indexOf(";" + ival + "|")+1));
		
	list = ival + "|" + ConvertDblToCurr(ConvertStrToDbl(NewLegalRes));
	window.opener.document.getElementById('hdnLegalList').value = before+list+after;
	ival = parseInt(ival) + 1;
	window.opener.document.getElementById('LegalList|'+ ival +'|NewLegalResAmt|TD|').innerText = ConvertDblToCurr(ConvertStrToDbl(NewLegalRes));

	for (var i = 2; i < 5; i ++)	
	{
		TotNewAmt += ConvertStrToDbl(window.opener.document.getElementById('LegalList|'+ i +'|NewLegalResAmt|TD|').innerText);
	}
	window.opener.document.getElementById('NewLegalInc').innerText = ConvertDblToCurr(ConvertStrToDbl(LegalInc) + TotNewAmt);
	window.opener.document.getElementById('NewLegalBalAmt').innerText = ConvertDblToCurr(ConvertStrToDbl(BalLegalRes) + TotNewAmt);
	// Populate values on 'Reserve Summary' tab
	window.opener.CalcResAna();
	window.close();	
	
	return false;
}*/

//Safeway: Mohit Yadav: Save the Reserve Worksheet for Other reserves
/*function onSaveOthReserve()
{
	var NewOthRes = document.getElementById('NewOthResAmt').value;
	var ival = document.getElementById('selectedrowid').value;
	var hdnList = window.opener.document.getElementById('hdnOthList').value;
	var before = hdnList.substring(0, hdnList.indexOf(";" + ival + "|") + 1);
	var after = "";
	var list = "";
	
	var OthInc = window.opener.document.getElementById('hdnOthIncurred').value;
	var BalOthRes = window.opener.document.getElementById('OthBalAmt').value;
	
	var TotNewAmt = 0.0;
	
	if (hdnList.indexOf(';', hdnList.indexOf(";" + ival + "|")+1) != -1)
		after = hdnList.substring(hdnList.indexOf(';', hdnList.indexOf(";" + ival + "|")+1));
		
	list = ival + "|" + ConvertDblToCurr(ConvertStrToDbl(NewOthRes));
	window.opener.document.getElementById('hdnOthList').value = before+list+after;
	ival = parseInt(ival) + 1;
	window.opener.document.getElementById('OthList|'+ ival +'|NewOthResAmt|TD|').innerText = ConvertDblToCurr(ConvertStrToDbl(NewOthRes));

	for (var i = 2; i < 4; i ++)	
	{
		TotNewAmt += ConvertStrToDbl(window.opener.document.getElementById('OthList|'+ i +'|NewOthResAmt|TD|').innerText);
	}
	window.opener.document.getElementById('NewOthInc').innerText = ConvertDblToCurr(ConvertStrToDbl(OthInc) + TotNewAmt);
	window.opener.document.getElementById('NewOthBalAmt').innerText = ConvertDblToCurr(ConvertStrToDbl(BalOthRes) + TotNewAmt);
	// Populate values on 'Reserve Summary' tab
	window.opener.CalcResAna();
	window.close();	
	
	return false;
}*/

/*function LoadValuesMed()//Mohit
{	
	var iRowID = parseInt(document.getElementById('selectedrowid').value);
	var ctrl = window.opener.document.getElementById('Edit'+iRowID);
	
	if (ctrl != null && ctrl.value == "false")
	{
		document.getElementById('NewMedResAmt').readOnly = true;
		document.getElementById('NewMedResAmt').style.backgroundColor = 'silver';
		
		document.getElementById('Save').disabled = true;
	}
	iRowID += 1;
	var Desc1 = "MedList|" + iRowID + "|MedResCat|TD|";
	document.getElementById('Desc1').value = window.opener.document.getElementById(Desc1).innerText;
	Desc1 = "MedList|" + iRowID + "|NewMedResAmt|TD|";
	document.getElementById('NewMedResAmt').value = window.opener.document.getElementById(Desc1).innerText;
	return false;
}*/

/*function LoadValuesInd()//Mohit
{	
	var iRowID = parseInt(document.getElementById('selectedrowid').value);
	var ctrl = window.opener.document.getElementById('Edit'+iRowID);
	
	if (ctrl != null && ctrl.value == "false")
	{
		document.getElementById('IndWeeks').readOnly = true;
		document.getElementById('IndWeeks').style.backgroundColor = 'silver';
		
		document.getElementById('IndRate').readOnly = true;
		document.getElementById('IndRate').style.backgroundColor = 'silver';
		
		document.getElementById('NewIndResAmt').readOnly = true;
		document.getElementById('NewIndResAmt').style.backgroundColor = 'silver';
		
		document.getElementById('Save').disabled = true;
	}
	iRowID += 1;
	if (iRowID == "2")
	   {
	    document.getElementById('IndRate').readOnly = true;
		document.getElementById('IndRate').style.backgroundColor = 'silver';
		
		document.getElementById('NewIndResAmt').readOnly = true;
		document.getElementById('NewIndResAmt').style.backgroundColor = 'silver';
	   }
	if (iRowID == "4")
	   {
	    document.getElementById('IndRate').readOnly = true;
		document.getElementById('IndRate').style.backgroundColor = 'silver';
		
		document.getElementById('NewIndResAmt').readOnly = true;
		document.getElementById('NewIndResAmt').style.backgroundColor = 'silver';
	   }
	if (iRowID == "9" || iRowID == "10")
	   {
	    document.getElementById('IndWeeks').readOnly = true;
		document.getElementById('IndWeeks').style.backgroundColor = 'silver';
		
		document.getElementById('IndRate').readOnly = true;
		document.getElementById('IndRate').style.backgroundColor = 'silver';
	   }
	var Desc2 = "IndList|" + iRowID + "|IndResCat|TD|";
	document.getElementById('Desc2').value = window.opener.document.getElementById(Desc2).innerText;	
	Desc2 = "IndList|" + iRowID + "|IndWeeks|TD|";
	document.getElementById('IndWeeks').value = window.opener.document.getElementById(Desc2).innerText;
	Desc2 = "IndList|" + iRowID + "|IndRate|TD|";
	document.getElementById('IndRate').value = window.opener.document.getElementById(Desc2).innerText;
	Desc2 = "IndList|" + iRowID + "|NewIndResAmt|TD|";
	document.getElementById('NewIndResAmt').value = window.opener.document.getElementById(Desc2).innerText;
	
	el = document.getElementById('IndWeeks');	
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", IndWeeks_Blur);
	}
	el = document.getElementById('IndRate');	
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", IndRate_Blur);
	}
	return false;
}*/

/*function LoadValuesRehab()//Mohit
{	
	var iRowID = parseInt(document.getElementById('selectedrowid').value);
	var ctrl = window.opener.document.getElementById('Edit'+iRowID);
	
	if (ctrl != null && ctrl.value == "false")
	{
		document.getElementById('RehabWeeks').readOnly = true;
		document.getElementById('RehabWeeks').style.backgroundColor = 'silver';
		
		document.getElementById('RehabRate').readOnly = true;
		document.getElementById('RehabRate').style.backgroundColor = 'silver';
		
		document.getElementById('NewRehabResAmt').readOnly = true;
		document.getElementById('NewRehabResAmt').style.backgroundColor = 'silver';
		
		document.getElementById('Save').disabled = true;
	}
	iRowID += 1;
	if (iRowID == "2")
	   {
	    document.getElementById('RehabRate').readOnly = true;
		document.getElementById('RehabRate').style.backgroundColor = 'silver';
		
		document.getElementById('NewRehabResAmt').readOnly = true;
		document.getElementById('NewRehabResAmt').style.backgroundColor = 'silver';
	   }
	if (iRowID == "3" || iRowID == "4" || iRowID == "5")
	   {
	    document.getElementById('RehabWeeks').readOnly = true;
		document.getElementById('RehabWeeks').style.backgroundColor = 'silver';
		
		document.getElementById('RehabRate').readOnly = true;
		document.getElementById('RehabRate').style.backgroundColor = 'silver';
	   }
	var Desc3 = "RehabList|" + iRowID + "|RehabResCat|TD|";
	document.getElementById('Desc3').value = window.opener.document.getElementById(Desc3).innerText;	
	Desc3 = "RehabList|" + iRowID + "|RehabWeeks|TD|";
	document.getElementById('RehabWeeks').value = window.opener.document.getElementById(Desc3).innerText;
	Desc3 = "RehabList|" + iRowID + "|RehabRate|TD|";
	document.getElementById('RehabRate').value = window.opener.document.getElementById(Desc3).innerText;
	Desc3 = "RehabList|" + iRowID + "|NewRehabResAmt|TD|";
	document.getElementById('NewRehabResAmt').value = window.opener.document.getElementById(Desc3).innerText;
	
	el = document.getElementById('RehabWeeks');	
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", RehabWeeks_Blur);
	}
	el = document.getElementById('RehabRate');	
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", RehabRate_Blur);
	}
	return false;
}*/

/*function LoadValuesLegal()//Mohit
{	
	var iRowID = parseInt(document.getElementById('selectedrowid').value);
	var ctrl = window.opener.document.getElementById('Edit'+iRowID);
	
	if (ctrl != null && ctrl.value == "false")
	{
		document.getElementById('NewLegalResAmt').readOnly = true;
		document.getElementById('NewLegalResAmt').style.backgroundColor = 'silver';
		
		document.getElementById('Save').disabled = true;
	}
	iRowID += 1;
	var Desc4 = "LegalList|" + iRowID + "|LegalResCat|TD|";
	document.getElementById('Desc4').value = window.opener.document.getElementById(Desc4).innerText;
	Desc4 = "LegalList|" + iRowID + "|NewLegalResAmt|TD|";
	document.getElementById('NewLegalResAmt').value = window.opener.document.getElementById(Desc4).innerText;
	return false;
}*/

//Safeway: Mohit Yadav: Reserve Worksheet for Other reserves
/*function LoadValuesOth()
{	
	var iRowID = parseInt(document.getElementById('selectedrowid').value);
	var ctrl = window.opener.document.getElementById('Edit'+iRowID);
	
	if (ctrl != null && ctrl.value == "false")
	{
		document.getElementById('NewOthResAmt').readOnly = true;
		document.getElementById('NewOthResAmt').style.backgroundColor = 'silver';
		
		document.getElementById('Save').disabled = true;
	}
	iRowID += 1;
	var Desc5 = "OthList|" + iRowID + "|OthResCat|TD|";
	document.getElementById('Desc5').value = window.opener.document.getElementById(Desc5).innerText;
	Desc5 = "OthList|" + iRowID + "|NewOthResAmt|TD|";
	document.getElementById('NewOthResAmt').value = window.opener.document.getElementById(Desc5).innerText;
	return false;
}*/
//sk
function ConvertStrToDbl(str)
{
	str = str.replace("$", "");
	
	while (str.indexOf(",") != -1)
		str = str.replace(",", "");
		
    if(str.indexOf('(') >= 0)
    {
        str=str.replace(/[\(\)]/g ,"");
	    str= new String(-1 * ConvertStrToDbl(str));
    }
		
	if (str == "")
		return 0.0;
	else
		return parseFloat(str);
}

function ConvertDblToCurr(dbl)
{
    //round to 2 places
	dbl=parseFloat(dbl);
	dbl=dbl.toFixed(2);

	//Handle Full Integer Section (Formats in groups of 3 with comma.)
	var base = new String(dbl);
	base = base.substring(0,base.lastIndexOf('.'));
	var newbase="";
	var j = 0;
	var i=base.length-1;
	for(i;i>=0;i--)
	{	if(j==3)
		{
			newbase = "," + newbase;	
			j=0;

		}
		newbase = base.charAt(i) + newbase;	
		j++;
	}
	
	// Handle Fractional Part
	var str = new String(dbl);
	str = str.substring(str.lastIndexOf(".")+1);

	// Reassemble formatted Currency Value string.
	return "$" + newbase + "." + str;
}

function ShowPlsWait()
{
	var wnd=window.open('csc-Theme\\riskmaster\\common\\html\\progress.html','progressWnd',
				'width=400,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-400)/2);
	self.parent.wndProgress=wnd;
	return false;
}

function formatRMDate2(sParamDate)
{
	var sDateSeparator;
	var iDayPos=0, iMonthPos=0;
	var d=new Date(1999,11,22);
	var s=d.toLocaleString();
	var sRet="";
	var sDate=new String(sParamDate);
	if(sDate=="")
		return "";
	iDayPos=s.indexOf("22");
	iMonthPos=s.indexOf("11");
	//if(IE4)
	//	sDateSeparator=s.charAt(iDayPos+2);
	//else
		sDateSeparator="/";
	if(iDayPos<iMonthPos)
		sRet=sDate.substr(6,2) + sDateSeparator + sDate.substr(4,2) + sDateSeparator + sDate.substr(0,4);
	else
		sRet=sDate.substr(4,2) + sDateSeparator + sDate.substr(6,2) + sDateSeparator + sDate.substr(0,4);
	return sRet;
}


function SetFormNameRSW(frmName)
{
	m_frmName = frmName;
}  

function Rsw_Load() {
    var ret = false;
    var rswid = document.getElementById('hdnRSWid').value;
	var ctrl = document.getElementById('hdnSubmittedTo');
	
	if (rswid == "-1")
	{  
	    if (ctrl != null)
	    {
		    if (ctrl.value == "None")
		    {
			    ret = false;
			    ret = confirm("No valid Supervisor / Manager exists for current user. Click 'OK' to continue and 'Cancel' to exit from Reserve Worksheet.");
			    if (!ret) {
			        return false;
			    }
			    else 
			    {
			        return true;
			    }
		    }
	    }
	}
	
	
	//onblur
    el = document.getElementById('TempTotalRate');	
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", TempTotalRate_Blur);
	}
	
	el = document.getElementById('TempPartialRate');	
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", TempPartialRate_Blur);
	}
	
	el = document.getElementById('PPDRate');	
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", PPDRate_Blur);
	}
	
	el = document.getElementById('MaxDurationPTDRate');	
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", MaxDurationPTDRate_Blur);
	}
	
	el = document.getElementById('SurvivorshipRate');	
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", SurvivorshipRate_Blur);
	}
	
	el = document.getElementById('DependencyRate');
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", DependencyRate_Blur);
	}
	
	el = document.getElementById('DisfigurementRate');
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", DisfigurementRate_Blur);
	}
	
	el = document.getElementById('LifePensionRate');
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", LifePensionRate_Blur);
	}
	
	el = document.getElementById('VocRehabTDRate');
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", VocRehabTDRate_Blur);
	}
	
	el = document.getElementById('Amt1MedWC');//Medical
	if(el!=null)
	{
        AddMedicalWC();
        CalcResAna();
	}
	
	el = document.getElementById('Amt1WeeksTemWC');//Temporary Disability
	if(el!=null)
	{
        AddTempDisWC();
	}
	
	el = document.getElementById('Amt1PerWC');//Permanent Disability
	if(el!=null)
	{
        AddPermDisWC();
	}
	
	el = document.getElementById('Amt1WeeksVocWC');//Vocational Rehabilitation
	if(el!=null)
	{
        AddVocRehabWC();
	}
	
	el = document.getElementById('Amt1LegWC');//Legal
	if(el!=null)
	{
        AddLegalWC();
	}
	
	el = document.getElementById('Amt1AllWC');//Allocated Expense
	if(el!=null)
	{
        AddAllExpWC();
	}
	
	el = document.getElementById('Amt1MedWC');//Hospital Expense
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt1MedWC_Blur);
	}
	
	el = document.getElementById('Amt2MedWC');//Physician Expense
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt2MedWC_Blur);
	}
	
	el = document.getElementById('Amt3MedWC');//Physical Therapy /Chiro/Work Hardening
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt3MedWC_Blur);
	}
	
	el = document.getElementById('Amt4MedWC');//Medical Case Management
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt4MedWC_Blur);
	}
	
	el = document.getElementById('Amt5MedWC');//Utilization Review
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt5MedWC_Blur);
	}
	
	el = document.getElementById('Amt6MedWC');//Diagnostic Testing
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt6MedWC_Blur);
	}
	
	el = document.getElementById('Amt7MedWC');//Drugs, Prosthetics, etc.
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt7MedWC_Blur);
	}
	
	el = document.getElementById('Amt8MedWC');//2nd Opinion/Consult (not legal)
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt8MedWC_Blur);
	}
	
	el = document.getElementById('Amt9MedWC');//Mileage
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt9MedWC_Blur);
	}
	
	el = document.getElementById('Amt10MedWC');//Equipment
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt10MedWC_Blur);
	}
	
	el = document.getElementById('Amt11MedWC');//Legal Exams/Procedures
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt11MedWC_Blur);
	}
	
	el = document.getElementById('Amt12MedWC');//Other (Future Medical, etc.)
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt12MedWC_Blur);
	}
	
	el = document.getElementById('Amt1WeeksTemWC');//Temporary Total Weeks
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt1WeeksTemWC_Blur);
	}
	
	el = document.getElementById('Amt2WeeksTemWC');//Temporary Partial Weeks
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt2WeeksTemWC_Blur);
	}
	
	el = document.getElementById('Amt1RateTemWC');//Temporary Total Rates
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt1RateTemWC_Blur);
	}
	
	el = document.getElementById('Amt2RateTemWC');//Temporary Partial Rates
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt2RateTemWC_Blur);
	}
	
	el = document.getElementById('Amt1TemWC');//Temporary Total
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt1TemWC_Blur);
	}
	
	el = document.getElementById('Amt2TemWC');//Temporary Partial
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt2TemWC_Blur);
	}
	
	el = document.getElementById('Amt3TemWC');//Other (Describe)
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt3TemWC_Blur);
	}
	
	el = document.getElementById('Amt1WeeksPerWC');
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt1WeeksPerWC_Blur);
	}
	
	el = document.getElementById('Amt2WeeksPerWC');
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt2WeeksPerWC_Blur);
	}
	
	el = document.getElementById('Amt3WeeksPerWC');
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt3WeeksPerWC_Blur);
	}
	
	el = document.getElementById('Amt4WeeksPerWC');
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt4WeeksPerWC_Blur);
	}
	
	el = document.getElementById('Amt5WeeksPerWC');
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt5WeeksPerWC_Blur);
	}
	
	el = document.getElementById('Amt6WeeksPerWC');
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt6WeeksPerWC_Blur);
	}
	
	el = document.getElementById('Amt9WeeksPerWC');
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt9WeeksPerWC_Blur);
	}
	
	el = document.getElementById('Amt10WeeksPerWC');
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt10WeeksPerWC_Blur);
	}
	
	el = document.getElementById('Amt11WeeksPerWC');
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt11WeeksPerWC_Blur);
	}
	
	el = document.getElementById('Amt1RatePerWC');//PPD Rates
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt1RatePerWC_Blur);
	}
	
	el = document.getElementById('Amt2RatePerWC');//Life Pension Rates
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt2RatePerWC_Blur);
	}
	
	el = document.getElementById('Amt3RatePerWC');//Max Duration/PTD Rates
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt3RatePerWC_Blur);
	}
	
	el = document.getElementById('Amt4RatePerWC');//Survivorship Rates
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt4RatePerWC_Blur);
	}
	
	el = document.getElementById('Amt5RatePerWC');//Dependency Rates
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt5RatePerWC_Blur);
	}
	
	el = document.getElementById('Amt6RatePerWC');//Disfigurement Rates
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt6RatePerWC_Blur);
	}
	
	el = document.getElementById('Amt9RatePerWC');//PPD Rates
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt9RatePerWC_Blur);
	}
	
	el = document.getElementById('Amt10RatePerWC');//PPD Rates
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt10RatePerWC_Blur);
	}
	
	el = document.getElementById('Amt11RatePerWC');//PPD Rates
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt11RatePerWC_Blur);
	}
	
	//Percent
	el = document.getElementById('Amt1PercentPerWC');//PPD
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt1PercentPerWC_Blur);
	}
	
	el = document.getElementById('Amt1PercentApportionPerWC');//PPD Apportionment
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt1PercentApportionPerWC_Blur);
	}
	
	el = document.getElementById('Amt1PercentAfterApportionPerWC');//PPD After Apportionment
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt1PercentAfterApportionPerWC_Blur);
	}
	
	el = document.getElementById('Amt2PercentApportionPerWC');//Life Pension Apportionment
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt2PercentApportionPerWC_Blur);
	}
	
	el = document.getElementById('Amt9PercentPerWC');//PPD
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt9PercentPerWC_Blur);
	}
	
	el = document.getElementById('Amt10PercentPerWC');//PPD
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt10PercentPerWC_Blur);
	}
	
	el = document.getElementById('Amt11PercentPerWC');//PPD
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt11PercentPerWC_Blur);
	}
			
	el = document.getElementById('Amt1PerWC');//PPD
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt1PerWC_Blur);
	}
	
	el = document.getElementById('Amt2PerWC');//Life Pension
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt2PerWC_Blur);
	}
	
	el = document.getElementById('Amt3PerWC');//Max Duration/PTD
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt3PerWC_Blur);
	}
	
	el = document.getElementById('Amt4PerWC');//Survivorship
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt4PerWC_Blur);
	}
	
	el = document.getElementById('Amt5PerWC');//Dependency
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt5PerWC_Blur);
	}
	
	el = document.getElementById('Amt6PerWC');//Disfigurement
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt6PerWC_Blur);
	}
	
	el = document.getElementById('Amt7PerWC');//LEC
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt7PerWC_Blur);
	}
	
	el = document.getElementById('Amt8PerWC');//Scheduled PD
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt8PerWC_Blur);
	}
	
	el = document.getElementById('Amt9PerWC');//PPD
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt9PerWC_Blur);
	}
	
	el = document.getElementById('Amt10PerWC');//PPD
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt10PerWC_Blur);
	}
	
	el = document.getElementById('Amt11PerWC');//PPD
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt11PerWC_Blur);
	}
	
	el = document.getElementById('Amt12PerWC');//Attorney Fee
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt12PerWC_Blur);
	}
	
	el = document.getElementById('Amt13PerWC');//Employer Liability
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt13PerWC_Blur);
	}
	
	el = document.getElementById('Amt14PerWC');//Other (Child Support, MSA, Burial, etc.)
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt14PerWC_Blur);
	}
	
	el = document.getElementById('Amt1WeeksVocWC');//Voc Rehab TD Weeks
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt1WeeksVocWC_Blur);
	}
	
	el = document.getElementById('Amt1RateVocWC');//Voc Rehab TD Rates
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt1RateVocWC_Blur);
	}
	
	el = document.getElementById('Amt1VocWC');//Voc Rehab TD
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt1VocWC_Blur);
	}
	
	el = document.getElementById('Amt2VocWC');//Rehab Counseling
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt2VocWC_Blur);
	}
	
	el = document.getElementById('Amt3VocWC');//Rehab Training
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt3VocWC_Blur);
	}
	
	el = document.getElementById('Amt4VocWC');//Job Displacement Voucher
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt4VocWC_Blur);
	}
	
	el = document.getElementById('Amt5VocWC');//Other (Describe)
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt5VocWC_Blur);
	}
	
	el = document.getElementById('Amt1LegWC');//Litigation Costs
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt1LegWC_Blur);
	}
	
	el = document.getElementById('Amt2LegWC');//Expense
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt2LegWC_Blur);
	}
	
	el = document.getElementById('Amt3LegWC');//Early Intervention
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt3LegWC_Blur);
	}
	
	el = document.getElementById('Amt4LegWC');//Medical Witness
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt4LegWC_Blur);
	}
	
	el = document.getElementById('Amt5LegWC');//Other (Describe)
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt5LegWC_Blur);
	}
	
	el = document.getElementById('Amt1AllWC');//Surveillance
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt1AllWC_Blur);
	}
	
	el = document.getElementById('Amt2AllWC');//Investigation
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt2AllWC_Blur);
	}
	
	el = document.getElementById('Amt3AllWC');//Medical Records
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt3AllWC_Blur);
	}
	
	el = document.getElementById('Amt4AllWC');//Other (Describe)
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", Amt4AllWC_Blur);
	}
	
	//onchange
	
	el = document.getElementById('Amt1MedWC');//Hospital Expense
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt1MedWC_Chg);
	}
	
	el = document.getElementById('Amt2MedWC');//Physician Expense
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt2MedWC_Chg);
	}
	
	el = document.getElementById('Amt3MedWC');//Physical Therapy /Chiro/Work Hardening
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt3MedWC_Chg);
	}
	
	el = document.getElementById('Amt4MedWC');//Medical Case Management
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt4MedWC_Chg);
	}
	
	el = document.getElementById('Amt5MedWC');//Utilization Review
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt5MedWC_Chg);
	}
	
	el = document.getElementById('Amt6MedWC');//Diagnostic Testing
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt6MedWC_Chg);
	}
	
	el = document.getElementById('Amt7MedWC');//Drugs, Prosthetics, etc.
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt7MedWC_Chg);
	}
	
	el = document.getElementById('Amt8MedWC');//2nd Opinion/Consult (not legal)
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt8MedWC_Chg);
	}
	
	el = document.getElementById('Amt9MedWC');//Mileage
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt9MedWC_Chg);
	}
	
	el = document.getElementById('Amt10MedWC');//Equipment
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt10MedWC_Chg);
	}
	
	el = document.getElementById('Amt11MedWC');//Legal Exams/Procedures
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt11MedWC_Chg);
	}
	
	el = document.getElementById('Amt12MedWC');//Other (Future Medical, etc.)
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt12MedWC_Chg);
	}
	
	el = document.getElementById('Amt1WeeksTemWC');//Temporary Total Weeks
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt1WeeksTemWC_Chg);
	}
	
	el = document.getElementById('Amt2WeeksTemWC');//Temporary Partial Weeks
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt2WeeksTemWC_Chg);
	}
		
	el = document.getElementById('Amt1RateTemWC');
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt1RateTemWC_Chg);
	}
	
	el = document.getElementById('Amt2RateTemWC');
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt2RateTemWC_Chg);
	}
	
	el = document.getElementById('Amt1TemWC');
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt1TemWC_Chg);
	}
	
	el = document.getElementById('Amt2TemWC');
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt2TemWC_Chg);
	}
	
	el = document.getElementById('Amt3TemWC');
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt3TemWC_Chg);
	}
	
	el = document.getElementById('Amt1WeeksPerWC');//PPD
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt1WeeksPerWC_Chg);
	}
	
	el = document.getElementById('Amt2WeeksPerWC');//Life Pension
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt2WeeksPerWC_Chg);
	}
	
	el = document.getElementById('Amt3WeeksPerWC');//Max Duration/PTD
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt3WeeksPerWC_Chg);
	}
	
	el = document.getElementById('Amt4WeeksPerWC');//Survivorship
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt4WeeksPerWC_Chg);
	}
	
	el = document.getElementById('Amt5WeeksPerWC');//Dependency
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt5WeeksPerWC_Chg);
	}
	
	el = document.getElementById('Amt6WeeksPerWC');//Disfigurement
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt6WeeksPerWC_Chg);
	}
	
	el = document.getElementById('Amt9WeeksPerWC');//PPD
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt9WeeksPerWC_Chg);
	}
	
	el = document.getElementById('Amt10WeeksPerWC');//PPD
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt10WeeksPerWC_Chg);
	}
	
	el = document.getElementById('Amt11WeeksPerWC');//PPD
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt11WeeksPerWC_Chg);
	}
			
	el = document.getElementById('Amt1RatePerWC');//PPD Rate
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt1RatePerWC_Chg);
	}
	
	el = document.getElementById('Amt2RatePerWC');//Life Pension Rate
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt2RatePerWC_Chg);
	}
	
	el = document.getElementById('Amt3RatePerWC');//Max Duration/PTD Rate
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt3RatePerWC_Chg);
	}
	
	el = document.getElementById('Amt4RatePerWC');//Survivorship
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt4RatePerWC_Chg);
	}
	
	el = document.getElementById('Amt5RatePerWC');//Dependency
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt5RatePerWC_Chg);
	}
	
	el = document.getElementById('Amt6RatePerWC');//Disfigurement
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt6RatePerWC_Chg);
	}
	
	el = document.getElementById('Amt9RatePerWC');//PPD
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt9RatePerWC_Chg);
	}
	
	el = document.getElementById('Amt10RatePerWC');//PPD
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt10RatePerWC_Chg);
	}
	
	el = document.getElementById('Amt11RatePerWC');//PPD
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt11RatePerWC_Chg);
	}
				
	el = document.getElementById('Amt1PerWC');//PPD
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt1PerWC_Chg);
	}
	
	el = document.getElementById('Amt2PerWC');//Life Pension
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt2PerWC_Chg);
	}
	
	el = document.getElementById('Amt3PerWC');//Max Duration/PTD
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt3PerWC_Chg);
	}
	
	el = document.getElementById('Amt4PerWC');//Survivorship
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt4PerWC_Chg);
	}
	
	el = document.getElementById('Amt5PerWC');//Dependency
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt5PerWC_Chg);
	}
	
	el = document.getElementById('Amt6PerWC');//Disfigurement
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt6PerWC_Chg);
	}
	
	el = document.getElementById('Amt7PerWC');//LEC
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt7PerWC_Chg);
	}
	
	el = document.getElementById('Amt8PerWC');//Scheduled PD
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt8PerWC_Chg);
	}
	
	el = document.getElementById('Amt9PerWC');//PPD
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt9PerWC_Chg);
	}
	
	el = document.getElementById('Amt10PerWC');//PPD
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt10PerWC_Chg);
	}
	
	el = document.getElementById('Amt11PerWC');//PPD
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt11PerWC_Chg);
	}
	
	el = document.getElementById('Amt12PerWC');//Attorney Fee
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt12PerWC_Chg);
	}
	
	el = document.getElementById('Amt13PerWC');//Employer Liability
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt13PerWC_Chg);
	}
	
	el = document.getElementById('Amt14PerWC');//Other (Child Support, MSA, Burial, etc.)
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt14PerWC_Chg);
	}
	
	el = document.getElementById('Amt1WeeksVocWC');//Voc Rehab TD Weeks
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt1WeeksVocWC_Chg);
	}
	
	el = document.getElementById('Amt2VocWC');//Rehab Counseling Weeks
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt2VocWC_Chg);
	}
	
	el = document.getElementById('Amt3VocWC');//Rehab Training
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt3VocWC_Chg);
	}
	
	el = document.getElementById('Amt4VocWC');//Job Displacement Voucher
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt4VocWC_Chg);
	}
	
	el = document.getElementById('Amt5VocWC');//Other (Describe)
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt5VocWC_Chg);
	}
	
	el = document.getElementById('Amt1LegWC');//Litigation Costs
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt1LegWC_Chg);
	}
	
	el = document.getElementById('Amt2LegWC');//Expense
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt2LegWC_Chg);
	}
	
	el = document.getElementById('Amt3LegWC');//Early Intervention
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt3LegWC_Chg);
	}
	
	el = document.getElementById('Amt4LegWC');//Medical Witness
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt4LegWC_Chg);
	}
	
	el = document.getElementById('Amt5LegWC');//Other (Describe)
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt5LegWC_Chg);
	}
	
	el = document.getElementById('Amt1AllWC');//Surveillance
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt1AllWC_Chg);
	}
	
	el = document.getElementById('Amt2AllWC');//Investigation
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt2AllWC_Chg);
	}
	
	el = document.getElementById('Amt3AllWC');//Medical Records
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt3AllWC_Chg);
	}
	
	el = document.getElementById('Amt4AllWC');//Other (Describe)
	if(el!=null)
	{
        el.setAttribute("onchange", "");
	    RMX.addEvent(el, "change", Amt4AllWC_Chg);
	}
	
	el = document.getElementById('CommMedWC');//Medical Comment
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", CommMedWC_Blur);
	}
	
	el = document.getElementById('CommTemWC');//Temporary Disability Comments
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", CommTemWC_Blur);
	}
	
	el = document.getElementById('CommPerWC');//Permanent Disability Comments
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", CommPerWC_Blur);
	}
	
	el = document.getElementById('CommVocWC');
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", CommVocWC_Blur);
	}
	
	el = document.getElementById('CommLegWC');
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", CommLegWC_Blur);
	}
	
	el = document.getElementById('CommAllWC');
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", CommAllWC_Blur);
	}
	
	el = document.getElementById('CommResSummWC');
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", CommResSummWC_Blur);
	}
	
	el = document.getElementById('ClaimInfo');
	if(el!=null)
	{
        el.setAttribute("onclick", "");
	    RMX.addEvent(el, "click", ClaimInfo_Click);
	}
	
	el = document.getElementById('MedicalWC');
	if(el!=null)
	{
        el.setAttribute("onclick", "");
	    RMX.addEvent(el, "click", MedicalWC_Click);
	}
	
	el = document.getElementById('TemporaryDisabilityWC');
	if(el!=null)
	{
        el.setAttribute("onclick", "");
	    RMX.addEvent(el, "click", TemporaryDisabilityWC_Click);
	}
	
	el = document.getElementById('PermanentDisabilityWC');
	if(el!=null)
	{
        el.setAttribute("onclick", "");
	    RMX.addEvent(el, "click", PermanentDisabilityWC_Click);
	}
	
	el = document.getElementById('VocationalRehabilitationWC');
	if(el!=null)
	{
        el.setAttribute("onclick", "");
	    RMX.addEvent(el, "click", VocationalRehabilitationWC_Click);
	}
	
	el = document.getElementById('LegalWC');
	if(el!=null)
	{
        el.setAttribute("onclick", "");
	    RMX.addEvent(el, "click", LegalWC_Click);
	}
	
	el = document.getElementById('AllocatedExpenseWC');
	if(el!=null)
	{
        el.setAttribute("onclick", "");
	    RMX.addEvent(el, "click", AllocatedExpenseWC_Click);
	}
	
	el = document.getElementById('ResSummaryWC');
	if(el!=null)
	{
        el.setAttribute("onclick", "");
	    RMX.addEvent(el, "click", ResSummaryWC_Click);
	}
	
	//Correct the numeric control
	el = document.getElementById('Amt1WeeksTemWC');//Temproary Total Weeks
	if(el!=null)
	{
        var dbl=new String(el.value);    	
	    if(dbl.indexOf('$') >= 0)
		    el.value=parseInt(dbl.replace(/[\$]/g ,""));
		
		if (parseInt(el.value) == 0 || el.value == "")
		    el.value = "";
		else
		    RSWnumLostFocus(el);
	}
	
	el = document.getElementById('Amt2WeeksTemWC');//Temproary Partial Weeks
	if(el!=null)
	{
        var dbl=new String(el.value);    	
	    if(dbl.indexOf('$') >= 0)
		    el.value=parseInt(dbl.replace(/[\$]/g ,""));
		
		if (parseInt(el.value) == 0 || el.value == "")
		    el.value = "";
		else
		    RSWnumLostFocus(el);
	}
	
	el = document.getElementById('Amt1WeeksPerWC');
	if(el!=null)
	{
        var dbl=new String(el.value);    	
	    if(dbl.indexOf('$') >= 0)
		    el.value=parseInt(dbl.replace(/[\$]/g ,""));
		
		if (parseInt(el.value) == 0 || el.value == "")
		    el.value = "";
		else
		    RSWnumLostFocus(el);
	}
	
	el = document.getElementById('Amt2WeeksPerWC');
	if(el!=null)
	{
       var dbl=new String(el.value);    	
	    if(dbl.indexOf('$') >= 0)
		    el.value=parseInt(dbl.replace(/[\$]/g ,""));
		
		if (parseInt(el.value) == 0 || el.value == "")
		    el.value = "";
		else
		    RSWnumLostFocus(el);
	}
	
	el = document.getElementById('Amt3WeeksPerWC');
	if(el!=null)
	{
       var dbl=new String(el.value);    	
	    if(dbl.indexOf('$') >= 0)
		    el.value=parseInt(dbl.replace(/[\$]/g ,""));
		
		if (parseInt(el.value) == 0 || el.value == "")
		    el.value = "";
		else
		    RSWnumLostFocus(el);
	}
	
	el = document.getElementById('Amt4WeeksPerWC');
	if(el!=null)
	{
        var dbl=new String(el.value);    	
	    if(dbl.indexOf('$') >= 0)
		    el.value=parseInt(dbl.replace(/[\$]/g ,""));
		
		if (parseInt(el.value) == 0 || el.value == "")
		    el.value = "";
		else
		    RSWnumLostFocus(el);
	}
	
	el = document.getElementById('Amt5WeeksPerWC');
	if(el!=null)
	{
        var dbl=new String(el.value);    	
	    if(dbl.indexOf('$') >= 0)
		    el.value=parseInt(dbl.replace(/[\$]/g ,""));
		
		if (parseInt(el.value) == 0 || el.value == "")
		    el.value = "";
		else
		    RSWnumLostFocus(el);
	}
	
	el = document.getElementById('Amt6WeeksPerWC');
	if(el!=null)
	{
        var dbl=new String(el.value);    	
	    if(dbl.indexOf('$') >= 0)
		    el.value=parseInt(dbl.replace(/[\$]/g ,""));
		
		if (parseInt(el.value) == 0 || el.value == "")
		    el.value = "";
		else
		    RSWnumLostFocus(el);
	}
	
	el = document.getElementById('Amt9WeeksPerWC');
	if(el!=null)
	{
        var dbl=new String(el.value);    	
	    if(dbl.indexOf('$') >= 0)
		    el.value=parseInt(dbl.replace(/[\$]/g ,""));
		
		if (parseInt(el.value) == 0 || el.value == "")
		    el.value = "";
		else
		    RSWnumLostFocus(el);
	}
	
	el = document.getElementById('Amt10WeeksPerWC');
	if(el!=null)
	{
        var dbl=new String(el.value);    	
	    if(dbl.indexOf('$') >= 0)
		    el.value=parseInt(dbl.replace(/[\$]/g ,""));
		
		if (parseInt(el.value) == 0 || el.value == "")
		    el.value = "";
		else
		    RSWnumLostFocus(el);
	}
	
	el = document.getElementById('Amt11WeeksPerWC');
	if(el!=null)
	{
        var dbl=new String(el.value);    	
	    if(dbl.indexOf('$') >= 0)
		    el.value=parseInt(dbl.replace(/[\$]/g ,""));
		
		if (parseInt(el.value) == 0 || el.value == "")
		    el.value = "";
		else
		    RSWnumLostFocus(el);
	}
	
	el = document.getElementById('Amt1WeeksVocWC');
	if(el!=null)
	{
       var dbl=new String(el.value);    	
	    if(dbl.indexOf('$') >= 0)
		    el.value=parseInt(dbl.replace(/[\$]/g ,""));
		
		if (parseInt(el.value) == 0 || el.value == "")
		    el.value = "";
		else
		    RSWnumLostFocus(el);
	}
	
	el = document.getElementById('IncurredAmtMedWC');
	if(el!=null)
       el.value = ConvertDblToCurr(ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById("hdnIncurredMedWC").value));
    
    el = document.getElementById('IncurredAmtTemWC');
	if(el!=null)
       el.value = ConvertDblToCurr(ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById("hdnIncurredTemWC").value));
       
    el = document.getElementById('IncurredAmtPerWC');
	if(el!=null)
       el.value = ConvertDblToCurr(ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById("hdnIncurredPerWC").value));
       
    el = document.getElementById('IncurredAmtVocWC');
	if(el!=null)
       el.value = ConvertDblToCurr(ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById("hdnIncurredVocWC").value));
    
    el = document.getElementById('IncurredAmtLegWC');
	if(el!=null)
       el.value = ConvertDblToCurr(ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById("hdnIncurredLegWC").value));
       
    el = document.getElementById('IncurredAmtAllWC');
	if(el!=null)
       el.value = ConvertDblToCurr(ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById("hdnIncurredAllWC").value));
	
}

function TransTypeNewReserveBalance_OnBlur(reserve_Type, trans_Type) {
    
    var txtCurrentTransTypeIncurredAmt = document.getElementById('txt_' + reserve_Type + '_TransTypeIncurredAmount_' + trans_Type);
    var txtCurrentTransTypeNewReserveBal = document.getElementById('txt_' + reserve_Type + '_TransTypeNewReserveBal_' + trans_Type);
    var txtCurrentTransTypePaidToDate = document.getElementById('txt_' + reserve_Type + '_Paid_' + trans_Type);
    var amount = 0;
    var dCurrentTransTypeIncuredAmt = 0; //Aman Multi Currency
    if (txtCurrentTransTypeNewReserveBal != null) {
        RSW_MultiCurrencyToDecimal(txtCurrentTransTypeNewReserveBal);
    }
    amount = txtCurrentTransTypeNewReserveBal.getAttribute("Amount");
    dCurrentTransTypeIncuredAmt = Number(amount);
    if (txtCurrentTransTypePaidToDate != null) {
        RSW_MultiCurrencyToDecimal(txtCurrentTransTypePaidToDate);
    }
    amount = Number(txtCurrentTransTypePaidToDate.getAttribute("Amount"));
    dCurrentTransTypeIncuredAmt = Number(dCurrentTransTypeIncuredAmt) + Number(amount);
    txtCurrentTransTypeIncurredAmt.value = dCurrentTransTypeIncuredAmt;
    RSW_MultiCurrencyOnBlur(txtCurrentTransTypeIncurredAmt);
    //dCurrentTransTypeIncuredAmt = ConvertStrToDbl(txtCurrentTransTypeNewReserveBal.value) + ConvertStrToDbl(txtCurrentTransTypePaidToDate.value)
    //txtCurrentTransTypeIncurredAmt.value = ConvertDblToCurr(dCurrentTransTypeIncuredAmt);

    CalculateReserveAmount_OnBlur(reserve_Type, trans_Type); 
}


//Added by Nitin for R6 
function CalculateReserveAmount_OnBlur(reserve_Type, trans_Type) {

    var txtReserveTypePaid = document.getElementById('txt_' + reserve_Type + '_Paid');
    var txtReserveTypeBal = document.getElementById('txt_' + reserve_Type + '_Outstanding');
    var txtReserveTypeIncurred = document.getElementById('txt_' + reserve_Type + '_TotalIncurred');
    var txtReserveTypeNewBal = document.getElementById('txt_' + reserve_Type + '_NEWOutstanding');
    var txtReserveTypeNewIncurred = document.getElementById('txt_' + reserve_Type + '_NEWTOTALINCURRED');
    var hdnReserveTypeCollection = document.getElementById('hdn_' + reserve_Type + '_Collection');
    var hdnReserveTypeReserveChange = document.getElementById('hdn_' + reserve_Type + '_ReserveChange');
    var hdnReserveTypeNewResAmt = document.getElementById('hdn_' + reserve_Type + '_ReserveAmt');
    var hdnReserveTypeNewBal = document.getElementById('hdn_' + reserve_Type + '_NEWOutstanding');
    var hdnReserveTypeNewIncurred = document.getElementById('hdn_' + reserve_Type + '_NEWTOTALINCURRED');
    var txtReserveTypeComments = document.getElementById('txt_' + reserve_Type + '_Comments');
    var txtReserveTypeReason = document.getElementById('txt_' + reserve_Type + '_ReserveWorksheetReason'); 

    var txtCurrentTransTypeIncurredAmt = document.getElementById('txt_' + reserve_Type + '_TransTypeIncurredAmount_' + trans_Type);
    var txtCurrentTransTypeNewReserveBal = document.getElementById('txt_' + reserve_Type + '_TransTypeNewReserveBal_' + trans_Type);
    var txtCurrentTransTypePaidToDate = document.getElementById('txt_' + reserve_Type + '_Paid_' + trans_Type);

    var txtCurrentTransTypeOther = document.getElementById('txt_' + reserve_Type + '_TransTypeIncurredAmount_OtherAdjustment');
    var hdnCurrentTransTypeOther = document.getElementById('hdn_' + reserve_Type + '_TransTypeIncurredAmount_OtherAdjustment');
    var hdnCurrentTransTypeIncurredAmt = document.getElementById('hdn_' + reserve_Type + '_TransTypeIncurredAmount_' + trans_Type);
    var hdnCurrentTransTypeNewReserveBal = document.getElementById('hdn_' + reserve_Type + '_TransTypeNewReserveBal_' + trans_Type);
    
    var arrCtrls;
    var transTypeControl = null;
    //Aman Multi Currency --Start
    var totalNewAmount = 0;
    var dTemp = 0;
    var dPaid = 0;
    var dCollect = 0;
    var dNewResAmt = 0;
    var dNewIncurred = 0;
    var dNewBalance = 0;
    var dCurrentTransTypeReserveBal = 0;
    var Amount = 0;
    var amt = 0;    
    var hdnCtrlToClear = document.getElementById('hdnCtrlToEdit');

    if (hdnCtrlToClear != null) 
    {
       arrCtrls = hdnCtrlToClear.value.split(",");

       for (var count = 0; count < arrCtrls.length; count++) 
       {
           if (arrCtrls[count].indexOf("txt_" + reserve_Type + "_TransTypeIncurredAmount") != -1) 
           {
               transTypeControl = document.getElementById(arrCtrls[count]);
               if (transTypeControl != null) 
               {
                   RSW_MultiCurrencyToDecimal(transTypeControl);
                   Amount = transTypeControl.getAttribute("Amount");
                   totalNewAmount = Number(totalNewAmount) + Number(Amount);
                   //totalNewAmount += ConvertStrToDbl(transTypeControl.value);
               }
           }
       }
       //srajindersin - 10/29/2013 - MITS 33055 - Rounding-Off to 2 decimal places
       totalNewAmount = totalNewAmount.toFixed(2);
   }
   //commented by Nitin for Mits 18877
   if (txtReserveTypeBal != null) {
       RSW_MultiCurrencyToDecimal(txtReserveTypeBal);
    }
    if (txtReserveTypePaid != null) {
        RSW_MultiCurrencyToDecimal(txtReserveTypePaid); 
    }
    if (txtReserveTypeIncurred != null) {
        RSW_MultiCurrencyToDecimal(txtReserveTypeIncurred); 
    }
   
   if (txtCurrentTransTypeIncurredAmt != null) {
       RSW_MultiCurrencyToDecimal(txtCurrentTransTypeIncurredAmt);
   }
   if (txtCurrentTransTypePaidToDate != null) {
       RSW_MultiCurrencyToDecimal(txtCurrentTransTypePaidToDate);
   }
   if (txtCurrentTransTypeOther != null) {
       RSW_MultiCurrencyToDecimal(txtCurrentTransTypeOther);
   }
   if (txtReserveTypeNewBal != null) {
       RSW_MultiCurrencyToDecimal(txtReserveTypeNewBal);
   }
   if (txtReserveTypeNewIncurred != null) {
       RSW_MultiCurrencyToDecimal(txtReserveTypeNewIncurred);
   }
    if (txtReserveTypePaid != null)  //&& totalNewAmount >= 0.0)
    {
        //dPaid = ConvertStrToDbl(txtReserveTypePaid.value);
        //RSW_MultiCurrencyToDecimal(txtReserveTypePaid);
        dPaid = Number(txtReserveTypePaid.getAttribute("Amount"));
        //dCollect = ConvertStrToDbl(hdnReserveTypeCollection.value);
        dCollect = Number(hdnReserveTypeCollection.value);
        //hdnReserveTypeNewIncurred.value = ConvertDblToCurr(totalNewAmount);
        //hdnReserveTypeNewBal.value = ConvertDblToCurr(totalNewAmount - dPaid);

        hdnReserveTypeNewIncurred.value = totalNewAmount;
        hdnReserveTypeNewBal.value = totalNewAmount - dPaid;
        //setting reserve amount would used at the time of saving approved ws
        hdnReserveTypeNewResAmt.value = hdnReserveTypeNewIncurred.value;                
        Amount = Number(txtReserveTypeBal.getAttribute("Amount"));        
        amt = Number(txtReserveTypePaid.getAttribute("Amount"));
        //calc Reserve change amount
        //if ((ConvertStrToDbl(txtReserveTypePaid.value) + ConvertStrToDbl(txtReserveTypeBal.value)) == ConvertStrToDbl(hdnReserveTypeNewIncurred.value))
        if ((dPaid + Amount) == hdnReserveTypeNewIncurred.value)
        {
            hdnReserveTypeReserveChange.value = "No Change";
            m_bReserveAmntChanges = false;
        }
        else
        {
            //hdnReserveTypeReserveChange.value = CorrectNegAmt(ConvertDblToCurr(ConvertStrToDbl(hdnReserveTypeNewIncurred.value) - (ConvertStrToDbl(txtReserveTypePaid.value) + ConvertStrToDbl(txtReserveTypeBal.value))));
            hdnReserveTypeReserveChange.value = hdnReserveTypeNewIncurred.value - (amt + Amount);
            m_bReserveAmntChanges = true;
        }
        
        //setting Reserchange flag true ,that would be used at the time of Saving Validation
        m_bReserveAmntChanges = true;
    }
    else if (txtReserveTypePaid != null && totalNewAmount == 0) 
    {
        //hdnReserveTypeNewBal.value = txtReserveTypeBal.value;        
        Amount = Number(txtReserveTypeBal.getAttribute("Amount"));
        hdnReserveTypeNewBal.value = Amount;        
        amt = Number(txtReserveTypeIncurred.getAttribute("Amount"));
        //hdnReserveTypeNewIncurred.value = txtReserveTypeIncurred.value;
        hdnReserveTypeNewIncurred.value = amt;
        hdnReserveTypeReserveChange.value = "No Change";
        m_bReserveAmntChanges = false;
    }

    txtReserveTypeNewBal.value = hdnReserveTypeNewBal.value;
    RSW_MultiCurrencyOnBlur(txtReserveTypeNewBal);
    txtReserveTypeNewIncurred.value = hdnReserveTypeNewIncurred.value;
    RSW_MultiCurrencyOnBlur(txtReserveTypeNewIncurred);
    
    if (txtCurrentTransTypeIncurredAmt != null) 
    {
        //currencyLostFocus(txtCurrentTransTypeIncurredAmt);

        //dCurrentTransTypeReserveBal = ConvertStrToDbl(txtCurrentTransTypeIncurredAmt.value) - ConvertStrToDbl(txtCurrentTransTypePaidToDate.value)
        dCurrentTransTypeReserveBal = Number(txtCurrentTransTypeIncurredAmt.getAttribute("Amount")) - Number(txtCurrentTransTypePaidToDate.getAttribute("Amount"));
        txtCurrentTransTypeNewReserveBal.value = dCurrentTransTypeReserveBal;
        RSW_MultiCurrencyOnBlur(txtCurrentTransTypeNewReserveBal);
        //updating corresponding hidden fields to retain changed values at serverside
        hdnCurrentTransTypeNewReserveBal.value = txtCurrentTransTypeNewReserveBal.getAttribute("Amount");

        //nnorouzi; MITS 18989 and cleaning up the code start
        //if (hdnCurrentTransTypeIncurredAmt.value != txtCurrentTransTypeIncurredAmt.value) 
        if(hdnCurrentTransTypeIncurredAmt.value != txtCurrentTransTypeIncurredAmt.getAttribute("Amount"))
        {
            m_arrReasonsToBeChecked[reserve_Type] = reserve_Type;
            m_arrCommentsToBeChecked[reserve_Type] = reserve_Type;
            hdnCurrentTransTypeIncurredAmt.value = txtCurrentTransTypeIncurredAmt.getAttribute("Amount");
        }
        //nnorouzi; MITS 18989 and cleaning up the code end
    }
    else 
    {
        //means it is other adjustment
        if (txtCurrentTransTypeOther != null) 
        {
                //currencyLostFocus(txtCurrentTransTypeOther);

                //nnorouzi; MITS 18989 and cleaning up the code start
                if (hdnCurrentTransTypeOther.value != txtCurrentTransTypeOther.getAttribute("Amount")) {
                    m_arrReasonsToBeChecked[reserve_Type] = reserve_Type;
                    m_arrCommentsToBeChecked[reserve_Type] = reserve_Type;
                    hdnCurrentTransTypeOther.value = txtCurrentTransTypeOther.getAttribute("Amount");
                }
                //nnorouzi; MITS 18989 and cleaning up the code end
            }
        }
}

function ReplaceAll(strContent,strReplaceFrom,strReplaceTo)
{
    var i = strContent.indexOf(strReplaceFrom);
    var c = strContent;
    while (i > -1) 
    {
        c = c.replace(strReplaceFrom, strReplaceTo);
        i = c.indexOf(strReplaceFrom);
    }
    return c;
}

//onblur
function TempTotalRate_Blur(){RSWcurrencyLostFocus(document.getElementById('TempTotalRate'));}
function TempPartialRate_Blur(){RSWcurrencyLostFocus(document.getElementById('TempPartialRate'));}
function PPDRate_Blur(){RSWcurrencyLostFocus(document.getElementById('PPDRate'));}
function MaxDurationPTDRate_Blur(){RSWcurrencyLostFocus(document.getElementById('MaxDurationPTDRate'));}
function SurvivorshipRate_Blur(){RSWcurrencyLostFocus(document.getElementById('SurvivorshipRate'));}
function DependencyRate_Blur(){RSWcurrencyLostFocus(document.getElementById('DependencyRate'));}
function DisfigurementRate_Blur(){RSWcurrencyLostFocus(document.getElementById('DisfigurementRate'));}
function LifePensionRate_Blur(){RSWcurrencyLostFocus(document.getElementById('LifePensionRate'));}
function VocRehabTDRate_Blur(){RSWcurrencyLostFocus(document.getElementById('VocRehabTDRate'));}

function Amt1MedWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt1MedWC'));}//Hospital Expense
function Amt2MedWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt2MedWC'));}//Physician Expense
function Amt3MedWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt3MedWC'));}//Physical Therapy /Chiro/Work Hardening
function Amt4MedWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt4MedWC'));}//Medical Case Management
function Amt5MedWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt5MedWC'));}//Utilization Review
function Amt6MedWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt6MedWC'));}//Diagnostic Testing
function Amt7MedWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt7MedWC'));}//Drugs, Prosthetics, etc.
function Amt8MedWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt8MedWC'));}//2nd Opinion/Consult (not legal)
function Amt9MedWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt9MedWC'));}//Mileage
function Amt10MedWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt10MedWC'));}//Equipment
function Amt11MedWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt11MedWC'));}//Legal Exams/Procedures
function Amt12MedWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt12MedWC'));}//Other (Future Medical, etc.)

function Amt1WeeksTemWC_Blur(){RSWnumLostFocus(document.getElementById('Amt1WeeksTemWC'));}//Temproary Total Weeks
function Amt2WeeksTemWC_Blur(){RSWnumLostFocus(document.getElementById('Amt2WeeksTemWC'));}//Temproary Partial Weeks

function Amt1WeeksPerWC_Blur(){RSWnumLostFocus(document.getElementById('Amt1WeeksPerWC'));}//PPD Weeks
function Amt2WeeksPerWC_Blur(){RSWnumLostFocus(document.getElementById('Amt2WeeksPerWC'));}//Life Pension Weeks
function Amt3WeeksPerWC_Blur(){RSWnumLostFocus(document.getElementById('Amt3WeeksPerWC'));}//Max Duration/PTD Weeks
function Amt4WeeksPerWC_Blur(){RSWnumLostFocus(document.getElementById('Amt4WeeksPerWC'));}//Survivorship Weeks
function Amt5WeeksPerWC_Blur(){RSWnumLostFocus(document.getElementById('Amt5WeeksPerWC'));}//Dependency Weeks
function Amt6WeeksPerWC_Blur(){RSWnumLostFocus(document.getElementById('Amt6WeeksPerWC'));}//Disfigurement Weeks
function Amt9WeeksPerWC_Blur(){RSWnumLostFocus(document.getElementById('Amt9WeeksPerWC'));}//PPD Weeks
function Amt10WeeksPerWC_Blur(){RSWnumLostFocus(document.getElementById('Amt10WeeksPerWC'));}//PPD Weeks
function Amt11WeeksPerWC_Blur(){RSWnumLostFocus(document.getElementById('Amt11WeeksPerWC'));}//PPD Weeks

function Amt1RateTemWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt1RateTemWC'));}//Temproary Total Rates
function Amt2RateTemWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt2RateTemWC'));}//Temproary Partial Rates

function Amt1RatePerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt1RatePerWC'));}//PPD Rates
function Amt2RatePerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt2RatePerWC'));}//Life Pension Rates
function Amt3RatePerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt3RatePerWC'));}//Max Duration/PTD Rates
function Amt4RatePerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt4RatePerWC'));}//Survivorship Rates
function Amt5RatePerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt5RatePerWC'));}//Dependency Rates
function Amt6RatePerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt6RatePerWC'));}//Disfigurement Rates
function Amt9RatePerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt9RatePerWC'));}//PPD Rates
function Amt10RatePerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt10RatePerWC'));}//PPD Rates
function Amt11RatePerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt11RatePerWC'));}//PPD Rates

function Amt1TemWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt1TemWC'));}//Temproary Total
function Amt2TemWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt2TemWC'));}//Temproary Partial
function Amt3TemWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt3TemWC'));}//Other (Describe)

function Amt1PerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt1PerWC'));}//PPD
function Amt2PerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt2PerWC'));}//Life Pension
function Amt3PerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt3PerWC'));}//Max Duration/PTD
function Amt4PerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt4PerWC'));}//Survivorship
function Amt5PerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt5PerWC'));}//Dependency
function Amt6PerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt6PerWC'));}//Disfigurement
function Amt7PerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt7PerWC'));}//LEC
function Amt8PerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt8PerWC'));}//Scheduled PD
function Amt9PerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt9PerWC'));}//PPD
function Amt10PerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt10PerWC'));}//PPD
function Amt11PerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt11PerWC'));}//PPD
function Amt12PerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt12PerWC'));}//Attorney Fee
function Amt13PerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt13PerWC'));}//Employer Liability
function Amt14PerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt14PerWC'));}//Other (Child Support, MSA, Burial, etc.)

function Amt1WeeksVocWC_Blur(){RSWnumLostFocus(document.getElementById('Amt1WeeksVocWC'));}//Voc Rehab TD Weeks

function Amt1RateVocWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt1RateVocWC'));}//Voc Rehab TD Rates

function Amt1VocWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt1VocWC'));}
function Amt2VocWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt2VocWC'));}
function Amt3VocWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt3VocWC'));}
function Amt4VocWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt4VocWC'));}
function Amt5VocWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt5VocWC'));}

function Amt1LegWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt1LegWC'));}
function Amt2LegWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt2LegWC'));}
function Amt3LegWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt3LegWC'));}
function Amt4LegWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt4LegWC'));}
function Amt5LegWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt5LegWC'));}

function Amt1AllWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt1AllWC'));}
function Amt2AllWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt2AllWC'));}
function Amt3AllWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt3AllWC'));}
function Amt4AllWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt4AllWC'));}

function Amt1PercentPerWC_Blur(){RSWpercentLostFocus(document.getElementById('Amt1PercentPerWC'));}
function Amt1PercentApportionPerWC_Blur(){RSWpercentLostFocus(document.getElementById('Amt1PercentApportionPerWC'));}
function Amt1PercentAfterApportionPerWC_Blur(){RSWpercentLostFocus(document.getElementById('Amt1PercentAfterApportionPerWC'));}

function Amt2PercentApportionPerWC_Blur(){RSWpercentLostFocus(document.getElementById('Amt2PercentApportionPerWC'));}

function Amt9PercentPerWC_Blur(){RSWpercentLostFocus(document.getElementById('Amt9PercentPerWC'));}
function Amt10PercentPerWC_Blur(){RSWpercentLostFocus(document.getElementById('Amt10PercentPerWC'));}
function Amt11PercentPerWC_Blur(){RSWpercentLostFocus(document.getElementById('Amt11PercentPerWC'));}

//onchange
function Amt1MedWC_Chg(){RSWsetDataChanged('true');}//Hospital Expense
function Amt2MedWC_Chg(){RSWsetDataChanged('true');}//Physician Expense
function Amt3MedWC_Chg(){RSWsetDataChanged('true');}//Physical Therapy /Chiro/Work Hardening
function Amt4MedWC_Chg(){RSWsetDataChanged('true');}//Medical Case Management
function Amt5MedWC_Chg(){RSWsetDataChanged('true');}//Utilization Review
function Amt6MedWC_Chg(){RSWsetDataChanged('true');}//Diagnostic Testing
function Amt7MedWC_Chg(){RSWsetDataChanged('true');}//Drugs, Prosthetics, etc.
function Amt8MedWC_Chg(){RSWsetDataChanged('true');}//2nd Opinion/Consult (not legal)
function Amt9MedWC_Chg(){RSWsetDataChanged('true');}//Mileage
function Amt10MedWC_Chg(){RSWsetDataChanged('true');}//Equipment
function Amt11MedWC_Chg(){RSWsetDataChanged('true');}//Legal Exams/Procedures
function Amt12MedWC_Chg(){RSWsetDataChanged('true');}//Other (Future Medical, etc.)

function Amt1WeeksTemWC_Chg(){RSWsetDataChanged('true');}//Temproary Total Weeks
function Amt2WeeksTemWC_Chg(){RSWsetDataChanged('true');}//Temproary Partial Weeks

function Amt1WeeksPerWC_Chg(){RSWsetDataChanged('true');}//PPD Weeks
function Amt2WeeksPerWC_Chg(){RSWsetDataChanged('true');}//Life Pension Weeks
function Amt3WeeksPerWC_Chg(){RSWsetDataChanged('true');}//Max Duration/PTD Weeks
function Amt4WeeksPerWC_Chg(){RSWsetDataChanged('true');}//Survivorship Weeks
function Amt5WeeksPerWC_Chg(){RSWsetDataChanged('true');}//Dependency Weeks
function Amt6WeeksPerWC_Chg(){RSWsetDataChanged('true');}//Disfigurement Weeks
function Amt9WeeksPerWC_Chg(){RSWsetDataChanged('true');}//PPD Weeks
function Amt10WeeksPerWC_Chg(){RSWsetDataChanged('true');}//PPD Weeks
function Amt11WeeksPerWC_Chg(){RSWsetDataChanged('true');}//PPD Weeks

function Amt1RateTemWC_Chg(){RSWsetDataChanged('true');}//Temproary Total Rates
function Amt2RateTemWC_Chg(){RSWsetDataChanged('true');}//Temproary Partial Rates

function Amt1RatePerWC_Chg(){RSWsetDataChanged('true');}//PPD Rates
function Amt2RatePerWC_Chg(){RSWsetDataChanged('true');}//Life Pension Rates
function Amt3RatePerWC_Chg(){RSWsetDataChanged('true');}//Max Duration/PTD Rates
function Amt4RatePerWC_Chg(){RSWsetDataChanged('true');}//Survivorship Rates
function Amt5RatePerWC_Chg(){RSWsetDataChanged('true');}//Dependency Rates
function Amt6RatePerWC_Chg(){RSWsetDataChanged('true');}//Disfigurement Rates
function Amt9RatePerWC_Chg(){RSWsetDataChanged('true');}//PPD Rates
function Amt10RatePerWC_Chg(){RSWsetDataChanged('true');}//PPD Rates
function Amt11RatePerWC_Chg(){RSWsetDataChanged('true');}//PPD Rates

function Amt1TemWC_Chg(){RSWsetDataChanged('true');}//Temproary Total
function Amt2TemWC_Chg(){RSWsetDataChanged('true');}//Temproary Partial
function Amt3TemWC_Chg(){RSWsetDataChanged('true');}//Other (Describe)

function Amt1PerWC_Chg(){RSWsetDataChanged('true');}//PPD
function Amt2PerWC_Chg(){RSWsetDataChanged('true');}//Life Pension
function Amt3PerWC_Chg(){RSWsetDataChanged('true');}//Max Duration/PTD
function Amt4PerWC_Chg(){RSWsetDataChanged('true');}//Survivorship
function Amt5PerWC_Chg(){RSWsetDataChanged('true');}//Dependency
function Amt6PerWC_Chg(){RSWsetDataChanged('true');}//Disfigurement
function Amt7PerWC_Chg(){RSWsetDataChanged('true');}//LEC
function Amt8PerWC_Chg(){RSWsetDataChanged('true');}//Scheduled PD
function Amt9PerWC_Chg(){RSWsetDataChanged('true');}//PPD
function Amt10PerWC_Chg(){RSWsetDataChanged('true');}//PPD
function Amt11PerWC_Chg(){RSWsetDataChanged('true');}//PPD
function Amt12PerWC_Chg(){RSWsetDataChanged('true');}//Attorney Fee
function Amt13PerWC_Chg(){RSWsetDataChanged('true');}//Employer Liability
function Amt14PerWC_Chg(){RSWsetDataChanged('true');}//Other (Child Support, MSA, Burial, etc.)

function Amt1WeeksVocWC_Chg(){RSWsetDataChanged('true');}//Voc Rehab TD weeks

function Amt2VocWC_Chg(){RSWsetDataChanged('true');}
function Amt3VocWC_Chg(){RSWsetDataChanged('true');}
function Amt4VocWC_Chg(){RSWsetDataChanged('true');}
function Amt5VocWC_Chg(){RSWsetDataChanged('true');}

function Amt1LegWC_Chg(){RSWsetDataChanged('true');}
function Amt2LegWC_Chg(){RSWsetDataChanged('true');}
function Amt3LegWC_Chg(){RSWsetDataChanged('true');}
function Amt4LegWC_Chg(){RSWsetDataChanged('true');}
function Amt5LegWC_Chg(){RSWsetDataChanged('true');}

function Amt1AllWC_Chg(){RSWsetDataChanged('true');}
function Amt2AllWC_Chg(){RSWsetDataChanged('true');}
function Amt3AllWC_Chg(){RSWsetDataChanged('true');}
function Amt4AllWC_Chg(){RSWsetDataChanged('true');}

//Comments blur
function CommMedWC_Blur(){Comm_Blur();}//Medical Comment
function CommTemWC_Blur(){Comm_Blur();}//Temporary Disability Comment
function CommPerWC_Blur(){Comm_Blur();}//Permanent Disability Comment
function CommVocWC_Blur(){Comm_Blur();}//Vocational Rehabilitation Comment
function CommLegWC_Blur(){Comm_Blur();}//Legal Comment
function CommAllWC_Blur(){Comm_Blur();}//Allocated Expense
function CommResSummWC_Blur(){Comm_Blur();}

function ClaimInfo_Click(){TabChange('ClaimInfo');}
function MedicalWC_Click(){TabChange('MedicalWC');}
function TemporaryDisabilityWC_Click(){TabChange('TemporaryDisabilityWC');}
function PermanentDisabilityWC_Click(){TabChange('PermanentDisabilityWC');}
function VocationalRehabilitationWC_Click(){TabChange('VocationalRehabilitationWC');}
function LegalWC_Click(){TabChange('LegalWC');}
function AllocatedExpenseWC_Click(){TabChange('AllocatedExpenseWC');}
function ResSummaryWC_Click(){TabChange('ResSummaryWC');}

function openRSWGridAddEditWindow(listname,mode,width,height)
{
	var openpage;
	var pagepath;
	var iPositionForSelectedGridRow;
	switch(listname)
	{			
		//Commented by Mohit Yadav as per new requirement: Start
		/*case "MedList":
			openpage="MedicalReserves";
			pagepath="/Reserves/ReserveWorksheet/MedList";		
			break;
		case "IndList":
			openpage="IndemnityReserves";
			pagepath="/Reserves/ReserveWorksheet/IndList";		
			break;
		case "RehabList":
			openpage="RehabReserves";
			pagepath="/Reserves/ReserveWorksheet/RehabList";		
			break;
		case "LegalList":
			openpage="LegalReserves";
			pagepath="/Reserves/ReserveWorksheet/LegalList";		
			break;
		case "OthList":
			openpage="OtherReserves";
			pagepath="/Reserves/ReserveWorksheet/OthList";		
			break;*/
		//Commented by Mohit Yadav as per new requirement: End
	}	
	if (mode=="add")
	{	
		RMX.window.open('home?pg=riskmaster'+pagepath,openpage,
			'width=' + width + ',height='+ height +',top='+(screen.availHeight-height)/2+',left='+(screen.availWidth-width)/2+',resizable=no,scrollbars=yes');
	}
	else
	{
		var CheckedFound=false;
		inputs = document.getElementsByTagName("input");
			for (i = 0; i < inputs.length; i++)
			{
				if ((inputs[i].type=="radio") && (inputs[i].checked==true))
				{
					inputname=inputs[i].id.substring(0,inputs[i].id.indexOf("|"));
					if (listname==inputname)	
					{
						CheckedFound=true;
						RMX.window.open('home?pg=riskmaster'+pagepath+'&amp;selectedid='+inputs[i].value,openpage,
							'width=' + width + ',height='+ height +',top='+(screen.availHeight-height)/2+',left='+(screen.availWidth-width)/2+',resizable=no,scrollbars=yes');
					}
				}
			}

		if (CheckedFound==false)
		{
			alert('Please select a record to edit.');
			return false;
		}
	}
	return false;
}

function ConvertDblToPer(dbl)
{
	dbl=parseFloat(dbl);
	dbl=dbl.toFixed(2);

	var base = new String(dbl);
	base = base.substring(0,base.lastIndexOf('.'));
	var newbase="";
	var j = 0;
	var i=base.length-1;
	for(i;i>=0;i--)
	{	if(j==3)
		{
			newbase = "," + newbase;	
			j=0;

		}
		newbase = base.charAt(i) + newbase;	
		j++;
	}
	var str = new String(dbl);
	str = str.substring(str.lastIndexOf(".")+1);
	return newbase + "." + str + "%" ;
}

//Safeway: Mohit Yadav: For Reserve Worksheet currency controls
function RSWcurrencyLostFocus(objCtrl) {
    
	var dbl=new String(objCtrl.value);
    
    if (!objCtrl.getAttribute('readonly'))
    {
	    // Strip and Validate Input
	    // BSB Remove commas first so parseFloat doesn't incorrectly truncate value.	
	    dbl=dbl.replace(/[,\$]/g ,"");
	    
	    if(dbl.length==0)
	    {
	        //This is done for cases where the currency field blank
	        objCtrl.value= "$" + "0" + "." + "00";
	        AutoPopulateRate(objCtrl);
		    return false;
	    }
	
	    //for Est. Collection 			
	    if(dbl.indexOf('(') >= 0)
	    {		
		    if(dbl.indexOf('-') >= 0)
		    {
			    dbl=dbl.replace(/[\-\(\)]/g ,"");
		    }
		    else
		    {
			    dbl=dbl.replace(/[\(\)]/g ,"");	
		    }
	    }				
	    else if(dbl.indexOf('-') >= 0)
	    {
		    dbl=dbl.replace(/[\-]/g ,"");
	    } 		
    
	    if(isNaN(parseFloat(dbl)))
	    {
		    objCtrl.value="$" + "0" + "." + "00";;	
		    return false;
	    }	
	    
	    //round to 2 places
	    dbl=parseFloat(dbl);
	    //hourly rate to have 3 places to decimal
	    if(objCtrl.id =='emphourlyrate')
	    {
	        dbl=dbl.toFixed(3);
	    }
	    else if (objCtrl.id =='TempTotalRate' || objCtrl.id =='TempPartialRate' || objCtrl.id =='PPDRate' ||objCtrl.id =='MaxDurationPTDRate' ||objCtrl.id =='SurvivorshipRate' ||objCtrl.id =='DependencyRate' ||objCtrl.id =='DisfigurementRate' ||objCtrl.id =='LifePensionRate' ||objCtrl.id =='VocRehabTDRate' || objCtrl.id =='Amt7PerWC')
	    {
	        dbl=dbl.toFixed(2);
	    }
	    else
	    {
	    dbl=Math.round(dbl);//As per Safeway they want the amounts to be rounded-off
	    dbl=dbl.toFixed(2);
	    }

	    //Handle Full Integer Section (Formats in groups of 3 with comma.)
	    var base = new String(dbl);
	    base = base.substring(0,base.lastIndexOf('.'));
	    var newbase="";
	    var j = 0;
	    var i=base.length-1;
	    for(i;i>=0;i--)
	    {	if(j==3)
		    {
			    newbase = "," + newbase;	
			    j=0;

		    }
		    newbase = base.charAt(i) + newbase;	
		    j++;
	    }

	
	    // Handle Fractional Part
	    var str = new String(dbl);
	    str = str.substring(str.lastIndexOf(".")+1);

	    // Reassemble formatted Currency Value string.

	    objCtrl.value= "$" + newbase + "." + str;
	    AutoPopulateRate(objCtrl);
	    return true;
	}
	else
	    return false;
}

//Safeway: Mohit Yadav: For Reserve Worksheet numeric controls
function AutoPopulateRate(objctrl) {
    var ival = objctrl.value;
    if (objctrl.id == "TempTotalRate")
       {
        if (document.getElementById('Amt1RateTemWC') != null)
        {
            document.getElementById('Amt1RateTemWC').innerText = ival;
            document.getElementById('Amt1TemWC').innerText = ConvertDblToCurr(Math.round(ConvertStrToDbl(document.getElementById('Amt1WeeksTemWC').value) * ConvertStrToDbl(ival)));
            AddTempDisWC();
        }
       }
       
    if (objctrl.id == "TempPartialRate")
       {
        if (document.getElementById('Amt2RateTemWC') != null)
        {
            document.getElementById('Amt2RateTemWC').innerText = ival;
            document.getElementById('Amt2TemWC').innerText = ConvertDblToCurr(Math.round(ConvertStrToDbl(document.getElementById('Amt2WeeksTemWC').value) * ConvertStrToDbl(ival)));
            AddTempDisWC();
        }
       }
    
    if (objctrl.id == "PPDRate")
    {
        if (document.getElementById('Amt1RatePerWC') != null)
        {
            document.getElementById('Amt1RatePerWC').innerText = ival;
            document.getElementById('Amt1PerWC').innerText = ConvertDblToCurr(Math.round(ConvertStrToDbl(document.getElementById('Amt1WeeksPerWC').value) * ConvertStrToDbl(ival)));
            AddPermDisWC();
        }
        
        if (document.getElementById('Amt9RatePerWC') != null)
        {
            document.getElementById('Amt9RatePerWC').innerText = ival;
            document.getElementById('Amt9PerWC').innerText = ConvertDblToCurr(Math.round((ConvertStrToDbl(ival) * ConvertStrToDbl(document.getElementById('Amt9WeeksPerWC').value) * ConvertStrToDbl(document.getElementById('Amt9PercentPerWC').value))/100));
            AddPermDisWC();
        }
        
        if (document.getElementById('Amt10RatePerWC') != null)
        {
            document.getElementById('Amt10RatePerWC').innerText = ival;
            document.getElementById('Amt10PerWC').innerText = ConvertDblToCurr(Math.round((ConvertStrToDbl(ival) * ConvertStrToDbl(document.getElementById('Amt10WeeksPerWC').value) * ConvertStrToDbl(document.getElementById('Amt10PercentPerWC').value))/100));
            AddPermDisWC();
        }
        
        if (document.getElementById('Amt11RatePerWC') != null)
        {
            document.getElementById('Amt11RatePerWC').innerText = ival;
            document.getElementById('Amt11PerWC').innerText = ConvertDblToCurr(Math.round((ConvertStrToDbl(ival) * ConvertStrToDbl(document.getElementById('Amt11WeeksPerWC').value) * ConvertStrToDbl(document.getElementById('Amt11PercentPerWC').value))/100));
            AddPermDisWC();
        }
        
    }
    
    if (objctrl.id == "MaxDurationPTDRate")
       {
        if (document.getElementById('Amt3RatePerWC') != null)
        {
            document.getElementById('Amt3RatePerWC').innerText = ival;
            document.getElementById('Amt3PerWC').innerText = ConvertDblToCurr(Math.round(ConvertStrToDbl(document.getElementById('Amt3WeeksPerWC').value) * ConvertStrToDbl(ival)));
            AddPermDisWC();
        }
       }
    
    if (objctrl.id == "SurvivorshipRate")
       {
        if (document.getElementById('Amt4RatePerWC') != null)
        {
            document.getElementById('Amt4RatePerWC').innerText = ival;
            document.getElementById('Amt4PerWC').innerText = ConvertDblToCurr(Math.round(ConvertStrToDbl(document.getElementById('Amt4WeeksPerWC').value) * ConvertStrToDbl(ival)));
            AddPermDisWC();
        }
       }
    
    if (objctrl.id == "DependencyRate")
       {
        if (document.getElementById('Amt5RatePerWC') != null)
        {
            document.getElementById('Amt5RatePerWC').innerText = ival;
            document.getElementById('Amt5PerWC').innerText = ConvertDblToCurr(Math.round(ConvertStrToDbl(document.getElementById('Amt5WeeksPerWC').value) * ConvertStrToDbl(ival)));
            AddPermDisWC();
        }
       }
    
    if (objctrl.id == "DisfigurementRate")
       {
        if (document.getElementById('Amt6RatePerWC') != null)
        {
            document.getElementById('Amt6RatePerWC').innerText = ival;
            document.getElementById('Amt6PerWC').innerText = ConvertDblToCurr(Math.round(ConvertStrToDbl(document.getElementById('Amt6WeeksPerWC').value) * ConvertStrToDbl(ival)));
            AddPermDisWC();
        }
       }
    
    if (objctrl.id == "LifePensionRate")
       {
        if (document.getElementById('Amt2RatePerWC') != null)
        {
            document.getElementById('Amt2RatePerWC').innerText = ival;
            document.getElementById('Amt2PerWC').innerText = ConvertDblToCurr(Math.round(ConvertStrToDbl(document.getElementById('Amt2WeeksPerWC').value) * ConvertStrToDbl(ival)));
            AddPermDisWC();
        }
       }
       
    if (objctrl.id == "VocRehabTDRate")
       {
        if (document.getElementById('Amt1RateVocWC') != null)
        {
            document.getElementById('Amt1RateVocWC').innerText = ival;
            document.getElementById('Amt1VocWC').innerText = ConvertDblToCurr(Math.round(ConvertStrToDbl(document.getElementById('Amt1WeeksVocWC').value) * ConvertStrToDbl(ival)));
            AddVocRehabWC();
        }
       }
       
    if (objctrl.id == "Amt1MedWC" || objctrl.id == "Amt2MedWC" || objctrl.id == "Amt3MedWC" || objctrl.id == "Amt4MedWC" || objctrl.id == "Amt5MedWC" || objctrl.id == "Amt6MedWC" || objctrl.id == "Amt7MedWC" || objctrl.id == "Amt8MedWC" || objctrl.id == "Amt9MedWC" || objctrl.id == "Amt10MedWC" || objctrl.id == "Amt11MedWC" || objctrl.id == "Amt12MedWC")
       {
        AddMedicalWC();
       }
    if (objctrl.id == "Amt1TemWC" || objctrl.id == "Amt2TemWC" || objctrl.id == "Amt3TemWC")
       {
        if (objctrl.id == "Amt1TemWC")
            document.getElementById('Amt1TemWC').innerText = ConvertDblToCurr(Math.round(ConvertStrToDbl(document.getElementById('Amt1WeeksTemWC').value) * ConvertStrToDbl(document.getElementById('Amt1RateTemWC').value)));
            
        if (objctrl.id == "Amt2TemWC")
            document.getElementById('Amt2TemWC').innerText = ConvertDblToCurr(Math.round(ConvertStrToDbl(document.getElementById('Amt2WeeksTemWC').value) * ConvertStrToDbl(document.getElementById('Amt2RateTemWC').value)));
            
        AddTempDisWC();
       }
    if (objctrl.id == "Amt7PerWC" || objctrl.id == "Amt8PerWC" || objctrl.id == "Amt12PerWC" || objctrl.id == "Amt13PerWC" || objctrl.id == "Amt14PerWC")
       {
        AddPermDisWC();
       }
    if (objctrl.id == "Amt2VocWC" || objctrl.id == "Amt3VocWC" || objctrl.id == "Amt4VocWC" || objctrl.id == "Amt5VocWC")
       {
        AddVocRehabWC();
       }
    if (objctrl.id == "Amt1LegWC" || objctrl.id == "Amt2LegWC" || objctrl.id == "Amt3LegWC" || objctrl.id == "Amt4LegWC" || objctrl.id == "Amt5LegWC")
       {
        AddLegalWC();
       }
    if (objctrl.id == "Amt1AllWC" || objctrl.id == "Amt2AllWC" || objctrl.id == "Amt3AllWC" || objctrl.id == "Amt4AllWC")
       {
        AddAllExpWC();
       }
       CalcResAna();
}

//Safeway: Mohit Yadav: For Reserve Worksheet numeric controls
function RSWnumLostFocus(objCtrl)
{
	if (!objCtrl.getAttribute('readonly'))
    {
	    if(objCtrl.value.length==0)
	    {
	        objCtrl.value=0;
	        AutoCalResAmt(objCtrl);
		    return false;
	    }
			
	    if(isNaN(parseFloat(objCtrl.value)))
		    objCtrl.value=0;
	    else
		    objCtrl.value=Math.round(objCtrl.value);  //Weeks to be non-decimal field //parseFloat(objCtrl.value);
	
	    if(objCtrl.getAttribute('min')!=null)
	    {
		    if (!(isNaN(parseFloat(objCtrl.getAttribute('min')))))
		    {
			    if (parseFloat(objCtrl.value)<parseFloat(objCtrl.getAttribute('min')))
			    {
				    objCtrl.value=0;
			    }
		    }
	    }
    	
	    if(objCtrl.getAttribute('max')!=null)
	    {
		    if (!(isNaN(parseFloat(objCtrl.getAttribute('max')))))
		    {
			    if (parseFloat(objCtrl.value)> parseFloat(objCtrl.getAttribute('max')))
			    {
				    objCtrl.value=0;
			    }
		    }
	    }
	
	    var dbl=new String(objCtrl.value);	
    	
	    if(dbl.indexOf('-') >= 0)
		    objCtrl.value=dbl.replace(/[\-]/g ,"");
    				
	    AutoCalResAmt(objCtrl);			
	    return true;
	}
	else
	    return false;
}

//Safeway: Mohit Yadav: For Calculating Reserve Amounts in Reserve Worksheet on filling weeks/Rate
function AutoCalResAmt(objctrl) {
    var ival = objctrl.value;
        
    if (objctrl.id == "Amt1WeeksTemWC")
       {
        document.getElementById('Amt1TemWC').innerText = ConvertDblToCurr(Math.round(ival * ConvertStrToDbl(document.getElementById('Amt1RateTemWC').value)));
        AddTempDisWC();
       }
    if (objctrl.id == "Amt2WeeksTemWC")
       {
        document.getElementById('Amt2TemWC').innerText = ConvertDblToCurr(Math.round(ival * ConvertStrToDbl(document.getElementById('Amt2RateTemWC').value)));
        AddTempDisWC();
       }
    if (objctrl.id == "Amt1WeeksPerWC")
       {
        document.getElementById('Amt1PerWC').innerText = ConvertDblToCurr(Math.round(ival * ConvertStrToDbl(document.getElementById('Amt1RatePerWC').value)));
        AddPermDisWC();
       }
    if (objctrl.id == "Amt2WeeksPerWC")
       {
        document.getElementById('Amt2PerWC').innerText = ConvertDblToCurr(Math.round(ival * ConvertStrToDbl(document.getElementById('Amt2RatePerWC').value)));
        AddPermDisWC();
       }
    if (objctrl.id == "Amt3WeeksPerWC")
       {
        document.getElementById('Amt3PerWC').innerText = ConvertDblToCurr(Math.round(ival * ConvertStrToDbl(document.getElementById('Amt3RatePerWC').value)));
        AddPermDisWC();
       }
    if (objctrl.id == "Amt4WeeksPerWC")
       {
        document.getElementById('Amt4PerWC').innerText = ConvertDblToCurr(Math.round(ival * ConvertStrToDbl(document.getElementById('Amt4RatePerWC').value)));
        AddPermDisWC();
       }
    if (objctrl.id == "Amt5WeeksPerWC")
       {
        document.getElementById('Amt5PerWC').innerText = ConvertDblToCurr(Math.round(ival * ConvertStrToDbl(document.getElementById('Amt5RatePerWC').value)));
        AddPermDisWC();
       }
    if (objctrl.id == "Amt6WeeksPerWC")
       {
        document.getElementById('Amt6PerWC').innerText = ConvertDblToCurr(Math.round(ival * ConvertStrToDbl(document.getElementById('Amt6RatePerWC').value)));
        AddPermDisWC();
       }
    if (objctrl.id == "Amt9WeeksPerWC")
       {
        document.getElementById('Amt9PerWC').innerText = ConvertDblToCurr(Math.round((ConvertStrToDbl(ival) * ConvertStrToDbl(document.getElementById('Amt9PercentPerWC').value) * ConvertStrToDbl(document.getElementById('Amt9RatePerWC').value))/100));
        AddPermDisWC();
       }
    if (objctrl.id == "Amt10WeeksPerWC")
       {
        document.getElementById('Amt10PerWC').innerText = ConvertDblToCurr(Math.round((ConvertStrToDbl(ival) * ConvertStrToDbl(document.getElementById('Amt10PercentPerWC').value) * ConvertStrToDbl(document.getElementById('Amt10RatePerWC').value))/100));
        AddPermDisWC();
       }
    if (objctrl.id == "Amt11WeeksPerWC")
       {
        document.getElementById('Amt11PerWC').innerText = ConvertDblToCurr(Math.round((ConvertStrToDbl(ival) * ConvertStrToDbl(document.getElementById('Amt11PercentPerWC').value) * ConvertStrToDbl(document.getElementById('Amt11RatePerWC').value))/100));
        AddPermDisWC();
       }
    if (objctrl.id == "Amt1WeeksVocWC")
       {
        document.getElementById('Amt1VocWC').innerText = ConvertDblToCurr(Math.round(ConvertStrToDbl(ival) * ConvertStrToDbl(document.getElementById('Amt1RateVocWC').value)));
        AddVocRehabWC();
       }
    CalcResAna();
}

//Safeway: Mohit Yadav: For Reserve Worksheet Comments
function Comm_Blur()
{    
    //Commented as per MITS 12892
    /*var ival = objctrl.value;        
    document.getElementById('CommMedWC').innerText = ival;
    document.getElementById('CommIndWC').innerText = ival;
    document.getElementById('CommRehWC').innerText = ival;
    document.getElementById('CommLegWC').innerText = ival;
    document.getElementById('CommOthWC').innerText = ival;
    document.getElementById('AllCommWC').innerText = ival;*/

    //Changes as per MITS 12892        
    //var sval = objctrl.value;
    var sCommAll = '';
    
    if (self.parent.frames["MainFrame"].document.getElementById('CommMedWC') != null)
        sCommAll = self.parent.frames["MainFrame"].document.getElementById('CommMedWC').innerText;//Medical Comment
    if (self.parent.frames["MainFrame"].document.getElementById('CommTemWC') != null)
        sCommAll = sCommAll + '\r\n' + self.parent.frames["MainFrame"].document.getElementById('CommTemWC').innerText;//Temporary Disability Comment
    if (self.parent.frames["MainFrame"].document.getElementById('CommPerWC') != null)
        sCommAll = sCommAll + '\r\n' + self.parent.frames["MainFrame"].document.getElementById('CommPerWC').innerText;//Permanent Disability Comment
    if (self.parent.frames["MainFrame"].document.getElementById('CommVocWC') != null)
        sCommAll = sCommAll + '\r\n' + self.parent.frames["MainFrame"].document.getElementById('CommVocWC').innerText;//Vocational Rehabilitation Comment
    if (self.parent.frames["MainFrame"].document.getElementById('CommLegWC') != null)
        sCommAll = sCommAll + '\r\n' + self.parent.frames["MainFrame"].document.getElementById('CommLegWC').innerText;//Legal Comment
    if (self.parent.frames["MainFrame"].document.getElementById('CommAllWC') != null)
        sCommAll = sCommAll + '\r\n' + self.parent.frames["MainFrame"].document.getElementById('CommAllWC').innerText;
    if (self.parent.frames["MainFrame"].document.getElementById('CommResSummWC') != null)
        self.parent.frames["MainFrame"].document.getElementById('CommResSummWC').innerText = sCommAll;
}

//Safeway: Mohit Yadav: For Reserve Worksheet Reserve Summary
function CalcResAna() {
    var sCurAmt;
	var sNewList = "ResSummWCList;RowId|Paid|PrevBal|NewBal|NewInc|ResChg";
	//For Medical WC Reserves
	if (self.parent.frames["MainFrame"].document.getElementById('ResSummWC|2|Paid|TD|') != null &&
	self.parent.frames["MainFrame"].document.getElementById('PaidAmtMedWC') != null)
	{
	    self.parent.frames["MainFrame"].document.getElementById('ResSummWC|2|Paid|TD|').innerText = self.parent.frames["MainFrame"].document.getElementById('PaidAmtMedWC').value;
	    self.parent.frames["MainFrame"].document.getElementById('ResSummWC|2|PrevBal|TD|').innerText = self.parent.frames["MainFrame"].document.getElementById('BalAmtMedWC').value;
	    //self.parent.frames["MainFrame"].document.getElementById('ResSummWC|2|NewBal|TD|').innerText = ConvertDblToCurr(ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('NewBalAmtMedWC').value));
	    self.parent.frames["MainFrame"].document.getElementById('ResSummWC|2|NewBal|TD|').innerText = self.parent.frames["MainFrame"].document.getElementById('NewBalAmtMedWC').value;
	    self.parent.frames["MainFrame"].document.getElementById('ResSummWC|2|NewInc|TD|').innerText = self.parent.frames["MainFrame"].document.getElementById('NewIncAmtMedWC').value;
	}
	//For Temporary Disability Reserves
	if (self.parent.frames["MainFrame"].document.getElementById('ResSummWC|3|Paid|TD|') != null &&
	self.parent.frames["MainFrame"].document.getElementById('PaidAmtTemWC') != null)
	{
	    self.parent.frames["MainFrame"].document.getElementById('ResSummWC|3|Paid|TD|').innerText = self.parent.frames["MainFrame"].document.getElementById('PaidAmtTemWC').value;
	    self.parent.frames["MainFrame"].document.getElementById('ResSummWC|3|PrevBal|TD|').innerText = self.parent.frames["MainFrame"].document.getElementById('BalAmtTemWC').value;
	    //self.parent.frames["MainFrame"].document.getElementById('ResSummWC|3|NewBal|TD|').innerText = ConvertDblToCurr(ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('NewBalAmtTemWC').value));
	    self.parent.frames["MainFrame"].document.getElementById('ResSummWC|3|NewBal|TD|').innerText = self.parent.frames["MainFrame"].document.getElementById('NewBalAmtTemWC').value;
	    self.parent.frames["MainFrame"].document.getElementById('ResSummWC|3|NewInc|TD|').innerText = self.parent.frames["MainFrame"].document.getElementById('NewIncAmtTemWC').value;
	}
	//For Permanent Disability Reserves
	if (self.parent.frames["MainFrame"].document.getElementById('ResSummWC|4|Paid|TD|') != null &&
	self.parent.frames["MainFrame"].document.getElementById('PaidAmtPerWC') != null)
	{
	    self.parent.frames["MainFrame"].document.getElementById('ResSummWC|4|Paid|TD|').innerText = self.parent.frames["MainFrame"].document.getElementById('PaidAmtPerWC').value;
	    self.parent.frames["MainFrame"].document.getElementById('ResSummWC|4|PrevBal|TD|').innerText = self.parent.frames["MainFrame"].document.getElementById('BalAmtPerWC').value;
	    //self.parent.frames["MainFrame"].document.getElementById('ResSummWC|4|NewBal|TD|').innerText = ConvertDblToCurr(ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('NewBalAmtPerWC').value));
	    self.parent.frames["MainFrame"].document.getElementById('ResSummWC|4|NewBal|TD|').innerText = self.parent.frames["MainFrame"].document.getElementById('NewBalAmtPerWC').value;
	    self.parent.frames["MainFrame"].document.getElementById('ResSummWC|4|NewInc|TD|').innerText = self.parent.frames["MainFrame"].document.getElementById('NewIncAmtPerWC').value;
	}
	//For Voc Rehab WC Reserves
	if (self.parent.frames["MainFrame"].document.getElementById('ResSummWC|5|Paid|TD|') != null &&
	self.parent.frames["MainFrame"].document.getElementById('PaidAmtVocWC') != null)
	{
	    self.parent.frames["MainFrame"].document.getElementById('ResSummWC|5|Paid|TD|').innerText = self.parent.frames["MainFrame"].document.getElementById('PaidAmtVocWC').value;
	    self.parent.frames["MainFrame"].document.getElementById('ResSummWC|5|PrevBal|TD|').innerText = self.parent.frames["MainFrame"].document.getElementById('BalAmtVocWC').value;
	    //self.parent.frames["MainFrame"].document.getElementById('ResSummWC|5|NewBal|TD|').innerText = ConvertDblToCurr(ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('NewBalAmtVocWC').value));
	    self.parent.frames["MainFrame"].document.getElementById('ResSummWC|5|NewBal|TD|').innerText = self.parent.frames["MainFrame"].document.getElementById('NewBalAmtVocWC').value;
	    self.parent.frames["MainFrame"].document.getElementById('ResSummWC|5|NewInc|TD|').innerText = self.parent.frames["MainFrame"].document.getElementById('NewIncAmtVocWC').value;
	}
	//For Legal WC Reserves
	if (self.parent.frames["MainFrame"].document.getElementById('ResSummWC|6|Paid|TD|') != null &&
	self.parent.frames["MainFrame"].document.getElementById('PaidAmtLegWC') != null)
	{
	    self.parent.frames["MainFrame"].document.getElementById('ResSummWC|6|Paid|TD|').innerText = self.parent.frames["MainFrame"].document.getElementById('PaidAmtLegWC').value;
	    self.parent.frames["MainFrame"].document.getElementById('ResSummWC|6|PrevBal|TD|').innerText = self.parent.frames["MainFrame"].document.getElementById('BalAmtLegWC').value;
	    //self.parent.frames["MainFrame"].document.getElementById('ResSummWC|6|NewBal|TD|').innerText = ConvertDblToCurr(ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('NewBalAmtLegWC').value));
	    self.parent.frames["MainFrame"].document.getElementById('ResSummWC|6|NewBal|TD|').innerText = self.parent.frames["MainFrame"].document.getElementById('NewBalAmtLegWC').value;
	    self.parent.frames["MainFrame"].document.getElementById('ResSummWC|6|NewInc|TD|').innerText = self.parent.frames["MainFrame"].document.getElementById('NewIncAmtLegWC').value;
	}
	//For Allocated Expense WC Reserves
	if (self.parent.frames["MainFrame"].document.getElementById('ResSummWC|7|Paid|TD|') != null &&
	self.parent.frames["MainFrame"].document.getElementById('PaidAmtAllWC') != null)
	{
	    self.parent.frames["MainFrame"].document.getElementById('ResSummWC|7|Paid|TD|').innerText = self.parent.frames["MainFrame"].document.getElementById('PaidAmtAllWC').value;
	    self.parent.frames["MainFrame"].document.getElementById('ResSummWC|7|PrevBal|TD|').innerText = self.parent.frames["MainFrame"].document.getElementById('BalAmtAllWC').value;
	    //self.parent.frames["MainFrame"].document.getElementById('ResSummWC|7|NewBal|TD|').innerText = ConvertDblToCurr(ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('NewBalAmtAllWC').value));
	    self.parent.frames["MainFrame"].document.getElementById('ResSummWC|7|NewBal|TD|').innerText = self.parent.frames["MainFrame"].document.getElementById('NewBalAmtAllWC').value;
	    self.parent.frames["MainFrame"].document.getElementById('ResSummWC|7|NewInc|TD|').innerText = self.parent.frames["MainFrame"].document.getElementById('NewIncAmtAllWC').value;
	}
	//For Reserve Change
	if (self.parent.frames["MainFrame"].document.getElementById('ResSummWC|2|Paid|TD|') != null)
	{
	    if ((ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|2|Paid|TD|').innerText) + ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|2|PrevBal|TD|').innerText)) == ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|2|NewInc|TD|').innerText))
	        self.parent.frames["MainFrame"].document.getElementById('ResSummWC|2|ResChg|TD|').innerText = "No Change";
	    else
	        self.parent.frames["MainFrame"].document.getElementById('ResSummWC|2|ResChg|TD|').innerText = CorrectNegAmt(ConvertDblToCurr(ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|2|NewInc|TD|').innerText) - (ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|2|Paid|TD|').innerText) + ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|2|PrevBal|TD|').innerText))));
    }
    if (self.parent.frames["MainFrame"].document.getElementById('ResSummWC|3|Paid|TD|') != null)
	{
        if ((ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|3|Paid|TD|').innerText) + ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|3|PrevBal|TD|').innerText)) == ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|3|NewInc|TD|').innerText))
	        self.parent.frames["MainFrame"].document.getElementById('ResSummWC|3|ResChg|TD|').innerText = "No Change";
	    else
	        self.parent.frames["MainFrame"].document.getElementById('ResSummWC|3|ResChg|TD|').innerText = CorrectNegAmt(ConvertDblToCurr(ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|3|NewInc|TD|').innerText) - (ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|3|Paid|TD|').innerText) + ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|3|PrevBal|TD|').innerText))));
	}
	if (self.parent.frames["MainFrame"].document.getElementById('ResSummWC|4|Paid|TD|') != null)
	{    
        if ((ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|4|Paid|TD|').innerText) + ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|4|PrevBal|TD|').innerText)) == ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|4|NewInc|TD|').innerText))
	        self.parent.frames["MainFrame"].document.getElementById('ResSummWC|4|ResChg|TD|').innerText = "No Change";
	    else
	        self.parent.frames["MainFrame"].document.getElementById('ResSummWC|4|ResChg|TD|').innerText = CorrectNegAmt(ConvertDblToCurr(ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|4|NewInc|TD|').innerText) - (ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|4|Paid|TD|').innerText) + ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|4|PrevBal|TD|').innerText))));
	}
	if (self.parent.frames["MainFrame"].document.getElementById('ResSummWC|5|Paid|TD|') != null)
	{     
	    if ((ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|5|Paid|TD|').innerText) + ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|5|PrevBal|TD|').innerText)) == ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|5|NewInc|TD|').innerText))
	        self.parent.frames["MainFrame"].document.getElementById('ResSummWC|5|ResChg|TD|').innerText = "No Change";
	    else
	        self.parent.frames["MainFrame"].document.getElementById('ResSummWC|5|ResChg|TD|').innerText = CorrectNegAmt(ConvertDblToCurr(ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|5|NewInc|TD|').innerText) - (ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|5|Paid|TD|').innerText) + ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|5|PrevBal|TD|').innerText))));
	}    
    if (self.parent.frames["MainFrame"].document.getElementById('ResSummWC|6|Paid|TD|') != null)
	{
        if ((ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|6|Paid|TD|').innerText) + ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|6|PrevBal|TD|').innerText)) == ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|6|NewInc|TD|').innerText))
	        self.parent.frames["MainFrame"].document.getElementById('ResSummWC|6|ResChg|TD|').innerText = "No Change";
	    else
	        self.parent.frames["MainFrame"].document.getElementById('ResSummWC|6|ResChg|TD|').innerText = CorrectNegAmt(ConvertDblToCurr(ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|6|NewInc|TD|').innerText) - (ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|6|Paid|TD|').innerText) + ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|6|PrevBal|TD|').innerText))));
	}
	if (self.parent.frames["MainFrame"].document.getElementById('ResSummWC|7|Paid|TD|') != null)
	{
        if ((ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|7|Paid|TD|').innerText) + ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|7|PrevBal|TD|').innerText)) == ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|7|NewInc|TD|').innerText))
	        self.parent.frames["MainFrame"].document.getElementById('ResSummWC|7|ResChg|TD|').innerText = "No Change";
	    else
	        self.parent.frames["MainFrame"].document.getElementById('ResSummWC|7|ResChg|TD|').innerText = CorrectNegAmt(ConvertDblToCurr(ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|7|NewInc|TD|').innerText) - (ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|7|Paid|TD|').innerText) + ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|7|PrevBal|TD|').innerText))));
	}
	//For Summary Totals
    var TPaid = 0.0;
    var TPBal = 0.0;
    var TNBal = 0.0;
    var TNInc = 0.0;
    
    for (var i = 2; i < 8; i ++)	
	{
	    if (self.parent.frames["MainFrame"].document.getElementById('ResSummWC|'+ i +'|Paid|TD|') != null)
	    {
	        TPaid += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|'+ i +'|Paid|TD|').innerText);
	        TPBal += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|'+ i +'|PrevBal|TD|').innerText);
	        TNBal += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|'+ i +'|NewBal|TD|').innerText);
	        TNInc += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|'+ i +'|NewInc|TD|').innerText);
	    }
	}
    
	if (self.parent.frames["MainFrame"].document.getElementById('ResSummWC|8|Paid|TD|') != null)
	{
	    self.parent.frames["MainFrame"].document.getElementById('ResSummWC|8|Paid|TD|').innerText = CorrectNegAmt(ConvertDblToCurr(TPaid));
	    self.parent.frames["MainFrame"].document.getElementById('ResSummWC|8|PrevBal|TD|').innerText = CorrectNegAmt(ConvertDblToCurr(TPBal));
	    self.parent.frames["MainFrame"].document.getElementById('ResSummWC|8|NewBal|TD|').innerText = CorrectNegAmt(ConvertDblToCurr(TNBal));
	    self.parent.frames["MainFrame"].document.getElementById('ResSummWC|8|NewInc|TD|').innerText = CorrectNegAmt(ConvertDblToCurr(TNInc));
    	
	    if ((ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|8|Paid|TD|').innerText) + ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|8|PrevBal|TD|').innerText)) == ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|8|NewInc|TD|').innerText))
        {
            self.parent.frames["MainFrame"].document.getElementById('ResSummWC|8|ResChg|TD|').innerText = "No Change";
	        self.parent.frames["MainFrame"].document.getElementById('hdnclaimType').value = "-1";
	    }
	    else
	    {
	        self.parent.frames["MainFrame"].document.getElementById('ResSummWC|8|ResChg|TD|').innerText = CorrectNegAmt(ConvertDblToCurr(ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|8|NewInc|TD|').innerText) - (ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|8|Paid|TD|').innerText) + ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('ResSummWC|8|PrevBal|TD|').innerText))));
	        self.parent.frames["MainFrame"].document.getElementById('hdnclaimType').value = "0";
	    }
	}
	
	sNewList = sNewList + 
	    ";1|" + self.parent.frames["MainFrame"].document.getElementById('ResSummWC|2|Paid|TD|').innerText + "|" + self.parent.frames["MainFrame"].document.getElementById('ResSummWC|2|PrevBal|TD|').innerText + "|" + self.parent.frames["MainFrame"].document.getElementById('ResSummWC|2|NewBal|TD|').innerText + "|" + self.parent.frames["MainFrame"].document.getElementById('ResSummWC|2|NewInc|TD|').innerText + "|" + self.parent.frames["MainFrame"].document.getElementById('ResSummWC|2|ResChg|TD|').innerText +
		";2|" + self.parent.frames["MainFrame"].document.getElementById('ResSummWC|3|Paid|TD|').innerText + "|" + self.parent.frames["MainFrame"].document.getElementById('ResSummWC|3|PrevBal|TD|').innerText + "|" + self.parent.frames["MainFrame"].document.getElementById('ResSummWC|3|NewBal|TD|').innerText + "|" + self.parent.frames["MainFrame"].document.getElementById('ResSummWC|3|NewInc|TD|').innerText + "|" + self.parent.frames["MainFrame"].document.getElementById('ResSummWC|3|ResChg|TD|').innerText +
		";3|" + self.parent.frames["MainFrame"].document.getElementById('ResSummWC|4|Paid|TD|').innerText + "|" + self.parent.frames["MainFrame"].document.getElementById('ResSummWC|4|PrevBal|TD|').innerText + "|" + self.parent.frames["MainFrame"].document.getElementById('ResSummWC|4|NewBal|TD|').innerText + "|" + self.parent.frames["MainFrame"].document.getElementById('ResSummWC|4|NewInc|TD|').innerText + "|" + self.parent.frames["MainFrame"].document.getElementById('ResSummWC|4|ResChg|TD|').innerText +
		";4|" + self.parent.frames["MainFrame"].document.getElementById('ResSummWC|5|Paid|TD|').innerText + "|" + self.parent.frames["MainFrame"].document.getElementById('ResSummWC|5|PrevBal|TD|').innerText + "|" + self.parent.frames["MainFrame"].document.getElementById('ResSummWC|5|NewBal|TD|').innerText + "|" + self.parent.frames["MainFrame"].document.getElementById('ResSummWC|5|NewInc|TD|').innerText + "|" + self.parent.frames["MainFrame"].document.getElementById('ResSummWC|5|ResChg|TD|').innerText +
		";5|" + self.parent.frames["MainFrame"].document.getElementById('ResSummWC|6|Paid|TD|').innerText + "|" + self.parent.frames["MainFrame"].document.getElementById('ResSummWC|6|PrevBal|TD|').innerText + "|" + self.parent.frames["MainFrame"].document.getElementById('ResSummWC|6|NewBal|TD|').innerText + "|" + self.parent.frames["MainFrame"].document.getElementById('ResSummWC|6|NewInc|TD|').innerText + "|" + self.parent.frames["MainFrame"].document.getElementById('ResSummWC|6|ResChg|TD|').innerText +
		";6|" + self.parent.frames["MainFrame"].document.getElementById('ResSummWC|7|Paid|TD|').innerText + "|" + self.parent.frames["MainFrame"].document.getElementById('ResSummWC|7|PrevBal|TD|').innerText + "|" + self.parent.frames["MainFrame"].document.getElementById('ResSummWC|7|NewBal|TD|').innerText + "|" + self.parent.frames["MainFrame"].document.getElementById('ResSummWC|7|NewInc|TD|').innerText + "|" + self.parent.frames["MainFrame"].document.getElementById('ResSummWC|7|ResChg|TD|').innerText +
		";7|" + self.parent.frames["MainFrame"].document.getElementById('ResSummWC|8|Paid|TD|').innerText + "|" + self.parent.frames["MainFrame"].document.getElementById('ResSummWC|8|PrevBal|TD|').innerText + "|" + self.parent.frames["MainFrame"].document.getElementById('ResSummWC|8|NewBal|TD|').innerText + "|" + self.parent.frames["MainFrame"].document.getElementById('ResSummWC|8|NewInc|TD|').innerText + "|" + self.parent.frames["MainFrame"].document.getElementById('ResSummWC|8|ResChg|TD|').innerText;
		
		
	self.parent.frames["MainFrame"].document.getElementById('hdnResSummWCList').value = sNewList;
	
	return false;
}

function TabChange(objname)
{
	document.forms[0].hTabName.value = objname;	
	var tmpName="";
	//Deselect All
	for(i=0;i<m_TabList.length;i++)
	{
		tmpName=m_TabList[i].id.substring(4);
		document.getElementById('FORMTAB' + tmpName).style.display="none";
		document.getElementById('TABS'+tmpName).className="NotSelected";
		document.getElementById('LINKTABS'+tmpName).className="NotSelected1";
	}
	//Select the tab requested
	document.getElementById('FORMTAB'+objname).style.display="";
	document.getElementById('TABS'+objname).className="Selected";
	document.getElementById('LINKTABS'+objname).className="Selected";
	setDefaultFocus();
	
	Comm_Blur();
	CalcResAna();
	
    //ResReasonMedWC, ResReasonMedWCbtn, ResReasonMedWC_cid
    if (self.parent.frames["MainFrame"].document.getElementById('hdnMode').value=='ReadOnly')
    {
        if (self.parent.frames["MainFrame"].document.getElementById('ResReasonMedWC') != null)
        {
            self.parent.frames["MainFrame"].document.getElementById('ResReasonMedWC').disabled = true;
		    self.parent.frames["MainFrame"].document.getElementById('ResReasonMedWCbtn').disabled = true;
		}
    }
    
    //TemWCResReason, ResReasonTemWCbtn, ResReasonTemWC_cid
    if (self.parent.frames["MainFrame"].document.getElementById('hdnMode').value=='ReadOnly')
    {
        if (self.parent.frames["MainFrame"].document.getElementById('ResReasonTemWC') != null)
        {
            self.parent.frames["MainFrame"].document.getElementById('ResReasonTemWC').disabled = true;
		    self.parent.frames["MainFrame"].document.getElementById('ResReasonTemWCbtn').disabled = true;
		}
    }
    
    //PerWCResReason, ResReasonPerWCbtn, ResReasonPerWC_cid
    if (self.parent.frames["MainFrame"].document.getElementById('hdnMode').value=='ReadOnly')
    {
        if (self.parent.frames["MainFrame"].document.getElementById('ResReasonPerWC') != null)
        {
            self.parent.frames["MainFrame"].document.getElementById('ResReasonPerWC').disabled = true;
		    self.parent.frames["MainFrame"].document.getElementById('ResReasonPerWCbtn').disabled = true;
		}
    }
    
    //ResReasonVocWC, ResReasonVocWCbtn, ResReasonVocWC_cid
    if (self.parent.frames["MainFrame"].document.getElementById('hdnMode').value=='ReadOnly')
    {
        if (self.parent.frames["MainFrame"].document.getElementById('ResReasonVocWC') != null)
        {
            self.parent.frames["MainFrame"].document.getElementById('ResReasonVocWC').disabled = true;
		    self.parent.frames["MainFrame"].document.getElementById('ResReasonVocWCbtn').disabled = true;
		}
    }
    
    //ResReasonLegWC, ResReasonLegWCbtn, ResReasonLegWC_cid
    if (self.parent.frames["MainFrame"].document.getElementById('hdnMode').value=='ReadOnly')
    {
        if (self.parent.frames["MainFrame"].document.getElementById('ResReasonLegWC') != null)
        {
            self.parent.frames["MainFrame"].document.getElementById('ResReasonLegWC').disabled = true;
		    self.parent.frames["MainFrame"].document.getElementById('ResReasonLegWCbtn').disabled = true;
		}
    }
    
    //ResReasonAllWC, ResReasonAllWCbtn, ResReasonAllWC_cid
    if (self.parent.frames["MainFrame"].document.getElementById('hdnMode').value=='ReadOnly')
    {
        if (self.parent.frames["MainFrame"].document.getElementById('ResReasonAllWC') != null)
        {
            self.parent.frames["MainFrame"].document.getElementById('ResReasonAllWC').disabled = true;
		    self.parent.frames["MainFrame"].document.getElementById('ResReasonAllWCbtn').disabled = true;
	    }
    }
	
	return true;
}

function ConfirmSave()
{
	var ret = false;
	if(m_DataChanged){
		ret = confirm('Data has changed. Do you want to save changes?');
		if (ret==false)
			m_DataChanged = false;
	}
	return ret;
}

function RSWsetDataChanged(b)
{
	m_DataChanged=b;
    
	return b;
}

function CalculateResAmount(dNewBalance, dPaid, dCollect) {
    
 var dTemp = 0.0; 
 var dNewResAmt = 0.0;
 
 //Calculate New Reserve Amount
 if (document.getElementById('hdnCollInRsvBal').value == "True")
 {
	dTemp = dPaid - dCollect;	
    if (dTemp < 0) 
        dTemp = 0.0;        
    dNewResAmt = dNewBalance + dTemp;
 }
 else
	dNewResAmt = dNewBalance + dPaid;

 return dNewResAmt;
}

function CalculateResBal(dReserveAmount, dPaid, dCollect) {

    var dTemp = 0.0;
    if (document.getElementById('hdnCollInRsvBal').value == "True") {
        dTemp = dPaid - dCollect;
        if (dTemp < 0) {
            dTemp = 0;
        }
        return (dReserveAmount - dTemp);
    }
    else {
        return (dReserveAmount - dPaid);
    }
}


function CalculateIncurred(dNewBalance, dPaid, dCollect)
{
 var dTemp = 0.0;
 var dNewIncurred = 0.0;
  
 //Calculate New Incurred Amount
 if (document.getElementById('hdnCollInRsvBal').value == "True")
 {
  dTemp = dPaid - dCollect;
  if (dTemp < 0)
      dTemp = 0.0;
  if (dNewBalance < 0)
      dNewIncurred = dTemp;
  else
      dNewIncurred = dNewBalance + dTemp;
 }
 else
 {
    if (dNewBalance < 0)
        dNewIncurred = dPaid;
    else
        dNewIncurred = dNewBalance + dPaid;
 }
    
 if (document.getElementById('hdnCollInIncurredBal').value == "True")
 {
    dNewIncurred = dNewIncurred - dCollect;
 }

 if (dNewIncurred < 0)
    dNewIncurred = 0.0;
    
 return dNewIncurred;    
}

function AddMedicalWC()
{
 var dTotNewAmt = 0.0;
 var dTemp = 0.0;
 var dPaid = 0.0;
 var dCollect = 0.0;
 
 var dNewResAmt = 0.0;
 var dNewIncurred = 0.0;
 
 //Hospital Expense 
 if (self.parent.frames["MainFrame"].document.getElementById('Amt1MedWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt1MedWC').value);
 //Physician Expense
 if (self.parent.frames["MainFrame"].document.getElementById('Amt2MedWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt2MedWC').value);
 //Physical Therapy /Chiro/Work Hardening
 if (self.parent.frames["MainFrame"].document.getElementById('Amt3MedWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt3MedWC').value);
 //Medical Case Management
 if (self.parent.frames["MainFrame"].document.getElementById('Amt4MedWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt4MedWC').value);
 //Utilization Review
 if (self.parent.frames["MainFrame"].document.getElementById('Amt5MedWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt5MedWC').value);
 //Diagnostic Testing
 if (self.parent.frames["MainFrame"].document.getElementById('Amt6MedWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt6MedWC').value);
 //Drugs, Prosthetics, etc.
 if (self.parent.frames["MainFrame"].document.getElementById('Amt7MedWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt7MedWC').value);
 //2nd Opinion/Consult (not legal)
 if (self.parent.frames["MainFrame"].document.getElementById('Amt8MedWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt8MedWC').value);
 //Mileage
 if (self.parent.frames["MainFrame"].document.getElementById('Amt9MedWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt9MedWC').value);
 //Equipment
 if (self.parent.frames["MainFrame"].document.getElementById('Amt10MedWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt10MedWC').value);
 //Legal Exams/Procedures
 if (self.parent.frames["MainFrame"].document.getElementById('Amt11MedWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt11MedWC').value);
 //Other (Future Medical, etc.)
 if (self.parent.frames["MainFrame"].document.getElementById('Amt12MedWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt12MedWC').value);
 
 if (self.parent.frames["MainFrame"].document.getElementById('PaidAmtMedWC') != null && dTotNewAmt > 0.00)
 {
    dPaid = ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('PaidAmtMedWC').value);
    dCollect = ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('hdnCollMedWC').value);
 
    self.parent.frames["MainFrame"].document.getElementById('NewBalAmtMedWC').innerText = ConvertDblToCurr(dTotNewAmt);
    dNewBalance = dTotNewAmt;
 
    dNewResAmt = CalculateResAmount(dNewBalance,dPaid,dCollect);
    self.parent.frames["MainFrame"].document.getElementById('hdnNewResMedWC').innerText = ConvertDblToCurr(dNewResAmt);
     
    dNewIncurred = CalculateIncurred(dNewBalance,dPaid,dCollect);
    if (dNewIncurred != dPaid)
        self.parent.frames["MainFrame"].document.getElementById('NewIncAmtMedWC').innerText = ConvertDblToCurr(dNewIncurred);
    else
        self.parent.frames["MainFrame"].document.getElementById('NewIncAmtMedWC').innerText = ConvertDblToCurr(0.00);
 }
 else if (self.parent.frames["MainFrame"].document.getElementById('PaidAmtMedWC') != null && dTotNewAmt == 0.00)
 {
    self.parent.frames["MainFrame"].document.getElementById('NewBalAmtMedWC').innerText = self.parent.frames["MainFrame"].document.getElementById('BalAmtMedWC').value;
    self.parent.frames["MainFrame"].document.getElementById('NewIncAmtMedWC').innerText = ConvertDblToCurr(self.parent.frames["MainFrame"].document.getElementById("hdnIncurredMedWC").value);
 }
 
}

function AddTempDisWC()
{ 
 var dTotNewAmt = 0.0;
 var dTemp = 0.0;
 var dPaid = 0.0;
 var dCollect = 0.0;
 
 var dNewResAmt = 0.0;
 var dNewIncurred = 0.0;
 
 if (self.parent.frames["MainFrame"].document.getElementById('Amt1TemWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt1TemWC').value);
 if (self.parent.frames["MainFrame"].document.getElementById('Amt2TemWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt2TemWC').value);
 if (self.parent.frames["MainFrame"].document.getElementById('Amt3TemWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt3TemWC').value);
 
 if (self.parent.frames["MainFrame"].document.getElementById('PaidAmtTemWC') != null && dTotNewAmt > 0.00)
  {
    dPaid = ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('PaidAmtTemWC').value);
    dCollect = ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('hdnCollTemWC').value);
 
    self.parent.frames["MainFrame"].document.getElementById('NewBalAmtTemWC').innerText = ConvertDblToCurr(dTotNewAmt);
    dNewBalance = dTotNewAmt;
 
    dNewResAmt = CalculateResAmount(dNewBalance,dPaid,dCollect);
    self.parent.frames["MainFrame"].document.getElementById('hdnNewResTemWC').innerText = ConvertDblToCurr(dNewResAmt);
 
    dNewIncurred = CalculateIncurred(dNewBalance,dPaid,dCollect);
    if (dNewIncurred != dPaid)
        self.parent.frames["MainFrame"].document.getElementById('NewIncAmtTemWC').innerText = ConvertDblToCurr(dNewIncurred);
    else
        self.parent.frames["MainFrame"].document.getElementById('NewIncAmtTemWC').innerText = ConvertDblToCurr(0.00);
  }
  else if (self.parent.frames["MainFrame"].document.getElementById('PaidAmtTemWC') != null && dTotNewAmt == 0.00)
  {
    self.parent.frames["MainFrame"].document.getElementById('NewBalAmtTemWC').innerText = self.parent.frames["MainFrame"].document.getElementById('BalAmtTemWC').value;
    self.parent.frames["MainFrame"].document.getElementById('NewIncAmtTemWC').innerText = ConvertDblToCurr(self.parent.frames["MainFrame"].document.getElementById("hdnIncurredTemWC").value);
  }
}

function AddPermDisWC()
{ 
 var dTotNewAmt = 0.0;
 var dTemp = 0.0;
 var dPaid = 0.0;
 var dCollect = 0.0;
 
 var dNewResAmt = 0.0;
 var dNewIncurred = 0.0;
 
 if (self.parent.frames["MainFrame"].document.getElementById('Amt1PerWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt1PerWC').value);
 if (self.parent.frames["MainFrame"].document.getElementById('Amt2PerWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt2PerWC').value);
 if (self.parent.frames["MainFrame"].document.getElementById('Amt3PerWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt3PerWC').value);
 if (self.parent.frames["MainFrame"].document.getElementById('Amt4PerWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt4PerWC').value);
 if (self.parent.frames["MainFrame"].document.getElementById('Amt5PerWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt5PerWC').value);
 if (self.parent.frames["MainFrame"].document.getElementById('Amt6PerWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt6PerWC').value);
 if (self.parent.frames["MainFrame"].document.getElementById('Amt7PerWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt7PerWC').value);
 if (self.parent.frames["MainFrame"].document.getElementById('Amt8PerWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt8PerWC').value);
 if (self.parent.frames["MainFrame"].document.getElementById('Amt9PerWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt9PerWC').value);
 if (self.parent.frames["MainFrame"].document.getElementById('Amt10PerWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt10PerWC').value);
 if (self.parent.frames["MainFrame"].document.getElementById('Amt11PerWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt11PerWC').value);
 if (self.parent.frames["MainFrame"].document.getElementById('Amt12PerWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt12PerWC').value);
 if (self.parent.frames["MainFrame"].document.getElementById('Amt13PerWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt13PerWC').value);
 if (self.parent.frames["MainFrame"].document.getElementById('Amt14PerWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt14PerWC').value);
 
 if (self.parent.frames["MainFrame"].document.getElementById('PaidAmtPerWC') != null && dTotNewAmt > 0.00)
 {
    dPaid = ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('PaidAmtPerWC').value);
    dCollect = ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('hdnCollPerWC').value);
 
    self.parent.frames["MainFrame"].document.getElementById('NewBalAmtPerWC').innerText = ConvertDblToCurr(dTotNewAmt);
    dNewBalance = dTotNewAmt;
 
    dNewResAmt = CalculateResAmount(dNewBalance,dPaid,dCollect);
    self.parent.frames["MainFrame"].document.getElementById('hdnNewResPerWC').innerText = ConvertDblToCurr(dNewResAmt);
 
    dNewIncurred = CalculateIncurred(dNewBalance,dPaid,dCollect);
    if (dNewIncurred != dPaid)
        self.parent.frames["MainFrame"].document.getElementById('NewIncAmtPerWC').innerText = ConvertDblToCurr(dNewIncurred);
    else
        self.parent.frames["MainFrame"].document.getElementById('NewIncAmtPerWC').innerText = ConvertDblToCurr(0.00);
  }
  else if (self.parent.frames["MainFrame"].document.getElementById('PaidAmtPerWC') != null && dTotNewAmt == 0.00)
  {
    self.parent.frames["MainFrame"].document.getElementById('NewBalAmtPerWC').innerText = self.parent.frames["MainFrame"].document.getElementById('BalAmtPerWC').value;
    self.parent.frames["MainFrame"].document.getElementById('NewIncAmtPerWC').innerText = ConvertDblToCurr(self.parent.frames["MainFrame"].document.getElementById("hdnIncurredPerWC").value);
  }
 
}

function AddVocRehabWC()
{ 
 var dTotNewAmt = 0.0;
 var dTemp = 0.0;
 var dPaid = 0.0;
 var dCollect = 0.0;
 
 var dNewResAmt = 0.0;
 var dNewIncurred = 0.0;
 
 if (self.parent.frames["MainFrame"].document.getElementById('Amt1VocWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt1VocWC').value);
 if (self.parent.frames["MainFrame"].document.getElementById('Amt2VocWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt2VocWC').value);
 if (self.parent.frames["MainFrame"].document.getElementById('Amt3VocWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt3VocWC').value);
 if (self.parent.frames["MainFrame"].document.getElementById('Amt4VocWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt4VocWC').value);
 if (self.parent.frames["MainFrame"].document.getElementById('Amt5VocWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt5VocWC').value);
  
 if (self.parent.frames["MainFrame"].document.getElementById('PaidAmtVocWC') != null && dTotNewAmt > 0.00)
 {
    dPaid = ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('PaidAmtVocWC').value);
    dCollect = ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('hdnCollVocWC').value);
 
    self.parent.frames["MainFrame"].document.getElementById('NewBalAmtVocWC').innerText = ConvertDblToCurr(dTotNewAmt);
    dNewBalance = dTotNewAmt;
    
    dNewResAmt = CalculateResAmount(dNewBalance,dPaid,dCollect);
    self.parent.frames["MainFrame"].document.getElementById('hdnNewResVocWC').innerText = ConvertDblToCurr(dNewResAmt);
    
    dNewIncurred = CalculateIncurred(dNewBalance,dPaid,dCollect);
    if (dNewIncurred != dPaid)
        self.parent.frames["MainFrame"].document.getElementById('NewIncAmtVocWC').innerText = ConvertDblToCurr(dNewIncurred);
    else
        self.parent.frames["MainFrame"].document.getElementById('NewIncAmtVocWC').innerText = ConvertDblToCurr(0.00);
 }
 else if (self.parent.frames["MainFrame"].document.getElementById('PaidAmtVocWC') != null && dTotNewAmt == 0.00)
 {
    self.parent.frames["MainFrame"].document.getElementById('NewBalAmtVocWC').innerText = self.parent.frames["MainFrame"].document.getElementById('BalAmtVocWC').value;
    self.parent.frames["MainFrame"].document.getElementById('NewIncAmtVocWC').innerText = ConvertDblToCurr(self.parent.frames["MainFrame"].document.getElementById("hdnIncurredVocWC").value);
 }
}

function AddLegalWC()
{ 
 var dTotNewAmt = 0.0;
 var dTemp = 0.0;
 var dPaid = 0.0;
 var dCollect = 0.0;
 
 var dNewResAmt = 0.0;
 var dNewIncurred = 0.0;
 
 if (self.parent.frames["MainFrame"].document.getElementById('Amt1LegWC') != null)//Litigation Costs
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt1LegWC').value);
 if (self.parent.frames["MainFrame"].document.getElementById('Amt2LegWC') != null)//Expense
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt2LegWC').value);
 if (self.parent.frames["MainFrame"].document.getElementById('Amt3LegWC') != null)//Early Intervention
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt3LegWC').value);
 if (self.parent.frames["MainFrame"].document.getElementById('Amt4LegWC') != null)//Medical Witness
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt4LegWC').value);
 if (self.parent.frames["MainFrame"].document.getElementById('Amt5LegWC') != null)//Other (Describe)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt5LegWC').value);
 
 if (self.parent.frames["MainFrame"].document.getElementById('PaidAmtLegWC') != null && dTotNewAmt > 0.00)
 {
     dPaid = ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('PaidAmtLegWC').value);
     dCollect = ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('hdnCollLegWC').value);
     
     self.parent.frames["MainFrame"].document.getElementById('NewBalAmtLegWC').innerText = ConvertDblToCurr(dTotNewAmt);
     dNewBalance = dTotNewAmt;
     
     dNewResAmt = CalculateResAmount(dNewBalance,dPaid,dCollect);
     self.parent.frames["MainFrame"].document.getElementById('hdnNewResLegWC').innerText = ConvertDblToCurr(dNewResAmt);
     
     dNewIncurred = CalculateIncurred(dNewBalance,dPaid,dCollect);
     if (dNewIncurred != dPaid)
        self.parent.frames["MainFrame"].document.getElementById('NewIncAmtLegWC').innerText = ConvertDblToCurr(dNewIncurred);
     else
        self.parent.frames["MainFrame"].document.getElementById('NewIncAmtLegWC').innerText = ConvertDblToCurr(0.00);
 }
 else if (self.parent.frames["MainFrame"].document.getElementById('PaidAmtLegWC') != null && dTotNewAmt == 0.00)
 {
    self.parent.frames["MainFrame"].document.getElementById('NewBalAmtLegWC').innerText = self.parent.frames["MainFrame"].document.getElementById('BalAmtLegWC').value;
    self.parent.frames["MainFrame"].document.getElementById('NewIncAmtLegWC').innerText = ConvertDblToCurr(self.parent.frames["MainFrame"].document.getElementById("hdnIncurredLegWC").value);
 }
}

function AddAllExpWC()
{ 
 var dTotNewAmt = 0.0;
 var dTemp = 0.0;
 var dPaid = 0.0;
 var dCollect = 0.0;
 
 var dNewResAmt = 0.0;
 var dNewIncurred = 0.0;
 
 if (self.parent.frames["MainFrame"].document.getElementById('Amt1AllWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt1AllWC').value);
 if (self.parent.frames["MainFrame"].document.getElementById('Amt2AllWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt2AllWC').value);
 if (self.parent.frames["MainFrame"].document.getElementById('Amt3AllWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt3AllWC').value);
 if (self.parent.frames["MainFrame"].document.getElementById('Amt4AllWC') != null)
    dTotNewAmt += ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('Amt4AllWC').value);

 if (self.parent.frames["MainFrame"].document.getElementById('PaidAmtAllWC') != null && dTotNewAmt > 0.00)
 {
     dPaid = ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('PaidAmtAllWC').value);
     dCollect = ConvertStrToDbl(self.parent.frames["MainFrame"].document.getElementById('hdnCollAllWC').value);
     
     self.parent.frames["MainFrame"].document.getElementById('NewBalAmtAllWC').innerText = ConvertDblToCurr(dTotNewAmt);
     dNewBalance = dTotNewAmt;
     
     dNewResAmt = CalculateResAmount(dNewBalance,dPaid,dCollect);
     self.parent.frames["MainFrame"].document.getElementById('hdnNewResAllWC').innerText = ConvertDblToCurr(dNewResAmt);
     
     dNewIncurred = CalculateIncurred(dNewBalance,dPaid,dCollect);
     if (dNewIncurred != dPaid)
        self.parent.frames["MainFrame"].document.getElementById('NewIncAmtAllWC').innerText = ConvertDblToCurr(dNewIncurred);
     else
        self.parent.frames["MainFrame"].document.getElementById('NewIncAmtAllWC').innerText = ConvertDblToCurr(0.00);
 }
 else if (self.parent.frames["MainFrame"].document.getElementById('PaidAmtAllWC') != null && dTotNewAmt == 0.00)
 {
    self.parent.frames["MainFrame"].document.getElementById('NewBalAmtAllWC').innerText = self.parent.frames["MainFrame"].document.getElementById('BalAmtAllWC').value;
    self.parent.frames["MainFrame"].document.getElementById('NewIncAmtAllWC').innerText = ConvertDblToCurr(self.parent.frames["MainFrame"].document.getElementById("hdnIncurredAllWC").value);
 }
}

function RSWOnClosePromptForPopUp()
{	 	 
	 //get the current URL 
     var url = self.parent.window.opener.location.toString();
     var parentScreen = "";
     //get the parameters 
     url.match(/\?(.+)$/); 
     var params = RegExp.$1; 
     //split up the query string and store in an associative array 
     var params = params.split("&"); 
     var queryStringList = {};  
     for(var i=0;i<1;i++) 
     {     
        var tmp = params[i].split(",");
        if (tmp[0].indexOf("ReserveApproval") > 0)
            parentScreen = "ReserveApproval";
        else
            parentScreen = "ReserveListing";
     }
	 
	 if (parentScreen == "ReserveListing")
	 {
	 
	     if (document.title=='Reserve Worksheet')
         {
            if (self.frames["MainFrame"].location.href.indexOf("MainFrame",1) > 0)
            {
                if (document.forms[0]!=null)
                {
                    if (document.forms[0].action!=null)
                    {
                     if (document.forms[0].action.value=="Save")
                         return;
                     else if(document.forms[0].action.value == "MemoSave")
                        return;                     
                    }
                }
                
                var sMsg = "Please confirm that you have completed work within this pop-up.";    
                
                if ((window.event.clientX < 0) || (window.event.clientY < 0))
                {
	                if(window.event!=null)
                    window.event.returnValue=sMsg;  
                } 
             }
         }
     }
}

function CorrectNegAmt(CtrlVal) {
    
	var dbl=new String(CtrlVal);
	CtrlVal = ConvertStrToDbl(CtrlVal);
	
	if(dbl.indexOf('(') >= 0)
	{
	    dbl=dbl.replace(/[\(\)]/g ,"");
	    dbl=-1 * dbl;
	    return dbl;
	}
	
	if (CtrlVal < 0)
	{
	    dbl = dbl.replace(/[,\-]/g ,"");
	    return "(" + dbl + ")";
	}
	else
	    return ConvertDblToCurr(CtrlVal);
}

function CheckRatesBSave() {
    
    if ((self.parent.frames["MainFrame"].document.getElementById('TempTotalRate').value != '' && 
    self.parent.frames["MainFrame"].document.getElementById('TempTotalRate').value != '$0.00') || 
    (self.parent.frames["MainFrame"].document.getElementById('TempPartialRate').value != '' &&
    self.parent.frames["MainFrame"].document.getElementById('TempPartialRate').value != '$0.00') ||
	(self.parent.frames["MainFrame"].document.getElementById('PPDRate').value != '' &&
	self.parent.frames["MainFrame"].document.getElementById('PPDRate').value != '$0.00') ||
	(self.parent.frames["MainFrame"].document.getElementById('MaxDurationPTDRate').value != '' &&
	self.parent.frames["MainFrame"].document.getElementById('MaxDurationPTDRate').value != '$0.00') ||
	(self.parent.frames["MainFrame"].document.getElementById('SurvivorshipRate').value != '' &&
	self.parent.frames["MainFrame"].document.getElementById('SurvivorshipRate').value != '$0.00') ||
	(self.parent.frames["MainFrame"].document.getElementById('DependencyRate').value != '' &&
	self.parent.frames["MainFrame"].document.getElementById('DependencyRate').value != '$0.00') ||
	(self.parent.frames["MainFrame"].document.getElementById('DisfigurementRate').value != '' &&
	self.parent.frames["MainFrame"].document.getElementById('DisfigurementRate').value != '$0.00') ||
	(self.parent.frames["MainFrame"].document.getElementById('LifePensionRate').value != '' &&
	self.parent.frames["MainFrame"].document.getElementById('LifePensionRate').value != '$0.00') ||
	(self.parent.frames["MainFrame"].document.getElementById('VocRehabTDRate').value != '' &&
	self.parent.frames["MainFrame"].document.getElementById('VocRehabTDRate').value != '$0.00'))
	    return true;
	else
	    return false;
}

function CheckMedicalWCBSave()
{
    if (self.parent.frames["MainFrame"].document.getElementById('Amt1MedWC') != null)
    {
        if ((self.parent.frames["MainFrame"].document.getElementById('Amt1MedWC').value != '' &&
        self.parent.frames["MainFrame"].document.getElementById('Amt1MedWC').value != '$0.00') || 
        (self.parent.frames["MainFrame"].document.getElementById('Amt2MedWC').value != '' &&
        self.parent.frames["MainFrame"].document.getElementById('Amt2MedWC').value != '$0.00') ||
        (self.parent.frames["MainFrame"].document.getElementById('Amt3MedWC').value != '' &&
        self.parent.frames["MainFrame"].document.getElementById('Amt3MedWC').value != '$0.00') ||
	    (self.parent.frames["MainFrame"].document.getElementById('Amt4MedWC').value != '' &&
	    self.parent.frames["MainFrame"].document.getElementById('Amt4MedWC').value != '$0.00') ||
	    (self.parent.frames["MainFrame"].document.getElementById('Amt5MedWC').value != '' &&
	    self.parent.frames["MainFrame"].document.getElementById('Amt5MedWC').value != '$0.00') ||
	    (self.parent.frames["MainFrame"].document.getElementById('Amt6MedWC').value != '' &&
	    self.parent.frames["MainFrame"].document.getElementById('Amt6MedWC').value != '$0.00') ||
	    (self.parent.frames["MainFrame"].document.getElementById('Amt7MedWC').value != '' &&
	    self.parent.frames["MainFrame"].document.getElementById('Amt7MedWC').value != '$0.00') ||
	    (self.parent.frames["MainFrame"].document.getElementById('Amt8MedWC').value != '' &&
	    self.parent.frames["MainFrame"].document.getElementById('Amt8MedWC').value != '$0.00') ||
	    (self.parent.frames["MainFrame"].document.getElementById('Amt9MedWC').value != '' &&
	    self.parent.frames["MainFrame"].document.getElementById('Amt9MedWC').value != '$0.00') ||
	    (self.parent.frames["MainFrame"].document.getElementById('Amt10MedWC').value != '' &&
	    self.parent.frames["MainFrame"].document.getElementById('Amt10MedWC').value != '$0.00') ||
	    (self.parent.frames["MainFrame"].document.getElementById('Amt11MedWC').value != '' &&
	    self.parent.frames["MainFrame"].document.getElementById('Amt11MedWC').value != '$0.00') ||
	    (self.parent.frames["MainFrame"].document.getElementById('Amt12MedWC').value != '' &&
	    self.parent.frames["MainFrame"].document.getElementById('Amt12MedWC').value != '$0.00'))
	        return true;
	    else
	        return false;
	}
}

function CheckTempDisWCBSave()
{
    if (self.parent.frames["MainFrame"].document.getElementById('Amt1TemWC') != null)
    {
        if ((self.parent.frames["MainFrame"].document.getElementById('Amt1TemWC').value != '' &&
        self.parent.frames["MainFrame"].document.getElementById('Amt1TemWC').value != '$0.00') || 
        (self.parent.frames["MainFrame"].document.getElementById('Amt2TemWC').value != '' &&
        self.parent.frames["MainFrame"].document.getElementById('Amt2TemWC').value != '$0.00') || 
        (self.parent.frames["MainFrame"].document.getElementById('Amt3TemWC').value != '' &&
        self.parent.frames["MainFrame"].document.getElementById('Amt3TemWC').value != '$0.00'))
	        return true;
	    else
	        return false;
    }
}

function CheckPermDisWCBSave()
{
    if (self.parent.frames["MainFrame"].document.getElementById('Amt1PerWC') != null)
    {
        if ((self.parent.frames["MainFrame"].document.getElementById('Amt1PerWC').value != '' &&
        self.parent.frames["MainFrame"].document.getElementById('Amt1PerWC').value != '$0.00') || 
        (self.parent.frames["MainFrame"].document.getElementById('Amt2PerWC').value != '' &&
        self.parent.frames["MainFrame"].document.getElementById('Amt2PerWC').value != '$0.00') ||
        (self.parent.frames["MainFrame"].document.getElementById('Amt3PerWC').value != '' &&
        self.parent.frames["MainFrame"].document.getElementById('Amt3PerWC').value != '$0.00') ||
	    (self.parent.frames["MainFrame"].document.getElementById('Amt4PerWC').value != '' &&
	    self.parent.frames["MainFrame"].document.getElementById('Amt4PerWC').value != '$0.00') ||
	    (self.parent.frames["MainFrame"].document.getElementById('Amt5PerWC').value != '' &&
	    self.parent.frames["MainFrame"].document.getElementById('Amt5PerWC').value != '$0.00') ||	
	    (self.parent.frames["MainFrame"].document.getElementById('Amt6PerWC').value != '' &&
	    self.parent.frames["MainFrame"].document.getElementById('Amt6PerWC').value != '$0.00') ||	
	    (self.parent.frames["MainFrame"].document.getElementById('Amt7PerWC').value != '' &&
	    self.parent.frames["MainFrame"].document.getElementById('Amt7PerWC').value != '$0.00') ||
	    (self.parent.frames["MainFrame"].document.getElementById('Amt8PerWC').value != '' &&
	    self.parent.frames["MainFrame"].document.getElementById('Amt8PerWC').value != '$0.00') ||
	    (self.parent.frames["MainFrame"].document.getElementById('Amt9PerWC').value != '' &&
	    self.parent.frames["MainFrame"].document.getElementById('Amt9PerWC').value != '$0.00') ||
	    (self.parent.frames["MainFrame"].document.getElementById('Amt10PerWC').value != '' &&
	    self.parent.frames["MainFrame"].document.getElementById('Amt10PerWC').value != '$0.00') ||
	    (self.parent.frames["MainFrame"].document.getElementById('Amt11PerWC').value != '' &&
	    self.parent.frames["MainFrame"].document.getElementById('Amt11PerWC').value != '$0.00') ||
	    (self.parent.frames["MainFrame"].document.getElementById('Amt12PerWC').value != '' &&
	    self.parent.frames["MainFrame"].document.getElementById('Amt12PerWC').value != '$0.00') ||
	    (self.parent.frames["MainFrame"].document.getElementById('Amt13PerWC').value != '' &&
	    self.parent.frames["MainFrame"].document.getElementById('Amt13PerWC').value != '$0.00') ||
	    (self.parent.frames["MainFrame"].document.getElementById('Amt14PerWC').value != '' &&
	    self.parent.frames["MainFrame"].document.getElementById('Amt14PerWC').value != '$0.00'))
	        return true;
	    else
	        return false;
    }
}

function CheckVocRehabWCBSave()
{
    if (self.parent.frames["MainFrame"].document.getElementById('Amt1VocWC') != null)
    {
        if ((self.parent.frames["MainFrame"].document.getElementById('Amt1VocWC').value != '' &&
        self.parent.frames["MainFrame"].document.getElementById('Amt1VocWC').value != '$0.00') || 
        (self.parent.frames["MainFrame"].document.getElementById('Amt2VocWC').value != '' &&
        self.parent.frames["MainFrame"].document.getElementById('Amt2VocWC').value != '$0.00') ||
        (self.parent.frames["MainFrame"].document.getElementById('Amt3VocWC').value != '' &&
        self.parent.frames["MainFrame"].document.getElementById('Amt3VocWC').value != '$0.00') ||
	    (self.parent.frames["MainFrame"].document.getElementById('Amt4VocWC').value != '' &&
	    self.parent.frames["MainFrame"].document.getElementById('Amt4VocWC').value != '$0.00') ||
	    (self.parent.frames["MainFrame"].document.getElementById('Amt5VocWC').value != '' &&
	    self.parent.frames["MainFrame"].document.getElementById('Amt5VocWC').value != '$0.00'))
	        return true;
	    else
	        return false;
	}
}

function CheckLegalWCBSave()
{
    if (self.parent.frames["MainFrame"].document.getElementById('Amt1LegWC') != null)
    {
        if ((self.parent.frames["MainFrame"].document.getElementById('Amt1LegWC').value != '' &&
        self.parent.frames["MainFrame"].document.getElementById('Amt1LegWC').value != '$0.00') || 
        (self.parent.frames["MainFrame"].document.getElementById('Amt2LegWC').value != '' &&
        self.parent.frames["MainFrame"].document.getElementById('Amt2LegWC').value != '$0.00') ||
        (self.parent.frames["MainFrame"].document.getElementById('Amt3LegWC').value != '' &&
        self.parent.frames["MainFrame"].document.getElementById('Amt3LegWC').value != '$0.00') ||
        (self.parent.frames["MainFrame"].document.getElementById('Amt4LegWC').value != '' &&
        self.parent.frames["MainFrame"].document.getElementById('Amt4LegWC').value != '$0.00') ||
        (self.parent.frames["MainFrame"].document.getElementById('Amt5LegWC').value != '' &&
        self.parent.frames["MainFrame"].document.getElementById('Amt5LegWC').value != '$0.00'))
	        return true;
	    else
	        return false;
	}
}

function CheckAllExpWCBSave()
{
    if (self.parent.frames["MainFrame"].document.getElementById('Amt1AllWC') != null)
    {
        if ((self.parent.frames["MainFrame"].document.getElementById('Amt1AllWC').value != '' &&
        self.parent.frames["MainFrame"].document.getElementById('Amt1AllWC').value != '$0.00') || 
        (self.parent.frames["MainFrame"].document.getElementById('Amt2AllWC').value != '' &&
        self.parent.frames["MainFrame"].document.getElementById('Amt2AllWC').value != '$0.00') || 
        (self.parent.frames["MainFrame"].document.getElementById('Amt3AllWC').value != '' &&
        self.parent.frames["MainFrame"].document.getElementById('Amt3AllWC').value != '$0.00') || 
        (self.parent.frames["MainFrame"].document.getElementById('Amt4AllWC').value != '' &&
        self.parent.frames["MainFrame"].document.getElementById('Amt4AllWC').value != '$0.00'))
	        return true;
	    else
	        return false;
	}
}

function CheckReserveApprovalScreen()
{
    //get the current URL 
    var url = self.parent.window.opener.location.toString();
    //get the parameters 
    url.match(/\?(.+)$/); 
    var params = RegExp.$1; 
    
    //split up the query string and store in an associative array 
    var params = params.split("&"); 
    var queryStringList = {};  
    for(var i=0;i<1;i++)
    {
        var tmp = params[i].split(",");
        if (tmp[0].indexOf("ReserveApproval") > 0)
        {
            return true
        }
    }
    return false;
}

function RSWpercentLostFocus(objCtrl)
{
	if (!objCtrl.getAttribute('readonly'))
    {
	    var dbl=new String(objCtrl.value);
	    dbl=dbl.replace(/[,\$]/g ,"");
	    
	    if(dbl.length==0)
	    {
	        objCtrl.value="";
		    return false;
	    }
	    
	    if(isNaN(parseFloat(dbl)))
	    {
		    objCtrl.value="";
		    return false;
		}
		
		//for Est. Collection 			
	    if(dbl.indexOf('(') >= 0)
	        dbl=dbl.replace(/[\(\)]/g ,"");
	     
		if(dbl.indexOf('-') >= 0)
		    dbl=dbl.replace(/[\-]/g ,"");
		
		dbl=parseFloat(dbl);
    	
	    objCtrl.value=dbl.toFixed(2);
	    
	    if(objCtrl.value > 100)
            objCtrl.value = "0.00%";
        else if (objCtrl.id == "Amt2PercentApportionPerWC")
        {
            if (objCtrl.value < 70)
                objCtrl.value = "0.00%";
            else
                objCtrl.value = objCtrl.value  + "%"
        }
        else if (objCtrl.value < 0)
            objCtrl.value = "0.00%";
        else
            objCtrl.value = objCtrl.value  + "%"
            
	    AutoCalApport(objCtrl);
	    return true;
	}
	else
	    return false;
}

function AutoCalApport(objctrl) {
    
    var ival = objctrl.value;
        
    if (objctrl.id == "Amt1PercentPerWC")
      document.getElementById('Amt1PercentAfterApportionPerWC').innerText = ConvertDblToPer((ConvertStrToDbl(ival)/100)*(1-(ConvertStrToDbl(document.getElementById('Amt1PercentApportionPerWC').value)/100)));
    
    if (objctrl.id == "Amt1PercentApportionPerWC")
      document.getElementById('Amt1PercentAfterApportionPerWC').innerText = ConvertDblToPer((ConvertStrToDbl(document.getElementById('Amt1PercentPerWC').value)/100)*(1-(ConvertStrToDbl(ival)/100))*100);
    
    //Nothing for Amt2PercentApportionPerWC
    
    if (objctrl.id == "Amt9PercentPerWC")
      document.getElementById('Amt9PerWC').innerText = ConvertDblToCurr(Math.round((ConvertStrToDbl(ival) * ConvertStrToDbl(document.getElementById('Amt9WeeksPerWC').value) * ConvertStrToDbl(document.getElementById('Amt9RatePerWC').value))/100));
    
    if (objctrl.id == "Amt10PercentPerWC")
      document.getElementById('Amt10PerWC').innerText = ConvertDblToCurr(Math.round((ConvertStrToDbl(ival) * ConvertStrToDbl(document.getElementById('Amt10WeeksPerWC').value) * ConvertStrToDbl(document.getElementById('Amt10RatePerWC').value))/100));
      
    if (objctrl.id == "Amt11PercentPerWC")
      document.getElementById('Amt11PerWC').innerText = ConvertDblToCurr(Math.round((ConvertStrToDbl(ival) * ConvertStrToDbl(document.getElementById('Amt11WeeksPerWC').value) * ConvertStrToDbl(document.getElementById('Amt11RatePerWC').value))/100));
}
//Aman Multicurrency --Start
function RSW_MultiCurrencyOnBlur(objCtrl) {
    functionname = objCtrl.getAttributeNode("onblur").value;       
    functionname = functionname.split('(')[0];
    window[functionname](objCtrl);
}

function RSW_MultiCurrencyToDecimal(objCtl) {
    var functionname = objCtl.getAttributeNode("onfocus").value;
    //remove 'format_' word
    functionname = functionname.replace("Format_", "");
    functionname = functionname.split('(')[0];
    window[functionname](objCtl);
}
function RSW_HiddentoMultiCurrencyType(mcControl, objCtl) {
    var functionname = mcControl.getAttributeNode("onblur").value;
    //remove 'format_' word
    functionname = functionname.replace("Format_", "");
    functionname = functionname.split('(')[0];
    objCtl.value = window[functionname](objCtl);
}
//Aman Multicurrency --End