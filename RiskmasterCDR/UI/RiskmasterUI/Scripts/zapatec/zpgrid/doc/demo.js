	zpDemo.zpdefDemo = "phone.html";
	zpDemo.zpdemos = {
		
			"stock.html": {title: "Grid With Tooltips", url: "../demo/stock.html", css: ["default"]},
			"phone.html": {title: "Phone Bill", url: "../demo/phone.html", css: ["winxp"]},
			"phone_ondemand.html": {title: "Zapatec Server-Side Grid", url: "../demo/phone_ondemand.html", css: ["winxp"]},
			"phone_ondemand_xml.html": {title: "Zapatec Server-Side XML Grid", url: "../demo/phone_ondemand_xml.html", css: ["winxp"]},
			"hidden.html": {title: "Phone Bill with Hidden Column", url: "../demo/hidden.html", css: ["winxp"]},
			"json_demo.html": {title: "Load Grid from Url", url: "../demo/json_demo.html", css: ["winxp"]},
			"json_hidden.html": {title: "Hidden Columns", url: "../demo/json_hidden.html", css: ["winxp"]},
			"yahoo.html": {title: "Yahoo Search", url: "../demo/yahoo.html", css: ["default"]},
			"yahoo_ondemand.html": {title: "Cross-Domain Server-Side Grid", url: "../demo/yahoo_ondemand.html", css: ["default"]},
			"asis.html": {title: "Show Data AS IS", url: "../demo/asis.html", css: ["default"]},
			"splice.html": {title: "Insert, replace and delete rows using splice method", url: "../demo/splice.html", css: ["lightblue"]},
			"splicexml.html": {title: "Insert, replace and delete rows using spliceXml method", url: "../demo/splicexml.html", css: ["lightblue"]},
			"splice_columns.html": {title: "Insert, replace, delete and rename columns", url: "../demo/splice_columns.html", css: ["lightblue"]},
			"custom_type.html": {title: "Custom Data Types", url: "../demo/custom_type.html", css: ["winxp"]},
			"query.html": {title: "Insert, Replace and Delete Rows", url: "../demo/query.html", css: ["lightblue"]},
			"query_alternative.html": {title: "Insert, Replace and Delete Alternative", url: "../demo/query_alternative.html", css: ["lightblue"]},
			"chart.html": {title: "Charts Integration", url: "../demo/chart.html", css: ["winxp"]},
			"scroll.html": {title: "Scrolling Grid", url: "../demo/scroll.html", css: ["winxp"]},
			"scroll_with_sliders.html": {title: "Scrolling Grid with Sliders", url: "../demo/scroll_with_sliders.html", css: ["winxp"]},
			"select_multiple_rows.html": {title: "Phone Bill with Multiple Select", url: "../demo/select_multiple_rows.html", css: ["winxp"]},
			"select_multiple_cells.html": {title: "Phone Bill with Multiple Select Cells", url: "../demo/select_multiple_cells.html", css: ["winxp"]},
			"custom_pagination.html": {title: "Phone Bill with Custom Pagination", url: "../demo/custom_pagination.html", css: ["winxp"]},
			"editable.html": {title: "Zapatec Editable Grid ", url: "../demo/editable.html", css: ["winxp"]},
			"editable_horizontal.html": {title: "Zapatec Editable Horizontal Grid ", url: "../demo/editable_horizontal.html", css: ["lightblue"]},
			"editable_autosave_cell.html": {title: "Zapatec Editable Grid Example With Auto Cell Saving", url: "../demo/editable_autosave_cell.html", css: ["winxp"]},
			"editable_xml.html": {title: "Zapatec Grid communications with server using XML", url: "../demo/editable_xml.html", css: ["winxp"]},
			"editable_json.html": {title: "Zapatec Grid communications with server using JSON", url: "../demo/editable_json.html", css: ["winxp"]},
			"countries.html": {title: "World Info With Pie Chart", url: "../demo/countries.html", css: ["winxp"]},
			"span.html": {title: "Span Multiple Columns", url: "../demo/span.html", css: ["winxp"]},
			"editors.html": {title: "External Editors", url: "../demo/editors.html", css: ["winxp"]},
			"text_insensitive.html": {title: "Case Insensitive Sorting", url: "../demo/text_insensitive.html", css: ["lightblue"]},
			"aggregates.html": {title: "Phone Bill With Totals", url: "../demo/aggregates.html", css: ["winxp"]},
			"dataprepared.html": {title: "Zapatec Extremely Large Grid", url: "../demo/dataprepared.html", css: ["lightblue"]},
			"dataprepared_without_pagination.html": {title: "Zapatec Extremely Large Grid", url: "../demo/dataprepared_without_pagination.html", css: ["lightblue"]},
			"autoresize.html": {title: "Dynamic Grid Adjustment To Parent Window", url: "../demo/autoresize.html", css: ["winxp"]},
			"autoresize-pane.html": {title: "Dynamic Grid Adjustment To Parent Pane", url: "../demo/autoresize-pane.html", css: ["winxp"]},
			"show_hide_columns.html": {title: "Show/Hide Columns Dynamically", url: "../demo/show_hide_columns.html", css: ["winxp"]},
			"realtime_update_json.html": {title: "Real Time Update With JSON", url: "../demo/realtime_update_json.html", css: ["lightblue"]},
			"realtime_update_xml.html": {title: "Real Time Update With XML", url: "../demo/realtime_update_xml.html", css: ["winxp"]},
			"select_rows_with_checkboxes.html": {title: "Multiple Row Selection With Checkboxes", url: "../demo/select_rows_with_checkboxes.html", css: ["winxp"]},
			"sort_by_multiple_columns.html": {title: "Sort By Several Columns Simultaneously", url: "../demo/sort_by_multiple_columns.html", css: ["lightblue"]},
			"toolbar.html": {title: "Grid With Toolbar", url: "../demo/toolbar.html", css: ["winxp"]},
			"context_menu.html": {title: "Grid With Context Menu", url: "../demo/context_menu.html", css: ["lightblue"]},
			"tooltips.html": {title: "Grid With Row Tooltips", url: "../demo/tooltips.html", css: ["winxp"]},
			"select_rows_with_radios.html": {title: "Multiple Row Selection With Radios", url: "../demo/select_rows_with_radios.html", css: ["winxp"]},
			"select_rows_with_checkboxes_json.html": {title: "Multiple Row Selection With Checkboxes - JSON", url: "../demo/select_rows_with_checkboxes_json.html", css: ["winxp"]},
			"select_rows_with_radios_json.html": {title: "Multiple Row Selection With Radios - JSON", url: "../demo/select_rows_with_radios_json.html", css: ["winxp"]},
			"horizontal.html": {title: "Horizontal Grid Layout", url: "../demo/horizontal.html", css: ["winxp"]},
			"compare.html": {title: "Compare Rows", url: "../demo/compare.html", css: ["winxp"]},
			"cell_style.html": {title: "Custom Style Of a Cell/Row/Column", url: "../demo/cell_style.html", css: ["winxp"]},
			"double.html": {title: "Two Grids Sharing Controls", url: "../demo/double.html", css: ["winxp"]},
			"dndcells.html": {title: "Drag and Drop Cells", url: "../demo/dndcells.html", css: ["winxp"]},
			"dndcolumns.html": {title: "Drag and Drop Columns", url: "../demo/dndcolumns.html", css: ["winxp"]},
			"spancells.html": {title: "Colspan And Rowspan", url: "../demo/spancells.html", css: ["winxp"]},
			"spancells_json.html": {title: "Colspan And Rowspan With JSON Data Source", url: "../demo/spancells_json.html", css: ["winxp"]},
			"spancells_xml.html": {title: "Colspan And Rowspan With XML Data Source", url: "../demo/spancells_xml.html", css: ["winxp"]},
			"scroll_stretched.html": {title: "Scrolling Grid Stretched to Container Width", url: "../demo/scroll_stretched.html", css: ["winxp"]}
	};
	zpDemo.zpdemogroups = {
	'AJAX':['phone_ondemand.html', 'phone_ondemand_xml.html', 'json_demo.html', 'editable_autosave_cell.html', 'editable_json.html' , 'editable_xml.html', 'realtime_update_json.html', 'realtime_update_xml.html', 'select_rows_with_checkboxes_json.html', 'select_rows_with_radios_json.html'], 'Queries':['splice.html', 'splicexml.html', 'splice_columns.html', 'query.html', 'query_alternative.html', 'select_rows_with_checkboxes.html', 'select_rows_with_radios.html', 'select_rows_with_checkboxes_json.html', 'select_rows_with_radios_json.html', 'compare.html'], 'Edit':['editable.html', 'editors.html', 'editable_horizontal.html'], 'Presentation':['scroll.html','scroll_with_sliders.html','stock.html', 'tooltips.html', 'horizontal.html', 'context_menu.html', 'toolbar.html', 'show_hide_columns.html', 'autoresize.html', 'autoresize-pane.html', 'aggregates.html', 'span.html', 'cell_style.html', 'dndcells.html', 'dndcolumns.html', 'scroll_stretched.html'], 'Charts':['chart.html', 'countries.html'], 'Advanced':['asis.html', 'editors.html', 'dataprepared.html', 'dataprepared_without_pagination.html', 'double.html', 'spancells.html', 'spancells_json.html', 'spancells_xml.html']
	};

	// Name of this widget
	zpDemo.strWidget='Grid'
	// path to css for this widget
	zpDemo.cssPath='../themes'

