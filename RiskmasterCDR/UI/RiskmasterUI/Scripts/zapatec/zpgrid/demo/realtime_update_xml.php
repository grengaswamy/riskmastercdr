<?php
/**
 * Part of Zapatec Grid realtime updates example. Generates random changes for
 * the Grid.
 *
 * Copyright (c) 2004-2006 by Zapatec, Inc.
 * http://www.zapatec.com
 * 1700 MLK Way, Berkeley, California,
 * 94709, U.S.A.
 * All rights reserved.
 */

/* $Id: realtime_update_xml.php 7323 2007-06-01 21:05:51Z alex $ */

// Array with company names used in this example
$aCompanies = array(
 "Alcoa Inc",
 "AT&amp;T Inc.",
 "General Electric Company",
 "General Motors Corporation",
 "Intel Corporation",
 "Microsoft Corporation",
 "Pfizer Inc",
 "The Home Depot, Inc.",
 "Verizon Communications",
 "Walt Disney Company"
);

// Forms XML fragment passed to spliceXml method of the Grid
function formChange() {
	global $aCompanies;

	// Get random company name
	$sCompany = $aCompanies[mt_rand(0, 9)];

	// Get random price
	$fPrice = mt_rand(2000, 4000) / 100;

	// Get random price change
	$fChange = mt_rand(1, 30) / 100;

	// Get randomly minus or plus
	$iMinus = mt_rand(0, 1);
	if ($iMinus) {
		$fChange = -$fChange;
	}

	// Calculate % change
	$fPercent = round($fChange * 100 / ($fPrice - $fChange), 2);

	// Get current time
	$iTime = time();

	// Form XML fragment for spliceXml
	return "<table><atkey>$sCompany</atkey><howmany>1</howmany><rows><row>" .
	 "<cell>$sCompany</cell>" .
	 "<cell>" . sprintf("%01.2f", $fPrice) . "</cell>" .
	 "<cell>" . sprintf("%+01.2f", $fChange) . "</cell>" .
	 "<cell>" . sprintf("%+01.2f", $fPercent) . "</cell>" .
	 "<cell>$iTime</cell>" .
	 "</row></rows></table>";

}

header("Content-type: text/xml");
echo "<?xml version=\"1.0\"?><grid>";

// Change random number of rows
$iChanges = mt_rand(1, 5);
for ($iChange = 0; $iChange < $iChanges; $iChange++) {
	echo formChange();
}

echo "</grid>";

?>