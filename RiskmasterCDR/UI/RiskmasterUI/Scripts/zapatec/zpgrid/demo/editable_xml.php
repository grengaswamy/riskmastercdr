<?php
/**
 * Part of Zapatec Grid communications with server example.
 *
 * Copyright (c) 2004-2006 by Zapatec, Inc.
 * http://www.zapatec.com
 * 1700 MLK Way, Berkeley, California,
 * 94709, U.S.A.
 * All rights reserved.
 */

/* $Id: editable_xml.php 7323 2007-06-01 21:05:51Z alex $ */

// Get arguments in XML format
if (empty($_POST['changes'])) {
	echo "Changes were not found.";
	exit;
}

// Parse XML
preg_match_all(
 '/<item>([^<]+)<\/item><column>([^<]+)<\/column><value>([^<]+)<\/value>/',
 $_POST['changes'], $out, PREG_PATTERN_ORDER);

if (!count($out[0])) {
	echo "Changes were not found.";
	exit;
}

$arrColumns = array(
	'Item',
	'Date',
	'Time',
	'Rate Period',
	'Phone Number',
	'Minutes',
	'Total Charges',
	'Local'
);

echo "Following changes were done:\n";

for ($iChange = 0; $iChange < count($out[0]); $iChange++) {
	echo 'Item #' . $out[1][$iChange] . ': new value of "' .
	 $arrColumns[$out[2][$iChange]] . '" is "' . $out[3][$iChange] . "\"\n";
}

?>
