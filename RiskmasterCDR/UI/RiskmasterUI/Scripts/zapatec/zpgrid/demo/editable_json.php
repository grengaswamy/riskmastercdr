<?php
/**
 * Part of Zapatec Grid communications with server example.
 *
 * Copyright (c) 2004-2006 by Zapatec, Inc.
 * http://www.zapatec.com
 * 1700 MLK Way, Berkeley, California,
 * 94709, U.S.A.
 * All rights reserved.
 */

/* $Id: editable_json.php 7323 2007-06-01 21:05:51Z alex $ */

require_once 'JSON.php';
$objJsonConverter = new Services_JSON();

// Get arguments in JSON format
$objArgs = array();
if (!empty($_POST['changes'])) {
	$objChanges = $objJsonConverter->decode(stripslashes($_POST['changes']));
}

if (!isset($objChanges) || !count($objChanges)) {
	echo "Changes were not found.";
	exit;
}

$arrColumns = array(
	'Item',
	'Date',
	'Time',
	'Rate Period',
	'Phone Number',
	'Minutes',
	'Total Charges',
	'Local'
);

echo "Following changes were done:\n";

for ($iChange = 0; $iChange < count($objChanges); $iChange++) {
	echo 'Item #' . $objChanges[$iChange]->{'item'} . ': new value of "' .
	 $arrColumns[$objChanges[$iChange]->{'column'}] . '" is "' .
	 $objChanges[$iChange]->{'value'} . "\"\n";
}

?>
