<?php
/**
 * Part of Zapatec Grid data on demand example.
 *
 * Copyright (c) 2004-2006 by Zapatec, Inc.
 * http://www.zapatec.com
 * 1700 MLK Way, Berkeley, California,
 * 94709, U.S.A.
 * All rights reserved.
 */

/* $Id: phone_ondemand.php 7951 2007-08-22 13:46:01Z alex $ */

require_once 'JSON.php';
$oJsonConverter = new Services_JSON();

// Get arguments in JSON format
$oArgs = array();
if (!empty($_POST['args'])) {
	$oArgs = $oJsonConverter->decode(stripslashes($_POST['args']));
}

// Get data in JSON format from file
$oData = $oJsonConverter->decode(file_get_contents('json_phone.txt'));

// Returns cell value to display
function getCellValueString($oCell) {
	$sValue = '';
	if (isset($oCell->{'v'})) {
		$sValue = $oCell->{'v'};
	}
	return $sValue;
}

// Returns cell value to compare
function getCellValueCompare($oCell, $iCol) {
	global $oData;
	if (isset($oCell->{'c'})) {
		return $oCell->{'c'};
	}
	// In this example convertion is done only for "date" data type
	$aFields = $oData->{'fields'};
	if (!empty($aFields[$iCol]->{'dataType'}) &&
	 $aFields[$iCol]->{'dataType'} == 'date') {
		// "date" data type is converted into number of milliseconds since
		// January 1, 1970, 00:00:00
		$aDate = split('/', getCellValueString($oCell));
		if (count($aDate) != 3) {
			return 0;
		} else {
			return mktime(0, 0, 0, $aDate[0], $aDate[1], $aDate[2]) * 1000;
		}
	} else {
		// Other data types are converted into string
		return getCellValueString($oCell);
	}
}

// Comparison function
function cmpKeys($left, $right) {
	if ($left{'c'} == $right{'c'}) {
		return 0;
	}
	return ($left{'c'} < $right{'c'}) ? -1 : 1;
}

// Returns range of values of the specified column
function getColumnRange($oData, $iCol) {
	// Get array of keys
	$aKeys = array();
	// Auxiliary associative array
	$oKeys = array();
	// Iterate over rows
	if (empty($oData) || empty($oData->{'rows'})) {
		return null;
	}
	$aRows = $oData->{'rows'};
	$iRows = count($aRows);
	for ($iRow = 0; $iRow < $iRows; $iRow++) {
		// Get row
		if (empty($aRows[$iRow]) || empty($aRows[$iRow]->{'cells'})) {
			continue;
		}
		$oRow = $aRows[$iRow];
		// Get cell
		if (empty($aRows[$iRow]->{'cells'}[$iCol])) {
			continue;
		}
		$oCell = $aRows[$iRow]->{'cells'}[$iCol];
		// Get cell value
		$sKey = getCellValueString($oCell);
		if (!isset($oKeys[$sKey])) {
			array_push($aKeys, array(
				'v' => $sKey,
				'c' => getCellValueCompare($oCell, $iCol)
			));
			$oKeys[$sKey] = true;
		}
	}
	$iKeys = count($aKeys);
	if (!$iKeys) {
		// Empty array
		return null;
	}
	// Sort array of keys
	usort($aKeys, "cmpKeys");
	// Return range of column values
	return array(
		'min' => $aKeys[0]{'c'},
		'minValue' => $aKeys[0]{'v'},
		'max' => $aKeys[$iKeys - 1]{'c'},
		'maxValue' => $aKeys[$iKeys - 1]{'v'},
		'values' => $aKeys
	);
}

if (!empty($oData) && !empty($oData->{'fields'}) &&
 !empty($oData->{'rows'})) {
	$aFields = $oData->{'fields'};
	$iCols = count($aFields);
	$aRows = $oData->{'rows'};

	// Set totalRows
	$oData->{'totalRows'} = count($aRows);

	// Set columnRange for the 2-nd column
	if (!empty($aFields[1])) {
		$oData->{'fields'}[1]->{'columnRange'} = getColumnRange($oData, 1);
	}
	// Set columnRange for the 4-th column
	if (!empty($aFields[3])) {
		$oData->{'fields'}[3]->{'columnRange'} = getColumnRange($oData, 3);
	}
	// Set columnRange for the 6-th column
	if (!empty($aFields[5])) {
		$oData->{'fields'}[5]->{'columnRange'} = getColumnRange($oData, 5);
	}

	// Apply filters
	if (!empty($oArgs) && !empty($oArgs->{'filters'})) {
		$aFilters = $oArgs->{'filters'};
		// Columns having regexp filter
		$aRegexpFilters = array();
		// Columns having text filter
		$aTextFilters = array();
		// Iterate over columns
		for ($iCol = 0; $iCol < $iCols; $iCol++) {
			if (empty($aFilters[$iCol])) {
				continue;
			}
			$oFilter = $aFilters[$iCol];
			// Set filters
			if (!empty($oFilter->{'hiddenValues'})) {
				$oData->{'fields'}[$iCol]->{'hiddenValues'} =
				 $oFilter->{'hiddenValues'};
			}
			if (isset($oFilter->{'minValue'})) {
				$oData->{'fields'}[$iCol]->{'minValue'} = $oFilter->{'minValue'};
			}
			if (isset($oFilter->{'maxValue'})) {
				$oData->{'fields'}[$iCol]->{'maxValue'} = $oFilter->{'maxValue'};
			}
			if (isset($oFilter->{'regexpFilter'})) {
				$oData->{'fields'}[$iCol]->{'regexpFilter'} =
				 $oFilter->{'regexpFilter'};
			}
			if (isset($oFilter->{'textFilter'})) {
				$oData->{'fields'}[$iCol]->{'textFilter'} =
				 $oFilter->{'textFilter'};
			}
			// Apply filters
			if (!empty($oFilter->{'hiddenValues'}) ||
			 isset($oFilter->{'minValue'}) || isset($oFilter->{'maxValue'})) {
				// Iterate over rows
				for ($iRow = count($aRows) - 1; $iRow >= 0; $iRow--) {
					$oRow = $aRows[$iRow];
					if (empty($oRow) || empty($oRow->{'cells'}) ||
					 empty($oRow->{'cells'}[$iCol])) {
						// Remove row
						array_splice($aRows, $iRow, 1);
						continue;
					}
					// Get cell
					$oCell = $oRow->{'cells'}[$iCol];
					// Remove row if value of the cell is hidden
					if (!empty($oFilter->{'hiddenValues'}) &&
					 array_search(getCellValueString($oCell),
						$oFilter->{'hiddenValues'}) !== false) {
						// Remove row
						array_splice($aRows, $iRow, 1);
						continue;
					}
					// Remove row if value of the cell is lesser then min value
					if (isset($oFilter->{'minValue'}) &&
					 $oFilter->{'minValue'} > getCellValueCompare($oCell, $iCol)) {
						// Remove row
						array_splice($aRows, $iRow, 1);
						continue;
					}
					// Remove row if value of the cell is greater then max value
					if (isset($oFilter->{'maxValue'}) &&
					 $oFilter->{'maxValue'} < getCellValueCompare($oCell, $iCol)) {
						// Remove row
						array_splice($aRows, $iRow, 1);
						continue;
					}
				}
			}
			// Check regexp filter
			if (isset($oFilter->{'regexpFilter'})) {
				array_push($aRegexpFilters, $iCol);
			}
			// Check text filter
			if (isset($oFilter->{'textFilter'})) {
				array_push($aTextFilters, $iCol);
			}
		}
		// Apply regexp filters
		$iRegexpFilters = count($aRegexpFilters);
		if ($iRegexpFilters) {
			// Iterate over rows
			for ($iRow = count($aRows) - 1; $iRow >= 0; $iRow--) {
				$oRow = $aRows[$iRow];
				// Indicates that row should be removed
				$bRemove = true;
				// Iterate over filters
				for ($iFilter = 0; $iFilter < $iRegexpFilters; $iFilter++) {
					// Column number
					$iCol = $aRegexpFilters[$iFilter];
					// Get cell
					$oCell = $oRow->{'cells'}[$iCol];
					// Get value
					$sValue = getCellValueString($oCell);
					// Get filter
					$sFilter = $aFilters[$iCol]->{'regexpFilter'};
					// Search text fragment
					if (preg_match('/' . $sFilter . '/', $sValue)) {
						$bRemove = false;
						break;
					}
				}
				// Remove row if text fragment not found
				if ($bRemove) {
					// Remove row
					array_splice($aRows, $iRow, 1);
				}
			}
		}
		// Apply text filters
		$iTextFilters = count($aTextFilters);
		if ($iTextFilters) {
			// Iterate over rows
			for ($iRow = count($aRows) - 1; $iRow >= 0; $iRow--) {
				$oRow = $aRows[$iRow];
				// Indicates that row should be removed
				$bRemove = true;
				// Iterate over filters
				for ($iFilter = 0; $iFilter < $iTextFilters; $iFilter++) {
					// Column number
					$iCol = $aTextFilters[$iFilter];
					// Get cell
					$oCell = $oRow->{'cells'}[$iCol];
					// Get value
					$sValue = getCellValueString($oCell);
					// Get filter
					$sFilter = $aFilters[$iCol]->{'textFilter'};
					// Search text fragment
					if (strpos($sValue, $sFilter) !== false) {
						$bRemove = false;
						break;
					}
				}
				// Remove row if text fragment not found
				if ($bRemove) {
					// Remove row
					array_splice($aRows, $iRow, 1);
				}
			}
		}
	}

	// Sort
	if (!empty($oArgs) && isset($oArgs->{'order'})) {
		$aOrder = $oArgs->{'order'};
		// Comparison function
		function cmpRows($left, $right) {
			global $aOrder;
			if (empty($left) || empty($right) || empty($left->{'cells'}) ||
			 empty($right->{'cells'})) {
				return 0;
			}
			$leftCells = $left->{'cells'};
			$rightCells = $right->{'cells'};
			for ($iCol = 0; $iCol < count($aOrder); $iCol++) {
				if (empty($aOrder[$iCol])) {
					continue;
				}
				$oCol = $aOrder[$iCol];
				if (!isset($oCol->{'col'}) || !isset($oCol->{'lt'}) ||
				 !isset($oCol->{'gt'})) {
					continue;
				}
				$iColNum = $oCol->{'col'};
				if (!isset($leftCells[$iColNum]) ||
				 !isset($rightCells[$iColNum])) {
					continue;
				}
				$leftVal = getCellValueCompare($leftCells[$iColNum], $iColNum);
				$rightVal = getCellValueCompare($rightCells[$iColNum], $iColNum);
				if ($leftVal == $rightVal) {
					continue;
				}
				if ($leftVal < $rightVal) {
					return $oCol->{'lt'};
				}
				return $oCol->{'gt'};
			}
			return 0;
		}
		// Sort data
		usort($aRows, "cmpRows");
	}

	// Set displayedRows
	$oData->{'displayedRows'} = count($aRows);

	// Apply paging (10 rows per page)
	$iRowsPerPage = 10;
	$iCurrentPage = 0;
	if (!empty($oArgs) && isset($oArgs->{'currentPage'})) {
		$iCurrentPage = $oArgs->{'currentPage'};
		if ($iCurrentPage < 0) {
			$iCurrentPage = 0;
		}
	}
	$iFirst = $iCurrentPage * $iRowsPerPage;
	if ($iFirst && $iFirst >= count($aRows)) {
		$iCurrentPage--;
		$iFirst = $iCurrentPage * $iRowsPerPage;
	}
	$aRows = array_slice($aRows, $iFirst, $iRowsPerPage);

	// Set currentPage
	$oData->{'currentPage'} = $iCurrentPage;

	$oData->{'rows'} = $aRows;
}

// Output result in JSON format
echo $oJsonConverter->encode($oData);

?>
