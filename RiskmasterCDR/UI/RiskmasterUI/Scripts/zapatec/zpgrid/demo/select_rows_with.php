<?php
/**
 * Part of Zapatec Grid example of multiple row selection with checkboxes.
 *
 * Copyright (c) 2004-2006 by Zapatec, Inc.
 * http://www.zapatec.com
 * 1700 MLK Way, Berkeley, California,
 * 94709, U.S.A.
 * All rights reserved.
 */

/* $Id: select_rows_with.php 7323 2007-06-01 21:05:51Z alex $ */

// Form response
$aResponse = array();
if (isset($_POST["checked"])) {
	$aResponse["received"] = stripcslashes($_POST["checked"]);
}

// Print response
require_once "JSON.php";
$oJsonConverter = new Services_JSON();
echo $oJsonConverter->encode($aResponse);

?>