<?php

// Check arguments
if (!isset($_POST['i']) || !isset($_POST['r']) || !isset($_POST['o'])) {
	exit;
}

$iCellId = $_POST['i'];
$iRowId = $_POST['r'];
$sValue = $_POST['o'];

// Check phone number format
if ($iCellId == 4) {
	if (preg_match('/^\(\d{3}\)[-\s\d]+$/', $sValue)) {
		// ok means success, otherwise error
		echo 'ok';
	}
} else {
	// In this example we don't check other columns
	echo 'ok';
}

?>