/**
 * @fileoverview Part of Zapatec Load Grid from URL demo.
 *
 * <pre>
 * Copyright (c) 2004-2007 by Zapatec, Inc.
 * http://www.zapatec.com
 * 1700 MLK Way, Berkeley, California,
 * 94709, U.S.A.
 * All rights reserved.
 * </pre>
 */

/* $Id: json_demo.js 7323 2007-06-01 21:05:51Z alex $ */

/**
 * Grid object.
 * @private
 */
var objGrid;

/**
 * Initializes grid from external source.
 *
 * @private
 * @param strUrl - source URL
 * @param strSourceType - source type: 'json/url', 'xml/url' or 'html/url'
 * @param strTheme - theme name
 */
function loadGrid(strUrl, strSourceType, strTheme) {
	objGrid = new Zapatec.Grid({

		// Use specified URL as source
		source: strUrl,
		sourceType: strSourceType,

		// Put the grid into element with id "gridContainer"
		container: 'gridContainer',

		// Use specified theme
		theme: strTheme,

		// Display filter out forms
		filterOut: [
			// Filter Date
			{
				// Use first column
				column: 0,
				// Put checkboxes into element with id "filterDate"
				container: 'filterDate'
			},
			// Filter Account
			{
				// Use second column
				column: 1,
				// Use custom function filterOutAccount to display checkboxes
				callback: filterOutAccount
			}
		]

	});
}

/**
 * Displays checkboxes to filter out Account.
 * @private
 */
function filterOutAccount(arrUniqueColumnValues) {
	var strHtml = '';
	for (var iVal = 0; iVal < arrUniqueColumnValues.length; iVal++) {
		strHtml +=
		 '<div>' + arrUniqueColumnValues[iVal].value +
		 ' <input type="checkbox" checked onclick="' +
		 arrUniqueColumnValues[iVal].onclick + '"/></div>';
	}
	document.getElementById('filterAccount').innerHTML = strHtml;
}

/**
 * Called when "Debits" button is clicked. Limits range of Amount to positive
 * numbers.
 * @private
 */
function limitRangeDebits(form) {
	if (objGrid) {
		objGrid.limitRange({
			column: 4,
			min: 0,
			max: Infinity
		});
	}
}

/**
 * Called when "Credits" button is clicked. Limits range of Amount to negative
 * numbers.
 * @private
 */
function limitRangeCredits(form) {
	if (objGrid) {
		objGrid.limitRange({
			column: 4,
			min: -Infinity,
			max: 0
		});
	}
}
