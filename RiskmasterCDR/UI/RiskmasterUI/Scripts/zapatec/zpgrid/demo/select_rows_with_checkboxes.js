/**
 * @fileoverview Part of Zapatec Grid Example Of Multiple Row Selection With
 * Checkboxes.
 *
 * <pre>
 * Copyright (c) 2004-2007 by Zapatec, Inc.
 * http://www.zapatec.com
 * 1700 MLK Way, Berkeley, California,
 * 94709, U.S.A.
 * All rights reserved.
 * </pre>
 */

/* $Id: select_rows_with_checkboxes.js 7323 2007-06-01 21:05:51Z alex $ */

/*
 * Check if demo was loaded from server
 */
if (document.location.toString().indexOf('http') != 0) {
	alert('Since this example demonstrates interaction between server and\
 javascript application, it must be loaded from server. This example does not\
 work if opened from local hard drive.');
}

/**
 * Will hold checked rows.
 * @ignore
 */
var oCheckedRows = {};

/**
 * Sends the rows picked as a POST using the Transport.
 * @ignore
 */
function submitChecked() {
	// Form value to send
	var aValues = [];
	for (var iRow in oCheckedRows) {
		if (oCheckedRows[iRow]) {
			aValues.push(oCheckedRows[iRow]);
		}
	}
	var sValue = aValues.join(';');
	// Show value
	showMsg('Following value will be sent to the server:', sValue);
	// Send value to the server
	Zapatec.Transport.fetchJsonObj({
		url: 'select_rows_with.php',
		method: 'POST',
		content: 'checked=' + sValue,
		onLoad: function(oResponse) {
			showMsg('Following value was received on server:', oResponse.received);
		}
	});
}

/**
 * Shows alert message.
 * @ignore
 */
function showMsg(sMsg, sData) {
	alert(sMsg + '\n' + sData.split(';').join(';\n'));
}

/**
 * Saves checkbox state in the corresponding row object.
 * @ignore
 */
function updateRow(oRow, bChecked) {
	if (bChecked) {
		// Pack row string to send to the server
		var aCells = oGrid.getRowCells(oRow);
		var aValues = [];
		for (var iCell = 1; iCell < aCells.length; iCell++) {
			aValues.push(oGrid.getCellValueString(aCells[iCell]));
		}
		// Put this row into checked array
		oCheckedRows[oGrid.getRowId(oRow)] = aValues.join(',');
		// Show this checkbox as checked after grid redrawing
		oGrid.setCellValue(oGrid.getCellByRow(oRow, 0),
		 '<input type="checkbox" onclick="checkboxOnClick()" checked="checked"/>');
	} else {
		// Remove this row from checked array
		oCheckedRows[oGrid.getRowId(oRow)] = null;
		// Show this checkbox as unchecked after grid redrawing
		oGrid.setCellValue(oGrid.getCellByRow(oRow, 0),
		 '<input type="checkbox" onclick="checkboxOnClick()"/>');
	}
}

/**
 * Returns checkbox for the specified row.
 * @ignore
 */
function getCheckboxByRow(oRow) {
	// Get first cell element
	var sCellId = 'zpGrid' + oGrid.getId() + 'Row' + oGrid.getRowId(oRow) +
	 'Cell0';
	var oCell = document.getElementById(sCellId);
	if (!oCell) {
		return null;
	}
	oCell = Zapatec.Utils.getFirstChild(oCell, 'div');
	// Return input element
	return Zapatec.Utils.getFirstChild(oCell, 'input');
}

/**
 * Selects/unselects a row.
 * @ignore
 */
function selectRow(oRow, bChecked) {
	// Save checkbox state
	updateRow(oRow, bChecked);
	// Check/uncheck checkbox
	var oCheckbox = getCheckboxByRow(oRow);
	if (!oCheckbox) {
		return;
	}
	oCheckbox.checked = bChecked;
}

/**
 * Selects/unselects all rows in the grid.
 * @ignore
 */
function selectAll(bSelect) {
	// For each row in the grid
	var aRows = oGrid.getFilteredRows();
	for (var iRow = 0; iRow < aRows.length; iRow++) {
		// Set checkbox
		selectRow(aRows[iRow], bSelect);
	}
	// Set "Select all rows on current page" checkbox
	document.getElementById('selectPageCheckbox').checked = bSelect;
}

/**
 * Selects/unselects all the rows on the current page.
 * @ignore
 */
function selectPage(bSelect) {
	// For each row on current page
	var aRows = oGrid.applyPaging();
	for (var iRow = 0; iRow < aRows.length; iRow++) {
		// Set checkbox
		selectRow(aRows[iRow], bSelect);
	}
	// Set "Select all rows" checkbox
	document.getElementById('selectAllCheckbox').checked = false;
}

/**
 * Gets called when any row checkbox is clicked.
 * @ignore
 */
function checkboxOnClick() {
	// Unset "Select all rows" checkbox
	document.getElementById('selectAllCheckbox').checked = false;
	// Unset "Select all rows on current page" checkbox
	document.getElementById('selectPageCheckbox').checked = false;
}

/**
 * Gets called when row is clicked.
 * @ignore
 */
function rowOnClick(oGrid, oRow) {
	// Get checkbox state
	var oCheckbox = getCheckboxByRow(oRow);
	if (!oCheckbox) {
		return;
	}
	// Save checkbox state
	updateRow(oRow, oCheckbox.checked);
}

/**
 * Gets called when grid is refreshed.
 * @ignore
 */
function gridOnRefresh(oGrid) {
	// For each row on current page
	var aRows = oGrid.applyPaging();
	for (var iRow = 0; iRow < aRows.length; iRow++) {
		// Test if row is checked
		var oCheckbox = getCheckboxByRow(aRows[iRow]);
		if (!oCheckbox) {
			continue;
		}
		if (!oCheckbox.checked) {
			// Uncheck checkboxes if there is unselected row on current page
			document.getElementById('selectPageCheckbox').checked = false;
			document.getElementById('selectAllCheckbox').checked = false;
			return;
		}
	}
	// Set checkbox if all rows on current page are selected
	document.getElementById('selectPageCheckbox').checked = true;
}
