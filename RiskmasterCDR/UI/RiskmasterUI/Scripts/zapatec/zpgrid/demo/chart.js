/**
 * @fileoverview Part of Zapatec Grid + Chart demo.
 *
 * <pre>
 * Copyright (c) 2004-2006 by Zapatec, Inc.
 * http://www.zapatec.com
 * 1700 MLK Way, Berkeley, California,
 * 94709, U.S.A.
 * All rights reserved.
 * </pre>
 */

/* $Id: chart.js 7323 2007-06-01 21:05:51Z alex $ */

/**
 * Checks if Flash is available and SWF file exists and calls specified
 * function.
 * @private
 */
function checkFlash(strSwfFile, funcCallback) {
	// Timeout to let browser continue page loading
	setTimeout(function() {
		// Check if Flash 6 or higher is available
		var iFlashVersion = 0;
		if (navigator.plugins && navigator.mimeTypes &&
		 navigator.mimeTypes.length) {
			// Mozilla and others
			var boolPlugin = false;
			if (navigator.mimeTypes['application/x-shockwave-flash']) {
				boolPlugin = navigator.mimeTypes['application/x-shockwave-flash']
				 .enabledPlugin;
			}
			if (boolPlugin) {
				iFlashVersion = parseInt(boolPlugin.description.substring(
				 boolPlugin.description.indexOf('.') - 1));
			}
		} else {
			// IE
			try {
				var objFlash = new ActiveXObject('ShockwaveFlash.ShockwaveFlash');
				for (var iVer = 6; objFlash != null; iVer++) {
					objFlash = new ActiveXObject('ShockwaveFlash.ShockwaveFlash.' + iVer);
					iFlashVersion = iVer;
				}
			} catch (objException) {}
		}
		if (iFlashVersion < 6) {
			alert('In order to view this example, you must have the ' +
			 'Macromedia Flash Player 6 or higher installed.');
		}
		// Check if swf file exists
		Zapatec.Transport.fetch({
			url: '../fusioncharts/' + strSwfFile,
			method: 'HEAD',
			onLoad: function() {
				if (funcCallback) {
					funcCallback();
				}
			},
			onError: function() {
				alert('This example works only in the PRO version.\n' +
				 'In order to view this example, ' + strSwfFile +
				 ' file must exist in ../fusioncharts/ folder.');
			}
		});
	}, 0);
}
