/**
 * @fileoverview Zapatec Grid language module. Default English language.
 *
 * <pre>
 * Copyright (c) 2004-2007 by Zapatec, Inc.
 * http://www.zapatec.com
 * 1700 MLK Way, Berkeley, California,
 * 94709, U.S.A.
 * All rights reserved.
 * </pre>
 */

/*
 * Define all messages that may appear in the Grid.
 */
Zapatec.Utils.createNestedHash(Zapatec, ['Langs', 'Zapatec.Grid', 'eng'], {
  'errorSource': "The grid's data source, %1, does not contain valid data.\n%2",
  'errorSelectRow': 'Please select at least one row.',
  'errorSelectCell': 'Please select at least one cell.',
  'errorHtmlTable': "Zapatec.Grid invalid configuration: Can't find source table",
  'errorHtmlHeader': "Zapatec.Grid invalid configuration: Can't find header for table",
  'errorContainer': 'Cannot find container for grid',
  'errorInvalidInput': 'Invalid input!',
  'labelPage': 'Page',
  'labelOf': 'of',
  'labelRows': 'rows',
  'labelSelectAll': 'Select All',
  'labelClear': 'Clear'
});
