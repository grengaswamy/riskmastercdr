/*
 *
 * Copyright (c) 2004-2005 by Zapatec, Inc.
 * http://www.zapatec.com
 * 1700 MLK Way, Berkeley, California,
 * 94709, U.S.A.
 * All rights reserved.
 *
 *
 */


Zapatec.Utils.emulateWindowEvent(['click','dblclick']);Zapatec.Grid=function(oArg){Zapatec.Grid.SUPERconstructor.call(this,oArg);};Zapatec.Grid.id='Zapatec.Grid';Zapatec.inherit(Zapatec.Grid,Zapatec.Widget);Zapatec.Grid.prototype.init=function(oArg){this.initialized=false;Zapatec.Grid.SUPERclass.init.call(this,oArg);this.data={};this.fields=[];this.rows=[];this.rowsIndex=[];this.filteredRows=[];this.currentPage=0;this.order=[];if(typeof this.config.sortColumn!='object'){this.order.push({col:this.config.sortColumn*1,desc:this.config.sortDesc,lt:this.config.sortDesc?1:-1,gt:this.config.sortDesc?-1:1});}
this.lastSelection=null;this.loadData();};Zapatec.Grid.prototype.reconfigure=function(oArg){Zapatec.Grid.SUPERclass.reconfigure.call(this,oArg);this.refresh();};Zapatec.Grid.prototype.configure=function(oArg){this.defineConfigOption('show_asis',false);this.defineConfigOption('funcStyle');this.defineConfigOption('convert');this.defineConfigOption('container');this.defineConfigOption('headerContainer');this.defineConfigOption('totalsContainer');this.defineConfigOption('horizontal',false);this.defineConfigOption('rowsPerPage',0);this.defineConfigOption('paginationContainer');this.defineConfigOption('selectRows',true);this.defineConfigOption('selectCells',true);this.defineConfigOption('activeRows',true);this.defineConfigOption('activeCells',true);this.defineConfigOption('multipleSelect',true);this.defineConfigOption('callbackHeaderDisplay');this.defineConfigOption('callbackDataDisplay');this.defineConfigOption('callbackRowDisplay');this.defineConfigOption('callbackTotalsDisplay');this.defineConfigOption('callbackTotalDisplay');this.defineConfigOption('callbackPaginationDisplay');this.defineConfigOption('callbackRowOnClick');this.defineConfigOption('callbackCellOnClick');this.defineConfigOption('callbackRowOnDblClick');this.defineConfigOption('callbackCellOnDblClick');this.defineConfigOption('callbackRowSelect');this.defineConfigOption('callbackCellSelect');this.defineConfigOption('callbackRowUnselect');this.defineConfigOption('callbackCellUnselect');this.defineConfigOption('callbackOnRefresh');this.defineConfigOption('sortColumn');this.defineConfigOption('sortDesc');this.defineConfigOption('filterOut',[]);this.defineConfigOption('totals',[]);this.defineConfigOption('dataPrepared',false);this.defineConfigOption('dataOnDemand',false);this.defineConfigOption('fixedLeft',0);this.defineConfigOption('columnWidth','auto');this.defineConfigOption('rowHeight','auto');Zapatec.Grid.SUPERclass.configure.call(this,oArg);this.config.rowsPerPage=parseInt(this.config.rowsPerPage);if(isNaN(this.config.rowsPerPage)){this.config.rowsPerPage=0;}
if(!this.config.rowsPerPage){this.config.dataOnDemand=false;}
this.visualize=true;if(typeof this.config.callbackHeaderDisplay=='function'&&(typeof this.config.callbackRowDisplay=='function'||typeof this.config.callbackDataDisplay=='function')){this.visualize=false;}
this.container=Zapatec.Widget.getElementById(this.config.container);this.headerContainer=Zapatec.Widget.getElementById(this.config.headerContainer);this.totalsContainer=Zapatec.Widget.getElementById(this.config.totalsContainer);this.paginationContainers=[];if(typeof this.config.paginationContainer!='undefined'){if(this.config.paginationContainer instanceof Array){for(var iEl=0;iEl<this.config.paginationContainer.length;iEl++){var oEl=Zapatec.Widget.getElementById(this.config.paginationContainer[iEl]);if(oEl){this.paginationContainers.push(oEl);}}}else{var oEl=Zapatec.Widget.getElementById(this.config.paginationContainer);if(oEl){this.paginationContainers.push(oEl);}}}
this.config.columnWidth=Zapatec.Utils.correctCssLength(this.config.columnWidth);if(this.config.columnWidth=='auto'&&(this.headerContainer||this.totalsContainer)){this.config.columnWidth='100px';}
this.config.rowHeight=Zapatec.Utils.correctCssLength(this.config.rowHeight);};Zapatec.Grid.prototype.addStandardEventListeners=function(){Zapatec.Grid.SUPERclass.addStandardEventListeners.call(this);this.addEventListener('fetchSourceError',this.displayErrorSource);if(this.displayLoading){this.addEventListener('fetchSourceStart',this.displayLoading);this.addEventListener('fetchSourceEnd',this.removeLoading);this.addEventListener('loadThemeEnd',this.visualizeThemeLoad);this.addEventListener('loadDataEnd',this.visualizeDataLoad);}};Zapatec.Grid.prototype.displayErrorSource=function(oError){alert("The grid's data source, "+this.config.source+", does not contain valid data.\n"+oError.errorDescription);};Zapatec.Grid.prototype.loadData=function(oArg){if(this.config.dataOnDemand){if(typeof oArg!='object'){oArg={};}
oArg.currentPage=this.currentPage;if(this.order.length){oArg.sortColumn=this.order[0].col;oArg.sortDesc=this.order[0].desc;oArg.order=this.order;}
oArg.filters=[];for(var iCol=0;iCol<this.fields.length;iCol++){var oField=this.fields[iCol];if(oField){oArg.filters[iCol]={hiddenValues:oField.hiddenValues,minValue:oField.minValue,maxValue:oField.maxValue,regexpFilter:oField.regexpFilter,textFilter:oField.textFilter};}else{oArg.filters[iCol]={};}}}
Zapatec.Grid.SUPERclass.loadData.call(this,oArg);};Zapatec.Grid.prototype.onRefresh=function(){if(this.visualizeRefresh&&this.visualize){this.visualizeRefresh();}
if(typeof this.config.callbackOnRefresh=='function'){this.config.callbackOnRefresh(this);}
this.fireEvent('gridRefreshed');};Zapatec.Grid.prototype.loadDataJson=function(oData){this.rowsIndex=null;if(!(oData instanceof Object)){oData={};}
if(!(oData.fields instanceof Array)){oData.fields=[];}
if(!(oData.rows instanceof Array)){oData.rows=[];}
this.data=oData;this.fields=this.data.fields;this.rows=this.data.rows;this.prepareData();this.rowsIndex=this.rows.slice();this.primaryKeyColumn=this.data.primaryKey;this.buildPrimaryKey();if(typeof oData.currentPage!='undefined'){this.currentPage=oData.currentPage;}else{this.currentPage=0;}
this.show();};Zapatec.Grid.prototype.buildPrimaryKey=function(){if(!this.fields[this.primaryKeyColumn]){return;}
this.primaryKey={};for(var iRow=0;iRow<this.rows.length;iRow++){var sKey=this.getCellValueCompare(this.getCellByRow(this.rows[iRow],this.primaryKeyColumn));if((typeof sKey=='string'&&sKey.length)||typeof sKey=='number'){this.primaryKey[sKey]=this.rows[iRow];}}};Zapatec.Grid.prototype.rebuildPrimaryKey=function(){if(this.primaryKey){this.buildPrimaryKey();}};Zapatec.Grid.prototype.show=function(){this.filteredRows=this.rows.slice();this.sort();this.refresh();this.displayFilterOut();if(!this.initialized){this.initialized=true;this.fireEvent('gridInitialized');}};Zapatec.Grid.prototype.prepareData=function(){if(!this.config.dataPrepared){for(var iCol=0;iCol<this.fields.length;iCol++){var oField=this.fields[iCol];if(!(oField instanceof Object)){oField={};}
oField.i=iCol;this.fields[iCol]=this.prepareField(oField);}
for(var iRow=0;iRow<this.rows.length;iRow++){var oRow=this.rows[iRow];if(!(oRow instanceof Object)){oRow={};}
oRow.i=iRow;this.rows[iRow]=this.prepareRow(oRow);}}};Zapatec.Grid.prototype.prepareField=function(oField){return oField;};Zapatec.Grid.prototype.prepareRow=function(oRow){if(!oRow.cells||!(oRow.cells instanceof Array)){oRow.cells=[];}
for(var iCol=0;iCol<oRow.cells.length;iCol++){var oCell=oRow.cells[iCol];if(!(oCell instanceof Object)){oCell={};}
oCell.i=iCol;oCell.r=oRow.i;oRow.cells[iCol]=this.convertCell(oCell);}
return oRow;};Zapatec.Grid.prototype.convertCell=function(oCell){if(!(oCell instanceof Object)){oCell={};}
if(this.getConvertByType){var oField=this.getFieldByCell(oCell);if(oField){var sMethod=this.getConvertByType(oField.dataType);if(sMethod){oCell=this[sMethod](oCell);}}}
oCell=this.convertCellCallback(oCell);return oCell;};Zapatec.Grid.prototype.convertCellCallback=function(oCell){if(!(oCell instanceof Object)){oCell={};}
if(typeof this.config.convert=='function'){var convertedValue=this.config.convert(this,oCell);if(typeof convertedValue!='undefined'){if(typeof oCell.o=='undefined'){oCell.o=oCell.v;}
oCell.v=oCell.c=convertedValue;}}
return oCell;};Zapatec.Grid.prototype.validateCell=function(oCell){if(!(oCell instanceof Object)){oCell={};}
if(this.getValidateByType){var oField=this.getFieldByCell(oCell);if(oField){var sMethod=this.getValidateByType(oField.dataType);if(sMethod){var undef;if(oCell.invalid){oCell.invalid=undef;}
var bValid=this[sMethod](oCell);if(!bValid){oCell.invalid=true;}
var oRow=this.getRowByCell(oCell);if(oRow){if(!bValid){oRow.invalid=true;}else{if(oRow.invalid){oRow.invalid=undef;}
var aCells=this.getRowCells(oRow);for(var iCell=0;iCell<aCells.length;iCell++){if(aCells[iCell]&&aCells[iCell].invalid){oRow.invalid=true;break;}}}}
return bValid;}}}
return true;};Zapatec.Grid.prototype.splice=function(aData){if(!aData){return;}
if(!(aData instanceof Array)){aData=[aData];}
this.fireEvent('gridPrepareModify');var aRemoved=[];var oData;for(var iData=0;iData<aData.length;iData++){oData=aData[iData];var iInsertPos=null;if(typeof oData.atKey!='undefined'){var iRowId=this.getRowIdByPrimaryKey(oData.atKey);if(typeof iRowId!='undefined'){oData.atRowId=iRowId;}}
if((typeof oData.atRowId=='string'&&oData.atRowId.length)||typeof oData.atRowId=='number'){iInsertPos=this.getRowIndexById(oData.atRowId);}
if(typeof iInsertPos!='number'){if((typeof oData.atRow=='string'&&oData.atRow.length)||typeof oData.atRow=='number'){var iRowNum=oData.atRow*1;if(typeof this.rows[iRowNum]!='undefined'){iInsertPos=iRowNum;}}}
if(typeof iInsertPos!='number'){if(typeof oData.afterKey!='undefined'){var iRowId=this.getRowIdByPrimaryKey(oData.afterKey);if(typeof iRowId!='undefined'){oData.afterRowId=iRowId;}}
if((typeof oData.afterRowId=='string'&&oData.afterRowId.length)||typeof oData.afterRowId=='number'){iInsertPos=this.getRowIndexById(oData.atRowId);if(typeof iInsertPos=='number'){iInsertPos++;}}}
if(typeof iInsertPos!='number'){if((typeof oData.afterRow=='string'&&oData.afterRow.length)||typeof oData.afterRow=='number'){var iRowNum=oData.afterRow*1;if(typeof this.rows[iRowNum]!='undefined'){iInsertPos=iRowNum+1;}}}
if(typeof iInsertPos!='number'){iInsertPos=this.rows.length;}
if(!(oData.rows instanceof Array)){oData.rows=[];}
var iHowManyToRemove=oData.howMany*1;var oPrimaryKey=this.primaryKey;this.primaryKey=null;var bRebuildPrimaryKey=false;var iRow=0;var iRemoved=0;while(iRemoved<iHowManyToRemove&&iRow<oData.rows.length){var oGridRow=this.rows[iInsertPos];if(typeof oGridRow=='undefined'){break;}
aRemoved.push(Zapatec.Utils.clone(oGridRow));var oRow=this.prepareRow(oData.rows[iRow]);for(var iCol=0;iCol<oGridRow.cells.length;iCol++){var oCell=oRow.cells[iCol];if(!oCell){continue;}
var oGridCell=oGridRow.cells[iCol];if(this.primaryKeyColumn==iCol&&oGridCell.c!=oCell.c){bRebuildPrimaryKey=true;}
oGridCell.v=oCell.v;oGridCell.c=oCell.c;oGridCell.o=oCell.o;oGridCell.style=oCell.style;}
oGridRow.style=oRow.style;iInsertPos++;iRow++;iRemoved++;}
for(;iRemoved<iHowManyToRemove;iRemoved++){if(typeof this.rows[iInsertPos]=='undefined'){break;}
var oRow=this.removeRow(iInsertPos);if(oRow){aRemoved.push(oRow);}
bRebuildPrimaryKey=true;}
for(;iRow<oData.rows.length;iRow++){var oRow=oData.rows[iRow];oRow.i=this.rowsIndex.length;oRow=this.prepareRow(oRow);this.rows.splice(iInsertPos++,0,oRow);this.rowsIndex.push(oRow);bRebuildPrimaryKey=true;}
if(bRebuildPrimaryKey){oPrimaryKey=null;this.buildPrimaryKey();}else{this.primaryKey=oPrimaryKey;oPrimaryKey=null;}}
if(!oData.noRefresh){this.modify();}
return aRemoved;};Zapatec.Grid.prototype.removeRow=function(iRow){var oRow=this.rows[iRow];if(!oRow){return;}
var undef;this.rowsIndex[oRow.i]=undef;var aRows=this.rows.splice(iRow,1);this.rebuildPrimaryKey();return aRows[0];};Zapatec.Grid.prototype.modify=function(){this.setFilters();this.displayFilterOut();this.fireEvent('gridModified');};Zapatec.Grid.prototype.sort=function(aArg){if(aArg){if(!(aArg instanceof Array)){aArg=[aArg];}
this.order=[];for(var iArg=0;iArg<aArg.length;iArg++){var oArg=aArg[iArg];if(!oArg){break;}
this.order.push({col:oArg.column*1,desc:oArg.desc,lt:oArg.desc?1:-1,gt:oArg.desc?-1:1});}}
if(this.order.length&&this.order.length<this.fields.length){var oUsed={};for(var iPos=0;iPos<this.order.length;iPos++){oUsed[this.order[iPos].col]=true;}
for(var iCol=0;iCol<this.fields.length&&this.order.length<this.fields.length;iCol++){if(oUsed[iCol]){continue;}
this.order.push({col:iCol,lt:-1,gt:1});}}
for(var iCol=0;iCol<this.fields.length;iCol++){var oField=this.fields[iCol];if(oField){var undef;oField.sorted=undef;oField.sortedDesc=undef;if(this.order.length&&iCol==this.order[0].col){if(this.order[0].desc){oField.sortedDesc=true;}else{oField.sorted=true;}}}}
if(!this.order.length){return;}
for(var iPos=0;iPos<this.order.length;iPos++){var iCol=this.order[iPos].col;if(this.fields[iCol]&&typeof this.fields[iCol].sortByColumn!='undefined'){this.order[iPos].col=this.fields[iCol].sortByColumn*1;}}
if(!this.config.dataOnDemand){if(this.displayUpdating){this.displayUpdating();}
var oGrid=this;setTimeout(function(){oGrid.filteredRows.sort(function(oLeft,oRight){for(var iCol=0;iCol<oGrid.order.length;iCol++){var iColNum=oGrid.order[iCol].col;var leftVal=oGrid.getCellValueCompare(oLeft.cells[iColNum]);var rightVal=oGrid.getCellValueCompare(oRight.cells[iColNum]);if(leftVal==rightVal){continue;}
if(leftVal<rightVal){return oGrid.order[iCol].lt;}
return oGrid.order[iCol].gt;}
return 0;});oGrid=null;},0);}};Zapatec.Grid.prototype.unsort=function(){this.order=[];this.filteredRows=this.rows.slice();this.sort();if(this.config.dataOnDemand){this.loadData();}else{this.refresh();}};Zapatec.Grid.sort=function(iGridId,iCol){var oGrid=Zapatec.Widget.getWidgetById(iGridId);if(!oGrid||!oGrid.fields[iCol]){return;}
if(!oGrid.fields[iCol].sorted){oGrid.sort({column:iCol});}else{oGrid.sort({column:iCol,desc:true});}
if(oGrid.config.dataOnDemand){oGrid.loadData();}else{oGrid.refresh();}};Zapatec.Grid.prototype.getCurrentPageNumber=function(){return this.currentPage+1;};Zapatec.Grid.prototype.totalPages=function(){var iRecords=this.recordsDisplayed();if(this.config.rowsPerPage<=0||iRecords<=0){return 1;}
return Math.ceil(iRecords/this.config.rowsPerPage);};Zapatec.Grid.prototype.setCurrentPage=function(iPage){var iPages=this.totalPages();if(iPage<0||iPage>=iPages){return;}
this.currentPage=iPage;};Zapatec.Grid.prototype.gotoPage=function(iPage){this.setCurrentPage(iPage);if(this.config.dataOnDemand){this.loadData();}else{this.refresh();}};Zapatec.Grid.gotoPage=function(iGridId,iPage){var oGrid=Zapatec.Widget.getWidgetById(iGridId);if(oGrid){oGrid.gotoPage(iPage-1);}};Zapatec.Grid.nextPage=function(iGridId){var oGrid=Zapatec.Widget.getWidgetById(iGridId);if(oGrid){oGrid.gotoPage(oGrid.currentPage+1);}};Zapatec.Grid.lastPage=function(iGridId){var oGrid=Zapatec.Widget.getWidgetById(iGridId);if(oGrid){oGrid.gotoPage(oGrid.totalPages()-1);}};Zapatec.Grid.previousPage=function(iGridId){var oGrid=Zapatec.Widget.getWidgetById(iGridId);if(oGrid){oGrid.gotoPage(oGrid.currentPage-1);}};Zapatec.Grid.firstPage=function(iGridId){var oGrid=Zapatec.Widget.getWidgetById(iGridId);if(oGrid){oGrid.gotoPage(0);}};Zapatec.Grid.prototype.gotoRowId=function(iRowId){var aRows=this.getFilteredRows();for(var iRow=0;iRow<aRows.length;iRow++){if(this.getRowId(aRows[iRow])==iRowId){this.gotoPage(Math.floor(iRow/this.config.rowsPerPage));return;}}};Zapatec.Grid.prototype.applyFilters=function(){this.fireEvent('gridPrepareFilter');if(this.config.dataOnDemand){this.currentPage=0;this.loadData();this.fireEvent('gridFiltered');}else{if(this.displayUpdating){this.displayUpdating();}
var oGrid=this;setTimeout(function(){oGrid.setFilters();oGrid.fireEvent('gridFiltered');},0);}};Zapatec.Grid.prototype.setFilters=function(){this.filteredRows=this.rows.slice();var aRegexpFilters=[];var aTextFilters=[];for(var iCol=0;iCol<this.fields.length;iCol++){if(!this.fields[iCol]){continue;}
var aHiddenValues=this.fields[iCol].hiddenValues;var minValue=this.fields[iCol].minValue;var maxValue=this.fields[iCol].maxValue;if(aHiddenValues instanceof Array||typeof minValue!='undefined'||typeof maxValue!='undefined'){for(var iRow=this.filteredRows.length-1;iRow>=0;iRow--){var oCell=this.filteredRows[iRow].cells[iCol];if(!oCell){continue;}
if(aHiddenValues instanceof Array&&Zapatec.Utils.arrIndexOf(aHiddenValues,this.getCellValueString(oCell))>=0){this.filteredRows.splice(iRow,1);continue;}
if(minValue>this.getCellValueCompare(oCell)){this.filteredRows.splice(iRow,1);continue;}
if(maxValue<this.getCellValueCompare(oCell)){this.filteredRows.splice(iRow,1);continue;}}}
if(this.fields[iCol].regexpFilter){aRegexpFilters.push(iCol);}
if(this.fields[iCol].textFilter){aTextFilters.push(iCol);}}
if(aRegexpFilters.length){for(var iRow=this.filteredRows.length-1;iRow>=0;iRow--){var bRemove=true;for(var iFilter=0;iFilter<aRegexpFilters.length;iFilter++){var iCol=aRegexpFilters[iFilter];var oCell=this.filteredRows[iRow].cells[iCol];if(!oCell){continue;}
var sVal=this.getCellValueString(oCell);var oRegExp=typeof this.fields[iCol].regexpFilter=='string'?new RegExp(this.fields[iCol].regexpFilter):this.fields[iCol].regexpFilter;if(oRegExp.test&&oRegExp.test(sVal)){bRemove=false;break;}}
if(bRemove){this.filteredRows.splice(iRow,1);}}}
if(aTextFilters.length){for(var iRow=this.filteredRows.length-1;iRow>=0;iRow--){var bRemove=true;for(var iFilter=0;iFilter<aTextFilters.length;iFilter++){var iCol=aTextFilters[iFilter];var oCell=this.filteredRows[iRow].cells[iCol];if(!oCell){continue;}
var sVal=this.getCellValueString(oCell);if(sVal.indexOf(this.fields[iCol].textFilter)>=0){bRemove=false;break;}}
if(bRemove){this.filteredRows.splice(iRow,1);}}}
this.sort();this.currentPage=0;this.refresh();};Zapatec.Grid.prototype.filterOut=function(oArg){if(this.filterOutColumn(oArg)){this.applyFilters();}};Zapatec.Grid.prototype.filterOutColumn=function(oArg){if(!oArg||typeof oArg.value=='undefined'){return false;}
var aVals=oArg.value;if(!(aVals instanceof Array)){aVals=[aVals];}
var aCols=oArg.column;if(!(aCols instanceof Array)){aCols=[aCols];}
var bApply=false;for(var iCol=0;iCol<aCols.length;iCol++){var oField=this.fields[aCols[iCol]];if(!oField){continue;}
if(!(oField.hiddenValues instanceof Array)){oField.hiddenValues=[];}
if(oArg.show){for(var iVal=0;iVal<aVals.length;iVal++){for(var iHv=oField.hiddenValues.length-1;iHv>=0;iHv--){if(oField.hiddenValues[iHv]==aVals[iVal]){oField.hiddenValues.splice(iHv,1);}}}}else{for(var iVal=0;iVal<aVals.length;iVal++){oField.hiddenValues.push(aVals[iVal]);}}
bApply=true;}
return bApply;};Zapatec.Grid.prototype.unfilterOut=function(oArg){if(this.unfilterOutColumn(oArg)){this.applyFilters();}};Zapatec.Grid.prototype.unfilterOutColumn=function(oArg){if(!oArg){return false;}
var aCols=oArg.column;if(!(aCols instanceof Array)){aCols=[aCols];}
var bApply=false;var undef;for(var iCol=0;iCol<aCols.length;iCol++){var oField=this.fields[aCols[iCol]];if(!oField){continue;}
if((oField.hiddenValues instanceof Array)&&oField.hiddenValues.length){oField.hiddenValues=undef;bApply=true;}}
return bApply;};Zapatec.Grid.prototype.limitRange=function(oArg){if(!oArg){return;}
var oField=this.fields[oArg.column];if(!oField){return;}
if(typeof oArg.min!='undefined'){oField.minValue=oArg.min;}else{if(typeof oArg.minValue=='undefined'){oField.minValue=oArg.minValue;}else{var oCell={i:oField.i,v:oArg.minValue};oCell=this.convertCell(oCell);oField.minValue=this.getCellValueCompare(oCell);}}
if(typeof oArg.max!='undefined'){oField.maxValue=oArg.max;}else{if(typeof oArg.maxValue=='undefined'){oField.maxValue=oArg.maxValue;}else{var oCell={i:oField.i,v:oArg.maxValue};oCell=this.convertCell(oCell);oField.maxValue=this.getCellValueCompare(oCell);}}
this.applyFilters();};Zapatec.Grid.prototype.setFilter=function(oArg){if(!oArg){oArg={};}
if(oArg.columns instanceof Array){for(var iCol=0;iCol<oArg.columns.length;iCol++){var oField=this.fields[oArg.columns[iCol]];if(oField){oField.regexpFilter=oArg.regexp;oField.textFilter=oArg.text;}}}else{for(var iCol=0;iCol<this.fields.length;iCol++){if(!(this.fields[iCol]instanceof Object)){this.fields[iCol]={};}
this.fields[iCol].regexpFilter=oArg.regexp;this.fields[iCol].textFilter=oArg.text;}}
this.applyFilters();};Zapatec.Grid.prototype.removeFilter=function(oArg){if(!oArg){oArg={};}
this.setFilter({columns:oArg.columns});};Zapatec.Grid.prototype.resetFilters=function(){for(var iCol=0;iCol<this.fields.length;iCol++){var oField=this.fields[iCol];if(oField){var undef;oField.hiddenValues=undef;oField.minValue=undef;oField.maxValue=undef;oField.regexpFilter=undef;oField.textFilter=undef;}}
this.applyFilters();this.displayFilterOut();};Zapatec.Grid.prototype.applyPaging=function(){if(this.config.rowsPerPage<=0||this.config.dataOnDemand){return this.filteredRows;}
if(this.currentPage<0){this.currentPage=0;}
var iFirst=this.currentPage*this.config.rowsPerPage;if(iFirst&&iFirst>=this.filteredRows.length){this.currentPage--;iFirst=this.currentPage*this.config.rowsPerPage;}
var iLast=iFirst+this.config.rowsPerPage*1;return this.filteredRows.slice(iFirst,iLast);};Zapatec.Grid.prototype.refresh=function(){this.fireEvent('gridPrepareRefresh');if(this.displayUpdating){this.displayUpdating();}
var oGrid=this;setTimeout(function(){if(!oGrid.visualize){oGrid.refreshCallback();}else if(oGrid.refreshContainer){if(Zapatec.Drag){oGrid.refreshContainer();}else{Zapatec.Transport.loadJS({module:'zpdrag',onLoad:function(){oGrid.refreshContainer();}});}}},0);};Zapatec.Grid.prototype.refreshCallback=function(){this.config.callbackHeaderDisplay(this);var aRows=this.applyPaging();if(typeof this.config.callbackDataDisplay=='function'){this.config.callbackDataDisplay(this,aRows);}else{for(var iRow=0;iRow<aRows.length;iRow++){this.config.callbackRowDisplay(this,aRows[iRow]);}}
if(typeof this.config.callbackTotalsDisplay=='function'||typeof this.config.callbackTotalDisplay=='function'){var aTotals;if(this.getTotals){aTotals=this.getTotals();}
if(typeof this.config.callbackTotalsDisplay=='function'){this.config.callbackTotalsDisplay(this,aTotals);}else if(aTotals){for(var iRow=0;iRow<aTotals.length;iRow++){this.config.callbackTotalDisplay(this,aTotals[iRow]);}}}
if(this.config.rowsPerPage>0){if(typeof this.config.callbackPaginationDisplay=='function'){this.config.callbackPaginationDisplay(this);}else if(this.paginationContainers.length&&this.outputPagination){for(var iEl=0;iEl<this.paginationContainers.length;iEl++){var aHtml=[];this.outputPagination(aHtml,iEl+1);this.paginationContainers[iEl].innerHTML=aHtml.join('');}}}
var oGrid=this;setTimeout(function(){oGrid.onRefresh();},0);};Zapatec.Grid.prototype.selectRow=function(oRow){if(!oRow||oRow.selected){return;}
if(typeof this.config.callbackCellSelect!='function'&&typeof this.config.callbackRowSelect=='function'){this.config.callbackRowSelect(this,oRow);}
if(this.visualizeSelectRow&&this.config.selectRows&&this.visualize){this.visualizeSelectRow(oRow);}
oRow.selected=true;};Zapatec.Grid.prototype.unselectRow=function(oRow){if(!oRow||!oRow.selected){return;}
var undef;oRow.selected=undef;if(this.visualizeUnselectRow&&this.config.selectRows&&this.visualize){this.visualizeUnselectRow(oRow);}
if(typeof this.config.callbackCellUnselect!='function'&&typeof this.config.callbackRowUnselect=='function'){this.config.callbackRowUnselect(this,oRow);}};Zapatec.Grid.prototype.selectCell=function(oCell){if(!oCell||oCell.selected){return;}
if(typeof this.config.callbackCellSelect=='function'){this.config.callbackCellSelect(this,oCell);}
if(this.visualizeSelectCell&&this.config.selectCells&&this.visualize){this.visualizeSelectCell(oCell);}
oCell.selected=true;};Zapatec.Grid.prototype.unselectCell=function(oCell){if(!oCell||!oCell.selected){return;}
var undef;oCell.selected=undef;if(this.visualizeUnselectCell&&this.config.selectCells&&this.visualize){this.visualizeUnselectCell(oCell);}
if(typeof this.config.callbackCellUnselect=='function'){this.config.callbackCellUnselect(this,oCell);}};Zapatec.Grid.prototype.rowOnClick=function(iRowId,iCellId){var oRow=this.getRowById(iRowId);if(!oRow){return;}
var oCell=null;if(typeof iCellId!='undefined'){oCell=this.getCellById(iRowId,iCellId);if(!oCell){return;}}
var bShift=false;var bCtrl=false;if(this.config.multipleSelect){if(this.lastSelection){bShift=window.event.shiftKey;}
bCtrl=window.event.ctrlKey||window.event.metaKey;}
if(!bShift&&!bCtrl){for(var iRow=0;iRow<this.rows.length;iRow++){var oCurrRow=this.rows[iRow];if(!oCurrRow||!oCurrRow.selected){continue;}
if(oCell){for(var iCol=0;iCol<this.fields.length;iCol++){var oCurrCell=oCurrRow.cells[iCol];if(!oCurrCell){continue;}
if(!(oCurrRow.i==iRowId&&oCurrCell.i==iCellId)){this.unselectCell(oCurrCell);}}}
if(oCurrRow.i!=iRowId){this.unselectRow(oCurrRow);}}}else if(bShift){if(this.lastSelection.rows instanceof Array){for(var iRow=0;iRow<this.lastSelection.rows.length;iRow++){this.unselectRow(this.lastSelection.rows[iRow]);}}
if(this.lastSelection.cells instanceof Array){for(var iCell=0;iCell<this.lastSelection.cells.length;iCell++){this.unselectCell(this.lastSelection.cells[iCell]);}}}
if(oCell&&typeof this.config.callbackCellOnClick=='function'){this.config.callbackCellOnClick(this,oCell);}else if(typeof this.config.callbackRowOnClick=='function'){this.config.callbackRowOnClick(this,oRow);}
if(!bShift){this.selectRow(oRow);this.lastSelection={rowId:iRowId};if(oCell){this.selectCell(oCell);this.lastSelection.cellId=iCellId;}}else{var iSelectionStartRowId=this.lastSelection.rowId;var iSelectionStartCellId=this.lastSelection.cellId;this.lastSelection.rows=[];this.lastSelection.cells=[];var aSelectedRows=this.lastSelection.rows;var aSelectedCells=this.lastSelection.cells;var iRow=0;var iLastRow=0;while(this.filteredRows[iRow]){var iCurrRowId=this.filteredRows[iRow].i;if(iCurrRowId==iRowId){iLastRow=iSelectionStartRowId;break;}else if(iCurrRowId==iSelectionStartRowId){iLastRow=iRowId;break;}
iRow++;}
var iFirstCell=0;var iLastCell=0;if(oCell&&typeof iSelectionStartCellId!='undefined'){if(iCellId<iSelectionStartCellId){iFirstCell=iCellId;iLastCell=iSelectionStartCellId;}else{iFirstCell=iSelectionStartCellId;iLastCell=iCellId;}}
while(this.filteredRows[iRow]){var oCurrRow=this.filteredRows[iRow];if(!oCurrRow.selected){this.selectRow(oCurrRow);aSelectedRows.push(oCurrRow);}
if(oCell){for(var iCell=iFirstCell;iCell<=iLastCell;iCell++){var oCurrCell=oCurrRow.cells[iCell];if(!oCurrCell.selected){this.selectCell(oCurrCell);aSelectedCells.push(oCurrCell);}}}
if(oCurrRow.i==iLastRow){break;}
iRow++;}}};Zapatec.Grid.rowOnClick=function(iGridId,iRowId,iCellId){var oGrid=Zapatec.Widget.getWidgetById(iGridId);if(oGrid&&oGrid.rowOnClick){oGrid.rowOnClick(iRowId,iCellId);}};Zapatec.Grid.prototype.rowOnDblClick=function(iRowId,iCellId){var oRow=this.getRowById(iRowId);if(!oRow){return;}
var oCell=null;if(typeof iCellId!='undefined'){oCell=this.getCellById(iRowId,iCellId);if(!oCell){return;}}
if(oCell&&typeof this.config.callbackCellOnDblClick=='function'){this.config.callbackCellOnDblClick(this,oCell);}else if(typeof this.config.callbackRowOnDblClick=='function'){this.config.callbackRowOnDblClick(this,oRow);}};Zapatec.Grid.rowOnDblClick=function(iGridId,iRowId,iCellId){var oGrid=Zapatec.Widget.getWidgetById(iGridId);if(oGrid&&oGrid.rowOnDblClick){oGrid.rowOnDblClick(iRowId,iCellId);}};Zapatec.Grid.prototype.clearSelection=function(){for(var iRow=0;iRow<this.rows.length;iRow++){var oCurrRow=this.rows[iRow];if(oCurrRow.selected){for(var iCol=0;iCol<oCurrRow.cells.length;iCol++){this.unselectCell(oCurrRow.cells[iCol]);}
this.unselectRow(oCurrRow);}}};Zapatec.Grid.prototype.getSelectedRows=function(){var aSelected=[];for(var iRow=0;iRow<this.rows.length;iRow++){var oCurrRow=this.rows[iRow];if(oCurrRow.selected){aSelected.push(oCurrRow);}}
return aSelected;};Zapatec.Grid.prototype.getSelectedCells=function(){var aSelected=[];for(var iRow=0;iRow<this.rows.length;iRow++){var oCurrRow=this.rows[iRow];if(oCurrRow.selected){for(var iCol=0;iCol<this.fields.length;iCol++){var oCurrCell=oCurrRow.cells[iCol];if(oCurrCell.selected){aSelected.push(oCurrCell);}}}}
return aSelected;};Zapatec.Grid.prototype.getInvalidRows=function(){var aInvalid=[];for(var iRow=0;iRow<this.rows.length;iRow++){var oCurrRow=this.rows[iRow];if(oCurrRow.invalid){aInvalid.push(oCurrRow);}}
return aInvalid;};Zapatec.Grid.prototype.getInvalidCells=function(){var aInvalid=[];for(var iRow=0;iRow<this.rows.length;iRow++){var oCurrRow=this.rows[iRow];if(oCurrRow.invalid){for(var iCol=0;iCol<this.fields.length;iCol++){var oCurrCell=oCurrRow.cells[iCol];if(oCurrCell.invalid){aInvalid.push(oCurrCell);}}}}
return aInvalid;};Zapatec.Grid.checkboxOnClick=function(iGridId,aCols,sVal,bChecked){var oGrid=Zapatec.Widget.getWidgetById(iGridId);if(oGrid&&oGrid.filterOut){oGrid.filterOut({column:aCols,value:sVal,show:bChecked});}};Zapatec.Grid.checkboxSelectAllOnClick=function(iGridId,aCols){var oGrid=Zapatec.Widget.getWidgetById(iGridId);if(!oGrid||!oGrid.unfilterOutColumn||!oGrid.applyFilters||!oGrid.displayFilterOut){return}
var bApply=oGrid.unfilterOutColumn({column:aCols});if(bApply){oGrid.applyFilters();oGrid.displayFilterOut();}};Zapatec.Grid.checkboxClearAllOnClick=function(iGridId,aCols){var oGrid=Zapatec.Widget.getWidgetById(iGridId);if(!oGrid||!oGrid.getColumnRange||!oGrid.filterOutColumn||!oGrid.applyFilters||!oGrid.displayFilterOut){return;}
var oRange=oGrid.getColumnRange({column:aCols});if(!oRange){return;}
var aVals=[];for(var iVal=0;iVal<oRange.values.length;iVal++){aVals.push(oRange.values[iVal].v+'');}
var bApply=oGrid.filterOutColumn({column:aCols,value:aVals,show:false});if(bApply){oGrid.applyFilters();oGrid.displayFilterOut();}};Zapatec.Grid.checkboxLinkOnClick=function(iGridId,aCols,sVal){var oGrid=Zapatec.Widget.getWidgetById(iGridId);if(!oGrid||!oGrid.getColumnRange||!oGrid.filterOutColumn||!oGrid.applyFilters||!oGrid.displayFilterOut){return;}
var oRange=oGrid.getColumnRange({column:aCols});if(!oRange){return;}
var aVals=[];for(var iVal=0;iVal<oRange.values.length;iVal++){aVals.push(oRange.values[iVal].v+'');}
var bClear=oGrid.filterOutColumn({column:aCols,value:aVals,show:false});var bShow=oGrid.filterOutColumn({column:aCols,value:sVal,show:true});if(bClear||bShow){oGrid.applyFilters();oGrid.displayFilterOut();}};Zapatec.Grid.prototype.addFilterOut=function(oArg){this.config.filterOut.push(oArg);};Zapatec.Grid.prototype.displayFilterOut=function(){for(var iFo=0;iFo<this.config.filterOut.length;iFo++){var oFilterOut=this.config.filterOut[iFo];var oRange=this.getColumnRange(oFilterOut);if(!oRange){continue;}
var aVals=oRange.values;if(oFilterOut.sortDesc){aVals.sort(function(leftVal,rightVal){if(leftVal.c<rightVal.c){return 1;}
if(leftVal.c>rightVal.c){return-1;}
return 0;});}
if(typeof oFilterOut.callback=='function'){var aCols=oFilterOut.column;if(!(aCols instanceof Array)){aCols=[aCols];}
var aFields=[];for(var iCol=0;iCol<aCols.length;iCol++){var oField=this.fields[aCols[iCol]];if(!oField){continue;}
aFields.push(oField);}
if(!aFields.length){continue;}
var sCols=aCols.join(',');var aChoices=[];for(var iVal=0;iVal<aVals.length;iVal++){var sVal=aVals[iVal].v+'';var sEscaped=escape(sVal);var oChoice={};oChoice.value=sVal;oChoice.onclick="Zapatec.Grid.checkboxOnClick('"+this.id+"',["+sCols+"],unescape('"+sEscaped+"'),this.checked)";oChoice.checked=false;for(var iField=0;iField<aFields.length;iField++){var oField=aFields[iField];if(!(oField.hiddenValues instanceof Array)||Zapatec.Utils.arrIndexOf(oField.hiddenValues,sVal)<0){oChoice.checked=true;break;}}
oChoice.link="Zapatec.Grid.checkboxLinkOnClick('"+this.id+"',["+sCols+"],unescape('"+sEscaped+"'))";if(iVal==0){oChoice.selectall="Zapatec.Grid.checkboxSelectAllOnClick('"+
this.id+"',["+sCols+"])";oChoice.clearall="Zapatec.Grid.checkboxClearAllOnClick('"+
this.id+"',["+sCols+"])";}
aChoices.push(oChoice);}
oFilterOut.callback(aChoices);}else if(this.visualizeFilterOut){this.visualizeFilterOut(oFilterOut,aVals);}}};Zapatec.Grid.prototype.getColumnRange=function(oArg){if(!oArg||typeof oArg.column=='undefined'){return null;}
var aCols=oArg.column;if(!(aCols instanceof Array)){var oField=this.fields[oArg.column];if(!oField){return null;}
if(typeof oField.columnRange!='undefined'){return oField.columnRange;}
aCols=[oArg.column];}
var aKeys=[];var oKeys={};var aRows=oArg.filtered?this.filteredRows:this.rows;for(var iRow=0;iRow<aRows.length;iRow++){var oRow=aRows[iRow];if(!oRow){continue;}
for(var iCol=0;iCol<aCols.length;iCol++){var oCell=oRow.cells[aCols[iCol]];if(!oCell){continue;}
var sKey=this.getCellValueString(oCell);if(sKey.length&&typeof oKeys[sKey]=='undefined'){aKeys.push({v:sKey,c:this.getCellValueCompare(oCell),o:this.getCellValueOriginal(oCell)});oKeys[sKey]=true;}}}
if(!aKeys.length){return null;}
aKeys.sort(function(leftVal,rightVal){if(leftVal.c<rightVal.c){return-1;}
if(leftVal.c>rightVal.c){return 1;}
return 0;});var iLastKey=aKeys.length-1;return{min:aKeys[0].c,minValue:aKeys[0].v,minOrig:aKeys[0].o,max:aKeys[iLastKey].c,maxValue:aKeys[iLastKey].v,maxOrig:aKeys[iLastKey].o,values:aKeys};};Zapatec.Grid.prototype.recordsDisplayed=function(){if(this.config.dataOnDemand&&typeof this.data.displayedRows!='undefined'){return this.data.displayedRows*1;}
return this.filteredRows.length;};Zapatec.Grid.prototype.setDisplayedRows=function(iRows){if(this.config.dataOnDemand){this.data.displayedRows=iRows;}};Zapatec.Grid.prototype.totalRecords=function(){if(this.config.dataOnDemand&&typeof this.data.totalRows!='undefined'){return this.data.totalRows*1;}
return this.rows.length;};Zapatec.Grid.prototype.setTotalRows=function(iRows){if(this.config.dataOnDemand){this.data.totalRows=iRows;}};Zapatec.Grid.prototype.getId=function(){return this.id;};Zapatec.Grid.prototype.getStyle=function(){if(this.data&&this.data.style){return this.data.style;}
return'';};Zapatec.Grid.prototype.getHeaderStyle=function(){if(this.data&&this.data.headerStyle){return this.data.headerStyle;}
return'';};Zapatec.Grid.prototype.getRows=function(){return this.rows;};Zapatec.Grid.prototype.getFilteredRows=function(){return this.filteredRows;};Zapatec.Grid.prototype.getRowId=function(oRow){if(oRow){return oRow.i;}};Zapatec.Grid.prototype.getRowIdByPrimaryKey=function(sKey){return this.getRowId(this.getRowByPrimaryKey(sKey));};Zapatec.Grid.prototype.getRowNumber=function(oRow){if(oRow){var iRowId=this.getRowId(oRow);var aRows=this.applyPaging();for(var iRow=0;iRow<aRows.length;iRow++){if(this.getRowId(aRows[iRow])==iRowId){return iRow;}}}};Zapatec.Grid.prototype.getRowIndexById=function(iRowId){for(var iRow=0;iRow<this.rows.length;iRow++){if(this.getRowId(this.rows[iRow])==iRowId){return iRow;}}};Zapatec.Grid.prototype.getRowStyle=function(oRow){if(oRow&&oRow.style){return oRow.style;}
return'';};Zapatec.Grid.prototype.getRowSelected=function(oRow){return(oRow&&oRow.selected);};Zapatec.Grid.prototype.getRowById=function(iRowId){return this.rowsIndex[iRowId];};Zapatec.Grid.prototype.getRowByPrimaryKey=function(sKey){if(this.primaryKey){return this.primaryKey[sKey];}};Zapatec.Grid.prototype.getRowByCell=function(oCell){if(oCell){return this.getRowById(oCell.r);}};Zapatec.Grid.prototype.getRowCells=function(oRow){if(oRow&&oRow.cells instanceof Array){return oRow.cells;}};Zapatec.Grid.prototype.setRowStyle=function(oRow,sStyle){if(oRow instanceof Object){oRow.style=sStyle;}
return oRow;};Zapatec.Grid.prototype.getFields=function(){return this.fields;};Zapatec.Grid.prototype.getFieldId=function(oField){if(oField){return oField.i;}};Zapatec.Grid.prototype.getFieldTitle=function(oField){if(oField&&oField.title){return oField.title;}
return'';};Zapatec.Grid.prototype.getFieldType=function(oField){if(oField){return oField.dataType;}};Zapatec.Grid.prototype.getFieldWidth=function(oField){if(oField&&oField.columnWidth){return oField.columnWidth;}
return'';};Zapatec.Grid.prototype.getFieldStyle=function(oField){if(oField&&oField.style){return oField.style;}
return'';};Zapatec.Grid.prototype.getFieldSpan=function(oField){if(oField&&!oField.hidden&&typeof oField.span=='number'&&oField.span>0){return oField.span;}
return 0;};Zapatec.Grid.prototype.getFieldSpanTitle=function(oField){if(oField&&oField.spanTitle){return oField.spanTitle;}
return'';};Zapatec.Grid.prototype.getFieldSpanStyle=function(oField){if(oField&&oField.spanStyle){return oField.spanStyle;}
return'';};Zapatec.Grid.prototype.getFieldHidden=function(oField){if(oField){return oField.hidden;}};Zapatec.Grid.prototype.getFieldNosort=function(oField){if(oField){return oField.nosort;}};Zapatec.Grid.prototype.getFieldSorted=function(oField){if(oField){return oField.sorted;}};Zapatec.Grid.prototype.getFieldSortedDesc=function(oField){if(oField){return oField.sortedDesc;}};Zapatec.Grid.prototype.getFieldOnclick=function(oField){if(oField&&!oField.nosort){return"Zapatec.Grid.sort('"+this.id+"','"+oField.i+"')";}
return'';};Zapatec.Grid.prototype.getFieldById=function(iFieldId){return this.fields[iFieldId];};Zapatec.Grid.prototype.getFieldByCell=function(oCell){if(oCell){return this.getFieldById(oCell.i);}};Zapatec.Grid.prototype.getCellId=function(oCell){if(oCell){return oCell.i;}};Zapatec.Grid.prototype.getCellRowId=function(oCell){if(oCell){return oCell.r;}};Zapatec.Grid.prototype.getCellRowNumber=function(oCell){if(oCell){var iRowId=this.getCellRowId(oCell);var aRows=this.applyPaging();for(var iRow=0;iRow<aRows.length;iRow++){if(this.getRowId(aRows[iRow])==iRowId){return iRow;}}}};Zapatec.Grid.prototype.getCellByRow=function(oRow,iCellId){var aCells=this.getRowCells(oRow);if(aCells){return aCells[iCellId];}};Zapatec.Grid.prototype.getCellById=function(iRowId,iCellId){return this.getCellByRow(this.getRowById(iRowId),iCellId);};Zapatec.Grid.prototype.getCellValue=function(oCell){if(!oCell){return;}
return oCell.v;};Zapatec.Grid.prototype.getCellValueString=function(oCell){return this.getCellValue(oCell)+'';};Zapatec.Grid.prototype.getCellValueCompare=function(oCell){if(!oCell){return'';}
if(typeof oCell.c!='undefined'){return oCell.c;}
return this.getCellValue(oCell);};Zapatec.Grid.prototype.getCellValueOriginal=function(oCell){if(!oCell){return'';}
if(typeof oCell.o!='undefined'){return oCell.o;}
return this.getCellValue(oCell);};Zapatec.Grid.prototype.getCellStyle=function(oCell,iRow){if(!oCell){return'';}
var sStyle='';if(typeof this.config.funcStyle=='function'){sStyle=this.config.funcStyle(this,oCell,iRow);}
if(!sStyle){sStyle=oCell.style;}
return sStyle;};Zapatec.Grid.prototype.getCellSelected=function(oCell){return(oCell&&oCell.selected);};Zapatec.Grid.prototype.getCellDataType=function(oCell){var oField=this.getFieldByCell(oCell);if(oField){return oField.dataType;}};Zapatec.Grid.prototype.getCellData=function(oCell,iMode){if(!oCell){return'undefined';}
if(!iMode){iMode=1;}
if((iMode==1&&!this.config.show_asis)||iMode==2){return this.getCellValueString(oCell);}
var sData=this.getCellValueOriginal(oCell)+'';if(iMode==4){return sData;}
if(typeof this.config.show_asis=='object'){if(typeof this.config.show_asis.funcShow=='function'){sData=this.config.show_asis.funcShow(this,oCell);}
if(this.config.show_asis.bBoth){sData='<u>'+sData+'</u><br>'+this.getCellValueString(oCell);}}
return sData;};Zapatec.Grid.prototype.setCellValue=function(oCell,value){if(!oCell){oCell={};}
oCell.v=value;return this.convertCell(oCell);};Zapatec.Grid.prototype.setCellStyle=function(oCell,sStyle){if(oCell instanceof Object){oCell.style=sStyle;}
return oCell;};Zapatec.Grid.prototype.hideColumns=function(oArg){if(!(oArg.columns instanceof Array)){if(typeof oArg.columns=='undefined'){return;}
oArg.columns=[oArg.columns];}
for(var iCol=0;iCol<oArg.columns.length;iCol++){var oCol=this.getFieldById(oArg.columns[iCol]);if(oCol){oCol.hidden=true;if(!oArg.noRefresh){this.refresh();}}}};Zapatec.Grid.prototype.unhideColumns=function(oArg){if(!(oArg.columns instanceof Array)){if(typeof oArg.columns=='undefined'){return;}
oArg.columns=[oArg.columns];}
for(var iCol=0;iCol<oArg.columns.length;iCol++){var oCol=this.getFieldById(oArg.columns[iCol]);if(oCol){oCol.hidden=false;if(!oArg.noRefresh){this.refresh();}}}};Zapatec.Grid.prototype.editData=function(oArg){Zapatec.Grid.SUPERclass.editData.call(this,oArg);if(!oArg.data){return;}
var aRows=oArg.data;if(!(aRows instanceof Array)){aRows=[aRows];}
for(var iRow=0;iRow<aRows.length;iRow++){var aCells=aRows[iRow];if(aCells.cells){break;}
if(!(aCells instanceof Array)){aCells=[aCells];}
var oRow={cells:[]};for(var iCell=0;iCell<aCells.length;iCell++){var oCell=aCells[iCell];if(typeof oCell.v!='undefined'){oRow.cells.push(oCell);}else{oRow.cells.push({v:oCell});}}
aRows[iRow]=oRow;}
this.splice({atRow:0,howMany:this.totalRecords(),rows:Zapatec.Utils.clone(aRows)});};Zapatec.Grid.prototype.editSelectedRows=function(oEditor){if(!oEditor||!oEditor.editData){return;}
var aRows=this.getSelectedRows();if(!aRows.length){alert('Please select at least one row.');return;}
oEditor.editData({data:aRows});};Zapatec.Grid.prototype.editSelectedCells=function(oEditor){if(!oEditor||!oEditor.editData){return;}
var aCells=this.getSelectedCells();if(!aCells.length){alert('Please select at least one cell.');return;}
oEditor.editData({data:aCells});};Zapatec.Grid.prototype.convertString=function(objCell){if(!(objCell instanceof Object)){objCell={};}
objCell.v+='';objCell.v=objCell.v.replace(/\s+/g,' ');objCell.v=objCell.v.replace(/^\s/,'').replace(/\s$/,'');objCell.c=objCell.o=objCell.v;objCell.c=objCell.c.replace(/<[^>]*>/g,'');return objCell;};Zapatec.Grid.prototype.convertInsensitiveString=function(objCell){objCell=this.convertString(objCell);objCell.c=objCell.c.toUpperCase();return objCell;};Zapatec.Grid.prototype.convertInteger=function(objCell){if(objCell&&(objCell.v==Infinity||objCell.v==-Infinity)){return objCell;}
objCell=this.convertString(objCell);objCell.c=objCell.c.replace(/[^0-9\.\-]/g,'');objCell.c=objCell.c.replace(/\..*/g,'');objCell.c=parseInt(objCell.c);if(isNaN(objCell.c)){objCell.c=0;}
return objCell;};Zapatec.Grid.prototype.convertIntegerGerman=function(objCell){if(objCell&&(objCell.v==Infinity||objCell.v==-Infinity)){return objCell;}
objCell=this.convertString(objCell);objCell.c=objCell.c.replace(/[^0-9,\-]/g,'');objCell.c=objCell.c.replace(/,.*/g,'');objCell.c=parseInt(objCell.c);if(isNaN(objCell.c)){objCell.c=0;}
return objCell;};Zapatec.Grid.prototype.convertFloat=function(objCell){if(objCell&&(objCell.v==Infinity||objCell.v==-Infinity)){return objCell;}
objCell=this.convertString(objCell);objCell.c=objCell.c.replace(/[^0-9\.\-]/g,'');objCell.c=parseFloat(objCell.c);if(isNaN(objCell.c)){objCell.c=0;}
return objCell;};Zapatec.Grid.prototype.convertFloatGerman=function(objCell){if(objCell&&(objCell.v==Infinity||objCell.v==-Infinity)){return objCell;}
objCell=this.convertString(objCell);objCell.c=objCell.c.replace(/[^0-9,\-]/g,'');objCell.c=objCell.c.replace(/,/g,'.');objCell.c=parseFloat(objCell.c);if(isNaN(objCell.c)){objCell.c=0;}
return objCell;};Zapatec.Grid.prototype.convertDate=function(objCell){objCell=this.convertString(objCell);objCell.c=Date.parse(objCell.c);return objCell;};Zapatec.Grid.prototype.convertTime=function(objCell){objCell=this.convertString(objCell);var arrMatch=objCell.c.match(/(\d{1,2})\D+(\d{1,2})(\D+(\d{1,2}))?\W*(AM|PM|A|P)?/i);if(!arrMatch){arrMatch=objCell.c.match(/(\d{2})(\d{2})((\d{2}))?\W*(AM|PM|A|P)?/i);}
if(arrMatch&&arrMatch[1]&&arrMatch[2]){var hour=arrMatch[1]*1;if(arrMatch[5]){var strAmPm=arrMatch[5].toUpperCase();if(strAmPm=='PM'||strAmPm=='P'){if(hour<12){hour+=12;}}else{if(hour==12){hour=0;}}}
if(hour<10){hour='0'+hour;}
var minute=arrMatch[2]*1;if(minute<10){minute='0'+minute;}
var second=0;if(arrMatch[4]){second=arrMatch[4]*1;}
if(second<10){second='0'+second;}
objCell.c=hour+':'+minute+':'+second;}
return objCell;};Zapatec.Grid.prototype.convertTimestampLocale=function(objCell){objCell=this.convertString(objCell);objCell.v=(new Date(parseInt(objCell.v)*1000)).toLocaleString();return objCell;};Zapatec.Grid.prototype.convertTimestampMMDDYYYY=function(objCell){objCell=this.convertString(objCell);var objDate=new Date(parseInt(objCell.v)*1000);var strMonth=objDate.getMonth()+1;if(strMonth<10){strMonth='0'+strMonth;}
var strDay=objDate.getDate();if(strDay<10){strDay='0'+strDay;}
var strYear=objDate.getYear();if(strYear<1900){strYear+=1900;}
objCell.v=strMonth+'/'+strDay+'/'+strYear;return objCell;};Zapatec.Grid.prototype.convertTimestampDDMMYYYY=function(objCell){objCell=this.convertString(objCell);var objDate=new Date(parseInt(objCell.v)*1000);var strMonth=objDate.getMonth()+1;if(strMonth<10){strMonth='0'+strMonth;}
var strDay=objDate.getDate();if(strDay<10){strDay='0'+strDay;}
var strYear=objDate.getYear();if(strYear<1900){strYear+=1900;}
objCell.v=strDay+'/'+strMonth+'/'+strYear;return objCell;};Zapatec.Grid.prototype.convertTimestampYYYYMMDD=function(objCell){objCell=this.convertString(objCell);var objDate=new Date(parseInt(objCell.v)*1000);var strMonth=objDate.getMonth()+1;if(strMonth<10){strMonth='0'+strMonth;}
var strDay=objDate.getDate();if(strDay<10){strDay='0'+strDay;}
var strYear=objDate.getYear();if(strYear<1900){strYear+=1900;}
objCell.v=strYear+'/'+strMonth+'/'+strDay;return objCell;};Zapatec.Grid.prototype.convertBoolean=function(objCell){objCell=this.convertString(objCell);switch(objCell.c.toUpperCase()){case'0':case'F':case'FALSE':case'N':case'NO':case Zapatec.Grid.booleanValues[0].toUpperCase():objCell.c=0;break;default:objCell.c=1;}
objCell.v=Zapatec.Grid.booleanValues[objCell.c];return objCell;};Zapatec.Grid.booleanValues=['No','Yes'];Zapatec.Grid.prototype.setBooleanValues=function(strNo,strYes){if(typeof strNo=='string'){Zapatec.Grid.booleanValues[0]=strNo;}
if(typeof strYes=='string'){Zapatec.Grid.booleanValues[1]=strYes;}};Zapatec.Grid.prototype.convertBooleanTF=function(objCell){objCell=this.convertString(objCell);switch(objCell.c.toUpperCase()){case'0':case'F':case'FALSE':case'N':case'NO':case Zapatec.Grid.booleanTFValues[0].toUpperCase():objCell.c=0;break;default:objCell.c=1;}
objCell.v=Zapatec.Grid.booleanTFValues[objCell.c];return objCell;};Zapatec.Grid.booleanTFValues=['False','True'];Zapatec.Grid.prototype.setBooleanTFValues=function(strFalse,strTrue){if(typeof strFalse=='string'){Zapatec.Grid.booleanTFValues[0]=strFalse;}
if(typeof strTrue=='string'){Zapatec.Grid.booleanTFValues[1]=strTrue;}};Zapatec.Grid.convertByType={'string':'convertString','istring':'convertInsensitiveString','int':'convertInteger','intGerman':'convertIntegerGerman','integer':'convertInteger','integerGerman':'convertIntegerGerman','float':'convertFloat','floatGerman':'convertFloatGerman','date':'convertDate','time':'convertTime','timestamp':'convertTimestampLocale','timestampMMDDYYYY':'convertTimestampMMDDYYYY','timestampDDMMYYYY':'convertTimestampDDMMYYYY','timestampYYYYMMDD':'convertTimestampYYYYMMDD','boolean':'convertBoolean','booleanTF':'convertBooleanTF'};Zapatec.Grid.prototype.getConvertByType=function(strType){return Zapatec.Grid.convertByType[strType];};Zapatec.Grid.classByType={'string':'zpGridTypeString','istring':'zpGridTypeStringInsensitive','int':'zpGridTypeInt','intGerman':'zpGridTypeIntGerman','integer':'zpGridTypeInt','integerGerman':'zpGridTypeIntGerman','float':'zpGridTypeFloat','floatGerman':'zpGridTypeFloatGerman','date':'zpGridTypeDate','time':'zpGridTypeTime','timestamp':'zpGridTypeTimestampLocale','timestampMMDDYYYY':'zpGridTypeTimestampMMDDYYYY','timestampDDMMYYYY':'zpGridTypeTimestampDDMMYYYY','timestampYYYYMMDD':'zpGridTypeTimestampYYYYMMDD','boolean':'zpGridTypeBoolean','booleanTF':'zpGridTypeBooleanTF'};Zapatec.Grid.prototype.getClassByType=function(strType){return Zapatec.Grid.classByType[strType];};Zapatec.Grid.typeByClass={'zpGridTypeString':'string','zpGridTypeStringInsensitive':'istring','zpGridTypeInt':'int','zpGridTypeIntGerman':'intGerman','zpGridTypeFloat':'float','zpGridTypeFloatGerman':'floatGerman','zpGridTypeDate':'date','zpGridTypeTime':'time','zpGridTypeTimestampLocale':'timestamp','zpGridTypeTimestampMMDDYYYY':'timestampMMDDYYYY','zpGridTypeTimestampDDMMYYYY':'timestampDDMMYYYY','zpGridTypeTimestampYYYYMMDD':'timestampYYYYMMDD','zpGridTypeBoolean':'boolean','zpGridTypeBooleanTF':'booleanTF'};Zapatec.Grid.prototype.getTypeByClass=function(strClass){var arrClasses=strClass.split(/\s+/);for(var iClass=0;iClass<arrClasses.length;iClass++){var strType=Zapatec.Grid.typeByClass[arrClasses[iClass]];if(typeof strType!='undefined'){return strType;}}
return'string';};Zapatec.Grid.createType=function(funcConvert,strTypeName,strTypeClass){if(typeof funcConvert!='function'||typeof strTypeName!='string'||!strTypeName.length){return;}
var strFuncName='convertCustom'+strTypeName.charAt(0).toUpperCase()+
strTypeName.substr(1);Zapatec.Grid.prototype[strFuncName]=funcConvert;Zapatec.Grid.convertByType[strTypeName]=strFuncName;if(typeof strTypeClass=='string'&&strTypeClass.length){Zapatec.Grid.classByType[strTypeName]=strTypeClass;Zapatec.Grid.typeByClass[strTypeClass]=strTypeName;}};Zapatec.Grid.prototype.loadDataHtml=function(oSource){var oTable=null;if(oSource){oTable=oSource;}else if(this.container){oTable=Zapatec.Utils.getFirstChild(this.container,'table');}
if(!oTable){alert("Zapatec.Grid invalid configuration: Can't find source table");return;}
var oThead=Zapatec.Utils.getFirstChild(oTable,'thead');var oTbodies=oTable.getElementsByTagName('tbody');if(!oTbodies||!oTbodies.length){oTbodies=[oTable];}
var oHeaderTr=Zapatec.Utils.getFirstChild(oThead||oTbodies[0],'tr');if(!oHeaderTr){alert("Zapatec.Grid invalid configuration: Can't find header for table");return;}
this.data=this.newDataHtml(oTable,oHeaderTr);this.fields=this.data.fields;this.rows=this.data.rows;this.rowsIndex=[];this.currentPage=0;var oTableCell=Zapatec.Utils.getFirstChild(oHeaderTr,'th','td');while(oTableCell){this.fields.push(this.newFieldHtml(oTableCell));oTableCell=Zapatec.Utils.getNextSibling(oTableCell,'th','td');}
for(var iTbody=0;iTbody<oTbodies.length;iTbody++){var oTrs=oTbodies[iTbody].getElementsByTagName('tr');var iTr=0;if(iTbody==0&&!oThead){iTr++;}
for(;iTr<oTrs.length;iTr++){var oRow=this.newRowHtml(oTrs[iTr]);this.rows.push(oRow);this.rowsIndex.push(oRow);}}
this.primaryKeyColumn=this.data.primaryKey;this.buildPrimaryKey();this.show();};Zapatec.Grid.prototype.newDataHtml=function(oTable,oHeaderTr){var oData={fields:[],rows:[]};var sStyle=Zapatec.Widget.getStyle(oTable);if(sStyle){oData.style=sStyle;}
var sHeaderStyle=Zapatec.Widget.getStyle(oHeaderTr);if(sHeaderStyle){oData.headerStyle=sHeaderStyle;}
return oData;};Zapatec.Grid.prototype.newFieldHtml=function(oTableCell){var oField={i:this.fields.length,title:oTableCell.innerHTML};if(this.getTypeByClass){oField.dataType=this.getTypeByClass(oTableCell.className);}
var sWidth=oTableCell.getAttribute('width');if(sWidth){sWidth+='';if(sWidth.length){oField.columnWidth=sWidth;}}
var sStyle=Zapatec.Widget.getStyle(oTableCell);if(sStyle){oField.style=sStyle;}
var sSpan=oTableCell.getAttribute('span');if(sSpan){oField.span=sSpan*1;}
var sSpanTitle=oTableCell.getAttribute('spantitle');if(!sSpanTitle){sSpanTitle=oTableCell.getAttribute('spanTitle');}
if(sSpanTitle){oField.spanTitle=sSpanTitle;}
var sSpanStyle=oTableCell.getAttribute('spanstyle');if(!sSpanStyle){sSpanStyle=oTableCell.getAttribute('spanStyle');}
if(sSpanStyle){oField.spanStyle=sSpanStyle;}
var bHidden=(oTableCell.className.indexOf('zpGridTypeHidden')>=0);if(bHidden){oField.hidden=bHidden;}
var bNosort=(oTableCell.className.indexOf('zpGridTypeNosort')>=0);if(bNosort){oField.nosort=bNosort;}
var bNotags=(oTableCell.className.indexOf('zpGridTypeNotags')>=0);if(bNotags){oField.notags=bNotags;}
var aMatch=oTableCell.className.match(/zpGridTypeSortBy(\d+)/);if(aMatch){oField.sortByColumn=aMatch[1];}
var bPrimaryKey=(oTableCell.className.indexOf('zpGridTypePrimaryKey')>=0);if(bPrimaryKey){this.data.primaryKey=oField.i;}
return oField;};Zapatec.Grid.prototype.newRowHtml=function(oTableRow){var oRow={i:this.rowsIndex.length,cells:[]};var sStyle=Zapatec.Widget.getStyle(oTableRow);if(sStyle){oRow.style=sStyle;}
var oTableCell=Zapatec.Utils.getFirstChild(oTableRow,'td','th');for(var iCol=0;iCol<this.fields.length;iCol++){oRow.cells.push(this.newCellHtml(oTableCell,oRow.i,iCol));if(oTableCell){oTableCell=Zapatec.Utils.getNextSibling(oTableCell,'td','th');}}
return oRow;};Zapatec.Grid.prototype.newCellHtml=function(oTableCell,iRow,iCol){var oCell={i:iCol,r:iRow,v:''};if(oTableCell){oCell.v=oTableCell.innerHTML;if(this.fields[iCol].notags){oCell.v=oCell.v.replace(/<[^>]*>/g,'');}
var sCellStyle=Zapatec.Widget.getStyle(oTableCell);if(sCellStyle){oCell.style=sCellStyle;}}
oCell=this.convertCell(oCell);return oCell;};Zapatec.Grid.prototype.syncContainers=function(){if(!this.container||(!this.headerContainer&&!this.totalsContainer&&!this.paginationContainers.length)){return;}
var iContainerWidth=this.container.clientWidth;var iContainerHeight=this.container.clientHeight;var oGridDims=this.getGridDimensions();if(oGridDims.width&&(!iContainerWidth||!this.container.style.width||oGridDims.width<iContainerWidth)){iContainerWidth=oGridDims.width;}
if(oGridDims.height&&(!iContainerHeight||!this.container.style.height||oGridDims.height<iContainerHeight)){iContainerHeight=oGridDims.height;}
if(iContainerWidth){if(this.headerContainer){this.headerContainer.style.width=iContainerWidth+'px';}
if(this.totalsContainer&&!this.config.horizontal){this.totalsContainer.style.width=iContainerWidth+'px';}
for(var iEl=0;iEl<this.paginationContainers.length;iEl++){this.paginationContainers[iEl].style.width=iContainerWidth+'px';}}
if(iContainerHeight){if(this.totalsContainer&&this.config.horizontal){if(this.headerContainer){iContainerHeight+=this.headerContainer.offsetHeight;}
this.totalsContainer.style.height=iContainerHeight+'px';}}};Zapatec.Grid.prototype.getGridDimensions=function(){var oDims={width:0,height:0};if(this.container&&this.container.firstChild&&this.container.firstChild.firstChild){var oTable=this.container.firstChild.firstChild.firstChild;oDims.width=oTable.offsetWidth;oDims.height=oTable.offsetHeight;}
return oDims;};Zapatec.Grid.prototype.syncScroll=function(){if(!this.config.horizontal&&typeof this.outAlignCols=='function'){if(Zapatec.StyleSheet){this.outAlignCols();}else{var oGrid=this;Zapatec.Transport.loadJS({module:'stylesheet',onLoad:function(){oGrid.outAlignCols();}});}}
this.syncRowHeights();if(this.headerContainer){this.createProperty(this.container,'zpGridHeader',this.headerContainer);}
if(this.totalsContainer){this.createProperty(this.container,'zpGridTotals',this.totalsContainer);}
if(this.config.fixedLeft>0||this.config.horizontal){this.createProperty(this.container,'zpGridFixedLeft',document.getElementById('zpGrid'+this.id+'FixedLeft'));if(this.headerContainer){this.createProperty(this.container,'zpGridDataFixedLeft',document.getElementById('zpGrid'+this.id+'DataFixedLeft'));}
if(this.totalsContainer){this.createProperty(this.container,'zpGridTotalsFixedLeft',document.getElementById('zpGrid'+this.id+'TotalsFixedLeft'));}}
if(this.container.zpGridHeader||this.container.zpGridTotals||this.container.zpGridFixedLeft){if(this.container.zpGridHeader){this.container.zpGridHeader.scrollLeft=this.container.scrollLeft;}
if(this.container.zpGridTotals){this.container.zpGridTotals.scrollLeft=this.container.scrollLeft;}
if(this.config.horizontal){this.createProperty(this.container,'onscroll',function(){if(this.zpGridFixedLeft){this.zpGridFixedLeft.style.left=this.scrollLeft+'px';}
if(this.zpGridDataFixedLeft){this.zpGridDataFixedLeft.style.left=this.scrollLeft+'px';}
if(this.zpGridTotalsFixedLeft){this.zpGridTotalsFixedLeft.style.top=this.scrollTop+'px';}
if(this.zpGridHeader){this.zpGridHeader.scrollLeft=this.scrollLeft;}
if(this.zpGridTotals){this.zpGridTotals.scrollTop=this.scrollTop;}});}else{this.createProperty(this.container,'onscroll',function(){if(this.zpGridFixedLeft){this.zpGridFixedLeft.style.left=this.scrollLeft+'px';}
if(this.zpGridDataFixedLeft){this.zpGridDataFixedLeft.style.left=this.scrollLeft+'px';}
if(this.zpGridTotalsFixedLeft){this.zpGridTotalsFixedLeft.style.left=this.scrollLeft+'px';}
if(this.zpGridHeader){this.zpGridHeader.scrollLeft=this.scrollLeft;}
if(this.zpGridTotals){this.zpGridTotals.scrollLeft=this.scrollLeft;}});}}};Zapatec.Grid.prototype.outAlignCols=function(){if(!Zapatec.StyleSheet){return;}
var oStyle=new Zapatec.StyleSheet();var iMaxH=0;var iMaxSpannedH=0;var iSpan=0;var iSpanH=0;for(var iField=0,iCol=0;iField<this.fields.length;iField++){var oField=this.fields[iField];if(!oField||oField.hidden){continue;}
var oSpan=document.getElementById('zpGrid'+this.id+'Span'+
oField.i);if(oSpan){iSpan=oSpan.getAttribute('colspan')*1;iSpanH=oSpan.offsetHeight;}
var oDiv=document.getElementById('zpGrid'+this.id+'Col'+iCol+'Title');if(oDiv){if(!oField.columnWidth){this.outSetColWidth(iCol,oDiv.offsetWidth);}
var iH=oDiv.offsetHeight;if(iSpan){iH+=iSpanH;if(iMaxSpannedH<iH){iMaxSpannedH=iH;}}else{if(iMaxH<iH){iMaxH=iH;}}}
if(iSpan){iSpan--;}
iCol++;}
if(iMaxH){oStyle.addRule('#zpGrid'+this.id+'Container .zpGridTable .zpGridField .zpGridDiv','height:'+iMaxH+'px');}
if(iMaxSpannedH){oStyle.addRule('#zpGrid'+this.id+'Container .zpGridTable .zpGridField .zpGridSpannedDiv','height:'+iMaxSpannedH+'px');}
this.outAlignCols=null;};Zapatec.Grid.prototype.syncRowHeights=function(){if(this.config.horizontal){for(var iField=0;iField<this.fields.length;iField++){var oField=this.fields[iField];if(!oField||oField.hidden){continue;}
var oCellHidden=document.getElementById('zpGrid'+this.id+'Field'+oField.i+'Hidden');var oCell=document.getElementById('zpGrid'+this.id+'Field'+oField.i);if(oCellHidden&&oCell){this.syncRowHeight(oCellHidden,oCell);this.syncColumnWidth(oCellHidden,oCell);}
oCell=document.getElementById('zpGrid'+this.id+'Total0Cell'+oField.i+'Hidden');if(oCellHidden&&oCell){this.syncRowHeight(oCellHidden,oCell);}
oCell=document.getElementById('zpGrid'+this.id+'Total0Cell'+oField.i);if(oCellHidden&&oCell){this.syncRowHeight(oCellHidden,oCell);}
oCellHidden=document.getElementById('zpGrid'+this.id+'Span'+oField.i+'Hidden');oCell=document.getElementById('zpGrid'+this.id+'Span'+oField.i);if(oCellHidden&&oCell){this.syncColumnWidth(oCellHidden,oCell);}}
for(var iField=0;iField<this.fields.length;iField++){var oField=this.fields[iField];if(!oField||oField.hidden){continue;}
for(var iRow=0;true;iRow++){var oCellHidden=document.getElementById('zpGrid'+this.id+'Total'+iRow+'Cell'+oField.i+'Hidden');var oCell=document.getElementById('zpGrid'+this.id+'Total'+iRow+'Cell'+oField.i);if(!oCellHidden||!oCell){break;}
this.syncColumnWidth(oCellHidden,oCell);}}}else if(this.config.fixedLeft>0){var aRows=this.applyPaging();for(var iRow=0;iRow<aRows.length;iRow++){var oRow=aRows[iRow];if(oRow){var oCellHidden=document.getElementById('zpGrid'+this.id+'Row'+
oRow.i+'Cell0Hidden');var oCell=document.getElementById('zpGrid'+this.id+'Row'+
oRow.i+'Cell0');if(oCellHidden&&oCell){this.syncRowHeight(oCellHidden,oCell);}}}
for(var iRow=0;true;iRow++){var oCellHidden=document.getElementById('zpGrid'+this.id+'Total'+
iRow+'Cell0Hidden');var oCell=document.getElementById('zpGrid'+this.id+'Total'+
iRow+'Cell0');if(!oCellHidden||!oCell){break;}
this.syncRowHeight(oCellHidden,oCell);}}};Zapatec.Grid.prototype.syncRowHeight=function(oCellHidden,oCell){var oRow=oCellHidden.parentNode;if(!oRow){return;}
var iHeight=0;var aCells=oRow.getElementsByTagName('td');for(var iCell=0;iCell<aCells.length;iCell++){var iCellH=aCells[iCell].offsetHeight;if(iHeight<iCellH){iHeight=iCellH;}}
var oRowFixed=oCell.parentNode;if(!oRowFixed){return;}
var iHeightFixed=0;var aCellsFixed=oRowFixed.getElementsByTagName('td');for(var iCell=0;iCell<aCellsFixed.length;iCell++){var iCellH=aCellsFixed[iCell].offsetHeight;if(iHeightFixed<iCellH){iHeightFixed=iCellH;}}
var iDiff=iHeight-iHeightFixed;if(iDiff){var oDiv=oCell.getElementsByTagName('div');if(!oDiv){return;}
oDiv=oDiv[0];if(!oDiv){return;}
oDiv.style.height=iHeight+'px';iHeight=0;for(var iCell=0;iCell<aCells.length;iCell++){var iCellH=aCells[iCell].offsetHeight;if(iHeight<iCellH){iHeight=iCellH;}}
iHeightFixed=0;for(var iCell=0;iCell<aCellsFixed.length;iCell++){var iCellH=aCellsFixed[iCell].offsetHeight;if(iHeightFixed<iCellH){iHeightFixed=iCellH;}}
iDiff=iHeight-iHeightFixed;if(iDiff){oDiv.style.height=(iHeight+iDiff)+'px';}}};Zapatec.Grid.prototype.syncColumnWidth=function(oCellHidden,oCell){var oDiv=oCell.getElementsByTagName('div');if(!oDiv){return;}
oDiv=oDiv[0];if(!oDiv){return;}
var iDiff=oCellHidden.offsetWidth-oCell.offsetWidth;if(iDiff){var iWidth=oDiv.offsetWidth+iDiff;oDiv.style.width=iWidth+'px';}};Zapatec.Grid.prototype.visualizeThemeLoad=function(){if(!this.visualize){return;}
this.syncContainers();};Zapatec.Grid.prototype.visualizeRefresh=function(){this.syncContainers();};Zapatec.Grid.prototype.visualizeDataLoad=function(){if(!this.container||!this.visualize){return;}
if(this.headerContainer){this.headerContainer.style.overflow='hidden';}
if(this.totalsContainer){this.totalsContainer.style.overflow='hidden';}
if(Zapatec.StyleSheet){this.outSetCellDims();}else{var oGrid=this;Zapatec.Transport.loadJS({module:'stylesheet',onLoad:function(){oGrid.outSetCellDims();}});}};Zapatec.Grid.prototype.outSetCellDims=function(){if(!Zapatec.StyleSheet){return;}
var oStyle=new Zapatec.StyleSheet(true);oStyle.addRule('#zpGrid'+this.id+'Container .zpGridTable .zpGridDiv','width:'+this.config.columnWidth);oStyle.addRule('#zpGrid'+this.id+'Container .zpGridTable .zpGridDiv','height:'+this.config.rowHeight);if(this.headerContainer){oStyle.addRule('#zpGrid'+this.id+'DataContainer .zpGridTable .zpGridDiv','width:'+this.config.columnWidth);oStyle.addRule('#zpGrid'+this.id+'DataContainer .zpGridTable .zpGridDiv','height:'+this.config.rowHeight);}
if(this.totalsContainer){if(!this.config.horizontal){oStyle.addRule('#zpGrid'+this.id+'TotalsContainer .zpGridTable .zpGridDiv','width:'+this.config.columnWidth);}
oStyle.addRule('#zpGrid'+this.id+'TotalsContainer .zpGridTable .zpGridDiv','height:'+this.config.rowHeight);}
if(!this.config.horizontal){for(var iField=0,iCol=0;iField<this.fields.length;iField++){var oField=this.fields[iField];if(!oField||oField.hidden){continue;}
if(oField.columnWidth){this.outSetColWidth(iCol,oField.columnWidth);}
iCol++;}}};Zapatec.Grid.prototype.outSetColWidth=function(iCol,iWidth){if(!iWidth){return;}
if(!Zapatec.StyleSheet){return;}
var oStyle=new Zapatec.StyleSheet(true);var sWidth=Zapatec.Utils.correctCssLength(iWidth);oStyle.addRule('#zpGrid'+this.id+'Container .zpGridTable .zpGridCell'+iCol+' .zpGridDiv','width:'+sWidth);if(this.headerContainer){oStyle.addRule('#zpGrid'+this.id+'DataContainer .zpGridTable .zpGridCell'+iCol+' .zpGridDiv','width:'+sWidth);}
if(this.totalsContainer){oStyle.addRule('#zpGrid'+this.id+'TotalsContainer .zpGridTable .zpGridCell'+iCol+' .zpGridDiv','width:'+sWidth);}};Zapatec.Grid.prototype.outputTableOpen=function(aHtml,bFixed){aHtml.push('<table class="');aHtml.push(this.getClassName({prefix:'zpGrid'}));aHtml.push('" cellpadding="0" cellspacing="0"><tbody><tr><td class="zpGridTable');if(bFixed){aHtml.push(' zpGridTableFixed');}
aHtml.push('"><table class="zpGridTableTable" style="');if(this.data.style){aHtml.push(this.data.style);}
aHtml.push('"><tbody>');};Zapatec.Grid.prototype.outputTableClose=function(aHtml){aHtml.push('</tbody></table></td></tr></tbody></table>');};Zapatec.Grid.prototype.outputFields=function(aHtml,aCols,aFixedCols,aSpans,aRows,aTotals,bFixed){if(this.config.horizontal){this.outputFieldsHoriz(aHtml,aCols,aFixedCols,aSpans,aRows,aTotals,bFixed);}else{this.outputFieldsVert(aHtml,aCols,aSpans,aRows,bFixed);}};Zapatec.Grid.prototype.outputFieldsHoriz=function(aHtml,aCols,aFixedCols,aSpans,aRows,aTotals,bFixed,bSecondRow){var iSpanned=0;for(var iCol=0;iCol<aFixedCols.length;iCol++){var oField=aFixedCols[iCol];var aCl=[];aCl.push('zpGridCol zpGridCol');aCl.push(iCol);aCl.push(' zpGridColFixed zpGridColFixed');aCl.push(iCol);if(iCol%2==1){aCl.push(' zpGridColOdd zpGridColFixedOdd');}else{aCl.push(' zpGridColEven zpGridColFixedEven');}
if(iCol==aFixedCols.length-1){aCl.push(' zpGridColFixedLast');}
var sClass=aCl.join('');var aTr=[];aTr.push('<tr id="zpGrid');aTr.push(this.id);aTr.push('Col');aTr.push(oField.i);if(bFixed){aTr.push('Fixed');}
aTr.push('" class="');aTr.push(sClass);if(this.data.headerStyle){aTr.push('" style="');aTr.push(this.data.headerStyle);}
aTr.push('">');if(aSpans.length){var iSpan=aSpans[oField.i];if(iSpan){this.outputSpan(aTr,oField,iCol,iSpan,aCols.length,!bFixed);iSpanned=iSpan-1;this.outputField(aTr,oField,iCol,aCols.length,!bFixed);}else{if(iSpanned){this.outputField(aTr,oField,iCol,aCols.length,!bFixed);iSpanned--;}else{this.outputField(aTr,oField,iCol,aCols.length,!bFixed,true);}}}else{this.outputField(aTr,oField,iCol,aCols.length,!bFixed);}
if(!bFixed){for(var iRow=0;iRow<aRows.length;iRow++){this.outputCell(aTr,oField,aRows[iRow],iCol,iRow,aRows.length);}
if(aTotals&&!this.totalsContainer&&this.outputTotalsCell){for(var iRow=0;iRow<aTotals.length;iRow++){this.outputTotalsCell(aTr,oField,aTotals[iRow],iCol,iRow,aTotals.length);}}}
aTr.push('</tr>');aHtml.push(aTr.join(''));}};Zapatec.Grid.prototype.outputFieldsVert=function(aHtml,aCols,aSpans,aRows,bFixed,bSecondRow){var aTr=[];aTr.push('<tr id="zpGrid');aTr.push(this.id);aTr.push('Head');if(bFixed){aTr.push('Fixed');}
if(bSecondRow){aTr.push('2');}
aTr.push('" class="zpGridRow zpGridRow0 zpGridRowEven"');if(this.data.headerStyle){aTr.push(' style="');aTr.push(this.data.headerStyle);aTr.push('"');}
aTr.push('>');var iFields=this.fields.length;var iShow=bFixed?this.config.fixedLeft:iFields;var bTwoRows=false;if(!bSecondRow&&aSpans.length){bTwoRows=true;}
var bHiddenCols=(!bFixed&&this.config.fixedLeft);for(var iField=0,iSpan=0,iCol=0;iField<iFields&&iField<iShow;iField++){var oField=this.fields[iField];if(!oField||oField.hidden){continue;}
if(aSpans[iField]){iSpan+=aSpans[iField];}
var bHidden=(bHiddenCols&&iCol<this.config.fixedLeft);if(!bSecondRow){if(iSpan){this.outputSpan(aTr,oField,iCol,iSpan,aCols.length,bHidden);iCol+=iSpan;iField+=iSpan-1;iSpan=0;}else{this.outputField(aTr,oField,iCol,aCols.length,bHidden,bTwoRows);iCol++;}}else{if(iSpan){this.outputField(aTr,oField,iCol,aCols.length,bHidden);iSpan--;}
iCol++;}}
aTr.push('</tr>');aHtml.push(aTr.join(''));if(bTwoRows){this.outputFieldsVert(aHtml,aCols,aSpans,aRows,bFixed,true);}};Zapatec.Grid.prototype.outputSpan=function(aHtml,oField,iCol,iSpan,iCols,bHidden){var aCl=[];aCl.push('zpGridCell zpGridCell');aCl.push(iCol);aCl.push(' zpGridField zpGridField');aCl.push(iCol);aCl.push(' zpGridFieldSpan zpGridFieldSpan');aCl.push(iCol);if(iCol%2==1){aCl.push(' zpGridCellOdd zpGridFieldOdd zpGridFieldSpanOdd');}else{aCl.push(' zpGridCellEven zpGridFieldEven zpGridFieldSpanEven');}
if(iCol+iSpan==iCols){aCl.push(' zpGridCellLast zpGridFieldLast zpGridFieldSpanLast');}
var sClass=aCl.join('');aCl.push(' zpGridCellActive zpGridCellActive');aCl.push(iCol);aCl.push(' zpGridFieldActive zpGridFieldActive');aCl.push(iCol);aCl.push(' zpGridFieldSpanActive zpGridFieldSpanActive');aCl.push(iCol);if(iCol%2==1){aCl.push(' zpGridCellActiveOdd zpGridFieldActiveOdd zpGridFieldSpanActiveOdd');}else{aCl.push(' zpGridCellActiveEven zpGridFieldActiveEven zpGridFieldSpanActiveEven');}
if(iCol+iSpan==iCols){aCl.push(' zpGridCellActiveLast zpGridFieldActiveLast zpGridFieldSpanActiveLast');}
var sClassActive=aCl.join('');var aTd=[];if(this.config.horizontal){aTd.push('<td rowspan="');}else{aTd.push('<td colspan="');}
aTd.push(iSpan);aTd.push('" class="');aTd.push(sClass);aTd.push('" id="zpGrid');aTd.push(this.id);aTd.push('Span');aTd.push(oField.i);if(bHidden){aTd.push('Hidden');}else{aTd.push('" onmouseover="this.className=\'');aTd.push(sClassActive);aTd.push('\'" onmouseout="this.className=\'');aTd.push(sClass);aTd.push("'");}
if(oField.spanStyle){aTd.push('" style="');aTd.push(oField.spanStyle);}
aTd.push('"><div style="position:relative;overflow:visible">');if(!bHidden){if(!this.config.horizontal){aTd.push('<div id="zpGrid');aTd.push(this.id);aTd.push('Span');aTd.push(iCol);aTd.push('Margin" class="zpGridColMargin"></div><div id="zpGrid');aTd.push(this.id);aTd.push('Span');aTd.push(iCol);aTd.push('Resize" onmousedown="return Zapatec.Drag.start(\
       window.event,this.id,{horizontal:true,limitLeft:10})"\
       class="zpGridColResizeHidden"></div>');}}
aTd.push('<span class="zpGridSpan">');aTd.push(oField.spanTitle);aTd.push('</span></div></td>');aHtml.push(aTd.join(''));};Zapatec.Grid.prototype.outputField=function(aHtml,oField,iCol,iCols,bHidden,bTwoRows){var aCl=[];aCl.push('zpGridCell zpGridCell');aCl.push(iCol);aCl.push(' zpGridField zpGridField');aCl.push(iCol);if(iCol%2==1){aCl.push(' zpGridCellOdd zpGridFieldOdd');}else{aCl.push(' zpGridCellEven zpGridFieldEven');}
if(iCol==iCols-1){aCl.push(' zpGridCellLast zpGridFieldLast');}
if(!oField.nosort){if(oField.sorted){aCl.push(' zpGridSortedAsc');}else if(oField.sortedDesc){aCl.push(' zpGridSortedDesc');}}
var sClass=aCl.join('');aCl.push(' zpGridCellActive zpGridCellActive');aCl.push(iCol);aCl.push(' zpGridFieldActive zpGridFieldActive');aCl.push(iCol);if(iCol%2==1){aCl.push(' zpGridCellActiveOdd zpGridFieldActiveOdd');}else{aCl.push(' zpGridCellActiveEven zpGridFieldActiveEven');}
if(iCol==iCols-1){aCl.push(' zpGridCellActiveLast zpGridFieldActiveLast');}
if(!oField.nosort){if(oField.sorted){aCl.push(' zpGridSortedAscActive');}else if(oField.sortedDesc){aCl.push(' zpGridSortedDescActive');}}
var sClassActive=aCl.join('');var aTd=[];aTd.push('<td');if(bTwoRows){if(this.config.horizontal){aTd.push(' colspan="2"');}else{aTd.push(' rowspan="2"');}}
aTd.push(' class="');aTd.push(sClass);aTd.push('" id="zpGrid');aTd.push(this.id);aTd.push('Field');aTd.push(oField.i);if(bHidden){aTd.push('Hidden');}else{aTd.push('" onmouseover="this.className=\'');aTd.push(sClassActive);aTd.push('\'" onmouseout="this.className=\'');aTd.push(sClass);aTd.push("'");if(!oField.nosort){aTd.push('" onclick="Zapatec.Grid.sort(\'');aTd.push(this.id);aTd.push("','");aTd.push(oField.i);aTd.push("')");}}
if(oField.style){aTd.push('" style="');aTd.push(oField.style);}
aTd.push('"><div style="position:relative;overflow:visible">');if(!bHidden){if(!this.config.horizontal){aTd.push('<div id="zpGrid');aTd.push(this.id);aTd.push('Col');aTd.push(iCol);aTd.push('Margin" class="zpGridColMargin"></div><div id="zpGrid');aTd.push(this.id);aTd.push('Col');aTd.push(iCol);aTd.push('Resize" onmousedown="return Zapatec.Drag.start(\
       window.event,this.id,{horizontal:true,limitLeft:10})"\
       class="zpGridColResizeHidden"></div>');}}
aTd.push('<div class="zpGridDiv');if(bTwoRows){aTd.push(' zpGridSpannedDiv');}
if(this.getClassByType){aTd.push(' ');aTd.push(this.getClassByType(this.getFieldType(oField)));}
aTd.push('" id="zpGrid');aTd.push(this.id);aTd.push('Col');aTd.push(iCol);aTd.push('Title');if(bHidden){aTd.push('Hidden');}
aTd.push('"><span class="zpGridSpan">');aTd.push(oField.title);aTd.push('</span></div></div></td>');aHtml.push(aTd.join(''));};Zapatec.Grid.prototype.outputRows=function(aHtml,aCols,aFixedCols,aSpans,aRows,aTotals,bFixed,oContainer){if(this.config.horizontal){this.outputRowsHoriz(aHtml,aCols,aFixedCols,aSpans,aRows,aTotals,bFixed);}else{this.outputRowsVert(aHtml,aCols,aFixedCols,aRows,bFixed,oContainer);if(aTotals&&!this.totalsContainer&&this.outputTotals){this.outputTotals(aHtml,aCols,aFixedCols,aTotals,bFixed);}}};Zapatec.Grid.prototype.outputRowsHoriz=function(aHtml,aCols,aFixedCols,aSpans,aRows,aTotals,bFixed){var iSpanned=0;for(var iCol=aFixedCols.length;iCol<aCols.length;iCol++){var oField=aCols[iCol];var aCl=[];aCl.push('zpGridCol zpGridCol');aCl.push(iCol);if(iCol%2==1){aCl.push(' zpGridColOdd');}else{aCl.push(' zpGridColEven');}
if(iCol==aCols.length-1){aCl.push(' zpGridColLast');}
var sClass=aCl.join('');var aTr=[];aTr.push('<tr id="zpGrid');aTr.push(this.id);aTr.push('Col');aTr.push(oField.i);if(bFixed){aTr.push('Fixed');}
aTr.push('" class="');aTr.push(sClass);aTr.push('">');if(aSpans.length){var iSpan=aSpans[oField.i];if(iSpan){this.outputSpan(aTr,oField,iCol,iSpan,aCols.length,!bFixed);iSpanned=iSpan-1;this.outputField(aTr,oField,iCol,aCols.length,!bFixed);}else{if(iSpanned){this.outputField(aTr,oField,iCol,aCols.length,!bFixed);iSpanned--;}else{this.outputField(aTr,oField,iCol,aCols.length,!bFixed,true);}}}else{this.outputField(aTr,oField,iCol,aCols.length,!bFixed);}
if(!bFixed){for(var iRow=0;iRow<aRows.length;iRow++){this.outputCell(aTr,oField,aRows[iRow],iCol,iRow,aRows.length);}
if(aTotals&&!this.totalsContainer&&this.outputTotalsCell){for(var iRow=0;iRow<aTotals.length;iRow++){this.outputTotalsCell(aTr,oField,aTotals[iRow],iCol,iRow,aTotals.length);}}}
aTr.push('</tr>');aHtml.push(aTr.join(''));}};Zapatec.Grid.prototype.outputRowsVert=function(aHtml,aCols,aFixedCols,aRows,bFixed,oContainer){for(var iRow=0;iRow<=aRows.length;iRow++){var oRow=aRows[iRow];if(!oRow){continue;}
if(oContainer&&iRow>0&&iRow%10==0){this.outputTableClose(aHtml);var oDiv=Zapatec.Utils.createElement('div',oContainer,true);oDiv.innerHTML=aHtml.join('');aHtml.splice(0,aHtml.length);this.outputTableOpen(aHtml,bFixed);}
var aCl=[];var iRowN=iRow+1;aCl.push('zpGridRow zpGridRow');aCl.push(iRowN);aCl.push(' zpGridRow');aCl.push(iRowN%2==1?'Odd':'Even');if(iRowN==aRows.length){aCl.push(' zpGridRowLast');}
if(this.config.selectRows&&oRow.selected){aCl.push(' zpGridRowSelected zpGridRowSelected');aCl.push(iRowN);aCl.push(' zpGridRowSelected');aCl.push(iRowN%2==1?'Odd':'Even');if(iRowN==aRows.length){aCl.push(' zpGridRowSelectedLast');}}
if(oRow.invalid){aCl.push(' zpGridRowInvalid zpGridRowInvalid');aCl.push(iRowN);aCl.push(' zpGridRowInvalid');aCl.push(iRowN%2==1?'Odd':'Even');if(iRowN==aRows.length){aCl.push(' zpGridRowInvalidLast');}}
var sClass=aCl.join('');var aClA=[];aClA.push(' zpGridRowActive zpGridRowActive');aClA.push(iRowN);aClA.push(' zpGridRowActive');aClA.push(iRowN%2==1?'Odd':'Even');if(iRowN==aRows.length){aClA.push(' zpGridRowActiveLast');}
var sClassActive=aClA.join('');var aTr=[];aTr.push('<tr id="zpGrid');aTr.push(this.id);aTr.push('Row');aTr.push(oRow.i);if(bFixed){aTr.push('Fixed');}
aTr.push('" class="');aTr.push(sClass);if(this.config.activeRows){aTr.push('" onmouseover="if(this.className.indexOf(\
       \'zpGridRowActive\')==-1){this.className+=\'');aTr.push(sClassActive);aTr.push("'");if(bFixed){aTr.push(";document.getElementById('zpGrid");aTr.push(this.id);aTr.push('Row');aTr.push(oRow.i);aTr.push("').className+='");aTr.push(sClassActive);aTr.push("'");}else if(this.config.fixedLeft>0){aTr.push(";document.getElementById('zpGrid");aTr.push(this.id);aTr.push('Row');aTr.push(oRow.i);aTr.push("Fixed').className+='");aTr.push(sClassActive);aTr.push("'");}
aTr.push("}\" onmouseout=\"this.className=this.className.replace(\
       / zpGridRowActive[^ ]*/g,'')");if(bFixed){aTr.push(";var oRow=document.getElementById('zpGrid");aTr.push(this.id);aTr.push('Row');aTr.push(oRow.i);aTr.push("');oRow.className=oRow.className.replace(\
         / zpGridRowActive[^ ]*/g,'')");}else if(this.config.fixedLeft>0){aTr.push(";var oRow=document.getElementById('zpGrid");aTr.push(this.id);aTr.push('Row');aTr.push(oRow.i);aTr.push("Fixed');oRow.className=oRow.className.replace(\
         / zpGridRowActive[^ ]*/g,'')");}}
aTr.push('" ondblclick="this.onmouseout()" style="');if(oRow.style){aTr.push(oRow.style);}
aTr.push('">');if(bFixed){for(var iCol=0;iCol<aFixedCols.length;iCol++){this.outputCell(aTr,aFixedCols[iCol],oRow,iRow,iCol,aCols.length);}}else{for(var iCol=0;iCol<aCols.length;iCol++){var bHidden=(iCol<aFixedCols.length);this.outputCell(aTr,aCols[iCol],oRow,iRow,iCol,aCols.length,bHidden);}}
aTr.push('</tr>');aHtml.push(aTr.join(''));}};Zapatec.Grid.prototype.outputCell=function(aTr,oField,oRow,iRow,iCol,iCols,bHidden){var oCell=this.getCellByRow(oRow,oField.i);if(!oCell){return;}
var aCl=[];aCl.push('zpGridCell zpGridCell');aCl.push(iCol);aCl.push(' zpGridCellData zpGridCellData');aCl.push(iCol);if(iCol%2==1){aCl.push(' zpGridCellOdd zpGridCellDataOdd');}else{aCl.push(' zpGridCellEven zpGridCellDataEven');}
if(iCol+1==iCols){aCl.push(' zpGridCellLast zpGridCellDataLast');}
if(this.config.selectCells&&oCell.selected){aCl.push(' zpGridCellSelected zpGridCellSelected');aCl.push(iCol);if(iCol%2==1){aCl.push(' zpGridCellSelectedOdd');}else{aCl.push(' zpGridCellSelectedEven');}
if(iCol==iCols-1){aCl.push(' zpGridCellSelectedLast');}}
if(oCell.invalid){aCl.push(' zpGridCellInvalid zpGridCellInvalid');aCl.push(iCol);if(iCol%2==1){aCl.push(' zpGridCellInvalidOdd');}else{aCl.push(' zpGridCellInvalidEven');}
if(iCol==iCols-1){aCl.push(' zpGridCellInvalidLast');}}
var sClass=aCl.join('');var aClA=[];aClA.push(' zpGridCellActive zpGridCellActive');aClA.push(iCol);aClA.push(' zpGridCellDataActive zpGridCellDataActive');aClA.push(iCol);if(iCol%2==1){aClA.push(' zpGridCellActiveOdd zpGridCellDataActiveOdd');}else{aClA.push(' zpGridCellActiveEven zpGridCellDataActiveEven');}
if(iCol==iCols-1){aClA.push(' zpGridCellActiveLast zpGridCellDataActiveLast');}
var sClassActive=aClA.join('');var aTd=[];aTd.push('<td class="');aTd.push(sClass);aTd.push('" id="zpGrid');aTd.push(this.id);aTd.push('Row');aTd.push(oRow.i);aTd.push('Cell');aTd.push(oCell.i);if(bHidden){aTd.push('Hidden');}else{if(this.config.activeCells){aTd.push('" onmouseover="if(this.className.indexOf(\
       \'zpGridCellActive\')==-1)this.className+=\'');aTd.push(sClassActive);aTd.push('\'" onmouseout="this.className=this.className.replace(\
       / zpGrid[^ ]+Active[^ ]*/g,\'\')');}
aTd.push('" onclick="Zapatec.Grid.rowOnClick(\'');aTd.push(this.id);aTd.push("','");aTd.push(oRow.i);aTd.push("','");aTd.push(oCell.i);aTd.push('\')" ondblclick="this.onmouseout();\
     Zapatec.Grid.rowOnDblClick(\'');aTd.push(this.id);aTd.push("','");aTd.push(oRow.i);aTd.push("','");aTd.push(oCell.i);aTd.push("')");}
var sStyle=this.getCellStyle(oCell,iRow);if(sStyle){aTd.push('" style="');aTd.push(sStyle);}
aTd.push('">');this.outputCellValue(aTd,oField,oCell);aTd.push('</td>');aTr.push(aTd.join(''));};Zapatec.Grid.prototype.outputCellValue=function(aTd,oField,oCell,bHidden){aTd.push('<div class="zpGridDiv');if(this.getClassByType){aTd.push(' ');aTd.push(this.getClassByType(this.getFieldType(oField)));}
if(bHidden){aTd.push('" style="visibility:hidden');}
aTd.push('">');var sData=this.getCellData(oCell);if(sData&&typeof sData!='string'){sData=sData.toString();}
if(!sData||!sData.length){sData='&nbsp;';}
aTd.push(sData);aTd.push('</div>');};Zapatec.Grid.prototype.outputPagination=function(aHtml,iContainer){if(typeof this.config.callbackPaginationDisplay=='function'||this.config.rowsPerPage<=0){return;}
if(!iContainer||iContainer<2){iContainer='';}else{iContainer='_'+iContainer;}
if(this.paginationContainers.length){aHtml.push('<div id="zpGrid');aHtml.push(this.id);aHtml.push('PaginationContainer');aHtml.push(iContainer);aHtml.push('"><div><table class="');aHtml.push(this.getClassName({prefix:'zpGrid'}));aHtml.push('" cellpadding="0" cellspacing="0" \
     style="width:100%"><tbody>');}
aHtml.push('<tr><td class="zpGridPagination" id="zpGrid');aHtml.push(this.id);aHtml.push('Pagination');aHtml.push(iContainer);aHtml.push('">Page');if(this.currentPage>0){aHtml.push(' <span id="zpGrid');aHtml.push(this.id);aHtml.push('FirstPage');aHtml.push(iContainer);aHtml.push('" class="zpGridFirstPage" \
     onclick="Zapatec.Grid.firstPage(\'');aHtml.push(this.id);aHtml.push('\')">&lt;&lt;</span> <span id="zpGrid');aHtml.push(this.id);aHtml.push('PrevPage');aHtml.push(iContainer);aHtml.push('" class="zpGridPrevPage" \
     onclick="Zapatec.Grid.previousPage(\'');aHtml.push(this.id);aHtml.push('\')">&lt;</span>');}
var iPages=this.totalPages();var iCurrentPage=this.getCurrentPageNumber();var iFirstPage=iCurrentPage-4;var iLastPage=iCurrentPage+5;for(var iPage=iFirstPage;iPage<iLastPage&&iPage<=iPages;iPage++){if(iPage<1){continue;}
aHtml.push(' <span id="zpGrid');aHtml.push(this.id);aHtml.push('Page');aHtml.push(iPage);aHtml.push(iContainer);aHtml.push('" class="zpGrid');if(iPage==iCurrentPage){aHtml.push('CurrentPage">');}else{aHtml.push('Page" onclick="Zapatec.Grid.gotoPage(\'');aHtml.push(this.id);aHtml.push("','");aHtml.push(iPage);aHtml.push('\')">');}
aHtml.push(iPage);aHtml.push('</span>');}
if(this.currentPage<iPages-1){aHtml.push(' <span id="zpGrid');aHtml.push(this.id);aHtml.push('NextPage');aHtml.push(iContainer);aHtml.push('" class="zpGridNextPage" \
     onclick="Zapatec.Grid.nextPage(\'');aHtml.push(this.id);aHtml.push('\')">&gt;</span> <span id="zpGrid');aHtml.push(this.id);aHtml.push('LastPage');aHtml.push(iContainer);aHtml.push('" class="zpGridLastPage" \
     onclick="Zapatec.Grid.lastPage(\'');aHtml.push(this.id);aHtml.push('\')">&gt;&gt;</span>');}
aHtml.push(' of ');aHtml.push(iPages);aHtml.push(' (');aHtml.push(this.recordsDisplayed());aHtml.push(' rows)</td></tr>');if(this.paginationContainers.length){aHtml.push('</tbody></table></div></div>');}};Zapatec.Grid.prototype.refreshContainer=function(){if(!this.container){alert("Can not find container for grid");return;}
this.container.style.position='relative';if(this.container.currentStyle&&this.container.currentStyle['width']=='auto'){this.container.style.width='100%';}
var aCols=[];var iFixedFields=Math.min(this.fields.length,this.config.fixedLeft);var aFixedCols=[];var aSpans=[];for(var iField=0;iField<this.fields.length;iField++){var oField=this.fields[iField];if(!oField||oField.hidden){continue;}
aCols.push(oField);if(iField<iFixedFields){aFixedCols.push(oField);}
var iSpan=this.getFieldSpan(oField);if(iSpan){for(var iSpanOffset=1;iSpanOffset<iSpan;iSpanOffset++){var oSpanField=this.fields[iField+iSpanOffset];if(!oSpanField||oSpanField.hidden){iSpan--;}else if(this.getFieldSpan(oSpanField)){iSpan=iSpanOffset;break;}}
if(iSpan){aSpans[iField]=iSpan;}}}
var aRows=this.applyPaging();var aTotals;if(this.getTotals){aTotals=this.getTotals();}
if(this.headerContainer){this.headerContainer.style.position='relative';if(this.headerContainer.currentStyle&&this.headerContainer.currentStyle['width']=='auto'){this.headerContainer.style.width='100%';}
var aHtml=[];aHtml.push('<div id="zpGrid');aHtml.push(this.id);aHtml.push('Container"><div>');this.outputTableOpen(aHtml);this.outputFields(aHtml,aCols,aFixedCols,aSpans,aRows,aTotals);this.outputTableClose(aHtml);aHtml.push('</div>');if(this.config.fixedLeft>0||this.config.horizontal){aHtml.push('<div id="zpGrid');aHtml.push(this.id);if(this.headerContainer.style.setAttribute){aHtml.push('FixedLeft" style="position:absolute;top:0px;\
         left:expression(this.offsetParent.scrollLeft+\'px\')"><div>');}else{aHtml.push('FixedLeft" style="position:absolute;top:0px;left:');aHtml.push(this.container.scrollLeft);aHtml.push('px"><div>');}
this.outputTableOpen(aHtml,true);this.outputFields(aHtml,aCols,aFixedCols,aSpans,aRows,aTotals,true);this.outputTableClose(aHtml);aHtml.push('</div></div>');}
aHtml.push('</div>');this.headerContainer.innerHTML=aHtml.join('');this.container.innerHTML='';var oContainer=Zapatec.Utils.createElement('div',this.container,true);oContainer.id='zpGrid'+this.id+'DataContainer';aHtml=[];this.outputTableOpen(aHtml);this.outputRows(aHtml,aCols,aFixedCols,aSpans,aRows,aTotals,false,oContainer);if(this.paginationContainers.length){this.outputTableClose(aHtml);var oDiv=Zapatec.Utils.createElement('div',oContainer,true);oDiv.innerHTML=aHtml.join('');aHtml=[];this.outputPagination(aHtml);for(var iEl=0;iEl<this.paginationContainers.length;iEl++){this.paginationContainers[iEl].innerHTML=aHtml.join('');}}else{aHtml.push('</tbody></table></td></tr>');this.outputPagination(aHtml);aHtml.push('</tbody></table>');var oDiv=Zapatec.Utils.createElement('div',oContainer,true);oDiv.innerHTML=aHtml.join('');}
if(this.config.fixedLeft>0||this.config.horizontal){var oFixed=Zapatec.Utils.createElement('div',oContainer,true);oFixed.id='zpGrid'+this.id+'DataFixedLeft';if(oFixed.style.setAttribute){oFixed.style.setAttribute('cssText','position:absolute;top:0px;\
         left:expression(this.offsetParent.scrollLeft+"px")',0);}else{oFixed.style.position='absolute';oFixed.style.top='0px';oFixed.style.left=this.container.scrollLeft+'px';}
var aHtml=[];this.outputTableOpen(aHtml,true);this.outputRows(aHtml,aCols,aFixedCols,aSpans,aRows,aTotals,true,oFixed);this.outputTableClose(aHtml);var oDiv=Zapatec.Utils.createElement('div',oFixed,true);oDiv.innerHTML=aHtml.join('');}
if(!this.config.horizontal){var oRs=Zapatec.Utils.createElement('div',oContainer,true);oRs.id='zpGrid'+this.id+'DataColResize';oRs.style.position='absolute';oRs.style.top='0px';oRs.style.left='0px';oRs.style.display='none';oRs.className=this.getClassName({prefix:'zpGrid',suffix:'ColResize'});}}else{var aHtml=[];aHtml.push('<div id="zpGrid');aHtml.push(this.id);aHtml.push('Container"><div>');this.outputTableOpen(aHtml);this.outputFields(aHtml,aCols,aFixedCols,aSpans,aRows,aTotals);this.outputRows(aHtml,aCols,aFixedCols,aSpans,aRows,aTotals);if(this.paginationContainers.length){this.outputTableClose(aHtml);aHtml.push('</div></div>');this.container.innerHTML=aHtml.join('');aHtml=[];this.outputPagination(aHtml);for(var iEl=0;iEl<this.paginationContainers.length;iEl++){this.paginationContainers[iEl].innerHTML=aHtml.join('');}}else{aHtml.push('</tbody></table></td></tr>');this.outputPagination(aHtml);aHtml.push('</tbody></table></div></div>');this.container.innerHTML=aHtml.join('');}
if(this.config.fixedLeft>0||this.config.horizontal){var oFixed=Zapatec.Utils.createElement('div',document.getElementById('zpGrid'+this.id+'Container'),true);oFixed.id='zpGrid'+this.id+'FixedLeft';if(oFixed.style.setAttribute){oFixed.style.setAttribute('cssText','position:absolute;top:0px;\
         left:expression(this.offsetParent.scrollLeft+"px")',0);}else{oFixed.style.position='absolute';oFixed.style.top='0px';oFixed.style.left=this.container.scrollLeft+'px';}
var aHtml=[];this.outputTableOpen(aHtml,true);this.outputFields(aHtml,aCols,aFixedCols,aSpans,aRows,aTotals,true);this.outputRows(aHtml,aCols,aFixedCols,aSpans,aRows,aTotals,true);this.outputTableClose(aHtml);var oDiv=Zapatec.Utils.createElement('div',oFixed,true);oDiv.innerHTML=aHtml.join('');}}
if(aTotals&&this.totalsContainer&&this.outputTotals){this.totalsContainer.style.position='relative';if(this.totalsContainer.currentStyle&&this.totalsContainer.currentStyle['width']=='auto'){this.totalsContainer.style.width='100%';}
var aHtml=[];aHtml.push('<div id="zpGrid');aHtml.push(this.id);aHtml.push('TotalsContainer"><div>');this.outputTableOpen(aHtml);this.outputTotals(aHtml,aCols,aFixedCols,aTotals);this.outputTableClose(aHtml);aHtml.push('</div>');if(this.config.fixedLeft>0){aHtml.push('<div id="zpGrid');aHtml.push(this.id);if(this.totalsContainer.style.setAttribute){aHtml.push('TotalsFixedLeft" style="position:absolute;top:0px;\
         left:expression(this.offsetParent.scrollLeft+\'px\')"><div>');}else{aHtml.push('TotalsFixedLeft" style="position:absolute;top:0px;left:');aHtml.push(this.container.scrollLeft);aHtml.push('px"><div>');}
this.outputTableOpen(aHtml,true);this.outputTotals(aHtml,aCols,aFixedCols,aTotals,true);this.outputTableClose(aHtml);aHtml.push('</div></div>');}
aHtml.push('</div>');this.totalsContainer.innerHTML=aHtml.join('');if(!this.config.horizontal){var oRs=Zapatec.Utils.createElement('div',this.totalsContainer,true);oRs.id='zpGrid'+this.id+'TotalsColResize';oRs.style.position='absolute';oRs.style.top='0px';oRs.style.left='0px';oRs.style.display='none';oRs.className=this.getClassName({prefix:'zpGrid',suffix:'ColResize'});}}
this.syncScroll();if(typeof this.config.callbackTotalsDisplay=='function'){this.config.callbackTotalsDisplay(this,aTotals);}else if(aTotals&&typeof this.config.callbackTotalDisplay=='function'){for(var iRow=0;iRow<aTotals.length;iRow++){this.config.callbackTotalDisplay(this,aTotals[iRow]);}}
if(typeof this.config.callbackPaginationDisplay=='function'&&this.config.rowsPerPage>0){this.config.callbackPaginationDisplay(this);}
var oGrid=this;setTimeout(function(){oGrid.onRefresh();},0);};Zapatec.Grid.prototype.visualizeSelectRow=function(oRow){if(!oRow){return;}
var oTr=document.getElementById('zpGrid'+this.id+'Row'+oRow.i);if(oTr){/zpGridRow(\d+)/.exec(oTr.className);var sRow=RegExp.$1;var aClSelected=[];aClSelected.push(' zpGridRowSelected zpGridRowSelected');aClSelected.push(sRow);aClSelected.push(' zpGridRowSelected');aClSelected.push(sRow%2==1?'Odd':'Even');if(oTr.className.indexOf('zpGridRowLast')>=0){aClSelected.push(' zpGridRowSelectedLast');}
var sClassSelected=aClSelected.join('');oTr.className+=sClassSelected;oTr=document.getElementById('zpGrid'+this.id+'Row'+oRow.i+'Fixed');if(oTr){oTr.className+=sClassSelected;}}};Zapatec.Grid.prototype.visualizeUnselectRow=function(oRow){if(!oRow){return;}
var oTr=document.getElementById('zpGrid'+this.id+'Row'+oRow.i);if(oTr){oTr.className=oTr.className.replace(/ zpGridRowSelected[^ ]*/g,'');oTr=document.getElementById('zpGrid'+this.id+'Row'+oRow.i+'Fixed');if(oTr){oTr.className=oTr.className.replace(/ zpGridRowSelected[^ ]*/g,'');}}};Zapatec.Grid.prototype.visualizeSelectCell=function(oCell){if(!oCell){return;}
var oTd=document.getElementById('zpGrid'+this.id+'Row'+oCell.r+'Cell'+oCell.i);if(oTd){var aClSelected=[];aClSelected.push(' zpGridCellSelected zpGridCellSelected');aClSelected.push(oCell.i);aClSelected.push(' zpGridCellSelected');aClSelected.push(oCell.i%2==1?'Odd':'Even');if(oCell.i==this.fields.length-1){aClSelected.push(' zpGridCellSelectedLast');}
oTd.className+=aClSelected.join('');}};Zapatec.Grid.prototype.visualizeUnselectCell=function(oCell){if(!oCell){return;}
var oTd=document.getElementById('zpGrid'+this.id+'Row'+oCell.r+'Cell'+oCell.i);if(oTd){oTd.className=oTd.className.replace(/ zpGridCellSelected[^ ]*/g,'');}};Zapatec.Grid.prototype.visualizeFilterOut=function(oFilterOut,aVals){if(!oFilterOut){return;}
var oContainer=Zapatec.Widget.getElementById(oFilterOut.container);if(!oContainer){return;}
var aCols=oFilterOut.column;if(!(aCols instanceof Array)){aCols=[aCols];}
var aFields=[];for(var iCol=0;iCol<aCols.length;iCol++){var oField=this.fields[aCols[iCol]];if(!oField){continue;}
aFields.push(oField);}
if(!aFields.length){return;}
var sCols=aCols.join(',');var aHtml=[];aHtml.push('<div><a href="javascript:void(0)" \
   onclick="Zapatec.Grid.checkboxSelectAllOnClick(\'');aHtml.push(this.id);aHtml.push("',[");aHtml.push(sCols);aHtml.push('])">Select all</a> | <a href="javascript:void(0)" \
   onclick="Zapatec.Grid.checkboxClearAllOnClick(\'');aHtml.push(this.id);aHtml.push("',[");aHtml.push(sCols);aHtml.push('])">Clear</a></div>');for(var iVal=0;iVal<aVals.length;iVal++){var sVal=aVals[iVal].v+'';var sEscaped=escape(sVal);aHtml.push('<div><input type="checkbox" ');for(var iField=0;iField<aFields.length;iField++){var oField=aFields[iField];if(!(oField.hiddenValues instanceof Array)||Zapatec.Utils.arrIndexOf(oField.hiddenValues,sVal)<0){aHtml.push('checked ');break;}}
aHtml.push('onclick="');if(oFilterOut.onclick){aHtml.push(oFilterOut.onclick);aHtml.push(';');}
aHtml.push("Zapatec.Grid.checkboxOnClick('");aHtml.push(this.id);aHtml.push("',[");aHtml.push(sCols);aHtml.push("],unescape('");aHtml.push(sEscaped);aHtml.push("'),this.checked)\" /><a href=\"javascript:void(0)\" \
     onclick=\"Zapatec.Grid.checkboxLinkOnClick('");aHtml.push(this.id);aHtml.push("',[");aHtml.push(sCols);aHtml.push("],unescape('");aHtml.push(sEscaped);aHtml.push("'))\">");aHtml.push(sVal);aHtml.push('</a></div>');}
oContainer.innerHTML=aHtml.join('');};Zapatec.Grid.prototype.getBusyContainer=function(){if(!this.container){return;}
var oContainer=this.container.firstChild;if(oContainer){oContainer=oContainer.firstChild;if(oContainer){var oTable=oContainer.firstChild;if(oTable&&oTable.tagName.toLowerCase()=='table'){if(oTable.offsetWidth>this.container.offsetWidth){oContainer=this.container;}else{oContainer=this.container;oContainer.style.overflow='visible';oContainer.style.width=oTable.offsetWidth+'px';}}else{oContainer=null;}}}
if(!oContainer){oContainer=this.container;};return oContainer;};Zapatec.Grid.prototype.displayLoading=function(){this.removeUpdating();var oContainer=this.getBusyContainer();if(!oContainer){return;}
Zapatec.Transport.showBusy({busyContainer:oContainer});};Zapatec.Grid.prototype.removeLoading=function(){var oContainer=this.getBusyContainer();if(!oContainer){return;}
Zapatec.Transport.removeBusy({busyContainer:oContainer});};Zapatec.Grid.prototype.displayUpdating=function(){if(!this.config.dataOnDemand&&!this.config.totals.length){var iFields=this.fields.length;if(this.totalRecords()*iFields<2500){if(this.recordsDisplayed()*iFields<250){return;}
if(this.config.rowsPerPage>0){if(this.config.rowsPerPage*iFields<250){return;}}}}
this.removeLoading();var oContainer=this.getBusyContainer();if(!oContainer){return;}
Zapatec.Transport.showBusy({busyContainer:oContainer,busyImage:'zpupdating.gif'});};Zapatec.Grid.prototype.removeUpdating=function(){var oContainer=this.getBusyContainer();if(!oContainer){return;}
Zapatec.Transport.removeBusy({busyContainer:oContainer,busyImage:'zpupdating.gif'});};Zapatec.Grid.prototype.getFieldElement=function(oField){if(oField){return document.getElementById('zpGrid'+this.id+'Field'+oField.i);}};Zapatec.Grid.prototype.getRowElement=function(oRow){if(oRow){return document.getElementById('zpGrid'+this.id+'Row'+oRow.i);}};Zapatec.Grid.prototype.getRowElementByCell=function(oCell){if(oCell){return document.getElementById('zpGrid'+this.id+'Row'+oCell.r);}};Zapatec.Grid.prototype.getCellElement=function(oCell){if(oCell){return document.getElementById('zpGrid'+this.id+'Row'+oCell.r+'Cell'+oCell.i);}};Zapatec.Grid.prototype.visualizeCellReadOnly=function(oCell){var iGridId=this.id;var oRow=this.getRowByCell(oCell);var iRowId=this.getRowId(oRow);var iCellId=this.getCellId(oCell);var sRowId='zpGrid'+iGridId+'Row'+iRowId;var oTr=document.getElementById(sRowId);var sCellId=sRowId+'Cell'+iCellId;var oTd=document.getElementById(sCellId);if(oTr&&oTd){var aHtml=[];this.outputCellValue(aHtml,this.getFieldByCell(oCell),oCell);var sHtml=aHtml.join('');oTd.innerHTML=sHtml;var oTdHidden=document.getElementById(sCellId+'Hidden');if(oTdHidden){oTdHidden.innerHTML=sHtml;}
oTr.className=oTr.className.replace(/ zpGridRowEditable[^ ]*/g,'').replace(/ zpGridRowInvalid[^ ]*/g,'');var oTrFixed=document.getElementById(sRowId+'Fixed');if(oTrFixed){oTrFixed.className=oTrFixed.className.replace(/zpGridRowEditable[^ ]*/g,'').replace(/zpGridRowInvalid[^ ]*/g,'').split(/\s+/).join(' ');}
oTd.className=oTd.className.replace(/ zpGridCellEditable[^ ]*/g,'').replace(/ zpGridCellInvalid[^ ]*/g,'');if(!this.validateCell(oCell)){var aCl=[];aCl.push(' zpGridRowInvalid zpGridRowInvalid');aCl.push(iRowId);aCl.push(' zpGridRowInvalid');aCl.push(oTr.className.indexOf('zpGridRowOdd')>=0?'Odd':'Even');if(oTr.className.indexOf('zpGridRowLast')>=0){aCl.push(' zpGridRowInvalidLast');}
var sClass=aCl.join('');oTr.className+=sClass;if(oTrFixed){oTrFixed.className+=sClass;}
var aCl=[];aCl.push(' zpGridCellInvalid zpGridCellInvalid');aCl.push(iCellId);aCl.push(' zpGridCellInvalid');aCl.push(iCellId%2==1?'Odd':'Even');if(oTr.className.indexOf('zpGridCellLast')>=0){aCl.push(' zpGridCellInvalidLast');}
oTd.className+=aCl.join('');}}};Zapatec.Grid.listenDragStart=function(oArg){if(!oArg||typeof oArg.id!='string'){return;}
var aMatch=oArg.id.match(/^zpGrid(\d+)(Col|Span)(\d+)Resize$/);if(!(aMatch instanceof Array)||aMatch.length<4){return;}
var iGrid=aMatch[1];var iCol=aMatch[3];var oGrid=Zapatec.Widget.getWidgetById(iGrid);if(!(oGrid instanceof Zapatec.Grid)){return;}
var oEl=document.getElementById(oArg.id);if(!oEl){return;}
var oOffsetEl=Zapatec.Utils.getElementOffset(oEl);var oOffsetCont=Zapatec.Utils.getElementOffset(oGrid.headerContainer||oGrid.container);var oOffsetContScroll=Zapatec.Utils.getElementOffsetScrollable(oGrid.headerContainer||oGrid.container);var iPH=0;if(!oGrid.paginationContainers.length){var oPC=document.getElementById('zpGrid'+oGrid.id+'Pagination');if(oPC){iPH=oPC.offsetHeight;}}
if(oOffsetEl.top!=oOffsetCont.top){oEl.style.top=oOffsetCont.top-oOffsetEl.top+'px';}
var iH=oOffsetCont.height;if(!oGrid.headerContainer){iH-=iPH;}
oEl.style.height=iH+'px';oEl.className='zpGridColResize';oEl=document.getElementById('zpGrid'+iGrid+'DataColResize');if(oEl){oEl.style.left=oOffsetEl.left-oOffsetContScroll.left+'px';iH=oEl.parentNode.offsetHeight-iPH;oEl.style.height=iH+'px';oEl.style.display='';}
oEl=document.getElementById('zpGrid'+iGrid+'TotalsColResize');if(oEl){oEl.style.left=oOffsetEl.left-oOffsetContScroll.left+'px';oEl.style.height=oEl.parentNode.offsetHeight+'px';oEl.style.display='';}
oEl=document.getElementById('zpGrid'+iGrid+'Col'+iCol+'Title');if(!oEl){return;}
if(oEl.zpTimeout){clearTimeout(oEl.zpTimeout);oEl.zpTimeout=null;}
oEl=oEl.parentNode;if(!oEl){return;}
oEl=oEl.parentNode;if(!oEl){return;}
if(oEl.onclick){oEl.zpOnclick=oEl.onclick;oEl.onclick=null;}};Zapatec.Grid.listenDragMove=function(oArg){if(!oArg||typeof oArg.id!='string'){return;}
var aMatch=oArg.id.match(/^zpGrid(\d+)(Col|Span)(\d+)Resize$/);if(!(aMatch instanceof Array)||aMatch.length<4){return;}
var iGrid=aMatch[1];oEl=document.getElementById('zpGrid'+iGrid+'DataColResize');if(oEl){oEl.style.left=parseInt(oEl.style.left)+oArg.left-oArg.prevLeft+'px';}
oEl=document.getElementById('zpGrid'+iGrid+'TotalsColResize');if(oEl){oEl.style.left=parseInt(oEl.style.left)+oArg.left-oArg.prevLeft+'px';}};Zapatec.Grid.listenDragEnd=function(oArg){if(!oArg||typeof oArg.id!='string'){return;}
var aMatch=oArg.id.match(/^zpGrid(\d+)(Col|Span)(\d+)Resize$/);if(!(aMatch instanceof Array)||aMatch.length<4){return;}
var iGrid=aMatch[1];var iCol=aMatch[3];var oGrid=Zapatec.Widget.getWidgetById(iGrid);if(!oGrid){return;}
var oDiv=document.getElementById(oArg.id);if(!oDiv){return;}
oDiv.className='zpGridColResizeHidden';oDiv.style.top='';oDiv.style.height='';var oEl=document.getElementById('zpGrid'+iGrid+'DataColResize');if(oEl){oEl.style.display='none';oEl.style.left='';oEl.style.height='';}
oEl=document.getElementById('zpGrid'+iGrid+'TotalsColResize');if(oEl){oEl.style.display='none';oEl.style.left='';oEl.style.height='';}
oEl=document.getElementById('zpGrid'+iGrid+'Col'+iCol+'Title');if(!oEl){return;}
var iShift=oArg.left-oArg.startLeft;if(iShift){oDiv.style.left='';var iWidth=oEl.offsetWidth+iShift;if(iWidth<10){iWidth=10;}
oGrid.outSetColWidth(iCol,iWidth);var oDiv=document.getElementById('zpGrid'+iGrid+'DataContainer');if(oDiv){for(var iC=0;iC<oDiv.childNodes.length;iC++){var oC=oDiv.childNodes[iC];if(oC.tagName.toLowerCase()=='div'&&!oC.id){oC.innerHTML+='';}}}
oDiv=document.getElementById('zpGrid'+iGrid+'DataFixedLeft');if(oDiv){oDiv.innerHTML+='';}
oGrid.syncRowHeights();oGrid.fireEvent('gridResizedColumn',iCol,iShift);}
oEl.zpTimeout=setTimeout(function(){var oEl=document.getElementById('zpGrid'+iGrid+'Col'+iCol+'Title');if(!oEl){return;}
oEl.zpTimeout=null;oEl=oEl.parentNode;if(!oEl){return;}
oEl=oEl.parentNode;if(!oEl){return;}
if(oEl.zpOnclick){oEl.onclick=oEl.zpOnclick;oEl.zpOnclick=null;}},0);};Zapatec.EventDriven.addEventListener('dragStart',Zapatec.Grid.listenDragStart);Zapatec.EventDriven.addEventListener('dragMove',Zapatec.Grid.listenDragMove);Zapatec.EventDriven.addEventListener('dragEnd',Zapatec.Grid.listenDragEnd);Zapatec.Grid.aggregate=function(oArg){if(!oArg||typeof oArg.aggregate!='function'){return;}
var oGrid=oArg.grid;if(!(oGrid instanceof Zapatec.Grid)){return;}
var aFields=oGrid.getFields();var aColumns=[];oArg.totals=[];if(oArg.column instanceof Array){for(var iCol=0;iCol<oArg.column.length;iCol++){var iColId=oArg.column[iCol];if(typeof aFields[iColId]!='undefined'){aColumns.push(iColId);oArg.totals[iColId]=0;}}}else if(typeof aFields[oArg.column]!='undefined'){aColumns.push(oArg.column);oArg.totals[oArg.column]=0;}
if(!aColumns.length){return;}
oArg.column=aColumns;if((oArg.rows instanceof Array)&&oArg.rows.length){oArg.aggregate(oArg);}
var iLabelColumn;if(typeof oArg.labelColumn!='undefined'&&typeof aFields[oArg.labelColumn]!='undefined'){iLabelColumn=oArg.labelColumn;}
var sPrefix='';if(typeof iLabelColumn=='undefined'&&oArg.label){sPrefix=oArg.label+' ';}
oResult={rows:[{cells:[]}]};for(var iCol=0;iCol<aFields.length;iCol++){var val=oArg.totals[iCol];if(typeof val=='undefined'){val='';}else{val=sPrefix+val;}
oResult.rows[0].cells.push({v:val});}
if(typeof iLabelColumn!='undefined'&&oArg.label){oResult.rows[0].cells[iLabelColumn].v=oArg.label;}
return oResult;};Zapatec.Grid.sum=function(oArg){oArg.aggregate=Zapatec.Grid.aggregateSum;if(typeof oArg.label=='undefined'){oArg.label='Total:'}
return Zapatec.Grid.aggregate(oArg);};Zapatec.Grid.aggregateSum=function(oArg){var oGrid=oArg.grid;var aPrecisions=oArg.totals.slice();for(var iRow=0;iRow<oArg.rows.length;iRow++){var oRow=oArg.rows[iRow];for(var iCol=0;iCol<oArg.column.length;iCol++){var iColId=oArg.column[iCol];var val=oGrid.getCellValueOriginal(oGrid.getCellByRow(oRow,iColId));var iVal=val*1;if(!isNaN(iVal)){var iPrecision=Zapatec.Utils.getPrecision(val);if(iPrecision>aPrecisions[iColId]){aPrecisions[iColId]=iPrecision;}
oArg.totals[iColId]=Zapatec.Utils.setPrecision(oArg.totals[iColId]+
iVal,aPrecisions[iColId]);}}}
for(var iCol=0;iCol<oArg.column.length;iCol++){var iColId=oArg.column[iCol];oArg.totals[iColId]=Zapatec.Utils.setPrecisionString(oArg.totals[iColId],aPrecisions[iColId]);}};Zapatec.Grid.avg=function(oArg){oArg.aggregate=Zapatec.Grid.aggregateAvg;if(typeof oArg.label=='undefined'){oArg.label='AVG:'}
return Zapatec.Grid.aggregate(oArg);};Zapatec.Grid.aggregateAvg=function(oArg){Zapatec.Grid.aggregateSum(oArg);var iRows=oArg.rows.length;for(var iCol=0;iCol<oArg.column.length;iCol++){var iColId=oArg.column[iCol];var iPrecision=Zapatec.Utils.getPrecision(oArg.totals[iColId]);var fAvg=oArg.totals[iColId]/iRows;oArg.totals[iColId]=Zapatec.Utils.setPrecisionString(fAvg,iPrecision);}};Zapatec.Grid.min=function(oArg){oArg.aggregate=Zapatec.Grid.aggregateCompare;oArg.sign='<';if(typeof oArg.label=='undefined'){oArg.label='Min:'}
return Zapatec.Grid.aggregate(oArg);};Zapatec.Grid.max=function(oArg){oArg.aggregate=Zapatec.Grid.aggregateCompare;oArg.sign='>';if(typeof oArg.label=='undefined'){oArg.label='Max:'}
return Zapatec.Grid.aggregate(oArg);};Zapatec.Grid.aggregateCompare=function(oArg){var oGrid=oArg.grid;var oRow=oArg.rows[0];for(var iCol=0;iCol<oArg.column.length;iCol++){var iColId=oArg.column[iCol];oArg.totals[iColId]=oGrid.getCellByRow(oRow,iColId);}
for(var iRow=0;iRow<oArg.rows.length;iRow++){oRow=oArg.rows[iRow];for(var iCol=0;iCol<oArg.column.length;iCol++){var iColId=oArg.column[iCol];var oCell=oGrid.getCellByRow(oRow,iColId);if(eval('oGrid.getCellValueCompare(oCell)'+oArg.sign+'oGrid.getCellValueCompare(oArg.totals[iColId])')){oArg.totals[iColId]=oCell;}}}
for(var iCol=0;iCol<oArg.column.length;iCol++){var iColId=oArg.column[iCol];oArg.totals[iColId]=oGrid.getCellValue(oArg.totals[iColId]);}};Zapatec.Grid.count=function(oArg){oArg.aggregate=Zapatec.Grid.aggregateCount;if(typeof oArg.label=='undefined'){oArg.label='Count:'}
return Zapatec.Grid.aggregate(oArg);};Zapatec.Grid.aggregateCount=function(oArg){var oGrid=oArg.grid;var iRows=oArg.rows.length;for(var iCol=0;iCol<oArg.column.length;iCol++){oArg.totals[oArg.column[iCol]]=iRows;}};Zapatec.Grid.countDistinct=function(oArg){oArg.aggregate=Zapatec.Grid.aggregateCountDistinct;if(typeof oArg.label=='undefined'){oArg.label='Distinct:'}
return Zapatec.Grid.aggregate(oArg);};Zapatec.Grid.aggregateCountDistinct=function(oArg){var oGrid=oArg.grid;var aDistinct=[];for(var iCol=0;iCol<oArg.column.length;iCol++){aDistinct[iCol]={};}
for(var iRow=0;iRow<oArg.rows.length;iRow++){var oRow=oArg.rows[iRow];for(var iCol=0;iCol<oArg.column.length;iCol++){var iColId=oArg.column[iCol];var sVal=oGrid.getCellValueString(oGrid.getCellByRow(oRow,iColId));if(typeof aDistinct[iCol][sVal]=='undefined'){aDistinct[iCol][sVal]=true;oArg.totals[iColId]++;}}}};Zapatec.Grid.prototype.getTotals=function(){if(!(this.config.totals instanceof Array)){return;}
var aTotals=[];for(var iTotal=0;iTotal<this.config.totals.length;iTotal++){var oTotal=this.config.totals[iTotal];if(typeof oTotal.callback!='function'){oTotal.callback=Zapatec.Grid.sum;}
var oResult=oTotal.callback({grid:this,rows:this.getFilteredRows(),column:oTotal.column,label:oTotal.label,labelColumn:oTotal.labelColumn});if(oResult&&(oResult.rows instanceof Array)){for(var iRow=0;iRow<oResult.rows.length;iRow++){aTotals.push(oResult.rows[iRow]);}}}
return aTotals;};Zapatec.Grid.prototype.outputTotals=function(aHtml,aCols,aFixedCols,aRows,bFixed){if(typeof this.config.callbackTotalsDisplay=='function'||typeof this.config.callbackTotalDisplay=='function'||!aRows){return;}
if(this.config.horizontal){this.outputTotalsHoriz(aHtml,aCols,aFixedCols,aRows,bFixed);}else{this.outputTotalsVert(aHtml,aCols,aFixedCols,aRows,bFixed);}};Zapatec.Grid.prototype.outputTotalsHoriz=function(aHtml,aCols,aFixedCols,aRows,bFixed){if(bFixed){aCols=aFixedCols;}
for(var iCol=0;iCol<aCols.length;iCol++){var oField=aCols[iCol];var aCl=[];aCl.push('zpGridCol zpGridCol');aCl.push(iCol);if(iCol%2==1){aCl.push(' zpGridColOdd');}else{aCl.push(' zpGridColEven');}
if(iCol==aCols.length-1){aCl.push(' zpGridColLast');}
var sClass=aCl.join('');var aTr=[];aTr.push('<tr id="zpGrid');aTr.push(this.id);aTr.push('TotalCol');aTr.push(oField.i);if(bFixed){aTr.push('Fixed');}
aTr.push('" class="');aTr.push(sClass);aTr.push('">');if(bFixed){for(var iRow=0;iRow<aRows.length;iRow++){this.outputTotalsCell(aTr,oField,aRows[iRow],iCol,iRow,aRows.length);}}else{for(var iRow=0;iRow<aRows.length;iRow++){var bHidden=(iCol<aFixedCols.length);this.outputTotalsCell(aTr,oField,aRows[iRow],iCol,iRow,aRows.length,bHidden);}}
aTr.push('</tr>');aHtml.push(aTr.join(''));}};Zapatec.Grid.prototype.outputTotalsVert=function(aHtml,aCols,aFixedCols,aRows,bFixed){for(var iRow=0;iRow<aRows.length;iRow++){var oRow=aRows[iRow];if(!oRow){return;}
var aCl=[];aCl.push('zpGridRow zpGridRowTotals zpGridRowTotals');aCl.push(iRow);aCl.push(' zpGridRowTotals');aCl.push(iRow%2==1?'Odd':'Even');if(iRow==aRows.length-1){aCl.push(' zpGridRowLast zpGridRowTotalsLast');}
var sClass=aCl.join('');var aTr=[];aTr.push('<tr id="zpGrid');aTr.push(this.id);aTr.push('Total');aTr.push(iRow);if(bFixed){aTr.push('Fixed');}
aTr.push('" class="');aTr.push(sClass);aTr.push('" style="');if(oRow.style){aTr.push(oRow.style);}
aTr.push('">');if(bFixed){for(var iCol=0;iCol<aFixedCols.length;iCol++){this.outputTotalsCell(aTr,aFixedCols[iCol],oRow,iRow,iCol,aCols.length);}}else{for(var iCol=0;iCol<aCols.length;iCol++){var bHidden=(iCol<aFixedCols.length);this.outputTotalsCell(aTr,aCols[iCol],oRow,iRow,iCol,aCols.length,bHidden);}}
aTr.push('</tr>');aHtml.push(aTr.join(''));}};Zapatec.Grid.prototype.outputTotalsCell=function(aTr,oField,oRow,iRow,iCol,iCols,bHidden){var oCell=oRow.cells[oField.i];if(!oCell){return;}
var aCl=[];aCl.push('zpGridCell zpGridCell');aCl.push(iCol);aCl.push(' zpGridCellData zpGridCellData');aCl.push(iCol);aCl.push(' zpGridCellTotals zpGridCellTotals');aCl.push(iCol);if(iCol%2==1){aCl.push(' zpGridCellOdd zpGridCellDataOdd zpGridCellTotalsOdd');}else{aCl.push(' zpGridCellEven zpGridCellDataEven zpGridCellTotalsEven');}
if(iCol+1==iCols){aCl.push(' zpGridCellLast zpGridCellDataLast zpGridCellTotalsLast');}
var sClass=aCl.join('');var aTd=[];aTd.push('<td class="');aTd.push(sClass);aTd.push('" id="zpGrid');aTd.push(this.id);aTd.push('Total');if(this.config.horizontal){aTd.push(iCol);}else{aTd.push(iRow);}
aTd.push('Cell');aTd.push(oField.i);if(bHidden){aTd.push('Hidden');}
var sStyle=this.getCellStyle(oCell,iRow);if(sStyle){aTd.push('" style="');aTd.push(sStyle);}
aTd.push('">');this.outputTotalsCellValue(aTd,oField,oRow,oCell);aTd.push('</td>');aTr.push(aTd.join(''));};Zapatec.Grid.prototype.outputTotalsCellValue=function(aTd,oField,oRow,oCell){aTd.push('<div class="zpGridDiv');if(this.getClassByType){aTd.push(' ');aTd.push(this.getClassByType(this.getFieldType(oField)));}
aTd.push('">');var sData=oCell.v+'';if(!sData||!sData.length){sData='&nbsp;';}
aTd.push(sData);aTd.push('</div>');};Zapatec.Grid.prototype.redrawTotals=function(oArg){if(this.visualize){var aTotals=this.getTotals();if(!aTotals){return;}
var aFields=this.getFields();var iGridId=this.id;for(var iTotal=0;iTotal<aTotals.length;iTotal++){var oRow=aTotals[iTotal];for(var iField=0;iField<aFields.length;iField++){var sCellId='zpGrid'+iGridId+'Total'+iTotal+'Cell'+iField;var oTd=document.getElementById(sCellId);if(oTd){var aTd=[];this.outputCellValue(aTd,aFields[iField],oRow.cells[iField]);var sTd=aTd.join('');oTd.innerHTML=sTd;var oTdHidden=document.getElementById(sCellId+'Hidden');if(oTdHidden){oTdHidden.innerHTML=sTd;}}}}}};