﻿var DEFAULT_SELECT_WIDTH = 162;
var objWord;
var fso;
var strForm, sPrefab, strFile, lDoc, lDocType;


function IsBrowserSupported() {
    var i = String(navigator.appVersion).indexOf("MSIE");
    if (i == -1)
        return false;
    if (5 > parseInt(String(navigator.appVersion).substr(i), 10))
        return false;
    return true;
}
function pageLoaded() {
    //Set focus to first field
    var i;
    for (i = 0; i < document.forms[0].length; i++) {
        if ((document.forms[0].item(i).type == "text") || (document.forms[0].item(i).type == "select-one") || (document.forms[0].item(i).type == "textarea")) {
            document.forms[0].item(i).focus();
            break;
        }
    }
}
function ValidateBrowser() {
    if (!IsBrowserSupported())
        ReportClientErr("1", "insufficient browser version", "", "client", "ConfRec");
    return true;
}


function AddSingle(objOptElt) {

    if (!IsSelected(objOptElt)) //not already transferred?
    {

        if (objOptElt.value != "") {
            document.getElementById('selectedfields').options[document.getElementById('selectedfields').options.length] = new Option(objOptElt.text, objOptElt.value, false, false);
        }
        else {
            alert("This is not a valid field.");
        }
    }
    return true;
}

function Cancel() {
    window.close();
    return false;
}

function IsSelected(Elt) {
    var i;
    for (i = 0; i < document.getElementById('selectedfields').options.length; i++) {
        if (String(Elt.value).indexOf(document.getElementById('selectedfields').options[i].value) != -1) {
            return true;
        }
    }
    return false;
}


function FindVisibleListBox() {
    var i;
    var str = new String();
    var ListBox;
    //Pick the Visible Field Listbox

    for (i = 0; i < document.forms[0].length; i++) {
        str = document.forms[0].elements[i].id
        if (str.substr(0, 7) == "fields_")
            if (document.forms[0].elements[i].style.display == "inline")
            ListBox = document.forms[0].elements[i];
    }

    return ListBox;

}



function FixInputString(str) {


    var s;
    s = String(str).replace("'", " ");
    s = String(s).replace('"', " ");
    return s;
}




function AdjustSelectedBoxWidth() {
    var CurWidth;
    CurWidth = FindVisibleListBox().offsetWidth;
    if (CurWidth > (Number(String(document.forms[0].selectedfields.style.width).replace(/[^0-9]/g, ""))))
        document.forms[0].selectedfields.style.width = CurWidth;
    return true;
}





function Validate_ConfRec() {
    if (document.getElementById('selectedperms').options.length == 0) {
        alert("Please assign at least one user.");
        return false;
    }
    var i;
    var ListBox;
    ListBox = document.getElementById('selectedperms');
    window.opener.document.getElementById('hdnSelectedpermsusers').value = "";

    for (i = 0; i < document.getElementById('selectedperms').options.length; i++) {
        window.opener.document.getElementById('hdnselectedpermsusers').value = window.opener.document.getElementById('hdnselectedpermsusers').value + ',' + document.getElementById('Selectedperms').options[i].value.substring(5, document.getElementById('selectedperms').options[i].value.length);
    }
    return true;
}

function AddSelected_ConfRec() {
    var i;
    var str = new String();
    var ListBox;
    var Opt;

    ListBox = document.getElementById('availperms');
    if (ListBox == null) return false;

    //Run through the selected elts.
    for (i = 0; i < ListBox.length; i++) {
        if (ListBox.options[i].selected == true) //selected for transfer?
            if (!IsSelected_ConfClaim(ListBox.options[i])) //not already transferred?
        {
            Opt = ListBox.options[i];
            document.getElementById('selectedperms').options[document.getElementById('selectedperms').options.length] = new Option(Opt.text, Opt.value, false, false);
        }
    }
    return true;
}



function RemoveSelected_ConfRec() {
    var i;
    var ListBox;
    var sCurrentUserID = "user_" + document.getElementById('CurrentUserID').value;

    ListBox = document.getElementById('selectedperms');
    for (i = ListBox.options.length - 1; i >= 0; i--)
        if (ListBox.options[i].selected == true) {
            if (ListBox.options[i].value == sCurrentUserID)
                alert("RMX Current logged user can not be removed");
            else 
                ListBox.options[i] = null;
    }
    // Always return values from function req. for Netscape compatiblity
    return true;
}

function AddSingle_ConfRec(objOptElt) {
    if (!IsSelected_ConfClaim(objOptElt)) //not already transferred?
    {
        if (objOptElt.value != "") {

            document.getElementById('selectedperms').options[document.getElementById('selectedperms').options.length] = new Option(objOptElt.text, objOptElt.value, false, false);
        }
        else {
            alert("This is not a valid user.");
        }
    }
    return false;
}

function RemoveSingle_ConfRec(objOptElt) {
    var sCurrentUserID = "user_" + document.getElementById('CurrentUserID').value;

    if (sCurrentUserID == document.getElementById('selectedperms').options[objOptElt.index].value) {
        alert("RMX Current loggin user can not be removed");
    }
    else {
        document.getElementById('selectedperms').options[objOptElt.index] = null;
    }
    return true;
}


function IsSelected_ConfClaim(Elt) {
    var i;
    for (i = 0; i < document.getElementById('selectedperms').options.length; i++) {
        if (Elt.value == document.getElementById('selectedperms').options[i].value) {
            return true;
        }
    }
    return false;
}

/////////////////////////////////////////////////





function VerifyCatChange() {
    var lCatIdx = 0;

    //if((lCatIdx == 0) || (document.getElementById('selectedfields').length == 0))
    if (document.getElementById('selectedfields').length == 0) {
        lCatIdx = document.getElementById('fieldcategory').selectedIndex;
        return true;
    }
    else {
        document.getElementById('fieldcategory').selectedIndex = lCatIdx;
        alert("Category cannot be changed while fields are selected.(All Admin Tracking fields must come from a single category)\n\nPlease remove the selected fields if you wish to use a different category.");
    }
    return false;
}

function AddSingle_ConfRec(objOptElt) {

    if (!IsSelected_ConfClaim(objOptElt)) //not already transferred?
    {
        if (objOptElt.value != "") {

            document.getElementById('selectedperms').options[document.getElementById('selectedperms').options.length] = new Option(objOptElt.text, objOptElt.value, false, false);
        }
        else {
            alert("This is not a valid user.");
        }
    }
    return false;
}


function onCodeClose() {
    m_codeWindow = null;
    return true;
}
function InitPageSettings_ConfClaim() {
    document.getElementById('selectedperms').style.width = document.getElementById('availperms').offsetWidth;

    var arrSelectedFields = new Array();
    var arrSelectedFieldIds = new Array();
    arrSelectedFields = document.getElementById('hdnselectedperms').value.split(';');
    arrSelectedFieldIds = document.getElementById('hdnselectedpermsids').value.split(' ');
    try {
        for (i = 1; i < arrSelectedFields.length; i++) {
            var sFieldSel = new String(arrSelectedFields[i].valueOf());
            var sFieldSelId = new String(arrSelectedFieldIds[i - 1].valueOf());
            document.getElementById('selectedperms').options[document.getElementById('selectedperms').options.length] = new Option(sFieldSel, sFieldSelId, false, false);
        }
    } catch (e) { ; }
}
