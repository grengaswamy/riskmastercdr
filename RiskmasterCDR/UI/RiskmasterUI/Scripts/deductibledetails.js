﻿function AddDecimals()
{
    var dimPercent = document.getElementById('diminishingpercent');
    if (dimPercent != null && dimPercent.value.indexOf('.') == -1) {
        dimPercent.value = dimPercent.value + ".00";
    }
}


function UpdateDiminishingFields() {
    var dimType = document.getElementById('diminishingtype');
    var dimPercent = document.getElementById('diminishingpercent');
    var dedAmount = document.getElementById('dedsiramount');
    var reducedAmount = document.getElementById('dimreducedamount');
    var remainingAmount = document.getElementById('remainingamount');
    var hdnDimCode = document.getElementById('prevdiminishingtypecodedesc');
    debugger;
    if (dimType.value == "") {
        if (dimPercent != null) {
            dimPercent.value = 0.00;
            dimPercent.readonly = 'true';
            dimPercent.style.backgroundColor = "#F2F2F2"
            
            hdnDimCode.value = 0;
        }
        if (dedAmount != null && reducedAmount != null && remainingAmount != null) {
            reducedAmount.value = dedAmount.value;
            remainingAmount.value = dedAmount.value;
        }
    }
    else {
        if (dimPercent != null) {
            dimPercent.readonly = 'false';
            dimPercent.style.backgroundColor = "#FFFFFF"
        }
    }
}