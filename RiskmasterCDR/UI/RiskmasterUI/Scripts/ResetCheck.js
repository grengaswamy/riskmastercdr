var ie,ns;
var browserName = navigator.appName;                   // detect browser
var browserVersion = navigator.appVersion;

if (browserName == "Netscape")
{
	ie=0;
	ns=1;
}
else		//Assume IE
{
	ieversion=browserVersion.substr(browserVersion.search("MSIE ")+5,1);
	ie=1;
	ns=0;
}



function selectAll()
{
	var sName, sType;
   
	for(var i = 0; i<document.forms[0].elements.length; i++)
	{
		sName = document.forms[0].elements[i].name;
		sType = document.forms[0].elements[i].type;
		if( (sType == 'checkbox') && (sName.substring(0,4)=="chk_") )
		{
			document.forms[0].elements[i].checked = true;
		}
	}
	if (document.getElementById("chkTransactions") != null) {
		document.getElementById("chkTransactions").checked = true;
	}
}

function deselectAll()
{
	var sName, sType;
	for(var i = 0; i<document.forms[0].elements.length; i++)
	{
		sName = document.forms[0].elements[i].name;
		sType = document.forms[0].elements[i].type;
		if( (sType == 'checkbox') && (sName.substring(0,4)=="chk_") )
		{
			document.forms[0].elements[i].checked = false;
		}
	}
	if (document.getElementById("chkTransactions") != null) {
		document.getElementById("chkTransactions").checked = false;
	}
}

function TransClick()
{
	if( document.getElementById("chkTransactions").checked )
		selectAll();
	else
		deselectAll();
}
//To check whether the particluar chek belongs to any RollUp or not
function CheckForRolUpId() {
   
    for (var i = 0; i < document.forms[0].elements.length; i++) {
        sName = document.forms[0].elements[i].name;

        sType = document.forms[0].elements[i].type;
        sId = document.forms[0].elements[i].id;

        if ((sType == 'checkbox') && (sName.substring(0, 4) == "chk_")) {

            if (document.forms[0].elements[i].checked == true) {
                
                var cntrl="";
                cntrl="RollUp_" + sId.substring(4, sId.length);
             
                if (window.document.getElementById(cntrl).value != '0') 
                {
                 
                    var answer = confirm("One of the selected checks is a part of a roll up, All checks in the roll up will be reset to released status.")
                    if (answer) 
                    {
                        return true;
                    }
                    else 
                    {
                        document.forms[0].elements[i].checked = false;
                        return false;
                    }

                }

            }
        }

    }
}


function RefreshChecks()
{
	document.getElementById("txtAction").value = "1";
	return true;
}

function processChecks() {
  	var sName, sType, sId;
	var SelectedChecks = 0;
	var chkid="";
	//start rsushilaggar MITS 19970 05/21/2010
	var sVoidReason;

	CheckForRolUpId(); 
	
	for(var i = 0; i<document.forms[0].elements.length; i++)
	{
		sName = document.forms[0].elements[i].name;
		
		sType = document.forms[0].elements[i].type;
		sId = document.forms[0].elements[i].id;
		
		if( (sType == 'checkbox') && (sName.substring(0,4)=="chk_") )
		{
		   
				if(document.forms[0].elements[i].checked == true)
				{
					if(chkid=="") chkid = chkid + sId.substring(4 , sId.length);
					else chkid = chkid + ',' + sId.substring(4, sId.length);
					//rsushilaggar MITS 21715 11-Aug-2010
					SelectedChecks = SelectedChecks + 1;
				}
		}
		
	}
	
	if(chkid=="")
	{
	    //alert("No check selected"); //MITS 31027 - Rakhel ML changes
	    alert(NoChksValidations.NoCheckSelected);
		return false;
    }
    //MITS 22053: Yatharth

if (SelectedChecks > 1)
             {
                //rsolanki2 : mits 22693
                var txtvoidcheckreason = document.getElementById("voidreason");
                if (txtvoidcheckreason != null) {
                    //rsolanki2 : mits 70580
                    // var bResult = confirm("You are about to apply the same void reason to multiple checks.\nPress Ok to continue...");  
                    //ijha :mits 24252 08/03/2011
                    //changed by Amitosh for Mits 24468 (03/25/2011)
                    if (document.getElementById("btnProcess").value == "Void Checks") {
                        var bResult = confirm("You are about to apply the same void reason to multiple checks");
                    }
                    else {
                        var bResult = confirm("The chosen checks have been successfully marked as cleared");
                    }
                    //end Amitosh
                    if (!bResult) { return false; } 
                }
	}
	//start rsushilaggar MITS 19970 05/21/2010
	document.getElementById("selectedtransids").value = chkid;
	return true;
}
