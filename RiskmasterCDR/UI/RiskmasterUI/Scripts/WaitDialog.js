// Arguments:
//   id - id of the IFrame tag to use.
//   varName - name of the variable this instance of the Please Wait is assigned to.
//   imageCount - number of image in the animation sequence.
//   imageNamePrefix - name prefix for each image.
//   imageNameSuffix - name suffix for each image.
//   imageDelay - number of milliseconds to display each image.
//   width - defines the width of the Please Wait (required for Netscape and Firefox)
//   height - defines the height of the Please Wait (required for Netscape and Firefox)
//   url - (optional) url to the page containing the custom Please Wait layout.
//
//   This is how to instantiate PleaseWaitDialog
//   var pleaseWait = new PleaseWaitDialog("PleaseWaitIFrame", "pleaseWait", 4, "pleasewait_", ".gif", 125, 308, 172)
//
//   This example uses a custom Please Wait layout defined in the pleaseWait.htm file.
//   var pleaseWait = new PleaseWaitDialog("PleaseWaitIFrame", "pleaseWait", 4, "pleasewait_", ".gif", 125, 308, 172, "../Views/Shared/PleaseWaitBox.htm");
//
function PleaseWaitDialog(id, varName, imageCount, imageNamePrefix, imageNameSuffix, imageDelay, width, height, message, url, name) {
    if (message == "" || message == null || message == 'undefined') {
        this.Message = "";
    }
    else {
        this.Message = message;
    }
    if (name=="" || name==null || name=='undefined')
    {
        name='PleaseWaitDialog1_pleaseWaitFrame';
    }
    this.FrameName = name;    
}
//Ashish Ahuja : Mits 30873 Start:
var sessionTimeout = '';
sessionTimeout = getCookie("sessionTime");
SetCookie("cookieWarningMessage", sessionTimeout, 1);

function SetCookie(cookieName, cookieValue, nDays) {
    var today = new Date();
    var expire = new Date();
    if (nDays == null || nDays == 0) nDays = 1;
    expire.setTime(today.getTime() + 3600000 * 24 * nDays);
    document.cookie = cookieName + "=" + escape(cookieValue)
                 + ";path=/;expires=" + expire.toGMTString();
}
function getCookie(c_name) {
    var i, x, y, ARRcookies = document.cookie.split(";");
    for (i = 0; i < ARRcookies.length; i++) {
        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
        x = x.replace(/^\s+|\s+$/g, "");
        if (x == c_name) {
            return unescape(y);
        }
    }

}
//Ashish Ahuja : Mits 30873 End

// Show:
// This function displays the Please Wait to the user.  This function centers the 
// Please Wait dialog box, makes it visible, and starts the animation.  This function 
// will typically be called by the body event.
//
// Example:
//		<body onbeforeunload="pleaseWait.Show();" >
PleaseWaitDialog.prototype.Show = function() {

    if (this.Message != "") {
         this.pleaseWait("start");
      }
    else {
        parent.pleaseWait('start');
    }
}


PleaseWaitDialog.prototype.pleaseWait = function(state) {

    var objPW = document.getElementById(this.FrameName);
    
    if (state == "start") {
        objPW.style.display = "block";
    }
    else { //stop
        objPW.style.display = "none";
    }
}

//MITS 27964 hlv 4/10/2012 begin
//alert(window.parent.name);

//if (window.opener != null &&
//    window.opener.window.parent != null &&
//    window.opener.window.parent.parent != null &&
//    window.opener.window.parent.parent.iwintype != null) {

//    window.opener.window.parent.parent.iwintype = document.title;

//    window.onunload = function () {
//        if (window.opener != null) {
//            window.opener.window.parent.parent.iwintype = "";
//        }
//    }
//}
//MITS 27964 hlv 4/10/2012 end

