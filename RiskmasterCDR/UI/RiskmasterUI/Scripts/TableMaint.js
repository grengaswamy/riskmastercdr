var m_ctname;
var m_typecode="";
var table_window;
var m_datachanged='false';

var ns, ie, ieversion;
var browserName = navigator.appName;                   // detect browser
var browserVersion = navigator.appVersion;

if (browserName == "Netscape")
{
	ie=0;
	ns=1;
}
else		//Assume IE
{
	ieversion=browserVersion.substr(browserVersion.search("MSIE ")+5,1);
	ie=1;
	ns=0;
}

function formHandler1(sLinkTo, sParams, sEnableForNew, sType, sTypeCode) {
    var sLinkParams = new String();
	var sLinkTorg = sLinkTo;

	if (sParams != "") {
	    var sp = new String(sParams);
		var arrParams = sp.split("&");

		for (var i = 0; i < arrParams.length; i++) {
		    var arrNameVal = arrParams[i].split("=");

		    if (arrNameVal.length == 2) {
			    if (arrNameVal[1].charAt(0) == "%" && arrNameVal[1].charAt(arrNameVal[1].length - 1) == "%") {
					// Try to replace it with real value
			        var sName = replace(arrNameVal[1], "%", "");
			        var objFormField = null;
			        objFormField = eval("document.forms[0]." + sName);

			        if (objFormField != null)
					    arrNameVal[1] = objFormField.value;
					else
					    arrNameVal[1] = "";
	            }

	            if (sLinkParams != "")
				    sLinkParams = sLinkParams + "&";

				sLinkParams = sLinkParams + arrNameVal[0] + "=" + escape(arrNameVal[1]);
			} else {
			    if (sLinkParams != "")
				    sLinkParams = sLinkParams + "&";

				sLinkParams = sLinkParams + arrParams[i];
			}
		}
    }

    if (sLinkParams != "")
        sLinkTo = replace(sLinkTo, "&amp;", "&");

    if (sType == "new") {
        window.open(sLinkTo, '', 'resizable=yes,scrollbars=yes');
    }

	if (sType=="swindow") {
	    if (sTypeCode != null && sTypeCode != "")
			m_typecode = sTypeCode;
		else {
		    var objField = null;
		    objField = eval(document.forms[0].TypeCode);
		    
		    if (objField != null)
		        m_typecode = document.forms[0].TypeCode.value;
			else
				m_typecode = "";
				//m_typecode = 3;
		}

		if (m_typecode != "") {
		    sLinkTo = sLinkTo + "&TypeCode=" + m_typecode;

		    if (sEnableForNew == "new")
			    table_window = window.open(sLinkTo, 'Table', 'width=500,height=350' + ',top=' +
				               (screen.availHeight - 290) / 2 + ',left=' +
				               (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
            else if (sEnableForNew == "edit")
                table_window = window.open(sLinkTo, 'Table', 'width=500,height=350' + ',top=' +
                               (screen.availHeight - 290) / 2 + ',left=' + 
                               (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
            else { 
                sLinkTo = sLinkTo + "&TableId=" + document.forms[0].hdnTableId.value;
                table_window = window.open(sLinkTo, 'Upload', 'width=500,height=350' + ',top=' +
                               (screen.availHeight - 290) / 2 + ',left=' + 
                               (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
            }
		} else {
		    if (table_window != null)
		        table_window.close();

		    sLinkTo = sLinkTo + "&TableId=" + document.forms[0].hdnTableId.value;
			table_window = window.open(sLinkTo, 'Upload', 'width=500,height=350' + ',top=' +
			               (screen.availHeight - 290) / 2 + ',left=' +
			               (screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
		}
    }

    if (sType == "close") {
		window.close();
	}

    /*if (sType=="save")
	{
		if(checkfields('req_fields','All _ fileds are required to fill','nullcheck')==true)
		{
			//document.forms[0].setAttribute("action", sLinkTorg+sParams);

			document.forms[0].submit();
		}
	}*/

	if (sType == "normal")
	    self.document.location.href = sLinkTorg + sParams;

	if (sType == "parentTable") {
	    //start - MITS 15504 - mpalinski
	    var objPath = window.opener.parent.frames['frm_Tables'].location.pathname + sParams;// + "?TypeCode=3";
	    window.opener.parent.frames['frm_Tables'].location.href = objPath;
	    //end - MITS 15504 - mpalinski
	    //parent.window.opener.document.location.href = sLinkTorg + sParams;
	    window.close();
	}
	
	return false;
}

function changeone(id2)
{
	if(id2.className=="visi")
		id2.className="mover";
	else
		id2.className="visi";
}

function changetext(id1)
{
	if(id1.src.indexOf("plus.bmp")>0)
		id1.src=mypic1[1];
	else
		if(id1.src.indexOf("minus.bmp")>0)
			id1.src=mypic1[0];
}

function display(id2)
{
	var d;
	if (ie)
	{
	    d = 'document.getElementById[' + id2 + ']';
		d=eval(id2);
		d.style.display="block";
	}
	else
	{
		d='document.layers['+id2+']';
		d=eval(d);
		d.visibility="show";
	}
}

function hide()
{
	if (ie)
	    document.getElementById['layer1'].style.visibility = "hidden";
	else if(ns)
		document.layers['layer1'].visibility = "hidden";
}

function display_Codedetail(id1,pagename)
{
	parent.frames['frm_Code_Detail'].location.href=pagename + "&amp;TableId="+id1+"&amp;SortOrder=ascending";
	return false;
}

function refresh_Codedetail(pagename,orderby)
{  
	window.location.href=pagename + "&amp;SortOrder="+orderby;
	return false;
}

function refresh_tables(pagename,alpha)
{
	var xtypecode='3';
	var x = 'document.forms[0].DropDown_Table_Types';
	x=eval(x);
	
	if((x.value!="") && isNaN(x.value)!=true)
		xtypecode=x.value;

	if(xtypecode=="3" || xtypecode=="10")
	{
	    /* actual pages paerhaps?
	    parent.frames['frm_Tables'].location.href='home?pg=riskmaster/TableMaintenance/' + pagename + '&Alpha='+alpha+'&TypeCode='+xtypecode;
	    parent.frames['frm_Code_Detail'].location.href='home?pg=riskmaster/TableMaintenance/CodeList&Alpha='+alpha+'&TypeCode='+xtypecode;
	    parent.frames['frm_Tables'].frameElement.width = "40%";
	    parent.frames['frm_Code_Detail'].frameElement.width = "60%";
	    }
	    else
	    {
	    parent.frames['frm_Tables'].location.href='home?pg=riskmaster/TableMaintenance/' + pagename + '&Alpha='+alpha+'&TypeCode='+xtypecode;	
	    parent.frames['frm_Tables'].frameElement.width = "100%";
	    parent.frames['frm_Code_Detail'].frameElement.width = "0%";*/
	    //hardcoded for testing
	    parent.frames['frm_Tables'].location.href = parent.frames['frm_Tables'].location.pathname + '?Alpha=' + alpha + '&TypeCode=' + xtypecode;
	    parent.frames['frm_Code_Detail'].location.href="CodeList.aspx" + '?Alpha='+alpha+'&TypeCode='+xtypecode;

        //caggarwal4 RMA-9347 starts 
	    //parent.frames['bottomFS'].cols = "30%,*"
        //caggarwal4 RMA-9347 ends
	    //parent.document.getElementById('main').style.cols = "40%,*";
	    
	    //parent.document.getElementById('frm_Tables').style.width = "40%";
	    //parent.document.getElementById('frm_Code_Detail').style.width = "60%";
	    

	    //parent.frames['frm_Tables'].frameElement.width = "40%";
	    //parent.frames['frm_Code_Detail'].frameElement.width = "60%";
	    }
    else
	{
	    parent.frames['frm_Tables'].location.href = parent.frames['frm_Tables'].location.pathname + '?Alpha=' + alpha + '&TypeCode=' + xtypecode;

        //caggarwal4 RMA-9347 starts
	    //parent.frames['bottomFS'].cols = "*,0"
	    //caggarwal4 RMA-9347 ends

	    //parent.document.getElementById('frm_Tables').style.width = "100%";
	    //parent.document.getElementById('frm_Code_Detail').style.width = "0%";
	    
	    //parent.frames['frm_Tables'].location.href='home?pg=riskmaster/TableMaintenance/' + pagename + '&Alpha='+alpha+'&TypeCode='+xtypecode;	
	    //parent.frames['frm_Tables'].frameElement.width = "100%";
	    //parent.frames['frm_Code_Detail'].frameElement.width = "0%";
	}

	return false;
}

function refreshme()
{
	window.close();
}

function select_option(pagename1,id1,ctname)
{
	m_ctname=ctname;
	if(table_window!=null)
		table_window.close();
	table_window=window.open(pagename1+id1,'Codes','width=500,height=350'+',top='+(screen.availHeight-290)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
	
	return false;
}

function pageloaded()
{
	//Code commented by nitesh 03jan2006 Starts:- for enabling import button
	//if(document.forms[0].btnImport!=null)
	//{
	//	document.forms[0].btnImport.disabled=true;
	//}
	//Code commented by nitesh 03jan2006 Ends	
 	document.dateSelected=dateSelected;
	document.onCodeClose=onCodeClose;
	document.selvalue=selvalue;
	document.codeSelected=codeSelected;
}

function selvalue(short_code,desc,id1,ctname)
{
	var ct_name;
	var ct_name1;
	ct_name = 'window.opener.document.forms[0].';
	//ct_name1 = ct_name + ctname + '_tableid';
	ct_name1 = ct_name + ctname + '_cid'; // Mihika 24-Feb-2006 Changed as the values were coming incorrectly.

	if (ctname == "Code") {
	    ct_name = eval(ct_name + ctname);
	    ct_name.value = short_code;
	}
	else {
	    ct_name = eval(ct_name + ctname);
        ct_name.value = short_code + ' ' + desc;
	}


	if (ct_name1 == "window.opener.document.forms[0].Code_cid") {
	    ct_name1 = eval(ct_name1);
	    ct_name1.value = desc;
	}
	else {
	    ct_name1 = eval(ct_name1);
	    ct_name1.value = id1;
	}
	
	window.close();

	return false;
}

function checkfields(fName, fmessage, valType, bwindowClose) {

	var str='document.forms[0].';
	if (valType=='nullcheck')
	{
		str=eval(str+fName);
		if (str.value!="")
		{
			var s=new String(str.value);
			var arr=s.split("|");
			var obj=null;
			var i;
			var c=arr.length;
			for(i=0;i<=c-1;i++)
			{
				obj=eval('document.forms[0].'+arr[i]);
                //ijha MITS 24382
				if (arr[i].indexOf("_cid") > 0) {
				    if (obj.value == 0) {
				        if (obj.disabled == false) {
				            alert(" Please enter some value in all the required fields.");
				          //  eval('document.forms[0].'+arr[i].substring(5, 0)).focus();
				            return false;
				        }
				    }
				}
				else 
                {
                    if (replace(obj.value, " ", "") == "") 
                    {
				        //Changed by Gagan for mits 20198 
                        if (obj.disabled == false) 
                        {
				            alert(" Please enter some value in all the required fields.");
				            obj.focus();
				            return false;
				        }
				    }
				}
			}
		}
		// Charanpreet for MITS 12391 : Start
          // Deb MITS 27000: Increased the size to 100
		if (document.forms[0].Code_Desc != null) {
		    if (document.forms[0].Code_Desc.value.length > 100) {
		        alert(" Code Description cannot exceed 100 characters.");
		        document.forms[0].Code_Desc.focus();
		        return false;
		    }
		}
		// Charanpreet for MITS 12391 : End		
		
		// MITS 15644 MAC - return false with alert message if table name contains a '
		// -- START --
		//Added Null Check for MITS:16033 as these fields don't exist on codelist.aspx
		if(document.forms[0].stable !=null)
		    var SysTable = document.forms[0].stable.value;
		if(document.forms[0].utable !=null)
		    var UserTable = document.forms[0].utable.value;
		//Added Null Check for MITS:16033 as these fields don't exist on codelist.aspx
		if(String(SysTable).indexOf("'") != "-1" || String(UserTable).indexOf("'") != "-1")
		{
			alert (" Please do not use single quotes in either table name field.");
			return false;
		}
	    // -- END --
	    //bpaskova JIRA 3869 start
		var x = document.forms[0].pTable;
		if (x != null && x.options[x.options.selectedIndex].text == SysTable) { //added null check for MITS 37576:aaggarwal29
		    alert('Parent - child relationship on same table is not allowed.');
		    return false;
		}
	    //bpaskova JIRA 3869 end
		if(bwindowClose == "true")
		{ 
			var objFlag = null;
			objFlag=eval('document.forms[0].FlagForRefresh');
			if(objFlag != null)
				objFlag.value = "1";
		}
		
		return true;
	}
}

//changed by Saurabh for MITS 12390 : Start
function compare_Date() {

    
    var objStartDate = document.getElementById("e_StartDate");
    var objEndDate = document.getElementById("e_EndDate");
    // start: mits 26514 :atavaragiri  cast to a Date object so proper format of date is passed to the variables. 
    var dStartDate = new Date(objStartDate.value);
    var dEndDate = new Date(objEndDate.value);

    //commented the below line :mits 26514

    //if (objStartDate.value > objEndDate.value) 
      if(dStartDate>dEndDate)   //mits 26514 
      {
    alert("Please enter End date greater than or equal to Start date!");
      return false;
   }
   return true;
}
//changed by Saurabh for MITS 12390 : End

function backpage()
{
	window.history.back();
}

function setchanges(comboname)
{
	var x='document.forms[0].'+comboname;
	x = eval(x);

	if (x.id == 'pTable') 
	{//Parijat: 20198
	    var obj = document.getElementById('pTable_Required');
	    if (x.value != '') {

	        obj.disabled = false;
	    }
	    else {
	        obj.disabled = true;
	    }
	    //Parijat: 20198
	    if(x.options[x.options.selectedIndex].value!=0)
	    {
	        document.forms[0].tree_disp.parentElement.removeAttribute('disabled');
	        document.forms[0].tree_disp.removeAttribute('disabled');
	        document.forms[0].tree_disp.Enabled = true;
	    }
	    else
	    {
	        alert('can not select null');
	        x.options[x.options.selectedIndex+1].selected=true;
	    }
	}
	if (x.id == 'DropDown_Table_Types')
	{
		var xtypecode=x.value;
		if(isNaN(xtypecode) || (xtypecode==""))
			xtypecode="3";
		
		if(xtypecode=="3" || xtypecode=="10")
		{
			//parent.frames['frm_Tables'].location.href='home?pg=riskmaster/TableMaintenance/TableList&TypeCode='+xtypecode;
			//parent.frames['frm_Code_Detail'].location.href='home?pg=riskmaster/TableMaintenance/CodeList&TypeCode='+xtypecode;
			//parent.frames['frm_Tables'].frameElement.width = "40%";
			//parent.frames['frm_Code_Detail'].frameElement.width = "60%";

			parent.frames['frm_Tables'].location.href = parent.frames['frm_Tables'].location.pathname + '?TypeCode=' + xtypecode;
			parent.frames['frm_Code_Detail'].location.href = parent.frames['frm_Code_Detail'].location.pathname + '?TypeCode=' + xtypecode;

            //caggarwal4 RMA-9347 starts
			//parent.frames['bottomFS'].cols = "30%,*"
            //caggarwal4 ends
		}
		else
		{
			//parent.frames['frm_Tables'].location.href='home?pg=riskmaster/TableMaintenance/TableList&TypeCode='+xtypecode;
			//parent.frames['frm_Tables'].frameElement.width = "100%";
			//parent.frames['frm_Code_Detail'].frameElement.width = "0%";

			parent.frames['frm_Tables'].location.href = parent.frames['frm_Tables'].location.pathname + '?TypeCode=' + xtypecode;
			parent.frames['frm_Code_Detail'].location.href = parent.frames['frm_Code_Detail'].location.pathname + '?TypeCode=' + xtypecode;

            //caggarwal4 RMA-9347 starts
			//parent.frames['bottomFS'].cols = "*,0"
		    //caggarwal4 RMA-9347 ends
		}
	}
	return false;
}



function onLostFocus(tid,xname)
{
    var obj;

   
	if(m_datachanged=='true')
	{
		obj='document.forms[0].'+xname;
		obj = eval(obj);
        //ijha MITS 24382
		if (eval("window.document.forms[0]." + xname + "_cid") != null)
		    m_LookupCancelFieldList = xname + "|" + xname + "_cid";
		else
		    m_LookupCancelFieldList = xname;
				

	
		var str=obj.value;
		if(table_window!=null)
			table_window.close();

		if(str!='')
		{
			m_sFieldName=xname;
			// Mihika 24-Feb-2006: Passing the correct 'type' to quicklookup -> codetable name
			m_codeWindow = window.open('../Codes/QuickLookup.aspx?codetype=code.' + tid + '&lookupstring=' + str + '&descSearch=' + '', 'codeWnd', 'width=500,height=290' + ',top=' + (screen.availHeight - 290) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
		}
		else
		{
			obj=eval('document.forms[0].'+xname+'_cid');
			if (obj!=null)
				obj.value=0;
		}
		m_datachanged='false';	
	}		
}

function datachanged(ischanged)
{
	m_datachanged=ischanged;
}

function checkFlag()
{
	if(document.forms[0].FlagForRefresh.value == '1')
	{
		window.opener.document.location.reload(false);
		window.close();
	}	
	return false;
}
/*function importMessage()
{
	var sURL;
	if((document.forms[0].finalMessage!=null)&&(document.forms[0].finalMessage.value!=""))
	{
		alert(document.forms[0].finalMessage.value);
		sURL = window.opener.location.href;
		window.opener.location.href = sURL;
		window.close();
	}
}*/
function Delete()
{
 if (!confirm("Are you sure you want to permanently delete current record?"))
	return false;
 else
 {
  	return true;
 }
	
}
function Edit()
{
	document.forms[0].chkDesc.value='true';
}
function RefreshCodeTable()
{   if ( document.forms[0].finalMessage.value!=null && (document.forms[0].finalMessage.value!="")) {
    window.opener.parent.frames['frm_Top_Menu'].location.href = 'TableMaintTop.aspx';
    window.opener.parent.frames['frm_Tables'].location.href = 'TableList.aspx';
    window.opener.parent.frames['frm_Code_Detail'].location.href = 'CodeList.aspx';
			window.close();
	}

}

function OpenTableDetails(TableID, TypeCode)
{
	window.open("TableDetail.aspx?Mode=edit&ID=" + TableID + "&TypeCode=" + TypeCode,"","resizable=yes,scrollbars=yes,width=500,height=350");
}

// MITS - 15831 MAC : Needs to call getDropDownValue() then reload form else links will not be created to individual tables.
function TableListOnLoad() 
{
    if (parent.frames['frm_Tables'].document.getElementById('hdnTableId').value == "")
    {
    	getDropDownValue();
  	    var fTableList = document.forms[0];
	    fTableList.submit();
    }
}

//start - MITS 15819 - mpalinski
function getDropDownValue() {
    var objSelectedValue = parent.frames['frm_Top_Menu'].document.getElementById('DropDown_Table_Types');
// akaushik5 Added for MITS 32735 Starts
    if (objSelectedValue != null) {
// akaushik5 Added for MITS 32735 Ends
	 var sValue = objSelectedValue.options[objSelectedValue.selectedIndex].value;
        parent.frames['frm_Tables'].document.getElementById('hdnTableId').value = sValue;
        parent.frames['frm_Tables'].document.getElementById('TypeCode').value = sValue;
// akaushik5 Added for MITS 32735 Starts
		}
// akaushik5 Added for MITS 32735 Ends
    //alert("Hello!");
    return false;
}
//end - MIT 15819 - mpalinski
