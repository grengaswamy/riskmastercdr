	
	var m_arName ;
	var m_arDepth;
	var m_iCheckboxNumber;
	var m_iCachedTabIndex;
	var m_DataChanged = false ;
	
	function ConfirmSave()
	{
		//If from Admin and user ID listbox is empty
		if( document.forms[0].IsAdminUser.value == 'True' )
		{
			if( document.forms[0].lstUsersGroups.options.length == 0 )
				return false;
		}
		
		if(m_DataChanged)
		{
			if( confirm('Data has changed. Do you want to save changes?') )
			{
				Save();
			}
		}		
	}
	
	function Save()
	{
		var sUserIDs = ""
		if( document.forms[0].IsAdminUser.value == 'True' )
		{
			for(var f=0;f<document.forms[0].lstUsersGroups.options.length;f++)
			{
				sUserID = document.forms[0].lstUsersGroups.options[f].value
				if(sUserIDs == "" )
					sUserIDs = sUserID;
				else
					sUserIDs = sUserIDs + "," + sUserID;
			}
		}
		document.forms[0].hiddenSelectedUser.value = sUserIDs ;
		
//		window.document.forms[0].functiontocall.value = "ExecutiveSummaryAdaptor.SaveUserPreference" ;
		//window.document.forms[0].screenAction.value = "PageRefresh" ;
//		window.document.forms[0].method = "post" ;
		DisableButtons();
		return true;
//		window.document.forms[0].submit();
	}
	
	function CheckboxClick()
	{
		var oElem, index, depth;
		GetCheckboxNames();
		oElem = event.srcElement;
		index = oElem.index;
		depth = oElem.depth;
			
		// select all parents and un-select all children
		if( oElem.checked )
		{
			SelectParents(index, depth);
		}
		else
		{
			UnselectChildren(index, depth);
		}
		m_DataChanged = true ;
	}
	
	// check all the parent checkboxes
	function SelectParents(index, depth)
	{
		var i, oElem, name, depth2;
		var currentDepth;
		
		//If a top level node, just return
		if( depth == 0 )
		{
			return;
		}
		
		//search for parent nodes
		currentDepth = depth;
		index = parseInt(index);
		for(i=index-1; i>0; i--)
		{
			name = m_arName[i];
			oElem = document.forms[0].elements[name];
			if( oElem != null )
			{
				depth2 = oElem.depth;
				if( depth2 == 0 )
				{
					oElem.checked = true;
					return;
				}
				else if( depth2 < currentDepth )
				{
					currentDepth = depth2;
					oElem.checked = true;
				}
			}
		} 
	}

	// uncheck all the parent checkboxes
	function UnselectChildren(index, depth)
	{
		var i, oElem, name, depth2;
		index = parseInt(index);
		
		//search for parent nodes
		for(i=index+1; i<= m_iCheckboxNumber; i++)
		{
			name = m_arName[i];
			oElem = document.forms[0].elements[name];
			if( oElem != null )
			{
				depth2 = oElem.depth;
				if( depth2 == depth )
				{
					return;
				}
				else if( depth2 > depth )
				{
					oElem.checked = false;
				}
			}
			else
			{
				return;
			}
		} 
	}
	
	// Get all checkbox names and depth
	function GetCheckboxNames()
	{
		var i, oElem, sName, index;
		var iTabIndex;
		
		iTabIndex = document.forms[0].hTabName.value;
		// If already created
		if( (m_arName != null) && (m_iCachedTabIndex == iTabIndex ))
		{			
			return;
		}
		
		m_iCachedTabIndex = iTabIndex;
		m_arName = new Array();
		m_iCheckboxNumber = 0;

		var bCheckBoxes = document.getElementsByTagName("input");
		
		for (i=0; i<bCheckBoxes.length - 1; i++)
		{
			if( bCheckBoxes[i].type == "checkbox" )
			{
				oElem = bCheckBoxes[i] ;
				sName=new String(oElem.id);	
				if( oElem.tab == iTabIndex )
				{
					if(sName.substring(0,3)=="chk")
					{
						index = oElem.index;
						m_arName[index] = oElem.name;
						m_iCheckboxNumber ++;
					}
				}
			}
		}
	}
	
	function SelectAll()
	{
	    var bCheckBoxes = document.getElementsByTagName("input");
		
		for (var i=0; i<bCheckBoxes.length -1; i++)
		{
			if( bCheckBoxes[i].type == "checkbox" )
			{
				var objElem = bCheckBoxes[i];
				var sName=new String(objElem.id);
				if((sName.substring(0,3)=="chk") && (objElem.disabled==false))
				{
					objElem.checked=true;
				}
			}
		}
		m_DataChanged=true;		
		return false;
	}
	
	function UnselectAll()
	{
	    var bCheckBoxes = document.getElementsByTagName("input");
		
		for (var i=0; i<bCheckBoxes.length -1; i++)
		{
			if( bCheckBoxes[i].type == "checkbox" )
			{
				var objElem = bCheckBoxes[i];
				var sName=new String(objElem.id);
				if((sName.substring(0,3)=="chk") && (objElem.disabled==false))
				{
					objElem.checked=false;
				}
			}
		}
		m_DataChanged=true;		
		return false;
	}
	
	
	function AddUserGroup()
	{
		var sUserID, sUserIDs, index;
		if(document.forms[0].cboAllUsersGroups.selectedIndex<0)
			return false;
		var sValue=document.forms[0].cboAllUsersGroups.options[document.forms[0].cboAllUsersGroups.selectedIndex].value;
		for(index=0;index<document.forms[0].lstUsersGroups.options.length;index++)
		{
			if( sValue == document.forms[0].lstUsersGroups.options[index].value )
				return false;
		}
		
		var sText=document.forms[0].cboAllUsersGroups.options[document.forms[0].cboAllUsersGroups.selectedIndex].text;
		var opt=new Option(sText, sValue, false, false);
		document.forms[0].lstUsersGroups.options[document.forms[0].lstUsersGroups.options.length]=opt;
		
		// Get all the User IDs in the list box
		sUserIDs = ""
		for(index=0;index<document.forms[0].lstUsersGroups.options.length;index++)
		{
			sUserID = document.forms[0].lstUsersGroups.options[index].value;
			if(sUserIDs == "" )
				sUserIDs = sUserID;
			else
				sUserIDs = sUserIDs + "," + sUserID;
		}
		document.forms[0].hiddenSelectedUser.value = sUserIDs;
		
		// If only one user in the list box, try to retrieve its preference
		if (document.forms[0].lstUsersGroups.options.length == 1)
		{
//			window.document.forms[0].functiontocall.value = "ExecutiveSummaryAdaptor.GetUserPreference" ;
//			//window.document.forms[0].screenAction.value = "PageRefresh" ;
//			window.document.forms[0].method = "post" ;
			DisableButtons();
//			window.document.forms[0].submit();
			return true;					
		}
		m_DataChanged=true;						
		return false;
	}
	
	function DeleteUserGroup()
	{
		var sUserID, sUserIDs, index
		if(document.forms[0].lstUsersGroups.selectedIndex<0)
			return;
		index = document.forms[0].lstUsersGroups.selectedIndex
		document.forms[0].lstUsersGroups.options[index]=null; 
		m_DataChanged=true;
		
		// Get all the User IDs in the list box
		sUserIDs = ""
		for(var f=0;f<document.forms[0].lstUsersGroups.options.length;f++)
		{
			sUserID = document.forms[0].lstUsersGroups.options[f].value
			if(sUserIDs == "" )
				sUserIDs = sUserID;
			else
				sUserIDs = sUserIDs + "," + sUserID;
		}
		document.forms[0].hiddenSelectedUser.value = sUserIDs ;
		
		// If only one user in the list box, and the deleted user is not
		// the first user in the list, then try to retrieve its preference
		if ((document.forms[0].lstUsersGroups.options.length == 1) && (index == 0 ))
		{
//			window.document.forms[0].functiontocall.value = "ExecutiveSummaryAdaptor.GetUserPreference" ;
			//window.document.forms[0].screenAction.value = "PageRefresh" ;
//			window.document.forms[0].method = "post" ;
			DisableButtons();
//			window.document.forms[0].submit();
			return true ;
		}
		
		//If there is no user left in the list, set to the default
		if ( document.forms[0].lstUsersGroups.options.length == 0 )
		{
			document.forms[0].hiddenSelectedUser.value = "" ;
//			window.document.forms[0].functiontocall.value = "ExecutiveSummaryAdaptor.GetUserPreference" ;
			//window.document.forms[0].screenAction.value = "PageRefresh" ;
//			window.document.forms[0].method = "post" ;
			DisableButtons();
//			window.document.forms[0].submit();
			return true ;
		}
	}
	
	function DisableButtons()	
	{
		window.document.forms[0].btnSaveConfiguration.disabled = true ;
		window.document.forms[0].btnSelectAll.disabled = true ;
		window.document.forms[0].btnUnselectAll.disabled = true ;
		if( document.forms[0].IsAdminUser.value == 'True' ) {
		    //igupta3 Mits: 31732
		    // window.document.forms[0].btnAddUser.disabled = true;
		    window.document.forms[0].btnAddAll.disabled = true;
		    window.document.forms[0].btnAddSelected.disabled = true;
		    window.document.forms[0].btnRemoveSelected.disabled = true;
		    window.document.forms[0].btnRemoveAll.disabled = true;
		} 				
	}
	
	function OnLoad()
	{
		if( document.forms[0].IsAdminUser.value == 'True' )
		{
			if( document.forms[0].hiddenSelectedUser.value == "" )
			{
				window.status="Finished";
				return ;
			}
				
			var arrUserIds = document.forms[0].hiddenSelectedUser.value.split( "," );
			for( var iIndex=0 ; iIndex < arrUserIds.length ; iIndex++ )
			{
				var sValue = arrUserIds[iIndex] ;
				var sText = "" ;
				for(index=0;index<document.forms[0].cboAllUsersGroups.options.length;index++)
				{
					if( sValue == document.forms[0].cboAllUsersGroups.options[index].value )
					{
						sText = document.forms[0].cboAllUsersGroups.options[index].text ;
					}
				}
				var opt = new Option( sText, sValue , false, false);
				document.forms[0].lstUsersGroups.options[document.forms[0].lstUsersGroups.options.length]=opt;
			}
		}
		window.status="Finished";
	}