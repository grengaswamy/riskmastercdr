
// Naresh - 14-Nov-2005 - This js file contains methods for supporting editable Grid control implementation.

// Copies data from hidden column on selected grid row/new row to controls on the pop-up window.
function CopyGridRowDataToPopup() {
    var Gridname=document.getElementById('gridname').value + '_';
    var SelectedRowPosition=document.getElementById('selectedrowposition').value;
    var mode = document.getElementById('mode').value; 
    
    var objParentDocument = window.opener.document;
	if(objParentDocument==null)
	{
		alert("Error. Unable to access parent window document. Please contact your System Administrator. ");
		return false;
	}
	
	if(SelectedRowPosition.length == 1)
	    SelectedRowPosition = '0' + SelectedRowPosition;
    //var ParentElementId = Gridname + 'gvData_ctl' + SelectedRowPosition + '_|' + SelectedRowPosition + '|Data';
	var ParentElementId = Gridname + 'gvData_|' + SelectedRowPosition + '|Data_' + (SelectedRowPosition-2);
	
	var ParentElement = objParentDocument.getElementById(ParentElementId);
	if (ParentElement != null)
	{
	    if (document.getElementById('txtData') != null) {
	        document.getElementById('txtData').value = ParentElement.value.replace('&#39;', ' ');           //Ankit Start : Worked for JIRA - 891
	    }
	    if (document.getElementById('comboRelations') != null)
	     {
	        var objGridComboRelations = objParentDocument.getElementById(Gridname + "ComboRelations");
	        if (objGridComboRelations != null)
	         {
	            document.getElementById('comboRelations').value = objGridComboRelations.value;
	         }
	    }
			    //Added Rakhi for R7:Add Emp Data Elements
//	    var oPopupGrid = document.getElementById('PopupGrid');
//	    var oPageLoad = document.getElementById('PageLoad');
//	    if (mode.toLowerCase()=="add" && oPopupGrid != null && oPageLoad != null && oPageLoad.value.toLowerCase() == "true") {
//	        document.forms[0].submit();
//	    }
	    //Added Rakhi for R7:Add Emp Data Elements

	    if (mode == 'edit' && document.getElementById('txtPostBack').value == '')
	     {
	         if (typeof (pleaseWait) != 'undefined')
	         {
	             if (pleaseWait.Message == '')
	             {
	                 pleaseWait.Message = "Loading ...";
	             }
	            pleaseWait.Show('start');
	        }
            //MITS 25273 rsushilaggar Date 06/22/2011
	        if (document.forms[0].SysFormName != null && document.forms[0].SysFormName.value == 'futurepayments') { 
            }
	        else {
	            document.forms[0].submit();
	        }//BOB
	    }
	}
}

// Updates the hidden column on the selected grid row/new row with the values of controls on the pop-up window
function GetDataFromOpener()
{
    window.opener.GetListBoxValues();
	var Gridname=document.getElementById('gridname').value + '_';
    var SelectedRowPosition=document.getElementById('selectedrowposition').value;
    var mode = document.getElementById('mode').value; 
    
    var objParentDocument = window.opener.document;
	if(objParentDocument==null)
	{
		alert("Error. Unable to access parent window document. Please contact your System Administrator. ");
		return false;
	}
	
	var objParentDocumentForm = objParentDocument.forms[0];
	if(objParentDocumentForm==null)
	{
		alert("Error. Unable to access parent window document form. Please contact your System Administrator. ");
		return false;
	}
	if(SelectedRowPosition.length == 1)
	    SelectedRowPosition = '0' + SelectedRowPosition;
    //var ParentElementId = Gridname + 'gvData_ctl' + SelectedRowPosition + '_|' + SelectedRowPosition + '|Data';
	var ParentElementId = Gridname + 'gvData_|' + SelectedRowPosition + '|Data_' + (SelectedRowPosition - 2);
	var ParentElement = objParentDocument.getElementById(ParentElementId);
	if(ParentElement != null)
	{
	    ParentElement.value=document.getElementById('txtData').value  ;
	    var objGridRowAddedFlag = objParentDocument.getElementById(Gridname+"RowAddedFlag");
	    var objGridRowAction = objParentDocument.getElementById(Gridname+"Action");
		if(objGridRowAddedFlag==null)
		{
			alert("Error. Unable to access RowAddedFlag on parent window document. Please contact your System Administrator. ");
			return false;
		}
		if(objGridRowAction == null)
		{
		    alert("Error. Unable to access Action on parent window document. Please contact your System Administrator. ");
			return false;
		}
		if(objParentDocumentForm.hdPostBack != null)
	    	objParentDocumentForm.hdPostBack.value = "1";
		objGridRowAddedFlag.value = "true";
		objGridRowAction.value = mode;

		if(objParentDocumentForm.AddedControlValue != null)
		    objParentDocumentForm.AddedControlValue.value = document.getElementById('txtData').value;
		if(objParentDocumentForm.SequenceID!=null)
		    objParentDocumentForm.SequenceID.value = parseInt(SelectedRowPosition,10) - 1;

		//Set hidden field main page DataChanged to true
		if( window.opener.setDataChanged != null )
		{
		    window.opener.setDataChanged(true);
		}
		if(objParentDocumentForm.SysCmd!=null)
		    objParentDocumentForm.SysCmd.value = 7;
		//objParentDocumentForm.submit();
		try
		{
			if(window.opener.pleaseWait !=null)
			{
			    	window.opener.pleaseWait.Show('Start');
			}
		}
		catch (ex) { }
		if (objParentDocumentForm.SysIsFormSubmitted != null)
		{
		    objParentDocumentForm.SysIsFormSubmitted.value = "1";
		}
		objParentDocumentForm.submit();
		window.close();
		return true;
	}
	else
	{
	    alert("Error. Unable to access parent window document. Please contact your System Administrator. ");
		return false;
	}
}
