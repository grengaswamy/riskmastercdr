function supportCookies()
{
	document.cookie="cookietest=1;";
	if(getCookie("cookietest")=="1")
	{
		deleteCookie("cookietest");
		return true;
	}
	return false;	
}

function getCookie(sCookie)
{
  // cookies are separated by semicolons
  var aCookie = document.cookie.split(";");
  for (var i=0; i < aCookie.length; i++)
  {
    // a name/value pair (a crumb) is separated by an equal sign
    var aCrumb = aCookie[i].split("=");
	 var sName=replace(aCrumb[0]," ","");
    if (sCookie == sName) 
      return unescape(aCrumb[1]);
  }
  // a cookie with the requested name does not exist
  return null;
}

function setCookie(sName, sValue)
{
	document.cookie = sName + "=" + escape(sValue)+";";
	return true;
}

function replace(sSource, sSearchFor, sReplaceWith)
{
	var arr = new Array();
	arr=sSource.split(sSearchFor);
	return arr.join(sReplaceWith);
}


function deleteCookie(sCookie)
{
	var dExp=new Date();
	dExp.setFullYear(dExp.getFullYear()-1);
	document.cookie=sCookie+"=;expires=Mon, 01 Dec 1980 23:00:00 UTC;";
	return true;
}

function goEvent(lEventId)
{
	// var s;
	// Start Naresh MITS 8543 Start 12/19/2006
	// s='RMX.urlNavigate("fdm?SysFormName=event&amp;SysFormId='+lEventId+'&amp;SysCmd=0")';
	// s='RMX.window.open("fdm?SysFormName=event&amp;SysFormId='+lEventId+'&amp;SysCmd=0","","width=800,height=500,top="+(screen.availHeight-500)/2+",left="+(screen.availWidth-800)/2+",resizable=yes,scrollbars=yes")';
	// End Naresh MITS 8543 Start 12/19/2006
	// setTimeout(s, 100);
	MDIParent.CreateSingletonMDIChild("fdm?SysFormName=event&SysFormId="+lEventId+"&SysCmd=0");
	return false;
}

function goEmployee(lEid)
{
	// var s;
	// s='RMX.urlNavigate("fdm?SysFormName=employee&amp;SysFormId='+lEid+'&amp;SysCmd=0")';
	// setTimeout(s, 100);
	MDIParent.CreateSingletonMDIChild("fdm?SysFormName=employee&SysFormId="+lEid+"&SysCmd=0");
	return false;
}

function goEntity(lEid)
{
	var s;
	var pformname;
	var peopletype;
	var a;
	pformname="entitymaint";
	
	s = new String(document.forms[0].sys_ex.value);
	a = s.split(",");

	if (a[0] == "people")
	{
		pformname=a[0];
		peopletype=a[1];
		var entitytableid=document.forms[0].entitytableid.value;
		// s='RMX.urlNavigate("fdm?SysFormName=people&amp;peopletype=' + peopletype + '&amp;sys_formpidname=entitytableid,peopletype&amp;sys_formidname=entityid&SysFormId='+lEid+'&amp;entitytableid='+entitytableid+'&amp;SysCmd=0")';	
		MDIParent.CreateSingletonMDIChild("fdm?SysFormName=people&peopletype=" + peopletype + "&sys_formpidname=entitytableid,peopletype&sys_formidname=entityid&SysFormId="+lEid+"&entitytableid="+entitytableid+"&SysCmd=0");
	}
	else
		// s='RMX.urlNavigate("fdm?SysFormName=entitymaint&amp;SysFormId='+lEid+'&amp;SysCmd=0")';	
		MDIParent.CreateSingletonMDIChild("fdm?SysFormName=entitymaint&SysFormId="+lEid+"&SysCmd=0");
	
	// setTimeout(s, 100);
	return false;
}

function goClaim(lClaimId, sClaimPage)
{
	// var s;
	// Start Naresh MITS 8543 Start 12/19/2006
	//s='RMX.urlNavigate("fdm?SysFormName=' +sClaimPage+ '&amp;SysFormId='+lClaimId+'&amp;SysCmd=0")';
	// s='RMX.window.open("fdm?SysFormName=' +sClaimPage+ '&amp;SysFormId='+lClaimId+'&amp;SysCmd=0","","width=850,height=500,top="+(screen.availHeight-500)/2+",left="+(screen.availWidth-850)/2+",resizable=yes,scrollbars=yes")';
	// End Naresh MITS 8543 Start 12/19/2006
	// setTimeout(s, 100);
	MDIParent.CreateSingletonMDIChild("fdm?SysFormName=" + sClaimPage + "&SysFormId="+lClaimId+"&SysCmd=0");
	return false;
}

function goPolicy(lId)
{
	// var s;
	// s='RMX.urlNavigate("fdm?SysFormName=policy&amp;SysFormId='+lId+'&amp;SysCmd=0")';
	// setTimeout(s, 100);
	MDIParent.CreateSingletonMDIChild("fdm?SysFormName=policy&SysFormId="+lId+"&SysCmd=0");
	return false;
}

function goVehicle(lId)
{
	// var s;
	// s='RMX.urlNavigate("fdm?SysFormName=vehicle&amp;SysFormId='+lId+'&amp;SysCmd=0")';	
	// setTimeout(s, 100);
	MDIParent.CreateSingletonMDIChild("fdm?SysFormName=vehicle&SysFormId="+lId+"&SysCmd=0");
	return false;
}

function goPatient(lId)
{
	// var s;
	var sExternalParam = "<SysExternalParam><from>search</from></SysExternalParam>" ;
	//document.location.href= "home?pg=riskmaster/fdm/fdm&amp;SysFormName=autoclaimchecks&amp;SysExternalParam=" + sExternalParam  ;						
	// s='RMX.urlNavigate("fdm?SysFormName=patient&amp;SysFormId='+lId+'&amp;SysCmd=0&amp;SysExternalParam=' + sExternalParam+'")';
	// setTimeout(s, 100);
	MDIParent.CreateSingletonMDIChild("fdm?SysFormName=patient&SysFormId="+lId+"&SysCmd=0&SysExternalParam=" + sExternalParam);
	return false;
}

function goPhysician(lId) 
{
    // var s;
	// s='RMX.urlNavigate("fdm?SysFormName=physician&amp;SysFormId='+lId+'&amp;SysCmd=0")';
	// setTimeout(s, 100);
	MDIParent.CreateSingletonMDIChild("fdm?SysFormName=physician&SysFormId="+lId+"&SysCmd=0");
	return false;
}

function goMedStaff(lId)
{
	// var s;
	// s='RMX.urlNavigate("fdm?SysFormName=staff&amp;SysFormId='+lId+'&amp;SysCmd=0")';
	// setTimeout(s, 100);
	MDIParent.CreateSingletonMDIChild("fdm?SysFormName=staff&SysFormId="+lId+"&SysCmd=0");
	return false;
}

function goPayment(lId)
{
	// var s;
	// s='RMX.urlNavigate("home?pg=riskmaster/Funds/trans&amp;TransID='+lId+'")';	
	// setTimeout(s, 100);
	MDIParent.CreateSingletonMDIChild("home?pg=riskmaster/Funds/trans&TransID="+lId);
	return false;
}

// Mihika Defect no. 2036: Adding case of 'Disability Plan' search
function goDisPlan(lId)
{
	// var s;
	// s='RMX.urlNavigate("fdm?SysFormName=plan&amp;SysFormId='+lId+'&amp;SysCmd=0")';
	// setTimeout(s, 100);
	MDIParent.CreateSingletonMDIChild("fdm?SysFormName=plan&SysFormId="+lId+"&SysCmd=0");
	return false;
}

function goLeavePlan(lId)
{
	// var s;
	// s='RMX.urlNavigate("fdm?SysFormName=leaveplan&amp;SysFormId='+lId+'&amp;SysCmd=0")';
	// setTimeout(s, 100);
	MDIParent.CreateSingletonMDIChild("fdm?SysFormName=leaveplan&SysFormId="+lId+"&SysCmd=0");
	return false;
}

function goPolicyEnh(lId)
{
	// var s;
	// s='RMX.urlNavigate("fdm?SysFormName=policyEnh&amp;SysFormId='+lId+'&amp;SysCmd=0")';
	// setTimeout(s, 100);
	// Naresh (6/5/2007) MITS 9279 
	MDIParent.CreateSingletonMDIChild("fdm?SysFormName=policyenh&SysFormId="+lId+"&SysCmd=0");
	return false;
}
//skhare7
function goDiary(lId) {
    
    MDIParent.CreateSingletonMDIChild("home?pg=riskmaster/Diaries/DiaryDetails&SysFormId=" + lId);
    return false;
}
function goADM(sADMTable, lRecordId)
{
	if (sADMTable.toLowerCase() == "cert_of_insurance" || sADMTable.substr(0, 5).toLowerCase() == "froi_")
	{
		setTimeout('document.location = "/app/pdf/froiselect.asp?op=selid&tablename=' + sADMTable + '&rid=' + String(lRecordId) + '"', 100);
	}
	else
	{
		// var s;
		//s='RMX.urlNavigate("adm.asp?formname=' + sADMTable + '&sys_admpid='+lRecordId+'&syscmd=0"';
		// s='RMX.urlNavigate("fdm?SysFormName=admintracking|'+sADMTable+'&amp;SysFormId='+lRecordId+'&amp;SysCmd=0")';
		// setTimeout(s, 100);
		MDIParent.CreateSingletonMDIChild("fdm?SysFormName=admintracking|"+sADMTable+"&SysFormId="+lRecordId+"&SysCmd=0");
	}
	return false;
}
//mbahl3 mits 30224
function selRecord(lRecordID, sEntityFormName, sIsActiveEntity, sUseEntityRole) {       //avipinsrivas start : Worked on JIRA - 7767
    sEntityFormName = replace(sEntityFormName, "&quot;", "\""); //MITS 25139
    if (window.opener != null) {
        if (sIsActiveEntity != null && sIsActiveEntity.toLowerCase() == "false") {
            alert(CreateEntityValidations.ValidSelectedInactiveEntity);
            //alert("Selected Entity is Inactive");
            return false;
        }
	//mbahl3 mits 30224
        //return selRecordFromPopUp(lRecordID, sEntityFormName); //MITS 17157 Pankaj 7/15/09 Org Hierarchy Advanced Search
        //Sumit - Start(11/08/2010) - MITS# 21950
         var bValue = selRecordFromPopUp(lRecordID, sEntityFormName); //MITS 17157 Pankaj 7/15/09 Org Hierarchy Advanced Search
        var objSysFormName = window.opener.document.getElementById("SysFormName");
        if (objSysFormName != null) {
            var sFormName = objSysFormName.value;
            switch (sFormName) {
                case "exposure":
                case "exposureal":
                case "exposurepc":
                case "Coverages":
                    window.opener.EnableSupplementalsOkButton();
                    break;
            }
        }
        return bValue;
        //Sumit - End
    }
    else
        return selRecordFromScreen(lRecordID, sEntityFormName, sUseEntityRole);        //avipinsrivas start : Worked for JIRA - 7767
}

function selRecordFromScreen(lRecordID, sEntityFormName, sUseEntityRole)       //avipinsrivas start : Worked for JIRA - 7767
{
    if(lRecordID!=0)
	{
		var searchcat;
		var admtable;
		var del = parent.unitSeparator;
		searchcat=document.forms[0].searchcat.value;
		admtable=document.forms[0].admtable.value;
        //avipinsrivas start : Worked for JIRA - 7767
		if (sUseEntityRole != null && sUseEntityRole.toLowerCase() == "true") {
		    //avipinsrivas start : Worked on JIRA - 14241 (For Epic 7767 and Story 13197)
		    var sTemp = '';    		    
		    var iIndex = -1;
		    if ((sEntityFormName != "") && (sEntityFormName != "&nbsp;")) {
		        sTemp = new String(sEntityFormName)
		        iIndex = sTemp.indexOf("|")
		    }
		    //avipinsrivas end
		    switch (searchcat) {
		        case "entity":
		        case "driver":
		        case "employee":
		        case "medstaff":
		        case "patient":
		        case "physician":
		            if (iIndex == -1) // Org Hierarchy          //avipinsrivas start : Worked on JIRA - 14241 (For Epic 7767 and Story 13197)
		                sEntityFormName = '';
		            searchcat = "entity";
		        default:
		            //Do nothing
		    }
		}
        //avipinsrivas end
		if (searchcat == "adm") {
		    parent.MDIShowScreen(lRecordID, "admintrackinglist" + del + "Admin Tracking (" + admtable + ")" + del + "UI/FDM/admintracking" + admtable + ".aspx" + del + "?recordID=(NODERECORDID)");
		/* nnorouzi April 2, 2009
        There are 2 parameters; the second represents the categoryArg and consists up to 6 items. We can choose to not provide all items but the order must be followed; empty strings are allowed to be passed in order to skip some items. if the categoryArg is only the sysName then we call the function as: MDIShowScreen(id, sysName); We provide more items because we want to change user prompt, path, passing parameters, or inject the node(or a new node) at another place in navTree.
        1: indicates the recordId for the node; since Diary List needs no id then we pass zero. if it was a new node(corresponding to a blank form/new screen) which needed an id to be assigned then we would have passed -1.
        2.a: indicates the system name/sysFormName or usually the .aspx file name.
        2.b: indicates the title. If nothing is provided(empty string) then MDI will try to get it from MDIMenu.xml.
        2.c: indicates the localpath of the .aspx file. If nothing is provided(empty string) then MDI will try to get it from MDIMenu.xml.
        2.d: indicates the url string parameter which must be passed along; when the .aspx file is called.
        2.e: if "SELECTED" is passed, it indicates that the node must be placed under selected node (note that if selected node is under a different root then "TOP" will be considered)
        if "TOP" is passed, it indicates that the node must be placed right under its Root. The default value is "SELECTED" for Document root but can be "TOP" for other roots.
        2.f: indicates another root under which this node must be added; other than its default root.
        
        hint: if 2.e is provided(if 2.f is passed we must pass 2.e as well) then even it is an empty string the node is treated as an injected node. we inject nodes in our desired places when MDI put them somewhere else by its own approach. injected nodes might not get the context menu.
        */
		}
		// PJS : Cases added for pages where searchcat is coming different
		else if (searchcat == "entity")
		{
		    if ((sEntityFormName != "") && (sEntityFormName != "&nbsp;"))
		    {
		        var sTemp = new String(sEntityFormName);
    		    
                var iIndex = sTemp.indexOf("|");
                
                if(iIndex != -1) // Org Hierarchy
                {
                    var arrTemp = sTemp.split("|");
                    
                    var sFormName = arrTemp[0];
                    var sEntityLevel = arrTemp[1];

                    parent.MDIShowScreen(lRecordID, "OrgHierarchyMaintenance" + del + "" + del + "UI/FDM/" + sFormName + ".aspx" + del + "?recordID=(NODERECORDID)&entitylevel=" + sEntityLevel);
                }
                else
                {
		            parent.MDIShowScreen(lRecordID, sEntityFormName);
                }
            }
            else
            {
                sEntityFormName = "entitymaint";
	            parent.MDIShowScreen(lRecordID, sEntityFormName);
            }
        }              
        //Mridul. 11/26/09. MITS:18229. To transfer request to correct page.     
		else if (searchcat == "policyenh")
		{
		    if ((sEntityFormName != "") && (sEntityFormName != "&nbsp;"))
		    {
                parent.MDIShowScreen(lRecordID, sEntityFormName);
            }
            else
            {
                parent.MDIShowScreen(lRecordID, searchcat);
            }
        }        
        //End Mridul.
        else if (searchcat == "displan")
        {
            searchcat = "plan";
            parent.MDIShowScreen(lRecordID, searchcat);
        }
        else if (searchcat == "payment") {
            searchcat = "funds";
            parent.MDIShowScreen(lRecordID, searchcat);
        }
        else if (searchcat == "medstaff") {
            searchcat = "staff";
            parent.MDIShowScreen(lRecordID, searchcat);
        }
        else if (searchcat == "driver") {
            searchcat = "driver";
          // parent.MDIShowScreen(lRecordID, searchcat);
            parent.MDIShowScreen(lRecordID, "driver" + del + "" + del + "UI/FDM/Driver.aspx" + del + "?recordID=(NODERECORDID)");
        }
        else if (searchcat == "patient") {
        //MGaba2:05/05/2009:MITS 16041:Adding entry of search/patient tracking in Activity Log
        var sExternalParam = "<SysExternalParam><from>search</from></SysExternalParam>";
        parent.MDIShowScreen(lRecordID, "Patient" + del + "" + del + "UI/FDM/patient.aspx" + del + "?recordID=(NODERECORDID)&SysExternalParam=" + sExternalParam);
		}
		else if (searchcat == "diary") {
		    var sAttachprompt = sEntityFormName;
		    //avipinsrivas start : Worked for JIRA - 14712 (correcting merging Issue against change-set #107088)
		    //parent.MDIShowScreen(lRecordID, "DiaryList" + del + "" + del + "UI/Diaries/DiaryDetails.aspx" + del + "?entryid=(NODERECORDID)&attachprompt=" + sAttachprompt);
		    parent.MDIShowScreen(lRecordID, "DiaryList" + del + "" + del + "UI/FDM/diarydetails.aspx" + del + "?entryid=(NODERECORDID)&recordID=(NODERECORDID)&attachprompt=" + sAttachprompt + "&CalledBy=diarydetails");
            //avipinsrivas end
        }
		else {
		    parent.MDIShowScreen(lRecordID, searchcat);
		}
		return;
		
		switch(searchcat)
		{
			case "claim":
			case "claimgc":
			case "claimwc":
			case "claimva":
			case "claimdi":
			//Mridul 09/30/09. MITS#18230. Property Claims.
			case "claimpc":
				goClaim(lRecordID,searchcat);
				break;
			case "event":
				goEvent(lRecordID);
				break;
			case "adm":
				goADM(admtable, lRecordID);
				break;
			case "employee":
				goEmployee(lRecordID);
				break;
			case "policy":
				goPolicy(lRecordID);
				break;
			case "payment":
				goPayment(lRecordID);
				break;
			case "entity":
				goEntity(lRecordID);
				break;
			case "vehicle":
				goVehicle(lRecordID);
				break;
			case "patient":
				goPatient(lRecordID);
				break;
			case "physician":
				goPhysician(lRecordID);
				break;
			case "medstaff":
				goMedStaff(lRecordID);
				break;
			case "displan":
				goDisPlan(lRecordID);
				break;
			//Start Shruti leave Plan Search
			case "leaveplan":
				goLeavePlan(lRecordID);
				break;
			//End Shruti Leave Plan Search
			// Start Naresh Enhanced Policy Search
			case "policyenh":
				goPolicyEnh(lRecordID);
				break;
			 // End Naresh Enhanced Policy Search
            case "diary":
                goDiary(lRecordID); //skhare7
                 break;
			default:
				alert("Default case." + document.forms[0].searchcat.value);
				break;
		}
	}
	return false;
}

function selRecordFromPopUp(lRecordID, sEntityFormName) 
{//MITS 17157 Pankaj 7/15/09 Org Hierarchy Advanced Search
      if (lRecordID != 0) {
        var searchcat;
        var admtable;
        searchcat = document.forms[0].searchcat.value;
        admtable = document.forms[0].admtable.value;

        var sScreenFlag = document.forms[0].hdScreenFlag.value;

        // This is required when an existing "Person Involved" is being searched from the Person
        // Involved screen.Nima to add a function just before return.
        if (sScreenFlag == '5') {

            return;
        }


        switch (searchcat) {
            case "claim":
                {
                    //if(haveProperty(window.opener,"lookupCallback"))
                    //{
                    if (window.opener.lookupCallback != null) {
                        eval("window.opener." + window.opener.lookupCallback + "(" + lRecordID + ")");
                        //Shruti for 12416
                        window.opener.setDataChanged(true);
                        return false;
                    }
                    //}
                    break;
                }
            case "payment":
                {
                    //if(haveProperty(window.opener,"lookupCallback"))
                    //{
                    if (window.opener.lookupCallback != null) {
                        eval("window.opener." + window.opener.lookupCallback + "(" + lRecordID + ")");
                        return false;
                    }
                    //}
                    break;
                }
            case "event":
                {
                    //if(haveProperty(window.opener,"lookupCallback"))
                    //{
                    if (window.opener.lookupCallback != null) {
                        eval("window.opener." + window.opener.lookupCallback + "(" + lRecordID + ")");
                        //Shruti for 12416
                        window.opener.setDataChanged(true);
                        return false;
                    }
                    //}
                    goEvent(lRecordID);
                    break;
                }
            case "adm":
                {
                    goADM(admtable, lRecordID);
                    break;
                }
            case "entity":
                {

                    if (window.name == "multiunitlookup") {
                    
                        window.opener.SetUnitNameList("SU_" + lRecordID + "_0", "Stat Unit: " + sEntityFormName);
                        window.close();

                        var sitenumber = "";
                        var objSiteNumber = document.getElementById("grdLookUp_" + pid);
                        if (objSiteNumber != null) {
                            if (objSiteNumber.childNodes != null) {
                                sitenumber = objSiteNumber.childNodes[0].innerText;
                                //window.opener.document.forms[0].slsiteid.value = sitenumber;
                            }
                        }

                    }
                    //MITS 17157 Pankaj 7/15/09 Org Hierarchy Advanced Search
                    if ((sEntityFormName != "") && (sEntityFormName != "&nbsp;")) {
                        var sTemp = new String(sEntityFormName);
                        var iIndex = sTemp.indexOf("|");
                        if (iIndex != -1) // Org Hierarchy
                        {
                            var arrTemp = sTemp.split("|");

                            var sFormName = arrTemp[0];
                            var sEntityLevel = arrTemp[1];
                        }
                    }

                    //if(haveProperty(window.opener,"lookupCallback"))
                    //{
                    if (window.opener.lookupCallback != null) {
                        //avipinsrivas Start : Worked for Jira-340(entityroleids is ~ seperated combination of Entity_X_Roles --> Entity_ID & Er_RowID
                        if (window.opener.lookupCallback == "MDIOnPersonSearchCallback")
                            eval("window.opener." + window.opener.lookupCallback + "(" + lRecordID + ",'" + searchcat + "')");
                        //avipinsrivas End
                        else if (sEntityLevel != "")//MITS 17157 Pankaj 7/15/09 Org Hierarchy Advanced Search
                            eval("window.opener." + window.opener.lookupCallback + "(" + lRecordID + ",'" + GetShortOrgLevel(sEntityLevel) + "')");
                        else
                            eval("window.opener." + window.opener.lookupCallback + "(" + lRecordID + ")");
                        return false;
                    }
                    //}
                    //window.opener.document.entitySelected(lRecordID);
                    window.opener.entitySelected(lRecordID);
                    break;
                }
            case "employees":
                {
                    //if(haveProperty(window.opener,"lookupCallback"))
                    //{
                    if (window.opener.lookupCallback != null) {
                        eval("window.opener." + window.opener.lookupCallback + "(" + lRecordID + ")");
                        return false;
                    }
                    //}
                    //window.opener.document.employeeSelected(lRecordID);
                    break;
                }
            case "policy":
                {
                    if (window.name == "multipolicylookup") {
                        //    window.opener.SetClaimPolicyList(lRecordID, sEntityFormName, '', '');
                        window.opener.ValidatePolicy(lRecordID, sEntityFormName); //Added by amitosh for PolicyInterface
                         window.close();
                    }
                    else {
                        window.opener.document.policySelected(lRecordID);
                    }
                    break;
                }
                // Start Naresh Enhanced Policy Search
            case "policyenh":
                {
                    window.opener.document.enhpolicySelected(lRecordID);
                    break;
                }
                // End Naresh Enhanced Policy Search
            case "vehicle":
                {
                    //if(haveProperty(window.opener,"lookupCallback"))
                    //{
                    try {
                    //parag new
                        if (window.name == "multiunitlookup") {

                          //  alert(lRecordID + sEntityFormName);
                            window.opener.SetUnitNameList("V_"+lRecordID+"_0", "Vehicle Unit: "+sEntityFormName);
                            window.close();
                        }

                        else if (window.opener.lookupCallback != null) {
                            eval("window.opener." + window.opener.lookupCallback + "(" + lRecordID + ")");
                            return false;
                        }
                    }
                    catch (e) {
                        alert(e);
                    }
                    //}
                    break;
                }
                //smahajan6 11/26/09 MITS:18230 :Start
            case "propertyunit":
                {
                    if (window.name == "multiunitlookup") {
                    //parag new
                       // alert(lRecordID + sEntityFormName);

                        window.opener.SetUnitNameList("P_" + lRecordID+"_0", "Property Unit: " + sEntityFormName);
                        window.close();
                    }
                    if (window.opener.lookupCallback != null) {
                        eval("window.opener." + window.opener.lookupCallback + "(" + lRecordID + ")");
                        return false;
                    }
                    break;
                }
                //smahajan6 11/26/09 MITS:18230 :End
            case "patient":
                {
                    //if(haveProperty(window.opener,"lookupCallback"))
                    //{
                    if (window.opener.lookupCallback != null) {
                        eval("window.opener." + window.opener.lookupCallback + "(" + lRecordID + ",'patient')");
                        return false;
                    }
                    //}
                    window.opener.document.entitySelected(lRecordID, 'patient');
                    break;
                }
                //BEGIN:changed by Mohit
            case "physician":
                {
                    if (window.opener.lookupCallback != null) {
                        eval("window.opener." + window.opener.lookupCallback + "(" + lRecordID + ")");
                        return false;
                    }
                    break;
                }
                //END:changed by Mohit
                //Start Shruti leave Plan Search
            case "leaveplan":
                {
                    if (window.opener.lookupCallback != null) {
                        eval("window.opener." + window.opener.lookupCallback + "(" + lRecordID + ")");
                        return false;
                    }
                    break;
                }
                //End Shruti Leave Plan Search
            case "driver":
                {
                    //if(haveProperty(window.opener,"lookupCallback"))
                    //{
                    if (window.opener.lookupCallback != null) {
                        eval("window.opener." + window.opener.lookupCallback + "(" + lRecordID + ",'driver')");
                        return false;
                    }
                    //}
                    window.opener.document.entitySelected(lRecordID, 'driver');
                    break;
                }
            //JIRA-8753 nshah28 start
            case "address":
                {
                    if (window.opener.lookupCallback != null) {
                        eval("window.opener." + window.opener.lookupCallback + "(" + lRecordID + ",'addressMaint')");
                        return false;
                    }
                    window.opener.document.entitySelected(lRecordID, 'address');
                    break;
                }
            //JIRA-8753 nshah28 end
            default:
                //if(haveProperty(window.opener,"lookupCallback"))
                //{
                if (window.opener.lookupCallback != null) {
                    eval("window.opener." + window.opener.lookupCallback + "(" + lRecordID + ")");
                    return false;
                }
                //}
                alert("Default case." + document.forms[0].searchcat.value);
                break;
        }
    }
    return false;
}


function launchExecSummary(lRecordID)
{
	if(lRecordID!=0)
	{
		var searchcat;
		var admtable;
		
		searchcat=document.forms[0].searchcat.value;
		switch(searchcat)
		{
			case "claim":
                window.open('/RiskmasterUI/UI/Executivesummary/ExecSummary.aspx?recid='+lRecordID+'&type=claim',
			            'execsummary', 'width=740,height=620' + ',top=' + (screen.availHeight - 620) / 2 + ',left=' + (screen.availWidth - 740) / 2 + ',resizable=yes,scrollbars=yes');
				//setTimeout('document.location.href = "../Executivesummary/ExecSummary.aspx?type=claim&recid=' + lRecordID + '"', 100);
				break;
		
			case "event":
                window.open('/RiskmasterUI/UI/Executivesummary/ExecSummary.aspx?recid='+lRecordID+'&type=event',
			            'execsummary', 'width=740,height=620' + ',top=' + (screen.availHeight - 620) / 2 + ',left=' + (screen.availWidth - 740) / 2 + ',resizable=yes,scrollbars=yes');
				break;
						
			default:
				alert("Default case." + document.forms[0].searchcat.value);
				return false;
		}
	}
}

function goToPage(sPage)
{
	RMX.urlNavigate('home?pg=riskmaster/Search/SearchResult&page='+ sPage);
}

function PrintSearch() 
{
    window.open('/RiskmasterUI/UI/Search/PrintSearch.aspx',
			'PrintSearch', 'width=740,height=620' + ',top=' + (screen.availHeight - 500) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
}

function GetShortOrgLevel(sTableId) {
//MITS 17157 Pankaj 7/15/09 Org Hierarchy Advanced Search
    var ShortOrgLevel = 'DT';
    switch (sTableId) {
        case '1005': ShortOrgLevel = 'C';
            break;
        case '1006': ShortOrgLevel = 'CO';
            break;
        case '1007': ShortOrgLevel = 'O';
            break;
        case '1008': ShortOrgLevel = 'R';
            break;
        case '1009': ShortOrgLevel = 'D';
            break;
        case '1010': ShortOrgLevel = 'L';
            break;
        case '1011': ShortOrgLevel = 'F';
            break;
        case '1012': ShortOrgLevel = 'DT';
            break;
        default: ShortOrgLevel = 1012;
    }
    return ShortOrgLevel;
}
function btnAddNew_Click()
{
    eval("window.opener.MDIOnPersonSearchCallback(-1)");
}