// This file is soley to remap from the old jscalendar to the zapatec calendar. Too many files use the old Calendar.setup function.
var Calendar = {};
Calendar.setup = function (params) {
	return Zapatec.Calendar.setup(params);
}