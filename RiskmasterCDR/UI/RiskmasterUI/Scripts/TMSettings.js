function MoveTo(lstScheduleType)
{
    var iScheduleType = lstScheduleType.value;
    var iTaskType = document.forms[0].lstTaskType.value;
    var sTaskTypeText = document.forms[0].hdnTaskType.value;
    //selected index of subtask.  : ukusvaha
    var iSubTaskValue = document.forms[0].lstSubTaskName.value;
    if (iSubTaskValue == "")
        iSubTaskValue = 0;
    
    
    //Mgaba2:R7: In case of History Tracking application ,name of dropdown for schedule type is lstScheduleTypeForHistoryTracking
    //document.forms[0].hdnScheduleType.value = document.forms[0].lstScheduleType.options[document.forms[0].lstScheduleType.selectedIndex].text;
    document.forms[0].hdnScheduleType.value = lstScheduleType.options[lstScheduleType.selectedIndex].text;
    var sScheduleTypeText = document.forms[0].hdnScheduleType.value;
    //getting text of selected subtask. in case of blank , subtask will be "None"    :ukusvaha
    document.forms[0].hdSubTaskNameText.value = document.forms[0].lstSubTaskName.options[document.forms[0].lstSubTaskName.selectedIndex].text;
    var sSubTaskNameText = document.forms[0].hdSubTaskNameText.value;
    if (sSubTaskNameText == "")
        sSubTaskNameText = "None";

    var sTaskName = document.forms[0].txtTaskName.value;

    var iSelectedTask = document.forms[0].lstTaskType.selectedIndex - 1;
    
        var sIsDataIntegratorJob = document.forms[0].hdDisabledStatusDetails.value;

        if (sIsDataIntegratorJob != "") 
        {
            var arrIsDataIntegratorTask = sIsDataIntegratorJob.split("^*^*^");
            var sSystemModuleName = document.forms[0].hdSystemModuleName.value;

            if (sSystemModuleName != "") 
            {
                var arrSystemModuleName = sSystemModuleName.split("^*^*^");
            }

        }
    
    
    if (iScheduleType != "0")
    {
        switch(iScheduleType)
	    {
            case '1':
                //window.location = "/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMSettingsOneTime.aspx?TaskType=" + iTaskType + "&SubTaskValue=" + iSubTaskValue + "&ScheduleType=" + iScheduleType + "&TaskTypetxt=" + sTaskTypeText + "&SubTaskNametxt=" + sSubTaskNameText + "&ScheduleTypetxt=" + sScheduleTypeText + "&TaskName=" + escape(sTaskName) + "&IsDataIntegratorTask=" + arrIsDataIntegratorTask[iSelectedTask] + "&SystemModuleName=" + arrSystemModuleName[iSelectedTask]; //Mits 21854:Added escape for taskname//MITS 33835 ksahu5 
                window.location = "/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMSettingsOneTime.aspx?Dummy=0&TaskType=" + encodeURIComponent(iTaskType) + "&SubTaskValue=" + encodeURIComponent(iSubTaskValue) + "&ScheduleType=" + encodeURIComponent(iScheduleType) + "&TaskTypetxt=" + encodeURIComponent(sTaskTypeText) + "&SubTaskNametxt=" + encodeURIComponent(sSubTaskNameText) + "&ScheduleTypetxt=" + encodeURIComponent(sScheduleTypeText) + "&TaskName=" + encodeURIComponent(sTaskName) + "&IsDataIntegratorTask=" + arrIsDataIntegratorTask[iSelectedTask] + "&SystemModuleName=" + arrSystemModuleName[iSelectedTask]; //MITS 33835 ksahu5
                break;
            case '2':
                //Start MITS:33849 - bram4
                //window.location = "/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMSettingsPeriodically.aspx?TaskType=" + iTaskType + "&SubTaskValue=" + iSubTaskValue + "&ScheduleType=" + iScheduleType + "&TaskTypetxt=" + sTaskTypeText + "&SubTaskNametxt=" + sSubTaskNameText + "&ScheduleTypetxt=" + sScheduleTypeText + "&TaskName=" + escape(sTaskName) + "&IsDataIntegratorTask=" + arrIsDataIntegratorTask[iSelectedTask] + "&SystemModuleName=" + arrSystemModuleName[iSelectedTask]; //Mits 21854:Added escape for taskname
                window.location = "/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMSettingsPeriodically.aspx?TaskType=" + iTaskType + "&SubTaskValue=" + iSubTaskValue + "&ScheduleType=" + iScheduleType + "&TaskTypetxt=" + encodeURIComponent(sTaskTypeText) + "&SubTaskNametxt=" + encodeURIComponent(sSubTaskNameText) + "&ScheduleTypetxt=" + encodeURIComponent(sScheduleTypeText) + "&TaskName=" + encodeURIComponent(sTaskName) + "&IsDataIntegratorTask=" + arrIsDataIntegratorTask[iSelectedTask] + "&SystemModuleName=" + arrSystemModuleName[iSelectedTask]; //Mits 21854:Added escape for taskname
                //End MITS:33849 - bram4
                break;
            case '3':
                //window.location = "/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMSettingsWeekly.aspx?TaskType=" + iTaskType + "&ScheduleType=" + iScheduleType + "&TaskTypetxt=" + sTaskTypeText + "&ScheduleTypetxt=" + sScheduleTypeText + "&TaskName=" + escape(sTaskName) + "&IsDataIntegratorTask=" + arrIsDataIntegratorTask[iSelectedTask] + "&SystemModuleName=" + arrSystemModuleName[iSelectedTask]; //Mits 21854:Added escape for taskname //MITs 33827 ksahu5
                window.location = "/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMSettingsWeekly.aspx?TaskType=" + iTaskType + "&ScheduleType=" + iScheduleType + "&TaskTypetxt=" + sTaskTypeText + "&ScheduleTypetxt=" + encodeURIComponent(sScheduleTypeText) + "&TaskName=" + encodeURIComponent(sTaskName) + "&IsDataIntegratorTask=" + arrIsDataIntegratorTask[iSelectedTask] + "&SystemModuleName=" + arrSystemModuleName[iSelectedTask]; //Mits 21854:Added escape for taskname//MITS 33827 ksahu5
                break;
            case '4':
                //kverma6- MITS 33898 - start
                //window.location = "/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMSettingsMonthly.aspx?TaskType=" + iTaskType + "&ScheduleType=" + iScheduleType + "&TaskTypetxt=" + sTaskTypeText + "&ScheduleTypetxt=" + sScheduleTypeText + "&TaskName=" + escape(sTaskName) + "&IsDataIntegratorTask=" + arrIsDataIntegratorTask[iSelectedTask] + "&SystemModuleName=" + arrSystemModuleName[iSelectedTask]; //Mits 21854:Added escape for taskname
                window.location = "/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMSettingsMonthly.aspx?TaskType=" + iTaskType + "&ScheduleType=" + iScheduleType + "&TaskTypetxt=" + encodeURIComponent(sTaskTypeText) + "&ScheduleTypetxt=" + encodeURIComponent(sScheduleTypeText) + "&TaskName=" + escape(sTaskName) + "&IsDataIntegratorTask=" + arrIsDataIntegratorTask[iSelectedTask] + "&SystemModuleName=" + arrSystemModuleName[iSelectedTask];
                //kverma6- MITS 33898 - end
                break;
            case '5':
                //window.location = "/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMSettingsYearly.aspx?TaskType=" + iTaskType + "&ScheduleType=" + iScheduleType + "&TaskTypetxt=" + sTaskTypeText + "&ScheduleTypetxt=" + sScheduleTypeText + "&TaskName=" + escape(sTaskName) + "&IsDataIntegratorTask=" + arrIsDataIntegratorTask[iSelectedTask] + "&SystemModuleName=" + arrSystemModuleName[iSelectedTask]; //Mits 21854:Added escape for taskname//MITS 33829
                window.location = "/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMSettingsYearly.aspx?TaskType=" + iTaskType + "&ScheduleType=" + iScheduleType + "&TaskTypetxt=" + sTaskTypeText + "&ScheduleTypetxt=" + encodeURIComponent(sScheduleTypeText) + "&TaskName=" + encodeURIComponent(sTaskName) + "&IsDataIntegratorTask=" + arrIsDataIntegratorTask[iSelectedTask] + "&SystemModuleName=" + arrSystemModuleName[iSelectedTask]; //Mits 21854:Added escape for taskname//MITS 33829 Yukti
                break;
            case '6':
                document.forms[0].hdnaction.value='MovetoDirectoryTrig';
                break;
            default :
                break;
                
            
        }
    }
}

function GetDisabled() {
    
    if(document.forms[0].lstTaskType.value == "0")
    {
        document.forms[0].lstScheduleType.disabled = true;
    }
    else
    {
        document.forms[0].lstScheduleType.disabled = false;
    }
    //Mgaba2:R7: In case of History Tracking application ,dropdown for schedule type should be lstScheduleTypeForHistoryTracking else lstScheduleType                        
    if (document.forms[0].lstTaskType.options[document.forms[0].lstTaskType.selectedIndex].text == "History Tracking") {
        document.forms[0].lstScheduleType.style.display = 'none';
        document.forms[0].lstScheduleTypeForHistoryTracking.style.display = '';
        document.forms[0].lstSubTaskName.disabled = false; 
    }
    else {
        document.forms[0].lstScheduleType.style.display = '';
        document.forms[0].lstScheduleTypeForHistoryTracking.style.display = 'none';
        document.forms[0].lstSubTaskName.disabled = true; 
    }
    document.forms[0].hdnTaskType.value = document.forms[0].lstTaskType.options[document.forms[0].lstTaskType.selectedIndex].text;
    document.forms[0].txtTaskName.value = document.forms[0].lstTaskType.options[document.forms[0].lstTaskType.selectedIndex].text;
}

function onPageLoaded() {
   
    if(document.forms[0].lstTaskType.value == "0")
    {
        document.forms[0].lstScheduleType.disabled = true;
        //Mgaba2:R7: In case of History Tracking application ,dropdown for schedule type should be lstScheduleTypeForHistoryTracking else lstScheduleType                                
        document.forms[0].lstScheduleTypeForHistoryTracking.style.display = 'none';        
    }
    if (document.forms[0].lstTaskType.options[document.forms[0].lstTaskType.selectedIndex].text != "History Tracking") 
    {
        document.forms[0].lstSubTaskName.disabled = true;        
    }
}

function onPeriodicalLoaded(sMode)
{
    //Mgaba2:R7: In case of History Tracking application ,
    //Names of dropdown for schedule and interval type  are different
    if (document.forms[0].hdSystemModuleName.value == "HistoryTracking")
    {
        if (document.forms[0].lstScheduleTypeForHistoryTracking != null && document.forms[0].hdnScheduleTypeId != null)
        {
               document.forms[0].lstScheduleTypeForHistoryTracking.value = document.forms[0].hdnScheduleTypeId.value;
        }       
        
        if (document.forms[0].ddlIntervalTypeForHistTracking != null &&  document.forms[0].hdnIntervalTypeId != null)
        {
                document.forms[0].ddlIntervalTypeForHistTracking.value = document.forms[0].hdnIntervalTypeId.value;
        }       
    }
    else
    {
        if (document.forms[0].ScheduleType != null && document.forms[0].hdnScheduleTypeId != null)
        {
            document.forms[0].ScheduleType.value = document.forms[0].hdnScheduleTypeId.value;
        }
        
        if (document.forms[0].IntervalType != null && document.forms[0].hdnIntervalTypeId != null)
        {
            document.forms[0].IntervalType.value = document.forms[0].hdnIntervalTypeId.value;
        }        
    }

    
//    if(document.forms[0].ScheduleType != null && document.forms[0].hdnScheduleTypeId != null)
//    {
//        document.forms[0].ScheduleType.value = document.forms[0].hdnScheduleTypeId.value
//    }
    CheckTaskParams(sMode);
    PerformDataIntegratorSettings();
    if(document.forms[0].hdnsaved.value == "true")
    {
        window.location = "/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMScheduledView.aspx";
    }
}

function SaveOneTimeSettings() {
    //debugger;
    if(!ValidateDateTime(document.forms[0].txtDate, document.forms[0].txtTime))
    {
        return false;
    }
    var sSystemModuleName = document.forms[0].hdSystemModuleName.value;
    if (sSystemModuleName == 'PrintCheckBatch') {
        var grid = document.getElementById('GridView1');
        if (grid == undefined) {
            //alert("Please select at least one print check parameter");jira-26
            alert(TMSettingValidations.ValidPrintcheckParams); //jira-26
            
            return false;
        }
    }
    // akaushik5 Added for MITS 36381 Starts
    if (sSystemModuleName == 'Resbal') {
        if (!SetResbalParameters()) {
            return false;
        }
    }
    // akaushik5 Added for MITS 36381 Ends
    var sTaskType = document.getElementById("hdTaskType").value;
    switch(sTaskType)
	{
		case '1':
		    break;
		case '2':
		    if (document.getElementById("chkZBFinHist_Param").checked)
		    {
		        if (document.forms[0].optClaimBasedFinHist.checked)
	                document.forms[0].hdUserArguments.value = "-zerobasedfinhist zerobasedcrit=claim";
                else if (document.forms[0].optEventBasedFinHist.checked)
                    document.forms[0].hdUserArguments.value = "-zerobasedfinhist zerobasedcrit=event";
		    }
		    if(document.getElementById("chkRecFinHist_Param").checked)
		    {
		        if(document.forms[0].hdUserArguments.value != null && document.forms[0].hdUserArguments.value != "")
		        {
	                document.forms[0].hdUserArguments.value = document.forms[0].hdUserArguments.value + " -full";
	            }
	            else
	            {
	                document.forms[0].hdUserArguments.value = "-full";
	            }
		    }
            // akaushik5 Commented for MITS 33420 Starts
		    //if(document.getElementById("chkCreateLog_Param").checked)
		    //{
		    //    if(document.forms[0].hdUserArguments.value != null && document.forms[0].hdUserArguments.value != "")
		    //    {
	        //        document.forms[0].hdUserArguments.value = document.forms[0].hdUserArguments.value + " -log";
	        //    }
	        //    else
	        //    {
	        //        document.forms[0].hdUserArguments.value = "-log";
	        //    }
		    //}
		    // akaushik5 Commented for MITS 33420 Ends
		    break;
		case '3':
		    document.forms[0].hdUserArguments.value = document.forms[0].BillOption.value;
		    break;
		  
		default:
		    if (document.forms[0].hdSystemModuleName.value == "ProcessOverDueDiary") {
		        if (document.forms[0].tbNoOfOverDueDays.value == "") {
                     alert(TMSettingValidations.ValidOverDueDays);
		            return false;
		        }
		        if (!document.getElementById("chkSysDiary").checked && !document.getElementById("chkEmailNotify").checked && !document.getElementById("chkBoth").checked) {
                   alert(TMSettingValidations.ValidNotificationType);
		            return false;
		        }

		        if (document.getElementById("chkAdd_Param").checked && Trim(document.getElementById("txtArgs").value) == "") {
                    alert(TMSettingValidations.ValidArgument);
		            document.getElementById("txtArgs").focus();
		            return false;
		        }
		    }
            if (document.getElementById("chkAdd_Param").checked && Trim(document.getElementById("txtArgs").value) == "") {
                alert(TMSettingValidations.ValidArgument);
                document.getElementById("txtArgs").focus();
                return false;
            }
           //JIRA RMA-4606 nshah28 start
            if (document.forms[0].hdSystemModuleName.value == "CurrencyExchangeInterface") {
                if (!SaveSettingsForCurrencyExchangeInterface()) {
                    return false;
                }
            }
            //JIRA RMA-4606 nshah28 end
    }



	document.forms[0].hdnaction.value = 'SaveOneTimeSettings';
	document.forms[0].submit();

    //OnCancel();
}

function SavePeriodicalSettings() {
    if(!ValidateDateTime(document.forms[0].txtDate, document.forms[0].txtTime))
    {
        return false;
    }
    
    //Mgaba2:R7: In case of History Tracking application 
    //Names of dropdown for schedule and interval type  are different
    var sSystemModuleName = document.forms[0].hdSystemModuleName.value;
    if (sSystemModuleName == 'PrintCheckBatch') {
        var grid = document.getElementById('GridView1');
        if (grid == undefined) {
             alert(TMSettingValidations.ValidPrintcheckParams);
            return false;
        }
    }
    // akaushik5 Added for MITS 36381 Starts
    if (sSystemModuleName == 'Resbal') {
        if (!SetResbalParameters()) {
            return false;
        }
    }
    // akaushik5 Added for MITS 36381 Ends
    if (sSystemModuleName == 'HistoryTracking') {
        if (document.forms[0].ddlvalTypeForHistTracking.value == "") {
             alert(TMSettingValidations.ValidIntervalType);
             document.forms[0].ddlvalTypeForHistTracking.focus();//dvatsa

            return false;
        }
    }
    else if (document.forms[0].IntervalType.value == "") {
        alert(TMSettingValidations.ValidIntervalType);
        document.forms[0].IntervalType.focus();
        return false;
    }
    if (document.forms[0].Interval.value == "" || document.forms[0].Interval.value == "0")
    {
        document.forms[0].Interval.focus();
         alert(TMSettingValidations.ValidNumericValueForInterval);
        return false;
    }
        
    var sTaskType = document.getElementById("hdTaskType").value;
    
	switch(sTaskType)
	{
		case '1':
		    break;
		case '2':
		    if (document.getElementById("chkZBFinHist_Param").checked)
		    {
		        if (document.forms[0].optClaimBasedFinHist.checked)
	                document.forms[0].hdUserArguments.value = "-zerobasedfinhist zerobasedcrit=claim";
                else if (document.forms[0].optEventBasedFinHist.checked)
                    document.forms[0].hdUserArguments.value = "-zerobasedfinhist zerobasedcrit=event";
		    }
		    if(document.getElementById("chkRecFinHist_Param").checked)
		    {
		        if(document.forms[0].hdUserArguments.value != null && document.forms[0].hdUserArguments.value != "")
		        {
	                document.forms[0].hdUserArguments.value = document.forms[0].hdUserArguments.value + " -full";
	            }
	            else
	            {
	                document.forms[0].hdUserArguments.value = "-full";
	            }
		    }
		    // akaushik5 Commented for MITS 33420 Starts
		    //if(document.getElementById("chkCreateLog_Param").checked)
		    //{
		    //    if(document.forms[0].hdUserArguments.value != null && document.forms[0].hdUserArguments.value != "")
		    //    {
	        //        document.forms[0].hdUserArguments.value = document.forms[0].hdUserArguments.value + " -log";
	        //    }
	        //    else
	        //    {
	        //        document.forms[0].hdUserArguments.value = "-log";
	        //    }
		    //}
		    // akaushik5 Commented for MITS 33420 Ends
		    break;
		case '3':
		    document.forms[0].hdUserArguments.value = document.forms[0].BillOption.value;
		    break;

		default:
		    if (document.forms[0].hdSystemModuleName.value == "ProcessOverDueDiary") {
		        if (document.forms[0].tbNoOfOverDueDays.value == "") {
		            alert(TMSettingValidations.ValidOverDueDays);
		            return false;
		        }
		        if (!document.getElementById("chkSysDiary").checked && !document.getElementById("chkEmailNotify").checked && !document.getElementById("chkBoth").checked) {
		            alert(TMSettingValidations.ValidNotificationType);
		            return false;
		        }

		        if (document.getElementById("chkAdd_Param").checked && Trim(document.getElementById("txtArgs").value) == "") {
		            alert(TMSettingValidations.ValidArgument);
		            document.getElementById("txtArgs").focus();
		            return false;
		        }
		    }
            //JIRA RMA-4606 nshah28 start
            if (document.forms[0].hdSystemModuleName.value == "CurrencyExchangeInterface") {
                if (!SaveSettingsForCurrencyExchangeInterface()) {
                    return false;
                }
            }
            //JIRA RMA-4606 nshah28 end
    }
//MGaba2:R7:In case of History Tracking,Name of DropDown list is different
    if (sSystemModuleName == 'HistoryTracking') {
        document.forms[0].hdnIntervalTypeId.value = document.forms[0].ddlvalTypeForHistTracking.value;//dvatsa
    }
    else {
        document.forms[0].hdnIntervalTypeId.value = document.forms[0].IntervalType.value;
    }

    document.forms[0].hdnaction.value='SavePeriodicalSettings';
    document.forms[0].submit();

   // OnCancel();
    return false;
}

function SaveWeeklySettings() {
    
    var error;
    if(!ValidateDateTime(document.forms[0].txtDate, document.forms[0].txtTime))
    {
        error = "true"
    }
    else if(!document.forms[0].chkMon_Run.checked && !document.forms[0].chkTue_Run.checked && !document.forms[0].chkWed_Run.checked &&
    !document.forms[0].chkThu_Run.checked && !document.forms[0].chkFri_Run.checked && !document.forms[0].chkSat_Run.checked &&
    !document.forms[0].chkSun_Run.checked)
    {
        error = "true"
        alert(TMSettingValidations.ValidDayToRunTask);
    }

    var sSystemModuleName = document.forms[0].hdSystemModuleName.value;
    if (sSystemModuleName == 'PrintCheckBatch') {
        var grid = document.getElementById('GridView1');
        if (grid == undefined) {
            alert(TMSettingValidations.ValidPrintcheckParams);
            return false;
        }
    }
    // akaushik5 Added for MITS 36381 Starts
    if (sSystemModuleName == 'Resbal') {
        if (!SetResbalParameters()) {
            return false;
        }
    }
    // akaushik5 Added for MITS 36381 Ends    
    var sTaskType = document.getElementById("hdTaskType").value;
    
	switch(sTaskType)
	{
		case '1':
		    break;
		case '2':
		    if (document.getElementById("chkZBFinHist_Param").checked)
		    {
		        if (document.forms[0].optClaimBasedFinHist.checked)
	                document.forms[0].hdUserArguments.value = "-zerobasedfinhist zerobasedcrit=claim";
                else if (document.forms[0].optEventBasedFinHist.checked)
                    document.forms[0].hdUserArguments.value = "-zerobasedfinhist zerobasedcrit=event";
		    }
		    if(document.getElementById("chkRecFinHist_Param").checked)
		    {
		        if(document.forms[0].hdUserArguments.value != null && document.forms[0].hdUserArguments.value != "")
		        {
	                document.forms[0].hdUserArguments.value = document.forms[0].hdUserArguments.value + " -full";
	            }
	            else
	            {
	                document.forms[0].hdUserArguments.value = "-full";
	            }
		    }
		    // akaushik5 Commented for MITS 33420 Starts
		    //if(document.getElementById("chkCreateLog_Param").checked)
		    //{
		    //    if(document.forms[0].hdUserArguments.value != null && document.forms[0].hdUserArguments.value != "")
		    //    {
	        //        document.forms[0].hdUserArguments.value = document.forms[0].hdUserArguments.value + " -log";
	        //    }
	        //    else
	        //    {
	        //        document.forms[0].hdUserArguments.value = "-log";
	        //    }
		    //}
		    // akaushik5 Commented for MITS 33420 Ends
		    break;
		case '3':
		    document.forms[0].hdUserArguments.value = document.forms[0].BillOption.value;
		    break;


		default:
		    if (document.forms[0].hdSystemModuleName.value == "ProcessOverDueDiary") {
		        if (document.forms[0].tbNoOfOverDueDays.value == "") {
		             alert(TMSettingValidations.ValidOverDueDays);
		            return false;
		        }
		        if (!document.getElementById("chkSysDiary").checked && !document.getElementById("chkEmailNotify").checked && !document.getElementById("chkBoth").checked) {
		           alert(TMSettingValidations.ValidNotificationType);
		            return false;
		        }

		        if (document.getElementById("chkAdd_Param").checked && Trim(document.getElementById("txtArgs").value) == "") {
		            alert(TMSettingValidations.ValidArgument);
		            document.getElementById("txtArgs").focus();
		            return false;
		        }
		    }
            if (document.getElementById("chkAdd_Param").checked && Trim(document.getElementById("txtArgs").value) == "") {
                alert(TMSettingValidations.ValidArgument);
                document.getElementById("txtArgs").focus();
                error = "true"
            }
            //JIRA RMA-4606 nshah28 start
            if (document.forms[0].hdSystemModuleName.value == "CurrencyExchangeInterface") {
                if (!SaveSettingsForCurrencyExchangeInterface()) {
                    return false;
                }
            }
            //JIRA RMA-4606 nshah28 end
    }
    if(error == "true")
    {
        return false;
    }

    document.forms[0].hdnaction.value='SaveWeeklySettings';
    document.forms[0].submit();

  //  OnCancel();
    return false;
}

function SaveYearlySettings() {
    
    var error;
    if(!ValidateDateTime(document.forms[0].txtDate, document.forms[0].txtTime))
    {
        error = "true"
    }
    else if(!document.forms[0].chkJan_Run.checked && !document.forms[0].chkFeb_Run.checked && !document.forms[0].chkMar_Run.checked &&
    !document.forms[0].chkApr_Run.checked && !document.forms[0].chkMay_Run.checked && !document.forms[0].chkJun_Run.checked &&
    !document.forms[0].chkJul_Run.checked && !document.forms[0].chkAug_Run.checked && !document.forms[0].chkSep_Run.checked &&
    !document.forms[0].chkOct_Run.checked && !document.forms[0].chkNov_Run.checked && !document.forms[0].chkDec_Run.checked)
    {
        error = "true"
        alert(TMSettingValidations.ValidMonthToRunTask);
    }
    var sSystemModuleName = document.forms[0].hdSystemModuleName.value;
    if (sSystemModuleName == 'PrintCheckBatch') {
        var grid = document.getElementById('GridView1');
        if (grid == undefined) {
            alert(TMSettingValidations.ValidPrintcheckParams);
            return false;
        }
    }
    // akaushik5 Added for MITS 36381 Starts
    if (sSystemModuleName == 'Resbal') {
        if (!SetResbalParameters()) {
            return false;
        }
    }
    // akaushik5 Added for MITS 36381 Ends
    var sTaskType = document.getElementById("hdTaskType").value;
    
	switch(sTaskType)
	{
		case '1':
		    break;
		case '2':
		    if (document.getElementById("chkZBFinHist_Param").checked)
		    {
		        if (document.forms[0].optClaimBasedFinHist.checked)
	                document.forms[0].hdUserArguments.value = "-zerobasedfinhist zerobasedcrit=claim";
                else if (document.forms[0].optEventBasedFinHist.checked)
                    document.forms[0].hdUserArguments.value = "-zerobasedfinhist zerobasedcrit=event";
		    }
		    if(document.getElementById("chkRecFinHist_Param").checked)
		    {
		        if(document.forms[0].hdUserArguments.value != null && document.forms[0].hdUserArguments.value != "")
		        {
	                document.forms[0].hdUserArguments.value = document.forms[0].hdUserArguments.value + " -full";
	            }
	            else
	            {
	                document.forms[0].hdUserArguments.value = "-full";
	            }
		    }
		    // akaushik5 Commented for MITS 33420 Starts
		    //if(document.getElementById("chkCreateLog_Param").checked)
		    //{
		    //    if(document.forms[0].hdUserArguments.value != null && document.forms[0].hdUserArguments.value != "")
		    //    {
	        //        document.forms[0].hdUserArguments.value = document.forms[0].hdUserArguments.value + " -log";
	        //    }
	        //    else
	        //    {
	        //        document.forms[0].hdUserArguments.value = "-log";
	        //    }
		    //}
		    // akaushik5 Commented for MITS 33420 Ends
		    break;
		case '3':
		    document.forms[0].hdUserArguments.value = document.forms[0].BillOption.value;
		    break;

		default:
		    if (document.forms[0].hdSystemModuleName.value == "ProcessOverDueDiary") {
		        if (document.forms[0].tbNoOfOverDueDays.value == "") {
		            alert(TMSettingValidations.ValidOverDueDays);
		            return false;
		        }
		        if (!document.getElementById("chkSysDiary").checked && !document.getElementById("chkEmailNotify").checked && !document.getElementById("chkBoth").checked) {
		            alert(TMSettingValidations.ValidNotificationType);
		            return false;
		        }

		        if (document.getElementById("chkAdd_Param").checked && Trim(document.getElementById("txtArgs").value) == "") {
		            alert(TMSettingValidations.ValidArgument);
		            document.getElementById("txtArgs").focus();
		            return false;
		        }
		    }
            if(document.getElementById("chkAdd_Param").checked && Trim(document.getElementById("txtArgs").value) == "")
            {
                error = "true"
                alert(TMSettingValidations.ValidArgument);
                document.getElementById("txtArgs").focus();
            }
            //JIRA RMA-4606 nshah28 start
            if (document.forms[0].hdSystemModuleName.value == "CurrencyExchangeInterface") {
                if (!SaveSettingsForCurrencyExchangeInterface()) {
                    return false;
                }
            }
            //JIRA RMA-4606 nshah28 end
    }
    
    if(error == "true")
    {
        return false;
    }

    document.forms[0].hdnaction.value='SaveYearlySettings';
    document.forms[0].submit();

  //  OnCancel();
    return false;
}

function SaveMonthlySettings() {
    if (document.forms[0].Month.value == "") {
        alert(TMSettingValidations.SelectMonth);
        document.forms[0].Month.focus();
        return false;
    }
    else if (document.forms[0].DayOfMonth.value == "") {
        alert(TMSettingValidations.EnterDayOfMonth);
        document.forms[0].DayOfMonth.focus();
        return false;
    }
    else if (document.forms[0].txtTime.value == "") {
        alert(TMSettingValidations.EnterTime);
        document.forms[0].txtTime.focus();
        return false;
    }

    var sSystemModuleName = document.forms[0].hdSystemModuleName.value;
    if (sSystemModuleName == 'PrintCheckBatch') {
        var grid = document.getElementById('GridView1');
        if (grid == undefined) {
            alert(TMSettingValidations.ValidPrintcheckParams);
            return false;
        }
    }
    // akaushik5 Added for MITS 36381 Starts
    if (sSystemModuleName == 'Resbal') {
        if (!SetResbalParameters()) {
            return false;
        }
    }
    // akaushik5 Added for MITS 36381 Ends    
    var sTaskType = document.getElementById("hdTaskType").value;
    
	switch(sTaskType)
	{
		case '1':
		    break;
		case '2':
		    if (document.getElementById("chkZBFinHist_Param").checked)
		    {
		        if (document.forms[0].optClaimBasedFinHist.checked)
	                document.forms[0].hdUserArguments.value = "-zerobasedfinhist zerobasedcrit=claim";
                else if (document.forms[0].optEventBasedFinHist.checked)
                    document.forms[0].hdUserArguments.value = "-zerobasedfinhist zerobasedcrit=event";
		    }
		    if(document.getElementById("chkRecFinHist_Param").checked)
		    {
		        if(document.forms[0].hdUserArguments.value != null && document.forms[0].hdUserArguments.value != "")
		        {
	                document.forms[0].hdUserArguments.value = document.forms[0].hdUserArguments.value + " -full";
	            }
	            else
	            {
	                document.forms[0].hdUserArguments.value = "-full";
	            }
		    }
		    // akaushik5 Commented for MITS 33420 Starts
		    //if(document.getElementById("chkCreateLog_Param").checked)
		    //{
		    //    if(document.forms[0].hdUserArguments.value != null && document.forms[0].hdUserArguments.value != "")
		    //    {
	        //        document.forms[0].hdUserArguments.value = document.forms[0].hdUserArguments.value + " -log";
	        //    }
	        //    else
	        //    {
	        //        document.forms[0].hdUserArguments.value = "-log";
	        //    }
		    //}
		    // akaushik5 Commented for MITS 33420 Ends
		    break;
		case '3':
		    document.forms[0].hdUserArguments.value = document.forms[0].BillOption.value;
		    break;

		default:
		    if (document.forms[0].hdSystemModuleName.value == "ProcessOverDueDiary") {
		        if (document.forms[0].tbNoOfOverDueDays.value == "") {
		            alert(TMSettingValidations.ValidOverDueDays);
		            return false;
		        }
		        if (!document.getElementById("chkSysDiary").checked && !document.getElementById("chkEmailNotify").checked && !document.getElementById("chkBoth").checked) {
		            alert(TMSettingValidations.ValidNotificationType);
		            return false;
		        }

		        if (document.getElementById("chkAdd_Param").checked && Trim(document.getElementById("txtArgs").value) == "") {
		            alert(TMSettingValidations.ValidArgument);
		            document.getElementById("txtArgs").focus();
		            return false;
		        }
		    }
            if (document.getElementById("chkAdd_Param").checked && Trim(document.getElementById("txtArgs").value) == "") {
                alert(TMSettingValidations.ValidArgument);
                document.getElementById("txtArgs").focus();
                return false;
            }
            //JIRA RMA-4606 nshah28 start
            if (document.forms[0].hdSystemModuleName.value == "CurrencyExchangeInterface") {
                if (!SaveSettingsForCurrencyExchangeInterface()) {
                    return false;
                }
            }
            //JIRA RMA-4606 nshah28 end
    }

    document.forms[0].hdnaction.value = "SaveMonthlySettings";
    document.forms[0].submit();

    //OnCancel();
    return false;
}

//JIRA RMA-4606 nshah28 start
function SaveSettingsForCurrencyExchangeInterface() {
    var sReturn = true;
    if (document.getElementById("rdoSharedFilePath") != null && document.getElementById("rdoFTP") != null) {

        if (document.getElementById("rdoSharedFilePath").checked) {
            if (Trim(document.getElementById("txtFilePath").value) == "") {
                alert(TMSettingValidations.validateFilePath);
                document.getElementById("txtFilePath").focus();
                sReturn = false;
                return sReturn
            }
            else if (Trim(document.getElementById("txtFileName").value) == "") {
                alert(TMSettingValidations.validateFileName);
                document.getElementById("txtFileName").focus();
                sReturn = false;
                return sReturn
            }
        }

        //For FTP
        else if (document.getElementById("rdoFTP").checked) {
            if ( Trim(document.getElementById("txtFTPServer").value) == "") {
                alert(TMSettingValidations.validateFTPServername);
                document.getElementById("txtFTPServer").focus();
                sReturn = false;
                return sReturn
            }
            else if (Trim(document.getElementById("txtFTPUserName").value) == "") {
               alert(TMSettingValidations.validateFTPUsername);
                document.getElementById("txtFTPUserName").focus();
                sReturn = false;
                return sReturn
            }
            else if (Trim(document.getElementById("txtFTPPass").value) == "") {
               alert(TMSettingValidations.validateFTPPassword);
                document.getElementById("txtFTPPass").focus();
                sReturn = false;
                return sReturn
            }
            else if (Trim(document.getElementById("txtFTPFilePath").value) == "") {
                alert(TMSettingValidations.validateFilePath);
                document.getElementById("txtFTPFilePath").focus();
                sReturn = false;
                return sReturn
            }
            else if (Trim(document.getElementById("txtFTPFileName").value) == "") {
               alert(TMSettingValidations.validateFileName);
                document.getElementById("txtFTPFileName").focus();
                sReturn = false;
                return sReturn
            }
        }

    }
    return sReturn;
}
//JIRA RMA-4606 nshah28 end
function ValidateDateTime(ctrlDate, ctrlTime) {
    
    if (Trim(ctrlDate.value) == "") {
        alert(TMSettingValidations.EnterDate);
        ctrlDate.value = "";
        ctrlDate.focus();
        return false;
    }
    if (Trim(ctrlTime.value) == "") {
        alert(TMSettingValidations.EnterTime);
        ctrlTime.value = "";
        ctrlTime.focus();
        return false;
    }
    var dtNow = new Date();
    //vkumar258 - RMA-6037 - Starts

    //Start ttumula2 RMA-9397
   // debugger;
    var target = $(document.forms[0].txtDate);
    var inst = $.datepicker._getInst(target[0]);
    fdate = inst.selectedDay;
    fmonth = inst.selectedMonth;
    fyear = inst.selectedYear;
    var effDate = new Date();
    if (fdate != 0 || fmonth != 0 || fyear != 0) {

        effDate.setFullYear(fyear, fmonth, fdate);
    }
    else if (document.getElementById("hdnCulture") != null) {
        if (document.getElementById("hdnCulture").value != "") {
            effDate = $.datepicker.parseDate("yymmdd", document.getElementById("hdnCulture").value);
        }
    }
    else {
        alert(TMSettingValidations.ValidDate);
        ctrlDate.value = "";
        ctrlDate.focus();
        return false;
    }

    effDate.setHours(12);
    effDate.setMinutes(00);
    effDate.setSeconds(00);
    effDate.setMilliseconds(00);
    dtNow.setHours(12);
    dtNow.setMinutes(00);
    dtNow.setSeconds(00);
    effDate.setMilliseconds(00);
    //End ttumula2 RMA-9397

    //vkumar258 - RMA-6037 - end


    var dtDateOnly = Date.parse(dtNow.toDateString());
     var dtEntered = Date.parse(effDate.toDateString());   //ttumula2 RMA-9397
    //var dtEntered = Date.parse(ctrlDate.value);
     var sTime = ctrlTime.value;
     dtNow = new Date();
    if(dtDateOnly > dtEntered)
    {
        //alert("Date entered should be equal to or greater than today's date.");
        //debugger;
        alert(TMSettingValidations.ValidDate);
        ctrlDate.value = "";
        ctrlDate.focus();
        return false;
    }
    else if(dtDateOnly == dtEntered)
    {
        var hrNow = dtNow.getHours();
        var minNow = dtNow.getMinutes();
        var sArr=ctrlTime.value.split(":");
        sArr[0]=new String(parseInt(sArr[0],10));
	    sArr[1]=new String(parseInt(sArr[1],10));
        //MITS - 11739
        //if (sTime.toLowerCase().indexOf("pm") >= 0) { //RMA-10903(bug of RMA-4606) commenting this and including below line
        if (sTime.toLowerCase().indexOf("pm") >= 0 && parseInt(sArr[0], 10) != 12) {
	        hrNow = hrNow - 12;
	    }
        if (hrNow > parseInt(sArr[0], 10)) {
            alert(TMSettingValidations.ValidTime);
            ctrlTime.value = "";
            ctrlTime.focus();
            return false;
        }
        else if (hrNow == parseInt(sArr[0], 10)) {
            if (minNow > parseInt(sArr[1], 10)) {
                alert(TMSettingValidations.ValidTime);
                ctrlTime.value = "";
                ctrlTime.focus();
                return false;
            }
        }
    }
    return true;
}

function Trim(sString) {
    
    while (sString.substring(0,1) == ' ')
    {
        sString = sString.substring(1, sString.length);
    }
    while (sString.substring(sString.length-1, sString.length) == ' ')
    {
        sString = sString.substring(0,sString.length-1);
    }
    return sString;
}

function RefreshTaskList() {
    var oRef = document.getElementById('hdnRefresh');
    if (oRef != null) {
        if (oRef.value == "true") {
            document.getElementById('hdnRefresh').value = "false";
            document.forms[0].hdnaction.value = "Refresh";
            document.forms[0].submit();
        }
    }
}

function AddSchedule() {
    
    window.location = "/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMSettings.aspx";
}

function OnTaskSelect() {
    var rowNum = "";
    var ctrl;
    var sSelectedId = "";
  
    var gridElementsRadio = document.getElementsByName('MyRadioButton');

    if (gridElementsRadio != null)
     {
        for (var i = 0; i < gridElementsRadio.length; i++)
         {
            var gridName = gridElementsRadio[i].name;
            if (gridName == "JobListGrid" && gridElementsRadio[i].checked)
             {
                // Get control
                sSelectedId = gridElementsRadio[i].value;
                break;
             }
         }
     }
	var sSelectedRowId = GetPositionForSelectedGridRow("JobListGrid", sSelectedId);
	//PJS , MITS 16370 - Modifed the id attribute
	//ctrl = self.document.getElementById(sSelectedRowId + "_lblLinkColumn0");

    //jramkumar for MITS 34691
    //if (sSelectedRowId.toString().length == 1)
	//    sSelectedRowId = '0' + sSelectedRowId;
    //ctrl = self.document.getElementById('JobListGrid_gvData_ctl' + sSelectedRowId + '_ctl00');    
	ctrl = self.document.getElementById('JobListGrid_gvData_ctl12_0');
	if (ctrl != null) {
	    if (ctrl.innerText == 'Running') {
	        document.forms[0].btnKill.disabled = false;
	    }
	    else {
	        document.forms[0].btnKill.disabled = true;
	    }
	}

}

function OnLoad()
{
    document.forms[0].hdnaction.value = "";
    document.forms[0].hdnSelectedRow.value = "";
    self.setTimeout('RefreshTaskList()',60000);
    document.forms[0].btnKill.disabled = true;
}

function KillTask() {

    // var gridElementsRadio = document.getElementsByName('MyRadioButton');
    var gridElementsRadio = document.forms[0].MyRadioButton; // aravi5 RMA-11850 Unable to Abort the scheduled job from task manager on chrome only 

    if (gridElementsRadio != null) {
        for (var i = 0; i < gridElementsRadio.length; i++) {
            var gridName = gridElementsRadio[i].name;
            if (gridName == "JobListGrid" && gridElementsRadio[i].checked) {
                // Get control
                sSelectedId = gridElementsRadio[i].value;
                break;
            }
        }
    }
    document.forms[0].hdnSelectedRow.value = sSelectedId; 
    document.forms[0].hdnaction.value = "KillTask";
    document.forms[0].submit();
}

function onTMLoaded() {
    
    CheckTaskParams('add');

    PerformDataIntegratorSettings();
    
    if(document.forms[0].hdnsaved.value == "true")
    {
        window.location = "/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMScheduledView.aspx";
    }
}
function onTMEditLoaded() {
    
    CheckTaskParams('edit');
    
    document.forms[0].ScheduleType.value = document.forms[0].hdnScheduleTypeId.value
    if(document.forms[0].hdnsaved.value == "true")
    {
        window.location = "/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMScheduledView.aspx";
    }
}

function CheckFinHistParams() {
    
    if (document.getElementById("chkZBFinHist_Param").checked)
    {
        document.getElementById("trZeroBasedOptions").style.display = '';
        if(!document.forms[0].optEventBasedFinHist.checked)
            document.forms[0].optClaimBasedFinHist.checked = true;
    }
    else
    {
        document.forms[0].hdUserArguments.value = '';
        document.getElementById("trZeroBasedOptions").style.display = 'none';
    }
}

function SelectClaimBasedFinHist() {
    
    document.forms[0].optEventBasedFinHist.checked = false;
}

function SelectEventBasedFinHist() {
    
    document.forms[0].optClaimBasedFinHist.checked = false;
}

function CheckTaskParams(sMode) {
    
    var sTaskType = document.getElementById("hdTaskType").value;

    var sSystemModuleName = document.forms[0].hdSystemModuleName.value;
//MGaba2:R7:In case of History Tracking Additional Parameters are not required
    // akaushik5 Changed for MITS 36381 Starts
    //if (sSystemModuleName == 'BES' || sSystemModuleName == 'HistoryTracking' || sSystemModuleName == "ClaimBalancing")
    if (sSystemModuleName == "Resbal") {
        DisplayResbalOptions(null);
    }
    if (sSystemModuleName == 'BES' || sSystemModuleName == 'HistoryTracking' || sSystemModuleName == "ClaimBalancing" || sSystemModuleName == "Resbal")
    // akaushik5 Changed for MITS 36381 Ends
    {
        document.getElementById("tdAdditionalParams").style.display = 'none';
        document.getElementById("chkAdd_Param").style.display = 'none';
        document.getElementById("tdArgLabel").style.display = 'none';
        document.getElementById("txtArgs").style.display = 'none';
        return;
    }
	switch(sTaskType)
	{
		case '1':
		case '3':
            document.getElementById("tdAdditionalParams").style.display = 'none';
            document.getElementById("chkAdd_Param").style.display = 'none';
            document.getElementById("tdArgLabel").style.display = 'none';
            document.getElementById("txtArgs").style.display = 'none';
            break;
		case '2':
            document.getElementById("tdAdditionalParams").style.display = 'none';
            document.getElementById("chkAdd_Param").style.display = 'none';
            document.getElementById("tdArgLabel").style.display = 'none';
            document.getElementById("txtArgs").style.display = 'none';
            
            if(sMode == 'edit')
            {
                if(!document.forms[0].chkZBFinHist_Param.checked)
                    document.getElementById("trZeroBasedOptions").style.display = 'none';
                
            }
            else
            {
                document.getElementById("trZeroBasedOptions").style.display = 'none';
            }
		    break;
		default:
            if(!document.getElementById("chkAdd_Param").checked)
            {
                document.getElementById("tdArgLabel").style.display = 'none';
                document.getElementById("txtArgs").style.display = 'none';
            }
            else
            {
                document.getElementById("tdArgLabel").style.display = '';
                document.getElementById("txtArgs").style.display = '';
            }
	}	
}

function ScheduleTypeChanged() {
//Mgaba2:R7: In case of History Tracking Schedule Type 
    var sScheduleType = "";

    if (document.forms[0].hdSystemModuleName != null && document.forms[0].hdSystemModuleName.value == "HistoryTracking")
        sScheduleType = document.forms[0].lstScheduleTypeForHistoryTracking.value;
    else
        sScheduleType = document.forms[0].ScheduleType.value;
    
//    switch (document.forms[0].ScheduleType.value) {
    switch (sScheduleType) {
        case '1':
            window.location = '/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMSettingsOneTime.aspx?ScheduleId=' + document.forms[0].hdnScheduleId.value + '&action=Edit';
            break;
        case '2':
            window.location = '/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMSettingsPeriodically.aspx?ScheduleId=' + document.forms[0].hdnScheduleId.value + '&action=Edit';
            break;
        case '3':
            window.location = '/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMSettingsWeekly.aspx?ScheduleId=' + document.forms[0].hdnScheduleId.value + '&action=Edit';
            break;
        case '4':
            window.location = '/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMSettingsMonthly.aspx?ScheduleId=' + document.forms[0].hdnScheduleId.value + '&action=Edit';
            break;
        case '5':
            window.location = '/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMSettingsYearly.aspx?ScheduleId=' + document.forms[0].hdnScheduleId.value + '&action=Edit';
            break;
        case '6':
            window.location = 'home?pg=riskmaster/RMUtilities/EditSettingsOneTime&ScheduleId=' + document.forms[0].hdnScheduleId.value;;
            break;
        default:
            break;
    }
    
}

function OnCancel() {
    
    //for any page opening from scheduled view screen
    window.location = '/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMScheduledView.aspx';
}

function Move() {
    
    //for any page opening from scheduled view screen
    window.location = '/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/TMSettings.aspx';
}
function AddParams(chkAddParm) {
    
    if(chkAddParm.checked)
    {
        document.getElementById("tdArgLabel").style.display = '';
        document.getElementById("txtArgs").style.display = '';
    }
    else
    {
        document.getElementById("tdArgLabel").style.display = 'none';
        document.getElementById("txtArgs").value = "";
        document.getElementById("txtArgs").style.display = 'none';
    }
}

function PerformDataIntegratorSettings() {
   
        //Condition added by Ankit Gupta MITS# 36997 PMC GAP06 - agupta298
    //if (document.forms[0].hdIsDataIntegratorTask.value == "-1" && document.forms[0].hdSystemModuleName.value != 'PrintCheckBatch' && document.forms[0].hdSystemModuleName.value != 'PolicySystemUpdate' && document.forms[0].hdSystemModuleName.value != 'ClaimBalancing' || document.forms[0].hdSystemModuleName.value == 'DCISettings')
    //if (document.forms[0].hdIsDataIntegratorTask.value == "-1" && document.forms[0].hdSystemModuleName.value != 'PrintCheckBatch' && document.forms[0].hdSystemModuleName.value != 'PolicySystemUpdate' && document.forms[0].hdSystemModuleName.value != 'ClaimBalancing')
    //JIRA RMA-4606 nshah28 condtion added for Currency Exchange
    if (document.forms[0].hdIsDataIntegratorTask.value == "-1" && document.forms[0].hdSystemModuleName.value != 'PrintCheckBatch' && document.forms[0].hdSystemModuleName.value != 'PolicySystemUpdate' && document.forms[0].hdSystemModuleName.value != 'ClaimBalancing' && document.forms[0].hdSystemModuleName.value != 'CurrencyExchangeInterface' || document.forms[0].hdSystemModuleName.value == 'DCISettings') {
            document.forms[0].btnSave.style.display = "none";
            document.getElementById("tdAdditionalParams").style.display = 'none';
            document.getElementById("chkAdd_Param").style.display = 'none';
            document.getElementById("tdArgLabel").style.display = 'none';
            document.getElementById("txtArgs").style.display = 'none';
        }
        else 
        {
            document.forms[0].btnOptionset.style.display = "none";
        }
    
}

function OpenOptionset(p_iScheduleTypeId)
{
    if(p_iScheduleTypeId == "4") // Monthly
    {
        if (document.forms[0].Month.value == "") {
            alert(TMSettingValidations.SelectMonth);
            document.forms[0].Month.focus();
            return false;
        }
        else if (document.forms[0].DayOfMonth.value == "") {
            alert(TMSettingValidations.EnterDayOfMonth);
            document.forms[0].DayOfMonth.focus();
            return false;
        }
        else if (document.forms[0].txtTime.value == "") {
            alert(TMSettingValidations.EnterTime);
            document.forms[0].txtTime.focus();
            return false;
        }    
    }
    else {
        if (!ValidateDateTime(document.forms[0].txtDate, document.forms[0].txtTime)) {
            return false;
        }
    }
}

function GetJobFiles(p_iJobId)
{
    window.open("/RiskmasterUI/UI/Utilities/ToolsDesigners/TaskManager/JobFiles.aspx?JobId="+p_iJobId,
	'InsufficientReserves', 'width=500,height=300' + ',top=' + (screen.availHeight - 290) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');

//    document.forms[0].hdnaction.value = "GetJobFiles";
//    document.forms[0].submit();
}

function dateLostFocus(sCtrlName) {
    var sDateSeparator;
    var iDayPos = 0, iMonthPos = 0;
    var d = new Date(1999, 11, 22);
    var s = d.toLocaleString();
    var sRet = "";
    var objFormElem = eval('document.forms[0].' + sCtrlName);
    var sDate = new String(objFormElem.value);

    //TR#2217 Pankaj 02/23/06	
    //Was creating problems for dates like 2/6/2006,which has 8 chars and thus was adding extra "/" and
    //making the sDate variable as 2//6//2006 creating more problems. Checking for "/" is a better option
    //for formatting. If no "/" exists only then do the "/" formating.
    //if(sDate.length == 8)
    if (sDate.indexOf('/') == -1) {
        sDate = sDate.substr(0, 2) + '/' + sDate.substr(2, 2) + '/' + sDate.substr(4, 4);
    }

    var iMonth = 0, iDay = 0, iYear = 0;
    var monthDays = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

    if (sDate == "") {
        return "";
    }

    iDayPos = s.indexOf("22");
    iMonthPos = s.indexOf("11");

    //if(IE4)
    //	sDateSeparator=s.charAt(iDayPos+2);
    //else

    sDateSeparator = "/";
    var sArr = sDate.split(sDateSeparator);

    if (sArr.length == 3) {
        sArr[0] = new String(parseInt(sArr[0], 10));
        sArr[1] = new String(parseInt(sArr[1], 10));
        sArr[2] = new String(parseInt(sArr[2], 10));

        //Convert potential 2-digit year to 4-digit year
        sArr[2] = Get4DigitYear(sArr[2]);

        // Classic leap year calculation
        if (((parseInt(sArr[2], 10) % 4 == 0) && (parseInt(sArr[2], 10) % 100 != 0)) || (parseInt(sArr[2], 10) % 400 == 0)) {
            monthDays[1] = 29;
        }

        if (iDayPos < iMonthPos) {
            // Date should be as dd/mm/yyyy
            if (parseInt(sArr[1], 10) < 1 || parseInt(sArr[1], 10) > 12 || parseInt(sArr[0], 10) < 0 || parseInt(sArr[0], 10) > monthDays[parseInt(sArr[1], 10) - 1]) {
                objFormElem.value = "";
            }
        }
        else {
            // Date is something like mm/dd/yyyy
            //if (parseInt(sArr[0], 10) < 1 || parseInt(sArr[0], 10) > 12 || parseInt(sArr[1], 10) < 0 || parseInt(sArr[1], 10) > monthDays[parseInt(sArr[0], 10) - 1])  Commented by csingh7 : MITS 14181
            if (parseInt(sArr[0], 10) < 1 || parseInt(sArr[0], 10) > 12 || parseInt(sArr[1], 10) < 1 || parseInt(sArr[1], 10) > monthDays[parseInt(sArr[0], 10) - 1]) { // Added by csingh7 : MITS 14181
                objFormElem.value = "";
            }
        }

        // Check the year
        if (parseInt(sArr[2], 10) < 10 || (sArr[2].length != 4 && sArr[2].length != 2)) {
            objFormElem.value = "";
        }

        // If date has been accepted
        if (objFormElem.value != "") {
            // Format the date
            if (sArr[0].length == 1) {
                sArr[0] = "0" + sArr[0];
            }
            if (sArr[1].length == 1) {
                sArr[1] = "0" + sArr[1];
            }
            if (sArr[2].length == 2) {
                sArr[2] = "20" + sArr[2];
            }
            if (iDayPos < iMonthPos) {
                objFormElem.value = formatRMDate(sArr[2] + sArr[1] + sArr[0]);
            }
            else {
                objFormElem.value = formatRMDate(sArr[2] + sArr[0] + sArr[1]);
            }
        }

        // Anjaneya : To make a final check if any character is not NaN - MITS 9373
        if (objFormElem != null && objFormElem.value != null && objFormElem.value != "") {
            var m = objFormElem.value.match("Na");
            if (m != null) {
                objFormElem.value = "";
            }

            var m = objFormElem.value.match("NN");
            if (m != null) {
                objFormElem.value = "";
            }
        }
    }
    else {
        objFormElem.value = "";
    }

    if (sCtrlName == 'birthdate' && objFormElem != null && sDate != "") {
        return calculateage(objFormElem);
    }

    return true;
}

//Convert 2-digit year to 4-digit year
function Get4DigitYear(sInputYear) {
    var sGuessedYear = sInputYear;
    var dCurrentDate = new Date();
    var iCurrentYear = dCurrentDate.getFullYear();
    var sCurrentYear = new String(iCurrentYear);

    //01/05/2010 Raman: Logic below failed for 2010 onwards
    //converting inputyear to 2 characters
    if (sInputYear.length == 1) {
        sInputYear = "0" + sInputYear;
    }
    
    //If the input year is not 4-digit year, get missing digit form current year
    //	if (sInputYear.length < 4)  Commented by csingh7 : MITS 14181
    if (sInputYear.length < 3)  // Added by csingh7 : MITS 14181
    {
        sGuessedYear = sCurrentYear.substr(0, 4 - sInputYear.length) + sInputYear;

        //If the guessed year is 20 years in the future, assume it's in the last centure
        var iGuessedYear = parseInt(sGuessedYear);
        if (iGuessedYear - iCurrentYear > 20) {
            iGuessedYear = iGuessedYear - 100;
            sGuessedYear = new String(iGuessedYear);
        }
    }

    return sGuessedYear;
}

//Vsoni5 : 04/05/2010 : MITS 17486: 
//Function added to Validate extension of Import file.

function validateFileType(sFilePath)
{
    if(sFilePath == null || sFilePath == ""){ // no file is selected skip validation. 
        return;
    }
    
    var allowedFileTypes = new Array(".txt");  // Add all the acceptable file extensions in this array.
    var sFileName = sFilePath.substr(sFilePath.lastIndexOf("\\")+1)
    var sTemp = sFileName.split(".");
    var sFileExt = ".";
    
    sFileExt = sFileExt + sTemp[sTemp.length-1].toLowerCase(); // Get the extension from file name and append '.' before it
    
    for (var iIndex = 0; iIndex < allowedFileTypes.length; iIndex++)
    {
        if( sFileExt == allowedFileTypes[iIndex] ){ // Check if File Extension is acceptable
            return true;            
        }
    }            
    alert(TMSettingValidations.ValidFileExtn1 + allowedFileTypes.join(" , ") + TMSettingValidations.ValidFileExtn2)
    return false;
} // EOF validateFileType

function  onCheckChanged(objCtrl)
{
    if (objCtrl.id=="chkSysDiary") {
        document.getElementById("chkBoth").checked = false;
        document.getElementById("chkEmailNotify").checked = false;
    }
    else if (objCtrl.id == "chkEmailNotify") {
        document.getElementById("chkSysDiary").checked = false;
        document.getElementById("chkBoth").checked = false;
    }
    else if (objCtrl.id=="chkBoth") {
        document.getElementById("chkSysDiary").checked = false;
        document.getElementById("chkEmailNotify").checked = false;
    }
}

function openGridWindowAddEdit(listname,mode) {
    document.forms[0].hdnRepeat.value = true;
    if (mode == 'Edit') {
        var inputs = document.getElementsByTagName("input");   //mozilla fix
        var SelectedFound = false;
        for (i = 0; i < inputs.length; i++) {
            if ((inputs[i].type == "radio") && (inputs[i].checked == true)) {
                inputname = inputs[i].name;
                if (listname == inputname) {
                    SelectedFound = true;
                }
            }
        }
        if (SelectedFound == false) {
            alert(TMSettingValidations.SelectRecordToDelete);
            return false;
        }
        window.open("CheckBatchPrintSettings.aspx?ID=" + document.forms[0].hdnId.value, "Settings", 'width=500,height=580,top=' + (screen.availHeight - 580) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=no,scrollbars=yes');
    }
    else if (mode == 'Add') {
        window.open("CheckBatchPrintSettings.aspx", "Settings", 'width=500,height=580,top=' + (screen.availHeight - 580) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=no,scrollbars=yes');
    }
}
function SelectGridRow(Id) {
    document.forms[0].hdnId.value = Id;
}

function GridForDeletion(listname) {
    
    var inputs = document.getElementsByTagName("input");   //mozilla fix
    var SelectedFound = false ;
    for (i = 0; i < inputs.length; i++) 
    {
        if ((inputs[i].type == "radio") && (inputs[i].checked == true)) 
        {
            inputname = inputs[i].name;
            if (listname == inputname) 
            {
                SelectedFound = true;
            }
        }
    }
    if (SelectedFound == false) {
        alert(TMSettingValidations.SelectRecordToDelete);
        return false;
    }
    else {
        
        document.forms[0].hdnDeteleSetting.value = true;
        document.forms[0].submit();
    }
}

// akaushik5 Added for MIST 36381 Starts
function SelectResbalOptions(selectedOption) {

    if (document.getElementById('optResbalAllClaims') != null) {
        document.getElementById('optResbalAllClaims').checked = false;
    }
    if (document.getElementById('optResbalSingleClaim') != null) {
        document.getElementById('optResbalSingleClaim').checked = false;
    }
    if (document.getElementById('optResbalOnClaimDate') != null) {
        document.getElementById('optResbalOnClaimDate').checked = false;
    }

    if (selectedOption != null) {
        selectedOption.checked = true;
    }

    if (document.getElementById('optResbalAllClaims') != null && document.getElementById('optResbalAllClaims').checked) {
        DisplayResbalOptions(null);
    }

    if (document.getElementById('optResbalSingleClaim') != null && document.getElementById('optResbalSingleClaim').checked) {
        DisplayResbalOptions(document.getElementById("trResbalSingleClaim"))
    }

    if (document.getElementById('optResbalOnClaimDate') != null && document.getElementById('optResbalOnClaimDate').checked) {
        DisplayResbalOptions(document.getElementById("trResbalClaimDate"))
    }
}

function DisplayResbalOptions(option) {
    if (document.getElementById("trResbalSingleClaim") != null) {
        document.getElementById("trResbalSingleClaim").style.display = 'none';


        if (document.getElementById('optResbalSingleClaim') != null && document.getElementById('optResbalSingleClaim').checked) {
            document.getElementById("trResbalSingleClaim").style.display = '';
        }
    }
    if (document.getElementById("trResbalClaimDate") != null) {
        document.getElementById("trResbalClaimDate").style.display = 'none';

        if (document.getElementById('optResbalOnClaimDate') != null && document.getElementById('optResbalOnClaimDate').checked) {
            document.getElementById("trResbalClaimDate").style.display = '';
        }
    }


    if (option != null) {
        option.style.display = '';
    }
}

function SetResbalParameters() {
    if (document.getElementById('optResbalAllClaims') != null && document.getElementById('optResbalAllClaims').checked) {
        document.getElementById('hdUserArguments').value = "-pIsSingleClaim = 0";
        return true;
    }
    else if (document.getElementById('optResbalSingleClaim') != null && document.getElementById('optResbalSingleClaim').checked) {
        if (document.getElementById('txtClaimNumber') != null) {
            var claimNumber = document.getElementById('txtClaimNumber').value;

            if (Trim(claimNumber) == '') {
                //alert('Please enter Claim Number.');
                alert(TMSettingValidations.ValidResbalClaimNumber);
                return false;
            }

            document.getElementById('hdUserArguments').value = "-pIsSingleClaim = 1 -pClaimNumber = " + claimNumber;
            return true;
        }
    }

    else if (document.getElementById('optResbalOnClaimDate') != null && document.getElementById('optResbalOnClaimDate').checked) {
        if (document.getElementById('txtResbalFromDate') != null) {
            var fromDate = document.getElementById('txtResbalFromDate').value;
            var toDate = document.getElementById('txtResbalToDate').value;

            var dtNow = new Date();
            var dtDateOnly = Date.parse(dtNow.toDateString());
            var dtEntered_fromDate = Date.parse(document.getElementById('txtResbalFromDate').value);
            var dtEntered_toDate = Date.parse(document.getElementById('txtResbalToDate').value);

            if (Trim(fromDate) == '') {
                //alert('Please enter From Date.');//ksahu5 ML change
                alert(TMSettingValidations.ValidResbalEnterFromDate);//ksahu5 ML change
                return false;
            }

            if (Trim(toDate) == '') {
                //alert('Please enter To Date.');//ksahu5 ML change
                alert(TMSettingValidations.ValidResbalEnterToDate);//ksahu5 ML change
                return false;
            }
            if (Trim(fromDate) > Trim(toDate)) {
                //alert('To Date Should be greater than From Date');//ksahu5 ML change
                alert(TMSettingValidations.ValidResbalCompareBothDate);//ksahu5 ML change
                return false;
            }
            if ((dtEntered_fromDate) > (dtDateOnly)) {
                // alert('From Date Should not be greater than Current Date');//ksahu5 ML change
                alert(TMSettingValidations.ValidResbalFromDateComp);//ksahu5 ML change
                return false;
            }
            if ((dtEntered_toDate) > (dtDateOnly)) {
                // alert('To Date Should not be greater than Current Date');//ksahu5 ML change
                alert(TMSettingValidations.ValidResbalToDateComp);//ksahu5 ML change
                return false;
            }

            document.getElementById('hdUserArguments').value = "-pIsSingleClaim = 0 -pClaimFromDate = " + fromDate + "-pClaimToDate = " + toDate;
            return true;
        }
    }
    else {
        alert(TMSettingValidations.ValidResbalParam);
        return false;
    }

    return false;
}
// akaushik5 Added for MIST 36381 Ends