
function trim(value)
{
	value = value.replace(/^(\s+)?(.*\S)(\s+)?$/, '$2');
	return value;
}
function DisableDiaryManager()
{
    if (document.forms[0].DiaryToManager.checked)
    {
        document.forms[0].QueuePayment.checked = false;
        document.forms[0].DisableDiaryNotifyForUnapprovedEntity.disabled = true;//sachin 7810
        //smahajan6: Payment Supervisory Approval: Start
        //document.forms[0].UseSupChain.checked=false;
        //document.forms[0].DaysApproval.value="";
        //document.forms[0].DaysApproval.disabled=true;
        //smahajan6: Payment Supervisory Approval: End
    }
    else
    {
        document.forms[0].DisableDiaryNotifyForUnapprovedEntity.disabled = false;//sachin 7810
            }
}
function DisableEnableSources() {
    if (document.forms[0].DirectToPrinter.checked) {
        document.forms[0].ddlPrinters.disabled = false;
        document.forms[0].ddlPbfc.disabled = false;
        document.forms[0].ddlpbfeob.disabled = false;

    }
    else {
        document.forms[0].ddlPrinters.disabled = true;
        document.forms[0].ddlPrinters.value = "";
        document.forms[0].ddlPbfc.disabled = true;
        document.forms[0].ddlpbfeob.disabled = true;
        document.forms[0].ddlPbfc.value = "";
        document.forms[0].ddlpbfeob.value = "";
    }
}
function SetValue() {
    
    document.forms[0].hdnAction.value = 'SelectedIndexChange';
}

function DataChangedOnLoad()
{
	alert(document.forms[0].hdnDataChanged.value);
	if (document.forms[0].hdnDataChanged.value=='1')
	{
		alert("setDataChanged");
		setDataChanged(true);
		document.forms[0].hdnDataChanged.value='0';
	}
}
function DisableTrans()
{
  if (document.forms[0].SpecTranTypeCodes.checked==false) {
   
      document.forms[0].TransCodes_multicode.disabled = true;
      document.forms[0].TransCodes_multicodebtndel.disabled=true;
      document.forms[0].TransCodes$multicodebtn.disabled = true;
  }
  else
  {

       document.forms[0].TransCodes_multicode.disabled = false;
       document.forms[0].TransCodes_multicodebtndel.disabled=false;
       document.forms[0].TransCodes$multicodebtn.disabled = false;
  }
  
}
function CheckExists()
{
	arr=document.forms[0].hdnPaymentNotifyValue.value.split("**");
	str=document.forms[0].UserNotify.value+"|"+document.forms[0].LOB.value+"|"+document.forms[0].NotifyPeriod.value+"|"+document.forms[0].DateCriteria.value;
	for(i=0;i<arr.length;i++)
	{
	   arrTemp=arr[i].split("|");
	   sValue=arrTemp[0]+"|"+arrTemp[1]+"|"+arrTemp[2]+"|"+arrTemp[3];
	   if (sValue==str)
	  { 
	    alert("The chosen settings have already been entered.");
	    return false;
	  }
	}
	return true;
}

function DisableDuplicatePayment()
{
  if (document.forms[0].DupPayCheck.checked)
  {
  	document.forms[0].DupCriteria.disabled=false;
  }
  else
  {
   	document.forms[0].DupCriteria.disabled=true;
  }
  
}
//Start: MITS 25716 mcapps2 Print EOB Detail on the check stub
function DisableRollup(param) {
    if (param == '0') {
        if (document.forms[0].PrintEOBStub.checked) {
            document.forms[0].RollUpChecks.checked = false;
            document.forms[0].RollUpChecks.disabled = true;
        }
        else {
            document.forms[0].RollUpChecks.disabled = false;
        }
    }
}
//End: MITS 25716 mcapps2 Print EOB Detail on the check stub
function DisableQueuePayment(param)
{
// JP 03.03.2006   No idea what this is here for. These things are totally unrelated.
//Shivendu uncommented the commented code for MITS 11937 
if(param=='0')
{
	if (document.forms[0].QueuePayment.checked)
	{
	document.forms[0].DiaryToManager.checked=false;

	document.forms[0].SupervisoryApproval.checked = true;

	document.forms[0].UseSupChain.checked=true;
	  
	document.forms[0].DaysApproval.disabled=false;
	  
	document.forms[0].DaysApproval.value="1";
	  
	}
 }

}

function DisableApprovalOptions() {
    // mcapps2 06//26/2011  MITS 23156 
    if (!document.forms[0].SupervisoryApproval.checked) {

        document.forms[0].UseSupChain.checked = false;
        document.forms[0].UseSupChain.disabled = true;

        document.forms[0].DaysApproval.value = "1";
        document.forms[0].DaysApproval.disabled = true;
    }
    if (document.forms[0].SupervisoryApproval.checked) {

        document.forms[0].UseSupChain.disabled = false;
        
        document.forms[0].DaysApproval.disabled = false;
        document.forms[0].DaysApproval.value = "1";
    }

}
function DisableUseSupChain(param)
{
if(param=='0')
{
	if (!document.forms[0].UseSupChain.checked)
	{
		document.forms[0].QueuePayment.checked=false;
	}
	//smahajan6: Payment Supervisory Approval: Start
	//if (document.forms[0].UseSupChain.checked)
	//{
	//document.forms[0].DiaryToManager.checked=false;
	//document.forms[0].DaysApproval.disabled=false;  
	//document.forms[0].DaysApproval.value="1"; 
	//}
	//else
	//{
	//document.forms[0].QueuePayment.checked=false;
	//document.forms[0].DaysApproval.value=""; 
	//document.forms[0].DaysApproval.disabled=true;    
	//}
	//smahajan6: Payment Supervisory Approval: End
}
}
function setSize()
{
	document.forms[0].DaysApproval.style.size="5"
}
function SelectAll()
{
    pleaseWait.Show();	// csingh7 : MITS 15863
	var allvalue="";
	//Asif Start          
      document.getElementById('hdnAction').value="Save";
     document.getElementById('hdnNotifyUsers').value="";
     document.getElementById('hdnNotifyPeriod').value="";
     document.getElementById('hdnLOB').value="";
      document.getElementById('hdnDateCriteria').value="";
      document.getElementById('UserNotify').value = "";
      document.getElementById('hidden_Data')
      if (document.getElementById('hidden_DataChanged') != null)     //csingh7  MITS 15861
          document.getElementById('hidden_DataChanged').value = "";  //csingh7  MITS 15861
   //  document.getElementById('hdnValue').value="";
      
     //Asif End 
	ctrl=document.getElementById('TransCodes');
	try
	{
	for(var f=0;f<ctrl.options.length;f++)
	{
		allvalue=allvalue+ctrl.options[f].value+",";
	}
	ctrl=document.getElementById('hdnTransValues');
	ctrl.value=allvalue;
	}
	catch(ex)
	{
	 return true;
	}
	
	document.forms[0].SysCmd.value="5";
	return true;
}
function SelectAll1()
{

	ctrl=document.getElementById('TransCodes');

	for(var f=0;f<ctrl.options.length;f++)
	{
		ctrl.options[f].selected=true;
			
	}
	value1=ctrl.value;

	//value=trim(value);

	ctrl=document.getElementById('hdnTransValues');
	ctrl.value=value1;

	return true;
}					
function SetCriteria()
{
    if (CheckExists())
    {
	document.forms[0].hdnCriteria.value="Y";
	return true;
	}
	else
	{
	 return false;
	}
}
function DeleteValue()
{
	for (i=0;i<document.forms[0].elements.length;i++)
	{
		if(document.forms[0].elements[i].type=="radio")
		{
			if (document.forms[0].elements[i].checked)
			{
				// MITS 11811 : Anjaneya
				if(document.forms[0].hdnDeletedValue != null)
				{			
					document.forms[0].hdnDeletedValue.value = document.forms[0].elements[i].value;
					// MITS 11811 : New Changes
				    if(document.forms[0].hdnCriteria != null)
				    {
				        document.forms[0].hdnCriteria.value="Y";
				    }
				}
				
				// MITS 11811 : Anjaneya
				return true;
			}
		}
	}
}
function OpenCriteria()
{
 /* if (document.forms[0].UseSupChain.checked)
  {
	document.forms[0].DaysApproval.disabled=true;
  }
  else
  {
    document.forms[0].DaysApproval.disabled=false;
  }
 */
 if (document.forms[0].DupCriteria.value=="1" ||document.forms[0].DupCriteria.value=="5"
     // akaushik5 Changed for MITS 37111 Starts
     //|| document.forms[0].DupCriteria.value == "6" || document.forms[0].DupCriteria.value == "8" )
     || document.forms[0].DupCriteria.value == "6" || document.forms[0].DupCriteria.value == "8" || document.forms[0].DupCriteria.value == "12")
     // akaushik5 Changed for MITS 37111 Ends
 {
	m_Wnd=window.open('/RiskmasterUI/UI/Utilities/Manager/PaymentParameterOptions.aspx','edit','width=350,height=150'+',top='+(screen.availHeight-600)/2+',left='+(screen.availWidth-600)/2+',resizable=yes,scrollbars=yes');
		
	}
}
//MGaba2:R6:ReserveWorksheet related setting in payment parameter
//Enabbling/disabling other controls corresponding to chkbox Use Supervisory Approval For Reserves
function DisableResSupChain()
{
    if (document.forms[0].UseSupAppReserves.checked)
 {
//	document.forms[0].NotifySupReserves.disabled=false;
//	document.forms[0].UseCurrAdjReserves.disabled = false;
//	document.forms[0].DDLTimeFrame.disabled = false;
//	document.forms[0].DaysApprovalReserves.disabled=false;
	if (document.forms[0].DaysApprovalReserves.value == "")
	    document.forms[0].DaysApprovalReserves.value = "0";
        //rsushilaggar MITS 26332 Date 11/28/2011
	document.forms[0].AccessGrpApproveReserve.disabled = false;
 }
 else
 {
//	document.forms[0].NotifySupReserves.disabled=true;
//	document.forms[0].UseCurrAdjReserves.disabled = true;
////	document.forms[0].DDLTimeFrame.disabled = true;
//	document.forms[0].DaysApprovalReserves.disabled=true;
	
	document.forms[0].DaysApprovalReserves.value="0";	
//	document.forms[0].NotifySupReserves.checked=false;
//	document.forms[0].UseCurrAdjReserves.checked = false;
	//rsushilaggar MITS 26332 Date 11/28/2011
	document.forms[0].AccessGrpApproveReserve.disabled = true;
	document.forms[0].AccessGrpApproveReserve.checked = false;
 }
}

//rsushilaggar MITS 26332 Date 11/28/2011
function DisablePaySupChain() {
    if (document.forms[0].SupervisoryApproval.checked) {
        document.forms[0].AccessGrpApprove.disabled = false;
//        document.forms[0].UseSupChain.disabled = false;
//        document.forms[0].PmtNotfyImmSup.disabled = false;

    }
    else {
        document.forms[0].AccessGrpApprove.disabled = true;
        document.forms[0].AccessGrpApprove.checked = false;
//        document.forms[0].UseSupChain.disabled = true;
//        document.forms[0].UseSupChain.checked = false;
////        document.forms[0].PmtNotfyImmSup.disabled = true;
//        document.forms[0].PmtNotfyImmSup.checked = false;
     }
}
function CheckNumeric(objCtrl)
{
    if(objCtrl.value.length==0)
	{
        objCtrl.value=0;
        return false;
    }
			
    if(isNaN(parseInt(objCtrl.value)))
        objCtrl.value=0;
    else
        objCtrl.value=parseInt(objCtrl.value);
	
    if(objCtrl.getAttribute('min')!=null)
    {
        if (!(isNaN(parseInt(objCtrl.getAttribute('min')))))
        {
            if (parseInt(objCtrl.value)<parseInt(objCtrl.getAttribute('min')))
            {
                objCtrl.value=0;
            }
        }
    }
    	
    if(objCtrl.getAttribute('max')!=null)
    {
        if (!(isNaN(parseInt(objCtrl.getAttribute('max')))))
        {
            if (parseInt(objCtrl.value)> parseInt(objCtrl.getAttribute('max')))
            {
                objCtrl.value=0;
            }
        }
    }
	
    var dbl=new String(objCtrl.value);	
    	
    if(dbl.indexOf('-') >= 0)
        objCtrl.value=dbl.replace(/[\-]/g ,"");
    				
	return true;
}


function fnAcceptMinimumValue(objCtrl, value)
{//MGaba2:MITS 18929: Max no of Auto Checks should contain value 1 or more than 1
    if (parseInt(objCtrl.value) < parseInt(value)) {
        alert("Value can not be less than 1.");
        objCtrl.value = parseInt(value);
    }
}
//npadhy JIRA 6418 starts - Function to add the setting for a Distribution Type and Print Options
function AddPrintOption()
{
    var optDistributionType = $('#lstDistributionType option:selected');
    if (optDistributionType.length == 0)
    {
        alert('Please select one of the Distribution type to add mapping');
        return;
    }
    var optPrintDstrType = new Option(optDistributionType.text() + '|' + $("#ddlPrintOptions option:selected").text(), optDistributionType.val() + '|' + $("#ddlPrintOptions").val());
    // Below line not working for IE
    //$('#lstPrintDistribution').append($(optPrintDstrType).clone());
    var lstPrintDistribution = document.getElementById('lstPrintDistribution');
    lstPrintDistribution.options[lstPrintDistribution.options.length] = optPrintDstrType;
    $(optDistributionType).remove();
    setDataChanged(true);
    if ($('#txtMapping').val() == "")
    {
        $('#txtMapping').val(optDistributionType.val() + '|' + $("#ddlPrintOptions").val() + ',');
    }
    else
    {
        $('#txtMapping').val($('#txtMapping').val() + optDistributionType.val() + '|' + $("#ddlPrintOptions").val() + ',');
    }

    return false;
}

function RemovePrintOption()
{
    var optPrintDstrType = $('#lstPrintDistribution option:selected');
    if (optPrintDstrType.length == 0)
    {
        alert('Please select one of the mappings to delete');
        return;
    }
    var optDistributionType = new Option(optPrintDstrType.text().substr(0, optPrintDstrType.text().indexOf("|")), optPrintDstrType.val().substr(0, optPrintDstrType.val().indexOf("|")));
    // Below line not working for IE
    //$('#lstPrintDistribution').append($(optPrintDstrType).clone());
    var lstDistributionType = document.getElementById('lstDistributionType');
    lstDistributionType.options[lstDistributionType.options.length] = optDistributionType;
    $(optPrintDstrType).remove();
    setDataChanged(true);
    $('#txtMapping').val($('#txtMapping').val().replace(optPrintDstrType.val() + ',' , ''));

    return false;
}
//npadhy JIRA 6418 Ends Function to add the setting for a Distribution Type and Print Options