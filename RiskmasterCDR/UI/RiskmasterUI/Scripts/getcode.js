﻿//rsushilaggar Modified the code to fix an issue in the Check Stub Text MITS 21600
//function selCode(sCodeText, lCodeId, sParText, bIsMultipleCode) {
//rupal:policy system interface
//mits 33996
function selCode(sCodeText, lCodeId, sParText, bIsMultipleCode, sAdditionalParam, sTransSeqNum, sCoverageKey) {
    //Deb ML Changes
    var QryStr = "";
    var ctrlName = "";
    var sPageURL = document.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == 'filter')
            ctrlName = sParameterName[1];
        if (sParameterName[0] == 'codetype') {
            QryStr = sParameterName[1];
        }
    }
    //Aman MITS 31354
    if (QryStr.toUpperCase() == "COUNTRY") {
        sAdditionalParam = "COUNTRY";
    }
    //Deb ML Changes
    //alert(sCodeText);
    var iPayIntervalDays = ""; //Add by kuladeep for Day interval for auto checks mits:19360
    var ResStatus = new Array();//skhare7 27920
    var oMMSEAFormname = window.opener.document.forms[0].SysClassName;

    if (oMMSEAFormname != null && oMMSEAFormname.value == "MMSEAData") {
        window.opener.document.forms[0].MMSEAEditedFlag.value = 1;
    }
    //Add by kuladeep for Day interval for auto checks mits:19360 Start
    if (window.opener != null) {
        if (sCodeText == "PD Day" && window.opener.document.forms[0].SysClassName.value == "FundsAutoBatch") {
            //            iPayIntervalDays = window.opener.document.getElementById('PaymentIntervalDays').value;
            //			 if (iPayIntervalDays= "") {
            window.opener.GetPaymentIntervalFrame(iPayIntervalDays);
            //}

        }
    }
    //Add by kuladeep for Day interval for auto checks mits:19360 End
    //Jira id: 8418,8417,7505 - Govind - Start
    if (window.opener != null && window.opener.document.forms[0] != null && window.opener.document.forms[0].SysFormName != null
        && window.opener.document.forms[0].SysFormName.value == "lobparameters" && window.opener.document.getElementById('lstPerRsvTypeRecovery_multicode') != null
        && window.opener.document.getElementById('lstPerRsvTypeIncurred_multicode') != null) {
        if (ctrlName == "LOBRSVMLTCD") {
            var varlstPerRsvTypeIncurred = window.opener.document.getElementById('lstPerRsvTypeIncurred_multicode');
            for (var i = 0; i < varlstPerRsvTypeIncurred.length; i++) {
                if (lCodeId == varlstPerRsvTypeIncurred[i].value) {
                    //alert(sCodeText + ' is already configured for Incurred Balance Calculations.');
                    alert(sCodeText + " " + parent.CommonValidations.AlertForIncurredBalanceCalculations);
                    return false;
                }
            }
        }
        if (ctrlName == "LOBINCRMLTCD") {
            var varlstPerRsvTypeRecovery = window.opener.document.getElementById('lstPerRsvTypeRecovery_multicode');
            for (var i = 0; i < varlstPerRsvTypeRecovery.length; i++) {
                if (lCodeId == varlstPerRsvTypeRecovery[i].value) {
                    //alert(sCodeText + ' is already configured for Reserve Balance Calculations.');
                    alert(sCodeText + " " + parent.CommonValidations.AlertForReserveBalanceCalculations);
                    return false;
                }
            }
        }
    }
    //Jira id: 8418,8417,7505 - Govind - End
    if (lCodeId != 0) {
        //skhare7 27920
        if (window.opener != null) {
            if (window.opener.document.forms[0] != null) {
                if (window.opener.document.forms[0].SysFormName != null) {
                    if (window.opener.document.forms[0].SysFormName.value == "billreviewsystem" && document.getElementById('displayname') != null && document.getElementById('displayname').value == "Transaction Type") {

                        ResStatus = sParText.split("_");
                        if (ResStatus[1] == 'H') {
                            alert('Payment/collection can not be made against the Reserve of hold Status.');
                            return false;

                        }
                    }
                }
            }
            //skhare7 27920
        }
        // Put the code on the calling form+close this one
        //document.getElementById('bUnload').value = 'true';	
        //alert("1");	
        //rsushilaggar Modified the code to fix an issue in the Check Stub Text MITS 21600
        //window.opener.codeSelected(sCodeText, lCodeId, sParText, null, bIsMultipleCode);
        //rupal:policy system interface
        window.opener.codeSelected(sCodeText, lCodeId, sParText, null, bIsMultipleCode, sAdditionalParam, sTransSeqNum, sCoverageKey); //mits 33996
        //alert("2");	
        if (window.opener.lookupCallback != null) {
            eval("window.opener." + window.opener.lookupCallback);
            return false;
        }
    }

    //Geeta 13/10/2006: Code added for fixing MITS no 8232 for Clear check Box on Funds screen
    if (window.opener != null) {//Add by kuladeep for handel error.
        if (window.opener.document.forms[0] != null) {
            if (window.opener.document.forms[0].formname != null) {
                if (window.opener.document.forms[0].formname.value == "funds") {
                    var chkFunds = eval('window.opener.document.forms[0].Cleared');
                    if (chkFunds != null) {
                        if (sCodeText == "P Printed") {
                            chkFunds.disabled = false;
                        }
                        else {
                            chkFunds.disabled = true;
                        }
                    }
                }

            }
        }
    }

    //Add by kuladeep for Day interval for auto checks mits:19360 Start
    //	if (window.opener != null) {
    //	    if (sCodeText == "PD Day" ) {
    //	        window.opener.GetPaymentIntervalFrame(iPayIntervalDays);
    //	    }
    //	}
    //Add by kuladeep for Day interval for auto checks mits:19360 End

    return false;

}
function RestorePrevValue() {
    if (window.opener.document.getElementById('pye_lastname') != undefined) {
        if (window.opener.document.getElementById('pye_lastname').cancelledvalue != undefined) {
            window.opener.document.getElementById('pye_lastname').value = window.opener.document.getElementById('pye_lastname').cancelledvalue;
        }
    }
}
//MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
//rsharma220 MITS 29790 start
function getSorting(sortField) {
    if (sortField == document.getElementById('hdSortColumn').value) {
        if (document.getElementById('hdSortOrder').value == "Asc") {
            document.getElementById('hdSortOrder').value = "Desc";
        }
        else {
            document.getElementById('hdSortOrder').value = "Asc";
        }
    }
    else {
        document.getElementById('hdSortOrder').value = "Asc";
    }
    document.getElementById('hdSortColumn').value = sortField;
    //rsharma220 MITS 29790 End
    var pageurl = document.getElementById('pageaction').value;
    if (document.getElementById('PageNumber').value != "" || document.getElementById('PageNumber').value != "0" || document.getElementById('PageNumber').value != 0) {
        document.forms[0].action = pageurl + "&SortColumn=" + sortField + "&PageNumber=" + document.getElementById('PageNumber').value + "&SortOrder=" + document.getElementById('hdSortOrder').value; //Add by kuladeep for Page issue.
    }
    else {
        document.forms[0].action = pageurl + "&SortColumn=" + sortField + "&SortOrder=" + document.getElementById('hdSortOrder').value;
    }
    document.forms[0].submit();
}

function getPage(lPageNumber) {
    document.getElementById('pagenumber').value = lPageNumber;
    var pageurl = document.getElementById('pageaction').value;//MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
    //document.forms[0].action = pageurl + "&PageNumber=" + lPageNumber + "&SortColumn=" + document.getElementById('hdSortColumn').value + "&SortOrder=" + document.getElementById('hdSortOrder').value;;  //Add by kuladeep for Page issue.
    //zmohammad MITS 38406 Start Putting null checks and case checking for BRS.
    document.forms[0].action = pageurl + "&PageNumber=" + lPageNumber;

    if (document.getElementById('hdSortColumn') != null) {
        document.forms[0].action = document.forms[0].action + "&SortColumn=" + document.getElementById('hdSortColumn').value;  //Add by kuladeep for Page issue.
    }

    if (document.getElementById('hdSortOrder') != null) {
        document.forms[0].action = document.forms[0].action + + "&SortOrder=" + document.getElementById('hdSortOrder').value;   //Add by kuladeep for Page issue.
    }
    //zmohammad MITS 38406 End
    document.forms[0].submit();
}
function TransferData() {    
    var sId, sText, sParentText;
    var PIds = new Array();
    //srajindersin MITS 34769 1/8/2014
    //sId=document.forms[0].resultid.value;
    var resultctrl = document.getElementById('resultid');
    if (resultctrl != null)
        sId = resultctrl.value;

    //sText=document.forms[0].resulttext.value;
    var resulttextctrl = document.getElementById('resulttext');
    if (resulttextctrl != null)
        sText = resulttextctrl.value;
    //zmohammad JIRa 6284 : Fix for Policy lookup
    var sCovKey = document.getElementById('hdnCovgKey');
    var sCovgSeqNum = document.getElementById('hdnCovSeqNum');
    var sTransSeqNum = document.getElementById('hdnTransSeqNum');
    sParentText = sId.substring(sId.indexOf("_") + 1);
	sId = sId.substring(0, sId.indexOf("_"));

	//abansal23 on 05/13/2009 MITS 14847 starts
	if (!(sParentText.search("//") == -1)) {
	    PIds = sParentText.split("//");
	    if (PIds.length == 8) {
	        for (i = 0; i < PIds.length - 1; i++) {
	            switch (i) {
	                case 0:
	                    if (window.opener.document.forms[0].OH_FACILITY_EID != null) {
	                        window.opener.document.forms[0].OH_FACILITY_EID.value = PIds[i];
	                    }
	                    break;
	                case 1:
	                    if (window.opener.document.forms[0].OH_LOCATION_EID != null) {
	                        window.opener.document.forms[0].OH_LOCATION_EID.value = PIds[i];
	                    }
	                    break;
	                case 2:
	                    if (window.opener.document.forms[0].OH_DIVISION_EID != null) {
	                        window.opener.document.forms[0].OH_DIVISION_EID.value = PIds[i];
	                    }
	                    break;
	                case 3:
	                    if (window.opener.document.forms[0].OH_REGION_EID != null) {
	                        window.opener.document.forms[0].OH_REGION_EID.value = PIds[i];
	                    }
	                    break;
	                case 4:
	                    if (window.opener.document.forms[0].OH_OPERATION_EID != null) {
	                        window.opener.document.forms[0].OH_OPERATION_EID.value = PIds[i];
	                    }
	                    break;
	                case 5:
	                    if (window.opener.document.forms[0].OH_COMPANY_EID != null) {
	                        window.opener.document.forms[0].OH_COMPANY_EID.value = PIds[i];
	                    }
	                    break;
	                case 6:
	                    if (window.opener.document.forms[0].OH_CLIENT_EID != null) {
	                        window.opener.document.forms[0].OH_CLIENT_EID.value = PIds[i];
	                    }
	                    break;

	            }
	        }

	    }
	}
	//abansal23 on 05/13/2009 MITS 14847 ends
	if (document.forms[0].lookuptype.value.substring(0, 5) == "code.")
	{
	    //Aman ML Changes
	    var QryStr = "";
	    var sPageURL = document.location.search.substring(1);
	    var sURLVariables = sPageURL.split('&');
	    for (var i = 0; i < sURLVariables.length; i++) {
	        var sParameterName = sURLVariables[i].split('=');
	        if (sParameterName[0] == 'codetype') {
	            QryStr = sParameterName[1].split('.')[1];
	        }
	    }
	    //Aman MITS 31354
	    var sAdditionalParam = ""
	    if (QryStr.toUpperCase() == "COUNTRY") {
	        sAdditionalParam = "COUNTRY";
	    }
	    else {
	        if(sCovgSeqNum.value != null || sCovgSeqNum.value != '')
	            sAdditionalParam = sCovgSeqNum.value;
	    }
	    //Aman ML Changes
	    //zmohammad JIRa 6284 : Fix for Policy lookup
	    if(sTransSeqNum.value != null && sCovKey.value != null){
	    window.opener.codeSelected(sText, sId, sParentText, '', '', sAdditionalParam, sTransSeqNum.value, sCovKey.value);
	  }
	    else{
	        window.opener.codeSelected(sText, sId, sParentText, '', '', sAdditionalParam);
	        //window.opener.codeSelected(sText,sId,sParentText);}
	    }
	}
	else
	{
		if(haveProperty(window.opener,"lookupCallback"))
		{
			if(window.opener.lookupCallback!=null)
			{
				eval("window.opener."+window.opener.lookupCallback+"("+sId+")");
				return false;
			}
		}
		window.opener.entitySelected(sId);
	}
	return false;
}

function handleUnload() {
    //rsushilaggar MITS 36233
    if (window.opener.document.getElementById('overlaydiv') != null)
        window.opener.document.getElementById('overlaydiv').style.display = "none";
    if (window.opener != null) {
        //pmittal5 Mits 18137 - Auto fill initials for patient in case of "Edit"
        if (window.opener.document.forms[0].SysFormName != null && window.opener.document.forms[0].SysFormName.value == "patient") {
            if (document.forms[0].EditSetting.value == "Edit")
                window.opener.fillinitials();
        }//End - pmittal5
    	window.opener.m_LookupBusy=false;
		window.opener.onCodeClose();
	}
	return true;
}
function haveProperty(obj, sPropName)
{
	for(p in obj)
	{
		if(p==sPropName)
			return true;
	}
	return false;
}

function FormSubmit(e) {
    var evt = e || window.event;
    //var current = evt.target || evt.srcElement;
    var sCovgSeqNum = "";
    var sTransSeqNum = "";
    var sCoverageKey = "";      //Ankit Start : Worked on MITS - 34297
    //if (window.event != null) {
    if(evt!=null){
//        if (document.forms[0].AddNewSetting.value != "AddNew") {
            if (document.forms[0].optResults.selectedIndex < 0)
             {
                alert("Please select the record.");
            }
            else {
                var sId, sText, sParentText;
                var PIds = new Array();

                sId = document.forms[0].optResults.options[document.forms[0].optResults.selectedIndex].value;
                sText = document.forms[0].optResults.options[document.forms[0].optResults.selectedIndex].text;

                var sSplitText = sId.split("_");
                sParentText = sSplitText[1];
                    if (sSplitText.length > 2) {
                        sCovgSeqNum = sSplitText[2];
                    }
                    if (sSplitText.length > 3) {
                        sTransSeqNum = sSplitText[3];
                    }
                    //Ankit Start : Worked on MITS - 34297
                    if (sSplitText.length > 4) {
                        sCoverageKey = sSplitText[4];
                    }
                    //Ankit End

                //sParentText = sId.substring(sId.indexOf("_") + 1);
                //abansal23 on 05/13/2009 MITS 14847 starts
                if (!(sParentText.search("//") == -1)) {
                    PIds = sParentText.split("//");
                    if (PIds.length == 8) {
                        for (i = 0; i < PIds.length - 1; i++) {
                            switch (i) {
                                case 0:
                                    if (window.opener.document.forms[0].OH_FACILITY_EID != null) {
                                        window.opener.document.forms[0].OH_FACILITY_EID.value = PIds[i];
                                    }
                                    break;
                                case 1:
                                    if (window.opener.document.forms[0].OH_LOCATION_EID != null) {
                                        window.opener.document.forms[0].OH_LOCATION_EID.value = PIds[i];
                                    }
                                    break;
                                case 2:
                                    if (window.opener.document.forms[0].OH_DIVISION_EID != null) {
                                        window.opener.document.forms[0].OH_DIVISION_EID.value = PIds[i];
                                    }
                                    break;
                                case 3:
                                    if (window.opener.document.forms[0].OH_REGION_EID != null) {
                                        window.opener.document.forms[0].OH_REGION_EID.value = PIds[i];
                                    }
                                    break;
                                case 4:
                                    if (window.opener.document.forms[0].OH_OPERATION_EID != null) {
                                        window.opener.document.forms[0].OH_OPERATION_EID.value = PIds[i];
                                    }
                                    break;
                                case 5:
                                    if (window.opener.document.forms[0].OH_COMPANY_EID != null) {
                                        window.opener.document.forms[0].OH_COMPANY_EID.value = PIds[i];
                                    }
                                    break;
                                case 6:
                                    if (window.opener.document.forms[0].OH_CLIENT_EID != null) {
                                        window.opener.document.forms[0].OH_CLIENT_EID.value = PIds[i];
                                    }
                                    break;

                            }
                        }

                    }
                }
                var sAdditionalParam = sCovgSeqNum;
                //abansal23 on 05/13/2009 MITS 14847 ends
                sId = sId.substring(0, sId.indexOf("_"));
                if (document.forms[0].lookuptype.value.substring(0, 5) == "code.") {
                    //window.opener.codeSelected(sText, sId, sParentText);
                    //Deb ML Changes
                    var QryStr = "";
                    var sPageURL = document.location.search.substring(1);
                    var sURLVariables = sPageURL.split('&');
                    for (var i = 0; i < sURLVariables.length; i++) {
                        var sParameterName = sURLVariables[i].split('=');
                        if (sParameterName[0] == 'codetype') {
                            QryStr = sParameterName[1].split('.')[1];
                        }
                    }
                    //Aman MITS 31354
                    if (QryStr.toUpperCase() == "COUNTRY") {
                        sAdditionalParam = "COUNTRY";
                    }
                    //Deb ML Changes

                    //tanwar2 - JIRA 6129 - start
                    //window.opener.codeSelected(sText, sId, sParentText, '', '', sAdditionalParam);
                    window.opener.codeSelected(sText, sId, sParentText, '', '', sAdditionalParam, sTransSeqNum, sCoverageKey);
                    //tanwar2 - JIRA 6129 - end
                    self.close();
                }
                else {
                    if (haveProperty(window.opener, "lookupCallback")) {
                        if (window.opener.lookupCallback != null) {

                                eval("window.opener." + window.opener.lookupCallback + "(" + sId + ")");
                            self.close();
                            return false;
                        }
                    }
                    window.opener.entitySelected(sId);
                    self.close();
                }
            }
        //}
        return false;
    }
}
function ResultsKeyDown()
{
	if(event.keyCode==13)
	{
		FormSubmit();
	}
	return true;
}


function ClearAutoFilled()
{
		var sId, sText,junk;
		sId="-10";
        //pmittal5 Mits 15308 04/21/09 - For Funds transaction and Auto Checks screens
        if(window.opener.document.forms[0].AddNewPayee != null)
            window.opener.document.forms[0].AddNewPayee.value = "true"; 
		sText=document.forms[0].resulttext;
		if(document.forms[0].lookuptype.value.substring(0,5)=="code.")
		{
			window.opener.codeSelected(sText,sId);
		}
		else
		{
			if(haveProperty(window.opener,"lookupCallback"))
			{
				if(window.opener.lookupCallback!=null)
				{
					eval("window.opener."+window.opener.lookupCallback+"("+sId+")");
					return false;
				}
			}
			window.opener.entitySelected(sId);
		}
	return false;
}
	
function DoNext()
{
	//Instance/Document.forms[0].action="quicklookup1.asp";
	document.forms[0].submit();
}

function setAddNewValue() {   
//	document.forms[0].AddNewSetting.value="AddNew";
	if (window.opener.document.forms[0].AddNewPatient != null)  //pmittal5 Mits 18137 - Auto fill initials for patient in case of "Add New"
	{
	    window.opener.document.forms[0].AddNewPatient.value = "true";
	}
	if (window.opener.document.forms[0].addNewEntity != null)  //mcapps2 MITS 23054 - New entity is not created from Payments
	{
	    window.opener.document.forms[0].addNewEntity.value = "true";
	}
//	if (window.opener.document.getElementById("AddNewAttorney") != null) //Amandeep MITS 28354, 28063  --Start
//    {
//        window.opener.document.getElementById("AddNewAttorney").value = "true";
//    }
    //Amandeep MITS 28354, 28063  --End
	return true;
}
function formOnload() {
    if (haveProperty(frmData, "optResults")) {
        document.forms[0].optResults.focus();
    }
    else
        if (!haveProperty(frmData, "noresult"))
            TransferData();
 }

 //pmittal5 Mits 18137 - Auto fill initials for patient in case of "Edit"
 function setEditSetting() {
     document.forms[0].EditSetting.value = "Edit";
 }
