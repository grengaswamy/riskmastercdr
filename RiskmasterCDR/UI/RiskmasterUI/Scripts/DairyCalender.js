     
//Created by Nitin for DiaryCalendar     
function WebSchedule_Click(oScheduleInfo, oEvent, oDialog, oActivity)
 {
    if(oDialog.className == 'igmv_NavigationButton igmv_NavigationButtonPrev')
    {
       // setupSchedPI(oScheduleInfo)
    }
    else if(oDialog.className == 'igmv_NavigationButton igmv_NavigationButtonNext')
    {
       // WebScheduleActiveDayChanged(oScheduleInfo);
    }
    else
    {
        var str = oScheduleInfo.getSelectedActivity();
        var popupDiv = document.getElementById('divDiaryDisplay');
        if(str != null)
        {
            //alert(str.getSubject());
            popupDiv.innerHTML = str.getDescription();
            ShowDiv(oEvent.event);
        }
        else
        {
           popupDiv.innerHTML = '';
           popupDiv.style.display = 'none';
        }
    }
    
    return true;
  }
              
              
  function ShowDiv(e)
  {
    // Get the click location
    var height = e.clientY + parseInt('5');
    // Get the height inside the clicked image
    var offsetHeight = parseInt(e.offsetY);
    height = height - offsetHeight + document.documentElement.scrollTop;
       
    // Get the click location(width)
    var width = e.clientX
    // Get the width inside the clicked image
    var offsetWidth = parseInt(e.offsetX);
    width = width - offsetWidth;
         
    var div = document.getElementById('divDiaryDisplay');
    div.style.position = 'absolute';

    // Set the div top to the height 
    div.style.top = parseInt(height) + parseInt('10');
    // Set the div Left to the height 
    div.style.left = parseInt(width) + parseInt('10');
         
    // Show the div layer
    div.style.display = 'block';

 }
        
 function ShowSelector()
 {
   //var div = document.getElementById('DateSelecter');
   //div.style.position = 'absolute';
   //var selectBtn = document.getElementById('imgSelectDate');
   //var displaydiv = document.getElementById('divCalendarDisplay');//vkumar258 RMA-9315 displaying calendar control on top
   //displaydiv.style.display = 'block';
   
   //// Set the div top to the height 
   //div.style.top = selectBtn.style.top - parseInt('2');  // parseInt(height) + parseInt('20');

   //// Set the div Left to the height 
   //div.style.left = selectBtn.style.left  - parseInt('2');
         
   //// Show the div layer
   //div.style.display = 'block';
 }
  
        
function HideSelector()
{
    //var div = document.getElementById('DateSelecter');
    //div.style.display = 'none';
    //var displaydiv = document.getElementById('divCalendarDisplay');//vkumar258 RMA-9315 displaying calendar control on top
    //displaydiv.style.display = 'none';
}
        
        
function setupSchedPI(webSchedule)
{
    var pi=webSchedule.getWebScheduleInfo().getProgressIndicator();
    pi.setRelativeContainer(document.getElementById("progressIndicatorTemplate"));
    pi.setLocation(ig_Location.BottomCenter);
}
    
    
function NavigateToSelectedFDM(attachrecordid,category)
{
    var arrAdmTrack = null;
    var del = String.fromCharCode(31);
    var arrAdmTrack;
    var admtable;
    
    //Added by Nitin for Mits 14882 on 08-Apr-09
    arrAdmTrack = category.split('|');

    if (arrAdmTrack != null) 
    {
        if (arrAdmTrack[0] == "admintracking")
        {
            admtable = arrAdmTrack[1];
            parent.parent.MDIShowScreen(attachrecordid, "admintrackinglist" + del + "Admin Tracking (" + admtable + ")" + del + "UI/FDM/admintracking" + admtable + ".aspx" + del + "?recordID=(NODERECORDID)"); // nnorouzi; no further items (SELECETD/TOP, another Root)are provided in the categoryArg, otherwise it will be considered as an injected node.
            return;
        }
    }
    //Ended by Nitin for Mits 14882 on 08-Apr-09

    //added by Nitin on 22-04-2009 in order to open some FDM screens as popups for Mits 15272
    if (IsOpenAttachedRecordInPopup(category)) 
    {
        var wnd = window.open("/RiskmasterUI/UI/FDM/" + category + ".aspx?recordID=" + attachrecordid, "AttachedRecord",
		"width=900,height=650,top=" + (screen.availHeight - 650) / 2 + ",left=" + (screen.availWidth - 900) / 2 + ",resizable=yes,scrollbars=yes");
    }
    else 
    {
        parent.parent.MDIShowScreen(attachrecordid, category);
    }
}

//added by Nitin on 22-04-2009 
//in order to open some FDM screens as popups for Mits 15272
function IsOpenAttachedRecordInPopup(category) 
{
    //cases for medwatch,medwatchtest,fallinfo,concomitant have been added by Nitin 
    //for Mits no 16940 on 15/June/2009 
    
    switch (category) 
    {
        case "adjuster":
        case "adjusterdatedtext":
        case "claimant":
        case "defendant":
        case "litigation":
        case "expert":
        case "piemployee":
        case "pidependent":
        case "pirestriction":
        case "piworkloss":
        case "pimedstaff":
        case "pimedstaffprivilege":
        case "pimedstaffcertification":
        case "piother":
        case "pipatient":
        case "piprocedure":
        case "piphysician":
        case "piprivilege":
        case "picertification":
        case "pieducation":
        case "piprevhospital":
        case "piwitness":
        case "cmxcmgrhist":
        case "cmxaccommodation":
        case "cmxvocrehab":
        case "cmxtreatmentpln":
        case "cmxmedmgtsavings":
        case "leave":
        case "unit":
        case "osha":
        case "eventdatedtext":
        case "casemgrnotes":
        case "dependent":
        case "medwatch":
        case "medwatchtest":
        case "fallinfo":
        case "concomitant":
            return true;
        default:
            return false;
    } 
}

function EditDiary(entryId,attachPrompt)
{
    var isPeek;
    var isProcessingOffice;
    var btnPeek ;
    var btnPeekProcOffice;
    var hdnUserName;

    btnPeek = parent.document.getElementById("btnPeek");
    btnPeekProcOffice = parent.document.getElementById("btnPeekProcOffice");
    hdnUserName = parent.document.getElementById("hdnUserName");
    
    if (btnPeek.value == "Home")
    {
        isPeek = "true";
    }
    else if (btnPeek.value == "Peek")
    {
        isPeek = "false";
    }
    
    if (btnPeekProcOffice.value == "Home")
    {
        isProcessingOffice = "true";
    }
    else
    {
        isProcessingOffice = "false";
    }

    //RMA - 4691
    //parent.window.location.href = "/RiskmasterUI/UI/Diaries/EditDiary.aspx?assigneduser=" + hdnUserName.value + "&isdiarycalendar=true&isdiarycalendarpeek=" + isPeek + "&isdiarycalendarprocessingoffice=" + isProcessingOffice + "&entryid=" + entryId + "&attachprompt=" + attachPrompt;
    parent.window.location.href = "/RiskmasterUI/UI/FDM/creatediary.aspx?assignedusername=" + hdnUserName.value + "&isdiarycalendar=true&isdiarycalendarpeek=" + isPeek + "&isdiarycalendarprocessingoffice=" + isProcessingOffice + "&entryid=" + entryId + "&attachprompt=" + attachPrompt + "&recordID=" + entryId + "&CalledBy=creatediary";
 }

function HideDiv()
{
    document.getElementById('divDiaryDisplay').style.display = 'none';
}
   


    
    