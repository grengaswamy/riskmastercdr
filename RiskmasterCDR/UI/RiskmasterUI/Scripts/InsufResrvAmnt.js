function clickReserve(action) 
{
			var amount=0;
			var allowSubmit = true;
			var dResLimit = 0;
			var dNewAmount = 0;
			var dClaimIncLimit = 0;//Jira 6385- Incurred Limit
			var dClaimcurrentIncLimit = 0;//Jira 6385- Incurred Limit
			if ((isNaN(document.forms[0].reslimit.value) == false) && (eval(document.forms[0].reslimit.value) != null) && parseFloat(document.forms[0].reslimit.value) > 0) {
			    dResLimit = parseFloat(document.forms[0].reslimit.value);
			}
    //Jira 6385- Incurred Limit starts
			if ((isNaN(document.forms[0].clminclimit.value) == false) && (eval(document.forms[0].clminclimit.value) != null) && parseFloat(document.forms[0].clminclimit.value) > 0) {
			    dClaimIncLimit = parseFloat(document.forms[0].clminclimit.value);
			}

			if ((isNaN(document.forms[0].currclminclimit.value) == false) && (eval(document.forms[0].currclminclimit.value) != null) && parseFloat(document.forms[0].currclminclimit.value) > 0) {
			    dClaimcurrentIncLimit = parseFloat(document.forms[0].currclminclimit.value);
			}
    //Jira 6385- Incurred Limit ends
			if ((document.forms[0].isreslimit.value == "Yes") && (action == "0")) {
			    if ((isNaN(document.forms[0].txtres.value) == false) && (eval(document.forms[0].txtres.value) != null) && (isNaN(document.forms[0].txtaddres.value) == false) && (eval(document.forms[0].txtaddres.value) != null) && parseFloat(document.forms[0].txtaddres.value) > 0) {
			        dNewAmount = (parseFloat(document.forms[0].txtaddres.value) + parseFloat(document.forms[0].txtres.value));
			    }
			}
			if ((document.forms[0].isreslimit.value == "Yes") && (action == "1")) {
			    if ((isNaN(document.forms[0].txtsetres.value) == false) && (eval(document.forms[0].txtsetres.value) != null) && parseFloat(document.forms[0].txtsetres.value) > 0) {
			        dNewAmount = parseFloat(document.forms[0].txtsetres.value);
			    }
			}

    //Jira 6385- Incurred Limit starts
			if ((document.forms[0].isclminclimit.value == "Yes") && (action != "2") && (action != "-1")) {
			    if ((dClaimcurrentIncLimit > 0) && (dClaimIncLimit > 0)) {
			        if ((dClaimcurrentIncLimit > dClaimIncLimit) || (dNewAmount > dClaimIncLimit)) {
			            if ((isNaN(document.forms[0].txtsetres.value) == false) && (eval(document.forms[0].txtsetres.value) != null))
			            {
			                amount = document.forms[0].txtsetres.value;
			                document.forms[0].reason.value = "Insuff. Reserves";             
			            }
			            alert(InsufficientReservesValidations.ValidExceedIncLimit);
			            window.opener.setDataChanged(true);
			            return false;
			        }
			    }
			}
    //Jira 6385- Incurred Limit ends
			if ((document.forms[0].isreslimit.value == "Yes") && (action != "2") && (action != "-1")) {
			    if ((dResLimit > 0) && (dNewAmount > 0)) {
			        if (dNewAmount > dResLimit) {
			            //alert("You do not have permission to set the reserve to this amount.  Please contact your manager for help.");
			            alert(InsufficientReservesValidations.ValidPermissionIssue);
			            window.opener.setDataChanged(true);
			            return false;
			        }
			    }
			}
            // MITS 32376 mcapps2 Start
			if (action == "-1") { 

			    var txtSysIsFormSubmitted = window.opener.document.getElementById('SysIsFormSubmitted');
			    if (txtSysIsFormSubmitted != null) {
			        txtSysIsFormSubmitted.value = "0";
			    }
			    window.close();

			}
			// MITS 32376 mcapps2 Start
			if(action=="1")
			{
				
				if((isNaN(document.forms[0].txtsetres.value)==false) && (eval(document.forms[0].txtsetres.value)!=null))
				{
					amount=document.forms[0].txtsetres.value;
					document.forms[0].reason.value="Insuff. Reserves";
					//document.forms[0].btnsetres.disabled = "true";
					document.forms[0].btnaddres.disabled="true";
					document.forms[0].btncont.disabled="true";
					document.forms[0].btnclose.disabled="true";
				}
				else
				{
				    //alert('Invalid data');
				    alert(InsufficientReservesValidations.ValidDataIssue);
					document.forms[0].txtsetres.focus();
					allowSubmit=false;
				}
			}
			if(action=="0")
			{
				if((isNaN(document.forms[0].txtaddres.value)==false) && (eval(document.forms[0].txtaddres.value)!=null) && parseFloat(document.forms[0].txtaddres.value)>0)
				{
					amount=parseFloat(document.forms[0].txtaddres.value) + parseFloat(document.forms[0].txtres.value);
					document.forms[0].reason.value="Insuff. Reserves";
					document.forms[0].btnsetres.disabled="true";
					//document.forms[0].btnaddres.disabled="true";
					document.forms[0].btncont.disabled="true";
					document.forms[0].btnclose.disabled="true";
				}
				else
				{
				    //alert('Invalid data');
				    alert(InsufficientReservesValidations.ValidDataIssue);
					document.forms[0].txtaddres.focus();
					allowSubmit=false;
				}
			}
			if(action=="2") {
			    var sXML = "";
			    var iIndexOf = "";
			    var iIndexOf2 = "";
				document.forms[0].btnsetres.disabled="true";
				document.forms[0].btnaddres.disabled="true";
				//document.forms[0].btncont.disabled="true";
				document.forms[0].btnclose.disabled="true";
				var sReason = document.forms[0].reason.value;
				switch (sReason) {

				    case "Insuff. Reserves":
				        sXML = document.forms[0].hdInsuffResXml.value;
					    iIndexOf = "";
					    iIndexOf2 = "";
					    sXML = replaceAll(sXML, '&lt;', '<');
					    sXML = replaceAll(sXML, '&gt;', '>');
					    sXML = replaceAll(sXML, ' ', '');
					    sXML = replaceAll(sXML, '&quot;>', '----');
					    sXML = replaceAll(sXML, '&quot;', '');
				        iIndexOf = sXML.indexOf("rcodeid=");
				        iIndexOf2 = sXML.indexOf("----", iIndexOf);
				        if ((iIndexOf > 0) && (iIndexOf2 > iIndexOf)) {
				            iIndexOf = (iIndexOf + 8);
				            sXML = sXML.substr(iIndexOf, (iIndexOf2 - iIndexOf));
				            window.opener.document.forms[0].skipreservetypes.value = window.opener.document.forms[0].skipreservetypes.value + ' ' + sXML;
				        }
				        window.opener.document.forms[0].SysCmd.value = "9";
				        window.opener.document.forms[0].submit();
				        allowSubmit = false;
				        window.close();
				        break;

				    default:
				        document.forms[0].reason.value = "AUTO-ADJUST";
				        sXML = document.forms[0].hdInsuffResXml.value;
				        iIndexOf = "";
				        iIndexOf2 = "";
				        sXML = replaceAll(sXML, '&lt;', '<');
				        sXML = replaceAll(sXML, '&gt;', '>');
				        sXML = replaceAll(sXML, ' ', '');
				        sXML = replaceAll(sXML, '&quot;>', '----');
				        sXML = replaceAll(sXML, '&quot;', '');
				        window.opener.document.forms[0].Reason.value = "AUTO-ADJUST";
				        iIndexOf = sXML.indexOf("rcodeid=");
				        iIndexOf2 = sXML.indexOf("----", iIndexOf);
				        if ((iIndexOf > 0) && (iIndexOf2 > iIndexOf))
				        {
				            iIndexOf = (iIndexOf + 8);
				            sXML = sXML.substr(iIndexOf, (iIndexOf2 - iIndexOf));
				            window.opener.document.forms[0].skipreservetypes.value = window.opener.document.forms[0].skipreservetypes.value + ' ' + sXML;
				        }
				        amount = document.forms[0].txtsetres.value;
				        break;
				}
				
			}
			if(amount<=0 )
				allowSubmit=false;

			if(allowSubmit==true)
			{
			document.forms[0].newamount.value=amount;
			return true;			
			}
			return false;
		}
		function replaceAll(s, searchFor, replaceWith) {
		    while (s.indexOf(searchFor) != -1) {
		        s = s.replace(searchFor, replaceWith);
		    }
		    return s;
		}