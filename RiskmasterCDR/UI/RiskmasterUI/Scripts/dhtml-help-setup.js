DHTMLHelp_setup = function (params) {
	function param_default(pname, def) { if (typeof params[pname] == "undefined") { params[pname] = def; } };

	param_default("inputField",     null);
	param_default("displayArea",    null);
	param_default("button",         null);
	param_default("eventName",      "click");
	param_default("align",          "Br");
	param_default("onSelect",       null);
	param_default("onClose",        null);
	param_default("onUpdate",       null);
	param_default("onCreate",       null);
	param_default("position",       null);
	param_default("width",       null);
	param_default("height",      null);

	var tmp = ["inputField", "displayArea", "button"];
	for (var i in tmp) {
		if (typeof params[tmp[i]] == "string") {
			params[tmp[i]] = document.getElementById(params[tmp[i]]);
		}
	}
	if (!(params.inputField || params.displayArea || params.button)) {
		//Abhishek: Commenting it as in case of readonly form we will not have controls and we dont want to show this message
		//alert("DHTMLPopup.setup:\n  Nothing to setup (no fields found).  Please check your code");
		return false;
	}

	function onSelect(popup) {
		// SAMPLE   var p = cal.params;
		// SAMPLE   var update = (cal.dateClicked);
		// SAMPLE   if (update && p.inputField) {
		// SAMPLE   	p.inputField.value = cal.date.print(p.ifFormat);
		// SAMPLE   	if (typeof p.inputField.onchange == "function")
		// SAMPLE   		p.inputField.onchange();
		// SAMPLE   }
		// SAMPLE   if (update && p.displayArea)
		// SAMPLE   	p.displayArea.innerHTML = cal.date.print(p.daFormat);
		// SAMPLE   if (update && typeof p.onUpdate == "function")
		// SAMPLE   	p.onUpdate(cal);
		// SAMPLE - Will close popup if something is selected		if (update && p.singleClick && cal.dateClicked)
		// SAMPLE - Will close popup if something is selected			cal.callCloseHandler();
	};

	// The onCreate function is where the content for the popup is drawn.
	// popup.tbody points to a div that is the "client" area that is available.
	// popup.title is the title area and can be used to display a caption for the popup window.
	function HtmlEncode(text){return text.replace(/&/g,'&amp').replace(/'/g,'&quot;').replace(/</g,'&lt;').replace(/>/g,'&gt;');}

	function onCreate(popup) {
		popup.title.innerHTML = "Help";
		
		var sBody = "<table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td valign='top'><img src='../../Images/bulb-large.gif'></td><td valign='top' style='padding-left: 1em'>"
		sBody += HtmlEncode(params.helpmsg);
		sBody += "</td></tr></table>";
		popup.tbody.innerHTML = sBody;

	};
	
	var triggerEl = params.button || params.displayArea || params.inputField;
	triggerEl["on" + params.eventName] = function() {
		var cal = null;
		
		window.calendar = cal = new DHTMLPopup(params.onSelect || onSelect,
							    params.onClose || function(cal) { cal.hide(); },
							    onCreate);
		mustCreate = true;

		cal.width = params["width"];
		cal.height = params["height"];
		cal.params = params;

		cal.create();
		
		cal.showAtElement(params.button || params.displayArea || params.inputField, params.align);
		return false;
	};

};
