//        var _mergeFailureErrorMessage = "<h3>Document Edit Failure Information</h3><br/>If you cannot resolve the problem using this information please call technical support."
//+ "<br/><br/><hr/>To complete the edit you must have these Internet Explorer security settings:"
//+ "<ul><li>In Internet Explorer click 'Tools' - 'Internet Options' - 'Security' - 'Local Intranet' - 'Sites' - 'Advanced'</li>"
//+ "<li>If you are using Windows Vista, and your RMX domain is reached via an Intranet, you must uncheck 'Automatically detect intranet network"
//+ "<li>Also on the 'Security' panel, with 'Intranet' selected, click 'Advanced'</li>"
//+ "<li>The domain of the Riskmaster application must NOT be included as an Intranet site (an example of a domain is 'https://www.YourRiskmasterSite.com'.  If your Riskmaster domain is listed as an Intranet site it must be removed from that list.</li>"
//+ "<li>In Internet Explorer click 'Tools' - 'Internet Options' - 'Security' - 'Trusted Sites' - 'Sites'</li>"
//+ "<li>The domain of the Riskmaster application MUST be included as a trusted site (an example of a domain is 'https://www.YourRiskmasterSite.com'</li>"
//+ "<li>Also on the 'Security' panel, with 'Trusted Sites' selected, click 'custom level':"
//    + "<ul>"
//        + "<li>'Initialize and script ActiveX Objects not marked as safe...' must be set to either 'Prompt' or 'Enabled'</li>"
//        + "<li>'Access data sources across domains' must be set to either 'Prompt' or 'Enabled'</li>"
//    + "</ul>"
//+ "</li>"
//+ "<li>Click 'OK' to accept the changes, then close and re-open your browser before trying again.</li></ul>"
        
        var Editor_Launched = 0;
        function DisplayError(errMsg) {
            //alert(errMsg);
            //document.write('<h3>Error Message</h3><p>' + errMsg + '</p><br />' + _mergeFailureErrorMessage);
            document.write('<h3>' + EditDocValidations.jsErrorMessage + '</h3><p>' + errMsg + '</p><br />' + EditDocValidations.jsFailureInfo);
            return false;
        }
        function DisposeAll()
        {
           CheckCloseToCall();           
        }
        function InitPageSettingsOnBodyLoad()
        {
            InitPageSettings();

            //enalbe Launch Merge button
            var btnLaunchMerge = document.getElementById("btnLaunchMerge");
            if (btnLaunchMerge != null)
            {
                btnLaunchMerge.disabled = false;
            }
        }
        function InitPageSettings() {
       
            var step = '';
            //get name of template document temporary file
            try {
                if (document.getElementById('hdnTempFileName') != null) {
                    if (document.getElementById('hdnTempFileName').value + "" != "") {
                        strFile = document.getElementById('hdnTempFileName').value;
                    }
                    else {
                        strFile = '';
                    }
                }
                else {
                    strFile = '';
                }
            }
            catch (e) { ; }

            //get full path to where the template document will be written on client machine
            try {
                //step = 'create Scripting.FileSystemObject';
                step = EditDocValidations.jsCreate;
                fso = new ActiveXObject("Scripting.FileSystemObject");
                //step = 'retrieve GetSpecialFolder(2).Path';
                step = EditDocValidations.jsRetrieve;
                var path = fso.GetSpecialFolder(2).Path;
                strForm = path + "\\" + strFile;                
                return true;
            }
            catch (e) {
                if(navigator.userAgent.toLowerCase().indexOf("firefox") != -1) {
                    //strForm = "Mail Merge is not supported on Firefox due to Broswer's limitation to support ActiveX Control";
                    strForm = EditDocValidations.jsFirefoxIssue;
                    return false;
                }

                //strForm = "Unable to discover user's special folder because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step;
                strForm = EditDocValidations.jsFolderIssue + e.name + ',' + e.message + '\r\n' + EditDocValidations.jsStep + ' ' + step;
                return false;
            }

        }
        function IsPPTOpen()
        {
            //Handle Case where User has not yet closed Word. (Do not open duplicate windows.)
            try
            {

                if (objWord != null)
                {
                    if (objWord.Presentations == null || objWord.Presentations.Count == 0)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {

                    return false;
                }

            }
            catch (e) { return false; }

        }
        function IsWordOpen() {
            //Handle Case where User has not yet closed Word. (Do not open duplicate windows.)
            try {

                if (objWord.visible != null)
                    return true;
                else
                    return false;
            }
            catch (e) { return false; }

        }
        function OpenWordEditor(sFileName, saveToDisk) {
            var wordDoc = null;
            var step = "";
            var isProtected = false;

            try {//open template
                //step = 'open word document: ' + strForm;
                step = EditDocValidations.jsOpenWordDoc + ' ' + strForm;
                objWord = new ActiveXObject("Word.Application");
                wordDoc = objWord.Documents.Open(strForm);

                //Try to save the document to make MSWord's SaveDate function work
                objWord.ActiveDocument.Saveas(strForm)
            }
            catch (e) {
                //DisplayError("The word document at " + strForm + " could not be opened by Microsft Word because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
                DisplayError(EditDocValidations.jsWordDoc1Error + ' ' + strForm + ' ' + EditDocValidations.jsWordDoc2Error + ' ' + e.name + ',' + e.message + '\r\n' + EditDocValidations.jsStep + ' ' + step);
                
                if (objWord != null) {
                    objWord.Quit(0);
                    objWord = null;
                    return false;
                }
            }

            try {//Verify we have full edit rights.	Isprotected added by pawan for 10983								   
                if (objWord.ActiveDocument.ProtectionType != -1) //if not totally unlocked then
                {
                    objWord.ActiveDocument.UnProtect("");
                    isProtected = true;
                }
            }
            catch (e) {
                //alert("The word document at " + strForm + " is protected from changes.\nPlease unlock it and click the file name again. ");
                alert(EditDocValidations.jsWordDoc1Error + ' ' + strForm + ' ' + EditDocValidations.jsWordDoc3Error.split('\\n').join('\n'));
                objWord.visible = true;
                objWord.Activate();
            }

            try {
                //step = 'activate Word';
                step = EditDocValidations.jsActivateWord;
                objWord.visible = true;
                objWord.Activate();

                objWord.ActiveDocument.Select();

                if (saveToDisk) {
                    //step = 'save the document over the original at ' + strForm;
                    step = EditDocValidations.jsDocumentSaving + ' ' + strForm;
                    objWord.ActiveDocument.Saveas(strForm);
                }

            }
            catch (e) {
                //DisplayError("Microsoft Word could not accomplish the editing because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
                DisplayError(EditDocValidations.jsWordDoc4Error + ' ' + e.name + ',' + e.message + '\r\n' + EditDocValidations.jsStep + ' ' + step);
                objWord.Quit(0);
                objWord = null;
                return false;
            }
            //Done
        }
        function pausecomp(millis)
        {
            var date = new Date();
            var curDate = null;

            do { curDate = new Date(); }
            while (curDate - date < millis);
        }
        var popUpObj;
        function LoadModalDiv()
        {

            var bcgDiv = document.getElementById("divBackground");

            bcgDiv.style.display = "block";

            if (bcgDiv != null)
            {

                if (document.body.clientHeight > document.body.scrollHeight)
                {

                    bcgDiv.style.height = document.body.clientHeight + "px";

                }

                else
                {

                    bcgDiv.style.height = document.body.scrollHeight + "px";

                }

                bcgDiv.style.width = "100%";

            }

        }
        function HideModalDiv()
        {

            var bcgDiv = document.getElementById("divBackground");

            bcgDiv.style.display = "none";

        } 



        function showModalPopUp()
        {

            popUpObj = window.open("PopUp.aspx",

    "ModalPopUp",

    "toolbar=no," +

    "scrollbars=no," +

    "location=no," +

    "statusbar=no," +

    "menubar=no," +

    "resizable=0," +

    "width=100," +

    "height=100," +

    "left = 490," +

    "top=300"

    );

            popUpObj.focus();

            LoadModalDiv();
            return popUpObj;

        } 


        function CheckEditorToCall(AX, saveToDisk)
        {
           
            var officetype = GetOfficeType();
            switch (officetype)
            {
                case "word":
                    OpenWordEditor(AX, saveToDisk);
                    break;
                case "xls":
                case "csv":
                    OpenExcelEditor(AX, saveToDisk);
                     break;
                case "ppt":
                    OpenPPTEditor(AX, saveToDisk);
                    break;
            }

            
        }
        function CheckFinishToCall(AX, saveToDisk)
        {
            
            var officetype = GetOfficeType();
            switch (officetype)
            {
                case "word":
                    return FinishWordEdit();
                    break;
                case "xls":
                case "csv":
                    return FinishEditExcel(AX, saveToDisk);
                    break;
                case "ppt":
                    return FinishEditPPT(AX, saveToDisk);
                    break;
            }
        }
        function CheckCloseToCall(AX, saveToDisk)
        {

            
            var officetype = GetOfficeType();
            m_DataChanged = false;
            switch (officetype)
            {
                case "word":
                    return CleanAndCancelWord();
                    break;
                case "xls":
                case "csv":
                    return CleanAndCancelXls();
                    break;
                case "ppt":
                    return CleanAndCancelPPT();
                    break;
            }
        }
        function OpenExcelEditor(sFileName, saveToDisk)
        {
            var wordDoc = null;
            var step = "";
            var isProtected = false;

            try
            {//open template
                //step = 'open excel document: ' + strForm;
                step = EditDocValidations.jsOpenExcel + ' ' + strForm;
                objWord = new ActiveXObject("Excel.Application");
                wordDoc = objWord.Workbooks.Open(strForm);

                //Try to save the document to make MSWord's SaveDate function work
                // objWord.ActiveWorkbook.Saveas(strForm)
            }
            catch (e)
            {
                //DisplayError("The document at " + strForm + " could not be opened by Microsft Excel because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
                DisplayError(EditDocValidations.jsExcel1Error + ' ' + strForm + ' ' + EditDocValidations.jsExcel2Error + ' ' + e.name + ',' + e.message + '\r\n' + EditDocValidations.jsStep + ' ' + step);
                if (objWord != null)
                {
                    objWord.Quit(0);
                    objWord = null;
                    return false;
                }
            }

            try
            {//Verify we have full edit rights.	Isprotected added by pawan for 10983
                if (objWord.ActiveWorkbook.ProtectionType != -1) //if not totally unlocked then
                {
                    objWord.ActiveWorkbook.UnProtect("");
                    isProtected = true;
                }
            }
            catch (e)
            {
                //alert("The word template at " + strForm + " is protected from changes.\nPlease unlock it and click the [Launch Word] button again. ");
                objWord.visible = true;
                objWord.Activate();
            }

            try
            {
                //step = 'activate excel';
                step = EditDocValidations.jsActivateExcel;
                objWord.visible = true;
                //objWord.Activate();

                // objWord.ActiveWorkbook.Select();

                //                if (saveToDisk) {
                //                    step = 'save the merged document over the original at ' + strForm;
                //                    objWord.ActiveWorkbook.Saveas(strForm);
                //                }

            }
            catch (e)
            {
                //DisplayError("Microsoft Excel could not accomplish the editing because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
                DisplayError(EditDocValidations.jsExcel3Error + ' ' + e.name + ',' + e.message + '\r\n' + EditDocValidations.jsStep + step);
                objWord.Quit(0);
                objWord = null;
                return false;
            }
            //Done
        }
        function OpenPPTEditor(sFileName, saveToDisk)
        {
            var wordDoc = null;
            var step = "";
            var isProtected = false;

            try
            {//open template
                //step = 'open ppt document: ' + strForm;
                step = EditDocValidations.jsOpenPPTDoc + ' ' + strForm;
                objWord = new ActiveXObject("PowerPoint.Application");
                objWord.visible = true;
                wordDoc = objWord.Presentations.Open(strForm);

                //Try to save the document to make MSWord's SaveDate function work
                // objWord.ActiveWorkbook.Saveas(strForm)
            }
            catch (e)
            {
                //DisplayError("The document at " + strForm + " could not be opened by Microsft PowerPoint because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
                DisplayError(EditDocValidations.jsPPT1Error + ' ' + strForm + ' ' + EditDocValidations.jsPPT2Error + ' ' + e.name + ',' + e.message + '\r\n' + EditDocValidations.jsStep + ' ' + step);
                if (objWord != null)
                {
                    objWord.Quit(0);
                    objWord = null;
                    return false;
                }
            }

            try
            {//Verify we have full edit rights.	Isprotected added by pawan for 10983
                if (objWord.ActivePresentation.ProtectionType != -1) //if not totally unlocked then
                {
                    //objWord.ActivePresentation.UnProtect("");
                    isProtected = true;
                }
            }
            catch (e)
            {
                //alert("The word template at " + strForm + " is protected from changes.\nPlease unlock it and click the [Launch Word] button again. ");
                objWord.visible = true;
                //objWord.Activate();
            }

            try
            {
                //step = 'activate ppt';
                step = EditDocValidations.jsActivatePPT;
                objWord.visible = true;
                //objWord.Activate();

                // objWord.ActiveWorkbook.Select();

                //                if (saveToDisk) {
                //                    step = 'save the merged document over the original at ' + strForm;
                //                    objWord.ActiveWorkbook.Saveas(strForm);
                //                }

            }
            catch (e)
            {
                //DisplayError("Microsoft PowerPoint could not accomplish the editing because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
                DisplayError(EditDocValidations.jsPPT3Error + ' ' + e.name + ',' + e.message + '\r\n' + EditDocValidations.jsStep + ' ' + step);
                objWord.Quit(0);
                objWord = null;
                return false;
            }
            //Done
        }
        function StartEditor(AX, saveToDisk)
        {
            
            if (AX == '') {
                //tkr 8/2007 HACK.
                //could not figure out how to get the error page to show from here.
                //the xpl logic is incredibly complex and fragile, and throws
                //confusing error message when clicked twice
                if (!InitPageSettings()) {
                    //InitPageSettings() sets strForm to the error message
                    //alert('ddd');
                    return DisplayError(strForm);
                }
            }
            else {
                return false;
            }
            setDataChanged(true);
            //strForm = strForm + "x";
            var adTypeBinary = 1;
            var adSaveCreateOverWrite = 2;
            var adTypeText = 2;
            //var step = 'check IsWordOpen';
            var step = EditDocValidations.jsEditorStep1;

            if (IsWordOpen()) {
                //step = 'activate Word';
                step = EditDocValidations.jsEditorStep2;
                objWord.Activate();
                return false;
            }

            try {//save template and datasource local
                //use DomDocument to convert base64 string to bytes
                //step = 'get document base64 text';
                step = EditDocValidations.jsEditorStep3;
                var template = (document.getElementById('hdnContent').value);
                if (template == null || template == "")
                    throw new Error(EditDocValidations.jsEditor1Error);


                //step = 'instantiate xml document for document';
                step = EditDocValidations.jsEditorStep4;
                var xmlDoc = new ActiveXObject("MSXML2.DOMDocument");
                //step = 'create b64 element for template';
                step = EditDocValidations.jsEditorStep5;
                var e = xmlDoc.createElement("b64");
                //step = 'set element.dataType to bin.base64 for document';
                step = EditDocValidations.jsEditorStep6;
                e.dataType = "bin.base64";

                //step = 'set element.text to document base64 text';
                step = EditDocValidations.jsEditorStep7;
                e.text = template;
                //step = 'get bytes from document element';
                step = EditDocValidations.jsEditorStep8;
                var templateBytes = e.nodeTypedValue;

                //use ADODB.Stream to save template bytes to disc
                // step = 'instantiate Stream object for document';
                step = EditDocValidations.jsEditorStep9;
                var stm = new ActiveXObject("ADODB.Stream");
                //step = 'set template stm.Type';
                step = EditDocValidations.jsEditorStep10;
                stm.Type = adTypeBinary;
                //step = 'open document stream';
                step = EditDocValidations.jsEditorStep11;
                stm.Open();
                //step = 'write bytes to document stream';
                step = EditDocValidations.jsEditorStep12;
                stm.Write(templateBytes);
                //step = 'save document stream to file ' + strForm;
                step = EditDocValidations.jsEditorStep13 + ' ' + strForm;
                stm.SaveToFile(strForm, adSaveCreateOverWrite);

                //step = 'close document';
                step = EditDocValidations.jsEditorStep14;
                stm.Close();
                stm = null;


                e = null;
                xmlDoc = null;
            }
            catch (e) {
                //return DisplayError("The document could not be saved to this machine for editing because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
                return DisplayError(EditDocValidations.jsEditor2Error + ' ' + e.name + ',' + e.message + '\r\n' + EditDocValidations.jsStep + ' ' + step);
            }

            try {
                //In the future if Office 2003 support is not required, should call WordMerge4Vesion11Lower12Higher
                //and change data source content.
                //Function call Changed by pawan for mits 10983 reverted back to routine used by Tom since few properties worked well well with it.
                CheckEditorToCall(strForm, saveToDisk);
                //objWord.FileSaveAs(strForm);
            }
            catch (e) {
                //return DisplayError("The local Microsft application required to complete the editing could not be loaded because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
                return DisplayError(EditDocValidations.jsEditor3Error + ' ' + e.name + ',' + e.message + '\r\n' + EditDocValidations.jsStep + ' ' + step);
            }

            Editor_Launched = 1;
            return false;

        }
        function CleanAndCancelWord()
        {
            try
            {
                if (objWord) { }
            }
            catch (e)
            {
                return;
            }
            
            try
                {
                    pleaseWait.Show();
                if (IsWordOpen())
                {
                    objWord.Quit(0);
                    objWord = null;
                }

                //Clean Up.
                if (fso.FileExists(strForm)) //Main doc temp file.
                    fso.DeleteFile(strForm);
            }
            catch (e)
	{ ; }
            //Shruti for 7337
         
            return true;
        }
        function CleanAndCancelPPT()
        {
            try
            {
                if (objWord) { }
            }
            catch (e)
            {
                return;
            }
            try
                {
                    pleaseWait.Show();
                if (IsPPTOpen())
                {
                  //  alert("You must 'Close' the opened power point document.");
                    //return false;
                    if (objWord.ActivePresentation)
                    {
                        objWord.ActivePresentation.Close();
                    }
                }
                objWord.Quit();
                objWord = null;
                idTmr = window.setInterval("Cleanup();", 1);
                //Clean Up.
                if (fso.FileExists(strForm)) //Main doc temp file.
                    fso.DeleteFile(strForm);
            }
            catch (e)
	{ ; }
            //Shruti for 7337

            return true;
        }
        function CleanAndCancelXls()
        {
            try
            {
                if (objWord) { }
                if (   objWord ==null) {return; }
            }
            catch (e)
                {                 
                return true;
            }
                
            if (Editor_Launched == 0)
            {
                return true;
            }
            try
                {
                    
                 
                    try
                    {
                        //testing the below condition to check if excel is opened / not
                        if (objWord.Workbooks)
                        {
                        }
                    }
                    catch (e)
                    {
                        //alert("You must 'Close' the opened excel document.");
                        alert(EditDocValidations.jsCloseExcel);
                        return false;
                    }

                    pleaseWait.Show();
                    if (objWord.ActiveWorkbook)
                    {
                        objWord.ActiveWorkbook.Close(0);
                    }

                    objWord.Quit();
                        objWord = null;
                        idTmr = window.setInterval("Cleanup();", 1);

                    

                //Clean Up.
                if (fso.FileExists(strForm)) //Main doc temp file.
                    fso.DeleteFile(strForm);
            }
            catch (e)
	{ ; }
            //Shruti for 7337

            return true;
        }
        function callBack() {
            document.forms[0].action = "WordDoc.aspx?reg=1"
            document.forms[0].submit();
        }
        function FinishEditExcel()
        {
            
            var step = '';
            
            try
            {
                if (Editor_Launched == 0)
                {
                     return true;
                }
                try
                {
                    //testing the below condition to check if excel is opened / not
                    
                    if (objWord.Workbooks)
                    {
                    }
                }
                catch (e)
                {
                    //alert("You must 'Save' and 'Close' edited excel document.");
                    alert(EditDocValidations.jsSaveAndCloseExcel);
                    return false;
                }
                pleaseWait.Show();
                if (IsWordOpen() && objWord.Workbooks.Count == 1)
                {
                    //step = 'activate Excel';
                    step = EditDocValidations.jsActivateExcel;
                    //objWord.Activate();

                    //step = 'get pointer to document';
                    step = EditDocValidations.jsFinishedEditStep1;
                    var wordDoc = objWord.Workbooks.Item(1);

                    //step = 'select, save and close the document';
                    step = EditDocValidations.jsFinishedEditStep2;
                    // wordDoc.Select();
                    wordDoc.Save();
                    wordDoc.Close(0);
                 
                }
                
                var adTypeBinary = 1;
                //step = 'instantiate Stream object';
                step = EditDocValidations.jsFinishedEditStep3;
                var stm = new ActiveXObject("ADODB.Stream");
                //step = 'set stm.Type';
                step = EditDocValidations.jsFinishedEditStep4;
                stm.Type = adTypeBinary;
                //step = 'open stream';
                step = EditDocValidations.jsFinishedEditStep5;
                stm.Open();
                step = EditDocValidations.jsFinishedEditStep6 + ' ' + strForm + ' ' + EditDocValidations.jsFinishedEditStep7;
                stm.LoadFromFile(strForm);
                //step = 'read bytes from stream';
                step = EditDocValidations.jsFinishedEditStep8;
                var bytes = stm.Read();
                //step = 'close stream';
                step = EditDocValidations.jsFinishedEditStep9;
                stm.Close();
                stm = null;

                //step = 'instantiate xml document';
                step = EditDocValidations.jsFinishedEditStep10;
                var xmlDoc = new ActiveXObject("MSXML2.DOMDocument");
                xmlDoc.async = false;
                //step = 'create root element';
                step = EditDocValidations.jsFinishedEditStep11;
                var xmlElement = xmlDoc.createElement("root");
                xmlDoc.documentElement = xmlElement;
                xmlElement.setAttribute("xmlns:dt", "urn:schemas-microsoft-com: datatypes ");
                //step = 'create element to hold document';
                step = EditDocValidations.jsFinishedEditStep12;
                var xmlNode = xmlElement.appendChild(xmlDoc.createElement("wordDoc"));
                xmlNode.dataType = "bin.base64"
                //step = 'read stream into element';
                step = EditDocValidations.jsFinishedEditStep13;
                xmlNode.nodeTypedValue = bytes;
                //step = 'read base64 text from element';
                step = EditDocValidations.jsFinishedEditStep14;
                var base64 = xmlNode.text;

                //step = 'cleanup';
                step = EditDocValidations.jsFinishedEditStep15;
                xmlDoc = null;
                xmlElement = null;
                xmlNode = null;

                document.all.hdnRTFContent.value = base64;
            }
            catch (e)
            {
                //alert("The local copy of the document at " + strForm + " could not be saved back into Riskmaster because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
                alert(EditDocValidations.jsFinishedEdit1Error + ' ' + strForm + '' + EditDocValidations.jsFinishedEdit2Error + ' ' + e.name + ',' + e.message + '\r\n' + EditDocValidations.jsStep + ' ' + step);
                xmlDoc = null;
                xmlElement = null;
                xmlNode = null;
                objWord = null;
                idTmr = window.setInterval("Cleanup();", 1);

                return false;
            }

            //Clean Up    
            try
            {
                //step = 'close word';
                step = EditDocValidations.jsFinishedEditStep16;
                if (IsWordOpen())
                {
                    objWord.Quit();
                    objWord = null;
                    idTmr = window.setInterval("Cleanup();", 1);

                }

                //step = 'delete local copy of document at ' + strForm;
                step = EditDocValidations.jsFinishedEditStep17 + ' ' + strForm;
                if (fso.FileExists(strForm)) //Main doc temp file.
                    fso.DeleteFile(strForm);


            }
            catch (e)
            {
                //alert("The local copy of the document could not be cleaned up because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
                alert(EditDocValidations.jsFinishedEdit3Error + ' ' + e.name + ',' + e.message + '\r\n' + EditDocValidations.jsStep + ' ' + step);
            }

            //document.getElementById('hdnaction').value="AttachAndFinish";
            
            document.forms[0].submit();
            return true;
        }
        function FinishWordEdit() {
            var step = '';

            try {
                if (Editor_Launched == 0) {
                      return true;
                }
                pleaseWait.Show();
                if (IsWordOpen() && objWord.Documents.Count == 1) {
                    //step = 'activate Word';
                    step = EditDocValidations.jsActivateWord;
                    objWord.Activate();

                    //step = 'get pointer to document';
                    step = EditDocValidations.jsFinishedEditStep1;
                    var wordDoc = objWord.Documents.Item(1);

                    //step = 'select, save and close the document';
                    step = EditDocValidations.jsFinishedEditStep2;
                    wordDoc.Select();
                    wordDoc.Save();
                    wordDoc.Close();
                }

                var adTypeBinary = 1;
                //step = 'instantiate Stream object';
                step = EditDocValidations.jsFinishedEditStep3;
                var stm = new ActiveXObject("ADODB.Stream");
                //step = 'set stm.Type';
                step = EditDocValidations.jsFinishedEditStep4;
                stm.Type = adTypeBinary;
                //step = 'open stream';
                step = EditDocValidations.jsFinishedEditStep5;
                stm.Open();
                step = EditDocValidations.jsFinishedEditStep6 + ' ' + strForm + ' ' + EditDocValidations.jsFinishedEditStep7;
                stm.LoadFromFile(strForm);
                //step = 'read bytes from stream';
                step = EditDocValidations.jsFinishedEditStep8;
                var bytes = stm.Read();
                //step = 'close stream';
                step = EditDocValidations.jsFinishedEditStep9;
                stm.Close();
                stm = null;

                //step = 'instantiate xml document';
                step = EditDocValidations.jsFinishedEditStep10;
                var xmlDoc = new ActiveXObject("MSXML2.DOMDocument");
                xmlDoc.async = false;
                //step = 'create root element';
                step = EditDocValidations.jsFinishedEditStep11;
                var xmlElement = xmlDoc.createElement("root");
                xmlDoc.documentElement = xmlElement;
                xmlElement.setAttribute("xmlns:dt", "urn:schemas-microsoft-com: datatypes ");
                //step = 'create element to hold document';
                step = EditDocValidations.jsFinishedEditStep12;
                var xmlNode = xmlElement.appendChild(xmlDoc.createElement("wordDoc"));
                xmlNode.dataType = "bin.base64"
                //step = 'read stream into element';
                step = EditDocValidations.jsFinishedEditStep13;
                xmlNode.nodeTypedValue = bytes;
                //step = 'read base64 text from element';
                step = EditDocValidations.jsFinishedEditStep14;
                var base64 = xmlNode.text;

                //step = 'cleanup';
                step = EditDocValidations.jsFinishedEditStep15;
                xmlDoc = null;
                xmlElement = null;
                xmlNode = null;
               
                document.all.hdnRTFContent.value = base64;
            }
            catch (e) {
                //alert("The local copy of the document at " + strForm + " could not be saved back into Riskmaster because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
                alert(EditDocValidations.jsFinishedEdit1Error + ' ' + strForm + ' ' + EditDocValidations.jsFinishedEdit2Error + ' ' + e.name + ',' + e.message + '\r\n' + EditDocValidations.jsStep + ' ' + step);
                xmlDoc = null;
                xmlElement = null;
                xmlNode = null;
                return false;
            }

            //Clean Up    
            try {
                //step = 'close word';
                step = EditDocValidations.jsFinishedEditStep16;
                if (IsWordOpen()) {
                    objWord.Quit(0);
                    objWord = null;
                }

                //step = 'delete local copy of document at ' + strForm;
                step = EditDocValidations.jsFinishedEditStep17 + ' ' + strForm;
                if (fso.FileExists(strForm)) //Main doc temp file.
                    fso.DeleteFile(strForm);


            }
            catch (e) {
                //alert("The local copy of the document could not be cleaned up because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
                alert(EditDocValidations.jsFinishedEdit3Error + ' ' + e.name + ',' + e.message + '\r\n' + EditDocValidations.jsStep + ' ' + step);
            }

           
            document.forms[0].submit();
            return true;
        }
        function GetOfficeType()
        {
            var doctype = document.forms[0].officeDocumentType.value;
            return doctype;
        }
        function FinishEditPPT()
        {
            var step = '';

            try
            {
                if (Editor_Launched == 0)
                {
                    return true;
                }
                pleaseWait.Show();
                if (IsPPTOpen() && objWord.Presentations.Count == 1)
                {
                    //step = 'activate PPT';
                    step = EditDocValidations.jsActivatePPT;
                    //objWord.Activate();

                    //step = 'get pointer to document';
                    step = EditDocValidations.jsFinishedEditStep1;
                    var wordDoc = objWord.Presentations.Item(1);

                    //step = 'select, save and close the document';
                    step = EditDocValidations.jsFinishedEditStep2;
                    // wordDoc.Select();
                    wordDoc.Save();
                    wordDoc.Close();
                }
                
                var adTypeBinary = 1;
                //step = 'instantiate Stream object';
                step = EditDocValidations.jsFinishedEditStep3;
                var stm = new ActiveXObject("ADODB.Stream");
                //step = 'set stm.Type';
                step = EditDocValidations.jsFinishedEditStep4;
                stm.Type = adTypeBinary;
                //step = 'open stream';
                step = EditDocValidations.jsFinishedEditStep5;
                stm.Open();
                //step = 'read file ' + strForm + ' into stream';
                step = EditDocValidations.jsFinishedEditStep6 + ' ' + strForm + ' ' + EditDocValidations.jsFinishedEditStep7;
                stm.LoadFromFile(strForm);
                //step = 'read bytes from stream';
                step = EditDocValidations.jsFinishedEditStep8;
                var bytes = stm.Read();
                //step = 'close stream';
                step = EditDocValidations.jsFinishedEditStep9;
                stm.Close();
                stm = null;

                //step = 'instantiate xml document';
                step = EditDocValidations.jsFinishedEditStep10;
                var xmlDoc = new ActiveXObject("MSXML2.DOMDocument");
                xmlDoc.async = false;
                //step = 'create root element';
                step = EditDocValidations.jsFinishedEditStep11;
                var xmlElement = xmlDoc.createElement("root");
                xmlDoc.documentElement = xmlElement;
                xmlElement.setAttribute("xmlns:dt", "urn:schemas-microsoft-com: datatypes ");
                //step = 'create element to hold document';
                step = EditDocValidations.jsFinishedEditStep12;
                var xmlNode = xmlElement.appendChild(xmlDoc.createElement("wordDoc"));
                xmlNode.dataType = "bin.base64"
                //step = 'read stream into element';
                step = EditDocValidations.jsFinishedEditStep13;
                xmlNode.nodeTypedValue = bytes;
                //step = 'read base64 text from element';
                step = EditDocValidations.jsFinishedEditStep14;
                var base64 = xmlNode.text;

                try
                {
                    //step = 'close PPT';
                    step = EditDocValidations.jsClosePPT;
                    if (objWord!=null)
                    {
                        objWord.Quit();
                        objWord = null;
                        idTmr = window.setInterval("Cleanup();", 1);

                    }

                    //step = 'delete local copy of document at ' + strForm;
                    step = EditDocValidations.jsFinishedEditStep17 + ' ' + strForm;
                    if (fso.FileExists(strForm)) //Main doc temp file.
                        fso.DeleteFile(strForm);


                }
                catch (e)
                {
                    //alert("The local copy of the document could not be cleaned up because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
                    alert(EditDocValidations.jsFinishedEdit3Error + ' ' + e.name + ',' + e.message + '\r\n' + EditDocValidations.jsStep + ' ' + step);
                }
                xmlDoc = null;
                xmlElement = null;
                xmlNode = null;

                document.all.hdnRTFContent.value = base64;
            }
            catch (e)
            {
                //alert("The local copy of the document at " + strForm + " could not be saved back into Riskmaster because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
                alert(EditDocValidations.jsFinishedEdit1Error + ' ' + strForm + ' ' + EditDocValidations.jsFinishedEdit2Error + ' ' + e.name + ',' + e.message + '\r\n' + EditDocValidations.jsStep + ' ' + step);
                xmlDoc = null;
                xmlElement = null;
                xmlNode = null;
                objWord = null;
                idTmr = window.setInterval("Cleanup();", 1);

                return false;
            }

            //Clean Up    
            try
            {
                //step = 'close word';
                step = EditDocValidations.jsFinishedEditStep16;
                if (IsPPTOpen())
                {
                    objWord.Quit();
                    objWord = null;
                    idTmr = window.setInterval("Cleanup();", 1);

                }

                //step = 'delete local copy of document at ' + strForm;
                step = EditDocValidations.jsFinishedEditStep17 + ' ' + strForm;
                if (fso.FileExists(strForm)) //Main doc temp file.
                    fso.DeleteFile(strForm);


            }
            catch (e)
            {
                //alert("The local copy of the document could not be cleaned up because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
                alert(EditDocValidations.jsFinishedEdit3Error + ' ' + e.name + ',' + e.message + '\r\n' + EditDocValidations.jsStep + ' ' + step);
            }

            //document.getElementById('hdnaction').value="AttachAndFinish";
            document.forms[0].submit();
            return true;
        }

        function Cleanup()
        {
            window.clearInterval(idTmr);
            CollectGarbage();
        }
 