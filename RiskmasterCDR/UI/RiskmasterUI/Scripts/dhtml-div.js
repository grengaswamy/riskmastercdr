/*  Copyright Mihai Bazon, 2002-2005  |  www.bazon.net/mishoo
 * -----------------------------------------------------------
 *
 * The DHTML Calendar, version 1.0 "It is happening again"
 *
 * Details and latest version at:
 * www.dynarch.com/projects/calendar
 *
 * This script is developed by Dynarch.com.  Visit us at www.dynarch.com.
 *
 * This script is distributed under the GNU Lesser General Public License.
 * Read the entire license text here: http://www.gnu.org/licenses/lgpl.html
 */

// $Id: calendar.js,v 1.51 2005/03/07 16:44:31 mishoo Exp $

/** The DHTMLPopup object constructor. */
DHTMLPopup = function (onSelected, onClose, onCreate) {
	// member variables
	this.activeDiv = null;
	this.currentDateEl = null;
	this.timeout = null;
	this.onSelected = onSelected || null;
	this.onClose = onClose || null;
	this.onCreate = onCreate || null;  // create hook - allows subclass to create content
	this.dragging = false;
	this.hidden = false;
	this.isPopup = true;

	// HTML elements
	this.table = null;
	this.element = null;
	this.tbody = null;
	this.firstdayname = null;

	// Information
	this.dateClicked = false;

};

// ** constants

/// "static", needed for event handlers.
DHTMLPopup._C = null;

/// detect a special case of "web browser"
DHTMLPopup.is_ie = ( /msie/i.test(navigator.userAgent) &&
		   !/opera/i.test(navigator.userAgent) );

DHTMLPopup.is_ie5 = ( DHTMLPopup.is_ie && /msie 5\.0/i.test(navigator.userAgent) );

/// detect Opera browser
DHTMLPopup.is_opera = /opera/i.test(navigator.userAgent);

/// detect KHTML-based browsers
DHTMLPopup.is_khtml = /Konqueror|Safari|KHTML/i.test(navigator.userAgent);

// BEGIN: UTILITY FUNCTIONS; beware that these might be moved into a separate
//        library, at some point.

DHTMLPopup.getAbsolutePos = function(el) {
	var SL = 0, ST = 0;
	var is_div = /^div$/i.test(el.tagName);
	if (is_div && el.scrollLeft)
		SL = el.scrollLeft;
	if (is_div && el.scrollTop)
		ST = el.scrollTop;
	var r = { x: el.offsetLeft - SL, y: el.offsetTop - ST };
	if (el.offsetParent) {
		var tmp = this.getAbsolutePos(el.offsetParent);
		r.x += tmp.x;
		r.y += tmp.y;
	}
	return r;
};

DHTMLPopup.isRelated = function (el, evt) {
	var related = evt.relatedTarget;
	if (!related) {
		var type = evt.type;
		if (type == "mouseover") {
			related = evt.fromElement;
		} else if (type == "mouseout") {
			related = evt.toElement;
		}
	}
	while (related) {
		if (related == el) {
			return true;
		}
		related = related.parentNode;
	}
	return false;
};

DHTMLPopup.removeClass = function(el, className) {
	if (!(el && el.className)) {
		return;
	}
	var cls = el.className.split(" ");
	var ar = new Array();
	for (var i = cls.length; i > 0;) {
		if (cls[--i] != className) {
			ar[ar.length] = cls[i];
		}
	}
	el.className = ar.join(" ");
};

DHTMLPopup.addClass = function(el, className) {
	DHTMLPopup.removeClass(el, className);
	el.className += " " + className;
};

// FIXME: the following 2 functions totally suck, are useless and should be replaced immediately.
DHTMLPopup.getElement = function(ev) {
	var f = DHTMLPopup.is_ie ? window.event.srcElement : ev.currentTarget;
	while (f.nodeType != 1 || /^div$/i.test(f.tagName))
		f = f.parentNode;
	return f;
};

DHTMLPopup.getTargetElement = function(ev) {
	var f = DHTMLPopup.is_ie ? window.event.srcElement : ev.target;
	while (f.nodeType != 1)
		f = f.parentNode;
	return f;
};

DHTMLPopup.stopEvent = function(ev) {
	ev || (ev = window.event);
	if (DHTMLPopup.is_ie) {
		ev.cancelBubble = true;
		ev.returnValue = false;
	} else {
		ev.preventDefault();
		ev.stopPropagation();
	}
	return false;
};

DHTMLPopup.addEvent = function(el, evname, func) {
	if (el.attachEvent) { // IE
		el.attachEvent("on" + evname, func);
	} else if (el.addEventListener) { // Gecko / W3C
		el.addEventListener(evname, func, true);
	} else {
		el["on" + evname] = func;
	}
};

DHTMLPopup.removeEvent = function(el, evname, func) {
	if (el.detachEvent) { // IE
		el.detachEvent("on" + evname, func);
	} else if (el.removeEventListener) { // Gecko / W3C
		el.removeEventListener(evname, func, true);
	} else {
		el["on" + evname] = null;
	}
};

DHTMLPopup.createElement = function(type, parent) {
	var el = null;
	if (document.createElementNS) {
		// use the XHTML namespace; IE won't normally get here unless
		// _they_ "fix" the DOM2 implementation.
		el = document.createElementNS("http://www.w3.org/1999/xhtml", type);
	} else {
		el = document.createElement(type);
	}
	if (typeof parent != "undefined") {
		parent.appendChild(el);
	}
	return el;
};

// END: UTILITY FUNCTIONS

// BEGIN: DHTMLPopup OBJECT FUNCTIONS

/** Internal function.  Starts dragging the element. */
DHTMLPopup.prototype._dragStart = function (ev) {
	if (this.dragging) {
		return;
	}
	this.dragging = true;
	var posX;
	var posY;
	if (DHTMLPopup.is_ie) {
		posY = window.event.clientY + document.body.scrollTop;
		posX = window.event.clientX + document.body.scrollLeft;
	} else {
		posY = ev.clientY + window.scrollY;
		posX = ev.clientX + window.scrollX;
	}
	var st = this.element.style;
	this.xOffs = posX - parseInt(st.left);
	this.yOffs = posY - parseInt(st.top);
	with (DHTMLPopup) {
		addEvent(document, "mousemove", popDragIt);
		addEvent(document, "mouseup", popDragEnd);
	}
};

// event handlers


DHTMLPopup.popDragIt = function (ev) {
	var cal = DHTMLPopup._C;
	if (!(cal && cal.dragging)) {
		return false;
	}
	var posX;
	var posY;
	if (DHTMLPopup.is_ie) {
		posY = window.event.clientY + document.body.scrollTop;
		posX = window.event.clientX + document.body.scrollLeft;
	} else {
		posX = ev.pageX;
		posY = ev.pageY;
	}
	cal.hideShowCovered();
	var st = cal.element.style;
	st.left = (posX - cal.xOffs) + "px";
	st.top = (posY - cal.yOffs) + "px";
	return DHTMLPopup.stopEvent(ev);
};

DHTMLPopup.popDragEnd = function (ev) {
	var cal = DHTMLPopup._C;
	if (!cal) {
		return false;
	}
	cal.dragging = false;
	with (DHTMLPopup) {
		removeEvent(document, "mousemove", popDragIt);
		removeEvent(document, "mouseup", popDragEnd);
		// NEEDED?    tableMouseUp(ev);
	}
	cal.hideShowCovered();
};

DHTMLPopup.popMouseDown = function(ev) {
	var el = DHTMLPopup.getElement(ev);
	if (el.disabled) {
		return false;
	}
	
	var cal = el.calendar;
	if (el.navType == 200) // close button
	{
		cal.callCloseHandler();	
	}
	else
	{
		cal.activeDiv = el;
		DHTMLPopup._C = cal;
		cal._dragStart(ev);
	}
	
	return DHTMLPopup.stopEvent(ev);
};

/**
 *  This function creates the DHTMLPopup inside the BODY element.
 *  Some properties need to be set before calling this function.
 */
DHTMLPopup.prototype.create = function () {
	var parent = null;
	parent = document.getElementsByTagName("body")[0];

	// TO DO: NEEDED??   Calendar.addEvent(table, "mousedown", Calendar.tableMouseDown);

	var div = DHTMLPopup.createElement("div");
	this.element = div;
	div.className = "dhtml-div";
	div.style.position = "absolute";
	div.style.display = "none";
	if (this.width)
		div.style.width = this.width.toString() + "px";
	if (this.height)
		div.style.height = this.height.toString() + "px";
	
	var table = DHTMLPopup.createElement("table", div);
	this.table = table;
	table.cellSpacing = 0;
	table.cellPadding = 0;
	table.calendar = this;
	table.width="100%";
	table.height="100%";

	div.appendChild(table);

	var thead = DHTMLPopup.createElement("thead", table);
	var cell = null;
	var row = null;

	var cal = this;
	var hh = function (text, cs, navtype) {
		cell = DHTMLPopup.createElement("td", row);
		cell.colSpan = cs;
		cell.className = "button";
		cell.navType = navtype;
		cell.calendar = cal;
		cell.innerHTML = "<div unselectable='on'>" + text + "</div>";
		return cell;
	};

	row = DHTMLPopup.createElement("tr", thead);
	//var title_length = 6;
	//--title_length;

	// Add drag bar to top
	//hh("?", 1, 400).ttip = Calendar._TT["INFO"];
	this.title = hh("", 1, 300);
	this.title.className = "title";
	//this.title.ttip = Calendar._TT["DRAG_TO_MOVE"];
	this.title.style.cursor = "move";
	this.title.width = "90%";
	DHTMLPopup.addEvent(this.title, "mousedown", DHTMLPopup.popMouseDown); // Add handler to allow dragging of window

	// Add closing X
	var xClose = hh("&#x00d7;", 1, 200); // .ttip = Calendar._TT["CLOSE"];
	xClose.width = "10%";
	DHTMLPopup.addEvent(xClose, "mousedown", DHTMLPopup.popMouseDown); // Add handler to allow dragging of window
	
	var tbody = DHTMLPopup.createElement("tbody", table);
	//this.tbody = tbody;

	row = DHTMLPopup.createElement("tr", tbody);
	row.classname = "clear";
	row.height = "100%";
	
	var popBody = DHTMLPopup.createElement("td", row);
	popBody.colSpan = 2;
	popBody.classname = "clear";
	

	var divBody = DHTMLPopup.createElement("div", popBody);
	div.classname = "scrollablediv";
	this.tbody = divBody;  // This is the element that extenders will use to write content.
	
	// Call onCreate callback to create app specific content
	if (this.onCreate) {
		this.onCreate(this);
	}

	//popBody.className = "button";
	//popBody.innerHTML = "<div unselectable='on'>" + text + "</div>";
	
// SAMPLE	for (i = 6; i > 0; --i) {
// SAMPLE		row = Calendar.createElement("tr", tbody);
// SAMPLE		if (this.weekNumbers) {
// SAMPLE			cell = Calendar.createElement("td", row);
// SAMPLE		}
// SAMPLE		for (var j = 7; j > 0; --j) {
// SAMPLE			cell = Calendar.createElement("td", row);
// SAMPLE			cell.calendar = this;
// SAMPLE		}
// SAMPLE	}

	//debugger;
	parent.appendChild(this.element);
};



/** Calls the second user handler (closeHandler). */
DHTMLPopup.prototype.callCloseHandler = function () {
	if (this.onClose) {
		this.onClose(this);
	}
	// JP 8-25-2005    this.hideShowCovered();
	this.destroy();
};

/** Removes the DHTMLPopup object from the DOM tree and destroys it. */
DHTMLPopup.prototype.destroy = function () {
	var el = this.element.parentNode;
	el.removeChild(this.element);
	DHTMLPopup._C = null;
	window._DHTMLPopup = null;
};

/**
 *  Moves the DHTMLPopup element to a different section in the DOM tree (changes
 *  its parent).
 */
DHTMLPopup.prototype.reparent = function (new_parent) {
	var el = this.element;
	el.parentNode.removeChild(el);
	new_parent.appendChild(el);
};

// This gets called when the user presses a mouse button anywhere in the
// document, if the calendar is shown.  If the click was outside the open
// calendar this function closes it.
DHTMLPopup._checkPopup = function(ev) {
	var calendar = window._DHTMLPopup;
	if (!calendar) {
		return false;
	}
	var el = DHTMLPopup.is_ie ? DHTMLPopup.getElement(ev) : DHTMLPopup.getTargetElement(ev);
	for (; el != null && el != calendar.element; el = el.parentNode);
	if (el == null) {
		// calls closeHandler which should hide the calendar.
		window._DHTMLPopup.callCloseHandler();
		return DHTMLPopup.stopEvent(ev);
	}
};

// Key handler for entire page. Only function is to close popup if ESC is hit.
DHTMLPopup._keyEvent = function(ev) {
	var cal = window._DHTMLPopup;
	if (!cal)
		return false;
	
	if (DHTMLPopup.is_ie)
		ev = window.event;
		
	var act = (DHTMLPopup.is_ie || ev.type == "keypress"),
		K = ev.keyCode;
	
	if (act)
		switch (K) {
			case 27: // KEY esc
				cal.callCloseHandler();
			break;
			default:
				return false;
		}
	return DHTMLPopup.stopEvent(ev);
};

/** Shows the calendar. */
DHTMLPopup.prototype.show = function () {
	this.element.style.display = "block";
	this.hidden = false;
	window._DHTMLPopup = this;
	
	// Wire up some events on the whole document - catches esc key or click outside of calendar and shuts it down
	DHTMLPopup.addEvent(document, "keydown", DHTMLPopup._keyEvent);
	DHTMLPopup.addEvent(document, "keypress", DHTMLPopup._keyEvent);
	DHTMLPopup.addEvent(document, "mousedown", DHTMLPopup._checkPopup);
	
	this.hideShowCovered();
};

/**
 *  Hides the calendar.  Also removes any "hilite" from the class of any TD
 *  element.
 */
DHTMLPopup.prototype.hide = function () {
	// Remove events on entire document
	DHTMLPopup.removeEvent(document, "keydown", DHTMLPopup._keyEvent);
	DHTMLPopup.removeEvent(document, "keypress", DHTMLPopup._keyEvent);
	DHTMLPopup.removeEvent(document, "mousedown", DHTMLPopup._checkPopup);

	this.element.style.display = "none";
	this.hidden = true;
	this.hideShowCovered();
};

/**
 *  Shows the DHTMLPopup at a given absolute position (beware that, depending on
 *  the DHTMLPopup element style -- position property -- this might be relative
 *  to the parent's containing rectangle).
 */
DHTMLPopup.prototype.showAt = function (x, y) {
	var s = this.element.style;
	s.left = x + "px";
	s.top = y + "px";
	this.show();
};

/** Shows the DHTMLPopup near a given element. */
DHTMLPopup.prototype.showAtElement = function (el, opts) {
	var self = this;
	var p = DHTMLPopup.getAbsolutePos(el);
	if (!opts || typeof opts != "string") {
		this.showAt(p.x, p.y + el.offsetHeight);
		return true;
	}
	function fixPosition(box) {
		if (box.x < 0)
			box.x = 0;
		if (box.y < 0)
			box.y = 0;
		var cp = document.createElement("div");
		var s = cp.style;
		s.position = "absolute";
		s.right = s.bottom = s.width = s.height = "0px";
		document.body.appendChild(cp);
		var br = DHTMLPopup.getAbsolutePos(cp);
		document.body.removeChild(cp);
		if (DHTMLPopup.is_ie) {
			br.y += document.body.scrollTop;
			br.x += document.body.scrollLeft;
		} else {
			br.y += window.scrollY;
			br.x += window.scrollX;
		}
		var tmp = box.x + box.width - br.x;
		if (tmp > 0) box.x -= tmp;
		tmp = box.y + box.height - br.y;
//		if (tmp > 0) box.y -= tmp;
	};
	this.element.style.display = "block";
	DHTMLPopup.continuation_for_the_khtml_browser = function() {
		var w = self.element.offsetWidth;
		var h = self.element.offsetHeight;
		self.element.style.display = "none";
		var valign = opts.substr(0, 1);
		var halign = "l";
		if (opts.length > 1) {
			halign = opts.substr(1, 1);
		}
		// vertical alignment
		switch (valign) {
		    case "T": p.y -= h; break;
		    case "B": p.y += el.offsetHeight; break;
		    case "C": p.y += (el.offsetHeight - h) / 2; break;
		    case "t": p.y += el.offsetHeight - h; break;
		    case "b": break; // already there
		}
		// horizontal alignment
		switch (halign) {
		    case "L": p.x -= w; break;
		    case "R": p.x += el.offsetWidth; break;
		    case "C": p.x += (el.offsetWidth - w) / 2; break;
		    case "l": p.x += el.offsetWidth - w; break;
		    case "r": break; // already there
		}
		p.width = w;
		p.height = h + 40;
		fixPosition(p);
		self.showAt(p.x, p.y);
	};
	if (DHTMLPopup.is_khtml)
		setTimeout("DHTMLPopup.continuation_for_the_khtml_browser()", 10);
	else
		DHTMLPopup.continuation_for_the_khtml_browser();
};

DHTMLPopup.prototype.hideShowCovered = function () {
	if (!DHTMLPopup.is_ie && !DHTMLPopup.is_opera)
		return;
	function getVisib(obj){
		var value = obj.style.visibility;
		if (!value) {
			if (document.defaultView && typeof (document.defaultView.getComputedStyle) == "function") { // Gecko, W3C
				if (!DHTMLPopup.is_khtml)
					value = document.defaultView.
						getComputedStyle(obj, "").getPropertyValue("visibility");
				else
					value = '';
			} else if (obj.currentStyle) { // IE
				value = obj.currentStyle.visibility;
			} else
				value = '';
		}
		return value;
	};

	var tags = new Array("applet", "iframe", "select");
	var el = this.element;

	var p = DHTMLPopup.getAbsolutePos(el);
	var EX1 = p.x;
	var EX2 = el.offsetWidth + EX1;
	var EY1 = p.y;
	var EY2 = el.offsetHeight + EY1;

	for (var k = tags.length; k > 0; ) {
		var ar = document.getElementsByTagName(tags[--k]);
		var cc = null;

		for (var i = ar.length; i > 0;) {
			cc = ar[--i];

			p = DHTMLPopup.getAbsolutePos(cc);
			var CX1 = p.x;
			var CX2 = cc.offsetWidth + CX1;
			var CY1 = p.y;
			var CY2 = cc.offsetHeight + CY1;

			if (this.hidden || (CX1 > EX2) || (CX2 < EX1) || (CY1 > EY2) || (CY2 < EY1)) {
				if (!cc.__msh_save_visibility) {
					cc.__msh_save_visibility = getVisib(cc);
				}
				cc.style.visibility = cc.__msh_save_visibility;
			} else {
				if (!cc.__msh_save_visibility) {
					cc.__msh_save_visibility = getVisib(cc);
				}
				cc.style.visibility = "hidden";
			}
		}
	}
};



// global object that remembers the calendar
window._DHTMLPopup = null;
