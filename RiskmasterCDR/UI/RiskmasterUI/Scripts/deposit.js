parent.CommonValidations = parent.parent.CommonValidations;
if (parent.CommonValidations == undefined) {
    if (parent.opener != undefined) {
        if (parent.opener.parent != undefined) {
            if (parent.opener.parent.parent != undefined) {
                parent.CommonValidations = parent.opener.parent.parent.CommonValidations;
            }
        }
    }
}

	function freezeList( objList )
	{
		var sSelectedValue = null ;
		
		if( objList.selectedIndex != null )
			sSelectedValue = objList.options[objList.selectedIndex].value ;
		
		objList.onchange = function()
		{
			for (i=0; i<objList.options.length; i++)
				if( sSelectedValue != objList.options[i].value )
					objList.options[i].selected = false ;
				else
					objList.options[i].selected = true ;												
		}		
	}

function ClearDeposit()
{
	if( !window.document.getElementById( "voidflag" ).checked )
	{
		if( window.document.getElementById( "clearedflag" ).checked )	
		{
			if( confirm( "Are you sure you wish to mark this deposit as cleared?" ) )
			{
				ToogleVoidClearDate();
			}
			else
			{
				window.document.getElementById( "clearedflag" ).checked = false ;
			}
		}
		else
		{
			ToogleVoidClearDate();		
		}
	}
	else
	{
		window.document.getElementById( "clearedflag" ).checked = !window.document.getElementById( "clearedflag" ).checked ;
	}
		
}

function VoidDeposit()
{
	if( window.document.getElementById( "voidflag" ).checked )
	{
		if( confirm( "Are you sure you wish to void this deposit?" ) )
		{
			DisabledAll();
			setDataChanged( true );
		}	
		else
		{
			window.document.getElementById( "voidflag" ).checked = false ;
		}
	}	
	else
	{
		window.document.getElementById( "voidflag" ).checked = ! window.document.getElementById( "voidflag" ).checked ;
	}
}

function ToogleVoidClearDate()
{
//	window.document.getElementById( "voidcleardate" ).readOnly = !window.document.getElementById( "voidcleardate" ).readOnly ;
//	window.document.getElementById( "voidcleardatebtn" ).disabled = !window.document.getElementById( "voidcleardatebtn" ).disabled ;

//Parijat: Mits 11916
if(window.document.getElementById( "clearedflag" )!=null)
    {
	    if( window.document.getElementById( "clearedflag" ).checked )
	    {
		    if(window.document.getElementById( "voidcleardate" )!=null)
		    {
		    window.document.getElementById( "voidcleardate" ).readOnly = false ;
		    window.document.getElementById( "voidcleardate" ).style.backgroundColor="";
		    }
		   if(window.document.getElementById( "voidcleardatebtn" )!=null)
		    window.document.getElementById( "voidcleardatebtn" ).disabled = false ;
	    }
	    else
	    {
	    
		    if(window.document.getElementById( "voidcleardate" )!=null)
		    {
		    window.document.getElementById( "voidcleardate" ).readOnly = true ;
		    window.document.getElementById( "voidcleardate" ).style.backgroundColor="silver";
		    
		    }
		    if(window.document.getElementById( "voidcleardatebtn" )!=null)
		    window.document.getElementById( "voidcleardatebtn" ).disabled = true ;
	    }
	}
	setDataChanged( true );
}

function OnLoadDeposit()
{

    //zalam 04/28/2008 Mits:-9376 Start
    if (document.forms[0].depositid.value == 0) {
        //Sumit(09/30/2010)-MITS# 22269 - Added null check.
        if (window.document.getElementById("btnClearMultDeposits") != null) {
            window.document.getElementById("btnClearMultDeposits").disabled = true;
        }
    }
    //zalam 04/28/2008 Mits:-9376 End
    //Null Check added by Shivendu for MITS 11361
    if(window.document.getElementById( "clearedflag" )!=null)
    {
	    if( window.document.getElementById( "clearedflag" ).checked )
	    {
		    //Null Check added by Shivendu for MITS 11361
		    if(window.document.getElementById( "voidcleardate" )!=null)
		    {
		    window.document.getElementById( "voidcleardate" ).readOnly = false ;
		    window.document.getElementById( "voidcleardate" ).style.backgroundColor="";
		    }
		    //Null Check added by Shivendu for MITS 11361
		    if(window.document.getElementById( "voidcleardatebtn" )!=null)
		    window.document.getElementById( "voidcleardatebtn" ).disabled = false ;
	    }
	    else//Parijat: Mits 11916
	    {
	    
		    if(window.document.getElementById( "voidcleardate" )!=null)
		    {
		    window.document.getElementById( "voidcleardate" ).readOnly = true ;
		    window.document.getElementById( "voidcleardate" ).style.backgroundColor="silver";
		   
		    }
		    if(window.document.getElementById( "voidcleardatebtn" )!=null)
		    window.document.getElementById( "voidcleardatebtn" ).disabled = true ;
	    }
	}
	//Null Check added by Shivendu for MITS 11361
	 if(window.document.getElementById( "voidflag" )!=null)
     {	
	    
	    if( window.document.getElementById( "voidflag" ).checked )
		    DisabledAll(); 
	 }
	
}	

function DisabledAll()
{
	window.document.getElementById( "ctlnumber" ).readOnly = true ;
	window.document.getElementById( "voidflag" ).readOnly = true ;	
	window.document.getElementById( "clearedflag" ).readOnly = true ;
	window.document.getElementById( "voidcleardate" ).readOnly = true ;
	window.document.getElementById( "voidcleardatebtn" ).disabled = true ;					
	
	freezeList( window.document.getElementById( "subbankaccount" ) );
	freezeList( window.document.getElementById( "deposittype" ) );
	
	window.document.getElementById( "transdate" ).readOnly = true ;
	window.document.getElementById( "transdatebtn" ).disabled = true ;		
	window.document.getElementById( "amount" ).readOnly = true ;
	window.document.getElementById( "description" ).readOnly = true ;
	window.document.getElementById("descriptionbtnMemo").disabled = true;
	//Changed for MITS 15155 : Start
	//window.document.getElementById("adjustcode").readOnly = true;
	window.document.getElementById( "adjustcode_codelookup").readOnly = true;
	//	window.document.getElementById("adjustcodebtn").disabled = true;
	window.document.getElementById("adjustcode_codelookupbtn").disabled = true;
	//Changed for MITS 15155 : End
	
}
//Defect 002063 Fixed .Feb 2006 by Neelima
function GetDeposits()
{
	var sAccountId=document.forms[0].accountid.value;
	var sSubAccountId=document.forms[0].subbankaccount.value;
	//Changed for MITS 15155 : Start

	window.open('/RiskmasterUI/UI/BankAccount/clearmultiplechecks.aspx?AccountId='+sAccountId+'&SubAccountId='+sSubAccountId,'ClearMultipleChecks',
			'width=700,height=400'+',top='+(screen.availHeight-400)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
	//Changed for MITS 15155 : End
		
//	window.open('home?pg=riskmaster/BankAccount/ClearMultipleChecks&AccountId='+sAccountId+'&SubAccountId='+sSubAccountId,'ClearMultipleChecks',
//			'width=700,height=400'+',top='+(screen.availHeight-400)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
		
	return false;
}

function selectAll()
{    

	var sName, sType;
	for(var i = 0; i < document.forms[0].elements.length; i++)
	{
		sName = document.forms[0].elements[i].name;
		sType = document.forms[0].elements[i].type;
		//if( (sType == 'checkbox') && (sName.substring(0,4)=="chk_") )
		if( (sType == 'checkbox') && (sName.indexOf('checkedrow') !=-1 ))
		{
			document.forms[0].elements[i].checked = true;
		}
	}
	//document.getElementsByName("chkDeposit").checked = true;
	//var x = document.getElementsByName("chkDeposit");
	//x[0].checked = true;
	var x = document.getElementById("chkDeposit");
	x.checked = true;
	
	return false;
}	

function deselectAll()
{
	var sName, sType;
	
	//GridView1_ctl02_checkedrow
	
	for(var i = 0; i < document.forms[0].elements.length; i++)
	{
		sName = document.forms[0].elements[i].name;
		sType = document.forms[0].elements[i].type;
	//	if( (sType == 'checkbox') && (sName.substring(0,4)=="chk_") )
		if( (sType == 'checkbox') && (sName.indexOf('checkedrow') !=-1 ))
		{
			document.forms[0].elements[i].checked = false;
		}
	}
//	var x = document.getElementsByName("chkDeposit");
//	x[0].checked = false;


	var x = document.getElementById("chkDeposit");
	x.checked = false;
	
	return false;

}

function DepositClick()
{
	if( document.getElementById("chkDeposit").checked )
		selectAll();
	else
		deselectAll();
}
function ClearDeposits()
{
	var sName, sType, sId;
	var chkid="";
	var iCount = 0;
	for(var i = 0; i<document.forms[0].elements.length; i++)
	{
		sName = document.forms[0].elements[i].name;
		sType = document.forms[0].elements[i].type;
		sId = document.forms[0].elements[i].id;
		
		
//		if( (sType == 'checkbox') && (sName.substring(0,4)=="chk_") )
//		{
//				
//				if(document.forms[0].elements[i].checked == true)
//				{
//					if(chkid=="") chkid = chkid + sId.substring(4 , sId.length);
//					else chkid = chkid + ',' + sId.substring(4 , sId.length);
//				}
//		}	
	    
	    if( (sType == 'checkbox') && (sName.indexOf('checkedrow')!= -1) )
	    {
	        if(document.forms[0].elements[i].checked == true)
	            iCount = iCount + 1;
	    }
	
	}
	
//		if( (sType == 'checkbox') && (sName.substring(0,4)=="chk_") )
//		{
//				
//				if(document.forms[0].elements[i].checked == true)
//				{
//					if(chkid=="") chkid = chkid + sId.substring(4 , sId.length);
//					else chkid = chkid + ',' + sId.substring(4 , sId.length);
//				}
//		}	
	
	//if(chkid=="")
	if(iCount == 0)
	{
	    //alert("No deposit selected");
	    //alert(parent.CommonValidations.SelectDeposit);
	    alert(clearmultiplechecksValidations.SelectDeposit);
		return false;
	}
	
	//document.getElementById("selecteddepositids").value = chkid;
	document.getElementById("ouraction").value = "OK"
	return true;
}