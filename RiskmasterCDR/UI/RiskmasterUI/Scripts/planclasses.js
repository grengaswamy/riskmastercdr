function openAltWaitPeriodsWindow()
{
	var classrowid=document.forms[0].classrowid.value;
    //MITS 10235 : Raman Bhatia : Alternate Waiting Period Window does not save in a new class
    //Window should onle be opened in an existing class..
    if(classrowid<=0 || classrowid=="")
	{
		self.alert(EXISTREC_ALERT);
		return false;
	}
//	var codeWindow=window.open('home?pg=riskmaster/AltWaitPeriods/AltWaitPeriods&ClassRowId='+classrowid,'codeWindow',
//		'width=500,height=300'+',top='+(screen.availHeight-300)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');

	var codeWindow=window.open('../AltWaitPeriods/AltWaitPeriods.aspx?ClassRowId='+classrowid,'codeWindow',
		'width=500,height=300'+',top='+(screen.availHeight-300)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
}

function openBenefitCalcTableWindow()
{
	var classrowid=document.forms[0].classrowid.value;
    //MITS 10235 : Raman Bhatia : Alternate Waiting Period Window does not save in a new class
    //Window should onle be opened in an existing class..
    if(classrowid<=0 || classrowid=="")
	{
		self.alert(EXISTREC_ALERT);
		return false;
	}
//	var codeWindow=window.open('home?pg=riskmaster/BenefitCalcTable/TableData&classid='+classrowid,'codeWindow',
//		'width=500,height=300'+',top='+(screen.availHeight-300)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
    var codeWindow=window.open('../BenefitCalcTable/TableData.aspx?ClassRowId='+classrowid,'codeWindow',
		'width=500,height=300'+',top='+(screen.availHeight-300)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
}

function enableBenefit(ctl)
{
	var b=ctl.checked;
	if(!b)
		return false;
		
	var s=ctl.value;
	switch(s)
	{
		case "BENE_PRCTG_FLAG":
			document.forms[0].peramt.disabled=false;
			document.forms[0].flatamt.disabled=true;
			//document.forms[0].flatamt.value=0;   //akaur9 Multi Currency
			document.forms[0].tabledriven.disabled=true;
			document.forms[0].tabledrivenbenefits.disabled=true;
			document.forms[0].SuppFlatAmt.disabled=true;
			//document.forms[0].SuppFlatAmt.value=0;  //akaur9 Multi Currency
			document.forms[0].SuppPercent.disabled=false;
			//document.forms[0].tabledriven_cid.value=0;
			//document.forms[0].btnAddtabledrivenbenefits.disabled=true;
			//document.forms[0].btnDeletetabledrivenbenefits.disabled=true;
			//m_AllowEdit=false;
			break;
		case "BENE_FLAT_AMT_FLAG":
			document.forms[0].peramt.disabled=true;
			document.forms[0].peramt.value = 0;
			document.forms[0].flatamt.disabled=false;
			document.forms[0].tabledriven.disabled=true;
			document.forms[0].tabledrivenbenefits.disabled=true;
			document.forms[0].SuppPercent.disabled=true;
			document.forms[0].SuppPercent.value=0;
			document.forms[0].SuppFlatAmt.disabled=false;
			//document.forms[0].tabledriven_cid.value=0;
			//document.forms[0].btnAddtabledrivenbenefits.disabled=true;
			//document.forms[0].btnDeletetabledrivenbenefits.disabled=true;
			//m_AllowEdit=false;
			break;
		case "BENE_TD_FLAG":
			document.forms[0].peramt.disabled=true;
			document.forms[0].peramt.value = 0;
			document.forms[0].flatamt.disabled=true;
			//document.forms[0].flatamt.value=0;   //akaur9 Multi Currency
			document.forms[0].tabledriven.disabled=false;
			document.forms[0].tabledrivenbenefits.disabled=false;
			document.forms[0].SuppFlatAmt.disabled=true;
			//document.forms[0].SuppFlatAmt.value = 0;   //akaur9 Multi Currency
			document.forms[0].SuppPercent.disabled=true;
			document.forms[0].SuppPercent.value=0;
			//document.forms[0].tabledriven_cid.value=document.forms[0].tabledriven.value;
			//the select box is disabled if 1+ table driven benefit records exist
			//if(document.forms[0].tabledrivenbenefits_rows.value=='0')
			//	document.forms[0].tabledriven.disabled=false;
			//else
			//	document.forms[0].tabledriven.disabled=true;
			//document.forms[0].btnAddtabledrivenbenefits.disabled=false;
			//document.forms[0].btnDeletetabledrivenbenefits.disabled=false;
			//m_AllowEdit=true;
			break;
	}
	SetUpSupplementBuyUpOption();
	return true;
	
}

function UpdateReserveType(ctl)
{
	if(ctl.value == "")
	{
		document.forms[0].ReserveType.value = "";
		return true;
	}
	ReserveTypeIdString = "ReserveType_" + ctl.value;
	
	elem = document.getElementById(ReserveTypeIdString);
	document.forms[0].ReserveType.value = elem.value;
}
function UpdateBankAccountStatus()
{
	if(document.forms[0].chkSuppSepPay.checked == true && document.forms[0].chkSuppSepPay.disabled == false)
	   {
			document.forms[0].BankAccount.disabled = false;
	   }
	   else
	   {
			document.forms[0].BankAccount.disabled = true;
	   }	
	return true;
}
