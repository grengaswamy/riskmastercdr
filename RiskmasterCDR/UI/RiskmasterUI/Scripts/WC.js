document.AWWRateSelected=AWWRateSelected;
//Parijat: benefit calculations
function benefitCalculations()
{//Parijat
    var claimid=document.forms[0].claimid.value;
    //alert("claim:"+claimid);
    var codeWindow = window.open('/RiskmasterUI/UI/BenefitCalculator/BenCalc.aspx?ClaimID=' + claimid, 'benefitcalc',
			    'width=750,height=450'+',top='+(screen.availHeight-300)/2+',left='+(screen.availWidth-800)/2+',resizable=yes,scrollbars=yes');
    return false;
}
function awwratecalc()
{
	//Nikhil Garg		Defect No: 2495 
	var weeklyhours=0;
	var ctrl=eval('document.forms[0].empweeklyhours');
	if (ctrl!=null)
		weeklyhours=parseFloat(ctrl.value);
	else
	{
		ctrl=eval('document.forms[0].empweeklyhours1');
		if (ctrl!=null)
			weeklyhours=parseFloat(ctrl.value);
		else
		{
			ctrl=eval('document.forms[0].empweeklyhours2');
			if (ctrl!=null)
				weeklyhours=parseFloat(ctrl.value);
		}
	}
	if (weeklyhours>64)
	{
		alert('Employee Weekly hours can not be more than 64');
		return false;
	}

	if (m_DataChanged)
	//if user changed jurisdiction this effects rate calculation
	{
		alert('Please save your changes before using the rate calculator.');
		return false;
	}
	
	//if(m_codeWindow!=null)
	//	m_codeWindow.close();

	var claimid=document.forms[0].claimid.value;
	if(eval(claimid)<=0)
	{
		alert('Please save your changes before using the rate calculator.');
		return false;
	}
	else
	{
			var codeWindow=window.open('../AWWCalc/Calculator.aspx?claimid='+claimid,'awwratecalc',
			'width=730,height=500'+',top='+(screen.availHeight-300)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
	}
	return false;
}

function AWWRateSelected(dAWW,dConstantWage,sLastWorkWeek,iIncludeZeros,dBonuses,sWeekArray)
{
	document.forms[0].constantwage.value = dConstantWage;
	document.forms[0].lastworkweek.value = getdbDate(sLastWorkWeek);
	document.forms[0].includezeros.value= iIncludeZeros;
	document.forms[0].bonuses.value=dBonuses;
	document.forms[0].aww.value=dAWW;
	document.forms[0].empweeklyrate.value = dAWW;
	document.forms[0].weekarray.value = sWeekArray;
	
	if(m_codeWindow!=null)
		m_codeWindow.close();
		
	m_codeWindow=null;
	
	setDataChanged(true);
}

