function refreshPolicyBilling() {
    setDataChanged(false);
    document.forms[0].SysCmd.value="7";
    document.forms[0].submit();
}

function OnLinkClick(spageToOpen)
{
    var width = 650;
    var height = 450;
    // aravi5 RMA-10436 On selecting a new billing type page shows a js error. Starts
    // var PolicyNumber = document.forms[0].all("PolicyNumber").value;
    // var termNumber = document.forms[0].all("TermNumber").value;
    // var policyId = document.forms[0].all("PolicyId").value;

    var PolicyNumber = document.forms[0].PolicyNumber.value;
    var termNumber = document.forms[0].TermNumber.value;
    var policyId = document.forms[0].policyid.value;
    // aravi5 RMA-10436 On selecting a new billing type page shows a js error. Ends

    if(spageToOpen == "Receipt")
    {
//        var sReturnValue = window.open('/RiskmasterUI/UI/Billing/ReceiptFrame.aspx?PolicyId=' + policyId + '&TermNumber=' + termNumber + '&PolicyNumber=' + PolicyNumber, 'Adjustment', 'width=' + width + ',height=' + height + ',top=' + (screen.availHeight - height) / 2 + ',left=' + (screen.availWidth - width) / 2 + ',resizable=no,scrollbars=no,status=no,center=yes');
//        return false;
        // aravi5 RMA-10436 On selecting a new billing type page shows a js error. Starts
        // wnd = showModalDialog('/RiskmasterUI/UI/Billing/ReceiptFrame.aspx?PolicyId=' + policyId + '&TermNumber=' + termNumber + '&PolicyNumber=' + PolicyNumber, 'Receipt', 'dialogWidth:' + width + 'px;dialogHeight:' + height + 'px;dialogtop:' + (screen.availHeight - height) / 2 + ';dialogleft:' + (screen.availWidth - width) / 2 + ';resizable:no;scrollbars:no;status:no;center:yes;');
        var IsIEbrowser = false || !!document.documentMode; // At least IE6
        if (!IsIEbrowser) {
            var wnd = window.open('/RiskmasterUI/UI/Billing/ReceiptFrame.aspx?PolicyId=' + policyId + '&TermNumber=' + termNumber + '&PolicyNumber=' + PolicyNumber, 'Receipt', 'width=' + width + ',height=' + height + ',top=' + (screen.availHeight - height) / 2 + ',left=' + (screen.availWidth - width) / 2 + ';resizable:no;scrollbars:no;status:no;center:yes;');
            showOverlayPopup();
        }
        else {
            var wnd = showModalDialog('/RiskmasterUI/UI/Billing/ReceiptFrame.aspx?PolicyId=' + policyId + '&TermNumber=' + termNumber + '&PolicyNumber=' + PolicyNumber, 'Receipt', 'dialogWidth:' + width + 'px;dialogHeight:' + height + 'px;dialogtop:' + (screen.availHeight - height) / 2 + ';dialogleft:' + (screen.availWidth - width) / 2 + ';resizable:no;scrollbars:no;status:no;center:yes;');
            if (wnd == "ok") {
                window.returnValue = "ok";
            }
            window.close();
        }
        // aravi5 RMA-10436 On selecting a new billing type page shows a js error. Ends
    }
    else if(spageToOpen == "Adjustment")
    {
//        var sReturnValue = window.open('/RiskmasterUI/UI/Billing/AdjustmentFrame.aspx?PolicyId=' + policyId + '&TermNumber=' + termNumber + '&PolicyNumber=' + PolicyNumber, 'Adjustment', 'width=' + width + ',height=' + height + ',top=' + (screen.availHeight - height) / 2 + ',left=' + (screen.availWidth - width) / 2 + ',resizable=no,scrollbars=no,status=no,center=yes');
//        return false;
        // aravi5 RMA-10436 On selecting a new billing type page shows a js error. Starts
        //wnd = showModalDialog('/RiskmasterUI/UI/Billing/AdjustmentFrame.aspx?PolicyId='+policyId+'&TermNumber='+termNumber+'&PolicyNumber='+PolicyNumber,'Adjustment', 'dialogWidth:' + width + 'px;dialogHeight:'+ height +'px;dialogtop:'+(screen.availHeight-height)/2+';dialogleft:'+(screen.availWidth-width)/2+';resizable:no;scrollbars:no;status:no;center:yes;');
        var IsIEbrowser = false || !!document.documentMode; // At least IE6
        if (!IsIEbrowser) {
            var wnd = window.open('/RiskmasterUI/UI/Billing/AdjustmentFrame.aspx?PolicyId=' + policyId + '&TermNumber=' + termNumber + '&PolicyNumber=' + PolicyNumber, 'Adjustment', 'width=' + width + ',height=' + height + ',top=' + (screen.availHeight - height) / 2 + ',left=' + (screen.availWidth - width) / 2 + ';resizable:no;scrollbars:no;status:no;center:yes;');
            showOverlayPopup();
        }
        else {
            var wnd = showModalDialog('/RiskmasterUI/UI/Billing/AdjustmentFrame.aspx?PolicyId=' + policyId + '&TermNumber=' + termNumber + '&PolicyNumber=' + PolicyNumber, 'Adjustment', 'dialogWidth:' + width + 'px;dialogHeight:' + height + 'px;dialogtop:' + (screen.availHeight - height) / 2 + ';dialogleft:' + (screen.availWidth - width) / 2 + ';resizable:no;scrollbars:no;status:no;center:yes;');
            if (wnd == "ok") {
                window.returnValue = "ok";
            }
            window.close();
        }
        // aravi5 RMA-10436 On selecting a new billing type page shows a js error. Ends        
    }
    else if(spageToOpen == "Disbursement")
    {
//        var sReturnValue = window.open('/RiskmasterUI/UI/Billing/DisbursementFrame.aspx?PolicyId=' + policyId + '&TermNumber=' + termNumber + '&PolicyNumber=' + PolicyNumber, 'Adjustment', 'width=' + width + ',height=' + height + ',top=' + (screen.availHeight - height) / 2 + ',left=' + (screen.availWidth - width) / 2 + ',resizable=no,scrollbars=no,status=no,center=yes');
//        return false;
        // aravi5 RMA-10436 On selecting a new billing type page shows a js error. Starts
        // wnd = showModalDialog('/RiskmasterUI/UI/Billing/DisbursementFrame.aspx?PolicyId='+policyId+'&TermNumber='+termNumber+'&PolicyNumber='+PolicyNumber,'Disbursement', 'dialogWidth:' + width + 'px;dialogHeight:'+ height +'px;dialogtop:'+(screen.availHeight-height)/2+';dialogleft:'+(screen.availWidth-width)/2+';resizable:no;scrollbars:no;status:no;center:yes;');
        var IsIEbrowser = false || !!document.documentMode; // At least IE6
        if (!IsIEbrowser) {
            var wnd = window.open('/RiskmasterUI/UI/Billing/DisbursementFrame.aspx?PolicyId=' + policyId + '&TermNumber=' + termNumber + '&PolicyNumber=' + PolicyNumber, 'Disbursement', 'width=' + width + ',height=' + height + ',top=' + (screen.availHeight - height) / 2 + ',left=' + (screen.availWidth - width) / 2 + ';resizable:no;scrollbars:no;status:no;center:yes;');
            showOverlayPopup();
        }
        else {
            var wnd = showModalDialog('/RiskmasterUI/UI/Billing/DisbursementFrame.aspx?PolicyId=' + policyId + '&TermNumber=' + termNumber + '&PolicyNumber=' + PolicyNumber, 'Disbursement', 'dialogWidth:' + width + 'px;dialogHeight:' + height + 'px;dialogtop:' + (screen.availHeight - height) / 2 + ';dialogleft:' + (screen.availWidth - width) / 2 + ';resizable:no;scrollbars:no;status:no;center:yes;');
            if (wnd == "ok") {
                window.returnValue = "ok";
            }
            window.close();
        }
        // aravi5 RMA-10436 On selecting a new billing type page shows a js error. Ends        
    }
    
}

function SelectForReconciliation()
{
    if(!IsAnyBillingItemSelected())
    {    
        alert("Please select a Billing Item");
        return false;
    }
    else
    {              
        var gridElementsChkBox = document.getElementsByName("BillingItemsGrid");//RMA-10432 : getElementsByName('MyCheckBox') wont work in IE11 and Chrome
	    if(gridElementsChkBox != null)
	    {
	        for(var i=0;i<gridElementsChkBox.length;i++)
	        {
	            var gridName = gridElementsChkBox[i].name;
	            if(gridName == 'BillingItemsGrid' && gridElementsChkBox[i].checked)
	            {
	                //RMA-10432 : bkuzhanthaim : getElementById("BillingItemsGrid_gvData_ctl" + iPos + "_hfGrid") wont work in IE11 and Chrome            							   
	                var iPos = GetPositionForSelectedGridRowforChkbox(gridName, gridElementsChkBox[i].value);
	                /*if (iPos.toString().length == 1)
	                {				   
                        iPos = "0" + iPos ;
                    }*/
	                var sBillTypeStatus = document.getElementById("BillingItemsGrid_gvData_hfGrid_" + (iPos)).value;
                    aBillTypeStatus = sBillTypeStatus.split("_");
                    
                    sBillItemType = aBillTypeStatus[0];
                    sBillItemStatus = aBillTypeStatus[1];
                     
                    if(sBillItemStatus=="SR")
                    {  
                        alert("Cannot select items - At least one of the Billing Items selected has already been selected for reconciliation. Billing Items must all be in Accepted status in order to select them. Please modify selection.");
                        return false;
                    }
                    if(sBillItemStatus=="RC") 
                    {
                        alert("Cannot select items - At least one of the Billing Items selected has been reconciled. Billing Items must all be in Accepted status in order to select them. Please modify selection.");
                        return false;
                    }
				   
	        }
            }
        }               
        
        
    
            
//        aSelectedBillingItems=GetSelectedCheckBoxRowIdsForTheGrid("BillingItemsGrid");
//        for (i = 0; i < aSelectedBillingItems.length; i++)
//        {
//            Itemvalue=GetGridValueForRowColumn("BillingItemsGrid","BillItemStatus",aSelectedBillingItems[i])
//            if(Itemvalue=="SR")
//            {  
//                alert("Cannot select items - At least one of the Billing Items selected has already been selected for reconciliation. Billing Items must all be in Accepted status in order to select them. Please modify selection.");
//                return false;
//            }
//            if(Itemvalue=="RC") 
//            {
//                alert("Cannot select items - At least one of the Billing Items selected has been reconciled. Billing Items must all be in Accepted status in order to select them. Please modify selection.");
//                return false;
//            }
//        }
    }
    document.forms[0].hdnPostBackAction.value="SELECT_FOR_RECONCILIATION";
    document.forms[0].SysCmd.value="7";
    document.forms[0].submit();
    return false;
}

function UnSelectForReconciliation()
{
    if(!IsAnyBillingItemSelected())
    {    
        alert("Please select a Billing Item");
        return false;
    }
    else
    {   
//        aSelectedBillingItems=GetSelectedCheckBoxRowIdsForTheGrid("BillingItemsGrid");
//        for (i = 0; i < aSelectedBillingItems.length; i++)
//        {
//            Itemvalue=GetGridValueForRowColumn("BillingItemsGrid","BillItemStatus",aSelectedBillingItems[i])
//            if(Itemvalue=="OK")
//            {  
//                alert("Cannot unselect items - At least one of the Billing Items selected is in Accepted status. Billing Items must all be in Selected for Reconciliation status in order to unselect them. Please modify selection.");
//                return false;
//            }
//            if(Itemvalue=="RC") 
//            {
//                alert("Cannot unselect items - At least one of the Billing Items selected has been reconciled. Billing Items must all be in Selected for Reconciliation status in order to unselect them. Please modify selection.");
//                return false;
//            }
//        }
//    }


        var gridElementsChkBox = document.getElementsByName("BillingItemsGrid");//RMA-10432 : getElementsByName('MyCheckBox') wont work in IE11 and Chrome
        
	    if(gridElementsChkBox != null)
	    {
	        for(var i=0;i<gridElementsChkBox.length;i++)
	        {
	            var gridName = gridElementsChkBox[i].name;
	            if(gridName == 'BillingItemsGrid' && gridElementsChkBox[i].checked)
	            {
	                //RMA-10432 : bkuzhanthaim : getElementById("BillingItemsGrid_gvData_ctl" + iPos + "_hfGrid") wont work in IE11 and Chrome            							   
	                var iPos = GetPositionForSelectedGridRowforChkbox(gridName, gridElementsChkBox[i].value);
	                /* if (iPos.toString().length == 1)
	                {				   
                        iPos = "0" + iPos ;
                    }*/
	                var sBillTypeStatus = document.getElementById("BillingItemsGrid_gvData_hfGrid_" + iPos).value;
                    aBillTypeStatus = sBillTypeStatus.split("_");
                    
                    sBillItemType = aBillTypeStatus[0];
                    sBillItemStatus = aBillTypeStatus[1];
                    
                    if(sBillItemStatus=="OK")
                    {  
                        alert("Cannot unselect items - At least one of the Billing Items selected is in Accepted status. Billing Items must all be in Selected for Reconciliation status in order to unselect them. Please modify selection.");
                        return false;
                    }
                    if(sBillItemStatus=="RC") 
                    {
                        alert("Cannot unselect items - At least one of the Billing Items selected has been reconciled. Billing Items must all be in Selected for Reconciliation status in order to unselect them. Please modify selection.");
                        return false;
                    }                 
	            }
          }
        }
    }
    document.forms[0].hdnPostBackAction.value="UNSELECT_FOR_RECONCILIATION";
    document.forms[0].SysCmd.value="7";
    //document.forms[0].submit();
    return true;

}
    
function Reconciliation()
{
    sSelectedGridRow = "";
    if(!IsAnyBillingItemSelected())
    {    
        
//        for (i = 0; i < aSelectedBillingItems.length; i++)
//        {
//            Itemvalue=GetGridValueForRowColumn("BillingItemsGrid","BillItemStatus",aSelectedBillingItems[i])

//            if(Itemvalue!="OK")
//            {  
//                alert("One of the Billing Item has not been selected for reconciliation.");
//                return false;
//            }
//            if(Itemvalue=="RC") 
//            {
//                alert("Billing Item has already been reconciled.");
//                return false;
//            }
//        }
//        for 
//          alert("Can not reconcile just one Billing Item");
//          return false;
//    }
//    else
//    {    
        alert("Please select a Billing Item that has been selected for reconciliation.");
        return false;
    }
    else {

        var gridElementsChkBox = document.getElementsByName("BillingItemsGrid");//RMA-10432 : getElementsByName('MyCheckBox') wont work in IE11 and Chrome
        if (gridElementsChkBox != null) {
            for (var i = 0; i < gridElementsChkBox.length; i++) {
                var gridName = gridElementsChkBox[i].name;
                if (gridName == 'BillingItemsGrid' && gridElementsChkBox[i].checked) {
                    if (sSelectedGridRow != "")
                        sSelectedGridRow = sSelectedGridRow + "_" + gridElementsChkBox[i].value;
                    else
                        sSelectedGridRow = gridElementsChkBox[i].value;
                }
            }
            //aSelectedGridRowIDs = sSelectedGridRow.split("_");
        }  
    
    
//         var sSelectedBillingItems = "";
//         aSelectedBillingItems = GetSelectedCheckBoxRowIdsForTheGrid("BillingItemsGrid");
//         for (i = 0; i < aSelectedBillingItems.length; i++)
//         {
//            if( sSelectedBillingItems=="")
//            sSelectedBillingItems=aSelectedBillingItems[i];
//            else
//            sSelectedBillingItems=sSelectedBillingItems +"_"+ aSelectedBillingItems[i];
//         }

        //RMX.window.open("home?pg=riskmaster/Billing/ReconciliationDetails&sSelectedBillingItems=" + sSelectedGridRow, "ReconciliationDetails",
        //'width=400,height=200'+',top='+(screen.availHeight-400)/2+',left='+(screen.availWidth-400)/2+',resizable=no,scrollbars=no');   

        window.open('/RiskmasterUI/UI/Billing/Reconciliation.aspx?sSelectedBillingItems=' + sSelectedGridRow, "ReconciliationDetails",
                        'width=400,height=200' + ',top=' + (screen.availHeight - 400) / 2 + ',left=' + (screen.availWidth - 400) / 2 + ',resizable=no,scrollbars=no');
         
        return false;
    }
}
//Shruti for print disbursement
function PrintDisbursement() {
    var sQueryString = "" ;
	var width = 460;
	var height = 240 ;
	var sSelectedBillingItems = "";
	//	aSelectedBillingItems = GetSelectedCheckBoxRowIdsForTheGrid("BillingItemsGrid");
	var gridElementsChkBox = document.getElementsByName("BillingItemsGrid");//RMA-10432 : getElementsByName('MyCheckBox') wont work in IE11 and Chrome
	var sSelectedGridRow = "";

	if (gridElementsChkBox != null) {
	    for (var i = 0; i < gridElementsChkBox.length; i++) {
	        var gridName = gridElementsChkBox[i].name;
	        if (gridName == 'BillingItemsGrid' && gridElementsChkBox[i].checked) {
	            if (sSelectedGridRow != "")
	                sSelectedGridRow = sSelectedGridRow + "_" + gridElementsChkBox[i].value;
	            else
	                sSelectedGridRow = gridElementsChkBox[i].value;
	        }
	     
	    }
	    aSelectedGridRowIDs = sSelectedGridRow.split("_");
	}


	for (i = 0; i < aSelectedGridRowIDs.length; i++)
    {
        if( sSelectedBillingItems=="")
            sSelectedBillingItems = aSelectedGridRowIDs[i];
    }
	sQueryString = "SelectedBillingItem=" + sSelectedBillingItems;
	if(wnd == null)
	{
	    //var wnd=showModalDialog("home?pg=riskmaster/Billing/PrintDisbursementFrame&" + sQueryString ,null , 'dialogWidth:' + width + 'px;dialogHeight:'+ height +'px;dialogtop:'+(screen.availHeight-height)/2+';dialogleft:'+(screen.availWidth-width)/2+';resizable:yes;scrollbars:yes;status:no;center:yes;');
	    //var wnd=showModalDialog("/RiskmasterUI/UI/Billing/PrintDisbursement.aspx?" + sQueryString ,null , 'dialogWidth:' + width + 'px;dialogHeight:'+ height +'px;dialogtop:'+(screen.availHeight-height)/2+';dialogleft:'+(screen.availWidth-width)/2+';resizable:yes;scrollbars:yes;status:no;center:yes;');
	    // aravi5 changes for RMA-10157 Policy billing show modal dialogue issues Starts
	    if (get_browserName() == "IE") {
	        var wnd = showModalDialog("/RiskmasterUI/UI/Billing/PrintDisbursement.aspx?" + sQueryString, null, 'dialogWidth:' + width + 'px;dialogHeight:' + height + 'px;dialogtop:' + (screen.availHeight - height) / 2 + ';dialogleft:' + (screen.availWidth - width) / 2 + ';resizable:yes;scrollbars:yes;status:no;center:yes;');
	    }
	    else {
	        var wnd = window.open("/RiskmasterUI/UI/Billing/PrintDisbursement.aspx?" + sQueryString, null, 'Width=' + width + ',height=' + height + ',top=' + (screen.availHeight - height) / 2 + ',left=' + (screen.availWidth - width) / 2 + ',resizable:yes,scrollbars:yes,status:no,center:yes;');
	        showOverlay();
	    }
	    // aravi5 changes for RMA-10157 Policy billing show modal dialogue issues Ends
	}
	
	if( wnd == "ok" )
	{
	    document.forms[0].SysCmd.value="7";
	    document.forms[0].submit();
	    return true ;
	}
	else
	{
		return false ;
	}
}

function ResetDataChange()
{
    m_DataChanged=false;
}

function ReconcileOk()
{
    //Changed by Gagan for MITS 9775 : Start
    var ans = false;
    //Changed by Gagan for MITS 9775 : Start
    
    //Shruti for MITs 9579
    if (document.forms[0].BatchTotal.value != "$0.00" && document.forms[0].ReconciliationType_codelookup.value == 'E Exact')
    {
        alert("Cannot reconcile Exact; batch total is not zero. Please select another reconciliation type.");
        //eval("document.forms[0].ReconciliationType.focus();");
        eval("document.forms[0].ReconciliationType_codelookup.focus();");
        return;
    }
    else if (document.forms[0].ReconciliationType_codelookup.value != 'E Exact' && document.forms[0].BatchTotal.value == "$0.00")
    {
        alert("Regardless of selection, batch will reconcile Exact because the batch total is zero.");
    }


    if (document.forms[0].ReconciliationType_codelookup_cid.value != "" && document.forms[0].ReconciliationType_codelookup_cid.value != '0' && document.forms[0].hdnMessage.value != "Error")
     {
         //Changed by Gagan for MITS 9775 : Start
         if(document.forms[0].CheckPaidInFull.value == 'True')
         {
             ans = window.confirm("The customer has paid their premium in full. Do you wish to cancel the pay plan? ");       
         }
         //Changed by Gagan for MITS 9775 : End
         window.opener.document.forms[0].hdnReconcileType.value = document.forms[0].ReconciliationType_codelookup_cid.value;     
         window.opener.document.forms[0].hdnPostBackAction.value="RECONCILIATION";
         window.opener.document.forms[0].SysCmd.value="7";
         //Changed by Gagan for MITS 9775 : Start
         window.opener.document.forms[0].hdnCancelPayPlan.value = ans;
         //Changed by Gagan for MITS 9775 : End
         window.opener.document.forms[0].submit();
         window.close();
    }
    else 
       alert("Please choose Reconciliation Type.");

}

function IsAnyBillingItemSelected()
{
    var gridElementsChkBox = document.getElementsByName("BillingItemsGrid");//RMA-10432 : getElementsByName('MyCheckBox') wont work in IE11 and Chrome
        
	    if(gridElementsChkBox != null)
	    {
	        for(var i=0;i<gridElementsChkBox.length;i++)
	        {
	            var gridName = gridElementsChkBox[i].name;
	            if(gridName == 'BillingItemsGrid' && gridElementsChkBox[i].checked)
	            {
	                //selectedValue=gridElementsChkBox[i].value;
				    SelectedFound=true;
				    return true;        
				    //if(gridElementsChkBox[i].name!=null)
				    //{
				        //selectedId = gridElementsChkBox[i].name;
				    //}
	            }
            }
        }
        
        return false;        
}


//  aSelectedBillingItems = GetSelectedCheckBoxRowIdsForTheGrid("BillingItemsGrid");
//  if(aSelectedBillingItems[0]=="")
//     return false ;
//  else 
//     return true;
//}
function GetSelectedCheckBoxRowIdsForTheGrid( listname )
{   
    var sSelectedGridRow = "";
    var aSelectedGridRowIDs;
    inputs = document.getElementsByTagName("input");
	for (i = 0; i < inputs.length; i++)
	    if ((inputs[i].type=="checkbox") && (inputs[i].checked==true))
		    if (listname==inputs[i].id.substring(0,inputs[i].id.indexOf("|")))	
			{   
			    if(sSelectedGridRow!="")
				sSelectedGridRow = sSelectedGridRow + "_" +inputs[i].value;
				else 
				sSelectedGridRow = inputs[i].value;
			}
	aSelectedGridRowIDs = sSelectedGridRow.split("_");
	return( aSelectedGridRowIDs )	;	
}












function GetGridValueForRowColumn(GridName,CoulmnName,RowID)
{
    positionID =  GetPositionForSelectedGridRow(GridName,RowID );
    ctrl = self.document.getElementById(GridName+"|"+ positionID +"|"+CoulmnName+"|TD|");
    return (ctrl.innerText);
}

function OnReconciliationLoad()
{

	if(document.forms[0].hdnMessage.value=="Error")
		document.forms[0].btnOk.disabled=true;
}

function OnReceiptLoad() {
   
    //pageLoaded(); it throws errror for SysFormId  csingh7   
    //Manish Multicurrency
    if (document.getElementById("Amount").getAttribute("Amount") != "0")
     //if(trim(document.forms[0].Amount.value)!="$0.00")
    {
        document.forms[0].Amount.readOnly="readonly";
        document.forms[0].Amount.style.backgroundColor="silver";
    }
    if (document.getElementById("hdnMessage").value == "Saved") {
        //RMA-10257,RMA-10432 : bkuzhanthaim : Show modal dialog
        var parentWindow = null;
        var IEbrowser = false || !!document.documentMode;
        if (IEbrowser) parentWindow = window.dialogArguments;
        else {
            parentWindow = window.opener;
            if (window.opener.document.forms[0].action.indexOf("NewBillingItemTypeFrame") > -1) {
                parentWindow = parentWindow.opener;
                window.opener.close();
            }
            parentWindow.document.forms[0].SysCmd.value = "7";
            parentWindow.document.forms[0].submit();
        }
        window.returnValue = "ok";
        window.close();
    }
    //Add by kuladeep for MITS:24930 Start
    if (document.getElementById('Comments') != null) {
        document.getElementById('Comments').readOnly = "readonly";
    }
    //Add by kuladeep for MITS:24930 End
}
function OnAdjustmentLoad()
{
    //pageLoaded(); it throws errror for SysFormId  csingh7    
    if(document.getElementById("hdnMessage").value == "Saved")
    {
        //RMA-10257,RMA-10432 : bkuzhanthaim : Show modal dialog
        var IEbrowser = false || !!document.documentMode;
        if (IEbrowser) parentWindow = window.dialogArguments;
        else {
            parentWindow = window.opener;
            if (window.opener.document.forms[0].action.indexOf("NewBillingItemTypeFrame") > -1) {
                parentWindow = parentWindow.opener;
                window.opener.close();
            }
            parentWindow.document.forms[0].SysCmd.value = "7";
            parentWindow.document.forms[0].submit();
        }
        window.returnValue = "ok";
        window.close();
    }
    //Add by kuladeep for MITS:24930 Start
    if (document.getElementById('Comments') != null) {
        document.getElementById('Comments').readOnly = "readonly";
    }
    //Add by kuladeep for MITS:24930 End

}
function OnDisbursementLoad()
{
    //pageLoaded(); it throws errror for SysFormId  csingh7    

    if(document.getElementById("hdnMessage").value == "Saved")
    {
        //RMA-10257,RMA-10432 : bkuzhanthaim : Show modal dialog
        var IEbrowser = false || !!document.documentMode;
        if (IEbrowser) parentWindow = window.dialogArguments;
        else {
            parentWindow = window.opener;
            if (window.opener.document.forms[0].action.indexOf("NewBillingItemTypeFrame") > -1) {
                parentWindow = parentWindow.opener;
                window.opener.close();
            }
            parentWindow.document.forms[0].SysCmd.value = "7";
            parentWindow.document.forms[0].submit();
        }
        window.returnValue = "ok";
        window.close();
    }
    //Add by kuladeep for MITS:24930 Start
    if (document.getElementById('Comments') != null) {
        document.getElementById('Comments').readOnly = "readonly";
    }
    //Add by kuladeep for MITS:24930 End

}
function OnPremiumItemLoad()  
{
    //pageLoaded(); it throws errror for SysFormId  csingh7    
    document.getElementById("PremiumItemType").disabled = true;
}
function AddReceipt()
{   var width = 650;
    var height = 450;
    var PolicyNumber=document.getElementById("PolicyNumber").value;
    var termNumber=document.getElementById("TermNumber").value;
    var policyId = document.getElementById("policyid").value;
    // aravi5 changes for RMA-10157 Policy billing show modal dialogue issues Starts
    //wnd = showModalDialog('/RiskmasterUI/UI/Billing/ReceiptFrame.aspx?PolicyId='+policyId+'&TermNumber='+termNumber+'&PolicyNumber='+PolicyNumber,'Receipt', 'dialogWidth:' + width + 'px;dialogHeight:'+ height +'px;dialogtop:'+(screen.availHeight-height)/2+';dialogleft:'+(screen.availWidth-width)/2+';resizable:no;scrollbars:no;status:no;center:yes;');    
    if (get_browserName() == "IE") {
        var wnd = showModalDialog('/RiskmasterUI/UI/Billing/ReceiptFrame.aspx?PolicyId=' + policyId + '&TermNumber=' + termNumber + '&PolicyNumber=' + PolicyNumber, 'Receipt', 'dialogWidth:' + width + 'px;dialogHeight:' + height + 'px;dialogtop:' + (screen.availHeight - height) / 2 + ';dialogleft:' + (screen.availWidth - width) / 2 + ';resizable:no;scrollbars:no;status:no;center:yes;');
        if (wnd == "ok") {
            document.forms[0].SysCmd.value = "7";
            document.forms[0].submit();
        }
    }
    else {
        var wnd = window.open('/RiskmasterUI/UI/Billing/ReceiptFrame.aspx?PolicyId=' + policyId + '&TermNumber=' + termNumber + '&PolicyNumber=' + PolicyNumber, self,
                        'width=' + width + ',height=' + height + ',top=' + (screen.availHeight - height) / 2 + ',left=' + (screen.availWidth - width) / 2 + ',resizable=no,scrollbars=no');
        showOverlay();
    }
    // aravi5 changes for RMA-10157 Policy billing show modal dialogue issues Ends
    
    //return true ;
    return false;
}
function AddAdjustment()
{
    var width = 650;
    var height = 450;
    var PolicyNumber=document.getElementById("PolicyNumber").value;
    var termNumber=document.getElementById("TermNumber").value;
    var policyId=document.getElementById("policyid").value ;
    //wnd = showModalDialog('/RiskmasterUI/UI/Billing/AdjustmentFrame.aspx?PolicyId='+policyId+'&TermNumber='+termNumber+'&PolicyNumber='+PolicyNumber,'Adjustment', 'dialogWidth:' + width + 'px;dialogHeight:'+ height +'px;dialogtop:'+(screen.availHeight-height)/2+';dialogleft:'+(screen.availWidth-width)/2+';resizable:no;scrollbars:no;status:no;center:yes;');  
    //var sReturnValue =  window.open('/RiskmasterUI/UI/Billing/AdjustmentFrame.aspx?PolicyId='+policyId+'&TermNumber='+termNumber+'&PolicyNumber='+PolicyNumber,'Adjustment','width=' + width + ',height='+ height +',top='+(screen.availHeight-height)/2+',left='+(screen.availWidth-width)/2+',resizable=no,scrollbars=no,status=no,center=yes');
    // aravi5 changes for RMA-10157 Policy billing show modal dialogue issues Starts
    if (get_browserName() == "IE") {
        var wnd = showModalDialog('/RiskmasterUI/UI/Billing/AdjustmentFrame.aspx?PolicyId=' + policyId + '&TermNumber=' + termNumber + '&PolicyNumber=' + PolicyNumber, 'Adjustment', 'dialogWidth:' + width + 'px;dialogHeight:' + height + 'px;dialogtop:' + (screen.availHeight - height) / 2 + ';dialogleft:' + (screen.availWidth - width) / 2 + ';resizable:no;scrollbars:no;status:no;center:yes;');
        if (wnd == "ok") {
            document.forms[0].SysCmd.value = "7";
            document.forms[0].submit();
        }
    }
    else {
        var wnd = window.open('/RiskmasterUI/UI/Billing/AdjustmentFrame.aspx?PolicyId=' + policyId + '&TermNumber=' + termNumber + '&PolicyNumber=' + PolicyNumber, self,
                        'width=' + width + ',height=' + height + ',top=' + (screen.availHeight - height) / 2 + ',left=' + (screen.availWidth - width) / 2 + ',resizable=no,scrollbars=no');
        showOverlay();
    }
    // aravi5 changes for RMA-10157 Policy billing show modal dialogue issues Ends
    // return true ;
    return false;
}
function AddDisbursement()
{
    var width = 650;
    var height = 450;
    var PolicyNumber=document.getElementById("PolicyNumber").value;
    var termNumber=document.getElementById("TermNumber").value;
    var policyId = document.getElementById("policyid").value;
    // aravi5 changes for RMA-10157 Policy billing show modal dialogue issues Starts
    //wnd = showModalDialog('/RiskmasterUI/UI/Billing/DisbursementFrame.aspx?PolicyId='+policyId+'&TermNumber='+termNumber+'&PolicyNumber='+PolicyNumber,'Disbursement', 'dialogWidth:' + width + 'px;dialogHeight:'+ height +'px;dialogtop:'+(screen.availHeight-height)/2+';dialogleft:'+(screen.availWidth-width)/2+';resizable:no;scrollbars:no;status:no;center:yes;');
    //var sReturnValue =  window.open('/RiskmasterUI/UI/Billing/DisbursementFrame.aspx?PolicyId='+policyId+'&TermNumber='+termNumber+'&PolicyNumber='+PolicyNumber,'Adjustment','width=' + width + ',height='+ height +',top='+(screen.availHeight-height)/2+',left='+(screen.availWidth-width)/2+',resizable=no,scrollbars=no,status=no,center=yes');
    if (get_browserName() == "IE") {
        var wnd = showModalDialog('/RiskmasterUI/UI/Billing/DisbursementFrame.aspx?PolicyId=' + policyId + '&TermNumber=' + termNumber + '&PolicyNumber=' + PolicyNumber, 'Disbursement', 'dialogWidth:' + width + 'px;dialogHeight:' + height + 'px;dialogtop:' + (screen.availHeight - height) / 2 + ';dialogleft:' + (screen.availWidth - width) / 2 + ';resizable:no;scrollbars:no;status:no;center:yes;');
        if (wnd == "ok") {
            document.forms[0].SysCmd.value = "7";
            document.forms[0].submit();
        }
    }
    else {
        var wnd = window.open('/RiskmasterUI/UI/Billing/DisbursementFrame.aspx?PolicyId=' + policyId + '&TermNumber=' + termNumber + '&PolicyNumber=' + PolicyNumber, self,
                         'width=' + width + ',height=' + height + ',top=' + (screen.availHeight - height) / 2 + ',left=' + (screen.availWidth - width) / 2 + ',resizable=no,scrollbars=no');
        showOverlay();
    }
    // aravi5 changes for RMA-10157 Policy billing show modal dialogue issues Ends
        //return true ;
        return false;
}
function AdjustmentOkclick()
{
    var sSetFocus = "";
    var sSortCode = new Array();
    sSortCode = document.forms[0].AdjustmentType.value.split(',');
    if(sSortCode.length == 2)
    {
        if(sSortCode[1] == "OP" || sSortCode[1] == "UP" || sSortCode[1] == "WO" || sSortCode[1] == "BI")
        {
           alert("Invalid Adjustment Type");
           document.forms[0].AdjustmentType.value = "";
           eval("document.forms[0].AdjustmentType.focus();");
           return false; 
        }
    }
    if(document.forms[0].AdjustmentType.value == "" || document.forms[0].AdjustmentType.value == 0)
    {
        sSetFocus = "document.forms[0].AdjustmentType";
    }
    else if (document.getElementById("Amount").getAttribute("Amount") == "" || document.getElementById("Amount").getAttribute("Amount") == "0")
    {
	    sSetFocus = "document.forms[0].Amount";
	}
	if(sSetFocus != "")
	{
	    alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
		tabChange("Adjustment");
		eval(sSetFocus + ".focus();");
		return false;
	}
	else
	{
	    //document.forms[0].action.value="OK"; 
	    document.forms[0].hdnAction.value="OK";
        document.forms[0].submit();
    }
}

function ReciptOkclick() {
    
    var sSetFocus = "";
    if(document.forms[0].PaymentMethod.value==""|| document.forms[0].PaymentMethod.value == 0)
    { 
        sSetFocus = 'document.forms[0].PaymentMethod';
    }
    //Manish Multicurrency document.getElementById("Amount").getAttribute("Amount")
    if (document.getElementById("Amount").getAttribute("Amount") == "" || document.getElementById("Amount").getAttribute("Amount") == "0")
    //else if(trim(document.forms[0].Amount.value)=="" || trim(document.forms[0].Amount.value)=="$0.00" )
    {
	    sSetFocus = "document.forms[0].Amount";
	}
	if(sSetFocus != "")
	{
	    alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
		tabChange("Receipt");
		eval(sSetFocus + ".focus();");
		return false;
	}
	else
	{
        //document.forms[0].action.value="OK"; 
	    document.forms[0].hdnAction.value = "OK";
	    document.forms[0].submit();
    }
}
function DisbursementOkclick()
{
    var sSetFocus = "";
    if(document.forms[0].PaymentMethod.value==""|| document.forms[0].PaymentMethod.value == 0)
    { 
        sSetFocus = 'document.forms[0].PaymentMethod';
    }
    else if (document.getElementById("Amount").getAttribute("Amount") == "" || document.getElementById("Amount").getAttribute("Amount") == "0")
    {
	    sSetFocus = "document.forms[0].Amount";
	}
    else if(document.forms[0].BankAccount.value==""|| document.forms[0].BankAccount.value == 0)
    {
        sSetFocus = "document.forms[0].BankAccount";
    }
    else if(document.forms[0].TransactionType.value== ""|| document.forms[0].TransactionType.value == 0)
    {
        sSetFocus = "document.forms[0].TransactionType";
    }
    
	if(sSetFocus != "")
	{
	    alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
		tabChange("Disbursement");
		eval(sSetFocus + ".focus();");
		return false;
	}
	else
	{
        //document.forms[0].action.value="OK"; 
        document.forms[0].hdnAction.value="OK";
        document.forms[0].submit();
    }
}
function OnSelectBillingItem()
{
    var gridElementsChkBox = document.getElementsByName("BillingItemsGrid");//RMA-10432 : getElementsByName('MyCheckBox') wont work in IE11 and Chrome
    var btnPrintDisbursment = document.getElementById("btnPrintDisbursment");
    var sSelectedGridRow = "";
    var bButtonDisable = false;

	    if(gridElementsChkBox != null)
	    {
	        for(var i=0;i<gridElementsChkBox.length;i++)
	        {
	            var gridName = gridElementsChkBox[i].name;
	            if (gridName == 'BillingItemsGrid' && gridElementsChkBox[i].checked) {
	                if (sSelectedGridRow != "")
	                    sSelectedGridRow = sSelectedGridRow + "_" + gridElementsChkBox[i].value;
	                else
	                    sSelectedGridRow = gridElementsChkBox[i].value;
	                //RMA-10432 : bkuzhanthaim : getElementById("BillingItemsGrid_gvData_ctl" + iPos + "_hfGrid") wont work in IE11 and Chrome            							   
	                var iPos = GetPositionForSelectedGridRowforChkbox(gridName, gridElementsChkBox[i].value);
	                /*if (iPos.toString().length == 1)
                    {				   
                        iPos = "0" + iPos ;
                    }
                    //var sBillTypeStatus = document.getElementById("BillingItemsGrid_gvData_ctl" + iPos + "_hfGrid").value;
                    var sBillTypeStatus = document.getElementById("BillingItemsGrid_gvData_hfGrid_" + (iPos - 2)).value;*/

	                var sBillTypeStatus = document.getElementById("BillingItemsGrid_gvData_hfGrid_" + iPos).value;
	                aBillTypeStatus = sBillTypeStatus.split("_");

	                sBillItemType = aBillTypeStatus[0];
	                sBillItemStatus = aBillTypeStatus[1];

	                //  sBillItemType=GetGridValueForRowColumn("BillingItemsGrid","BillItemType",aSelectedBillingItems[i])
	                //  sBillItemStatus=GetGridValueForRowColumn("BillingItemsGrid","BillItemStatus",aSelectedBillingItems[i])
	                if (sBillItemType == "Disbursement" && sBillItemStatus == "RC") {
	                    bButtonDisable = true;
	                }
	                else {
	                    bButtonDisable = false;
	                    break;
	                }
	            }
	        }
        }
          btnPrintDisbursment.disabled = !bButtonDisable;
      }


      

function OpenPrintInvoicesNotices() {
    var width = screen.availWidth - 60;
    var height = screen.availHeight - 60;
    var sFileName = document.forms[0].filename.value;

    var sReturnValue = window.open("/RiskmasterUI/UI/Billing/PrintInvoicesNotices.aspx?FileName=" + sFileName, 'Adjustment', 'width=' + width + ',height=' + height + ',top=' + (screen.availHeight - height) / 2 + ',left=' + (screen.availWidth - width) / 2 + ',resizable=no,scrollbars=no,status=no,center=yes');
    return false;
}
