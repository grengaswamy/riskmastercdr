var m_groupNames=[]; 
var m_groupId = [];
var m_count = null;
function ScanListForIterms(selectObject,hiddenObject)
{
  var strValues = "";       
  for(i=0;i < selectObject.length;i++)
  {
     strValues = strValues + selectObject[i].text + "|" + selectObject[i].value + "|";
  }
  if(strValues != "")
    strValues = strValues.substr(0,strValues.length - 1);
  hiddenObject.value = strValues;
}
function GetWizardIndex()
{
    var index;
    if(document.getElementById('Wizardlist_index0')!=null)
        index = 0;
        else if(document.getElementById('Wizardlist_index1')!=null)
                index = 1;
                else if(document.getElementById('Wizardlist_index2')!=null)
                        index = 2;
                        else if(document.getElementById('Wizardlist_index3')!=null)
                            index = 3;
                            else if(document.getElementById('Wizardlist_index4')!=null)
                                index = 4; //Boeing
                                //else if (document.getElementById('Wizardlist_index5') != null) //PJS 02-18-2009
                                //index = 5;
                                else if (document.getElementById('Wizardlist_index5') != null)
                                    index = 5;
                                        else
                                            index = -1;
                            
    return index;                        
                            
}     
function EnableDisablePassword()
{
    var objchkOveridePass = eval(document.forms[0].Wizardlist_chkOveridePass);
    if(objchkOveridePass != null)
    {
        if(objchkOveridePass.checked)
        {
            document.forms[0].Wizardlist_txtPass.disabled = false;
        }
        else
        {
             document.forms[0].Wizardlist_txtPass.disabled = true;
        }
    }
}  
function CopyItemToList(selectObject,hiddenObject)
{
    for(i=selectObject.length - 1; i >= 0 ;i--)
    {
      deleteOption(selectObject,i);
    }
    var arr = new Array();
    arr=hiddenObject.value.split("|");
    var i = 0;
    while(i < arr.length && arr.length > 1)
    {
      addOption(selectObject,arr[i],arr[i + 1]);
      i = i + 2;
    }  
}
      
function addOption(selectObject,optionText,optionValue) 
{
  if (!recheck (selectObject, optionText, optionValue))
  {
    var optionObject = new Option(optionText,optionValue)
    var optionRank = selectObject.options.length
    selectObject.options[optionRank]=optionObject
  }
} 

function deleteOption(selectObject,optionRank)
{
  if (selectObject.options.length!=0) 
    selectObject.options[optionRank]=null
}

function recheck (selectObject, optionText, optionValue)
{ 
  var sltbool = false;
  for (i=0; i < selectObject.options.length; i++)
  {
    if (optionText == selectObject.options[i].text ||  optionValue == selectObject.options[i].value )
    {
      sltbool = true;
      break
    }
  } 
  return sltbool;
}

function SetCancelAction()
{
  if(confirm("Are you sure you want to close this window without saving the changes?"))
    this.close();
  else
    return false;
}

function ValidateEmptyList(SelectObject,sMessage)
{
  if(SelectObject.options.length == 0)
  {
    alert("Please " + sMessage);
    return false;
  }
  return true;
}
  function disableAdjField(opt)
  {     
      if(opt == 'Wizardlist_optAllAdjTextTypes')
      {
          if(eval('document.forms[0].'+opt).checked == true)
          {
//          document.forms[0].Wizardlist_btnAdjTextTypes.disabled = true;
//          document.forms[0].Wizardlist_btndelAdjTextTypes.disabled = true;
            document.forms[0].Wizardlist_lstAdjTextTypes_multicodebtn.disabled = true;
            document.forms[0].Wizardlist_lstAdjTextTypes_multicodebtndel.disabled = true;
            document.forms[0].Wizardlist_lstAdjTextTypes_multicode.disabled =true;
          document.forms[0].Wizardlist_optAllAdjTextTypes.value="1";
          }
      }
      else if(opt == 'Wizardlist_optSomeAdjTextTypes')
      {
          if(eval('document.forms[0].'+opt).checked == true)
          {
//          document.forms[0].Wizardlist_btnAdjTextTypes.disabled = false;
//          document.forms[0].Wizardlist_btndelAdjTextTypes.disabled = false;
            document.forms[0].Wizardlist_lstAdjTextTypes_multicodebtn.disabled = false;
            document.forms[0].Wizardlist_lstAdjTextTypes_multicodebtndel.disabled = false;
            document.forms[0].Wizardlist_lstAdjTextTypes_multicode.disabled = false;
          document.forms[0].Wizardlist_optSomeAdjTextTypes.value="2";
          }
      }
      else if(opt == 'Wizardlist_optExceptAdjTextTypes')
      {
          if(eval('document.forms[0].'+opt).checked == true)
          {
//          document.forms[0].Wizardlist_btnAdjTextTypes.disabled = false;
//          document.forms[0].Wizardlist_btndelAdjTextTypes.disabled = false;
             document.forms[0].Wizardlist_optExceptAdjTextTypes.value="3";
            document.forms[0].Wizardlist_lstAdjTextTypes_multicodebtn.disabled = false;
            document.forms[0].Wizardlist_lstAdjTextTypes_multicodebtndel.disabled = false;
            document.forms[0].Wizardlist_lstAdjTextTypes_multicode.disabled = false;
          }
      }
      else
      {
//      document.forms[0].Wizardlist_btnAdjTextTypes.disabled = true;
//      document.forms[0].Wizardlist_btndelAdjTextTypes.disabled = true;
        document.forms[0].Wizardlist_lstAdjTextTypes_multicodebtn.disabled = true;
        document.forms[0].Wizardlist_lstAdjTextTypes_multicodebtndel.disabled = true;
        document.forms[0].Wizardlist_lstAdjTextTypes_multicode.disabled = true;
      }
  }
  function GetCollectionOfGroupName() 
  {
     var count = 0, ObjTR = null, countCommon = 0;
   
    while (true) {
        if (count >= 8)
            ObjTR = window.opener.document.getElementById('OrgSetGrid_' + 'gvData_ctl' + (count + 2) + '_|' + (count + 2) + '|Data');
        else
            ObjTR = window.opener.document.getElementById('OrgSetGrid_' + 'gvData_ctl0' + (count + 2) + '_|0' + (count + 2) + '|Data');
        
        if (ObjTR != null) {
            var sData = ObjTR.value;
            sData = sData.replace(/&lt;/g, "<");
            sData = sData.replace(/&gt;/g, ">");
            sData = sData.replace(/&quot;/g, "'");
            var sStartSelectionString = "<GroupName>";
            var sEndSelectionString = "</GroupName>";
            var sStartPos = sData.indexOf(sStartSelectionString);
            var sEndPos = sData.indexOf(sEndSelectionString);
            var sGroupName = sData.substring((parseInt(sStartPos) + 11), sEndPos);
            m_groupNames[count] = sGroupName;
            sStartSelectionString = "<RowId>";
            sEndSelectionString = "</RowId>";
            sStartPos = sData.indexOf(sStartSelectionString);
            sEndPos = sData.indexOf(sEndSelectionString);
            var sRowId =sData.substring((parseInt(sStartPos) + 7), sEndPos);
            m_groupId[count] = sRowId;
            count++;
        }
        else break;
    }
    m_count = count;
}

function noteTypeSelection(opt) {
    if (opt == 'Wizardlist_optAllEnhanceNoteTypes') {
        if (eval('document.forms[0].' + opt).checked == true) {
            //          document.forms[0].Wizardlist_btnAdjTextTypes.disabled = true;
            //          document.forms[0].Wizardlist_btndelAdjTextTypes.disabled = true;
            document.forms[0].Wizardlist_lstEnhanceNoteTypes_multicodebtn.disabled = true;
            document.forms[0].Wizardlist_lstEnhanceNoteTypes_multicodebtndel.disabled = true;
            document.forms[0].Wizardlist_lstEnhanceNoteTypes_multicode.disabled = true;
            document.forms[0].Wizardlist_optAllEnhanceNoteTypes.value = "1";
        }
    }
    else if (opt == 'Wizardlist_optSomeEnhanceNoteTypes') {
        if (eval('document.forms[0].' + opt).checked == true) {
            //          document.forms[0].Wizardlist_btnAdjTextTypes.disabled = false;
            //          document.forms[0].Wizardlist_btndelAdjTextTypes.disabled = false;
            document.forms[0].Wizardlist_lstEnhanceNoteTypes_multicodebtn.disabled = false;
            document.forms[0].Wizardlist_lstEnhanceNoteTypes_multicodebtndel.disabled = false;
            document.forms[0].Wizardlist_lstEnhanceNoteTypes_multicode.disabled = false;
            document.forms[0].Wizardlist_optSomeEnhanceNoteTypes.value = "2";
        }
    }
    else if (opt == 'Wizardlist_optExceptEnhanceNoteTypes') {
        if (eval('document.forms[0].' + opt).checked == true) {
            //          document.forms[0].Wizardlist_btnAdjTextTypes.disabled = false;
            //          document.forms[0].Wizardlist_btndelAdjTextTypes.disabled = false;
            document.forms[0].Wizardlist_optExceptEnhanceNoteTypes.value = "3";
            document.forms[0].Wizardlist_lstEnhanceNoteTypes_multicodebtn.disabled = false;
            document.forms[0].Wizardlist_lstEnhanceNoteTypes_multicodebtndel.disabled = false;
            document.forms[0].Wizardlist_lstEnhanceNoteTypes_multicode.disabled = false;
        }
    }
    else {
        //      document.forms[0].Wizardlist_btnAdjTextTypes.disabled = true;
        //      document.forms[0].Wizardlist_btndelAdjTextTypes.disabled = true;
        document.forms[0].Wizardlist_lstEnhanceNoteTypes_multicodebtn.disabled = true;
        document.forms[0].Wizardlist_lstEnhanceNoteTypes_multicodebtndel.disabled = true;
        document.forms[0].Wizardlist_lstEnhanceNoteTypes_multicode.disabled = true;
    }
}
  function disableBrokerField(opt)
  {     
      if(opt == 'Wizardlist_optAllBrokers')
      {
          if(eval('document.forms[0].'+opt).checked == true)
          {

            document.forms[0].Wizardlist_lstBrokerFirmbtn2.disabled = true;
            document.forms[0].Wizardlist_lstBrokerFirmbtnde2.disabled = true;
            document.forms[0].Wizardlist_lstBrokerFirm2.disabled =true;
          document.forms[0].Wizardlist_optAllBrokers.value="1";
          }
      }
      else if(opt == 'Wizardlist_optSomeBrokers')
      {
          if(eval('document.forms[0].'+opt).checked == true)
          {

             document.forms[0].Wizardlist_lstBrokerFirmbtn2.disabled = false;
            document.forms[0].Wizardlist_lstBrokerFirmbtnde2.disabled = false;
            document.forms[0].Wizardlist_lstBrokerFirm2.disabled =false;
          document.forms[0].Wizardlist_optSomeBrokers.value="2";
          }
      }
      
      else
      {

            document.forms[0].Wizardlist_lstBrokerFirmbtn2.disabled = true;
            document.forms[0].Wizardlist_lstBrokerFirmbtnde2.disabled = true;
            document.forms[0].Wizardlist_lstBrokerFirm2.disabled =true;
      }
  }
  
  function disableInsurerField(opt)
  {     
      if(opt == 'Wizardlist_optAllInsurers')
      {
          if(eval('document.forms[0].'+opt).checked == true)
          {

            document.forms[0].Wizardlist_lstInsurerFirmbtn3.disabled = true;
            document.forms[0].Wizardlist_lstInsurerFirmbtnde3.disabled = true;
            document.forms[0].Wizardlist_lstInsurerFirm3.disabled =true;
          document.forms[0].Wizardlist_optAllInsurers.value="1";
          }
      }
      else if(opt == 'Wizardlist_optSomeInsurers')
      {
          if(eval('document.forms[0].'+opt).checked == true)
          {

           document.forms[0].Wizardlist_lstInsurerFirmbtn3.disabled = false;
            document.forms[0].Wizardlist_lstInsurerFirmbtnde3.disabled = false;
            document.forms[0].Wizardlist_lstInsurerFirm3.disabled =false;
          document.forms[0].Wizardlist_optSomeInsurers.value="2";
          }
      }
      
      else
      {

             document.forms[0].Wizardlist_lstInsurerFirmbtn3.disabled = true;
            document.forms[0].Wizardlist_lstInsurerFirmbtnde3.disabled = true;
            document.forms[0].Wizardlist_lstInsurerFirm3.disabled =true;
      }
  }
  