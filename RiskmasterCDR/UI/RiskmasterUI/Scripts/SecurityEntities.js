//rsolanki2 : domain authentication updates
var m_bIsDomainAuthenticationEnabled=false;


var ModuleGroupsLoadedForId="";
var imgFolderOpen = new Image(16,16);      
var imgFolderClosed = new Image(16,16);      
//imgFolderOpen.src = "/oxf/csc-Pages/riskmaster/SecurityMgtSystem/Common/TreeSpecific/Images/handle.collapse.more.gif"; 
//imgFolderClosed.src = "/oxf/csc-Pages/riskmaster/SecurityMgtSystem/Common/TreeSpecific/Images/handle.expand.end.gif";

imgFolderOpen.src = "csc-Theme/riskmaster/img/handle.collapse.more.gif"; 
imgFolderClosed.src = "csc-Theme/riskmaster/img/handle.expand.end.gif";
var sPreviousLinkSelectedId="";
var imgMgLoading=new Image(16,16);
//imgMgLoading.src="/oxf/csc-Pages/riskmaster/SecurityMgtSystem/Common/TreeSpecific/Images/connect.end.gif";
imgMgLoading.src="csc-Theme/riskmaster/img/connect.end.gif";
//Show or hide a folder and save its state.
function ToggleFolder(FolderName, state)
    {
        if (state == null) {
    if (document.getElementById(FolderName).style.display == "inline") {
        state = "false";
    } else {
        state = "true";
    }
    }
    if (state == "false") {
    document.getElementById(FolderName).style.display = "none";
    document.getElementById('icon-' + FolderName).src = imgFolderClosed.src;
    UpdateState(FolderName, false);
    } else {
    document.getElementById(FolderName).style.display = "inline";
    document.getElementById('icon-' + FolderName).src = imgFolderOpen.src;
    UpdateState(FolderName, true);
    }
    return false;
    
}
//Store the expanded/collapsed state of a node.
function UpdateState(FolderName, State)
 {
    if (State) 
    {
    folderstate += "[" + FolderName + "]";
    } 
    else 
    {
    folderstate = folderstate.replace("[" + FolderName + "]", "");
    }
}
//Change the nodestate list into a commma-delimited list for the ASPX.
function SerializeState() 
{
    var buffer = new Array();
    var lcv;
    
    if (folderstate.length == 0) return folderstate;
    buffer = folderstate.substring(1, folderstate.length-1);
    buffer = buffer.split("][");
    return buffer.join(",");
}
function InitPage() 
{  
    if(PageMainFrame!=null && PageMainFrame!="")
    {
        parent.frames['MainFrame'].location.href=PageMainFrame;
    }
    //rsolanki2 : domain authentication updates
    var DomainElement= document.getElementById('hdIsDomainAuthenticationEnabled');
    if (DomainElement!=null)
        m_bIsDomainAuthenticationEnabled = DomainElement.value;
   
    if ((m_bIsDomainAuthenticationEnabled+'').toLowerCase()=="true" )
        HideToolbarChgPasswordButton();
        
    //alert ("SecurityEntity.js: m_bIsDomainAuthenticationEnabled = " + m_bIsDomainAuthenticationEnabled );	
   
    return true;
}
//rsolanki2 : Domain authentication updates.
function HideToolbarChgPasswordButton()
{
    var ToolbarButtonElement= window.parent.frames['ToolBar'].document.getElementById('changelogininfo');   
    if (ToolbarButtonElement!=null)
    {
        ToolbarButtonElement.style.display="none";
    }
}
function ErrorMessage(p_sMessage) 
{
    if(window.parent.trimIt(p_sMessage)!="")
     {
      alert(p_sMessage);
     }
}

function openInFrame(p_sUrl,p_sEntityType,p_iId,p_iIdDb,p_sCalledFromXsl)
	{
	    
	    
	    window.parent.frames['LeftTree'].document.getElementById('SelectedItemTreeId').value = p_iId
	
	if(p_sCalledFromXsl=="true")
	{
	  sPreviousLinkSelectedId=p_iId;
	}
	  if(p_sCalledFromXsl!="true")
	  {
	    if(ConfirmSave())
        {
        return;
        }
	  }
	  try
	  {
	     if(p_sUrl==null)
	      {
	       return;
	      }  
	      switch(p_sEntityType)
	      {
	        
	            case "User" :
	            {
					SetTreePostedByValue("UpdateUserEntity");
					SetUserUpdateIds(p_iIdDb)
					break;
	            }
	            case "Datasource" :
	            {
					SetTreePostedByValue("UpdateDatasourceEntity");
					SetDatasourceUpdateIds(p_iIdDb)
					break;
	            }
	        case "PermUser":
	        case "UserPerm":
	            {
					SetTreePostedByValue("UpdateUserPermEntity");
					SetPermUserUpdateIds(p_iIdDb)
					
					break;
	            }
	            case "Module Security Group" : case "GrpModuleAccessPermission":
	            {
					SetTreePostedByValue("UpdateModuleGroupPermissionEntity");
					SetModuleGroupEntityIds(p_iIdDb);
					 if(p_sCalledFromXsl!="true")
	                   {
							var sRead=window.parent.m_sRead
							window.parent.frames['MainFrame'].document.write(sRead)
					   }
					break;
	            }
	            case "Module Group User" :
	            {
					SetTreePostedByValue("UpdateModuleGroupUserEntity");
					SetModuleGroupUserEntityIds(p_iIdDb);
					break;
	            }
	            default :
	            {
	               SetTreePostedByValue("NoPost");
                   break;
                 }

	                
	      }
	      window.parent.frames['LeftTree'].document.getElementById('SelectedEntityFromLeftTree').value = p_sEntityType;
	      window.parent.frames['LeftTree'].document.getElementById('EntityIdForDeletion').value = p_iIdDb
	    if(p_sCalledFromXsl!="true")
	     {
	     
	    if(sPreviousLinkSelectedId==null || sPreviousLinkSelectedId=="")
	    {
		  //window.parent.frames['LeftTree'].document.all["hyp-"+p_iId].style.cssText="text-decoration:none;COLOR: white; BACKGROUND-COLOR: darkblue"
		  sPreviousLinkSelectedId=p_iId
		  
		}
		else
		{
		    //window.parent.frames['LeftTree'].document.all["hyp-"+sPreviousLinkSelectedId].style.cssText="FONT-SIZE: 10pt;FONT-FAMILY: Tahoma, Arial, Helvetica, sans-serif;HEIGHT: 10px;text-decoration:none;COLOR: black;BACKGROUND-COLOR: none"
		   //window.parent.frames['LeftTree'].document.all["hyp-"+p_iId].style.cssText="FONT-SIZE: 10pt;FONT-FAMILY: Tahoma, Arial, Helvetica, sans-serif;HEIGHT: 10px;text-decoration:none;COLOR: white; BACKGROUND-COLOR: darkblue"
		   sPreviousLinkSelectedId=p_iId
		}
		
		
			window.parent.frames['LeftTree'].document.getElementById('SelectedEntityFromLeftTree').value=p_sEntityType;
			if(window.parent.frames['LeftTree'].document.getElementById('DsnIdForCurrentlyLoadedMGrps')!=null)
				{
				if(window.parent.frames['LeftTree'].document.getElementById('DsnIdForCurrentlyLoadedMGrps').value=="")
					{
						parent.frames['MainFrame'].location.href=p_sUrl;
					}
				else
					{
						if(p_sEntityType=="PermUser")
						{
						p_sUrl=p_sUrl+"&amp;"+"DsnIdForCurrentlyLoadedMGrps="+window.parent.frames['LeftTree'].document.getElementById('DsnIdForCurrentlyLoadedMGrps').value
						}
						parent.frames['MainFrame'].location.href=p_sUrl
					}
			    }
			 else
			     { 
			        parent.frames['MainFrame'].location.href=p_sUrl;
          		 }
         }
         
         if(window.parent.frames['LeftTree'].document.forms['IsPostBack']!=null)
           {
           window.parent.frames['LeftTree'].document.forms['IsPostBack'].value="true"
           }
          return false;
      }
      catch(p_objException)
      {
        ErrorSink(p_objException);
      }
	}
function LoadModuleGroups(FolderName, entityType, idDb, state) 
  {
     if(!ConfirmSave())
    {
         
      if (state == null) {
        if (document.getElementById(FolderName).style.display == "inline") {
          state = "false";
        } else {
          state = "true";
        }
      }
      if (state == "false") {
        document.getElementById(FolderName).style.display = "none";
        document.getElementById('icon-' + FolderName).src = imgFolderClosed.src;
        UpdateState(FolderName, false);
      } else {
        document.getElementById(FolderName).style.display = "inline";
        document.getElementById('icon-' + FolderName).src = imgFolderOpen.src;
        UpdateState(FolderName, true);
      }
      if (window.parent.frames['LeftTree'].document.getElementById('DsnIdForCurrentlyLoadedMGrps') != null)
        {
            if (window.parent.frames['LeftTree'].document.getElementById('DsnIdForCurrentlyLoadedMGrps').value == idDb)
       {
        return false;
       }
       }
       var sOrigHtml = window.parent.frames['LeftTree'].document.getElementById('td-' + FolderName).innerHTML
      var sToAppend3="<Div cellpadding='0' cellspacing='0' style='COLOR: black' align='left'><img border='0' src='csc-Theme/riskmaster/img/connect.end.gif'>Loading...</Div>"
      var sToShowHtml=sOrigHtml+sToAppend3
      window.parent.frames['LeftTree'].document.getElementById('td-' + FolderName).innerHTML = sToShowHtml;


      window.parent.frames['LeftTree'].document.getElementById('TreePostedBy').value = "LoadModuleGroups";
      window.parent.frames['LeftTree'].document.getElementById('NodeState').value=folderstate;
      window.parent.frames['LeftTree'].document.getElementById('DsnIdForLoadingMG').value=idDb;
      window.parent.frames['LeftTree'].document.getElementById('IsPostBack').value='true';
      window.parent.frames['LeftTree'].document.forms['frmLeftTree'].submit();
      return false;
      }
    }
function TempWindow()
{    
		parent.frames['LeftTree'].location.href='home?pg=riskmaster/SecurityMgtSystem/SecurityEntities'
		return false;
}
function SetTreePostedByValue(p_sTreePostVal)
{
	try
	{
	window.parent.frames['LeftTree'].document.getElementById('TreePostedBy').value = p_sTreePostVal;
	}
	catch(p_objException)
	{	
		//throw p_objException;
	 }		
}
function SetUserUpdateIds(idDb)
{
	try
	{
	window.parent.frames['LeftTree'].document.getElementById('UserEntityUserId').value = idDb;
	}
	catch(p_objException)
	{	
		throw p_objException;
	 }		
}
function SetDatasourceUpdateIds(idDb)
{
	try
	{
	window.parent.frames['LeftTree'].document.getElementById('DatasourceEntityDsnId').value = idDb;
	}
	catch(p_objException)
	 {	
		throw p_objException;
	 }		
}
function SetPermUserUpdateIds(idDb)
{  
var iPos=0
var arrTemp=new Array()
try
{
	if(idDb.length<2)
	{
	InSuffParamsForUpdatePermUser = new Error ("Insufficient parameters for updating perm user.");
	throw InSuffParamsForUpdatePermUser;
	}
	arrTemp=idDb.split(',')
	
	window.parent.frames['LeftTree'].document.getElementById('UserPermEntityDsnId').value = arrTemp[0];
	window.parent.frames['LeftTree'].document.getElementById('UserPermEntityUserId').value = arrTemp[1]

	//Raman for R5
	window.parent.frames['LeftTree'].document.getElementById('DsnIdForCurrentlyLoadedMGrps').value = arrTemp[0];
	
}
catch(p_objException)
{	
	throw p_objException;
	}		
}
function SetModuleGroupEntityIds(idDb)
{  
	var iPos=0
	try
	{ 
		if(idDb.length<2)
		{
		InSuffParamsForUpdatePermUser = new Error ("Insufficient parameters for updating module group entity.");
		throw InSuffParamsForUpdatePermUser;
		}
		iPos=idDb.indexOf(",")
		if(iPos>0)
		{
		window.parent.frames['LeftTree'].document.getElementById('ModuleGroupEntityDsnId').value = idDb.substring(0,iPos);
		window.parent.frames['LeftTree'].document.getElementById('ModuleGroupEntityGroupId').value = idDb.substr(iPos+1)
		}
	}
	catch(p_objException)
	 {	
		throw p_objException;
	 }		
}

function ErrorSink(p_objException)
	{  
		alert(p_objException.message);
		throw p_objException;
		
	}
function SetModuleGroupUserEntityIds(idDb)
{  

	var iPos=0
	var arrTemp=new Array()
	try
	{
		if(idDb.length<2)
		{
		InSuffParamsForUpdateModuleGroupUser = new Error ("Insufficient parameters for updating Module Group User.");
		throw InSuffParamsForUpdateModuleGroupUser;
		}
		arrTemp=idDb.split(',')
		window.parent.frames['LeftTree'].document.getElementById('ModuleGroupUserEntityDsnId').value = arrTemp[0];
		window.parent.frames['LeftTree'].document.getElementById('ModuleGroupUserEntityGrpId').value = arrTemp[1]
		window.parent.frames['LeftTree'].document.getElementById('ModuleGroupUserEntityUserId').value = arrTemp[2]
	}
	catch(p_objException)
	    {	
		throw p_objException;
		}		
}
function NoScrollBar()
    {
        //window.frames['LeftTree'].pleaseWait.Show();
        window.frames['LeftTree'].document.location.href='SecurityEntities.aspx';
        document.body.scroll="no";
		document.body.marginwidth="0";
		document.body.marginheight="0"; 
		document.body.bottommargin="0";
		document.body.leftmargin="0";
		document.body.rightmargin="0"; 
		document.body.scrolltop="0";
		document.body.scrollleft="0";
		document.body.topmargin="0";
		document.body.leftMargin="0";
		document.body.rightMargin="0";
		document.body.bottomMargin="0";
		document.body.topMargin="0";
		document.body.className="padding:0;topmargin:0; leftmargin:0; marginheight:0; marginwidth:0; top:0;left:0;border:none;margin:0;topMargin:0;margin-top:0;margin: 0;marginRight:0;marginBottom:0;topMargin:0; marginLeft:0;paddingRight:0;paddingBottom:0;paddingTop:0; paddingLeft:0";
        return false;
        
        

    }
function haveProperty(obj, sPropName)
{
	for(p in obj)
	{
		if(p == sPropName)
			return true;
	}
	return false;
}
