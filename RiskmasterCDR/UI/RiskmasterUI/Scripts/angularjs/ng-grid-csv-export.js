//******************************************************************************************************************************************
//*   Date     |  JIRA   | Programmer | Description                                                                                        *
//******************************************************************************************************************************************
//* 01/06/2015 | RMA4307  | vchouhan6   | Generic Export to Excel for visible columns, independent of size, order of columns and UI filters.
//******************************************************************************************************************************************//
// Changes:
// 1) Future Scope : Made the button prettier, Added css classes .csv-data-link-span, pull-right, btne, btne-default, btne-xs  to improve UI
// 2) add a config option for IE users which takes a URL.  That URL should accept a POST request with a
//    JSON encoded object in the payload and return a CSV.  This is necessary because IE doesn't let you
//    download from a data-uri link
// 3) Only display columns that are visible, Remove Addtional disabled Columns from exported excel
// 4) Export filtered rows only, rather than all
// 5) Replace strings like SBLANK etc from header with ' '

function ngGridCsvExportPlugin(opts) {
    var self = this;
    self.grid = null;
    self.scope = null;
    self.services = null;
    self.init = function (scope, grid, services) {
        self.grid = grid;
        self.scope = scope;
        self.services = services;
        function showDs() {
            // CUSTOMIZATION by vchouhan6 for JIRA-RMA4307: Only display columns that are visible (ie not hidden via the showColumnMenu dropdown)
            var keys = [];
            var displayNames = [];
            // BEFORE:
            // for (var f in grid.config.columnDefs) { keys.push(grid.config.columnDefs[f].field); }
            //AFTER
            angular.forEach(self.scope.columns, function (col) {
                if (col.visible) {
                    displayNames.push(col.displayName);
                    keys.push(col.field);
                }
            });
            // END CUSTOMIZATION

            // CUSTOMIZATION by vchouhan6 for JIRA-RMA4307: output the displayName for each headers instead of the field
            var csvData = '';
            // BEFORE: 
            // for (var k in keys) {
            //     csvData += '"' + csvStringify(keys[k]) + '",';
            // }
            // AFTER:
            for (var k in displayNames) {
                csvData += '"' + csvStringify(displayNames[k]) + '",';
            }
            // END CUSTOMIZATION

            csvData = swapLastCommaForNewline(csvData);

            // CUSTOMIZATION by vchouhan6 for JIRA-RMA4307: Export filtered rows only, rather than all
            // BEFORE:
            //var gridData = grid.data;
            //for (var gridRow in gridData) {
            //    for (k in keys) {
            //        var curCellRaw;
            //        if (opts != null && opts.columnOverrides != null && opts.columnOverrides[keys[k]] != null) {
            //            curCellRaw = opts.columnOverrides[keys[k]](gridData[gridRow][keys[k]]);
            //        }
            //        else {
            //            curCellRaw = gridData[gridRow][keys[k]];
            //        }
            //        csvData += '"' + csvStringify(curCellRaw) + '",';
            //    }
            //    csvData = swapLastCommaForNewline(csvData);
            //}
            // AFTER:
            var gridData = self.grid.filteredRows;//As compared to before code this is 1st change
            for (var gridRow in gridData) {
                for (k in keys) {
                    var curCellRaw;

                    if (opts != null && opts.columnOverrides != null && opts.columnOverrides[keys[k]] != null) {
                        curCellRaw = opts.columnOverrides[keys[k]](self.services.UtilityService.evalProperty(gridData[gridRow].entity, keys[k])); //As compared to before code this is 2nd change
                    } else {
                        curCellRaw = self.services.UtilityService.evalProperty(gridData[gridRow].entity, keys[k]); //As compared to before code this is 3rd change
                    }

                    csvData += '"' + csvStringify(curCellRaw) + '",';
                }
                csvData = swapLastCommaForNewline(csvData);
            }
            // END CUSTOMIZATION


            // CUSTOMIZATION by vchouhan6 for JIRA-RMA4307: Replace strings like SBLANK etc from header with ' '
            // AFTER
            csvData = csvData.replace(/SBLANK/g, ' ').replace(/SBSLASH/g, '/').replace(/STILED/g, '~').replace(/SXCLAIMATION/g, '!').replace(/SATADD/g, '@').replace(/SHASH/g, '#').replace(/SDOLLAR/g, '$').replace(/SPERCENT/g, '%').replace(/SCAP/g, '^').replace(/SAMPERSAND/g, '&').replace(/SSTAR/g, '*').replace(/SUBRACE/g, '(').replace(/SLBRACE/g, ')').replace(/SDASH/g, '-').replace(/SFSLASH/g, '\'').replace(/SQUES/g, '?').replace(/SDOTS/g, '.');
            //END CUSTOMIZATION

            // CUSTOMIZATION by vchouhan6 for JIRA-RMA4307: Added css classes .csv-data-link-span, pull-right, btne, btne-default, btne-xs  to improve UI for future scope
            //setting visibility false to anchor tag and hiding the span size(display:none) so, it will not occupy space when not visible
            var fp = grid.$root.find(".ngFooterPanel");
            //BEFORE
            //            var csvDataLinkHtml = "<span class=\"csv-data-link-span\">";
            //            csvDataLinkHtml += "<br><a href=\"data:text/csv;charset=UTF-8,";
            //            csvDataLinkHtml += encodeURIComponent(csvData);
            //            csvDataLinkHtml += "\" download=\"Export.csv\">CSV Export</a></br></span>" ;
            // AFTER
            var csvDataLinkPrevious = grid.$root.find('.ngFooterPanel .csv-data-link-span');
            if (csvDataLinkPrevious != null) { csvDataLinkPrevious.remove(); }
            var csvDataLinkHtml = "<span class=\"csv-data-link-span pull-right\" style=\"display:none\">";

            //RMA - 7520 Issue Fix Start: CUSTOMIZATION by vchouhan6 for IE11 issue on 4-feb-2015
            //BEFORE
            //csvDataLinkHtml += "<br><a id=\"hdnanchorExport\" class=\"btne btne-default btne-xs\" role=\"button\" href=\"data:text/csv;charset=UTF-8,";
            //csvDataLinkHtml += encodeURIComponent(csvData);
            //csvDataLinkHtml += "\" download=\"Export.csv\">CSV Export</a></br></span>";
            //AFTER
            csvDataLinkHtml += " <a id=\"hdnanchorExport\" class=\"btne btne-default btne-xs\" role=\"button\" style=\"visibility: hidden\"";
            if (!supportsDataUri()) {
                csvDataLinkHtml += " data-csv=\"";
                csvDataLinkHtml += encodeURIComponent(csvData);
                csvDataLinkHtml += "\" onclick='getCsvFileForIE(this);' >";
            } else {
                csvDataLinkHtml += " href=\"data:text/csv;charset=UTF-8,";
                csvDataLinkHtml += encodeURIComponent(csvData);
                csvDataLinkHtml += "\" download=\"Export.csv\">";
            }

            csvDataLinkHtml += "CSV Export</a> &nbsp;&nbsp;";
            csvDataLinkHtml += "</br></span>"; //End csv-data-link-span 
            //RMA - 7520 Issue Fix END

            // END CUSTOMIZATION
            fp.append(csvDataLinkHtml);

        }

        //CUSTOMIZATION by vchouhan6 
        //CodeReview : 21-Jan-2015  ||  RMA-6694 : Code Review
        //#1 Pulling Function out of ShowD()
        function csvStringify(str) {
            if (str == null) { // we want to catch anything null-ish, hence just == not ===
                return '';
            }
            if (typeof (str) === 'number') {
                return '' + str;
            }
            if (typeof (str) === 'boolean') {
                return (str ? 'TRUE' : 'FALSE');
            }
            if (typeof (str) === 'string') {

                //"-" char handling: if string is with negation like -text than in general settings its better to present it via '-text'
                if (str.indexOf("-") === 0) {
                    var strnew = str.replace(/"/g, '""')
                    strnew = "'" + strnew + "'";
                }
                else {
                    strnew = str.replace(/"/g, '""')
                }
                return strnew;
            }

            return JSON.stringify(str).replace(/"/g, '""');
        }
        //ENDCUSTOMIZATION

        //CUSTOMIZATION by vchouhan6 
        //CodeReview : 21-Jan-2015  || RMA-6694 : Code Review
        //#2 Pulling Function out of ShowD()
        function swapLastCommaForNewline(str) {
            var newStr = str.substr(0, str.length - 1);
            return newStr + "\n";
        }
        //ENDCUSTOMIZATION

        setTimeout(showDs, 0);

        //CUSTOMIZATION by vchouhan6 
        //ISSUE RESOLUTION : 20-Jan-2015  || RMA-6887 Change page size & click random page will always export page #1
        //BEFORE
        //scope.catHashKeys = function () {
        //    var hash = '';
        //    for (var idx in scope.renderedRows) {
        //        hash += scope.renderedRows[idx].$$hashKey;
        //    }
        //    return hash;
        //};
        //scope.$watch('catHashKeys()', showDs); 
        //AFTER
        scope.catHashKeys = function () {
            var hash = '';
            for (var idx in grid.columns) {
                if (grid.columns[idx].visible) {
                    hash += grid.columns[idx].$$hashKey;
                }
            }
            return hash;
        };

        if (opts && opts.customDataWatcher) {
            scope.$watch(opts.customDataWatcher, showDs);
        } else {
            scope.$watch(scope.catHashKeys, showDs);
        }
        //ENDCUSTOMIZATION

        // CUSTOMIZATION by vchouhan6 for JIRA-RMA4307: only export filtered rows and visible columns.
        // to do this, we need to react when the filter changes or the visible columns change
        // NEW:
        scope.$parent.$on('ngGridEventFilter', showDs);
        scope.$parent.$on('ngGridEventColumns', showDs);
        //ISSUE RESOLUTION : 20-Jan-2015 || RMA-6887 Change page size & click random page will always export page #1
        scope.$parent.$on('ngGridEventData', showDs);
        // END CUSTOMIZATION
    };
}

//RMA - 7520 Issue Fix Start: CUSTOMIZATION by vchouhan6 for IE11 issue on 4-feb-2015
function getCsvFileForIE(target) {
    var csvData = target.attributes["data-csv"].value;
    if (!supportsDataUri()) {
        csvData = decodeURIComponent(csvData);

        var iframe = document.getElementById('csvDownloadFrame');
        iframe = iframe.contentWindow || iframe.contentDocument;

        csvData = 'sep=,\r\n' + csvData;

        iframe.document.open("text/html", "replace");
        iframe.document.write(csvData);
        iframe.document.close();
        iframe.focus();
        iframe.document.execCommand('SaveAs', true, 'Export.csv');
    } else {
        if (console && console.log) {
            console.log('Trying to call getCsvFileForIE with non IE browser.');
        }
    }
};


function supportsDataUri() {
    var isOldIE = navigator.appName === "Microsoft Internet Explorer";
    var isIE11 = !!navigator.userAgent.match(/Trident\/7\./);
    return !(isOldIE || isIE11); //Return true if not any IE
};

//RMA - 7520 Issue Fix END

// CUSTOMIZATION by vchouhan6 for JIRA-RMA4307: to call anchor tag "anchorExport" in ng-grid-csv-export.js on click of input excel button "btnExport"
function redirectE2E() {
    document.getElementById("hdnanchorExport").click();
}
// END CUSTOMIZATION

