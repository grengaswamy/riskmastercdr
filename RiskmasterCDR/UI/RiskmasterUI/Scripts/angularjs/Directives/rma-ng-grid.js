/*exported app*/
var app = angular.module('rmaApp', ['ngGrid']);
app.directive('rmaGrid', function ($timeout, $http) {
    return {
        restrict: 'E',
        transclude: false,
        scope: false,
        template:

        '<div><img ng-src="../../Images/tb_save_active.png" style="cursor: pointer;cursor: hand;" ng-click="savePreference()" title="' + NgGridToolTips.ttSavePreference + '"/> '
       + '<img ng-src="../../Images/Restore_button.png" style="cursor: pointer;cursor: hand;" ng-click="restoreDefault()" title="' + NgGridToolTips.ttRestoreDefaults + '"/> '
       + '<img ng-src="../../Images/tb_exportexcel_active.png" style="cursor: pointer;cursor: hand;" ng-click="ExportToExcel()" title="' + NgGridToolTips.ttExport + '"/> '
       + '</div> '
       + '<div class="gridStyle" ng-grid="gridOptions">'   
       + '</div>',


        link: function (scope, attrs) {
            //can declare variable inside directive and they will be available on page's scope
            scope.moreFilters = {
                a: 'true',
                b: 'true'
            };
        },
        controller: function ($timeout, $scope, $attrs) {

          
            $scope.sortDetails = { fields: [], direction: [] };
            
            $scope.setSortDetails = function (sortCols, sortDirections) {
                $scope.sortDetails = {
                    fields: sortCols,
                    directions: sortDirections
                };
            };
            //debugger;
            $scope.gridData = [];            
            $scope.gridColumns = [];
            $scope.additionalData = [];
            $scope.totalItems = 0;
            $scope.totalServerItems = 0;
            $scope.AdditionalUserPref = [];
            $scope.mySelections = [];// RMA-6404    achouhan3   Added to Enable MultiSelection          
            //var layoutPlugin = new ngGridLayoutPlugin();
            //$scope.updateLayout = function () {
            //    layoutPlugin.updateGridLayout();
            //};


            //read attribute values :start            
            //Enable Cell Edit in Focus
            if ($attrs["enablecelleditonfocus"] != undefined && $attrs["enablecelleditonfocus"] != "")
                $scope.enableCellEditOnFocus = $scope.$eval($attrs.enablecelleditonfocus);
            else
                $scope.enableCellEditOnFocus = false;
            if ($attrs["enablerowselection"] != undefined && $attrs["enablerowselection"] != "")
                $scope.enableRowSelection = $scope.$eval($attrs.enablerowselection);
            else
                $scope.enableRowSelection = true;
            //Enable multiple row selection or not, defaul;t will be true if attribute not provided
            if ($attrs["multiselect"] != undefined && $attrs["multiselect"] != "")
                $scope.multiSelect = $scope.$eval($attrs.multiselect);
            else
                $scope.multiSelect = false;

            
            if ($attrs["id"] != undefined && $attrs["id"] != "") //mendatory attribute to have on grid
                $scope.gridid = $attrs.id;
            if ($attrs["name"] != undefined && $attrs["name"] != "") //mendatory attribute to have on grid
                $scope.gridName = $attrs.name;
            //enable client side sorting/ if clientsidesorting attribute is not suplllied, we will consider it as true for default value
            if ($attrs["clientsidesorting"] != undefined && $attrs["clientsidesorting"] != "")
                $scope.clientsidesorting = $scope.$eval($attrs.clientsidesorting); //string values for the attribute can be directly picked wth out eval, but we want thevalue as boolean so using eval
            else
                $scope.clientsidesorting = true;

            if ($scope.clientsidesorting == false)
                $scope.useExternalSorting = true; //enable custom sorting if clientsidesorting is set to false
            else
                $scope.useExternalSorting = false;

            if ($attrs["showselectioncheckbox"] != undefined && $attrs["showselectioncheckbox"] != "")
                $scope.showSelectionCheckbox = $scope.$eval($attrs.showselectioncheckbox); //string values for the attribute can be directly picked wth out eval, but we want thevalue as boolean so using eval
            else
                $scope.showSelectionCheckbox = true;

            //read attributes: end

            //this function is used to show error
            $scope.ShowError = function (erorData) {
                var errorCtrl = $("#ErrorControl1_lblError");
                errorCtrl.html('<font class="errortext">' + NgGridLabels.lblErrors + '</font><table cellspacing="0" cellpadding="0" border="0" class="errortext"><tbody><tr><td valign="top"><img src="/RiskmasterUI/Images/error-large.gif"></td><td valign="top" style="padding-left: 1em"><ul><li class="warntext">' + erorData + '</li></ul></td></tr></tbody></table>');
            };

            //this function is used to bind the grid on page load for the first time
            $scope.getGridData = function () {
                //RMA-6404    achouhan3   Modified to support multiple grid on same page starts
                var JsonData;
                var JsonUserPref;
                var JsonAdditionalData;
                try {
                    if (document.getElementById("hdnJsonData") !== null && typeof(document.getElementById("hdnJsonData").value)!=='undefined')
                        JsonData = JSON.parse(document.getElementById("hdnJsonData").value);
                    else if (document.getElementById("hdnJsonData" + "_" + $scope.InputDataForGrid.GridId) != null)
                        JsonData = JSON.parse($("#hdnJsonData" + "_" + $scope.InputDataForGrid.GridId).val())

                    if (document.getElementById("hdnJsonUserPref") !== null && typeof (document.getElementById("hdnJsonUserPref").value) !== 'undefined')
                        JsonUserPref = JSON.parse(document.getElementById("hdnJsonUserPref").value);
                    else if (document.getElementById("hdnJsonUserPref" + "_" + $scope.InputDataForGrid.GridId) != null)
                        JsonUserPref = JSON.parse($("#hdnJsonUserPref" + "_" + $scope.InputDataForGrid.GridId).val())

                    if (document.getElementById("hdnJsonAdditionalData") !== null && typeof (document.getElementById("hdnJsonAdditionalData").value) !== 'undefined')
                        JsonAdditionalData = JSON.parse(document.getElementById("hdnJsonAdditionalData").value);
                    else if (document.getElementById("hdnJsonAdditionalData" + "_" + $scope.InputDataForGrid.GridId) != null)
                        JsonAdditionalData = JSON.parse($("#hdnJsonAdditionalData" + "_" + $scope.InputDataForGrid.GridId).val())
                    //RMA-6404    achouhan3   Modified to support multiple grid on same page Ends
                    $scope.UpdateScopeVariablesForGrid(JsonData, JsonUserPref, JsonAdditionalData);
                    $scope.updateGridOptions();
                }
                catch (error) {
                    $scope.ShowError(error);
                }
            };

            //this function is used to provide all grid options for the grid. need to give this only once during the page life cycle on page load. for refreshing the grids on postbacks, we just need to update the scope variables.
            $scope.updateGridOptions = function () {
                // $scope.pagingOptions.currentPage = 1;// //no paging at client side hence commenting,this will be modified for server side paging

                //$scope.gridColumns = gridColDef;
                $scope.gridOptions = {
                    data: 'gridData',
                    sortInfo: $scope.sortDetails,
                    columnDefs: 'gridColumns',
                    enableSorting: true,
                    //enablePaging: true,
                    selectedItems: $scope.mySelections, //RMA-6404    achouhan3   Added to Enable MultiSelection
                    showFilter: false,
                    showFooter: true,                    
                    footerTemplate: '<div ng-show="showFooter" class="ngFooterPanel" ng-class="{\'ui-widget-content\': jqueryUITheme, \'ui-corner-bottom\': jqueryUITheme}" ng-style="footerStyle()"><div class="ngTotalSelectContainer"><div class="ngFooterTotalItems" ng-class="{\'ngNoMultiSelect\': !multiSelect}" ><span class="ngLabel" ng-show="totalItems" >' + NgGridLabels.lblTotalItems + ' {{maxRows()}}</span><span class="ngLabel" ng-show="!totalItems" >' + NgGridLabels.lblNoRows + '</span><span ng-show=" totalItems> 0 && filterText.length > 0" class="ngLabel">({{i18n.ngShowingItemsLabel}} {{totalFilteredItemsLength()}})</span></div><div class="ngFooterSelectedItems" ng-show="totalItems>0 && multiSelect"><span class="ngLabel">{{i18n.ngSelectedItemsLabel}} {{selectedItems.length}}</span></div></div>',
                    //footerTemplate: '<div ng-show="showFooter" class="ngFooterPanel" ng-class="{\'ui-widget-content\': jqueryUITheme, \'ui-corner-bottom\': jqueryUITheme}" ng-style="footerStyle()"><div class="ngTotalSelectContainer"><div class="ngFooterTotalItems" ng-class="{\'ngNoMultiSelect\': !multiSelect}" ><span class="ngLabel">' + NgGridLabels.lblTotalItems + ' {{maxRows()}}</span><span ng-show="filterText.length > 0" class="ngLabel">({{i18n.ngShowingItemsLabel}} {{totalFilteredItemsLength()}})</span></div><div class="ngFooterSelectedItems" ng-show="multiSelect"><span class="ngLabel">{{i18n.ngSelectedItemsLabel}} {{selectedItems.length}}</span></div></div>',
                    enableColumnResize: true,
                    enableCellSelection: false,
                    enableColumnReordering: true,
                    showColumnMenu: true,
                    enableCellEditOnFocus: $scope.enableCellEditOnFocus,
                    plugins: [new FilterPlugin(), new ngGridExportPlugin()],
                    jqueryUIDraggable: true,
                    i18n: 'en',
                    headerRowHeight: 60,
                    multiSelect: $scope.multiSelect,
                    useExternalSorting: $scope.useExternalSorting,
                    showSelectionCheckbox: $scope.showSelectionCheckbox,
                    totalServerItems: 'totalServerItems',
                    afterSelectionChange: function (rowItem) { return $scope.grid_afterSelectionChange(rowItem); }
                };
            };

            //this function i sused to set/update scope variables required for the grid. when we need to update grid on postbacks/non-postbacks, we just need to update the scope variables required for the grid
            $scope.UpdateScopeVariablesForGrid = function (JsonData, JsonUserPref, additionalInfo) {
                $scope.gridData = JsonData;
                //$scope.completeData = JsonData;//commented as there is no paging for client grid
                $scope.gridColumns = JsonUserPref.colDef;
                $scope.additionalData = additionalInfo;
                $scope.JsonAdditionalUserPref = JsonUserPref.AdditionalUserPref;
                //var iTotItems = Number.parseInt(JsonAdditionalData[0].TotalCount);
                $scope.totalItems = parseInt(additionalInfo[0].TotalCount);
                $scope.totalServerItems = $scope.totalItems; //must be available on controller from server
                $scope.setSortDetails(JsonUserPref.SortColumn, JsonUserPref.SortDirection);
            }
            $scope.getGridData();

            //this event is called after grid sorting is done. we will update the sort details object so that we can save it later on save preference
            $scope.$on('ngGridEventSorted', function (args, sortInfo) {
                $scope.sortDetails.fields = sortInfo.fields;
                $scope.sortDetails.directions = sortInfo.directions;
            });

            //filter code starts
            function FilterPlugin() {

                var self = this;
                self.grid = null;
                self.scope = null;
                self.init = function (scope, grid) {
                    self.scope = scope;
                    self.grid = grid;

                    scope.SearchText = function () {
                        var searchQuery = "";
                        angular.forEach(self.scope.columns, function (col) {
                            if (col.visible && col.filterText) {
                                var filterText = col.filterText + '; ';
                                //searchQuery += col.displayName + ": " + filterText;
                                searchQuery += col.field + ": " + filterText;
                                //var filterText = (col.filterText.indexOf('*') === 0 ? col.filterText.replace('*', '') : "^" + col.filterText) + ";";
                                //searchQuery += col.displayName + ": " + filterText;
                            }
                        });
                        return searchQuery;
                    };

                    function SetFilterValues(searchQuery) {
                        self.scope.$parent.filterText = searchQuery;
                        self.grid.searchProvider.evalFilter();
                    };

                    scope.$watch(scope.SearchText, SetFilterValues);
                },
                self.scope = undefined,
                self.grid = undefined
            };
            var pluginInstance = new FilterPlugin();

            $scope.setFilter = function (searchQuery) {
                angular.forEach(FilterPlugin.scope.columns, function (col) {
                    if (col.visible && col.filterText) {
                        col.filterText = searchQuery;
                    }
                });
                FilterPlugin.scope.$parent.filterText = searchQuery;
                FilterPlugin.grid.searchProvider.evalFilter();
            };

            //filter code ends

            //save preference code starts
            $scope.$on('ngGridEventColumns', function (event, newColumns) {
                $scope.colCollection = newColumns;
            });

            //this function is used to save preference of the grid. 
            $scope.savePreference = function () {

                $scope.colDef = [];
                for (var i = 0; i < $scope.colCollection.length; i++) {
                    //if degault Name is undefined 
                    //which means that this column was never added by rma and it is column added by NG grid, for example, selection check box. this kind of columns are added by NG automaticaly so 
                    //we don't need to store them as user preference in dtabae. these columns will also not available in column menu option
                    if ($scope.colCollection[i].colDef.defaultName != undefined) {
                        $scope.colDef.push({
                            field: $scope.colCollection[i].colDef.field,
                            displayName: $scope.colCollection[i].displayName,
                            defaultName: $scope.colCollection[i].colDef.defaultName,
                            visible: $scope.colCollection[i].visible,
                            cellTemplate: $scope.colCollection[i].colDef.cellTemplate,
                            width: $scope.colCollection[i].width,
                            headerCellTemplate: $scope.colCollection[i].headerCellTemplate,
                            alwaysInvisibleOnColumnMenu: $scope.colCollection[i].colDef.alwaysInvisibleOnColumnMenu,
                            sortingAlgorithm: $scope.colCollection[i].sortingAlgorithm,
                            sortFn: $scope.colCollection[i].colDef.sortingAlgorithm
                        });
                    }
                }
                $scope.gridPreference =
                   {
                       colDef: $scope.colDef,
                       //PageSize: $scope.pagingOptions.pageSize,
                       SortColumn: $scope.sortDetails.fields,
                       SortDirection: $scope.sortDetails.directions,
                       AdditionalUserPref: $scope.JsonAdditionalUserPref //set values in this object from controller if page specific preference for the grid needs to be saved
                   }

                var imgLoading = $("#pleaseWaitFrame", parent.document.body);
                imgLoading.css({ display: "block" });

                $http({
                    //url: "FinancialDetailHistory.aspx/SavePreferences",
                    url: $scope.pageName + "/SavePreferences",
                    method: "POST",
                    data: { gridPreference: JSON.stringify($scope.gridPreference), gridId: $scope.gridid },
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .success(function (data, status, config, headers) {
                    imgLoading.css({ display: "none" });
                    var responseJsonObj = JSON.parse(data.d);
                    if (JSON.parse(data.d).error == null) {//no error occoured
                    }
                    else {
                        $scope.ShowError(JSON.parse(data.d).errorMessage);
                    }

                })
                .error(function (data, status, config, headers) {
                    imgLoading.css({ display: "none" });
                    $scope.ShowError(JSON.parse(data.d).errorMessage);

                });

                //return false;
            };
            //save preference code ends

            //restore default code starts
            $scope.restoreDefault = function () {
                if (confirm(NgGridAlertMessages.RestoreDefaults)) {
                    var imgLoading = $("#pleaseWaitFrame", parent.document.body);
                    imgLoading.css({ display: "block" });
                    $scope.inputData =
                    {
                        GridId: $scope.gridid
                    }
                    $http({
                        url: $scope.pageName + "/RestoreDefault",
                        // url: "FinancialDetailHistory.aspx/RestoreDefault",
                        method: "POST",
                        data: { inputData: JSON.stringify($scope.inputData) },
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .success(function (data, status, config, headers) {
                        imgLoading.css({ display: "none" });
                        if (JSON.parse(data.d).error == null) {

                            var userPref = JSON.parse(data.d);
                            if (!(userPref.SortColumn == undefined || userPref.SortColumn == ''))
                                $scope.gridOptions.sortBy(userPref.SortColumn[0]);
                            //$scope.gridColumns = userPref.colDef;
                            $scope.UpdateScopeVariablesForGrid($scope.gridData, userPref, $scope.additionalData);//RMA - 10408
                            //$scope.bindGrid($scope.gridData, userPref, $scope.additionalData);// RMA - 10408 if we update grid options completly after restore default, the grid looses its services and other objects that were attached to the grid on first time load. we don't need to update grid options again, we just need to update the scope variables and they will be applied to the grid automatically
                            if ($scope.RestoreDefaultCallBak)//hook to call page specific methods after restore default
                                $scope.RestoreDefaultCallBak();
                        }
                        else {
                            $scope.ShowError(JSON.parse(data.d).errorMessage);
                        }

                    })
                    .error(function (data, status, config, headers) {
                        imgLoading.css({ display: "none" });
                        $scope.ShowError(JSON.parse(data.d).errorMessage);
                    });
                }
            };

            $scope.ExportToExcel = function () {
                redirectE2E($scope.gridName);
            };


            //restore default code ends
            

            $scope.rowCount = parseInt($scope.totalItems);
           //achouhan3 Event hook for before selection chnage
            $scope.grid_afterSelectionChange = function (rowItem) {

                if (typeof $scope.ev_afterSelectionChange == 'function')
                     $scope.ev_afterSelectionChange(rowItem); 
            };
        }
    }
});