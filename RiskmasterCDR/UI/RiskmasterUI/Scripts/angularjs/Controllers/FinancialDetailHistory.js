﻿//var app = angular.module('rmaApp', ['ngGrid']);
//var sCall =  ' ';
app.controller('FinancialHistoryController', function ($scope, $http, $attrs) {
    //var layoutPlugin = new ngGridLayoutPlugin();
    
    //$scope.completeData = [];
    $scope.sortOptions = { fields: [], directions: [] };
    //$scope.ColDefs = [];
    //$scope.FinanicalHistoryDetailData = [];
    //$scope.isAllItem = false;
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [],
        pageSize: '',
        currentPage: 1
    };
    $scope.pageName = "FinancialDetailHistory.aspx"; //this is mendatory to be defined
    //PostInputDataForGrid - this variable is must in each controller. grid directive will use this array to pass input parameters used to bind the grid data
    $scope.InputDataForGrid =
    {
        ClaimId: document.getElementById("ClaimId").value,
        ClaimantId: document.getElementById("ClaimantId").value,
        PolicyId: document.getElementById("PolicyId").value,
        UnitId: document.getElementById("UnitId").value,
        CoverageId: document.getElementById("CoverageId").value,
        CoverageLoss: document.getElementById("CoverageLoss").value,
        ReserveType: document.getElementById("ReserveType").value,
        ReserveStatus: document.getElementById("ReserveStatus").value,
        RcRowId: document.getElementById("RcRowId").value,
        Multicurrencyindicator: document.getElementById("Multicurrencyindicator").value,
        claimcurrency: document.getElementById("claimcurrency").value,
        Selected: document.getElementById("Selected").value,
        previouscurrencytype: document.getElementById("previouscurrencytype").value,
        selectedcurrencytype: document.getElementById("selectedcurrencytype").value,
        GridId: "gridFDH", //this is mendatory for each grid
        summaryLevel: 1//default summary level is set to claim->1
    };

    //code to decide the values available for summary level : starts
    var strCarrierClaim = $('#CarrierClaim').val();
    if (strCarrierClaim == "No") {
        $("#drdFS").hide();
        $("#lblLevel").hide();
    }
    else {
        if ($scope.InputDataForGrid.RcRowId == "0" || $scope.InputDataForGrid.RcRowId == "") {
            if ($scope.InputDataForGrid.ClaimantId == "" || $scope.InputDataForGrid.ClaimantId == "0") {
                $("#drdFS").hide();
                $("#lblLevel").hide();
            }
            else {
                var $list = $("#drdFS")
                toRemove = $();

                toRemove = toRemove.add($list.find('option[value="6"]'));
                toRemove = toRemove.add($list.find('option[value="5"]'));
                toRemove = toRemove.add($list.find('option[value="4"]'));
                toRemove = toRemove.add($list.find('option[value="3"]'));
                toRemove.remove();
            }
        }
    }
    // code to decide the values available for summary level : ends

   

    $("#drdFS").change(function () {
        var imgLoading = $("#pleaseWaitFrame", parent.document.body);
        imgLoading.css({ display: "block" });
        $scope.setCaption();        
        if (document.getElementById("Selected") != null)
            document.getElementById("Selected").value = $("#drdFS option:selected").val();        
        imgLoading.css({ display: "none" });
        return true;
    });

    
    $scope.setCaption = function () {
        //code to populate value for caption starts
        $scope.InputDataForGrid.summaryLevel = $("#drdFS option:selected").val();
       
        var strSelectedDropdown = $scope.InputDataForGrid.summaryLevel;
        var strClaimNo = $("#ClaimNumber").val();
        var strClaimantName = $("#ClaimantName").val();
        var strReserveDesc = $("#ReserveTypeDesc").val();
        var strPolicy = $('#PolicyName').val();
        var strUnit = $('#Unit').val();
        var strCoverage = $('#Coverage').val();
        var sFDH = FDHLabels.lblFDHCaption;
        if (strClaimNo != "") {
            if (strSelectedDropdown == "1")//claim level, rkotak 8546        
                var strcaption = sFDH + "[" + strClaimNo + "]";
            else if (strSelectedDropdown == "2")//claimant level, rkotak 8546
                var strcaption =sFDH + "[" + strClaimNo + " * " + strClaimantName + "]";
            else if (strSelectedDropdown == "3") //reserve level //rkotak 8546
                var strcaption = sFDH + "[" + strClaimNo + " * " + strReserveDesc + "]";
            else if (strSelectedDropdown == "4") //policy level rkotak 8546
                var strcaption = sFDH + "[" + strClaimNo + " * " + strPolicy + "]";
            else if (strSelectedDropdown == "5")//unit level rkotak 8546
                var strcaption = sFDH + "[" + strClaimNo + " * " + strPolicy + " * " + strUnit + "]";
            else if (strSelectedDropdown == "6")//coverage level rkotak 8546
                var strcaption = sFDH + "[" + strClaimNo + " * " + strPolicy + " * " + strUnit + " * " + strCoverage + "]";
            else
                var strcaption = sFDH + "[" + strClaimNo + "]";
        }

        $("#lblcaption").text(strcaption);
        //code to populate value for caption ends
    };
    $scope.setCaption();

    $("#btnBack").click(function () {

        var sRedirectString;
        var strSysFormName = $('#SysFormName').val();
        var sClaimId = $('#ClaimId').val();
        var sClaimantEid = $('#ClaimantId').val();
        var sClaimantRowId = $('#ClaimantRowID').val();
        var sUnitId = $('#UnitId').val();
        var sFrozenflag = $('#FrozenFlag').val();
        var sClaimNumber = $('#ClaimNumber').val();
        var sFromFunds = $('#FromFunds').val();
        var ssBackToClaim = "";
        var scarrier = $('#CarrierClaim').val();

        if (strSysFormName != "") {
            switch (strSysFormName) {
                case 'claimgc':
                    sRedirectString = "/RiskmasterUI/UI/FDM/claimgc.aspx?recordID=" + $('#ClaimId').val() + "&SysCmd=0";
                    break;
                case 'claimva':
                    sRedirectString = "/RiskmasterUI/UI/FDM/claimva.aspx?recordID=" + $('#ClaimId').val() + "&SysCmd=0";
                    break;
                case 'claimwc':
                    sRedirectString = "/RiskmasterUI/UI/FDM/claimwc.aspx?recordID=" + $('#ClaimId').val() + "&SysCmd=0";
                    break;
                case 'claimdi':
                    sRedirectString = "./RiskmasterUI/UI/FDM/claimdi.aspx?recordID=" + $('#ClaimId').val() + "&SysCmd=0";
                    break;
                case 'claimpc':
                    sRedirectString = "./RiskmasterUI/UI/FDM/claimpc.aspx?recordID=" + $('#ClaimId').val() + "&SysCmd=0";
                    break;
            }
        }
        else {
            if (scarrier == "No") {
                sRedirectString = '/RiskmasterUI/UI/Reserves/ReserveListing.aspx?ClaimId=' + sClaimId + "&ClaimantId=" + sClaimantRowId + "&ClaimantEId=" + sClaimantEid + "&FrozenFlag=" + sFrozenflag + "&ClaimNumber=" + sClaimNumber;
            }
            else {
                sRedirectString = "/RiskmasterUI/UI/Reserves/ReserveListingBOB.aspx?ClaimId=" + $('#ClaimId').val();
            }
        }
        window.location.href = sRedirectString;

        return false;
    });
   
});
