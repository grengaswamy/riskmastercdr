﻿//var app = angular.module('rmaApp', ['ngGrid']);
//var sCall =  ' ';
app.controller('PendingClaimsController', function ($scope, $http, $attrs) {
    //var layoutPlugin = new ngGridLayoutPlugin();
    //$scope.completeData = [];
    $scope.sortOptions = { fields: [], directions: [] };
    //$scope.ColDefs = [];
    //$scope.FinanicalHistoryDetailData = [];
    //$scope.isAllItem = false;
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [],
        pageSize: '',
        currentPage: 1
    };

    $scope.JsonAdditionalUserPref = { showAll: false };
    //$scope.additionalInfo = false;
    $scope.check = JSON.parse(document.getElementById("hdnJsonUserPref").value).AdditionalUserPref.showAll;
    $scope.pageName = "PendingClaims.aspx"; //this is mendatory to be defined
    //PostInputDataForGrid - this variable is must in each controller. grid directive will use this array to pass input parameters used to bind the grid data
    $scope.InputDataForGrid =
    {        
        GridId: "gridPenClaims" //this is mendatory for each grid
    };

    //Add adjusters to drop down list
    var userListData = JSON.parse(document.getElementById("hdndropdownData").value);
    
    if (userListData != null) {
        if (userListData.length == 0)
        {
            chkShowAll.disabled = true;
                    
        }
        else {
            $scope.ddSelectOptions = userListData;
            $scope.ddlSelected = $scope.ddSelectOptions[0];
        }
    }
    
   
    Object.toparams = function ObjecttoParams(obj) {
        var p = [];
        for (var key in obj) {
            p.push(key + '=' + obj[key]);
        }
        return p.join('&');
    };

    $scope.RestoreDefaultCallBak = function () {
        $scope.check = false;
        $scope.ShowAllCheckBox();
    }
   
    $scope.ddlChangeEvent = function () {
        var imgLoading = $("#pleaseWaitFrame", parent.document.body);
        imgLoading.css({ display: "block" });
        $scope.inputData =
                 {

}
        $http({
            url: $scope.pageName + "/BindDataToGrid",
            method: "POST",
            data: { userId: $scope.ddlSelected.userId, showAll: $scope.check },
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .success(function (data, status, config, headers) {
            imgLoading.css({ display: "none" });
            if (JSON.parse(data.d).response == "Success") {
                $scope.UpdateScopeVariablesForGrid(JSON.parse(JSON.parse(data.d).data), JSON.parse(JSON.parse(data.d).userPref), JSON.parse(JSON.parse(data.d).additionalData));
               // $scope.setPagingData($scope.completeData, $scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize, $scope.totalServerItems);

                if (JSON.parse(JSON.parse(data.d).additionalData)[0].TotalCount > 0) {
                    $('.no-rows').hide();
                }
                else { $('.no-rows').show(); }
            }
            else {
                $scope.ShowError(JSON.parse(data.d).response);
            }

        })
        .error(function (data, status, config, headers) {
            imgLoading.css({ display: "none" });
            $scope.ShowError(JSON.parse(data.d).response);
        });
    };

    $scope.ShowAllCheckBox = function () {
        var imgLoading = $("#pleaseWaitFrame", parent.document.body);
        //set value of additional field parameter with checkbox state
        $scope.JsonAdditionalUserPref.showAll = $scope.check;
        //$scope.additionalInfo = $scope.check;
        imgLoading.css({ display: "block" });
        $scope.inputData =
                 {
                     
                     
                 }
        var l_userId;
                try {
            l_userId = $scope.ddlSelected.userId;
        }
        catch (err) {
            l_userId = -1;
        }
        $http({
            url: $scope.pageName + "/BindDataToGrid",
            method: "POST",
            data: { userId: l_userId, showAll: $scope.check },
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .success(function (data, status, config, headers) {
            imgLoading.css({ display: "none" });
            if (JSON.parse(data.d).response == "Success") {
                $scope.UpdateScopeVariablesForGrid(JSON.parse(JSON.parse(data.d).data), JSON.parse(JSON.parse(data.d).userPref), JSON.parse(JSON.parse(data.d).additionalData));
                //$scope.setPagingData($scope.completeData, $scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize, $scope.totalServerItems);
                if (JSON.parse(JSON.parse(data.d).additionalData)[0].TotalCount > 0) {
                    $('.no-rows').hide();
                }
                else { $('.no-rows').show(); }

            }
            else {
                $scope.ShowError(JSON.parse(data.d).response);
            }

        })
        .error(function (data, status, config, headers) {
            imgLoading.css({ display: "none" });
            $scope.ShowError(JSON.parse(data.d).response);
        });
    };
    
    ////$scope.savePreference = function () {

    ////    $scope.colDef = [];
    ////    for (var i = 0; i < $scope.colCollection.length; i++) {
    ////        $scope.colDef.push({
    ////            field: $scope.colCollection[i].colDef.field,
    ////            displayName: $scope.colCollection[i].displayName,
    ////            defaultName: $scope.colCollection[i].colDef.defaultName,
    ////            visible: $scope.colCollection[i].visible,
    ////            headerCellTemplate: $scope.colCollection[i].headerCellTemplate,
    ////            cell: $scope.colCollection[i].colDef.cellTemplate,
    ////            width: $scope.colCollection[i].width
    ////            //position:

    ////        });
    ////    }
    ////    $scope.gridPreference =
    ////       {
    ////           colDef: $scope.colDef,
    ////           PageSize: $scope.pagingOptions.pageSize,
    ////           SortType: "asc",
    ////           SortColumn: ""
    ////       }

       

    ////    //return false;
    ////};

   // var myEl = angular.element(document.querySelector('#btnRestoreDefault'));
   // myEl.attr('ng-click', "RestorePenclaim()");
   //// (document.getElementsByTagName('img')[1]).attributes["ng-click"] = "RestorePenclaim()";
   // $scope.RestorePenclaim = function () {
   //     alert("inside REstore")
   // };
    
  //  $('#gridPenClaims').on('click', '#btnRestoreDefault', function ()
  //  {   alert("inside REstore");
  //  });
  ////  $scope.$apply();
    if($("#pleaseWaitFrame", parent.document.body)[0].style.cssText=='display: block;') 
        $("#pleaseWaitFrame", parent.document.body)[0].style.cssText = 'display: none;'

});
