
/*
//-- Author	:	Vaibhav Kaushik
//-- Dated	:	22th,Dec 2005
//-- Purpose:	Contains client side functions.
*/

	var m_codeWindow = null ;
	var m_TabList=null;
	var NewScriptWindow = null ;
	var m_arrCompleteObjectTypeList = null ;
	
	var m_ImgFolderOpen = "csc-Pages/riskmaster/ScriptEditor/Common/Images/Collapse.gif" ; 
	var m_ImgFolderClosed = "csc-Pages/riskmaster/ScriptEditor/Common/Images/Expand.gif" ;
	
//	function ToggleFolder( Id ) {

//		var sNodeName = "NodeName-" + Id ;
//		var sIconName = "IconName-" + Id ;
//		
//		if( document.getElementById(sNodeName).style.display != "inline" ) 
//        {
//			document.getElementById(sNodeName).style.display = "inline";			
//			document.getElementById(sIconName).src = m_ImgFolderOpen ;
//			folderstate += "[" + Id + "]";
//		} 
//		else 
//		{
//			document.getElementById(sNodeName).style.display = "none";
//			document.getElementById(sIconName).src = m_ImgFolderClosed ;
//			folderstate = folderstate.replace("[" + Id + "]", "");
//		}
//		return false;   
//	}	
	
	function ShowScript( iScriptId ) {
    // Check for any unsaved data.
    //MITS 12540:Asif Start
    //below code commented by Asif  
    // Check for any unsaved data.
    //if( DataSaveAlert() )
    //return false;
	    //MITS 12540:Asif End
	    var bisIE = true;
		var sFunctionToCall = "" ;
		var sQueryString = "" ;
		var sIconName = "IconName-" + iScriptId ;		
		var sLastHyperLinkSelected ;
		var browser = get_browser();
		
		//PJS : Commented the cod as using TreeView
//		// Get the current HyperLink selected.
//		if( self.parent.frames["MainFrame"].document.getElementById("RowId") != null )
//		{
//			if( self.parent.frames["MainFrame"].document.getElementById("RowId").value != 0 )
//				sLastHyperLinkSelected = "IconName-" + self.parent.frames["MainFrame"].document.getElementById("RowId").value ;
//		}		
//		
//		// If any HyperLink is selected, unhighlight it.
//		if( sLastHyperLinkSelected != null )
//			self.parent.frames["LeftTree"].document.getElementById(sLastHyperLinkSelected).style.cssText = "FONT-SIZE: 10pt;FONT-FAMILY: Tahoma, Arial, Helvetica, sans-serif;HEIGHT: 10px;text-decoration:none;vertical-align: top; BACKGROUND-COLOR: none;COLOR: black;";
//		
//		// Higlight the selected HyperLink.
//		self.parent.frames["LeftTree"].document.getElementById(sIconName).style.cssText = "FONT-SIZE: 10pt;FONT-FAMILY: Tahoma, Arial, Helvetica, sans-serif;HEIGHT: 10px;text-decoration:none;COLOR: white;vertical-align: top;BACKGROUND-COLOR: darkblue;";
//		
		// Set the querystring and call Load script.
		sFunctionToCall = "ScriptEditorAdaptor.LoadScript"
		sQueryString = "rowid=" + iScriptId + "&functiontocall=" + sFunctionToCall;
	    //Bkuzhanthaim : RMA-7043 :Script editor page is not loading properly in  Chrome
		if (browser.trim().toLowerCase().indexOf("ie") > -1)
		    self.parent.frames["MainFrame"].location.href = "../ScriptEditor/ScriptView.aspx?" + sQueryString;
		else 
		    self.parent.frames["MainFrame"].src = "../ScriptEditor/ScriptView.aspx?" + sQueryString;
		// Show the Progress window.
		OnSubmitMainFrame();

		return false;  
	}
	
	function TreeOnLoad()
	{
	    var iScriptId;
	    var LeftTreeDocument;
	    var browser = get_browser();
	    if (browser.trim().toLowerCase().indexOf("ie") > -1)
	        LeftTreeDocument = self.parent.frames["LeftTree"].document;
	    else
	        LeftTreeDocument = self.parent.frames["LeftTree"].contentWindow.document;

	    var iScriptId = LeftTreeDocument.getElementById("FocusScriptId").value;
	    if (LeftTreeDocument.getElementById("IconName-" + iScriptId) != null)
	        LeftTreeDocument.getElementById("IconName-" + iScriptId).focus();
		
	}
	
	function ScriptViewOnLoad() {
	    var LeftTreeDocument, ToolBarDocument;
	    var browser = get_browser();
	    var IEbrowser = false;
	    var oldUrl;
	    // Close the progress window.
		CloseProgressWindow();
		// define TABS. 
		loadTabList(); 		
		tabChange('Properties');
		if (browser.trim().toLowerCase().indexOf("ie") > -1) {
		    IEbrowser = true;
		    LeftTreeDocument = self.parent.frames["LeftTree"].document;
		    ToolBarDocument = self.parent.frames["ToolBar"].document;
		}
		else
		{
		    LeftTreeDocument = self.parent.frames["LeftTree"].contentWindow.document;
		    ToolBarDocument = self.parent.frames["ToolBar"].contentWindow.document;
		}
		// If window is loading after SAVE.
		if( document.forms[0].hFunctionToCall.value == "ScriptEditorAdaptor.SaveScript" )
		{
		    var sIconName = "IconName-" + document.forms[0].RowId.value;
		    // If this is a NEW Script save, refresh the TREE.
		    if (LeftTreeDocument.getElementById(sIconName) == null)
			{
				// Set the ScriptId which need to be highlight.
		        LeftTreeDocument.getElementById("FocusScriptType").value = document.forms[0].ScriptType.value;
		        LeftTreeDocument.getElementById("FocusScriptId").value = document.forms[0].RowId.value;
		        LeftTreeDocument.forms[0].method = "post";
		        if(IEbrowser)
		            //oldUrl = LeftTreeDocument.window.location.href;
		            oldUrl = LeftTreeDocument.location.href;
		        else
		            oldUrl = self.parent.frames["LeftTree"].src;
				var newUrl = oldUrl;
				if (oldUrl.indexOf("?") != -1) {
				    newUrl = oldUrl.substring(0, oldUrl.indexOf("?")) + "?rowid=" + document.forms[0].RowId.value;
				}
				else {
				    newUrl = oldUrl + "?rowid=" + document.forms[0].RowId.value;
				}
				if (IEbrowser)
				    LeftTreeDocument.location.href = newUrl;
				else
				    self.parent.frames["LeftTree"].src = newUrl;
			}
		    if (ToolBarDocument.getElementById('SysIsFormSubmitted') != null) {
		        if (ToolBarDocument.getElementById('SysIsFormSubmitted').value == "1")
		            ToolBarDocument.getElementById('SysIsFormSubmitted').value = "";
			}

        }
        var errorControl = document.getElementById("ErrorControl1_lblError");
        if (errorControl.innerHTML == "") {
            parent.m_DataChanged = false;
        }
        parent.parent.MDIScreenLoaded();
	}

	function OnSubmitMainFrame()
	 {
	    var browser = get_browser();
	    if (browser.trim().toLowerCase().indexOf("ie") > -1)
	        self.parent.frames["MainFrame"].pleaseWait.Show();
	    else
	        self.parent.frames["MainFrame"].contentWindow['pleaseWait'].Show();
     }

	function SaveScript() {
	    var sQueryString = "";
	    var rowid = -1;
	    var MainFrameDocument;
	    var browser = get_browser();
         if (document.getElementById('SysIsFormSubmitted') != null) {
             if (document.getElementById('SysIsFormSubmitted').value == "1") {
                 alert('Application is busy saving record. Please try again later.');
                 return true;
             }
             else {
                 document.getElementById('SysIsFormSubmitted').value = "1";
                 //return false;
             }
         }
         //return false;   
         //Bkuzhanthaim : RMA-7043 :Script editor page is not loading properly in  Chrome
         if (browser.trim().toLowerCase().indexOf("ie") > -1)
             MainFrameDocument = self.parent.frames["MainFrame"].document;
         else
             MainFrameDocument = self.parent.frames["MainFrame"].contentWindow.document;

         if (MainFrameDocument.getElementById("RowId") != null)
	     {
             rowid = MainFrameDocument.getElementById("RowId").value;
	     }
		// Save the Script.		
         if (MainFrameDocument.forms[0].hFunctionToCall != null)
		{
             MainFrameDocument.forms[0].hFunctionToCall.value = "ScriptEditorAdaptor.SaveScript";
             MainFrameDocument.forms[0].method = "post";
             MainFrameDocument.forms[0].submit();
			
			// Show Progress window.
			OnSubmitMainFrame();
			// This will refresh the Script Tree after script is saved successfully.
			sQueryString = "rowid=" + rowid;
//			if (rowid == "-1")
//			    self.parent.frames["LeftTree"].location.href = "../ScriptEditor/LeftTreeLoad.aspx";
//		    else
//		        self.parent.frames["LeftTree"].location.href = "../ScriptEditor/LeftTreeLoad.aspx?" + sQueryString;
		}
		return false ;
	}

	function DeleteScript() {
         var MainFrameDocument, LeftTreeDocument;
         var IEbrowser = false;
         var browser = get_browser();
         //Bkuzhanthaim : RMA-7043 :Script editor page is not loading properly in  Chrome
         if (browser.trim().toLowerCase().indexOf("ie") > -1)
         {
             IEbrowser = true;
             MainFrameDocument = self.parent.frames["MainFrame"].document;
             LeftTreeDocument = self.parent.frames["LeftTree"].document;
         }
         else
         {
             MainFrameDocument = self.parent.frames["MainFrame"].contentWindow.document;
             LeftTreeDocument = self.parent.frames["LeftTree"].contentWindow.document;
         }
		// If any Script is selected.
         if (MainFrameDocument.getElementById("RowId") != null)
		{
             var iScriptId = MainFrameDocument.getElementById("RowId").value;
			// Is Script is in database.
			if( iScriptId > 0 )
			{	//Changed by Saurabh Arora for MITS 18558:Start
                if(confirm("Are you sure you want to delete the script?"))
				{
				//Changed by Saurabh Arora for MITS 18558:End
                    var sQueryString = "DeleteRowid=" + iScriptId;
                    // Delete the script.
				    LeftTreeDocument.getElementById("hdnAction").value = "Delete";
				    LeftTreeDocument.getElementById("RowID").value = iScriptId;

				    LeftTreeDocument.forms[0].method = "post";
				    LeftTreeDocument.forms[0].submit();
				    //LeftTreeDocument.getElementById("btnpostback").click();
				    //LeftTreeDocument.getElementById("frmLeftTree").submit;
				    //LeftTreeDocument.frmLeftTree.submit();
				    //LeftTreeDocument.forms['frmLeftTree'].submit();
				    
				// Show Progress window.
				OnSubmitMainFrame();

                    // This will refresh the Script Tree after script is deleted successfully.
				if (IEbrowser)
				{
				    self.parent.frames["MainFrame"].location.href = "../ScriptEditor/NoData.aspx";
				    self.parent.frames["LeftTree"].location.href = "../ScriptEditor/LeftTreeLoad.aspx";
				}
				else
				{
				    self.parent.frames["MainFrame"].src = "../ScriptEditor/NoData.aspx";
				    self.parent.frames["LeftTree"].src = "../ScriptEditor/LeftTreeLoad.aspx?hdnAction=postback&" + sQueryString;
				}
				}
			}

        }
				
	}
	
	function DeleteScriptOnLoad()
     {
	    var browser = get_browser();
	    // This Function will refresh the Script Tree after script is deleted successfully.
	    if (browser.trim().toLowerCase().indexOf("ie") > -1)
	    {
	        self.parent.frames["MainFrame"].location.href = "../ScriptEditor/NoData.aspx";
	        self.parent.frames["LeftTree"].location.href = "../ScriptEditor/LeftTreeLoad.aspx";
	    }
	    else
	    {
	        self.parent.frames["MainFrame"].src = "../ScriptEditor/NoData.aspx";
	        self.parent.frames["LeftTree"].src = "../ScriptEditor/LeftTreeLoad.aspx";
	    }
	    		
	}
	
	function AddNewScript()
	{
		var width = 360 ;
		var height = 260 ;
		
		// Check for any unsaved data.		
		if( DataSaveAlert() )
			return false;
		// Load a new script.

        NewScriptWindow = window.open("../ScriptEditor/NewScript.aspx", 'NewScript', 'width=' + width + ',height=' + height + ',top=' + (screen.availHeight - height) / 2 + ',left=' + (screen.availWidth - width) / 2 + ',resizable=no,scrollbars=no'); 		
		return false ;
	}
	
	function RefreshPage()
	{
	    var browser;
		// Check for any unsaved data.		
		if( DataSaveAlert() )
			return false;
	    //Bkuzhanthaim : RMA-7043 :Script editor page is not loading properly in  Chrome
		browser = get_browser();
		if (browser.trim().toLowerCase().indexOf("ie") > -1)
		    MainFrameDocument = self.parent.frames["MainFrame"].document;
		else
		    MainFrameDocument = self.parent.frames["MainFrame"].contentWindow.document;
		// Is there any script which need to be Refreshed.

		if (MainFrameDocument.getElementById("Rowid") != null)
		{
		    var iScriptId = MainFrameDocument.getElementById("Rowid").value;
			// Reload the script.
			ShowScript( iScriptId );
		}
	}
	
	function DataSaveAlert()
	{
		if( self.parent.frames["MainFrame"].m_DataChanged )
		{
			if( confirm('Data has changed. Do you want to save changes?') )
			{
				SaveScript();
				return true ;
			}
		}
		return false ;	
	}
			
			
	function NewScriptOk() {
	    var browser = get_browser();
		if( document.forms[0].cboScriptType.value == "" )
		{
			alert( "Please select the Script Type" );
			return false ;
		}
		if( document.forms[0].cboObjectType.value == "" )
		{
			alert( "Please select the Object Type" );
			return false ;
		}
		var iScriptType ;
		var sObjectType ;
		var sFunctionToCall = "" ;
		var objParentWindow ;
		var sQueryString = "" ;
				
		iScriptType = document.forms[0].cboScriptType.value ;
		sObjectType = document.forms[0].cboObjectType.value ;
		sFunctionToCall = "ScriptEditorAdaptor.NewScript"
		sQueryString = "scripttype=" + iScriptType + "&objectname=" + sObjectType + "&functiontocall=" + sFunctionToCall;
		
		objParentWindow = self.opener ;
		self.close();
		
		//abisht MITS 11115
		//var wnd=window.open('csc-Theme/riskmaster/common/html/progress.html','progressWnd',
		//		'width=400,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-400)/2);
	    //objParentWindow.parent.wndProgress = wnd ;
		if (browser.trim().toLowerCase().indexOf("ie") > -1)
		    objParentWindow.parent.frames["MainFrame"].location.href = "../ScriptEditor/ScriptView.aspx?" + sQueryString;
		else
		    objParentWindow.parent.frames["MainFrame"].src = "../ScriptEditor/ScriptView.aspx?" + sQueryString;
		
//		OnSubmitMainFrame();
		
		return false ;			
	}
	
	function FilterObjectTypes() {
		var iIndex = 0 ;
		var sOptionText = "" ;
		var sSelectedObjectTypeList = "" ;
		var iSelectedIndex = document.forms[0].cboScriptType.selectedIndex;
		var LeftTreeDocument;
	        //Bkuzhanthaim : RMA-7043/RMA-9365 : JavaScript Error at Script Editor
		LeftTreeDocument = window.opener.parent.document.getElementById("LeftTree").contentWindow.document;
		if( iSelectedIndex == 1 )
		    sSelectedObjectTypeList = LeftTreeDocument.getElementById("ValidationList").value;
		else if( iSelectedIndex == 2 )
		    sSelectedObjectTypeList = LeftTreeDocument.getElementById("CalculationList").value;
		else if( iSelectedIndex == 3 )
		    sSelectedObjectTypeList = LeftTreeDocument.getElementById("InitializationList").value;
		else if( iSelectedIndex == 4 )
		    sSelectedObjectTypeList = LeftTreeDocument.getElementById("BeforeSaveList").value;
		else if( iSelectedIndex == 5 )
		    sSelectedObjectTypeList = LeftTreeDocument.getElementById("AfterSaveList").value;
		else if ( iSelectedIndex == 6 )
		    sSelectedObjectTypeList = LeftTreeDocument.getElementById("BeforeDeleteList").value;
		
		if( m_arrCompleteObjectTypeList == null )
		{			
			m_arrCompleteObjectTypeList = new CloneObject( document.forms[0].cboObjectType.options );			
		}
		else
		{
			for( iIndex = 0 ; iIndex < document.forms[0].cboObjectType.options.length ; iIndex++ )
				document.forms[0].cboObjectType.options[iIndex] = null ;				
						
			for( iIndex = 0 ; iIndex < m_arrCompleteObjectTypeList.length ; iIndex++ )
				document.forms[0].cboObjectType.options[iIndex] = m_arrCompleteObjectTypeList[iIndex] ;					
		}
		
		for( iIndex = 0 ; iIndex < document.forms[0].cboObjectType.length ; iIndex++ )
		{
			sOptionText = document.forms[0].cboObjectType.options[iIndex].value ;			
			if( sSelectedObjectTypeList.indexOf( sOptionText + "," ) != -1  )
			{
				document.forms[0].cboObjectType.options[iIndex] = null ;											
				iIndex = iIndex - 1 ;
			}
		}					
	}
	
	
	
	function CloneObject( object )
	{
		for( iIndex in object ) 
			this[iIndex] = object[iIndex];		
	}

	function ToolBarOnLoad() {
	    if (document.getElementById('SysIsFormSubmitted') != null) {
	        if (document.getElementById('SysIsFormSubmitted').value == "1")
	            document.getElementById('SysIsFormSubmitted').value = "";
	    }
	}