var ie,ns;
var browserName = navigator.appName;                   // detect browser
var browserVersion = navigator.appVersion;

if (browserName == "Netscape")
{
	ie=0;
	ns=1;
}
else		//Assume IE
{
	ieversion=browserVersion.substr(browserVersion.search("MSIE ")+5,1);
	ie=1;
	ns=0;
}


function onPageLoaded5()
{
	document.dateSelected = dateSelected;
	document.onCodeClose = onCodeClose;
	if (ie)
	{
//		if ((eval("document.all.divForms")!=null) && (ieversion>=6))
//		    divForms.style.height = window.innerHeight * 0.58; ;
	}
	else
	{
		var o_divforms;
		o_divforms=document.getElementById("divForms");
		if (o_divforms!=null)
		{
			o_divforms.style.height=window.frames.innerHeight*0.58;
			o_divforms.style.width=window.frames.innerWidth*0.995;
		}
	}
	//start rsushilaggar MITS 19970 05/21/2010
	
	var v_voidReason = document.getElementById("txtVoidReason");
	if (v_voidReason != null) {
		document.getElementById("txtVoidReason").value = "";
	}
	var v_voidReason = document.getElementById("voidreason_HTML");
	if (v_voidReason != null) {
		document.getElementById("voidreason_HTML").value = "";
	}
//end rsushilaggar
//rsushilaggar MITS 22045 Date 01-Sep-2010
var v_hdnFormType = document.getElementById("hdnMarkChecksCleared");
var v_VoidReasonTable = document.getElementById("voidcheckreason");
    //Raman MITS 31026
if (v_hdnFormType.value == "true" && v_VoidReasonTable != null) {
    v_VoidReasonTable.style.display = "none";
}
//End rsushilaggar
}
/*
function selectAll()
{
	var chkid;
	var childnodescount = document.getElementById("childnodescount").value;
	for (var i = 1; i<=childnodescount; i++)
	{
		chkid = "chk_" + i;
		document.getElementById(chkid).checked=true;		
	}
	document.getElementById("chkTransactions").checked = true;
}

function deselectAll()
{
	var chkid;
	var childnodescount = document.getElementById("childnodescount").value;
	for (var i = 1; i<=childnodescount; i++)
	{
		chkid = "chk_" + i;
		document.getElementById(chkid).checked=false;		
		
	}
	document.getElementById("chkTransactions").checked = false;
}
*/
function selectAll()
{
	var sName, sType;
   
	for(var i = 0; i<document.forms[0].elements.length; i++)
	{
		sName = document.forms[0].elements[i].name;
		sType = document.forms[0].elements[i].type;
		if( (sType == 'checkbox') && (sName.substring(0,4)=="chk_") )
		{
			document.forms[0].elements[i].checked = true;
		}
	}
	if (document.getElementById("chkTransactions") != null) {
		document.getElementById("chkTransactions").checked = true;
	}
}

function deselectAll()
{
	var sName, sType;
	for(var i = 0; i<document.forms[0].elements.length; i++)
	{
		sName = document.forms[0].elements[i].name;
		sType = document.forms[0].elements[i].type;
		if( (sType == 'checkbox') && (sName.substring(0,4)=="chk_") )
		{
			document.forms[0].elements[i].checked = false;
		}
	}
	if (document.getElementById("chkTransactions") != null) {
		document.getElementById("chkTransactions").checked = false;
	}
}

function TransClick()
{
	if( document.getElementById("chkTransactions").checked )
		selectAll();
	else
		deselectAll();
}

function RefreshChecks()
{
	document.getElementById("txtAction").value = "1";
	return true;
}

function processChecks() {    
    var sName, sType, sId;
    var SelectedChecks = 0;
    var chkid = "";
    //start rsushilaggar MITS 19970 05/21/2010
    var sVoidReason;

	for(var i = 0; i<document.forms[0].elements.length; i++)
	{
        sName = document.forms[0].elements[i].name;

        sType = document.forms[0].elements[i].type;
        sId = document.forms[0].elements[i].id;

		if( (sType == 'checkbox') && (sName.substring(0,4)=="chk_") )
		{
				if(document.forms[0].elements[i].checked == true)
				{
                if (chkid == "") chkid = chkid + sId.substring(4, sId.length);
                else chkid = chkid + ',' + sId.substring(4, sId.length);
                //rsushilaggar MITS 21715 11-Aug-2010
                SelectedChecks = SelectedChecks + 1;
            }
        }

    }

    
	
    
	if(chkid=="")

	{
	    //alert("No check selected"); //MITS 31027 - Rakhel ML changes
	    alert(NoChecksValidations.NoCheckSelected);
            return false;
    }
    

    //MITS 22053: Yatharth
    var v_hdnFormType = document.getElementById("hdnMarkChecksCleared");


        if (v_hdnFormType.value == "true") { }
        else {
            //start rsushilaggar MITS 19970 05/21/2010
            var v_voidReason = document.getElementById("voidreason");
            if (v_voidReason != null) {
                sVoidReason = document.getElementById("voidreason").value;
                if (sVoidReason == "") {
                    alert("Please Enter Void Check Reason");
                    return false;
                }
            }
            //rsushilaggar MITS 21715 11-Aug-2010
    }
    if (SelectedChecks > 1) {
        //rsolanki2 : mits 22693
        var txtvoidcheckreason = document.getElementById("voidreason");
        if (txtvoidcheckreason != null) {
            //rsolanki2 : mits 70580
            // var bResult = confirm("You are about to apply the same void reason to multiple checks.\nPress Ok to continue...");  
            //ijha :mits 24252 08/03/2011
            //changed by Amitosh for Mits 24468 (03/25/2011)
            //Raman MITS 31026 -- Cannot rely on text of control for business decisions
            //if (document.getElementById("btnProcess").value == "Void Checks") {
            if (document.getElementById("hdnMarkChecksCleared").value == "false") {
                var bResult = confirm("You are about to apply the same void reason to multiple checks");
            }
            else {
                var bResult = confirm("The chosen checks have been successfully marked as cleared");
            }
            //end Amitosh
            if (!bResult) { return false; }
        }
    }
    //start rsushilaggar MITS 19970 05/21/2010
    document.getElementById("selectedtransids").value = chkid;
    return true;
}
