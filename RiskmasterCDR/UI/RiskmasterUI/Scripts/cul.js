 /*************************************************************
	 * $File		: cul.js
	 * $Revision	: 1.0
	 * $Date		: 10/16/07
	 * $Author		: Geeta Sharma
	 * $Comment		: Customized User List Enhancement
	 * $Source		:  	
 *************************************************************/
var m_sFirstName ="";
var m_sLastName ="";
var m_sGroupName ="";
var m_bFirstTime = false;
var sUserGroupsName = [];

  //Changed by Gagan for MITS 19725 : start
function AddCustomizedListUserLookup(sParent, sListName, sUserId, sUserName) {   
    document.OnAddCustomizedListUser = OnAddCustomizedListUser;
    m_codeWindow = window.open('/RiskmasterUI/UI/SysUsers/CustomizeUserList.aspx?parent=' + sParent + '&parentcontrolname=' + sListName + '&userid=' + sUserId + '&username=' + sUserName + '&lookup=' + 'true', 'codeWnd',
				     'width=600,height=400' + ',top=' + (screen.availHeight - 200) / 2 + ',left=' + (screen.availWidth - 400) / 2 + ',resizable=yes,scrollbars=yes'); //igupta3 Mits: 33301

    return false;
}
  //Changed by Gagan for MITS 19725 : end

function AddCustomizedListUser(sParent, sListName, sUserId, sUserName) {   
    document.OnAddCustomizedListUser = OnAddCustomizedListUser;
    m_codeWindow = window.open('/RiskmasterUI/UI/SysUsers/CustomizeUserList.aspx?parent=' + sParent + '&parentcontrolname=' + sListName + '&userid=' + sUserId + '&username=' + sUserName, 'codeWnd',
				     'width=600,height=400' + ',top=' + (screen.availHeight - 200) / 2 + ',left=' + (screen.availWidth - 400) / 2 + ',resizable=yes,scrollbars=yes'); //igupta3 Mits: 33301

    return false;
}

function OnAddCustomizedListUser(sId, sText, sParent, sListName, sUserId, sUserName) {

    var sCustomizedUserList  = "";
    var sCustomizedUserIdList  = "";
    var sSeperator = " ";    
    var objCtrl = null;
    var objCtrlName = null;
    var objCtrlId = null;
    var sUserLoginString = "";   
    var sDisplayUser = "";    
    var objOption = null;
    var sIdEmail = "";

   
    objCtrl=document.getElementById(sListName);
    //akaur9 MITS 26518
    if (sParent == 'toplevelapproval')
    {
        objCtrl.value = sText[0] + ', ' + sText[1] + ' (Login Name: ' + sText[2] + ')';
        objCtrlId = document.getElementById(sUserId);
       
        objCtrlId.value = sId;
        return true;   
    }

    if(sParent == 'routediary' || sParent == 'emaildocs' || sParent == 'peekdiary')
    {
       if(sParent == 'emaildocs')
       {   
           if(sText[3] != "")
           {                     
                    if(objCtrl.value != "")
                    {                
                        objCtrl.value = objCtrl.value + ' , ' + sText[3];                                                    
                    }                   
                    else
                    {
                        objCtrl.value =  sText[3];               
                    }               
           }          
       }
       else
       {    
            objCtrl.value = sText[2];
            objCtrlName = document.getElementById(sUserName);                   
            objCtrlName.value = sText[0] + ' ' + sText[1];    
       }          
             
       return true;   
    }
        
    objCtrlId = document.getElementById(sUserId);      
    objCtrlName = document.getElementById(sUserName);       
//mbahl3 Strataware enhancement
    if (sParent == 'GeneralSystemParameterSetup') {
        objCtrl.value = sText[1] + ' ' + sText[0];;
        objCtrlName.value = sText[2];
        objCtrlId.value = sId;
        return true;
    }
    //mbahl3 Strataware enhancement
    if(sParent == 'autodiarysetup')
    {
        sSeperator = '|';   
        sUserLoginString = sText[0] + " " + sText[1] + " " + sText[2];        
    }    
    else if(sParent == 'enhancednotes')
    {
        sSeperator = ','; 
        sUserLoginString = sText[2] + " " + sText[0] + " " + sText[1]; 
    }   
    
    else if(sParent == 'creatediary')
    {           
        sUserLoginString = sText[2] ;  
        //sDisplayUser = sText[0] + sText[1] ;  
        sDisplayUser = sUserLoginString ;
    }

    else if (sParent == 'weblinksetup') {
        sUserLoginString = sText[2];
        //sDisplayUser = sText[0] + sText[1] ;  
        sDisplayUser = sUserLoginString;
    }

    else if(sParent == 'email-doc')
    {   
        if(sText[3] != "")
        {
            sUserLoginString = sText[0] + " " + sText[1] + "(" + sText[2] + ")";                
            sIdEmail = sText[3];   
        }        
    }
    else
    {
        sUserLoginString = sText[2] ;          
    }

    if (sParent != 'creatediary' && sParent != 'weblinksetup')
    {
        sDisplayUser = sUserLoginString ;
    }
    
    var bAdd=true;
    
    for(var i=0;i < objCtrl.length;i++)
    {
      if(objCtrl.options[i].value==sId && sId!="0")
        bAdd=false;
    }
    if(bAdd)
    {  
        if(sDisplayUser != "")
        {
            objOption =	new Option(sDisplayUser, sId, false, false);
            objCtrl.options[objCtrl.length] = objOption;
            //Aman MITS 27357--Start
            if (sParent != 'creatediary' && sParent != 'enhancednotes' && sParent != 'autodiarysetup')
                objCtrl=null;
        }
        //MITS 27309 : Ishan
        //if (sParent == 'creatediary' || sParent != 'weblinksetup')
        if (sParent == 'creatediary' || sParent == 'enhancednotes' || sParent == 'autodiarysetup')   //Aman MITS 27357--End
	{  
            objCtrlName.value = "";
            for (var i = 0; i < objCtrl.length; i++) {
                if (i == 0)
                    objCtrlName.value = objCtrl.options[i].text;
                else
                    objCtrlName.value = objCtrlName.value + sSeperator + objCtrl.options[i].text;
            }
//            if (objCtrlId.value != "")
//                objCtrlId.value = objCtrlId.value + sSeperator;
            for (var i = 0; i < objCtrl.length; i++) {
                if (i == 0)
                    objCtrlId.value = objCtrl.options[i].value;
                else
                    objCtrlId.value = objCtrlId.value + sSeperator + objCtrl.options[i].value;
            }
            objCtrl = null;
        }
        else {
            if (objCtrlName.value != "")
                objCtrlName.value = objCtrlName.value + sSeperator;
            objCtrlName.value = objCtrlName.value + sUserLoginString;

            if (objCtrlId.value != "")
                objCtrlId.value = objCtrlId.value + sSeperator;
        }  
        if(sParent == 'email-doc')
        {      
            objCtrlId.value=objCtrlId.value + sIdEmail;           
        }
        else {
            if (sParent != 'creatediary' && sParent != 'enhancednotes' && sParent != 'autodiarysetup')   //Aman MITS 27357
            objCtrlId.value=objCtrlId.value + sId; 
        }
    }
    
    return true;     
  }
  
 function DelCustomizedListUser(sParent,sListName,sUserId,sUserName) { 
    var objCtrl = null;
    var objCtrlId = null;
    var objCtrlName = null;
    var sId="";
    var sSeperator = " ";   
    var sTempEmailIds = null;
    var arrEmailId = "";
    var groupFlag = 0;
    
    objCtrl=document.getElementById(sListName); 
    objCtrlId = document.getElementById(sUserId); 
    objCtrlName = document.getElementById(sUserName);       
    
    if(sParent == 'email-doc')
    {        
        sTempEmailIds = new String(objCtrlId.value);       
        arrEmailId = sTempEmailIds.split(' ');
    }
    
    if(sParent == 'autodiarysetup')
    {
        sSeperator = '|';           
    }
    else if(sParent == 'enhancednotes')
    {
        sSeperator = ',';    
    } 
            
    if(objCtrl.selectedIndex < 0)
        return false;
    
    var bRepeat=true;
    if (sParent == "autodiarysetup") {
        if (sUserId == "CtrlAutoDiaryWizard_SendUsersIdList") {
            if (document.getElementById('hdnDiaryLstUsers') && document.getElementById('CtrlAutoDiaryWizard_SendUsersIdList'))
                document.getElementById('hdnDiaryLstUsers').value = document.getElementById('CtrlAutoDiaryWizard_SendUsersIdList').value;
            if (document.getElementById('hdnDiaryLstGroups'))
                document.getElementById('hdnDiaryLstGroups').value = document.getElementById('CtrlAutoDiaryWizard_SendGroupsIdList').value;
        }
        else if (sUserId == "CtrlAutoDiaryWizard_UsersNotifiedIdList") {
            if (document.getElementById('hdnDiaryLstUsers') && document.getElementById('CtrlAutoDiaryWizard_UsersNotifiedIdList'))
                document.getElementById('hdnDiaryLstUsers').value = document.getElementById('CtrlAutoDiaryWizard_UsersNotifiedIdList').value;
            if (document.getElementById('hdnDiaryLstGroups'))
                document.getElementById('hdnDiaryLstGroups').value = document.getElementById('CtrlAutoDiaryWizard_GroupsNotifiedIdList').value;
        }
        else {
            if (document.getElementById('hdnDiaryLstUsers') && document.getElementById('CtrlAutoDiaryWizard_UsersRoutedIdList'))
                document.getElementById('hdnDiaryLstUsers').value = document.getElementById('CtrlAutoDiaryWizard_UsersRoutedIdList').value;
            if (document.getElementById('hdnDiaryLstGroups') && document.getElementById('CtrlAutoDiaryWizard_UsersRoutedIdList'))
                document.getElementById('hdnDiaryLstGroups').value = document.getElementById('CtrlAutoDiaryWizard_GroupsRoutedIdList').value;
        }
    }
    var varhdnUserStr = document.getElementById('hdnDiaryLstUsers');
    var varhdnGroupStr = document.getElementById('hdnDiaryLstGroups');
    var hdnGroupStrlength = varhdnGroupStr.value.length;
    var hdnUserStrlength = varhdnUserStr.value.length;
    var strGroup = varhdnGroupStr.value.replace(/\|/g, ",").split(",");
    var strUser = varhdnUserStr.value.replace(/\|/g, ",").split(",");
    
    while(bRepeat)
    {
        bRepeat = false;
        for (var i = 0; i < objCtrl.length; i++)
        {
            if(objCtrl.options[i].selected)
            {
                if(sParent == 'email-doc')
                {
                    arrEmailId.splice(i,1)
                }
                if (sParent == 'creatediary' || sParent == 'routediary' || sParent == 'autodiarysetup') {
                    if (varhdnGroupStr) {

                        for (var j = 0; j < strGroup.length; j++) {
                            if ((objCtrl.options[i].value == strGroup[j])) {
                                strGroup.splice(j, 1);
                            }
                        }
                    }
                    if (varhdnUserStr) {
                        for (var k = 0; k < strUser.length; k++) {
                            if (sParent == 'autodiarysetup') {
                                if ((objCtrl.options[i].value == strUser[k])) {
                                    strUser.splice(k, 1);
                                }
                            }
                            else {
                                if ((objCtrl.options[i].innerHTML == strUser[k])) {
                                    strUser.splice(k, 1);
                                }
                            }
                        }
                    }
                }
                objCtrl.options[i]=null;
                bRepeat=true;
                break;
            }

        }

        if (document.getElementById('hdnDiaryLstGroups'))
            document.getElementById('hdnDiaryLstGroups').value = strGroup.join(",");
        if (document.getElementById('hdnDiaryLstUsers'))
            document.getElementById('hdnDiaryLstUsers').value = strUser.join(",");
        if (sParent == "autodiarysetup") {
            if (sUserId == "CtrlAutoDiaryWizard_SendUsersIdList") {
                if (document.getElementById('CtrlAutoDiaryWizard_SendGroupsIdList'))
                    document.getElementById('CtrlAutoDiaryWizard_SendGroupsIdList').value = strGroup.join(",");
                if (document.getElementById('CtrlAutoDiaryWizard_SendUsersIdList'))
                    document.getElementById('CtrlAutoDiaryWizard_SendUsersIdList').value = strUser.join(",");
            }
            else if (sUserId == "CtrlAutoDiaryWizard_UsersNotifiedIdList") {
                if (document.getElementById('CtrlAutoDiaryWizard_GroupsNotifiedIdList'))
                    document.getElementById('CtrlAutoDiaryWizard_GroupsNotifiedIdList').value = strGroup.join(",");
                if (document.getElementById('CtrlAutoDiaryWizard_UsersNotifiedIdList'))
                    document.getElementById('CtrlAutoDiaryWizard_UsersNotifiedIdList').value = strUser.join(",");
            }
            else {
                if (document.getElementById('CtrlAutoDiaryWizard_GroupsRoutedIdList'))
                    document.getElementById('CtrlAutoDiaryWizard_GroupsRoutedIdList').value = strGroup.join(",");
                if (document.getElementById('CtrlAutoDiaryWizard_UsersRoutedIdList'))
                    document.getElementById('CtrlAutoDiaryWizard_UsersRoutedIdList').value = strUser.join(",");
            }
        }
    }
     //deb:MITS 24453
    if (objCtrl.length > 0)
    {
        objCtrlId.value = "";
        objCtrlName.value = "";
        for (var i = 0; i < objCtrl.length; i++)
        {
            if (sParent == 'creatediary' || sParent == 'autodiarysetup')
            {
                for (var j = 0; j < strGroup.length; j++)
                {
                    if (objCtrl.options[i].value == strGroup[j])
                    {
                        groupFlag = 1;
                    }
                }
                if (groupFlag != 1)
                {
                    objCtrlName.value = objCtrlName.value + sSeperator + objCtrl.options[i].text;
                    objCtrlId.value = objCtrlId.value + sSeperator + objCtrl.options[i].value;
                }
                groupFlag = 0;
            }
            else
            {
                if (i == 0)
                {
                    objCtrlName.value = objCtrl.options[i].text;

                    if (sParent == 'email-doc')
                    {
                        objCtrlId.value = arrEmailId[i];
                    }
                    else
                    {
                        objCtrlId.value = objCtrl.options[i].value;
                    }
                }
                else
                {
                    objCtrlName.value = objCtrlName.value + sSeperator + objCtrl.options[i].text;

                    if (sParent == 'email-doc')
                    {
                        objCtrlId.value = objCtrlId.value + sSeperator + arrEmailId[i];
                    }
                    else
                    {
                        objCtrlId.value = objCtrlId.value + sSeperator + objCtrl.options[i].value;
                    }

                }
            }
        }
    }
    else
    {
        objCtrlId.value = "";
        objCtrlName.value = "";
    }
      return true;
  }

 function OnOK()
 {
    
    var selectedTemp;
    var selected ;        
    var sParent = "";
    var sId = "";
    var sUsersTemp = "";
    var sSysUserField = "";             
    var sUserLoginName = "";  
    var sUserlistname = "";
    var sUserGroupSelected = "";
    var sUseridstr = "";
    var sUsernamestr = "";
    var objElem = null;
    var sParentControlname = "";
    var sMultiSelct = "";
    var sUserGroup = "";
    var commaSeparator = "";
    //mbahl3 Strataware enhancement
    //  sParent = document.forms[0].hdnParentform.value;     
    sParent = document.getElementById('hdnParentform').value;
    // Mits 35697 starts
    
    // npadhy JIRA 6415 Retrieve the value User Group - For Supplemental - User group it will have value 0 for users and 1 for groups, and for rest of controls it would be blank
    sUserGroup = document.getElementById('hdnUserGroups').value;
    commaSeparator = document.getElementById('hdnCommaSeparator').value;
    if (sParent != 'creatediary' && sParent != 'routediary' && sParent != 'autodiarysetup') {
        if (sUserGroup == '' || sUserGroup == '0')
        {
            var scount = document.getElementById('hdnUserData').value.split('/');
            for (var i = 0 ; i < scount.length; i++)
            {
                if (scount[i] != '')
                {
                    selected = scount[i];
                    // Mits 35697 ends
                    selectedTemp = new String(selected);
                    sId = selectedTemp.substring(0, selectedTemp.indexOf(','));
                    sUsersTemp = selectedTemp.substring(selectedTemp.indexOf(',') + 1, selectedTemp.length);
                    sUsersTemp = sUsersTemp.split(",");
                    sUserLoginName = sUsersTemp[0] + " " + sUsersTemp[1] + " " + sUsersTemp[2];

                    if (window.opener != null && window.opener != self && sParent == 'RM_SYS_USERS')
                    {
                        sParentControlname = document.forms[0].hdnParentControlname.value;
                        objElem = eval('window.opener.document.forms[0].' + sParentControlname);

                        if (objElem != null)
                        {
                            objCtrl = eval('window.opener.document.forms[0].' + sParentControlname + "_cid");
                            objElem.value = sUserLoginName;
                            objCtrl.value = sId;
                            window.opener.setDataChanged(true);
                        }
                    }
                    else
                    {
                        //sUserlistname = document.forms[0].hdnParentControlname.value;
                        //sUseridstr = document.forms[0].hdnUseridstr.value;
                        //sUsernamestr=  document.forms[0].hdnUsernamestr.value;
                        sUserlistname = document.getElementById('hdnParentControlname').value
                        sUseridstr = document.getElementById('hdnUseridstr').value
                        sUsernamestr = document.getElementById('hdnUsernamestr').value

                        //mbahl3 Strataware enhancement
                        //sharishkumar Jira 6415]                 
                        if (sParent.indexOf('_usrgrpbtn') != -1)
                        {
                            sMultiSelct = document.getElementById('hdnMultiSelect').value;
                            window.opener.document.OnAddSuppCustomListUserLookup(sId, sUsersTemp, sUserlistname, sMultiSelct, sUserGroup, commaSeparator);
                        }
                        else
                            //sharishkumar Jira 6415 ends
                            window.opener.document.OnAddCustomizedListUser(sId, sUsersTemp, sParent, sUserlistname, sUseridstr, sUsernamestr);
                    }
                }
                //}            
            }
        }
        else
        {
            //sharishkumar 8618 starts - 
            //As null is disabled the condition (below) will be true if and only if ALL is selected.
            //If all is selected - we will first select all the options
            //then remove selection from All and Null
            if ($('#lstUserGroups > option[value=0]').prop('selected')) {
                $('#lstUserGroups > option').prop('selected', true);
                $('#lstUserGroups > option[value=0]').prop('selected', false);
            }
            //sharishkumar Jira 8618 - ends
            // We need to retrieve the values for User Type
            $("#lstUserGroups > option:selected").each(function () {
                sId = this.value;
                sUsersTemp = this.text;
                sUserlistname = document.getElementById('hdnParentControlname').value
                sUseridstr = document.getElementById('hdnUseridstr').value
                sUsernamestr = document.getElementById('hdnUsernamestr').value
                sMultiSelct = document.getElementById('hdnMultiSelect').value;
                window.opener.document.OnAddSuppCustomListUserLookup(sId, sUsersTemp, sUserlistname, sMultiSelct, sUserGroup, commaSeparator);
            });
        }
    }
    else {
        var scount = document.getElementById('hdnUserData').value.split('/');
        $('#hdnLstUsers').val('');
        for (var i = 0 ; i < scount.length; i++) {
            if (scount[i] != '') {
                selected = scount[i];
                selectedTemp = new String(selected);
                sId = selectedTemp.substring(0, selectedTemp.indexOf(','));
                sUsersTemp = selectedTemp.substring(selectedTemp.indexOf(',') + 1, selectedTemp.length);
                sUsersTemp = sUsersTemp.split(",");

                if(document.getElementById('hdnLstUsers'))
                {
                    if (document.getElementById('hdnLstUsers').value == '') {
                        document.getElementById('hdnLstUsers').value = sId + '|' + sUsersTemp[2];
                    }
                    else {
                        document.getElementById('hdnLstUsers').value = document.getElementById('hdnLstUsers').value + '~' + sId + '|' + sUsersTemp[2];
                    }
                }
            }
        }

        sUserlistname = document.getElementById('hdnParentControlname').value
        sUseridstr = document.getElementById('hdnUseridstr').value
        sUsernamestr = document.getElementById('hdnUsernamestr').value
        if (sParent == 'autodiarysetup') {
            if (sUseridstr == "CtrlAutoDiaryWizard_SendUsersIdList") {
                if (window.opener.document.getElementById('hdnDiaryLstUsers') && window.opener.document.getElementById('CtrlAutoDiaryWizard_SendUsersIdList'))
                    window.opener.document.getElementById('hdnDiaryLstUsers').value = window.opener.document.getElementById('CtrlAutoDiaryWizard_SendUsersIdList').value;
                if (window.opener.document.getElementById('hdnDiaryLstGroups') && window.opener.document.getElementById('CtrlAutoDiaryWizard_SendGroupsIdList'))
                    window.opener.document.getElementById('hdnDiaryLstGroups').value = window.opener.document.getElementById('CtrlAutoDiaryWizard_SendGroupsIdList').value
            }
            else if (sUseridstr == "CtrlAutoDiaryWizard_UsersNotifiedIdList") {
                if (window.opener.document.getElementById('hdnDiaryLstUsers') && window.opener.document.getElementById('CtrlAutoDiaryWizard_UsersNotifiedIdList'))
                    window.opener.document.getElementById('hdnDiaryLstUsers').value = window.opener.document.getElementById('CtrlAutoDiaryWizard_UsersNotifiedIdList').value;
                if (window.opener.document.getElementById('hdnDiaryLstGroups') && window.opener.document.getElementById('CtrlAutoDiaryWizard_GroupsNotifiedIdList'))
                    window.opener.document.getElementById('hdnDiaryLstGroups').value = window.opener.document.getElementById('CtrlAutoDiaryWizard_GroupsNotifiedIdList').value
            }
            else {
                if (window.opener.document.getElementById('hdnDiaryLstUsers') && window.opener.document.getElementById('CtrlAutoDiaryWizard_UsersRoutedIdList'))
                    window.opener.document.getElementById('hdnDiaryLstUsers').value = window.opener.document.getElementById('CtrlAutoDiaryWizard_UsersRoutedIdList').value;
                if (window.opener.document.getElementById('hdnDiaryLstUsers') && window.opener.document.getElementById('CtrlAutoDiaryWizard_GroupsRoutedIdList'))
                    window.opener.document.getElementById('hdnDiaryLstGroups').value = window.opener.document.getElementById('CtrlAutoDiaryWizard_GroupsRoutedIdList').value
            }
        }

        if ($('#hdnLstGroups').val() != '') {
            var varhdnGroupStr = window.opener.document.getElementById('hdnGroupStr');
            var selectedValues = $('#hdnLstGroups').val().split('~');
            $.each(selectedValues, function (index, value) {
                var valueToSelect = value.split('|')[0];
                var valueToSelectGroupName = value.split('|')[1];

                if (index === 0) {
                    if (varhdnGroupStr)
                        varhdnGroupStr.value = valueToSelect;
                }
                else {
                    if (varhdnGroupStr)
                        varhdnGroupStr.value = varhdnGroupStr.value + "," + valueToSelect;
                }
                window.opener.document.OnAddCustomListUserLookup(valueToSelect, valueToSelectGroupName, sUserlistname, sUseridstr, sUsernamestr);
            });
            if (window.opener.document.getElementById('hdnDiaryLstGroups')) {
                if (window.opener.document.getElementById('hdnDiaryLstGroups').value == '')
                    window.opener.document.getElementById('hdnDiaryLstGroups').value = varhdnGroupStr.value;
                else
                    window.opener.document.getElementById('hdnDiaryLstGroups').value = window.opener.document.getElementById('hdnDiaryLstGroups').value + ',' + varhdnGroupStr.value;
            }
            if (sParent == 'autodiarysetup') {
                if (sUseridstr == "CtrlAutoDiaryWizard_SendUsersIdList") {
                    if (window.opener.document.getElementById('CtrlAutoDiaryWizard_SendGroupsIdList') && window.opener.document.getElementById('hdnDiaryLstGroups'))
                        window.opener.document.getElementById('CtrlAutoDiaryWizard_SendGroupsIdList').value = window.opener.document.getElementById('hdnDiaryLstGroups').value;
                }
                else if (sUseridstr == "CtrlAutoDiaryWizard_UsersNotifiedIdList") {
                    if (window.opener.document.getElementById('CtrlAutoDiaryWizard_GroupsNotifiedIdList') && window.opener.document.getElementById('hdnDiaryLstGroups'))
                        window.opener.document.getElementById('CtrlAutoDiaryWizard_GroupsNotifiedIdList').value = window.opener.document.getElementById('hdnDiaryLstGroups').value;
                }
                else {
                    if (window.opener.document.getElementById('CtrlAutoDiaryWizard_GroupsRoutedIdList') && window.opener.document.getElementById('hdnDiaryLstGroups'))
                        window.opener.document.getElementById('CtrlAutoDiaryWizard_GroupsRoutedIdList').value = window.opener.document.getElementById('hdnDiaryLstGroups').value;
                }
            }
        }

        if ($('#hdnLstUsers').val() != '') {
            var varhdnUserStr = window.opener.document.getElementById('hdnUserStr');
            var selectedValues = $('#hdnLstUsers').val().split('~');
            var valueToSelectUser = "";
            var valueToSelectUsername = "";
            $.each(selectedValues, function (index, value) {
                valueToSelectUser = value.split('|')[0];
                valueToSelectUsername = value.split('|')[1];

                if (sParent == 'autodiarysetup') {
                    if (index === 0) {
                        if (varhdnUserStr)
                            varhdnUserStr.value = valueToSelectUser;
                    }
                    else {
                        if (varhdnUserStr)
                            varhdnUserStr.value = varhdnUserStr.value + "," + valueToSelectUser;
                    }
                }
                else {
                    if (index === 0) {
                        if (varhdnUserStr)
                            varhdnUserStr.value = valueToSelectUsername;
                    }
                    else {
                        if (varhdnUserStr)
                            varhdnUserStr.value = varhdnUserStr.value + "," + valueToSelectUsername;
                    }
                }
                window.opener.document.OnAddCustomListUserLookup(valueToSelectUser, valueToSelectUsername, sUserlistname, sUseridstr, sUsernamestr);
            });
            if (window.opener.document.getElementById('hdnDiaryLstUsers')) {
                if (window.opener.document.getElementById('hdnDiaryLstUsers').value == '')
                    window.opener.document.getElementById('hdnDiaryLstUsers').value = varhdnUserStr.value;
                else
                    window.opener.document.getElementById('hdnDiaryLstUsers').value = window.opener.document.getElementById('hdnDiaryLstUsers').value + ',' + varhdnUserStr.value;
            }

            if (sParent == 'autodiarysetup') {
                if (sUseridstr == "CtrlAutoDiaryWizard_SendUsersIdList") {
                    if (window.opener.document.getElementById('CtrlAutoDiaryWizard_SendUsersIdList') && window.opener.document.getElementById('hdnDiaryLstUsers'))
                        window.opener.document.getElementById('CtrlAutoDiaryWizard_SendUsersIdList').value = window.opener.document.getElementById('hdnDiaryLstUsers').value;
                }
                else if (sUseridstr == "CtrlAutoDiaryWizard_UsersNotifiedIdList") {
                    if (window.opener.document.getElementById('CtrlAutoDiaryWizard_UsersNotifiedIdList') && window.opener.document.getElementById('hdnDiaryLstUsers'))
                        window.opener.document.getElementById('CtrlAutoDiaryWizard_UsersNotifiedIdList').value = window.opener.document.getElementById('hdnDiaryLstUsers').value;
                }
                else {
                    if (window.opener.document.getElementById('CtrlAutoDiaryWizard_UsersRoutedIdList') && window.opener.document.getElementById('hdnDiaryLstUsers'))
                        window.opener.document.getElementById('CtrlAutoDiaryWizard_UsersRoutedIdList').value = window.opener.document.getElementById('hdnDiaryLstUsers').value;
                }
            }
        }
    }
    if (sParent == 'autodiarysetup') {
        if (window.opener.document.getElementById('hdnDiaryLstUsers'))
            window.opener.document.getElementById('hdnDiaryLstUsers').value = "";
        if (window.opener.document.getElementById('hdnDiaryLstGroups'))
            window.opener.document.getElementById('hdnDiaryLstGroups').value = "";
    }
    self.close();
    window.opener.m_DataChanged= true;    
    return true;

}
function OnCancel()
{
  self.close();
  return true;
}

// rsolanki2 10/23/07 : Added for Customized User List Enhancement 

function onCULPageLoaded()
{
  var sSysUser = "";
  var sTempUserName = "";  
  var sUserName = "";
  var sUserGroupsTemp = "";
  var objSysUser = null;  
  var OptUserType = null;    
  var oOption = null;
  var count = 0;
  var i = 0 ;  
  var ObjTempTable = null;
  var sParent = "";
  //Changed by Gagan for MITS 19725 : start
  var bIsLookup = false;
  var bReturn = false;
  //Changed by Gagan for MITS 19725 : end
  
    //sharishkumar Jira 6415 starts
  var sUsergroups = "";
  sUsergroups = document.getElementById('hdnUserGroups').value;
    //sharishkumar Jira 6415 ends

  //mbahl3 Strataware enhancement
  //sSysUser = document.forms[0].hdnParentControlname.value;
  //sParent = document.forms[0].hdnParentform.value;
  sSysUser = document.getElementById('hdnParentControlname').value;
  sParent = document.getElementById('hdnParentform').value;
  //Changed by Gagan for MITS 19725 : start
  //bIsLookup = document.forms[0].hdnLookup.value;
  bIsLookup = document.getElementById('hdnLookup').value;
  if ((sParent == "routediary" || sParent == "GeneralSystemParameterSetup") && bIsLookup == "true")
  {  
      if (SetParentFromLookup())  //If value matches then close window and set field in parent form
          return;                 //else display the customised user list
  }
  //Changed by Gagan for MITS 19725 : end

  if(sParent =="RM_SYS_USERS")
  {
      objSysUser = eval('window.opener.document.forms[0].'+sSysUser);
      sTempUserName = new String(objSysUser.value);
      
      if(sTempUserName != "")
      { 
        sUserName = sTempUserName.split(" ");                      
        if(sUserName[0] != undefined)
        {
            document.forms[0].firstname.value = sUserName[0];
        }
        if(sUserName[1] != undefined)
        {
            document.forms[0].lastname.value = sUserName[1];      
        }
      }
  }
      
  OptUserType=document.getElementById('lstUserGroups');    
  count = OptUserType.length;	 
  oOption = document.createElement("OPTION");
  OptUserType.options.add(oOption);
  oOption.text = ""
  oOption.value = "";
	
  for (i=count;i>0;i--)
  {	    
      OptUserType.options[i].text=OptUserType.options[i-1].text;
	  OptUserType.options[i].value=OptUserType.options[i-1].value;
	  OptUserType.options[i].selected=OptUserType.options[i-1].selected;
  }
  
  OptUserType.options[0].text="----- ALL -----";
  OptUserType.options[1].text="----- NULL -----";
  OptUserType.options.selectedIndex= 0;        
  
    //sharishkumar Jira 6415 starts
  if (sParent.indexOf('_usrgrpbtn') != -1)
  {
      if (sUsergroups == "0") {
          //for (i = 1; i <= count; i++) {              
          //    OptUserType.options[i].disabled = true;
          //}
          OptUserType.options.selectedIndex = 0;
      }
          //sharishkumar jira 8618 starts
      else
      {
          if ($('#hdnMultiSelect') != null && $('#hdnMultiSelect').val() == 0) {
              $('#lstUserGroups > option[value=0]').prop('disabled', 'disabled');
              $('#lstUserGroups > option[value!=0]:first').prop('selected', true);
          }
          else {
              //sharishkumar jira 8618 starts
              //OptUserType.options[0].disabled = true;//sachin              
              //OptUserType.options[1].disabled = true;
              $('#lstUserGroups > option[value=0]:last()').prop('disabled', 'disabled');
              //sharishkumar jira 8618 ends
          }
      }
      //sharishkumar jira 8618 ends
  }
    //sharishkumar Jira 6415 ends

  setTimeout('filter2()',0);       
  setInterval('filter2()',2000);   
    // ObjTempTable = new ScrollableTable(document.getElementById('CULListMainTable'), 100)//   Mits 35697   commeneted -Binding it with the GridView  
  //alert(document.getElementById('recOf').innerHTML);
 
}

/* rsolanki2 10/23/07 : Added for Customized User List Enhancement 
This fucntion iterates thru the contents of the table specified in the variable '_ID' and filters the rows 
depending on the values of the variables ObjFirstName, ObjLastName and ObjUserGroups
*/
function filter2()
{	
    var commaLoc ;
	var searchItem= " " ;	
	var i=0 ;
	var count = 0;
	var _id = 'gvUserList'; // Mits 35697 
	var displayRecordCount;
	var totalRecordCount;
	var objTable = null;
	var ele = "";
	var ele2 = "";
	var l1 = "";		
	var l2 = "";
    var ObjFirstName= null;
    var ObjLastName= null;
    var ObjUserGroups= null;
	var sFirstNameTemp = "";      
	var sLastNameTemp = "";       
	var sUserGroupsTemp = "";    		
	var cellNr = 2 ; 
    var cellNr2 = 3 ; 
    
    //sharishkumar Jira 8618 - Additional check - strt
    if ($('#lstUserGroups > option:selected').length <= 0) {
        return false;
    }
    //sharishkumar Jira 8618 - end
    ObjFirstName = document.getElementById('firstname'); 
    ObjLastName = document.getElementById('lastname');
    ObjUserGroups = document.getElementById('lstUserGroups');  
    sFirstNameTemp =ObjFirstName.value.toLowerCase();   
	sLastNameTemp = ObjLastName.value.toLowerCase(); 
    
	// getting selected User types	
	count = ObjUserGroups.length-1;	
	if (ObjUserGroups.options.selectedIndex== 0)
	{
	    searchItem ="true";
	}	
	else
	{
	    for (i=0;i<=count;i++)
	    {
	        sUserGroupsTemp = ObjUserGroups.options[i].text.toLowerCase();// UserType column
	        if (sUserGroupsTemp=="----- null -----") sUserGroupsTemp="";	        
	        if (ObjUserGroups.options[i].selected )
	            {
	                searchItem = searchItem + 'escape(ele2.toLowerCase())==escape("' + sUserGroupsTemp + '")  || ';
	            }
	    }
	    searchItem =searchItem + 'false';
	}
	
	//rsolanki: similarty check being done here.
	//basically we want to skip the table filter if no changes are made in 1) FirstName 2)LastName 3) Groups selected
	
	if (m_bFirstTime && m_sFirstName == sFirstNameTemp && m_sLastName == sLastNameTemp && searchItem == m_sGroupName )
	    {return;}
	m_bFirstTime = true;
	m_sFirstName = sFirstNameTemp;
	m_sLastName = sLastNameTemp;
	m_sGroupName = searchItem  ;	
		
	objTable = document.getElementById(_id);
		
	totalRecordCount=objTable.rows.length;
	displayRecordCount = 0;// Changed for MITS 38145, aaggarwal29 : By default count is displayed to one even if no record is present
	var iTableLength = objTable.rows.length;
	for (var r = 1; r <= iTableLength-1 ; r++) // Changed for MITS 38145, aaggarwal29 : Last record was not getting checked.
	{
		ele = trim(objTable.rows[r].cells[cellNr].innerHTML.replace(/<[^>]+>/g,""));		// Name
		ele2 = trim(objTable.rows[r].cells[cellNr2].innerHTML.replace(/<[^>]+>/g, ""));   // User type
			    
	    commaLoc = ele.indexOf(',',0);
	    //Merged by Nitin for R6 in order to correct the filteration of First Name and Last Name in CustomizeUserList screen starts
            //MITS 13835   
            //l1=ele.toLowerCase().indexOf(sFirstNameTemp);
	    //l2=ele.toLowerCase().indexOf(sLastNameTemp,commaLoc);	  
	    if(sLastNameTemp!="")
	        l1 = ele.toLowerCase().indexOf(sLastNameTemp);
	    if(sFirstNameTemp!="")
            l2 = ele.toLowerCase().indexOf(sFirstNameTemp, commaLoc);
        //Merged by Nitin for R6 in order to correct the filteration of First Name and Last Name in CustomizeUserList screen ends	    
	    
	    if ((l1>=0 && l2>=0 && l1<=commaLoc && (l2 >=commaLoc || l2==0 )) || (l2==0 && l1==0))
		{			
			  if (eval(searchItem))  // Checking for User Type
			  {   
			    objTable.rows[r].style.display = '';
			    displayRecordCount=displayRecordCount+1;
	          }
			  else objTable.rows[r].style.display = 'none';
		}
		else objTable.rows[r].style.display = 'none';		
	}
		document.getElementById('recOf').innerHTML=displayRecordCount;
		document.getElementById('recTo').innerHTML=totalRecordCount-1;		
}

// rsolanki2 10/23/07 : Added for Customized User List Enhancement 
//client based function to remove the filters & display all items 
function ListAll()
{
    var i = 0;
    var count = 0;
    var _id = 'gvUserList'; // Mits 35697 
    var ObjFirstName = null;
    var ObjLastName = null;
    var ObjUserGroups = null;
    var objTable= null
    
    ObjFirstName = document.getElementById('firstname'); 
    ObjLastName = document.getElementById('lastname');    
    ObjFirstName.value = "";
    ObjLastName.value = "";	
    
	ObjUserGroups=document.getElementById('lstUserGroups');
	ObjUserGroups.selectedindex=1;
	
	count =ObjUserGroups.length-1;
	
	for (i=0;i<=count;i++)
	{
	    ObjUserGroups.options[i].selected =false;
	}
	
	ObjUserGroups.options[0].selected =true;
	
	var objTable = document.getElementById(_id);
	
    for (var r = 1; r < objTable.rows.length; r++)
	{
	    objTable.rows[r].style.display = '';
	}
}

// rsolanki2 10/23/07 : Added for Customized User List Enhancement 
// client based function to select all checkboxes in all the rows of the table 
function SelectAll() {
    var _id = 'gvUserList';// Mits 35697 

    var sParent = "";
    var sdata = '';
    var objTable = document.getElementById(_id);
    sParent = document.forms[0].hdnParentform.value;
    // Mits 35697 starts
    sdata = document.getElementById('hdnUserData').value;
    var sdiff = "";
    sdiff = document.forms[0].elements.length - objTable.rows.length - 1
    // Mits 35697 ends
    for (var i = 0; i < document.forms[0].elements.length; i++) {
        // akaushik Changed for MITS 31905 Starts
        // if (document.forms[0].elements[i].type == 'checkbox')
        if (document.forms[0].elements[i].type == 'checkbox' && document.forms[0].elements[i].parentNode.parentNode.style.display != 'none')
            // akaushik Changed for MITS 31905 Ends
        {
            document.forms[0].elements[i].checked = true;
            // Mits 35697 starts
            sdata = '/' + document.forms[0].elements[i].value + ',' + objTable.rows[i - sdiff + 1].cells[2].innerText + ',' + objTable.rows[i - sdiff + 1].cells[1].innerText + ',' + objTable.rows[i - sdiff + 1].cells[4].innerText;
            if (sdata != '')
                document.getElementById('hdnUserData').value = document.getElementById('hdnUserData').value + sdata;
            // Mits 35697 ends
        }
    }
}

// rsolanki2 10/23/07 : Added for Customized User List Enhancement 
function InvertSelection()
{
    var _id = 'gvUserList';// Mits 35697 
  
    var sParent = "";
    var sdata = '';
    var objTable = document.getElementById(_id);
    sParent = document.forms[0].hdnParentform.value;
    // Mits 35697 starts
    document.getElementById('hdnUserData').value = '';
    var sdiff = "";
    sdiff = document.forms[0].elements.length - objTable.rows.length - 1
    // Mits 35697 ends

    sParent = document.forms[0].hdnParentform.value;
    for (var i = 0; i < document.forms[0].elements.length; i++) {
        // Mits 35697 starts
        if (document.forms[0].elements[i].type == 'checkbox' && document.forms[0].elements[i].parentNode.parentNode.style.display != 'none') {
            document.forms[0].elements[i].checked = !(document.forms[0].elements[i].checked);
            if (document.forms[0].elements[i].checked == true) {
                sdata = '/' + document.forms[0].elements[i].value + ',' + objTable.rows[i - sdiff + 1].cells[2].innerText + ',' + objTable.rows[i - sdiff + 1].cells[1].innerText + ',' + objTable.rows[i - sdiff + 1].cells[4].innerText;
                if (sdata != '')
                    document.getElementById('hdnUserData').value = document.getElementById('hdnUserData').value + sdata;
            }
        }
        // Mits 35697 ends

    }
   
}

// rsolanki2 10/23/07 : Added for Customized User List Enhancement 
function funSelectNone()
{
    var sParent = "";
    
    sParent = document.forms[0].hdnParentform.value;    
    for(var i = 0; i  <  document.forms[0].elements.length;i++)
    {  
        if( document.forms[0].elements[i].type=='checkbox')
        {   
           document.forms[0].elements[i].checked= false;                    
        }
    } 
}

//rsolanki2: resizing grid on browser resize
function ResizeGrid()
{    
   // getting browser width & height
    var x,y;
            
    if (self.innerHeight) // all except Explorer
    {
	    x = self.innerWidth;
	    y = self.innerHeight;
    }
    else if (document.documentElement && document.documentElement.clientHeight)
	    // Explorer 6 Strict Mode
    {
	    x = document.documentElement.clientWidth;
	    y = document.documentElement.clientHeight;
    }
    else if (document.body) // other Explorers
    {
	    x = document.body.clientWidth;
	    y = document.body.clientHeight;
    }    
   try
    {  
       //var objGrid = document.getElementById('CULListMainTable');                        
       var objGrid = document.getElementById('frmData').childNodes[4];                        
       var newHeight = y -  300;
       var newWidth = x -  30;
       //ObjTempTable = new ScrollableTable(objGrid, newWidth );       
       objGrid.style.width = newWidth ;
       objGrid.childNodes[0].style.width = newWidth-10 ;
       objGrid.style.height = newHeight ;
    }
	catch (e)
	{	}
   
}



//Changed by Gagan for MITS 19725 : start
function SetParentFromLookup() 
{
    var selectedTemp;
    var selected;
    var sUsersTemp = "";
    var sSysUserField = "";
    var sUserLoginName = "";
    //var sUserlistname = "";    
    //var sUsernamestr = "";
    var objElem = null;
    var objElemParent = null;
    var sParentControlname = "";
       //mbahl3 Strataware enhancement      
    var sParent = document.forms[0].hdnParentform.value;
    
    if (window.opener != null && window.opener != self &&  (sParent == 'GeneralSystemParameterSetup')) {
        for (var i = 0; i < document.forms[0].elements.length; i++) {
            if (document.forms[0].elements[i].type == 'radio' || document.forms[0].elements[i].type == 'checkbox') {
                selected = document.forms[0].elements[i].value;
                selectedTemp = new String(selected);
                sUsersTemp = selectedTemp.substring(selectedTemp.indexOf(',') + 1, selectedTemp.length);
                sUsersTemp = sUsersTemp.split(",");
                sUserLoginName = sUsersTemp[0] + " " + sUsersTemp[1] + " " + sUsersTemp[2];

                //sParentControlname = document.forms[0].hdnUserlistname.value;
                objElemParent = eval('window.opener.document.forms[0].txtUser');

                //mbahl3 Strataware enhancement      

                if (objElemParent != null) {
                    //If Match found
                    if (objElemParent.value == sUsersTemp[2]) {
                        //Setting last name + " " + first name
                        objElemParent.value = sUsersTemp[1] + " " + sUsersTemp[0];
                        //mbahl3 Strataware enhancement      
                       // sParentControlname = document.forms[0].hdnUsernamestr.value;
                        objElem = eval('window.opener.document.forms[0].UserName');

                        //Setting login name
                        if (objElem != null) {
                            //objCtrl=eval('window.opener.document.forms[0].'+sParentControlname);
                            objElem.value = sUsersTemp[2];
                        }

                       // sParentControlname = document.forms[0].hdnUseridstr.value;
                        objElem = eval('window.opener.document.forms[0].StratawareUserID');

                        //Setting login name
                        if (objElem != null) {
                            //objCtrl=eval('window.opener.document.forms[0].'+sParentControlname);
                            objElem.value = selectedTemp.substring(0,selectedTemp.indexOf(','));
                        }
                        self.close();
                        return true;


                    }
                }

            }


        }

        //If no match found then blank out the "Route to" textbox     
        //mbahl3 Strataware enhancement          
        sParentControlname = document.forms[0].hdnUsernamestr.value;
        objElemParent = eval('window.opener.document.forms[0].' + sParentControlname);
        if (objElemParent != null) {
            objElemParent.value = "";
        }

        return false;

    }
    if (window.opener != null && window.opener != self && sParent == 'routediary') 
    {
    for(var i = 0; i < document.forms[0].elements.length;i++)
    {  
        if(document.forms[0].elements[i].type == 'radio' || document.forms[0].elements[i].type == 'checkbox')
        {            
           selected = document.forms[0].elements[i].value;          
           selectedTemp  = new String(selected);                                        
           sUsersTemp = selectedTemp.substring(selectedTemp.indexOf(',')+1,selectedTemp.length);
           sUsersTemp = sUsersTemp.split(",");                 
           sUserLoginName = sUsersTemp[0] + " " + sUsersTemp[1] + " " + sUsersTemp[2] ;  
           
           sParentControlname = document.forms[0].hdnUsernamestr.value;
           objElemParent = eval('window.opener.document.forms[0].' + sParentControlname);



                   if (objElemParent != null) 
                   {
                     //If Match found
                       if (objElemParent.value == sUsersTemp[2]) 
                       {                           
                           //Setting last name + " " + first name
                           objElemParent.value = sUsersTemp[0] + " " + sUsersTemp[1];                          

                           sParentControlname = document.forms[0].hdnParentControlname.value;
                           objElem = eval('window.opener.document.forms[0].' + sParentControlname);

                           //Setting login name
                           if (objElem != null) {
                               //objCtrl=eval('window.opener.document.forms[0].'+sParentControlname);
                               objElem.value = sUsersTemp[2];
                           }
                           self.close();
                           return true;                        
                           
                           
                       }
                   }                 
        
               }


           }

           //If no match found then blank out the "Route to" textbox         
           sParentControlname = document.forms[0].hdnUsernamestr.value;
           objElemParent = eval('window.opener.document.forms[0].' + sParentControlname);
           if (objElemParent != null) 
           {
               objElemParent.value = "";
           }

           return false;

    }
}
//Changed by Gagan for MITS 19725 : end
//Aman when a user from the customized user list is selected, and then deleted(text box made empty), onchange event will be called to fill the user id as 0  --Start
function CheckUser(sUserName, sUserId) {  
    var objCtrlName = null;
    var objCtrlId = null;
    objCtrlName = document.getElementById(sUserName);
    objCtrlId = document.getElementById(sUserId);
    if (objCtrlName != null && objCtrlId != null) {
        var sName = objCtrlName.value;
        if (trim(sName) == '') {
            objCtrlId.value = 0;
           setDataChanged(true);      
        }
    }
}

//Aman ---     End

//Add by kuladeep for MITS:35142 Start
function OnPostbackLoadUserData() {
    var objCtrl = null;
    var objOption = null;
    if (document.getElementById('UserIdStr') != null && document.getElementById('UserStr') != null) {
        if (document.getElementById('UserIdStr').value != "" && document.getElementById('UserStr').value != "") {
            var arrUserId = document.getElementById('UserIdStr').value.split(' ');
            var arrUserName = document.getElementById('UserStr').value.split(' ');
            if (arrUserId != null && arrUserName != null) {
                objCtrl = document.getElementById('lstUsers');
                for (var i = 0; i < arrUserId.length; i++) {
                    if (arrUserId[i] != "" && arrUserName[i] != "") {
                        if (document.forms[0].DefaultAssignedTo != null) {
                            if (document.forms[0].DefaultAssignedTo.value == "True") {
                                //Check for Login User--To remove that if already there.
                                if (document.forms[0].LoginUserName != null) {
                                    if (document.forms[0].LoginUserName.value != arrUserName[i]) {
                                        objOption = new Option(arrUserName[i], arrUserId[i], false, false);
                                        objCtrl.options[objCtrl.length] = objOption;
                                    }
                                }
                            }
                            else {
                                objOption = new Option(arrUserName[i], arrUserId[i], false, false);
                                objCtrl.options[objCtrl.length] = objOption;
                            }
                        }
                    }
                }
            }
        }
    }
}
//Add by kuladeep for MITS:35142 End