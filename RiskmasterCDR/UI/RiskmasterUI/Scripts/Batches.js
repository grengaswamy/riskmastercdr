function OnRefresh()
{      
    var dt = self.document.getElementById('DateTo').value;
    var dToDate = new Date(dt.substring(0,2), dt.substring(3,5),dt.substring(6,10) );              
    dt = self.document.getElementById('DateFrom').value;
    var dFromDate = new Date(dt.substring(0,2), dt.substring(3,5),dt.substring(6,10) );  
    
    if(dFromDate !="" && dToDate !="" && dToDate < dFromDate ) 
    {
       alert('To Date cannot be less than From date'); 
       return false;
    }
   
    //window.document.forms[0].action.value = "" ;    
    window.document.forms[0].action.value = "Refresh" ;    
	window.document.forms[0].submit();        
}



function SelectAfterPostBack() {
    // Get Control Name
    //var sControlName = listname.substring(0, listname.length - 4) + 'SelectedId';
    //var radioSelect = document.getElementById(sControlName);

    //if (radioSelect != null) {

        var gridElementsRadio = document.getElementsByName('selectrdo');
        var sSelectedId = document.forms[0].selectedrow.value;

        if (gridElementsRadio != null) 
        {
            for (var i = 0; i < gridElementsRadio.length; i++) 
            {                
                var selectedValue = gridElementsRadio[i].value;
                if ( selectedValue == sSelectedId) 
                {
                    gridElementsRadio[i].checked = true;
                    break;
                }
            }
        }
    
}



function OnSelect() {
   // document.forms[0].selectedrow.value = this.value;
    //window.document.forms[0].action.value = "" ;    
    window.document.forms[0].action.value = "Select" ;
    document.forms[0].submit();
    //self.setTimeout('document.forms[0].submit();',300);
    //var wnd=RMX.window.open('csc-Theme/riskmaster/common/html/progress.html','progressWnd','width=400,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-400)/2);
    //self.parent.wndProgress=wnd;

    //pleaseWait.show();
}


function CloseProgressWindow()
{
	if(self.parent!=null)
	{
		if(haveProperty(self.parent,"wndProgress") && self.parent.wndProgress!=null)
		{
			self.parent.wndProgress.close();
			self.parent.wndProgress=null;
		}
	}
}



function OnClose()
{
    var CheckedFound = false;
    var ctrl;
    var iBatchId;
    var iBatchTotal;
    var iBillingItems;
    


    inputs = document.getElementsByName('selectrdo');

    if (inputs != null) {
        for (i = 0; i < inputs.length; i++) {
            if (inputs[i].checked == true) {
                CheckedFound = true;
                break;
            }
        }
    }
            
	if(CheckedFound)
	{    
	    //positionID =  GetPositionForSelectedGridRow("Batches", inputs[i].value);         
     
        //Status
        //ctrl = self.document.getElementById("Batches"+"|"+ positionID +"|"+"Status"+"|TD|");

	    //if(ctrl.innerText == 'Closed')
	    if (document.forms[0].Status.value == 'Closed')
	    {
	        alert("The batch has already been closed.");   
	        return false;
	    }

	    //if(ctrl.innerText == 'Reversed')
	    if (document.forms[0].Status.value == 'Reversed')	    
	    {
	        alert("Batch has been reversed.");
	        return false;
	    }	   		    
	    
	    //ctrl = self.document.getElementById("Batches"+"|"+ positionID +"|"+"Type"+"|TD|");

	    if (document.forms[0].Type.value == 'Installment')	    
	    //if(ctrl.innerText == 'Installment')
	    {
	    	alert('Cannot manually close an installment batch.');
	    	return false;
	    }	          
	    
	    //ctrl = self.document.getElementById("Batches"+"|"+ positionID +"|"+"BatchCount"+"|TD|");
	    //iBillingItems = ctrl.innerText;
	    //iBillingItems = ctrl.innerText;
	    iBillingItems = document.forms[0].BatchCount.value;
	    
        if(iBillingItems <= 1)
        {
            alert("Cannot close batch with just one billing item");
            return false;
        }	            
       
        
	    //BatchRowID
        //ctrl = self.document.getElementById("Batches"+"|"+ positionID +"|"+"BatchRowId"+"|TD|");

        //iBatchId = ctrl.innerText;
        iBatchId = document.forms[0].selectedrow.value;   	        	                         
        
        iBatchTotal = window.document.forms[0].iBatchTotal.value;


        window.open("/RiskmasterUI/UI/Billing/BatchReconciliationDetails.aspx?iBatchTotal=" + 
                        iBatchTotal + "&iBatchId=" + iBatchId
                        ,'ReconciliationDetails',
                        'width=400,height=200'+',top='
                        + (screen.availHeight - 400) / 2 + ',left=' + (screen.availWidth - 400) / 2 + ',resizable=no,scrollbars=no');
        return false;
	 }          
     else
     {
         alert("Please select a batch to close.");
         return false;    
     }           
}


function OnReconcile() {    
    var ans = false;
    
    var iBatchTotal = window.opener.document.forms[0].iBatchTotal.value;
    var sReconciliationTypeCode = self.document.getElementById('ReconciliationType_codelookup').value;
    
    sReconciliationTypeCode = sReconciliationTypeCode.substring(0, 1 );

    if (document.forms[0].BatchId.value == "" || document.forms[0].BatchId.value == 0)
        return false;        
    
    
    if(sReconciliationTypeCode == 'E'  && iBatchTotal != '0')
    {         
        alert("Cannot reconcile Exact; batch total is not zero. Please select another reconciliation type.");
        return false;
    }
    
    if(sReconciliationTypeCode != 'E' && iBatchTotal == '0')     
    {
        alert("Regardless of selection, batch will reconcile Exact because the batch total is zero.");
        return false;
    }        
            
    if(document.forms[0].CheckPaidInFull.value == 'True')
    {
        ans = window.confirm("The customer has paid their premium in full. Do you wish to cancel the pay plan? ");       
    }

    if (document.forms[0].ReconciliationType_codelookup_cid.value != ""
         && document.forms[0].ReconciliationType_codelookup_cid.value != '0')
       {      
           window.opener.document.forms[0].ReconcileType.value
            = document.forms[0].ReconciliationType_codelookup_cid.value;	              
              
           window.opener.document.forms[0].bCancelPayPlan.value = ans;
          
           //window.opener.document.forms[0].action.value = "" ;    
           window.opener.document.forms[0].action.value = "Close" ;   	           
                    
	       window.opener.document.forms[0].submit();
	       window.close();
        }
        else     
            alert("Please choose a Reconciliation Type.");  
}


function OnCancel()
{
    window.close();
}


function OnReverse()
{
    var CheckedFound = false;
    var ctrl;


    inputs = document.getElementsByName('selectrdo');

    if (inputs != null) {
        for (i = 0; i < inputs.length; i++) {
            if (inputs[i].checked == true) {
                CheckedFound = true;
                break;
            }
        }
    }

    //inputs = document.all.tags("input");
//	for (i = 0; i < inputs.length; i++)
//	{
//		if ((inputs[i].type=="radio") && (inputs[i].checked==true))
//		{
//			inputname=inputs[i].id.substring(0,inputs[i].id.indexOf("|"));
//			if ("Batches" == inputname)	
//			{
//				CheckedFound = true;				
//				break;
//			}
//		}
//	}
//	
	if(CheckedFound)
	{    
	    //positionID =  GetPositionForSelectedGridRow("Batches", inputs[i].value);         
     
        //Status
        //ctrl = self.document.getElementById("Batches"+"|"+ positionID +"|"+"Status"+"|TD|");

	    if (document.forms[0].Status.value == 'Open')
	    {
	        alert("Cannot reverse an open batch.");
	        return false;
	    }

	    if (document.forms[0].Status.value == 'Reversed')       
	    {
	        alert("Batch has already been reversed.");
	        return false;
	    }
	    	    
	    //ctrl = self.document.getElementById("Batches"+"|"+ positionID +"|"+"Type"+"|TD|");

	    if (document.forms[0].Type.value == 'Installment')
	    {
	    	alert('Cannot manually reverse an installment batch.');
	    	return false;
	    }	    
	   
	    if(document.forms[0].bPrintDisb.value == 'True')
	    {	    
	    	alert('There is a printed disbursement in the batch. Batch cannot be reversed.');
	    	return false;
	    }
	    else
	    {  	        
	        ans=window.confirm("Are you sure you wish to reverse the batch ?");
	        if (ans == true) {
	            //window.document.forms[0].action.value = "" ;    
	            window.document.forms[0].action.value = "Reverse";
	            document.forms[0].submit();
	            //self.setTimeout('document.forms[0].submit();',300);
	            //var wnd=RMX.window.open('csc-Theme/riskmaster/common/html/progress.html','progressWnd',
	            //'width=400,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-400)/2);		
	            //self.parent.wndProgress=wnd;
	        }
	        else
	            return false;	        
	    }  
     }          
     else
     {
         alert("Please select a batch to reverse");
         return false;
     }        
}



function OnAbort()
{
    var CheckedFound = false;
    var ctrl;
//    inputs = document.all.tags("input");

//	for (i = 0; i < inputs.length; i++)
//	{
//		if ((inputs[i].type=="radio") && (inputs[i].checked==true))
//		{
//			inputname=inputs[i].id.substring(0,inputs[i].id.indexOf("|"));
//			if ("Batches" == inputname)	
//			{
//				CheckedFound = true;				
//				break;
//			}
//		}
//}


        inputs = document.getElementsByName('selectrdo');

        if (inputs != null) {
            for (i = 0; i < inputs.length; i++) {
                if (inputs[i].checked == true) {
                    CheckedFound = true;
                    break;
                }
            }
        }

	
	
	
	if(CheckedFound)
	{    
	    //positionID =  GetPositionForSelectedGridRow("Batches", inputs[i].value);         
     
        //Status
        //ctrl = self.document.getElementById("Batches"+"|"+ positionID +"|"+"Status"+"|TD|");

	    if (document.forms[0].Status.value == 'Closed')
	    {
	        alert("Cannot abort a closed batch.");
	        return false;
	    }

	    if (document.forms[0].Status.value == 'Reversed')       
	    {
	        alert("Cannot abort a reversed batch.");
	        return false;
	    }	   	    
	
        ans=window.confirm("Are you sure you wish to abort the batch?");
        if (ans == true) {
            //window.document.forms[0].action.value = "" ;
            window.document.forms[0].action.value = "Abort";
            // The Page posts twice. So for the second time it does not find the Batch. So commenting the code for double postback
            //window.document.forms[0].submit();

            //self.setTimeout('document.forms[0].submit();',300);
            //var wnd=RMX.window.open('csc-Theme/riskmaster/common/html/progress.html','progressWnd',
            //'width=400,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-400)/2);		
            //self.parent.wndProgress=wnd;
        }
        else {
            return false;
        }
     }          
     else
     {
         alert("Please select a batch to abort.");
         return false; 
     }           
}