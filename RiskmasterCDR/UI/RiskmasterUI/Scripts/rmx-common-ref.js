/*
* Allows child screens to consistently access a "Singleton" MDIParent 
*
* This allows a centralized way to co-ordinate what windows are open etc.
*/
/// Reference the single global MDIParent
var wnd = window;
var PopupParent;
while((wnd.parent!=null && wnd.parent !=wnd) && !wnd.MDIParent)
wnd = wnd.parent;
//Could be here from nav-tree or a pop-up - not embedded in window widget.
if(!wnd.MDIParent)
{
	//Test for here from nav-tree.
	if(window.parent.frames["content"])
	{
		waitFor('window.parent.frames["content"].MDIParent','window.MDIParent = window.parent.frames["content"].MDIParent; window.RMXUID = MDIParent.generateID("RMXUID");');
	}
	else if (window.opener)
	{
		wnd=window.opener;
	}
}
var MDIParent = wnd.MDIParent;
//BSB 08.15.2006 Optional Debugging Aid
// waitFor will give up on this after 5 seconds if not available.
//waitFor('wnd.Trace','window.Trace = wnd.Trace;');


if (typeof RMX == 'undefined') {
  /**
   * Namespace definition.
   */
  RMX = {};
}

//BSB Generate a UID for handling associating clean-up tasks with a specific window
if(MDIParent)
	window.RMXUID = MDIParent.generateID("RMXUID");



RMX.unAmp= function(s){return s.replace(/&amp;/g,"&");}

// BSB 08.15.2006 Added "timeOutCounter" in order to allow this method to "drain" in case the 
// condition never results in "true" and allows this looping to end.  This seems especially prone to 
// happen if the window which registers the setTimeout is a parent window of the requesting child and 
// the child is subsequently unloaded before the eval becomes true.
// Defaults to 5 seconds although a caller may specify a timeout as 
// any number of 10ms cycles to wait. This addresses *some* of the cpu consumption issue
// with our mdi app code.
function waitFor(cond,block,timeOutCounter)
{
	if(timeOutCounter==null)
		timeOutCounter = 500; //Example: 500 retry iterations * 10 ms = 5,000ms (5 seconds)
	
	if(timeOutCounter==0) //TimeOut - time to give up.
	{
		//alert("Time Ran out Trying to eval block: " +  block + "\n\n\On window: " + window.RMXUID);
		return;
	}
	
	//BSB eval does not like crlf's.
	if(block.indexOf('\n')!=-1)
		block = block.replace(/\n/g,'');

	//alert("waiting now");
	if(eval(cond))
		return eval(block);
	else
	{
		var firstDoubleQuotePos = block.indexOf('"');
		if(firstDoubleQuotePos<=0)
			firstDoubleQuotePos = block.length;
			
		if(block.indexOf("'")!=-1 && block.indexOf("'")<firstDoubleQuotePos)
		{	alert("Javascript waitFor block parsing problem. Must use double quotes on string values in the block parameter.");
		}else 
			self.setTimeout("waitFor('" +cond+"','"+block+"',"+(timeOutCounter-1)+");",10);
	}
}

// BSB Pick up the Zapatec Window Widget in which we were loaded.
// Note relies on the "RMXHandle" property having been set when
// this window was created (From rmx-mdiparent.js "MDIParent.CreateMDIChild" routine.)
RMX.getZPWidgetRef = function ()
{
	var elt = window.frameElement
	if(elt==null)
		return null;
	do
	{
		if(elt.RMXHandle!=null)
			break;
		//Increment
		elt = elt.parentElement;
	}while(elt!=null)
	
	if(elt==null) return null;
	
	
	return waitFor("(MDIParent && MDIParent.MDIChildList)",'MDIParent.MDIChildList.GetChildByHandle("' + elt.RMXHandle+'")');
	//return MDIParent.MDIChildList.GetChildByHandle(elt.RMXHandle);

}

RMX.getCurrentPageTitle = function()
{
	divs = document.getElementsByTagName("DIV");
	for(var i=0;i<divs.length;i++)
	{
		div = divs[i];
		if(div.className=="msgheader") //Just take the first match.
		{
			div.style.display="none";
			br = document.createElement('br');
			div.parentElement.insertBefore(br,div.nextSibling);
			return div.innerText;
		}
	}
	return "";
}

RMX.ApplyTitle = function()
{
	var wnd = RMX.getZPWidgetRef();
	//alert(wnd.state.content);
	if(wnd!=null)
	{	
		var strTmp = RMX.getCurrentPageTitle();
		//wnd.top.title = strTmp;
		wnd.setTitle(strTmp);
		wnd.maximize();
	}
}

RMX._addWndParam = function(sUrl)
{
	var sSuffix="";
	
	if(sUrl.indexOf("#")!=-1)
	{
		sSuffix = sUrl.substr(sUrl.lastIndexOf("#"));
		sUrl = sUrl.substr(0,sUrl.lastIndexOf("#"));
	}
		
	if(sUrl.indexOf("wnd=")==-1 && (MDIParent || (document.forms[0] && document.forms[0].SysWindowId) || (window.location.search.indexOf("wnd=")!=-1)))
	{	
		if(sUrl.indexOf("?")==-1)
			sUrl = sUrl + "?";
		else
			sUrl = sUrl + "&amp;";
		
		if(MDIParent)	
			sUrl = sUrl + "wnd=" + escape(MDIParent.GetActiveWindowHandle()) + sSuffix;
		else if(document.forms[0] &&  document.forms[0].SysWindowId)
			sUrl = sUrl + "wnd=" + escape(document.forms[0].SysWindowId.value)+ sSuffix;
		else if(window.location.search.indexOf("wnd=")!=-1)
		{
			var s = window.location.search;
			var iPos = s.indexOf("wnd=");
			var iEndPos = s.indexOf("&");
			if(iEndPos==-1)iEndPos = (s.length-iPos);
			s = s.substr(iPos,iEndPos);
			sUrl = sUrl + s + sSuffix
		}
	}	
	return sUrl;
}
RMX.urlNavigate = function(sUrl)
{

	window.location.href = RMX._addWndParam(sUrl);
	return true;
}
RMX.window = {};
RMX.window.open = function(sUrl, sName, sFeatures, bReplace)
{
	
    try
    	{
    		//alert("Try.. " + sUrl + "," + sName);

        	var wndPopup = window.open(RMX._addWndParam(sUrl),sName,sFeatures,bReplace);
		RMX.AddToOwnerPopupList(sName,wndPopup);

    		//Parijat: Red "X" issue---adding a prompt event in case Popup close.
        	//Right now commented since the generic event (in rmx-common-ref.js)for prompt in case 
        	//of pops was having problem in case the parent was navigated through 'move next' button.
		//RMX.addEvent(wndPopup, "beforeunload", RMX.window.ConfirmPopupUnload);

		return wndPopup;
        }
	catch(ex)
	{
		//alert('Catch');
		return;
	}
}

//Parijat: Generic event being used for showing prompt in case of popups.
RMX.window.ConfirmPopupUnload = function(evt)
{
    PopupParent=MDIParent.GetActiveMDIChild();
    var wndPop =null;
  	wndPop=PopupParent.RMXWindowState.GetActivePopup();
  	//sometimes previous GetActivePopup() statement returns null so we again ask for 
  	//an 'OPEN' window from GetActivePopup() function in the next statement
	if (wndPop==null)
	    wndPop=PopupParent.RMXWindowState.GetActivePopup();
	// Check in order to avoid Prompt from appearing in search popups like 'Authority Contacted'.
	if (wndPop!=null)
    {
        if (wndPop.document!=null)
        {
           //Parijat: Developer can switch to any of the code depending,
           // if he wants to filter on the basis of popups where prompt 'is not required'
           // or popups where it 'is required'.
           
           /* if (MDIParent._NoCloseMessagePopUps.indexOf("|" + wndPop.document.title + "|")>-1)
            {
               return;
            }
           */
            if (MDIParent._ShowCloseMessagePopUps.indexOf("|" + wndPop.document.title + "|")==-1)
            {
               return;
            }
            //To avoid prompt message incase the Editor gets closed through the 'Save' button.   
            if (wndPop.document.title=='Riskmaster Editor')
            {
                if (wndPop.document.forms[0]!=null)
                    if (wndPop.document.forms[0].action!=null)
                 if (wndPop.document.forms[0].action.value=="Save")
                    return;
            }
        }
        var sMsg = "Please confirm that you have completed work within this pop-up.";
        if(evt!=null)
        evt.returnValue=sMsg;
        else if(window.event!=null)
        window.event.returnValue=sMsg;  
    }
}
RMX.AddToOwnerPopupList = function(sName,wndPopup)
{
	// If this is not considered a "modal popup" by being named in the pipe delimited list
	// then we should not affect the PopupCount...
	if (sName && MDIParent && MDIParent._NonModalPopUps.indexOf("|" + sName + "|")>-1)
		return;	

	var wnd=MDIParent.GetActiveMDIChild();
	if(wnd)
		wnd.RMXWindowState.PopupList.Add(wndPopup);
}

//Cross-Window Registration causes permission problems on Refresh.
// Target el could be from a page that was navigated seperately.
RMX._removeOnUnload = [];
RMX.addEvent = function(el, evname, func, useCapture) {
	var wnd= el.window;
	if(!wnd)
		wnd=el.ownerDocument.parentWindow;

	//if(wnd!=window)
	//if(evname=="beforeunload")
		//debugger;
	//	alert("cross window registration " + wnd.location.href + "\n\n" + window.location.href  + "\n\n" + wnd.RMXUID);
	if (el.addEventListener) { // Gecko / W3C
	  if (!useCapture) {
	    useCapture = false;
	  }
		el.addEventListener(evname, func, useCapture);
	} else if (el.attachEvent) { // IE
		if(!el.attachEvent("on" + evname, func))
			alert("event not attached");
		if (useCapture) {el.setCapture(false);}
	} else {
		el["on" + evname] = func;
	}
	RMX._removeOnUnload.push({"element" : el, "event" : evname, "listener" : func, "owner" : wnd.RMXUID});
};

/// Removes an event handler added with Zapatec.Utils.addEvent().  The
/// prototype scheme is the same.
RMX.removeEvent = function(el, evname, func) {
	var arr = RMX._removeOnUnload;
	var bMustShift=false;
	if(!arr[arr.length-1])
		alert("Incorrect Length Entering removeEvent");
	if (el.detachEvent) { // IE
		el.detachEvent("on" + evname, func);
	} else if (el.removeEventListener) { // Gecko / W3C
		el.removeEventListener(evname, func, false);
	} else {
		el["on" + evname] = null;
	}
	
	//Determine owner
	var wnd= el.window;
	if(!wnd)
		wnd=el.ownerDocument.parentWindow;
	
	//Clean-up the "onUnload" list now rather than waiting.
	//debugger;
	for (var iLis = arr.length - 1; iLis >= 0; iLis--)
	{
  		if(arr.length>0 && !arr[arr.length-1])
			debugger;
  	 	var listener = arr[iLis];
  		if( (listener["element"]==el) &&
  		(listener["event"]==evname) &&
  		(listener["listener"]==func) &&
  		(listener["owner"]==wnd.RMXUID))
  		{
  			
  			//debugger;
  			listener["element"]=null;
  			listener["event"]=null;
  			listener["listener"]=null;
  			listener["owner"]=null;
  			listener=null;
  			bMustShift = (iLis< (arr.length-1));
//  			if(bMustShift)
 // 				debugger;
 
  			if(arr.length>0 && !arr[arr.length-1])
				debugger;
	  			
 			if(bMustShift)
  			{
  				var iLisShift	
  				for(iLisShift = iLis+1;arr[iLisShift];iLisShift++)
  					arr[iLisShift-1] = arr[iLisShift];
  			}
  			arr.length -= 1;
  			
  			if(arr.length>0 && !arr[arr.length-1])
				debugger;

  			break;
  	 	 }
  	}
  	if(arr.length>0 && !arr[arr.length-1])
		alert("Incorrect Length Leaving removeEvent");
 	
};
RMX.addEvent(window, "load", RMX.ApplyTitle);

// Remove circular references to prevent memory leaks in IE
RMX.addEvent(window, 'unload', function() {
  for (var iLis = RMX._removeOnUnload.length - 1; iLis >= 0; iLis--) {
  	var listener = RMX._removeOnUnload[iLis];
  //debugger;
  	//BSB Only clean up the object if we're the owner.
  	if(window.RMXUID==listener["owner"])
  		RMX.removeEvent(listener["element"], listener["event"], listener["listener"]);
  }
});

// BSB 09.06.2007 Primary portion of "Red X" close w/o prompt solution.
// This logic interrupts the MDI child close initiated by the "Inner" "Red X" click.
// This implementation filters ALL screens (not just FDM). 
// However, conditional logic could be added to limit the scope of these prompts.
// Leaving this as is currently for "Proof of Concept" (POC).
RMX.window.ConfirmMDIUnload = function(evt)
{
	var sMsg = "Please confirm that you have completed work within this window.\nAny unsaved work will be lost.  \n\nPress OK to close without Saving or Cancel to continue working in this window.";
    //Parijat: Red "X" issue ---putting filter so that the message is only shown only if there is any change.
    var wndTarget =MDIParent.GetActiveMDIChild().content.getContainer().contentWindow;
	var bDoPrompt = false; //This becomes the System Default behavior...

	if (wndTarget.RMXOnClosePromptRequired) //Check to see if hook function is defined
		bDoPrompt = wndTarget.RMXOnClosePromptRequired(); //Allow hooked routine to control this behavior
	if(bDoPrompt)
	{
		if(confirm(sMsg))
		{
			MDIParent.GetActiveMDIChild().close();
		}
	}else
	{
			MDIParent.GetActiveMDIChild().close();
	}	

};
    


//Debugging Only: Check if parent has Top Level Script Libraries
//if(!(!!wnd.MDIParent))
//	alert("Global RMX MDIParent could not be referenced.");
