// Author: Raman Bhatia and Vaibhav Kaushik, 10/10/2005

var m_ClaimProgressNoteId = 0;
var m_NoteMemo = "";
var m_recordflag = true;//
//Parijat
function OnLoadViewAllNotes()
{
//debugger;
}
//Parijat
function funcViewAsHtml(isPrintHtml) {
       //15148
    if (document.getElementById("tdNoNotes") != null)
	{
	    //alert('There are no notes to be viewed.');
	    alert(ProgressNotesValidations.ValidNoNoteToView);
		return ;		
	}
//   window.open('home?pg=riskmaster/ProgressNotes/ViewAsHtml&amp;EventID=' + document.forms[0].eventid.value + '&amp;ClaimID=' + document.forms[0].claimid.value,'View_As_Html',
//			'width=750,height=500,top='+(screen.availHeight-400)/2+',left='+(screen.availWidth-750)/2+',resizable=yes,scrollbars=yes');

//if condition added by Shivendu to pass the freezetext value:MITS 18485
//rsolanki2: mits 21294

//tanwar2 - Print HTML Notes - start
var rdOptionList = eval('document.forms[0].rdbtnPrintNotes');
var sRadioValue = document.getElementById('hdnPrintOption');
for (var iItems = 0; rdOptionList != null && iItems < rdOptionList.length; iItems++) {
    if (rdOptionList !=null && sRadioValue!=null && rdOptionList[iItems].checked) {
        sRadioValue.value = rdOptionList[iItems].value;
        break;
    }
}
//MITS 29576
var bPrintAllPages = false;
if (isPrintHtml != "true") {
   if (sRadioValue !=null)
    sRadioValue.value = 0;
    if (document.getElementById('btnPrintHtmlClick') != null)
    document.getElementById('btnPrintHtmlClick').value = false;
}
else {
    bPrintAllPages = true;
    if (sRadioValue !=null && sRadioValue.value == "2" && document.forms[0].NoteType_codelookup.value == '') {
        //alert("Note Type cannot be blank!");
        alert(ProgressNotesValidations.ValidIsBlank);//Changed as per multilingual implementation.
        return false;
    }
    if (document.getElementById('btnPrintHtmlClick') != null)
           document.getElementById('btnPrintHtmlClick').value = true;
}
//tanwar2 - Print HTML Notes - end


var iPageNum = 1;
var objPageCount = document.getElementById("lblPageDetails");
if (objPageCount != null && isPrintHtml != "true") 
{
//    objPageCount = objPageCount.innertext.toString();
    //if (objPageCount.innerText != "Page 1 Of 1") 
    if (objPageCount.innerText != ProgressNotesValidations.ValidPageCount)
    {
        //bPrintAllPages = confirm("Do you want to view all pages ? \n\nPress Ok to view/print all pages \nPress Cancel to view/print just this page");
        bPrintAllPages = confirm(ProgressNotesValidations.ValidViewAllPages.split("\\n").join("\n"));
    } 
}
if (!bPrintAllPages) {
    //zmohammad MITS 31025 start
    var str = "";
    if (objPageCount != null) //mkaran2 MITS 27038
        str = objPageCount.innerText;

   var matches = str.match(/\d+/);
   if (matches) {
       iPageNum = parseInt(matches[0],7);
   }
//zmohammad MITS 31025 end
}
    //mkaran2 MITS 27038 start
var rdViewNotesOptionList = eval('document.forms[0].rdbtnViewNotes');
var sRadioViewNotesValue = "";
for (var iItems = 0; rdViewNotesOptionList != null && iItems < rdViewNotesOptionList.length; iItems++) {
    if (rdViewNotesOptionList != null && sRadioViewNotesValue != null && rdViewNotesOptionList[iItems].checked) {
        sRadioViewNotesValue = rdViewNotesOptionList[iItems].value;
        break;
    }
}

if (document.forms[0].lstNoteTypes_lst != null) {
    if (sRadioViewNotesValue !="0" && document.forms[0].lstNoteTypes_lst.value == "") {
        alert(AdvancedSearchValidations.jsValidIsBlank); 
        return false;
    }
    if (document.forms[0].notetypelist != null && document.forms[0].lstNoteTypes_lst.value !="") {
        document.forms[0].notetypelist.value = replace(document.forms[0].lstNoteTypes_lst.value, " ", ",");
    }

}
if (document.forms[0].hdClaimantId.value == "")
{
    document.forms[0].hdClaimantId.value = "-1";
}

    //mkaran2 MITS 27038 end
//alert(bPrintAllPages);
if (document.forms[0].freezetext != null && document.forms[0].showDateStamp != null && document.forms[0].username.value != null) {//Parijat :MITS 20194 :Direction and sort expression added as a querystring
    //pmittal5 Mits 21514
    //window.open('/RiskmasterUI/UI/ProgressNotes/ViewNotesAsHtml.aspx?EventID=' + document.forms[0].eventid.value + '&ClaimID=' + document.forms[0].claimid.value + '&FreezeText=' + document.forms[0].freezetext.value + '&ShowDateStamp=' + document.forms[0].showDateStamp.value + '&UserName=' + document.forms[0].username.value,'View_As_Html',
    //Add 'm_codeWindow' by kuladeep for mits:25424 "Force to close this window when parent window closed or after logout."
    //williams-neha goel---start:added for policy enhanced notes::Mits 21704
    //tmalhotra2: 26197
    //mbahl3 Mit:25495 Added claimant field 
    //Aman MITS 27290 Note Type List, Activity From Date, Activity To Date, User Type List, Sort By, Notes Text and Entered By List fields added
    // Added orderby field by akaushik5 for MITS 30789
    window.open('/RiskmasterUI/UI/ProgressNotes/ViewNotesAsHtml.aspx?EventID=' + document.forms[0].eventid.value + '&ClaimID='
            + document.forms[0].claimid.value + '&printallpages=' + bPrintAllPages
            + '&pagenum=' + iPageNum
            + '&FreezeText=' + document.forms[0].freezetext.value + '&ShowDateStamp=' + document.forms[0].showDateStamp.value + '&UserName='
            + document.forms[0].username.value + '&ClaimantName=' + escape(document.forms[0].claimant.value) //tmalhotra3-MITS 32493
            + '&ClaimantId=' + document.forms[0].hdClaimantId.value     //added by swati for MITS # 35530 Gap 15 WWIG
            + '&PolicyId=' + document.forms[0].policyid.value + '&PolicyName=' + document.forms[0].policyname.value
            + '&FormName=' + document.forms[0].SysFormName.value + '&PolicyNumber=' + document.forms[0].policynumber.value + '&CodeId=' 
            + document.forms[0].NoteParentType.value + '&LOB=' + document.forms[0].LOB.value
            + '&hdSortColumn=' + document.forms[0].hdSortColumn.value + '&hdDirection=' + document.forms[0].hdDirection.value
            + '&claimidlist=' + document.getElementById('claimidlist').value
            + '&activityfromdate=' + document.getElementById('activityfromdate').value
            + '&activitytodate= ' + document.getElementById('activitytodate').value
            + '&enteredbylist=' + document.getElementById('enteredbylist').value
            + '&notetypelist=' + document.getElementById('notetypelist').value
            + '&usertypelist=' + document.getElementById('usertypelist').value
            + '&sortby=' + document.getElementById('sSortBy').value
            + '&notestextcontains=' + document.getElementById('txtnotestextcontains').value
            + '&activatefilter=' + document.getElementById('activatefilter').value    
            //tanwar2 - print HTML NOTES - start
            + '&printOption=' + (sRadioValue !=null ? sRadioValue.value:'')
            + '&claimprogressnoteid=' + document.forms[0].claimprogressnoteid.value
            + '&claimNoteTypeid=' + document.getElementById('NoteType_codelookup_cid').value
            //tanwar2 - print HTML NOTES - start
            //mkaran2 MITS 27038 start
            + '&viewNotesOption=' + (sRadioViewNotesValue != "" ? sRadioViewNotesValue : '')
            //mkaran2 MITS 27038 end
            + '&orderby=' + document.getElementById('sOrderBy').value
            , 'View_As_Html','width=750,height=500,top=' + (screen.availHeight - 400) / 2 + ',left=' + (screen.availWidth - 750) / 2 + ',resizable=yes,scrollbars=yes');
	 //mbahl3 Mit:25495 Added claimant field 
	//williams-neha goel---end:Mits 21704
}
else {
    //pmittal5 Mits 21514
    //window.open('/RiskmasterUI/UI/ProgressNotes/ViewNotesAsHtml.aspx?EventID=' + document.forms[0].eventid.value + '&ClaimID=' + document.forms[0].claimid.value,'View_As_Html',
    //Add 'm_codeWindow' by kuladeep for mits:25424
    //tmalhotra2: 26197
    //williams-neha goel---start:added for policy enhanced notes:Mits 21704
    //Aman MITS 27290 Note Type List, Activity From Date, Activity To Date, User Type List, Sort By, Notes Text and Entered By List fields added
    // Added orderby field by akaushik5 for MITS 30789
    window.open('/RiskmasterUI/UI/ProgressNotes/ViewNotesAsHtml.aspx?EventID=' + document.forms[0].eventid.value
                + '&ClaimID=' + document.forms[0].claimid.value + '&printallpages=' + bPrintAllPages
                + '&pagenum=' + iPageNum
	         	 + '&ClaimantName=' + escape(document.forms[0].claimant.value)  //tmalhotra3-MITS 32493     //mbahl3 Mit:25495 Added claimant field 
                + '&PolicyId=' + document.forms[0].policyid.value + '&PolicyName=' + document.forms[0].policyname.value + '&FormName=' 
                + document.forms[0].SysFormName.value + '&PolicyNumber=' + document.forms[0].policynumber.value + '&CodeId=' + document.forms[0].NoteParentType.value
                + '&LOB=' + document.forms[0].LOB.value + '&hdSortColumn=' + document.forms[0].hdSortColumn.value
                + '&hdDirection=' + document.forms[0].hdDirection.value //, 'View_As_Html' //mkaran2 MITS 27038
                + '&claimidlist=' + document.getElementById('claimidlist').value
                + '&activityfromdate=' + document.getElementById('activityfromdate').value
                + '&activitytodate=' + document.getElementById('activitytodate').value
                + '&enteredbylist=' + document.getElementById('enteredbylist').value
                + '&notetypelist=' + document.getElementById('notetypelist').value
                + '&usertypelist=' + document.getElementById('usertypelist').value
                + '&sortby=' + document.getElementById('sSortBy').value
                + '&notestextcontains=' + document.getElementById('txtnotestextcontains').value
                + '&activatefilter=' + document.getElementById('activatefilter').value    
                //tanwar2 - print HTML NOTES - start
                + '&printOption=' + (sRadioValue != null ? sRadioValue.value : '')
                + '&claimprogressnoteid=' + document.forms[0].claimprogressnoteid.value
                + '&claimNoteTypeid=' + document.getElementById('NoteType_codelookup_cid').value
                //tanwar2 - print HTML NOTES - end
                //mkaran2 MITS 27038 start
                + '&viewNotesOption=' + (sRadioViewNotesValue != "" ? sRadioViewNotesValue : '')
                + '&orderby=' + document.getElementById('sOrderBy').value
                , 'View_As_Html'  //mkaran2 MITS 27038 end
                , 'width=750,height=500,top=' + (screen.availHeight - 400) / 2 + ',left=' + (screen.availWidth - 750) / 2 + ',resizable=yes,scrollbars=yes');

    //williams-neha goel---end:Mits 21704   
}
//window.open("/RiskmasterUI/UI/ProgressNotes/ProgressNotes.aspx?EventID="+escape(eventid)+"&ClaimID=0&EventNumber="+escape(eventnumber)+"&FormName="+escape(m_FormName),"mergeWnd",'width=900,height=600'+',top='+(screen.availHeight-600)/2+',left='+(screen.availWidth-900)/2+',resizable=no,scrollbars=yes');
	                        
   return false;
 
}
//Parijat
function ProgressNoteSelectionChanged( DateEntered , DateCreated , TimeCreated , EnteredByName , NoteTypeCode, UserTypeCode , AttachedTo , NotePosition , NoteTypeCodeId , ClaimProgressNoteId , ClaimID)
{
	if( document.forms[0].all("row" + NotePosition) != null )
	{
	    document.forms[0].all("row" + NotePosition).style.backgroundColor = 'LightGrey';
	}
	document.forms[0].all("DateEntered").innerText = DateEntered;
	document.forms[0].all("DateCreated").innerText = DateCreated;
	document.forms[0].all("TimeCreated").innerText = TimeCreated;
	document.forms[0].all("EnteredByName").innerText = EnteredByName;
	document.forms[0].all("NoteTypeCode").innerText = NoteTypeCode;
	document.forms[0].all("UserTypeCode").innerText = UserTypeCode;
	document.forms[0].all("AttachedTo").innerText = AttachedTo;
	var objNoteMemo=eval('document.getElementById("' + NotePosition + '")');
	document.forms[0].all("NoteMemoDiv").innerHTML = objNoteMemo.value;
	document.forms[0].all("notetypecodeid").innerText = NoteTypeCodeId;
	m_ClaimProgressNoteId = ClaimProgressNoteId;
	m_NoteMemo = objNoteMemo.value;
	if(ClaimID!=0)
		document.forms[0].claimid.value = ClaimID;
	
}
//Parijat
function NoteSelectionChanged()
{
var i = '<%=GridChange %>'
alert("NoteSelectionChanged");
}
//Parijat
function fnCheckSel(intObjId)
{
alert('<%=GridView1.Rows[intObjId].Cells[0].Text%>');
}
//Parijat
function SelectFirstRadio() 
{
//debugger;
    var bRadio = document.all.tags("input");
	for( var i = 0 ; i < bRadio.length - 1 ; i++ )
	{
		if( bRadio[i].type == "radio" )
		{
		bRadio[i].checked = true;
			bRadio[i].click();
			bRadio[i].focus();
			
		}
	}
}
//Parijat
function SelectOnlyOneRadio(objRadioButton, grdName) {
   
 var i, obj;
    //example of radio button id inside the grid (grdAddress): grdAddress__ctl2_radioSelect
    for (i=0; i<document.all.length; i++)
     {
        obj = document.all(i);
        if (obj.type == "radio") 
        {
            if (objRadioButton.id.substr(0, grdName.length) == grdName)
            if (objRadioButton.id == obj.id)
            obj.checked = true;
            else
            obj.checked = false;
        }
    }
}
//Start by Shivendu for  MITS 17443
function SelectOnlyOneRadioKeyPress(e) 
{
       var e = e ? e : window.event;	
	
            var KeyCode = e.which ? e.which : e.keyCode;
            
            if(KeyCode == 38)
                SelectOnlyOneRadioKeyUp();
            else if(KeyCode == 40)
                SelectOnlyOneRadioKeyDown();
                
            return false;

}

//rsolanki2 : mits 20462:- adding KB support (space + enter) to prog notes Infra grid
function SelectRowOnKeyPress(e) 
{
        var e = e ? e : window.event;
		var KeyCode = e.which ? e.which : e.keyCode;
		
		if(KeyCode == 32 || KeyCode == 13 )
        {	//space =32 : enter = 13
            ClientSelectRow();            
            //aanandpraka2:Changes for MITS 27384
            if (document.getElementById("btnEditNote") != null) {
                document.getElementById("btnEditNote").click();
            }
		}
		else if(KeyCode == 38 || KeyCode == 40 )
        {
            GridScrolltoView();
            ClientSelectRow(); //srajindersin: 12/28/2012 Changes for MITS 27384
		}
}


//rsolanki2: scroll progress notes infra grid into view 
function GridScrolltoView()
{
    var igRow = igtbl_getActiveRow("UltraWebGrid1");
    if(igRow!=null)
	{
		var clickedRowIndex = igRow.FirstRow.rowIndex - 1;
		//scroll Notes div into view using JS
		var myIframe = document.getElementById('div_NotesGrid');
		//Ankit Start : Worked for MITS - 33494 (Enhanced Notes Not Centered, scrolls after selecting a note)
		var childCellHeight = document.getElementById(igRow.getCell(0).Id).offsetHeight;
		//myIframe.scrollTop = 11 + clickedRowIndex * 15;
		myIframe.scrollTop = 11 + clickedRowIndex * childCellHeight;
        //Ankit End
			
	}
}

function SelectOnlyOneRadioKeyDown() 
{

 var i, obj,bCurrentRadioButton,objRadioPrevious;
 bCurrentRadioButton = false;
    
    for (i=0; i<document.all.length; i++)
     {
        obj = document.all(i);
        if (obj.type == "radio" && obj.id.indexOf('rdbtnPrintNotes') < 0) 
        {
            if(bCurrentRadioButton)
            {
                obj.checked = true;
                if(objRadioPrevious != null)
                {
                    objRadioPrevious.checked = false;
                }
                break;
            }

            if(obj.checked == true)
            {
                bCurrentRadioButton = true;
               objRadioPrevious = obj;
            }
            
        }
    }
    if(obj != null && obj.id != "" && obj.id.indexOf('rdbtnPrintNotes') < 0)
    document.getElementById(obj.id).click();
}

function SelectOnlyOneRadioKeyUp() 
{

 var i, obj,objRadioPrevious;
    
    for (i=0; i<document.all.length; i++)
     {
        obj = document.all(i);
        
        if (obj.type == "radio") 
        {
            
            if(obj.checked == true)
            {
                if( objRadioPrevious != null)
                {
                obj.checked = false;
                objRadioPrevious.checked = true;
                
                }
                else{
                
                }
                break;
            }
            else
            {
            obj.checked = false;
            objRadioPrevious = document.all(i);
            }
        }
    }
    if(objRadioPrevious != null && objRadioPrevious.id != "")
    document.getElementById(objRadioPrevious.id).click();
}
//End by Shivendu for  MITS 17443

//parijat
function ChangeColor(NotePosition)
{
	frmData.all("row" + NotePosition).style.backgroundColor='white';
}

function OnLoad()
{
	var bRadio = document.all.tags("input");
	for( var i = 0 ; i < bRadio.length - 1 ; i++ )
	{
		if( bRadio[i].type == "radio" )
		{
			bRadio[i].click();
			bRadio[i].focus();
			break;
		}
	}
	
	//Assign innerText to innerHTML for NoteMemoCareTech (MITS 7771)
	//Try to display &lsquo/&rsquo correctly
	var oTD = null;
	for(var i=1; i<1000; i++)
	{
		oTD = document.getElementById("NoteMemoCareTechTD" + i);
		if( oTD == null )
			break;
			
		oTD.innerHTML = oTD.innerText;
	}

}

function sortByCol( colName )
{
	if( document.forms[0].all("SortCol").value == colName )
	{
		if( document.forms[0].all("Ascending").value == "False" )
			document.forms[0].all("Ascending").value = "True" ;
		else
			document.forms[0].all("Ascending").value = "False" ;							
	}
	else
	{
		document.forms[0].all("Ascending").value = "True" ;
		document.forms[0].all("SortCol").value = colName ;	
	}				
	document.forms[0].ouraction.value = 'PageRefresh';
	document.forms[0].applysort.value = true;
	document.forms[0].functiontocall.value = "ProgressNotesAdaptor.OnLoad" ;
	document.forms[0].submit();											
}

//Parijat: MITS 14969
function DeleteNote()
{
    if (document.forms[0].claimprogressnoteid.value == '')
     {
         //alert('Please select a Note to Delete.');
         alert(ProgressNotesValidations.ValidDeleteNote);
        return false;
    }
    if (!self.confirm(ProgressNotesValidations.ValidConfirmDelete))
		return false;

//	document.forms[0].functiontocall.value = "ProgressNotesAdaptor.DeleteProgressNote" ;
//	document.forms[0].claimprogressnoteid.value = m_ClaimProgressNoteId;
//	document.forms[0].submit();

}
//Added by abhishek for MITS 11403 : End 
		

function PrintNotesReport()
{
	document.forms[0].functiontocall.value = "ProgressNotesAdaptor.PrintProgressNotesReport" ;	
	//Changed by Gagan for MITS 8697 : Start
	document.forms[0].claimprogressnoteid.value = "";
	//Changed by Gagan for MITS 8697 : End
	document.forms[0].submit();										
}

//Changed by Gagan for MITS 8697 : Start
function PrintSelectedNotesReport()
{
	document.forms[0].functiontocall.value = "ProgressNotesAdaptor.PrintProgressNotesReport" ;
	document.forms[0].claimprogressnoteid.value = m_ClaimProgressNoteId;
	document.forms[0].submit();										
}
//Changed by Gagan for MITS 8697 : End
			
//Parijat			
function selectClaim(sFieldName)
{
//	m_codeWindow=window.open('home?pg=riskmaster/ProgressNotes/SelectClaim&amp;EventID=' + document.forms[0].eventid.value ,'codeWnd',
//			'width=750,height=400'+',top='+(screen.availHeight-400)/2+',left='+(screen.availWidth-750)/2+',resizable=yes,scrollbars=yes');
//pmittal5 Mits 21514
    //  m_codeWindow=window.open('/RiskmasterUI/UI/ProgressNotes/SelectClaim.aspx?EventID=' + document.forms[0].eventid.value ,'codeWnd',
    m_codeWindow = window.open('/RiskmasterUI/UI/ProgressNotes/SelectClaim.aspx?EventID=' + document.forms[0].eventid.value + '&ClaimID=' + document.forms[0].claimid.value + '&LOB=' + document.forms[0].LOB.value, 'codeWnd',
			'width=750,height=400'+',top='+(screen.availHeight-400)/2+',left='+(screen.availWidth-750)/2+',resizable=yes,scrollbars=yes');


//	window.open('/RiskmasterUI/UI/ProgressNotes/ViewNotesAsHtml.aspx?EventID=' + document.forms[0].eventid.value + '&ClaimID=' + document.forms[0].claimid.value,'View_As_Html',
//			'width=750,height=500,top='+(screen.availHeight-400)/2+',left='+(screen.availWidth-750)/2+',resizable=yes,scrollbars=yes');

	return false;
}
function deleteSelClaim(sFieldName)
{
	var objCtrl=null;
	if(m_codeWindow!=null)
		m_codeWindow.close();
	objCtrl=eval('document.forms[0].lstClaims');
	if(objCtrl==null)
		return false;
	if(objCtrl.selectedIndex<0)
		return false;
	
	var bRepeat=true;
	while(bRepeat)
	{
		bRepeat=false;
		for(var i=0;i<objCtrl.length;i++)
		{
			// remove selected elements
			if(objCtrl.options[i].selected)
			{
				objCtrl.options[i]=null;
				bRepeat=true;
				break;
			}
		}
	}
	// Now create ids list
	var sId="";
	for(var i=0;i<objCtrl.length;i++)
	{
			if(sId!="")
				sId=sId+",";
			sId=sId+objCtrl.options[i].value;
	}
	objCtrl=null;
	objCtrl=eval("document.forms[0]." + sFieldName+"_lst");
	if(objCtrl!=null)
		objCtrl.value=sId;
	
	return true;
}
function CallParentPage(sClaimID , sClaimNumber)
{
	window.opener.UpdateParentPage(sClaimID , sClaimNumber);
	window.close();
	return false;
}
function UpdateParentPage(sClaimID , sClaimNumber)
{
	var objCtrl=null;
	
	objCtrl=eval('document.forms[0].lstClaims');
	
	var oldvalue=objCtrl.value;
	
	if(objCtrl.type=="textarea")
	{
		objCtrl.value=objCtrl.value + sCodeText + "\n";
	}
	else if(objCtrl.type=="select-one" || objCtrl.type=="select-multiple")
	{
		var bAdd=true;
		for(var i=0;i<objCtrl.length;i++)
		{
			if(objCtrl.options[i].value==sClaimID)
				bAdd=false;
		}
		if(bAdd)
		{
			var objOption = new Option(sClaimNumber, sClaimID, false, false);
			if ( objCtrl.options[0]!=null && objCtrl.options[0].value == 0 )
			{
				objCtrl.options[0]=null;
			}
			objCtrl.options[objCtrl.length] = objOption;
			objCtrl=null;
			objCtrl=eval("document.forms[0].lstClaims_lst");
			if(objCtrl!=null)
			{
				if(objCtrl.value!="" && objCtrl.value.substring(objCtrl.value.length,1)!=",")
					objCtrl.value=objCtrl.value+" ";
				objCtrl.value=objCtrl.value+sClaimID;
			}
		}
	}
	
}

function ValidateInfoCreateFilterString() {   
	if(document.forms[0].activityfromdate.value!='' && document.forms[0].activitytodate.value!='')
	{
		var activityfromdate = new Date(document.forms[0].activityfromdate.value);
		var activitytodate = new Date(document.forms[0].activitytodate.value);
		var datediff = activitytodate - activityfromdate;
		
		if(datediff<0)
		{
		    //alert("'Activity From Date' should be lesser than 'Activity To Date'.");
		    alert(AdvancedSearchValidations.jsValidDateCheck);
			return false;
		}
	}
	
	var tmpSortBy;				
	tmpSortBy = "";
    // Added by akaushik5 for MITS 30789 Starts
	var tmpOrderBy = "";
    // Added by akaushik5 for MITS 30789 Ends
	//Added by Amitosh for mits 23473 (05/11/2011)	
	//if (document.forms[0].selSortBy1.value != 0)
	    if (document.forms[0].selSortBy1.value != "")
		tmpSortBy = document.forms[0].selSortBy1.value;

    // Added by akaushik5 for MITS 30789 Starts
	    if (document.getElementById("selOrderBy1") != null) {
	        tmpOrderBy = document.getElementById("selOrderBy1").value;
	    }
    // Added by akaushik5 for MITS 30789 Ends

	    //	if (document.forms[0].selSortBy2.value!=0)
	    if (document.forms[0].selSortBy2.value != "")
	{
		if (tmpSortBy.match(document.forms[0].selSortBy2.value))
		{
		    //alert("Duplicate sort order is not allowed.");
		    alert(AdvancedSearchValidations.jsValidDuplicateSortOrder);
			return false;
		}
		else {
		    //			tmpSortBy=tmpSortBy + "," + document.forms[0].selSortBy2.value;
		    tmpSortBy = tmpSortBy + "|" + document.forms[0].selSortBy2.value; //Added by Amitosh for mits 23473 (05/11/2011)
		    // Added by akaushik5 for MITS 30789 Starts
		    if (document.getElementById("selOrderBy2") != null) {
		        tmpOrderBy = tmpOrderBy + "|" + document.getElementById("selOrderBy2").value;
		    }
		    // Added by akaushik5 for MITS 30789 Ends
		}
	}
//	if (document.forms[0].selSortBy3.value!=0)
if (document.forms[0].selSortBy3.value != "")
	{
		if (tmpSortBy.match(document.forms[0].selSortBy3.value))
		{
		    //alert("Duplicate sort order is not allowed.");
		    alert(AdvancedSearchValidations.jsValidDuplicateSortOrder);
			return false;
		}
		else {
		    //			tmpSortBy=tmpSortBy + "," + document.forms[0].selSortBy3.value;
		    tmpSortBy = tmpSortBy + "|" + document.forms[0].selSortBy3.value; //Added by Amitosh for mits 23473 (05/11/2011)
		    // Added by akaushik5 for MITS 30789 Starts
		    if (document.getElementById("selOrderBy3") != null) {
		        tmpOrderBy = tmpOrderBy + "|" + document.getElementById("selOrderBy3").value;
		    }
		    // Added by akaushik5 for MITS 30789 Ends
		}
	}
//	if (document.forms[0].selSortBy4.value!=0)
if (document.forms[0].selSortBy4.value != "")
	{
		if (tmpSortBy.match(document.forms[0].selSortBy4.value))
		{
		    //alert("Duplicate sort order is not allowed.");
		    alert(AdvancedSearchValidations.jsValidDuplicateSortOrder);
			return false;
		}
		else {
		    //			tmpSortBy=tmpSortBy + "," + document.forms[0].selSortBy4.value;
		    tmpSortBy = tmpSortBy + "|" + document.forms[0].selSortBy4.value; //Added by Amitosh for mits 23473 (05/11/2011)
		    // Added by akaushik5 for MITS 30789 Starts
		    if (document.getElementById("selOrderBy4") != null) {
		        tmpOrderBy = tmpOrderBy + "|" + document.getElementById("selOrderBy4").value;
		    }
		    // Added by akaushik5 for MITS 30789 Ends
		}
	}
	
//	if(tmpSortBy.substring(0 , 1 ) == ',')
if (tmpSortBy.substring(0, 1) == '|')
    tmpSortBy = tmpSortBy.substring(1); //Added by Amitosh for mits 23473 (05/11/2011)
	
document.forms[0].sSortBy.value = tmpSortBy;
    // Added by akaushik5 for MITS 30789 Starts
if (document.getElementById("sOrderBy") != null) {
    document.getElementById("sOrderBy").value = tmpOrderBy;
}
    // Added by akaushik5 for MITS 30789 Ends
		 //parijat:EnhancedMDI					
	//activating filter on parentpage and filling information on parent page
	document.forms[0].activatefilter.value = true;
	document.forms[0].claimidlist.value = replace(document.forms[0].lstClaims_lst.value, " ", ",");
	document.forms[0].usertypelist.value = replace(document.forms[0].lstUserTypes_lst.value, " ", ",");
	document.forms[0].notetypelist.value = replace(document.forms[0].lstNoteTypes_lst.value, " ", ",");
	document.forms[0].enteredbylist.value = replace(document.forms[0].lstEnteredBy_lst.value, " ", ",");
//	window.document.forms[0].activityfromdate.value = document.forms[0].activityfromdate.value;
//	window.document.forms[0].activitytodate.value = document.forms[0].activitytodate.value;
//	window.opener.document.forms[0].txtnotestextcontains.value = document.forms[0].txtnotestextcontains.value;
	
	//creating sorting string
	document.forms[0].sSortBy.value = replace(document.forms[0].sSortBy.value, "1", "CLAIM_PRG_NOTE.ENTERED_BY_NAME");
//MGaba2:MITS 22186:Enhanced Note Advance Search sorting was done on the basis of note description instead of note type code
	//document.forms[0].sSortBy.value = replace(document.forms[0].sSortBy.value, "2", "NOTE_TYPE_DESC");
	document.forms[0].sSortBy.value = replace(document.forms[0].sSortBy.value, "2", "NOTE_TYPE_SHORT_CODE");
	document.forms[0].sSortBy.value = replace(document.forms[0].sSortBy.value, "3", "DATE_ENTERED");
	//document.forms[0].sSortBy.value = replace(document.forms[0].sSortBy.value, "4", "USER_TYPE_DESC");
	document.forms[0].sSortBy.value = replace(document.forms[0].sSortBy.value, "4", "USER_TYPE_CODE");
	document.forms[0].sSortBy.value = replace(document.forms[0].sSortBy.value , "5" , "ADJUSTER_LASTNAME, ADJUSTER_FIRSTNAME");
	
//	window.opener.document.forms[0].sSortBy.value = document.forms[0].sSortBy.value;

	document.forms[0].ouraction.value = "Filter";
	document.forms[0].functiontocall.value = "ProgressNotesAdaptor.OnLoad";
	document.forms[0].applysort.value = false;
	document.forms[0].txtSubmit.value = "SUBMIT"; //so that it does not go into (!IsPostBack)
//	window.opener.document.forms[0].submit();
//    window.opener.document.location.reload(true);
//	window.close(); //parijat:EnhancedMDI
	return true;
}
//function MoveToAllNotes()
//{
//    document.forms[0].activatefilter.value = false;
//	document.forms[0].claimidlist.value = replace(document.forms[0].lstClaims_lst.value , " " , ",");
//	window.opener.document.forms[0].usertypelist.value = replace(document.forms[0].lstUserTypes_lst.value , " " , ",");
//	window.opener.document.forms[0].notetypelist.value = replace(document.forms[0].lstNoteTypes_lst.value , " " , ",");
//	window.opener.document.forms[0].enteredbylist.value = replace(document.forms[0].lstEnteredBy_lst.value , " " , ",");
//	window.opener.document.forms[0].activityfromdate.value = document.forms[0].activityfromdate.value;
//	window.opener.document.forms[0].activitytodate.value = document.forms[0].activitytodate.value;
//	window.opener.document.forms[0].txtnotestextcontains.value = document.forms[0].txtnotestextcontains.value;
//	
//	//creating sorting string
//	document.forms[0].sSortBy.value = r
//}
function AdvancedSearch()
{
//	var objWnd=window.open("home?pg=riskmaster/ProgressNotes/AdvancedSearch&amp;EventID=" + document.forms[0].eventid.value + "&amp;ClaimID=" + document.forms[0].claimid.value + "&amp;FormName=" + document.forms[0].formname.value + "&amp;EventNumber=" + document.forms[0].eventnumber.value,"mergeWndAdvancedSearch",'width=700,height=500'+',top='+(screen.availHeight-500)/2+',left='+(screen.availWidth-700)/2+',resizable=yes,scrollbars=yes');
	
	//willimas-neha goel---start:added for policy enhanced notes
    //var objWnd=window.open("/RiskmasterUI/UI/ProgressNotes/AdvancedSearch.aspx?EventID=" + document.forms[0].eventid.value + "&ClaimID=" + document.forms[0].claimid.value + "&FormName=" + document.forms[0].formname.value + "&EventNumber=" + document.forms[0].eventnumber.value,"mergeWndAdvancedSearch",'width=700,height=500'+',top='+(screen.availHeight-500)/2+',left='+(screen.availWidth-700)/2+',resizable=yes,scrollbars=yes');
    var objWnd = window.open("/RiskmasterUI/UI/ProgressNotes/AdvancedSearch.aspx?EventID=" + document.forms[0].eventid.value + "&ClaimID=" + document.forms[0].claimid.value + "&FormName=" + document.forms[0].formname.value + "&EventNumber=" + document.forms[0].eventnumber.value + "&PolicyId=" + document.forms[0].policyid.value + "&PolicyName=" + document.forms[0].policyname.value, "mergeWndAdvancedSearch", 'width=700,height=500' + ',top=' + (screen.availHeight - 500) / 2 + ',left=' + (screen.availWidth - 700) / 2 + ',resizable=yes,scrollbars=yes');
    //willimas-neha goel---end
//	window.open('/RiskmasterUI/UI/ProgressNotes/ViewNotesAsHtml.aspx?EventID=' + document.forms[0].eventid.value + '&ClaimID=' + document.forms[0].claimid.value,'View_As_Html',
//			'width=750,height=500,top='+(screen.availHeight-400)/2+',left='+(screen.availWidth-750)/2+',resizable=yes,scrollbars=yes');

	return false;
}
//Parijat
function CreateNotes(NewRecordFlag) 
{      
	//Changed for MITS 12334 : Start
        var m_FreezeText = document.forms[0].freezetext.value;
        var m_UserName = document.forms[0].username.value;
        var hdnewrecord = document.forms[0].hdNewRecord //parijat:EnhancedMDI
	//Changed for MITS 12334 : Edit
        if (NewRecordFlag == 'true')
         {
             NewRecord = true;
             document.forms[0].hdNewRecord.value = "true"; //parijat:EnhancedMDI
        }
        else
         {
             //		if(m_recordflag == 'false')
            if (document.forms[0].claimprogressnoteid.value == '')
             {
                 //alert('Please select a Note to edit.');
                 alert(ProgressNotesValidations.ValidEditNote);
                return false;
            }
            NewRecord = false;
            document.forms[0].hdNewRecord.value = "false"; //parijat:EnhancedMDI
        }
	var temp;
	//Parijat :EnhancedMDI
//	var flag ="1";
//	try
//	{//Changed for MITS 12334 : Edit
////	temp=window.open("home?pg=riskmaster/ProgressNotes/MainPage&amp;EventID=" + document.forms[0].eventid.value + "&amp;ClaimID=" + document.forms[0].claimid.value + "&amp;SysFormName=" + document.forms[0].formname.value + "&amp;EventNumber=" + document.forms[0].eventnumber.value + "&amp;NoteType=" + frmData.all("NoteTypeCode").innerText + "&amp;NoteTypecodeid=" + document.forms[0].notetypecodeid.value + "&amp;ClaimProgressNoteId=" + m_ClaimProgressNoteId + "&amp;ActivityDate=" + frmData.all("DateEntered").innerText + "&amp;CommentsFlag=true&amp;NewRecord=" + NewRecord,"mergeWndCreateEditNote",'width=700,height=500'+',top='+(screen.availHeight-500)/2+',left='+(screen.availWidth-700)/2+',resizable=yes,scrollbars=yes');
//	temp=window.open("/RiskmasterUI/UI/ProgressNotes/ProgressNotesEditPage.aspx?EventID=" + document.forms[0].eventid.value + "&ClaimID=" + document.forms[0].claimid.value + "&SysFormName=" + document.forms[0].formname.value + "&EventNumber=" + document.forms[0].eventnumber.value + "&NoteType=" + frmData.all("NoteTypeCode").innerText + "&NoteTypecodeid=" + document.forms[0].notetypecodeid.value + "&ClaimProgressNoteId=" + document.forms[0].claimprogressnoteid.value + "&ActivityDate=" + frmData.all("DateEntered").innerText + "&CommentsFlag=true&NewRecord=" + NewRecord + "&FreezeText=" + m_FreezeText + "&UserName=" + m_UserName,"mergeWndCreateEditNote",'width=700,height=500'+',top='+(screen.availHeight-500)/2+',left='+(screen.availWidth-700)/2+',resizable=yes,scrollbars=yes');
////Changed for MITS 12334 : Edit
//	
//	}
//	catch(ex)
//	{
//	    flag = "0";
//	}
//	//Parijat: to accomodate changes made in RMX.window.open(try ..catch) for 10284. 
//		if (flag =="1")
//            objWnd=temp;
	return true;
}
function ValidateExistingRecord()
{
	if(document.forms[0].lstNoteTypes.value=='')
	{
		alert("Please enter the Note Type Code.");
		return false;
	}
			
	document.forms[0].action.value = "Save";
	document.forms[0].submit();	
}
function ConfirmCancel()
{
	var ret = false;
	ret = confirm('Press OK to confirm Cancel (All unsaved changes would be lost!!)');
	return ret;

}

function UpdateRecordFlag(value)
{
	m_recordflag = value;
}

function OnNoteLoad()
{
	var oCtrl = document.getElementById("bNewRecord");
	var bNewRecord;
	if( oCtrl != null )
	{
		bNewRecord = oCtrl.value;
		
		if( bNewRecord == 'false' )
		
		{
		    //MITS 8784..Enhanced Notes should be editable.. Raman Bhatia
		    //Commenting following code as now existing comments would not display in a seperate readonly frame..
		    /*
			var oExistingComments = null;
			if( document.frames.length > 0 )
			{
				oExistingComments = eval('document.frames("iFrameExistingComments").document.forms[0].all.txtExistingComments');
			}
			if ( oExistingComments != null )
			{
				
				oExistingComments.innerHTML = window.opener.m_NoteMemo;
			}
			*/
		}
		
		else
		{
			var Today = new Date();
			var CurrentMonth = parseInt(Today.getMonth())+1
			var strDate = CurrentMonth + "/" + Today.getDate() + "/"+ Today.getFullYear();
			document.forms[0].activitydate.value = strDate;
		}
	}
}
//MITS 14592 sgoel6
function CheckPrintNoNote()
{
    if (document.forms[0].claimprogressnoteid.value == '')
     {
        alert('There is no note to be printed.');
        return false;
    }
}
function CheckTemplates() {
    if (document.forms[0].TemplateId.value == '') {
        //alert('There is no existing template.');
        alert(ProgressNotesTemplatesValidations.ValidNoExistingTmplts); //Amandeep ML Changes
        return false;
    }

}

//Rsolanki2: Adding delete confirmation
function DeleteTemplates() {
    if (document.forms[0].TemplateId.value == '') 
	{
	    //alert('There is no existing template.');
	    alert(ProgressNotesTemplatesValidations.ValidNoExistingTmplts); //Amandeep ML changes
        return false;
    } else 
	{
        //MGaba2: MITS 22607: Initial character of the first word in CAPS.
	    //return (confirm ("Are you sure you want to delete this template?"));
	    return (confirm(ProgressNotesTemplatesValidations.ValidDeleteTemp)); //Amandeep ML Changes
	}

}

function lookupTemplateName() {
//pmittal5 Mits 21514 - Pass claimid , eventid and LOB for checking Module permissions
    //m_TemplateNameWindow = window.open('/RiskmasterUI/UI/ProgressNotes/ProgressNotesTemplates.aspx?LookUp=true', 'lookupWnd',
    // 		                                'width=600,height=250,top=' + (screen.availHeight - 250) / 2 + ',left=' + (screen.availWidth - 600) / 2 + ',resizable=yes,scrollbars=yes');
    m_TemplateNameWindow = window.open('/RiskmasterUI/UI/ProgressNotes/ProgressNotesTemplates.aspx?EventID=' + document.forms[0].eventid.value + '&ClaimID=' + document.forms[0].hdclaimid.value + '&LOB=' + document.forms[0].hdLOBCode.value + '&LookUp=true', 'lookupWnd',
 		                                'width=600,height=250,top=' + (screen.availHeight - 250) / 2 + ',left=' + (screen.availWidth - 600) / 2 + ',resizable=yes,scrollbars=yes');
//End - pmittal5
    return false;
}

function setTemplateName(sTemplateId, sTemplateName) {

    objCtrl = eval('window.opener.document.forms[0].TemplateName_cid');
    if (objCtrl != null) {
        objCtrl.value = sTemplateId;
    }
    objCtrl = eval('window.opener.document.forms[0].TempName');
    if (objCtrl != null) {
        objCtrl.value = sTemplateName;
    }
    objCtrl = eval('window.opener.document.forms[0].TemplateName');
    if (objCtrl != null) {
        if (objCtrl.value == "")
            objCtrl.value = sTemplateName; //for onChange to fire on every instance
        else
            objCtrl.value = ""; //for onChange to fire on every instance
        
        // rrachev JIRA RMA-4789 BEGIN
        // window.opener.document.forms[0].submit();
        window.opener.m_IsSetTemplateName = true;
        try{
            window.opener.document.forms[0].submit();
        }
        finally{
            window.opener.m_IsSetTemplateName = false;
        }
        // rrachev JIRA RMA-4789 END
    }
    
    window.close();
    return true;
}
function CancelTemplates() {
   objTempName = eval('window.opener.document.forms[0].TempName');
   if (objTempName!= null) {
       objTemplateName = eval('window.opener.document.forms[0].TemplateName');
       if (objTemplateName != null) {
           if (objTemplateName.value == "") {
               objTemplateName.value = "";
               objTempName.value = "";
               objTemplateName_cid = eval('window.opener.document.forms[0].TemplateName_cid');
               if (objTemplateName_cid != null) {
                   objTemplateName_cid.value = 0;
               }
           }
           else {
               objTemplateName.value = objTempName.value;
           }
       }
   }
    window.close();
    return false;

}

function DisableEditButton(){
    if (document.forms[0].hdnEditButtonState != null) {
        if(document.forms[0].hdnEditButtonState.value == "enabled")
            document.forms[0].btnEditNote.disabled = false;
        else
            document.forms[0].btnEditNote.disabled = true;
    }
}
 //williams:neha goel----start:Mits 21704   
function DisableDeleteButton(){   
    if (document.forms[0].hdnDeleteButtonState != null) {
        if(document.forms[0].hdnDeleteButtonState.value == "enabled")
            document.forms[0].btnDeleteNotes.disabled = false;
        else
            document.forms[0].btnDeleteNotes.disabled = true;
    }
}

//williams:neha goel----end :Mits 21704  
//tanwar2 - Print HTML Note
function printPage() {
    if (document.getElementById('btnPrintHtmlClick') != null) {
        if (window.opener.document.getElementById('btnPrintHtmlClick').value != "false") {
            document.title = "Print HTML Notes";
            if (window.opener.document.getElementById('btnPrintHtmlClick').value != "disabled") {
                self.print();
                window.opener.document.getElementById('btnPrintHtmlClick').value = "disabled";
            }
        }
    }
}
// mbahl3 Added for MITS 30513 Starts
function showHideElements() {
    if (document.getElementById('formname') != null && document.getElementById('divSelectNotesLevel') != null) {
        var formName = document.getElementById('formname').value;
        if (formName == "policy" || formName == "policyenhal" || formName == "policyenhgl" || formName == "policyenhpc" || formName == "policyenhwc") {
            document.getElementById('divSelectNotesLevel').style.display = "none";
        }
        else {
            document.getElementById('divSelectNotesLevel').style.display = "";
        }
    }
}
// mbahl3 Added for MITS 30513 Ends
/*GBINDRA MITS#34104 02062014 WWIG GAP15 START*/
function selectClaimant(sFieldName) {
    //	m_codeWindow=window.open('home?pg=riskmaster/ProgressNotes/SelectClaim&amp;EventID=' + document.forms[0].eventid.value ,'codeWnd',
    //			'width=750,height=400'+',top='+(screen.availHeight-400)/2+',left='+(screen.availWidth-750)/2+',resizable=yes,scrollbars=yes');
    //pmittal5 Mits 21514
    //  m_codeWindow=window.open('/RiskmasterUI/UI/ProgressNotes/SelectClaim.aspx?EventID=' + document.forms[0].eventid.value ,'codeWnd',
    m_codeWindow = window.open('/RiskmasterUI/UI/ProgressNotes/SelectClaimant.aspx?EventID=' + document.forms[0].eventid.value + '&ClaimID=' + document.forms[0].claimid.value + '&LOB=' + document.forms[0].LOB.value, 'codeWnd',
			'width=750,height=400' + ',top=' + (screen.availHeight - 400) / 2 + ',left=' + (screen.availWidth - 750) / 2 + ',resizable=yes,scrollbars=yes');


    //	window.open('/RiskmasterUI/UI/ProgressNotes/ViewNotesAsHtml.aspx?EventID=' + document.forms[0].eventid.value + '&ClaimID=' + document.forms[0].claimid.value,'View_As_Html',
    //			'width=750,height=500,top='+(screen.availHeight-400)/2+',left='+(screen.availWidth-750)/2+',resizable=yes,scrollbars=yes');

    return false;
}
function deleteSelClaimant(sFieldName) {
    var objCtrl = null;
    if (m_codeWindow != null)
        m_codeWindow.close();
    objCtrl = eval('document.forms[0].lstClaimant');
    if (objCtrl == null)
        return false;
    if (objCtrl.selectedIndex < 0)
        return false;

    var bRepeat = true;
    while (bRepeat) {
        bRepeat = false;
        for (var i = 0; i < objCtrl.length; i++) {
            // remove selected elements
            if (objCtrl.options[i].selected) {
                objCtrl.options[i] = null;
                bRepeat = true;
                break;
            }
        }
    }
    // Now create ids list
    var sId = "";
    for (var i = 0; i < objCtrl.length; i++) {
        if (sId != "")
            sId = sId + ",";
        sId = sId + objCtrl.options[i].value;
    }
    objCtrl = null;
    objCtrl = eval("document.forms[0]." + sFieldName + "_lst");
    if (objCtrl != null)
        objCtrl.value = sId;

    return true;
}
function CallClaimantParentPage(sClaimName, sClaimantId, sClaimantLOB) {
    window.opener.UpdateClaimantParentPage(sClaimName, sClaimantId, sClaimantLOB);
    window.close();
    return false;
}
function UpdateClaimantParentPage(sClaimName, sClaimantId, sClaimantLOB) {
    var objCtrl = null;

    objCtrl = eval('document.forms[0].lstClaimant');

    var oldvalue = objCtrl.value;

    if (objCtrl.type == "textarea") {
        objCtrl.value = objCtrl.value + sCodeText + "\n";
    }
    else if (objCtrl.type == "select-one" || objCtrl.type == "select-multiple") {
        var bAdd = true;
        for (var i = 0; i < objCtrl.length; i++) {
            if (objCtrl.options[i].value == sClaimName)
                bAdd = false;
        }
        if (bAdd) {
            var objOption = new Option(sClaimName, sClaimantId, false, false);
            if (objCtrl.options[0] != null && objCtrl.options[0].value == 0) {
                objCtrl.options[0] = null;
            }
            objCtrl.options[objCtrl.length] = objOption;
            objCtrl = null;
            objCtrl = eval("document.forms[0].lstClaimant_lst");
            if (objCtrl != null) {
                if (objCtrl.value != "" && objCtrl.value.substring(objCtrl.value.length, 1) != ",")
                    objCtrl.value = objCtrl.value + " ";
                objCtrl.value = objCtrl.value + sClaimName;
            }
            objCtrl = eval("document.forms[0].hdClaimantId");
            if (objCtrl != null) {
                objCtrl.value = sClaimantId;
            }
            objCtrl = eval("document.forms[0].LOB");
            if (objCtrl != null) {
                objCtrl.value = sClaimantLOB;
            }
        }
    }

}
/*GBINDRA MITS#34104 02062014 WWIG GAP15 END*/