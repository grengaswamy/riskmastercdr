// Author: Denis Basaric, 03/28/2000
// Last Modified by: J. Partin 7/10/2000    Hacked for use in SM Report Queue

function getSelJobs()
{
	var sJobs=new String();
	for(var i=0;i<document.forms[0].length;i++)
	{
		var objElem=document.forms[0].elements[i];
		var sName=new String(objElem.name);
		if(sName.substring(0,6)=="seljob" && objElem.checked)
		{
			if(sJobs!="")
				sJobs=sJobs + ",";
			sJobs=sJobs + sName.substring(6,sName.length);
		}
	}
	return sJobs;
}

function ArchiveJob()
{
	var sJobs=getSelJobs();

	if(sJobs=="")
	{
		alert("Please select report jobs you would like to archive.");
		return true;
	}
		
	// Send request to the server
	document.location="smarchivejob.asp?jobs=" + escape(sJobs);
	return true;
}

function DeleteJob()
{
	var sJobs=getSelJobs();
	if(sJobs=="")
	{
		alert("Please select report jobs to delete.");
		return true;
	}
	//document.location="smdeletejob.asp?jobs=" + escape(sJobs);
	document.forms[0].jobs.value=sJobs;
	document.forms[0].action='smdeletejob.asp';
	document.forms[0].method='post';
	document.forms[0].submit();	
	return true;

}

function EMailJob()
{
	var sJobs=getSelJobs();
	if(sJobs=="")
	{
		alert("Please select report jobs you would like to e-mail.");
		return true;
	}
	document.location="smemailjob.asp?jobs=" + escape(sJobs);
	return true;
}
