﻿/**********************************************************************************************
*   Date     |  MITS/JIRA       | Programmer | Description                                   *
**********************************************************************************************
* 12/09/2014 | RMA-438          | ajohari2   | Underwriters - EFT Payments
**********************************************************************************************/
parent.CommonValidations = parent.parent.CommonValidations;
if (parent.CommonValidations == undefined) {
    if (parent.opener != undefined) {
        if (parent.opener.parent != undefined) {
            if (parent.opener.parent.parent != undefined) {
                parent.CommonValidations = parent.opener.parent.parent.CommonValidations;
            }
        }
    }
}
var NS4 = (document.layers);
var IE4 = (document.all);
var OPERA=(navigator.userAgent.toLowerCase().indexOf('opera') != -1);
var m_sParentName=""; // Mihika 10/14/2005 Added for filling up the parent field, in cae of a code table
var m_codeWindow=null;
var m_LookupType=0;
var m_sFieldName="";
var m_sNoDataId = "";
var m_DataChanged=false;
var m_bSubmitted=false;
var m_LookupTextChanged=false;
var m_LookupCancelFieldList = "";
var m_LookupBusy = false;
var m_FormID="";
var m_ClaimIDLookup=false; //Tr : - 1547 Nitesh sarts
var m_AvoidDataChangeConfirmation=false; 
var m_IsAppBusy = false;

try{var newwin=null;}
catch(e){alert(e);}
// Parijat : Red "X" Issue --Putting filter so that the 
//          prompt shoud come only if there is a change in any of the mdi window.
//trans.js is used in place of form.js in case of funds since 'm_DataChanged' is defined here in this case.
function RMXOnClosePromptRequired()
{
	return m_DataChanged;	
}
//Tr : - 1547 Nitesh sarts
function lookupLostFocus(objField)
{	
	if(m_LookupTextChanged && objField.value!="")
	{  
		var objButton=eval("document.forms[0]."+objField.id+"btn");
		
		if(objButton!=null)
		{   
			m_AutoLookup=true;
			objButton.click();
		}
	}
	else
		m_LookupTextChanged=false;
	
	return true;
}
function EditMemo(sFieldName)
{
	if(sFieldName=="")
		return false;
		
	document.OnEditMemoSave=OnEditMemoSave;
	document.OnEditMemoLoaded=OnEditMemoLoaded;
	
	var sLink="home?pg=riskmaster/Comments/EditorPage&loaded=OnEditMemoLoaded&fieldname="+escape(sFieldName)+"&CommentsFlag=false";
	m_CommentsWnd=RMX.window.open(sLink,"memoWnd",
	'width=620,height=450,top='+(screen.availHeight-400)/2+',left='+(screen.availWidth-620)/2+',resizable=yes,scrollbars=yes');
		
	return true;
}

function EditHTMLMemo(sFieldName)
{
	if(sFieldName=="")
		return false;
		
	document.OnEditMemoSave=OnEditMemoSave;
	document.OnEditMemoLoaded=OnEditMemoLoaded;
	
	var sLink="home?pg=riskmaster/Comments/MainPage&loaded=OnEditMemoLoaded&fieldname="+escape(sFieldName)+"&CommentsFlag=false";
	m_CommentsWnd=RMX.window.open(sLink,"memoWnd",
	'width=620,height=450,top='+(screen.availHeight-400)/2+',left='+(screen.availWidth-620)/2+',resizable=yes,scrollbars=yes');
		
	return true;
}

function OnEditMemoLoaded()
{//debugger
	var objField=eval("document.forms[0]."+m_CommentsWnd.document.frmWizardData.hdFieldName.value);
	var objHTMLField=eval("document.forms[0]."+m_CommentsWnd.document.frmWizardData.hdFieldName.value + "_HTML");

	if(objField==null)
		return false;

	if(objField.value != '')
	{
		m_CommentsWnd.document.frmWizardData.all.hdComments.value = objField.value + '\n';
		if(objHTMLField != null)
		{
			if(objHTMLField.value == "" && objField.value != "")
				m_CommentsWnd.document.frmWizardData.all.hdHTMLComments.value = objField.value + '<BR>';
			else
				m_CommentsWnd.document.frmWizardData.all.hdHTMLComments.value = objHTMLField.value + '<BR>';
		}
		//m_CommentsWnd.document.frmWizardData.all.txtExistingComments.innerHTML = objField.value + '\n';
	}

	return true;
}

function OnEditMemoSave()
{
	var objNewMemo, objDateTimeStamp, objExistingComments;
	
	if(m_CommentsWnd==null)
		return false;
	var objField=eval("document.forms[0]."+m_CommentsWnd.document.frmWizardData.hdFieldName.value);
	var objHTMLField=eval("document.forms[0]."+m_CommentsWnd.document.frmWizardData.hdFieldName.value + "_HTML");
	
	if(objField!=null)
	{
		objNewMemo = eval(m_CommentsWnd.document.frmWizardData.all.hdPlainText);
		objNewHTMLMemo = eval(m_CommentsWnd.document.frmWizardData.all.hdTextHtml);
		objDateTimeStamp=eval(m_CommentsWnd.document.frmWizardData.all.txtDtmStamp);
		objExistingComments = eval(m_CommentsWnd.document.frmWizardData.all.hdComments);
		objExistingHTMLComments = eval(m_CommentsWnd.document.frmWizardData.all.hdHTMLComments);
		if(objExistingHTMLComments == null)
			objExistingHTMLComments.value = "";

		if (objNewMemo!=null)
		{
			if (objDateTimeStamp!=null)
			{		
				if(objNewMemo.value != '')
				{
					objField.value=objExistingComments.value + '\n' + objDateTimeStamp.value + ' ' + objNewMemo.value;
					if(objHTMLField != null)
						objHTMLField.value=objExistingHTMLComments.value + '<BR>' + objDateTimeStamp.value + ' ' + objNewHTMLMemo.value;
				}
			}	
			else
			{
				objField.value=objExistingComments.value + '\n' + objNewMemo.value;
				if(objHTMLField != null)
					objHTMLField.value=objExistingHTMLComments.value + '<BR>' + objNewHTMLMemo.value;
			}
		}
		else
		{
			objField.value=objExistingComments.value;
			if(objHTMLField != null)
				objHTMLField.value=objExistingHTMLComments.value;
		}
	}
	
	setDataChanged(true);
	m_CommentsWnd.close();
	return true;
}

var m_objWndSpellCheck = "";
function FCKSpellCheckCommandLoad()
{
	try
	{
		if(document.frmWizardData.txtMemo.value == "")
		{
			alert("No comments found. Spell check operation aborted.");
			document.frmWizardData.txtMemo.focus();
		}
		else
		{
			document.OnSpellCheckLoad=OnSpellCheckLoad;
			document.OnSpellCheckComplete=OnSpellCheckComplete;
			m_objWndSpellCheck = RMX.window.open('home?pg=riskmaster/Comments/SpellCheck','SpellWnd',
			'width=500,height=400'+',top='+(screen.availHeight-400)/2+',left='+(screen.availWidth-500)/2+',resizable=no,scrollbars=yes');
		}
	}
	catch(e)
	{
		if(e.number==-2146827859)
		{
				RMX.window.open('home?pg=riskmaster/Comments/SpellCheck');
		}
		else 
			alert('Error Loading Spell Check: '+e.message+' ('+e.number+')');
			//alert('Error Loading ieSpell: '+e.message+' ('+e.number+')');
	}
}

function OnSpellCheckLoad()
{
	if(m_objWndSpellCheck != null)
	{
		m_objWndSpellCheck.document.getElementById("hdnOriginalText").value = document.frmWizardData.txtMemo.value; //StripHTMLTags(FCK.GetXHTML(true));
		m_objWndSpellCheck.document.getElementById("hdnOriginalHTMLText").value = document.frmWizardData.txtMemo.value; //FCK.GetXHTML(true);
	}
}

function OnSpellCheckComplete()
{
	if(m_objWndSpellCheck != null)
	{	
		document.frmWizardData.txtMemo.value = m_objWndSpellCheck.document.getElementById("hdnOriginalText").value;
		//FCK.EditorDocument.activeElement.innerHTML = m_objWndSpellCheck.document.all("hdnOriginalText").value;
	}
}
//Tr : - 1547 Nitesh Ends
function ConfirmSave()
{
	//alert("confirmsave");
	var ret = false;
	if(m_DataChanged){
		//alert("datachanged");
		ret = confirm('Data has changed. Do you want to save changes?');
	}
	//alert("confirmed");
	return ret;
}
function closeNewWin()
{
	if(newwin!=null)
	{
		newwin.close();
		newwin=null;
	}
}
//pmahli 11/30/2007 MITS 10811 - Start
function OpenProgressWindow()
{
     var wnd=RMX.window.open('csc-Theme/riskmaster/common/html/progress.html','progressWnd',
			'width=400,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-400)/2);
	self.parent.wndProgress=wnd;
}
//pmahli 11/30/2007 - End
function CloseProgressWindow()
{
	if(self.parent!=null)
	{
		if(haveProperty(self.parent,"wndProgress") && self.parent.wndProgress!=null)
		{
			self.parent.wndProgress.close();
			self.parent.wndProgress=null;
		}
	}
}
function OFACCheck(sLastName , sFirstName)
{
	if (!sLastName) 
	{
		var sLastName;
		if(document.forms[0].txtLastName!=null) sLastName = document.forms[0].txtLastName.value;
		else sLastName = "";
	}
	if (!sFirstName)
	{	
		if(document.forms[0].txtFirstName!=null) sFirstName = document.forms[0].txtFirstName.value;
		else sFirstName = "";
	}
			
	//alert(sLastName + ' ' + sFirstName);
	
	if(sFirstName == "")
		sFirstName = " "; //Patriot Protector will complain if this is completely blank.
	if(sLastName == "")
		alert("A last name is required in order to perform an OFAC Check.  \nPlease enter a payee last name and try again.");
	else
	{
		this.document.callback="OnOFACCHECKCompleted" ;
		
		if(wnd==null)
		{
			var wnd=RMX.window.open("home?pg=riskmaster/Funds/patriotprotector&amp;lastname=" + sLastName
						+ "&firstname=" + sFirstName,"PatriotProtector",
			'width=600,height=600'+',top='+(screen.availHeight-600)/2+',left='+(screen.availWidth-600)/2+',resizable=yes,scrollbars=yes');
		}
			
	}
	return false;
}
function pageLoaded(errortext,showerrors,showwarnings,showconfirmations)
{	
	self.focus();
	//Set focus to first field
	 var i;
	 //setDataChanged(false); 
	 document.forms[0].method="post";
	 document.forms[0].AddEditPaymentFlag.value="false";
	 document.forms[0].NewPaymentAdded.value = "false";
	 <!--MITS 12836 : Umesh-->
	 document.forms[0].PaymentAddedSuccessfully.value = "false";
	 document.forms[0].ExistingPaymentEdited.value = "false";
	 if(document.forms[0].deletetarget.value!='false') setDataChanged(true);
	 document.forms[0].deletetarget.value="false";
	 document.forms[0].recordsaved.value="false";
	 document.forms[0].BrsCancel.value = "false";
	 document.forms[0].FormPostThroughBrs.value = "false";
	 m_AvoidDataChangeConfirmation = false; 
	// alert(document.forms[0].length);
	
	 for (i=0;i<document.forms[0].length;i++)			
		{	 				
 
          if((document.forms[0][i].type=="text") || (document.forms[0][i].type=="select-one") || (document.forms[0][i].type=="textarea"))
			{
				if(document.forms[0][i].disabled==false)
				{
					try
					{
						document.forms[0][i].focus();
					}
					catch(e){}
					break;
				}
			}
		}	
	//	 alert("in page Loaded after setfocus");
	document.codeSelected=codeSelected;
	//document.SetClaimPolicy=SetClaimPolicy;
	document.onCodeClose=onCodeClose;
	document.dateSelected=dateSelected;
	document.entitySelected=entitySelected;
	document.ReserveInfoSelected=ReserveInfoSelected;
	//document.policySelected=policySelected;
	document.formHandler=formHandler;
	document.OnFormSubmit=OnFormSubmit;
	document.Navigate=Navigate;
	document.FilteredDiary=FilteredDiary;
	document.doSearch=doSearch;
	document.doLookup=doLookup;
	document.showSupp=showSupp;
	self.onfocus=onWindowFocus;
	document.ClaimantChoosen=ClaimantChoosen;
	document.ConfirmSave=ConfirmSave;
	document.goToDoc=goToDoc;
	document.ViewSplit=ViewSplit;
	document.ViewBRSSplit=ViewBRSSplit;
	document.SetApplicationBusy=SetApplicationBusy;
	if(self.parent!=null)
		if(haveProperty(self.parent,"wndProgress") && self.parent.wndProgress!=null)
		{
			try
			{
				self.parent.wndProgress.close();
				self.parent.wndProgress=null;
			}
			catch(e){}
		}
	
	if(IE4)
	{
		if(document.getElementById('toolbardrift')!=null)
		{
			document.getElementById('toolbardrift').style.visibility = "visible";
			document.getElementById('toolbardrift').style.position="absolute";
			startDrift(document.getElementById('toolbardrift'));
			document.getElementById('formtitle').style.marginTop="48px";
		}
	}

	if(eval(document.forms[0].SysFormIdName)!=null)
		m_FormID=document.forms[0].SysFormIdName.value;	
		
	self.onunload=pageUnload;
	
	//Raman MITS 10333 
	// In case of a closed claim system pops up a confirmation before saving a transaction
	// Efforts to void such a transaction fails as void flag is not retained .. 
	// Void flag cannot be copied over from previous instance as this would change the UI of the page

	if(document.forms[0].Void != null)
	{
		if(document.forms[0].VoidTransactionClosedClaim.value == 'true')
			document.forms[0].Void.checked = true;
	}
	
	//Raman 3/8/2006 If save was succesful and we need to redirect to reserves..(save must have been triggered
	//a positive save confirmation after "Back to Financials" was pressed)
	
	if(document.forms[0].redirecttoreserves.value == "true")
	{
		if((showerrors == "false") && (showconfirmations == "false") && (showwarnings == "false"))
		//save is succesful as no error,warning or confirmation has come from server side validations
		//hence redirect to reserves..
		{
			var sClaimId=document.forms[0].txtClaimNumber_cid.value;
			var sClaimant=document.forms[0].claimanteid.value;
			var sClaimNumber=document.forms[0].txtClaimNumber.value;
			var sUnitID = document.forms[0].unitid.value;
			
			//if detail level tracking is off for GC and VA then claimant information should not be passed to reserve listing screen
			var lob = GetLOB();
			switch(lob)
	{
		case '241': if(document.forms[0].restracking.value == 1)
					{
						//03/09/2006 Raman Bhatia: TR 2469..In case record is readonly then Detail level tracking would not be combobox
						if(document.getElementById("cboclaimant").options)
						{
							selIndex = document.getElementById("cboclaimant").selectedIndex;
							sClaimant = document.getElementById("cboclaimant").options[selIndex].value.substring(1);
						}
						else
							sClaimant = document.getElementById("cboclaimant").value;
					}
					else
						sClaimant = "";
					break;
		
		case '242': if(document.forms[0].restracking.value == 0)
						sClaimant = "";
					break;
		
		case '243': //03/09/2006 Raman Bhatia: TR 2469..In case record is readonly then Detail level tracking would not be combobox
					if(document.getElementById("cboclaimant").options)
					{
						selIndex = document.getElementById("cboclaimant").selectedIndex;
						sClaimant = document.getElementById("cboclaimant").options[selIndex].value.substring(1);
					}
					else
						sClaimant = document.getElementById("cboclaimant").value;
					break;
					
		case '844': //03/09/2006 Raman Bhatia: TR 2469..In case record is readonly then Detail level tracking would not be combobox
					if(document.getElementById("cboclaimant").options)
					{
						selIndex = document.getElementById("cboclaimant").selectedIndex;
						sClaimant = document.getElementById("cboclaimant").options[selIndex].value.substring(1);
					}
					else
						sClaimant = document.getElementById("cboclaimant").value;
					break;
		
		default:    break;
	}
			
			if (sUnitID!="0")
				RMX.urlNavigate('home?pg=riskmaster/Reserves/ReserveListing&ClaimId='+sClaimId+'&UnitID='+sUnitID+'&ClaimNumber='+sClaimNumber);
			else
				RMX.urlNavigate('home?pg=riskmaster/Reserves/ReserveListing&ClaimId='+sClaimId+'&ClaimantId='+sClaimant+'&ClaimNumber='+sClaimNumber);
			return false;
		}
		document.forms[0].redirecttoreserves.value = "";
	}
	
	if(document.forms[0].redirecttopayhist.value == "true")
	{
	    if((showerrors == "false") && (showconfirmations == "false") && (showwarnings == "false"))
	    {
	        document.forms[0].ouraction.value = "gotoPaymentHistory";
	        document.forms[0].submit();     
	    }
	}
		
	//tkr 1/2003 check if user action was interrupted by "do you want to save your changes" prompt
	//sysCmdQueue, if it exists, is the interrupted function call
	var obj=eval("document.forms[0].sysCmdQueue");
	if(obj!=null){
		var s = new String(document.forms[0].sysCmdQueue.value);
		if(s.length>0){
			s='document.'+s;
			document.forms[0].sysCmdConfirmSave.value=0;
			document.forms[0].sysCmdQueue.value='';
			//self.setTimeout(s,200);
			eval(s);
			window.status="Finished";
			return true;
		}
	}
	UpdateAccountId();
	
	if(document.forms[0].datachanged.value=="1")
		m_DataChanged=true;
	document.forms[0].datachanged.value="0";

	//TR#1302,1303,1304-1/18/06 Message box for insufficient reserves when the "Do not allow negavite reserve adj' in Utilities->Lob params checked
	if((errortext.substring(0,5)=='There')&&(showerrors == 'true'))
	{
		alert(errortext);
		window.status="Finished";
		return true;
	}
	//ravi---02/06/04
	//will display either error window or confirmation window at a time
	//easy to handle...bcoz no use of confirmation when error occured..make sense???
	//Insufficient Reserve Amount
	if((errortext!='')&&(showerrors == 'true')&&(errortext == 'Insufficient Reserves Available'))
	{
		
		var sLink;
		
		sLink='home?pg=riskmaster/Funds/InsufficientReserves';
		if(newwin!=null)
		{
			newwin.close();
			newwin=null;
		}
		
		document.forms[0].showerrors.value = false;
		//Raman Bhatia 12/20/2005 Reserve Type missing alert moved to the split screen
		/*
		if(document.forms[0].transreserveslinked.value==0)
		{
			alert("Transaction Type must be linked to a Reserve Type,Cannot Save!");
			return false;
		}
		*/
		
		this.document.callback="OnInsufficientReservesDisplayed" ;

		if(wnd == null)
		{
			var wnd=RMX.window.open(sLink ,'payeeExp',
			'width=500,height=300'+',top='+(screen.availHeight-290)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
				
		}
		window.status="Finished";
		return false;
	}
	else
	{		
		window.setTimeout("DisplayConfirmation()",100);
		window.status="Finished";
	}
	window.status="Finished";
	
	/* Arnab: MITS-10314 - No need to set chkSettlement.value on CheckChange, xforms:value serves the same	
	objCtrl = document.forms[0].chkSettlement;
	if(objCtrl)
	{
		if(objCtrl.checked)
			objCtrl.value = '1';
		else
			objCtrl.value = '0';
	}
	
	MITS-10314 - End (Commented lines)
	*/
		
	//Geeta 13/10/2006: function call added for fixing 8232 for Clear check Box on Funds screen	
	CheckForCheckStatus();
	//Shruti for 11889
	//pmittal5 MITS 12746 08/11/08  -Locking supplementals when Claim is closed and Check is cleared.
	//if(document.forms[0].hdnVoid != null && document.forms[0].hdnVoid.value == "true")
	if((document.forms[0].hdnVoid != null && document.forms[0].hdnVoid.value == "true") || (document.forms[0].hdnCleared != null && document.forms[0].hdnCleared.value == "true" && document.forms[0].hdnIsClaimOpen != null && document.forms[0].hdnIsClaimOpen.value == "False" ))
    {
        LockSuppWhenVoid();
    }
	return true;
}
/* Arnab: MITS-10314 - No need to set chkSettlement.value on CheckChange, xforms:value serves the same	
//Ratheen(#1788)
function IfIncludePayee(objCtrl)
{
	if(objCtrl.checked)
		objCtrl.value = '1';
	else
		objCtrl.value = '0';	
}
MITS-10314 - End (Commented lines)
*/
function numLostFocus(objCtrl)
{
	if(objCtrl.value.length==0)
			return false;
	if(isNaN(parseFloat(objCtrl.value)))
		objCtrl.value="";
	else if(parseInt(objCtrl.value,10)<0)
		objCtrl.value=""; 
	else
		objCtrl.value=parseInt(objCtrl.value,10);
	return true;
}

function pageUnload()
{
	if(m_codeWindow!=null)
		m_codeWindow.close();
	m_codeWindow=null;
	return true;
}

function haveProperty(obj, sPropName)
{
	
	for(p in obj)
	{
		if(p==sPropName)
			return true;
	}
	return false;
}

function Navigate(direction)
{
	if(m_bSubmitted)
		return false;
	m_bSubmitted=true;
	document.forms[0].sysCmd.value=direction;
	if(direction=='')  //tkr need single quotes for sysCmdQueue value
		direction="''";
	if(OnFormSubmit("Navigate("+direction+");"))
		self.setTimeout('document.forms[0].submit();',100);
	else
		m_bSubmitted = false;
	return true;
}

function urlNavigate(s){return RMX.urlNavigate(s);}


function codeSelected(sCodeText,lCodeId,sParentText)
{
	//raman.. hack to correctly update reservetypecode
	if(m_sFieldName=='transtypecode')
	{
		var claimid = 0;
		if(window.opener.document.forms[0].txtClaimNumber_cid)
			claimid = window.opener.document.forms[0].txtClaimNumber_cid.value;
			
		//Raman - TR 2609 claimant id passed to add transaction pop-up should be decided by Detail Level Tracking field..
		//this will affect which reserve to update and hence claimanteid should have nothing to do with this ..
		//commenting following code and using detail level tracking field to pass info to add transaction pop-up
		var claimanteid = 0;
		var unitid = 0;
		/*
		if(window.opener.document.forms[0].claimanteid)
			claimanteid = window.opener.document.forms[0].claimanteid.value;
			
		
		if(window.opener.document.forms[0].unitid)
			unitid = window.opener.document.forms[0].unitid.value;

		*/
		
		//if detail level tracking is off for GC and VA then claimant information should not be passed to add transaction screen
			var lob = GetLOB();
			switch(lob)
			{
				case '241': if(window.opener.document.forms[0].restracking.value == 1)
							{
								if(window.opener.document.getElementById("cboclaimant").options)
								{
									selIndex = window.opener.document.getElementById("cboclaimant").selectedIndex;
									claimanteid = window.opener.document.getElementById("cboclaimant").options[selIndex].value.substring(1);
								}
								else
									claimanteid = window.opener.document.getElementById("cboclaimant").value;
							}
							else
								claimanteid = 0;
							break;
				
				case '242': if(window.opener.document.forms[0].restracking.value == 1)
							{
								if(window.opener.document.getElementById("claimantunitid").options)
								{
									selIndex = window.opener.document.getElementById("claimantunitid").selectedIndex;
									if(window.opener.document.getElementById("claimantunitid").options[selIndex].value.substring(0,1)=='C')
									{
										claimanteid = window.opener.document.getElementById("claimantunitid").options[selIndex].value.substring(1);
										unitid = 0;
									}
									else
									{
										unitid = window.opener.document.getElementById("claimantunitid").options[selIndex].value.substring(1);
										claimanteid = 0;
									}
								}
								else if(window.opener.document.getElementById("claimantunitid").value.substring(0,1)=='C')
								{
									claimanteid = window.opener.document.getElementById("claimantunitid").options[selIndex].value.substring(1);
									unitid = 0;
								}
								else
								{
									unitid = window.opener.document.getElementById("claimantunitid").options[selIndex].value.substring(1);
									claimanteid = 0;
								}
									
							}
							else
							{
								claimanteid = 0;
								unitid = 0;
							}
							break;
				
				case '243': if(window.opener.document.getElementById("cboclaimant").options)
							{
								selIndex = window.opener.document.getElementById("cboclaimant").selectedIndex;
								claimanteid = window.opener.document.getElementById("cboclaimant").options[selIndex].value.substring(1);
							}
							else
								claimanteid = window.opener.document.getElementById("cboclaimant").value;
							break;
							
				case '844': if(window.opener.document.getElementById("cboclaimant").options)
							{
								selIndex = window.opener.document.getElementById("cboclaimant").selectedIndex;
								claimanteid = window.opener.document.getElementById("cboclaimant").options[selIndex].value.substring(1);
							}
							else
								claimanteid = window.opener.document.getElementById("cboclaimant").value;
							break;
				
				default:    break;
			}
	
		
		if(claimid!=0)
		{
			self.lookupCallback="ReserveInfoSelected";
			self.setTimeout("m_Wnd=window.open('home?pg=riskmaster/Funds/ReservesInfoRender&ClaimId="+claimid+"&TransTypeCode="+lCodeId+"&ClaimantEid="+claimanteid+"&UnitId="+unitid+
					"','progressWnd','width=400,height=150,top="+((screen.availHeight-150)/2)+
					",left="+((screen.availWidth-400)/2)+"');",100);
					// self.lookupCallback="entitySelected";			
		}				
	}
	if( m_sFieldName == 'TransTypeCode' )
	{
		var claimid = 0;
		if(window.opener.document.forms[0].claimid)
			claimid = window.opener.document.forms[0].claimid.value;
			
		var claimanteid = 0;
		if(window.opener.document.forms[0].clm_entityid)
			claimanteid = window.opener.document.forms[0].clm_entityid.value;
		
		var unitid = 0;
		if(window.opener.document.forms[0].unitid)
			unitid = window.opener.document.forms[0].unitid.value;
		
		if(claimid!=0)
		{
			self.lookupCallback="ReserveInfoSelected";
			self.setTimeout("m_Wnd=window.open('home?pg=riskmaster/Funds/ReservesInfoRender&ClaimId="+claimid+"&TransTypeCode="+lCodeId+"&ClaimantEid="+claimanteid+"&UnitId="+unitid+
					"','progressWnd','width=400,height=150,top="+((screen.availHeight-150)/2)+
					",left="+((screen.availWidth-400)/2)+"');",100);
					// self.lookupCallback="entitySelected";			
		}	
	}
	var objCtrl=null;
	var objParCtrl = null;
	
	if(m_codeWindow!=null)
		m_codeWindow.close();
		
 			
	  
	objCtrl=eval('document.forms[0].'+m_sFieldName);
	
	
	m_sParentName = "";
	var oldvalue=objCtrl.value;
	
	if(objCtrl!=null)
	{
		if(objCtrl.type=="textarea")
		{
			objCtrl.value=objCtrl.value + sCodeText + "\n";
		}
		else if(objCtrl.type=="select-one" || objCtrl.type=="select-multiple")
		{
			var bAdd=true;
			for(var i=0;i<objCtrl.length;i++)
			{
				if(objCtrl.options[i].value==lCodeId)
					bAdd=false;
			}
			if(bAdd)
			{
				var objOption = new Option(sCodeText, lCodeId, false, false);
				if ( objCtrl.options[0]!=null && objCtrl.options[0].value == 0 )
				{
					objCtrl.options[0]=null;
				}
				objCtrl.options[objCtrl.length] = objOption;
				objCtrl=null;
				objCtrl=eval("document.forms[0]." + m_sFieldName+"_lst");
				if(objCtrl!=null)
				{
					if(objCtrl.value!="" && objCtrl.value.substring(objCtrl.value.length,1)!=" ")
						objCtrl.value=objCtrl.value+" ";
					objCtrl.value=objCtrl.value+lCodeId;
				}
			}
		}
		else
		{
			objCtrl.value=sCodeText;
			objCtrl=eval('document.forms[0].'+m_sFieldName+"_cid");
			if(objCtrl!=null)
				objCtrl.value=lCodeId;
			else if(m_sFieldName=="txtLastName")
				{
					document.forms[0].payeeeid.value=lCodeId;		
					var sType="";
					self.setTimeout("m_Wnd=window.open('home?pg=riskmaster/Funds/SearchRender&entityid="+lCodeId+"&type="+sType+"&lookuptype="+m_LookupType+
					"','progressWnd','width=400,height=150,top="+((screen.availHeight-150)/2)+
					",left="+((screen.availWidth-400)/2)+"');",100);
					self.lookupCallback="entitySelected";
				}
		}
		setDataChanged(true);
		
	}
	
	m_sFieldName="";
	m_codeWindow=null;
	return true;
}


function onCodeClose()
{
	m_codeWindow=null;
	return true;
}

function deleteSelCode(sFieldName)
{
	var objCtrl=null;
	if(m_codeWindow!=null)
		m_codeWindow.close();
	objCtrl=eval('document.forms[0].'+sFieldName);
	if(objCtrl==null)
		return false;
	if(objCtrl.selectedIndex<0)
		return false;
	
	var bRepeat=true;
	while(bRepeat)
	{
		bRepeat=false;
		for(var i=0;i<objCtrl.length;i++)
		{
			// remove selected elements
			if(objCtrl.options[i].selected)
			{
				objCtrl.options[i]=null;
				bRepeat=true;
				break;
			}
		}
	}
	// Now create ids list
	var sId="";
	for(var i=0;i<objCtrl.length;i++)
	{
			if(sId!="")
				sId=sId+" ";
			sId=sId+objCtrl.options[i].value;
	}
	objCtrl=null;
	objCtrl=eval("document.forms[0]." + sFieldName+"_lst");
	if(objCtrl!=null)
		objCtrl.value=sId;
	
	setDataChanged(true);

	return true;
}

function setDataChanged(b)
{
	m_DataChanged=b;
	return b;
}
function codeLostFocus(sCtrlName)
{
	var objFormElem=eval('document.forms[0].'+sCtrlName);
	if(objFormElem.value=="")
	{
		objFormElem=null;
		objFormElem=eval('document.forms[0].'+sCtrlName+"_cid");
		if(objFormElem!=null)
			objFormElem.value="0";
		else if(sCtrlName=="txtLastName")
			ClearPayee();
	}
	else
	{
	    //pmittal5  MITS:12149 05/29/08 
	    if(m_LookupTextChanged)
	    {
	        var objButton=eval('document.forms[0].'+sCtrlName+'btn');
	        if(objButton!=null)
	        {
		        objButton.click();
		    }
	    }
	    else
	    {
	    //End - pmittal5 
		    objFormElem=null;
		    objFormElem=eval('document.forms[0].'+sCtrlName+"_cid");
		    if(objFormElem!=null && (objFormElem.value=="0" || objFormElem.value==""))
		    {
			    var objFormElem=eval('document.forms[0].'+sCtrlName);
			    objFormElem.value="";
		    }
		}
	}
	
	return true;
}

//tkr 1/2003 add NS 4 & 7 compatibility
function eatKeystrokes(evt) {
	//alert("begin");
	if(IE4) {
		if(window.event.keyCode!=9) {
			window.event.returnValue=false;
			return true;
		}
	}
	else {
		evt = (evt) ? evt : ((event) ? event : null);
		if (evt) {
		    var charCode = (evt.charCode || evt.charCode == 0) ? evt.charCode : 
		                   ((evt.keyCode) ? evt.keyCode : evt.which);
		    if (charCode!=9 && charCode!=0) {
		        if (evt.returnValue) {
		            evt.returnValue = false;
		        } else if (evt.preventDefault) {
		            evt.preventDefault();
		        } else {
		            return false;
		        }
		    }
		}
	}
	//alert("done");
}

//function dateLostFocus(sCtrlName)
function dateLostFocus(sCtrlId)
{
	//alert(sCtrlId);
	var sDateSeparator;
	var iDayPos=0, iMonthPos=0;
	var d=new Date(1999,11,22);
	var s=d.toLocaleString();
	var sRet="";
	//var objFormElem=eval('document.forms[0].'+sCtrlName);
	var objFormElem = eval( "document.getElementById('" + sCtrlId + "')" );
	var sDate=new String(objFormElem.value);
	var iMonth=0, iDay=0, iYear=0;
	var monthDays = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	if(sDate=="")
		return "";
	if(sDate.indexOf('/') == -1)
	{
		sDate = sDate.substr(0 , 2) + '/' + sDate.substr(2 , 2) + '/' + sDate.substr(4 , 4);
	}
	iDayPos=s.indexOf("22");
	iMonthPos=s.indexOf("11");
	//if(IE4)
	//	sDateSeparator=s.charAt(iDayPos+2);
	//else
		sDateSeparator="/";
	var sArr=sDate.split(sDateSeparator);
	if(sArr.length==3)
	{
		sArr[0]=new String(parseInt(sArr[0],10));
		sArr[1]=new String(parseInt(sArr[1],10));
		sArr[2]=new String(parseInt(sArr[2],10));
		
		//Convert potential 2-digit year to 4-digit year
		sArr[2] = Get4DigitYear(sArr[2]);		
		
		// Classic leap year calculation
		if (((parseInt(sArr[2],10) % 4 == 0) && (parseInt(sArr[2],10) % 100 != 0)) || (parseInt(sArr[2],10) % 400 == 0))
			monthDays[1] = 29;
		if(iDayPos<iMonthPos)
		{
			// Date should be as dd/mm/yyyy
			if(parseInt(sArr[1],10)<1 || parseInt(sArr[1],10)>12 ||  parseInt(sArr[0],10)<0 || parseInt(sArr[0],10)>monthDays[parseInt(sArr[1],10)-1])
				objFormElem.value="";
		}
		else
		{
			// Date is something like mm/dd/yyyy
			if(parseInt(sArr[0],10)<1 || parseInt(sArr[0],10)>12 ||  parseInt(sArr[1],10)<0 || parseInt(sArr[1],10)>monthDays[parseInt(sArr[0],10)-1])
				objFormElem.value="";
		}
		// Check the year
		if(parseInt(sArr[2],10)<10 || (sArr[2].length!=4 && sArr[2].length!=2))
			objFormElem.value="";
		// If date has been accepted
		if(objFormElem.value!="")
		{
			// Format the date
			if(sArr[0].length==1)
				sArr[0]="0" + sArr[0];
			if(sArr[1].length==1)
				sArr[1]="0" + sArr[1];
			if(sArr[2].length==2)
				sArr[2]="19"+sArr[2];
			if(iDayPos<iMonthPos)
				objFormElem.value=formatDate1(sArr[2] + sArr[1] + sArr[0]);
			else
				objFormElem.value=formatDate1(sArr[2] + sArr[0] + sArr[1]);
		}
	}
	else
		objFormElem.value="";
	return true;
}

//Convert 2-digit year to 4-digit year
function Get4DigitYear(sInputYear)
{
	var sGuessedYear = sInputYear;
	var dCurrentDate = new Date();
	var iCurrentYear = dCurrentDate.getFullYear();
	var sCurrentYear = new String(iCurrentYear);

	//01/05/2010 Raman: Logic below failed for 2010 onwards
	//converting inputyear to 2 characters

	if (sInputYear.length == 1) {
	    sInputYear = "0" + sInputYear;
	}
	
	//If the input year is not 4-digit year, get missing digit form current year
//	if (sInputYear.length < 4)  Commented by csingh7 : MITS 14181
    	if (sInputYear.length < 3)  // Added by csingh7 : MITS 14181
	{
		sGuessedYear = sCurrentYear.substr(0, 4 - sInputYear.length) + sInputYear;
		
		//If the guessed year is 20 years in the future, assume it's in the last centure
		var iGuessedYear = parseInt(sGuessedYear);
		if( iGuessedYear - iCurrentYear > 20 )
		{
			iGuessedYear = iGuessedYear - 100;
			sGuessedYear = new String(iGuessedYear);
		}
	}
		
	return sGuessedYear;
}

function selectDate(sFieldName)
{
	//alert("selectDate");
	if(m_codeWindow!=null)
		m_codeWindow.close();
		
	m_sFieldName=sFieldName;
	
	m_codeWindow=RMX.window.open('csc-Theme/riskmaster/common/html/calendar.html','codeWnd',
		'width=230,height=230'+',top='+(screen.availHeight-230)/2+',left='+(screen.availWidth-230)/2+',resizable=yes,scrollbars=no');
	//alert("calendaropen");
	return false;
}

function dateSelected(sDay, sMonth, sYear)
{
	//alert("dateselected");
	var objCtrl=null;
	if(m_codeWindow!=null)
		m_codeWindow.close();
//	objCtrl=eval('document.forms[0].'+m_sFieldName);
    objCtrl=document.getElementById(m_sFieldName);
	if(objCtrl!=null)
	{
		sDay=new String(sDay);
		if(sDay.length==1)
			sDay="0"+sDay;
		sMonth=new String(sMonth);
		if(sMonth.length==1)
			sMonth="0"+sMonth;
		sYear=new String(sYear);
			
		objCtrl.value=formatDate1(sYear+sMonth+sDay);
	}
	m_sFieldName="";
	m_codeWindow=null;
	setDataChanged(true);
	return true;
}

function formatDate1(sParamDate)
{
	//alert("nahin aayega");
	var sDateSeparator;
	var iDayPos=0, iMonthPos=0;
	var d=new Date(1999,11,22);
	var s=d.toLocaleString();
	var sRet="";
	var sDate=new String(sParamDate);
	if(sDate=="")
		return "";
	iDayPos=s.indexOf("22");
	iMonthPos=s.indexOf("11");
	//if(IE4)
	//	sDateSeparator=s.charAt(iDayPos+2);
	//else
		sDateSeparator="/";
	if(iDayPos<iMonthPos)
		sRet=sDate.substr(6,2) + sDateSeparator + sDate.substr(4,2) + sDateSeparator + sDate.substr(0,4);
	else
		sRet=sDate.substr(4,2) + sDateSeparator + sDate.substr(6,2) + sDateSeparator + sDate.substr(0,4);
	return sRet;
}

function ssnLostFocus(objCtrl)
{
	// ###-##-####
	if(objCtrl.value.length==0)
			return false;
	var sValue=new String(objCtrl.value);
	sValue=stripNonDigits(sValue);
	if(sValue.length!=9)
	{
		alert("Please enter valid SSN number.");
		objCtrl.value="";
		objCtrl.focus();
		return false;
	}
	objCtrl.value=sValue.substr(0,3)+"-"+sValue.substr(3,2)+"-"+sValue.substr(5,4);
	return true;
}

function ClearPayee()
{
	//alert("enteredClearPayee");
	document.forms[0].claimanteid.value="";
	document.forms[0].txtLastName.value="";
	document.forms[0].txtFirstName.value="";
	document.forms[0].txtTaxId.value="";
	document.forms[0].txtTaxIdPrev.value="";
	document.forms[0].txtAddr1.value="";
	document.forms[0].txtAddr2.value="";
	document.forms[0].txtAddr3.value="";
	document.forms[0].txtAddr4.value="";
	document.forms[0].txtCity.value="";
	document.forms[0].txtState.value="";
	document.forms[0].txtState_cid.value="";
	document.forms[0].txtZip.value="";
	document.forms[0].payeeeid.value="";
	document.forms[0].txtTaxId.disabled = false;
}

function PayeeChanged()
{
	setDataChanged(true);
	var sVal=document.forms[0].cboPayeeType.options[document.forms[0].cboPayeeType.selectedIndex].value;
	var bClear=false;

	if(sVal=="")
		bClear=true;
	if(isNaN(parseInt(sVal,10)))
		bClear=true;
	if(parseInt(sVal,10)<=0)
		bClear=true;
	if(isNaN(parseInt(document.frmClaimant.ClaimantCount.value,10))==true)
	{
		ClearPayee();
		return false;
	}
	if(bClear || parseInt(document.frmClaimant.ClaimantCount.value,10)==0)
	{
		ClearPayee();
		return false;
	}
	
	if(document.forms[0].cboPayeeType.selectedIndex>1)
	{
		ClearPayee();
		return false;
	}
	
	
	if(parseInt(document.frmClaimant.ClaimantCount.value,10)==1)
	{
		document.forms[0].payeeeid.value=document.frmClaimant.eid_1.value;
		document.forms[0].claimanteid.value=document.frmClaimant.eid_1.value;
		document.forms[0].txtLastName.value=document.frmClaimant.lastname_1.value;
		document.forms[0].txtFirstName.value=document.frmClaimant.firstname_1.value;
		document.forms[0].txtTaxId.value=document.frmClaimant.taxid_1.value;
		document.forms[0].txtTaxIdPrev.value=document.frmClaimant.taxid_1.value;
		document.forms[0].txtAddr1.value=document.frmClaimant.addr1_1.value;
		document.forms[0].txtAddr2.value=document.frmClaimant.addr2_1.value;
		document.forms[0].txtAddr3.value=document.frmClaimant.addr3_1.value;
		document.forms[0].txtAddr4.value=document.frmClaimant.addr4_1.value;
		document.forms[0].txtCity.value=document.frmClaimant.city_1.value;
		document.forms[0].txtState.value=document.frmClaimant.shortcode_1.value + document.frmClaimant.desc_1.value;
		document.forms[0].txtState_cid.value=document.frmClaimant.stateid_1.value;
		document.forms[0].txtZip.value=document.frmClaimant.zipcode_1.value;
		document.forms[0].txtTaxId.disabled = true;
		return true;
	}
	
	
    //Changed for MITS 9503 : Start    	
	var wnd=RMX.window.open('','progressWnd',
			'width=350,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-350)/2+',resizable=0,scrollbars=1');
	//Changed for MITS 9503 : End		
	wnd.document.writeln(parent.CommonValidations.PayeeChangedHtml1);
	
		//^Rajeev 01/21/2003 -- CSS Consolidation
	/*wnd.document.writeln("<link rel=\"stylesheet\" href=\"forms.css\" type=\"text/css\" />");
	wnd.document.writeln("</head><body>");
	wnd.document.writeln("<table border=\"0\" width=\"95%\">");
	wnd.document.writeln("<tr><td colspan=\"2\" align=\"center\" class=\"ctrlgroup\">Please choose Claimant</td></tr>");
	var c=parseInt(document.frmClaimant.ClaimantCount.value,10);
	for(var i=0;i<c;i++)
	{
		wnd.document.writeln("<tr><td><a href=\"#\" onClick=\"window.opener.document.ClaimantChoosen("+i+")\">"+eval("document.frmClaimant.lastname_"+i).value+"</a>");
		wnd.document.writeln(", "+eval("document.frmClaimant.firstname_"+i).value+"</td>");
		wnd.document.writeln("<td>"+eval("document.frmClaimant.type_"+i).value+"</td></tr>");
	}*/
	wnd.document.writeln(parent.CommonValidations.PayeeChangedHtml2);
	var c=parseInt(document.frmClaimant.ClaimantCount.value,10);
	for(var i=1;i<=c;i++)
	{
		wnd.document.writeln(parent.CommonValidations.PayeeChangedHtml3 + i + parent.CommonValidations.EmailDocAddToUsersHtml3 + eval(parent.CommonValidations.PayeeChangedHtml4+i).value+parent.CommonValidations.PayeeChangedHtml5);
		wnd.document.writeln(", "+eval(parent.CommonValidations.PayeeChangedHtml6+i).value+parent.CommonValidations.FormJsHtml8);
		wnd.document.writeln(eval(parent.CommonValidations.PayeeChangedHtml7+i).value+parent.CommonValidations.FormJsHtml10);
	}
	//^^Rajeev 01/21/2003 -- CSS Consolidation

	wnd.document.writeln("</table></body></html>");
	wnd.document.close();
	m_codeWindow=wnd;
	
	return true;
}

function ClaimantChoosen(l)
{
	document.forms[0].payeeeid.value=eval("document.frmClaimant.eid_"+l).value;
	document.forms[0].claimanteid.value=eval("document.frmClaimant.eid_"+l).value;
	document.forms[0].txtLastName.value=eval("document.frmClaimant.lastname_"+l).value;
	document.forms[0].txtFirstName.value=eval("document.frmClaimant.firstname_"+l).value;
	document.forms[0].txtTaxId.value=eval("document.frmClaimant.taxid_"+l).value;
	document.forms[0].txtTaxIdPrev.value=eval("document.frmClaimant.taxid_"+l).value;
	document.forms[0].txtAddr1.value=eval("document.frmClaimant.addr1_"+l).value;
	document.forms[0].txtAddr2.value=eval("document.frmClaimant.addr2_"+l).value;
	document.forms[0].txtAddr3.value=eval("document.frmClaimant.addr3_"+l).value;
	document.forms[0].txtAddr4.value=eval("document.frmClaimant.addr4_"+l).value;
	document.forms[0].txtCity.value=eval("document.frmClaimant.city_"+l).value;
	document.forms[0].txtState.value=eval("document.frmClaimant.shortcode_"+l).value + ' ' +eval("document.frmClaimant.desc_"+l).value;
	document.forms[0].txtState_cid.value=eval("document.frmClaimant.stateid_"+l).value;
	document.forms[0].txtZip.value=eval("document.frmClaimant.zipcode_"+l).value;
	document.forms[0].txtTaxId.disabled = true;

	m_DataChanged=true;
	if(m_codeWindow!=null)
	    m_codeWindow.close();
	m_codeWindow=null;
	return true;
}

// Hides code popup window
function onWindowFocus()
{
	if(m_codeWindow!=null)
		m_codeWindow.close();
	m_codeWindow=null;
	return true;
}

function OnLastNameClick()
{
	var sVal=document.forms[0].cboPayeeType.options[document.forms[0].cboPayeeType.selectedIndex].value;

	if(sVal=="")
		return false;

	if(isNaN(parseInt(sVal,10)))
	{
		if(sVal.charAt(0)=='e')
		{
			lookupData("txtLastName","-1",4,"txtLastName",1); //TR: 1547 Nitesh
		}
		else if(sVal.charAt(0)=='o')
		{
			selectCode("orgh","txtLastName");
		}
	}
	else if(parseInt(document.frmClaimant.ClaimantCount.value,10)>1)
	{
		PayeeChanged();
	}
	return true;
}
//TR: 1547 Nitesh changed function signature made similar to form.js:- function lookupData(sTableId, sViewId, sFieldMark, lookupType)
function lookupData(sFieldName,sTableId, sViewId, sFieldMark, lookupType)
{
	if(m_codeWindow!=null)
		m_codeWindow.close();

	if(lookupType>=6 && lookupType<=8)
		self.lookupCallback="numLookupCallback";
	else if (lookupType==10)
		self.lookupCallback="VehicleLookupCallback";
	else if (lookupType==11)
		self.lookupCallback="EventLookupCallback";
	else if (lookupType==12)
		self.lookupCallback="PaymentLookupCallback";
	else
		self.lookupCallback="entitySelected";


	m_sFieldName=sFieldMark;
	//Hijack the claimid into a variable before retrieving the claimnumber... BB
	//if(sFieldMark=="txtClaimNumber") 
	//	m_sFieldName="claimid";
	m_LookupType=lookupType;
	if(sTableId=="0" || sTableId=="")
		sTableId="-1";
	//tkr 6/2003 must pass indicator that this is entity search from payment window
	
	//pmittal5  MITS:12149 05/29/08  -Enabling filtering the search records on the basis of text entered in textbox
	var objField=eval("document.forms[0]."+sFieldName);
	if(m_DataChanged == true && objField.value != "")
	{
		if(lookupType==1 || lookupType==2)
		{
			// Entity
			if(sTableId!="-1" &&  sTableId!="0")
				sTableId="entity."+sTableId;
			else
				sTableId="entity";
		}
		var sCreatable = "";
		if (document.getElementById(objField.id + "_creatable")!=null)
			if (eval("document.forms[0]." + objField.id + "_creatable.value") == "1")
			{
				sCreatable = "&creatable=1"
				objField.Previous = objField.value;
			}		
		m_LookupBusy = true;
		var s = 'home?pg=riskmaster/CodesList/quicklookup&find=' + escape(objField.value)+"&type="+sTableId+sCreatable;
		m_codeWindow=RMX.window.open(s,'codeWnd',
			'width=500,height=290'+',top='+(screen.availHeight-290)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');		
	}
	else
	//End - pmittal5
	{
	//Searchlink TO DO Standard Entity Search
	
	m_codeWindow=RMX.window.open('home?pg=riskmaster/Search/MainPage&viewid='+sViewId+'&formname='+sTableId+'&sys_ex=bFunds&screenflag=2','searchWnd',
		'width=550,height=450'+',top='+(screen.availHeight-450)/2+',left='+(screen.availWidth-550)/2+',resizable=yes,scrollbars=yes');
    }
    
    //pmittal5  MITS:12149 05/29/08 
    m_DataChanged = false;
	return false;
}


//Changed by Gagan for MITS 8515 : Start
function setClaimNumberLostFocus(objCtrl)
{    
    var sValue=new String(objCtrl.value);    
    setDataChanged(true);
    
    if(sValue.length==0)
	    return false;
	    
	if(document.forms[0].TransId.value != 0)
	    return false;
	    
	document.forms[0].txtClaimNumber_cid.value = 0;			
    document.forms[0].txtClaimNumber.value = sValue;
    document.forms[0].ouraction.value='gototransaction';
    document.forms[0].submit();   
}
//Changed by Gagan for MITS 8515 : End



/*
function selectCode(sCodeTable,sFieldName)
{
	if(m_codeWindow!=null)
		m_codeWindow.close();

	var sFormName;
	if (eval('document.forms[0].formname')!=null)
		sFormName=document.forms[0].formname.value;
	else
		sFormName='';

	var sTriggerDate;
	if (eval('document.forms[0].transdate')!=null)
		sTriggerDate=getdbDate(document.forms[0].transdate.value);
	else
		sTriggerDate='';
	
	var sOrgEid;
	if (eval('document.forms[0].sys_departmenteid')!=null)
		sOrgEid=document.forms[0].sys_departmenteid.value;
	else
		sOrgEid='0';
	
	m_sFieldName=sFieldName;
	//added by ravi on 12th feb 03
		if(sCodeTable=='orghall')
			{
			var orglevel;
			//var orgfor=eval('document.forms[0].formname');
			//alert(orgfor);
			orglevel='';
			
			//orgfor=orgfor.value;
			//alert(orglevel);
			if(m_codeWindow!=null)
				m_codeWindow.close();
			m_codeWindow=window.open('org.asp','Table','width=500,height=290'+',top='+(screen.availHeight-290)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
			}
		else{
			m_codeWindow=window.open('getcode.asp?code='+sCodeTable+"&formname="+sFormName+"&triggerdate="+sTriggerDate+"&orgeid="+sOrgEid,'codeWnd',
			'width=500,height=300'+',top='+(screen.availHeight-300)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
			}
	return false;
}

*/
function entitySelected(sEntityId ,sType )
{
	if(sEntityId!="")
	{
		if(sEntityId!="-1")
		{
			if(m_LookupType<=4)
			{
				self.setTimeout("m_Wnd=window.open('home?pg=riskmaster/Search/SearchRender&entityid="+sEntityId+"&type="+sType+"&lookuptype="+m_LookupType+
					"','progressWnd','width=400,height=150,top="+((screen.availHeight-150)/2)+
					",left="+((screen.availWidth-400)/2)+"');",100);
				//m_Wnd=window.open('home?pg=riskmaster/Search/SearchRender&entityid='+sEntityId+'&type='+sType,'progressWnd','width=500,height=450');
				//document.entityLoaded=entitySelected;
				self.lookupCallback="entitySelected";
			}
			return true;
		}
		else
		{
			var obj=null, obj2=null;
			if(m_LookupType==1 || m_LookupType==4)
			{
				obj=eval("m_Wnd.document.frames[1].forms[0].lastname");
				if(obj!=null)
					obj2=eval("document.forms[0]." + m_sFieldName);
				if(obj2!=null)
				{
					obj2.value=obj.value;
					obj2.cancelledvalue=obj.value; 
				}
				
				//RamanBhatia..bug no 616..firstname,address,zip-code,state and tax-id should also be filled
				// Vaibhav: Autocheck Split screens use the trans.js.
				
				obj=eval("m_Wnd.document.frames[1].forms[0].firstname");
				if(obj!=null)
					obj2=eval("document.forms[0].txtFirstName");
				if( obj2== null )
					obj2=eval("document.forms[0].FirstName");
				if(obj2!=null)
				{
					obj2.value=obj.value;
					obj2.cancelledvalue=obj.value; 
				}								
				
				obj=eval("m_Wnd.document.frames[1].forms[0].taxid");
				if(obj!=null)
					obj2=eval("document.forms[0].txtTaxId");
				if( obj2== null )
					obj2=eval("document.forms[0].TaxId");
				if(obj2!=null)
				{
					obj2.value=obj.value;
					obj2.cancelledvalue=obj.value; 
					obj2.disabled = true;
				}
				//Whenever TaxId is filled we have to disable it
				// Moved above and replaced with obj2.disabled.      document.forms[0].txtTaxId.disabled = true;
				obj=eval("m_Wnd.document.frames[1].forms[0].entityid");
				if(obj!=null)
					obj2=eval("document.forms[0].payeeeid");
				if(obj2!=null)
				{
					obj2.value=obj.value;
					obj2.cancelledvalue=obj.value; 
				}
				obj=eval("m_Wnd.document.frames[1].forms[0].addr1");
				if(obj!=null)
					obj2=eval("document.forms[0].txtAddr1");
				if( obj2== null )
					obj2=eval("document.forms[0].Addr1");
				if(obj2!=null)
				{
					obj2.value=obj.value;
					obj2.cancelledvalue=obj.value; 
				}
				obj=eval("m_Wnd.document.frames[1].forms[0].addr2");
				if(obj!=null)
					obj2=eval("document.forms[0].txtAddr2");
				if( obj2== null )
					obj2=eval("document.forms[0].Addr2");
				if(obj2!=null)
				{
					obj2.value=obj.value;
					obj2.cancelledvalue=obj.value; 
				}
				obj=eval("m_Wnd.document.frames[1].forms[0].addr3");
				if(obj!=null)
				    obj2=eval("document.forms[0].txtAddr3");
				if( obj2== null )
				    obj2=eval("document.forms[0].Addr3");
				if(obj2!=null)
				{
				    obj2.value=obj.value;
				    obj2.cancelledvalue=obj.value; 
				}
				obj=eval("m_Wnd.document.frames[1].forms[0].addr4");
				if(obj!=null)
				    obj2=eval("document.forms[0].txtAddr4");
				if( obj2== null )
				    obj2=eval("document.forms[0].Addr4");
				if(obj2!=null)
				{
				    obj2.value=obj.value;
				    obj2.cancelledvalue=obj.value; 
				}
				obj=eval("m_Wnd.document.frames[1].forms[0].city");
				if(obj!=null)
					obj2=eval("document.forms[0].txtCity");
				if( obj2== null )
					obj2=eval("document.forms[0].City");
				if(obj2!=null)
				{
					obj2.value=obj.value;
					obj2.cancelledvalue=obj.value; 
				}
				obj=eval("m_Wnd.document.frames[1].forms[0].zipcode");
				if(obj!=null)
					obj2=eval("document.forms[0].txtZip");
				if( obj2== null )
					obj2=eval("document.forms[0].ZipCode");
				if(obj2!=null)
				{
					obj2.value=obj.value;
					obj2.cancelledvalue=obj.value; 
				}
				obj=eval("m_Wnd.document.frames[1].forms[0].stateid");
				
				if(obj!=null)
					obj2=eval("document.forms[0].txtState");
				if( obj2== null )
					obj2=eval("document.forms[0].StateId");
				if(obj2!=null)
				{
					obj2.value=obj.value;
					obj2.cancelledvalue=obj.value; 
				}
				
				obj=eval("m_Wnd.document.frames[1].forms[0].stateid_cid");
				
				if(obj!=null)
					obj2=eval("document.forms[0].txtState_cid");
				if( obj2== null )
					obj2=eval("document.forms[0].StateId_cid");
				if(obj2!=null)
				{
					obj2.value=obj.value;
					obj2.cancelledvalue=obj.value; 
				}
				/*var objElements=eval("m_Wnd.document.frames[1].forms[0]");
				if (objElements != null)
				{
					// Entity Lookup
					for(i=0;i<objElements.length;i++)
					{
						obj=null;
						obj=eval("document.forms[0]."+m_sFieldName+objElements[i].id);
						if(obj!=null)
						{
							obj.value=objElements[i].value;
							//BSB 01.18.2003
							//All quicklookup fields are input type="text".
							//Quicklookup hack - record this in the cancelledvalue
							// attribute so that we track what to restore in case of 
							// "no matching codes found".
							obj.cancelledvalue=objElements[i].value; 
						}
					}
				}*/
				else
				{
					setDataChanged(false);
					if(m_Wnd != null)
						m_Wnd.close();
					if(m_codeWindow != null)
						m_codeWindow.close();
						
					m_Wnd=null;
					m_codeWindow = null;
					
					return false;
				}
			}
			else if (m_LookupType==2)
			{
				// EID Lookup
				obj=eval("m_Wnd.document.frames[1].forms[0].lastfirstname");
				if(obj!=null)
					obj2=eval("document.forms[0]." + m_sFieldName);
				if(obj2!=null)
				{
					obj2.value=obj.value;
					//BSB 01.18.2003
					//All quicklookup fields are input type="text".
					//Quicklookup hack - record this in the cancelledvalue
					// attribute so that we track what to restore in case of 
					// "no matching codes found".
					obj2.cancelledvalue=obj.value; 
				}
			// Record Id
				obj2=null;
				obj=null;
				obj=eval("m_Wnd.document.frames[1].forms[0].entityid");
				if(obj!=null)
					obj2=eval("document.forms[0]." + m_sFieldName + "_cid");
				if(obj2!=null)
				{
					obj2.value=obj.value;
					obj2.cancelledvalue=obj.value; 
				}
			}
			else if(m_LookupType==3)//No quick lookup for this..
			{
				var sEntityName, sEntityId;
				obj=eval("m_Wnd.document.frames[1].forms[0].lastfirstname");
				if(obj!=null)
					sEntityName=obj.value;
				obj=null;				
				obj=eval("m_Wnd.document.frames[1].forms[0].entityid");
				if(obj!=null)
					sEntityId=obj.value;
				
				obj2=eval("document.forms[0]." + m_sFieldName);
				if(obj2!=null)
				{
					var bAdd=true;
					for(var i=0;i<obj2.length;i++)
					{
							if(obj2.options[i].value==sEntityId)
								bAdd=false;
					}
					if(bAdd)
					{
						var objOption = new Option(sEntityName, sEntityId, false, false);
						// Mihika 10/18/2005 Defect No. 170 Removed the default 'None Selected' option
						if ( obj2.options[0]!=null && obj2.options[0].value == 0 )
						{
							obj2.options[0]=null;
						}
						obj2.options[obj2.length] = objOption;
						obj2=null;
						obj2=eval("document.forms[0]." + m_sFieldName+"_lst");
						if(obj2!=null)
						{
							if(obj2.value!="" && obj2.value.substring(obj2.value.length-1,1)!=" ")
								obj2.value=obj2.value+" ";
							obj2.value=obj2.value+sEntityId;
						}
					}					
				}
										
			}
			
			setDataChanged(true);
		}
	}
	
	if (m_Wnd!=null)
		m_Wnd.close();
	m_Wnd=null;
	m_sFieldName="";
	if(m_codeWindow!=null)
		m_codeWindow.close();
	m_codeWindow=null;
	m_AllowRestore= true;
	return true;
}

function ReserveInfoSelected(sEntityId ,sType )
{
	setDataChanged(true);
	
	if (m_Wnd!=null)
		m_Wnd.close();
	m_Wnd=null;
	m_sFieldName="";
	if(m_codeWindow!=null)
		m_codeWindow.close();
	m_codeWindow=null;
	m_AllowRestore= true;
	return true;
}

function stripNonDigits(str) {
	return str.replace(/[^0-9]/g,"")
}
//nadim MITS 9181
// MITS 7906 : Anjaneya
function canadaZip(obj)                                                                                                                                              
{                                                                                                                                                                    
	if(obj.value.length==0)                                                                                                                                          
	return false;                                                                                                                                                    
	var str=obj.value;                                                                                                                                               
	                                                                                                                                                                 
  //pmittal5 Mits 13580 01/29/09 - It can be a Blank or No Space at fourth place	                                                                                                                                                                 
	//if(str.replace(/[a-z][0-9][a-z]-[0-9][a-z][0-9]/i,"")=="")  
	if((str.replace(/[a-z][0-9][a-z]\s[0-9][a-z][0-9]/i,"")=="") || (str.replace(/[a-z][0-9][a-z][0-9][a-z][0-9]/i,"")==""))                                                                                                     
		{                                                                                                                                                            
		                                                                                                                                                             
		return true;                                                                                                                                                 
		                                                                                                                                                             
		}                                                                                                                                                            
	else                                                                                                                                                             
		{return false;}                                                                                                                                              
}     

// MITS 7906 : Anjaneya
function zipLostFocus(objCtrl)                                                                                                                                       
{                                                                                                                                                                    
	         
	if(objCtrl.value.length==0)                                                                                                                                      
			return false;                                                                                                                                            
	if(canadaZip(objCtrl)==true)                                                                                                                                     
	{                                                                                                                                                                
		return true;                                                                                                                                                 
	}                                                                                                                                                                
	                                                                                                                                                                 
	var sValue=new String(objCtrl.value);                                                                                                                            
	var sLetter="";                                                                                                                                                  
	if(sValue.substr(0,1)<'0' || sValue.substr(0,1)>'9')                                                                                                             
		sLetter=sValue.substr(0,1);                                                                                                                                  
		                                                                                                                                                             
		                                                                                                                                                             
	sValue=stripNonDigits(sValue);                                                                                                                                   
	//edited by ravi 05/02/03                                                                                                                                        
	if(((sValue.length==5 || sValue.length==9) && sLetter==""))                                                                                
	{                                                                                                                                                                
		if(sValue.length>5)                                                                                                                                          
		{                                                                                                                                                            
			if(sLetter=="")                                                                                                                                          
				sValue=sValue.substr(0,5)+"-"+sValue.substr(5);                                                                                                      
			//else	ravi                                                                                                                                             
			//sValue=sLetter+sValue.substr(0,4)+"-"+sValue.substr(4);                                                                                                
		}                                                                                                                                                            
		else                                                                                                                                                         
		{                                                                                                                                                            
			if(sLetter!="")                                                                                                                                          
				sValue=sLetter+sValue;                                                                                                                               
		}                                                                                                                                                            
		objCtrl.value=sValue;                                                                                                                                        
		return true;                                                                                                                                                 
	}                                                                                                                                                                
	else                                                                                                                                                             
	{                                                                                                                                                                
		alert("Invalid ZIP code entry. Please enter valid ZIP code.");                                                                                               
		objCtrl.value="";                                                                                                                                            
		objCtrl.focus();                                                                                                                                             
		return false;                                                                                                                                                
	}                                                                                                                                                                
} 

/*
function PayeeExp(lEID)
{
	if(m_codeWindow!=null)
		m_codeWindow.close();
	m_codeWindow=null;
	m_codeWindow=window.open('payeeexp.asp?eid='+lEID,'payeeExp',
		'width=350,height=180'+',top='+(screen.availHeight-180)/2+',left='+(screen.availWidth-350)/2+',resizable=yes,scrollbars=no');
}
*/
function PayeeExp()
{
this.document.callback="OnPayeeExperienceDisplayed" ;

	if(wnd == null)
					{
					var wnd=RMX.window.open("home?pg=riskmaster/Funds/PayeeExperience&amp;payeeeid="+ document.getElementById('payeeeid').value + "&iscollection="+ document.getElementById('IsCollection').value ,'payeeExp',
					'width=350,height=180'+',top='+(screen.availHeight-180)/2+',left='+(screen.availWidth-350)/2+',resizable=yes,scrollbars=no');
					
					}
	return false;
}
/*
function PayorExp(lEID)
{
	if(m_codeWindow!=null)
		m_codeWindow.close();
	m_codeWindow=null;
	m_codeWindow=window.open('payeeexp.asp?eid='+lEID+'&colflag=1','payeeExp',
		'width=350,height=180'+',top='+(screen.availHeight-180)/2+',left='+(screen.availWidth-350)/2+',resizable=yes,scrollbars=no');
}
*/

function AddPayment()
{	
	this.document.callback="OnPaymentAdded" ;

	if(wnd == null)
					{
					document.forms[0].NewPaymentAdded.value = "true";
					document.forms[0].ExistingPaymentEdited.value = "false";
					//zalam mits:-11188 03/14/2008			
					//var wnd=RMX.window.open("home?pg=riskmaster/Funds/addPayment&amp;ctlnumber=" + document.forms[0].all("ControlNumber").value + "&amp;lob=" + document.forms[0].all("lineofbusinesscode").value +"&amp;claimid=" + document.forms[0].all("txtClaimNumber_cid").value,"AddPayment",
					var wnd=RMX.window.open("home?pg=riskmaster/Funds/addPayment&amp;ctlnumber=" + document.getElementById("ControlNumber").value + "&amp;lob=" + document.getElementById("lineofbusinesscode").value +"&amp;claimid=" + document.getElementById("txtClaimNumber_cid").value+"&amp;orgEid="+document.getElementById("sys_departmenteid").value,"AddPayment",
			"width=500,height=450,top="+(screen.availHeight-450)/2+",left="+(screen.availWidth-500)/2+",resizable=yes,scrollbars=yes");
					}
	return false;
}

function EditPayment(transtypecode , transtypecode_codeid , splitrowid , split_amount , glaccountcode , glaccountcode_codeid , fromdate , todate , invoicedby , invoicenumber , invoiceamount , invoicedate , reservetypecode , reservetypecode_codeid , reservebalance , PoNumber)
{
	//alert(TransTypeCode + split_amount + glaccountcode + fromdate + todate + invoicedby + invoicenumber + invoiceamount + invoicedate + PoNumber);
	//alert(TransTypeCode);
	this.document.callback="OnPaymentEdited" ;
		
	if(wnd==null)
			{	
						document.forms[0].NewPaymentAdded.value = "false";
						document.forms[0].ExistingPaymentEdited.value = "true";	
						var wnd=RMX.window.open("home?pg=riskmaster/Funds/addPayment&amp;ctlnumber=" + document.getElementById("ControlNumber").value
						+ "&transtypecode_codeid=" + transtypecode_codeid + "&splitrowid=" + splitrowid
						+ "&transtypecode=" + escape(transtypecode) + "&split_amount=" + split_amount
						+ "&glaccountcode=" + escape(glaccountcode) + "&glaccountcode_codeid=" + glaccountcode_codeid +"&fromdate=" + fromdate
						+ "&todate=" + todate + "&invoicedby=" + invoicedby
						+ "&invoicenumber=" + invoicenumber + "&invoiceamount=" + invoiceamount
						+ "&reservetypecode=" + escape(reservetypecode) + "&reservetypecode_codeid=" + reservetypecode_codeid
						+ "&reservebalance=" + reservebalance 
						+ "&invoicedate=" + invoicedate + "&PoNumber=" + PoNumber
						+ "&lob=" + document.getElementById("lineofbusinesscode").value
						+"&amp;claimid=" + document.getElementById("txtClaimNumber_cid").value
						 ,"AddPayment",
			"width=500,height=450,top="+(screen.availHeight-450)/2+",left="+(screen.availWidth-500)/2+",resizable=yes,scrollbars=yes");
					}
	return false;

}

function AddBRSPayment()
{
	var sParam="";
	
	this.document.callback="OnPaymentAdded";	
	//BRS Needs a zip code to function.
	if(document.forms[0].txtZip.value == null || document.forms[0].txtZip.value=="" || document.forms[0].txtZip.value==0)
	{
		alert("Bills Cannot be reviewed until a zip code is set on the current record.  \nPlease enter the zip-code of the payee and try again.");
		document.forms[0].txtZip.focus();
		return false;
	}
	if(document.forms[0].payeeeid.value == null || document.forms[0].payeeeid.value=="" || document.forms[0].payeeeid.value==0)
	{
		alert("Bills Cannot be reviewed until a payee is set on the current record.  \nPlease enter the payee and try again.");
		document.forms[0].txtLastName.focus();
		return false;
	}
	EnableLockedCtlsForPostBack();
	document.forms[0].FormPostThroughBrs.value='true';	
	document.forms[0].submit();
	
	return true;
}

function CloneBRSPayment(iPos)
{
	//alert("CloneBRSPayment");
	var sParam="";
	
	this.document.callback="OnPaymentAdded";
	
	if(document.forms[0].TransId.value!="0")
			sParam="transid="+document.forms[0].TransId.value;
	else
			sParam="claimid="+document.forms[0].claimid.value;
	
	//BRS Needs a zip code to function.
	if(document.forms[0].txtZip.value == null || document.forms[0].txtZip.value=="" || document.forms[0].txtZip.value==0)
	{
		alert("Bills Cannot be reviewed until a zip code is set on the current record.  \nPlease enter the zip-code of the payee and try again.");
		document.forms[0].txtZip.focus();
		return false;
	}else
		sParam = sParam + "&zip=" + document.forms[0].txtZip.value;

	if(document.forms[0].payeeeid.value == null || document.forms[0].payeeeid.value=="" || document.forms[0].payeeeid.value==0)
	{
		alert("Bills Cannot be reviewed until a payee is set on the current record.  \nPlease enter the payee and try again.");
		document.forms[0].txtLastName.focus();
		return false;
	}else
		sParam = sParam + "&providereid=" + document.forms[0].payeeeid.value;
	
	sParam = sParam + "&loadcurrent="+iPos; 
	sParam = sParam + "&createclone=1"; 
	//alert(sParam);
	EnableLockedCtlsForPostBack();	
	m_codeWindow=RMX.window.open("brs.asp?"+sParam,"addbrspayment",
			"width=780,height=580,top="+(screen.availHeight-580)/2+",left="+(screen.availWidth-780)/2+",resizable=yes,scrollbars=yes");
	return false;		
}

function ViewBRSSplit(lTransId, lClaimId,iPos)
{
	var sParam="";
	var ret=true;
/*
	if(ConfirmSave()) {
		if(!validateForm())
			return false;
		else{
			document.forms[0].sysCmdConfirmSave.value=1;
			var obj=eval("document.forms[0].sysCmdQueue");
			if(obj!=null)
				obj.value="ViewBRSSplit("+lTransId+","+lClaimId+","+iPos+");";
			self.setTimeout('document.forms[0].submit();',200);
			return false;
		}
	}		
*/
	CloseProgressWindow();	
	EnableLockedCtlsForPostBack();	
	self.document.callback="OnPaymentEdited";
	
	if(lTransId!="0")
			sParam="transid="+lTranId;
	else
			sParam="claimid="+lClaimId;
	
	sParam = sParam + "&loadcurrent="+iPos; 
	
	if(document.forms[0].payeeeid.value == null || document.forms[0].payeeeid.value=="" || document.forms[0].payeeeid.value==0)
	{
		alert("Bills Cannot be reviewed until a payee is set on the current record.  \nPlease enter the payee and try again.");
		document.forms[0].txtLastName.focus();
		return false;
	}else
		sParam = sParam + "&providereid=" + document.forms[0].payeeeid.value;

	EnableLockedCtlsForPostBack();	
	m_codeWindow=RMX.window.open("brs.asp?"+sParam,"editbrspayment",
			"width=780,height=580,top="+(screen.availHeight-580)/2+",left="+(screen.availWidth-780)/2+",resizable=yes,scrollbars=yes");
}
function OnPaymentEdited()
{
	//alert("payment edited callback");
	if(m_codeWindow!=null)
		m_codeWindow.close();
	m_codeWindow=null;
	document.forms[0].sysCmd.value="7";
	EnableLockedCtlsForPostBack();	
	document.forms[0].submit();	
}

function OnPaymentAdded()
{
//alert("payment added callback");
	if(m_codeWindow!=null)
		m_codeWindow.close();
	m_codeWindow=null;
	document.forms[0].sysCmd.value="7";
	//document.forms[0].transsplitcount.value++;
	
	EnableLockedCtlsForPostBack();	
	document.forms[0].submit();	
}

function ViewSplit(lTransId, lSplitId,lClaimId)
{
	var ret=true;

	if(ConfirmSave()) {
		if(!validateForm())
			return false;
		else{
			document.forms[0].sysCmdConfirmSave.value=1;
			var obj=eval("document.forms[0].sysCmdQueue");
			if(obj!=null)
				obj.value="ViewSplit("+lTransId+","+lSplitId+");";
			self.setTimeout('document.forms[0].submit();',200);
			return false;
		}
	}		

	CloseProgressWindow();	
	EnableLockedCtlsForPostBack();	
	self.document.callback="OnPaymentEdited";

	m_codeWindow=RMX.window.open("transsplit.asp?transid="+lTransId+"&splitrowid="+lSplitId+"&claimid="+lClaimId + "&usesession=1","addpayment",
			"width=500,height=400,top="+(screen.availHeight-400)/2+",left="+(screen.availWidth-500)/2+",resizable=yes,scrollbars=yes");

	//urlNavigate("transsplit.asp?transid="+lTransId+"&splitrowid="+lSplitId+"&claimid="+lClaimId);
	return true;
}

//BSB Modified to call into ASP and have the indicated item removed from the Session XML Cache.
//Removes a SINGLE item at a time by position in the page and in the cache which must match exactly.
// No changes or deletions are made to the database until the funds record save button is hit. (This behavior is new 02.24.2004)
function DeleteSplit(iPos)
{
			//alert(iPos);
			if(!self.confirm("Data will be permanently deleted after this funds transaction is saved.  Are you sure you want to delete this detail record?"))
				return false;
			document.forms[0].sysCmd.value="8";
			document.forms[0].deletetarget.value=iPos;
			//alert(document.forms[0].deletetarget.value);
			//EnableLockedCtlsForPostBack();
			setDataChanged(false);
			document.forms[0].txtTaxId.disabled = false;	
			//document.forms[0].submit();

	return true;
}

function OnFormSubmit(sCmdQueue)
{
	document.forms[0].method="post";
	
	//patch for ShowConfirmations and showwarnings flag reset
	document.forms[0].showconfirmations.value = 'false';
	document.forms[0].showwarnings.value = 'false';
	
	
	//patch for not submitting this way while saving
	if (document.forms[0].FormPostThroughBrs.value=='true')
	{
	 if(m_DataChanged==true)
	  {
	    document.forms[0].datachanged.value="1";
	  }
	 return true;
	}
	if (document.forms[0].recordsaved.value=='true') return true;
	
	
	var ret=true;
	
	
	//if(!m_DataChanged && document.forms[0].sysCmd.value=='5')
	//	return false;
	
	//if(document.forms[0].sysCmd.value=='5')
	//	ret=validateForm();
		
	//else
	if(m_AvoidDataChangeConfirmation==true)
		return true;
	
	 if(m_DataChanged)
	{
		if(document.forms[0].sysCmd.value!='5')
		{
			if(ConfirmSave())
			{
				if(!validateForm())
					return false;
				else
					{
						if(sCmdQueue==null)
							sCmdQueue='';
						document.forms[0].sysCmdConfirmSave.value=1;
						document.forms[0].sysCmdQueue.value=sCmdQueue;
						SaveRecord();
						self.setTimeout('document.forms[0].submit();',200);
						return false;
					}
			}
		}
		else
		{
			SaveRecord();
		}
		//alert("aa gaya");
		
		var wnd=RMX.window.open('csc-Theme/riskmaster/common/html/progress.html','progressWnd',
			'width=400,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-400)/2);
		//alert(self.parent.wndProgress);
		
		self.parent.wndProgress=wnd;
					
		return true;
	}
	//alert(document.forms[0].all("TransId").value);
	/*
	if(document.forms[0].sysCmd.value=='' && ret) //New record button pressed.
	{
		document.forms[0].TransId.value='0'
		document.forms[0].claimid.value='0'
	}
	*/
	//alert(document.forms[0].all("TransId").value);
	
	
	//if (ret) 
		//utility of function no longer seems to exist
		//EnableLockedCtlsForPostBack();
	//pmahli 11/30/2007 MITS 10811 - Start
	OpenProgressWindow();
	//pmahli 11/30/2007 MITS 10811 - End
	return true;
}

function EnableLockedCtlsForPostBack()
{
	var ret = true;
	if (haveProperty(document.forms[0],"txtTaxId")) 
		document.forms[0].txtTaxId.disabled=false;
	//If we're really going to the server then be sure to
	// send along any Disabled (Read Locked) fields.
	if (ret && haveProperty(document.forms[0],"txtClaimNumber")) 
		document.forms[0].txtClaimNumber.disabled=false;	
	if (ret && haveProperty(document.forms[0],"txtTaxId")) 
		document.forms[0].txtTaxId.disabled=false;	
	if (ret && haveProperty(document.forms[0],"txtFirstName")) 
		document.forms[0].txtFirstName.disabled=false;	
	if (ret && haveProperty(document.forms[0],"txtAddr1")) 
		document.forms[0].txtAddr1.disabled=false;	
	if (ret && haveProperty(document.forms[0],"txtAddr2")) 
	    document.forms[0].txtAddr2.disabled=false;	
	if (ret && haveProperty(document.forms[0],"txtAddr3")) 
	    document.forms[0].txtAddr3.disabled=false;	
	if (ret && haveProperty(document.forms[0],"txtAddr4")) 
	    document.forms[0].txtAddr4.disabled=false;	
	if (ret && haveProperty(document.forms[0],"txtCity")) 
		document.forms[0].txtCity.disabled=false;	
	if (ret && haveProperty(document.forms[0],"txtZip")) 
		document.forms[0].txtZip.disabled=false;	
	//txtState_cid is not disabled since it's a hidden code-id field.
	return true;
}
	
//Special case for an otherwise "read-only" record which has
// been voided or cleared.
function OnClearOrVoid()
{
	
	var ret=true;
	
	//Only validation is check to see whether flags changed...
	if(!m_DataChanged)
		return false;
	//Otherwise - we're in for a "corvonly" save in asp.
	//document.forms[0].sysCmd.value='5';
	//Shruti, 11889
    //MITS 12129 Abhishek Added check for null 
	if(document.forms[0].hdnCheckStatus!= null && document.forms[0].hdnCheckStatus.value.substring(0,1) != 'P')
	{
	    document.forms[0].all["sysCmd"].value='5';
	    document.forms[0].all["corvonly"].value='1';
	}
	else
	{
	    saveTrasaction();
	    return ret;
	}
	SaveRecord();
	// document.forms[0].submit();
	//document.forms[0].corvonly.value='1';
	//document.forms[0].submit();
	return ret;
}

function replace(sSource, sSearchFor, sReplaceWith)
{
	var arr = new Array();
	arr=sSource.split(sSearchFor);
	return arr.join(sReplaceWith);
}

function validateForm()
{
	//alert("validateForm");
	//tkr 2/2003 OK-Cancel calls this, but skip validation if corvonly
	//ugly hack in trans.asp adds the "corvonlyflag" if save button calls OnClearOrVoid()
	if (haveProperty(document.frmCorvOnly, "corvonlyflag"))
		return true;
	
	// Vaibhav: In case of readonly, the page does not have control but user can still save to set the void flag. 
	if( document.forms[0].ReadOnlyFlagCalculated.value == "True" )
		return true ;
		
	var msg = "Not all required fields contain the value. Please enter the data into all underlined fields.";
	//if (haveProperty(document.forms[0],"transdate"))	//BB 06.11.2002
	if(document.forms[0].transdate)
	{	
		var s=replace(document.forms[0].transdate.value," ","");
		//alert("transdate");
		if(s=="")
		{
			alert(msg);
			document.forms[0].transdate.focus();
			document.forms[0].sysCmdConfirmSave.value=0;
			document.forms[0].sysCmdQueue.value='';
			return false;
		}
	}
	//if( haveProperty(document.forms[0],"cboBankAccount"))
	if(document.forms[0].cboBankAccount)
	{
		if(document.forms[0].cboBankAccount.selectedIndex<0)
		{
			alert(msg);
			document.forms[0].cboBankAccount.focus();
			document.forms[0].sysCmdConfirmSave.value=0;
			document.forms[0].sysCmdQueue.value='';
			return false;
		}
	}
	else
	{
		alert("The department for the current claim must be given access to at least one account before a payment can be saved.");
		document.forms[0].sysCmdConfirmSave.value=0;
		document.forms[0].sysCmdQueue.value='';
		return false;
	}
	if(document.forms[0].cboPayeeType.selectedIndex<=0)
	{
		alert(msg);
		document.forms[0].cboPayeeType.focus();
		document.forms[0].sysCmdConfirmSave.value=0;
		document.forms[0].sysCmdQueue.value='';
		return false;
	}
	s=replace(document.forms[0].txtLastName.value," ","");
	if(s=="")
	{
		alert(msg);
		document.forms[0].txtLastName.focus();
		document.forms[0].sysCmdConfirmSave.value=0;
		document.forms[0].sysCmdQueue.value='';
		return false;
	}
	//Shruti for MITS 8955 starts
	if(document.forms[0].SysReqSupp)
	{
		if(document.forms[0].SysReqSupp.value != "")
		{
			
			var s = document.forms[0].SysReqSupp.value;
			var ctrls = s.split("^");
			var CurrControlType = false;
			for(var i = 0; i<ctrls.length - 1; i++)
			{
				if(ctrls[i].substring(0,1) == '_')
				{ 
					var ctrl = eval("document.forms[0]." + ctrls[i].substring(1));
					CurrControlType = true;
				}
				else
				{
					var ctrl = eval("document.forms[0]." + ctrls[i]);
					CurrControlType = false;
				}
				if((CurrControlType && (ctrl.value == "" || ctrl.value == " " ||ctrl.value == "$0.00")) ||
					(!CurrControlType && (ctrl.value == "" || ctrl.value == " ")))
				{
					alert(msg);
					clickEventHandler('tabChange', 'Supplementals');
					ctrl.focus();
					document.forms[0].sysCmdConfirmSave.value=0;
					document.forms[0].sysCmdQueue.value='';
					return false;
				}

			}
		}
	}
	
	//Shruti for MITS 8955 ends
	if(document.forms[0].PaymentExists.value=="false")
	{
		alert("You must enter transaction detail information before the payment can be saved.");
		document.forms[0].sysCmdConfirmSave.value=0;
		document.forms[0].sysCmdQueue.value='';
		return false;
	}
	
	return true;
}

function GeneratePayeeHTML()
{
	var sRet = "";
	sRet = sRet + (parent.CommonValidations.GeneratePayeeHTML1 + "," + parent.CommonValidations.GeneratePayeeHTML11);
	return sRet;
	
}

function GenerateClosedClaimHTML()
{
	var sRet = "";
	sRet = sRet + (parent.CommonValidations.GenerateClosedClaimHTML1 + "," + parent.CommonValidations.GenerateClosedClaimHTML11);

	return sRet;
	
}
//Shruti for Max Rate Check
function GenerateMaxRateHTML(sYear, sMaxRate)
{
	var sRet = "";
	sRet = sRet + (parent.CommonValidations.GenerateMaxRateHTML1 + sYear + parent.CommonValidations.GenerateMaxRateHTML2 + sMaxRate + parent.CommonValidations.GenerateMaxRateHTML3);
	return sRet;
}

function GenerateCompRateHTML(sCompRate)
{
	var sRet = "";
	sRet = sRet + (parent.CommonValidations.GenerateCompRateHTML1 + sCompRate + parent.CommonValidations.GenerateCompRateHTML2);
	return sRet;
}
//Shruti for Max Rate Check ends
function GenerateDupCheckHTML()
{	
	//MITS 12313 Raman Bhatia
	//Prohibit Duplicate Payment Flag is not working
	var bIsDuplicatePaymentSaveProhibited = "";
	if(document.forms[0].IsDuplicatePaymentSaveProhibited != null)
	{
	    bIsDuplicatePaymentSaveProhibited = document.forms[0].IsDuplicatePaymentSaveProhibited.value;
	}
	
	var sRet="";
	sRet = sRet + (parent.CommonValidations.GenerateDupCheckHTML1);
	
	//^Rajeev 01/21/2003 -- CSS Consolidation
	//sRet = sRet + ("<link rel=\"stylesheet\" href=\"forms.css\" type=\"text/css\" />");
	//sRet = sRet + ("</head><body onUnload=\"self.opener.onCodeClose();\">");

	sRet = sRet + (parent.CommonValidations.GenerateDupCheckHTML2);
	//^^Rajeev 01/21/2003 -- CSS Consolidation

	sRet = sRet + (parent.CommonValidations.GenerateDupCheckHTML3);
	var ctrlConfirm=eval(parent.CommonValidations.GenerateDupCheckHTML4 +  new String(parseInt(document.forms[0].confirmedcount.value,10)));
	if (ctrlConfirm!=null)
		sConfirmText = ctrlConfirm.value;
		
	var lines=sConfirmText.split("*");
	var s="datatd";
	for(var i=1;i<lines.length;i++)
	{
		if(lines[i]=="")
			continue;
		var rows=lines[i].split("|");
		sRet = sRet + (parent.CommonValidations.FormJsHtml12 + s + parent.CommonValidations.FormJsHtml13 + rows[0] + parent.CommonValidations.FormJsHtml14);
        sRet = sRet + (parent.CommonValidations.FormJsHtml15 + s + parent.CommonValidations.FormJsHtml13 + rows[1] + parent.CommonValidations.FormJsHtml14);
        sRet = sRet + (parent.CommonValidations.FormJsHtml15 + s + parent.CommonValidations.FormJsHtml13 + rows[2] + parent.CommonValidations.FormJsHtml14);
        sRet = sRet + (parent.CommonValidations.FormJsHtml15 + s + parent.CommonValidations.FormJsHtml13 + rows[3] + parent.CommonValidations.FormJsHtml14);
        sRet = sRet + (parent.CommonValidations.FormJsHtml15 + s + parent.CommonValidations.FormJsHtml13 + rows[4] + parent.CommonValidations.FormJsHtml14);
        sRet = sRet + (parent.CommonValidations.FormJsHtml15 + s + parent.CommonValidations.FormJsHtml13 + rows[5] + parent.CommonValidations.FormJsHtml14);
        sRet = sRet + (parent.CommonValidations.FormJsHtml15 + s + parent.CommonValidations.FormJsHtml13 + rows[6] + parent.CommonValidations.FormJsHtml14);
        sRet = sRet + (parent.CommonValidations.FormJsHtml15 + s + parent.CommonValidations.FormJsHtml13 + rows[7] + parent.CommonValidations.FormJsHtml14);
        sRet = sRet + (parent.CommonValidations.FormJsHtml15 + s + parent.CommonValidations.FormJsHtml13 + rows[8] + parent.CommonValidations.FormJsHtml10);
		if(s=="datatd")
			s="datatd1";
		else
			s="datatd";
	}
	sRet = sRet + (parent.CommonValidations.GenerateDupCheckHTML5);
	if(bIsDuplicatePaymentSaveProhibited != "True" )
	{
	    sRet = sRet + (parent.CommonValidations.GenerateDupCheckHTML6);
	}
	else
	{
	    sRet = sRet + (parent.CommonValidations.GenerateDupCheckHTML7);
	}
	
	sRet = sRet + (parent.CommonValidations.GenerateDupCheckHTML8);
	
	if(bIsDuplicatePaymentSaveProhibited != "True" )
	{
	    sRet = sRet + (parent.CommonValidations.GenerateDupCheckHTML9);
	}
	else
	{
	    sRet = sRet + (parent.CommonValidations.GenerateDupCheckHTML10);
	}
	
	sRet = sRet + (parent.CommonValidations.GenerateDupCheckHTML11);
	return sRet;
}

function GenerateRollUpHTML()
{	
	var sRet="";
	sRet = sRet + (parent.CommonValidations.GenerateRollUpHTML1);

	//^Rajeev 01/21/2003 -- CSS Consolidation
	//sRet = sRet + ("<link rel=\"stylesheet\" href=\"forms.css\" type=\"text/css\" />");
	//sRet = sRet + ("</head><body onUnload=\"self.opener.onCodeClose();\">");

	sRet = sRet + (parent.CommonValidations.GenerateRollUpHTML2);
	//^^Rajeev 01/21/2003 -- CSS Consolidation

	sRet = sRet + (parent.CommonValidations.GenerateRollUpHTML3);
	var ctrlConfirm=eval(parent.CommonValidations.GenerateDupCheckHTML4 +  new String(parseInt(document.forms[0].confirmedcount.value,10)));
	if (ctrlConfirm!=null)
	{
		sConfirmText = ctrlConfirm.value;
		sConfirmText = sConfirmText.slice(sConfirmText.indexOf("roll_up*") + 8);
	}
	var lines=sConfirmText.split("*");
	var s="datatd";
	for(var i=0;i<lines.length;i++)
	{
		if(lines[i]=="")
			continue;
		var rows=lines[i].split("|");
		sRet = sRet + (parent.CommonValidations.FormJsHtml12 + s + parent.CommonValidations.FormJsHtml13 + rows[0] + parent.CommonValidations.FormJsHtml14);
        sRet = sRet + (parent.CommonValidations.FormJsHtml15 + s + parent.CommonValidations.FormJsHtml13 + rows[1] + parent.CommonValidations.FormJsHtml14);
        sRet = sRet + (parent.CommonValidations.FormJsHtml15 + s + parent.CommonValidations.FormJsHtml13 + rows[2] + parent.CommonValidations.FormJsHtml14);
        sRet = sRet + (parent.CommonValidations.FormJsHtml15 + s + parent.CommonValidations.FormJsHtml13 + rows[3] + parent.CommonValidations.FormJsHtml14);
        sRet = sRet + (parent.CommonValidations.FormJsHtml15 + s + parent.CommonValidations.FormJsHtml13 + rows[4] + parent.CommonValidations.FormJsHtml14);
        sRet = sRet + (parent.CommonValidations.FormJsHtml15 + s + parent.CommonValidations.FormJsHtml13 + rows[5] + parent.CommonValidations.FormJsHtml14);
        sRet = sRet + (parent.CommonValidations.FormJsHtml15 + s + parent.CommonValidations.FormJsHtml13 + rows[6] + parent.CommonValidations.FormJsHtml14);
        sRet = sRet + (parent.CommonValidations.FormJsHtml15 + s + parent.CommonValidations.FormJsHtml13 + rows[7] + parent.CommonValidations.FormJsHtml14);
        sRet = sRet + (parent.CommonValidations.FormJsHtml15 + s + parent.CommonValidations.FormJsHtml13 + rows[8] + parent.CommonValidations.FormJsHtml10);
		if(s=="datatd")
			s="datatd1";
		else
			s="datatd";
	}
	sRet = sRet + (parent.CommonValidations.GenerateRollUpHTML4 + "," + parent.CommonValidations.GenerateRollUpHTML41 +","+ parent.CommonValidations.GenerateRollUpHTML42);
	// npadhy 8/6/2008 MITS 12860
	sRet = sRet + (parent.CommonValidations.GenerateRollUpHTML5);
	return sRet;
}

//gagnihotri MITS 12329 10/06/2008
function GenerateClearHTML()
{	
	var sRet="";
	sRet = sRet + (parent.CommonValidations.GenerateClearHTML1);

	//^Rajeev 01/21/2003 -- CSS Consolidation
	//sRet = sRet + ("<link rel=\"stylesheet\" href=\"forms.css\" type=\"text/css\" />");
	//sRet = sRet + ("</head><body onUnload=\"self.opener.onCodeClose();\">");

	sRet = sRet + (parent.CommonValidations.GenerateClearHTML2);
	//^^Rajeev 01/21/2003 -- CSS Consolidation

	sRet = sRet + (parent.CommonValidations.GenerateClearHTML3);
	var ctrlConfirm=eval('document.forms[0].confirmation_' +  new String(parseInt(document.forms[0].confirmedcount.value,10)));
	if (ctrlConfirm!=null)
	{
		sConfirmText = ctrlConfirm.value;
		sConfirmText = sConfirmText.slice(sConfirmText.indexOf("clear*") + 6);
	}
	var lines=sConfirmText.split("*");
	var s="datatd";
	for(var i=0;i<lines.length;i++)
	{
		if(lines[i]=="")
			continue;
		var rows=lines[i].split("|");
		sRet = sRet + (parent.CommonValidations.FormJsHtml12 + s + parent.CommonValidations.FormJsHtml13 + rows[0] + parent.CommonValidations.FormJsHtml14);
        sRet = sRet + (parent.CommonValidations.FormJsHtml15 + s + parent.CommonValidations.FormJsHtml13 + rows[1] + parent.CommonValidations.FormJsHtml14);
        sRet = sRet + (parent.CommonValidations.FormJsHtml15 + s + parent.CommonValidations.FormJsHtml13 + rows[2] + parent.CommonValidations.FormJsHtml14);
        sRet = sRet + (parent.CommonValidations.FormJsHtml15 + s + parent.CommonValidations.FormJsHtml13 + rows[3] + parent.CommonValidations.FormJsHtml14);
        sRet = sRet + (parent.CommonValidations.FormJsHtml15 + s + parent.CommonValidations.FormJsHtml13 + rows[4] + parent.CommonValidations.FormJsHtml14);
        sRet = sRet + (parent.CommonValidations.FormJsHtml15 + s + parent.CommonValidations.FormJsHtml13 + rows[5] + parent.CommonValidations.FormJsHtml14);
        sRet = sRet + (parent.CommonValidations.FormJsHtml15 + s + parent.CommonValidations.FormJsHtml13 + rows[6] + parent.CommonValidations.FormJsHtml14);
        sRet = sRet + (parent.CommonValidations.FormJsHtml15 + s + parent.CommonValidations.FormJsHtml13 + rows[7] + parent.CommonValidations.FormJsHtml14);
        sRet = sRet + (parent.CommonValidations.FormJsHtml15 + s + parent.CommonValidations.FormJsHtml13 + rows[8] + parent.CommonValidations.FormJsHtml10);
		if(s=="datatd")
			s="datatd1";
		else
			s="datatd";
	}
	sRet = sRet + (parent.CommonValidations.GenerateClearHTML4 + "," + parent.CommonValidations.GenerateClearHTML41 + "," + parent.CommonValidations.GenerateClearHTML42 );
	// gagnihotri Merging 12860 Changes
	// npadhy 8/6/2008 MITS 12860
	sRet = sRet + (parent.CommonValidations.GenerateClearHTML5);
	return sRet;
}


function DisplayConfirmation()
{
	var o=eval("document.forms[0].confirmationcount");
	if(o==null || o.value=="" || parseInt(o.value,10)<=0)
		return false;
		
	var sConfirmText; 
	
	if (new String(o.value) != document.forms[0].confirmedcount.value)
	{
		var ctrlConfirm=eval('document.forms[0].confirmation_' +  new String(parseInt(document.forms[0].confirmedcount.value,10)));
		if (ctrlConfirm!=null)
			sConfirmText = ctrlConfirm.value;
		else
			sConfirmText="";

		//BB 08.01.2002 Switched about:blank to ad.html to eliminate silly SSL prompts.
		if (m_codeWindow==null)
			m_codeWindow=RMX.window.open('ad.html','ConfirmationsWindow',
				'scrollbars=yes,resizable=yes,width=640,height=350'+',top='+(screen.availHeight-350)/2+',left='+(screen.availWidth-640)/2);
		else
			m_codeWindow.document.open();
					
		if(sConfirmText.indexOf("roll_up*")>0) // Is a roll-up confirmation.
			m_codeWindow.document.writeln(GenerateRollUpHTML());
		//gagnihotri MITS 12329 07/10/08
		else if(sConfirmText.indexOf("clear*")>0) // Is a clear confirmation.
			m_codeWindow.document.writeln(GenerateClearHTML());
		else if(sConfirmText.indexOf("new_payee*")>=0) // Is a roll-up confirmation.
			m_codeWindow.document.writeln(GeneratePayeeHTML());
		else if(sConfirmText.indexOf("claim_closed*")>=0) // Is a claim closed confirmation.
			m_codeWindow.document.writeln(GenerateClosedClaimHTML());
		//Shruti
		else if(sConfirmText.indexOf("CompRateFromFL")>=0) // Payment exceeds max comp rate.
		{
		    var sArray = sConfirmText.split("_");
			m_codeWindow.document.writeln(GenerateMaxRateHTML(sArray[1],sArray[2]));
		}
		else if(sConfirmText.indexOf("CompRate")>=0) // Payment exceeds calculated comp rate.
		{
		    var sArray = sConfirmText.split("_");
			m_codeWindow.document.writeln(GenerateCompRateHTML(sArray[1]));
		}
		else
			m_codeWindow.document.writeln(GenerateDupCheckHTML());
		
		m_codeWindow.document.close();
	}
	return false;
}

function ConfirmationSave()
{
	m_DataChanged=true;
	document.forms[0].confirmedcount.value=new String(parseInt(document.forms[0].confirmedcount.value,10) + 1);
		
	var o=eval("document.forms[0].confirmationcount");
	if(o==null || o.value=="" || parseInt(o.value,10)<=0)
		return false;

	//Is this the last confirmation required?	
	if (new String(o.value) == document.forms[0].confirmedcount.value)
	{
		// close code window
		if(m_codeWindow!=null)
		{
			m_codeWindow.close();
			m_codeWindow=null;
		}
	
		if(String(document.forms[0].confirmation_0.value).indexOf("roll_up*")>0)// Is a void roll-up confirmation. 
		{	document.forms[0].Void.checked = true;
			OnClearOrVoid();
			document.forms[0].submit();
		}
		//gagnihotri MITS 12329 10/06/2008
		else if(String(document.forms[0].confirmation_0.value).indexOf("clear*")>0)// Is a clear roll-up confirmation. 
		{	document.forms[0].Cleared.checked = true;
			OnClearOrVoid();
			document.forms[0].submit();
		}
		else
		{	
			document.forms[0].sysCmd.value="5";
			m_AvoidDataChangeConfirmation = true;
			//Bug Fix.  08.01.2002 BB - Payee info was disappearing.  
			// Found that disabled fields were not being re-enabled for submit
			// when save was done from confirmation screen.
			//document.forms[0].submit();
			
			if(OnFormSubmit("ConfirmationSave()"))
			{
				document.forms[0].ouraction.value="savetransaction";
				document.forms[0].recordsaved.value="true";
				self.setTimeout('document.forms[0].submit();',100);
			}
		}
		return true;
	}
	else
	{
		DisplayConfirmation();
		return false;
	}
	return false;
}

// npadhy 8/6/2008 MITS 12860 Changed the signature to know, from which page this function is getting called.
function ConfirmationCancel(CalledFrom)
{
	if(m_codeWindow!=null)
		m_codeWindow.close();
	m_codeWindow=null;

	document.forms[0].confirmedcount.value="0";
	document.forms[0].showconfirmations.value='false';
	
	// If the Function is called from Roll Up Page, reset the void checkbox and set data change to false
	if(CalledFrom == 'RollUp')
	{
	    if(document.forms[0].Void != null)
	    {
	        document.forms[0].Void.checked = false;
	        setDataChanged(false);
	    }
	}
	// gagnihotri Merging MITS 12860 Changes
	// If the Function is called from Clear Roll Up Page, reset the Cleared checkbox and set data change to false
	else if(CalledFrom == 'ClearRollUp')
	{
	    if(document.forms[0].Cleared != null)
	    {
	        document.forms[0].Cleared.checked = false;
	        setDataChanged(false);
	    }
	}
	else
	{
	    setDataChanged(true);
	}
	
	
	return true;
}

function ValidateTaxId()
{
	if (document.forms[0].payeeeid.value!="0" && document.forms[0].payeeeid.value!="")
	{
		alert("The Tax Id of an existing entity cannot be modified from this screen.  Please use the entity maintenance screen to make this change.");
		document.forms[0].txtTaxId.value = document.forms[0].txtTaxIdPrev.value;
		return false;
	}
	return true;
}
function showSupp()
{

	if(ConfirmSave()){
		if(!validateForm())
			return false
		else{
			document.forms[0].sysCmdConfirmSave.value=1;
			document.forms[0].sysCmdQueue.value="showSupp()";
			self.setTimeout('document.forms[0].submit();',200);
			return false;
		}
	}

	CloseProgressWindow();

	var lCurrentId=document.forms[0].TransId.value;
	if(lCurrentId==null || lCurrentId=="" || lCurrentId=="0")
	{
		alert("To display the supplemental data you have to select or create a record first.");
		return false;
	}
	//claim_supp&amp;sys_formidname=claim_id&amp;claim_id=%claimid%&amp;sys_formpform=claim&amp;sys_formpidname=claim_id
	
	sLinkParams="formname=funds_supp&sys_formidname=trans_id&trans_id=" + lCurrentId + "&sys_formpform=funds&sys_formpidname=trans_id";
	
	if(sLinkParams!="")
		sLinkParams=sLinkParams+"&";
	sLinkParams=sLinkParams+"sys_psid=9650"

	sLinkParams=replace(sLinkParams,"&amp;","&");
	alert(sLinkParams);
	//Link with supplementals later
	//window.setTimeout("urlNavigate('supp.asp?"+sLinkParams+"')", 10);
	return true;
}

function doSearch()
{
	if(ConfirmSave())
	{
		if(!validateForm())
			return false
		else
		{
			document.forms[0].all["sysCmdConfirmSave"].value = 1;
			document.forms[0].all["sysCmdQueue"].value="doSearch()";
			self.setTimeout('document.forms[0].submit();',200);
			return false;
			}
	}
	
	CloseProgressWindow();
	
	//return false;
	//window.setTimeout("urlNavigate('searchgen.asp?formname=payment')", 100);
	window.setTimeout("urlNavigate('home?pg=riskmaster/Search/MainPage&amp;formname=payment&ifLookUp=1')", 100);//Parijat Mits 10397 back button
	//^^Rajeev 12/10/2002 -- MITS#3899
	
	return false;
}

function Comments()
{
	var sId=document.forms[0].TransId.value;
	if(sId<=0 || sId=="")
	{
		self.alert("You are working on the new record and this functionality is available for existing records only. Please save the data and try again.");
		return false;
	}
	// Set callback for comments page so it can set "dirty" flag on funds screen
	document.OnCommentsChanged = OnCommentsChanged;  // Callback so red comments notepad function can "dirty" the form
	//link with comments
	var sLink='home?pg=riskmaster/Comments/MainPage&SysFormName=Funds'+"&recordid="+sId+"&CommentsFlag=true";
	
	sLink=sLink+"&psid=9650";
	
	RMX.window.open(sLink,'qdWnd',
		'width=620,height=450'+',top='+(screen.availHeight-400)/2+',left='+(screen.availWidth-620)/2+',resizable=yes,scrollbars=yes');
	return false;
}
function FilteredDiary()
{
	
	if(ConfirmSave()){
		if(!validateForm())
			return false
			else{
				document.forms[0].sysCmdConfirmSave.value=1;
				document.forms[0].sysCmdQueue.value="FilteredDiary()";
				self.setTimeout('document.forms[0].submit();',200);
				return false;
			}
	}

	CloseProgressWindow();
	
	var sId=document.forms[0].TransId.value;
	if(sId<=0 || sId=="")
	{
		self.alert("You are working on a new record and this functionality is available for existing records only. Please save the data and try again.");
		return false;
	}
	document.forms[0].tablename.value="FUNDS";
	
	return true;
}
// Anjaneya : MITS 11873
function NewFilteredDiary()
{
    if(ConfirmSave()){
		if(!validateForm())
			return false
			else{
				document.forms[0].sysCmdConfirmSave.value=1;
				//document.forms[0].sysCmdQueue.value="NewFilteredDiary()";
				self.setTimeout('document.forms[0].submit();',200);
				return false;
			}
	}
    CloseProgressWindow();
	var sId=document.forms[0].TransId.value;
	if(sId<=0 || sId=="")
	{
		self.alert("You are working on a new record and this functionality is available for existing records only. Please save the data and try again.");
		return false;
	}
	//zalam 08/22/2008 Mits:-12831 Start
	if(document.forms[0].tablename != null)
	{
	    var sClaimNumber = document.forms[0].ControlNumber.value;
	    sLob = "Funds";
	}
	//zalam 08/22/2008 Mits:-12831 End
	document.forms[0].tablename.value="FUNDS";
	//zalam 08/22/2008 Mits:-12831 Start
	//MDIParent.CreateSingletonMDIChild('home?pg=riskmaster/Diaries/DiaryList&AttachTable=FUNDS&AttachRecordId=' + sId);
	MDIParent.CreateSingletonMDIChild('home?pg=riskmaster/Diaries/DiaryList&AttachTable=FUNDS&AttachRecordId=' + sId + "&AttClaimNumber=" + sClaimNumber + "&AttLob=" + sLob);
	//zalam 08/22/2008 Mits:-12831 End
	// This is to make sure form is not submitted since new window is opening up
	return false;
}
function OnCommentsChanged()
{
	setDataChanged(true);
	clickEventHandler('saveTrasaction');
	document.forms[0].submit();
}

function Diary()
{
	var sId=document.forms[0].TransId.value;
	if(sId<=0 || sId=="")
	{
		self.alert("You are working on a new record and this functionality is available for existing records only. Please save the data and try again.");
		return false;
	}
	
	document.forms[0].tablename.value="FUNDS";
	
	//link with diary
	
	this.document.callback="OnDiaryAttached" ;
	
	if(wnd==null)
			{	
					var wnd=RMX.window.open("home?pg=riskmaster/Diaries/AttachDiary&amp;AttachTable=" + document.forms[0].tablename.value
						+ "&AttachRecordId=" + sId ,"AttachDiary",
			"width=600,height=600,top="+(screen.availHeight-600)/2+",left="+(screen.availWidth-600)/2+",resizable=yes,scrollbars=yes");
					}
	return false;


	//window.open('attachdiary.asp?formname=funds&recordid='+sId,'adWnd',
	//	'width=500,height=500'+',top='+(screen.availHeight-500)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
	
	

}

function doLookup()
{
	
	if(ConfirmSave()){
		if(!validateForm())
			return false
		else{
			document.forms[0].sysCmdConfirmSave.value=1;
			document.forms[0].sysCmdQueue.value="doLookup()";
			self.setTimeout('document.forms[0].submit();',200);
			return false;
		}
	}
	CloseProgressWindow();
	
	//Replace with lookup code later
	var sUrl="home?pg=riskmaster/LookupData/Lookup&amp;form=funds";
	
	if(wnd==null)
			{	
					var wnd=RMX.window.open(sUrl ,"LookupData",
			"width=600,height=600,top="+(screen.availHeight-600)/2+",left="+(screen.availWidth-600)/2+",resizable=yes,scrollbars=yes");
					}
	return false;
}

function attach()
{
	var sid="";
	sid="11000"; // Events
	
	var lCurrentId=document.forms[0].TransId.value;
	if(lCurrentId<=0 || lCurrentId=="" || lCurrentId==null || lCurrentId=="0")
	{
		self.alert("You are working on the new record and this functionality is available for existing records only. Please save the data and try again.");
		return false;
	}
	//link with document attach
	var sUrl="home?pg=riskmaster/DocumentManagement/file-listing&amp;TableName=funds&RecordId=" + lCurrentId + "&FormName=funds";
	
	var wnd=RMX.window.open(sUrl,'attachWnd',
			'width=500,height=400'+',top='+(screen.availHeight-400)/2+',left='+(screen.availWidth-500)/2 + ',resizable=yes,scrollbars=yes');
	return false;
}
function MailMerge()
{
	var sId=document.forms[0].TransId.value;
	if(sId<=0 || sId=="")
	{
		self.alert("To begin a Mail Merge you have to select or create a record first.");
		return false;
	}
	//link with mail-merge
	var objWnd=RMX.window.open("home?pg=riskmaster/MailMerge/MergeTemplate1&amp;TableName=Funds&RecordId="+ sId ,"mergeWnd",'width=620,height=400'+',top='+(screen.availHeight-400)/2+',left='+(screen.availWidth-620)/2+',resizable=yes,scrollbars=yes');
	
	return false;
	
}
function numLookupCallback(sId)
{
	var obj=null;
	var obj2 = null;
	
	if(m_codeWindow!=null)
		m_codeWindow.close();
	m_codeWindow=null;
	self.lookupCallback=null;
	m_sNoDataId = sId;
	
		if(m_LookupType==6)
		{
			// Claim Number Lookup
			if(sId==-1)
			{
				obj2 = eval("m_Wnd.document.frames[1].forms[0]");
				if(obj2 != null)
				{
					obj=eval("document.forms[0]."+m_sFieldName);			
					if(obj!=null)
					{	
						obj.value=m_Wnd.document.frames[1].forms[0].claimnumber.value;
						obj.cancelledvalue=m_Wnd.document.frames[1].forms[0].claimnumber.value;
					}

					// Record Id
					obj2=null;
					obj=null;
					obj=eval("m_Wnd.document.frames[1].forms[0].claimid");
					if(obj!=null)
						obj2=eval("document.forms[0]." + m_sFieldName + "_cid");
					if(obj2!=null)
					{
						obj2.value=obj.value;
						obj2.cancelledvalue=obj.value; 
					}
					
					//Tr : - 1547 Nitesh sarts
					if (m_ClaimIDLookup)
					{	document.forms[0].ouraction.value='gototransaction';
						m_ClaimIDLookup=false;
						document.forms[0].submit();
					}
					//Tr : - 1547 Nitesh Ends
					
				}
				else
				{
					if(m_Wnd != null)
						m_Wnd.close();
					alert("Payment with control number " + m_sNoDataId + " is not found in Database.");
					setDataChanged(false);
				}				
				if(m_Wnd != null)
					m_Wnd.close();
				m_Wnd=null;
			}
			else
			{
				self.lookupCallback="numLookupCallback";
				
				self.setTimeout("m_Wnd=window.open('home?pg=riskmaster/Search/SearchRender&amp;entityid="+sId+"&lookuptype="+m_LookupType+
						"','progressWnd','width=400,height=150,top="+((screen.availHeight-150)/2)+
						",left="+((screen.availWidth-400)/2)+"');",100);
				//m_Wnd=window.open('getclaimdata.asp?claimid='+sId,'progressWnd',
				//		'width=400,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-400)/2);
			}
		}
		else if(m_LookupType==7)
		{
			// Event Number Lookup
			if(sId==-1)
			{
				obj2 = eval("m_Wnd.document.frames[1].forms[0]");
				if(obj2 != null)
				{
					obj=eval("document.forms[0]."+m_sFieldName);
					if(obj!=null)
					{	
						obj.value=m_Wnd.document.frames[1].forms[0].eventnumber.value;
						obj.cancelledvalue=m_Wnd.document.frames[1].forms[0].eventnumber.value;
					}
				}
				else
				{
					if(m_Wnd != null)
						m_Wnd.close();
					alert("Event with control number " + m_sNoDataId + " is not found in Database.");
					setDataChanged(false);
				}
				if(m_Wnd != null)
					m_Wnd.close();
				m_Wnd=null;
			}
			else
			{
				self.lookupCallback="numLookupCallback";
				self.setTimeout("m_Wnd=window.open('home?pg=riskmaster/Search/SearchRender&amp;entityid="+sId+"&lookuptype="+m_LookupType+
						"','progressWnd','width=400,height=150,top="+((screen.availHeight-150)/2)+
						",left="+((screen.availWidth-400)/2)+"');",100);
				//m_Wnd=window.open('geteventdata.asp?eventid='+sId,'progressWnd',
				//		'width=400,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-400)/2);
			}
		}
		else if(m_LookupType==8)
		{
			// Vehicle Number Lookup
			if(sId==-1)
			{
				obj2 = eval("m_Wnd.document.frames[1].forms[0]");
				if(obj2 != null)
				{
					obj=eval("document.forms[0]."+m_sFieldName);
					
					if(obj!=null)
					{	
						obj.value=m_Wnd.document.frames[1].forms[0].vin.value;
						obj.cancelledvalue=m_Wnd.document.frames[1].forms[0].vin.value;
					}
				}
				else
				{
					if(m_Wnd != null)
						m_Wnd.close();
					alert("Vehicle with control number " + m_sNoDataId + " is not found in Database.");
					setDataChanged(false);
				}
				if(m_Wnd != null)
					m_Wnd.close();
				m_Wnd=null;
			}
			else
			{
				self.lookupCallback="numLookupCallback";
				self.setTimeout("m_Wnd=window.open('home?pg=riskmaster/Search/SearchRender&amp;entityid="+sId+"&lookuptype="+m_LookupType+
					"','progressWnd','width=400,height=150,top="+((screen.availHeight-150)/2)+
					",left="+((screen.availWidth-400)/2)+"');",100);
				//m_Wnd=window.open('getvehicledata.asp?unitid='+sId,'progressWnd',
				//		'width=400,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-400)/2);
			}
		}
		else if(m_LookupType==9)
		{
			// Policy Number Lookup
			if(sId==-1)
			{
				obj2 = eval("m_Wnd.document.frames[1].forms[0]");
				if(obj2 != null)
				{
					obj=eval("document.forms[0]."+m_sFieldName);
					if(obj!=null)
					{	
						obj.value=m_Wnd.document.frames[1].forms[0].policynumber.value;
						obj.cancelledvalue=m_Wnd.document.frames[1].forms[0].policynumber.value;
					}
				}
				else
				{
					if(m_Wnd != null)
						m_Wnd.close();
					alert("Policy with control number " + m_sNoDataId + " is not found in Database.");
					setDataChanged(false);
				}
				if(m_Wnd != null)
					m_Wnd.close();
				m_Wnd=null;
			}
			else
			{
				self.lookupCallback="numLookupCallback";
				
				/*self.setTimeout("m_Wnd=window.open('getpolicydata.asp?policyid="+sId+
					"','progressWnd','width=400,height=150,top="+((screen.availHeight-150)/2)+
					",left="+((screen.availWidth-400)/2)+"');",100);*/
				self.setTimeout("m_Wnd=window.open('home?pg=riskmaster/Search/SearchRender&amp;entityid="+sId+"&lookuptype="+m_LookupType+
					"','progressWnd','width=400,height=150,top="+((screen.availHeight-150)/2)+
					",left="+((screen.availWidth-400)/2)+"');",100);
			}
		}
		m_AllowRestore= true;
		return true;
}

function ClaimChosen()
{
	document.forms[0].sysCmd.value="0";
	document.forms[0].submit();
}

function formHandler(sLinkTo)
{
	if(ConfirmSave()) {
		if(!validateForm())
			return false;
		else{
			document.forms[0].sysCmdConfirmSave.value=1;
			var obj=eval("document.forms[0].sysCmdQueue");
			if(obj!=null)
				obj.value="formHandler('"+sLinkTo+"');";
			self.setTimeout('document.forms[0].submit();',200);
			return false;
		}
	}		
		
	CloseProgressWindow();

	self.setTimeout("urlNavigate('"+sLinkTo+"',10)");
		
	return true;
}
//^^Rajeev 12/21/2002 -- MITS#3899

function showWarning(chk,sWarning)
{
	if(chk.checked==true){
		if (!self.confirm(sWarning))
			return false;
		else
			return true;
	}
}
//Arnab: MITS-10751 - Added new function for the clear checkbox to implement business rule
function setVoidCheckStatus(chkPrimary, chkTarget, sMessage)
{       
        var chk = eval('window.document.forms[0].' + chkTarget);
        if(chk != null)
        {
            chkPrimary.checked = showWarning(chkPrimary, sMessage)
            if(chkPrimary.checked)
                chk.disabled = true;
            else
                chk.disabled = false; 
        }
}
//MITS-10751 End

function goToDoc(sURL)
{
	window.navigate(sURL)
}

function UpdateAccountId()
{
	var selectedIndex;
	
	if(haveProperty(document.forms[0].cboBankAccount,"selectedIndex"))
	{
		selectedIndex = document.forms[0].cboBankAccount.selectedIndex;
		document.forms[0].hAccountId.value = document.forms[0].cboBankAccount[selectedIndex].value;
	}
	//alert(selectedIndex);
	//alert(document.forms[0].hAccountId.value);
	return true;
}

function getdbDate(sDate)
{
	if(sDate=="")
		return "";
	var d=new Date(sDate);
	var sYear=new String(d.getFullYear());
	var sMonth=new String(d.getMonth()+1);
	var sDay=String(d.getDate());
	if(sMonth.length==1)
		sMonth="0" + sMonth;
	if(sDay.length==1)
		sDay="0" + sDay;
	return sYear+sMonth+sDay;
	
}

//added by raman

function GetLOB()
{

	var objLOBFormEle = eval('document.forms[0].lineofbusinesscode');
	if(objLOBFormEle != null)
		return document.forms[0].lineofbusinesscode.value;
				
	if (!haveProperty(document.forms[0], "formname"))
		return '';
		
	sFormName = String(eval('document.forms[0].formname.value'));
	switch(sFormName.substr(sFormName.length-2,2))
	{
		case 'gc':
			return "241";
		case 'va':
			return "242";
		case 'wc':
			return "243";
		case 'di':
			return "844";
	}
	return '';
}

function lookupTextChanged(objField)
{
//	alert("lookupTextChanged");
	if (m_LookupBusy)
	{
		window.alert("The application is busy looking up data for a previous field.\nPlease wait for the results before modifying additional fields.");
		objField.value= objField.cancelledvalue;
		return false;
	}
	setDataChanged(true);
	m_LookupTextChanged=true;
	return true;
}


function selectCode(sCodeTable,sFieldName,fieldtitle)
{
	if(m_codeWindow!=null)
		m_codeWindow.close();
	
	m_sFieldName=sFieldName;
	/*MIts 9251 passing form name to identify processing at the backend and may be for future filers*/
	var sFormName;
	//zalam mits:-11188 03/19/2008			
	var sTriggerDate="";
	var sOrgEid="";
	var sEventDate="";	
	if(eval('(document.getElementById("orgEid"))') !=null)
	    sOrgEid=(document.getElementById("orgEid").value);	
	//zalam mits:-11188 03/19/2008 :End
	
	if (eval('document.forms[0].formname')!=null)
		sFormName=document.forms[0].formname.value;
	else
		sFormName='';
	
	if(m_LookupTextChanged)
	{
	    //pmittal5  MITS:12289  06/19/08
			if (sFormName!='')
			{
				switch (sFormName)
				{
					case 'event':
						if(document.forms[0].dateofevent!=null)
						{
							sTriggerDate=getdbDate(document.forms[0].dateofevent.value);
						    sEventDate = sTriggerDate;
						}
						if(document.forms[0].depteid_cid!=null)
							sOrgEid=document.forms[0].depteid_cid.value;
						break;
						
					case 'claimgc':
					case 'claimwc':
					case 'claimva':
					case 'claimdi':
						if(document.forms[0].dateofclaim!=null)
							sTriggerDate=getdbDate(document.forms[0].dateofclaim.value);
						else
							sTriggerDate='';
						if(document.forms[0].ev_depteid_cid!=null)
							sOrgEid=getdbDate(document.forms[0].ev_depteid_cid.value);
						else
							sOrgEid=0;
					    if(document.forms[0].ev_dateofevent != null)
						    sEventDate=getdbDate(document.forms[0].ev_dateofevent.value);
						break;
						
					case 'policy':
						sTriggerDate=getdbDate(document.forms[0].effectivedate.value);
						sOrgEid=0;
						break;
					
					case 'employee':
						sOrgEid=document.forms[0].deptassignedeid_cid.value;
						break;
						
					default:
						var dateObj;
						if (document.forms[0].transdate)
							dateObj = eval(document.forms[0].transdate);
						else if (sCodeTable=="GL_ACCOUNTS" && sFieldName=="glaccountcode" )
						{		
						    var objfrmtemp=window.opener.forms[0].sys_departmenteid;
						    if (objfrmtemp!=null) sOrgEid = objfrmtemp.value;
						        else sOrgEid = 0;
						    dateObj = eval(window.opener.forms[0].transdate);
						    if (dateObj !=null)
						    {
							    sTriggerDate = getdbDate(dateObj.value);
						    }
						    else
						    { 
							    sTriggerDate=''; 
						    }						    
						    break;											    
						}
						else if (window.opener)
						 	dateObj = eval(window.opener.forms[0].transdate);						 						
						if (dateObj !=null)
						{
							sTriggerDate = getdbDate(dateObj.value);
						}
						else
						{ 
							sTriggerDate=''; 
						}						
				}
			}	
	
		var sSessionClaimId="0";
		var objClaimIdFormEle= eval('document.forms[0].claimid');
		if(objClaimIdFormEle != null)
		{
		    sSessionClaimId = document.forms[0].claimid.value;
		}
	    //End-pmittal5
	    
		var objFormElem=eval('document.forms[0].'+sFieldName);
		var sFind=objFormElem.value;
		
		objFormElem.value="";
		var objFormElem=eval('document.forms[0].'+sFieldName+"_cid");
		if(objFormElem != null)
		    objFormElem.value="0";
		
		//Track the Fields to restore in case of cancel.
		m_LookupCancelFieldList=sFieldName + "|"+sFieldName+"_cid";
	        //zalam mits:-11188 03/19/2008
		//m_codeWindow=RMX.window.open('home?pg=riskmaster/CodesList/quicklookup&amp;type=code.'+sCodeTable+"&amp;find="+sFind+"&amp;lob="+GetLOB(), 'codeWnd',

		//pmittal5 MITS 12369 08/28/08
    	if ( fieldtitle!="" && fieldtitle!="undefined" && fieldtitle!=null)
	    	m_sParentName=fieldtitle;
	    else
	        m_sParentName = "";	
		sCodeTable=escape(sCodeTable);		
		if(sCodeTable == 'orgh')
		{
	        m_codeWindow=RMX.window.open('/RiskmasterUI/UI/Codes/QuickLookup.aspx?type=code.'+sCodeTable+"&amp;find="+sFind+"&amp;lob="+GetLOB()+"&amp;orgeid="+sOrgEid+"&amp;formname="+sFormName+"&amp;triggerdate="+sTriggerDate+"&amp;eventdate="+sEventDate+"&amp;sessionclaimid="+sSessionClaimId+"&amp;orgLevel="+m_sParentName, 'codeWnd',
		    'width=500,height=290'+',top='+(screen.availHeight-290)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');			    
		}
		else
		{
		//End - pmittal5
            //pmittal5  MITS:12289  06/19/08
            //m_codeWindow=RMX.window.open('home?pg=riskmaster/CodesList/quicklookup&amp;type=code.'+sCodeTable+"&amp;find="+sFind+"&amp;lob="+GetLOB()+"&amp;orgeid="+sOrgEid, 'codeWnd',
		    //'width=500,height=290'+',top='+(screen.availHeight-290)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
	        m_codeWindow=RMX.window.open('/RiskmasterUI/UI/Codes/QuickLookup.aspx?type=code.'+sCodeTable+"&amp;find="+sFind+"&amp;lob="+GetLOB()+"&amp;orgeid="+sOrgEid+"&amp;formname="+sFormName+"&amp;triggerdate="+sTriggerDate+"&amp;eventdate="+sEventDate+"&amp;sessionclaimid="+sSessionClaimId, 'codeWnd',
		    'width=500,height=290'+',top='+(screen.availHeight-290)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
		}
		m_LookupTextChanged=false;
	}
	else
	{
	//added by ravi on 12th feb 03
		if(sCodeTable=='orgh')
		{
			var orglevel;
			var sFind = '';
			var objFormElem;
			
			objFormElem = eval('document.forms[0].'+sFieldName+'_cid');
			if(objFormElem != null)
				sFind = objFormElem.value;
				
			orglevel=getOrgLevel(fieldtitle);
			if(m_codeWindow!=null)
				m_codeWindow.close();
			if(sFind=='0')
			sFind='';
			//alert('org.asp?tablename='+orgfor+'&rowid='+getCurrentID()+'&lob='+orglevel+'&searchOrgId='+sFind);
			m_codeWindow=RMX.window.open('home?pg=riskmaster/OrgHierarchy/Org&amp;tablename='+sFormName+'&rowid='+getCurrentID()+'&lob='+orglevel+'&searchOrgId='+sFind,'Table','width=700,height=600'+',top='+(screen.availHeight-500)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');			
		}
		else
		{
		        //zalam mits:-11188 03/19/2008
			//var sTriggerDate="";
			//var sOrgEid="";
			//var sEventDate="";
	                //zalam mits:-11188 03/19/2008 :End
			
			if (sFormName!='')
			{
				switch (sFormName)
				{
					case 'event':
						if(document.forms[0].dateofevent!=null)
						{
							sTriggerDate=getdbDate(document.forms[0].dateofevent.value);
						    sEventDate = sTriggerDate;
						}
						if(document.forms[0].depteid_cid!=null)
							sOrgEid=document.forms[0].depteid_cid.value;
						break;
						
					case 'claimgc':
					case 'claimwc':
					case 'claimva':
					case 'claimdi':
						if(document.forms[0].dateofclaim!=null)
							sTriggerDate=getdbDate(document.forms[0].dateofclaim.value);
						else
							sTriggerDate='';
						if(document.forms[0].ev_depteid_cid!=null)
							sOrgEid=getdbDate(document.forms[0].ev_depteid_cid.value);
						else
							sOrgEid=0;
					    if(document.forms[0].ev_dateofevent != null)
						    sEventDate=getdbDate(document.forms[0].ev_dateofevent.value);
						break;
						
					case 'policy':
						sTriggerDate=getdbDate(document.forms[0].effectivedate.value);
						sOrgEid=0;
						break;
					
					case 'employee':
						sOrgEid=document.forms[0].deptassignedeid_cid.value;
						break;
						
					default:
						/* in case of funds it will fall through this which will pass the same values as before except the fact that now formname will be 'funds' instead of 'undefined' - mits 9251*/					
						var dateObj;
						if (document.forms[0].transdate)
							dateObj = eval(document.forms[0].transdate);
						else if (sCodeTable=="GL_ACCOUNTS" && sFieldName=="glaccountcode" )
						{		
						    //rsolanki2 :- CodeList not populating properly(when effective org hier added ) for funds-GL 
						    var objfrmtemp=window.opener.forms[0].sys_departmenteid;
						    if (objfrmtemp!=null) sOrgEid = objfrmtemp.value;
						        else sOrgEid = 0;
						    dateObj = eval(window.opener.forms[0].transdate);
						    if (dateObj !=null)
						    {
							    sTriggerDate = getdbDate(dateObj.value);
						    }
						    else
						    { 
							    sTriggerDate=''; 
						    }						    
						    break;											    
						}
						else if (window.opener)
						 	dateObj = eval(window.opener.forms[0].transdate);						 						
						if (dateObj !=null)
						{
							sTriggerDate = getdbDate(dateObj.value);
						}
						else
						{ 
							sTriggerDate=''; 
						}						
						//sOrgEid=0; zalam mits:-11188 03/19/2008						
				}
			}
			if ( fieldtitle!="")
				m_sParentName=fieldtitle;

			var sSessionLOB="0";
			var objLOBFormEle = eval('document.forms[0].lineofbusinesscode');
			if(objLOBFormEle != null)
			{
				sSessionLOB = document.forms[0].lineofbusinesscode.value;
			}
			//MITS:9499 by umesh
			var sSessionClaimId="0";
			var objClaimIdFormEle= eval('document.forms[0].claimid');
			if(objClaimIdFormEle != null)
			{
				sSessionClaimId = document.forms[0].claimid.value;
			}
			m_codeWindow=RMX.window.open('home?pg=riskmaster/CodesList/getcode&amp;code='+sCodeTable+"&amp;lob="+GetLOB()+"&amp;formname="+sFormName+"&amp;triggerdate="+sTriggerDate+"&amp;orgeid="+sOrgEid+"&amp;sessionclaimid="+sSessionClaimId + "&amp;sessionlob="+sSessionLOB+"&amp;eventdate="+sEventDate,'codeWnd',
				'width=500,height=300'+',top='+(screen.availHeight-300)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
		}
	}
	
	m_LookupTextChanged=false;
	return false;
}

function getCurrentID()
{
	if(m_FormID=="")
		return 0;
	var objElem=eval('document.forms[0].'+m_FormID);
	if(objElem!=null)
		return objElem.value;
	return 0;
}


function getOrgLevel(fieldtitle)
{
var orglevel;
newstr=new String(fieldtitle);
	switch(newstr.toUpperCase())
			{
				case 'DEPARTMENT':
					orglevel='8';
					break;
				case 'FACILITY':
					orglevel='7';
					break;
				case 'LOCATION':
					orglevel='6';
					break;
				case 'DIVISION':
					orglevel='5';
					break;
				case 'REGION':
					orglevel='4';
					break;
				case 'OPERATION':
					orglevel='3';
					break;
				case 'COMPANY':
					orglevel='2';
					break;
				case 'CLIENT':
					orglevel='1';
					break;
				default:
					orglevel='0';
			}
		return orglevel;
	
}

function getCurrencyValue(strValue)
{
	strValue = strValue.toString().replace(/\$|\,/g,'');

	if( isNaN(parseFloat(strValue)) )
		strValue = "0.00";
	
	return strValue;
}

function formatCurrency(strValue, strSymbol)
{
	if( strSymbol == null )
		strSymbol = "$";
		
	strValue = strValue.toString().replace(/\$|\,/g,'');
	if( isNaN(parseFloat(strValue)) )
		strValue = "0.00";
	
	//Get sign (negtive or positive)
	var dAmount = parseFloat(strValue);
	var sSign = "";
	if(dAmount < 0.0)
	{
		sSign = "-";	
		dAmount = Math.abs(dAmount)
	}
	
	//round to 2 decimal points
	dAmount = Math.floor(dAmount*100+0.50000000001);
	var cents = dAmount%100;
	dAmount = Math.floor(dAmount/100).toString();
	if(cents<10)
		cents = "0" + cents;
		
	//separate thousand by comma
	var sFormatedValue = "";
	var sAmount = dAmount.toString();
	var iTotalDigitals = sAmount.length;
	var iStartPosition = 0;
	var iLength = 3;
	var iLoops = Math.floor((iTotalDigitals+2)/3);
	for(var i=0; i<iLoops; i++)
	{
		if( sFormatedValue != "" )
			sFormatedValue = "," + sFormatedValue;
		iStartPosition = iTotalDigitals-(i+1)*3;
		if( iStartPosition < 0 )
		{
			iLength += iStartPosition;
			iStartPosition = 0;
		}
		sFormatedValue = sAmount.substr(iStartPosition, iLength) + sFormatedValue;
	}
	
	return sSign + strSymbol + sFormatedValue + "." + cents;
}


function formatDate(sParamDate)
{
	var sDateSeparator;
	var iDayPos=0, iMonthPos=0;
	var d=new Date(1999,11,22);
	var s=d.toLocaleString();
	var sRet="";
	var sDate=new String(sParamDate);
	if(sDate=="")
		return "";
	iDayPos=s.indexOf("22");
	iMonthPos=s.indexOf("11");
	//if(IE4)
	//	sDateSeparator=s.charAt(iDayPos+2);
	//else
		sDateSeparator="/";
	if(iDayPos<iMonthPos)
		sRet=sDate.substr(6,2) + sDateSeparator + sDate.substr(4,2) + sDateSeparator + sDate.substr(0,4);
	else
		sRet=sDate.substr(4,2) + sDateSeparator + sDate.substr(6,2) + sDateSeparator + sDate.substr(0,4);
	return sRet;
}

function codeLostFocus_updated(sCtrlName)
{
	var objFormElem=eval('document.forms[0].'+sCtrlName);
	if(objFormElem.value=="")
	{
		objFormElem=eval('document.forms[0].'+sCtrlName+"_cid");
		objFormElem.value="0";
		m_LookupTextChanged=false;
	}
	else
	{
		if(m_LookupTextChanged)
		{
			var objFormElemButton=eval('document.forms[0].'+sCtrlName+"btn");
			objFormElemButton.click()
		}
		else
		{
			objFormElem=eval('document.forms[0].'+sCtrlName+"_cid");
			if(objFormElem.value=="0" || objFormElem.value=="")
			{
				var objFormElem=eval('document.forms[0].'+sCtrlName);
				objFormElem.value="";
			}
		}
	}
	
	return true;
}

function RequestCancel()
{
	var i;
	var obj;
	var arrElements;
	//alert("Cancel Requested: "+m_LookupCancelFieldList+"\nbusy?"+window.m_LookupBusy);
	if(self.document.forms[0] == null)
		return false;
	arrElements=String(m_LookupCancelFieldList).split("|");
	
	for(i=0;i<arrElements.length;i++)
	{
		obj=null;
		
		if (arrElements[i]!="")
		{
			obj = eval('window.document.forms[0].'+arrElements[i]);
			if (obj!=null)
				waitFor("(!window.m_LookupBusy)",'window.DoCancel("'+obj.id+'");');
		}
	}
	return false;
}

function DoCancel(sName)
{
	var obj=eval("document.forms[0]."+sName);
	
	if (obj.cancelledvalue==null)
		obj.value=obj.defaultValue;
	else
		obj.value=obj.cancelledvalue;
	return true;
}

function waitFor(cond,block)
{
	if(eval(cond))
		eval(block);
	else
		self.setTimeout("waitFor('" +cond+"','"+block+"');",10);
	return true;
}

function SaveRecord()
{
	//alert("saverecord");
	if(!m_DataChanged)
		return false;
		
	if(!validateForm())
		return false;
	SetApplicationBusy();
	document.forms[0].ouraction.value="savetransaction";
	document.forms[0].recordsaved.value="true";
    //pmahli MITS 10811 11/30/2007 - Start
    OpenProgressWindow();
    //pmahli MITS 10811 11/30/2007
	return true;
	
}


function SetApplicationBusy()
{
	m_IsAppBusy = true;
}


function clickEventHandler(funcName)
{
	//check if it's busy doing saving
	if( m_IsAppBusy == true )
	{
		alert('Application is busy saving record. Please try again later.');
		return false;
	}
	
	var callargs = funcName + "(";
	for (var i=1;i < arguments.length;i++)
	{
		callargs = callargs+"arguments["+i+"]";
		if(i < arguments.length-1) callargs += ",";
	}
	callargs += ")";

	return eval(callargs);
}


function newTransaction()
{
	document.forms[0].functiontocall.value='FundManagementAdaptor.GetNewTransaction';
	return true;
}

function saveTrasaction()
{
	document.forms[0].functiontocall.value='FundManagementAdaptor.SaveTransaction'; 
	return SaveRecord();
}

function firstTransaction()
{
	document.forms[0].functiontocall.value='FundManagementAdaptor.GetFirstTransaction';
	return true;
}

function PrintTrans()
{
	var sId=document.forms[0].TransId.value;
	if(sId<=0 || sId=="")
	{
		self.alert("You are working on the new record and this functionality is available for existing records only. Please save the data and try again.");
		return false;
	}
	var sQueryString = 'TransId=' + document.getElementById("TransId").value;
	var m_windowCallToXPL = RMX.window.open('home?pg=riskmaster/Funds/TransactionReport&amp;' + sQueryString, 2,'width=500,height=300,top='+(screen.availHeight-300)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
	return false;
}
function prevTransaction()
{
	document.forms[0].functiontocall.value='FundManagementAdaptor.GetPreviousTransaction';
	return true;
}

function nextTransaction()
{
	document.forms[0].functiontocall.value='FundManagementAdaptor.GetNextTransaction';
	return true;
}

function lastTransaction()
{
	document.forms[0].functiontocall.value='FundManagementAdaptor.GetLastTransaction';
	return true;
}

function claimLookup(sFieldName,sTableId, sViewId, sFieldMark, lookupType)
{
	m_ClaimIDLookup=true; 
	return lookupData(sFieldName,sTableId, sViewId, sFieldMark, lookupType);
}

function PrintEOB()
{
	this.document.callback="OnReportPrinted" ;
	if(wnd == null)
		{
//			var wnd=RMX.window.open("home?pg=riskmaster/Funds/PrintEOB&amp;transid=" + document.forms[0].all("TransId").value 
//			+ "&silent=" + "false" ,"PrintEOB",
//			"width=500,height=170,top="+(screen.availHeight-400)/2+",left="+(screen.availWidth-500)/2+",resizable=yes,scrollbars=yes");
			var wnd=window.open("PrintEOB.aspx?transid=723" + "&silent=" + "false" ,"PrintEOB",
			"width=500,height=170,top="+(screen.availHeight-400)/2+",left="+(screen.availWidth-500)/2+",resizable=yes,scrollbars=yes");
		}
			
	return false;
}

function SendRegarding()
{

	this.document.callback="OnEmailSent" ;

	if(wnd == null)
					{
					var wnd=RMX.window.open("home?pg=riskmaster/EmailDocuments/EmailDocuments&amp;Regarding=Funds Management - Payments" ,"SendRegarding",
			"width=600,height=500,top="+(screen.availHeight-500)/2+",left="+(screen.availWidth-600)/2+",resizable=yes,scrollbars=yes");
					}

	return false;
}


// vaibhav has added the code for PrintCheck functionality.
function PrintCheck()
{
	var sQueryString = "" ;
	var width = 460;
	var height = 240 ;
	//abisht Commented the code to work with dummy .htm page.Need to uncomment this.
	var sId=document.forms[0].TransId.value;
	if(sId<=0 || sId=="")
	{
		self.alert("You are working on the new record and this functionality is available for existing records only. Please save the data and try again.");
		return false;
	}
	
	if( m_DataChanged )
	{
		alert( "Data has changed with this record. Save it before printing check." );
		return false ;
	}

    // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
    // So introducing Distribution Type and removing out EFTPayment
    //JIRA:438 START: ajohari2
	//var IsEFTPayment = "false";
    
	//if(document.getElementById("IsEFTPayment")!=null)
	//{
	//    IsEFTPayment = document.getElementById("IsEFTPayment").checked;
	//}
	var iDistrbutionType = window.document.forms[0].ddlDistributionTypePre.value;
    

	sQueryString = "transid=" + document.getElementById("TransId").value + "&IncludeClaimantInPayee=" + document.getElementById("chkSettlement").value+"&totalamount=" + document.getElementById("totalcollection").value + "&accountname=" + document.forms[0].cboBankAccount.options[document.forms[0].cboBankAccount.selectedIndex].text ;
	sQueryString = sQueryString + /*"&EFTPayment=" + bchkEFTPayment +*/ "&DistributionType=" + iDistrbutionType;
    //JIRA:438 End:
    // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
    // So introducing Distribution Type and removing out EFTPayment
	//sQueryString = "transid=" + document.getElementById("TransId").value + "&IncludeClaimantInPayee=" + document.getElementById("chkSettlement").value+"&totalamount=" + document.getElementById("totalcollection").value + "&accountname=" + document.forms[0].cboBankAccount.options[document.forms[0].cboBankAccount.selectedIndex].text ;
	//sQueryString = "transid=499&IncludeClaimantInPayee=true&totalamount=50&accountname=Low Balance Test";
	if(wnd == null)
	{
		//var wnd=showModalDialog("home?pg=riskmaster/Funds/PrintSingleCheckFrame&" + sQueryString ,null , 'dialogWidth:' + width + 'px;dialogHeight:'+ height +'px;dialogtop:'+(screen.availHeight-height)/2+';dialogleft:'+(screen.availWidth-width)/2+';resizable:no;scrollbars:no;status:no;center:yes;');
		var wnd=showModalDialog("PrintSingleCheckFrame.aspx?" + sQueryString ,null , 'dialogWidth:' + width + 'px;dialogHeight:'+ height +'px;dialogtop:'+(screen.availHeight-height)/2+';dialogleft:'+(screen.availWidth-width)/2+';resizable:no;scrollbars:no;status:no;center:yes;');
	}
	
	if( wnd == "ok" )
	{
		var wnd=RMX.window.open('csc-Theme/riskmaster/common/html/progress.html','progressWnd',
			'width=400,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-400)/2);
		self.parent.wndProgress=wnd;
		return true ;
	}
	else
		return false ;
}
//skhare7:MITS 23664
function PrintedChecksImage()
{

	var sQueryString = "" ;
	var width = 460;
	var height = 240 ;
	//abisht Commented the code to work with dummy .htm page.Need to uncomment this.
	var sId=document.forms[0].TransId.value;
	if(sId<=0 || sId=="")
	{
		self.alert("You are working on the new record and this functionality is available for existing records only. Please save the data and try again.");
		return false;
	}
	
	if( m_DataChanged )
	{
		alert( "Data has changed with this record. Save it before printing check." );
		return false ;
	}
		 //averma62 MITS 26999	
	sQueryString = "CheckId=" + document.getElementById("TransId").value +  "&IncludeClaimantInPayee=false" +"&AccountId=" + document.getElementById("AccountNumber").value + "&CheckDate=" + document.getElementById("txtCheckDate").value +"&CheckStockId=" + document.forms[0].cboStocks.options[document.forms[0].cboStocks.selectedIndex].value + "&FirstCheck=" + document.getElementById("CheckNumber").value +"&PrintMode=" + document.getElementById("PrintMode").value + "&IsBatchPrint=SingleCheck" ;
	//sQueryString = "transid=499&IncludeClaimantInPayee=true&totalamount=50&accountname=Low Balance Test";
	if(wnd == null)
	{
		
	
	//var returnval = showModalDialog("home?pg=riskmaster/PrintChecks/PrintChecksBatchFrame&" + sQueryString ,null , 'dialogWidth:' + width + 'px;dialogHeight:'+ height +'px;dialogtop:'+(screen.availHeight-height)/2+';dialogleft:'+(screen.availWidth-width)/2+';resizable:no;scrollbars:no;status:no;center:yes;');
	var returnval = showModalDialog("PrintChecksBatch.aspx?" + sQueryString ,null , 'dialogWidth:' + width + 'px;dialogHeight:'+ height +'px;dialogtop:'+(screen.availHeight-height)/2+';dialogleft:'+(screen.availWidth-width)/2+';resizable:no;scrollbars:no;status:no;center:yes;');
	}
	
	if( wnd == "ok" )
	{
		var wnd=RMX.window.open('csc-Theme/riskmaster/common/html/progress.html','progressWnd',
			'width=400,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-400)/2);
		self.parent.wndProgress=wnd;
		return true ;
	}
	else
		return false ;
}
//skhare7:MITS 23664 End
function PrintSingleCheckOnLoad()
{
    var IEbrowser = false || !!document.documentMode; //RMA-10491
    if(document.forms[0].txtError.value != "")
    {
        alert(document.forms[0].txtError.value)
        window.close();
    }
    if(document.forms[0].PrintMode.value == "PR")
    {
    document.getElementById("Ok").value="Print Preview";
    }
    //Added by Amitosh For Eft Payment
     var isEftAcc = window.document.forms[0].tbIsEFTAccount.value;

    
	if((document.forms[0].cboStocks.length == 0) && (isEftAcc == "false"))
	{		
		alert( "There are no check stocks defined for this bank account. You will not be able to print any checks.");
		window.document.forms[0].Ok.disabled = true ;
	}
	
	if( window.document.forms[0].functiontocall.value == "PrintChecksAdaptor.UpdateStatusForPrintedChecks"  )
	{
	    window.returnValue = "ok" ;
	    if (!IEbrowser){
	        window.opener.document.forms[0].SysCmd.value= 0;
	        window.opener.document.forms[0].__EVENTTARGET.value = "PrintCheck";
	        window.opener.document.forms[0].submit();
	    }
		window.close();
	}
	
	if( window.document.forms[0].functiontocall.value == "BillingAdaptor.PostPrintDisb"  )
	{
		window.returnValue = "ok" ;
		window.close();
	}
	 //MITS 12301 Raman Bhatia
     //Allow Post Dated Checks checkbox needs to be implemented
     //Check Date should be disabled in such a scenario
                
	if(document.forms[0].AllowPostDate != null)
	{
	    if(document.forms[0].txtCheckDate != null && document.forms[0].AllowPostDate.value == "False")
	    {
	        document.forms[0].txtCheckDate.disabled = true;
	    }
	    if(document.forms[0].txtCheckDatebtn != null && document.forms[0].AllowPostDate.value == "False")
	    {
	        document.forms[0].txtCheckDatebtn.disabled = true;
	    }
	}
	 if (window.document.forms[0].txtIsPrinterSelected.value == "false") {
        document.getElementById('formdemotitle').innerText = "You need to select a default printer in payment parameter setup or change the option 'Print Direct To Printer' off.";
        window.document.forms[0].Ok.disabled = true;  
	 }

    // RMA-10491 - Start : bkuzhanthaim :ShowModalDialog Issue in Chrome
	 var PrintBatchOk = window.document.forms[0].hdnPrintBatchOk.value;
	 window.document.forms[0].hdnPrintBatchOk.value = "";
	 if(window.parent.document.forms[0].hdnPrintBatchOk.value == "Processed")  window.parent.document.forms[0].submit();
	 if (!IEbrowser && PrintBatchOk !="")
	 {
	     var arrCheckDetail = PrintBatchOk.split("||");
	     var iStartNum = arrCheckDetail[0];
	     var iEndNum = arrCheckDetail[1];
	     var sPrintCheckDetails = arrCheckDetail[3];
	     var hdnZeroChecksValue = 0;
	     window.document.forms[0].PrintCheckDetails.value = arrCheckDetail[3];
	     if (arrCheckDetail.length >= 6) {
	         hdnZeroChecksValue = arrCheckDetail[5];
	     }
	     var sPrintMode = arrCheckDetail[4];
	     if (sPrintMode != "PR") {	
	         if (hdnZeroChecksValue == "0") {
	             if (confirm("You have printed checks in the range " + iStartNum + " through " + iEndNum + ". Press 'Ok' to record all of checks as having printed successfully. Press 'Cancel' if check printing did not print all checks correctly.")) {
	                 if (window.parent.document.forms[0].Source.value == "Disbursement") {
	                     window.parent.document.forms[0].hdnPrintBatchOk.value = "Processed";
	                     window.document.forms[0].functiontocall.value = "BillingAdaptor.PostPrintDisb";
	                     window.document.forms[0].method = "post";
	                     window.document.forms[0].submit();
	                 }
	                 else {
	                     //tanwar2 - added if condition
	                     if (hdnZeroChecksValue == "2") {
	                         window.close();
	                     }
	                     else {
	                         window.document.forms[0].functiontocall.value = "PrintChecksAdaptor.UpdateStatusForPrintedChecks";
	                         window.document.forms[0].method = "post";
	                         window.document.forms[0].submit();
	                     }
	                 }
	             }
	             else {
	                 // All Checks does not print successfully. Ask the check number, up to where the checks has printed successfully.
	                 width = 475;
	                 height = 280;
	                 sQueryString = "";
	                 sQueryString = "StartNum=" + iStartNum + "&EndNum=" + iEndNum;
	                 window.open("ChecksFailedToPrint.aspx?" + sQueryString, self,'width='+ width +',height='+ height +',top=' + (screen.availHeight - height) / 2 + ',left=' + (screen.availWidth - width) / 2 + ',resizable=no,scrollbars=no'); 
	                 showOverlayPopup();
	             }
	         }
	     }
	 }
    // RMA-10491 - End
}

function PrintSingleCheckCancel()
{
    //bkuzhanthaim : RMA-10491 : PrintSingleCheckFrame is loading as child frame of PrintDisbursement
    if(window.parent.document.forms[0].action.search('PrintDisbursement') != -1)
        window.parent.close();
    else
        window.close();
}

function PrintSingleCheckOk()
{
    var iFirstCheck = document.getElementById("CheckNumber").value ;
      //skhare7:MITS 23664
    var sPrintMode=document.getElementById("PrintMode").value ;
    // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
    // So introducing Distribution Type and removing out EFTPayment
    //JIRA:438 START: ajohari2
    //var sPrintEFTPayment =   "false";
    //if( document.getElementById("PrintEFTPayment")!= null)
    //{
    //    sPrintEFTPayment =  document.getElementById("PrintEFTPayment").value ;
    //}
    //JIRA:438 End: 
    var DistributionType = document.getElementById('DistributionTypeId').value;
    // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
    // So introducing Distribution Type and removing out EFTPayment
    //skhare7:MITS 23664 End
	if( isNaN(iFirstCheck) || iFirstCheck.indexOf(".") != -1 )
	{
		alert( "Invalid First Check Number " );
		document.getElementById("CheckNumber").focus();
		return false ;
	}

	var sQueryString = "" ;
	var width = 475 ;
	var height = 280 ;
	//skhare7:MITS 23664
     //averma62 MITS 26999
	sQueryString = "CheckId=" + document.getElementById("TransId").value +  "&IncludeClaimantInPayee=false" +"&AccountId=" + document.getElementById("AccountNumber").value + "&CheckDate=" + document.getElementById("txtCheckDate").value  + "&FirstCheck=" + document.getElementById("CheckNumber").value +"&PrintMode=" + document.getElementById("PrintMode").value + "&IsBatchPrint=SingleCheck" ;
    	//skhare7:MITS 23664 End
        //Added by Amitosh for EFT
        if(document.forms[0].cboStocks.selectedIndex >-1)
            sQueryString= sQueryString+ "&CheckStockId=" + document.forms[0].cboStocks.options[document.forms[0].cboStocks.selectedIndex].value;
    // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
    // So introducing Distribution Type and removing out EFTPayment
    //JIRA:438 START: ajohari2
        //if(sPrintEFTPayment!= "")
        //{
        //    sQueryString= sQueryString+ "&PrintEFTPayment=" +sPrintEFTPayment;
        //}

        if(DistributionType != '0')
        {
            sQueryString = sQueryString+ "&DistributionType=" +DistributionType;
        }
    //JIRA:438 End: 
    //End Amitosh
    // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
    // So introducing Distribution Type and removing out EFTPayment
	//var returnval = showModalDialog("home?pg=riskmaster/PrintChecks/PrintChecksBatchFrame&" + sQueryString ,null , 'dialogWidth:' + width + 'px;dialogHeight:'+ height +'px;dialogtop:'+(screen.availHeight-height)/2+';dialogleft:'+(screen.availWidth-width)/2+';resizable:no;scrollbars:no;status:no;center:yes;');
    // bkuzhanthaim: RMA- 10491 Policy billing - Print - Show modal Starts
	//var returnval = showModalDialog("PrintChecksBatch.aspx?" + sQueryString ,null , 'dialogWidth:' + width + 'px;dialogHeight:'+ height +'px;dialogtop:'+(screen.availHeight-height)/2+';dialogleft:'+(screen.availWidth-width)/2+';resizable:no;scrollbars:no;status:no;center:yes;');
       var IEbrowser = false || !!document.documentMode; // At least IE6
       if (!IEbrowser) {
            popUpWin = window.open("PrintChecksBatch.aspx?" + sQueryString, null,'width='+ width +',height='+ height +',top=' + (screen.availHeight - height) / 2 + ',left=' + (screen.availWidth - width) / 2 + ',resizable=no,scrollbars=no');
            // aravi5 RMA-11806 User is allowed to move on other screen while print pop up is open on chrome starts
            //showOverlayPopup();
            if (window.parent.parent.document.getElementById("overlaydiv") == null) {
                var div = document.createElement('div');
                div.id = 'overlaydiv';
                div.className = 'overlay';
                div.style.display = 'block';
                window.opener.parent.parent.document.getElementById('cphHeaderBody').appendChild(div);
            }
            // aravi5 RMA-11806 User is allowed to move on other screen while print pop up is open on chrome ends
            return false ;
        }
        else {
            var returnval = showModalDialog("PrintChecksBatch.aspx?" + sQueryString ,null , 'dialogWidth:' + width + 'px;dialogHeight:'+ height +'px;dialogtop:'+(screen.availHeight-height)/2+';dialogleft:'+(screen.availWidth-width)/2+';resizable:no;scrollbars:no;status:no;center:yes;');
        }
    // bkuzhanthaim: RMA- 10491 Policy billing - Print - Show modal Ends
	if( returnval == null || returnval == "||" )
		return false ;
		
	var arrCheckDetail = returnval.split( "||" );		
	var iStartNum = arrCheckDetail[0] ;
	var iEndNum = arrCheckDetail[1] ;
	var sPrintCheckDetails = arrCheckDetail[3];
    //tanwar2 - mits 30910 - start
    var hdnZeroChecksValue = 0; 
    if (arrCheckDetail.length>=6) {
        hdnZeroChecksValue = arrCheckDetail[5];
    }
    //tanwar2 - mits 30910 - end
    //skhare7:MITS 23664
    var sPrintMode= arrCheckDetail[4];
	//skhare7:MITS 23664 End
	window.document.forms[0].PrintCheckDetails.value = sPrintCheckDetails;
    //skhare7:MITS 23664
    if(sPrintMode!="PR")
{	//skhare7:MITS 23664 End
    //tanwar2 - mits 30910 - start
    if (hdnZeroChecksValue == "0") {
    //tanwar2 - mits 30910 - end
	if( confirm( "You have printed checks in the range " + iStartNum + " through " + iEndNum + ". Press 'Ok' to record all of checks as having printed successfully. Press 'Cancel' if check printing did not print all checks correctly."  ) )
	{
		// Checks has been printed Successfully. Call the web service to update the Check Status. 	
		window.document.forms[0].FirstFailedCheckNumber.value = "0" ; 	
	}
	else
	{
		// All Checks does not print successfully. Ask the check number, up to where the checks has printed successfully.
		width = 475 ;
		height = 280 ;
		sQueryString = "" ;
			
		sQueryString = "StartNum=" + iStartNum + "&EndNum=" + iEndNum ;				
		
		//var FirstFailedCheckNumber = showModalDialog( "home?pg=riskmaster/PrintChecks/ChecksFailedToPrint&" + sQueryString , null , "dialogHeight:"+height+"px;dialogWidth:"+width+"px;resizable:no;status:no;scroll:no;help:no;center:yes;" );			
		var FirstFailedCheckNumber = showModalDialog( "ChecksFailedToPrint.aspx?" + sQueryString , null , "dialogHeight:"+height+"px;dialogWidth:"+width+"px;resizable:no;status:no;scroll:no;help:no;center:yes;" );			
		
		if( FirstFailedCheckNumber == "" )
		{
			// Checks has been printed Successfully. Call the web service to update the Check Status.
			window.document.forms[0].FirstFailedCheckNumber.value = "0" ;
						
		}
		else
		{
			window.document.forms[0].FirstFailedCheckNumber.value = FirstFailedCheckNumber ;	
								
		}		
	}
    }
	    if(document.getElementById("Source").value == "Disbursement")
	    {
	        window.document.forms[0].functiontocall.value = "BillingAdaptor.PostPrintDisb" ;
	        window.document.forms[0].method = "post" ;
	        window.document.forms[0].submit();

	    }
	    else
	    {
            //tanwar2 - added if condition
	        if (hdnZeroChecksValue=="2") {
            window.close();
            }
            else{
	        window.document.forms[0].functiontocall.value = "PrintChecksAdaptor.UpdateStatusForPrintedChecks" ;
	        window.document.forms[0].method = "post" ;
	        window.document.forms[0].submit();
            }
	    }	
	    
}
}

function BackToClaim()
{
	if(wnd == null)
		{
			var wnd=window.location("fdm?SysFormName=claim&amp;SysFormId=" + document.forms[0].txtClaimNumber_cid.value + "&amp;SysCmd=0");
		}
		
	return false;

}

function gotoPaymentHistory()
{
	if(m_DataChanged)
	{
		if(ConfirmSave())
		 {
			if(!validateForm())
				return false;
			else
				{
					//need to save the record first.then if all server side validations are passed then need to redirect
					//to payment history else stay on transaction page..logic handled in pageloaded()
					SaveRecord();
					document.forms[0].redirecttopayhist.value = "true";
					return true;
				}
		}	
		else
		{
	    	// MITS 11816 : Anjaneya
		    m_DataChanged = false;
		    document.forms[0].redirecttopayhist.value = "true";
		    //document.forms[0].ouraction.value = "gotoPaymentHistory";
	            return true;
		}
	}
	else
	{
	    // MITS 11816 : Anjaneya
	    document.forms[0].redirecttopayhist.value = "true";
	    //document.forms[0].ouraction.value = "gotoPaymentHistory";
	    return true;
	}
}

function gotoreserves()
{
	var sClaimId=document.forms[0].txtClaimNumber_cid.value;
	var sClaimant=document.forms[0].claimanteid.value;
	var sClaimNumber=document.forms[0].txtClaimNumber.value;
	var sUnitID = document.forms[0].unitid.value;

	//if detail level tracking is off for GC and VA then claimant information should not be passed to reserve listing screen
	var lob = GetLOB();
	switch(lob)
	{
		case '241': if(document.forms[0].restracking.value == 1)
					{
						//03/09/2006 Raman Bhatia: TR 2469..In case record is readonly then Detail level tracking would not be combobox
						if(document.getElementById("cboclaimant").options)
						{
							selIndex = document.getElementById("cboclaimant").selectedIndex;
							sClaimant = document.getElementById("cboclaimant").options[selIndex].value.substring(1);
						}
						else
							sClaimant = document.getElementById("cboclaimant").value;
					}
					else
						sClaimant = "";
					break;
		
		case '242': if(document.forms[0].restracking.value == 0)
						sClaimant = "";
					break;
		
		case '243': //03/09/2006 Raman Bhatia: TR 2469..In case record is readonly then Detail level tracking would not be combobox
					if(document.getElementById("cboclaimant").options)
					{
						selIndex = document.getElementById("cboclaimant").selectedIndex;
						sClaimant = document.getElementById("cboclaimant").options[selIndex].value.substring(1);
					}
					else
						sClaimant = document.getElementById("cboclaimant").value;
					break;
					
		case '844': //03/09/2006 Raman Bhatia: TR 2469..In case record is readonly then Detail level tracking would not be combobox
					if(document.getElementById("cboclaimant").options)
					{
						selIndex = document.getElementById("cboclaimant").selectedIndex;
						sClaimant = document.getElementById("cboclaimant").options[selIndex].value.substring(1);
					}
					else
						sClaimant = document.getElementById("cboclaimant").value;
					break;
		
		default:    break;
	}
	
	if(m_DataChanged)
	{
		if(ConfirmSave())
		 {
			if(!validateForm())
				return false;
			else
				{
					//need to save the record first.then if all server side validations are passed then need to redirect
					//to reserves else stay on transaction page..logic handled in pageloaded()
					SaveRecord();
					document.forms[0].redirecttoreserves.value = "true";
					self.setTimeout('document.forms[0].submit();',200);
					return false;
				}
		}	
	}
	//need to simply move to reserve listing page as either data has not changed or user does not want to save record 
	if (sUnitID!="0")
		RMX.urlNavigate('home?pg=riskmaster/Reserves/ReserveListing&ClaimId='+sClaimId+'&UnitID='+sUnitID+'&ClaimNumber='+sClaimNumber);
	else
		RMX.urlNavigate('home?pg=riskmaster/Reserves/ReserveListing&ClaimId='+sClaimId+'&ClaimantId='+sClaimant+'&ClaimNumber='+sClaimNumber);
	return false;
}

function modifycheckstatus(objCtrl)
{
  //Parijat: MITS 10752	
	if(document.getElementById("IsCollection").value!='True')
	{
	    UpdatePrintBtn();
	if(objCtrl.value!='')
	{
	//Abhishek MITS 11072
		if(document.forms[0].txtStatusCode != null)
		{
		 document.forms[0].txtStatusCode.value = "P Printed";
		//Abhishek MITS 12129 start
		if(document.forms[0].txtStatusCode_hdn != null)
		{
		 document.forms[0].txtStatusCode_hdn.value = "P Printed";
		}
		//MITS 12129 end

		 document.forms[0].txtStatusCode_cid.value = document.forms[0].printedstatuscode.value ;

		    }
	}
	}
	else
	{
	    var chkFunds = eval('document.forms[0].Cleared');
	    if(chkFunds != null && (document.forms[0].txtCheckNumber.value > 0 ) )
	    {	
		    //Enable Cleared check box for collections screen
		    chkFunds.disabled = false;
	    }
	    else if(chkFunds != null && (document.forms[0].txtCheckNumber.value == 0 || document.forms[0].txtCheckNumber.value == '' ) )	
	    {
	    chkFunds.checked = false;
	    chkFunds.disabled = true;
	    }
	}
}
function UpdatePrintBtn()
{
	//Abhishek MITS 11072
	if(document.forms[0].btnPrintCheck != null)
	{
	  if((document.forms[0].cboBankAccount.options.length >= 0) && (document.forms[0].TransId.value > 0) && (document.forms[0].txtCheckNumber.value == 0 || document.forms[0].txtCheckNumber.value == ''))
	  {
		document.forms[0].btnPrintCheck.disabled = false;
	  }
	  else
		document.forms[0].btnPrintCheck.disabled = true;
	}
	
}

// Anjaneya : MITS 9554

function ValidateDate()
{
    
   var sEffectiveDate   = document.getElementById("fromdate").value;
   var sExpirationDate  = document.getElementById("todate").value;
   var sEffdate = sEffectiveDate.substring(6,10)+sEffectiveDate.substring(0,2)+sEffectiveDate.substring(3,5);
   var sExpDate = sExpirationDate.substring(6,10)+sExpirationDate.substring(0,2)+sExpirationDate.substring(3,5);
   if(sExpirationDate != "" && sExpDate<sEffdate )
   {
        alert("From date must be less than To date");
        document.getElementById("fromdate").value ="";
        document.getElementById("todate").value = "";
   }
}

//MGaba2:MITS 15483: Moving it to supportscreens.js
////Added Rakhi for MITS 12389(Payment Detail Screen):START 
//function DateCompare()
//{
//   
//   var sEffectiveDate   = new Date(document.getElementById("fromdate").value);
//   var sExpirationDate  = new Date(document.getElementById("todate").value);
//   if(sExpirationDate<sEffectiveDate)
//   {
//        alert("From date must be less than To date");
//        document.getElementById("fromdate").value ="";
//        document.getElementById("todate").value = "";
//   }
//}
////Added Rakhi for MITS 12389(Payment Detail Screen):END

//Start by Shivendu for MITS 9518
function currencyLostFocus(objCtrl)
{
	
	
	var dbl=new String(objCtrl.value);
	
	// Strip and Validate Input
	// BSB Remove commas first so parseFloat doesn't incorrectly truncate value.	
	dbl=dbl.replace(/[,\$]/g ,"");
	
	if(dbl.length==0)
		return false;
	
	//Geeta 11/21/06 : Modified to fix Mits no. 8427 for Est. Collection 			
	if(dbl.indexOf('(') >= 0)
	{		
		if(dbl.indexOf('-') >= 0)
		{
			dbl=dbl.replace(/[\-\(\)]/g ,"");
		}
		else
		{
			dbl='-'+ dbl.replace(/[\(\)]/g ,"");	
		}
	}				
	else if(dbl.indexOf('-') >= 0)
	{
		dbl='-' + dbl.replace(/[\-]/g ,"");				
	} 		

	if(isNaN(parseFloat(dbl)))
	{
		objCtrl.value="";	
		return false;
	}	

	//round to 2 places
	dbl=parseFloat(dbl);
	dbl=dbl.toFixed(2);

	//Handle Full Integer Section (Formats in groups of 3 with comma.)
	var base = new String(dbl);
	base = base.substring(0,base.lastIndexOf('.'));
	var newbase="";
	var j = 0;
	var i=base.length-1;
	for(i;i>=0;i--)
	{	if(j==3)
		{
			newbase = "," + newbase;	
			j=0;

		}
		newbase = base.charAt(i) + newbase;	
		j++;
	}

	
	// Handle Fractional Part
	var str = new String(dbl);
	str = str.substring(str.lastIndexOf(".")+1);

	// Reassemble formatted Currency Value string.

	objCtrl.value= "$" + newbase + "." + str;
	return true;
}
//End by Shivendu for MITS 9518
function CheckForCheckStatus()
{	
	//Geeta 13/10/2006: function added for fixing MITS no 8232 for Clear check Box on Funds screen

	if(document.forms[0] !=null)
	{
		if(document.forms[0].formname !=null)		
		{			
			if(document.forms[0].formname.value == "funds")			
			{
				var sCodeText=eval('document.forms[0].txtStatusCode');
				var chkFunds = eval('document.forms[0].Cleared');
				var chkResubmit =  eval('document.forms[0].Resubmit');
				
				if(chkFunds != null && document.getElementById("IsCollection").value=='True')
				{	
					//Disable Cleared check box for collections screen
					chkFunds.disabled = true;
					//Parijat:Mits 10752 (cleared checkbox has to be enabled if there is value in the deposit amount
					if(document.getElementById("txtCheckNumber").value > 0)
					   chkFunds.disabled = false; 
					//pmahli BRS 11/7/2007
					chkResubmit.disabled = true;
					//pmahli

				}	
				else
				{				
					if(sCodeText !=null)
					{
						if(chkFunds != null)
						{
							//Check for the check status 							
							if(sCodeText.value == "P Printed")
							{					
								chkFunds.disabled = false;
							}
							else
							{
								chkFunds.disabled = true;
							}
						}
						//pmahli BRS 11/7/2007
						if(chkResubmit != null)
						{
							//Check for the check status 							
							if(sCodeText.value == "P Printed")
							{					
								chkResubmit.disabled = false;
							}
							else
							{
								chkResubmit.disabled = true;
							}
						}
						//pmahli
					}
				}
			}
		}
	}
}
// BRS FL Merge : Umesh
function ApplyBool(frmElt)
{   //debugger;
	
	var eltMirror = eval('document.forms[0].' + frmElt.id + "_mirror");
	eltMirror.value="";

	if(!frmElt.checked)
		eltMirror.value="False";
	
	setDataChanged(true);
}
// BRS FL Merge :End

//function Added by Shivendu for MITS 11052. 
function parseLong(objCtrl)
{
    
	if(objCtrl.value.length==0)
			return false;
	if(isNaN(parseFloat(objCtrl.value)))
		objCtrl.value="";
	else if(parseInt(objCtrl.value,10)<0)
		objCtrl.value=""; 
	else
	{
	            var firstPart = objCtrl.value.substring(0,9);
	            var secondPart = objCtrl.value.substring(9,18);
		        var firstPartString = new String(parseInt(firstPart,10));
		        var secondPartString = new String(parseInt(secondPart,10));
		        if(!isNaN(parseInt(firstPart,10))&& !isNaN(parseInt(secondPart,10)))
		        objCtrl.value= firstPartString.concat(secondPartString);
		        else  if(!isNaN(parseInt(firstPart,10))&& isNaN(parseInt(secondPart,10)))
		        objCtrl.value= firstPartString;
		        else if(isNaN(parseInt(firstPart,10))&& !isNaN(parseInt(secondPart,10)))
		        objCtrl.value= secondPartString;
		        else if(isNaN(parseInt(firstPart,10))&& isNaN(parseInt(secondPart,10)))
		        objCtrl.value= "";
	}
	return true;
}
//Shruti, 11889
function LockSuppWhenVoid()
{
    var inputs = document.getElementsByTagName("input");
    for (i = 0; i < inputs.length; i++)
    {
        if (inputs[i].id.substring(0,inputs[i].id.indexOf("_")) == "supp" && inputs[i].id.substring(inputs[i].id.length - 3,inputs[i].id.length) != "lst"
            && inputs[i].id.substring(inputs[i].id.length - 3,inputs[i].id.length) != "cid")
        {
            inputs[i].disabled = true;
	    }
    }
}
//pmittal5 MITS 12149 09/02/08  - Enabling "Add New" functionality for Payee: Last Name field.
function RequestRestore(sVal)
{
	var i;
	var obj;
	var objElements;
	if(self.document.forms[0] == null)
		return false;
	objElements=self.document.forms[0].elements;
	//Reset the "wait" object.
	m_AllowRestore = false;
	
	for(i=0;i<objElements.length;i++)
	{
		obj=null;
		obj = objElements[i];
		if (haveProperty(obj,"Previous"))
			if( String(obj.Previous) == String(sVal))
			{	
				obj.cancelledvalue = obj.Previous;//changed by ravi to keep storing previous value 
				waitFor("window.m_AllowRestore",'window.DoRestore('+i+');');
				return true;
			}
	}
		
	return false;
}

function DoRestore(Idx)
{
	var obj=document.forms[0].elements[Idx];
	obj.value=obj.Previous;
	
	//obj.Previous=null;//changed by ravi to set new value during addnew
	return true;
}
//End - pmittal5

function PrintSelectedReport(name)
{
    document.forms[0].hdnFileRequest.value = "1" ;
    document.getElementById('file').src = "PrintCheckDownload.aspx?name=" + name;
    pleaseWait.Show();
    checkReadyState();		    
}

function PrintEOBOnClose() {
    var name = eval(window.document.forms[0].hdnFileNames);
    if(name!=null)
        var FileName = window.document.forms[0].hdnFileNames.value
    if(FileName != "0" && name !=null){
    document.getElementById('file').src = "PrintCheckDownload.aspx?name=" + FileName + "&deleteEOBFiles=true";
    }
}

function PrintEOBOk() {
    var name = window.document.forms[0].hdnFileNames.value;
    document.getElementById('file').src = "PrintCheckDownload.aspx?name=" + name + "&deleteEOBFiles=true";
    self.close();
}