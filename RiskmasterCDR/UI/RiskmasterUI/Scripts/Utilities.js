// rahul 11/28/07 - optional frameset enhancements
var m_tabNames=[]; 
var m_count = null;
var m_PortletName; 
// end - optional frameset enhancements
function TellMinMaxAndSet(ctrl)
{
	if (ctrl.value=='' || ctrl.value < 10)
	{
		alert("Minimum value required is 10.");
		ctrl.value='10';
	}
	if (ctrl.value > 10000)
	{
		alert("Maximum value allowed is 10000.");
		ctrl.value='10000';
	}
}
function BRSFeeTable_RadioClicked(oCtrl)
{
	var modcount=document.getElementById(oCtrl.id + "|BrsModCount|")
	document.forms[0].btnModifierValues.value="Modifier Values (" + modcount.value +  ")";
	modcount=null;
	var CalcType=document.getElementById(oCtrl.id + "|CalcTypeCode|");
	switch(CalcType.value)
	{
		case "12":

			document.forms[0].btnBasicTable.value="Basic Table";
			document.forms[0].btnBasicTable.disabled=false;
			document.forms[0].btnModifierValues.disabled=true;
			document.forms[0].btnPerDiem.disabled=true;

			break;
		case "14":
			document.forms[0].btnBasicTable.value="Extended Table";
			document.forms[0].btnBasicTable.disabled=false;
			document.forms[0].btnModifierValues.disabled=false;
			document.forms[0].btnPerDiem.disabled=true;
			break;
		case "15":
		case "16":
		case "17":
			document.forms[0].btnBasicTable.disabled=true;
			document.forms[0].btnModifierValues.disabled=true;
			document.forms[0].btnPerDiem.disabled=true;
			break;
		case "20":
			//BRS FL Merge  : Umesh
			document.forms[0].btnBasicTable.disabled=true;
			document.forms[0].btnModifierValues.disabled=true;
			document.forms[0].btnPerDiem.value="Per Diem";
			document.forms[0].btnPerDiem.disabled=false;
			break;
		default:
			document.forms[0].btnBasicTable.disabled=true;
			document.forms[0].btnModifierValues.disabled=false;
			document.forms[0].btnPerDiem.disabled=true;
			break;
			//does nothing
	}

	return true;
}
//BRS FL Merge  : Umesh
function BRSFeeTable_PerDiem()
{
		inputs = document.getElementsByTagName("input");
		for (i = 0; i < inputs.length; i++)
		{
			if ((inputs[i].type=="radio") && (inputs[i].checked==true))
			{

			    var tabnam = document.getElementById("hdnUserTableName");
			    var calctype = document.getElementById("hdnCalcTypeCode");
			    var tableid = document.getElementById("hdnTableId");
			    window.open('BRSHospitalPerDiem.aspx?tableid=' + tableid.value + '&amp;calctype=' + calctype.value + '&amp;usertablename=' + tabnam.value, 'Hello',
				'width=600,height=500'+',top='+(screen.availHeight-290)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
				return;
			}
		}
    //alert("Please select a Fee Table");
		alert(BRSFeeTableVal.valFeeTable); //MITS 34712 hlv
		
}
//BRS FL Merge End

function BRSFeeTable_BasicTable()
{
		inputs = document.getElementsByTagName("input");
		for (i = 0; i < inputs.length; i++)
		{
			if ((inputs[i].type=="radio") && (inputs[i].checked==true))
			{
				
				 var tabnam = document.getElementById("hdnUserTableName");
				 var calctype = document.getElementById("hdnCalcTypeCode");
				 var tableid = document.getElementById("hdnTableId");
				 window.open('BRSCPTCodesListing.aspx?tableid=' + tableid.value + '&amp;calctype=' + calctype.value + '&amp;usertablename=' + tabnam.value, 'BRSCPTCodesListing',
				'width=600,height=500'+',top='+(screen.availHeight-290)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
				return;
			}
		}
    //alert("Please select a Fee Table");
		alert(BRSFeeTableVal.valFeeTable); //MITS 34712 hlv
}
//zalam 05/22/2008 Mits:-7347 Start
function TrimSpace() 
{
    if (document.getElementById('EvPrefix') != null)
    {
        var e_prefix = document.getElementById('EvPrefix').value;
	    while (e_prefix.substring(0,1) == ' ')
	    {
		    e_prefix = e_prefix.substring(1, e_prefix.length);
	    }
	    while (e_prefix.substring(e_prefix.length-1, e_prefix.length) == ' ')
	    {
		    e_prefix = e_prefix.substring(0,e_prefix.length-1);
	    }
	    document.getElementById('EvPrefix').value=e_prefix;
	}
}
//zalam 05/22/2008 Mits:-7347 End

function BRSFeeTable_ModifierValues()
{
	var selectedFound=false;
	inputs = document.getElementsByTagName("input");
		for (i = 0; i < inputs.length; i++)
		{
			if ((inputs[i].type=="radio") && (inputs[i].checked==true))
			{
			    var tabnam = document.getElementById("hdnUserTableName");
			    var tableid = document.getElementById("hdnTableId");

			    window.open('BRSModifierValues.aspx?tableid=' + tableid.value + '&amp;usertablename=' + tabnam.value, 'BRSModifierValues',
				'width=600,height=500'+',top='+(screen.availHeight-290)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
				tabnam=null;
				return;
			}
		}
    //alert("Please select a Fee Table");
		alert(BRSFeeTableVal.valFeeTable); //MITS 34712 hlv
}


function BRSFeeTableDetail_Load()
{
	if (document.forms[0].FormMode.value=="close")
	{
		window.opener.document.forms['frmData'].submit();
		window.close();
		return;
	}

	if(document.forms[0].RowId.value=="" || document.forms[0].hdnTableType.value=="1")
	{
		//Add mode
		if(document.forms[0].hdnTableType.value=="1")
		{
			inputs = document.getElementsByTagName("input");
			for (i = 0; i < inputs.length; i++)
			{
				if (inputs[i].type=="radio")
				{
					inputs[i].parentElement.style.display = "none";
					//inputs[i].parentNode.parentNode.previousSibling.style.display = "none";
				}
			}
		}
	}
	var FeeSched=document.getElementById("FeeSchedule");
	switch(document.forms[0].DbType.value)
	{
		case "14":
		case "15":
		case "16":
		case "17":
		case "20":
			FeeSched.parentElement.parentElement.style.display = "none";
			break;
		default:
			FeeSched.parentElement.parentElement.style.display = "";
			break;
	}
	FeeSched=null;
	BRSFeeTable_ChangeCalculationType();
	BRSFeeTableDetail_EnableUCR();
	if(document.forms[0].DbType.value=='12')
	BRSFeeTableDetail_EnableFeeSchdLink();
}

function BRSFeeTableDetail_EnableUCR()
{
	var ucr=document.getElementById("UCRPerc");
	var def=document.getElementById("DefPerc");
	if(document.forms[0].iCalculationType.value=="0" || document.forms[0].iCalculationType.value=="")
	{
		ucr.parentElement.parentElement.style.display = "";
		def.parentElement.parentElement.parentElement.style.display = "";
		document.forms[0].DefPercMin.value="25";
		document.forms[0].DefPercMax.value="95";
	}
	else
	{
		ucr.parentElement.parentElement.style.display = "none";
		def.parentElement.parentElement.parentElement.style.display = "none";
	}
	
	/*
	******Mohit Yadav for Bug No. 000177**********************
	******Not Required****************************************
	if(document.forms[0].RowId.value=="")
	{
		ucr.parentElement.parentElement.style.display = "none";
		def.parentElement.parentElement.parentElement.style.display = "none";
	}
	**********************************************************
	*/
	
	ucr=null;
	def=null;
}

function BRSFeeTable_ChangeCalculationType()
{
	var FeeSched=document.getElementById("FeeSchedule");
	switch(document.forms[0].DbType.value)
	{
		/*************Mohit Yadav changed the sequence**********/
		case "0":
			document.forms[0].iCalculationType.value="-1";
			break;
		case "1":
		case "2":
		case "4":
		case "5":
		case "9":
		case "19":
			document.forms[0].iCalculationType.value="0";
			break;
		case "14":
		case "3":
		case "10":
		case "11":
		case "12":
		case "13":
		case "20":
			document.forms[0].iCalculationType.value="2";
			break;
		case "15":
			document.forms[0].iCalculationType.value="2";
			FeeSched.parentElement.parentElement.style.display = "none";
			break;
		case "8":
			document.forms[0].iCalculationType.value="3";
			break;
		case "16":
		case "17":
		case "18":
			FeeSched.parentElement.parentElement.style.display = "none";
			document.forms[0].iCalculationType.value="7";
			break;
		default:
			FeeSched.parentElement.parentElement.style.display = "";
			break;
	}

	if(document.forms[0].DbType.value=="15")
		document.forms[0].btnFactor.style.display = "";
	else
		document.forms[0].btnFactor.style.display = "none";
}

//*****Mohit added this function for Default Percentile************
function BRSFeeTable_ChangeDefaultPercentile()
{
	switch(document.forms[0].UCRPerc.value)
	{
		case "0":
			document.forms[0].DefPerc.value="";
			break;
		case "1":
			document.forms[0].DefPerc.value="50";
			break;
		case "2":
			document.forms[0].DefPerc.value="25";
			break;
		default:
			document.forms[0].DefPerc.value="25";
			break;
	}
}

function BRSFeeTableDetail_DbType()
{
	BRSFeeTable_ChangeCalculationType();
	if(document.forms[0].iCalculationType.value!="-1")
		BRSFeeTableDetail_FillODBCNames();
	BRSFeeTableDetail_EnableUCR();
}

//MITS 7389 Umesh
function BRSFeeTableDetail_EnableFeeSchdLink()
{
	var objElts = document.getElementsByTagName("U"); 
	for(i=0;i<objElts.length;i++)
	  {
		
        /*
        if(objElts[i].innerHTML == 'Fee Schedule')
        {
         objElts[i].parentNode.innerHTML = "Fee Sched link:"
        
        } 
        */
	    //MITS 34721 hlv begin
	    if (objElts[i].innerHTML == BRSFeeTableDetailLabels.lblFeeSchedule) {
	        //objElts[i].parentNode.innerHTML = BRSFeeTableDetailLabels.lblFeeScheLink;
	        objElts[i].parentNode.innerHTML = "Fee Sched link:"
	    }
	    //MITS 34721 hlv end
        
	  }
}


//End Umesh

function BRSFeeTableDetail_FillODBCNames()
{
	window.document.forms['frmData'].submit();
}


function BRSFeeTableDetail_Factors()
{

    window.open('BRSFactors.aspx?tableid=' + document.forms[0].TableId.value + '&amp;usertablename=' + document.forms[0].hdntitle.value, 'BRSFactors',
				'width=600,height=500' + ',top=' + (screen.availHeight - 290) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');

}

function BRSFeeTableDetail_FeeSchedule()
{
	window.document.forms['frmData'].submit();
}

function ValidateCPTExtendedCodeDetails()
{
	if (document.forms[0].AsstAtSurgery.value!="" && document.forms[0].AsstAtSurgery.value!="Y" && document.forms[0].AsstAtSurgery.value!="N" && document.forms[0].AsstAtSurgery.value!="R")
	{
		alert("Please Enter <Y> or <N> or <R> or <blank>");
		document.forms[0].AsstAtSurgery.focus();
		return false;
	}

	if (document.forms[0].Strrd.value!="" && document.forms[0].Strrd.value!="*")
	{
		alert("Please Enter <*>");
		document.forms[0].Strrd.focus();
		return false;
	}

	if (document.forms[0].AnsthByRep.value!="" && document.forms[0].AnsthByRep.value!="BR")
	{
		alert("Please Enter <BR>");
		document.forms[0].AnsthByRep.focus();
		return false;
	}

	if (document.forms[0].AnsthNotAppl.value!="" && document.forms[0].AnsthNotAppl.value!="NA")
	{
		alert("Please Enter <NA>");
		document.forms[0].AnsthNotAppl.focus();
		return false;
	}

	if (document.forms[0].ByRep.value!="" && document.forms[0].ByRep.value!="BR")
	{
		alert("Please Enter <BR> or <blank>");
				document.forms[0].ByRep.focus();
		return false;
	}

	if (document.forms[0].CPTCode.value=="")
	{
		alert("Please enter a CPT Code");
		document.forms[0].CPTCode.focus();
		return false;
	}

	if (document.forms[0].CodeDesc.value=="")
	{
		alert("Please enter a Description");
		document.forms[0].CodeDesc.focus();
		return false;
	}

	if (document.forms[0].Amount.value=="")
	{
		alert("Please enter an Amount");
		document.forms[0].Amount.focus();
		return false;
	}
	return true;
}

function BRSFactor_ClinicalCatChange()
{
	if(document.forms[0].ClinicalCat.value="ANES Anesthesia")
	{
			document.forms[0].StartCpt.value = "00100";
            document.forms[0].EndCpt.value = "01999";
	}
	else if(document.forms[0].ClinicalCat.value="MD-ANES MD Surg Anesthesia")
	{
			document.forms[0].StartCpt.value = "10040";
			document.forms[0].EndCpt.value = "69990";
	}
}

function BRSModifierValue_Save() {

    //zalam 06/06/2008 Mits:-12269 Start
    if (document.forms[0].ModifierCode.value == "" || document.forms[0].StartCPT.value == "" ||
       document.forms[0].EndCPT.value == "" || document.forms[0].ModValue.value == "") {
        //alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
        alert(BRSModifierValue.valMain); //MITS 34779 hlv
        return false;
    }
    //zalam 06/06/2008 Mits:-12269 End
	if(document.forms[0].StartCPT.value>document.forms[0].EndCPT.value)
	{
	    //alert("Please enter an End CPT that is greater than or equal to the Start CPT");
	    alert(BRSModifierValue.valCPT); //MITS 34779 hlv
		return false;
	}

	if(document.forms[0].MaxRLV.value=="" || document.forms[0].MaxRLV.value=="0")
		document.forms[0].MaxRLV.value="999999999.99";
	
	return true;
}


function NewTable_Save() {
    //Nitesh(10/09/2006) :MITS 8018 Starts
	var str =document.forms[0].SysTableName.value;
	var len=str.length;
	
	if(str.substring(0,1)=="0" || str.substring(len-1,len)=="0" ||
		str.substring(0,1)=="1" || str.substring(len-1,len)=="1" ||
		str.substring(0,1)=="2" || str.substring(len-1,len)=="2" ||
		str.substring(0,1)=="3" || str.substring(len-1,len)=="3" ||
		str.substring(0,1)=="4" || str.substring(len-1,len)=="4" ||
		str.substring(0,1)=="5" || str.substring(len-1,len)=="5" ||
		str.substring(0,1)=="6" || str.substring(len-1,len)=="6" ||
		str.substring(0,1)=="7" || str.substring(len-1,len)=="7" ||
		str.substring(0,1)=="8" || str.substring(len-1,len)=="8" ||
		str.substring(0,1)=="9" || str.substring(len-1,len)=="9")
	{
		alert("The System Table Name entered begins or ends with a number. Please make sure the name begins and ends with a letter.");
		return false;
	}
	if (str.indexOf("(")!=-1||str.indexOf(")")!=-1||
		str.indexOf("`")!=-1||str.indexOf("|")!=-1||
		str.indexOf("?")!=-1||str.indexOf(",")!=-1||
		str.indexOf("[")!=-1||str.indexOf("]")!=-1||
		str.indexOf("{")!=-1||str.indexOf("}")!=-1||
		str.indexOf("!")!=-1||str.indexOf("@")!=-1||
		str.indexOf("#")!=-1||str.indexOf("$")!=-1||
		str.indexOf("%")!=-1||str.indexOf("^")!=-1||
		str.indexOf("+")!=-1||str.indexOf(".")!=-1||
		str.indexOf(":")!=-1||str.indexOf(";")!=-1||
		str.indexOf("<")!=-1||str.indexOf(">")!=-1||
		str.indexOf("~")!=-1||str.indexOf(" ")!=-1||
		str.indexOf("*")!=-1||str.indexOf("-")!=-1||
		str.indexOf("\\")!=-1||str.indexOf("\/")!=-1||
		str.indexOf("\"") != -1 || str.indexOf("\'") != -1 || str.indexOf("&") != -1
		)
	{	
	    alert("System Table Name contains invalid characters.Please enter valid name.");
		return false;
	}
	//Nitesh(10/09/2006) :MITS 8018 Ends 
	document.forms[0].FormMode.value="close";
	return true;
}

function TableField_Save() {
//MGaba2: R7 :In case History Tracking is ON for a particular supp field and user is deleting it 
    if (document.forms[0].hdnHistTrack.value == "True" && document.forms[0].DeleteFlag.checked == true) {
        alert("Please ensure that History Tracking executable is configured through Task Manager for asynchronous processing to stop history tracking for this field");
    }
	
	//Start by Shivendu for MITS 9680
	if(document.forms[0].FieldType.value == 7)
	{
    	alert("Primary Record Index data type is no longer in use.");
    	return false; 
	}
	//End by Shivendu for MITS 9680
	//Start by Shivendu for Supplemental Grid
	if(document.forms[0].ColCount.value==0 && document.forms[0].FieldType.value == 17 &&  document.forms[0].RowId.value == 0 ) 
	{
	    alert("No Columns added");
	    return false;
	}
	if(document.forms[0].ColCount.value==0 && document.forms[0].FieldType.value == 17 && document.forms[0].GridProperty.value==1 && document.forms[0].RowId.value !=0 ) 
	{
	    alert("No Columns added");
	    return false;
	}
	//End by Shivendu for Supplemental Grid
	if(document.forms[0].FieldType.value=="")
	{
		alert("Field Type must be selected.");
		return false;
	}
	if(document.forms[0].UserPrompt.value=="")
	{
		alert("Please enter a User Prompt to proceed!");
		document.forms[0].UserPrompt.focus();
		return false;
	}
	if(document.forms[0].SysFieldName.value=="")
	{
		alert("Please enter a System Field Name to proceed!");
		document.forms[0].SysFieldName.focus();
		return false;
	}
	if(document.forms[0].FieldType.value=="")
	{
		alert("Field Type must be selected.");
		return false;
	}

    //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
	if (document.forms[0].FieldType.value == 23 && document.forms[0].CodeFileId.value == "") {
	    alert("Hyperlink must be selected.");
	    return false;
	}
    //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691

	//gagnihotri MITS 12094
	//Removed repeated code, added case for pattern not chosen
	if (document.forms[0].FieldType.value == "0" && document.forms[0].Pattern.value == "" && document.getElementById('patterned').checked) {
	    alert("Please select a Pattern to proceed!");
	    return false;
	}

    //bkuzhanthaim : JIRA : RMA-803 : decimal values wont be allowed in FieldSize
	if (document.forms[0].FieldSize != null && document.forms[0].FieldType.value == "0")
	{
	    if (document.forms[0].FieldSize.value < 1 || document.forms[0].FieldSize.value > 255) {
	        alert("Field Size must be between 1 and 255!");
	        document.forms[0].FieldSize.focus();
	        return false;
	    }
	    else if (document.forms[0].FieldSize.value.indexOf(".") != -1) {
	        alert("You cannot enter a decimal value in the Field Size.Please enter another value.");
	        document.forms[0].FieldSize.focus();
	        return false;
	    }
	    
	}

	if(document.forms[0].FieldType.value =="6" && document.forms[0].CodeFileId.value=="")
	{
		alert("Please select a Code File to continue!");
		return false;
	}
    //mdhamija MITS 27553
    if (document.forms[0].UserPrompt.value.length > 50) 
    {
        alert("The number of Characters in UserPromt Field cannot be greater than 50");
        document.forms[0].UserPrompt.value = "";
        document.forms[0].UserPrompt.focus();
        return false;
    }
    //MITS 27553 End

	var str =document.forms[0].SysFieldName.value;
	var len=str.length;

	//Ankit Start : Worked on MITS - 32246
	if (str.substring(0, 1) == "0" ||
		str.substring(0, 1) == "1" ||
		str.substring(0, 1) == "2" ||
		str.substring(0, 1) == "3" ||
		str.substring(0, 1) == "4" ||
		str.substring(0, 1) == "5" ||
		str.substring(0, 1) == "6" ||
		str.substring(0, 1) == "7" ||
		str.substring(0, 1) == "8" ||
		str.substring(0, 1) == "9")
	{
	    alert("The System Field Name entered begins with a number. Please make sure the name begins with a letter.");
	    return false;
	}
	//End MITS - 32246

	if (str.indexOf("(")!=-1||str.indexOf(")")!=-1||
		str.indexOf("`")!=-1||str.indexOf("|")!=-1||
		str.indexOf("?")!=-1||str.indexOf(",")!=-1||
		str.indexOf("[")!=-1||str.indexOf("]")!=-1||
		str.indexOf("{")!=-1||str.indexOf("}")!=-1||
		str.indexOf("!")!=-1||str.indexOf("@")!=-1||
		str.indexOf("#")!=-1||str.indexOf("$")!=-1||
		str.indexOf("%")!=-1||str.indexOf("^")!=-1||
		str.indexOf("+")!=-1||str.indexOf(".")!=-1||
		str.indexOf(":")!=-1||str.indexOf(";")!=-1||
		str.indexOf("<")!=-1||str.indexOf(">")!=-1||
		str.indexOf("~")!=-1||str.indexOf(" ")!=-1||
		str.indexOf("*")!=-1||str.indexOf("-")!=-1||
		str.indexOf("\\")!=-1||str.indexOf("\/")!=-1||
		str.indexOf("\"")!=-1 || str.indexOf("'")!=-1 || str.indexOf("&") != -1
		)
	{	
	    alert("System Field Name contains special characters.Please enter valid name.");
		return false;
	}

	//if(document.forms[0].TableType.value =="9")
		//document.forms[0].JuridictionHidden.value=document.forms[0].Juridiction_lst.value;

	if (document.forms[0].TableType.value == "6") {
	    //abisht MITS 10740 Added an if condition.
	    if (document.forms[0].GAList_lst != null && document.forms[0].GAList != null) {
	        document.forms[0].GAListHidden.value = "";
	        for (var i = 0; i < document.forms[0].GAList.length; i++) {
	            document.forms[0].GAListHidden.value = document.forms[0].GAListHidden.value + document.forms[0].GAList[i].value + ' ';
	        }
	        document.forms[0].GAListHidden.value = document.forms[0].GAListHidden.value.replace(/^\s+|\s+$/g, "");
	    }

	    if (document.forms[0].GAEntLookup_cid != null && document.forms[0].GAEntLookup_cid.value != "") {
	        document.forms[0].GAListHidden.value = document.forms[0].GAEntLookup_cid.value;
	    }
	}
	else if (document.forms[0].TableType.value != "9") {
	            document.forms[0].GAListHidden.value = document.forms[0].GAList_cid.value;
	       
	}
	document.forms[0].FormMode.value="close";
	return true;
}
function TableField_FieldTypeChange()
{
	var codefileid=document.getElementById("CodeFileId");
	TableField_EnableDisablePattern();
	//Added by Shivendu for Supplemental Grid
	document.forms[0].ModifyGrid.disabled = true;
	switch(document.forms[0].FieldType.value)
	{

		case "6":
		case "5":
		case "14":
			document.forms[0].ControlChanged.value = "FIELDTYPE";
			document.forms[0].hdnCodeFile.value="CODEFILE";
			document.forms[0].CodeFileId.style.display="";
			window.document.forms['frmData'].submit();
			break;
		case "8":
		case "16":
			document.forms[0].ControlChanged.value = "FIELDTYPE";
			document.forms[0].hdnCodeFile.value="ENTITYFILE";
			codefileid.parentElement.parentElement.style.display = "";
			window.document.forms['frmData'].submit();
			break;
		//Added by Shivendu for Supplemental Grid
		case "17":
		    document.forms[0].ModifyGrid.disabled = false;
		default:
			codefileid.parentElement.parentElement.style.display = "none";
			document.forms[0].hdnCodeFile.value="";
			break;
	}
	codefileid=null;
}

function selectARadio()
{
	if (document.forms[0].selectedseq.value=="")
	{
		document.forms[0].selectedseq.value="2";
	}
	if (document.getElementById("selectedseq")!=null)
	{
		var selectedseq=document.getElementById("FieldListGrid|"+document.forms[0].selectedseq.value);
		if (selectedseq!=null)
		{
			selectedseq.checked=true;	
		}
	}
}
function TableField_EnableDisableControls()
{

//Final correct one
	CheckForWindowClosing();

	TableField_EnableDisablePattern();

	var LookupFlag=document.getElementById("LookupFlag");
	switch(document.forms[0].TableType.value)
	{
		case "8": //Admin tracking
			document.getElementById('GrpAssocia').style.display = "none";
			LookupFlag.parentElement.parentElement.style.display = "";
			break;
		case "6": //supplemental
			document.getElementById('GrpAssocia').style.display = "";
			LookupFlag.parentElement.parentElement.style.display = "none";
			TableField_UpdateList('GAList');
			break;
		case "9": //Jurisdictional
			//Pankaj 418, Juris Association no more required
			document.getElementById('GrpAssocia').style.display = "none";
			document.getElementById('Juridiction').parentElement.parentElement.style.display = "none";
			LookupFlag.parentElement.parentElement.style.display = "none";
			//Start:Mohit Yadav For Bug No. 000196************************			
			//Pankaj 418, Juris Association no more required
			//TableField_UpdateList('Juridiction');
			//End:For Bug No. 000196**************************************			
			break;

	}
	LookupFlag=null;
//	var NetVisibleFlag=document.getElementById("NetVisibleFlag");
	if(document.forms[0].RowId.value==""||document.forms[0].RowId.value=="0")
	{
		//add mode
		document.forms[0].DeleteFlag.disabled=true;
		var FieldSize=document.getElementById("FieldSize");
		FieldSize.parentElement.parentElement.style.display = "none";
		//NetVisibleFlag.parentElement.parentElement.style.display = "none";
	}
	else
	{
		//edit mode
		document.forms[0].FieldSize.readOnly=true;
		document.forms[0].SysFieldName.readOnly=true;
		document.forms[0].DeleteFlag.readonly=false;
		document.forms[0].FieldType.disabled=true;
		//if(document.forms[0].TableType.value=="6")
		//	NetVisibleFlag.parentElement.parentElement.style.display = "";
		//else
		//	NetVisibleFlag.parentElement.parentElement.style.display = "none";
	}
//	NetVisibleFlag=null;
	var CodeFileId=document.getElementById("CodeFileId");
	switch(document.forms[0].FieldType.value)
	{

		case "6":
		case "5":
		case "14":
			CodeFileId.parentElement.parentElement.style.display = "";
			break;
		case "8":
		case "16":
			CodeFileId.parentElement.parentElement.style.display = "";
			break;
		default:
			CodeFileId.parentElement.parentElement.style.display = "none";
			document.forms[0].hdnCodeFile.value="";
			break;
	}
	CodeFileId=null;
//Final Correc

//Start by Shivendu for Supplemental Grid
document.forms[0].ModifyGrid.disabled = true;
if(document.forms[0].SysFieldName.value!=null && document.forms[0].SysFieldName.value!="")
{
//Modified by Shivendu for MITS 10991
if(document.forms[0].RowId.value==0 && Number(document.forms[0].ColCount.value)>0)
{
document.forms[0].FieldType.options[18].selected = true;
//document.forms[0].FieldType.disabled = true;

}
//document.forms[0].FieldType.selectedIndex = 17;
document.forms[0].ModifyGrid.disabled = false;



}

if(document.forms[0].RowId.value!=0)
if(document.forms[0].FieldType.selectedIndex != 17)
document.forms[0].ModifyGrid.disabled = true;
//End by Shivendu for Supplemental Grid
//Start by Shivendu for MITS 11633
if(document.forms[0].RowId.value==0)
{
    if(document.forms[0].FieldType.selectedIndex != 18)
    {
        document.forms[0].ModifyGrid.disabled = true;
    }
    else
    {
        document.forms[0].ModifyGrid.disabled = false;
    }
}
//End by Shivendu for MITS 11633
//Start by Shivendu for MITS 10990
if(document.forms[0].ModifyGrid.disabled==false)
{
    if(document.getElementById('Juridiction')!=null)
    document.getElementById('Juridiction').parentElement.parentElement.style.display = "none";

}
//Start by Shivendu for MITS 10990
TableField_IsPaternClick();
}


function TableField_SourceFieldChange()
{
	document.forms[0].ControlChanged.value = "SOURCEFIELD";
	document.forms[0].ListType.value = document.forms[0].SourceField.value;
	pleaseWait.Show();
	window.document.forms['frmData'].submit();
}


function TableField_EnableDisablePattern()
{
	var FieldSize=document.getElementById("FieldSize");
	//&& condition added by Shivendu for MITS 11248
	if((document.forms[0].FieldType.value=="0"||document.forms[0].FieldType.value=="") && document.forms[0].ModifyGrid.disabled==true )
	{
		FieldSize.parentElement.parentElement.style.display = "";
		//document.forms[0].Pattern.disabled=false;
		TableField_IsPaternClick();
		inputs = document.getElementsByTagName("input");
		for (i = 0; i < inputs.length; i++)
		{
			if ((inputs[i].type=="radio"))
			{
				inputs[i].disabled=false;
			}
		}
	}
	else
	{
		FieldSize.parentElement.parentElement.style.display = "none";
		document.forms[0].Pattern.disabled=true;
		inputs = document.getElementsByTagName("input");
		for (i = 0; i < inputs.length; i++)
		{
			if ((inputs[i].type=="radio"))
			{
				inputs[i].disabled=true;
			}
		}
	}
	FieldSize=null;
}


function AddIndex_Submit()
{
	document.forms[0].FormMode.value="close";
}


function CheckForPopUpClosing()
{
	if (document.forms[0].FormMode.value=="close")
	{
		window.opener.document.location.reload();
		window.close();
	}
}


function TableField_IsPaternClick()
{
	if (document.getElementById('patterned').checked)
	{
	    document.forms[0].Pattern.disabled = false;
	    document.forms[0].FieldSize.style.display = "none";
	    document.getElementById("lblFieldSize").style.display = "none"; //abansal23 : MITS 14287
	}
	else
	{
	    document.forms[0].Pattern.disabled = true;
	    document.forms[0].FieldSize.style.display = "";
	    document.getElementById("lblFieldSize").style.display = ""; //abansal23 : MITS 14287
	}
}


function FieldListGrid_moveup()
{
	var SelectedFound=false;	
	inputs = document.getElementsByTagName("input");
	for (i = 0; i < inputs.length; i++)
	{
		if ((inputs[i].type=="radio") && (inputs[i].checked==true))
		{
			var curRadio=inputs[i];
			inputname=inputs[i].id.substring(0,inputs[i].id.indexOf("|"));
			var pipepos=inputs[i].id.indexOf("|");
			pipepos++;
			var seq=inputs[i].id.substring(pipepos,inputs[i].id.length);
			var curSeqNo=document.getElementById("FieldListGrid|" + seq + "|SeqNo|");
			seq--;
			var prevRadio=document.getElementById("FieldListGrid|" + seq)
			if(prevRadio==null)
			{
				alert("There is no previous field to swap");
				return false;
			}
			else
			{
				document.forms[0].Field1.value=curRadio.value;
				document.forms[0].Field2.value=prevRadio.value;
				var prevSeqNo=document.getElementById("FieldListGrid|" + seq + "|SeqNo|");
				document.forms[0].SeqNo1.value=curSeqNo.value;
				document.forms[0].SeqNo2.value=prevSeqNo.value;
				document.forms[0].selectedseq.value=seq;
				return true;
			}
			
		}
	}	
	if (SelectedFound==false)
	{
		alert('Please select some row for Swapping');
		return false;
	}
	return true;
}


function FieldListGrid_movedown()
{
	var SelectedFound=false;	
	inputs = document.getElementsByTagName("input");
	for (i = 0; i < inputs.length; i++)
	{
		if ((inputs[i].type=="radio") && (inputs[i].checked==true))
		{
			var curRadio=inputs[i];
			inputname=inputs[i].id.substring(0,inputs[i].id.indexOf("|"));
			var pipepos=inputs[i].id.indexOf("|");
			pipepos++;
			var seq=inputs[i].id.substring(pipepos,inputs[i].id.length);
			var curSeqNo=document.getElementById("FieldListGrid|" + seq + "|SeqNo|");
			seq++;
			var nextRadio=document.getElementById("FieldListGrid|" + seq)
			if(nextRadio==null)
			{
				alert("There is no next field to swap");
				return false;
			}
			else
			{
				document.forms[0].Field1.value=curRadio.value;
				document.forms[0].Field2.value=nextRadio.value;
				var nextSeqNo=document.getElementById("FieldListGrid|" + seq + "|SeqNo|");
				document.forms[0].SeqNo1.value=curSeqNo.value;
				document.forms[0].SeqNo2.value=nextSeqNo.value;
				document.forms[0].selectedseq.value=seq;
				return true;
			}			
		}
	}	
	if (SelectedFound==false)
	{
		alert('Please select some row for Swapping');
		return false;
	}
	return true;

}

function BRSFeeTableDetail_Save()
{
//zalam 05/30/2008 Mits:-12269 Start
            var FeeSched=document.getElementById("FeeSchedule");
            var ucr=document.getElementById("UCRPerc");
	        var def=document.getElementById("DefPerc");
	        
	        //This section is used for validating dates Start
	        if(document.forms[0].StartDt.value != "")
            {
                var strfd=document.forms[0].StartDt.value;
            
                if(document.forms[0].EndDt.value != "")
                {
                    var strtd=document.forms[0].EndDt.value;
            
                    var arrfd=strfd.split("/");
                    var arrtd=strtd.split("/");

                    if(arrfd[0].length!=2)
                    {
                        arrfd[0]='0'+arrfd[0];
                    }
                    if(arrfd[1].length!=2)
                    {
                        arrfd[1]='0'+arrfd[1];
                    }
                    if(arrtd[0].length!=2)
                    {
                        arrtd[0]='0'+arrtd[0];
                    }
                    if(arrtd[1].length!=2)
                    {
                        arrtd[1]='0'+arrtd[1];
                    }
                    var strfd=arrfd[2]+arrfd[0]+arrfd[1];
                    var strtd=arrtd[2]+arrtd[0]+arrtd[1];
                }
            }
	        //Validation dates End
	        
            if(document.forms[0].UserTableName.value=="" || document.forms[0].DbType.value=='-1' ||
               document.forms[0].StartDt.value=="" || document.forms[0].state.value=="" ||
               document.forms[0].priority.value=="")
			{	
				alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
				return false;
		    }
		    else if(FeeSched.parentElement.parentElement.style.display != "none" && document.forms[0].FeeSchedule.value=='-1' && document.forms[0].DbType.value !='12')
		    {
		        alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
				return false;  
		    }
		    else if(ucr.parentElement.parentElement.style.display != "none" && document.forms[0].UCRPerc.value=='0')
		    {
		        alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
				return false;  
		    }
		    else if(def.parentElement.parentElement.parentElement.style.display != "none" && document.forms[0].DefPerc.value=="")
		    {
		        alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
				return false;  
		    }
            else if(strfd > strtd)
            {
                //alert("To Date must be greator or equal to From Date or it should be Blank");//zalam 08/13/2008 Mis:-13052
                alert("End Date must be greator or equal to Start Date or it should be Blank");
                return false;
            }
			else
			{
			document.forms[0].FormMode.value="close";
		        return true;
			}		
//	        document.forms[0].FormMode.value="close";
//	        return true;
//zalam 05/30/2008 Mits:-12269 End
}

//BRS FL Merge : Umesh
function BRSHospPerDiem_OK()
{	
	document.forms[0].hdnaction.value = 'Save';	
	window.document.forms[0].submit();
	window.close();
	
}
function BRSHospPerDiem_Cancel()
{	
		window.close();
	
}

function BRSCPTExtendedCodeDetails_Save()
{
	document.forms[0].FormMode.value="close";
	return ValidateCPTExtendedCodeDetails();
}

function BRSCPTBasicCodeDetails_Save()
{
    if (document.forms[0].CPTCode.value == "" || document.forms[0].CodeDesc.value == "" || document.forms[0].Amount.value == "") {
        //alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
        alert(BRSCPTBasicCodeDetails.valSave) //MITS 34530 hlv
        return false;
    }
    else {
        return true;
    }
}


function BRSCPTBasicCodeDetails_Load()
{
	CheckForWindowClosing();
	document.forms[0].CPTCodeSupp.readOnly = true;
	document.forms[0].SpecialityCode.readOnly = true;
	if(document.forms[0].RowId.value!="")
		document.forms[0].CPTCode.readOnly = true;
}

function BRSCPTExtendedCodeDetails_Load()
{
	//CheckForWindowClosing();
	if(document.forms[0].RowId.value!="")
		document.forms[0].CPTCode.readOnly = true;
}

function BRSFactor_Save()
{
	document.forms[0].FormMode.value="close";
	return true;
}

function BRSImportSchedule_ProviderChange()
{
	document.forms[0].hdAction.value="ProviderChange";
	window.document.forms['frmData'].submit();
}

function BRSImportSchedule_Cancel()
{
	window.close();
}

function BRSImportSchedule_Next() 
{
	var iStepNo=0;
	var sMsg=null;
	iStepNo = document.forms[0].StepNo.value;
	var sFilePath = document.forms[0].Files.value;
	var sTemp = "";
	
	if(iStepNo==1)
	{
		if (document.forms[0].Files.value=='')
		{
		    //alert('Please select files to be imported');
		    alert(BRSImportSchedule1.valFileImport) //MITS 34756 hlv
					return false;
	    }
	    if (document.forms[0].FeeScheduleType.value == '13')     //csingh7 : MITS 14906 : Handled for Worker's Comp 2000
	    {
            //srajinder - check is made case-insensitive - 02/20/2012
	        if (document.forms[0].Files.value.toLowerCase().indexOf("wrkfct.dbf") == -1) {
	            sMsg = sMsg + ",WRKFCT.DBF";
	        }
	        if (document.forms[0].Files.value.toLowerCase().indexOf("wrkmas.dbf") == -1) {
	            sMsg = sMsg + ",WRKMAS.DBF";
	        }
	        if (document.forms[0].Files.value.toLowerCase().indexOf("wrkmod.dbf") == -1) {
	            sMsg = sMsg + ",WRKMOD.DBF";
	        }
	        if (document.forms[0].Files.value.toLowerCase().indexOf("wrkuva.dbf") == -1) {
	            sMsg = sMsg + ",WRKUVA.DBF";
	        }
	        if (document.forms[0].Files.value.toLowerCase().indexOf("wrkzip.dbf") == -1) {
	            sMsg = sMsg + ",WRKZIP.DBF";
	        }
	        if (sMsg != null) {
	            sMsg = sMsg.replace("null,", "");
	            //alert(" Please select file(s) " + sMsg);
	            alert(BRSImportSchedule1.valFiles) //MITS 34756 hlv
	            return false;
	        }
	    }

	}

	if(iStepNo==2) {
	  
		if(document.forms[0].FeeScheduleType.value==19)
		{
		    //sMsg = "could not be found. Check the spelling of the file name and verify that the file location is correct";
		    sMsg = alert(BRSImportSchedule1.valNotFound) //MITS 34756 hlv
		    document.forms[0].CallImport.value = "No";
		    objFile = new ActiveXObject("Scripting.FileSystemObject");

		    if (document.forms[0].CptRvu.value == '' || document.forms[0].CptRvu.value.indexOf(".xls") == -1) {
		        //alert('Please enter a valid Relative Value Unit .xls');
		        alert(BRSImportSchedule1.valUnit) //MITS 34756 hlv
		        document.forms[0].CptRvu.focus();
		        return false;
		    }
		    if (document.forms[0].Gpci.value == '' || document.forms[0].Gpci.value.indexOf(".xls") == -1) {
		        //alert('Please enter a valid GPCI by Zip .xls');
		        alert(BRSImportSchedule1.valZip) //MITS 34756 hlv
		        document.forms[0].Gpci.focus();
		        return false;
		    }
		    if (document.forms[0].Mods.value == '' || document.forms[0].Mods.value.indexOf(".xls") == -1) {
		        //alert('Please enter a valid Modifier .xls');
		        alert(BRSImportSchedule1.valModifier) //MITS 34756 hlv
		        document.forms[0].Mods.focus();
		        return false;
		    }
		    if (sFilePath.indexOf(document.forms[0].CptRvu.value) == -1) {
		        sTemp = sTemp + " , " + document.forms[0].CptRvu.value;
		        document.forms[0].CptRvu.focus();
		    }
		    else if (document.forms[0].CptRvu.value.indexOf("rvu") == -1) {
		        //alert("Please enter a valid Relative Value Unit.xls");
		        alert(BRSImportSchedule1.valUnit) //MITS 34756 hlv
		        document.forms[0].CptRvu.focus();
		        return false;
		    }
		    if (sFilePath.indexOf(document.forms[0].Gpci.value) == -1) {
		        sTemp = sTemp + " , " + document.forms[0].Gpci.value;
		        document.forms[0].Gpci.focus();
		    }
		    else if (document.forms[0].Gpci.value.indexOf("zip") == -1) {
		        //alert("Please enter a valid GPCI by Zip.xls");
		        alert(BRSImportSchedule1.valZip) //MITS 34756 hlv
		        document.forms[0].Gpci.focus();
		        return false;
		    }
		    if (sFilePath.indexOf(document.forms[0].Mods.value) == -1) {
		        sTemp = sTemp + " , " + document.forms[0].Mods.value;
		        document.forms[0].Mods.focus();
		    }
		    else if (document.forms[0].Mods.value.indexOf("mod") == -1) {
		        //alert("Please enter a valid Modifier .xls");
		        alert(BRSImportSchedule1.valModifier) //MITS 34756 hlv
		        document.forms[0].Mods.focus();
		        return false;
		    }
		    if (sTemp != "") 
		    {
		        sTemp = sTemp.replace(",", "");
		        alert(sTemp + " " + sMsg);
		        return false;
		    }

		    sTemp = "";
			
		}

		if(document.forms[0].FeeScheduleName.value=='')
		{
		    //alert('Please enter a valid Fee Schedule name.');
		    alert(BRSImportSchedule1.valFSName) //MITS 34756 hlv
			return false;
		}

		if(document.forms[0].FeeScheduleName.value.indexOf("'")>-1 ||
			document.forms[0].FeeScheduleName.value.indexOf(".")>-1 ||
			document.forms[0].FeeScheduleName.value.indexOf("@")>-1 ||
			document.forms[0].FeeScheduleName.value.indexOf(",")>-1 ||
			document.forms[0].FeeScheduleName.value.indexOf("\\")>-1 ||
			document.forms[0].FeeScheduleName.value.indexOf("*")>-1 ||
			document.forms[0].FeeScheduleName.value.indexOf("%")>-1 ||
			document.forms[0].FeeScheduleName.value.indexOf("/")>-1)
		{
            /*
				alert('The fee schedule named ' + document.frmData.FeeScheduleName.value + 
					' contains one of the following characters ' + 
					", ., @, ,, \, *, %,  or /. Please enter a different name.");
            */
		    alert(BRSImportSchedule1.valSymbolA + document.frmData.FeeScheduleName.value + 
                  BRSImportSchedule1.valSymbolB + ", ., @, ,, \, *, %,  or /. " + 
                  BRSImportSchedule1.valSymbolC) //MITS 34756 hlv
				return false;
		}
	}


	iStepNo++;
	if(iStepNo>3) iStepNo=3;

	document.forms[0].StepNo.value=iStepNo;
	if(document.forms[0].StepNo.value=="3")
	{
		document.forms[0].hdAction.value="Finish";
	}
	else
		document.forms[0].hdAction.value="Next";


return true;
}

function BRSImportSchedule_Load() {

	var lbl=document.getElementById("SelectFile");	
	if(document.forms[0].StepNo.value=='2' || document.forms[0].StepNo.value=='3' )
	{
		lbl.parentNode.parentNode.style.display="none";
	}

	if(document.forms[0].ErrMsg.value!='')
	{
		alert(document.forms[0].ErrMsg.value);
		document.forms[0].ErrMsg.value='';
		return false
	}

	if(document.forms[0].StepNo.value=="1")
	    document.forms[0].btnBack.disabled = true;
	else
	    document.forms[0].btnBack.disabled = false;

	if(document.forms[0].Files.value!='')
	{
	    //lbl.parentNode.previousSibling.innerText = 'Files to Import :- ' + document.frmData.Files.value;
	    lbl.parentNode.previousSibling.innerText = BRSImportSchedule1.lblFileImport + document.forms[0].Files.value; //MITS 34756 hlv
	}
	
	//move to next page if the upload has begun
	if(document.forms[0].StepNo.value=='3')
	{
		//urlNavigate('home?pg=riskmaster/RMUtilities/BRSImportFinish&amp;name=' + document.forms[0].FeeScheduleName.value);
		return true;
	}
}

function BRSImportFinish_Load()
{
		//Timer control
    if (document.forms[0].UploadStatus.value == "1" && document.forms[0].hdnBreak.value != "1") {
        clock_timer = setInterval('BRSImportFinish_Refresh();', document.forms[0].RefreshInterval.value);
	}
//	if(document.forms[0].UploadStatus.value=="-1")
//		document.forms[0].FeeSched.disabled=false;
//	else
//		document.forms[0].FeeSched.disabled=true;
}





function BRSImportSchedule_FeeSched()
{
	urlNavigate('home?pg=riskmaster/RMUtilities/BRSFeeTable');
}

function BRSImportFinish_Refresh()
{
	window.document.forms['frmData'].submit();
}


function BRSImportSchedule_Back()
{
	var iStepNo=0;
	iStepNo=document.forms[0].StepNo.value;
	iStepNo--;
	document.forms[0].StepNo.value=iStepNo;
	document.forms[0].hdAction.value = "Back";
	window.document.forms['frmData'].submit();
 
}

function SystemParameter_CheckTnEClick()
{
	if(document.forms[0].UseTnE.checked==true)
	{
		document.forms[0].UseTnE.checked=false;
		window.open('/RiskmasterUI/UI/Utilities/Manager/SystemParameterCheckTnE.aspx','SystemParameterCheckTnE',
				'width=400,height=300'+',top='+(screen.availHeight-290)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
	}
}

function SystemParameterCheckTnE_Ok() {
    if (document.forms[0].Success.value != "0")//Asif
        document.forms[0].hdnValue.value = "1"; //Asif
    if (document.forms[0].Success.value == "0")
        window.close();
    window.document.forms['frmData'].submit();
}


function SystemParameterCheckTnE_Load()
{
	if(document.forms[0].FormMode.value=="close")
	{
		window.opener.document.forms[0].UseTnE.checked = true;
		window.close();
    }
	
}

function ChkClickd()
{
	if(document.forms[0].FreezeFreeText.checked==false)
	{
		document.forms[0].AllowEditDateStamp.checked=false;
	}
}


function ShowConfigButton()
{
	inputs = document.getElementsByTagName("input");
	for (i = 0; i < inputs.length; i++)
	{
		if ((inputs[i].type=="button") && (inputs[i].value=="Configure..."))
		{			
			if(document.forms[0].ClaimantProcFlag.checked==true)
			{
				inputs[i].style.display = "";
			}
			else
			{
				inputs[i].style.display = "none";
			}
		}
	}
}

function SystemParameter_P2()
{
 window.open('/RiskmasterUI/UI/Utilities/Manager/SystemParameterP2Credentials.aspx','SystemParameterP2Credentials',
 'width=400,height=290'+',top='+(screen.availHeight-290)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
}

function SystemParameterP2_Ok()
{
    
   // mbahl3 mits 34705
    window.opener.document.forms[0].P2Login.value = window.document.forms[0].User.value;
    window.opener.document.forms[0].P2Pass.value = window.document.forms[0].Pwd.value;
	if(self.opener!=null && self.opener!=self)
		self.close();
	else
		self.setTimeout("urlNavigate('home?pg=riskmaster/RMUtilities/SystemParameter')");
	return true;
}

function SystemParameterP2_Cancel()
{
	if(self.opener!=null && self.opener!=self)
		self.close();
	else
		self.setTimeout("urlNavigate('home?pg=riskmaster/RMUtilities/SystemParameter')");
	return true;
}

function P2Credentials_Load()
{
	document.forms[0].User.value=window.opener.document.forms[0].P2Login.value;
	document.forms[0].Pwd.value=window.opener.document.forms[0].P2Pass.value;
}


function SubBankAcct()
{
	if(document.forms[0].SubBankAcc.checked==true)
	{
		document.forms[0].AccDepositBal.disabled=false;
		document.forms[0].UseMasterBankAcc.disabled=true;
		document.forms[0].UseMasterBankAcc.checked=false;
	}
	else
	{
		document.forms[0].UseMasterBankAcc.disabled=false;
		document.forms[0].AccDepositBal.disabled=true;
		document.forms[0].AccDepositBal.checked=false;
	}
	if(document.forms[0].hdAction.value=='SubBankAcct')
	{
		m_DataChanged=true;
		document.forms[0].hdAction.value = '';
	}
}

function SystemParameter_UseCaseMgt()
{
	if(document.forms[0].UseCaseMgmt.checked==true)
	{
		document.forms[0].UseCaseMgmt.checked=false;
		window.open('/RiskmasterUI/UI/Utilities/Manager/SystemParameterUseCaseMgt.aspx','SystemParameterUseCaseMgt',
				'width=400,height=300'+',top='+(screen.availHeight-290)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
	}
}

function SystemParameterUseCaseMgt_Load()
{
	if(document.forms[0].FormMode.value=="close")
	{
		window.opener.document.forms[0].UseCaseMgmt.checked=true;
		window.close();	
	}
}


function SystemParameterUseCaseMgt_Ok()
{
if(document.forms[0].Success.value!="0")//Asif
document.forms[0].hdnValue.value="1";//Asif
	if(document.forms[0].Success.value=="0")
		window.close();
	window.document.forms['frmData'].submit();
}

function SystemParameter_UseCarrierClaims() 
{
	//Chnaged By Nitika for FNOL Reserve
    if (document.forms[0].MultiCovgPerClm.checked == true) 
	{
        document.forms[0].MultiCovgPerClm.checked = false;
		if (document.forms[0].UsePolicyInterface.checked == true) 
        {
            document.forms[0].FNOLReserve.disabled = false;
        }
        window.open('/RiskmasterUI/UI/Utilities/Manager/SystemParameterUseCarrierClaims.aspx', 'SystemParameterUseCarrierClaims',
				'width=400,height=300' + ',top=' + (screen.availHeight - 290) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
    }
	//Added By Nitika for FNOL Reserve
	else 
	{
        document.forms[0].FNOLReserve.disabled = true;
	}
}

function SystemParameterUseCarrierClaims_Load() {
    if (document.forms[0].FormMode.value == "close") {
        window.opener.document.forms[0].MultiCovgPerClm.checked = true;
        window.close();
    }
}

function SystemParameterUseCarrierClaims_Ok() {
    if (document.forms[0].Success.value != "0")//Asif
        document.forms[0].hdnValue.value = "1"; //Asif
    if (document.forms[0].Success.value == "0")
        window.close();
    window.document.forms['frmData'].submit();
}

function SystemParameter_UseScriptEditor()
{
	if(document.forms[0].UseScriptEditor.checked==true)
	{
		document.forms[0].UseScriptEditor.checked=false;
		window.open('/RiskmasterUI/UI/Utilities/Manager/SystemParameterUseScriptEditor.aspx','SystemParameterUseScriptEditor',
				'width=400,height=300'+',top='+(screen.availHeight-290)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
	}
}
function SystemParameterUseScriptEditor_Load()
{
	if(document.forms[0].FormMode.value=="close")
	{
		window.opener.document.forms[0].UseScriptEditor.checked=true;
		window.close();	
	}
}
function SystemParameterUseScriptEditor_Ok()
{
if(document.forms[0].Success.value!="0")//Asif
document.forms[0].hdnValue.value="1";//Asif
	if(document.forms[0].Success.value=="0")
		window.close();
	window.document.forms['frmData'].submit();
}

//Start: Sumit - 02/07/2010 - Functions for BRS Activation.
function SystemParameter_UseBRS() {
    if (document.forms[0].UseBRS.checked == true) {
        document.forms[0].UseBRS.checked = false;
        window.open('/RiskmasterUI/UI/Utilities/Manager/SystemParameterUseBRS.aspx', 'SystemParameterUseBRS',
				'width=400,height=300' + ',top=' + (screen.availHeight - 290) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
    }
}
function SystemParameterUseBRS_Load() {
    if (document.forms[0].FormMode.value == "close") {
        window.opener.document.forms[0].UseBRS.checked = true;
        window.close();
    }
}
function SystemParameterUseBRS_Ok() {
    if (document.forms[0].Success.value != "0")
        document.forms[0].hdnValue.value = "1";
    if (document.forms[0].Success.value == "0")
        window.close();
    window.document.forms['frmData'].submit();
}
//End - Sumit

//Start: Sumit - 02/07/2010 - Functions for GC LOB Activation.
function SystemParameter_UseGCLOB() {
    if (document.forms[0].UseGCLOB.checked == true) {
        document.forms[0].UseGCLOB.checked = false;
        window.open('/RiskmasterUI/UI/Utilities/Manager/SystemParameterUseGCLOB.aspx', 'SystemParameterUseGCLOB',
				'width=400,height=300' + ',top=' + (screen.availHeight - 290) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
    }
}
function SystemParameterUseGCLOB_Load() {
    if (document.forms[0].FormMode.value == "close") {
        window.opener.document.forms[0].UseGCLOB.checked = true;
        window.close();
    }
}
function SystemParameterUseLOB_Ok() {
    if (document.forms[0].Success.value != "0")
        document.forms[0].hdnValue.value = "1";
    if (document.forms[0].Success.value == "0")
        window.close();
    window.document.forms['frmData'].submit();
}
//End - Sumit

//Start: Sumit - 02/07/2010 - Functions for DI LOB Activation.
function SystemParameter_UseDILOB() {
    if (document.forms[0].UseDILOB.checked == true) {
        document.forms[0].UseDILOB.checked = false;
        window.open('/RiskmasterUI/UI/Utilities/Manager/SystemParameterUseDILOB.aspx', 'SystemParameterUseDILOB',
				'width=400,height=300' + ',top=' + (screen.availHeight - 290) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
    }
}
function SystemParameterUseDILOB_Load() {
    if (document.forms[0].FormMode.value == "close") {
        window.opener.document.forms[0].UseDILOB.checked = true;
        window.close();
    }
}
//End - Sumit

//Start: Sumit - 02/07/2010 - Functions for PC LOB Activation.
function SystemParameter_UsePCLOB() {
    if (document.forms[0].UsePCLOB.checked == true) {
        document.forms[0].UsePCLOB.checked = false;
        window.open('/RiskmasterUI/UI/Utilities/Manager/SystemParameterUsePCLOB.aspx', 'SystemParameterUsePCLOB',
				'width=400,height=300' + ',top=' + (screen.availHeight - 290) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
    }
}
function SystemParameterUsePCLOB_Load() {
    if (document.forms[0].FormMode.value == "close") {
        window.opener.document.forms[0].UsePCLOB.checked = true;
        window.close();
    }
}
//End - Sumit

//Start: Sumit - 02/07/2010 - Functions for VA LOB Activation.
function SystemParameter_UseVALOB() {
    if (document.forms[0].UseVALOB.checked == true) {
        document.forms[0].UseVALOB.checked = false;
        window.open('/RiskmasterUI/UI/Utilities/Manager/SystemParameterUseVALOB.aspx', 'SystemParameterUseVALOB',
				'width=400,height=300' + ',top=' + (screen.availHeight - 290) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
    }
}
function SystemParameterUseVALOB_Load() {
    if (document.forms[0].FormMode.value == "close") {
        window.opener.document.forms[0].UseVALOB.checked = true;
        window.close();
    }
}
//End - Sumit

//Start: Sumit - 02/07/2010 - Functions for WC LOB Activation.
function SystemParameter_UseWCLOB() {
    if (document.forms[0].UseWCLOB.checked == true) {
        document.forms[0].UseWCLOB.checked = false;
        window.open('/RiskmasterUI/UI/Utilities/Manager/SystemParameterUseWCLOB.aspx', 'SystemParameterUseWCLOB',
				'width=400,height=300' + ',top=' + (screen.availHeight - 290) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
    }
}
function SystemParameterUseWCLOB_Load() {
    if (document.forms[0].FormMode.value == "close") {
        window.opener.document.forms[0].UseWCLOB.checked = true;
        window.close();
    }
}
//End - Sumit


function SystemParameter_UseEnhPol()
{
	if(document.forms[0].UseEnhPol.checked==true)
	{
		document.forms[0].UseEnhPol.checked=false;
		//Sumit - 09/22/2009 MITS#18227 Check if Activation code window needs to be displayed or not.
		//Start - Sumit - 06/23/2010 Now Activation code will be asked for every LOB
		//if(document.forms[0].hdnIsActCodeReq.value=="Y")
		window.open('/RiskmasterUI/UI/Utilities/Manager/SystemParameterUseEnhPol.aspx','SystemParameterUseEnhPol',
				'width=400,height=300'+',top='+(screen.availHeight-290)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
//		else
		//		    document.forms[0].UseEnhPol.checked=true;
        //End - Sumit
	}
	else
		//Added by Divya 04/23/2007 Disable billing checkbox when enhance policy is disabled
	{
		if(document.forms[0].UseEnhPol.checked==false)
		{
		//Sumit - 09/22/2009 MITS#18227 Commented as the billing flag is disabled from 
//		  document.forms[0].BillingFlag.disabled=true;
		    //Changed by gagan for MITS 18278 : Start
		    //Start:Change by kuladeep 04/20/2010 Retrofit for MITS 18278 
		    //window.document.forms[0].ShowJurisdiction.checked = true;
		    //window.document.forms[0].ShowJurisdiction.disabled = false;
		    //window.document.forms[0].State1.disabled = false;
		    //window.document.forms[0].State2.disabled = false;
		    //End:Change by kuladeep 04/20/2010 Retrofit for MITS 18278
		    //Changed by gagan for MITS 18278 : End
		}
	}
}
//Start:Added by Nitin Goel:Created a method to disable property/vehicle filter,06/09/2010
function DisablePolicyFilterFlag() 
{
    if (document.forms[0].UseEnhPol.value != null)
         {
             if (document.forms[0].UseEnhPol.checked == true)
             {
                document.forms[0].chkIsPolicyFilter.disabled = false;
              }
            else if (document.forms[0].UseEnhPol.checked == false) 
            {
                document.forms[0].chkIsPolicyFilter.disabled = true;
                document.forms[0].chkIsPolicyFilter.checked = false;
            }
        }
    
}
//End:Nitin Goel:Created a method to disable property/vehicle filter,06/09/2010

//Changed by gagan for MITS 18278 : Start
//Changed by Saurabh Arora for MITS 19205 :Start
// nnorouzi; generalizing this function for similar scenarios like Auto Assign Adjuster
function EnableDisableRB(parentID) {
    var parent = document.getElementById(parentID);
    var option1 = document.getElementById(parentID + '1');
    var option2 = document.getElementById(parentID + '2');
    if (parent && option1 && option2) {
        if (parent.checked) {
            option1.disabled = false;
            option2.disabled = false;
            option1.checked = true
        }
        else {
            option1.checked = false;
            option1.disabled = true;
            option2.checked = false;
            option2.disabled = true;
        }
    }
}
//Changed by Saurabh Arora for MITS 19205 :End
//Changed by gagan for MITS 18278 : end

function EnableDisableAddressSetting(parentID) {
    var parent = document.getElementById(parentID);
    var option1 = document.getElementById('AddModifyAddress');

    if (parent && option1 ) {
        if (parent.checked) {
            option1.disabled = false;
        }
        else {
            option1.checked = false;
            option1.disabled = true;
        }
    }
}
function SetRB() {
    var boolValue = false;
    var radio;
    var inputs;
    var tds = document.getElementsByTagName("td");
    for (var i = 0; i < tds.length; i++) {
        if (tds[i].id && tds[i].id != "") {
            if (tds[i].id.indexOf('GroupParentRB') == 0) {
                for (var j = 0; j < tds[i].children.length; j++) {
                    if (tds[i].children[j].children[0].type.toUpperCase() == 'CHECKBOX') {//the first child is a SPAN element and inside the SPAN there is the checkbox
                        if (tds[i].children[j].children[0].checked == true) {
                            boolValue = false;
                        }
                        else {
                            boolValue = true;
                        }
                        for (var k = 1; k <= 2; k++) {
                            radio = document.getElementById(tds[i].children[j].children[0].id + k);
                            if (radio) {
                                radio.disabled = boolValue;
                            }
                        }
                        break;
                    }
                }
            }
        }
    }
}
//Changed by gagan for MITS 18278 : end


function SystemParameterUseEnhPol_Load() {
    //Start(06/23/2010) - Sumit - Set selected LOB
    var objSelectedLOB = window.opener.document.forms[0].hdnLOBSelected;
    if (objSelectedLOB != null) {
        document.forms[0].SelectedLOB.value = objSelectedLOB.value;
    }
    //End - Sumit
    if (document.forms[0].FormMode.value == "close") {
        window.opener.document.forms[0].UseEnhPol.checked = true;
		//Start:Added by Nitin Goel:Created a method to disable property/vehicle filter,06/09/2010
        window.opener.document.forms[0].chkIsPolicyFilter.disabled = false;
		//End:Nitin Goel:Created a method to disable property/vehicle filter,06/09/2010
        //Added by Divya 04/23/2007 Enable billing checkbox 
        //window.opener.document.forms[0].BillingFlag.disabled = false;
        //window.opener.document.forms[0].BillingFlag.checked = true;
        //Changed by gagan for MITS 18278 : Start
        //Start:Change by kuladeep 04/20/2010 Retrofit for MITS 18278
        //window.opener.document.forms[0].ShowJurisdiction.checked = false;
        //window.opener.document.forms[0].ShowJurisdiction.disabled = true;
        //window.opener.document.forms[0].State1.disabled = true;
        //window.opener.document.forms[0].State2.disabled = true;
        //End:Change by kuladeep 04/20/2010 Retrofit for MITS 18278
        //Changed by gagan for MITS 18278 : End  
        window.close();
    }        
}


function SystemParameterUseEnhPol_Ok()
{
if(document.forms[0].Success.value!="0")//Asif
document.forms[0].hdnValue.value="1";//Asif
	if(document.forms[0].Success.value=="0")
		window.close();
	window.document.forms['frmData'].submit();
}

function SystemParameter_Load()
{
	document.forms[0].onsubmit=OnFormSubmit;
	if(self.parent!=null)
	{
		if(haveProperty(self.parent,"wndProgress") && self.parent.wndProgress!=null)
		{
			self.parent.wndProgress.close();
			self.parent.wndProgress=null;
		}
	}
	ShowConfigButton();
	SubBankAcct();
//	subEnhPolBilling();
}

//Added by Divya 04/23/2007 On page load Disable billing checkbox when enhance policy is disabled
function subEnhPolBilling()
{  

   if(document.forms[0].UseEnhPol.checked==false)
		{ 
		  document.forms[0].BillingFlag.disabled=true;
		}
	

}

function SystemParameter_CP()
{
			window.open('/RiskmasterUI/UI/Utilities/Manager/SystemParameterCPCredentials.aspx','SystemParameterCPCredentials',
			'width=400,height=290'+',top='+(screen.availHeight-290)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
}

function SystemParameterCP_Ok()
{
	window.opener.document.forms[0].CPUserID.value=window.forms[0].User.value
	window.opener.document.forms[0].CPPass.value=window.forms[0].Pwd.value
	window.opener.document.forms[0].CPUrl.value=window.forms[0].CPUrl.value
	if(self.opener!=null && self.opener!=self)
		self.close();
	else
		self.setTimeout("urlNavigate('/RiskmasterUI/UI/Utilities/Manager/GeneralSystemParameterSetup.aspx')");
	return true;
}

function SystemParameterCP_Cancel()
{
	if(self.opener!=null && self.opener!=self)
		self.close();
	else
		self.setTimeout("urlNavigate('/RiskmasterUI/UI/Utilities/Manager/GeneralSystemParameterSetup.aspx')");
	return true;
}

function CPCredentials_Load()
{
	document.forms[0].User.value=window.opener.document.forms[0].CPUserID.value;
	document.forms[0].Pwd.value=window.opener.document.forms[0].CPPass.value;
	document.forms[0].CPUrl.value=window.opener.document.forms[0].CPUrl.value;
}


function SystemParameter_FinHist()
{
	document.forms[0].hdAction.value='FinHistChange';
	window.document.forms['frmData'].submit();
}

function SystemParameter_SubBankAccCheck()
{
	document.forms[0].hdAction.value='SubBankAcct';
	window.document.forms['frmData'].submit();
}
function WindowOpen()
{	
	window.open('PopUp.aspx','',
			"width=700,height=400,top="+(screen.availHeight-400)/2+",left="+(screen.availWidth-700)/2+",resizable=yes,scrollbars=yes");
	return false;								
}

function FileTransfer()
{
	document.forms[0].hdAction.value='Upload';
	document.forms[0].btnSave.style.display = "none";
	pleaseWait.Show('start');
	window.document.forms['frmData'].submit();
	return true;	
}

function BRSImportSchedule_Upload()
{
	var lbl=window.opener.document.getElementById("SelectFile");	
	if(document.forms[0].filename.value!='')
	{
		if(window.opener.document.forms[0].Files.value=='')
			window.opener.document.forms[0].Files.value= document.forms[0].filename.value;
		else
			window.opener.document.forms[0].Files.value=window.opener.document.forms[0].Files.value + ';' + document.forms[0].filename.value;
	    //lbl.parentNode.previousSibling.innerText = 'Files to Import :-' +  window.opener.document.forms[0].Files.value;
		//lbl.parentNode.previousSibling.innerText = PopUpLabel.lblFileToImport + window.opener.document.forms[0].Files.value; //MITS 34822 hlv

		var objlblFileImp = window.opener.document.getElementById('lblFileImp'); //Jira id: 7572
		objlblFileImp.innerText = PopUpLabel.lblFileToImport + window.opener.document.forms[0].Files.value;

		window.close();
	}
		
	lbl=null;
	document.forms[0].btnSave.style.display = "";
}

function SysParamForm_Save()
{
	document.forms[0].SysCmd.value='5';
	return true;
}
//mbahl3 strataware enhancement
function UtilitiesForm_Save()
{
    if (document.getElementById("EnableSingleuser")!=null){
        if (document.getElementById("EnableSingleuser").checked) {
            if ((document.getElementById("txtUser").value == "") || (document.getElementById("txtUser").value == "") || (document.getElementById("StratawareUserID").value == "") || (document.getElementById("StratawareUserID").value == " ") || (document.getElementById("StratawareUserID").value == "0")) {
                // alert("please select user for stratware");
                alert(parent.CommonValidations.SelectUser);
                return false;
            }
        }
    }
    
	document.forms[0].SysCmd.value='5';
	return true;
}

//mbahl3 strataware enhancement
function FROIBatchPrinting_LoadLowLevel()
{
	document.forms[0].hdDataChanged.value='-1';
	document.forms[0].SysCmd.value='5';
	window.document.forms['frmData'].submit();
}

function FROIBatchPrinting_Load()
{
	if(document.forms[0].hdDataChanged.value=='-1')
		m_DataChanged=true;
}

///Functions for HolidaySetup moved to Support Screens
//function OnHolidaySave()
//{
//	document.forms[0].FormMode.value="close";
//	return true;
//}
///Functions for HolidaySetup moved to Support Screens
function TableField_UpdateList(sControl)
{
	var objCtrl=null;
	objCtrl=eval('document.forms[0].' + sControl);
	if(objCtrl==null)
		return false;

	var sId="";
	for(var i=0;i<objCtrl.length;i++)
	{
		if(sId!="")
			sId=sId+" ";
		sId=sId+objCtrl.options[i].value;
	}
	objCtrl=null;
	objCtrl=eval("document.forms[0]." + sControl + "_lst");
	if(objCtrl!=null)
		objCtrl.value=sId;
}

//Raman Bhatia --- ACROSOFT INTEGRATION -- RMX PHASE 2

function SystemParameter_UseAcrosoftInterface()
{
	if(document.forms[0].UseAcrosoftInterface.checked==true)
	{
		document.forms[0].UseAcrosoftInterface.checked=false;
		window.open('/RiskmasterUI/UI/Utilities/Manager/SystemParameterUseAcrosoftInterface.aspx','SystemParameterUseAcrosoftInterface',
				'width=400,height=300'+',top='+(screen.availHeight-290)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
	}
}

function SystemParameterUseAcrosoftInterface_Load()
{
	if(document.forms[0].FormMode.value=="close")
	{
		window.opener.document.forms[0].UseAcrosoftInterface.checked=true;
		//Mona:PaperVisionMerge: Animesh inserted for mutually exclusive functionality between MCM and PaperVision
		if (window.opener.document.forms[0].UsePaperVisionInterface.checked==true)
		{
		    window.opener.document.forms[0].UsePaperVisionInterface.checked=false;
		}
		if (window.opener.document.forms[0].UseMediaViewInterface.checked == true) {
		    window.opener.document.forms[0].UseMediaViewInterface.checked = false;
		}

		//tanwar2 - ImageRight - start
		if (window.opener.document.forms[0].UseImgRight) {
		    window.opener.document.forms[0].UseImgRight.checked = false;
		}
		if (window.opener.document.forms[0].UseImgRighWebService) {
		    window.opener.document.forms[0].UseImgRighWebService.checked = false;
		    window.opener.document.forms[0].UseImgRighWebService.disabled = true;
		}
        //tanwar2 - ImageRight - end
		window.close();	
	}
}


function SystemParameterUseAcrosoftInterface_Ok()
{
if(document.forms[0].Success.value!="0")//Asif
document.forms[0].hdnValue.value="1";//Asif
	if(document.forms[0].Success.value=="0")
		window.close();
	window.document.forms['frmData'].submit();
}

//rbhatia4:R8: Use Media View Setting : July 05 2011

function SystemParameter_UseMediaViewInterface() {
    if (document.forms[0].UseMediaViewInterface.checked == true) {
        document.forms[0].UseMediaViewInterface.checked = false;
        window.open('/RiskmasterUI/UI/Utilities/Manager/SystemParameterUseMediaViewInterface.aspx', 'SystemParameterUseMediaViewInterface',
				'width=400,height=300' + ',top=' + (screen.availHeight - 290) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
    }
}

function SystemParameterUseMediaViewInterface_Load() {
    if (document.forms[0].FormMode.value == "close") {
        window.opener.document.forms[0].UseMediaViewInterface.checked = true;
        if (window.opener.document.forms[0].UsePaperVisionInterface.checked == true) {
            window.opener.document.forms[0].UsePaperVisionInterface.checked = false;
        }
        if (window.opener.document.forms[0].UseAcrosoftInterface.checked == true) {
            window.opener.document.forms[0].UseAcrosoftInterface.checked = false;
        }

        window.close();
    }
}


function SystemParameterUseMediaViewInterface_Ok() {
    if (document.forms[0].Success.value != "0")
        document.forms[0].hdnValue.value = "1"; 
    if (document.forms[0].Success.value == "0")
        window.close();
    window.document.forms['frmData'].submit();
}


//Mona:PaperVisionMerge : Animesh Inserted For Papervision Enhancment
function SystemParameter_UsePaperVision()
{
	if(document.forms[0].UsePaperVisionInterface.checked==true)
	{
		document.forms[0].UsePaperVisionInterface.checked=false;
		window.open('/RiskmasterUI/UI/Utilities/Manager/SystemParameterUsePaperVision.aspx','SystemParameterUsePaperVision',
				'width=400,height=300'+',top='+(screen.availHeight-290)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
	}
}

function SystemParameterUsePaperVision_Load()
{
	if(document.forms[0].FormMode.value=="close")
	{
		window.opener.document.forms[0].UsePaperVisionInterface.checked=true;
		//Animesh inserted for mutually exclusive functionality between MCM and PaperVision
		if (window.opener.document.forms[0].UseAcrosoftInterface.checked==true)
		{
		    window.opener.document.forms[0].UseAcrosoftInterface.checked=false;
		}
		if (window.opener.document.forms[0].UseMediaViewInterface.checked == true) {
		    window.opener.document.forms[0].UseMediaViewInterface.checked = false;
		}

		//tanwar2 - ImageRight - start
		if (window.opener.document.forms[0].UseImgRight) {
		    window.opener.document.forms[0].UseImgRight.checked = false;
		}
		if (window.opener.document.forms[0].UseImgRighWebService) {
		    window.opener.document.forms[0].UseImgRighWebService.checked = false;
		    window.opener.document.forms[0].UseImgRighWebService.disabled = true;
		}
		//tanwar2 - ImageRight - end
		window.close();	
	}
}


function SystemParameterUsePaperVision_Ok()
{
if(document.forms[0].Success.value!="0")
document.forms[0].hdnValue.value="1";
	if(document.forms[0].Success.value=="0")
		window.close();
	window.document.forms['frmData'].submit();
}
//Mona:PaperVisionMerge : Animesh insertion Ends


//Moved these methods from form.js

function GoFocus()
{
	EnableDisable();
}

function EnableDisable()
{
    //npadhy RMSC retrofit Starts
	var bIsDays=false;
	var bIsDate = false;
	//npadhy RMSC retrofit Ends
	if (trim(document.forms[0].InstGenType_codelookup.value.substring(0,1))=="C")
	{	   
	    for (i=1;i<=12;i++)
		{
		    //npadhy RMSC retrofit Starts
			if (bIsDays==false)
			{
				ctrl3=document.getElementById('GenLagDays'+i);
				ctrl4=document.getElementById('DueDays'+i);
				if (Trim(ctrl3.value)!="" || Trim(ctrl4.value)!="")
				{
					bIsDays=true;
				}
			}
			if(bIsDate==false)
			{
				ctrl5=document.getElementById('GenDate'+i);
				ctrl6=document.getElementById('DueDate'+i);
				if (Trim(ctrl5.value)!="" || Trim(ctrl6.value)!="")
				{
					bIsDate=true;
				}
			}
		}
        //npadhy RMSC retrofit Ends
		document.forms[0].GenDay_codelookup.value="";
		document.forms[0].DueDays.value="";
		document.forms[0].Frequency_codelookup.value="";
		document.forms[0].Frequency_codelookup_cid.value="";
		document.forms[0].Frequency_codelookup.disabled=true;
		document.forms[0].GenDay_codelookup.disabled=true;
		//Shruti
		document.forms[0].Frequency_codelookupbtn.disabled=true;
		document.forms[0].GenDay_codelookupbtn.disabled=true;
		document.forms[0].DueDays.disabled=true;
		
		//npadhy RMSC retrofit Starts
		document.forms[0].DueDate.value="";
		document.forms[0].GenDate.value="";
		document.forms[0].DueDate.disabled=true;
		document.forms[0].GenDate.disabled=true;
		document.forms[0].DueDate.style.backgroundColor="silver";
		document.forms[0].GenDate.style.backgroundColor="silver";
		document.forms[0].DueDatebtn.disabled=true;
		document.forms[0].GenDatebtn.disabled=true;
		document.forms[0].DueDatebtn.style.backgroundColor="silver";
		document.forms[0].GenDatebtn.style.backgroundColor="silver";
		//npadhy RMSC retrofit Ends
		
		//Changed by Gagan for MITS 9475 : Start
		
		document.forms[0].Frequency_codelookup.style.backgroundColor="silver";
		document.forms[0].GenDay_codelookup.style.backgroundColor="silver";
		document.forms[0].DueDays.style.backgroundColor="silver";
		
		//Changed by Gagan for MITS 9475 : End
		
		for (i=1;i<=12;i++)
		{
            //npadhy RMSC retrofit Starts
		    if (bIsDays == true && bIsDate == false) 
		    {
		        ctrl3 = document.getElementById('GenLagDays' + i);
		        ctrl4 = document.getElementById('DueDays' + i);
		        ctrl5 = document.getElementById('GenDate' + i);
		        ctrl6 = document.getElementById('DueDate' + i);
		        ctrl7 = document.getElementById('GenDateBtn' + i);
		        ctrl8 = document.getElementById('DueDateBtn' + i);
		        ctrl5.disabled = true;
		        ctrl7.disabled = true;
		        ctrl6.disabled = true;
		        ctrl8.disabled = true;
		        ctrl5.value = "";
		        ctrl7.value = "";
		        ctrl6.value = "";
		        ctrl8.value = "";
		        ctrl5.style.backgroundColor = "silver";
		        ctrl6.style.backgroundColor = "silver";
		        ctrl7.style.backgroundColor = "silver";
		        ctrl8.style.backgroundColor = "silver";
		        ctrl3.disabled = false;
		        ctrl4.disabled = false;
		        //Changed by Shruti for MITS 9513 : Start
		        ctrl3.style.backgroundColor = "";
		        ctrl4.style.backgroundColor = "";
		    }
		    else if (bIsDays == false && bIsDate == true) 
		    {
		        ctrl3 = document.getElementById('GenLagDays' + i);
		        ctrl4 = document.getElementById('DueDays' + i);
		        ctrl5 = document.getElementById('GenDate' + i);
		        ctrl6 = document.getElementById('DueDate' + i);
		        ctrl7 = document.getElementById('GenDateBtn' + i);
		        ctrl8 = document.getElementById('DueDateBtn' + i);
		        ctrl5.disabled = false;
		        ctrl7.disabled = false;
		        ctrl6.disabled = false;
		        ctrl8.disabled = false;
		        ctrl5.style.backgroundColor = "";
		        ctrl6.style.backgroundColor = "";
		        ctrl7.style.backgroundColor = "";
		        ctrl8.style.backgroundColor = "";
		        ctrl3.disabled = true;
		        ctrl4.disabled = true;
		        ctrl3.value = "";
		        ctrl4.value = "";
		        //Changed by Shruti for MITS 9513 : Start
		        ctrl3.style.backgroundColor = "silver";
		        ctrl4.style.backgroundColor = "silver";
		    }
		    else 
		    {
		        ctrl3 = document.getElementById('GenLagDays' + i);
		        ctrl4 = document.getElementById('DueDays' + i);
		        ctrl5 = document.getElementById('GenDate' + i);
		        ctrl6 = document.getElementById('DueDate' + i);
		        ctrl7 = document.getElementById('GenDateBtn' + i);
		        ctrl8 = document.getElementById('DueDateBtn' + i);
		        ctrl5.disabled = false;
		        ctrl7.disabled = false;
		        ctrl6.disabled = false;
		        ctrl8.disabled = false;
		        ctrl5.style.backgroundColor = "";
		        ctrl6.style.backgroundColor = "";
		        ctrl7.style.backgroundColor = "";
		        ctrl8.style.backgroundColor = "";
		        ctrl3.disabled = false;
		        ctrl4.disabled = false;
		        //Changed by Shruti for MITS 9513 : Start
		        ctrl3.style.backgroundColor = "";
		        ctrl4.style.backgroundColor = "";
		        //Changed by Shruti for MITS 9513 : End
		    }	  
        //npadhy RMSC retrofit Ends
		}	  
	}
	else if(trim(document.forms[0].InstGenType_codelookup.value.substring(0,1))=="F")
	{
        //npadhy RMSC retrofit Starts
		if (Trim(document.forms[0].GenDay_codelookup.value)!="" || Trim(document.forms[0].DueDays.value)!="")
		{
			bIsDays=true; 
		}
		if (Trim(document.forms[0].DueDate.value)!="" || Trim(document.forms[0].GenDate.value))
		{
			bIsDate=true; 
		}
	   	document.forms[0].Frequency_codelookup.disabled=false;
		if (bIsDays==true && bIsDate==false)
		{
			document.forms[0].GenDay_codelookup.disabled=false;
	   	   	document.forms[0].Frequency_codelookupbtn.disabled=false;
			document.forms[0].GenDay_codelookupbtn.disabled=false;
			document.forms[0].DueDays.disabled=false;
			document.forms[0].DueDate.disabled=true;
			document.forms[0].GenDate.disabled=true;
			document.forms[0].DueDatebtn.disabled=true;
			document.forms[0].GenDatebtn.disabled=true;
			document.forms[0].Frequency_codelookup.style.backgroundColor="";
			document.forms[0].GenDay_codelookup.style.backgroundColor="";
			document.forms[0].DueDays.style.backgroundColor="";
			document.forms[0].DueDate.value="";
			document.forms[0].GenDate.value="";
			document.forms[0].DueDate.style.backgroundColor="silver";
			document.forms[0].GenDate.style.backgroundColor="silver";
			document.forms[0].DueDatebtn.style.backgroundColor="silver";
			document.forms[0].GenDatebtn.style.backgroundColor="silver";
		}
		else if (bIsDays==false && bIsDate==true)
		{
			document.forms[0].GenDay_codelookup.disabled=true;
	   	   	document.forms[0].Frequency_codelookupbtn.disabled=false;
			document.forms[0].GenDay_codelookupbtn.disabled=true;
			document.forms[0].DueDays.disabled=true;
			document.forms[0].DueDate.disabled=false;
			document.forms[0].GenDate.disabled=false;
			document.forms[0].DueDatebtn.disabled=false;
			document.forms[0].GenDatebtn.disabled=false;
			document.forms[0].Frequency_codelookup.style.backgroundColor="";
			document.forms[0].GenDay_codelookup.style.backgroundColor="silver";
			document.forms[0].DueDays.style.backgroundColor="silver";
			document.forms[0].GenDay_codelookup.value="";
			document.forms[0].DueDays.value="";
			document.forms[0].DueDate.style.backgroundColor="";
			document.forms[0].GenDate.style.backgroundColor="";
			document.forms[0].DueDatebtn.style.backgroundColor="";
			document.forms[0].GenDatebtn.style.backgroundColor="";
		}
		else
		{
			document.forms[0].GenDay_codelookup.disabled=false;
	   		//Shruti
		   	document.forms[0].Frequency_codelookupbtn.disabled=false;
			document.forms[0].GenDay_codelookupbtn.disabled=false;
			document.forms[0].DueDays.disabled=false;
			//Changed by Gagan for MITS 9475 : Start
			document.forms[0].Frequency_codelookup.style.backgroundColor="";
			document.forms[0].GenDay_codelookup.style.backgroundColor="";
			document.forms[0].DueDays.style.backgroundColor="";
			//Changed by Gagan for MITS 9475 : End
			document.forms[0].GenDay_codelookup.value="";
			document.forms[0].DueDays.value="";
			document.forms[0].Frequency_codelookup.value="";
			document.forms[0].Frequency_codelookup_cid.value="";
			document.forms[0].DueDate.value="";
			document.forms[0].GenDate.value="";
			document.forms[0].DueDate.disabled=false;
			document.forms[0].GenDate.disabled=false;
			document.forms[0].DueDate.style.backgroundColor="";
			document.forms[0].GenDate.style.backgroundColor="";
			document.forms[0].DueDatebtn.disabled=false;
			document.forms[0].GenDatebtn.disabled=false;
			document.forms[0].DueDatebtn.style.backgroundColor="";
			document.forms[0].GenDatebtn.style.backgroundColor="";
		}		
		for (i=1;i<=12;i++)
		{
		  ctrl3=document.getElementById('GenLagDays'+i);
		  ctrl4=document.getElementById('DueDays'+i);
		  ctrl3.value="";
		  ctrl4.value="";
		  ctrl3.disabled=true;
          ctrl4.disabled=true;
          //Changed by Shruti for MITS 9513 : Start
		  ctrl3.style.backgroundColor="silver";
		  ctrl4.style.backgroundColor="silver";
		  //Changed by Shruti for MITS 9513 : End
		  ctrl5=document.getElementById('GenDate'+i);
		  ctrl6=document.getElementById('DueDate'+i);
		  ctrl7=document.getElementById('GenDateBtn'+i);
		  ctrl8=document.getElementById('DueDateBtn'+i);
		  ctrl5.disabled=true;
		  ctrl7.disabled=true;
		  ctrl8.disabled=true;
		  ctrl6.disabled=true;
		  ctrl5.value="";
		  ctrl7.value="";
		  ctrl8.value="";
		  ctrl6.value="";
		  ctrl5.style.backgroundColor="silver";
		  ctrl6.style.backgroundColor="silver";
		  ctrl7.style.backgroundColor="silver";
		  ctrl8.style.backgroundColor="silver";
		}
        //npadhy RMSC retrofit Ends
	}
	
}
///Functions for FL Max Rate moved to Support Screens
//function CheckForMaxRateUpdated()
//{
//	if (document.forms[0].hdnRefresh.value=="Y")
//	{
//		document.forms[0].hdnNewOld.value="Y";
//		if (document.forms[0].hdnMessage.value=="Saved")
//	    {
//		    window.opener.document.forms[0].submit();
//		    window.close();
//		}
//		document.forms[0].hdnRefresh.value="N";	
//		
//	}
//}

//function ValidateMaxRate()
//{

//   document.forms[0].hdnNewOld.value="O";
//   document.forms[0].hdnRefresh.value="Y";
//   if(CheckRequiredFields())
//        return true;
//   else
//        return false;
//}
	
// Shruti FL Max Rate Implementation
//function CheckRequiredFields()
//{
//    var Year = document.getElementById('Year');
//    var MaxRate = document.getElementById('MaxRate');
//    
//    var iYear;
//    var dMaxRate;
//    if(isNaN(iYear = parseInt(Year.value,10)))
//    {
//        alert('Please Enter the Year in format YYYY');
//        Year.value = "";
//        Year.focus();
//        return false;
//    }
//    else if(iYear <= 999 )
//    {
//        alert('Please Enter the Year in format YYYY');
//        Year.value = "";
//        Year.focus();
//        return false;
//    }
//    if( Year.value.length != 4 ) 
//    {
//        alert('Please Enter the Year in format YYYY');
//        Year.value = "";
//        Year.focus();
//        return false;
//    }
//    if(isNaN(dMaxRate = parseFloat(MaxRate.value)))
//    {
//        alert('Please Enter a numeric value for Max Rate');
//        MaxRate.value = "";
//        MaxRate.focus();
//        return false;
//    }
//    else if(dMaxRate <= 0)
//    {
//        alert('Max Rate should be a positive amount');
//        MaxRate.value = "";
//        MaxRate.focus();
//        return false;
//    }
//    if(trim(MaxRate.value) == '')
//    {
//        alert('Please Enter a numeric value for Max Rate');
//        MaxRate.value = "";
//        MaxRate.focus();
//        return false;
//    }
//    return true;
//}	

//function OnYearChange()
//{
//    var Year = document.getElementById('Year');
//    var bError = "";
//    var iYear;
//    if(isNaN(iYear = parseInt(Year.value,10)))
//    {
//        bError = "true";
//    }
//    else if(iYear <= 999 )
//    {
//        bError = "true";
//    }
//    if( Year.value.length != 4 ) 
//    {
//        bError = "true";
//    }
//    if(bError == "true")
//    {
//	alert('Please Enter the Year in format YYYY');
//        Year.value = "";
//        Year.focus();
//        return false;
//    }
//}

//function OnMaxRateChange()
//{
//    var MaxRate = document.getElementById('MaxRate');
//    var dMaxRate = Number(MaxRate.value);

//    if(isNaN(dMaxRate))
//    {
//        alert('Please Enter a numeric value for Max Rate');
//        MaxRate.value = "";
//        MaxRate.focus();
//        return false;
//    }
//    else if(dMaxRate <= 0)
//    {
//        alert('Max Rate should be a positive amount');
//        MaxRate.value = "";
//        MaxRate.focus();
//        return false;
//    }
//    if(trim(MaxRate.value) == '')
//    {
//        alert('Please Enter a numeric value for Max Rate');
//        MaxRate.value = "";
//        MaxRate.focus();
//        return false;
//    }
//}
///Functions for FL Max Rate moved to Support Screens
//Start by Shivendu for supplemental Grid

function Trim(TRIM_VALUE)
{
	if(TRIM_VALUE.length < 1)
	{
		return"";
	}
	TRIM_VALUE = RTrim(TRIM_VALUE);
	TRIM_VALUE = LTrim(TRIM_VALUE);
	if(TRIM_VALUE=="")
	{
		return "";
	}
	else
	{
		return TRIM_VALUE;
	}
} //End Function

function RTrim(VALUE)
{
	var w_space = String.fromCharCode(32);
	var v_length = VALUE.length;
	var strTemp = "";
	if(v_length < 0)
	{
		return"";
	}
	var iTemp = v_length -1;
	while(iTemp > -1)
	{
		if(VALUE.charAt(iTemp) == w_space)
		{
		}
		else
		{
			strTemp = VALUE.substring(0,iTemp +1);
			break;
		}
		iTemp = iTemp-1;
	} //End While
	return strTemp;
} //End Function

function LTrim(VALUE)
{
	var w_space = String.fromCharCode(32);
	if(v_length < 1)
	{
		return"";
	}
	var v_length = VALUE.length;
	var strTemp = "";
	var iTemp = 0;
	while(iTemp < v_length)
	{
		if(VALUE.charAt(iTemp) == w_space)
		{
		}
		else
		{
			strTemp = VALUE.substring(iTemp,v_length);
			break;
		}
		iTemp = iTemp + 1;
	} //End While
	return strTemp;
} //End Function
function GridPropertiesScreen_Load()
{
var colXML = "";
if(document.forms[0].UserPrompt.value==null || document.forms[0].UserPrompt.value == "" || document.forms[0].SysFieldName.value==null || document.forms[0].SysFieldName.value == "")
{
alert("Please enter UserPrompt and SysFieldName before clciking");
return false;
}
document.forms[0].hdnAction.value = 'ModifyGrid';
document.forms[0].submit();

return false;
}
function AddColumn_Load()
{

if(document.forms[0].colCount.value==document.forms[0].maxCols.value)
{
alert("You have reached the Maximum Allowable Columns Limit. No more columns can be added to the Grid");
return false;
}
document.forms[0].hdnaction.value = 'Save';
document.forms[0].submit();
return true;
}
 
 function CancelAddColumn()
{

 document.forms[0].colName.value = "";
 document.forms[0].hdnaction.value = 'Cancel';
 return true;
}
 function SubmitColumnName()
{

    var xmlDoc;
    if(document.forms[0].colName.value == null || Trim(document.forms[0].colName.value) == "")
    {
        alert("Column name cannot be empty");
        return false;

    }
    else
       {
       
         // code for IE
          //if (window.ActiveXObject)
		  if ("ActiveXObject" in window || window.ActiveXObject) //JIRA id: RMA-4329 --govind IE11 issue
          {
          xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
          }
          // code for Mozilla, Firefox, Opera, etc.
          else if (document.implementation && document.implementation.createDocument)
          {
          xmlDoc=document.implementation.createDocument("","",null);
          }
          else
          {
          alert('Your browser cannot handle this script');
          }
          
          xmlDoc.loadXML(document.forms[0].colXML.value);
          var x = xmlDoc.getElementsByTagName('title');
          for (i=0;i<x.length;i++)
          {
            
             var sColumn = new String(x[i].childNodes[0].nodeValue);
             var sTextColumn = new String(document.forms[0].colName.value);
             if(sColumn.toUpperCase() == sTextColumn.toUpperCase())
             {
               alert("Column Already Exists");
               CancelAddColumn();
               return;
             }
          
          }
         document.forms[0].colCount.value = Number(document.forms[0].colCount.value) + 1;
         document.forms[0].hdnaction.value = 'Save';
         document.forms[0].submit();
         return true;

       
    }

         
}
 
         function ZapatecGridDisplay()
          {
          //debugger;
          if(document.forms[0].gridProperty!=null)
          document.forms[0].gridProperty.value = 1;
          
          if(document.forms[0].duplicateField!=null)
          if(document.forms[0].duplicateField.value=="Duplicate")
          {
              alert("Cannot modify:The field with System Field name " + document.forms[0].sysFieldName.value + " already exists in the table");
              self.close();
              return true;
           
          }

          if((document.forms[0].colXML.value == "" || document.forms[0].colXML.value==null) && document.forms[0].rowId.value == 0)
          {
              var xmlDoc=loadXMLDoc("csc-Pages/riskmaster/RMUtilities/AddColumn/columns.xml");
              var xmlObj = xmlDoc.documentElement;
              document.forms[0].colXML.value = xmlObj.xml;
              document.forms[0].colCount.value = 0;
          }
          else if((document.forms[0].colXML.value == "" || document.forms[0].colXML.value==null) && document.forms[0].rowId.value != 0)
          {

              var columns = document.forms[0].existingColumns.value;
              if(columns!="")
              {
                  var existingColumns = columns.split(",");
                  var columnCount = 0;
                  xmlDoc=loadXMLDoc("csc-Pages/riskmaster/RMUtilities/AddColumn/columns.xml");
                  for(j = 0; j<existingColumns.length ; j++)
                  {
                    
                    columnCount++;
                    var y = xmlDoc.getElementsByTagName('fields');
                    var x=xmlDoc.getElementsByTagName('fields')[y.length-1];
                    var newel,newtext;
                    newel=xmlDoc.createElement('field');
                    x.appendChild(newel);
                    y = xmlDoc.getElementsByTagName('field');
                    x=xmlDoc.getElementsByTagName('field')[y.length-1];
                    newel=xmlDoc.createElement('title');
                    newtext=xmlDoc.createTextNode(existingColumns[j]);
                    newel.appendChild(newtext);
                    x.appendChild(newel);
                    newel=xmlDoc.createElement('datatype');
                    newtext=xmlDoc.createTextNode('istring');
                    newel.appendChild(newtext);
                    x.appendChild(newel);
                    var z = xmlDoc.getElementsByTagName('field');
                    for(i=0;i<z.length;i++)
                      {
                      z.item(i).setAttribute("nosort","true");
                      }
                   
                   
                  }
              }
              else
              {
               var columnCount = 0;
               xmlDoc=loadXMLDoc("csc-Pages/riskmaster/RMUtilities/AddColumn/columns.xml");
              
              }
               xmlObj = xmlDoc.documentElement;
               document.forms[0].colXML.value=xmlObj.xml;
               document.forms[0].colCount.value = columnCount ;
               
          }
          else
          {
               // code for IE
              //if (window.ActiveXObject)
			  if ("ActiveXObject" in window || window.ActiveXObject)  //JIRA id: RMA-4329 --govind IE11 issue
              {
                  xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
              }
              // code for Mozilla, Firefox, Opera, etc.
              else if (document.implementation && document.implementation.createDocument)
              {
                  xmlDoc=document.implementation.createDocument("","",null);
              }
              else
              {
                  alert('Your browser cannot handle this script');
              }
              var colName = document.forms[0].colName.value;
              if(colName == "" || colName == null )
                {
                xmlDoc.loadXML(document.forms[0].colXML.value);
                xmlObj = xmlDoc.documentElement;
                document.forms[0].colXML.value=xmlObj.xml;

                }
                else
                {
                
                xmlDoc.loadXML(document.forms[0].colXML.value);
                var y = xmlDoc.getElementsByTagName('fields');
                var x=xmlDoc.getElementsByTagName('fields')[y.length-1];
                var newel,newtext;
                newel=xmlDoc.createElement('field');
                x.appendChild(newel);
                y = xmlDoc.getElementsByTagName('field');
                x=xmlDoc.getElementsByTagName('field')[y.length-1];
                newel=xmlDoc.createElement('title');
                newtext=xmlDoc.createTextNode(document.forms[0].colName.value);
                newel.appendChild(newtext);
                x.appendChild(newel);
                newel=xmlDoc.createElement('datatype');
                newtext=xmlDoc.createTextNode('istring');
                newel.appendChild(newtext);
                x.appendChild(newel);
                var z = xmlDoc.getElementsByTagName('field');
                for(i=0;i<z.length;i++)
                  {
                  z.item(i).setAttribute("nosort","true");
                  }
                xmlObj = xmlDoc.documentElement;
                document.forms[0].colXML.value=xmlObj.xml;
                
                }
          }
            
             // Initialize grid
            var objGrid = new Zapatec.EditableGrid({
            // Use xmlDoc as grid data source
            source: xmlDoc  ,
            sourceType: 'xml',
            // Use "lightblue" theme
            theme: 'csc-Theme/riskmaster/common/javascript/zapatec/zpgrid/themes/lightblue.css',
            // Put the grid into element with id "gridContainer"
            container: 'gridContainer',
            callbackCellOnClick: function(objGrid, objCell){
                     
            var numberOfColumns = document.forms[0].colCount.value;
            for(var i=0;i<numberOfColumns;i++)
            {
            if (objCell==objGrid.getCellById(0,i))
            {
            
            document.forms[0].colToBeRemoved.value = i;
            document.forms[0].btnRemove.disabled = false;
          
            } 
            
            }
            alert("To remove the above column from the Grid click the Remove button");
            } 
            

            
            });

          }
          function loadXMLDoc(dname)
          {
          var xmlDoc;
          // code for IE
          //if (window.ActiveXObject)
		  if ("ActiveXObject" in window || window.ActiveXObject) //JIRA id: RMA-4329 --govind IE11 issue
          {
          xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
          }
          // code for Mozilla, Firefox, Opera, etc.
          else if (document.implementation && document.implementation.createDocument)
          {
          xmlDoc=document.implementation.createDocument("","",null);
          }
          else
          {
          alert('Your browser cannot handle this script');
          }
          xmlDoc.async=false;
          xmlDoc.load(dname);
          return(xmlDoc);
          }

function RemoveAll()
{
if(document.forms[0].colCount.value==0)
{
alert("Grid contains 0 columns. No Columns found to remove");
return false;
}
else
{
    var ret = false;
	ret = confirm('There should be at least' + document.forms[0].minCols.value + ' column(s) in the Grid. By removing any column you will lose the associated data. Are you sure you want to remove the selected column?');
    if(ret==true)
    {
    xmlDoc=loadXMLDoc("csc-Pages/riskmaster/RMUtilities/AddColumn/columns.xml");
    var xmlObj = xmlDoc.documentElement;
    document.forms[0].colXML.value = xmlObj.xml;
    document.forms[0].colCount.value = 0;
     // Initialize grid
    var objGrid = new Zapatec.EditableGrid({
    // Use xmlDoc as grid data source
    source: xmlDoc  ,
    sourceType: 'xml',
    // Use "lightblue" theme
    theme: 'csc-Theme/riskmaster/common/javascript/zapatec/zpgrid/themes/lightblue.css',
    // Put the grid into element with id "gridContainer"
    container: 'gridContainer',
     callbackCellOnClick: function(objGrid, objCell){
            
          
            var numberOfColumns = document.forms[0].colCount.value;
            for(var i=0;i<numberOfColumns;i++)
            {
            if (objCell==objGrid.getCellById(0,i))
            {
            
            document.forms[0].colToBeRemoved.value = i;
            document.forms[0].btnRemove.disabled = false;
            } 
            
            }
            alert("To remove the above column from the Grid click the Remove button"); 
            }
            

    });
    }
    else
    {
     
    
    }
    document.forms[0].btnRemove.disabled = true;
    if(ret == true)
    {
    document.forms[0].hdnaction.value = "";
    document.forms[0].submit();
    }
    return ret;
}
}

function Remove()
{
    var ret = false;
    if(document.forms[0].colCount.value==1)
    ret = confirm('There should be at least' + document.forms[0].minCols.value + ' column(s) in the Grid. By removing any column you will lose the associated data. Are you sure you want to remove the selected column?');
    else
    ret = confirm('By removing any column you will lose the associated data. Are you sure you want to remove the selected column?');
    var xmlDoc;
    if(ret==true)
    {
    // code for IE
          //if (window.ActiveXObject)
		  if ("ActiveXObject" in window || window.ActiveXObject) //JIRA id: RMA-4329 --govind IE11 issue
          {
          xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
          }
          // code for Mozilla, Firefox, Opera, etc.
          else if (document.implementation && document.implementation.createDocument)
          {
          xmlDoc=document.implementation.createDocument("","",null);
          }
          else
          {
          alert('Your browser cannot handle this script');
          }
    //debugger;
    xmlDoc.loadXML(document.forms[0].colXML.value);
    var i = document.forms[0].colToBeRemoved.value;
    var y = xmlDoc.getElementsByTagName('fields');
    var parent = xmlDoc.getElementsByTagName('fields')[y.length-1];
    var child = xmlDoc.getElementsByTagName('field')[i];
    parent.removeChild(child);
    var xmlObj = xmlDoc.documentElement;
    document.forms[0].colXML.value=xmlObj.xml;
    document.forms[0].colCount.value = document.forms[0].colCount.value - 1;
     // Initialize grid
    var objGrid = new Zapatec.EditableGrid({
    // Use xmlDoc as grid data source
    source: xmlDoc  ,
    sourceType: 'xml',
    // Use "lightblue" theme
    theme: 'csc-Theme/riskmaster/common/javascript/zapatec/zpgrid/themes/lightblue.css',
    // Put the grid into element with id "gridContainer"
    container: 'gridContainer',
     callbackCellOnClick: function(objGrid, objCell){
            
           
            var numberOfColumns = document.forms[0].colCount.value;
            for(var i=0;i<numberOfColumns;i++)
            {
            if (objCell==objGrid.getCellById(0,i))
            {
            
            document.forms[0].colToBeRemoved.value = i;
            document.forms[0].btnRemove.disabled = false;
            } 
            
            }
            alert("To remove the above column from the Grid click the Remove button"); 
            }
            

    });
    }
    else
    {
     
    
    }
    document.forms[0].btnRemove.disabled = true;
    if(ret == true)
    {
    document.forms[0].hdnaction.value = "";
    document.forms[0].submit();
    }
    return ret;

}
function CloseAndTransfer()
{
          if(Number(document.forms[0].colCount.value)<Number(document.forms[0].minCols.value))
            {
            alert("Minimum allowed columns in grid is " + document.forms[0].minCols.value + ". Please add atleast " + document.forms[0].minCols.value + "Column(s)to continue.");
            return false;
            }
          var xmlDoc = null;
          var cols = "";
          // code for IE
          //if (window.ActiveXObject)
		  if ("ActiveXObject" in window || window.ActiveXObject) //JIRA id: RMA-4329 --govind IE11 issue
          {
          xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
          }
          // code for Mozilla, Firefox, Opera, etc.
          else if (document.implementation && document.implementation.createDocument)
          {
          xmlDoc=document.implementation.createDocument("","",null);
          }
          else
          {
          alert('Your browser cannot handle this script');
          }
         
          xmlDoc.loadXML(document.forms[0].colXML.value);
          var x = xmlDoc.getElementsByTagName('title');
          for (i=0;i<x.length;i++)
          {
             
             if(i!=(x.length-1))
             cols = cols + x[i].childNodes[0].nodeValue + ",";
             else
             cols = cols + x[i].childNodes[0].nodeValue;
             
          
          }
          document.forms[0].columns.value = cols;
          document.forms[0].hdnaction.value = 'Close';
          document.forms[0].submit();
          return true;
}
         function checkEnter(e)
          { 
          
          //e is event object passed from function invocation
          var characterCode; //literal character code will be stored in this variable

          if(e && e.which)
          { //if which property of event object is supported (NN4)
          e = e;
          characterCode = e.which; //character code is contained in NN4's which property
          }
          else{
          e = event;
          characterCode = e.keyCode; //character code is contained in IE's keyCode property
          }

          if(characterCode == 13){ //if generated character code is equal to ascii 13 (if enter key)
           document.forms[0].colName.value = "";
           document.forms[0].hdnaction.value = 'Cancel';
           document.forms[0].submit();
           return false;
          }
//          else{
//          return true
//          }

          }
        
//End by Shivendu for supplemental Grid
// Rahul - 11/28/07 - Optional Frameset enhancements
function ValidateRMXPortalSave()
{
    var objHomePage = document.getElementById('txtHomePage');
    var reply=true;
    objHomePage.value=trim(objHomePage.value);
    if(objHomePage.value == '' )
    {
       alert('Please enter all the values');
       return false;
    }    
    if(objHomePage.value.substring(0,3) != "www" && objHomePage.value.substring(0,4) != "http")
    {      
      reply = confirm("Prefix 'www' to the Home Page URL ? \n\nPress 'OK' to prepend 'www' \nPress 'Cancel' to save the URL as it is."); 
      if (reply) objHomePage.value = "www." + objHomePage.value;
    }
    if(objHomePage.value.substring(0,4) != "http")
    {     
      objHomePage.value = "http://" + objHomePage.value;
    }
    document.forms[0].hdnaction.value = 'save';
    document.getElementById('DataChanged').value = false;
    document.getElementById('SysPageDataChanged').value = false;
    setDataChanged(false);
    document.forms[0].CommonPortletsGrid_Action.value = "save";
    document.forms[0].IndividualPortletsGrid_Action.value = "save";
    //document.forms[0].submit();
    return true;
}

function RMXPortalGenerateTableDetails()
{    
    //var objChkBox;
    //var maxNoOfPortlets=6;
    var count = 0, ObjTR = null, countCommon = 0;
    m_PortletName = document.getElementById('Name').value;
    //m_PortletName = document.getElementById('NameOfPortlet').value;
    while (true) {
        if (count >= 8)
            //ObjTR = window.opener.document.getElementById('IndividualPortletsGrid_' + 'gvData_ctl' + (count + 2) + '_|' + (count + 2) + '|Data'); Mits# 35389
            ObjTR = window.opener.document.getElementById('IndividualPortletsGrid$' + 'gvData$ctl' + (count + 2) + '$|' + (count + 2) + '|Data');
        else
            //ObjTR = window.opener.document.getElementById('IndividualPortletsGrid_' + 'gvData_ctl0' + (count + 2) + '_|0' + (count + 2) + '|Data');Mits# 35389
            ObjTR = window.opener.document.getElementById('IndividualPortletsGrid$' + 'gvData$ctl0' + (count + 2) + '$|0' + (count + 2) + '|Data');
        //ObjTR = window.opener.document.getElementById('IndividualPortletsGrid_' + 'gvData_ctl0' + (count + 2) + '_|0' + (count + 2) + '|Data');
        if (ObjTR != null) {

            var sData = ObjTR.value;
            sData = sData.replace(/&lt;/g, "<");
            sData = sData.replace(/&gt;/g, ">");
            sData = sData.replace(/&quot;/g, "'");
            var sStartSelectionString = "<Name>";
            var sEndSelectionString = "</Name>";
            var sStartPos = sData.indexOf(sStartSelectionString);
            var sEndPos = sData.indexOf(sEndSelectionString);
            var sPortalName = sData.substring((parseInt(sStartPos) + 6), sEndPos);
            m_tabNames[count] = sPortalName;
            count++;
        }
        else break;
    }
    m_count = count;
    countCommon = count;
    count = 0;
    while (true) {
        if (count >= 8)
            //ObjTR = window.opener.document.getElementById('CommonPortletsGrid_' + 'gvData_ctl' + (count + 2) + '_|' + (count + 2) + '|Data'); Mits# 35389
            ObjTR = window.opener.document.getElementById('CommonPortletsGrid$' + 'gvData$ctl' + (count + 2) + '$|' + (count + 2) + '|Data');
        else
            //ObjTR = window.opener.document.getElementById('CommonPortletsGrid_' + 'gvData_ctl0' + (count + 2) + '_|0' + (count + 2) + '|Data'); Mits# 35389
            ObjTR = window.opener.document.getElementById('CommonPortletsGrid$' + 'gvData$ctl0' + (count + 2) + '$|0' + (count + 2) + '|Data');
        //ObjTR = window.opener.document.getElementById('CommonPortletsGrid_' + 'gvData_ctl0' + (count + 2) + '_|0' + (count + 2) + '|Data');
        if (ObjTR != null) {
            var sData = ObjTR.value;
            sData = sData.replace(/&lt;/g, "<");
            sData = sData.replace(/&gt;/g, ">");
            sData = sData.replace(/&quot;/g, "'");
            var sStartSelectionString = "<Name>";
            var sEndSelectionString = "</Name>";
            var sStartPos = sData.indexOf(sStartSelectionString);
            var sEndPos = sData.indexOf(sEndSelectionString);
            var sPortalName = sData.substring((parseInt(sStartPos) + 6), sEndPos);
            m_tabNames[countCommon] = sPortalName;
            countCommon++;
            count++;
        }
        else break;
    }
    m_count = countCommon;

    //    if (window.location.href.toLowerCase().indexOf("edit=true")==-1 && count>=maxNoOfPortlets)
    //    {
    //        window.alert('Cannot add Portlet.\nOnly a maximum of ' + maxNoOfPortlets +' portlets are allowed');
    //        window.close();
    //    }    


}
// for saving the portlet name and url
function Save() {    
    var ObjPortletUrl = document.getElementById('urlPortlet');
    var ObjPortletName = document.getElementById('Name');
    var reply = true;
    if (ObjPortletUrl != null)
        ObjPortletUrl.value = trim(ObjPortletUrl.value);
    if (ObjPortletName != null)
        ObjPortletName.value = trim(ObjPortletName.value);
    if (ObjPortletName != null && ObjPortletUrl != null) {
        if (ObjPortletName.value == '' || ObjPortletUrl.value == '') {
            alert('Please enter all the values');
            return false;
        }
    }
    for (var i = 0; i <= m_count - 1; i++) {
        if (m_tabNames[i].toUpperCase() == ObjPortletName.value.toUpperCase()) {
            if (m_PortletName.toUpperCase() != ObjPortletName.value.toUpperCase()) {
                alert('The specified Portlet name already exists. \nPlease choose a different Name');
                ObjPortletName.focus();
                return false;
            }
        }
    }

    var lblUrlOfPortlet = document.getElementById('tdUrlOfPortlet');
    if (ObjPortletUrl != null && lblUrlOfPortlet != null) {
        if (ObjPortletUrl.value.substring(0, 3) != "www" && ObjPortletUrl.value.substring(0, 4) != "http" && lblUrlOfPortlet.style.display != "none") {
            reply = confirm("Prefix 'www' to the portlet URL ? \n\nPress 'OK' to prepend 'www' \nPress 'Cancel' to save the URL as it is.");
            if (reply) ObjPortletUrl.value = "www." + ObjPortletUrl.value;
        }
    }
    if (ObjPortletUrl != null) {
        if (ObjPortletUrl.value.substring(0, 4) != "http") {
            ObjPortletUrl.value = "http://" + ObjPortletUrl.value;
        }
    }
    document.getElementById('hdnaction').value = 'Save';
    document.forms[0].FormMode.value = "close";

    window.opener.document.getElementById('DataChanged').value = true;
    return true;
}

function trim(svalue)
{
          svalue = svalue.replace(/^(\s+)?(.*\S)(\s+)?$/, '$2');
          return svalue;
}

function checkWindow()
{         
          if (document.forms[0].FormMode.value=="close")
          {
        //window.opener.document.forms[0].hdnaction.value = "reload";
        //window.opener.document.forms[0].submit();
        //window.close();
              
          }
          else
          { 
                RMXPortalGenerateTableDetails();
          }
}

//function ChangeTabOrderUpdateInfo(p_direction)
//{
//    // p_direction = 1 for up ; anything else for down;
//    //alert('hi');
//    var windowType=1; // 1 for common , 2 for individual 
//    var	inputs = document.all.tags("input");
//    var SelectedFound, selectedValue, selectedId;
//	for (i = 0; i < inputs.length; i++)
//	{
//		if ((inputs[i].type=="radio") && (inputs[i].checked==true))
//		{
//			inputname=inputs[i].id.substring(0,inputs[i].id.indexOf("|"));			
//			selectedValue=inputs[i].value;
//			SelectedFound=true;
//			if(inputs[i].id!=null)
//			{
//			    selectedId = inputs[i].id;
//			}
//		}
//	}
//	if (selectedId==null)
//    {
//        alert("please choose a portlet to continue");
//        return false;
//    }    
//    selectedId = (selectedId+'').substring(19, (selectedId+'').length);	    
//    if (p_direction == 1 )
//    {
//	    document.getElementById("direction").value ="1"; //UP
//	}else
//	{
//	    document.getElementById("direction").value ="2"; //Down
//	}	
//	document.getElementById('sequenceid').value=selectedId-3;
//	document.getElementById('hdnaction').value='update';
//	
//	var objPassedLoginName = document.getElementById('passedLoginName');
//	if (objPassedLoginName !=null)
//	{   
//	    // Change Tab Order - individual portlets   
//	    var p_username=getQval("username");	   
//	    objPassedLoginName.value = p_username ; 		    	    
//	    windowType=2;
//	}else
//	{
//	    // Change Tab Order - common portlets
//	    windowType=1;
//	
//	}
//	document.forms[0].submit();	

//	if (windowType==2)
//	{   
//	    // Change Tab Order - individual portlets   
//	 		    	    
//	}else
//	{
//	    // Change Tab Order - common portlets
//	    alert('interval added');
//	    window.setInterval("alert('navigating');window.navigate('/oxf/home?pg=riskmaster/RMUtilities/ChangeTabOrder')",1000);	
//	}
//	
function ChangeTabOrderUpdateInfo() {
    var inputs = document.getElementsByTagName("input");
    var SelectedFound, selectedValue, selectedId = "0";
    for (i = 0; i < inputs.length; i++) {
        if ((inputs[i].type == "radio") && (inputs[i].checked == true)) {
            SelectedFound = true;
            if (inputs[i].id != null) {
                selectedId = inputs[i].id;
            }
        }
    }
    if(selectedId == "0"){
        alert("Please choose a portlet to continue.");
        return false;
    }
    document.forms[0].SelectedID.value = selectedId;
}

function UpdateTree(selection) {
    var selectednodes = document.getElementById('hdnselectedportletsdata').value;
    var sSelectedPortletId = selection.id + "|SequenceId|";
    var sStartSelectionString = '#' + selection.value + "$";
    var sEndSelectionString = "$" + selection.value + "#";
    var sStartPos = selectednodes.indexOf(sStartSelectionString);
    var sEndPos = selectednodes.indexOf(sEndSelectionString);
    var sSelectedLoginNames = selectednodes.substring((parseInt(sStartPos) + 3), sEndPos);
    window.frames['portalzapatectree'].document.forms[0].selectedportletid.value = selection.value;
    window.frames['portalzapatectree'].document.forms[0].hdnSelectedLoginNames.value = sSelectedLoginNames;
    //window.frames['portalzapatectree'].document.forms[0].submit();
    //window.frames['portalzapatectree'].UpdateZapatecTree(sSelectedLoginNames);
    window.frames['portalzapatectree'].SelectUsers();
    window.frames['portalzapatectree'].document.forms[0].hdngriddatachangedflag.value = "true";
}
function unCheckRadioButtons() {
    var count = 0, ObjTR = null, countCommon = 0;
    while (true) {
        ObjTR = document.getElementById('CommonPortalCustomizationGrid|' + (count + 4));
        if (ObjTR != null) {
            document.getElementById('CommonPortalCustomizationGrid|' + (count + 4)).status = false;
            count++;
        }
        else break;
    }
    count = 0;
    while (true) {
        ObjTR = document.getElementById('PortalCustomizationGrid|' + (count + 4));
        if (ObjTR != null) {
            document.getElementById('PortalCustomizationGrid|' + (count + 4)).status = false;
            count++;
        }
        else break;
    }
}

function unCheckRadioButtons_ChangeTabOrderPage() {
    var count = 0, ObjTR = null, countCommon = 0;
    while (true) {
        ObjTR = document.getElementById('ChangetabOrderGrid|' + (count + 4));
        if (ObjTR != null) {
            document.getElementById('ChangetabOrderGrid|' + (count + 4)).status = false;
            count++;
        }
        else break;
    }
    if (document.forms[0].CommonPortletXML != null) {
        if (document.forms[0].CommonPortletXML.value == "") {
            document.forms[0].CommonPortletXML.value = window.opener.document.forms[0].txtCommonPortletGenXML.value;
            document.forms[0].submit();
        }
    }
//    if (document.forms[0].IndividualPortletXML != null) {
//        if (document.forms[0].IndividualPortletXML.value == "") {
//            document.forms[0].IndividualPortletXML.value = window.opener.document.forms[0].txtIndPortletGenXML.value;
//            document.forms[0].submit();
//        }
//    }
}
function passPortletXML_ChangeTabOrderPage() {
    window.opener.document.forms[0].CommonPortletXML.value = document.forms[0].CommonPortletXML.value;
    window.opener.document.forms[0].CommonPortletsGrid_Action.value = "tab";
    if (document.forms[0].SortChanged.value != "false") {
        window.opener.document.forms[0].submit();
    }
}

function checkRMXisZapatecTreeWindowSaving() {
    if (window.parent.top.document.getElementById('RMXisZapatecTreeWindowSaving').value == "true" || window.parent.top.document.getElementById('RMXisZapatecTreeWindowSaving').value == true) {
        window.parent.top.document.getElementById("RMXisZapatecTreeWindowSaving").value = "false";
        window.navigate('/oxf/home?pg=riskmaster/RMUtilities/CustomizeRMXPortal');
        if (wnd != null) wnd.close();
        return false;
    }
}
function ShowTitle()
{
 document.getElementById('formtitle').style.display='';
}

// Rahul end updates - 11/28/07 - Optional Frameset enhancements

//Parijat: Post Editable Enhanced Notes
function EnhTimeValidate() {
    var TimeValue = document.getElementById("EnhNoteTime").value;
    if (TimeValue == '') {
        //rsolanki2: mits 23130
        document.getElementById("EnhNoteTime").value = "0.00";
        return;
    }
    if (isNaN(TimeValue)) {
        alert(parent.CommonValidations.DiariesEhnacedNotesFreezealert);  	//asharma326 JIRA 6414
        document.getElementById("EnhNoteTime").value = '0.00';
        document.getElementById("txtDaysnHours").value = '0.00 hour';//sharishkumar Jira 6414
        return;
    }
    if (TimeValue > 1000 || TimeValue < 0) {
        alert(parent.CommonValidations.DiariesEhnacedNotesFreezealert);  	//asharma326  JIRA 6414
        document.getElementById("EnhNoteTime").value = '0.00';
        document.getElementById("txtDaysnHours").value = '0.00 hour';//sharishkumar Jira 6414
        return;
    }
    document.getElementById("EnhNoteTime").value = Math.round(TimeValue * 100) / 100;
    //calculate time and hours
    CalculateEnhNotesTime();
}
//Parijat :End 
function TableField_SetFieldSize() {
    if (document.forms[0].Pattern.value == "(###) ###-####   Ext. #####") {
        //considering extn can be of any no. of digits
        document.forms[0].FieldSize.value = '30';
    }
    else if (document.forms[0].Pattern.value == "(###) ###-####") {
        document.forms[0].FieldSize.value = '14';
    }
    else if (document.forms[0].Pattern.value == "###-##-####") {
        document.forms[0].FieldSize.value = '11';
    }
    else if (document.forms[0].Pattern.value == "#####-####") {
        document.forms[0].FieldSize.value = '10';
    }
 
}
//sgoel6 MITS 15071 04/23/2009 Enable/Disable Use Master Bank Account checkbox
function ToggleUseMasterBankAcc(ctrl)
{
    if (ctrl=='SubBankAcc')
    {
	    if(document.getElementById(ctrl).checked)
	    {
	        document.forms[0].UseMasterBankAcc.checked=false;
	        document.forms[0].UseMasterBankAcc.disabled=true;
	        document.forms[0].AssignSubAcc.disabled = false;     //Added Rakhi for MITS:16804-Assign Check Stock to Sub Accounts/Do Not Check Sub Account Deposit Balances checkboxes should be enabled only when Use Sub Bank Accounts is selected
	        document.forms[0].AccDepositBal.disabled = false;     //Added Rakhi for MITS:16804-Assign Check Stock to Sub Accounts/Do Not Check Sub Account Deposit Balances checkboxes should be enabled only when Use Sub Bank Accounts is selected
	    }
	    else
	    {
	        document.forms[0].UseMasterBankAcc.disabled = false;
	        //Added Rakhi for MITS:16804-Assign Check Stock to Sub Accounts/Do Not Check Sub Account Deposit Balances checkboxes should be enabled only when Use Sub Bank Accounts is selected	    
	        document.forms[0].AssignSubAcc.checked = false;
	        document.forms[0].AssignSubAcc.disabled = true;
	        document.forms[0].AccDepositBal.checked = false;
	        document.forms[0].AccDepositBal.disabled = true;
	        //Added Rakhi for MITS:16804-Assign Check Stock to Sub Accounts/Do Not Check Sub Account Deposit Balances checkboxes should be enabled only when Use Sub Bank Accounts is selected	     
	    }	
	}
}
//npadhy RMSC retrofit Starts
function PayPlanDateLostFocus(sCtrlName) {
	
    var sDateSeparator;
    var iDayPos = 0, iMonthPos = 0;
    var d = new Date(1999, 11, 22);
    var s = d.toLocaleString();
    var sRet = "";
    var objFormElem = eval('document.forms[0].' + sCtrlName);
    var sDate = new String(objFormElem.value);
    if (sDate.indexOf('/') == -1) {
        sDate = sDate.substr(0, 2) + '/' + sDate.substr(2, 2) + '/' + sDate.substr(4, 4);
    }
    var iMonth = 0, iDay = 0, iYear = 0;
    var monthDays = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
    if (sDate == "") {
        return "";
    }
    iDayPos = s.indexOf("22");
    iMonthPos = s.indexOf("11");
    sDateSeparator = "/";
    var sArr = sDate.split(sDateSeparator);
    if (sArr.length == 3) {
        sArr[0] = new String(parseInt(sArr[0], 10));
        sArr[1] = new String(parseInt(sArr[1], 10));
        sArr[2] = new String(parseInt(sArr[2], 10));
        sArr[2] = Get4DigitYear(sArr[2]);
        
        if (((parseInt(sArr[2], 10) % 4 == 0) && (parseInt(sArr[2], 10) % 100 != 0)) || (parseInt(sArr[2], 10) % 400 == 0)) {
            monthDays[1] = 29;
        }
        
        if (iDayPos < iMonthPos) {
            if (parseInt(sArr[1], 10) < 1 || parseInt(sArr[1], 10) > 12 || parseInt(sArr[0], 10) < 0 || parseInt(sArr[0], 10) > monthDays[parseInt(sArr[1], 10) - 1]) {
                objFormElem.value = "";
            }
        }
        else {
            if (parseInt(sArr[0], 10) < 1 || parseInt(sArr[0], 10) > 12 || parseInt(sArr[1], 10) < 1 || parseInt(sArr[1], 10) > monthDays[parseInt(sArr[0], 10) - 1]) { // Added by csingh7 : MITS 14181
                objFormElem.value = "";
            }
        }
        
        if (parseInt(sArr[2], 10) < 10 || (sArr[2].length != 4 && sArr[2].length != 2)) {
            objFormElem.value = "";
        }
        
        if (objFormElem.value != "") {
            if (sArr[0].length == 1) {
                sArr[0] = "0" + sArr[0];
            }
            if (sArr[1].length == 1) {
                sArr[1] = "0" + sArr[1];
            }
            if (sArr[2].length == 2) {
                sArr[2] = "20" + sArr[2];
            }
            if (iDayPos < iMonthPos) {
                objFormElem.value = formatRMDate(sArr[2] + sArr[1] + sArr[0]);
            }
            else {
                objFormElem.value = formatRMDate(sArr[2] + sArr[0] + sArr[1]);
            }
        }
        
 		if (objFormElem != null && objFormElem.value != null && objFormElem.value != "") {
		    var m = objFormElem.value.match("Na");
		    if (m != null)
		    {
			    objFormElem.value="";
		    }
		    
		    var m = objFormElem.value.match("NN");
		    if (m != null)
		    {
			    objFormElem.value="";
		    }
		}
    }
    else {
        objFormElem.value = "";
    }
    
    EnableGenDueDate(sCtrlName);
    return true;
}

function EnableGenDueDate(sCtrlName)
{
	var strCtlName;
	var IsGenDueDate;
	var objFormElem = eval('document.forms[0].' + sCtrlName);
    var sValue = new String(objFormElem.value);
	if (trim(document.forms[0].InstGenType_codelookup.value.substring(0,1))=="F")
	{
		
		if (Trim(sCtrlName)=="GenDate" || Trim(sCtrlName)=="DueDate")
		{
			sValue = new String(document.forms[0].GenDate.value);
			if (Trim(sValue) == "")
			{
				sValue = new String(document.forms[0].DueDate.value);
			}
			if (Trim(sValue)!="")
			{
				document.forms[0].GenDay_codelookup.value="";
				document.forms[0].DueDays.value="";
				document.forms[0].GenDay_codelookup.disabled=true;
				document.forms[0].GenDay_codelookupbtn.disabled=true;
				document.forms[0].DueDays.disabled=true;
				document.forms[0].GenDay_codelookup.style.backgroundColor="silver";
				document.forms[0].DueDays.style.backgroundColor="silver";
			}
			else
			{
				document.forms[0].GenDay_codelookup.disabled=false;
				document.forms[0].GenDay_codelookupbtn.disabled=false;
				document.forms[0].DueDays.disabled=false;
				document.forms[0].GenDay_codelookup.style.backgroundColor="";
				document.forms[0].DueDays.style.backgroundColor="";
			}
		}
	}
	else if (trim(document.forms[0].InstGenType_codelookup.value.substring(0,1))=="C")
	{
		for (i=1;i<=12;i++)
			{
				if (Trim(sValue)=="")
				{
					ctrl3=document.getElementById('GenDate'+i);
					ctrl4=document.getElementById('DueDate'+i);
					sValue=ctrl3.value;
					if (Trim(sValue)=="")
					{
						sValue=ctrl4.value;
					}
				}
			}
		if (Trim(sValue)!="")
		{
			for (i=1;i<=12;i++)
			{
		   	  ctrl3=document.getElementById('GenLagDays'+i);
			  ctrl4=document.getElementById('DueDays'+i);
			  ctrl3.value="";
			  ctrl4.value="";
			  ctrl3.disabled=true;
			  ctrl4.disabled=true;
			  ctrl3.style.backgroundColor="silver";
			  ctrl4.style.backgroundColor="silver";
			}
		}
		else
		{
			for (i=1;i<=12;i++)
			{
			  ctrl3=document.getElementById('GenLagDays'+i);
			  ctrl4=document.getElementById('DueDays'+i);
			  ctrl3.disabled=false;
			  ctrl4.disabled=false;
			  ctrl3.style.backgroundColor="";
			  ctrl4.style.backgroundColor="";	
			}

		}

	}
}

function EnableDueDays(sCtrlName)
{
	var strCtlName;
	var IsGenDueDate;
	var objFormElem = eval('document.forms[0].' + sCtrlName);
    var sValue = new String(objFormElem.value);
	if (trim(document.forms[0].InstGenType_codelookup.value.substring(0,1))=="F")
	{
		
		if (Trim(sCtrlName)=="GenDay_codelookup" || Trim(sCtrlName)=="DueDays")
		{
			sValue = new String(document.forms[0].GenDay_codelookup.value);
			if (Trim(sValue) == "")
			{
				sValue = new String(document.forms[0].DueDays.value);
			}
			if (Trim(sValue)!="")
			{
				ctrl3=document.getElementById('GenDate');
				ctrl4=document.getElementById('DueDate');
				ctrl5=document.getElementById('GenDatebtn');
				ctrl6=document.getElementById('DueDatebtn');
			    ctrl3.value="";
			    ctrl4.value="";
				ctrl3.disabled=true;
				ctrl4.disabled=true;
				ctrl5.disabled=true;
				ctrl6.disabled=true;
				ctrl3.style.backgroundColor="silver";
				ctrl4.style.backgroundColor="silver";
				ctrl5.style.backgroundColor="silver";
				ctrl6.style.backgroundColor="silver";
			}	
			else
			{
				ctrl3=document.getElementById('GenDate');
				ctrl4=document.getElementById('DueDate');
				ctrl5=document.getElementById('GenDatebtn');
				ctrl6=document.getElementById('DueDatebtn');
				ctrl3.disabled=false;
				ctrl4.disabled=false;
				ctrl5.disabled=false;
				ctrl6.disabled=false;
				ctrl3.style.backgroundColor="";
				ctrl4.style.backgroundColor="";
				ctrl5.style.backgroundColor="";
				ctrl6.style.backgroundColor="";
			}
		}
	}
	else if (trim(document.forms[0].InstGenType_codelookup.value.substring(0,1))=="C")
	{
		for (i=1;i<=12;i++)
			{
				if (Trim(sValue)=="")
				{
					ctrl3=document.getElementById('GenLagDays'+i);
					ctrl4=document.getElementById('DueDays'+i);
					sValue=ctrl3.value;
					if (Trim(sValue)=="")
					{
						sValue=ctrl4.value;
					}
				}
			}
		if (Trim(sValue)!="")
		{
			for (i=1;i<=12;i++)
			{
		   	    ctrl3=document.getElementById('GenDate'+i);
				ctrl4=document.getElementById('DueDate'+i);
				ctrl5=document.getElementById('GenDateBtn'+i);
				ctrl6=document.getElementById('DueDateBtn'+i);
			    ctrl3.value="";
			    ctrl4.value="";
				ctrl3.disabled=true;
				ctrl4.disabled=true;
				ctrl5.disabled=true;
				ctrl6.disabled=true;
				ctrl3.style.backgroundColor="silver";
				ctrl4.style.backgroundColor="silver";
				ctrl5.style.backgroundColor="silver";
				ctrl6.style.backgroundColor="silver";
			}
		}
		else
		{
			for (i=1;i<=12;i++)
			{
			    ctrl3=document.getElementById('GenDate'+i);
				ctrl4=document.getElementById('DueDate'+i);
				ctrl5=document.getElementById('GenDateBtn'+i);
				ctrl6=document.getElementById('DueDateBtn'+i);
			    ctrl3.disabled=false;
				ctrl4.disabled=false;
				ctrl5.disabled=false;
				ctrl6.disabled=false;
				ctrl3.style.backgroundColor="";
				ctrl4.style.backgroundColor="";
				ctrl5.style.backgroundColor="";
				ctrl6.style.backgroundColor="";	
			}

		}

	}
}	
//npadhy RMSC retrofit Ends

//update existing task manager tasks
function UpdateExistingTasks(Ctl, notificationtype) {
    var sTaskExistFlag = document.forms[0].hdUpdateTasks.value ;
    if (Ctl.checked == true) 
    {
        if (notificationtype == "diary")
        {
            if (sTaskExistFlag == "True") 
            {
                if (confirm(parent.CommonValidations.UpdateTaskforDiaryConfirm))
               // if (confirm('Do you want to update existing tasks to support task manager diary ?')) 
                {
                    window.open('/RiskmasterUI/UI/Utilities/Manager/UpdateExistingTasks.aspx', 'UpdateExistingTasks',
                    'width=550,height=350' + ',top=' + (screen.availHeight - 200) / 2 + ',left=' + (screen.availWidth - 200) / 2 + ',resizable=yes,scrollbars=yes');
                }
            }
        }
        else if(notificationtype == "email") //tmalhotra3: changes for RMA 4613
        { 
            if (sTaskExistFlag == "True") 
            {
            if (confirm(parent.CommonValidations.UpdateTaskforEmailConfirm))
            {
                window.open('/RiskmasterUI/UI/Utilities/Manager/UpdateExistingTasks.aspx', 'UpdateExistingTasks',
                'width=550,height=350' + ',top=' + (screen.availHeight - 200) / 2 + ',left=' + (screen.availWidth - 200) / 2 + ',resizable=yes,scrollbars=yes');
            }
        }
        }
    }   
}

//Changed by Saurabh Arora for MITS 20258:Start
function EnableMultipleReInsurer()
{

if(document.forms[0].MultipleInsurer.checked==true)
{
    document.forms[0].chkMultipleReInsurer.disabled=false;
}
else
{
    document.forms[0].chkMultipleReInsurer.disabled=true;
    document.forms[0].chkMultipleReInsurer.checked=false;
}
}
//Changed by Saurabh Arora for MITS 20258:End
function OpenPrintFroiAcord() {
    var width = screen.availWidth - 60;
    var height = screen.availHeight - 60;
    var sFileName = document.forms[0].filename.value;

    var sReturnValue = window.open("/RiskmasterUI/UI/Utilities/Manager/PrintFroiAcord.aspx?FileName=" + sFileName, 'Adjustment', 'width=' + width + ',height=' + height + ',top=' + (screen.availHeight - height) / 2 + ',left=' + (screen.availWidth - width) / 2 + ',resizable=no,scrollbars=no,status=no,center=yes');
    return false;
}

//spahariya MITS 25646 start- 08/05/2011 add utility settingfor PSO reporting system
function SystemParameter_UsePSOReporting() {
    if (document.forms[0].PSOReportingSys.checked == true) {
        document.forms[0].PSOReportingSys.checked = false;
        window.open('/RiskmasterUI/UI/Utilities/Manager/SystemParameterUsePSOReporting.aspx', 'SystemParameterUsePSOReporting',
				'width=400,height=300' + ',top=' + (screen.availHeight - 290) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
    }
}
function SystemParameterUsePSOReporting_Load() {
    if (document.forms[0].FormMode.value == "close") {
        window.opener.document.forms[0].PSOReportingSys.checked = true;
        window.close();
    }
}
function SystemParameterUsePSOReporting_Ok() {
    if (document.forms[0].Success.value != "0")//Asif
        document.forms[0].hdnValue.value = "1"; //Asif
    if (document.forms[0].Success.value == "0")
        window.close();
    window.document.forms['frmData'].submit();
}
//spahariya MITS 25646 - end

function DisableAutoFillDpt(ctrl) {
    if (ctrl == 'AutoPopulateDpt') {
        if (document.getElementById(ctrl).checked) {
            document.forms[0].AutoFillDpteid.disabled = false;
            document.forms[0].AutoFillDpteidbtn.disabled = false;
            document.getElementById('AutoFillDpteid').style.backgroundColor = ""; //MITS 25124
        }
        else {
            document.forms[0].AutoFillDpteid.disabled = true;
            document.forms[0].AutoFillDpteidbtn.disabled = true;
            document.getElementById('AutoFillDpteid').style.backgroundColor = "#F2F2F2"; //MITS 25124
        }
    }
    if (ctrl == 'UseMultiCurrency') {
        if (document.getElementById(ctrl).checked) {
            document.forms[0].basecurrencytype_codelookupbtn.disabled = false;
            document.forms[0].basecurrencytype_codelookup.disabled = false;
            document.getElementById('basecurrencytype_codelookup').style.backgroundColor = ""; 
        }
        else {
            document.forms[0].basecurrencytype_codelookupbtn.disabled = true;
            document.forms[0].basecurrencytype_codelookup.disabled = true;
            document.getElementById('basecurrencytype_codelookup').style.backgroundColor = "#F2F2F2";
        }
    }
}
//skhare7 MITS 28805
function ToggleUseEnableBillReviewFee(ctrl) {
    if (ctrl == 'BillReviewFee') {
        if (document.getElementById(ctrl).checked) {
            document.forms[0].OrgBillRevFee.disabled = false;

            document.forms[0].OrgEntlastfirstname.readOnly = false; //MGaba2:MITS 29111         
            document.forms[0].OrgEntlastfirstnamebtn.disabled = false;   
        }
        else {
            document.forms[0].OrgBillRevFee.disabled = true;

            document.forms[0].OrgEntlastfirstname.value = '';
            document.forms[0].OrgEntentityid.value = '';
            document.forms[0].OrgEntlastfirstname.readOnly = true; //MGaba2:MITS 29111         
            document.forms[0].OrgEntlastfirstnamebtn.disabled = true;
        }
    }
}
//skhare7 MITS 28805 End
////skhare7 R8 Multilingual
//function SystemParameter_CountryChanged()
// {
//    
//    document.forms[0].hdAction.value = 'CountryChanged';
//    var sTabName = document.forms[0].hTabName;
//    m_DataChanged = true;

//    for (var i = 0; i <= m_TabList.length - 1; i++) {

//        if (m_TabList[i].id.substring(4, m_TabList[i].id.length).toUpperCase() == sTabName.value.toUpperCase())
//      {

//          m_TabList[i].focus();
//          window.document.forms['frmData'].submit();
//            
//        }
//  }


//}

function SetFocus() {
   
    var i = 0;
    var j = 0;
    if (m_TabList != null && m_TabList.length != 0)
        return;
    m_TabList = new Array();
    document.forms[0].hdAction.value = 'CountryChanged';
    var sTabName = document.forms[0].hTabName;
    m_DataChanged = true;

    if (m_TabList.length == 0) {

        var objElts = document.getElementsByTagName("DIV");

        for (i = 0; i < objElts.length; i++) {
            if (objElts[i].id != null)
                if (objElts[i].id.substring(0, 4) == 'TABS') {
                    m_TabList.length++;
                    m_TabList[j++] = objElts[i];
                }

        }
    }
        for (i = 0; i < m_TabList.length; i++) 
    {
      
            var selectedTabName = m_TabList[i].id.substring(4);
            if (selectedTabName == 'Globalization') {
                document.getElementById('TABS' + selectedTabName).className = "Selected";
                m_TabList[i].focus();
                break;

            }
        
    }
//    for (var i = 0; i <= m_TabList.length - 1; i++)
//     {

//         if (m_TabList[i].id.substring(4, m_TabList[i].id.length).toUpperCase() == sTabName.value.toUpperCase())
//         {

//            m_TabList[i].focus();
//            window.document.forms['frmData'].submit();

//        }
//    }


}
  


//skhare7 R8 Multilingual End

//tanwar2 - ImageRight - start
function SystemParameter_UseImageRightInterface() {
    var UseImgRight = document.getElementById('UseImgRight');
    var UseImgRightWS = document.getElementById('UseImgRighWebService');
    var UseAcrosoftInterface = document.getElementById('UseAcrosoftInterface');
    var UsePaperVisionInterface = document.getElementById('UsePaperVisionInterface');

    if (UseImgRight) {
        if (UseImgRight.checked) {
            if (UseImgRightWS) {
                UseImgRightWS.disabled = false;
            }
            if (UseAcrosoftInterface) {
                UseAcrosoftInterface.checked = false;
            }
            if (UsePaperVisionInterface) {
                UsePaperVisionInterface.checked = false;
            }
        }
        else {
            if (UseImgRightWS) {
                UseImgRightWS.checked = false;
                UseImgRightWS.disabled = true;
            }
        }
    }
}
//tanwar2 - ImageRight - end

// JIRA 6414 - Calculate the Hours in Day and hour format.
function CalculateEnhNotesTime() {
    var varEnhNoteTime = "";
    if (document.getElementById('EnhNoteTime') != null) {
        varEnhNoteTime = document.getElementById('EnhNoteTime').value;
        if (document.getElementById('txtDaysnHours') != null)
            if (varEnhNoteTime >= 24) {
                var noOfDays = Math.floor(varEnhNoteTime / 24);
                var noOfHours = Math.floor(varEnhNoteTime % 24);
                document.getElementById('txtDaysnHours').value = noOfDays + " day" + (noOfDays > 1 ? "s" : " ") + (noOfHours > 0 ? (" " + noOfHours + " hour" + (noOfHours > 1 ? "s" : " ")) : " ");//sharishkumar Jira 6414
            }
            else
                document.getElementById('txtDaysnHours').value = varEnhNoteTime + " hour" + (varEnhNoteTime > 1 ? "s" : " ");//sharishkumar Jira 6414
    }
}