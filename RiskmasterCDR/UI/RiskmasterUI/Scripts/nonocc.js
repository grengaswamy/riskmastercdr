var m_Popup;
var SysCmd_Calc=1
var SysCmd_DisplayPrinted=2
var SysCmd_DisplayFuture=3
var SysCmd_Continue=4
var SysCmd_DupePayments=5
var SysCmd_SavePayments=6
var SysCmd_EditPayment=7
var SysCmd_DeletePayment=8
var SysCmd_SaveEdit=9
var m_codeWindow=null;
var m_DataChanged_NonOccAsp=false;
var m_DataChanged=false;
var m_sFieldName="";
var m_Width=1000;
var m_Height=500;
var m_ThisDoc='';
var dGrossCollectionAmount=0;
var m_amountchange = false;

document.calcPayments=calcPayments;

function pageLoaded_NonOccAsp(bRecalc)
{
	document.calcPayments=calcPayments;
	document.dateSelected=dateSelected;
	document.onCodeClose=onCodeClose;
	m_ThisDoc='nonocc.asp';   //nonocc.js also included by buttonscript on page with form.js

	var obj=document.forms[0].sysCmdQueue.value;
	if(obj!=null){
		if(document.forms[0].sysCmdQueue.value=='1'){
			document.forms[0].sysCmdQueue.value='';
			document.forms[0].syscmd.value=SysCmd_DisplayFuture;
			document.forms[0].submit();
			return true;
		}
	}
	
	if(eval("document.forms[0].width")){
		m_Width=document.forms[0].width.value;
		m_Height=document.forms[0].height.value;
		window.resizeTo(m_Width,m_Height)
	}
	if(bRecalc==-1)
		setDataChanged_NonOccAsp(true);
}

function ConfirmSave_NonOccAsp()
{
	var ret = false;
	if(m_DataChanged_NonOccAsp){
		ret = confirm('Data has changed. Do you want to save changes?');
	}
	return ret;
}

function cancelEdit() {
 
    //Raman Bhatia: Closing should close the pop up and refresh parent
	//commenting code and writing new
	
	var win = window.opener;
	//PJS MITS 12171,05-08-2008: Commented the line below 	
	win.futurePayments();
	//win.close();
	return false;
	
	/*
	//user in Edit Payment window and cancels.  keep window open and show user future payments
	if(ConfirmSave_NonOccAsp()){
		//document.forms[0].sysCmdQueue.value='1';
		document.forms[0].syscmd.value=SysCmd_SaveEdit;
		document.forms[0].action.value="SaveAndClose";
		document.forms[0].submit();
		//self.setTimeout('document.forms[0].submit();',200);
		return false;
	}
	else
		document.forms[0].syscmd.value=SysCmd_DisplayFuture;
	//document.forms[0].submit();
	return true;
	*/
	
	
}

function chkAll(b)
{
	if(!eval("document.forms[0].chkDelete"))
		return false;
//	debugger;
	var iLength = eval("document.forms[0].chkDelete.length");
	var sSelected = "";
	if(iLength!=null){
		for(i=0;i<iLength;i++){
			if(document.forms[0].chkDelete[i].checked==true)
			    sSelected=sSelected+"|"+document.forms[0].chkDelete[i].value+','+document.forms[0].chkDelete[i].getAttribute('batchid');
		}
		sSelected=sSelected.substring(1);
	}//Parijat: mits 12126-- value sSelected only there if only one radio option is checked.
	else{
		if(document.forms[0].chkDelete.checked==true)
		    sSelected = document.forms[0].chkDelete.value + ',' + document.forms[0].chkDelete.getAttribute('batchid');
	}
	
	if(sSelected=="")
	{
		alert('Please select a batch to delete');
		return false;
	}
	if(!confirm('Delete Current Batch?'))
		return false;
		
	document.forms[0].deletelist.value=sSelected;
	document.forms[0].batchDelete.value="true";
	
}

function doEdit(bRecalc , autosplitid)
{
	//user in Edit Payment window and saves edit or recalcs the gross with new to/from dates
	
	var sFromDate=document.forms[0].fromdate.value;
	if(sFromDate=='' || sFromDate==null){
		alert("'From Date' is a required field.");
		document.forms[0].fromdate.focus();
		return false;
	}
	
	var sToDate=document.forms[0].todate.value;
	if(sToDate=='' || sToDate==null){
		alert("'To Date' is a required field.");
		document.forms[0].todate.focus();
		return false;
	}
	
	var sDateFormat=dateFormat();  //mdy or dmy
	var sFromDay='';
	var sFromMonth='';
	var sFromYear='';
	var sToDay='';
	var sToMonth='';
	var sToYear='';
	if(sDateFormat=='mdy'){
		sFromMonth=sFromDate.substring(0,2);
		sFromDay=sFromDate.substring(3,5);
		sToMonth=sToDate.substring(0,2);
		sToDay=sToDate.substring(3,5);
	}
	else{
		sFromMonth=sFromDate.substring(3,5);
		sFromDay=sFromDate.substring(0,2);
		sToMonth=sToDate.substring(3,5);
		sToDay=sToDate.substring(0,2);
	}
	sFromYear=sFromDate.substring(6,10);
	sToYear=sToDate.substring(6,10);
	var dFromDate=new Date(sFromYear,sFromMonth,sFromDay,'0','0','0','0');
	var dToDate=new Date(sToYear,sToMonth,sToDay,'0','0','0','0');

	if(dFromDate.getTime() > dToDate.getTime()){
		alert('From Date must be <= To Date');
		return false;
	}
	
	if(bRecalc==0 && (document.forms[0].gross.value=='' || document.forms[0].gross.value==null || document.forms[0].gross.value==0)){
		alert("'Gross' is a required field.");
		document.forms[0].gross.focus();
		return false;
	}
	//MITS 14253 : Umesh  Net amount should not be negative
	if(bRecalc==0 && document.forms[0].Net.value <0)
	{
	    alert("Net Payment cannot be negative");
	    return false;
	    
	}

    //MITS 14253 End	    
	document.forms[0].syscmd.value=SysCmd_SaveEdit;
	if(document.forms[0].autosplitid)
	{
	    document.forms[0].autosplitid.value = autosplitid;
	}
	document.forms[0].recalc.value=bRecalc;  //user chose new to/from dates, wants to recalc the gross
	if(document.forms[0].amountchange.value != "true")
	    document.forms[0].amountchange.value = m_amountchange;
	//document.forms[0].submit();
	return true;
}

function deleteSelectedPayments()
{
//user sees list of payments and desires to delete some/all
//after delete user sees all future payments
	if(!eval("document.forms[0].chkDelete"))
		return false;

	var iLength = eval("document.forms[0].chkDelete.length");
	var sSelected=new String("");
	//could be one checkbox or multiple
	if(iLength!=null){
		for(i=0;i<iLength;i++){
			if(document.forms[0].chkDelete[i].checked==true)
			    sSelected=sSelected+"|"+document.forms[0].chkDelete[i].value+','+document.forms[0].chkDelete[i].getAttribute('batchid');
		}
		sSelected=sSelected.substring(1);
	}
	else{
		if(document.forms[0].chkDelete.checked==true)
			sSelected=document.forms[0].chkDelete.value+','+document.forms[0].chkDelete.getAttribute('batchid');		
	}
	
	if(sSelected=="")
	{
		alert('Please select some Payment to Delete');
		return false;
	}
		
	if(!confirm('Delete selected record(s)?'))
		return false;
		
	document.forms[0].deletelist.value=sSelected;
	document.forms[0].syscmd.value=SysCmd_DeletePayment;
	//Parijat
	document.forms[0].batchDelete.value="false";
	//document.forms[0].submit();
	return true;	
}

function SavePayments() {
   
	//user sees list of payments in popup and chooses to save them			
	document.forms[0].syscmd.value=SysCmd_SavePayments;
	var lClaimId=document.forms[0].claimid.value;
	var taxarray=document.forms[0].taxarray.value;
	var claimnumber = document.forms[0].claimnumber.value;
	document.forms[0].submit();	
	
	if (m_Popup!=null)
		m_Popup.close();
	
//	debugger;
//	var lClaimId=document.forms[0].claimid.value;
//	var taxarray=document.forms[0].taxarray.value;
//	var claimnumber = document.forms[0].claimnumber.value;
//	var batchid = document.forms[0].batchid.value;
//	
//	m_Popup=window.open("../NonOccPayments/FuturePayments.aspx?claimid="+lClaimId+"&taxarray="+taxarray+"&syscmd="+SysCmd_SavePayments+"&claimnumber="+claimnumber+"&batchrestrict=true"+"&batchid="+batchid,"future",
//			'width='+m_Width+',height='+m_Height+',top='+(screen.availHeight-m_Height)/2+',left='+(screen.availWidth-m_Width)/2+',resizable=yes,scrollbars=yes');
    //bkumar33: Cosmetic change
    pleaseWait.Show();
    //end
	return false;	
}

function Continue()
//user in dupe payments popup and choses to continue
{
	document.forms[0].syscmd.value=SysCmd_Continue;
	//document.forms[0].submit();
	//return false;
	return true;
}

function calcPayments(lTransTypeCode) {
//pmittal5 Mits 17436 09/15/09 - When Non-Occ Payments are opened for the first time, BENEFIT_START_DATE field in CLAIM is blank.
    //Prompting to save the Payment so as to update BENEFIT_START_DATE in CLAIM
    if (document.forms[0].ClaimBenefitStartDate != null && document.forms[0].ClaimBenefitStartDate.value == "")
    {
        alert("You must save the record before calculating payments.");
        return false;
    }
//pmittal5 Mits 14196 04/23/09 - Dont allow opening Calculate payments if data not Saved on Non-Occ Payments screen
   if(m_DataChanged)
   {
       alert("Data has changed. Please save the changes.");
       return false;
   }
 //End - pmittal5  
	var l=0;

	if(document.forms[0].ballowcalc.value!='True'){
		alert('Unable to calculate payments, please repair missing/incomplete data.')
		return false
	}
	
	//all changes must be saved before can calculate payment
	//if(ConfirmSave()){
	//	var l = document.forms[0].transactiontypecode.value;
	//	document.forms[0].sysCmdQueue.value='calcPayments('+l+')';
	//	document.forms[0].sysCmdConfirmSave.value=1;
	//	Navigate(5);
	//	return false;
	//}	
	
	//if user chose to not save data, cannot do calculation.
	//calculations depend up the four dates on form saved to database
	//if (m_DataChanged)
	//	return true;
	
	//trans type code might get passed in if ConfirmSave triggered save
	if(lTransTypeCode==null || lTransTypeCode==0){
		lTransTypeCode=document.forms[0].transactiontypecode.value;
		if(lTransTypeCode==null || lTransTypeCode==0){
			alert("You must choose a Transaction Type to calculate payments.");
			return false;
		}
	}
	else{
		for(i=0;i<=document.forms[0].transactiontypecode.options.length;i++)
			if (document.forms[0].transactiontypecode.options[i].value==lTransTypeCode){
				document.forms[0].transactiontypecode.options[i].selected=true;
				i=document.forms[0].transactiontypecode.options.length;
			}
	}
	
	var lAccount=0;
	var objCombo=document.forms[0].bankaccount;
	lAccount=objCombo.options[objCombo.selectedIndex].value;
	
	//if(lAccount==null || lAccount==0){
	if(lAccount==0){
		alert("You must choose a Bank Account to calculate payments.")
		return false;
	}
		
	if (m_Popup!=null)
		m_Popup.close();

	var lClaimId=document.forms[0].claimid.value;
	var lPiEid=document.forms[0].pieid.value;
	var taxarray=document.forms[0].taxarray.value;
	var lTaxFlags=1;
	if(document.forms[0].chkfederaltax.checked)
		lTaxFlags=lTaxFlags+2;
	if(document.forms[0].chksstax.checked)
		lTaxFlags=lTaxFlags+4;
	if(document.forms[0].chkmedtax.checked)
		lTaxFlags=lTaxFlags+8;
	if(document.forms[0].chkstatetax.checked)
		lTaxFlags=lTaxFlags+16;
	var sBenStart=document.forms[0].benstartdate.value;
	var sBenThrough=document.forms[0].benthroughdate.value;
	var sPayStart=document.forms[0].paymentsstartdate.value;
	var sPayThrough=document.forms[0].paymentsthroughdate.value;
	var dailyamount = document.forms[0].dailyamount.value;
	var dailysuppamount = document.forms[0].dailysuppamount.value;
	var dPension=document.forms[0].dPension.value;
	var dSS = document.forms[0].dSS.value;
	var iDaysWorkingInMonth=document.forms[0].iDaysWorkingInMonth.value;
	var iDaysWorkingInWeek = document.forms[0].iDaysWorkingInWeek.value;
	var dOther = document.forms[0].dOther.value;
	var iOffsetCalc = document.forms[0].iOffsetCalc.value;
	var iFederalTax = document.forms[0].FederalTax.value;
	var iSocialSecurityAmount = document.forms[0].SocialSecurityAmount.value;
	var iMedicareAmount = document.forms[0].MedicareAmount.value;
	var iStateAmount = document.forms[0].StateAmount.value;
	var iNetPayment = document.forms[0].netpayment.value;
	var iGrossCalculatedPayment = document.forms[0].grosscalculatedpayment.value;
	var iGrossCalculatedSupplement = document.forms[0].grosscalculatedsupplement.value;
	var iGrossTotalNetOffsets = document.forms[0].grosstotalnetoffsets.value;
	var pensionoffset = document.forms[0].pensionoffset.value;
	var ssoffset = document.forms[0].socialsecurityoffset.value;
	var oioffset = document.forms[0].otherincomeoffset.value;
	var ilblDaysIncluded = document.forms[0].ilblDaysIncluded.value;
	var dSuppRate = document.forms[0].supplement.value;
	var dWeeklyBenefit = document.forms[0].weeklybenefit.value;
	//Animesh Inserted for GHS Enhancement - pmittal5 Mits 14841
	var dOther1=document.forms[0].dOtherOffset1.value;
	var dOther2=document.forms[0].dOtherOffset2.value;
	var dOther3=document.forms[0].dOtherOffset3.value;
	var dPostTax1=document.forms[0].dPostTaxDed1.value;
	var dPostTax2=document.forms[0].dPostTaxDed2.value;
	//
	var OtherOffset1=document.forms[0].OtherOffset1.value;
	var OtherOffset2=document.forms[0].OtherOffset2.value;
	var OtherOffset3=document.forms[0].OtherOffset3.value;
	var PostTaxDed1=document.forms[0].PostTaxDed1.value;
	var PostTaxDed2=document.forms[0].PostTaxDed2.value;
	//Animesh Insertion Ends 
	iNetPayment = iNetPayment.replace("(", "-").replace(")","^^");//skhare7 MITS 27497
	//MITS 13132 : Umesh
	if (!(bCompareDate(sBenStart, sPayStart))) {
	    alert("Payments Beginning Date must be >= " + sBenStart);
	    return false;
	}
	if (!(bCompareDate(sPayThrough, sBenThrough))) {
	    alert("Payments Through Date must be <= " + sBenThrough);
	    return false;
	}
	if (!(bCompareDate(sPayStart, sPayThrough))) {
	    alert("Payments Through Date must be greater than or equal to Payments Beginning Date");
	    return false;
	}
	//MITS 13132 : End
//Animesh Modified the defination for GHS Enhancment - pmittal5 Mits 14841	
//	m_Popup=window.open("../NonOccPayments/CalculatePayments.aspx?benst="+sBenStart+"&bento="+sBenThrough+"&dWeeklyBenefit="+dWeeklyBenefit+"&ilblDaysIncluded="+ilblDaysIncluded+"&iNetPayment="+iNetPayment+"&dSuppRate="+dSuppRate+"&pensionoffset="+pensionoffset+"&ssoffset="+ssoffset+"&oioffset="+oioffset+"&iGrossCalculatedPayment="+iGrossCalculatedPayment+"&iGrossCalculatedSupplement="+iGrossCalculatedSupplement+"&iGrossTotalNetOffsets="+iGrossTotalNetOffsets+"&payst="+sPayStart+"&payto="+sPayThrough+"&iFederalTax="+iFederalTax+"&iSocialSecurityAmount="+iSocialSecurityAmount+"&iMedicareAmount="+iMedicareAmount+"&iStateAmount="+iStateAmount+"&claimid="+lClaimId+"&iDaysWorkingInWeek="+iDaysWorkingInWeek+"&iDaysWorkingInMonth="+iDaysWorkingInMonth+"&iOffsetCalc="+iOffsetCalc+"&pieid="+lPiEid+"&taxarray="+taxarray+"&transtype="+lTransTypeCode+"&acct="+lAccount+"&dailyamount="+dailyamount+"&dailysuppamount="+dailysuppamount+"&taxflags="+lTaxFlags+"&dOther="+dOther+"&dSS="+dSS+"&dPension="+dPension+"&syscmd="+SysCmd_Calc,"calc",
//			'width='+m_Width+',height='+m_Height+',top='+(screen.availHeight-m_Height)/2+',left='+(screen.availWidth-m_Width)/2+',resizable=yes,scrollbars=yes');

	m_Popup = window.open("../NonOccPayments/CalculatePayments.aspx?benst=" + sBenStart + "&bento=" + sBenThrough + "&dWeeklyBenefit=" + dWeeklyBenefit + "&ilblDaysIncluded=" + ilblDaysIncluded + "&iNetPayment=" + iNetPayment + "&dSuppRate=" + dSuppRate + "&pensionoffset=" + pensionoffset + "&ssoffset=" + ssoffset + "&oioffset=" + oioffset + "&iGrossCalculatedPayment=" + iGrossCalculatedPayment + "&iGrossCalculatedSupplement=" + iGrossCalculatedSupplement + "&iGrossTotalNetOffsets=" + iGrossTotalNetOffsets + "&payst=" + sPayStart + "&payto=" + sPayThrough + "&iFederalTax=" + iFederalTax + "&iSocialSecurityAmount=" + iSocialSecurityAmount + "&iMedicareAmount=" + iMedicareAmount + "&iStateAmount=" + iStateAmount + "&claimid=" + lClaimId + "&iDaysWorkingInWeek=" + iDaysWorkingInWeek + "&iDaysWorkingInMonth=" + iDaysWorkingInMonth + "&iOffsetCalc=" + iOffsetCalc + "&pieid=" + lPiEid + "&taxarray=" + taxarray + "&transtype=" + lTransTypeCode + "&acct=" + lAccount + "&dailyamount=" + dailyamount + "&dailysuppamount=" + dailysuppamount + "&taxflags=" + lTaxFlags + "&dOther=" + dOther + "&dSS=" + dSS + "&dPension=" + dPension + "&syscmd=" + SysCmd_Calc + "&doffset1=" + dOther1 + "&doffset2=" + dOther2 + "&doffset3=" + dOther3 + "&dPostTaxDed1=" + dPostTax1 + "&dPostTaxDed2=" + dPostTax2 + "&othoffset1=" + OtherOffset1 + "&othoffset2=" + OtherOffset2 + "&othoffset3=" + OtherOffset3 + "&psttaxded1=" + PostTaxDed1 + "&psttaxded2=" + PostTaxDed2,"calc",
			'width='+m_Width+',height='+m_Height+',top='+(screen.availHeight-m_Height)/2+',left='+(screen.availWidth-m_Width)/2+',resizable=yes,scrollbars=yes');


	return false;
}

//MITS 13132 : Umesh
function bCompareDate(sDate1, sDate2) {
    var Date1 = new Date(sDate1);
    var Date2 = new Date(sDate2);

    if (Date2 >= Date1) {
        return true;
    }
    else {
        return false;
    }
}
//MITS 13132 : End

function printedPayments()
{
//pmittal5 Mits 14196 04/23/09 - Dont allow opening Printed payments if data not Saved on Non-Occ Payments screen
   if(m_DataChanged)
   {
       alert("Data has changed. Please save the changes.");
       return false;
   }
 //End - pmittal5  
	if (m_Popup!=null)
		m_Popup.close();
	
	var lClaimId=document.forms[0].claimid.value;
	var taxarray=document.forms[0].taxarray.value;
	m_Popup=window.open("../NonOccPayments/PrintedPayments.aspx?claimid="+lClaimId+"&taxarray="+taxarray+"&syscmd="+SysCmd_DisplayPrinted,"printed",
			'width='+m_Width+',height='+m_Height+',top='+(screen.availHeight-m_Height)/2+',left='+(screen.availWidth-m_Width)/2+',resizable=yes,scrollbars=yes');

	return false;
}

function futurePayments() {
   
//pmittal5 Mits 14196 04/23/09 - Dont allow opening future payments if data not Saved on Non-Occ Payments screen
   if(m_DataChanged)
   {
       alert("Data has changed. Please save the changes.");
       return false;
   }
 //End - pmittal5  
	if (m_Popup!=null)
		m_Popup.close();
	
	var lClaimId=document.forms[0].claimid.value;
	var taxarray=document.forms[0].taxarray.value;
	var claimnumber = document.forms[0].claimnumber.value;
	

	m_Popup=window.open("../NonOccPayments/NonOccFuturePayments.aspx?claimid="+lClaimId+"&taxarray="+taxarray+"&syscmd="+SysCmd_DisplayFuture+"&claimnumber="+claimnumber,"future",
			'width='+m_Width+',height='+m_Height+',top='+(screen.availHeight-m_Height)/2+',left='+(screen.availWidth-m_Width)/2+',resizable=yes,scrollbars=yes');

	return false;
}

function nonocccollection()
{
//pmittal5 Mits 14196 04/23/09 - Dont allow opening Collections if data not Saved on Non-Occ Payments screen
   if(m_DataChanged)
   {
       alert("Data has changed. Please save the changes.");
       return false;
   }
 //End - pmittal5  
	if (m_Popup!=null)
		m_Popup.close();

    var lClaimId = document.forms[0].claimid.value;
    var sClaimNumber = document.forms[0].claimnumber.value;
	var taxarray=document.forms[0].taxarray.value;
	m_Popup = window.open("../NonOccPayments/Collection.aspx?ClaimId=" + lClaimId + "&amp;ClaimNumber=" + sClaimNumber, "nonocccollection",
			'width='+m_Width+',height='+m_Height+',top='+(screen.availHeight-m_Height)/2+',left='+(screen.availWidth-m_Width)/2+',resizable=yes,scrollbars=yes');     
  
	return false ;
}

function setReserveType(objCtrl) {
   //skhare7 Changes done for MITS 27920
    var ReserveTypeArray = document.forms[0].reservetypearray.value;
    var arrReserves = ReserveTypeArray.split(",");
	var objCombo;
	if (objCtrl != null) {

	    objCombo = document.getElementById(objCtrl);

	    if (objCombo != null) {
	        for (var i = 0; i < objCombo.options.length; i++) {
	            if (objCombo.options[i].selected) {
	                if (i > 0) {
	                    if (arrReserves.length > 0) { //averma62 - MITS 31325
	                        for (var j = 0; j < arrReserves.length; j++) {
	                            if (arrReserves[j].indexOf('_') != -1) {
	                                var arrReservesStatus = arrReserves[j].split("_");
	                                document.forms[0].reservetype.value = arrReservesStatus[0];
	                                if (arrReservesStatus[1] == "H") {
	                                    alert('Payment/collection can not be made against the Reserve of hold Status.');
	                                    document.forms[0].reservetype.value = "";
	                                    objCombo.options[0].selected = true;

	                                    return false;
	                                }
	                            }
	                            else {
	                                document.forms[0].reservetype.value = arrReserves[i];
	                                return true;

	                            }
	                        }
	                    }
	                }
	                // return true;
	            }
	        }
	    }
	}
    
	return true;
	//document.forms[0].reservetype.value=document.forms[0].transactiontypecode.options[document.forms[0].transactiontypecode.selectedIndex].reservetype;
}

function EditPayment(iPaymentNumber,lBatchId,dFromDate,dToDate,iGross,iSuppPayment,pensionoffset,ssoffset,oioffset,OtherOffset1,OtherOffset2,OtherOffset3,PostTax1,PostTax2,autosplitid,printdate,claimid)
{
    var sClaimNumber = document.forms[0].claimnumber.value;
    var claimid = document.forms[0].claimid.value;
	
//Animesh Modified the defination for GHS Enhancment - pmittal5 Mits 14841
//	m_Popup = window.open("../NonOccPayments/EditPayment.aspx?PaymentNumber="+ iPaymentNumber + "&BatchId=" + lBatchId + "&FromDate=" + dFromDate + "&ToDate=" + dToDate + "&Gross=" + iGross + "&SuppPayment=" + iSuppPayment + "&pensionoffset=" + (pensionoffset*-1) + "&autosplitid=" + autosplitid + "&printdate=" + printdate+ "&ssoffset=" + (ssoffset*-1) + "&oioffset=" + (oioffset*-1) + "&claimnumber=" + sClaimNumber+"&claimid=" + claimid,'progressWnd',
//							'width=' + 700 + ',height=' + 450 + ',top=' + (screen.availHeight - 450) / 2 + ',left=' + (screen.availWidth - 700) / 2 + ',resizable=no,scrollbars=yes');

	m_Popup = window.open("../NonOccPayments/EditPayment.aspx?PaymentNumber="+ iPaymentNumber + "&BatchId=" + lBatchId + "&FromDate=" + dFromDate + "&ToDate=" + dToDate + "&Gross=" + iGross + "&SuppPayment=" + iSuppPayment + "&pensionoffset=" + (pensionoffset*-1) + "&autosplitid=" + autosplitid + "&printdate=" + printdate+ "&ssoffset=" + (ssoffset*-1) + "&oioffset=" + (oioffset*-1) + "&claimnumber=" + sClaimNumber+"&claimid=" + claimid + "&othoffset1=" + (OtherOffset1*-1) + "&othoffset2=" + (OtherOffset2*-1) + "&othoffset3=" + (OtherOffset3*-1) + "&psttaxded1=" + (PostTax1*-1) + "&psttaxded2=" + (PostTax2*-1),'progressWnd',
							'width=' + 700 + ',height=' + 450 + ',top=' + (screen.availHeight - 450) / 2 + ',left=' + (screen.availWidth - 700) / 2 + ',resizable=no,scrollbars=yes');
	 				
	return false;
}

//Raman: Incorrect version: Use only form.js version.. hence commenting
/*
function setDataChanged(b)
{
	m_DataChanged=b;
	if (m_ThisDoc=='nonocc.asp')
		setDataChanged_NonOccAsp(b);
	return b;
}
*/

function setDataChanged_NonOccAsp(b)
{
	m_DataChanged_NonOccAsp=b;
	return b;
}

function setDataChanged_NonOccAsp(b , amountchange)
{
	m_DataChanged_NonOccAsp=b;
	if(b == true)
	{
	    m_amountchange = amountchange;
	}
	return b;
}

// RMA 978 start
function dateLostFocus(sCtrlName) {
   try {
        var target = $("#" + sCtrlName);
        if (String(target[0].readOnly).toLowerCase() == "true") {
            return true;
        }
    }
    catch (e) {
        dateLostFocusOld(sCtrlName);
        return true;
    }
    //Deb
    //Rakhel - Modified for ML - Start
    var inst = $.datepicker._getInst(target[0]);
    var format = target.datepicker('option', 'dateFormat');
    var format1 = format.substr(0, 2).toUpperCase();
    var format2 = format.substr(3, 2).toUpperCase();
    var format3 = format.substr(6, 4).toUpperCase();
    //Rakhel - Modified for ML - End
    var sDateSeparator;
    var iDayPos = 0, iMonthPos = 0;
    var d = new Date(1999, 11, 22);
    var s = d.toLocaleString();
    var sRet = "";
    var objFormElem = eval('document.forms[0].' + sCtrlName);
    var sDate = new String(objFormElem.value);
    var iMonth = 0, iDay = 0, iYear = 0;
    var monthDays = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
    if (sDate == "")
        return "";
    if ((sDate.length == 4) && (sDate.indexOf('/') == -1)) {
        sDate = "0" + sDate.substr(0, 1) + '/' + "0" + sDate.substr(1, 1) + '/' + sDate.substr(2, 2);
    }
    else if ((sDate.length == 6) && (sDate.indexOf('/') == -1)) {
        sDate = sDate.substr(0, 2) + '/' + sDate.substr(2, 2) + '/' + sDate.substr(4, 4);
    }
    else if ((sDate.length == 8) && (sDate.indexOf('/') == -1)) {
        if (format1 != "YY") {
            sDate = sDate.substr(0, 2) + '/' + sDate.substr(2, 2) + '/' + sDate.substr(4, 4);
        }
        else {
            sDate = sDate.substr(0, 4) + '/' + sDate.substr(4, 2) + '/' + sDate.substr(6, 2);
        }
    }
    iDayPos = s.indexOf("22");
    iMonthPos = s.indexOf("11");
    //if(IE4)
    //	sDateSeparator=s.charAt(iDayPos+2);
    //else
    sDateSeparator = "/";
    var sArr = sDate.split(sDateSeparator);
    var dArr = new Array();
    if (sArr.length == 3) {
        //Rakhel - Modified for ML - Start
        if (format1.substring(0, 1) === "D") {
            dArr[1] = new String(parseInt(sArr[0], 10));
            if (format3.substring(0, 1) === "Y") { // ddmmyy
                dArr[0] = new String(parseInt(sArr[1], 10));
                dArr[2] = new String(parseInt(sArr[2], 10));
            }
            else { //ddyymm
                dArr[0] = new String(parseInt(sArr[2], 10));
                dArr[2] = new String(parseInt(sArr[1], 10));
            }
        }

        if (format1.substring(0, 1) === "M") {
            dArr[0] = new String(parseInt(sArr[0], 10));
            if (format3.substring(0, 1) === "Y") { // mmddyy
                dArr[1] = new String(parseInt(sArr[1], 10));
                dArr[2] = new String(parseInt(sArr[2], 10));
            }
            else { //mmyydd
                dArr[1] = new String(parseInt(sArr[2], 10));
                dArr[2] = new String(parseInt(sArr[1], 10));
            }
        }

        if (format1.substring(0, 1) === "Y") {
            dArr[2] = new String(parseInt(sArr[0], 10));
            if (format3.substring(0, 1) === "M") { // yyddmm
                dArr[1] = new String(parseInt(sArr[1], 10));
                dArr[0] = new String(parseInt(sArr[2], 10));
            }
            else { //yymmdd
                dArr[1] = new String(parseInt(sArr[2], 10));
                dArr[0] = new String(parseInt(sArr[1], 10));
            }
        }
        //Rakhel - Modified for ML - End

        //Convert potential 2-digit year to 4-digit year
        dArr[2] = Get4DigitYear(dArr[2]);
        //Rakhel - Modified for ML - Start
        var valid = true;
        if ((dArr[0] < 1) || (dArr[0] > 12)) valid = false;
        else if ((dArr[1] < 1) || (dArr[1] > 31)) valid = false;
            //asingh263 mits 34110 starts
        else if ((dArr[2] < 1900) || (dArr[2] > 9999)) valid = false;
            //asingh263 mits 34110 ends
        else if (((dArr[0] == 4) || (dArr[0] == 6) || (dArr[0] == 9) || (dArr[0] == 11)) && (dArr[1] > 30)) valid = false;
        else if ((dArr[0] == 2) && (((dArr[2] % 400) == 0) || ((dArr[2] % 4) == 0)) && ((dArr[2] % 100) != 0) && (dArr[1] > 29)) valid = false;
        else if ((dArr[0] == 2) && ((dArr[2] % 100) == 0) && (dArr[1] > 29)) valid = false;


        if (!valid) {
            if (inst.input) {
	    //rkulavil: RMA-6372 start
                // alert(parent.CommonValidations.ValidationDateGreaterorEqualTo);
                inst.selectedDay = inst.currentDay = "01";
                inst.selectedMonth = inst.currentMonth = "00";
                inst.selectedYear = inst.currentYear = "1900";
                var dateStr = $.datepicker._formatDate(inst, inst.currentDay, inst.currentMonth, inst.currentYear);
                dateStr = (dateStr != null ? dateStr : $.datepicker._formatDate(inst));
                alert(parent.CommonValidations.ValidationDateGreaterorEqualTo + dateStr);
	    //rkulavil: RMA-6372 end
                inst.input.val("");
            }
            return true;
        }
        //var target = $("#" + sCtrlName);
        //var inst = $.datepicker._getInst(target[0]);
        //Rakhel - Modified for ML - End

        inst.selectedDay = inst.currentDay = dArr[1].toString();
        inst.selectedMonth = inst.currentMonth = parseInt(dArr[0]) - 1;
        inst.selectedYear = inst.currentYear = parseInt(dArr[2]);
        var dateStr = $.datepicker._formatDate(inst, inst.currentDay, inst.currentMonth, inst.currentYear);
        dateStr = (dateStr != null ? dateStr : $.datepicker._formatDate(inst));
        if (inst.input)
            inst.input.val(dateStr);

        if (sCtrlName == 'birthdate' && objFormElem != null && sDate != "") {
            return calculateage(objFormElem); //Needs to be changed for localization
        }
        //Added for Mits 22132
        var objFormName = document.getElementById('SysFormName');
        if (objFormName != null && objFormName.value.toLowerCase() == "cmxaccommodation") {
            return checkAccomodation(objFormElem); //Needs to be changed for localization
        }
        //Added for Mits 22132

        return true; //aaggarwal29 :  Commented since code returns from here everytime and later part of method is never executed // rkulavil : Uncommenting the code and moving the code done for this change before the return true code
        // Classic leap year calculation
        if (((parseInt(sArr[2], 10) % 4 == 0) && (parseInt(sArr[2], 10) % 100 != 0)) || (parseInt(sArr[2], 10) % 400 == 0))
            monthDays[1] = 29;
        if (iDayPos < iMonthPos) {
            // Date should be as dd/mm/yyyy
            if (parseInt(sArr[1], 10) < 1 || parseInt(sArr[1], 10) > 12 || parseInt(sArr[0], 10) < 0 || parseInt(sArr[0], 10) > monthDays[parseInt(sArr[1], 10) - 1])
                objFormElem.value = "";
        }
        else {
            // Date is something like mm/dd/yyyy
            //	if(parseInt(sArr[0],10)<1 || parseInt(sArr[0],10)>12 ||  parseInt(sArr[1],10)<0 || parseInt(sArr[1],10)>monthDays[parseInt(sArr[0],10)-1])
            if (parseInt(sArr[0], 10) < 1 || parseInt(sArr[0], 10) > 12 || parseInt(sArr[1], 10) < 1 || parseInt(sArr[1], 10) > monthDays[parseInt(sArr[0], 10) - 1])  // Added by csingh7 : MITS 14181
                objFormElem.value = "";
        }
        // Check the year
        if (parseInt(sArr[2], 10) < 10 || (sArr[2].length != 4 && sArr[2].length != 2))
            objFormElem.value = "";
        // If date has been accepted
        if (objFormElem.value != "") {
            // Format the date
            if (sArr[0].length == 1)
                sArr[0] = "0" + sArr[0];
            if (sArr[1].length == 1)
                sArr[1] = "0" + sArr[1];
            if (sArr[2].length == 2)
                sArr[2] = "20" + sArr[2];
            if (iDayPos < iMonthPos)
                objFormElem.value = formatDate(sArr[2] + sArr[1] + sArr[0]);
            else
                objFormElem.value = formatDate(sArr[2] + sArr[0] + sArr[1]);
        }
    }

    return true;
}

//function dateLostFocus(sCtrlName)
//{
//    debugger;
//	var sDateSeparator;
//	var iDayPos=0, iMonthPos=0;
//	var d=new Date(1999,11,22);
//	var s=d.toLocaleString();
//	var sRet="";
//	var objFormElem=eval('document.forms[0].'+sCtrlName);
//	var sDate=new String(objFormElem.value);
//	var iMonth=0, iDay=0, iYear=0;
//	var monthDays = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
//	if(sDate=="")
//		return "";
//	if(sDate.indexOf('/') == -1)
//	{
//		sDate = sDate.substr(0 , 2) + '/' + sDate.substr(2 , 2) + '/' + sDate.substr(4 , 4);
//	}
//	iDayPos=s.indexOf("22");
//	iMonthPos=s.indexOf("11");
//	//if(IE4)
//	//	sDateSeparator=s.charAt(iDayPos+2);
//	//else
//		sDateSeparator="/";
//	var sArr=sDate.split(sDateSeparator);
//	if(sArr.length==3)
//	{
//		sArr[0]=new String(parseInt(sArr[0],10));
//		sArr[1]=new String(parseInt(sArr[1],10));
//		sArr[2]=new String(parseInt(sArr[2],10));
		
//		//Convert potential 2-digit year to 4-digit year
//		sArr[2] = Get4DigitYear(sArr[2]);		
		
//		// Classic leap year calculation
//		if (((parseInt(sArr[2],10) % 4 == 0) && (parseInt(sArr[2],10) % 100 != 0)) || (parseInt(sArr[2],10) % 400 == 0))
//			monthDays[1] = 29;
//		if(iDayPos<iMonthPos)
//		{
//			// Date should be as dd/mm/yyyy
//			if(parseInt(sArr[1],10)<1 || parseInt(sArr[1],10)>12 ||  parseInt(sArr[0],10)<0 || parseInt(sArr[0],10)>monthDays[parseInt(sArr[1],10)-1])
//				objFormElem.value="";
//		}
//		else
//		{
//			// Date is something like mm/dd/yyyy
//			if(parseInt(sArr[0],10)<1 || parseInt(sArr[0],10)>12 ||  parseInt(sArr[1],10)<0 || parseInt(sArr[1],10)>monthDays[parseInt(sArr[0],10)-1])
//				objFormElem.value="";
//		}
//		// Check the year
//		if(parseInt(sArr[2],10)<10 || (sArr[2].length!=4 && sArr[2].length!=2))
//			objFormElem.value="";
//		// If date has been accepted
//		if(objFormElem.value!="")
//		{
//			// Format the date
//			if(sArr[0].length==1)
//				sArr[0]="0" + sArr[0];
//			if(sArr[1].length==1)
//				sArr[1]="0" + sArr[1];
//			if(sArr[2].length==2)
//				sArr[2]="19"+sArr[2];
//			if(iDayPos<iMonthPos)
//				objFormElem.value=formatDate(sArr[2] + sArr[1] + sArr[0]);
//			else
//				objFormElem.value=formatDate(sArr[2] + sArr[0] + sArr[1]);
//		}
//	}
//	else
//		objFormElem.value="";
//	return true;
//}
// RMA 978 End

//Convert 2-digit year to 4-digit year
function Get4DigitYear(sInputYear) {
    var sGuessedYear = sInputYear;
    var dCurrentDate = new Date();
    var iCurrentYear = dCurrentDate.getFullYear();
    var sCurrentYear = new String(iCurrentYear);

    //01/05/2010 Raman: Logic below failed for 2010 onwards
    //converting inputyear to 2 characters

    if (sInputYear.length == 1) {
        sInputYear = "0" + sInputYear;
    }

    //If the input year is not 4-digit year, get missing digit form current year
    //	if (sInputYear.length < 4)  Commented by csingh7 : MITS 14181
    if (sInputYear.length < 3)  // Added by csingh7 : MITS 14181
    {
        sGuessedYear = sCurrentYear.substr(0, 4 - sInputYear.length) + sInputYear;

        //If the guessed year is 20 years in the future, assume it's in the last centure
        var iGuessedYear = parseInt(sGuessedYear);
        if (iGuessedYear - iCurrentYear > 20) {
            iGuessedYear = iGuessedYear - 100;
            sGuessedYear = new String(iGuessedYear);
        }
    }

    return sGuessedYear;
}


function dateSelected(sDay, sMonth, sYear)
{
	var objCtrl=null;
	if(m_codeWindow!=null)
		m_codeWindow.close();
	objCtrl=eval('document.forms[0].'+m_sFieldName);
	if(objCtrl!=null)
	{
		sDay=new String(sDay);
		if(sDay.length==1)
			sDay="0"+sDay;
		sMonth=new String(sMonth);
		if(sMonth.length==1)
			sMonth="0"+sMonth;
		sYear=new String(sYear);
				
		objCtrl.value=formatDate(sYear+sMonth+sDay);
	}
	m_sFieldName="";
	m_codeWindow=null;
	
	setDataChanged(true);
		
	return true;
}

function selectDate(sFieldName)
{
	if(m_codeWindow!=null)
		m_codeWindow.close();
		
	m_sFieldName=sFieldName;
	
	m_codeWindow=window.open('calendar.html','codeWnd',
		'width=230,height=230'+',top='+(screen.availHeight-230)/2+',left='+(screen.availWidth-230)/2+',resizable=yes,scrollbars=no');
	return false;
}
function onCodeClose()
{
	m_codeWindow=null;
	return true;
}

function formatDate(sParamDate)
{
	var sDateSeparator;
	var iDayPos=0, iMonthPos=0;
	var d=new Date(1999,11,22);
	var s=d.toLocaleString();
	var sRet="";
	var sDate=new String(sParamDate);
	if(sDate=="")
		return "";
	iDayPos=s.indexOf("22");
	iMonthPos=s.indexOf("11");
	//if(IE4)
	//	sDateSeparator=s.charAt(iDayPos+2);
	//else
		sDateSeparator="/";
	if(iDayPos<iMonthPos)
		sRet=sDate.substr(6,2) + sDateSeparator + sDate.substr(4,2) + sDateSeparator + sDate.substr(0,4);
	else
		sRet=sDate.substr(4,2) + sDateSeparator + sDate.substr(6,2) + sDateSeparator + sDate.substr(0,4);
	return sRet;
}

function dateFormat()
{
	var iDayPos=0, iMonthPos=0;
	var d=new Date(1999,11,22);
	var s=d.toLocaleString();
	var sRet="";
	iDayPos=s.indexOf("22");
	iMonthPos=s.indexOf("11");

	if(iDayPos<iMonthPos)
		sRet='dmy';
	else
		sRet='mdy';
	return sRet;
}

function selectBatch(obj)
{
	var iBatchId=obj.value;
	var iLength = eval("document.forms[0].chkDelete.length");
		
	//could be one checkbox or multiple
	if(iLength!=null)
	{
		for(i=0;i<iLength;i++)
		{
			if (document.forms[0].chkDelete[i].getAttribute('batchid')==iBatchId)
			{
				if (obj.checked)
					document.forms[0].chkDelete[i].checked=true;
				else
					document.forms[0].chkDelete[i].checked=false;
			}
		}
	}
	else
	{
	    if (document.forms[0].chkDelete.getAttribute('batchid') == iBatchId)
		{
			if (obj.checked)
					document.forms[0].chkDelete.checked=true;
				else
					document.forms[0].chkDelete.checked=false;
		}
	}
	return true;
}


function CheckCollectionFields()
{
	if (document.forms[0].txtStart.value!="" && document.forms[0].txtEnd.value!="" && document.forms[0].txtAmount.value!="")
	{
		if (parseInt(document.forms[0].txtAmount.value,10)!=0)
		{
			document.forms[0].FormMode.value="close";
			//Raman Bhatia: Enabling all disabled controls
			document.forms[0].txtNet.disabled = false;
			document.forms[0].txtMed.disabled = false;
			document.forms[0].txtFed.disabled = false;
			document.forms[0].txtSS.disabled = false;
			document.forms[0].txtState.disabled = false;
			return true;
		}
	}
	alert('From, To, Collection Amount are required fields');
	return false;
}

function ManualEntryOfTaxes()
{
	if (document.forms[0].chkManual.checked==true)
	{
	    document.forms[0].hdnManulaentry.value = "1";
		document.forms[0].txtFed.disabled=false;
		document.forms[0].txtSS.disabled=false;
		document.forms[0].txtMed.disabled=false;
		document.forms[0].txtState.disabled=false;
	}
	else
	{
	    document.forms[0].hdnManulaentry.value = "";
		document.forms[0].txtFed.disabled=true;
		document.forms[0].txtSS.disabled=true;
		document.forms[0].txtMed.disabled=true;
		document.forms[0].txtState.disabled=true;
		CalculateTaxes();
	}
}

function TaxCheckBoxClicked(chkName)
{
 var chkTax = eval("document.forms[0]."+chkName);
 var chkTaxFlag = eval("document.forms[0].hdn" + chkName);
 
 if(chkTax.checked == true)
 {
    chkTaxFlag.value = "1"; 
 }
 else
 {
    chkTaxFlag.value = "";  
 }
 
 if (document.forms[0].chkManual.checked==false)
		CalculateTaxes();
}
function storeAmount()
{
	if(isNaN(parseFloat(document.forms[0].txtAmount.value)))
		dGrossCollectionAmount=0;
	else
		dGrossCollectionAmount = document.forms[0].txtAmount.value;
}
function CalculateTaxes()
{
	var dGrossPayment=0;
	var dTaxablePercent=0;
	var FED_TAX_RATE=0;
	var SS_TAX_RATE=0;
	var MEDICARE_TAX_RATE=0;
	var STATE_TAX_RATE=0;
	var dTotalWithholding=0;
	var dNetAmount=0;
	var dTempGross;
	if(isNaN(parseFloat(dGrossCollectionAmount)))
		dGrossPayment=0;
	else
		dGrossPayment=parseFloat(dGrossCollectionAmount);
	
	if(isNaN(parseFloat(document.forms[0].TaxablePercent.value)))
		dTaxablePercent=0;
	else
		dTaxablePercent=parseFloat(document.forms[0].TaxablePercent.value);
	if(isNaN(parseFloat(document.forms[0].FED_TAX_RATE.value)))
		FED_TAX_RATE=0;
	else
		FED_TAX_RATE=parseFloat(document.forms[0].FED_TAX_RATE.value);
		
	if(isNaN(parseFloat(document.forms[0].SS_TAX_RATE.value)))
		SS_TAX_RATE=0;
	else
		SS_TAX_RATE=parseFloat(document.forms[0].SS_TAX_RATE.value);
		
	if(isNaN(parseFloat(document.forms[0].MEDICARE_TAX_RATE.value)))
		MEDICARE_TAX_RATE=0;
	else
		MEDICARE_TAX_RATE=parseFloat(document.forms[0].MEDICARE_TAX_RATE.value);
		
	if(isNaN(parseFloat(document.forms[0].STATE_TAX_RATE.value)))
		STATE_TAX_RATE=0;
	else
		STATE_TAX_RATE=parseFloat(document.forms[0].STATE_TAX_RATE.value);
	
	var dFederalTax=0;
	var dSocialSecurityAmount=0;
	var dMedicareAmount=0;
	var dStateAmount=0;
	
	if(document.forms[0].chkManual.checked == true)
	{
	    dFederalTax = parseFloat(document.forms[0].txtFed.value);
	    dSocialSecurityAmount = parseFloat(document.forms[0].txtSS.value);
	    dMedicareAmount = parseFloat(document.forms[0].txtMed.value);
	    dStateAmount = parseFloat(document.forms[0].txtState.value);
	    dTotalWithholding = dFederalTax + dSocialSecurityAmount + dMedicareAmount + dStateAmount;
	    dNetAmount = dGrossPayment - dTotalWithholding;
	    document.forms[0].txtNet.value=dNetAmount;
	    return false; 
	}
		
	if (document.forms[0].Fed.checked==true)
	{
		if (dTaxablePercent > 0)
		{
			dFederalTax = (dGrossPayment * dTaxablePercent) * FED_TAX_RATE;
		}
		else
		{
			dFederalTax = dGrossPayment *  FED_TAX_RATE;
		}		
	}
	else
		dFederalTax=0;
	
	if (document.forms[0].txtFed.disabled==false)
		dFederalTax=parseFloat(document.forms[0].txtFed.value);
		
	if (document.forms[0].SS.checked==true)
	{
		if (dTaxablePercent > 0)
			dSocialSecurityAmount = (dGrossPayment * dTaxablePercent) * SS_TAX_RATE;
		else
			dSocialSecurityAmount = dGrossPayment *  SS_TAX_RATE;
	}
	else
		dSocialSecurityAmount=0;
		
	if (document.forms[0].txtSS.disabled==false)
		dSocialSecurityAmount=parseFloat(document.forms[0].txtSS.value);
		
	if (document.forms[0].Med.checked==true)
	{
		if (dTaxablePercent > 0)
			dMedicareAmount = (dGrossPayment * dTaxablePercent) * MEDICARE_TAX_RATE;
		else
			dMedicareAmount = dGrossPayment *  MEDICARE_TAX_RATE;
	}
	else
		dMedicareAmount=0;
		
	if (document.forms[0].txtMed.disabled==false)
		dMedicareAmount=parseFloat(document.forms[0].txtMed.value);
		
	if (document.forms[0].State.checked==true)
	{
		if (dTaxablePercent > 0)
			dStateAmount = (dGrossPayment * dTaxablePercent) * STATE_TAX_RATE;
		else
			dStateAmount = dGrossPayment *  STATE_TAX_RATE;
	}
	else
		dStateAmount=0;
	
	if (document.forms[0].txtState.disabled==false)
		dStateAmount=parseFloat(document.forms[0].txtState.value);
	
	dTotalWithholding = dFederalTax + dSocialSecurityAmount + dMedicareAmount + dStateAmount;
    dNetAmount = dGrossPayment - dTotalWithholding;
	document.forms[0].txtFed.value=dFederalTax.toFixed(2);
	document.forms[0].txtSS.value=dSocialSecurityAmount.toFixed(2);
	document.forms[0].txtMed.value=dMedicareAmount.toFixed(2);
	document.forms[0].txtState.value=dStateAmount.toFixed(2);
	document.forms[0].txtNet.value=dNetAmount.toFixed(2);
}

function CheckForWindowClosing()
{
	if (document.forms[0].hdnformmode.value=="close")
		window.close();
}
function numLostFocus(objCtrl)
{
	if(objCtrl.value.length==0)
			return false;
			
	if(isNaN(parseFloat(objCtrl.value)))
		objCtrl.value="0";
	else
		objCtrl.value=parseFloat(objCtrl.value);
	
	if(objCtrl.getAttribute('min')!=null)
	{
		if (!(isNaN(parseFloat(objCtrl.getAttribute('min')))))
		{
			if (parseFloat(objCtrl.value)<parseFloat(objCtrl.getAttribute('min')))
			{
				objCtrl.value=0;
			}
		}
	}
	
	return true;
}

function ManualPaymentDetails()
{
    //pmittal5 Mits 14300 02/09/09 - Dont process if payments are frozen
    var paymentFrozen =document.forms[0].paymntfrozen.value;
    if(paymentFrozen == "True")
    {
        alert("This claim has its payments frozen. No further payments may be processed until it is unfrozen.");
        return false;
    }        
    //End - pmittal5
    	if(!eval("document.forms[0].chkDelete"))
		return false;

	var iLength = eval("document.forms[0].chkDelete.length");
	var sSelected=new String("");
	var sAutoSplitId=new String("");
	//could be one checkbox or multiple
	if(iLength!=null){
		for(i=0;i<iLength;i++){
			if(document.forms[0].chkDelete[i].checked==true)
				{
			    sSelected=sSelected+"|"+document.forms[0].chkDelete[i].value+','+document.forms[0].chkDelete[i].getAttribute('batchid');
				    sAutoSplitId = document.forms[0].chkDelete[i].autosplitid;
				}
		}
		sSelected=sSelected.substring(1);
	}
	else{
		if(document.forms[0].chkDelete.checked==true)
			{
		    sSelected=document.forms[0].chkDelete.value+','+document.forms[0].chkDelete.getAttribute('batchid');		
			    sAutoSplitId = document.forms[0].chkDelete.autosplitid;
			}
	}
	
	if(sSelected=="")
	{
		alert('Please select some Payment to Process');
		return false;
	}
		
		
	document.forms[0].deletelist.value=sSelected;
	document.forms[0].syscmd.value=SysCmd_DeletePayment;
	//document.forms[0].submit();
	
	wnd=window.open("../NonOccPayments/ManualPaymentDetails.aspx?autosplitid=" + sAutoSplitId
						,"ManualPaymentDetail",
			"width=400,height=150,top="+((screen.availHeight-150)/2)+
			",left="+((screen.availWidth-400)/2)+"');",100);
	
	return false;	

}
function processManualPayments()
{
    if(confirm("This Process assumes you have manually written a check to replace this one. It will treat this payment as if it had printed in RISKMASTER but without printing a physical check."))
    {
       document.forms[0].chkDate.disabled = false;
       return true; 
    }
    else
    {
        return false;
    }
        
    
}
function ClosePopupIfPaymentProcessed(p_iPaymentProcessedFlag)
{       
    p_iPaymentProcessedFlag = document.forms[0].manualpaymentprocessedflag.value;
    window.opener.document.forms[0].processpaymentflag.value="True";
    
    if(p_iPaymentProcessedFlag == 'True')
    {
        alert("Payment has been succesfully processed");        
        window.opener.document.forms[0].transidfuture.value = document.forms[0].transiddetail.value; //igupta3 Mits 28566
        window.opener.document.forms[0].submit();        
        window.close();
    }
    if(p_iPaymentProcessedFlag == 'False')
    {
        alert("Error in processing Payment");
        window.opener.document.forms[0].submit();
        window.close();
    }

}
//ybhaskar: MITS 14172. Function is added to set the value of selected dropdown item into a hidden textbox
function onindexchange() 
{
    var ctrlclassname = document.getElementById('classname');
    var ctrlclasslist = document.getElementById('classlist');
    ctrlclasslist.value = ctrlclassname.value;
} 