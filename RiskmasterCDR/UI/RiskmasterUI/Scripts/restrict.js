function dateFormat() {
    
	var iDayPos=0, iMonthPos=0;
	var d=new Date(1999,11,22);
	var s=d.toLocaleString();
	var sRet="";
	iDayPos=s.indexOf("22");
	iMonthPos=s.indexOf("11");

	if(iDayPos<iMonthPos)
		sRet='dmy';
	else
		sRet='mdy';
	return sRet;
}

function confirmRestrictDate(ctrl) {
    
	var s=new String(ctrl.value);
	m_sFieldName=ctrl.id;
	
    //abansal23: MITS 15399 on 4/21/2009 Starts
    if (s == '' || s == null) {
        if (document.forms[0].duration != null) {
            document.forms[0].duration.value = 0;
        }
        return false;
    }
    //abansal23: MITS 15399 on 4/21/2009 Ends
	//pad to 2-digit day/month
	var i=s.indexOf("/");
	if(i!=2)
		s='0'+s;
		
	i=s.indexOf("/", 3);
	if(i!=5)
		s=s.substring(0,3)+"0"+s.substring(3)

	var sDateFormat=dateFormat();
	if(sDateFormat=='mdy')
		var ret = dateSelectedRestrictNew(s.substring(3,5),s.substring(0,2),s.substring(6,10),ctrl);
	else
		var ret = dateSelectedRestrictNew(s.substring(0,2),s.substring(3,5),s.substring(6,10),ctrl);
	if (ret==true)
	{
//	var wnd=window.open('csc-Theme\\riskmaster\\common\\html\\progress.html','progressWnd',
//				'width=400,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-400)/2);
	    //			self.parent.wndProgress=wnd;
	    pleaseWait.Show();
		document.forms[0].SysCmd.value="7";
		window.document.forms[0].submit();
	}
        setDataChanged(true);  
        SetFocus(ctrl);       //Ritesh: Added to reset focus on control-MITS 27402

}


function selectRestrictDate(sFieldName) {
    
	if(m_codeWindow!=null)
		m_codeWindow.close();
		
	m_sFieldName=sFieldName;
	document.dateSelected=dateSelectedRestrict;
	
	// TODO - Use new calendar
//	m_codeWindow=window.open('csc-Theme\\riskmaster\\common\\html\\calendar.html','codeWnd',
//		'width=230,height=230'+',top='+(screen.availHeight-230)/2+',left='+(screen.availWidth-230)/2+',resizable=yes,scrollbars=no');
	
	return false;
	
}

function dateSelectedRestrictNew(sDay, sMonth, sYear,ctrl) {
    
	var dLast=null;
	var dFirst=null;
	var iDuration=0;
	var bValidated=false;
	var objCtrl=null;
	if(m_codeWindow!=null)
		m_codeWindow.close();
		
	sDay=new String(sDay);
	sMonth=new String(sMonth);
	if(sMonth.length==1)
		sMonth="0"+sMonth;
	sYear=new String(sYear);
	
	var dToday = new Date();  //today's date
	dToday.setHours(0,0,0,0);
	var sTempMonth = new String(parseInt(sMonth,10) - 1);   //Date() expects month 0-11
	var sDateFormat=dateFormat();  //mdy or dmy
	var sTempDay='';
	switch(m_sFieldName){
		case 'datefirstrestrct':

			dFirst=new Date(sYear,sTempMonth,sDay,'0','0','0','0');
			//rsushilaggar MITS 23694
//			if(dToday.getTime() < dFirst.getTime())
//			{
//				alert('First Day Restricted must be <= today date');
//				ctrl.value='';
//				return false;
//			}
            //abansal23: MITS 15399 on 4/21/2009 Starts
            if (document.forms[0].datelastrestrct.value == "") {
                if (document.forms[0].duration != null) {
                    document.forms[0].duration.value = 0;
                }
                return false;
            }
            //abansal23: MITS 15399 on 4/21/2009 Ends	
			break;
			
		case 'datelastrestrct':
            //abansal23: MITS 15399 on 4/21/2009 Starts
            if (document.forms[0].datefirstrestrct.value == "") {
                if (document.forms[0].duration != null) {
                    document.forms[0].duration.value = 0;
                }
                return false;
            }
            //abansal23: MITS 15399 on 4/21/2009 Ends
			break;
	}
	return true;
}

function dateSelectedRestrict(sDay, sMonth, sYear) {
    
	var dLast=null;
	var dFirst=null;
	var iDuration=0;
	var bValidated=false;
	var objCtrl=null;
	if(m_codeWindow!=null)
		m_codeWindow.close();
		
	sDay=new String(sDay);
	sMonth=new String(sMonth);
	if(sMonth.length==1)
		sMonth="0"+sMonth;
	sYear=new String(sYear);
	
	var dToday = new Date();  //today's date
	dToday.setHours(0,0,0,0);
	var sTempMonth = new String(parseInt(sMonth,10) - 1);   //Date() expects month 0-11
	var sDateFormat=dateFormat();  //mdy or dmy
	var sTempDay='';
	switch(m_sFieldName){
		case 'datefirstrestrct':

			dFirst=new Date(sYear,sTempMonth,sDay,'0','0','0','0');
			//rsushilaggar MITS 23694
//			if(dToday.getTime() < dFirst.getTime()){
//				alert('First Day Restricted must be <= today date');
//				return false;
//			}
			
			var s = new String(document.forms[0].datelastrestrct.value);
			if(s!=null && s!=''){
				if(sDateFormat=='mdy'){
					sTempMonth=s.substring(0,2);
					sTempDay=s.substring(3,5);
				}
				else{
					sTempMonth=s.substring(3,5);
					sTempDay=s.substring(0,2);
				}
					
				if(sTempMonth.substring(0,1)=='0')
					sTempMonth=sTempMonth.substring(1,2);
				sTempMonth=parseInt(sTempMonth,10)-1;
				dLast=new Date(s.substring(6,10),sTempMonth,sTempDay,'0','0','0','0');
				
				if(dFirst.getTime() > dLast.getTime()){
					//first date restr cannot be > last date restr
					document.forms[0].datelastrestrct.value='';
					iDuration=dToday.getTime() - dFirst.getTime();
				}
				else
					iDuration=dLast.getTime() - dFirst.getTime();
			}
			else {
				//no datelastrestrct, so use dToday to figure default duration
				iDuration=dToday.getTime() - dFirst.getTime();	
			}
			
			iDuration= Math.floor(iDuration / (1000*60*60*24)) + 1;
			
			bValidated=true;
			
			break;
			
		case 'datelastrestrct':
		
			dLast=new Date(sYear,sTempMonth,sDay,'0','0','0','0');
			//if(dToday.getTime() < dLast.getTime()){
			//	alert('Last Day Restricted must be <= today date');
			//	return false;
			//}
			
			var s = new String(document.forms[0].datefirstrestrct.value);
			if(s!=null && s!=''){
				if(sDateFormat=='mdy'){
					sTempMonth=s.substring(0,2);
					sTempDay=s.substring(3,5);
				}
				else{
					sTempMonth=s.substring(3,5);
					sTempDay=s.substring(0,2);
				}
				
				if(sTempMonth.substring(0,1)=='0')
					sTempMonth=sTempMonth.substring(1,2);
				sTempMonth=parseInt(sTempMonth,10)-1;
				dFirst=new Date(s.substring(6,10),sTempMonth,sTempDay,'0','0','0','0');
				
				if(dFirst.getTime() > dLast.getTime()){
					//first date restr cannot be > last date restr
					document.forms[0].datefirstrestrct.value='';
					iDuration=-1;
				}
				else
					iDuration=dLast.getTime() - dFirst.getTime();
			}
			else {
				//no datefirstrestrct, so set duration to 0
				iDuration=-1;
			}
		
			iDuration= Math.floor(iDuration / (1000*60*60*24)) + 1;
			
			bValidated=true;
			
			break;
			
		default:
			alert('Error in restrict.js, unknown control name ' + m_sFieldName +', please advise the appropriate authority.');
			break;
			
	}
	
	if(sDay.length==1)
		sDay="0"+sDay;
		
	if (bValidated){
		objCtrl=eval('document.forms[0].'+m_sFieldName);
		
		if(objCtrl!=null){
			//objCtrl.value=formatDate(sYear+sMonth+sDay);

		    document.getElementById("duration").value = iDuration;
			setDataChanged(true);
		}
	}
	
	m_sFieldName="";
	m_codeWindow=null;
	document.dateSelected=dateSelected;
	return true;
	
}
