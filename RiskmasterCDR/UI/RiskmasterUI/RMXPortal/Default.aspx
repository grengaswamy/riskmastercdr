﻿<%@ page language="C#" autoeventwireup="true" masterpagefile="~/App_Master/RMXMain.Master"
    codebehind="Default.aspx.cs" theme="RMX_Default" inherits="Riskmaster.RMXPortal._Default"
    stylesheettheme="RMX_Portal" Async="true" %>
<asp:content id="cHead" contentplaceholderid="head" runat="server">
    <script src="../Scripts/jquery/jquery-1.8.0.js"></script>
    <script type="text/javascript" src="Scripts/RMXPortal.js"></script>
	    <script type="text/javascript" >
	        setInterval("window.scrollTo(0,0);", 750);
	        //igupta3 Mits# 33301
	        window.onload= function Styling()
	        {	
	            try{
	                var td = document.getElementsByClassName('cphMainBody_uwtPortal_s');
	                td[0].width='auto';}
	            catch(ex)
	            {}
	        }

    	</script>

</asp:content>
<asp:content id="cHeader" contentplaceholderid="cphHeader" runat="server">
    <div class="banner" id="banner" runat="server">
    </div>
</asp:content>
<asp:content id="Content3" contentplaceholderid="cphMainBody" runat="server">
    <%@ Import  Namespace="Riskmaster.Security.Encryption" %>
    <%--Ashish Ahuja : Mits 30873 - added hidden field to store session timeout and alert time value --%>
     <input runat="server" type="hidden" id="hdnSessionTimeout" name="hdnSessionTimeout" value="" />
     <input runat="server" type="hidden" id="hdnAlertTime" name="hdnAlertTime" value="" />
   <%--Mits 30873 - End --%>
    <!-- //MITS:34257 - JIRA RMA-429 ajohari2 Start -->
    <input runat="server" type="hidden" id="hdnMultiCovgPerClm" name="hdnMultiCovgPerClm" value="" />
    <!-- //MITS:34257 - JIRA RMA-429 ajohari2 End -->
    <div>
        <div id="loginInfoOuter" class="loginInfoOuter" runat="server">
            <div class="loginInfoInner" id="loginInfoInner" runat="server">
                <%-- Bharani : MITS : 32445 - Removing the div and putting the Login Name and Logout in the same span to prevent overlapping --%>
                User: <span id="loginUserValue" class="loginValue" runat="server" style="display:inline">
                    <asp:loginname id="LoginName1" runat="server" />
                    ( <asp:loginstatus forecolor="Red" id="Loginstatus1" cssclass="logoutStatus" runat="server"
                        onloggingout="Loginstatus1_LoggingOut" logoutaction="Redirect"/>
                    )
                    <%--Debabrata Biswas 05/25/2010. Added to show lss link. Single Sign On MITS# MITS 21124 --%>
                    &nbsp;&nbsp;&nbsp;<asp:HyperLink runat="server" ID="RMXLSSSingleSignOnHyperLink" Text="Legal Solution Suite" NavigateUrl="~/UI/Home/LSSSignOn.aspx" Target="_blank" Visible="false" ></asp:HyperLink>
                 </span>
                 <br />
                <div>
                    Datasource: <span id="loginDSValue" class="loginValue" runat="server">
                        <asp:Label ID="lblDSN" runat="server" /></span>

                    &nbsp;&nbsp;&nbsp; 
                    <asp:HyperLink runat="server" ID="RMXVSSSingleSignOnHyperLink" Text="VSS" NavigateUrl="~/UI/Home/VSSSignOn.aspx" Target="_blank" Visible="false"></asp:HyperLink>
                    <%--start averma62 mits 32386--%>
                    &nbsp;&nbsp; 
                    <asp:HyperLink runat="server" ID="RMXFASSingleSignOnHyperLink" Text="FAS" NavigateUrl="~/UI/Home/FASSignOn.aspx" Target="_blank" Visible="false" ></asp:HyperLink><br />
                    <%--end averma62 mits 32386--%>
                View: <span id="loginViewValue" class="loginValue" runat="server">
                    <asp:Label ID="lblViewName" runat="server" /></span>
                </div>
            </div>
        </div>
        <igtab:ultrawebtab id="uwtPortal" runat="server" displaymode="Scrollable" asyncmode="On"
            cssclass="Portal" borderwidth="0px" borderstyle="None" xmlns:igtab="infragistics.webui.ultrawebtab">
             <selectedtabstyle cssclass="Portal_ActiveTab" backgroundimage="../Images/ActiveTab.gif"
                borderwidth="0px" /> <defaulttabstyle cssclass="Portal_NonActiveTab" backgroundimage="../Images/InactiveTab.gif"
                borderwidth="0px" /> <clientsideevents click="uwtPortalTab_Click" /> <%--<DefaultTabSeparatorStyle BackColor="Black" />--%> </igtab:ultrawebtab>
        <div id="tabBar" />
    </div>

    <script type="text/javascript">
        //Ashish Ahuja : Mits 30873 Start
        var sessionTimeout = document.getElementById('<%=hdnSessionTimeout.ClientID%>').value;
        var alertTime = document.getElementById('<%=hdnAlertTime.ClientID%>').value;  
        SetCookie("cookieWarningMessage", sessionTimeout , 1); 
        SetCookie("sessionTime",  sessionTimeout-1 , 1);
        //Added by sharishkumar for Mits 35482
        var timemout = "";
        var datefirst = "";
        var ChkSession = true;
        //End 35482
        function DisplaySessionTimeout()
        {
        sessionTimeout = getCookie("cookieWarningMessage");
        sessionTimeout=sessionTimeout-1;
        SetCookie("cookieWarningMessage", sessionTimeout, 1); 
        if (sessionTimeout == alertTime-1)
            {
            //Mits 35482 sharishkumar 
            //var retVal = alert("Your current Session is about to be over in "+ alertTime +" minute(s). Do you want to renew?");
           // SetCookie("cookieWarningMessage", sessionTimeout , 1); 
            //var img = new Image(1,1);                                        
            // img.src = 'Refresh.aspx';
            Redirect();//Mits 35482 sharishkumar
        }
        if (ChkSession)//Mits 35482 sharishkumar
        {
            window.setTimeout("DisplaySessionTimeout()", 60000);
        }
        }

        //Mits 35482 sharishkumar
        function Redirect()
        {
            var currentdate = new Date();
            starttime = currentdate.getTime();
            var retValue = confirm("Your current Session is about to be over in " + alertTime + " minute(s). Do you want to renew?");            

            if (retValue == true) {
                var currentdate = new Date();
                var exptime = currentdate.getTime();
                var timeduration = exptime - starttime;
                if (timeduration < (60000 * alertTime)) {
                    sessionTimeout = document.getElementById('<%=hdnSessionTimeout.ClientID%>').value;
                    sessionTimeout = sessionTimeout - 1;
                    SetCookie("cookieWarningMessage", sessionTimeout, 1);
                    var img = new Image(1, 1);
                    img.src = 'Refresh.aspx';
                }
                else {
                    ChkSession = false;
                    window.onbeforeunload = "";
                    <%if (AppHelper.ClientId != 0)
                     { %>
                    window.location.href = '../UI/Home/Login.aspx?' + '<%=RMCryptography.EncryptString(AppHelper.ClientId.ToString())%>';
                    <%} else {%>
                    window.location.href = '../UI/Home/Login.aspx';
                    <%} %>
                }
            }
            else {
                window.onbeforeunload = "";
                <%if (AppHelper.ClientId != 0)
                  { %>
                window.location.href = '../UI/Home/Login.aspx?' + '<%=RMCryptography.EncryptString(AppHelper.ClientId.ToString())%>';
                <%} else {%>
                window.location.href = '../UI/Home/Login.aspx';
                <%} %>
            }
    }
        //End Mits 35482 sharishkumar

        function SetCookie(cookieName, cookieValue, nDays) 
        {
            var today = new Date();
            var expire = new Date();
            if (nDays==null || nDays==0) nDays=1;
 
            expire.setTime(today.getTime() + 3600000*24*nDays);
            document.cookie = cookieName + "=" + escape(cookieValue)
            + ";path=/;expires="+expire.toGMTString();
        }

       function getCookie(c_name)
        {
            var i,x,y,ARRcookies=document.cookie.split(";");
            for (i=0;i<ARRcookies.length;i++)
                {
                    x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
                    y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
                    x=x.replace(/^\s+|\s+$/g,"");

                    if (x==c_name)
                    {
                    return unescape(y);
                    }

                }
        }
        //Ashish Ahuja : Mits 30873 End

        window.onbeforeunload = confirmClose;
    </script>

</asp:content>
