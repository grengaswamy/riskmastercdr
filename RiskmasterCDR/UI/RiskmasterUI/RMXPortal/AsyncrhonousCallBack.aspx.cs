﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Riskmaster.UI.UI
{
    public partial class AsyncrhonousCallBack : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LogoutSession();
        }
        private void LogoutSession()
        {
            Riskmaster.UI.RMAuthentication.AuthenticationServiceClient authService = new Riskmaster.UI.RMAuthentication.AuthenticationServiceClient();
            //Open the call to the Service
            authService.Open();
            string strSessionID = AppHelper.ReadCookieValue("SessionId");
            //Log out the user
            authService.LogOutUser(strSessionID);
            //Remove the user's cookie
            AppHelper.RemoveCookie();
           //Remove the Forms authentication ticket
          System.Web.Security.FormsAuthentication.SignOut();
        }
    }

}