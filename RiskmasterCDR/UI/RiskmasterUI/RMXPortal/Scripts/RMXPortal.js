/* =========================================================================

NAME: Michael A. Cojocari

AUTHOR: CSC Financial Services Group , CSC
DATE  : 4/23/2009

COMMENT: RMXPortal.js

============================================================================ */
// APEYKOV - JIRA:RMA-3816
var bIsLogout = false;
var bIsFirstUnload = false;
var bIsIE = (getInternetExplorerVersion() > -1);
var bPageIsReadyForUnload = false;
//MITS 16226: Raman
var bChangeDatabaseClicked = false;

// MITS 14524 MAC
var bInitiatedUnauthentication = false;

//hlv MITS 27964 begin
var iwintype = "";
var iwinES = new Array();
//hlv MITS 27964 end

function confirmClose() {
    var sConfirmationMessage = "You are exiting RISKMASTER and any unsaved work will be lost.";

    // APEYKOV - JIRA:RMA-3816 start 
    if (bIsIE && bIsLogout == true) {
        bIsFirstUnload = true;
        bIsLogout = false;
    } else if (!bIsIE && bIsLogout == true) {
        bIsLogout = false;
    }
    if ((bIsLogout == false && !bIsFirstUnload) && !bInitiatedUnauthentication && !bChangeDatabaseClicked) {
    //// MITS 14524 MAC - Added check so dialogue does not pop up twice once the user clicks "Ok" to log out
    //if (!bIsLogout && !bInitiatedUnauthentication && !bChangeDatabaseClicked) {
    // APEYKOV - JIRA:RMA-3816 end
        bPageIsReadyForUnload = true;

        //MITS 27964 hlv 4/10/2012 begin
        //        if (iwintype == "ES") {
        //            alert(iwinES[0].closed);
        //            if (iwinES.length > 0 && !iwinES[0].closed) {
        //                sConfirmationMessage = "You are DISCARDING and closing unsaved screens; including pop-ups!";
        //            }
        //            else {
        //                sConfirmationMessage = "You are exiting RISKMASTER and DISCARDING any unsaved changes!";
        //            }
        //        }
        //        else {
        if (iwintype.toLowerCase().indexOf("comment") >= 0) {
            sConfirmationMessage = "You are DISCARDING your changes and closing Comments!";
        }
        else if (iwintype.toLowerCase().indexOf("notes") >= 0) {
            sConfirmationMessage = "You are DISCARDING your changes and closing Notes!";
        }
        else if (iwintype == "") {
            sConfirmationMessage = "You are exiting RISKMASTER and DISCARDING any unsaved changes!";
        }
        else {
            sConfirmationMessage = "You are DISCARDING and closing unsaved screens; including pop-ups!";
        }
        //}
        //MITS 27964 hlv 4/10/2012 end
        return sConfirmationMessage;
    }
    // APEYKOV - JIRA:RMA-3816 start
    //else {
    //	bIsLogout=false;
    //}
    bIsFirstUnload = false;
    // APEYKOV - JIRA:RMA-3816 end
}

// APEYKOV - JIRA:RMA-3816
function getInternetExplorerVersion()
{
    var rv = -1; // Return value assumes failure.
    if (navigator.appName == 'Microsoft Internet Explorer') {
        var ua = navigator.userAgent;
        var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
        if (re.exec(ua) != null)
            rv = parseFloat(RegExp.$1);
    }
    return rv;
}

// MITS 14524 MAC
function UnAuthenticate() {
    
    //MITS 16226: Raman
    if (bPageIsReadyForUnload && !bChangeDatabaseClicked)
	{
    	bInitiatedUnauthentication = true;
		// MITS 14524 MAC 
    	//ExpireAuthenticationCookie();
    	SessionLogOut(); //MITS 30545, bsharma33
		CloseSMS();
	}
}

// MITS 14524 MAC
function ExpireAuthenticationCookie()
{
	var cookie_date = new Date ( );  // current date & time
	cookie_date.setTime ( cookie_date.getTime() - 1 );
	document.cookie = ".RMXAUTH" + "=; expires=Thu, 01-Jan-1970 00:00:01 GMT";
}

// MITS 14524 MAC - Opens a window with name 'SecurityMgtSystem'.  
// If the SMS window is opened, then it will not open a new window but
// grab control of that one, then it immediately closes it.  If the SMS window
// is NOT opened, then a small blank window will open and close instantly
function CloseSMS() 
{
    var SMSWindow = window.open("about:blank","SecurityMgtSystem","menubar=no,toolbar=no,scrollbars=no,width=5,height=5,resizable=yes");
    SMSWindow.close();
}

// MITS 16414 MAC - References a hidden variable created in the code behind for each tab,
// grabs the URL from the hidden variable and sets it to the tab as it is clicked.
function uwtPortalTab_Click(oWebTab, oTab, oEvent)
{
	var sTabKey = "h_Tab" + oTab.index;
	var sTabHidden = document.getElementById(sTabKey);
	oTab.setTargetUrl(sTabHidden.value);

}
//MITS 30545, bsharma33
function SessionLogOut() {
    //debugger;
    var xmlhttp;
    if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    }
    else {
    // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function () {
        
        //Write code for execution after logout here.
    }

    xmlhttp.open("POST", "AsyncrhonousCallBack.aspx", true);
    xmlhttp.send();


}
