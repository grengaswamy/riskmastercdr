﻿/**********************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                    *
 **********************************************************************************************
 * 11/08/2014 | 34257/ RMA-429  | ajohari2   | Default Claim Type by Policy LOB (if only single claim type is there for that LOB)
 * 09/02/2014 | RMA-88          | achouhan3  | CSC SSO Implementation
 **********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Riskmaster.AppHelpers;
using Riskmaster.BusinessAdaptor.Common;
//using Riskmaster.UI.RMAuthentication;
using Riskmaster.UI.App_Master;
using System.Collections.Specialized;
using System.Collections;
using Riskmaster.RMXResourceManager;
using System.Xml.Linq; //MITS:34257 - JIRA RMA-429 ajohari2
using System.Xml;
using Riskmaster.Models;
using System.Web;
using System.IO;
using Riskmaster.Cache; //MITS:34257 - JIRA RMA-429 ajohari2
using System.Configuration;
using Riskmaster.Cache;
using Riskmaster.Security.Encryption;

namespace Riskmaster.RMXPortal
{
    public partial class _Default : System.Web.UI.Page
    {
        //private AuthenticationServiceClient authService = new AuthenticationServiceClient();
        //RMA-88 CSC SSO Implementation Starts
        private bool m_bUseSSO = false;
        //RMA-88 CSC SSO Implementation Ends
        /// <summary>
        /// Event to handle the Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //Ashish Ahuja Mits 30873 start
            hdnSessionTimeout.Value = AppHelper.GetAuthenticationTimeout().ToString();
            hdnAlertTime.Value = System.Configuration.ConfigurationManager.AppSettings["AlertTime"].ToString();
            //Ashish Ahuja Mits 30873 End
            if (!Page.IsPostBack)
            {

                if (Request.QueryString["src"] != null)
                {
                    LoadRMALogo();
                    return;
                }
                //Ashish Ahuja : Mits 30873 
                Page.ClientScript.RegisterStartupScript(this.GetType(), "onLoad", "DisplaySessionTimeout();", true);
                //Deb ML changes
                string sResources = JavaScriptResourceHandler.GlobalProcessRequest(this.Context, "CommonValidations");
                ClientScript.RegisterStartupScript(this.GetType(), "ResourcesScript", sResources, true);
                //Deb ML changes

                //MITS:34257 - JIRA RMA-429 ajohari2 Start
                string sCWSresponse = AppHelper.CallCWSService(GetMessageTemplateBESStatus().ToString());

                XmlDocument oFDMPageDom = new XmlDocument();
                oFDMPageDom.LoadXml(sCWSresponse);
                XmlElement xmlMultiCovgPerClm = null;
                xmlMultiCovgPerClm = (XmlElement)oFDMPageDom.SelectSingleNode("/ResultMessage/Document/BESStatusInfo/MultiCovgPerClm");
                if (xmlMultiCovgPerClm != null)
                {
                    hdnMultiCovgPerClm.Value = xmlMultiCovgPerClm.InnerText.Trim();
                }
                //MITS:34257 - JIRA RMA-429 ajohari2 End

                string sBackgroundColor = string.Empty;
                string sLoginLabelColor = string.Empty;
                string sLoginValueColor = string.Empty;
                string sLogoutFontColor = string.Empty;
                //Debabrata Biswas 05/25/2010. Added to show lss link. Single Sign On MITS# MITS 21124 
                bool bShowRMXLSSSingleSignOnLink = false;
                string bShowRMXVSSSingleSignOnLink = string.Empty;//averma62 01/09/2013. Added to show VSS link on rmA home page for single sign on
                string bShowRMXFASSingleSignOnLink = string.Empty; //averma62 MITS 32386 rmA - FAS Integration
                // Modify master page body tag to use the Portal css class
                RMXMain MP = (RMXMain)Page.Master;
                MP.BodyControl.Attributes.Add("class", "PortalBody");
                MP.BodyControl.Attributes.Add("onunload", "UnAuthenticate();");

                //MP.BodyControl.Attributes.Add("onResize", "adjustMDIMeter();"); asharma326 MITS 34425

                MP.HTMLControl.Attributes.Add("class", "PortalBody");

                sBackgroundColor = RMXPortalHelper.GetBannerColorAsHTML();
                sLoginLabelColor = RMXPortalHelper.GetLoginLabelFontColorAsHTML();
                sLoginValueColor = RMXPortalHelper.GetLoginValueFontColorAsHTML();
                sLogoutFontColor = RMXPortalHelper.GetLogoutFontColorAsHTML();
                //Debabrata Biswas 05/25/2010. Added to show lss link. Single Sign On MITS# MITS 21124 
                bShowRMXLSSSingleSignOnLink = RMXPortalHelper.ShowRMXLSSSingleSignOnLink();
                bShowRMXVSSSingleSignOnLink = AppHelper.ReadCookieValue("EnableVSS");   //averma62 01/09/2013. Added to show VSS link on rmA home page for single sign on
                bShowRMXFASSingleSignOnLink = AppHelper.ReadCookieValue("EnableFAS");
                banner.Style.Add("background-color", sBackgroundColor);
                loginInfoInner.Style.Add("background-color", sBackgroundColor);
                loginInfoOuter.Style.Add("background-color", sBackgroundColor);
                loginUserValue.Style.Add("color", sLoginValueColor);
                loginDSValue.Style.Add("color", sLoginValueColor);
                loginViewValue.Style.Add("color", sLoginValueColor);
                loginInfoInner.Style.Add("color", sLoginLabelColor);
                Loginstatus1.Style.Add("color", sLogoutFontColor);

                Loginstatus1.Attributes.Add("onclick", "bIsLogout=true;");

                //Start Debabrata Biswas 05/25/2010. Added to show lss link
                ////Debabrata Biswas 05/25/2010. Added to show lss link. Single Sign On MITS# MITS 21124 
                if (bShowRMXLSSSingleSignOnLink)
                {
                    RMXLSSSingleSignOnHyperLink.Visible = true;
                    RMXLSSSingleSignOnHyperLink.Style.Add("color", sLogoutFontColor);
                }
                //Start : averma62 01/09/2013. Added to show VSS link on rmA home page for single sign on
                if (bShowRMXVSSSingleSignOnLink.Equals("-1"))
                {
                    RMXVSSSingleSignOnHyperLink.Visible = true;
                    RMXVSSSingleSignOnHyperLink.Style.Add("color", sLogoutFontColor);
                }
                //End : averma62 01/09/2013. Added to show VSS link on rmA home page for single sign on

                //Start : averma62 MITS 32386 - rmA FAS Integration
                if (bShowRMXFASSingleSignOnLink.Equals("-1"))
                {
                    RMXFASSingleSignOnHyperLink.Visible = true;
                    RMXFASSingleSignOnHyperLink.Style.Add("color", sLogoutFontColor);
                }
                //End : averma62 MITS 32386 - rmA FAS Integration
                //banner.Style.Add("background-image", "../Images/" + AppHelper.ClientId + "_rmx_logo.gif");
                banner.Style.Add("background-image", "Default.aspx?src=rmx_logo.gif");
            }
            //End Debabrata Biswas 05/25/2010. Added to show lss link
            if (!Page.IsPostBack)
            {
                VerifyUserInfoCookie();

                Dictionary<int, String> dictPortalTabs = new Dictionary<int, String>();

                // Build the portal
                RMXPortalHelper.BuildPortal(ref uwtPortal, ref dictPortalTabs);

                // MITS 16414 MAC - Adds each id and URL to a dictionary, later used to create hidden
                // variables in the main Portal page.  These hidden variables will then reference
                // each tabs destination when clicked
                var oTabs = from pt in dictPortalTabs
                            select new 
                            {
                                id = pt.Key,
                                url = pt.Value
                            };
                StringBuilder sbHiddenVar = new StringBuilder();
                foreach (var tab in oTabs)
                {
                    sbHiddenVar.Append("<input type='hidden' id='h_Tab");//RMA-13498
                    sbHiddenVar.Append(tab.id.ToString());
                    sbHiddenVar.Append("' value='");
                    sbHiddenVar.Append(tab.url.ToString());
                    sbHiddenVar.Append("' />");
                    Page.Controls.Add(new HtmlGenericControl(sbHiddenVar.ToString()));
                    sbHiddenVar = new StringBuilder();
                } // foreach
                //Deb ML changes
                //string sResources = JavaScriptResourceHandler.GlobalProcessRequest(this.Context, "CommonValidations");
                //ClientScript.RegisterStartupScript(this.GetType(), "ResourcesScript", sResources, true);
                //Deb ML changes
            }
        }
        /// <summary>
        /// Load Image
        /// </summary>
        private void LoadRMALogo()
        {
            try
            {
                byte[] filecontent = RMXPortalHelper.PortalHomeImage;
                if (filecontent == null)
                {
                    //banner.Style.Add("background-image", "../Images/rmx_logo.gif");
                    string cacheimageKey = "RMXPortalImage";
                    FileStream fs = new FileStream(AppDomain.CurrentDomain.BaseDirectory + @"\Images\rmx_logo.gif", FileMode.Open, FileAccess.Read);
                    BinaryReader br = new BinaryReader(fs);
                    filecontent = br.ReadBytes((int)fs.Length);
                    CacheCommonFunctions.UpdateValue2Cache<byte[]>(cacheimageKey, AppHelper.ClientId, filecontent);
                }
                Response.Buffer = true;
                Response.Charset = "";
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.ContentType = "images/jpeg";
                Response.AddHeader("content-disposition", "attachment;filename=imagelogodef");
                Response.BinaryWrite(filecontent);
                Response.Flush();
                Response.End();
            }
            catch
            {
            }
        }
        /// <summary>
        /// Verifies that the UserInfo cookie exists
        /// </summary>
        private void VerifyUserInfoCookie()
        {
            //NameValueCollection nvCookieColl = AppHelper.ReadCookieInfo();

            //IEnumerator myEnumerator = nvCookieColl.GetEnumerator();


            //foreach (string nvCollKey in nvCookieColl.AllKeys)
            //{
            //    string strCookieValue = AppHelper.ReadCookieValue(nvCollKey);

            //    if (string.IsNullOrEmpty(strCookieValue))
            //    {
            //        Response.Write("Cookie could not be set: " + nvCollKey);
            //        Response.End();
            //    } // if 
            //    else
            //    {
            //        Response.Write("Cookie key is: " + nvCollKey);
            //        Response.Write("Cookie value is: " + strCookieValue);

            //    } // else

            //}

            //Response.End();

            //Populate a couple status fields
            if (!string.IsNullOrEmpty(AppHelper.ReadCookieValue("DsnName")))
            {
                lblDSN.Text = AppHelper.ReadCookieValue("DsnName");
                lblViewName.Text = AppHelper.ReadCookieValue("ViewName").Replace("^@", "&");  //Aman MITS 27767;
            } // if
            else
            {

                StringBuilder strCookieErrMessage = new StringBuilder();
                strCookieErrMessage.AppendLine("Cookie information could not be read.");
                strCookieErrMessage.AppendLine("Please verify your browser Security and Privacy settings.");

                throw new ApplicationException(strCookieErrMessage.ToString());
            } // else
        }


        /// <summary>
        /// Logs out a user and removes their corresponding information
        /// such as session information, cookie information and Forms Authentication tokens/tickets
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Loginstatus1_LoggingOut(object sender, LoginCancelEventArgs e)
        {
            //Open the call to the Service
            //authService.Open();

            string strSessionID = AppHelper.ReadCookieValue("SessionId");
            //authService.LogOutUser(strSessionID);
            //Log out the user
            //authService.LogOutUserAsync(strSessionID);
            AuthData oAuthData = new AuthData();
            oAuthData.ClientId = Convert.ToInt32(AppHelper.ClientId);
            oAuthData.Token = strSessionID;
            AppHelper.GetAsyncResponse("RMService/Authenticate/logout", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oAuthData);
            //Remove the user's cookie
            AppHelper.RemoveCookie();
             
            //Change the redirection to the Logout Url 
            if (AppHelper.ClientId != 0)
            {
                Loginstatus1.LogoutPageUrl = "~/UI/Home/Login.aspx?" + RMCryptography.EncryptString(AppHelper.ClientId.ToString());//Riskmaster.AppHelpers.ConfigHelper.GetLogoutPageUrl();
                Loginstatus1.LogoutAction = LogoutAction.Redirect;
                FormsAuthentication.SignOut();
            }
            else
            {
                //PSARIN2 Start
                //Change the redirection to the Logout Url configured in the web.config file
                //Loginstatus1.LogoutPageUrl = Riskmaster.AppHelpers.ConfigHelper.GetLogoutPageUrl();
                //Loginstatus1.LogoutAction = LogoutAction.Redirect;
                //PSARIN2 End
                //Remove the Forms authentication ticket
                //RMA-88  SSO      achouhan3       Redirect to logout page rather than rmA login page in case of SSO enable
                Boolean.TryParse(ConfigurationManager.AppSettings["UseSSO"] ?? "false", out m_bUseSSO);
                if (m_bUseSSO)
                {
                    Loginstatus1.LogoutPageUrl = ResolveUrl("~/UI/Home/Logout.aspx");
                    Loginstatus1.LogoutAction = LogoutAction.Redirect;
                }
                else
                    FormsAuthentication.SignOut();


            }

        }//event: Loginstatus1_LoggingOut

        /// <summary>
        /// Handles the Unload event of the page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Unload(object sender, EventArgs e)
        {
            //Ensure that the call to the WCF service is closed upon termination of the page
            //authService.Close();

            //Clean up
            //if (authService != null)
            //{
            //    authService = null;
            //} // if

        }//event: Page_Unload

        //MITS:34257 - JIRA RMA-429 ajohari2 Start
        /// <summary>
        /// This used to generate a xml format
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplateBESStatus()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function>SetUpPolicySystemAdaptor.GetBESStatus</Function></Call>");
            sXml = sXml.Append("<Document>");
            sXml = sXml.Append("<BESStatusInfo>");
            sXml = sXml.Append("<BESStatus>");
            sXml = sXml.Append("</BESStatus>");
            sXml = sXml.Append("</BESStatusInfo>");
            sXml = sXml.Append("</Document>");
            sXml = sXml.Append("</Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }
        //MITS:34257 - JIRA RMA-429 ajohari2 End

    }
}
