﻿using System;
using System.Collections.Generic;
using System.Resources;
using System.Collections.Specialized;

namespace Riskmaster.RMXResourceManager
{
    /// <summary>
    /// Created By: Deb Jena
    /// Date:04/24/2012
    /// </summary>
    public class RMXResourceReader : DisposableBase, IResourceReader, IEnumerable<KeyValuePair<string, object>>
    {
        private ListDictionary lDic;
        public RMXResourceReader(ListDictionary dic)
        {
            this.lDic = dic;
        }

        public void Close()
        {
            this.Dispose();
        }
        public System.Collections.IDictionaryEnumerator GetEnumerator()
        {
            if (Disposed)
                throw new ObjectDisposedException("object is already disposed.");

            return this.lDic.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            if (Disposed)
                throw new ObjectDisposedException("object is already disposed.");
            return this.lDic.GetEnumerator();
        }

        protected override void Cleanup()
        {
            try
            {
                this.lDic = null;
            }
            finally
            {
                base.Cleanup();
            }
        }

        IEnumerator<KeyValuePair<string, object>> IEnumerable<KeyValuePair<string, object>>.GetEnumerator()
        {
            if (Disposed)
                throw new ObjectDisposedException("object is already disposed.");

            return this.lDic.GetEnumerator() as IEnumerator<KeyValuePair<string, object>>;
        }
    }

}
