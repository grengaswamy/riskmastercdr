﻿using System.Web.Compilation;

namespace Riskmaster.RMXResourceManager
{
    /// <summary>
    /// Created By: Deb Jena
    /// Date:04/24/2012
    /// </summary>
    public class RMXResourceProviderFactory : ResourceProviderFactory
    {
        public override IResourceProvider CreateGlobalResourceProvider(string classKey)
        {
            return new RMXResourceProvider(classKey);
        }

        public override IResourceProvider CreateLocalResourceProvider(string virtualPath)
        {
            string classKey = virtualPath;
            if (!string.IsNullOrEmpty(virtualPath))
            {
                virtualPath = virtualPath.Remove(0, 1);
                //classKey = virtualPath.Remove(0, virtualPath.IndexOf('/') + 1);
                classKey = virtualPath.Substring(virtualPath.LastIndexOf('/') + 1);
            }
            return new RMXResourceProvider(classKey);
        }
    }
}
