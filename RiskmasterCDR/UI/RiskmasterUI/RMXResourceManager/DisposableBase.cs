﻿using System;

namespace Riskmaster.RMXResourceManager
{
    /// <summary>
    /// Created By: Deb Jena
    /// Date:04/24/2012
    /// </summary>
    public class DisposableBase : IDisposable
    {
        private bool m_Disposed;
        protected bool Disposed
        {
            get
            {
                lock (this)
                {
                    return m_Disposed;
                }
            }
        }
        public void Dispose()
        {
            lock (this)
            {
                if (m_Disposed == false)
                {
                    Cleanup();
                    m_Disposed = true;

                    GC.SuppressFinalize(this);
                }
            }
        }
        protected virtual void Cleanup()
        {
        }
        //~DisposableBase()
        //{
        //    Cleanup();
        //}
    }
}
