//Author: Brian Battah 3/6/02
//Note: Dependent on form.js
var frmData = document.frmData; //BSB Hack for NS7

//Called to populate the Selected Entities box. 
function selectReportTarget() {
	if(document.frmData.byoshaestablishmentflag[0].checked)
		lookupData('selectedentities', 'OSHA_ESTABLISHMENT' /*sTableId*/, 4 /*sViewId*/, 'selectedentities', 3/*lookupType*/);
	else
		selectCodeLevel('orghlevel','selectedentities',String(document.frmData.reportlevel.options[document.frmData.reportlevel.selectedIndex].text).toUpperCase());
}

//Called when the By Establishment flag is changed. - Resets and locks out screen inputs as needed.
function byEstablishmentFlagChanged()
{
	
	var bByEst = document.frmData.byoshaestablishmentflag[0].checked;
	
	//Always Clear the selectedentities collection
	document.frmData.selectedentities.options.length = 0;
	document.frmData.selectedentities_lst.value = "";
	
	if(bByEst)
	{
		//Default & Disable unused GUI Controls
		document.frmData.usereportlevel.disabled=true;
		document.frmData.reportlevel.disabled=true;
		if(eval("document.frmData.establishmentnameprefix") != null) 
			document.frmData.establishmentnameprefix.disabled=true;
		//Swap Label "Entities" to "Establishments"
		document.frmData.allentitiesflag[0].nextSibling.nodeValue = "Use All OSHA Establisments";
		document.frmData.allentitiesflag[1].nextSibling.nodeValue = "Use Selected OSHA Establisments";
		if(document.frmData.selectedentities.parentNode.previousSibling.childNodes[0])
			document.frmData.selectedentities.parentNode.previousSibling.childNodes[0].nodeValue = "Selected OSHA Establisments";
		else //Netscape 7 code.
		{
			var p = document.frmData.selectedentities.parentNode.parentNode;
			p.deleteCell(0);
			elt = p.insertCell(0);
			elt.appendChild(document.createTextNode("Selected OSHA Establisments"));
		}
		
	}
	else
	{
		//Default & Disable unused GUI Controls
		document.frmData.usereportlevel.disabled=false;
		document.frmData.reportlevel.disabled=false;
		if(eval("document.frmData.establishmentnameprefix") != null) 
			document.frmData.establishmentnameprefix.disabled=false;
		//Swap Label "Entities" to "Establishments"
		document.frmData.allentitiesflag[0].nextSibling.nodeValue = "Use All Entities";
		document.frmData.allentitiesflag[1].nextSibling.nodeValue = "Use Selected Entities";
		if(document.frmData.selectedentities.parentNode.previousSibling.childNodes[0])
			document.frmData.selectedentities.parentNode.previousSibling.childNodes[0].nodeValue = "Selected Entities";
		else //Netscape 7 code.
		{
			var p = document.frmData.selectedentities.parentNode.parentNode;
			p.deleteCell(0);
			elt = p.insertCell(0);
			elt.appendChild(document.createTextNode("Selected Entities"));
		}
	}
}
function ValidateCriteria()
{
	var UseReportLevel;
	var ReportLevel;
	//Ensure that UseReport is higher or equal to ReportOn
	UseReportLevel =  frmData.usereportlevel.options[frmData.usereportlevel.selectedIndex].value;
	ReportLevel =  frmData.reportlevel.options[frmData.reportlevel.selectedIndex].value;
	//alert("arrived in Validate\n reportlevel:" + ReportLevel + "\nUseReportLevel:" + UseReportLevel );
	if (UseReportLevel > ReportLevel)
	{
		alert("The organizational level at which these reports are to be used must be at or above the organization level selected to report on.");
		return false;
	}
	return true;
	//Ensure valid dates provided if range selected
	
	//Ensure at least one entity is selected if selectedentities is true
	
}

function reportlevelChanged()
{
	//Reset the Selected Entities since they
	//must all come from the same (new) level.
	frmData.selectedentities.options.length = 0;
	frmData.selectedentities_lst.value="";
	return true;
}

function lookupFilteredOrgH(sTableId, sViewId, sFieldMark, lookupType)
{
	if(m_codeWindow!=null)
		m_codeWindow.close();
	m_sFieldName=sFieldMark;
	m_LookupType=lookupType;
	if(sTableId=="0" || sTableId=="")
		sTableId="-1";
	m_codeWindow=window.open('searchpopup.asp?viewid='+sViewId+'&tableid='+sTableId,'searchWnd',
		'width=550,height=450'+',top='+(screen.availHeight-450)/2+',left='+(screen.availWidth-550)/2+',resizable=yes,scrollbars=yes');
	if(lookupType>=6 && lookupType<=8)
		self.lookupCallback="numLookupCallback";
	else if (lookupType==10)
		self.lookupCallback="VehicleLookupCallback";
	else if (lookupType==11)
		self.lookupCallback="EventLookupCallback";
	else
		self.lookupCallback="entitySelected";	
		
	return false;
}
