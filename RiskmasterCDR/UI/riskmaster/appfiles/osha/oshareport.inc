<script language="VBScript" runat="server">

'**********************************************
' Begin Custom Report Queuing Code
' The following functions are provided to 
' integrate with the CriteriaEdit.asp page
' supporting custom report queuing.
' Author: Brian Battah 
' 3/8/02
'**********************************************

Function GenerateOSHACriteriaHTML()
	Dim  objResult, objNode, objFrm, objOpt, i, lReportType
	
	lReportType = GetReportType(CLng(Request("reportid")))

	Set objResult = CreateObject("Microsoft.XMLDOM")
	
	Set objXML = CreateObject("Microsoft.XMLDOM")
	objXML.load GetReportTemplateFile() 'Pull a blank template	
	
	Set objXSL = CreateObject("Microsoft.XMLDOM")
	objXSL.load Application(APP_DATAPATH) & "/oshareport.xsl" 'Pull a blank template	
	
	CleanXMLDefaults objXML

	Response.ContentType = "text/html"
	'****************************************
	' Apply standard form XSL against criteria
	'*****************************************
	objXML.transformNodeToObject objXSL.documentElement,objResult

	'*****************************************************
	' Make some final adjustments to XSL output
	' 1.) Add the event handlers
	' 2.) Add the year selections
	' 3.) Pick some friendly defaults
	'*****************************************************
	Set objFrm=objResult.getElementsByTagName("div").Item(0)
	
	'By Establishment Flag Validate Handler (Clear the selected entities if changed) 'BSB 09.25.2003
	Set objNode=objResult.selectSingleNode("//input[@name = 'byoshaestablishmentflag' && @value='false']")
	If Not objNode is Nothing Then objNode.setAttribute "onClick", "byEstablishmentFlagChanged();"
	
	Set objNode=objResult.selectSingleNode("//input[@name = 'byoshaestablishmentflag' && @value='true']")
	If Not objNode is Nothing Then objNode.setAttribute "onClick", "byEstablishmentFlagChanged();"
	

	'ReportOn Validate Handler (Clear the selected entities if changed)
	Set objNode=objResult.selectSingleNode("//select[@name = 'reportlevel']")
	objNode.setAttribute "onClick", "reportlevelChanged();"

	'Orgh Code Lookup by Level (Add indirection layer to onClick so that we can divert to an OSHA Establishment List if needed.)
	Set objNode=objResult.selectSingleNode("//input[@name = 'selectedentitiesbtn']")
	objNode.setAttribute "onClick", "selectReportTarget()"'"selectCodeLevel('orghlevel','selectedentities',String(document.frmData.reportlevel.options(document.frmData.reportlevel.selectedIndex).text).toUpperCase());"

	'Add Year of Report options
	Set objNode=objResult.selectSingleNode("//select[@name = 'yearofreport']")
	i = 2002
	
	Dim iSelected 'Pick up the previously entered year (current as default).
	If not IsEmpty(objNode.getAttribute("codeid")) and objNode.getAttribute("codeid") <> "" then iSelected = clng(objNode.getAttribute("codeid"))
	If not IsNumeric(iSelected) or iSelected = 0 Then iSelected = Year(Now)
		
	for i = 2002 to Year(Now)
		Set objOpt = objResult.createElement("option")
		objOpt.setAttribute "value", cstr(i)
		if i = iSelected then objOpt.setAttribute "selected", ""
		objOpt.text = cstr(i)
		objNode.appendChild objOpt
	next

	'Set up some good Default Selections

	'Clear the "By Establishment Flag"  'BSB 09.25.2003
	Set objNode=objResult.selectSingleNode("//input[@name = 'byoshaestablishmentflag' && @value='false']")
	If Not objNode is Nothing Then objNode.setAttribute "checked", ""

	
	'Set "ReportType"  to OSHA300
	'Note: The "//report" node has been XSL traslated away...
	'Set objNode = objResult.selectSingleNode("//report")
	'objNode.setAttribute "type","1"
	Set objNode = objResult.selectSingleNode("//input[@name = 'reporttype']")
	objNode.setAttribute "value",CStr(lReportType)
	
	'Set to "AllEntities"
	Set objNode = objResult.selectSingleNode("//input[@name = 'allentitiesflag' && @value = 'true']")
	objNode.setAttribute "checked" ,"" 
	
	'Set to "UseYear" 
	Set objNode = objResult.selectSingleNode("//input[@name = 'datemethod' && @value = 'useyear']")
    objNode.setAttribute "checked", ""

    'Set to "ShowSoftErrors"
    Set objNode = objResult.selectSingleNode("//input[@name = 'printsofterrlog']")
    objNode.setAttribute "checked", ""

    GenerateOSHACriteriaHTML = objResult.xml
    End Function

    Function GenerateOSHACriteriaHTMLForAdmin()
    Dim  objResult, objNode, objFrm, objOpt, i, lReportType

    lReportType = GetReportTypeForAdmin(CLng(Request("reportid")))

    Set objResult = CreateObject("Microsoft.XMLDOM")

    Set objXML = CreateObject("Microsoft.XMLDOM")
    objXML.load GetReportTemplateFileForAdmin() 'Pull a blank template

    Set objXSL = CreateObject("Microsoft.XMLDOM")
    objXSL.load Application(APP_DATAPATH) & "/oshareport.xsl" 'Pull a blank template

    CleanXMLDefaults objXML

    Response.ContentType = "text/html"
    '****************************************
    ' Apply standard form XSL against criteria
    '*****************************************
    objXML.transformNodeToObject objXSL.documentElement,objResult

    '*****************************************************
    ' Make some final adjustments to XSL output
    ' 1.) Add the event handlers
    ' 2.) Add the year selections
    ' 3.) Pick some friendly defaults
    '*****************************************************
    Set objFrm=objResult.getElementsByTagName("div").Item(0)

    'By Establishment Flag Validate Handler (Clear the selected entities if changed) 'BSB 09.25.2003
    Set objNode=objResult.selectSingleNode("//input[@name = 'byoshaestablishmentflag' && @value='false']")
    If Not objNode is Nothing Then objNode.setAttribute "onClick", "byEstablishmentFlagChanged();"

    Set objNode=objResult.selectSingleNode("//input[@name = 'byoshaestablishmentflag' && @value='true']")
    If Not objNode is Nothing Then objNode.setAttribute "onClick", "byEstablishmentFlagChanged();"


    'ReportOn Validate Handler (Clear the selected entities if changed)
    Set objNode=objResult.selectSingleNode("//select[@name = 'reportlevel']")
    objNode.setAttribute "onClick", "reportlevelChanged();"

    'Orgh Code Lookup by Level (Add indirection layer to onClick so that we can divert to an OSHA Establishment List if needed.)
    Set objNode=objResult.selectSingleNode("//input[@name = 'selectedentitiesbtn']")
    objNode.setAttribute "onClick", "selectReportTarget()"'"selectCodeLevel('orghlevel','selectedentities',String(document.frmData.reportlevel.options(document.frmData.reportlevel.selectedIndex).text).toUpperCase());"

    'Add Year of Report options
    Set objNode=objResult.selectSingleNode("//select[@name = 'yearofreport']")
    i = 2002

    Dim iSelected 'Pick up the previously entered year (current as default).
    If not IsEmpty(objNode.getAttribute("codeid")) and objNode.getAttribute("codeid") <> "" then iSelected = clng(objNode.getAttribute("codeid"))
    If not IsNumeric(iSelected) or iSelected = 0 Then iSelected = Year(Now)

    for i = 2002 to Year(Now)
    Set objOpt = objResult.createElement("option")
    objOpt.setAttribute "value", cstr(i)
    if i = iSelected then objOpt.setAttribute "selected", ""
    objOpt.text = cstr(i)
    objNode.appendChild objOpt
    next

    'Set up some good Default Selections

    'Clear the "By Establishment Flag"  'BSB 09.25.2003
    Set objNode=objResult.selectSingleNode("//input[@name = 'byoshaestablishmentflag' && @value='false']")
    If Not objNode is Nothing Then objNode.setAttribute "checked", ""


    'Set "ReportType"  to OSHA300
    'Note: The "//report" node has been XSL traslated away...
    'Set objNode = objResult.selectSingleNode("//report")
    'objNode.setAttribute "type","1"
    Set objNode = objResult.selectSingleNode("//input[@name = 'reporttype']")
    objNode.setAttribute "value",CStr(lReportType)

    'Set to "AllEntities"
    Set objNode = objResult.selectSingleNode("//input[@name = 'allentitiesflag' && @value = 'true']")
    objNode.setAttribute "checked" ,""

    'Set to "UseYear"
    Set objNode = objResult.selectSingleNode("//input[@name = 'datemethod' && @value = 'useyear']")
      objNode.setAttribute "checked", ""

      'Set to "ShowSoftErrors"
      Set objNode = objResult.selectSingleNode("//input[@name = 'printsofterrlog']")
      objNode.setAttribute "checked", ""

      GenerateOSHACriteriaHTMLForAdmin = objResult.xml
      End Function

      '********************************
      ' Test the Criteria by applying
      ' them to the Engine and looking for errors.
      '********************************
      Function ApplyOSHACriteria(ByRef sXML,ByRef errorHTML)

      Dim e, objOshaMgr,pEngine,objXML
      ApplyOSHACriteria = false
      Set objOshaMgr = CreateObject("OSHALib.COSHAManager")
      objOshaMgr.DSN = objSessionStr(SESSION_DSN)
      objOshaMgr.Init
      Set pEngine = objOshaMgr.GetIReportEngine
      Set objXML = CreateObject("Microsoft.XMLDOM")
      objXML.load GetReportTemplateFile() 'Pull a blank template
      RequestToXML objXML 				'Fill the template
      On Error Resume Next
      Set pEngine.CriteriaXMLDom = objXML 'Apply the filled template
      sXML = pEngine.CriteriaXMLDom.xml
      On Error Goto 0
      For each e in objOshaMgr.Errors
      errorHTML = errorHTML & "<li>" & e.Source & " : " & e.Description & "</li>"
      Next
      If objOshaMgr.Errors.Count = 0 Then ApplyOSHACriteria = true
      End Function

      Function ApplyOSHACriteriaForAdmin(ByRef sXML,ByRef errorHTML)

      Dim e, objOshaMgr,pEngine,objXML
      ApplyOSHACriteriaForAdmin = false
      Set objOshaMgr = CreateObject("OSHALib.COSHAManager")
      objOshaMgr.DSN = objSessionStr(SESSION_DSN)
      objOshaMgr.Init
      Set pEngine = objOshaMgr.GetIReportEngine
      Set objXML = CreateObject("Microsoft.XMLDOM")
      objXML.load GetReportTemplateFileForAdmin() 'Pull a blank template
      RequestToXML objXML 				'Fill the template
      On Error Resume Next
      Set pEngine.CriteriaXMLDom = objXML 'Apply the filled template
      sXML = pEngine.CriteriaXMLDom.xml
      On Error Goto 0
      For each e in objOshaMgr.Errors
      errorHTML = errorHTML & "<li>" & e.Source & " : " & e.Description & "</li>"
      Next
      If objOshaMgr.Errors.Count = 0 Then ApplyOSHACriteriaForAdmin = true
      End Function

      Public Function GetReportTemplateFileForAdmin()
      Dim lReportId, lReportType
      If not IsEmpty(Request("reportid")) then
      lReportId = clng(Request("reportid") & "")
      End If

      lReportType = GetReportTypeForAdmin(lReportId)
      Select Case lReportType
      Case 1 'OSHA 300
      GetReportTemplateFileForAdmin = Application(APP_DATAPATH) & "/oshareport.xml"
      Case 3 'Osha 300A Summary
      GetReportTemplateFileForAdmin = Application(APP_DATAPATH) & "/osha300Areport.xml"
      Case 4 'Osha 301 Incident Report
      GetReportTemplateFileForAdmin = Application(APP_DATAPATH) & "/osha301report.xml"
      Case 5 'Osha Sharps Log Report
      GetReportTemplateFileForAdmin = Application(APP_DATAPATH) & "/oshasharpslog.xml"
      Case Else 'Unknown ReportType
      GetReportTemplateFileForAdmin = ""
      End Select
      End Function

Public Function GetReportTemplateFile()
    Dim lReportId, lReportType
    If not IsEmpty(Request("reportid")) then
        lReportId = clng(Request("reportid") & "")
    End If

	lReportType = GetReportType(lReportId)
	Select Case lReportType
		Case 1 'OSHA 300
			GetReportTemplateFile = Application(APP_DATAPATH) & "/oshareport.xml"
		Case 3 'Osha 300A Summary
			GetReportTemplateFile = Application(APP_DATAPATH) & "/osha300Areport.xml"
		Case 4 'Osha 301 Incident Report
			GetReportTemplateFile = Application(APP_DATAPATH) & "/osha301report.xml"
		Case 5 'Osha Sharps Log Report
			GetReportTemplateFile = Application(APP_DATAPATH) & "/oshasharpslog.xml"
		Case Else 'Unknown ReportType
			GetReportTemplateFile = ""
	End Select
End Function

</script>