<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
	<xsl:template match="/CULGroupsAndUsers">
		<form>
			<control name="SystemUserList">
				<listhead>
					<rowhead colname="LoginName">Login Name</rowhead>
					<rowhead colname="Name">Name</rowhead>
					<rowhead colname="UserType">User Type</rowhead>
				</listhead>				
				<xsl:apply-templates select="Users"/>				
			</control>
			<FirstName/>
			<LastName/>
			<UserType/>
			<OptionFlag></OptionFlag>
			<Parentform></Parentform>
			<SystemUserGroup/>
			<SystemUserGroups>
				<UserTypes>
					<xsl:apply-templates select="Groups"/>
				</UserTypes>
			</SystemUserGroups>
		</form>
	</xsl:template>
	<xsl:template match="Users">
		<!--<xsl:for-each select="users">-->
		<listrow>
			<xsl:attribute name="value"><xsl:value-of select="concat( USER_ID ,',',  FIRST_NAME,',', LAST_NAME ,',', LOGIN_NAME  ,',', EMAIL_ADDR )"/></xsl:attribute>
			<rowtext type="label" name="LoginName" title="csc">
				<xsl:attribute name="title"><xsl:value-of select="LOGIN_NAME"/></xsl:attribute>
			</rowtext>
			<rowtext type="label" name="Name" title="csc">
				<xsl:attribute name="title"><xsl:value-of select="concat( FIRST_NAME,',', LAST_NAME)"/></xsl:attribute>
			</rowtext>
			<rowtext type="label" name="User Type" title="csc">
				<xsl:attribute name="title"><xsl:value-of select="GROUP_NAME"/></xsl:attribute>
			</rowtext>			
		</listrow>
		<!--</xsl:for-each>-->
	</xsl:template>
	<xsl:template match="Groups">
		<GroupName><xsl:attribute name="groupid"><xsl:value-of select="GroupId"/></xsl:attribute>
			<xsl:value-of select="GroupName"/>
		</GroupName>
	</xsl:template>
</xsl:stylesheet>
