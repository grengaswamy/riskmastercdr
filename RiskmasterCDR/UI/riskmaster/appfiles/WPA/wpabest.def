///////////////////////////////////////////////////////////////////////////////////////////////
//
// WPA BEST PRACTICES DEFINITIONS
// Copyright (c) 1997, DORN Technology Group, Inc.
// All Rights Reserved.
//
// Author: Rob D'Oria
// Updated: 07/18/1997
//
// ROLES/RESPONSIBILITIES
// ----------------------
// Adjuster 			1
// Case Manager			2
// Claims Administrator		4
// Risk Manager			8
// Employee Health		16
//
// LINE OF BUSINESS
// ----------------------
// General Liability		1
// Workers' Compensation	2
// Vehicle Accident		4
// Short Term Disability	8
//
// TOPICS
// ----------------------
// New Claim Administration	1
// Litigation Control		2
// Case Management		3
// Regulatory Compliance	4
// Cost Control/Containment	5
// Claim Closing		6
// Ongoing Claims Management	7
// Event Management		8
// Policy Management		9
// Employee Health Management	10
//
///////////////////////////////////////////////////////////////////////////////////////////////

[30 Day Review]
 ID = 1
 DEF_ID = 1
 ROLES = 1
 LOB = 15
 TOPICS = 1
 PRIORITY=Important
 TIME_ESTIMATE=60
 BILLABLE=No
 INSTRUCTIONS=
 [Open Claims]
  FILTER_ID = 8
  DATA = ,*9,
 [30 Days from Date of Claim]
  FILTER_ID = 3
  DATA = 30

[Suit Response]
 ID = 2
 DEF_ID = 1
 ROLES = 1
 LOB = 15
 TOPICS = 2
 PRIORITY=Important
 TIME_ESTIMATE=60
 BILLABLE=No
 INSTRUCTIONS=
 [Open Claims]
  FILTER_ID = 8
  DATA = ,*9,
 [20 Days from Date of Suit]
  FILTER_ID = 16
  DATA = 20

[Carrier Notification]
 ID = 3
 DEF_ID = 1
 ROLES = 1
 LOB = 7
 TOPICS = 1
 PRIORITY=Important
 TIME_ESTIMATE=60
 BILLABLE=No
 INSTRUCTIONS=
 [Open Claims]
  FILTER_ID = 8
  DATA = ,*9,
 [7 Days from Date of Claim]
  FILTER_ID = 3
  DATA = 7

[Property Appraisals]
 ID = 4
 DEF_ID = 1
 ROLES = 1
 LOB = 4
 TOPICS = 1
 PRIORITY=Important
 TIME_ESTIMATE=60
 BILLABLE=No
 INSTRUCTIONS=
 [Open Claims]
  FILTER_ID = 8
  DATA = ,*9,
 [Vehicle Accident Claims]
  FILTER_ID = 6
  DATA = ,242,
 [10 Days from Date of Claim]
  FILTER_ID = 3
  DATA = 10
 
[Vehicle Damage Estimates]
 ID = 5
 DEF_ID = 1
 ROLES = 1
 LOB = 4
 TOPICS = 1
 PRIORITY=Important
 TIME_ESTIMATE=60
 BILLABLE=No
 INSTRUCTIONS=
 [Open Claims]
  FILTER_ID = 8
  DATA = ,*9,
 [Vehicle Accident Claims]
  FILTER_ID = 6
  DATA = ,242,
 [7 Days from Date of Claim]
  FILTER_ID = 3
  DATA = 7

[Required Workers' Compensation Notification]
 ID = 6
 DEF_ID = 1
 ROLES = 1
 LOB = 2
 TOPICS = 4
 PRIORITY=Important
 TIME_ESTIMATE=60
 BILLABLE=No
 INSTRUCTIONS=
 [Open Claims]
  FILTER_ID = 8
  DATA = ,*9,
 [Workers' Compensation Claims]
  FILTER_ID = 6
  DATA = ,243,
 [7 Days from Date of Claim]
  FILTER_ID = 3
  DATA = 7
 
[Independent Medical Exam Followup]
 ID = 7
 DEF_ID = 1
 ROLES = 2
 LOB = 10
 TOPICS = 3
 PRIORITY=Important
 TIME_ESTIMATE=60
 BILLABLE=No
 INSTRUCTIONS=
 [Open Claims]
  FILTER_ID = 8
  DATA = ,*9,
 [Workers' Compensation Claims]
  FILTER_ID = 6
  DATA = ,243,
 [14 Days from Date of Event]
  FILTER_ID = 1
  DATA = 14
  
[Lost Time Review]
 ID = 8
 DEF_ID = 1
 ROLES = 2
 LOB = 10
 TOPICS = 3
 PRIORITY=Important
 TIME_ESTIMATE=60
 BILLABLE=No
 INSTRUCTIONS=
 [Open Claims]
  FILTER_ID = 8
  DATA = ,*9,
 [Workers' Compensation Claims]
  FILTER_ID = 6
  DATA = ,243,
 [14 Days from Date Last Worked]
  FILTER_ID = 19
  DATA = 14

[Restricted Duty Review]
 ID = 9
 DEF_ID = 1
 ROLES = 2
 LOB = 10
 TOPICS = 3
 PRIORITY=Important
 TIME_ESTIMATE=60
 BILLABLE=No
 INSTRUCTIONS=
 [Open Claims]
  FILTER_ID = 8
  DATA = ,*9,
 [Workers' Compensation Claims]
  FILTER_ID = 6
  DATA = ,243,
 [14 Days from Restricted Duty Date]
  FILTER_ID = 20
  DATA = 14

[First Report of Injury]
 ID = 10
 DEF_ID = 1
 ROLES = 8
 LOB = 2
 TOPICS = 4
 PRIORITY=Important
 TIME_ESTIMATE=60
 BILLABLE=No
 INSTRUCTIONS=
 [Open Claims]
  FILTER_ID = 8
  DATA = ,*9,
 [Workers' Compensation Claims]
  FILTER_ID = 6
  DATA = ,243,
 [7 Days from Date of Event]
  FILTER_ID = 20
  DATA = 14

[Review of Claims with No Activity]
 ID = 11
 DEF_ID = 1
 ROLES = 12
 LOB = 15
 TOPICS = 7
 PRIORITY=Important
 TIME_ESTIMATE=60
 BILLABLE=No
 INSTRUCTIONS=
 [Open Claims]
  FILTER_ID = 8
  DATA = ,*9,
 [No Financial Activity for 60 Days]
  FILTER_ID = 13
  DATA = 60
 
[Review of Claims with Inadequate Reserves]
 ID = 12
 DEF_ID = 1
 ROLES = 12
 LOB = 15
 TOPICS = 7
 PRIORITY=Important
 TIME_ESTIMATE=60
 BILLABLE=No
 INSTRUCTIONS=
 [Open Claims]
  FILTER_ID = 8
  DATA = ,*9,
 [Reserve Usage of 50%]
  FILTER_ID = 14
  DATA = 50

[Review Claims with No Reserves Set]
 ID = 13
 DEF_ID = 1
 ROLES = 9
 LOB = 15
 TOPICS = 1
 PRIORITY=Important
 TIME_ESTIMATE=60
 BILLABLE=No
 INSTRUCTIONS=
 [Open Claims]
  FILTER_ID = 8
  DATA = ,*9,
 [Reserves Not Set]
  FILTER_ID = 18
  DATA = ,*,

[Checks On Hold 10 Days or More]
 ID = 14
 DEF_ID =  3
 ROLES = 8
 LOB = 7
 TOPICS = 15
 PRIORITY=Important
 TIME_ESTIMATE=60
 BILLABLE=No
 INSTRUCTIONS=
 [10 Days Since Last Update]
  FILTER_ID = 3
  DATA = 10
 [Checks With Hold Status]
  FILTER_ID = 6
  DATA = ,*478,

[Checks That Have Not Cleared in 60 Days]
 ID = 15
 DEF_ID =  3
 ROLES = 8
 LOB = 15
 TOPICS = 7
 PRIORITY=Important
 TIME_ESTIMATE=60
 BILLABLE=No
 INSTRUCTIONS=
 [60 Days from Date of Check]
  FILTER_ID = 2
  DATA = 60
 [Checks With Printed Status]
  FILTER_ID = 6
  DATA = ,1054,
 [Checks That Have Not Cleared]
  FILTER_ID = 7
  DATA = =0

[Events That Resulted in Severe Outcomes]
 ID = 16
 DEF_ID =  2
 ROLES = 8 
 LOB = 0
 TOPICS = 8
 PRIORITY=Important
 TIME_ESTIMATE=60
 BILLABLE=No
 INSTRUCTIONS=
 [Outcome Code of Severe]
  FILTER_ID = 9
  DATA = 115

[Events That Resulted in Death]
 ID = 17
 DEF_ID =  2
 ROLES = 8 
 LOB = 0
 TOPICS = 8
 PRIORITY=Important
 TIME_ESTIMATE=60
 BILLABLE=No
 INSTRUCTIONS=
 [Outcome Code of Death]
  FILTER_ID = 9
  DATA = 116

[Policy Renewal Alert]
 ID = 18
 DEF_ID = 4
 ROLES = 8
 LOB = 0
 TOPICS = 9
 PRIORITY=Important
 TIME_ESTIMATE=60
 BILLABLE=No
 INSTRUCTIONS=
 [Open Policies]
  FILTER_ID = 6
  DATA = ,*212,
 [30 Days Before Expiration Date]
  FILTER_ID = 5
  DATA = 30