CREATE OR REPLACE TRIGGER MODIFY_CLAIM_COMMENTS AFTER INSERT OR UPDATE ON CLAIM 
REFERENCING NEW AS new OLD AS old
FOR EACH ROW
DECLARE Comment_Prior CLOB;
        Comment_NEW CLOB;
        comm_comp NUMBER;
        comm_id INTEGER;
        commlength INTEGER;
BEGIN
    IF INSERTING
    THEN
        BEGIN
            SELECT DBMS_LOB.GETLENGTH(:new.COMMENTS) INTO commlength FROM dual;
            IF (commlength > 0) THEN
                BEGIN
                    SELECT NEXT_UNIQUE_ID INTO comm_id FROM GLOSSARY WHERE SYSTEM_TABLE_NAME LIKE 'COMMENTS_TEXT';
                    INSERT INTO COMMENTS_TEXT (COMMENT_ID,ATTACH_RECORDID,ATTACH_TABLE,COMMENTS,HTMLCOMMENTS) 
                        SELECT (comm_id),:new.CLAIM_ID,'CLAIM',:new.COMMENTS,:new.HTMLCOMMENTS FROM dual;
                    UPDATE GLOSSARY SET NEXT_UNIQUE_ID=(comm_id + 1) WHERE SYSTEM_TABLE_NAME LIKE 'COMMENTS_TEXT';
                END;
            END IF;
        END;
    ELSIF UPDATING
    THEN
                BEGIN
                    comm_comp := 0;

                    SELECT nvl(:old.COMMENTS, empty_clob()) INTO Comment_Prior FROM dual;
                    SELECT nvl(:new.COMMENTS, empty_clob()) INTO Comment_NEW FROM dual;
                    comm_comp := dbms_lob.compare(Comment_Prior, Comment_NEW);
                    IF (comm_comp != 0) THEN
                        BEGIN
                                 SELECT count(*) INTO comm_id  FROM COMMENTS_TEXT WHERE ATTACH_RECORDID=:new.CLAIM_ID AND ATTACH_TABLE='CLAIM';
                                 IF(comm_id > 0) THEN
                                            UPDATE COMMENTS_TEXT ct SET ct.COMMENTS = :new.COMMENTS,ct.HTMLCOMMENTS = :new.HTMLCOMMENTS WHERE CT.ATTACH_RECORDID = :new.CLAIM_ID AND CT.ATTACH_TABLE='CLAIM';
                                 ELSE 
                                        BEGIN
                                            SELECT NEXT_UNIQUE_ID INTO comm_id FROM GLOSSARY WHERE SYSTEM_TABLE_NAME LIKE 'COMMENTS_TEXT';
                                            INSERT INTO COMMENTS_TEXT (COMMENT_ID,ATTACH_RECORDID,ATTACH_TABLE,COMMENTS,HTMLCOMMENTS) 
                                                SELECT (comm_id),:new.CLAIM_ID,'CLAIM',:new.COMMENTS,:new.HTMLCOMMENTS FROM dual;
                                            UPDATE GLOSSARY SET NEXT_UNIQUE_ID=(comm_id + 1) WHERE SYSTEM_TABLE_NAME LIKE 'COMMENTS_TEXT';
                                        END;
                                END IF;
						END;
                END IF;
                END;
    END IF;
END;
go
CREATE OR REPLACE TRIGGER MODIFY_EVENT_COMMENTS AFTER INSERT OR UPDATE ON EVENT 
REFERENCING NEW AS new OLD AS old
FOR EACH ROW
DECLARE Comment_Prior CLOB;
        Comment_NEW CLOB;
        comm_comp NUMBER;
        comm_id INTEGER;
        commlength INTEGER;
BEGIN
    IF INSERTING
    THEN
        BEGIN
            SELECT DBMS_LOB.GETLENGTH(:new.COMMENTS) INTO commlength FROM dual;
            IF (commlength > 0) THEN
                BEGIN
                    SELECT NEXT_UNIQUE_ID INTO comm_id FROM GLOSSARY WHERE SYSTEM_TABLE_NAME LIKE 'COMMENTS_TEXT';
                    INSERT INTO COMMENTS_TEXT (COMMENT_ID,ATTACH_RECORDID,ATTACH_TABLE,COMMENTS,HTMLCOMMENTS) 
                        SELECT (comm_id),:new.EVENT_ID,'EVENT',:new.COMMENTS,:new.HTMLCOMMENTS FROM dual;
                    UPDATE GLOSSARY SET NEXT_UNIQUE_ID=(comm_id + 1) WHERE SYSTEM_TABLE_NAME LIKE 'COMMENTS_TEXT';
                END;
            END IF;
        END;
    ELSIF UPDATING
    THEN
                BEGIN
                    comm_comp := 0;

                    SELECT nvl(:old.COMMENTS, empty_clob()) INTO Comment_Prior FROM dual;
                    SELECT nvl(:new.COMMENTS, empty_clob()) INTO Comment_NEW FROM dual;
                    comm_comp := dbms_lob.compare(Comment_Prior, Comment_NEW);
                    IF (comm_comp != 0) THEN
                        BEGIN
                            SELECT count(*) INTO comm_id  FROM COMMENTS_TEXT WHERE ATTACH_RECORDID=:new.EVENT_ID AND ATTACH_TABLE='EVENT';
                                 IF(comm_id > 0) THEN
                                            UPDATE COMMENTS_TEXT ct SET ct.COMMENTS = :new.COMMENTS,ct.HTMLCOMMENTS = :new.HTMLCOMMENTS WHERE CT.ATTACH_RECORDID = :new.EVENT_ID AND CT.ATTACH_TABLE='EVENT';
                                 ELSE 
                                        BEGIN
                                            SELECT NEXT_UNIQUE_ID INTO comm_id FROM GLOSSARY WHERE SYSTEM_TABLE_NAME LIKE 'COMMENTS_TEXT';
                                            INSERT INTO COMMENTS_TEXT (COMMENT_ID,ATTACH_RECORDID,ATTACH_TABLE,COMMENTS,HTMLCOMMENTS) 
                                                SELECT (comm_id),:new.EVENT_ID,'EVENT',:new.COMMENTS,:new.HTMLCOMMENTS FROM dual;
                                            UPDATE GLOSSARY SET NEXT_UNIQUE_ID=(comm_id + 1) WHERE SYSTEM_TABLE_NAME LIKE 'COMMENTS_TEXT';
                                        END;
                                END IF;
                        END;
                    END IF;
                END;
    END IF;
END;
go