<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:asp="remove"  xmlns:cc1="remove" xmlns:uc="remove" xmlns:dg="remove" xmlns:mc="remove" xmlns:cul="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard" version="1.0">
    <xsl:output method="xml" encoding="ISO-8859-1" omit-xml-declaration="yes"/>
  <xsl:param name="divClass"></xsl:param>
  <!--**********************************************************************************************
*   Date     |  MITS   | Programmer | Description                                            *
**********************************************************************************************
* 11/04/2014 | 34279  | ajohari2   | Changes req for Claimant lookup
**********************************************************************************************-->
  <xsl:template match="control">
<xsl:choose>
	<xsl:when test="@type[.='message']"><div  runat="server">
    <!--MGaba2:R6 Adding the class check for Reserve WorkSheet specific layout -->
    <xsl:attribute name="class"><xsl:choose><xsl:when test="@class"><xsl:value-of select="@class"/></xsl:when><xsl:otherwise><xsl:value-of select="$divClass"></xsl:value-of></xsl:otherwise ></xsl:choose></xsl:attribute>   
    <xsl:attribute name="id">div_<xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="bgcolor"><xsl:value-of select="@bgcolor"/></xsl:attribute><b><FONT><xsl:attribute name="color"><xsl:value-of select="@color"/></xsl:attribute><xsl:value-of select="@title"/></FONT></b></div></xsl:when>

  <!--added by Nitin for R6-->
  <xsl:when test="@type[.='divcontainer']">
    <div runat="server">
      <xsl:attribute name="class">
        <xsl:value-of select="@class"></xsl:value-of>
      </xsl:attribute>
      <xsl:attribute name="id">
        <xsl:value-of select="@name"/>
      </xsl:attribute>
    </div>
  </xsl:when>

  <xsl:when test="@type[.='textarea']">
    <span class="formw"><asp:TextBox runat="server"  onchange="setDataChanged(true);">
        <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
        <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
        <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
        <xsl:attribute name="TextMode">MultiLine</xsl:attribute>
        <xsl:attribute name="Wrap">true</xsl:attribute>
        <xsl:if test="@Columns"><xsl:attribute name="Columns"><xsl:value-of select="@Columns"/></xsl:attribute></xsl:if>
        <xsl:if test="@onblur"><xsl:attribute name="onblur"><xsl:value-of select="@onblur"/></xsl:attribute></xsl:if>
        <xsl:attribute name="onchange"><xsl:choose><xsl:when test="@onchange"><xsl:value-of select="@onchange"/></xsl:when></xsl:choose>;setDataChanged(true);</xsl:attribute>
        <xsl:attribute name="tooltip"><xsl:value-of select="@tooltip"/></xsl:attribute>
        <xsl:if test="@maxlength"><xsl:attribute name="maxlength"><xsl:value-of select="@maxlength"/></xsl:attribute></xsl:if>
        <xsl:if test="@ontextchanged"><xsl:attribute name="ontextchanged"><xsl:value-of select="@ontextchanged"/></xsl:attribute></xsl:if>
        <xsl:if test="((@readonly[.='true']) or (@PowerViewReadOnly[.='true']))">
          <!--Readonly attribute-Ratheen--><xsl:attribute name="readonly">true</xsl:attribute><xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
        </xsl:if>
        <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
        <xsl:if test="@width"><xsl:attribute name="width"><xsl:value-of select="@width"/></xsl:attribute></xsl:if>
        <xsl:if test="@height">
        <xsl:attribute name="height"><xsl:value-of select="@height"/>
        </xsl:attribute>
      </xsl:if>
        <xsl:if test="@tabindex"><xsl:attribute name="tabindex"><xsl:value-of select="@tabindex" /></xsl:attribute></xsl:if>
      </asp:TextBox>
    </span >
  </xsl:when>


  <xsl:when test="@type[.='sectionheader']">
    <table width="100%" align="left">
      <tr>
        <td class="ctrlgroup" width="100%">
          <xsl:if test="@id">
            <xsl:attribute name="id">
              <xsl:value-of select="@id" />
            </xsl:attribute>
          </xsl:if>
          <xsl:value-of select="@sectiontitle"/>
        </td>
      </tr>
    </table>
  </xsl:when>


	<!--PJS MITS #11475 04/08/2008 start-->
  <xsl:when test="@type[.='space']">
    <DIV class="half" style="padding-top:5px; padding-bottom:5px">
      <asp:Label runat="server">
        <xsl:choose>
          <xsl:when test="@percent">
            <xsl:attribute name="width"><xsl:value-of select="@width"/></xsl:attribute>
          </xsl:when>
          <xsl:otherwise>
            <xsl:attribute name="width"><xsl:value-of select="@width"/></xsl:attribute>    
          </xsl:otherwise>
        </xsl:choose>
     </asp:Label>
      </DIV>
   
  </xsl:when>
	<!--PJS MITS #11475 04/08/2008 end-->
	<xsl:when test="@type[.='id']">
    <asp:TextBox style="display:none" runat="server"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute><xsl:if test="@value"><xsl:attribute name="Text"><xsl:value-of select="@value"/></xsl:attribute></xsl:if>
      <xsl:if test="@rmxignorevalue"><xsl:attribute name="rmxignorevalue"><xsl:value-of select="@rmxignorevalue"/></xsl:attribute></xsl:if>
      <xsl:if test="@rmxignoreget"><xsl:attribute name="rmxignoreget"><xsl:value-of select="@rmxignoreget"/></xsl:attribute></xsl:if>
      <xsl:if test="@rmxignoreset"><xsl:attribute name="rmxignoreset"><xsl:value-of select="@rmxignoreset"/></xsl:attribute></xsl:if>
    </asp:TextBox>
  </xsl:when>
	<xsl:otherwise>
		<xsl:choose>
      <xsl:when test="@type[.='labelonly']">
        <div  runat="server">
          <xsl:choose>
            <xsl:when test="@class">
              <xsl:attribute name="class">
                <xsl:value-of select="@class"/>
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>

              <xsl:choose>
                <xsl:when test="count(../control) = '3' and not(../control[@type='hidden']) and not(../control[@type='id'])">
                  <xsl:attribute name="class">threecontrols</xsl:attribute>
                </xsl:when>
                <xsl:when test="count(../control) = '1'">
                  <xsl:attribute name="class">completerow</xsl:attribute>
                </xsl:when>
                <xsl:when test="@completeRow='true'">
                  <xsl:attribute name="class">completerow</xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:attribute name="class">
                    <xsl:value-of select="$divClass"></xsl:value-of>
                  </xsl:attribute>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:otherwise>
          </xsl:choose>
<xsl:attribute name="id">div_<xsl:value-of select="@name"/></xsl:attribute>
          <!--Mgaba2<xsl:attribute name="style">display:none;</xsl:attribute>-->
         <!-- <span class="label">-->
          <asp:Label runat="server" class="label">
          
            <xsl:attribute name="id">
              <xsl:value-of select="@name"/>
            </xsl:attribute>
            <xsl:if test="@ref">
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref" /></xsl:attribute>
              <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
            </xsl:if>
            <xsl:attribute name="Text">
              <xsl:choose>
                <xsl:when test="@title">
                  <xsl:choose>
                    <xsl:when test="@title[.!='']">
                      <xsl:value-of select="@title"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="text()"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="text()"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:attribute>
            <xsl:if test="@DataRunTime">
              <xsl:attribute name="DataRunTime">
                <xsl:value-of select="@DataRunTime"/>
              </xsl:attribute>
            </xsl:if>
              <xsl:if test="@title[.!='']">
                  <xsl:attribute name="StaticValue">
                    <xsl:value-of select="@title"/>
                  </xsl:attribute>
              </xsl:if>
              
          </asp:Label>
           <!-- </span>-->
        </div>
      </xsl:when>
      <!-- MAC : employeelastnamelookup Control-->
      <xsl:when test="@type[.='employeelastnamelookup']">
		<div  runat="server">
			  <xsl:attribute name="class">
				  <xsl:value-of select="$divClass"></xsl:value-of>
			  </xsl:attribute>
			  <xsl:attribute name="id">div_<xsl:value-of select="@name"/></xsl:attribute>
			 <span class="label">
                  <xsl:value-of select="@title"/>
                </span>
		<span class="formw">
        <asp:TextBox runat="server" onchange="lookupTextChanged(this);" onblur="codeLostFocus(this.name);">
          <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
          <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
          <xsl:if test="@tabindex">
            <xsl:attribute name="tabindex">
              <xsl:value-of select="@tabindex" />
            </xsl:attribute>
          </xsl:if>
          <xsl:attribute name="cancelledvalue">
            <xsl:value-of select="text()"/>
          </xsl:attribute>
        <xsl:if test="@PowerViewReadOnly[.='true']">
                <xsl:attribute name="readonly">true</xsl:attribute>
                <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
                  </xsl:if>
        <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
        </asp:TextBox>
        <xsl:if test="@creatable">
            <asp:TextBox runat="server">
            <xsl:attribute name="id"><xsl:value-of select="@name"/>_creatable</xsl:attribute>
            </asp:TextBox>
        </xsl:if>
        <input type="button" class="EllipsisControl">
          <xsl:if test="@tabindex">
            <xsl:attribute name="tabindex">
              <xsl:value-of select="number(@tabindex)+1" />
            </xsl:attribute>
          </xsl:if>
          <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@tableid ='EMPLOYEE.LASTNAME'">
              <xsl:attribute name="onclick">return lookupData('<xsl:value-of select="@name"/>','EMPLOYEE.LASTNAME',-1,'<xsl:value-of select="@fieldmark"/>',4)</xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="onclick">return lookupData('<xsl:value-of select="@name"/>','employees',3,'<xsl:value-of select="@fieldmark"/>',4)</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                      <xsl:attribute name="disabled">true</xsl:attribute>
                  </xsl:if>
        <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
        </input>
        <xsl:choose>
          <xsl:when test="not(@fieldmark)">
            <asp:TextBox runat="server">
				<!--Ashish Ahuja Mits 30264-->
				<xsl:if test="@tabindex">
					<xsl:attribute name="tabindex">
						<xsl:value-of select="number(@tabindex)+1" />
					</xsl:attribute>
				</xsl:if>
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/>/@codeid</xsl:attribute>
              <xsl:attribute name="style">display:none;</xsl:attribute>
              <xsl:attribute name="id"><xsl:value-of select="@name"/>_cid</xsl:attribute>
              <xsl:attribute name="cancelledvalue">
                <xsl:value-of select="codeid"/>               
              </xsl:attribute>
             <xsl:if test="@PowerViewReadOnly[.='true']">
                <xsl:attribute name="readonly">true</xsl:attribute>
                <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
                  </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:TextBox>
          </xsl:when>
          <xsl:otherwise>
            <input type="hidden">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>_cid</xsl:attribute>
            </input>
          </xsl:otherwise>
        </xsl:choose>
    </span>
			</div>  
	</xsl:when>
      <!--MAC : End of Control-->
      
      <xsl:when test="@type[.='controlgroup']">
				<!-- no td between controls in controlgroup -->
        <div  runat="server">
          <!--
          <xsl:attribute name="class">
            <xsl:value-of select="$divClass"></xsl:value-of>
          </xsl:attribute>
          -->
          <xsl:attribute name="id">div_<xsl:value-of select="@name"/></xsl:attribute><xsl:if test="@listitem"><li></li></xsl:if>
					<xsl:for-each select="control">
					<xsl:choose>
						<xsl:when test="@required[.='yes']"><span class="required"><xsl:value-of select="@title"/></span></xsl:when>
            <xsl:otherwise>
              <xsl:if test="@type[.!='hidden' and .!='button' and .!='buttonscript' and .!= 'labelonly' and .!= 'imagebutton' and .!= 'imagebuttonscript' ]"> <!--Ramkumar MITS 33606-->
                <span class="label">
                  <xsl:value-of select="@title"/>
                </span>
              </xsl:if>
            </xsl:otherwise>
					</xsl:choose>
					<xsl:choose>
						<xsl:when test="@type[.='id']"><asp:TextBox style="display:none" runat="server"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute><xsl:if test="@value"><xsl:attribute name="Text"><xsl:value-of select="@value"/></xsl:attribute></xsl:if>
              <xsl:if test="@rmxignorevalue"><xsl:attribute name="rmxignorevalue"><xsl:value-of select="@rmxignorevalue"/></xsl:attribute></xsl:if>
              <xsl:if test="@rmxignoreget"><xsl:attribute name="rmxignoreget"><xsl:value-of select="@rmxignoreget"/></xsl:attribute></xsl:if>
              <xsl:if test="@rmxignoreset"><xsl:attribute name="rmxignoreset"><xsl:value-of select="@rmxignoreset"/></xsl:attribute></xsl:if></asp:TextBox>
          </xsl:when>

            <xsl:when test="@type[.='button']">
              <span class="formw">
                <asp:button class="button" runat="Server">
                  <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
                  <!--mona: added so that normal buttons of the screen can also show count of child screens like leave management in non occ-->
                  <xsl:if test="@countref"><xsl:attribute name="RMXRef"><xsl:value-of select="@countref"/></xsl:attribute></xsl:if>
                  <!--<xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>-->
                  <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
                  <xsl:attribute name="Text">
                    <xsl:value-of select="@title"/>
                  </xsl:attribute>
                  <xsl:if test="@onServerClick">
                    <xsl:attribute name="OnClick">
                      <xsl:value-of select="@onServerClick"/>
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:attribute name="OnClientClick">
                    <xsl:choose>
                      <xsl:when test="@functionname">
                        if(!(<xsl:value-of select="@functionname"/>&amp;&amp;XFormHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
                      </xsl:when>
                      <xsl:otherwise>
                        if(!(XFormHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
                      </xsl:otherwise>
                    </xsl:choose >
                  </xsl:attribute>
                  <xsl:attribute name="PostBackUrl">
                    <xsl:value-of select="@pagetomove"/>?<xsl:value-of select="@param"/>
                  </xsl:attribute>
                  <xsl:if test='@width'>
                    <xsl:attribute name="style">
                      width:<xsl:value-of select="@width"/>
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:if test='@visible'>
                      <xsl:attribute name="visible"><xsl:value-of select="@visible"/></xsl:attribute>
                  </xsl:if>
                  <xsl:if test="@disabled">
                      <xsl:attribute name="Enabled">False</xsl:attribute>
                  </xsl:if>
                </asp:button>
                </span>
            </xsl:when>
            
            <xsl:when test="@type[.='text']">
              <span class="formw"><asp:TextBox runat="server"  onchange="setDataChanged(true);"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
                <!-- Changed by bsharma33 textbox validation for < and >, Pan testing -->
                <!--<xsl:if test="@onblur"><xsl:attribute name="onblur"><xsl:value-of select="@onblur"/></xsl:attribute></xsl:if>-->
                <xsl:attribute name="onblur">
                  <xsl:choose>
                    <xsl:when test="@onblur">
                      <xsl:value-of select='concat("if(isProperString(this)==true){", @onblur, "} else return false;")'/>
                    </xsl:when>
                    <xsl:otherwise>return isProperString(this);</xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
                <!-- End changes by bsharma33 -->
                
              <xsl:attribute name="onchange"><xsl:choose><xsl:when test="@onchange"><xsl:value-of select="@onchange"/></xsl:when></xsl:choose>;setDataChanged(true);</xsl:attribute>
              <xsl:attribute name="tooltip">
                <xsl:value-of select="@tooltip"/>
              </xsl:attribute>
              <xsl:if test="@maxlength">
                <xsl:attribute name="maxlength">
                  <xsl:value-of select="@maxlength"/>
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="@ontextchanged">
                  <xsl:attribute name="ontextchanged">
                    <xsl:value-of select="@ontextchanged"/>
                  </xsl:attribute>
              </xsl:if>
              <xsl:if test="((@readonly[.='true']) or (@PowerViewReadOnly[.='true']))">
                <!--Readonly attribute-Ratheen-->
                <xsl:attribute name="readonly">true</xsl:attribute>
                <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
              </xsl:if>
                <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
                <xsl:if test="@width">
                  <xsl:attribute name="width">
                    <xsl:value-of select="@width"/>
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="@tabindex">
                  <xsl:attribute name="tabindex">
                    <xsl:value-of select="@tabindex" />
                  </xsl:attribute>
                </xsl:if>
            </asp:TextBox>
						<xsl:if test="@href"><a class="LightBold" href="#"><xsl:attribute name="onClick">formHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')</xsl:attribute><xsl:value-of select="@href"/></a></xsl:if>
						<xsl:if test="@button"><input type="button" class="button"><xsl:attribute name="value"><xsl:value-of select="@button"/></xsl:attribute>
              <xsl:choose>
                <xsl:when test="@buttonfunctionname">
                  <xsl:attribute name="onClick">
                    <xsl:value-of select="@buttonfunctionname" />
                  </xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:attribute name="onClick">
                    formHandler('<xsl:value-of select="@linkto" />','<xsl:value-of select="@param" />','<xsl:value-of select="@enablefornew" />','<xsl:value-of select="@type"/>')
                  </xsl:attribute>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="number(@tabindex)+1" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                <xsl:attribute name="disabled">true</xsl:attribute>
              </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </input></xsl:if>
             </span>
						</xsl:when>
            <xsl:when test="@type[.='space']">
              <span class="formw">
                <asp:Label runat="server">
                  <xsl:choose>
                    <xsl:when test="@percent">
                      <xsl:attribute name="width"><xsl:value-of select="@width"/></xsl:attribute>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:attribute name="width"><xsl:value-of select="@width"/>px</xsl:attribute>
                    </xsl:otherwise>
                  </xsl:choose>

                </asp:Label>
              </span>
            </xsl:when>
						<xsl:when test="@type[.='numeric']">
              <span class="formw"><asp:TextBox runat="server" ><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute><xsl:if test="@fixed"><xsl:attribute name="fixed"><xsl:value-of select="@fixed"/></xsl:attribute></xsl:if><xsl:if test="@min"><xsl:attribute name="min"><xsl:value-of select="@min"/></xsl:attribute></xsl:if>
                <xsl:if test="((@readonly[.='true']) or (@PowerViewReadOnly[.='true']))"><xsl:attribute name="readonly">true</xsl:attribute><xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
                </xsl:if>
                <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
                <!-- MGaba2:MITS 16062:Added setDataChanged(true) in case some other function is also called-->
                <!--csingh7 : moved numlostfocus() from onblur to onchange-->
                <!-- Mits 16505:Asif Start-->
							<!--<xsl:attribute name="onChange">numLostFocus(this);	<xsl:choose><xsl:when test="@onchange"><xsl:value-of select="@onchange"/>setDataChanged(true);</xsl:when><xsl:otherwise>setDataChanged(true);</xsl:otherwise></xsl:choose></xsl:attribute>-->
                <xsl:attribute name="onChange">numLostFocus(this);	<xsl:choose><xsl:when test="@onchange"><xsl:value-of select="@onchange"/>;setDataChanged(true);</xsl:when><xsl:otherwise>setDataChanged(true);</xsl:otherwise></xsl:choose></xsl:attribute>
                <!-- Mits 16505:Asif End-->
                <xsl:if test="@tabindex">
                  <xsl:attribute name="tabindex">
                    <xsl:value-of select="@tabindex" />
                  </xsl:attribute>
                </xsl:if>
							</asp:TextBox>
              </span>
            </xsl:when>
            <!--this type is added by Nitin for Mits 16583 on 14-May-2009-->
            <!--this will create a numeric textbox with SetDataChanged(False) on text change-->
            <xsl:when test="@type[.='numericWithNoDataChange']">
              <asp:TextBox runat="server" ><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute><xsl:if test="@fixed"><xsl:attribute name="fixed"><xsl:value-of select="@fixed"/></xsl:attribute></xsl:if><xsl:if test="@min"><xsl:attribute name="min"><xsl:value-of select="@min"/></xsl:attribute></xsl:if>
                <xsl:if test="((@readonly[.='true']) or (@PowerViewReadOnly[.='true']))"><xsl:attribute name="readonly">true</xsl:attribute><xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
                </xsl:if>
                <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
                <!-- MGaba2:MITS 16062:Added setDataChanged(true) in case some other function is also called-->
                <!--csingh7 : moved numlostfocus() from onblur to onchange-->
                <!-- Mits 16505:Asif Start-->
							<!--<xsl:attribute name="onChange">numLostFocus(this);	<xsl:choose><xsl:when test="@onchange"><xsl:value-of select="@onchange"/>setDataChanged(true);</xsl:when><xsl:otherwise>setDataChanged(true);</xsl:otherwise></xsl:choose></xsl:attribute>-->
                <xsl:attribute name="onChange">numLostFocus(this);	<xsl:choose><xsl:when test="@onchange"><xsl:value-of select="@onchange"/>;setDataChanged(false);</xsl:when><xsl:otherwise>setDataChanged(false);</xsl:otherwise></xsl:choose></xsl:attribute>
                <!-- Mits 16505:Asif End-->
                <xsl:if test="@tabindex">
                  <xsl:attribute name="tabindex">
                    <xsl:value-of select="@tabindex" />
                  </xsl:attribute>
                </xsl:if>
							</asp:TextBox>
            </xsl:when>
            <xsl:when test="@type[.='textwithbuttonscript']">
              <span class="formw">
                <asp:textbox runat="server">
                  <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
                  <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
                  <xsl:if test="@tabindex">
                    <xsl:attribute name="tabindex"><xsl:value-of select="@tabindex" /></xsl:attribute>
                  </xsl:if>
                  <xsl:attribute name="onchange">
                    <xsl:choose>
                      <xsl:when test="@onchange"><xsl:value-of select="@onchange"/></xsl:when></xsl:choose>;setDataChanged(true);
                  </xsl:attribute>
                  <xsl:if test="((@readonly[.='true']) or (@PowerViewReadOnly[.='true']))">
                    <xsl:attribute name="readonly">true</xsl:attribute>
                    <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
                  </xsl:if>
                <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
                </asp:textbox>
                <xsl:if test="@buttonscript">
                  <asp:button class="EllipsisControl" runat="server">
                    <xsl:attribute name="Text"><xsl:value-of select="@buttonscript"/></xsl:attribute>
                    <xsl:attribute name="onclientclick"><xsl:value-of select="@functionname"/></xsl:attribute>
                    <xsl:if test="@tabindex"><xsl:attribute name="tabindex"><xsl:value-of select="number(@tabindex)+1" /></xsl:attribute></xsl:if>
                  </asp:button>
                  </xsl:if>
              </span>
                
            </xsl:when>
            <!-- Start PJS implemented numericIntOnly-->
            <xsl:when test="@type[.='numericIntOnly']">
              <span class="formw">
                <asp:TextBox runat="server" size="30" onblur="numLostFocus(this);">
                <xsl:attribute name="id"><xsl:value-of select="@name" /></xsl:attribute>
                <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
                <xsl:if test="@tabindex">
                  <xsl:attribute name="tabindex"><xsl:value-of select="@tabindex" /></xsl:attribute>
                </xsl:if>
                <xsl:if test="@size">
                  <xsl:attribute name="size"><xsl:value-of select="@size"/></xsl:attribute>
                </xsl:if>
                <xsl:if test="@maxlength">
                  <xsl:attribute name="maxlength">
                    <xsl:value-of select="@maxlength"/>
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="@fixed">
                  <xsl:attribute name="fixed"><xsl:value-of select="@fixed" /></xsl:attribute>
                </xsl:if>
                <xsl:if test="@min">
                  <xsl:attribute name="min">
                    <xsl:value-of select="@min" />
                  </xsl:attribute>
                </xsl:if>
                <xsl:attribute name="onchange">
                  AllowIntOnly(this);
                  <xsl:choose>
                    <xsl:when test="@onchange">
                      <xsl:value-of select="@onchange" />
                    </xsl:when>
                    <xsl:otherwise>setDataChanged(true);</xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
                </asp:TextBox>
				      </span>
            </xsl:when>
            <!-- End PJS implemented -->
            <xsl:when test="@type[.='currency']">
              <span class="formw">
                <!--<asp:TextBox runat="server"  onchange="setDataChanged(true);" onblur="currencyLostFocus(this);"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>-->
                <mc:CurrencyTextbox runat="server"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
                   <!--Parijat: Mits 7257 :Attribute for allowing upto 3 decimal values-->
                  <xsl:choose>
                    <xsl:when test="@decimalValueUptoThree">
                      <xsl:choose>
                        <xsl:when test="@decimalValueUptoThree[.='true'] ">
                          <xsl:attribute name="rmxforms:as">currency_three</xsl:attribute>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:attribute name="rmxforms:as">currency</xsl:attribute>
                        </xsl:otherwise>
                      </xsl:choose>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:attribute name="rmxforms:as">currency</xsl:attribute>
                    </xsl:otherwise>
                  </xsl:choose>
                  <!--/Parijat-->
                  <xsl:if test="@tabindex">
                    <xsl:attribute name="tabindex">
                      <xsl:value-of select="@tabindex" />
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:if test="@min">
                    <!--Added by csingh7 Mits 15325-->
                    <xsl:attribute name="min">
                      <xsl:value-of select="@min" />
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:if test="@max">
                    <!--Added by Raman -->
                    <xsl:attribute name="max">
                      <xsl:value-of select="@max" />
                    </xsl:attribute>
                  </xsl:if>
                  <!--<xsl:attribute name="onchange"><xsl:choose><xsl:when test="@onchange"><xsl:value-of select="@onchange"/></xsl:when></xsl:choose>;setDataChanged(true);</xsl:attribute>-->
                  <!--<xsl:attribute name="onChange">currencyLostFocus(this);<xsl:choose><xsl:when test="@onchange"><xsl:value-of select="@onchange"/>;setDataChanged(true);</xsl:when><xsl:otherwise>setDataChanged(true);</xsl:otherwise></xsl:choose></xsl:attribute>-->
                  <xsl:attribute name="onChange">
                    <xsl:choose>
                      <xsl:when test="@onchange">
                        <xsl:value-of select="@onchange"/>;setDataChanged(true);
                      </xsl:when>
                      <xsl:otherwise>setDataChanged(true);</xsl:otherwise>
                    </xsl:choose>
                  </xsl:attribute>
					 <!--Rupal Multi Currency-->
					<xsl:if test="@onblur">
						<xsl:attribute name="onblur">
							<xsl:value-of select="@onblur" />
						</xsl:attribute>
					</xsl:if>
					<!--Rupal Multi Currency-->
                  <xsl:if test="((@readonly[.='true']) or (@PowerViewReadOnly[.='true']))">
                    <!--Readonly attribute-Tanuj-->
                    <xsl:attribute name="readonly">true</xsl:attribute>
                    <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
                  </xsl:if>
                  <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
                  <!--Deb Multi Currency-->
                  <xsl:if test="@CurrencyMode">
                    <xsl:attribute name="CurrencyMode">
                      <xsl:value-of select="@CurrencyMode" />
                    </xsl:attribute>
                  </xsl:if>
                  <!--Deb Multi Currency-->
                </mc:CurrencyTextbox>
                </span>

              <!-- abhateja 12.22.2008 -->
              <!-- "Amount" control implementation for autochecks. -->
              <xsl:if test="@buttonscript">
                  <input type="button" class="button">
                    <xsl:attribute name="name">
                      <xsl:value-of select="@buttonscriptname"/>
                    </xsl:attribute>
                    <xsl:attribute name="value">
                      <xsl:value-of select="@buttonscripttitle"/>
                    </xsl:attribute>
                    <xsl:attribute name="onClick">
                      <xsl:value-of select="@functionname"/>
                    </xsl:attribute>
                  </input>
              </xsl:if>
            </xsl:when>
                  <!--sharishkumar for Jira 6415 Starts-->
      <xsl:when test="@type[.='UserLookup']">
        
            <xsl:choose>
              <xsl:when test="@multiselect ='-1'">
                <asp:ListBox runat="server"  multiple="multiple">
                  <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
                  <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
                  <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
                  <xsl:attribute name="multiselect"><xsl:value-of select="@multiselect"/></xsl:attribute>
                  <xsl:attribute name="usergroups"><xsl:value-of select="@usergroups"/></xsl:attribute>
                  <xsl:if test="@tabindex"><xsl:attribute name="Tabindex"><xsl:value-of select="@tabindex" /></xsl:attribute>
                  </xsl:if>
                  <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                    <xsl:attribute name="Readonly">true</xsl:attribute>
                    <xsl:attribute name="BackColor">#F2F2F2</xsl:attribute>
                  </xsl:if>
                <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
                </asp:ListBox>
              </xsl:when>
              <xsl:otherwise>
                <asp:TextBox runat="server">
                  <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
                  <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
                  <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
                  <xsl:attribute name="multiselect"><xsl:value-of select="@multiselect"/></xsl:attribute>
                  <xsl:attribute name="usergroups"><xsl:value-of select="@usergroups"/></xsl:attribute>
                  <xsl:attribute name="onchange">RemoveExistingValues('<xsl:value-of select="@name"/>');</xsl:attribute>
                  <xsl:if test="@tabindex"><xsl:attribute name="Tabindex"><xsl:value-of select="@tabindex" /></xsl:attribute>
                  </xsl:if>
                  <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                    <xsl:attribute name="Readonly">true</xsl:attribute>
                    <xsl:attribute name="BackColor">#F2F2F2</xsl:attribute>
                  </xsl:if>
                <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
                </asp:TextBox>
              </xsl:otherwise>
            </xsl:choose>
                
            <asp:button runat="server" class="EllipsisControl">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
              <xsl:if test="@tabindex"><xsl:attribute name="tabindex"><xsl:value-of select="number(@tabindex)+1" /></xsl:attribute>
              </xsl:if>
              <xsl:attribute name="onclientclick">return AddSuppCustomListUserLookup(this.name,'<xsl:value-of select="@name"/>','UserIdStr','UserStr','<xsl:value-of select="@multiselect"/>','<xsl:value-of select="@usergroups"/>');</xsl:attribute>
              <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                <xsl:attribute name="disabled">true</xsl:attribute>
              </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:button>
            <xsl:if test="@multiselect ='-1'">
              <asp:button runat="server" class="BtnRemove">
                <xsl:attribute name="id"><xsl:value-of select="@name" />btndel</xsl:attribute>
                <xsl:if test="@tabindex">
                  <xsl:attribute name="tabindex">
                    <xsl:value-of select="number(@tabindex)+2" />
                  </xsl:attribute>
                </xsl:if>
                <xsl:attribute name="onclientclick">return deleteSelectedUserGroup('<xsl:value-of select="@name"/>')</xsl:attribute>
                <xsl:attribute name="Text">-</xsl:attribute>
                <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                  <xsl:attribute name="disabled">true</xsl:attribute>
                </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
              </asp:button>
            </xsl:if>
            <asp:TextBox runat="server">
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/>/@codeid</xsl:attribute>
              <xsl:attribute name="style">display:none;</xsl:attribute>
              <xsl:attribute name="id"><xsl:value-of select="@name"/>_lst</xsl:attribute>
            </asp:TextBox>
          
      </xsl:when>
          <!--sharishkumar for Jira 6415 ends-->
      <!--asharma326 jira 6422 new control for Supplemental HTML same as memo with different properties-->
<xsl:when test="@type[.='htmltext']">
       
            <asp:TextBox runat="Server">
              <xsl:attribute name="id">
                <xsl:value-of select="@name"/>
              </xsl:attribute>
              <xsl:attribute name="RMXRef">
                <xsl:value-of select="@ref"/>
              </xsl:attribute>
              <xsl:attribute name="RMXType">
                <xsl:value-of select="@type"/>
              </xsl:attribute>
              
                <xsl:attribute name="readonly">true</xsl:attribute>
              
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
              <xsl:if test="@fieldtype[.='configurablesupphtmlfield']">
                <xsl:attribute name="fieldtype">configurablesupphtmlfield</xsl:attribute>
              </xsl:if>
              <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
              <xsl:attribute name="onchange">setDataChanged(true);</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="@tabindex" />
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name="TextMode">MultiLine</xsl:attribute>
              <xsl:choose>
                <xsl:when test="@cols[.!='']">
                  <xsl:attribute name="Columns">
                    <xsl:value-of select="@cols" />
                  </xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:attribute name="Columns">30</xsl:attribute>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:choose>
                <xsl:when test="@rows[.!='']">
                  <xsl:attribute name="rows">
                    <xsl:value-of select="@rows" />
                  </xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:attribute name="rows">5</xsl:attribute>
                </xsl:otherwise>
              </xsl:choose>
            </asp:TextBox>
            <asp:button runat="server"  class="MemoButton">
              <xsl:attribute name="name"><xsl:value-of select="@name"/>btnMemo</xsl:attribute>
              <xsl:attribute name="id"><xsl:value-of select="@name"/>btnMemo</xsl:attribute>
              <xsl:if test="((@readonly='true') or (@PowerViewReadOnly[.='true']))">
                <xsl:attribute name="disabled">true</xsl:attribute>
              </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="number(@tabindex)+1" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="((@fieldtype[.='supphtmlfield'])or (@fieldtype[.='configurablesupphtmlfield']))">
                <xsl:attribute name="onclientclick">return EditHTMLMemo('<xsl:value-of select="@name"/>','supphtmlfield')</xsl:attribute>
              </xsl:if>
            </asp:button>
            <asp:TextBox style="display:none" runat="server">
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/>_HTMLComments</xsl:attribute>
              <xsl:attribute name="id"><xsl:value-of select="@name" />_HTML</xsl:attribute>
            </asp:TextBox>
        
      </xsl:when>

            <xsl:when test="@type[.='titleOrlabel']">
              <Label>
                <xsl:value-of select="text()"/>
              </Label>
            </xsl:when>
            <xsl:when test="@type[.='code']">
              <span class="formw">
                <uc:CodeLookUp  runat="server">
		  <xsl:attribute name="onchange">
			<xsl:choose>
				<xsl:when test="@onchange">
					<xsl:value-of select="@onchange"/>
				</xsl:when>
			</xsl:choose>setDataChanged(true);
		  </xsl:attribute>
                  <xsl:attribute name="ID">
                    <xsl:value-of select="@name"/>
                  </xsl:attribute>
                  <!--Mona:Adding a filter property where user can add custom where clause-->
                  <xsl:if test="@filter">
                    <xsl:attribute name="Filter">
                      <xsl:value-of select="@filter"/>
                    </xsl:attribute >
                  </xsl:if>                  
                  <xsl:attribute name="CodeTable">
                    <xsl:value-of select="@codetable"/>
                  </xsl:attribute>
                  <xsl:attribute name="ControlName">
                    <xsl:value-of select="@name"/>
                  </xsl:attribute>
                  <xsl:if test="@parentnode">
                    <xsl:attribute name="ParentNode">
                      <xsl:value-of select="@parentnode"/>
                    </xsl:attribute>
                  </xsl:if >
                  <xsl:attribute name="RMXRef">
                    <xsl:value-of select="@ref"/>
                  </xsl:attribute>
                  <!--Tushar:MITS 18231-->
                  <xsl:attribute name="CodeFilter">
                    <xsl:value-of select="@CodeFilter"/>
                  </xsl:attribute>
                  <!--End-->
                  <xsl:attribute name="RMXType">
                    <xsl:value-of select="@type"/>
                  </xsl:attribute>
                  <xsl:if test="@tabindex">
                    <xsl:attribute name="tabindex">
                      <xsl:value-of select="@tabindex" />
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:if test="@required[.='yes']">
                    <xsl:attribute name="Required">true</xsl:attribute>
                    <xsl:attribute name="ValidationGroup">vgSave</xsl:attribute>
                  </xsl:if>
                  <xsl:if test="@enabled"><xsl:attribute name="Enabled"><xsl:choose><xsl:when test='@enabled[.="True"]'>True</xsl:when><xsl:otherwise>False</xsl:otherwise></xsl:choose></xsl:attribute></xsl:if>
		  <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="ReadOnly">true</xsl:attribute>
                  </xsl:if>
                <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
                </uc:CodeLookUp >  
              </span>
            </xsl:when>

            <xsl:when test="@type[.='codewithdetail']">
              <span class="formw">
                <uc:CodeLookUp  runat="server">
                  <xsl:attribute name="ID">
                    <xsl:value-of select="@name"/>
                  </xsl:attribute>
                  <xsl:attribute name="CodeTable">
                    <xsl:value-of select="@codetable"/>
                  </xsl:attribute>
                  <!--Mona:Adding a filter property where user can add custom where clause-->
                  <xsl:if test="@filter">
                    <xsl:attribute name="Filter">
                      <xsl:value-of select="@filter"/>
                    </xsl:attribute >
                  </xsl:if>
                  <xsl:attribute name="ControlName">
                    <xsl:value-of select="@name"/>
                  </xsl:attribute>
                  <xsl:if test="@parentnode">
                    <xsl:attribute name="ParentNode">
                      <xsl:value-of select="@parentnode"/>
                    </xsl:attribute>
                  </xsl:if >
                  <xsl:attribute name="RMXRef">
                    <xsl:value-of select="@ref"/>
                  </xsl:attribute>
                  <xsl:attribute name="RMXType">
                    <xsl:value-of select="@type"/>
                  </xsl:attribute>
                  <xsl:if test="@tabindex">
                    <xsl:attribute name="tabindex">
                      <xsl:value-of select="@tabindex" />
                    </xsl:attribute>
                  </xsl:if>
		              <xsl:if test="@required[.='yes']">
		                <xsl:attribute name="Required">true</xsl:attribute>
		                <xsl:attribute name="ValidationGroup">vgSave</xsl:attribute>
		              </xsl:if>
                  <xsl:if test="@enabled"><xsl:attribute name="Enabled"><xsl:choose><xsl:when test='@enabled[.="True"]'>True</xsl:when><xsl:otherwise>False</xsl:otherwise></xsl:choose></xsl:attribute></xsl:if>
                  <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="ReadOnly">true</xsl:attribute></xsl:if>
                  <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
                </uc:CodeLookUp >
                <asp:button class="ClaimStatusButton" runat="Server" alt="Detail" >
                <xsl:attribute name="id"><xsl:value-of select="@name"/>detailbtn</xsl:attribute>
                <xsl:if test="@onServerClick"><xsl:attribute name="OnClick"><xsl:value-of select="@onServerClick"/></xsl:attribute></xsl:if>
                <xsl:attribute name="onclientclick">return selectCodeWithDetail('<xsl:value-of select="@name"/>_codelookup',<xsl:value-of select="@detailtype"/>);</xsl:attribute>
                <xsl:attribute name="tabindex"><xsl:value-of select="number(@tabindex)+2" /></xsl:attribute>
                 <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                   <xsl:attribute name="disabled">true</xsl:attribute>
                  </xsl:if>
                <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
              </asp:button>
              </span>
            </xsl:when>

            <xsl:when test="@type[.='combobox']">
              <span class="formw">
              <asp:DropDownList runat="server">
                <xsl:attribute name="id">
                  <xsl:value-of select="@name"/>
                </xsl:attribute>
                <xsl:if test="@isblank">
                  <xsl:attribute name="isblank">
                    <xsl:value-of select="@isblank"/>
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="@rmxignoreget">
                  <xsl:attribute name="rmxignoreget">
                    <xsl:value-of select="@rmxignoreget"/>
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="@rmxignoreset">
                  <xsl:attribute name="rmxignoreset">
                    <xsl:value-of select="@rmxignoreset"/>
                  </xsl:attribute>
                </xsl:if>
		<xsl:if test="@autopostback">
                  <xsl:attribute name="autopostback">
                    <xsl:value-of select="@autopostback"/>
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="@tabindex">
                  <xsl:attribute name="tabindex">
                    <xsl:value-of select="@tabindex" />
                  </xsl:attribute>
                </xsl:if>
                <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
                <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
                <xsl:if test="@itemsetref">
                  <xsl:attribute name="ItemSetRef">
                    <xsl:value-of select="@itemsetref" />
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="@width">
                  <xsl:attribute name="width">
                    <xsl:value-of select="@width" />
                  </xsl:attribute>
                </xsl:if>
                <xsl:attribute name="onchange">
                  <xsl:choose>
                    <!-- MGaba2:added setdatachanged(true) in case a function is already present-->
                    <xsl:when test="@onchange"><xsl:value-of select="@onchange" />;setDataChanged(true);</xsl:when>
                    <xsl:otherwise>setDataChanged(true);</xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
                <xsl:if test="@includefilename">
                  <script language="JavaScript">
                    <xsl:attribute name="SRC">
                      <xsl:value-of select="@includefilename" />
                    </xsl:attribute>{var i;}
                  </script>
                </xsl:if>
                <xsl:if test="@readonly">
                  <xsl:attribute name="Enabled">
                    <xsl:choose>
                      <xsl:when test="@readonly[.='True' or .='true']">False</xsl:when>
                      <xsl:otherwise>True</xsl:otherwise>
                    </xsl:choose>
                  </xsl:attribute>
                </xsl:if>
		<xsl:if test="@PowerViewReadOnly">
                  <xsl:attribute name="Enabled">
                    <xsl:choose>
                      <xsl:when test="@PowerViewReadOnly[.='True' or .='true']">False</xsl:when>
                      <xsl:otherwise>True</xsl:otherwise>
                    </xsl:choose>
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
                <!--<xsl:if test="@usenodeset">-->
                  <xsl:for-each select="..//option">
                      <asp:ListItem>
                                <xsl:attribute name="Value"><xsl:value-of select="@value" /></xsl:attribute>
                                <xsl:attribute name="Text"><xsl:value-of select="."/></xsl:attribute>
                      </asp:ListItem>                    
                  </xsl:for-each>
                <!--</xsl:if>-->
              </asp:DropDownList>
              </span>
						</xsl:when>
						<xsl:when test="@type[.='radio']">
							<!--<input type="radio"><xsl:attribute name="onClick"><xsl:value-of select="@onclick"/></xsl:attribute><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute><xsl:if test="@checked"><xsl:attribute name="checked"></xsl:attribute></xsl:if><xsl:attribute name="value"><xsl:value-of select="@value"/></xsl:attribute></input>-->
              <span class="formw">
              <asp:RadioButton runat="server">
                <xsl:if test="@id">
                  <xsl:attribute name="id">
                    <xsl:value-of select="@id"/>
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="@name">
                  <xsl:attribute name="id">
                    <xsl:value-of select="@name"/>
                  </xsl:attribute>
                </xsl:if>
                <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
                <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
                <xsl:attribute name="onclick">
                  <xsl:value-of select="@onclick"/>
                </xsl:attribute>
                <xsl:if test="@onServerClick">
                  <xsl:attribute name="onclick">
                    <xsl:value-of select="@onServerClick"/>
                  </xsl:attribute>
                </xsl:if>
                <xsl:attribute name="Text">
                  <xsl:value-of select="@label"/>
                </xsl:attribute>
                <xsl:if test="@value">
                  <xsl:attribute name="value">
                    <xsl:value-of select="@value"/>
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="@GroupName">
                  <xsl:attribute name="GroupName">
                    <xsl:value-of select="@GroupName"/>
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="@readonly">
                  <xsl:attribute name="Enabled"><xsl:choose><xsl:when test="@readonly[.='True' or .='true']">False</xsl:when><xsl:otherwise>True</xsl:otherwise>
                    </xsl:choose>
                  </xsl:attribute>
                </xsl:if>
		<xsl:if test="@PowerViewReadOnly">
                  <xsl:attribute name="Enabled"><xsl:choose><xsl:when test="@PowerViewReadOnly[.='True' or .='true']">False</xsl:when><xsl:otherwise>True</xsl:otherwise>
                    </xsl:choose>
                  </xsl:attribute>
                </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
              </asp:RadioButton>
             </span>
            </xsl:when>
						<xsl:when test="@type[.='hidden']">
              <span class="formw"><asp:TextBox style="display:none" runat="server"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute></asp:TextBox></span>
            </xsl:when>
            <!--rupal,mits 29024-->
            <xsl:when test="@type[.='inputhidden']">
              <input type="hidden" runat="server">
                <xsl:attribute name="id">
                  <xsl:value-of select="@name"/>
                </xsl:attribute>
                <xsl:attribute name="name">
                  <xsl:value-of select="@name"/>
                </xsl:attribute>
                <xsl:if test="@RMXRef">
                  <xsl:attribute name="RMXRef">
                    <xsl:value-of select="@ref"/>
                  </xsl:attribute>
                </xsl:if>
              </input>
            </xsl:when>
            <!--rupal:end-->
						<xsl:when test="@type[.='textlabel']">
              <span class="formw">
              <asp:Label>
                <xsl:attribute name="id">
                  <xsl:value-of select="@name"/>
                </xsl:attribute>
                <xsl:if test="@color">
                  <xsl:attribute name="color">
                    <xsl:value-of select="@color"/>
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="@bgcolor">
                  <xsl:attribute name="BackColor">
                    <xsl:value-of select="@bgcolor"/>
                  </xsl:attribute>
                </xsl:if>
                <xsl:attribute name="Text">
                  <xsl:value-of select="text()"/>
                </xsl:attribute>
              </asp:Label>
              </span>
            </xsl:when>
						<xsl:when test="@type[.='buttonscript']">
              <div  runat="server">
	        <xsl:if test="@visible">
                  <xsl:attribute name="visible">
                    <xsl:value-of select="@visible"/>
                  </xsl:attribute>
                </xsl:if>
                <xsl:attribute name="class">onecontrol</xsl:attribute>
                <xsl:attribute name="id">div_<xsl:value-of select="@name"/></xsl:attribute>
                <script language="JavaScript">
                  <xsl:attribute name="src">
                    <xsl:value-of select="@includefilename"/>
                  </xsl:attribute><![CDATA[{var i;}]]>
                </script>
                <span class="formw">
                  <asp:button class="button" runat="server">
                    <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
                    <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
                    <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
                    <xsl:choose>
                      <xsl:when test="@buttontitle">
                        <xsl:attribute name="Text"><xsl:value-of select="@buttontitle" /></xsl:attribute>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:attribute name="Text"><xsl:value-of select="@title" /></xsl:attribute>
                      </xsl:otherwise>
                    </xsl:choose>
                    <xsl:if test='@width'>
                      <xsl:attribute name="width"><xsl:value-of select="@width"/></xsl:attribute>
                    </xsl:if>
                    <xsl:if test='@visible'>
		         <xsl:attribute name="visible"><xsl:value-of select="@visible"/></xsl:attribute>
		    </xsl:if>
                    <xsl:attribute name="onClientClick"><xsl:value-of select="@functionname"/></xsl:attribute>
                    <xsl:if test="@onserverclick"><xsl:attribute name="onclick"><xsl:value-of select="@onserverclick"/>
                      </xsl:attribute>
                    </xsl:if>
                    <xsl:if test="(@PowerViewReadOnly[.='true'])">
                      <xsl:attribute name="disabled">true</xsl:attribute>
                      <xsl:attribute name="powerviewreadonly">true</xsl:attribute>
                    </xsl:if>
                  </asp:button>
                </span>
              </div>
						</xsl:when>

            <!--npadhy Start MITS 18931 The Comments control was not getting displayed in prem Calculation because the control was inside a 
            controlgroup and implementation of Memo control in controlgroup was missing--> 
            <xsl:when test="@type[.='memo']">
              <asp:TextBox runat="Server">
                <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
                <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
                <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
				<!--jramkumar MITS 33904 & 33452-->
				<xsl:if test="((@name[.!='locationareadesc' and .!='eventdescription' and .!='ev_locationareadesc' and .!='ev_eventdescription']) or (@PowerViewReadOnly[.='true']))">
					<xsl:attribute name="readonly">true</xsl:attribute>
				</xsl:if>
                <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
				<!--jramkumar MITS 33904 & 33452-->
                <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
                <xsl:attribute name="onchange">setDataChanged(true);</xsl:attribute>
                <xsl:if test="@tabindex"><xsl:attribute name="tabindex"><xsl:value-of select="@tabindex" /></xsl:attribute></xsl:if>
                <xsl:attribute name="TextMode">MultiLine</xsl:attribute>
                <xsl:choose>
                  <xsl:when test="@cols[.!='']">
                    <xsl:attribute name="Columns"><xsl:value-of select="@cols" /></xsl:attribute>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:attribute name="Columns">30</xsl:attribute>
                  </xsl:otherwise>
                </xsl:choose>
                <xsl:choose>
                  <xsl:when test="@rows[.!='']">
                    <xsl:attribute name="rows"><xsl:value-of select="@rows" /></xsl:attribute>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:attribute name="rows">3</xsl:attribute>
                  </xsl:otherwise>
                </xsl:choose>
              </asp:TextBox>
              <asp:button runat="server"  class="MemoButton">
                <xsl:attribute name="name"><xsl:value-of select="@name"/>btnMemo</xsl:attribute>
                <xsl:attribute name="id"><xsl:value-of select="@name"/>btnMemo</xsl:attribute>
                <xsl:if test="((@readonly='true') or (@PowerViewReadOnly[.='true']))"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
                <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
                <!--Ashish Ahuja Mits 30264-->
                <xsl:if test="@tabindex">
                  <xsl:attribute name="tabindex">
                    <xsl:value-of select="number(@tabindex)+1" />
                  </xsl:attribute>
                </xsl:if>
                <xsl:choose>
                  <xsl:when test="@AllowFormatText">
                    <xsl:choose>
                      <xsl:when test="@locevtdesc">
                        <xsl:attribute name="onclientclick">return EditHTMLMemo('<xsl:value-of select="@name" />','<xsl:value-of select="@locevtdesc" />');</xsl:attribute>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:attribute name="onclientclick">return EditHTMLMemo('<xsl:value-of select="@name" />','');</xsl:attribute>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:choose>
                      <xsl:when test="@locevtdesc">
                        <xsl:attribute name="onclientclick">return EditMemo('<xsl:value-of select="@name"/>','<xsl:value-of select="@locevtdesc" />');</xsl:attribute>
                      </xsl:when>
                      <xsl:when test="@fieldtype[.='supplementalfield']">
                        <xsl:attribute name="onclientclick">return EditMemo('<xsl:value-of select="@name"/>','<xsl:value-of select="@fieldtype" />')</xsl:attribute>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:attribute name="onclientclick">return EditMemo('<xsl:value-of select="@name" />','');</xsl:attribute>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:otherwise>
                </xsl:choose>
              </asp:button>
              <xsl:if test="@AllowFormatText">
                <asp:TextBox style="display:none" runat="server">
                  <xsl:attribute name="RMXRef">
                    <xsl:value-of select="@ref"/>_HTMLComments
                  </xsl:attribute>
                  <xsl:attribute name="id">
                    <xsl:value-of select="@name" />_HTML
                  </xsl:attribute>
                </asp:TextBox>
              </xsl:if>
            </xsl:when>
            <!--npadhy End MITS 18931 The Comments control was not getting displayed in prem Calculation because the control was inside a 
            controlgroup and implementation of Memo control in controlgroup was missing-->
          </xsl:choose>
					</xsl:for-each>
				</div>
			</xsl:when>
			<xsl:when test="@type[.='javascript']">
				<script language="JavaScript"><xsl:value-of select="text()"/></script>
			</xsl:when>
      <xsl:when test="@type[.='radio']">
        <div  runat="server">
          <xsl:choose>
            <xsl:when test="@class">
              <xsl:attribute name="class">
                <xsl:value-of select="@class"/>
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>

              <xsl:choose>
            <xsl:when test="count(../control) = '3' and not(../control[@type='hidden']) and not(../control[@type='id'])">
              <xsl:attribute name="class">threecontrols</xsl:attribute>
            </xsl:when>
            <xsl:when test="count(../control) = '1' and @type!='linebreak'">
              <xsl:attribute name="class">completerow</xsl:attribute>
            </xsl:when>
            <xsl:when test="count(../control) = '1' and @type='linebreak'">
              <xsl:attribute name="class">almostfull</xsl:attribute>
            </xsl:when>
            <xsl:when test="@completeRow='true'">
              <xsl:attribute name="class">completerow</xsl:attribute>
            </xsl:when>
            <xsl:when test="@float='false'">
              <xsl:attribute name="class">halfwithoutfloat</xsl:attribute>
            </xsl:when>
                <xsl:otherwise>
                  <xsl:attribute name="class">
                    <xsl:value-of select="$divClass"></xsl:value-of>
                  </xsl:attribute>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:otherwise>
          </xsl:choose>
<xsl:attribute name="id">div_<xsl:value-of select="@name"/></xsl:attribute>
          <span class="formw">
              <xsl:if test="@listitem"><li></li></xsl:if>
        <asp:RadioButton runat="server">
          <xsl:if test="@id">
            <xsl:attribute name="id">
              <xsl:value-of select="@id"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="@name">
            <xsl:attribute name="id">
              <xsl:value-of select="@name"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
          <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
          <xsl:attribute name="onclick">
            <xsl:value-of select="@onclick"/>
          </xsl:attribute>
          <xsl:if test="@onServerClick">
            <xsl:attribute name="onclick">
              <xsl:value-of select="@onServerClick"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="@class">
            <xsl:attribute name="class">
              <xsl:value-of select="@class"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:attribute name="Text">
            <xsl:value-of select="@label"/>
          </xsl:attribute>
          <xsl:if test="@value[.='0']">
            <xsl:attribute name="checked">
              <xsl:value-of select="true"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="@value">
            <xsl:attribute name="value">
              <xsl:value-of select="@value"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="@GroupName">
          <xsl:attribute name="GroupName">
              <xsl:value-of select="@GroupName"/>
            </xsl:attribute>
          </xsl:if>
	<xsl:if test="@readonly">
            <xsl:attribute name="Enabled"><xsl:choose><xsl:when test="@readonly[.='True' or .='true']">False</xsl:when><xsl:otherwise>True</xsl:otherwise>
              </xsl:choose>
            </xsl:attribute>
          </xsl:if>
	<xsl:if test="@PowerViewReadOnly">
            <xsl:attribute name="Enabled"><xsl:choose><xsl:when test="@PowerViewReadOnly[.='True' or .='true']">False</xsl:when><xsl:otherwise>True</xsl:otherwise>
              </xsl:choose>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>

        </asp:RadioButton>
           </span>
          <!--<xsl:if test="@listitem">
            <li></li>
          </xsl:if>-->
        </div>
      </xsl:when>

      <xsl:when test="@type[.='blank']">
        <div id="div1" class="half" xmlns="">
          <img src="../../Images/Blank.JPG" id="Img1" class="half" />
        </div>
      </xsl:when>
      
      
      <xsl:when test="@type[.='button']">
        
        <div  runat="server">
          <xsl:choose>
            <xsl:when test="@class">
              <xsl:attribute name="class">
                <!--Ashish Ahuja Mits 30264-->
                <xsl:if test="@tabindex">
                  <xsl:attribute name="tabindex">
                    <xsl:value-of select="number(@tabindex)+1" />
                  </xsl:attribute>
                </xsl:if>
                <xsl:value-of select="@class"/>
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>

              <xsl:choose>
                <xsl:when test="@type[.='numeric'] and @name[.='recordid']">
                  <xsl:attribute name="class">admin</xsl:attribute>
                </xsl:when>
                <!--this type is added by Nitin for Mits 16583 on 14-May-2009-->
                <xsl:when test="@type[.='numericWithNoDataChange'] and @name[.='recordid']">
                  <xsl:attribute name="class">admin</xsl:attribute>
                </xsl:when>
                <xsl:when test="@type[.='buttonscript'] and @name[.='btnGoTo']">
                  <xsl:attribute name="class">half</xsl:attribute>
                </xsl:when>
                <xsl:when test="@type[.='GridAndButtons' or .='Grid']">
                  <xsl:attribute name="class">partial</xsl:attribute>
                </xsl:when>
                <xsl:when test="count(../control) = '1' ">
                  <xsl:attribute name="class">completerow</xsl:attribute>
                </xsl:when>
                <xsl:when test="count(../control) = '3' and not(../control[@type='hidden']) and not(../control[@type='id'])">
                  <xsl:attribute name="class">threecontrols</xsl:attribute>
                </xsl:when>
                <xsl:when test="@completeRow='true'">
                  <xsl:attribute name="class">completerow</xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:attribute name="class">
                    <xsl:value-of select="$divClass"></xsl:value-of>
                  </xsl:attribute>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:otherwise>
          </xsl:choose>
<xsl:attribute name="id">div_<xsl:value-of select="@name"/></xsl:attribute>
          <span class="formw">
          <asp:button class="button" runat="Server">
            <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
             <!--mona: added so that normal buttons of the screen can also show count of child screens like leave management in non occ-->
              <xsl:if test="@countref"><xsl:attribute name="RMXRef"><xsl:value-of select="@countref"/></xsl:attribute></xsl:if>
            <!--<xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>-->
            <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
            <xsl:attribute name="Text">
              <xsl:value-of select="@title"/>
            </xsl:attribute>
            <xsl:if test="@onServerClick">
              <xsl:attribute name="OnClick">
                <xsl:value-of select="@onServerClick"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:attribute name="OnClientClick">if(!(XFormHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;</xsl:attribute>
            <xsl:attribute name="PostBackUrl">
              <xsl:value-of select="@pagetomove"/>?<xsl:value-of select="@param"/>
            </xsl:attribute>
            <xsl:if test='@width'>
              <xsl:attribute name="style">
                width:<xsl:value-of select="@width"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test='@visible'>
                <xsl:attribute name="visible"><xsl:value-of select="@visible"/></xsl:attribute>
            </xsl:if>
            <xsl:if test="@disabled">
                <xsl:attribute name="Enabled">False</xsl:attribute>
            </xsl:if>
          </asp:button>
            </span>
        </div>
        
      </xsl:when>

	<!--AP - MITS 5229 - Add/Edit Org Hierarchy-->
			<xsl:when test="@type[.='divbutton']">
        <div runat="server">
          <xsl:attribute name="class">
            <xsl:value-of select="$divClass"></xsl:value-of>
          </xsl:attribute><xsl:attribute name="id">div_<xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="style"><xsl:value-of select="@style"/></xsl:attribute>
          <span class="formw"><input type="button" class="button"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@title"/></xsl:attribute><xsl:attribute name="onClick"><xsl:value-of select="@functionname"/></xsl:attribute></input>
          </span>
        </div>
			</xsl:when>
			<!--End-->

      <xsl:when test="@type[.='buttonscript']">
        <div  runat="server">
          <xsl:choose>
            <xsl:when test="@class">
              <xsl:attribute name="class">
                <xsl:value-of select="@class"/>
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>

              <xsl:choose>
                <xsl:when test="count(../control) = '1' ">
                  <xsl:attribute name="class">completerow</xsl:attribute>
                </xsl:when>
                <xsl:when test="@type[.='buttonscript'] and @name[.='btnGoTo'] and count(../control) != '1'">
                  <xsl:attribute name="class">half</xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:attribute name="class">
                    <xsl:value-of select="$divClass"></xsl:value-of>
                  </xsl:attribute>
                </xsl:otherwise>

              </xsl:choose>
            </xsl:otherwise>
          </xsl:choose>
<xsl:attribute name="id">div_<xsl:value-of select="@name"/></xsl:attribute>
        <script language="JavaScript">
          <xsl:attribute name="src">
            <xsl:value-of select="@includefilename"/>
          </xsl:attribute><![CDATA[{var i;}]]>
        </script>
          <span class="formw">
        <asp:button class="button" runat="server">
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
          <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
          <xsl:choose>
            <xsl:when test="@buttontitle">
              <xsl:attribute name="Text">
                <xsl:value-of select="@buttontitle" />
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="Text">
                <xsl:value-of select="@title" />
              </xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:if test='@width'>
            <xsl:attribute name="width">
              <xsl:value-of select="@width"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test='@visible'>
                <xsl:attribute name="visible"><xsl:value-of select="@visible"/></xsl:attribute>
            </xsl:if>
          <xsl:attribute name="onClientClick">
            <xsl:value-of select="@functionname"/>
          </xsl:attribute>
          <xsl:if test="(@PowerViewReadOnly[.='true'])">
            <xsl:attribute name="disabled">true</xsl:attribute>
            <xsl:attribute name="powerviewreadonly">true</xsl:attribute>
          </xsl:if>
          <xsl:if test="@onserverclick">
            <xsl:attribute name="onclick">
              <xsl:value-of select="@onserverclick"/>
            </xsl:attribute>
          </xsl:if></asp:button>
          </span>
				</div>
        <xsl:choose>
          <xsl:when test="@type[.='buttonscript'] and @name[.='btnGoTo']">
            <br/><br/>
          </xsl:when>
        </xsl:choose>
      </xsl:when>
      
			
      <xsl:when test="@type[.='href']">
        <div  runat="server">
          <xsl:attribute name="class">
            <xsl:value-of select="$divClass"></xsl:value-of>
          </xsl:attribute><xsl:attribute name="id">div_<xsl:value-of select="@name"/></xsl:attribute>
          <span class="formw"><a href="#"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="onClick">formHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')</xsl:attribute><xsl:attribute name="class"><xsl:value-of select="@style"/></xsl:attribute><xsl:value-of select="@title"/></a>
          </span>
        </div>
			</xsl:when>
			<xsl:when test="@type[.='textlabel']">
        <xsl:if test="text()!=''">
          <div  runat="server">
            <xsl:choose>
              <xsl:when test="@class">
                <xsl:attribute name="class">
                  <xsl:value-of select="@class"/>
                </xsl:attribute>
              </xsl:when>
              <xsl:otherwise>

                <xsl:choose>
                  <xsl:when test="count(../control) = '3' and not(../control[@type='hidden']) and not(../control[@type='id'])">
                    <xsl:attribute name="class">threecontrols</xsl:attribute>
                  </xsl:when>
                  <xsl:when test="count(../control) = '1' and @type!='linebreak'">
                    <xsl:attribute name="class">completerow</xsl:attribute>
                  </xsl:when>
                  <xsl:when test="count(../control) = '1' and @type='linebreak'">
                    <xsl:attribute name="class">almostfull</xsl:attribute>
                  </xsl:when>
                  <xsl:when test="@completeRow='true'">
                    <xsl:attribute name="class">completerow</xsl:attribute>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:attribute name="class">
                      <xsl:value-of select="$divClass"></xsl:value-of>
                    </xsl:attribute>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:otherwise>
            </xsl:choose>
            
<xsl:attribute name="id">div_<xsl:value-of select="@name"/></xsl:attribute>
            <span class="label">
            <asp:Label runat="server">
              <xsl:attribute name="id">
                <xsl:value-of select="@name"/>
              </xsl:attribute>
              <xsl:if test="@color">
                <xsl:attribute name="color">
                  <xsl:value-of select="@color"/>
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="@bgcolor">
                <xsl:attribute name="BackColor">
                  <xsl:value-of select="@bgcolor"/>
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name="Text">
                <xsl:value-of select="text()"/>
              </xsl:attribute>
            </asp:Label>
            </span>
          </div>
        </xsl:if>
			</xsl:when>
			
			<!--Sanket Added for supp grid -start-->
			<xsl:when test="@type[.='suppgrid']">
                 <asp:TextBox style="display:none" runat="server"><xsl:attribute name="name"><xsl:value-of select="@name"/>_suppgrid</xsl:attribute>
                 </asp:TextBox>
                 
				 <td>
					<xsl:value-of select="@Value"/>&amp;nbsp;&amp;nbsp;&amp;nbsp;
				</td>
				
				<td>
					<xsl:element name="table" >
					<xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
		
				<tr>
					<xsl:attribute name="class" >grayline2</xsl:attribute>
					<xsl:for-each select="fields/field">
			   <th>
							<font size="2pt"><xsl:value-of select="title"/></font>
			   </th>
			</xsl:for-each>					
			</tr>

			<xsl:for-each select="rows/row">
			<tr>
				<xsl:attribute name="class" >grayline</xsl:attribute>
				<xsl:for-each select="cell">
			<td>
				<xsl:value-of select="text()"/>
			</td>
			</xsl:for-each>
</tr>
</xsl:for-each>
</xsl:element>
</td>
<!--Sanket Added for supp grid -end-->
</xsl:when>
			
			
			
			<xsl:when test="@type[.='subtable']">
				<script language="JavaScript"><xsl:attribute name="src"><xsl:value-of select="@includefilename"/></xsl:attribute><![CDATA[{var i;}]]></script>
				<asp:TextBox style="display:none" runat="server"><xsl:attribute name="name"><xsl:value-of select="@name"/>_rows</xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@rows"/></xsl:attribute>
      </asp:TextBox>
        <div  runat="server">
          <xsl:attribute name="class">
            <xsl:value-of select="$divClass"></xsl:value-of>
          </xsl:attribute>
<xsl:attribute name="id">div_<xsl:value-of select="@name"/></xsl:attribute>
          <span class="formw">
				<div color = "red" style="position:relative;left:0;top:0;width:100px;height:50px;overflow:auto" class="singletopborder">
				<table>
					<tr class="colheader" bgcolor = "red" ><td align="center" bgcolor = "blue" ><xsl:attribute name="colspan"><xsl:value-of select="@colspan"/></xsl:attribute><xsl:value-of select="@title"/></td></tr>
					<xsl:for-each select="row">
						<tr bgcolor = "red" ><xsl:attribute name="class"><xsl:value-of select="@rowclass"/></xsl:attribute>
							<xsl:for-each select="control">
								<td>
								<xsl:choose>
									<xsl:when test="@required[.='yes']"><span class="required"><xsl:value-of select="@title"/></span></xsl:when>
									<xsl:otherwise><xsl:if test="@type[.!='hidden']">
                    <span class="label"><xsl:value-of select="@title"/></span>
                  </xsl:if></xsl:otherwise>
								</xsl:choose>
								<xsl:choose>
									<xsl:when test="@type[.='id']"><asp:TextBox style="display:none" runat="server"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute><xsl:if test="@value"><xsl:attribute name="Text"><xsl:value-of select="@value"/></xsl:attribute></xsl:if>
                    <xsl:if test="@rmxignorevalue"><xsl:attribute name="rmxignorevalue"><xsl:value-of select="@rmxignorevalue"/></xsl:attribute></xsl:if>
                    <xsl:if test="@rmxignoreget"><xsl:attribute name="rmxignoreget"><xsl:value-of select="@rmxignoreget"/></xsl:attribute></xsl:if>
                    <xsl:if test="@rmxignoreset"><xsl:attribute name="rmxignoreset"><xsl:value-of select="@rmxignoreset"/></xsl:attribute></xsl:if>
                  </asp:TextBox>
                </xsl:when>
                  <xsl:when test="@type[.='text']">
                    <asp:TextBox runat="server" >
                      <xsl:attribute name="id">
                        <xsl:value-of select="@name"/>
                      </xsl:attribute>
                      <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
                      <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
                      <xsl:if test="@onblur">
                        <xsl:attribute name="onblur">
                          <xsl:value-of select="@onblur"/>
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:if test="@maxlength">
                        <xsl:attribute name="maxlength">
                          <xsl:value-of select="@maxlength"/>
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:attribute name="onchange">
                        <xsl:choose>
                          <xsl:when test="@onchange">
                            <xsl:value-of select="@onchange"/>
                          </xsl:when>
                        </xsl:choose>;setDataChanged(true);
                      </xsl:attribute>
                      <xsl:if test="@ontextchanged">
                        <xsl:attribute name="ontextchanged">
                          <xsl:value-of select="@ontextchanged"/>
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:attribute name="tooltip">
                        <xsl:value-of select="@tooltip"/>
                      </xsl:attribute>
                      <xsl:if test="@maxlength">
                        <xsl:attribute name="maxlength">
                          <xsl:value-of select="@maxlength"/>
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:if test="((@readonly[.='true']) or (@PowerViewReadOnly[.='true']))">
                        <!--Readonly attribute-Ratheen-->
                        <xsl:attribute name="readonly">true</xsl:attribute>
                        <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
                      </xsl:if>
                      <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
                      <xsl:if test="@tabindex">
                        <xsl:attribute name="tabindex">
                          <xsl:value-of select="@tabindex" />
                        </xsl:attribute>
                      </xsl:if>
                    </asp:TextBox>
                    <xsl:if test="@href">
                      <a class="LightBold" href="#">
                        <xsl:attribute name="onClick">
                          formHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')
                        </xsl:attribute>
                        <xsl:value-of select="@href"/>
                      </a>
                    </xsl:if>
                    <xsl:if test="@lookupbutton">
                      <input type="button" class="button">
                        <xsl:attribute name="value">
                          <xsl:value-of select="@lookupbutton"/>
                        </xsl:attribute>
                        <xsl:attribute name="onClick">
                          <xsl:value-of select="@functionname"/>
                        </xsl:attribute>
                        <xsl:if test="@tabindex">
                          <xsl:attribute name="tabindex">
                            <xsl:value-of select="number(@tabindex)+1" />
                          </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="(@PowerViewReadOnly[.='true'])">
                          <xsl:attribute name="disabled">true</xsl:attribute>
                        </xsl:if>
                      <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
                      </input>
                    </xsl:if>

                    <xsl:if test="@button">
                      <input type="button" class="button">
                        <xsl:choose>
                          <xsl:when test="@buttonfunctionname">
                            <xsl:attribute name="onclick">
                              <xsl:value-of select="@buttonfunctionname" />
                            </xsl:attribute>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:attribute name="onClick">
                              formHandler('<xsl:value-of select="@linkto" />','<xsl:value-of select="@param" />','<xsl:value-of select="@enablefornew" />','<xsl:value-of select="@type"/>')
                            </xsl:attribute>
                          </xsl:otherwise>
                        </xsl:choose>
                        <xsl:attribute name="value">
                          <xsl:value-of select="@button" />
                        </xsl:attribute>
                        <xsl:if test="@tabindex">
                          <xsl:attribute name="tabindex">
                            <xsl:value-of select="number(@tabindex)+1" />
                          </xsl:attribute>
                        </xsl:if>
                        <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                          <xsl:attribute name="disabled">true</xsl:attribute>
                        </xsl:if>
                        <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
                      
                      </input>
                    </xsl:if>
                    
                  </xsl:when>
                  
									<xsl:when test="@type[.='numeric']"><asp:TextBox runat="server" ><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute><xsl:if test="@fixed"><xsl:attribute name="fixed"><xsl:value-of select="@fixed"/></xsl:attribute></xsl:if><xsl:if test="@min"><xsl:attribute name="min"><xsl:value-of select="@min"/></xsl:attribute></xsl:if>
                    <xsl:if test="@max">
                      <!--Added by Mridul MITS#18230 Added MAX attribute-->
                      <xsl:attribute name="max">
                        <xsl:value-of select="@max" />
                      </xsl:attribute>
                    </xsl:if>
                    <xsl:if test="((@readonly[.='true']) or (@PowerViewReadOnly[.='true']))"><xsl:attribute name="readonly">true</xsl:attribute><xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute></xsl:if>
                    <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
                    <!--csingh7 : moved numlostfocus() from onblur to onchange-->
                    <!-- MGaba2:MITS 16062:Added setDataChanged(true) in case some other function is also called-->
                    <!-- Mits 16505 Asif Start-->
                    <!--<xsl:attribute name="onChange">numLostFocus(this);	<xsl:choose><xsl:when test="@onchange"><xsl:value-of select="@onchange"/>setDataChanged(true);</xsl:when><xsl:otherwise>setDataChanged(true);</xsl:otherwise></xsl:choose></xsl:attribute>-->
										<xsl:attribute name="onChange">numLostFocus(this);	<xsl:choose><xsl:when test="@onchange"><xsl:value-of select="@onchange"/>;setDataChanged(true);</xsl:when><xsl:otherwise>setDataChanged(true);</xsl:otherwise></xsl:choose></xsl:attribute>
                    <!-- Mits 16505 Asif End-->
                    <xsl:if test="@tabindex">
                      <xsl:attribute name="tabindex">
                        <xsl:value-of select="@tabindex" />
                      </xsl:attribute>
                    </xsl:if>
										</asp:TextBox>
									</xsl:when>
                  <!--this type is added by Nitin for Mits 16583 on 14-May-2009-->
                  <!--this will create a numeric textbox with SetDataChanged(False) on text change-->
                  <xsl:when test="@type[.='numericWithNoDataChange']"><asp:TextBox runat="server" ><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute><xsl:if test="@fixed"><xsl:attribute name="fixed"><xsl:value-of select="@fixed"/></xsl:attribute></xsl:if><xsl:if test="@min"><xsl:attribute name="min"><xsl:value-of select="@min"/></xsl:attribute></xsl:if>
                    <xsl:if test="((@readonly[.='true']) or (@PowerViewReadOnly[.='true']))"><xsl:attribute name="readonly">true</xsl:attribute><xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute></xsl:if>
                    <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
                    <!--csingh7 : moved numlostfocus() from onblur to onchange-->
                    <!-- MGaba2:MITS 16062:Added setDataChanged(true) in case some other function is also called-->
                    <!-- Mits 16505 Asif Start-->
                    <!--<xsl:attribute name="onChange">numLostFocus(this);	<xsl:choose><xsl:when test="@onchange"><xsl:value-of select="@onchange"/>setDataChanged(true);</xsl:when><xsl:otherwise>setDataChanged(true);</xsl:otherwise></xsl:choose></xsl:attribute>-->
										<xsl:attribute name="onChange">numLostFocus(this);	<xsl:choose><xsl:when test="@onchange"><xsl:value-of select="@onchange"/>;setDataChanged(false);</xsl:when><xsl:otherwise>setDataChanged(false);</xsl:otherwise></xsl:choose></xsl:attribute>
                    <!-- Mits 16505 Asif End-->
                    <xsl:if test="@tabindex">
                      <xsl:attribute name="tabindex">
                        <xsl:value-of select="@tabindex" />
                      </xsl:attribute>
                    </xsl:if>
										</asp:TextBox>
									</xsl:when>
                  <xsl:when test="@type[.='numericIntOnly']">
                      <asp:TextBox runat="server" size="30" onblur="numLostFocus(this);">
                        <xsl:attribute name="id">
                          <xsl:value-of select="@name" />
                        </xsl:attribute>
                        <xsl:attribute name="RMXRef">
                          <xsl:value-of select="@ref"/>
                        </xsl:attribute>
                        <xsl:if test="@tabindex">
                          <xsl:attribute name="tabindex">
                            <xsl:value-of select="@tabindex" />
                          </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="@size">
                          <xsl:attribute name="size">
                            <xsl:value-of select="@size"/>
                          </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="@maxlength">
                          <xsl:attribute name="maxlength">
                            <xsl:value-of select="@maxlength"/>
                          </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="@fixed">
                          <xsl:attribute name="fixed">
                            <xsl:value-of select="@fixed" />
                          </xsl:attribute>
                        </xsl:if>
                        <xsl:if test="@min">
                          <xsl:attribute name="min">
                            <xsl:value-of select="@min" />
                          </xsl:attribute>
                        </xsl:if>
                        <xsl:attribute name="onchange">
                          AllowIntOnly(this);
                          <xsl:choose>
                            <xsl:when test="@onchange">
                              <xsl:value-of select="@onchange" />
                            </xsl:when>
                            <xsl:otherwise>setDataChanged(true);</xsl:otherwise>
                          </xsl:choose>
                        </xsl:attribute>
                      </asp:TextBox>
                  </xsl:when>
                  <xsl:when test="@type[.='currency']">
                    <!--<asp:TextBox runat="server"  onchange="setDataChanged(true);" onblur="currencyLostFocus(this);">-->
                    <mc:CurrencyTextbox runat="server">
                      <xsl:attribute name="id">
                        <xsl:value-of select="@name"/>
                      </xsl:attribute>
                      <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
                      <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>

                      <!--Parijat: Mits 7257 :Attribute for allowing upto 3 decimal values-->
                      <xsl:choose>
                        <xsl:when test="@decimalValueUptoThree">
                          <xsl:choose>
                            <xsl:when test="@decimalValueUptoThree[.='true'] ">
                              <xsl:attribute name="rmxforms:as">currency_three</xsl:attribute>
                            </xsl:when>
                            <xsl:otherwise>
                              <xsl:attribute name="rmxforms:as">currency</xsl:attribute>
                            </xsl:otherwise>
                          </xsl:choose>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:attribute name="rmxforms:as">currency</xsl:attribute>
                        </xsl:otherwise>
                      </xsl:choose>
                      <!--/Parijat-->
                      <xsl:if test="@tabindex">
                        <xsl:attribute name="tabindex">
                          <xsl:value-of select="@tabindex" />
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:if test="@min">
                        <!--Added by csingh7 Mits 15325-->
                        <xsl:attribute name="min">
                          <xsl:value-of select="@min" />
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:if test="@max"><xsl:attribute name="max"><xsl:value-of select="@max" /></xsl:attribute>
                      </xsl:if>
                      <!--<xsl:attribute name="onchange"><xsl:choose><xsl:when test="@onchange"><xsl:value-of select="@onchange"/></xsl:when></xsl:choose>;setDataChanged(true);</xsl:attribute>-->
                    <!--<xsl:attribute name="onChange">currencyLostFocus(this);<xsl:choose><xsl:when test="@onchange"><xsl:value-of select="@onchange"/>;setDataChanged(true);</xsl:when><xsl:otherwise>setDataChanged(true);</xsl:otherwise></xsl:choose></xsl:attribute>-->
                      <xsl:attribute name="onChange">
                        <xsl:choose>
                          <xsl:when test="@onchange">
                            <xsl:value-of select="@onchange"/>;setDataChanged(true);
                          </xsl:when>
                          <xsl:otherwise>setDataChanged(true);</xsl:otherwise>
                        </xsl:choose>
                      </xsl:attribute>
                      <xsl:if test="((@readonly[.='true']) or (@PowerViewReadOnly[.='true']))">
                        <!--Readonly attribute-Tanuj-->
                        <xsl:attribute name="readonly">true</xsl:attribute>
                        <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
                      </xsl:if>
                      <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
                      <!--Deb Multi Currency-->
                      <xsl:if test="@CurrencyMode">
                        <xsl:attribute name="CurrencyMode">
                          <xsl:value-of select="@CurrencyMode" />
                        </xsl:attribute>
                      </xsl:if>
                      <!--Deb Multi Currency-->
						<!--Rupal Multi Currency-->
						<xsl:if test="@onblur">
							<xsl:attribute name="onblur">
								<xsl:value-of select="@onblur" />
							</xsl:attribute>
						</xsl:if>
						<!--Rupal Multi Currency-->		
                    </mc:CurrencyTextbox>
                    
                    <!-- abhateja 12.22.2008 -->
                    <!-- "Amount" control implementation for autochecks. -->
                    <xsl:if test="@buttonscript">
                      <input type="button" class="button">
                        <xsl:attribute name="name">
                          <xsl:value-of select="@buttonscriptname"/>
                        </xsl:attribute>
                        <xsl:attribute name="value">
                          <xsl:value-of select="@buttonscripttitle"/>
                        </xsl:attribute>
                        <xsl:attribute name="onClick">
                          <xsl:value-of select="@functionname"/>
                        </xsl:attribute>
                      </input>
                    </xsl:if>
                  </xsl:when>
									<xsl:when test="@type[.='combobox']">
                    <asp:DropDownList runat="server">
                      <xsl:attribute name="id">
                        <xsl:value-of select="@name"/>
                      </xsl:attribute>
                      <xsl:if test="@isblank">
                        <xsl:attribute name="isblank">
                          <xsl:value-of select="@isblank"/>
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:if test="@tabindex">
                        <xsl:attribute name="tabindex">
                          <xsl:value-of select="@tabindex" />
                        </xsl:attribute>
                      </xsl:if>
		<xsl:if test="@autopostback">
                  <xsl:attribute name="autopostback">
                    <xsl:value-of select="@autopostback"/>
                  </xsl:attribute>
                </xsl:if>
                      <xsl:if test="@rmxignoreget">
                        <xsl:attribute name="rmxignoreget">
                          <xsl:value-of select="@rmxignoreget"/>
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:if test="@rmxignoreset">
                        <xsl:attribute name="rmxignoreset">
                          <xsl:value-of select="@rmxignoreset"/>
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
                      <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
                      <xsl:if test="@itemsetref">
                        <xsl:attribute name="ItemSetRef">
                          <xsl:value-of select="@itemsetref" />
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:if test="@width">
                        <xsl:attribute name="width">
                          <xsl:value-of select="@width" />
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:attribute name="onchange">
                        <xsl:choose>
                          <!-- MGaba2:added setdatachanged(true) in case a function is already present-->
                          <xsl:when test="@onchange"><xsl:value-of select="@onchange" />;setDataChanged(true);</xsl:when>
                          <xsl:otherwise>setDataChanged(true);</xsl:otherwise>
                        </xsl:choose>
                      </xsl:attribute>
                      <xsl:if test="@includefilename">
                        <script language="JavaScript">
                          <xsl:attribute name="SRC">
                            <xsl:value-of select="@includefilename" />
                          </xsl:attribute>{var i;}
                        </script>
                      </xsl:if>
                      <xsl:if test="@readonly">
                        <xsl:attribute name="Enabled">
                          <xsl:choose>
                            <xsl:when test="@readonly[.='True' or .='true']">False</xsl:when>
                            <xsl:otherwise>True</xsl:otherwise>
                          </xsl:choose>
                        </xsl:attribute>
                      </xsl:if>
		      <xsl:if test="@PowerViewReadOnly">
                        <xsl:attribute name="Enabled">
                          <xsl:choose>
                            <xsl:when test="@PowerViewReadOnly[.='True' or .='true']">False</xsl:when>
                            <xsl:otherwise>True</xsl:otherwise>
                          </xsl:choose>
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
                        <!--<xsl:if test="@usenodeset">-->
                            <xsl:for-each select="..//option">
                                <asp:ListItem>
                                    <xsl:attribute name="Value"><xsl:value-of select="@value" /></xsl:attribute>
                                    <xsl:attribute name="Text"><xsl:value-of select="."/></xsl:attribute>
                                </asp:ListItem>
                            </xsl:for-each>
                        <!--</xsl:if>-->
                    </asp:DropDownList>
                    </xsl:when>
					<xsl:when test="@type[.='radio']">
                    <asp:RadioButton runat="server">
                      <xsl:if test="@id">
                        <xsl:attribute name="id">
                          <xsl:value-of select="@id"/>
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:if test="@name">
                        <xsl:attribute name="id">
                          <xsl:value-of select="@name"/>
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
                      <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
                      <xsl:attribute name="onclick">
                        <xsl:value-of select="@onclick"/>
                      </xsl:attribute>
                      <xsl:if test="@onServerClick">
                        <xsl:attribute name="onclick">
                          <xsl:value-of select="@onServerClick"/>
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:attribute name="Text">
                        <xsl:value-of select="@label"/>
                      </xsl:attribute>
                      <xsl:if test="@value">
                        <xsl:attribute name="checked">
                          <xsl:value-of select="@value"/>
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:if test="@readonly">
                        <xsl:attribute name="Enabled"><xsl:choose><xsl:when test="@readonly[.='True' or .='true']">False</xsl:when><xsl:otherwise>True</xsl:otherwise>
                          </xsl:choose>
                        </xsl:attribute>
                      </xsl:if>
		      <xsl:if test="@PowerViewReadOnly">
                        <xsl:attribute name="Enabled"><xsl:choose><xsl:when test="@PowerViewReadOnly[.='True' or .='true']">False</xsl:when><xsl:otherwise>True</xsl:otherwise>
                          </xsl:choose>
                        </xsl:attribute>
                      </xsl:if>
                    <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
                    </asp:RadioButton>
									</xsl:when>
									<xsl:when test="@type[.='hidden']"><asp:TextBox style="display:none" runat="server"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute></asp:TextBox>
									</xsl:when>
                  <!--rupal,mits 29024-->
                  <xsl:when test="@type[.='inputhidden']">
                    <input type="hidden" runat="server">
                      <xsl:attribute name="id">
                        <xsl:value-of select="@name"/>
                      </xsl:attribute>
                      <xsl:attribute name="name">
                        <xsl:value-of select="@name"/>
                      </xsl:attribute>
                      <xsl:if test="@RMXRef">
                        <xsl:attribute name="RMXRef">
                          <xsl:value-of select="@ref"/>
                        </xsl:attribute>
                      </xsl:if>                      
                    </input>
									</xsl:when>
                  <!--rupal:end-->
									<xsl:when test="@type[.='textlabel']">
                    <asp:Label runat="server">
                      <xsl:attribute name="id">
                        <xsl:value-of select="@name"/>
                      </xsl:attribute>
                      <xsl:if test="@color">
                        <xsl:attribute name="color">
                          <xsl:value-of select="@color"/>
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:if test="@bgcolor">
                        <xsl:attribute name="BackColor">
                          <xsl:value-of select="@bgcolor"/>
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:attribute name="Text">
                        <xsl:value-of select="text()"/>
                      </xsl:attribute>
                    </asp:Label>
									</xsl:when>
                  <xsl:when test="@type[.='buttonscript']">
                    <script language="JavaScript">
                      <xsl:attribute name="src">
                        <xsl:value-of select="@includefilename"/>
                      </xsl:attribute><![CDATA[{var i;}]]>
                    </script>
                    <asp:button class="button" runat="server">
                      <xsl:attribute name="id">
                        <xsl:value-of select="@name"/>
                      </xsl:attribute>
                      <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
                      <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
                      <xsl:if test="(@PowerViewReadOnly[.='true'])">
                        <xsl:attribute name="disabled">true</xsl:attribute>
                        <xsl:attribute name="powerviewreadonly">true</xsl:attribute>
                      </xsl:if>
                      <xsl:choose>
                        <xsl:when test="@buttontitle">
                          <xsl:attribute name="Text">
                            <xsl:value-of select="@buttontitle" />
                          </xsl:attribute>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:attribute name="Text">
                            <xsl:value-of select="@title" />
                          </xsl:attribute>
                        </xsl:otherwise>
                      </xsl:choose>
                      <xsl:if test='@width'>
                        <xsl:attribute name="width">
                          <xsl:value-of select="@width"/>
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:attribute name="onClientClick">
                        <xsl:value-of select="@functionname"/>
                      </xsl:attribute>
                      <xsl:if test="@onserverclick">
                        <xsl:attribute name="onclick">
                          <xsl:value-of select="@onserverclick"/>
                        </xsl:attribute>
                      </xsl:if>
                    </asp:button>
                  </xsl:when>
									
								</xsl:choose>
							</td>
							</xsl:for-each>
						<xsl:if test="@input"><td><input><xsl:attribute name="type"><xsl:value-of select="@inputtype"/></xsl:attribute><xsl:attribute name="name"><xsl:value-of select="@input"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@rowid"/></xsl:attribute></input></td></xsl:if>
						</tr>
					</xsl:for-each>
					<tr><td align="left"><xsl:attribute name="colspan"><xsl:value-of select="@colspan"/></xsl:attribute>
						<input type="button" class="button" value=" Add "><xsl:attribute name="name">btnAdd<xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="onClick">Add<xsl:value-of select="@name"/>();</xsl:attribute></input>
						<input type="button" class="button" value="Delete"><xsl:attribute name="name">btnDelete<xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="onClick">Delete<xsl:value-of select="@name"/>();</xsl:attribute></input>
					</td></tr>
				</table>
				</div>
       </span>
		</div>
				<script language="JavaScript"><xsl:value-of select="@onload"/></script>
			</xsl:when>
			<xsl:when test="@type[.='subtable1']" >
				<!--<script language="JavaScript"><xsl:attribute name="src"><xsl:value-of select="@includefilename"/></xsl:attribute><![CDATA[{var i;}]]></script>-->
				<asp:TextBox style="display:none" runat="server">
							<xsl:attribute name="name"><xsl:value-of select="@name"/>_rows</xsl:attribute>
							<xsl:attribute name="value"><xsl:value-of select="count(row)"/></xsl:attribute>
							<xsl:attribute name="DisplayColumns"><xsl:for-each select="./header/column"><xsl:if test="@hidden[.='false']"><xsl:value-of select="@name"/>/</xsl:if></xsl:for-each></xsl:attribute>
				</asp:TextBox>
        <div  runat="server">
          <xsl:attribute name="class">
            <xsl:value-of select="$divClass"></xsl:value-of>
          </xsl:attribute><xsl:attribute name="id">div_<xsl:value-of select="@name"/></xsl:attribute>
          <span class="formw">
					<div class = "divscroll">
					<!--div style="position:relative;left:0;top:0;width:700px;height:100px;overflow:auto" class = "divscroll"-->
						<xsl:attribute name="style"><xsl:value-of select="@style"/></xsl:attribute>
						<xsl:element name="table">
								<xsl:attribute name="id"><xsl:value-of select="@name"/>_table</xsl:attribute>
								<!--xsl:attribute name="border">1</xsl:attribute-->
								<!-- Table Header -->
								<xsl:if test="@title">
									<tr class="ctrlgroup">
										<!-- Leave room for selection check boxes -->
										<!--xsl:if test="@rowselect">
											<td>&amp;nbsp;</td>
										</xsl:if-->
										<xsl:element name="td">
											<xsl:attribute name="colspan">
												<xsl:value-of select="count(header/column)  + 1"/>
											</xsl:attribute>
											<xsl:value-of select="@title"/>
										</xsl:element>
									</tr>
								</xsl:if>
								<!-- Column Headers -->
								<tr class="colheader3">
									<!-- Leave room for selection check boxes -->
									<xsl:if test="@rowselect">
										<td></td>
									</xsl:if>
					
									<xsl:for-each select="./header/column">
										<xsl:if test="@hidden != 'true'">
											<td>
												<xsl:value-of select="@title"/>
												<!--xsl:variable name="display-columns" select="concat($display-columns, '@name', ',' )" /-->
											</td>
										</xsl:if>
									</xsl:for-each>
					
									<!-- Single Column Header for All hidden columns -->
									<xsl:if test="./header/column[@hidden = 'true']">
										<xsl:element name="td">
											<xsl:attribute name="colspan">
												<xsl:value-of select="count(./header/column[@hidden = 'true'])"/>
											</xsl:attribute>
										</xsl:element>
									</xsl:if>
								</tr>
					
								<!-- Render Value Cells -->
								<xsl:for-each select="row">
									<xsl:variable select="position()" name="row-position"/>
									<tr>
										<!-- Leave room for selection check boxes -->
										<xsl:choose>
													<xsl:when  test="position() mod 2 = 0">
														<xsl:attribute name="class" >grayline2</xsl:attribute>
													</xsl:when>
													<xsl:otherwise>
														<xsl:attribute name="class" >grayline</xsl:attribute>
													</xsl:otherwise>
											</xsl:choose>
										<xsl:if test="../@rowselect">
											<td>
												<xsl:choose>
													<xsl:when test="@selectable!='false'">
														<xsl:element name="input">
															<xsl:attribute name="type">checkbox</xsl:attribute>
															<xsl:attribute name="name"><xsl:value-of select="../@name"/>_chk_<xsl:value-of select="position()"/></xsl:attribute>
															<xsl:attribute name="id"><xsl:value-of select="../@name"/>_chk_<xsl:value-of select="position()"/></xsl:attribute>
															<xsl:attribute name="onclick">onClickCheckBox(<xsl:value-of select="position()"/>, '<xsl:value-of select="../@name"/>')</xsl:attribute>
														</xsl:element>
													</xsl:when>
													<xsl:otherwise>
															&amp;nbsp;
													</xsl:otherwise>
												</xsl:choose>
											</td>
										</xsl:if>
					
										<xsl:for-each select="cell">
											<xsl:variable select="position()" name="cell-position"/>
											<xsl:if test="../../header/column[position()=$cell-position]/@hidden != 'true'">
												<xsl:element name="td">
													<!--xsl:attribute name="name">
														<xsl:value-of select="../../@name"/>_<xsl:value-of select="../../header/column[position()=$cell-position]/@name"/>_<xsl:value-of select="$row-position"/></xsl:attribute-->
					
													<xsl:choose>
														<xsl:when test="@onclick">
															<xsl:element name="a">
																<xsl:attribute name="href">
																	<xsl:value-of select="@onclick"/>
																</xsl:attribute>
																<xsl:value-of select="text()"/>
															</xsl:element>
														</xsl:when>
														<xsl:otherwise>
															<xsl:value-of select="text()"/>
														</xsl:otherwise>
													</xsl:choose>
					
													<xsl:element name="input">
														<xsl:attribute name="type">hidden</xsl:attribute>
														<xsl:attribute name="id">
															<xsl:value-of select="../../@name"/>_<xsl:value-of select="../../header/column[position()=$cell-position]/@name"/>_<xsl:value-of select="$row-position"/></xsl:attribute>
														<xsl:attribute name="name">
															<xsl:value-of select="../../@name"/>_<xsl:value-of select="../../header/column[position()=$cell-position]/@name"/>_<xsl:value-of select="$row-position"/></xsl:attribute>
														<xsl:attribute name="value">
															<xsl:value-of select="@idvalue"/>
														</xsl:attribute>
														<xsl:attribute name="fieldCaption">
															<xsl:value-of select="text()"/>
														</xsl:attribute>
													</xsl:element>
												</xsl:element>
											</xsl:if>
										</xsl:for-each>
										<td>
											<xsl:for-each select="cell">
												<xsl:variable select="position()" name="cell-position"/>
												<xsl:if test="../../header/column[position()=$cell-position]/@hidden = 'true'">
													<xsl:element name="input">
														<xsl:attribute name="type">hidden</xsl:attribute>
														<xsl:attribute name="id">
															<xsl:value-of select="../../@name"/>_<xsl:value-of select="../../header/column[position()=$cell-position]/@name"/>_<xsl:value-of select="$row-position"/></xsl:attribute>
														<xsl:attribute name="name">
															<xsl:value-of select="../../@name"/>_<xsl:value-of select="../../header/column[position()=$cell-position]/@name"/>_<xsl:value-of select="$row-position"/></xsl:attribute>
														<xsl:attribute name="value">
															<xsl:value-of select="text()"/>
														</xsl:attribute>
														<xsl:attribute name="fieldCaption">
															<xsl:value-of select="text()"/>
														</xsl:attribute>
													</xsl:element>
												</xsl:if>
											</xsl:for-each>
										</td>
									</tr>
									</xsl:for-each>
								</xsl:element>
						</div>
						<table>
							<tr>
								<xsl:apply-templates select="./control | ./section/control"/>
							</tr>
						</table>

            </span>
					</div>
		
					


				
				<!--td></td-->
				<!--script language="JavaScript"><xsl:value-of select="@onload"/></script-->
   
			   </xsl:when>
		<xsl:otherwise>
      <div  runat="server">
        <xsl:choose>
          <!--linebreak condition added by Shivendu for MITS 17586-->
          <xsl:when test="@type[.='linebreak']">
            <xsl:attribute name="class">completerow</xsl:attribute>
          </xsl:when>
          <xsl:when test="@class and @type[.='ZapatecGrid']">
            <xsl:attribute name="class">completerow</xsl:attribute>
          </xsl:when>
          <xsl:when test="@class and contains(@ref , 'Jurisdictionals')">
            <xsl:attribute name="class">completerow</xsl:attribute>
          </xsl:when>
          <xsl:when test="@class">
            <xsl:attribute name="class">
              <xsl:value-of select="@class"/>
            </xsl:attribute>
          </xsl:when>
          <xsl:otherwise>
            <xsl:choose>
              <xsl:when test="@type[.='numeric'] and @name[.='recordid']">
                <xsl:attribute name="class">admin</xsl:attribute>
              </xsl:when>
              <!--this type is added by Nitin for Mits 16583 on 14-May-2009-->
              <xsl:when test="@type[.='numericWithNoDataChange'] and @name[.='recordid']">
                <xsl:attribute name="class">admin</xsl:attribute>
              </xsl:when>
              <xsl:when test="@type[.='buttonscript'] and @name[.='btnGoTo']">
                <xsl:attribute name="class">half</xsl:attribute>
              </xsl:when>
              <xsl:when test="@type[.='GridAndButtons' or .='Grid']">
                <xsl:attribute name="class">partial</xsl:attribute>
              </xsl:when>
              <xsl:when test="count(../control) = '3' and not(../control[@type='hidden']) and not(../control[@type='id'])">
                <xsl:attribute name="class">threecontrols</xsl:attribute>
              </xsl:when>
              <xsl:when test="count(../control) = '1' and @type!='linebreak'">
                <xsl:attribute name="class">completerow</xsl:attribute>
              </xsl:when>
              <xsl:when test="count(../control) = '1' and @type='linebreak'">
                <xsl:attribute name="class">almostfull</xsl:attribute>
              </xsl:when>
              <xsl:when test="@completeRow='true'">
                <xsl:attribute name="class">completerow</xsl:attribute>
              </xsl:when>
              <xsl:otherwise>
                <xsl:attribute name="class">
                  <xsl:value-of select="$divClass"></xsl:value-of>
                </xsl:attribute>
              </xsl:otherwise>
            </xsl:choose>    
          </xsl:otherwise>
        </xsl:choose>
        
          <xsl:attribute name="id">div_<xsl:value-of select="@name"/></xsl:attribute>
        <!--MGaba2 <xsl:if test="@type[.='hidden' or .='labelonly']">-->
        <xsl:if test="@type[.='hidden']">
            <xsl:attribute name="style">display:none;</xsl:attribute>
        </xsl:if>       
          <xsl:choose>
          <xsl:when test="@ignorelabel[.='true']"></xsl:when><!--Added Rakhi for R7:Add Emp Data Elements-->
          <xsl:otherwise>
          <xsl:choose>
					<xsl:when test="@required[.='yes']">
					<!--MGaba2-->
                     <asp:label runat="server" class="required">
                         <xsl:attribute name="id">lbl_<xsl:value-of select="@name"/></xsl:attribute>
                         <xsl:attribute name ="Text"><xsl:value-of select="@title"/></xsl:attribute>                          
                     </asp:label>
                     <!-- <span class="required"><xsl:value-of select="@title"/></span>-->
                    </xsl:when>
					<xsl:otherwise>
					    <xsl:if test="@type[.!='hidden' and .!='linebreak']"><!--linebreak condition added by Shivendu for MITS 17586-->
                            <!--<span class="label"><xsl:value-of select="@title"/></span>-->
                            <asp:label runat="server" class="label">
                                <xsl:attribute name="id">lbl_<xsl:value-of select="@name"/></xsl:attribute>
                                <xsl:attribute name ="Text"><xsl:value-of select="@title"/></xsl:attribute>
                            </asp:label>
          </xsl:if>
                    </xsl:otherwise>
                  </xsl:choose>
          </xsl:otherwise>
				</xsl:choose>
          <span>
            
            <xsl:if test="@type[.!='GridAndButtons' and .!='Grid' and .!='phonetype']"> <!--Added phonetype check for R7:Add Emp Data Elements-->
              <xsl:attribute name="class">formw</xsl:attribute>
            </xsl:if>
				<xsl:choose>
					<xsl:when test="@type[.='text']"><asp:TextBox runat="server"  onchange="setDataChanged(true);"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute><xsl:if test="@onblur"><xsl:attribute name="onblur"><xsl:value-of select="@onblur"/></xsl:attribute></xsl:if>
            <xsl:if test="@tabindex">
              <xsl:attribute name="tabindex">
                <xsl:value-of select="@tabindex" />
              </xsl:attribute>
              <xsl:if test="@maxlength">
                <xsl:attribute name="maxlength">
                  <xsl:value-of select="@maxlength"/>
                </xsl:attribute>
              </xsl:if>
            </xsl:if>
            <!--MGaba2-->
            <xsl:if test="@ontextchanged">
              <xsl:attribute name="ontextchanged">
                <xsl:value-of select="@ontextchanged"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:attribute name="onchange"><xsl:choose><xsl:when test="@onchange"><xsl:value-of select="@onchange"/></xsl:when></xsl:choose>;setDataChanged(true);</xsl:attribute>
                        <xsl:if test="((@readonly[.='true']) or (@PowerViewReadOnly[.='true']))">
                            <!--Readonly attribute-Mona-->
                            <xsl:attribute name="readonly">true</xsl:attribute>
                            <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
                        </xsl:if>
          <xsl:if test="@PowerViewReadOnly"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
          </asp:TextBox>
					<xsl:if test="@href"><a class="LightBold" href="#"><xsl:attribute name="onClick">formHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')</xsl:attribute><xsl:value-of select="@href"/></a></xsl:if>
            <xsl:if test="@lookupbutton">
            <input type="button" class="button"><xsl:attribute name="value"><xsl:value-of select="@lookupbutton"/></xsl:attribute><xsl:attribute name="onClick"><xsl:value-of select="@functionname"/></xsl:attribute>
              <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                      <xsl:attribute name="disabled">true</xsl:attribute>
                  </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </input></xsl:if>
            <xsl:if test="@button">
              <input type="button" class="button">
              <xsl:attribute name="value">
                <xsl:value-of select="@button"/>
              </xsl:attribute>
                <xsl:choose>
                  <xsl:when test="@buttonfunctionname">
                    <xsl:attribute name="onClick">
                      <xsl:value-of select="@buttonfunctionname" />
                    </xsl:attribute>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:attribute name="onClick">
                      formHandler('<xsl:value-of select="@linkto" />','<xsl:value-of select="@param" />','<xsl:value-of select="@enablefornew" />','<xsl:value-of select="@type"/>')
                    </xsl:attribute>
                  </xsl:otherwise>
                </xsl:choose>
                <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                  <xsl:attribute name="disabled">true</xsl:attribute>
                </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
              </input>
            </xsl:if>
		      </xsl:when>
          <xsl:when test="@type[.='hidden']">
            <asp:TextBox style="display:none" runat="server"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute></asp:TextBox></xsl:when>
          <!--rupal,mits 29024-->
          <xsl:when test="@type[.='inputhidden']">
            <input type="hidden" runat="server">
              <xsl:attribute name="id">
                <xsl:value-of select="@name"/>
              </xsl:attribute>
              <xsl:attribute name="name">
                <xsl:value-of select="@name"/>
              </xsl:attribute>
              <xsl:if test="@RMXRef">
                <xsl:attribute name="RMXRef">
                  <xsl:value-of select="@ref"/>
                </xsl:attribute>
              </xsl:if>
            </input>
          </xsl:when>
          <!--rupal:end-->
					<xsl:when test="@type[.='textml']">
            <asp:TextBox runat="Server">
              <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
              <xsl:attribute name="readonly">true</xsl:attribute>
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
              <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
              <xsl:attribute name="onchange">setDataChanged(true);</xsl:attribute>
              <xsl:if test="@tabindex"><xsl:attribute name="tabindex"><xsl:value-of select="@tabindex" /></xsl:attribute></xsl:if>
              <xsl:attribute name="TextMode">MultiLine</xsl:attribute>
              <xsl:choose>
                <xsl:when test="@cols[.!='']">
                  <xsl:attribute name="Columns"><xsl:value-of select="@cols" /></xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:attribute name="Columns">30</xsl:attribute>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:choose>
                <xsl:when test="@rows[.!='']">
                  <xsl:attribute name="rows"><xsl:value-of select="@rows" /></xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:attribute name="rows">3</xsl:attribute>
                </xsl:otherwise>
              </xsl:choose>
            </asp:TextBox>
            <xsl:if test="@AllowFormatText">
              <asp:TextBox style="display:none" runat="server">
                <xsl:attribute name="id"><xsl:value-of select="@name"/>_HTML</xsl:attribute>
                <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/>_HTMLComments</xsl:attribute>
                <xsl:attribute name="Text"><xsl:value-of select="text()"/></xsl:attribute>
              </asp:TextBox>
            </xsl:if>
           
					<input type="button" class="EllipsisControl"><xsl:attribute name="name"><xsl:value-of select="@name"/>btnMemo</xsl:attribute>
            <xsl:if test="@tabindex"><xsl:attribute name="tabindex"><xsl:value-of select="number(@tabindex)+1" /></xsl:attribute></xsl:if>
            <xsl:choose>
              <xsl:when test="@AllowFormatText">
                <xsl:choose>
                  <xsl:when test="@locevtdesc">
                    <xsl:attribute name="onclick">EditHTMLMemo('<xsl:value-of select="@name"/>','<xsl:value-of select="@locevtdesc" />')</xsl:attribute>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:attribute name="onclick">EditHTMLMemo('<xsl:value-of select="@name" />','')</xsl:attribute>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:when>
              <xsl:otherwise>
                <xsl:choose>
                  <xsl:when test="@locevtdesc">
                    <xsl:attribute name="onclick">EditMemo('<xsl:value-of select="@name"/>','<xsl:value-of select="@locevtdesc" />')</xsl:attribute>
                  </xsl:when>
                  <xsl:when test="@fieldtype[.='supplementalfield']">
                    <xsl:attribute name="onclick">EditMemo('<xsl:value-of select="@name"/>','<xsl:value-of select="@fieldtype" />')</xsl:attribute>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:attribute name="onclick">EditMemo('<xsl:value-of select="@name" />','')</xsl:attribute>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:if test="@PowerViewReadOnly[.='true']">
              <xsl:attribute name="disabled">true</xsl:attribute>
            </xsl:if>
          <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
          </input>
           
          </xsl:when>
                <!--sharishkumar for Jira 6415 Starts-->
      <xsl:when test="@type[.='UserLookup']">
            <xsl:choose>
              <xsl:when test="@multiselect ='-1'">
                <asp:ListBox runat="server"  multiple="multiple">
                  <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
                  <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
                  <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
                  <xsl:attribute name="multiselect"><xsl:value-of select="@multiselect"/></xsl:attribute>
                  <xsl:attribute name="usergroups"><xsl:value-of select="@usergroups"/></xsl:attribute>
                  <xsl:if test="@tabindex"><xsl:attribute name="Tabindex"><xsl:value-of select="@tabindex" /></xsl:attribute>
                  </xsl:if>
                  <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                    <xsl:attribute name="Readonly">true</xsl:attribute>
                    <xsl:attribute name="BackColor">#F2F2F2</xsl:attribute>
                  </xsl:if>
                <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
                </asp:ListBox>
              </xsl:when>
              <xsl:otherwise>
                <asp:TextBox runat="server">
                  <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
                  <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
                  <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
                  <xsl:attribute name="multiselect"><xsl:value-of select="@multiselect"/></xsl:attribute>
                  <xsl:attribute name="usergroups"><xsl:value-of select="@usergroups"/></xsl:attribute>
                  <xsl:attribute name="onchange">RemoveExistingValues('<xsl:value-of select="@name"/>');</xsl:attribute>
                  <xsl:if test="@tabindex"><xsl:attribute name="Tabindex"><xsl:value-of select="@tabindex" /></xsl:attribute>
                  </xsl:if>
                  <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                    <xsl:attribute name="Readonly">true</xsl:attribute>
                    <xsl:attribute name="BackColor">#F2F2F2</xsl:attribute>
                  </xsl:if>
                <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
                </asp:TextBox>
              </xsl:otherwise>
            </xsl:choose>
                
            <asp:button runat="server" class="EllipsisControl">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
              <xsl:if test="@tabindex"><xsl:attribute name="tabindex"><xsl:value-of select="number(@tabindex)+1" /></xsl:attribute>
              </xsl:if>
              <xsl:attribute name="onclientclick">return AddSuppCustomListUserLookup(this.name,'<xsl:value-of select="@name"/>','UserIdStr','UserStr','<xsl:value-of select="@multiselect"/>','<xsl:value-of select="@usergroups"/>');</xsl:attribute>
              <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                <xsl:attribute name="disabled">true</xsl:attribute>
              </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:button>
            <xsl:if test="@multiselect ='-1'">
              <asp:button runat="server" class="BtnRemove">
                <xsl:attribute name="id"><xsl:value-of select="@name" />btndel</xsl:attribute>
                <xsl:if test="@tabindex">
                  <xsl:attribute name="tabindex">
                    <xsl:value-of select="number(@tabindex)+2" />
                  </xsl:attribute>
                </xsl:if>
                <xsl:attribute name="onclientclick">return deleteSelectedUserGroup('<xsl:value-of select="@name"/>')</xsl:attribute>
                <xsl:attribute name="Text">-</xsl:attribute>
                <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                  <xsl:attribute name="disabled">true</xsl:attribute>
                </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
              </asp:button>
            </xsl:if>
            <asp:TextBox runat="server">
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/>/@codeid</xsl:attribute>
              <xsl:attribute name="style">display:none;</xsl:attribute>
              <xsl:attribute name="id"><xsl:value-of select="@name"/>_lst</xsl:attribute>
            </asp:TextBox>
      </xsl:when>
          <!--sharishkumar for Jira 6415 ends-->
      <!--asharma326 jira 6422 new control for Supplemental HTML same as memo with different properties-->
					<xsl:when test="@type[.='htmltext']">
            <asp:TextBox runat="Server">
              <xsl:attribute name="id">
                <xsl:value-of select="@name"/>
              </xsl:attribute>
              <xsl:attribute name="RMXRef">
                <xsl:value-of select="@ref"/>
              </xsl:attribute>
              <xsl:attribute name="RMXType">
                <xsl:value-of select="@type"/>
              </xsl:attribute>
              
                <xsl:attribute name="readonly">true</xsl:attribute>
              
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
              <xsl:if test="@fieldtype[.='configurablesupphtmlfield']">
                <xsl:attribute name="fieldtype">configurablesupphtmlfield</xsl:attribute>
              </xsl:if>
              <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
              <xsl:attribute name="onchange">setDataChanged(true);</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="@tabindex" />
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name="TextMode">MultiLine</xsl:attribute>
              <xsl:choose>
                <xsl:when test="@cols[.!='']">
                  <xsl:attribute name="Columns">
                    <xsl:value-of select="@cols" />
                  </xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:attribute name="Columns">30</xsl:attribute>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:choose>
                <xsl:when test="@rows[.!='']">
                  <xsl:attribute name="rows">
                    <xsl:value-of select="@rows" />
                  </xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:attribute name="rows">5</xsl:attribute>
                </xsl:otherwise>
              </xsl:choose>
            </asp:TextBox>
            <asp:button runat="server"  class="MemoButton">
              <xsl:attribute name="name"><xsl:value-of select="@name"/>btnMemo</xsl:attribute>
              <xsl:attribute name="id"><xsl:value-of select="@name"/>btnMemo</xsl:attribute>
              <xsl:if test="((@readonly='true') or (@PowerViewReadOnly[.='true']))">
                <xsl:attribute name="disabled">true</xsl:attribute>
              </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="number(@tabindex)+1" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="((@fieldtype[.='supphtmlfield'])or (@fieldtype[.='configurablesupphtmlfield']))">
                <xsl:attribute name="onclientclick">return EditHTMLMemo('<xsl:value-of select="@name"/>','supphtmlfield')</xsl:attribute>
              </xsl:if>
            </asp:button>
            <asp:TextBox style="display:none" runat="server">
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/>_HTMLComments</xsl:attribute>
              <xsl:attribute name="id"><xsl:value-of select="@name" />_HTML</xsl:attribute>
            </asp:TextBox>
      </xsl:when>
          <xsl:when test="@type[.='code']">
            <uc:CodeLookUp  runat="server">
	    		  <xsl:attribute name="OnChange">
			<xsl:choose>
				<xsl:when test="@onchange">
					<xsl:value-of select="@onchange"/>
				</xsl:when>
			</xsl:choose>setDataChanged(true);
		  </xsl:attribute>
              <xsl:attribute name="ID">
                <xsl:value-of select="@name"/>
              </xsl:attribute>
              <!--Mona:Adding a filter property where user can add custom where clause-->
              <xsl:if test="@filter">
                <xsl:attribute name="Filter">
                  <xsl:value-of select="@filter"/>
                </xsl:attribute >
              </xsl:if>
              <xsl:attribute name="CodeTable">
                <xsl:value-of select="@codetable"/>
              </xsl:attribute>
              <xsl:attribute name="ControlName">
                <xsl:value-of select="@name"/>
              </xsl:attribute>
              <xsl:if test="@parentnode">
                <xsl:attribute name="ParentNode">
                  <xsl:value-of select="@parentnode"/>
                </xsl:attribute>
              </xsl:if >
              <xsl:attribute name="RMXRef">
                <xsl:value-of select="@ref"/>
              </xsl:attribute>
              <!--Tushar:MITS 18231-->
              <xsl:attribute name="CodeFilter">
                <xsl:value-of select="@CodeFilter"/>
              </xsl:attribute>
              <!--End-->
              <xsl:attribute name="RMXType">
                <xsl:value-of select="@type"/>
              </xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="@tabindex" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="@required[.='yes']">
                <xsl:attribute name="Required">true</xsl:attribute>
                <xsl:attribute name="ValidationGroup">vgSave</xsl:attribute>
              </xsl:if>
              <xsl:if test="@enabled"><xsl:attribute name="Enabled"><xsl:choose><xsl:when test='@enabled[.="True"]'>True</xsl:when><xsl:otherwise>False</xsl:otherwise></xsl:choose></xsl:attribute></xsl:if>
	      <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="ReadOnly">true</xsl:attribute></xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </uc:CodeLookUp >
          <!--Add div by kuladeep for rmA14.1 performance change -->
             <xsl:if test="@divrequired[.='yes']">
                 <div runat="server">
                   <xsl:attribute name="class">
                   <xsl:value-of select="@class"></xsl:value-of>
                   </xsl:attribute>
                   <xsl:attribute name="id">
                   <xsl:value-of select="@name"/>_codelookup_div</xsl:attribute>
                 <xsl:attribute name="style">display:none</xsl:attribute>
                 </div>
          </xsl:if> 
               
          </xsl:when>
					
          <xsl:when test="@type[.='codewithdetail']">
            <uc:CodeLookUp  runat="server">
              <xsl:attribute name="ID">
                <xsl:value-of select="@name"/>
              </xsl:attribute>
              <xsl:attribute name="CodeTable">
                <xsl:value-of select="@codetable"/>
              </xsl:attribute>
              <!--Mona:Adding a filter property where user can add custom where clause-->
              <xsl:if test="@filter">
                <xsl:attribute name="Filter">
                  <xsl:value-of select="@filter"/>
                </xsl:attribute >
              </xsl:if>
              <xsl:attribute name="ControlName">
                <xsl:value-of select="@name"/>
              </xsl:attribute>
              <xsl:if test="@parentnode">
                <xsl:attribute name="ParentNode">
                  <xsl:value-of select="@parentnode"/>
                </xsl:attribute>
              </xsl:if >
              <xsl:attribute name="RMXRef">
                <xsl:value-of select="@ref"/>
              </xsl:attribute>
              <xsl:attribute name="RMXType">
                <xsl:value-of select="@type"/>
              </xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="@tabindex" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="@required[.='yes']">
                <xsl:attribute name="Required">true</xsl:attribute>
                <xsl:attribute name="ValidationGroup">vgSave</xsl:attribute>
              </xsl:if>
              <xsl:if test="(@enabled[.='True'])"><xsl:attribute name="Enabled">false</xsl:attribute></xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="ReadOnly">true</xsl:attribute></xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </uc:CodeLookUp >
            <asp:button class="ClaimStatusButton" runat="Server" alt="Detail" ToolTip="Status Detail">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>detailbtn</xsl:attribute>
              <xsl:if test="@onServerClick">
                <xsl:attribute name="OnClick"><xsl:value-of select="@onServerClick"/></xsl:attribute>
              </xsl:if>
              <xsl:attribute name="onclientclick">return selectCodeWithDetail('<xsl:value-of select="@name"/>_codelookup',<xsl:value-of select="@detailtype"/>);</xsl:attribute>
              <xsl:attribute name="tabindex">
                <xsl:value-of select="number(@tabindex)+2" />
              </xsl:attribute>
               <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                      <xsl:attribute name="disabled">true</xsl:attribute>
                  </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:button>
          </xsl:when>
          
					<xsl:when test="@type[.='freecode']">
            <asp:TextBox runat="Server">
              <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
              <xsl:attribute name="onchange">setDataChanged(true);</xsl:attribute>
              <xsl:if test="@tabindex"><xsl:attribute name="tabindex"><xsl:value-of select="@tabindex" /></xsl:attribute>
              </xsl:if>
              <xsl:attribute name="TextMode">MultiLine</xsl:attribute>
              <xsl:choose>
                <xsl:when test="@cols[.!='']"><xsl:attribute name="Columns"><xsl:value-of select="@cols" /></xsl:attribute>
                </xsl:when>
                <xsl:otherwise><xsl:attribute name="Columns">30</xsl:attribute></xsl:otherwise>
              </xsl:choose>
              <xsl:choose>
                <xsl:when test="@rows[.!='']">
                  <xsl:attribute name="rows"><xsl:value-of select="@rows" /></xsl:attribute>
                </xsl:when>
                <xsl:otherwise><xsl:attribute name="rows">3</xsl:attribute></xsl:otherwise>
              </xsl:choose>

              <xsl:if test="@PowerViewReadOnly[.='true']">
                <xsl:attribute name="readonly">true</xsl:attribute>
                <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
              </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:TextBox>
           <input type="button" class="EllipsisControl"><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">selectCode('<xsl:value-of select="@codetable"/>','<xsl:value-of select="@name"/>')</xsl:attribute>
             <xsl:if test="@tabindex">
               <xsl:attribute name="tabindex"><xsl:value-of select="number(@tabindex)+1" /></xsl:attribute>
             </xsl:if>
             <xsl:if test=" (@PowerViewReadOnly[.='true'])">
               <xsl:attribute name="disabled">true</xsl:attribute>
             </xsl:if>
           <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
           </input>
					</xsl:when>
					<!--ngupta20 MITS 6812 01/17/2007 Added the onFocus event for date control-->
			<xsl:when test="@type[.='date']">
				<asp:TextBox runat="server" FormatAs="date" >
					<xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
					<xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
					<xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
					<xsl:if test="@tabindex">
						<xsl:attribute name="tabindex"><xsl:value-of select="@tabindex" /></xsl:attribute>
					</xsl:if>
					<xsl:if test="((@readonly[.='true']) or (@PowerViewReadOnly[.='true']))">
						<xsl:attribute name="readonly">true</xsl:attribute><xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
					</xsl:if>
          <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
          <!--Mgaba2:MITS 15483:Adding DateLostFocus in onchange event.Date sould be formatted before any other function executes -->
          <!--Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement-->
          <xsl:attribute name="onchange">dateLostFocus(this.id);validateDateYear(this.id);<xsl:if test="@onchangefunctionname"><xsl:value-of select="@onchangefunctionname" /></xsl:if>
            <xsl:choose>
              <xsl:when test="@setDataChanged[.='false']">
                setDataChanged(false);
              </xsl:when>
              <xsl:otherwise>
                setDataChanged(true);
              </xsl:otherwise>
            </xsl:choose>
            <xsl:if test="@onchangepost">
              <xsl:value-of select="@onchangepost" />
            </xsl:if>
          </xsl:attribute>
          <!--Ankit End-->
					<xsl:attribute name="onblur">dateLostFocus(this.id);<xsl:if test="@onblur"><xsl:value-of select="@onblur" /></xsl:if></xsl:attribute>
				</asp:TextBox>
        <!--Deb ML Changes-->
        <!--<asp:button class="DateLookupControl" runat="server">
          <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
          <xsl:if test="@tabindex">
            <xsl:attribute name="tabindex">
              <xsl:value-of select="number(@tabindex)+1" />
            </xsl:attribute>
            <xsl:if test="@readonly">
              <xsl:attribute name="disabled">
                <xsl:value-of select="@readonly" />
              </xsl:attribute>
            </xsl:if>
          </xsl:if>
        </asp:button>-->
        <!--Deb ML Changes-->
        <script type="text/javascript">
          <!--Deb ML Changes-->
					<!--Zapatec.Calendar.setup(
					{
					inputField : "<xsl:value-of select="@name"/>",
					ifFormat : "%m/%d/%Y",
					button : "<xsl:value-of select="@name"/>btn"
					}
					);-->
          $(function () {
           $("#<xsl:value-of select="@name"/>").datepicker({
          showOn: "button",
          buttonImage: "../../Images/calendar.gif",
          <!--akaushik5 Commneted for MITS 37848 Starts-->
          <!--buttonImageOnly: true,-->
          <!--akaushik5 Commneted for MITS 37848 Ends-->
          showOtherMonths: true,
          <xsl:if test=" (@PowerViewReadOnly[.='true'])">disabled: true,</xsl:if>
          selectOtherMonths: true,
          changeYear: true
          <!--akaushik5 Added for MITS 37848 Starts-->
          }).next('button.ui-datepicker-trigger')<xsl:if test="@tabindex">.attr("tabindex", "<xsl:value-of select= "number(@tabindex + 1)" />")
		  </xsl:if>.css({border: 'none', background:'none'});
          <!--akaushik5 Added for MITS 37848 Ends-->
          });
          <!--Deb ML Changes-->
				</script>
          </xsl:when>
          


                    <xsl:when test="@type[.='datebuttonscript']">
                        <asp:TextBox runat="server" FormatAs="date" >
                            <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
                            <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
                            <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
                            <xsl:attribute name="name"><xsl:value-of select="@name"/> </xsl:attribute>
                          <!--Mgaba2:MITS 15483:Adding DateLostFocus in onchange event.Date sould be formatted before any other function executes -->
                            <xsl:attribute name="onchange">dateLostFocus(this.id);<xsl:if test="@onchangefunctionname"><xsl:value-of select="@onchangefunctionname" /></xsl:if>setDataChanged(true);<xsl:if test="@onchangepost"><xsl:value-of select="@onchangepost" /></xsl:if></xsl:attribute>  
                            <!--<xsl:attribute name="onchange"><xsl:value-of select="@onchangefunctionname" />setDataChanged(true);</xsl:attribute>         csingh7 for MITS 14120-->
                            <xsl:attribute name="onblur">dateLostFocus(this.id);<xsl:if test="@onblur"><xsl:value-of select="@onblur" /></xsl:if></xsl:attribute>
                            <xsl:if test="@tabindex">
                                <xsl:attribute name="tabindex"><xsl:value-of select="@tabindex" /></xsl:attribute>
                            </xsl:if>
                            <xsl:if test="((@readonly[.='true' or 'True']) or (@PowerViewReadOnly[.='true']))">
                                <xsl:attribute name="ReadOnly">True</xsl:attribute>
                                <xsl:attribute name="style">
                                    background-color: #F2F2F2;
                                </xsl:attribute>
                            </xsl:if>
                        <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
                        </asp:TextBox>

                        <script language="JavaScript">
                            <xsl:attribute name="SRC">
                                <xsl:value-of select="@includefilename" />
                            </xsl:attribute>{var i;}
                        </script>

                        <!--Deb ML Changes-->
                        <!--<asp:button class="DateLookupControl" runat="server">
                            <xsl:attribute name="id"><xsl:value-of select="@name" />btn</xsl:attribute>
                            <xsl:if test="@tabindex">
                                <xsl:attribute name="tabindex">
                                    <xsl:value-of select="number(@tabindex)+1" />
                                </xsl:attribute>
                            </xsl:if>
                            <xsl:attribute name="onclientclick">
                                <xsl:value-of select="@onclickfunctionname" />
                            </xsl:attribute>

                            <xsl:if test="@readonly">
                                <xsl:attribute name="disabled">
                                    <xsl:value-of select="@readonly" />
                                </xsl:attribute>
                            </xsl:if>

                        </asp:button>-->
                        <!--Deb ML Changes-->
                        <script type="text/javascript">
                          <!--Deb ML Changes-->
                            <!--Zapatec.Calendar.setup(
                            {
                            inputField : "<xsl:value-of select="@name"/>",
                            ifFormat : "%m/%d/%Y",
                            button : "<xsl:value-of select="@name"/>btn"
                            }
                            );-->
          $(function () {
           $("#<xsl:value-of select="@name"/>").datepicker({
          showOn: "button",
          buttonImage: "../../Images/calendar.gif",
                        <!--akaushik5 Commneted for MITS 37848 Starts-->
                        <!--buttonImageOnly: true,-->
                        <!--akaushik5 Commneted for MITS 37848 Ends-->
          showOtherMonths: true,
          selectOtherMonths: true,
          <xsl:if test=" (@PowerViewReadOnly[.='true'])">disabled: true,</xsl:if>
          changeYear: true
                        <!--akaushik5 Added for MITS 37848 Starts-->
                        }).next('button.ui-datepicker-trigger')<xsl:if test="@tabindex">.attr("tabindex", "<xsl:value-of select= "number(@tabindex + 1)" />")
                        </xsl:if>.css({border: 'none', background:'none'});
                        <!--akaushik5 Added for MITS 37848 Ends-->
          });
                           <!--Deb ML Changes-->
                        </script>
                    </xsl:when>





          <xsl:when test="@type[.='customizedlistlookup']">
              <cul:SystemUsers  runat="server">
                <xsl:attribute name="ID">
                  <xsl:value-of select="@name"/>
                </xsl:attribute>
                <xsl:attribute name="CodeTable">
                  <xsl:value-of select="@codetable"/>
                </xsl:attribute>
                <xsl:attribute name="ControlName">
                  <xsl:value-of select="@name"/>
                </xsl:attribute>             
                <xsl:attribute name="RMXRef">
                  <xsl:value-of select="@ref"/>
                </xsl:attribute>                
                <xsl:if test="@tabindex">
                  <xsl:attribute name="tabindex">
                    <xsl:value-of select="@tabindex" />
                  </xsl:attribute>
                </xsl:if>    
               <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                      <xsl:attribute name="Enabled">false</xsl:attribute>
                  </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
              </cul:SystemUsers >
          </xsl:when>
          

	

          <xsl:when test="@type[.='time']">
            <asp:TextBox runat="server"  FormatAs="time" onchange="setDataChanged(true);" onblur="timeLostFocus(this.id);">
              <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
              <xsl:if test="@tabindex"><xsl:attribute name="tabindex"><xsl:value-of select="@tabindex" /></xsl:attribute></xsl:if>
              <xsl:if test="@size"><xsl:attribute name="size"><xsl:value-of select="@size" /></xsl:attribute>
              </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])">
                <xsl:attribute name="ReadOnly">True</xsl:attribute>
                <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
              </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:TextBox>
          </xsl:when>
          <!--Parijat: Changes for ORGH-->
					<xsl:when test="@type[.='orgh']">
            <asp:TextBox runat="server"  onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);">
              <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
              <xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
              <!-- Bsharma33:MITS 22032  The caption of any of the powerviews can be updated, but we need to have the original caption name as well,i.e, Department, even if the caption text is updated. therefore a tool tip is added to keep track of the original caption name.-->
              <xsl:attribute name="orgLevel"><xsl:value-of select="@orglevel"/></xsl:attribute>
              <!-- Bsharma33:MITS 22032 Ends-->
              <xsl:attribute name="cancelledvalue"><xsl:value-of select="text()"/></xsl:attribute>
              <xsl:if test="@tabindex"><xsl:attribute name="tabindex"><xsl:value-of select="@tabindex" /></xsl:attribute></xsl:if>
          <!--npadhy MITS 17516  If the orgh attribute is readonly, then show the Textbox Readonly-->
              <xsl:if test="((@readonly[.='true']) or (@PowerViewReadOnly[.='true']))">
                <xsl:attribute name="readonly">true</xsl:attribute>
                <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
              </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
              <!--Parijat <xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute>-->
          </asp:TextBox>
            <xsl:if test="@creatable">
              <asp:TextBox style="display:none" runat="server" value="1">
                <xsl:attribute name="name"><xsl:value-of select="@name"/>_creatable</xsl:attribute>
                <xsl:if test="@tabindex">
                  <xsl:attribute name="tabindex">
                    <xsl:value-of select="@tabindex" />
                  </xsl:attribute>
                </xsl:if>
            </asp:TextBox>
          </xsl:if>
          <!--npadhy MITS 17516  If the orgh attribute is readonly, then do not show the Button-->
            <xsl:choose>
              <xsl:when test="((@readonly[.='true']) or (@PowerViewReadOnly[.='true']))">
              </xsl:when>
              
              <xsl:otherwise>
                <asp:button runat="server" class="CodeLookupControl" Text="">
                  <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
                  <xsl:choose>
                    <xsl:when test="@SupTitle">
                      <xsl:attribute name="onclientclick">return selectCode('orgh','<xsl:value-of select="@name" />','<xsl:value-of select="@SupTitle" />');</xsl:attribute>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:attribute name="onclientclick">return selectCode('orgh','<xsl:value-of select="@name" />','<xsl:value-of select="@title" />');</xsl:attribute>
                    </xsl:otherwise>
                  </xsl:choose>
                  <xsl:if test="@tabindex">
                    <xsl:attribute name="tabindex"><xsl:value-of select="number(@tabindex)+1" /></xsl:attribute>
                  </xsl:if>
                </asp:button>
              </xsl:otherwise>
            </xsl:choose>
         
					<xsl:if test="@instructions">
            <asp:button runat="server"  class="InstructionControl" alt="Instructions" ToolTip="Instructions" >
            <xsl:attribute name="id"><xsl:value-of select="@name"/>btnInstructions</xsl:attribute>
              <xsl:attribute name="onclientclick">return ClaimInstructions();</xsl:attribute>
              <xsl:if test="@tabindex"><xsl:attribute name="tabindex"><xsl:value-of select="number(@tabindex)+1" /></xsl:attribute></xsl:if>
             <xsl:if test=" (@PowerViewReadOnly[.='true'])">
               <xsl:attribute name="disabled">true</xsl:attribute>
                  </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:button>
          </xsl:if>
					<asp:TextBox style="display:none" runat="server">
            <xsl:attribute name="id"><xsl:value-of select="@name"/>_cid</xsl:attribute>
              <xsl:if test="@ref">
            <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/>/@codeid</xsl:attribute>
              </xsl:if >  
            <!--<xsl:attribute name="value"><xsl:value-of select="@codeid"/></xsl:attribute>-->
            <xsl:attribute name="cancelledvalue"><xsl:value-of select="@codeid"/></xsl:attribute>
        </asp:TextBox>
          </xsl:when>
					<xsl:when test="@type[.='label']"><asp:TextBox runat="server"   onkeypress="return eatKeystrokes(event);"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute>
          </asp:TextBox>
          </xsl:when>
          <!--Deb Multi Currency-->
          <xsl:when test="@type[.='labelAndlabel']">
              <asp:Label runat="server">
                <xsl:attribute name="id">
                  <xsl:value-of select="@name"/>
                </xsl:attribute>
                <xsl:attribute name="RMXRef">
                  <xsl:value-of select="@ref"/>
                </xsl:attribute>
                <xsl:attribute name="RMXType">
                  <xsl:value-of select="@type"/>
                </xsl:attribute>
                <xsl:attribute name="name">
                  <xsl:value-of select="@name"/>
                </xsl:attribute>
                <xsl:attribute name="value">
                  <xsl:value-of select="text()"/>
                </xsl:attribute>
              </asp:Label>
					</xsl:when>
          <!--Deb Multi Currency-->
					<xsl:when test="@type[.='readonly']">
						<xsl:choose>
							
              <xsl:when test="@readonlymemo[.='1']">
                <asp:TextBox runat="Server">
                  <xsl:attribute name="id">
                    <xsl:value-of select="@name"/>
                  </xsl:attribute>
                  <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
                  <xsl:attribute name="RMXType"><xsl:value-of select="@type"/>memo</xsl:attribute>
                  <xsl:attribute name="readonly">true</xsl:attribute>
                  <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
                  <xsl:attribute name="onchange">setDataChanged(true);</xsl:attribute>
                  <xsl:attribute name="onkeypress">return eatKeystrokes(event);</xsl:attribute>
                  <xsl:if test="@tabindex">
                    <xsl:attribute name="tabindex">
                      <xsl:value-of select="@tabindex" />
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:attribute name="TextMode">MultiLine</xsl:attribute>
                  <xsl:choose>
                    <xsl:when test="@cols[.!='']">
                      <xsl:attribute name="Columns">
                        <xsl:value-of select="@cols" />
                      </xsl:attribute>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:attribute name="Columns">30</xsl:attribute>
                    </xsl:otherwise>
                  </xsl:choose>
                  <xsl:choose>
                    <xsl:when test="@rows[.!='']">
                      <xsl:attribute name="rows">
                        <xsl:value-of select="@rows" />
                      </xsl:attribute>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:attribute name="rows">3</xsl:attribute>
                    </xsl:otherwise>
                  </xsl:choose>
                </asp:TextBox>
                <input type="button" class="EllipsisControl">
                  <xsl:attribute name="name">
                    <xsl:value-of select="@name"/>btnMemo
                  </xsl:attribute>
                  <xsl:attribute name="tabindex">
                    <xsl:value-of select="number(@tabindex)+1" />
                  </xsl:attribute>
                  <xsl:attribute name="id">
                    <xsl:value-of select="@name"/>btnMemo
                  </xsl:attribute>
                  <xsl:if test="@readonly='true'">
                    <xsl:attribute name="disabled">true</xsl:attribute>
                  </xsl:if>
                  <xsl:choose>
                    <xsl:when test="@AllowFormatText">
                      <xsl:choose>
                        <xsl:when test="@locevtdesc">
                          <xsl:attribute name="onclick">
                            EditHTMLMemo('<xsl:value-of select="@name" />','<xsl:value-of select="@locevtdesc" />')
                          </xsl:attribute>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:attribute name="onclick">
                            EditHTMLMemo('<xsl:value-of select="@name" />','')
                          </xsl:attribute>
                        </xsl:otherwise>
                      </xsl:choose>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:choose>
                        <xsl:when test="@locevtdesc">
                          <xsl:attribute name="onclick">
                            EditMemo('<xsl:value-of select="@name"/>','<xsl:value-of select="@locevtdesc" />')
                          </xsl:attribute>
                        </xsl:when>
                        <xsl:when test="@fieldtype[.='supplementalfield']">
                          <xsl:attribute name="onclick">
                            EditMemo('<xsl:value-of select="@name"/>','<xsl:value-of select="@fieldtype" />')
                          </xsl:attribute>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:attribute name="onclick">
                            EditMemo('<xsl:value-of select="@name" />','')
                          </xsl:attribute>
                        </xsl:otherwise>
                      </xsl:choose>
                    </xsl:otherwise>
                  </xsl:choose>
                </input>
                <xsl:if test="@AllowFormatText">
                  <asp:TextBox style="display:none" runat="server">
                    <xsl:attribute name="RMXRef">
                      <xsl:value-of select="@ref"/>_HTMLComments
                    </xsl:attribute>
                    <xsl:attribute name="id">
                      <xsl:value-of select="@name" />_HTML
                    </xsl:attribute>
                  </asp:TextBox>
                </xsl:if>
              
               
               
              </xsl:when>
							<xsl:otherwise>
                <asp:TextBox  runat="Server">
                  <xsl:attribute name="RMXref"><xsl:value-of select="@ref"/></xsl:attribute>
                  <xsl:attribute name="id">
                    <xsl:value-of select="@name"/>
                  </xsl:attribute>
                  <xsl:if test="@tabindex"><xsl:attribute name="tabindex"><xsl:value-of select="@tabindex" /></xsl:attribute>
                  </xsl:if>
                  <!-- Mihika 05/12/2005 Added 'rmxforms:as' attribut to 'readonly' -->
                  <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
                  <xsl:attribute name="readonly">true</xsl:attribute>
                  <!--pmittal5 Mits 15308 05/14/09 - Tax id field on AutoChecks and Transaction is made enabled when new payee is added. Onchange function added to check its format-->
                  <!--<xsl:if test="@onchange"><xsl:attribute name="onchange"><xsl:value-of select="@onchange"/></xsl:attribute></xsl:if>-->
                  <xsl:attribute name="onChange">
                    <xsl:choose>
                      <xsl:when test="@onchange">
                        <xsl:value-of select="@onchange"/>;setDataChanged(true);
                      </xsl:when>
                      <xsl:otherwise>setDataChanged(true);</xsl:otherwise>
                    </xsl:choose>
                  </xsl:attribute>
                  <xsl:if test="@formatas">
                    <xsl:attribute name="FormatAs">
                      <xsl:value-of select="@formatas" />
                    </xsl:attribute>
                  </xsl:if>
                </asp:TextBox>
								<xsl:choose>
								<xsl:when test="@linktobutton[.='1']">
									<input type="button" class="button">
                    <xsl:attribute name="onclick">return if(!( XFormHandler('<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
                    </xsl:attribute>
                    <xsl:attribute name="id">
                      <xsl:value-of select="@name"/>_open
                    </xsl:attribute>
                    <xsl:attribute name="name">
                      <xsl:value-of select="button"/>
                    </xsl:attribute>
                    <xsl:if test="@tabindex">
                      <xsl:attribute name="tabindex">
                        <xsl:value-of select="number(@tabindex)+1" />
                      </xsl:attribute>
                    </xsl:if>
                  </input>
								</xsl:when>
								<xsl:otherwise>
                  <xsl:if test="@lookupbutton[.='1']">
                    <input type="button" class="EllipsisControl">
                      <xsl:attribute name="id">
                        <xsl:value-of select="@name"/>btn
                      </xsl:attribute>
                      <xsl:if test="@tabindex">
                        <xsl:attribute name="tabindex">
                          <xsl:value-of select="number(@tabindex)+1" />
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:attribute name="onclick">
                        lookupData('<xsl:value-of select="@name"/>','event',2,'<xsl:value-of select="@fieldmark"/>',11)
                      </xsl:attribute>
                    </input>
                  </xsl:if>
								</xsl:otherwise>
								</xsl:choose>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
          <!-- Debabrata Biswas added hyperlink-->
          <xsl:when test="@type[.='hyperlink']">
            <asp:HyperLink>
              <xsl:attribute name="id">
                <xsl:value-of select="@name"/>
              </xsl:attribute>
              <xsl:attribute name="runat">server</xsl:attribute>              
              <xsl:attribute name="RMXRef">
                <xsl:value-of select="@ref"/>
              </xsl:attribute>
              <xsl:attribute name="RMXType">
                <xsl:value-of select="@type"/>
              </xsl:attribute>
              <xsl:attribute name="name">
                <xsl:value-of select="@name"/>
              </xsl:attribute>
              <xsl:attribute name="text">
                <xsl:value-of select="@title"/>
              </xsl:attribute>
              <xsl:attribute name="target">_blank</xsl:attribute>
            </asp:HyperLink>
          </xsl:when>
          <!--Debabrata Biswas End-->         
          <!-- PJS:  Made changes for memo-->
          <xsl:when test="@type[.='memo']">
            <asp:TextBox runat="Server">
              <xsl:attribute name="id">
                <xsl:value-of select="@name"/>
              </xsl:attribute>
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
              <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
			  <!--jramkumar MITS 33904 & 33452-->
              <xsl:if test="(((@name[.!='locationareadesc' and .!='eventdescription' and .!='ev_locationareadesc' and .!='ev_eventdescription']) or (@PowerViewReadOnly[.='true'])) or (@PowerViewReadOnly[.='true']))">
                <xsl:attribute name="readonly">true</xsl:attribute>
              </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
			  <!--jramkumar MITS 33904 & 33452-->
              <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
              <xsl:attribute name="onchange">setDataChanged(true);</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="@tabindex" />
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name="TextMode">MultiLine</xsl:attribute>
              <xsl:choose>
                <xsl:when test="@cols[.!='']">
                  <xsl:attribute name="Columns">
                    <xsl:value-of select="@cols" />
                  </xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:attribute name="Columns">30</xsl:attribute>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:choose>
                <xsl:when test="@rows[.!='']">
                  <xsl:attribute name="rows">
                    <xsl:value-of select="@rows" />
                  </xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:attribute name="rows">3</xsl:attribute>
                </xsl:otherwise>
              </xsl:choose>
            </asp:TextBox>
            <asp:button runat="server"  class="MemoButton">
              <!--Ashish Ahuja Mits 30264-->
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="number(@tabindex)+1" />
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name="name">
                <xsl:value-of select="@name"/>btnMemo</xsl:attribute>
              <xsl:attribute name="id">
                <xsl:value-of select="@name"/>btnMemo</xsl:attribute>
              <xsl:if test="((@readonly='true') or (@PowerViewReadOnly[.='true']))">
                <xsl:attribute name="disabled">true</xsl:attribute>
              </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
              <xsl:choose>
                <xsl:when test="@AllowFormatText">
                  <xsl:choose>
                    <xsl:when test="@locevtdesc">
                      <xsl:attribute name="onclientclick">return EditHTMLMemo('<xsl:value-of select="@name" />','<xsl:value-of select="@locevtdesc" />');</xsl:attribute>                      
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:attribute name="onclientclick">return EditHTMLMemo('<xsl:value-of select="@name" />','');</xsl:attribute>                      
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:choose>
                    <xsl:when test="@locevtdesc">
                      <xsl:attribute name="onclientclick">return EditMemo('<xsl:value-of select="@name"/>','<xsl:value-of select="@locevtdesc" />');</xsl:attribute>
                    </xsl:when>
                    <xsl:when test="@fieldtype[.='supplementalfield']">
                      <xsl:attribute name="onclientclick">return EditMemo('<xsl:value-of select="@name"/>','<xsl:value-of select="@fieldtype" />')</xsl:attribute>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:attribute name="onclientclick">return EditMemo('<xsl:value-of select="@name" />','');</xsl:attribute>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                <xsl:attribute name="disabled">true</xsl:attribute>
              </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>

            </asp:button>
            <xsl:if test="@AllowFormatText">
              <asp:TextBox style="display:none" runat="server">
                <xsl:attribute name="RMXRef">
                  <xsl:value-of select="@ref"/>_HTMLComments</xsl:attribute>
                <xsl:attribute name="id">
                  <xsl:value-of select="@name" />_HTML</xsl:attribute>
              </asp:TextBox>
            </xsl:if>
            <!--Ankit Start : Financial Enhancements - Payee Phrase Change-->
              <xsl:if test="@additionalbutton">
                <asp:button class="button" runat="server">
                  <!--Ashish Ahuja Mits 30264-->
                  <xsl:if test="@tabindex">
                    <xsl:attribute name="tabindex">
                      <xsl:value-of select="number(@tabindex)+1" />
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:attribute name="id">
                    <xsl:value-of select="@buttonname"/>
                  </xsl:attribute>
                  <xsl:attribute name="Text">
                    <xsl:value-of select="@buttontitle" />
                  </xsl:attribute>
                  <xsl:if test='@buttonwidth'>
                    <xsl:attribute name="width">
                      <xsl:value-of select="@buttonwidth"/>
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:if test="@onbuttonclientclick">
                    <xsl:attribute name="onClientClick">
                      <xsl:value-of select="@onbuttonclientclick"/>
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:if test="@onbuttonserverclick">
                    <xsl:attribute name="onclick">
                      <xsl:value-of select="@onbuttonserverclick"/>
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:if test='@buttontooltip'>
                    <xsl:attribute name="ToolTip">
                      <xsl:value-of select="@buttontooltip"/>
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:if test='@buttontag'>
                    <xsl:attribute name="Tag">
                      <xsl:value-of select="@buttontag"/>
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:if test="((@readonly='true') or (@PowerViewReadOnly[.='true']))">
                    <xsl:attribute name="disabled">true</xsl:attribute>
                  </xsl:if>
                </asp:button>
            </xsl:if>
            <!--Ankit End-->
          </xsl:when>
          <!-- PJS:  Made changes for Checkbox-->
          <xsl:when test="@type[.='checkbox']">
            <asp:CheckBox  runat="server">
              <xsl:attribute name="onchange">setDataChanged(true);</xsl:attribute>
              <xsl:attribute name="id">
                <xsl:value-of select="@name"/>
              </xsl:attribute>
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
              <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
              <xsl:if test="@onclick">
                <xsl:attribute name="onclick">
                  <xsl:value-of select="@onclick"/>
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="@hideExceptOracle">
                <xsl:if test="@hideExceptOracle = 'true'">
                  <xsl:attribute name="style">display:none;</xsl:attribute>
                </xsl:if>
              </xsl:if>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="@tabindex" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="@disabled">
                <xsl:attribute name="disabled">
                  <xsl:value-of select="@disabled" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="@disablecss[.='true']">
                <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
              </xsl:if>
              <xsl:if test="((@readonly[.='True' or .='true']) or (@PowerViewReadOnly[.='true']))">
                <xsl:attribute name="disabled">true</xsl:attribute>
                <xsl:attribute name="style">background-color: #F2F2F2; pointer-events:none;</xsl:attribute>
              </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
              <xsl:if test="text()[.='1']">
                <xsl:attribute name="checked">true</xsl:attribute>
              </xsl:if>
			  <xsl:attribute name="Height">24px</xsl:attribute>
            </asp:CheckBox>
          </xsl:when>
          <!-- PJS:  Made changes for zip-->
          <xsl:when test="@type[.='zip']">
            <asp:TextBox runat="server"  onchange="setDataChanged(true);" onblur="zipLostFocus(this);">
	      <!-- sgoel6:  onChange added-->
	      <xsl:attribute name="onchange">
		<xsl:choose>
			<xsl:when test="@onchange">
				<xsl:value-of select="@onchange"/>
			</xsl:when>
		</xsl:choose>setDataChanged(true);</xsl:attribute>
               <xsl:if test="(@PowerViewReadOnly[.='true'])">
                <xsl:attribute name="readonly">true</xsl:attribute>
                <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
              </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
              <xsl:attribute name="id">
                <xsl:value-of select="@name"/>
              </xsl:attribute>
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
              <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="TabIndex">
                  <xsl:value-of select="@tabindex" />
                </xsl:attribute>
              </xsl:if>
            <xsl:attribute name="countrymappedid"><xsl:value-of select="@countrymappedid"/></xsl:attribute>
            </asp:TextBox>
          </xsl:when>
          <!--parijat: changes for Phone-->
					<xsl:when test="@type[.='phone']">
            <asp:TextBox runat="server"  onchange="setDataChanged(true);" onfocus="phoneGotFocus(this);"><!--Moved onblur event to the next line for R7:Add Emp Data Elements-->
            <xsl:attribute name="onblur"><xsl:choose><xsl:when test="@onblur"><xsl:value-of select="@onblur"/></xsl:when><xsl:otherwise>phoneLostFocus(this);</xsl:otherwise></xsl:choose></xsl:attribute><!--Added for R7:Add Emp Data Elements-->
            <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
            <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
              
              <!--<xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute>-->
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="@tabindex" />
                </xsl:attribute>
              </xsl:if>
			  <xsl:attribute name="countrymappedid"><xsl:value-of select="@countrymappedid"/></xsl:attribute>
              <xsl:if test="(@PowerViewReadOnly[.='true'])">
                <xsl:attribute name="readonly">true</xsl:attribute>
                <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
              </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:TextBox>
        </xsl:when>
          <!--Added Rakhi for R7:Add Emp Data Elements-->
          <xsl:when test="@type[.='phonetype']">
            <span class="label">
            <asp:DropDownList runat="server">
              <xsl:attribute name="id">
                <xsl:value-of select="@comboname"/>
              </xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="@tabindex" />
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name="RMXRef">
                <xsl:value-of select="@comboref"/>
              </xsl:attribute>
              <xsl:attribute name="RMXType">
                <xsl:value-of select="@type"/>
              </xsl:attribute>
		<xsl:if test="@autopostback">
                  <xsl:attribute name="autopostback">
                    <xsl:value-of select="@autopostback"/>
                  </xsl:attribute>
                </xsl:if>
              <xsl:if test="@itemsetref">
                <xsl:attribute name="ItemSetRef">
                  <xsl:value-of select="@itemsetref" />
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name="onchange">
                <xsl:choose>
                  <xsl:when test="@onchange">
                    <xsl:value-of select="@onchange" />
                  </xsl:when>
                </xsl:choose>
              </xsl:attribute>
              <xsl:if test="@readonly">
                <xsl:attribute name="Enabled">
                  <xsl:choose>
                    <xsl:when test="@readonly[.='True' or .='true']">False</xsl:when>
                    <xsl:otherwise>True</xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
              </xsl:if>
	      <xsl:if test="@PowerViewReadOnly">
                <xsl:attribute name="Enabled">
                  <xsl:choose>
                    <xsl:when test="@PowerViewReadOnly[.='True' or .='true']">False</xsl:when>
                    <xsl:otherwise>True</xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
              <xsl:for-each select="..//option">
                <asp:ListItem>
                  <xsl:attribute name="Value">
                    <xsl:value-of select="@value" />
                  </xsl:attribute>
                  <xsl:attribute name="Text">
                    <xsl:value-of select="."/>
                  </xsl:attribute>
                </asp:ListItem>
              </xsl:for-each>
            </asp:DropDownList>
            </span>
            <span class="formw">
            <asp:TextBox runat="server"  onchange="setDataChanged(true);" onfocus="phoneGotFocus(this);">
                  <xsl:attribute name="onblur">
                    <xsl:choose>
                      <xsl:when test="@onblur">
                        <xsl:value-of select="@onblur"/>
                      </xsl:when>
                      <xsl:otherwise>phoneLostFocus(this);</xsl:otherwise>
                    </xsl:choose>
                  </xsl:attribute>
                  <xsl:attribute name="id">
                    <xsl:value-of select="@textname"/>
                  </xsl:attribute>
                  <xsl:attribute name="RMXRef">
                    <xsl:value-of select="@textref"/>
                  </xsl:attribute>
                  <xsl:attribute name="RMXType">
                    <xsl:value-of select="@type"/>
                  </xsl:attribute>
                  <xsl:if test="@tabindex">
                    <xsl:attribute name="tabindex">
                      <xsl:value-of select="number(@tabindex) + 1" />
                    </xsl:attribute>
                  </xsl:if>
             <xsl:if test="(@PowerViewReadOnly[.='true'])">
                <xsl:attribute name="readonly">true</xsl:attribute>
                <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
              </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
                </asp:TextBox>
            </span>
          </xsl:when>
          <!--Added Rakhi for R7:Add Emp Data Elements-->
          <!--Parijat :changes for ssn-->
					<xsl:when test="@type[.='ssn']">
            <asp:TextBox runat="server"  onchange="setDataChanged(true);">
             <!--mbahl3 mits 29316-->
	            <xsl:attribute name="onblur"><xsl:choose><xsl:when test="@onblur"><xsl:value-of select="@onblur"/></xsl:when><xsl:otherwise>ssnLostFocus(this);</xsl:otherwise></xsl:choose></xsl:attribute>
              <xsl:attribute name="id">
                <xsl:value-of select="@name"/>
              </xsl:attribute>
              <xsl:attribute name="RMXRef">
                <xsl:value-of select="@ref"/>
              </xsl:attribute><xsl:attribute name="RMXType">
                <xsl:value-of select="@type"/>
              </xsl:attribute>
              <xsl:attribute name="name">
                <xsl:value-of select="@name"/>
              </xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="@tabindex" />
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name="countrymappedid"><xsl:value-of select="@countrymappedid"/></xsl:attribute>
               <xsl:if test="(@PowerViewReadOnly[.='true'])">
                <xsl:attribute name="readonly">true</xsl:attribute>
                <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
              </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
              <!--Parijat:taken care while transforming:<xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute>-->
            </asp:TextBox>
          </xsl:when>
					<xsl:when test="@type[.='taxid']"><asp:TextBox runat="server"  onchange="setDataChanged(true);" onblur="taxidLostFocus(this);"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute>
            <!-- pmittal5 Mits 17397 09/17/09 - Adding "tabindex" attribute-->
            <xsl:if test="@tabindex"><xsl:attribute name="tabindex"><xsl:value-of select="@tabindex" /></xsl:attribute>
            </xsl:if>
           <xsl:if test="(@PowerViewReadOnly[.='true'])">
                <xsl:attribute name="readonly">true</xsl:attribute>
                <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
              </xsl:if>
          <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
          </asp:TextBox>
          </xsl:when>
          <!--PJS : Made changes for numeric -->
          <xsl:when test="@type[.='numeric']">
            <asp:TextBox runat="server" >
              <xsl:attribute name="id">
                <xsl:value-of select="@name"/>
              </xsl:attribute>
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
              <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="@tabindex" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="((@readonly[.='true']) or (@PowerViewReadOnly[.='true']))">
                <xsl:attribute name="readonly">true</xsl:attribute>
                <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
              </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
              <xsl:if test="@maxlength">
                <xsl:attribute name="maxlength">
                  <xsl:value-of select="@maxlength"/>
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="@size">
                <xsl:attribute name="size">
                  <xsl:value-of select="@size" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="@fixed">
                <xsl:attribute name="fixed">
                  <xsl:value-of select="@fixed"/>
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="@min">
                <xsl:attribute name="min">
                  <xsl:value-of select="@min"/>
                </xsl:attribute>
              </xsl:if>
              <!--Mridul MITS#18230 Added MAX attribute-->
              <xsl:if test="@max">
                <xsl:attribute name="max">
                  <xsl:value-of select="@max" />
                </xsl:attribute>
              </xsl:if>
              <!-- MGaba2:MITS 16062:Added setDataChanged(true) in case some other function is also called-->
              <!--csingh7 : moved numlostfocus() from onblur to onchange-->
              <!--Mits 16505 Asif start-->
              <!--<xsl:attribute name="onChange">numLostFocus(this);<xsl:choose><xsl:when test="@onchange"><xsl:value-of select="@onchange"/>setDataChanged(true);</xsl:when><xsl:otherwise>setDataChanged(true);</xsl:otherwise></xsl:choose></xsl:attribute>-->
              <xsl:attribute name="onChange">numLostFocus(this);<xsl:choose><xsl:when test="@onchange"><xsl:value-of select="@onchange"/>;setDataChanged(true);</xsl:when><xsl:otherwise>setDataChanged(true);</xsl:otherwise></xsl:choose></xsl:attribute>
              <!--Mits 16505 Asif end-->
            </asp:TextBox>
          </xsl:when>
          <!--this type is added by Nitin for Mits 16583 on 14-May-2009-->
          <!--this will create a numeric textbox with SetDataChanged(False) on text change-->
          <xsl:when test="@type[.='numericWithNoDataChange']">
              <asp:TextBox runat="server" ><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute><xsl:if test="@fixed"><xsl:attribute name="fixed"><xsl:value-of select="@fixed"/></xsl:attribute></xsl:if><xsl:if test="@min"><xsl:attribute name="min"><xsl:value-of select="@min"/></xsl:attribute></xsl:if>
                <xsl:if test="((@readonly[.='true']) or (@PowerViewReadOnly[.='true']))"><xsl:attribute name="readonly">true</xsl:attribute><xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
                </xsl:if>
                <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
                <!-- MGaba2:MITS 16062:Added setDataChanged(true) in case some other function is also called-->
                <!--csingh7 : moved numlostfocus() from onblur to onchange-->
                <!-- Mits 16505:Asif Start-->
							<!--<xsl:attribute name="onChange">numLostFocus(this);	<xsl:choose><xsl:when test="@onchange"><xsl:value-of select="@onchange"/>setDataChanged(true);</xsl:when><xsl:otherwise>setDataChanged(true);</xsl:otherwise></xsl:choose></xsl:attribute>-->
                <xsl:attribute name="onChange">numLostFocus(this);	<xsl:choose><xsl:when test="@onchange"><xsl:value-of select="@onchange"/>;setDataChanged(false);</xsl:when><xsl:otherwise>setDataChanged(false);</xsl:otherwise></xsl:choose></xsl:attribute>
                <!-- Mits 16505:Asif End-->
                <xsl:if test="@tabindex">
                  <xsl:attribute name="tabindex">
                    <xsl:value-of select="@tabindex" />
                  </xsl:attribute>
                </xsl:if>
							</asp:TextBox>
            </xsl:when>
          <xsl:when test="@type[.='numericIntOnly']">
              <asp:TextBox runat="server" size="30" onblur="numLostFocus(this);">
                <xsl:attribute name="id">
                  <xsl:value-of select="@name" />
                </xsl:attribute>
                <xsl:attribute name="RMXRef">
                  <xsl:value-of select="@ref"/>
                </xsl:attribute>
                <xsl:if test="@tabindex">
                  <xsl:attribute name="tabindex">
                    <xsl:value-of select="@tabindex" />
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="@size">
                  <xsl:attribute name="size">
                    <xsl:value-of select="@size"/>
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="@maxlength">
                  <xsl:attribute name="maxlength">
                    <xsl:value-of select="@maxlength"/>
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="@fixed">
                  <xsl:attribute name="fixed">
                    <xsl:value-of select="@fixed" />
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="@min">
                  <xsl:attribute name="min">
                    <xsl:value-of select="@min" />
                  </xsl:attribute>
                </xsl:if>
                <xsl:attribute name="onchange">
                  AllowIntOnly(this);
                  <xsl:choose>
                    <xsl:when test="@onchange">
                      <xsl:value-of select="@onchange" />
                    </xsl:when>
                    <xsl:otherwise>setDataChanged(true);</xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
                <xsl:if test="(@PowerViewReadOnly[.='true'])">
                  <xsl:attribute name="readonly">true</xsl:attribute>
                  <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
                </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
              </asp:TextBox>
          </xsl:when>
          <xsl:when test="@type[.='currency']">
            <mc:CurrencyTextbox runat="server" >
              <xsl:attribute name="id">
                <xsl:value-of select="@name"/>
              </xsl:attribute>
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
              <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
              <!--<xsl:attribute name="onblur"><xsl:choose><xsl:when test="@onblur"><xsl:value-of select="@onblur"/></xsl:when></xsl:choose>;currencyLostFocus(this);</xsl:attribute>-->
              <!--Parijat: Mits 7257 :Attribute for allowing upto 3 decimal values-->
              <xsl:choose>
                <xsl:when test="@decimalValueUptoThree">
                  <xsl:choose>
                    <xsl:when test="@decimalValueUptoThree[.='true'] ">
                      <xsl:attribute name="rmxforms:as">currency_three</xsl:attribute>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:attribute name="rmxforms:as">currency</xsl:attribute>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:attribute name="rmxforms:as">currency</xsl:attribute>
                </xsl:otherwise>
              </xsl:choose>
              <!--/Parijat-->
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="@tabindex" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="@min">
                <!--Added by csingh7 Mits 15325-->
                <xsl:attribute name="min">
                  <xsl:value-of select="@min" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="@max"><xsl:attribute name="max"><xsl:value-of select="@max" /></xsl:attribute></xsl:if>
              <!--<xsl:attribute name="onchange"><xsl:choose><xsl:when test="@onchange"><xsl:value-of select="@onchange"/></xsl:when></xsl:choose>;setDataChanged(true);</xsl:attribute>-->
              <!--<xsl:attribute name="onChange">currencyLostFocus(this);<xsl:choose><xsl:when test="@onchange"><xsl:value-of select="@onchange"/>;setDataChanged(true);</xsl:when><xsl:otherwise>setDataChanged(true);</xsl:otherwise></xsl:choose>-->
                <xsl:attribute name="onChange">
                  <xsl:choose>
                    <xsl:when test="@onchange">
                      <xsl:value-of select="@onchange"/>;setDataChanged(true);
                    </xsl:when>
                    <xsl:otherwise>setDataChanged(true);</xsl:otherwise>
                  </xsl:choose>
              </xsl:attribute>
              <xsl:if test="((@readonly[.='true']) or (@PowerViewReadOnly[.='true']))">
                <!--Readonly attribute-Tanuj-->
                <xsl:attribute name="readonly">true</xsl:attribute>
                <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
              </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
              <xsl:if test="@size">
                <xsl:attribute name="size">
                  <xsl:value-of select="@size"/>
                </xsl:attribute>
              </xsl:if>
              <!--Deb Multi Currency-->
              <xsl:if test="@CurrencyMode">
                <xsl:attribute name="CurrencyMode">
                  <xsl:value-of select="@CurrencyMode" />
                </xsl:attribute>
              </xsl:if>
              <!--Deb Multi Currency-->
				<!--Rupal Multi Currency-->
				<xsl:if test="@onblur">
					<xsl:attribute name="onblur">
						<xsl:value-of select="@onblur" />
					</xsl:attribute>
				</xsl:if>
				<!--Rupal Multi Currency--> 	
            </mc:CurrencyTextbox>
            <xsl:if test="@lookupbutton">
              <input type="button" class="button">
                <xsl:attribute name="value">
                  <xsl:value-of select="@lookupbutton"/>
                </xsl:attribute>
                <xsl:attribute name="onclick">
                  <xsl:value-of select="@functionname"/>
                </xsl:attribute>
                <xsl:if test="@tabindex">
                  <xsl:attribute name="tabindex">
                    <xsl:value-of select="number(@tabindex)+1" />
                  </xsl:attribute>
                </xsl:if>
              <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                      <xsl:attribute name="disabled">true</xsl:attribute>
                  </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
              </input>
            </xsl:if>
            
            <!--<cc1:MaskedEditExtender runat="server"  MaskType="Number" Mask="9999999.99">
              <xsl:attribute name="TargetControlID"><xsl:value-of select="@name"/></xsl:attribute>
              <xsl:attribute name="id"><xsl:value-of select="@name"/>_ajax</xsl:attribute>
            </cc1:MaskedEditExtender>-->

            <!-- abhateja 12.22.2008 -->
            <!-- "Amount" control implementation for autochecks. -->
            <xsl:if test="@buttonscript">
              <input type="button" class="button">
                <xsl:attribute name="name">
                  <xsl:value-of select="@buttonscriptname"/>
                </xsl:attribute>
                <xsl:attribute name="value">
                  <xsl:value-of select="@buttonscripttitle"/>
                </xsl:attribute>
                <xsl:attribute name="onClick">
                  <xsl:value-of select="@functionname"/>
                </xsl:attribute>
              <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                      <xsl:attribute name="disabled">true</xsl:attribute>
                  </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
              </input>
            </xsl:if>
          </xsl:when>
          <!-- Shruti Adding control definitions from xforms-controls.xsl starts-->

          <xsl:when test="@type[.='linebreak']"><br /></xsl:when>

          <xsl:when test="@type[.='codewithdes']">
            <asp:TextBox runat="server"   onchange="lookupTextChanged(this);">
              <xsl:attribute name="id">
                <xsl:value-of select="@name"/>
              </xsl:attribute>
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
              <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
              <xsl:attribute name="cancelledvalue">
                <xsl:value-of select="text()"/>
              </xsl:attribute>
              <xsl:if test="((@readonly[.='true']) or (@PowerViewReadOnly[.='true']))">
                <xsl:attribute name="readonly">true</xsl:attribute>
              </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
              <xsl:choose>
                <xsl:when test="@parentnode">
                  <xsl:attribute name="onblur">
                    codeLostFocus(this.id,'<xsl:value-of select="@parentnode"/>');
                  </xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:attribute name="onblur">codeLostFocus(this.id);</xsl:attribute>
                </xsl:otherwise>
              </xsl:choose>
            </asp:TextBox>
            <xsl:if test="@creatable">
              <asp:TextBox style="display:none" runat="server" value="1">
                <xsl:attribute name="id">
                  <xsl:value-of select="@name"/>_creatable
                </xsl:attribute>
              </asp:TextBox>
            </xsl:if>
            <asp:button class="EllipsisControl" runat="Server">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
              <xsl:if test="@onServerClick"><xsl:attribute name="OnClick"><xsl:value-of select="@onServerClick"/></xsl:attribute>
              </xsl:if>
              <xsl:choose>
                <xsl:when test="@parentnode">
                  <xsl:attribute name="onclientclick">return selectCode('<xsl:value-of select="@codetable"/>','<xsl:value-of select="@name"/>');</xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:attribute name="onclientclick">return selectCode('<xsl:value-of select="@codetable"/>','<xsl:value-of select="@name"/>','','1');</xsl:attribute>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                      <xsl:attribute name="disabled">true</xsl:attribute>
                  </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:button>
            <asp:TextBox style="display:none" runat="server">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>_cid</xsl:attribute>
              <xsl:attribute name="cancelledvalue">
                <xsl:value-of select="@codeid"/>
              </xsl:attribute>
              <xsl:attribute name="oldvalue">
                <xsl:value-of select="@codeid"/>
              </xsl:attribute>
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/>/@codeid</xsl:attribute>
            </asp:TextBox>
          </xsl:when>

          <!-- abhateja 11.14.2008 *BEGIN* -->
          <!-- Implemented for autochecks screen-->
          <xsl:when test="@type[.='claimantlookupforclaim']">
            <asp:Textbox  runat ="server"  onchange="setDataChanged(true);" >
              <xsl:attribute name="RMXRef">
                <xsl:value-of select="@ref"/>
              </xsl:attribute>
              <xsl:attribute name="RMXType">
                <xsl:value-of select="@type"/>
              </xsl:attribute>
              <xsl:attribute name="id">
                <xsl:value-of select="@name" />
              </xsl:attribute>
            </asp:Textbox>
            <asp:button runat="server" class="EllipsisControl">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="number(@tabindex) + 1" />
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name="onclientclick">return OnClaimantLastNameClick();</xsl:attribute>
              <xsl:if test="((@readonly='yes') or (@PowerViewReadOnly[.='true']))" >
                <xsl:attribute name="disabled">true</xsl:attribute>
              </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:button>
          </xsl:when>

          <xsl:when test="@type[.='unitlookupforclaim']">
            <asp:Textbox  runat ="server"  onchange="setDataChanged(true);" >
              <xsl:attribute name="RMXRef">
                <xsl:value-of select="@ref"/>
              </xsl:attribute>
              <xsl:attribute name="RMXType">
                <xsl:value-of select="@type"/>
              </xsl:attribute>
              <xsl:attribute name="id">
                <xsl:value-of select="@name" />
              </xsl:attribute>
            </asp:Textbox>
            <asp:button runat="server" class="EllipsisControl">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="number(@tabindex) + 1" />
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name="onclientclick">return OnUnitLastNameClick();</xsl:attribute>
              <xsl:if test="((@readonly='yes') or (@PowerViewReadOnly[.='true']))" >
                <xsl:attribute name="disabled">true</xsl:attribute>
              </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:button>
          </xsl:when>
          <xsl:when test="@type[.='editpayeetaxid']">
            <asp:Textbox  runat ="server"  onchange="ssnLostFocus(this);">
              <xsl:if test="not(@cancelonblur[.='yes'])">
                <xsl:attribute name="onblur">lookupLostFocus(this);</xsl:attribute>
              </xsl:if>
              <xsl:attribute name="RMXRef">
                <xsl:value-of select="@ref"/>
              </xsl:attribute>
              <xsl:attribute name="RMXType">
                <xsl:value-of select="@type"/>
              </xsl:attribute>
              <xsl:attribute name="id">
                <xsl:value-of select="@name" />
              </xsl:attribute>
            <xsl:if test="(@PowerViewReadOnly[.='true'])">
                <xsl:attribute name="readonly">true</xsl:attribute>
                <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
                  </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:Textbox>
              <!--Ashish Ahuja Mits 30264-->
            <asp:button runat="server" ToolTip="Edit Tax ID" class="MemoButton">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="number(@tabindex) + 1" />
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name="onclientclick">return OnEditPayeeTaxId();</xsl:attribute>
              <xsl:if test="((@readonly='yes') or (@PowerViewReadOnly[.='true']))" >
                <xsl:attribute name="disabled">true</xsl:attribute>
              </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:button>
            <asp:button runat="server" ToolTip="Load Entity" class="EllipsisControl">
              <xsl:attribute name="id"><xsl:value-of select="'LoadEntity'"/>btn</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="number(@tabindex) + 1" />
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name="onclientclick">LoadPayeeEntity();return false;</xsl:attribute>
              <xsl:if test="((@readonly='yes') or (@PowerViewReadOnly[.='true']))" >
                <xsl:attribute name="disabled">true</xsl:attribute>
              </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:button>
          </xsl:when>
          <xsl:when test="@type[.='editpayeetaxidauto']">
            <asp:Textbox  runat ="server"  onchange="ssnLostFocus(this);" >
              <xsl:if test="not(@cancelonblur[.='yes'])">
                <xsl:attribute name="onblur">lookupLostFocus(this);</xsl:attribute>
              </xsl:if>
              <xsl:attribute name="RMXRef">
                <xsl:value-of select="@ref"/>
              </xsl:attribute>
              <xsl:attribute name="RMXType">
                <xsl:value-of select="@type"/>
              </xsl:attribute>
              <xsl:attribute name="id">
                <xsl:value-of select="@name" />
              </xsl:attribute>
            </asp:Textbox>
            <asp:button runat="server" ToolTip="Load Entity" class="EllipsisControl">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="number(@tabindex) + 1" />
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name="onclientclick">return OnEditPayeeTaxId();</xsl:attribute>
              <xsl:if test="((@readonly='yes') or (@PowerViewReadOnly[.='true']))" >
                <xsl:attribute name="disabled">true</xsl:attribute>
              </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:button>
          </xsl:when>
          <xsl:when test="@type[.='financiallastname']">
            <asp:Textbox  runat ="server"  onchange="lookupTextChanged(this);" >
              <xsl:if test="not(@cancelonblur[.='yes'])">
                <xsl:attribute name="onblur">lookupLostFocus(this);</xsl:attribute>
              </xsl:if>
              <xsl:if test="@orgLevel">
		             <xsl:attribute name="orgLevel">
		                <xsl:value-of select="@orgLevel" /> 
		             </xsl:attribute>
	            </xsl:if>
              <xsl:attribute name="RMXRef">
                <xsl:value-of select="@ref"/>
              </xsl:attribute>
              <xsl:attribute name="RMXType">
                <xsl:value-of select="@type"/>
              </xsl:attribute>
              <xsl:if test="@maxlength">
                <xsl:attribute name="maxlength">
                  <xsl:value-of select="@maxlength"/>
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name="id">
                <xsl:value-of select="@name" />
              </xsl:attribute>
              <!--Ashish Ahuja Mits 30264-->
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="number(@tabindex)+1" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])">
                <xsl:attribute name="readonly">true</xsl:attribute>
                <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
              </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:Textbox>
            <xsl:if test="@creatable">
              <asp:Textbox  runat ="server" style="display:none" Text="1">
                <xsl:attribute name="id"><xsl:value-of select="@name"/>_creatable</xsl:attribute>
              </asp:Textbox>
            </xsl:if>
            <asp:button runat="server" class="EllipsisControl">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="number(@tabindex) + 1" />
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name="onclientclick">return OnLastNameClick();</xsl:attribute>
              <xsl:if test="((@readonly='yes') or (@PowerViewReadOnly[.='true']))" >
                <xsl:attribute name="disabled">true</xsl:attribute>
                            </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:button>
            <xsl:if test="@withlink[.='yes']">
					<img src="img\spacer.gif" width="10" height="1"></img>
					<a href="#">
							<xsl:attribute name="name">
							  <xsl:value-of select="@name"/>
							</xsl:attribute>
							<xsl:attribute name="onClick">LinkHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>')</xsl:attribute>
							<xsl:attribute name="class">
							  <xsl:value-of select="@style"/>
							</xsl:attribute>
							<xsl:value-of select="@LinkTitle"/>
					</a>
			</xsl:if>
          </xsl:when>
          <xsl:when test="@type[.='textwithbuttonscript']">
              <asp:textbox runat="server">
                <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
                <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
                <xsl:if test="@tabindex"><xsl:attribute name="tabindex"><xsl:value-of select="@tabindex" /></xsl:attribute></xsl:if>
                <xsl:attribute name="onchange">
                  <xsl:choose><xsl:when test="@onchange"><xsl:value-of select="@onchange"/></xsl:when></xsl:choose>;setDataChanged(true);</xsl:attribute>
                <xsl:if test="((@readonly[.='true']) or (@PowerViewReadOnly[.='true']))">
                  <xsl:attribute name="readonly">true</xsl:attribute>
                  <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
                </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
              </asp:textbox>
              <xsl:if test="@buttonscript">
                <asp:button class="EllipsisControl" runat="server">
                  <xsl:attribute name="Text"><xsl:value-of select="@buttonscript"/></xsl:attribute>
                  <xsl:attribute name="onclientclick"><xsl:value-of select="@functionname"/></xsl:attribute>
                  <xsl:if test="@tabindex"><xsl:attribute name="tabindex"><xsl:value-of select="number(@tabindex)+1" /></xsl:attribute></xsl:if>
                </asp:button>
              </xsl:if>
            

          </xsl:when>
          <!-- abhateja 11.14.2008 *END* -->
          <!-- Shruti Adding control definitions from xforms-controls.xsl ends-->
          
          <xsl:when test="@type[.='entitylookup']">
            <asp:Textbox  runat ="server"  onchange="lookupTextChanged(this);" >
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="@tabindex" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="not(@cancelonblur[.='yes'])">
                <xsl:attribute name="onblur">lookupLostFocus(this);</xsl:attribute>
              </xsl:if>
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
              <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
              <xsl:attribute name="id">
                <xsl:value-of select="@name" />
              </xsl:attribute>
              <xsl:if test="((@locked[.='true']) or (@PowerViewReadOnly[.='true']))">
                <xsl:attribute name="readonly">true</xsl:attribute>
                <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
              </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
              <xsl:if test="@maxlength">
                <xsl:attribute name="maxlength">
                  <xsl:value-of select="@maxlength"/>
                </xsl:attribute>
              </xsl:if>

              <xsl:if test="@withlink[.='yes']">
                <xsl:attribute name="Text">
                  <xsl:value-of select="@title"/>
                </xsl:attribute>
                <xsl:attribute name="LinkTitle">
                  <xsl:value-of select="@LinkTitle"/>
                </xsl:attribute>
                <xsl:attribute name="article1">
                  <xsl:value-of select="@article1"/>
                </xsl:attribute>
                <xsl:attribute name="article2">
                  <xsl:value-of select="@article2"/>
                </xsl:attribute>
                
              </xsl:if>
            </asp:Textbox>
            <xsl:if test="@creatable">
              <asp:Textbox  runat ="server" style="display:none" Text="1">
                <xsl:attribute name="id"><xsl:value-of select="@name"/>_creatable</xsl:attribute>
              </asp:Textbox>
          </xsl:if>


            <!-- abhateja 10.31.2008 -->
            <!-- Replaced the lookupdataFDM implementation with the legacy lookupdata.We don't need lookupattachnodepath parameter any more.-->
            <asp:button runat="server" class="EllipsisControl">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="number(@tabindex) + 1" />
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name="onclientclick">return lookupData('<xsl:value-of select="@name"/>','<xsl:value-of select="@tableid"/>',4,'<xsl:value-of select="@fieldmark"/>',1);</xsl:attribute>
              <xsl:if test="@onServerClick">
                <xsl:attribute name="onclick"><xsl:value-of select="@onServerClick" /></xsl:attribute>
              </xsl:if>
              <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                <xsl:attribute name="disabled">true</xsl:attribute>
              </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:button>


            <!-- Added to support old entitylookupwithlink type    JP 8/11/2005 -->
            <xsl:if test="@withlink[.='yes']">
              <img src="img\spacer.gif" width="10" height="1"></img>
              <a href="#">
                <xsl:attribute name="name">
                  <xsl:value-of select="@name"/>
                </xsl:attribute>
                <xsl:attribute name="onClick">
                  LinkHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>')
                </xsl:attribute>
                <xsl:attribute name="class">
                  <xsl:value-of select="@style"/>
                </xsl:attribute>
                <xsl:value-of select="@LinkTitle"/>
              </a>
            </xsl:if>
            <!-- end  JP 8/11/2005 -->

          </xsl:when>
          
					<xsl:when test="@type[.='entitylookupwithLink']">
						<asp:TextBox runat="server"  onchange="lookupTextChanged(this);" onblur="lookupLostFocus(this);">
							<xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
							<xsl:attribute name="title"><xsl:value-of select="@title"/></xsl:attribute>
							<xsl:attribute name="LinkTitle"><xsl:value-of select="@LinkTitle"/></xsl:attribute>
							<xsl:attribute name="article1"><xsl:value-of select="@article1"/></xsl:attribute>
							<xsl:attribute name="article2"><xsl:value-of select="@article2"/></xsl:attribute>
							<xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute>
							<xsl:attribute name="cancelledvalue"><xsl:value-of select="text()"/></xsl:attribute>
							<xsl:attribute name="required"><xsl:value-of select="@required"/></xsl:attribute>
						</asp:TextBox>
						<xsl:if test="@creatable">
							<asp:TextBox style="display:none" runat="server">
								<xsl:attribute name="value"><xsl:value-of select="@creatable"/></xsl:attribute>
								<xsl:attribute name="name"><xsl:value-of select="@name"/>_creatable</xsl:attribute>
							</asp:TextBox>
						</xsl:if>
						<input type="button" class="EllipsisControl">
							<xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute>
							<xsl:attribute name="onClick">return lookupData('<xsl:value-of select="@name"/>','<xsl:value-of select="@tableid"/>',4,'<xsl:value-of select="@fieldmark"/>',1)</xsl:attribute>
						</input>
						<img src="img\spacer.gif" width="10" height="1"></img>
						<a href="#"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="onClick">LinkHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>')</xsl:attribute><xsl:attribute name="class"><xsl:value-of select="@style"/></xsl:attribute><xsl:value-of select="@LinkTitle"/></a>
          </xsl:when>
          <!--Parijat: changes for EmployeeLookup-->
					<xsl:when test="@type[.='employeelookup']">
            <asp:TextBox runat="server"  onchange="lookupTextChanged(this);" onblur="lookupLostFocus(this);">
              <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
              <xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
            <!--<xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute>-->
            <xsl:attribute name="cancelledvalue"><xsl:value-of select="text()"/></xsl:attribute>
              <!--MGaba2:MITS 14658 -->
            <xsl:if test="@tabindex"><xsl:attribute name="tabindex"><xsl:value-of select="@tabindex" /></xsl:attribute></xsl:if>
            <xsl:if test="((@readonly[.='true']) or (@PowerViewReadOnly[.='true']))">
                <xsl:attribute name="readonly">true</xsl:attribute>
                <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
                  </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:TextBox>
            <xsl:if test="@creatable">
              <asp:TextBox style="display:none" runat="server" value="1">
                <xsl:attribute name="id"><xsl:value-of select="@name"/>_creatable</xsl:attribute>
              </asp:TextBox>
            </xsl:if>
            <asp:button runat="server" class="EllipsisControl" >
            <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
            <!--Changed for Mits 18341:Default search not getting displayed-->
            <!--<xsl:attribute name="onclientclick">return lookupData('<xsl:value-of select="@name"/>','employees',3,'<xsl:value-of select="@fieldmark"/>',4)</xsl:attribute>-->
              <xsl:attribute name="onclientclick">return lookupData('<xsl:value-of select="@name"/>','employee',-1,'<xsl:value-of select="@fieldmark"/>',4)</xsl:attribute>
              <!--Changed for Mits 18341:Default search not getting displayed-->
            <xsl:if test="@tabindex"><xsl:attribute name="tabindex"><xsl:value-of select="number(@tabindex)+1" /></xsl:attribute>
              </xsl:if>
             <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                      <xsl:attribute name="disabled">true</xsl:attribute>
                  </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:button>
          </xsl:when>
          <!-- PJS : Made changes for eidlookup-->
          <xsl:when test="@type[.='eidlookup']">
            <asp:TextBox runat="server"  onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);">
              <xsl:attribute name="id">
                <xsl:value-of select="@name"/>
              </xsl:attribute>
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
              <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
              <xsl:attribute name="cancelledvalue">
                <xsl:value-of select="text()"/>
              </xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="@tabindex" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="@maxlength">
                <xsl:attribute name="maxlength">
                  <xsl:value-of select="@maxlength"/>
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])">
                <xsl:attribute name="readonly">true</xsl:attribute>
                <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
              </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:TextBox>
            <xsl:if test="@creatable">
              <asp:TextBox style="display:none" runat="server" value="1">
                <xsl:attribute name="name">
                  <xsl:value-of select="@name"/>_creatable</xsl:attribute>
              </asp:TextBox>
            </xsl:if>
            <asp:Button runat="server" class="EllipsisControl">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
              <xsl:if test="@tabindex"><xsl:attribute name="tabindex"><xsl:value-of select="number(@tabindex)+1" /></xsl:attribute></xsl:if>
              <xsl:choose>
                <xsl:when test="@fieldmark">
                  <xsl:choose>
                    <xsl:when test="@viewid">  <!--pmittal5 Mits 14475 - In case of Supervisor, show default Employee Search -->
                      <xsl:attribute name="onclientclick">
                        return lookupData('<xsl:value-of select="@name" />','<xsl:value-of select="@tableid" />','<xsl:value-of select="@viewid"/>','<xsl:value-of select="@fieldmark" />',1)
                      </xsl:attribute>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:attribute name="onclientclick">
                        return lookupData('<xsl:value-of select="@name" />','<xsl:value-of select="@tableid" />',4,'<xsl:value-of select="@fieldmark" />',1)
                      </xsl:attribute>    
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:choose> <!--pmittal5 Mits 14475 - In case of Supervisor, show default Employee Search -->
                    <xsl:when test="@viewid">
					  <!-- Mits id: 20750- Changed tableid from 'EMPLOYEE' to 'EMPLOYEES' -->
                      <!--<xsl:attribute name="onclientclick">return lookupData('<xsl:value-of select="@name" />','<xsl:value-of select="@tableid" />','<xsl:value-of select="@viewid"/>','<xsl:value-of select="@name" />',2)</xsl:attribute> -->
						<xsl:attribute name="onclientclick">return lookupData('<xsl:value-of select="@name" />','EMPLOYEES','<xsl:value-of select="@viewid"/>','<xsl:value-of select="@name" />',2)</xsl:attribute>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:attribute name="onclientclick">return lookupData('<xsl:value-of select="@name" />','<xsl:value-of select="@tableid" />',4,'<xsl:value-of select="@name" />',2)</xsl:attribute>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:otherwise>
              </xsl:choose>
             <xsl:if test="(@PowerViewReadOnly[.='true'])">
               <xsl:attribute name="disabled">true</xsl:attribute>
                  </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:Button>
		<!--spahariya MITS 30426 -->
		<xsl:if test="@entityNev[.='1']">			     
              <asp:button class="EntityNevButton" runat="server" ToolTip="Open">
                <xsl:attribute name="id"><xsl:value-of select="@name"/>_open</xsl:attribute>  
		<xsl:attribute name="onclientclick">return fnEntityNev('<xsl:value-of select="@name"/>','<xsl:value-of select="@entitytype" />')</xsl:attribute>
                <xsl:if test="@tabindex">
                  <xsl:attribute name="Tabindex"><xsl:value-of select="number(@tabindex)+2" /></xsl:attribute>
                </xsl:if>
                <xsl:if test="(@PowerViewReadOnly[.='true'])">
                  <xsl:attribute name="disabled">true</xsl:attribute>
                </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
              </asp:button>
            </xsl:if>
	    <!-- spahariya end -->
            <asp:TextBox style="display:none" runat="server">
              <xsl:attribute name="id">
                <xsl:value-of select="@name"/>_cid</xsl:attribute>
              <xsl:attribute name="RMXref">
                <xsl:value-of select="@ref"/>/@codeid</xsl:attribute>
              <xsl:attribute name="cancelledvalue">
                <xsl:value-of select="@codeid"/>
              </xsl:attribute>
            </asp:TextBox>
          </xsl:when>
   <!--ajohari2 Start : Worked on MITS - 34279 - Claimant lookup Enhancement-->
          <xsl:when test="@type[.='claimantentitylookup']">
            <asp:Textbox runat ="server"  onchange="lookupTextChanged(this);">
              <xsl:if test="@tabindex"><xsl:attribute name="tabindex"><xsl:value-of select="@tabindex"/></xsl:attribute></xsl:if>
              <xsl:if test="not(@cancelonblur[.='yes'])"><xsl:attribute name="onblur">lookupLostFocus(this);</xsl:attribute></xsl:if>
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
              <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
              <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
              <xsl:if test="((@locked[.='true']) or (@PowerViewReadOnly[.='true']))"><xsl:attribute name="readonly">true</xsl:attribute><xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute></xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:Textbox>
            <xsl:if test="@creatable"><asp:Textbox  runat ="server" style="display:none" Text="1"><xsl:attribute name="id"><xsl:value-of select="@name"/>_creatable</xsl:attribute></asp:Textbox></xsl:if>
            <asp:button runat="server" class="CodeLookupControl"><xsl:attribute name="id"><xsl:value-of select="@name"/>cmbtn</xsl:attribute><xsl:if test="@tabindex"><xsl:attribute name="tabindex"><xsl:value-of select="number(@tabindex) + 1"/></xsl:attribute></xsl:if>
              <xsl:attribute name="onclientclick">return lookupData('<xsl:value-of select="@name"/>','<xsl:value-of select="@listtableid"/><xsl:value-of select="@listparam"/>',4,'<xsl:value-of select="@fieldmark"/>',1);</xsl:attribute>
              <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                      <xsl:attribute name="disabled">true</xsl:attribute>
                  </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
              <xsl:if test="@onServerClick"><xsl:attribute name="onclick"><xsl:value-of select="@onServerClick"/></xsl:attribute></xsl:if></asp:button>
            <asp:button runat="server" class="EllipsisControl"><xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:if test="@tabindex"><xsl:attribute name="tabindex"><xsl:value-of select="number(@tabindex) + 1"/></xsl:attribute></xsl:if>
              <xsl:attribute name="onclientclick">return lookupData('<xsl:value-of select="@name"/>','<xsl:value-of select="@tableid"/>',4,'<xsl:value-of select="@fieldname"/>',2);</xsl:attribute><xsl:if test="@onServerClick"><xsl:attribute name="onclick"><xsl:value-of select="@onServerClick"/></xsl:attribute>
              </xsl:if><xsl:if test=" (@PowerViewReadOnly[.='true'])">
                      <xsl:attribute name="disabled">true</xsl:attribute>
                  </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:button>
            <xsl:if test="@withlink[.='yes']"><img src="img\spacer.gif" width="10" height="1"></img><a href="#"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:choose><xsl:when test="@Retval"><xsl:if test="@Retval[.='1']">
                      <xsl:attribute name="onClick">LinkHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>');return false;</xsl:attribute></xsl:if>
                  </xsl:when><xsl:otherwise ><xsl:attribute name="onClick">LinkHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>');</xsl:attribute>
                  </xsl:otherwise>
                </xsl:choose>
                <xsl:attribute name="class"><xsl:value-of select="@style"/></xsl:attribute><xsl:value-of select="@LinkTitle"/></a>
            </xsl:if>
            <asp:TextBox style="display:none" runat="server"><xsl:attribute name="id"><xsl:value-of select="@name"/>_cid</xsl:attribute>
              <xsl:attribute name="RMXref"><xsl:value-of select="@ref"/>/@codeid</xsl:attribute></asp:TextBox>
          </xsl:when>
          <!--ajohari2 End : Worked on MITS - 34279 - Claimant lookup Enhancement-->       
          <!--Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement-->
          <xsl:when test="@type[.='claimadjusterlookup']">
            <asp:TextBox runat="server"  onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);">
              <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
              <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex"><xsl:value-of select="@tabindex" /></xsl:attribute>
              </xsl:if>
              <xsl:if test="@maxlength">
                <xsl:attribute name="maxlength">
                  <xsl:value-of select="@maxlength"/>
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])">
                <xsl:attribute name="readonly">true</xsl:attribute>
                <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
              </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:TextBox>
            <asp:button runat="server" class="EllipsisControl">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex"><xsl:value-of select="number(@tabindex) + 1" /></xsl:attribute>
              </xsl:if>
              <xsl:attribute name="onclientclick">return lookupData('<xsl:value-of select="@name"/>','ADJUSTERS',7,'<xsl:value-of select="@name"/>',2);</xsl:attribute>
              <xsl:if test="@onServerClick">
                <xsl:attribute name="onclick"><xsl:value-of select="@onServerClick" /></xsl:attribute>
              </xsl:if>
              <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                <xsl:attribute name="disabled">true</xsl:attribute>
              </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:button>
            <asp:TextBox style="display:none" runat="server">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>_cid</xsl:attribute>
              <xsl:attribute name="RMXref"><xsl:value-of select="@ref"/>/@codeid</xsl:attribute>
            </asp:TextBox>
          </xsl:when>
          <!--Ankit End-->
	  <xsl:when test="@type[.='SuppTypeVehicleLookup']">
						<asp:TextBox runat="server"  onchange="lookupTextChanged(this);" onblur="lookupLostFocus(this);">
							<xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
              <xsl:attribute name="tabindex"><xsl:value-of select="@tabindex"/></xsl:attribute>
							<xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute>
							<xsl:attribute name="cancelledvalue"><xsl:value-of select="text()"/></xsl:attribute>
              <xsl:if test="@PowerViewReadOnly[.='true']">
                <xsl:attribute name="readonly">true</xsl:attribute>
                <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
              </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
						</asp:TextBox>
						<xsl:if test="@creatable">
							<asp:TextBox style="display:none" runat="server">
								<xsl:attribute name="value"><xsl:value-of select="@creatable"/></xsl:attribute>
								<xsl:attribute name="id"><xsl:value-of select="@name"/>_creatable</xsl:attribute>
							</asp:TextBox>
						</xsl:if>


            <!-- abhateja 10.31.2008 -->
            <!-- Changed the input button control to aspx.-->
            <asp:button runat="server" class="EllipsisControl">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="number(@tabindex) + 1" />
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name="onclientclick">return lookupData('<xsl:value-of select="@name"/>','vehicle',5,'<xsl:value-of select="@name"/>',8);</xsl:attribute>
              <xsl:if test="@onServerClick">
                <xsl:attribute name="onclick"><xsl:value-of select="@onServerClick" /></xsl:attribute>
              </xsl:if>
              <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                <xsl:attribute name="disabled">true</xsl:attribute>
              </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:button>
            <!-- input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">lookupData('<xsl:value-of select="@name"/>','vehicle',5,'<xsl:value-of select="@fieldmark"/>',10)</xsl:attribute></input -->
          </xsl:when>
					<xsl:when test="@type[.='vehiclelookup']">
						<asp:TextBox runat="server"  onchange="lookupTextChanged(this);" onblur="lookupLostFocus(this);">
							<xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
							<xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute>
							<xsl:attribute name="cancelledvalue"><xsl:value-of select="text()"/></xsl:attribute>
              <xsl:attribute name="tabindex"><xsl:value-of select="@tabindex"/></xsl:attribute>
              <xsl:if test="@PowerViewReadOnly[.='true']"><xsl:attribute name="readonly">true</xsl:attribute><xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute></xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
						</asp:TextBox>
						<xsl:if test="@creatable">
							<asp:TextBox style="display:none" runat="server">
								<xsl:attribute name="value"><xsl:value-of select="@creatable"/></xsl:attribute>
								<xsl:attribute name="id"><xsl:value-of select="@name"/>_creatable</xsl:attribute>
							</asp:TextBox>
						</xsl:if>


            <!-- abhateja 10.31.2008 -->
            <!-- Changed the input button control to aspx.-->
            <asp:button runat="server" class="EllipsisControl">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="number(@tabindex) + 1" />
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name="onclientclick">return lookupData('<xsl:value-of select="@name"/>','vehicle',5,'<xsl:value-of select="@fieldmark"/>',10);</xsl:attribute>
              <xsl:if test="@onServerClick">
                <xsl:attribute name="onclick"><xsl:value-of select="@onServerClick" /></xsl:attribute>
              </xsl:if>
              <xsl:if test=" (@PowerViewReadOnly[.='true'])"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:button>
          <!--Add div by kuladeep for rmA14.1 performance change -->
             <xsl:if test="@divrequired[.='yes']">
                 <div runat="server">
                   <xsl:attribute name="class">
                   <xsl:value-of select="@class"></xsl:value-of>
                   </xsl:attribute>
                   <xsl:attribute name="id">
                   <xsl:value-of select="@name"/>_codelookup_div</xsl:attribute>
                 <xsl:attribute name="style">display:none</xsl:attribute>
                 </div>
          </xsl:if> 
            <!-- input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">lookupData('<xsl:value-of select="@name"/>','vehicle',5,'<xsl:value-of select="@fieldmark"/>',10)</xsl:attribute></input -->
          </xsl:when>
					<xsl:when test="@type[.='claimnumberlookup']"><asp:TextBox runat="server"  onchange="lookupTextChanged(this);" onblur="lookupLostFocus(this);"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute><xsl:attribute name="cancelledvalue"><xsl:value-of select="text()"/></xsl:attribute>
            <xsl:attribute name="tabindex"><xsl:value-of select="@tabindex"/></xsl:attribute>
            <xsl:if test="@PowerViewReadOnly[.='true']">
              <xsl:attribute name="readonly">true</xsl:attribute>
              <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
            </xsl:if>
          <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
          </asp:TextBox><xsl:if test="@creatable"><asp:TextBox style="display:none" runat="server" value="1"><xsl:attribute name="name"><xsl:value-of select="@name"/>_creatable</xsl:attribute>
            </asp:TextBox>
          </xsl:if>
		  		  
		    <asp:button runat="server" class="EllipsisControl">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="number(@tabindex) + 1" />
                </xsl:attribute>
              </xsl:if>
	      <!--MITS 19424: yatharth : Changed 1 -> -1... This is the ViewId as taken by the lookupData -->
              <xsl:attribute name="onclientclick">return lookupData('<xsl:value-of select="@name"/>','claim',-1,'<xsl:value-of select="@name"/>',6);</xsl:attribute>
              <xsl:if test="@onServerClick">
                <xsl:attribute name="onclick"><xsl:value-of select="@onServerClick" /></xsl:attribute>
              </xsl:if>
          <xsl:if test=" (@PowerViewReadOnly[.='true'])">
            <xsl:attribute name="disabled">true</xsl:attribute>
          </xsl:if>
        <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:button>
		  
		  
		  </xsl:when>
          <xsl:when test="@type[.='payeeaddresslookup']">
            <asp:TextBox runat="server"  onchange="lookupTextChanged(this);" onblur="lookupLostFocus(this);">
              <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
              <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
              <xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute>
              <xsl:attribute name="cancelledvalue"><xsl:value-of select="text()"/></xsl:attribute>
              <xsl:attribute name="tabindex"><xsl:value-of select="@tabindex"/></xsl:attribute>
              <xsl:if test="(@PowerViewReadOnly[.='true'])">
                <xsl:attribute name="readonly">true</xsl:attribute>
                <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
              </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
              <xsl:if test="@maxlength">
                <xsl:attribute name="maxlength">
                  <xsl:value-of select="@maxlength"/>
                </xsl:attribute>
              </xsl:if>
            </asp:TextBox>
            <xsl:if test="@creatable">
              <asp:TextBox style="display:none" runat="server" value="1">
                <xsl:attribute name="name"><xsl:value-of select="@name"/>_creatable</xsl:attribute>
              </asp:TextBox>
            </xsl:if>

            <asp:button runat="server" class="EllipsisControl">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
              <xsl:if test="@tabindex"><xsl:attribute name="tabindex"><xsl:value-of select="number(@tabindex) + 1" /></xsl:attribute>
              </xsl:if>
              <!--MITS 19424: yatharth : Changed 1 -> -1... This is the ViewId as taken by the lookupData -->
              <xsl:attribute name="onclientclick">doLookupaddress('<xsl:value-of select="@name"/>','<xsl:value-of select="@EntityIdCtrl"/>');return false;</xsl:attribute>
              <xsl:if test="@onServerClick"><xsl:attribute name="onclick"><xsl:value-of select="@onServerClick" /></xsl:attribute></xsl:if>
              <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                <xsl:attribute name="disabled">true</xsl:attribute>
              </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:button>


          </xsl:when>
		  <!--Start:Added by Nitin Goel for Policy Lookup for Admin Tracking,03/22/2010,MITS 20373-->
		  
		  <xsl:when test="@type[.='SuppTypePolicyManagementLookup']"> 
		  <asp:TextBox runat="server"  onchange="lookupTextChanged(this);" onblur="lookupLostFocus(this);"> 
		  <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute> 
		  <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute> 
		  <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute> 
		  <xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute> 
		  <xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute> 
		  <xsl:attribute name="cancelledvalue"><xsl:value-of select="text()"/></xsl:attribute>
            <xsl:attribute name="tabindex"><xsl:value-of select="@tabindex"/></xsl:attribute>
        <xsl:if test="@PowerViewReadOnly[.='true']">
                    <xsl:attribute name="Enabled">false</xsl:attribute>
                    <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
                  </xsl:if>
      <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
          </asp:TextBox>
	  <xsl:if test="@creatable">
	  <asp:TextBox style="display:none" runat="server" value="1">
	  <xsl:attribute name="name"><xsl:value-of select="@name"/>_creatable</xsl:attribute>
            </asp:TextBox>
          </xsl:if>
		  		  
		    <asp:button runat="server" class="EllipsisControl">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="number(@tabindex) + 1" />
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name="onclientclick">return lookupData('<xsl:value-of select="@name"/>','policyenh',-1,'<xsl:value-of select="@name"/>',20);</xsl:attribute>
              <xsl:if test="@onServerClick">
                <xsl:attribute name="onclick"><xsl:value-of select="@onServerClick" /></xsl:attribute>
              </xsl:if>
          <xsl:if test=" (@PowerViewReadOnly[.='true'])">
            <xsl:attribute name="disabled">true</xsl:attribute>
          </xsl:if>
        <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:button>
          		  
		  </xsl:when>

		<!--Rename the name of Policy Number Lookup to Policy Tracking Lookup,05/05/2010,MITS 20618-->
		<!-- <xsl:when test="@type[.='SuppTypePolicyLookup']"> -->
		<xsl:when test="@type[.='SuppTypePolicyTrackingLookup']">		  
		  <asp:TextBox runat="server"  onchange="lookupTextChanged(this);" onblur="lookupLostFocus(this);"> 
		  <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute> 
		  <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute> 
		  <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute> 
		  <xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute> 
		  <xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute> 
		  <xsl:attribute name="cancelledvalue"><xsl:value-of select="text()"/></xsl:attribute>
            <xsl:attribute name="tabindex"><xsl:value-of select="@tabindex"/></xsl:attribute>
        <xsl:if test="@PowerViewReadOnly[.='true']">
          <xsl:attribute name="Enabled">false</xsl:attribute>
          <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
        </xsl:if>
      <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
          </asp:TextBox>
	  <xsl:if test="@creatable">
	  <asp:TextBox style="display:none" runat="server" value="1">
	  <xsl:attribute name="name"><xsl:value-of select="@name"/>_creatable</xsl:attribute>
            </asp:TextBox>
          </xsl:if>
		  		  
		    <asp:button runat="server" class="EllipsisControl">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="number(@tabindex) + 1" />
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name="onclientclick">return lookupData('<xsl:value-of select="@name"/>','policy',-1,'<xsl:value-of select="@name"/>',9);</xsl:attribute>
              <xsl:if test="@onServerClick">
                <xsl:attribute name="onclick"><xsl:value-of select="@onServerClick" /></xsl:attribute>
              </xsl:if>
          <xsl:if test=" (@PowerViewReadOnly[.='true'])">
            <xsl:attribute name="disabled">true</xsl:attribute>
          </xsl:if>
        <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:button>
         
		  </xsl:when>
		 
		  <!--End: Nitin Goel for Policy Lookup for Admin Tracking,03/22/2010,MITS 20373-->

					<xsl:when test="@type[.='eventnumberlookup']"><asp:TextBox runat="server"  onchange="lookupTextChanged(this);" onblur="lookupLostFocus(this);"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute><xsl:attribute name="cancelledvalue"><xsl:value-of select="text()"/></xsl:attribute>
            <xsl:attribute name="tabindex"><xsl:value-of select="@tabindex"/></xsl:attribute>
            <xsl:if test="@PowerViewReadOnly[.='true']"><xsl:attribute name="Enabled">false</xsl:attribute>
              <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
            </xsl:if>
          <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
          </asp:TextBox><xsl:if test="@creatable"><asp:TextBox style="display:none" runat="server" value="1"><xsl:attribute name="name"><xsl:value-of select="@name"/>_creatable</xsl:attribute>
            </asp:TextBox>
          </xsl:if><input type="button" class="EllipsisControl"><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">return lookupData('<xsl:value-of select="@name"/>','event',2,'<xsl:value-of select="@name"/>',7)</xsl:attribute>
            <xsl:if test="@tabindex"><xsl:attribute name="tabindex"><xsl:value-of select="number(@tabindex)+1" /></xsl:attribute>
            </xsl:if>
            <xsl:if test=" (@PowerViewReadOnly[.='true'])">
              <xsl:attribute name="disabled">true</xsl:attribute>
            </xsl:if>
          <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
          </input>
          </xsl:when>
					<xsl:when test="@type[.='eventlookup']"><asp:TextBox runat="server"  onchange="lookupTextChanged(this);" onblur="lookupLostFocus(this);"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute><xsl:if test="@tabindex"><xsl:attribute name="tabindex"><xsl:value-of select="@tabindex" /></xsl:attribute></xsl:if><xsl:if test="@readonly"><xsl:attribute name="readonly">true</xsl:attribute><xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute></xsl:if><xsl:attribute name="cancelledvalue"><xsl:value-of select="text()"/></xsl:attribute></asp:TextBox><xsl:if test="@creatable"><asp:TextBox style="display:none" runat="server" value="1"><xsl:attribute name="name"><xsl:value-of select="@name"/>_creatable</xsl:attribute>
            </asp:TextBox>
          </xsl:if>
            <asp:button runat="server" class="button">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>_Open</xsl:attribute>
              <xsl:attribute name="Text">Open</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="number(@tabindex) + 1" />
                </xsl:attribute>
              </xsl:if>
            </asp:button>  
            <xsl:if test="@lookupbutton[.='1']">
              <!-- abhateja 10.31.2008 -->
              <!-- Changed the input button control to aspx.-->
              <asp:button runat="server" class="EllipsisControl">
                <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
                <xsl:if test="@tabindex">
                  <xsl:attribute name="tabindex">
                    <xsl:value-of select="number(@tabindex) + 1" />
                  </xsl:attribute>
                </xsl:if>
                <xsl:attribute name="onclientclick">return lookupData('<xsl:value-of select="@name"/>','event',2,'<xsl:value-of select="@fieldmark"/>',11);</xsl:attribute>
                <xsl:if test="@onServerClick">
                  <xsl:attribute name="onclick"><xsl:value-of select="@onServerClick" /></xsl:attribute>
                </xsl:if>
              </asp:button>
              
              <!-- input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">lookupData('<xsl:value-of select="@name"/>','event',2,'<xsl:value-of select="@fieldmark"/>',11)</xsl:attribute></input -->
						</xsl:if>
          </xsl:when>
					<xsl:when test="@type[.='vehiclenumberlookup']"><asp:TextBox runat="server"  onchange="lookupTextChanged(this);" onblur="lookupLostFocus(this);"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute><xsl:attribute name="cancelledvalue"><xsl:value-of select="text()"/></xsl:attribute>
          </asp:TextBox><xsl:if test="@creatable"><asp:TextBox style="display:none" runat="server" value="1"><xsl:attribute name="name"><xsl:value-of select="@name"/>_creatable</xsl:attribute>
            </asp:TextBox>
          </xsl:if><input type="button" class="EllipsisControl"><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">return lookupData('<xsl:value-of select="@name"/>','vehicle',5,'<xsl:value-of select="@name"/>',8)</xsl:attribute></input>
          </xsl:when>
					<!--MITS:26975, removed call to function eatkeystrokes <xsl:when test="@type[.='policynumberlookup']"><asp:TextBox runat="server"  onchange="setDataChanged(true);" onkeydown="eatKeystrokes();"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute>-->
          <!-- Added onblur event to reflect the change in textbox value to the corresponding hidden cid control, so that after clicking on Save button it is reflected properly - aaggarwal29; MITS 26975 -->
					<xsl:when test="@type[.='policynumberlookup']"><asp:TextBox runat="server"  onchange="setDataChanged(true);" onblur="codeLostFocus(this.name);"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute>
          
   <xsl:if test="(@PowerViewReadOnly[.='true'])">
                <xsl:attribute name="readonly">true</xsl:attribute>
                <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
                  </xsl:if>
          <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
          </asp:TextBox>
          <asp:TextBox style="display:none" runat="server">
            <xsl:attribute name="id"><xsl:value-of select="@name"/>_cid</xsl:attribute><xsl:attribute name="RMXRef"><xsl:choose><xsl:when test="@idref"><xsl:value-of select="@idref"/></xsl:when><xsl:otherwise><xsl:value-of select="@ref"/></xsl:otherwise></xsl:choose></xsl:attribute>
          </asp:TextBox>
                        <asp:button runat="server" class="EllipsisControl">
                            <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
                           <!-- <xsl:attribute name="onclientclick">return lookupData('<xsl:value-of select="@name"/>','policy',6,'<xsl:value-of select="@name"/>',9);</xsl:attribute> -->
                            <!--<xsl:if test="@onServerClick">
                                <xsl:attribute name="onclick">
                                    <xsl:value-of select="@onServerClick" />
                                </xsl:attribute>
                            </xsl:if>-->
                            <xsl:if test="@tabindex">
                                <xsl:attribute name="Tabindex"><xsl:value-of select="number(@tabindex)+1" /></xsl:attribute>
                            </xsl:if>
                        <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                      <xsl:attribute name="disabled">true</xsl:attribute>
                  </xsl:if>
                        <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
                        </asp:button>
         <!-- <input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">return lookupData('<xsl:value-of select="@name"/>','policy',6,'<xsl:value-of select="@name"/>',9)</xsl:attribute></input>-->
          </xsl:when>

          <!--Amandeep Catastrophe Enhancement MITS 28528: Start-->
          <xsl:when test="@type[.='catastrophelookup']">
            <asp:TextBox runat="server"  onblur="lookupCatastrophes(this.name, false);" onchange="setDataChanged(true);">
              <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
              <xsl:attribute name="RMXType">
                <xsl:value-of select="@type"/>
              </xsl:attribute>
              <xsl:attribute name="CodeTable">
                <xsl:value-of select="@codetable"/>
              </xsl:attribute>             
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="@tabindex" />
                </xsl:attribute>
              </xsl:if>
            </asp:TextBox>
            <asp:button class="EllipsisControl" runat="server">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="Tabindex">
                  <xsl:value-of select="number(@tabindex)+1" />
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name="onclientclick">lookupCatastrophes('<xsl:value-of select="@name"/>', true);return false;</xsl:attribute>
            </asp:button>
            <asp:TextBox style="display:none" runat="server">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>_cid</xsl:attribute>
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/>/@codeid</xsl:attribute>
            </asp:TextBox>
          </xsl:when>
          <!--Amandeep Catastrophe Enhancement MITS 28528: End-->
          <!--amitosh-->
          <xsl:when test="@type[.='addresslookup']">
            <asp:TextBox runat="server">
              <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
              <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
              <xsl:attribute name="Readonly">true</xsl:attribute>
              <xsl:attribute name="BackColor">#F2F2F2</xsl:attribute>
              <xsl:attribute name="TextMode">MultiLine</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="Tabindex">
                  <xsl:value-of select="number(@tabindex)" />
                </xsl:attribute>
              </xsl:if>
             <xsl:if test="(@PowerViewReadOnly[.='true'])">
                <xsl:attribute name="readonly">true</xsl:attribute>
                <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
                  </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:TextBox>
            
            <asp:button class="EllipsisControl" runat="server">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="Tabindex">
                  <xsl:value-of select="number(@tabindex)+1" />
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name="onclientclick">doLookupaddress('<xsl:value-of select="@name"/>','<xsl:value-of select="@EntityIdCtrl"/>');return false;
              </xsl:attribute>
            <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                      <xsl:attribute name="disabled">true</xsl:attribute>
                  </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:button>

            <asp:button runat="server" class="BtnRemove">
              <xsl:attribute name="id"><xsl:value-of select="@name" />btndel</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="number(@tabindex)+2" />
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name="onclientclick">return deleteSelectedAddress('<xsl:value-of select="@name"/>')
              </xsl:attribute>
              <xsl:attribute name="Text">-</xsl:attribute>
            <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                      <xsl:attribute name="disabled">true</xsl:attribute>
                  </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:button>
            <asp:Textbox runat="server" style="display:none">
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/>/@codeid</xsl:attribute>
              <xsl:attribute name="id"><xsl:value-of select="@name"/>_lst</xsl:attribute>
            </asp:Textbox>
          
          </xsl:when>

          <!--end Amitosh-->
          <xsl:when test="@type[.='multipolicylookup']">
            <asp:listbox runat="server">
              <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
              <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
              <xsl:attribute name="Readonly">true</xsl:attribute>
              <xsl:attribute name="BackColor">#F2F2F2</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="Tabindex">
                  <xsl:value-of select="number(@tabindex)" />
                </xsl:attribute>
              </xsl:if>
            </asp:listbox>
            <asp:button class="EllipsisControl" runat="server">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="Tabindex"><xsl:value-of select="number(@tabindex)+1" /></xsl:attribute>
              </xsl:if>
              <xsl:attribute name="onclientclick">lookupClaimPolicy('<xsl:value-of select="@name"/>','<xsl:value-of select="@type"/>');return false;</xsl:attribute>
             <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:button>

            <asp:button runat="server" class="BtnRemove" ToolTip="Remove">
              <xsl:attribute name="id"><xsl:value-of select="@name" />btndel</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex"><xsl:value-of select="number(@tabindex)+2" /></xsl:attribute>
              </xsl:if>
              <xsl:attribute name="onclientclick">return deleteSelectedPolicy('<xsl:value-of select="@name"/>')</xsl:attribute>
              <xsl:attribute name="Text">-</xsl:attribute>
             <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:button>
            <asp:Textbox runat="server" style="display:none">
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/>/@codeid</xsl:attribute>
              <xsl:attribute name="id"><xsl:value-of select="@name"/>_lst</xsl:attribute>
            </asp:Textbox>
            <xsl:if test="@linktobutton[.='1']">
			      <!--Added by Amitosh for Policy interface-->
		        <asp:button class="PolicyDownloadButton" runat="server" ToolTip="Policy Search">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>PSDownloadbtn</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="Tabindex"><xsl:value-of select="number(@tabindex)+1" /></xsl:attribute>
              </xsl:if>
              <xsl:attribute name="onclientclick">openPolicySystemDownload('<xsl:value-of select="@name"/>');return false;</xsl:attribute>
             <xsl:if test=" (@PowerViewReadOnly[.='true'])">
               <xsl:attribute name="disabled">true</xsl:attribute>
                  </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:button>
              <asp:button class="PolicyButton" runat="server" ToolTip="Open">
                <xsl:attribute name="id"><xsl:value-of select="@name"/>_open</xsl:attribute>
                <!--<xsl:attribute name="value"><xsl:value-of select="@button"/></xsl:attribute>-->
                  <!--<xsl:attribute name="Text"><xsl:value-of select="@button"/></xsl:attribute>-->
                <!--<xsl:attribute name="OnClientClick">if(!( XFormHandler('<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;</xsl:attribute>-->
                <!--<xsl:attribute name="PostBackUrl">
                  <xsl:value-of select="@pagetomove"/>?<xsl:value-of select="@param"/>
                </xsl:attribute>
                <xsl:if test="@onServerClick">
                  <xsl:attribute name="onclick">
                    <xsl:value-of select="@onServerClick" />
                  </xsl:attribute>
                </xsl:if>-->
                <xsl:if test="@tabindex">
                  <xsl:attribute name="Tabindex"><xsl:value-of select="number(@tabindex)+2" /></xsl:attribute>
                </xsl:if>
               <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                 <xsl:attribute name="disabled">true</xsl:attribute>
                  </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
              </asp:button>
            </xsl:if>
          <asp:button runat="server" class="OpenMediaView" ToolTip="Open Media View">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>btnopenmediaview</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="Tabindex"><xsl:value-of select="number(@tabindex)+4" /></xsl:attribute>
              </xsl:if>
             <xsl:choose>
             <xsl:when test="@showmediaviewbutton"><xsl:attribute name="Visible"><xsl:value-of select="@showmediaviewbutton" /></xsl:attribute></xsl:when>
             <xsl:otherwise><xsl:attribute name="Visible">false</xsl:attribute></xsl:otherwise>
             </xsl:choose>
             <xsl:attribute name="onclientclick">return CheckPolicySelection();</xsl:attribute>
           <xsl:if test=" (@PowerViewReadOnly[.='true'])">
             <xsl:attribute name="disabled">true</xsl:attribute>
                  </xsl:if>
          <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:button>
            <asp:button runat="server" class="OpenPointPolicy" ToolTip="Open Policy In Point System">
              <xsl:attribute name="id">
                <xsl:value-of select="@name"/>btnopenpointpolicy</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="Tabindex">
                  <xsl:value-of select="number(@tabindex)+5" /></xsl:attribute>
              </xsl:if>
              <xsl:choose>
                <xsl:when test="@showpolicyinpoint">
                  <xsl:attribute name="Visible">
                    <xsl:value-of select="@showpolicyinpoint" /></xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:attribute name="Visible">false</xsl:attribute>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:attribute name="onclientclick">return OpenPointPolicy();</xsl:attribute>
             <xsl:if test=" (@PowerViewReadOnly[.='true'])">
               <xsl:attribute name="disabled">true</xsl:attribute>
                  </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:button>
          </xsl:when>

          <xsl:when test="@type[.='multiunitlookup']">
            <asp:listbox runat="server">
              <xsl:attribute name="id">
                <xsl:value-of select="@name"/>
              </xsl:attribute>
              <xsl:attribute name="RMXRef">
                <xsl:value-of select="@ref"/>
              </xsl:attribute>
              <xsl:attribute name="RMXType">
                <xsl:value-of select="@type"/>
              </xsl:attribute>
              <xsl:attribute name="Readonly">true</xsl:attribute>
              <xsl:attribute name="BackColor">#F2F2F2</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="Tabindex">
                  <xsl:value-of select="number(@tabindex)" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="@onchange">
                <xsl:attribute name="onchange"><xsl:value-of select="@onchange" />
                </xsl:attribute>
              </xsl:if>
              
            </asp:listbox>
            <asp:button class="EllipsisControl" runat="server">
              <xsl:attribute name="id">
                <xsl:value-of select="@name"/>btn</xsl:attribute>
                          
              <xsl:if test="@tabindex">
                <xsl:attribute name="Tabindex">
                  <xsl:value-of select="number(@tabindex)+1" /></xsl:attribute>
              </xsl:if>
            <xsl:if test=" (@PowerViewReadOnly[.='true'])"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
              <xsl:attribute name="onclientclick">
                lookupUnitTypes('<xsl:value-of select="@name"/>','<xsl:value-of select="@type"/>');return false;</xsl:attribute>
            </asp:button>
            <asp:button runat="server" class="BtnRemove">
              <xsl:attribute name="id">
                <xsl:value-of select="@name" />btndel</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="number(@tabindex)+2" />
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name="onclientclick">
                return deleteSelectedPolicy('<xsl:value-of select="@name"/>')
              </xsl:attribute>
             <xsl:if test=" (@PowerViewReadOnly[.='true'])"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
              <xsl:attribute name="Text">-</xsl:attribute>
            </asp:button>
            <asp:Textbox runat="server" style="display:none">
              <xsl:attribute name="RMXRef">
                <xsl:value-of select="@ref"/>/@codeid</xsl:attribute>
              <xsl:attribute name="id"><xsl:value-of select="@name"/>_lst</xsl:attribute>
            </asp:Textbox>
          </xsl:when>

	<!--added by Yukti for MITS 35772-->
	<xsl:when test="@type[.='multiunitlistbox']">
            <asp:listbox runat="server">
              <xsl:attribute name="id">
                <xsl:value-of select="@name"/>
              </xsl:attribute>
              <xsl:attribute name="RMXRef">
                <xsl:value-of select="@ref"/>
              </xsl:attribute>
              <xsl:attribute name="RMXType">
                <xsl:value-of select="@type"/>
              </xsl:attribute>
              <xsl:attribute name="Readonly">true</xsl:attribute>
	       <xsl:attribute name="style">WIDTH: 195px;</xsl:attribute>
              <xsl:attribute name="BackColor">#F2F2F2</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="Tabindex">
                  <xsl:value-of select="number(@tabindex)" />
                </xsl:attribute>
              </xsl:if>
            </asp:listbox>
            <asp:button class="EllipsisControl" runat="server">
              <xsl:attribute name="id">
                <xsl:value-of select="@name"/>btn</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="Tabindex">
                  <xsl:value-of select="number(@tabindex)+1" /></xsl:attribute>
              </xsl:if>
              <xsl:attribute name="onclientclick">
                ShowUnitType();return false;</xsl:attribute>
            </asp:button>
            <asp:button runat="server" class="BtnRemove">
              <xsl:attribute name="id">
                <xsl:value-of select="@name" />btndel</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="number(@tabindex)+2" />
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name="onclientclick">
                return deleteSelectedUnit('<xsl:value-of select="@name"/>')
              </xsl:attribute>
              <xsl:attribute name="Text">-</xsl:attribute>
            </asp:button>
            <asp:Textbox runat="server" style="display:none">
              <xsl:attribute name="RMXRef">
                <xsl:value-of select="@ref"/>/@codeid</xsl:attribute>
              <xsl:attribute name="id"><xsl:value-of select="@name"/>_lst</xsl:attribute>
            </asp:Textbox>
          </xsl:when>
 	<!--added by Yukti for MITS 35772-->

          <xsl:when test="@type[.='policylookup' or .='planlookup']">
            <asp:TextBox runat="server"  onblur="codeLostFocus(this.name);" onchange="setDataChanged(true);" onkeydown="eatKeystrokes();">
              <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
              <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
              <xsl:attribute name="Readonly">true</xsl:attribute>
              <xsl:attribute name="BackColor">#F2F2F2</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="Tabindex"><xsl:value-of select="number(@tabindex)" /></xsl:attribute>
              </xsl:if>
            </asp:TextBox>
            <asp:button class="EllipsisControl" runat="server">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="Tabindex"><xsl:value-of select="number(@tabindex)+1" /></xsl:attribute>
              </xsl:if>
              <xsl:attribute name="onclientclick">lookupClaimPolicy('<xsl:value-of select="@name"/>','<xsl:value-of select="@type"/>');return false;</xsl:attribute>
              <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                <xsl:attribute name="disabled">true</xsl:attribute>
              </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:button>
            <asp:TextBox style="display:none" runat="server">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>_cid</xsl:attribute>
              <xsl:attribute name="RMXRef"><xsl:choose><xsl:when test="@idref"><xsl:value-of select="@idref"/></xsl:when>
                  <xsl:otherwise><xsl:value-of select="@ref"/></xsl:otherwise></xsl:choose></xsl:attribute>
            </asp:TextBox>
            <xsl:if test="@linktobutton[.='1']">
              <asp:button class="PolicyButton" runat="server" ToolTip="Open">
                <xsl:attribute name="id"><xsl:value-of select="@name"/>_open</xsl:attribute>
                <!--<xsl:attribute name="value"><xsl:value-of select="@button"/></xsl:attribute>-->
                <!--<xsl:attribute name="Text"><xsl:value-of select="@button"/></xsl:attribute>-->
                <!--<xsl:attribute name="OnClientClick">if(!( XFormHandler('<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;</xsl:attribute>-->
                <!--<xsl:attribute name="PostBackUrl">
                  <xsl:value-of select="@pagetomove"/>?<xsl:value-of select="@param"/>
                </xsl:attribute>
                <xsl:if test="@onServerClick">
                  <xsl:attribute name="onclick">
                    <xsl:value-of select="@onServerClick" />
                  </xsl:attribute>
                </xsl:if>-->
                <xsl:if test="@tabindex">
                  <xsl:attribute name="Tabindex"><xsl:value-of select="number(@tabindex)+2" /></xsl:attribute>
                </xsl:if>
                <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                  <xsl:attribute name="disabled">true</xsl:attribute>
                </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
              </asp:button>
            </xsl:if>
          </xsl:when>

          <xsl:when test="@type[.='banklookup']">
            <asp:Textbox runat="server" onchange="lookupTextChanged(this);" onblur="lookupLostFocus(this);">
              <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
              <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
              <xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="TabIndex"><xsl:value-of select="@tabindex" /></xsl:attribute>
              </xsl:if>
              <xsl:if test="@maxlength">
                <xsl:attribute name="maxlength">
                  <xsl:value-of select="@maxlength"/>
                </xsl:attribute>
              </xsl:if>
             <xsl:if test="(@PowerViewReadOnly[.='true'])">
                <xsl:attribute name="readonly">true</xsl:attribute>
                <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
                  </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:Textbox>
            <xsl:if test="@creatable">
              <asp:TextBox style="display:none" runat="server" value="1">
                <xsl:attribute name="id"><xsl:value-of select="@name"/>_creatable</xsl:attribute>
              </asp:TextBox>
            </xsl:if>
            <asp:button runat="server" class="EllipsisControl">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
              <!--<xsl:attribute name="onclientclick">return lookupDataFDM('<xsl:value-of select="@name"/>','<xsl:value-of select="@tableid"/>',4,'<xsl:value-of select="@fieldmark"/>',1,"Instance/Account/BankEntity");</xsl:attribute>-->
              <xsl:attribute name="onclientclick">return lookupDataFDM('<xsl:value-of select="@name"/>','<xsl:value-of select="@tableid"/>',4,'<xsl:value-of select="@fieldmark"/>',1,"Instance/Account/BankEntity");</xsl:attribute>
              <!--return lookupData('<xsl:value-of select="@name"/>','<xsl:value-of select="@tableid"/>',4,'<xsl:value-of select="@fieldmark"/>',1);-->
              <xsl:if test="@onServerClick">
                <xsl:attribute name="onclick"><xsl:value-of select="@onServerClick" /></xsl:attribute>
              </xsl:if>
              <xsl:if test="@tabindex">
                <xsl:attribute name="Tabindex"><xsl:value-of select="number(@tabindex)+1" /></xsl:attribute>
              </xsl:if>
             <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                      <xsl:attribute name="disabled">true</xsl:attribute>
                  </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:button>


            <asp:TextBox style="display:none" runat="server">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>_cid</xsl:attribute>
              <xsl:attribute name="Text"><xsl:value-of select="@codeid"/></xsl:attribute>
              <xsl:attribute name="RMXRef">
                <xsl:choose>
                  <xsl:when test="@idref"><xsl:value-of select="@idref"/></xsl:when>
                  <!--<xsl:otherwise><xsl:value-of select="@ref"/></xsl:otherwise>-->
                </xsl:choose>
              </xsl:attribute>
            </asp:TextBox>

          </xsl:when>

	 <!--Added Patientlookup for R7:Search not yielding results for Home and Office Phone and Addresses Grid-->
	<xsl:when test="@type[.='patiententitylookup']">
            <asp:Textbox runat="server" onchange="lookupTextChanged(this);" onblur="lookupLostFocus(this);">
              <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
              <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
              <xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="TabIndex"><xsl:value-of select="@tabindex" /></xsl:attribute>
              </xsl:if>
            <xsl:if test="@PowerViewReadOnly[.='true']">
                <xsl:attribute name="readonly">true</xsl:attribute>
                <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
                  </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
              <xsl:if test="@maxlength">
                <xsl:attribute name="maxlength">
                  <xsl:value-of select="@maxlength"/>
                </xsl:attribute>
              </xsl:if>
            </asp:Textbox>
            <xsl:if test="@creatable">
              <asp:TextBox style="display:none" runat="server" value="1">
                <xsl:attribute name="id"><xsl:value-of select="@name"/>_creatable</xsl:attribute>
              </asp:TextBox>
            </xsl:if>
            <asp:button runat="server" class="EllipsisControl">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
              <xsl:attribute name="onclientclick">return lookupDataFDM('<xsl:value-of select="@name"/>','<xsl:value-of select="@tableid"/>',4,'<xsl:value-of select="@fieldmark"/>',1,"Instance/Patient/PatientEntity");</xsl:attribute>
              <xsl:if test="@onServerClick">
                <xsl:attribute name="onclick"><xsl:value-of select="@onServerClick" /></xsl:attribute>
              </xsl:if>
              <xsl:if test="@tabindex">
                <xsl:attribute name="Tabindex"><xsl:value-of select="number(@tabindex)+1" /></xsl:attribute>
              </xsl:if>
              <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                      <xsl:attribute name="disabled">true</xsl:attribute>
                  </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:button>
            <asp:TextBox style="display:none" runat="server">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>_cid</xsl:attribute>
              <xsl:attribute name="Text"><xsl:value-of select="@codeid"/></xsl:attribute>
              <xsl:attribute name="RMXRef">
                <xsl:choose>
                  <xsl:when test="@idref"><xsl:value-of select="@idref"/></xsl:when>
                  <!--<xsl:otherwise><xsl:value-of select="@ref"/></xsl:otherwise>-->
                </xsl:choose>
              </xsl:attribute>
            </asp:TextBox>
          </xsl:when>
	   <!--Added Patientlookup for R7:Search not yielding results for Home and Office Phone and Addresses Grid-->

          <!-- Start Naresh Controls added for Enhanced Policy-->
          <!-- Start Naresh Enhanced Policy Search -->
          <xsl:when test="@type[.='enhpolicylookup']">
            <asp:Textbox runat="server"  onchange="setDataChanged(true);">
              <xsl:attribute name="id">
                <xsl:value-of select="@name"/>
              </xsl:attribute>
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
              <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
              <xsl:attribute name="name">
                <xsl:value-of select="@name"/>
              </xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="TabIndex">
                  <xsl:value-of select="@tabindex" />
                </xsl:attribute>
              </xsl:if>
            </asp:Textbox>
            <asp:button runat="server" class="EllipsisControl">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
              <xsl:attribute name="onclientclick">return lookupData('<xsl:value-of select="@name"/>','policyenh',-1,'<xsl:value-of select="@name"/>',20);</xsl:attribute>
              <xsl:if test="@onServerClick"><xsl:attribute name="onclick"><xsl:value-of select="@onServerClick" /></xsl:attribute></xsl:if>
              <xsl:if test="@tabindex">
                <xsl:attribute name="Tabindex">
                  <xsl:value-of select="number(@tabindex)+1" />
                </xsl:attribute>
              </xsl:if>
            </asp:button>
            <asp:TextBox style="display:none" runat="server">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>_cid</xsl:attribute>
              <xsl:attribute name="Text"><xsl:value-of select="@codeid"/></xsl:attribute>
                <xsl:attribute name="RMXRef">
                    <xsl:choose>
                        <xsl:when test="@idref"><xsl:value-of select="@idref"/></xsl:when>
                        <xsl:otherwise><xsl:value-of select="@ref"/></xsl:otherwise>
                    </xsl:choose>
                </xsl:attribute>
            </asp:TextBox>
           
          </xsl:when>
          <!-- End Naresh Enhanced Policy Search-->
          <!-- Start Prashant LeavePlanlookup -->
	   <!-- npadhy RMSC retrofit Starts Inserted new control to have texbox with % sign -->
      <xsl:when test="@type[.='textpercent']">
                    <span class="formw">
                      <!-- Manish Jain: R8 Withholding Enhancement, MITS 26471 added max attribute -->
                <asp:TextBox runat="server" size="10" onblur="numLostFocus(this);">
                <xsl:attribute name="id"><xsl:value-of select="@name" /></xsl:attribute>
                <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
                <xsl:if test="@tabindex">
                  <xsl:attribute name="tabindex"><xsl:value-of select="@tabindex" /></xsl:attribute>
                </xsl:if>
                <xsl:if test="@size">
                  <xsl:attribute name="size"><xsl:value-of select="@size"/></xsl:attribute>
                </xsl:if>
                <xsl:if test="@maxlength">
                  <xsl:attribute name="maxlength">
                    <xsl:value-of select="@maxlength"/>
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="@fixed">
                  <xsl:attribute name="fixed"><xsl:value-of select="@fixed" /></xsl:attribute>
                </xsl:if>
                <xsl:if test="@min">
                  <xsl:attribute name="min">
                    <xsl:value-of select="@min" />
                  </xsl:attribute>
                </xsl:if>
                  <xsl:if test="@max">
                    <xsl:attribute name="max">
                      <xsl:value-of select="@max" />
                    </xsl:attribute>
                  </xsl:if>
                <xsl:attribute name="onchange">
                  <!-- smishra54: R8 Withholding Enhancement, MITS 26019 -->
                  <xsl:choose>
                    <xsl:when test="@allowfloat='true'"></xsl:when>
                    <xsl:otherwise>AllowIntOnly(this);</xsl:otherwise>
                  </xsl:choose>
                  <!--smishra54:End-->
                  <!-- Start:MITS 27279-Ritesh -->
                  <xsl:choose>
                    <xsl:when test="@notallownegative='true'">NotAllowNegative(this);</xsl:when>
                    <xsl:otherwise></xsl:otherwise>
                  </xsl:choose>
                  <!--End:MITS 27279-Ritesh-->
                  <xsl:choose>
                    <xsl:when test="@onchange">
                      <xsl:value-of select="@onchange" />
                    </xsl:when>
                    <xsl:otherwise>setDataChanged(true);</xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
                </asp:TextBox>
				<asp:Label runat="server">
						<xsl:attribute name="text">%</xsl:attribute>
						<xsl:attribute name="width">10px</xsl:attribute>
				</asp:Label>
				</span>
                  </xsl:when>
      <!-- npadhy RMSC retrofit Ends -->
	  <!-- MITS 15069 ybhaskar: Removing onblur="codeLostFocus(this);" as not needed on the page -->
          <xsl:when test="@type[.='leaveplanlookup']">
            <asp:Textbox runat="server" size="30" onchange="setDataChanged(true);">
              <xsl:attribute name="id">
                <xsl:value-of select="@name"/>
              </xsl:attribute>
              <xsl:attribute name="RMXRef">
                <xsl:value-of select="@ref"/>
              </xsl:attribute>
              <xsl:attribute name="name">
                <xsl:value-of select="@name"/>
              </xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="TabIndex">
                  <xsl:value-of select="@tabindex" />
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name="readonly">true</xsl:attribute>
              <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
            </asp:Textbox>
            <asp:button runat="server" class="EllipsisControl">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
              <xsl:attribute name="onclientclick">return lookupLeavePlanName('<xsl:value-of select="@name" />','<xsl:value-of select="@lookup" />');</xsl:attribute>
              <xsl:if test="@onServerClick"><xsl:attribute name="onclick"><xsl:value-of select="@onServerClick" /></xsl:attribute></xsl:if>
              <xsl:if test="@tabindex"><xsl:attribute name="Tabindex"><xsl:value-of select="number(@tabindex)+1" /></xsl:attribute></xsl:if>
            </asp:button>
            <asp:TextBox style="display:none" runat="server">
              <xsl:attribute name="name"><xsl:value-of select="@name"/>_cid</xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@codeid"/></xsl:attribute>
            </asp:TextBox>
            <xsl:if test="@linktobutton[.='1']">
              <asp:button runat="server" class="button">
                <xsl:attribute name="id"><xsl:value-of select="@name"/>_open</xsl:attribute>
                <xsl:attribute name="value"><xsl:value-of select="@button"/></xsl:attribute>
                <xsl:attribute name="onclientclick">formHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')</xsl:attribute>
                <xsl:if test="@onServerClick"><xsl:attribute name="onclick"><xsl:value-of select="@onServerClick" /></xsl:attribute></xsl:if>
                <xsl:if test="@tabindex"><xsl:attribute name="Tabindex"><xsl:value-of select="number(@tabindex)+2" /></xsl:attribute></xsl:if>
              </asp:button>
            </xsl:if>
          </xsl:when>
          <!-- End Prashant LeavePlanlookup -->
          <!-- Start Naresh pay plan Look up for Policy Billing Tab -->
          <xsl:when test="@type[.='payplanlookup']">
            <asp:Textbox runat="server"  onchange="setDataChanged(true);"> <!--onblur="codeLostFocus(this);">   Commented by csingh7-->
              <xsl:attribute name="id">
                <xsl:value-of select="@name"/>
              </xsl:attribute>
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
              <xsl:attribute name="name">
                <xsl:value-of select="@name"/>
              </xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="TabIndex">
                  <xsl:value-of select="@tabindex" />
                </xsl:attribute>
              </xsl:if>
            </asp:Textbox>
            <asp:button runat="server" class="EllipsisControl">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
              <xsl:attribute name="onclientclick">return lookupPayPlan();</xsl:attribute>
              <xsl:if test="@onServerClick"><xsl:attribute name="onclick"><xsl:value-of select="@onServerClick" /></xsl:attribute>
              </xsl:if>
              <xsl:if test="@tabindex"><xsl:attribute name="Tabindex"><xsl:value-of select="number(@tabindex)+1" /></xsl:attribute>
              </xsl:if>
            </asp:button>
            <asp:TextBox style="display:none" runat="server">
              <xsl:attribute name="name"><xsl:value-of select="@name"/>_cid</xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@codeid"/></xsl:attribute>
            </asp:TextBox>
            <xsl:if test="@linktobutton[.='1']">
              <asp:button runat="server" class="button">
                <xsl:attribute name="id"><xsl:value-of select="@name"/>_open</xsl:attribute>
                <xsl:attribute name="value"><xsl:value-of select="@button"/></xsl:attribute>
                <xsl:attribute name="onclientclick">formHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')</xsl:attribute>
                <xsl:if test="@onServerClick">
                  <xsl:attribute name="onclick"><xsl:value-of select="@onServerClick" /></xsl:attribute>
                </xsl:if>
                <xsl:if test="@tabindex">
                  <xsl:attribute name="Tabindex"><xsl:value-of select="number(@tabindex)+2" /></xsl:attribute>
                </xsl:if>
              </asp:button>
            </xsl:if>
          </xsl:when>
          <!-- End Naresh pay plan Look up for Policy Billing Tab-->

          <!-- Start Naresh Billing Rule Code Look up for Policy Billing -->
          <xsl:when test="@type[.='billingrulelookup']">
            <asp:Textbox runat="server"  onchange="setDataChanged(true);" onkeydown="eatKeystrokes();" > <!--onblur="codeLostFocus(this);">   Commented by csingh7-->
              <xsl:attribute name="id">
                <xsl:value-of select="@name"/>
              </xsl:attribute>
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
              <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
              <xsl:attribute name="name">
                <xsl:value-of select="@name"/>
              </xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="TabIndex">
                  <xsl:value-of select="@tabindex" />
                </xsl:attribute>
              </xsl:if>
            </asp:Textbox>
            <asp:button runat="server" class="EllipsisControl">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
              <xsl:attribute name="onclientclick">return lookupBillingRule();</xsl:attribute>
              <xsl:if test="@onServerClick"><xsl:attribute name="onclick"><xsl:value-of select="@onServerClick" /></xsl:attribute>
              </xsl:if>
              <xsl:if test="@tabindex">
                <xsl:attribute name="Tabindex"><xsl:value-of select="number(@tabindex)+1" /></xsl:attribute>
              </xsl:if>
            </asp:button>
            <asp:TextBox style="display:none" runat="server">
              <xsl:attribute name="name"><xsl:value-of select="@name"/>_cid</xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@codeid"/></xsl:attribute>
            </asp:TextBox>
            <xsl:if test="@linktobutton[.='1']">
              <asp:button runat="server" class="button">
                <xsl:attribute name="id"><xsl:value-of select="@name"/>_open</xsl:attribute>
                <xsl:attribute name="value"><xsl:value-of select="@button"/></xsl:attribute>
                <xsl:attribute name="onclientclick">formHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')</xsl:attribute>
                <xsl:if test="@onServerClick">
                  <xsl:attribute name="onclick"><xsl:value-of select="@onServerClick" /></xsl:attribute>
                </xsl:if>
                <xsl:if test="@tabindex">
                  <xsl:attribute name="Tabindex"><xsl:value-of select="number(@tabindex)+2" /></xsl:attribute>
                </xsl:if>
              </asp:button>
            </xsl:if>
          </xsl:when>
          <!-- End Naresh Billing Rule Code Look up for Policy Billing -->
	  <!--Anu Tennyson for MITS 18922 STARTS-->
	 <xsl:when test="@type[.='deductiblelookup']">
     <mc:CurrencyTextbox runat="server">
       <!--rupal:mits 27502-->
	    <!--<asp:Textbox runat="server"  onchange="setDataChanged(true);" onblur="currencyLostFocus(this);">-->
        <!--onblur="codeLostFocus(this);">   Commented by csingh7-->
              <xsl:attribute name="id">
                <xsl:value-of select="@name"/>
              </xsl:attribute>
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
              <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
              <xsl:attribute name="name">
                <xsl:value-of select="@name"/>
              </xsl:attribute>
	      <xsl:attribute name="onchange">
	      <xsl:choose>
	      <xsl:when test="@onchange"><xsl:value-of select="@onchange"/></xsl:when></xsl:choose>;setDataChanged(true);</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="TabIndex">
                  <xsl:value-of select="@tabindex" />
                </xsl:attribute>
              </xsl:if>
        </mc:CurrencyTextbox>
            <!--</asp:Textbox> rupal:mits 27502-->
            <asp:button runat="server" class="EllipsisControl">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
              <xsl:attribute name="onclientclick">return lookupDeductible();</xsl:attribute>
              <xsl:if test="@onServerClick"><xsl:attribute name="onclick"><xsl:value-of select="@onServerClick" /></xsl:attribute>
              </xsl:if>
              <xsl:if test="@tabindex">
                <xsl:attribute name="Tabindex"><xsl:value-of select="number(@tabindex)+1" /></xsl:attribute>
              </xsl:if>
            </asp:button>
         </xsl:when>
	  <!--Anu Tennyson for MITS 18922-->
          <!--Anu Tennyson for MITS 18922 STARTS-->
	  <xsl:when test="@type[.='uarcodelist']">
            <asp:Listbox runat="server" size="3">
                <xsl:attribute name="id">
                  <xsl:value-of select="@name"/>
                </xsl:attribute>
                <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
                <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
                <xsl:attribute name="style">
                  <xsl:value-of select="@style"/>
                </xsl:attribute>
                <xsl:if test="@tabindex">
                  <xsl:attribute name="Tabindex">
                    <xsl:value-of select="@tabindex" />
                  </xsl:attribute>
                </xsl:if>
            </asp:Listbox>
           <xsl:if test="@type[.='uarcodelist']">
              <asp:button runat="server" class="EllipsisControl">
                <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
                <xsl:if test="@tabindex"><xsl:attribute name="tabindex"><xsl:value-of select="number(@tabindex)+1"/>
		</xsl:attribute>
                </xsl:if>
                <xsl:if test="@type[.='uarcodelist']">
                  <xsl:attribute name="onclientclick">return getSelectedUAR()</xsl:attribute>
                </xsl:if>
              </asp:button>
            </xsl:if>
	    <asp:button runat="server" class="BtnRemove">
              <xsl:attribute name="id"><xsl:value-of select="@name" />btndel</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex"><xsl:value-of select="number(@tabindex)+2" /></xsl:attribute>
              </xsl:if>
              <xsl:attribute name="onclientclick">return deleteSelectedUAR('<xsl:value-of select="@name"/>')</xsl:attribute>
              <xsl:attribute name="Text">-</xsl:attribute>
            </asp:button>
            <asp:Textbox runat="server" style="display:none">
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/>/@codeid</xsl:attribute>
              <xsl:attribute name="id"><xsl:value-of select="@name"/>_lst</xsl:attribute>
            </asp:Textbox>    
        </xsl:when>
	  <!--Anu Tennyson for MITS 18922-->

          <!-- Start Naresh Controls added for Enhanced Policy-->
          <xsl:when test="@type[.='codelist']">
            <uc:MultiCode runat="server">
              <xsl:attribute name="ID">
                <xsl:value-of select="@name"/>
              </xsl:attribute>
              <!--Mona:Adding a filter property where user can add custom where clause-->
              <xsl:if test="@filter">
                <xsl:attribute name="Filter">
                  <xsl:value-of select="@filter"/>
                </xsl:attribute >
              </xsl:if>
              <xsl:attribute name="CodeTable">
                <xsl:value-of select="@codetable"/>
              </xsl:attribute>
              <xsl:attribute name="ControlName">
                <xsl:value-of select="@name"/>
              </xsl:attribute>
              <xsl:attribute name="RMXRef">
                <xsl:value-of select="@ref"/>
              </xsl:attribute>
              <xsl:attribute name="RMXType">
                <xsl:value-of select="@type"/>
              </xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="Tabindex">
                  <xsl:value-of select="@tabindex" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="@required[.='yes']">
                <xsl:attribute name="Required">true</xsl:attribute>
                <xsl:attribute name="ValidationGroup">vgSave</xsl:attribute>
              </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])">
                <xsl:attribute name="BackColor">#F2F2F2</xsl:attribute>
                <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
                <xsl:attribute name="Enabled">false</xsl:attribute>
              </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </uc:MultiCode >            
          </xsl:when >
          <xsl:when test="@type[.='multientitylist']">
            <asp:Listbox runat="server"  multiple="multiple">
              <xsl:if test="@size">
                <xsl:attribute name="size">
                  <xsl:value-of select="@size" />
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
              <xsl:attribute name="RMXRef">
                <xsl:value-of select="@ref"/>
              </xsl:attribute>
              <xsl:attribute name="RMXType">
                <xsl:value-of select="@type"/>
              </xsl:attribute>
              <xsl:attribute name="style">
                <xsl:value-of select="@style"/>
              </xsl:attribute>
              <!--skhare7 R8-->
              <xsl:if test="@width">
                <xsl:attribute name="width">
                  <xsl:value-of select="@width" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="@tabindex">
                <xsl:attribute name="Tabindex">
                  <xsl:value-of select="@tabindex" />
                </xsl:attribute>
                <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                  <xsl:attribute name="Readonly">true</xsl:attribute>
                  <xsl:attribute name="BackColor">#F2F2F2</xsl:attribute>
                </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
              </xsl:if>
            </asp:Listbox>
            <asp:button runat="server" class="BtnRemove" ToolTip="Remove">
              <xsl:attribute name="id">
                <xsl:value-of select="@name" />btndel</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="number(@tabindex)+2" />
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name="onclientclick">
                return deleteSelCode('<xsl:value-of select="@name"/>')
              </xsl:attribute>
              <xsl:attribute name="Text">-</xsl:attribute>
              <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                <xsl:attribute name="disabled">true</xsl:attribute>
              </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:button>
            <asp:ImageButton runat="server"  src="../../Images/up.gif" width="20" height="20" title="Move Up">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>up</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="number(@tabindex)+2" />
                </xsl:attribute>
              </xsl:if>
                <xsl:attribute name="onclientclick">
                   MoveListBoxField(0,' <xsl:value-of select="@name"/>');return false;
                </xsl:attribute>
              <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                <xsl:attribute name="disabled">true</xsl:attribute>
              </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
               </asp:ImageButton>
            <asp:ImageButton runat="server"  src="../../Images/down.gif" width="20" height="20" title="Move Down">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>down</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="number(@tabindex)+2" />
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name="onclientclick">
                 MoveListBoxField(1,' <xsl:value-of select="@name"/>');return false;
              </xsl:attribute>
              <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                <xsl:attribute name="disabled">true</xsl:attribute>
              </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:ImageButton>
            <asp:Textbox runat="server" style="display:none">
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/>/@codeid</xsl:attribute>
              <xsl:attribute name="id"><xsl:value-of select="@name"/>_lst</xsl:attribute>
            </asp:Textbox>
          </xsl:when >
          <xsl:when test="@type[.='entitylist' or .='codedeslist']">
		  <!--bganta : Adding the panel for scrolling : MITS : 31772 : Start-->
		  <xsl:choose>
		  <xsl:when test="@scroll[.='yes']">
		  <div style=" width:200px; height:auto; overflow:auto">
			<asp:Listbox runat="server" size="3">
                <xsl:attribute name="id">
                  <xsl:value-of select="@name"/>
                </xsl:attribute>
                <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
                <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
                <xsl:attribute name="style">
                  <xsl:value-of select="@style"/>
                </xsl:attribute>
                <xsl:if test="@tabindex">
                  <xsl:attribute name="Tabindex">
                    <xsl:value-of select="@tabindex" />
                  </xsl:attribute>
                </xsl:if>
        <xsl:if test=" (@PowerViewReadOnly[.='true'])">
          <xsl:attribute name="Readonly">true</xsl:attribute>
          <xsl:attribute name="BackColor">#F2F2F2</xsl:attribute>
        </xsl:if>
      <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:Listbox>
		  </div>
		  </xsl:when>
		  <xsl:otherwise>
            <asp:Listbox runat="server" size="3">
                <xsl:attribute name="id">
                  <xsl:value-of select="@name"/>
                </xsl:attribute>
                <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
                <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
                <xsl:attribute name="style">
                  <xsl:value-of select="@style"/>
                </xsl:attribute>
                <xsl:if test="@tabindex">
                  <xsl:attribute name="Tabindex">
                    <xsl:value-of select="@tabindex" />
                  </xsl:attribute>
                </xsl:if>
              <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                <xsl:attribute name="Readonly">true</xsl:attribute>
                <xsl:attribute name="BackColor">#F2F2F2</xsl:attribute>
              </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:Listbox>
			</xsl:otherwise>
			</xsl:choose>
			<!--bganta : Adding the panel for scrolling : MITS : 31772 : End-->
            <xsl:if test="@type[.='codedeslist']">
              <xsl:if test="@lookupbutton">
                <asp:button runat="server" class="CodeLookupControl">
                  <xsl:attribute name="onclientclick">
                    <xsl:value-of select="@functionname"/>
                  </xsl:attribute>
                  <xsl:attribute name="value">
                    <xsl:value-of select="@lookupbutton"/>
                  </xsl:attribute>
                  <xsl:if test="@tabindex">
                    <xsl:attribute name="tabindex">
                      <xsl:value-of select="number(@tabindex)+1" />
                    </xsl:attribute>
                  </xsl:if>
                 <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                      <xsl:attribute name="disabled">true</xsl:attribute>
                  </xsl:if>
                <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
                </asp:button>
              </xsl:if>
            </xsl:if>  
            <xsl:if test="@type[.='entitylist']">
              <!--dbisht6 start for mits 35331-->
              <xsl:if test="@additionalbutton">
                <asp:button class="PolicyButton" runat="server">
                  <xsl:if test="@tabindex">
                    <xsl:attribute name="tabindex">
                      <xsl:value-of select="number(@tabindex)+1" />
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:attribute name="id">
                    <xsl:value-of select="@buttonname"/>
                  </xsl:attribute>
                  <xsl:attribute name="Text">
                    <xsl:value-of select="@buttontitle" />
                  </xsl:attribute>
                  <xsl:if test='@buttonwidth'>
                    
                    <xsl:attribute name="width">
                      <xsl:value-of select="@buttonwidth"/>
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:if test="@onbuttonclientclick">
                    <xsl:attribute name="onClientClick">
                      <xsl:value-of select="@onbuttonclientclick"/>
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:if test="@onbuttonserverclick">
                    <xsl:attribute name="onclick">
                      <xsl:value-of select="@onbuttonserverclick"/>
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:if test='@buttontooltip'>
                    <xsl:attribute name="ToolTip">
                      <xsl:value-of select="@buttontooltip"/>
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:if test='@buttontag'>
                    <xsl:attribute name="Tag">
                      <xsl:value-of select="@buttontag"/>
                    </xsl:attribute>
                  </xsl:if>
                <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                      <xsl:attribute name="disabled">true</xsl:attribute>
                  </xsl:if>
                <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
                </asp:button>
              </xsl:if>

              <!--dbisht6 end-->

              <asp:button runat="server" class="EllipsisControl">
                <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
                <xsl:if test="@tabindex"><xsl:attribute name="tabindex"><xsl:value-of select="number(@tabindex)+1" /></xsl:attribute>
                </xsl:if>
                <xsl:if test="@type[.='entitylist']">

                  <xsl:choose>
                    <!--smishra25 Mits 14619 - In case of Employee, show default Employee Search -->
                    <xsl:when test="@viewid and @tableid[.='EMPLOYEE']">
                      <xsl:attribute name="onclientclick">return lookupData('<xsl:value-of select="@name" />','<xsl:value-of select="@tableid" />','<xsl:value-of select="@viewid"/>','<xsl:value-of select="@name" />',3)</xsl:attribute>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:attribute name="onclientclick">return lookupData('<xsl:value-of select="@name"/>','<xsl:value-of select="@tableid" />',4,'<xsl:value-of select="@name" />',3)</xsl:attribute>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
               <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                      <xsl:attribute name="disabled">true</xsl:attribute>
                  </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
              </asp:button>
            </xsl:if>
            <asp:button runat="server" class="BtnRemove">
              <xsl:attribute name="id"><xsl:value-of select="@name" />btndel</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex"><xsl:value-of select="number(@tabindex)+2" /></xsl:attribute>
              </xsl:if>
              <xsl:attribute name="onclientclick">return deleteSelCode('<xsl:value-of select="@name"/>')</xsl:attribute>
              <xsl:attribute name="Text">-</xsl:attribute>
             <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                      <xsl:attribute name="disabled">true</xsl:attribute>
                  </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:button>
	    <!--spahariya MITS 30426 -->
		<xsl:if test="@entityNev[.='1']">			     
              <asp:button class="EntityNevButton" runat="server" ToolTip="Open">
                <xsl:attribute name="id"><xsl:value-of select="@name"/>_open</xsl:attribute>  
		<xsl:attribute name="onclientclick">return fnEntityNev('<xsl:value-of select="@name"/>','<xsl:value-of select="@entitytype" />')</xsl:attribute>
                <xsl:if test="@tabindex">
                  <xsl:attribute name="Tabindex"><xsl:value-of select="number(@tabindex)+2" /></xsl:attribute>
                </xsl:if>
               <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                      <xsl:attribute name="disabled">true</xsl:attribute>
                  </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
              </asp:button>
            </xsl:if>
	    <!-- spahariya end -->
            <asp:Textbox runat="server" style="display:none">
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/>/@codeid</xsl:attribute>
              <xsl:attribute name="id"><xsl:value-of select="@name"/>_lst</xsl:attribute>
            </asp:Textbox>    
        </xsl:when>
          <!-- Codelist Ends-->

          <!--Sumit - MITS#18230 10/26/2009 Added control for Property lookup-->
          <xsl:when test="@type[.='propertylookup']">
            <asp:TextBox runat="server"  onchange="setDataChanged(true);" onkeydown="eatKeystrokes();">
              <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
              <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
              <xsl:attribute name="text"><xsl:value-of select="text()"/></xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="Tabindex">
                  <xsl:value-of select="number(@tabindex)+1" />
                </xsl:attribute>
              </xsl:if>
            <xsl:if test="@PowerViewReadOnly[.='true']">
                <xsl:attribute name="readonly">true</xsl:attribute>
                <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
                  </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:TextBox>
            <asp:TextBox style="display:none" runat="server">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>_cid</xsl:attribute>
              <xsl:attribute name="RMXRef"><xsl:choose><xsl:when test="@idref"><xsl:value-of select="@idref"/></xsl:when><xsl:otherwise><xsl:value-of select="@ref"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:attribute>
            </asp:TextBox>
            <asp:button runat="server" class="EllipsisControl">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
              <xsl:attribute name="onclientclick">return lookupData('<xsl:value-of select="@name"/>','propertyunit',-1,'<xsl:value-of select="@fieldmark"/>',25);</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="Tabindex"><xsl:value-of select="number(@tabindex)+2" /></xsl:attribute>
              </xsl:if>
            <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                      <xsl:attribute name="disabled">true</xsl:attribute>
                  </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:button>
          </xsl:when>
         <!--Policy Billing Search : Gagan-->
          
          <!--rupal:start, policy system interface enh-->
          <xsl:when test="@type[.='sitelookup']">
            <asp:TextBox runat="server"  onchange="setDataChanged(true);" onkeydown="eatKeystrokes();">
              <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
              <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
              <xsl:attribute name="text"><xsl:value-of select="text()"/></xsl:attribute>
              <xsl:if test="@tabindex"><xsl:attribute name="Tabindex"><xsl:value-of select="number(@tabindex)+1" /></xsl:attribute></xsl:if>
            </asp:TextBox>
            <asp:TextBox style="display:none" runat="server">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>_cid</xsl:attribute>
              <xsl:attribute name="RMXRef"><xsl:choose><xsl:when test="@idref"><xsl:value-of select="@idref"/></xsl:when><xsl:otherwise><xsl:value-of select="@ref"/></xsl:otherwise></xsl:choose></xsl:attribute>
            </asp:TextBox>
            <asp:button runat="server" class="EllipsisControl">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
              <xsl:attribute name="onclientclick">return lookupData('<xsl:value-of select="@name"/>','siteunit',-1,'<xsl:value-of select="@fieldmark"/>',35);</xsl:attribute>
              <xsl:if test="@tabindex"><xsl:attribute name="Tabindex"><xsl:value-of select="number(@tabindex)+2" /></xsl:attribute></xsl:if>
            </asp:button>
          </xsl:when>
          <!--rupal:end-->
          
          <xsl:when test="@type[.='policybillinglookup']">
            <asp:Textbox runat ="server"  size="30" >
              <!--<xforms:input xhtml:size="30">-->
              <xsl:attribute name="readonly">true</xsl:attribute>
              <!--<xsl:attribute name="xhtml:readonly">true</xsl:attribute>-->
              <xsl:attribute name="RMXRef">
                <xsl:value-of select="@ref"/>
              </xsl:attribute>              
              <xsl:attribute name="id">
                <xsl:value-of select="@name"/>
              </xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="@tabindex" />
                </xsl:attribute>
              </xsl:if>
              
              <!-- <xsl:attribute name="value">
												<xsl:value-of select="text()"/>
											</xsl:attribute> -->
              <!--</xforms:input>-->
              
            </asp:Textbox>


            <asp:button runat="server" class="EllipsisControl">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="number(@tabindex) + 1" />
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name="onclientclick">
                <!--return lookupData('<xsl:value-of select="@name"/>','<xsl:value-of select="@tableid"/>',4,'<xsl:value-of select="@fieldmark"/>',1);-->
                return lookupData('<xsl:value-of select="@name"/>','policybilling',-1,'<xsl:value-of select="@fieldmark" />',21)
              </xsl:attribute>
              <xsl:if test="@onServerClick">
                <xsl:attribute name="onclick">
                  <xsl:value-of select="@onServerClick" />
                </xsl:attribute>
              </xsl:if>              
            </asp:button>


            <!--<xhtml:input xhtml:type="button" xhtml:class="button">
              --><!--<xsl:attribute name="xhtml:value">...</xsl:attribute>
              <xsl:attribute name="xhtml:id">
                <xsl:value-of select="@name" separator=""/>btn
              </xsl:attribute>--><!--
              --><!--<xsl:if test="@tabindex">
                <xsl:attribute name="xhtml:tabindex">
                  <xsl:value-of select="number(@tabindex)+1" />
                </xsl:attribute>
              </xsl:if>--><!--
              <xsl:attribute name="xhtml:onclick">
                lookupData('<xsl:value-of select="@name" separator=""/>','policybilling',-1,'<xsl:value-of select="@fieldmark" separator=""/>',21)
              </xsl:attribute>
            </xhtml:input>-->



            <asp:Textbox runat="server" style="display:none" >

              <!--<xforms:input xhtml:style="display:none">-->
              <xsl:attribute name="ref">
                <xsl:choose>
                  <xsl:when test="@idref">
                    <xsl:value-of select="@idref"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="@ref"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:attribute>
              <xsl:attribute name="id"><xsl:value-of select="@name" />_cid</xsl:attribute>
              <xsl:attribute name="Text">
                <xsl:value-of select="@codeid"/>
              </xsl:attribute>
              <!--</xforms:input>-->
            </asp:Textbox>
            
            <!--<xsl:if test="@linktobutton[.='1']">
              <asp:button class="button">
              --><!--<xforms:submit xhtml:class="button">--><!--
                <xsl:attribute name="onclientclick">
                  ;return XFormHandler('<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')
                </xsl:attribute>
                <xsl:attribute name="id">
                  <xsl:value-of select="@name"/>_open
                </xsl:attribute>
                
                --><!--<xforms:label>--><!--
                <asp:label>
                  <xsl:value-of select="@button" separator=""/>
                </asp:label>
                --><!--</xforms:label>--><!--
                  
                --><!--<xforms:setvalue ref="//UI/ScreenFlowStack/ScreenFlow[position()=1]/SysScreenAction">formbutton</xforms:setvalue>--><!--
                <xsl:call-template name="ParseSetValue"/>
                <xsl:if test="@tabindex">
                  <xsl:attribute name="tabindex">
                    <xsl:value-of select="number(@tabindex)+2" />
                  </xsl:attribute>
                </xsl:if>
              </asp:button>              
            </xsl:if>-->            
            
          </xsl:when>

          <!--Policy Billing Search : Gagan-->


          <!-- Added by Gagan from xform-controls :  Start-->
          <xsl:when test="@type[.='pientitylookup']">
            <asp:Textbox runat ="server"  onchange="lookupTextChanged(this);" >
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="@tabindex" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="not(@cancelonblur[.='yes'])">
                <xsl:attribute name="onblur">lookupLostFocus(this);</xsl:attribute>
              </xsl:if>
              <xsl:attribute name="RMXRef">
                <xsl:value-of select="@ref"/>
              </xsl:attribute>
              <xsl:if test="@maxlength">
                <xsl:attribute name="maxlength">
                  <xsl:value-of select="@maxlength"/>
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name="RMXType">
                <xsl:value-of select="@type"/>
              </xsl:attribute>
              <xsl:attribute name="id">
                <xsl:value-of select="@name" />
              </xsl:attribute>
              <xsl:if test="((@locked[.='true']) or (@PowerViewReadOnly[.='true']))">
                <xsl:attribute name="readonly">true</xsl:attribute>
                <xsl:attribute name="style">background-color: #F2F2F2;</xsl:attribute>
              </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:Textbox>


            <xsl:if test="@creatable">
              <asp:Textbox  runat ="server" style="display:none" Text="1">
                <xsl:attribute name="id"><xsl:value-of select="@name"/>_creatable</xsl:attribute>
              </asp:Textbox>
            </xsl:if>


            <!-- abhateja 10.31.2008 -->
            <!-- Replaced the lookupdataFDM implementation with the legacy lookupdata.We don't need lookupattachnodepath parameter any more.-->
            <asp:button runat="server" class="CodeLookupControl">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>pibtn</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="number(@tabindex) + 1" />
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name="onclientclick">return lookupData('<xsl:value-of select="@name"/>','<xsl:value-of select="@listtableid"/><xsl:value-of select="@listparam"/>',4,'<xsl:value-of select="@fieldmark"/>',1);</xsl:attribute>
              <xsl:if test="@onServerClick">
                <xsl:attribute name="onclick"><xsl:value-of select="@onServerClick" /></xsl:attribute>
              </xsl:if>
              <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                      <xsl:attribute name="disabled">true</xsl:attribute>
                  </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:button>
            
            
            <asp:button runat="server" class="EllipsisControl">
              <xsl:attribute name="id"><xsl:value-of select="@name"/>btn</xsl:attribute>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="number(@tabindex) + 1" />
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name="onclientclick">return lookupData('<xsl:value-of select="@name"/>','<xsl:value-of select="@tableid"/>',4,'<xsl:value-of select="@fieldmark"/>',1);</xsl:attribute>
              <xsl:if test="@onServerClick">
                <xsl:attribute name="onclick">
                  <xsl:value-of select="@onServerClick" />
                </xsl:attribute>
              </xsl:if>
             <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                      <xsl:attribute name="disabled">true</xsl:attribute>
                  </xsl:if>
            <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
            </asp:button>


            <xsl:if test="@withlink[.='yes']">
              <img src="img\spacer.gif" width="10" height="1"></img>
              <a href="#">
                <xsl:attribute name="name">
                  <xsl:value-of select="@name"/>
                </xsl:attribute>

                <!--bijender cosmetic change-->
                <xsl:choose >
                  <xsl:when test="@Retval">
                    <xsl:if test="@Retval[.='1']">
                      <xsl:attribute name="onClick">LinkHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>');return false;
                      </xsl:attribute>
                    </xsl:if>
                  </xsl:when>

                  <xsl:otherwise >
                    <xsl:attribute name="onClick">LinkHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>')
                    </xsl:attribute>
                  </xsl:otherwise>
                </xsl:choose>
                <!--Bijender end-->
                
                <!--<xsl:attribute name="onClick">LinkHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>')</xsl:attribute>-->
                <xsl:attribute name="class">
                  <xsl:value-of select="@style"/>
                </xsl:attribute>
                <xsl:value-of select="@LinkTitle"/>
              </a>
            </xsl:if>
          </xsl:when>

          <!-- Added by Gagan from xform-controls :  End-->

          <!--Added Rakhi Zapatec Grid-->
          <xsl:when test="@type[.='ZapatecGrid']">
            <table>
              <tr>
                <td>
                  <!-- div tag added by Shivendu to separate header and data of grid for MITS 17162-->
                  <div>
                    <xsl:attribute name="id"><xsl:value-of select="@valueXML"/>_header</xsl:attribute>
                    <xsl:attribute name="style">width:350px;</xsl:attribute>
                  </div>
                </td>
                <td>

                  <asp:TextBox style="display:none" runat="server" >
                    <xsl:attribute name="id">
                      <xsl:value-of select="@name"/>
                    </xsl:attribute>
                    <xsl:attribute name="RMXRef">
                      <xsl:value-of select="@ref"/>
                    </xsl:attribute>
                  </asp:TextBox>
                  <xsl:if test="@GroupAssoc[.!='']">
                    <asp:TextBox style="display:none" runat="server" >
                      <xsl:attribute name="id"><xsl:value-of select="@valueXML"/>_GroupAssoc</xsl:attribute>
		      <xsl:attribute name="Text"><xsl:value-of select="@GroupAssoc"/></xsl:attribute>
                    </asp:TextBox>
                  </xsl:if>
                </td>
               
              </tr>
              
            
              <tr>
                <!--<td>
                  <xsl:choose>
                    <xsl:when test="@required[.='yes']">
                      <span class="required">
                        <u>
                          <xsl:value-of select="@title"/>
                        </u>:
                      </span>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="@title"/>:
                    </xsl:otherwise>
                  </xsl:choose>
                </td>-->
                <td>
                  
                  <table style="overflow:scroll">
                    <tr>
                      <td>
                        
                        <div>
                          <xsl:attribute name="id"><xsl:value-of select="@valueXML"/></xsl:attribute>
                          <xsl:attribute name="float">left</xsl:attribute>
                          <xsl:attribute name="style">width:350px;height:250px;overflow:scroll</xsl:attribute>
                            <script type="text/javascript">
                              <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                                m_readOnlyGrid = true;
                                var readOnlyGrid = true;
                              </xsl:if>
                              var id = new String("");
                              var gridXML = new String("");
                              var gridvalue = new String("");
                              var minRows = new String("");
                              var maxRows = new String("");
                              var title = new String("");
                              id = id + '<xsl:value-of select="@valueXML"/>';
                              gridXML = gridXML + '<xsl:value-of select="@gridXML"/>';
                              gridvalue = gridvalue + '<xsl:value-of select="@name"/>';
                              minRows = minRows + '<xsl:value-of select="@minRows"/>';
                              maxRows = maxRows + '<xsl:value-of select="@maxRows"/>';
                              title = title + '<xsl:value-of select="@title"/>';
                              //ZapatecGridDisplay(id,gridXML,gridvalue,minRows,maxRows,title);
                              UpdateZapatecGridData(id , gridvalue , title);
                             
                            </script>
                        </div>
                      </td>
                      <td>
                        <asp:button ToolTip="Add Row" class="EllipsisControl" runat="Server">
                          <xsl:attribute name="onclientclick">
                            return InsertRow('<xsl:value-of select="@name"/>')
                          </xsl:attribute>
                          <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                            <xsl:attribute name="disabled">true</xsl:attribute>
                          </xsl:if>
                        <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
                        </asp:button>
                        <asp:button ToolTip="Remove Row" class="BtnRemove" runat="Server">
                          <xsl:attribute name="onclientclick">
                            return DeleteRow('<xsl:value-of select="@name"/>')
                          </xsl:attribute>
                          <xsl:attribute name="Text">-</xsl:attribute>
                          <xsl:attribute name="id"><xsl:value-of select="@name"/>_delButton</xsl:attribute>
                          <xsl:if test=" (@PowerViewReadOnly[.='true'])">
                            <xsl:attribute name="disabled">true</xsl:attribute>
                          </xsl:if>
                        <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
                        </asp:button>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </xsl:when>
          <!--Added Rakhi Zapatec Grid-->

          <xsl:when test="@type[.='policysearch']">
						<asp:TextBox runat="server"  onblur="eidLostFocus(this.name);" onchange="setDataChanged(true);">
							<xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
							<xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute>
						</asp:TextBox>
						<asp:TextBox runat="server" type="button" class="EllipsisControl">
							<xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute>
							<xsl:attribute name="onClick">searchPolicies(6,'<xsl:value-of select="@name"/>')</xsl:attribute>
          </asp:TextBox>
						<asp:TextBox style="display:none" runat="server">
							<xsl:attribute name="name"><xsl:value-of select="@name"/>_cid</xsl:attribute>
							<xsl:attribute name="value"><xsl:value-of select="@codeid"/></xsl:attribute>
						</asp:TextBox>
					</xsl:when>
					<xsl:when test="@type[.='combobox']">
            <asp:DropDownList runat="server">
              <xsl:attribute name="id">
                <xsl:value-of select="@name"/>
              </xsl:attribute>
              <xsl:if test="@isblank">
                <xsl:attribute name="isblank">
                  <xsl:value-of select="@isblank"/>
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="@tabindex" />
                </xsl:attribute>
              </xsl:if>
		<xsl:if test="@autopostback">
                  <xsl:attribute name="autopostback">
                    <xsl:value-of select="@autopostback"/>
                  </xsl:attribute>
                </xsl:if>
              <xsl:if test="@rmxignoreget">
                <xsl:attribute name="rmxignoreget">
                  <xsl:value-of select="@rmxignoreget"/>
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="@rmxignoreset">
                <xsl:attribute name="rmxignoreset">
                  <xsl:value-of select="@rmxignoreset"/>
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>
              <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
              <xsl:if test="@itemsetref">
                <xsl:attribute name="ItemSetRef">
                  <xsl:value-of select="@itemsetref" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="@width">
                <xsl:attribute name="width">
                  <xsl:value-of select="@width" />
                </xsl:attribute>
              </xsl:if>
              <xsl:attribute name="onchange">
                <xsl:choose>
                  <!-- MGaba2:added setdatachanged(true) in case a function is already present-->
                  <!--start :BKumar33 has changed for 17058-->
                  <xsl:when test="@onchange">setDataChanged(true);<xsl:value-of select="@onchange" /></xsl:when>
                  <!--End :BKumar33 has changed for 17058-->
                  <xsl:otherwise>setDataChanged(true);</xsl:otherwise>
                </xsl:choose>
              </xsl:attribute>
              <xsl:if test="@includefilename">
                <script language="JavaScript">
                  <xsl:attribute name="SRC">
                    <xsl:value-of select="@includefilename" />
                  </xsl:attribute>{var i;}
                </script>
              </xsl:if>
              <xsl:if test="((@readonly[.='True' or .='true']) or (@PowerViewReadOnly[.='true']))">
                <xsl:attribute name="Enabled">False</xsl:attribute>
              </xsl:if>
              <xsl:if test="(@PowerViewReadOnly[.='true'])"><xsl:attribute name="powerviewreadonly">true</xsl:attribute></xsl:if>
                <!--<xsl:if test="@usenodeset">-->
                    <xsl:for-each select="..//option">
                        <asp:ListItem>
                            <xsl:attribute name="Value"><xsl:value-of select="@value" /></xsl:attribute>
                            <xsl:attribute name="Text"><xsl:value-of select="."/></xsl:attribute>
                        </asp:ListItem>
                    </xsl:for-each>
                <!--</xsl:if>-->
            </asp:DropDownList>
					</xsl:when>
					<xsl:when test="@type[.='accountlist']"><asp:TextBox runat="server"  onblur="codeLostFocus(this.name);" onchange="setDataChanged(true);" onkeypress="return eatKeystrokes(event);"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute>
          </asp:TextBox><input type="button" class="EllipsisControl"><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">selectAccountList('<xsl:value-of select="@name"/>')</xsl:attribute></input>
					<asp:TextBox style="display:none" runat="server"><xsl:attribute name="name"><xsl:value-of select="@name"/>_cid</xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@codeid"/></xsl:attribute>
        </asp:TextBox>
					</xsl:when>
          <!--npadhy Implementation of Grid-->
          <xsl:when test="@type[.='GridAndButtons' or .='Grid']">
            <dg:UserControlDataGrid runat="server">
              <xsl:attribute name="ID"><xsl:value-of select="@name"/></xsl:attribute>
              <xsl:attribute name="GridName"><xsl:value-of select="@name"/></xsl:attribute>
              <xsl:attribute name ="GridTitle"><xsl:value-of select="@gridtitle"/></xsl:attribute>
              <xsl:attribute name="Target"><xsl:value-of select="@target"/></xsl:attribute>
              <xsl:attribute name="DynamicHideNodes"><xsl:value-of select="@DynamicHideNodes"/></xsl:attribute>                            
              <xsl:attribute name="Ref"><xsl:value-of select="@ref"/></xsl:attribute>
              <xsl:attribute name="Unique_Id"><xsl:value-of select="@uniqueid"/></xsl:attribute>
	      <xsl:attribute name="ShowCloneButton"><xsl:choose><xsl:when test='@showclonebutton[.="True"]'>True</xsl:when><xsl:otherwise>False</xsl:otherwise></xsl:choose></xsl:attribute>
              <xsl:attribute name="ShowRadioButton"><xsl:choose><xsl:when test='@showradiobutton[.="True"]'>True</xsl:when><xsl:otherwise>False</xsl:otherwise></xsl:choose></xsl:attribute>
              <xsl:attribute name="ShowCheckBox"><xsl:choose><xsl:when test='@showcheckbox[.="True"]'>True</xsl:when><xsl:otherwise>False</xsl:otherwise></xsl:choose></xsl:attribute>
              <!--Ref Implementation-->
              <xsl:attribute name="RefImplementation"><xsl:choose><xsl:when test='@refimplementation[.="True"]'>True</xsl:when><xsl:otherwise>False</xsl:otherwise></xsl:choose></xsl:attribute>
              <!--Naresh Reverting this change for now. As this is causing problem. Will fix later-->
              <xsl:attribute name="OnClick">KeepRowForEdit('<xsl:value-of select="@name"/>');<xsl:choose><xsl:when test="@onclick"><xsl:value-of select="@onclick"/></xsl:when></xsl:choose>;</xsl:attribute>
              <!--<xsl:if test="@onclick"><xsl:attribute name="OnClick"><xsl:value-of select="@onclick"/></xsl:attribute></xsl:if>-->
              <xsl:attribute name="Width"><xsl:value-of select="@width"/></xsl:attribute>
              <xsl:attribute name="Height"><xsl:value-of select="@height"/></xsl:attribute>
              <xsl:attribute name="hidenodes"><xsl:value-of select="@hidenodes"/></xsl:attribute>
              <xsl:attribute name="ShowHeader"><xsl:value-of select="@showheader"/></xsl:attribute>
              <xsl:attribute name="ShowFooter"><xsl:choose><xsl:when test='@showfooter[.="True"]'>True</xsl:when><xsl:otherwise>False</xsl:otherwise></xsl:choose></xsl:attribute>
              <xsl:attribute name="LinkColumn"><xsl:value-of select="@linkcolumn"/></xsl:attribute>
              <xsl:if test="@linktype"><xsl:attribute name="LinkType"><xsl:value-of select="@linktype"/></xsl:attribute></xsl:if>
              <xsl:if test="@imageurl"><xsl:attribute name="ImageUrl"><xsl:value-of select="@imageurl"/></xsl:attribute></xsl:if>
              <xsl:if test="@textcolumn"><xsl:attribute name="TextColumn"><xsl:value-of select="@textcolumn"/></xsl:attribute></xsl:if>
              <xsl:attribute name="PopupWidth"><xsl:value-of select="@popupwidth"/></xsl:attribute>
              <xsl:attribute name="PopupHeight"><xsl:value-of select="@popupheight"/></xsl:attribute>
              <xsl:if test="@type[.='Grid']"><xsl:attribute name="HideButtons">New|Edit|Delete</xsl:attribute></xsl:if>
              <xsl:if test="@type[.='GridAndButtons']">
                <xsl:if test="@hidebuttons">
                  <xsl:attribute name="HideButtons">
                    <xsl:value-of select="@hidebuttons"/>
                  </xsl:attribute>
                </xsl:if>
              </xsl:if>
              <xsl:attribute name="Type"><xsl:value-of select="@type"/></xsl:attribute>
              <xsl:if test="@align"><xsl:attribute name="Align"><xsl:value-of select="@align"/></xsl:attribute></xsl:if>
              <xsl:if test="@centeraligncolumns"><xsl:attribute name="CenterAlignColumns"><xsl:value-of select="@centeraligncolumns"/></xsl:attribute></xsl:if>
              <xsl:if test="@rightaligncolumns"><xsl:attribute name="RightAlignColumns"><xsl:value-of select="@rightaligncolumns"/></xsl:attribute></xsl:if>
              <xsl:attribute name="IncludeLastRecord"><xsl:choose><xsl:when test='@includelastrecord[.="True"]'>True</xsl:when><xsl:otherwise>False</xsl:otherwise></xsl:choose></xsl:attribute>
              <!--Ashish Ahuja : Mits 30264 tabindex attribute aadded -->
              <xsl:if test="@tabindex">
                <xsl:attribute name="tabindex">
                  <xsl:value-of select="@tabindex" />
                </xsl:attribute>
              </xsl:if>

              <!--Added Rakhi for R7:Add Emp Data Elements-->
              <!--<xsl:attribute name="PopupGrid"><xsl:choose><xsl:when test='@popupgrid[.="True"]'>True</xsl:when><xsl:otherwise>False</xsl:otherwise></xsl:choose></xsl:attribute>-->
              <!--<xsl:attribute name="SessionGrid"><xsl:choose><xsl:when test='@sessiongrid[.="True"]'>True</xsl:when><xsl:otherwise>False</xsl:otherwise></xsl:choose></xsl:attribute>-->
              <!--Added Rakhi for R7:Add Emp Data Elements-->
            </dg:UserControlDataGrid>
					</xsl:when>
					<xsl:when test="@type[.='labelonly']">
            <asp:Label runat="server">
              <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
              <xsl:if test="@ref">
                <xsl:attribute name="RMXRef"><xsl:value-of select="@ref" /></xsl:attribute>
                <xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
              </xsl:if>
              <xsl:attribute name="Text">
                <xsl:choose>
                  <xsl:when test="@title">
                    <xsl:choose>
                      <xsl:when test="@title[.!='']">
                        <xsl:value-of select="@title"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="text()"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="text()"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:attribute>
              <xsl:if test="@DataRunTime">
                <xsl:attribute name="DataRunTime">
                  <xsl:value-of select="@DataRunTime"/>
                </xsl:attribute>
              </xsl:if>
            </asp:Label>
					</xsl:when>
					<xsl:otherwise>Unknown Element Type: <xsl:value-of select="@type"/>
					</xsl:otherwise>
				</xsl:choose>
            <!--Added by Abhishek to show Tool Tip-->
            <xsl:if test="@helpmsg">
              <img src="../../Images/help-small.gif" valign="middle" style="cursor: pointer">
                <xsl:attribute name="id"><xsl:value-of select="@name" />hlp</xsl:attribute>
                <xsl:attribute name="name"><xsl:value-of select="@name" />hlp</xsl:attribute>
              </img>
            </xsl:if>
            <xsl:if test="@helpmsg">
              <script type="text/javascript">
                DHTMLHelp_setup(
                {
                button : "<xsl:value-of select='@name' />hlp",
                helpmsg: "<xsl:value-of select='@helpmsg'/>",
                width: 250,
                height: 10   // height will autosize so set low
                }
                );
              </script>
            </xsl:if>
          </span>
        
			</div>
      <xsl:choose>

        <xsl:when test="@type[.='buttonscript'] and @name[.='btnGoTo']">
          <br />
          <br />
        </xsl:when>

      </xsl:choose>
			<!--<td>&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;</td>	-->
		</xsl:otherwise></xsl:choose>
	</xsl:otherwise>
</xsl:choose>

	<xsl:if test="@GroupAssoc[.!='']">
		<asp:TextBox style="display:none" runat="server">
			<xsl:attribute name="id"><xsl:value-of select="@name"/>_GroupAssoc</xsl:attribute>
			<xsl:attribute name="Text"><xsl:value-of select="@GroupAssoc"/></xsl:attribute>
		</asp:TextBox>
	</xsl:if>

</xsl:template>

<xsl:template match="internal">
    <xsl:choose>
      <xsl:when test="@type[.='hidden' or .='id']">
        <xsl:choose>
          <xsl:when test="@name[.='SysSerializationConfig']">
            <asp:TextBox style="display:none" runat="server">
              <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
              <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
              <!--<xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>-->
              <!--<xsl:attribute name="value"><xsl:for-each select="*/.">&amp;lt;<xsl:value-of select="local-name()"/>&amp;gt;<xsl:for-each select="*/.">&amp;lt;<xsl:value-of select="local-name()"/>&amp;gt;<xsl:for-each select="*/.">&amp;lt;<xsl:value-of select="local-name()"/>/&amp;gt;</xsl:for-each>&amp;lt;/<xsl:value-of select="local-name()"/>&amp;gt;</xsl:for-each>&amp;lt;/<xsl:value-of select="local-name()"/>&amp;gt;</xsl:for-each></xsl:attribute>-->
              <xsl:attribute name="Text"><xsl:for-each select="*/.">&amp;lt;<xsl:value-of select="local-name()"/>&amp;gt;<xsl:for-each select="*/.">&amp;lt;<xsl:value-of select="local-name()"/>&amp;gt;<xsl:for-each select="*/.">&amp;lt;<xsl:value-of select="local-name()"/>&amp;gt;<xsl:for-each select="*/.">&amp;lt;<xsl:value-of select="local-name()"/>/&amp;gt;</xsl:for-each>&amp;lt;/<xsl:value-of select="local-name()"/>&amp;gt;</xsl:for-each>&amp;lt;/<xsl:value-of select="local-name()"/>&amp;gt;</xsl:for-each>&amp;lt;/<xsl:value-of select="local-name()"/>&amp;gt;</xsl:for-each></xsl:attribute>
            </asp:TextBox>
          </xsl:when>         
          <xsl:otherwise>
            <xsl:if test="@type[.='hidden' or .='id'] and not(contains('|sys_supplemental|sys_formidname|sys_formpidname|sys_formpform|sys_ex|sys_notreqnew|sys_required',@name))">
              <asp:TextBox style="display:none" runat="server">
                <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
                <xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute><xsl:attribute name="RMXType"><xsl:value-of select="@type"/></xsl:attribute>
                <xsl:attribute name="Text"><xsl:value-of select="@value"/></xsl:attribute>
                <xsl:if test="@rmxignorevalue"><xsl:attribute name="rmxignorevalue"><xsl:value-of select="@rmxignorevalue"/></xsl:attribute>
                </xsl:if>
                <xsl:if test="@rmxignoreget"><xsl:attribute name="rmxignoreget"><xsl:value-of select="@rmxignoreget"/></xsl:attribute></xsl:if>
                <xsl:if test="@rmxignoreset"><xsl:attribute name="rmxignoreset"><xsl:value-of select="@rmxignoreset"/></xsl:attribute></xsl:if>
              </asp:TextBox>
            </xsl:if>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <!--rupal,mits 29024-->
      <xsl:when test="@type[.='inputhidden']">
        <input type="hidden" runat="server">
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="name">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:if test="@RMXRef">
            <xsl:attribute name="RMXRef">
              <xsl:value-of select="@ref"/>
            </xsl:attribute>
          </xsl:if>
        </input>
      </xsl:when>
      <!--rupal:end-->
    </xsl:choose>
  </xsl:template>



<xsl:template match="form">
	<xsl:value-of select="@title"/>
</xsl:template>

<xsl:template match="toolbar/button">
	<!--<td align="center" valign="middle" height="32" width="28">-->
  <div class="toolBarButton" runat="server">
    <xsl:attribute name="id">div_<xsl:value-of select="@type"/></xsl:attribute>
    <xsl:choose>
      <xsl:when test="@type[.='movefirst']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClick">NavigateFirst</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_first_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_first_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_first_active.png';</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            <xsl:value-of select="@OnClientClick"/>
          </xsl:attribute>
        </xsl:element>
      </xsl:when>
      <xsl:when test="@type[.='moveprevious']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClick">NavigatePrev</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_previous_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_previous_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_previous_active.png';</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            <xsl:value-of select="@OnClientClick"/>
          </xsl:attribute>
        </xsl:element>
      </xsl:when>
      <xsl:when test="@type[.='movenext']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClick">NavigateNext</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_next_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_next_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_next_active.png';</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            <xsl:value-of select="@OnClientClick"/>
          </xsl:attribute>
        </xsl:element>
      </xsl:when>
      <xsl:when test="@type[.='movelast']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClick">NavigateLast</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_last_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_last_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_last_active.png';</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            <xsl:value-of select="@OnClientClick"/>
          </xsl:attribute>
        </xsl:element>
      </xsl:when>
      <xsl:when test="@type[.='save']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose><xsl:when test="@functionname"><xsl:attribute name="OnClientClick"><xsl:value-of select="@functionname"></xsl:value-of>return OnSaveButtonClicked();</xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return OnSaveButtonClicked();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <!--<xsl:attribute name="OnClientClick">return OnSaveButtonClicked();</xsl:attribute>-->
          <xsl:attribute name="OnClick">NavigateSave</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_save_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
           <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="ValidationGroup">vgSave</xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_save_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_save_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>
      <xsl:when test="@type[.='saveandsend']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClick">NavigateFirst</xsl:attribute>
          <xsl:attribute name="src">../../Images/saveandsend.gif</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/saveandsend2.gif';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/saveandsend.gif';</xsl:attribute>
        </xsl:element>
      </xsl:when>
      <xsl:when test="@type[.='delete']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClick">NavigateDelete</xsl:attribute>
          <xsl:attribute name="OnClientClick">return DeleteRecord();</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_delete_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
           <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_delete_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_delete_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>
		<!--Added by Amitosh for R8 enhancement of EFT Payments-->
      <xsl:when test="@type[.='BankingInfo']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return bankingInfo();</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_bankinfosetup_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_bankinfosetup_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_bankinfosetup_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>
		<!--End Amitosh-->
   
      <xsl:when test="@type[.='new']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClick">NavigateNew</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_new_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_new_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_new_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>
      <!--skhare7: Mits 23664 :-->
      <xsl:when test="@type[.='printpre']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return PrintPreview();</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_printpreview_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
           <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_printpreview_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_printpreview_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>
      <xsl:when test="@type[.='printedimage']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return Printedchecksclone();</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_clonecheck_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
                    <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_clonecheck_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_clonecheck_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>
      <!--skhare7: Mits 23664 End:-->
      <xsl:when test="@type[.='printcheck']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return PrintCheck();</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_printcheck_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
           <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_printcheck_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_printcheck_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>
      <xsl:when test="@type[.='print']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick"><xsl:value-of select="@onclick"/></xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_print_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_print_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_print_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>
      <xsl:when test="@type[.='printeob']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return PrintEOB();</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_printeob_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_printeob_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_printeob_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>
      <xsl:when test="@type[.='voidreissue']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return VoidReissue();</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_voidreissue_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_voidreissue_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_voidreissue_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>
      <xsl:when test="@type[.='editpayee']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return EditPayeeEntity();</xsl:attribute>
          <xsl:attribute name="src">../../Images/tbEditPayee.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tbEditPayee2.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tbEditPayee.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>
      <xsl:when test="@type[.='payeeexp']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return DisplayPayeeExperience();</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_payeeexperience_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_payeeexperience_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_payeeexperience_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>
      <xsl:when test="@type[.='fundspaymenthistory']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return BackToPaymentHistory();</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_paymenthistory_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_paymenthistory_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_paymenthistory_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>      
      <xsl:when test="@type[.='backtofinancials']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return BackToReserves();</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_backtofinancials_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_backtofinancials_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_backtofinancials_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>  
      <xsl:when test="@type[.='loadclaim']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return LoadClaim();</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_loadclaim_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_loadclaim_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_loadclaim_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>    
      <xsl:when test="@type[.='backtotande']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return BackToTimeandExpense();</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_tande_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_tande_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_tande_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>       
      <xsl:when test="@type[.='ofac']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return OFACLookUp();</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_ofac_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_ofac_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_ofac_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>
     <!--ngupta73 MITS 28869-->
    <xsl:when test="@type[.='Clone']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return CloneTransaction();</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_clonepayment_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_clonepayment_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_clonepayment_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>
       <!--ngupta73 MITS 28869-->
      <xsl:when test="@type[.='printsummary']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return PrintTrans();</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_print_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
           <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_print_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_print_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>
      <xsl:when test="@type[.='froi']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClick">NavigateFirst</xsl:attribute>
          <xsl:attribute name="src">../../Images/froi.gif</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/froi2.gif';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/froi.gif';</xsl:attribute>
        </xsl:element>
      </xsl:when>
      <xsl:when test="@type[.='attach']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return attach();</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_attach_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
           <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_attach_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_attach_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>
      <xsl:when test="@type[.='esumm']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return ShowExecSummary();</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_executivesummary_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
           <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_executivesummary_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_executivesummary_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>
      <xsl:when test="@type[.='search']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return doSearch();</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_search_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_search_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_search_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>
      <xsl:when test="@type[.='lookup']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">if(!doLookup()) return false;</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_lookup_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
           <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_lookup_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_lookup_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>
      <xsl:when test="@type[.='summary']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return showSummary();</xsl:attribute>
          <xsl:attribute name="src">../../Images/summary.gif</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/summary2.gif';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/summary.gif';</xsl:attribute>
        </xsl:element>
      </xsl:when>
      <xsl:when test="@type[.='supplemental']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return showSupp();</xsl:attribute>
          <xsl:attribute name="src">../../Images/supp.gif</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/supp2.gif';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/supp.gif';</xsl:attribute>
        </xsl:element>
      </xsl:when>
      <xsl:when test="@type[.='jurisdictionals']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return showJuris();</xsl:attribute>
          <xsl:attribute name="src">../../Images/juris.gif</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/juris2.gif';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/juris.gif';</xsl:attribute>
        </xsl:element>
      </xsl:when>
      <xsl:when test="@type[.='quickdiary']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return QuickDiary();</xsl:attribute>
          <xsl:attribute name="src">../../Images/quickdiary.gif</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/quickdiary2.gif';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/quickdiary.gif';</xsl:attribute>
        </xsl:element>
      </xsl:when>
      <xsl:when test="@type[.='filtereddiary']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return FilteredDiary();</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_diaryviewrecord_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
           <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_diaryviewrecord_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_diaryviewrecord_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>
      <xsl:when test="@type[.='diary']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return Diary();</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_diary_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
           <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_diary_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_diary_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>
      <xsl:when test="@type[.='comments']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return Comments();</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_comments_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
           <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_comments_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_comments_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>
      <xsl:when test="@type[.='mailmerge']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return MailMerge();</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_mailmerge_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
           <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_mailmerge_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_mailmerge_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>
      <xsl:when test="@type[.='recordsummary']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return recordSummary();</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_recordsummary_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
           <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_recordsummary_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_recordsummary_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>
      <xsl:when test="@type[.='psodata']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return psodata();</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_psodata_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_psodata_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_psodata_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>
      <xsl:when test="@type[.='commentsummary']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return CommentSummary();</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_claimcomments_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
           <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_claimcomments_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_claimcomments_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>
      <xsl:when test="@type[.='printadjustertext']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return PrintAdjusterText();</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_print_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
            <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_print_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_print_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>
      <xsl:when test="@type[.='eventexplorer']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return EventExplorer();</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_quicksummary_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
           <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_quicksummary_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_quicksummary_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>
      <xsl:when test="@type[.='sendmail']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return SendMail();</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_sendmail_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
           <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_sendmail_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_sendmail_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>
      <xsl:when test="@type[.='enhancednotes']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return EnhancedNotes();</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_enhancednotes_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
           <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_enhancednotes_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_enhancednotes_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>
      <xsl:when test="@type[.='deletealldiaries']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return DeleteAllDiaries();</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_diarydelete_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
           <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_diarydelete_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_diarydelete_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>
      <!-- rsushilaggar: ISO Claim Search : Start -->
      <!-- smishra25: MITS 22196 : Changing the image to bring it in sync with other toolbar buttons. -->
      <xsl:when test="@type[.='iso']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return ISOClaimSubHistory();</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_iso_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
           <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_iso_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_iso_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>
      <!-- rsushilaggar : ISO Claim Search : End -->
      <!--smishra54: R8 Withholding Enhancement, MITS 26019-->
      <xsl:when test="@type[.='withholding']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return Withholding();</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_witholdingsetup_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_witholdingsetup_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_witholdingsetup_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>
      <!--smishra54:End-->
	<!--nsachdeva2: R8 Claim Activity Log, MITS 26418-->
      <xsl:when test="@type[.='claimActLog']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return ClaimActLog();</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_claimactivity_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
           <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_claimactivity_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_claimactivity_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>
      <!--spahariya: FNOL Claim Reserve MITS 30911 01/09/2013 -->
      <xsl:when test="@type[.='fnolReserve']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="OnClientClick">return FNOLClaimReserve();</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_fnoladdreserve_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_fnoladdreserve_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_fnoladdreserve_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>
	<!--spahariya: End-->
	<!--nsachdeva2: End-->
	<!-- tanwar2: ImageRight -->
     <xsl:when test="@type[.='openImageRight']">
	<xsl:element name="asp:ImageButton">
	  <xsl:attribute name="runat">server</xsl:attribute>
	  <xsl:attribute name="OnClientClick">return OpenImageRightDesktop();</xsl:attribute>
	  <xsl:attribute name="src">../../Images/tb_imageready_active.png</xsl:attribute>
	  <xsl:attribute name="width">28</xsl:attribute>
	  <xsl:attribute name="height">28</xsl:attribute>
	  <xsl:attribute name="border">0</xsl:attribute>
	  <xsl:attribute name="id">
	    <xsl:value-of select="@type"/>
	  </xsl:attribute>
	  <xsl:attribute name="AlternateText">
	    <xsl:value-of select="@title"/>
	  </xsl:attribute>
	  <xsl:attribute name="onMouseOver">this.src='../../Images/tb_imageready_mo.png';</xsl:attribute>
	  <xsl:attribute name="onMouseOut">this.src='../../Images/tb_imageready_active.png';</xsl:attribute>
	</xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='generateFUP']">
	<xsl:element name="asp:ImageButton">
	  <xsl:attribute name="runat">server</xsl:attribute>
	  <xsl:attribute name="OnClientClick">return GenerateFUPFile();</xsl:attribute>
	  <xsl:attribute name="src">../../Images/tb_createFUP_active.png</xsl:attribute>
	  <xsl:attribute name="width">28</xsl:attribute>
	  <xsl:attribute name="height">28</xsl:attribute>
	  <xsl:attribute name="border">0</xsl:attribute>
	  <xsl:attribute name="id">
	    <xsl:value-of select="@type"/>
	  </xsl:attribute>
	  <xsl:attribute name="AlternateText">
	    <xsl:value-of select="@title"/>
	  </xsl:attribute>
	  <xsl:attribute name="onMouseOver">this.src='../../Images/tb_createFUP_mo.png';</xsl:attribute>
	  <xsl:attribute name="onMouseOut">this.src='../../Images/tb_createFUP_active.png';</xsl:attribute>
	</xsl:element>
      </xsl:when>
	<!-- tanwar2: ImageRight -->
	
	
	
      <!-- Indu MITS 28937 For toolbar button-->
      <!--Claimant-->
      <xsl:when test="@type[.='claimHistory']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>return displayclaimhistory('claimant');
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return displayclaimhistory('claimant');</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_claimhistory_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_claimhistory_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_claimhistory_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='financials']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>return gotoreserves();
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return gotoreserves();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_financialreserve_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
            <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_financialreserve_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_financialreserve_active.png';</xsl:attribute>
          <xsl:attribute name="visible">false</xsl:attribute>
        </xsl:element>
        <xsl:if test="@visible">
          <xsl:if test="@visible='false'">
            <xsl:attribute name="style">display:none</xsl:attribute>
          </xsl:if>
        </xsl:if>
      </xsl:when>

      <xsl:when test="@type[.='claimantMMSEAData']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>return MMSEAData();
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return MMSEAData();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_MMSEA_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_MMSEA_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_MMSEA_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>

      <!--Admin Tracking-->
      <xsl:when test="@type[.='tableList']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>return gotoadmintrackingtablelist();
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return gotoadmintrackingtablelist();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_admintrackingtable_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_admintrackingtable_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_admintrackingtable_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>

      <!--Bank Account-->
      <xsl:when test="@type[.='checkStocks']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>return GoToCheckStocks();
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return GoToCheckStocks();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_checkstock_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_checkstock_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_checkstock_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='balance']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>return AccountBalanceReconciliation();
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return AccountBalanceReconciliation();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_balance_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_balance_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_balance_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='accountBalance']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>return LoadAccountBalance();
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return LoadAccountBalance();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_acctbalance_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_acctbalance_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_acctbalance_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='accBankReports']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>return BankAccountReports();
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return BankAccountReports();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_bankacctreport_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_bankacctreport_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_bankacctreport_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='deposit']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_funds_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_funds_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_funds_active.png';</xsl:attribute>
          <xsl:attribute name="RMXRef">/Instance/Account/FundsDepositList/@committedcount</xsl:attribute>
          <xsl:attribute name="PostBackUrl">deposit.aspx?SysFormName=deposit&amp;SysEx=AccountId&amp;SysExMapCtl=AccountId&amp;SysCmd=1</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormName=deposit&amp;SysEx=AccountId&amp;SysExMapCtl=AccountId&amp;SysCmd=1','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='subAccounts']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_bankacctsub_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_bankacctsub_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_bankacctsub_active.png';</xsl:attribute>
          <xsl:attribute name="RMXRef">Instance/Account/BankAccSubList/@committedcount</xsl:attribute>
          <xsl:attribute name="PostBackUrl">bankaccountsub.aspx?SysFormName=bankaccountsub&amp;SysCmd=1&amp;SysEx=AccountId&amp;SysExMapCtl=AccountId</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormName=bankaccountsub&amp;SysCmd=1&amp;SysEx=AccountId&amp;SysExMapCtl=AccountId','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <!--General Claim-->
      <xsl:when test="@type[.='claimAcord']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>return gotoAcordForms();
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return gotoAcordForms();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_accordforms_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_accordforms_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_accordforms_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>

      <!--Workers Compensation Claim-->
      <xsl:when test="@type[.='claimwcWCForms']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>return gotowcpdfforms();
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return gotowcpdfforms();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_wcpdfforms_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_wcpdfforms_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_wcpdfforms_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='claimwcFROI']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>return gotoPrep();
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return gotoPrep();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_froi_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_froi_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_froi_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='claimwcEDIHistory']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>return displayEDIHistory();
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return displayEDIHistory();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_EDIhistory_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_EDIhistory_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_EDIhistory_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='restrictions']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_workrestrictions_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_workrestrictions_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_workrestrictions_active.png';</xsl:attribute>
          <xsl:attribute name="RMXRef">/Instance/Claim/PrimaryPiEmployee/PiXRestrictList/@count</xsl:attribute>
          <xsl:attribute name="PostBackUrl">pirestrictionlist.aspx?SysFormPIdName=claimid&amp;SysFormName=pirestrictionlist&amp;SysCmd=1&amp;SysFormIdName=pirowid&amp;SysViewType=controlsonly&amp;SysEx=ClaimId|ClaimNumber|PiRowId|EventNumber|EventId&amp;SysExMapCtl=ClaimId|ClaimNumber|SysFormPiRowId|EventNumber|EventId&amp;SysExChildControl=ClaimId|ClaimNumber|PiRowId|EventNumber|EventId</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormPIdName=claimid&amp;SysFormName=pirestrictionlist&amp;SysCmd=1&amp;SysFormIdName=pirowid&amp;SysViewType=controlsonly&amp;SysEx=ClaimId|ClaimNumber|PiRowId|EventNumber|EventId&amp;SysExMapCtl=ClaimId|ClaimNumber|SysFormPiRowId|EventNumber|EventId&amp;SysExChildControl=ClaimId|ClaimNumber|PiRowId|EventNumber|EventId','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='workForms']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_workloss_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_workloss_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_workloss_active.png';</xsl:attribute>
          <xsl:attribute name="RMXRef">/Instance/Claim/PrimaryPiEmployee/PiXWorkLossList/@count</xsl:attribute>
          <xsl:attribute name="PostBackUrl">piworklosslist.aspx?SysFormPIdName=claimid&amp;SysFormName=piworklosslist&amp;SysCmd=1&amp;SysViewType=controlsonly&amp;SysFormIdName=pirowid&amp;SysEx=ClaimId|ClaimNumber|PiRowId|EventNumber|EventId|DeptAssignedEid&amp;SysExMapCtl=ClaimId|ClaimNumber|SysFormPiRowId|EventNumber|EventId|DeptAssignedEid&amp;SysExChildControl=ClaimId|ClaimNumber|pirowid|EventNumber|EventId|DeptAssignedEid</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormPIdName=claimid&amp;SysFormName=piworklosslist&amp;SysCmd=1&amp;SysViewType=controlsonly&amp;SysFormIdName=pirowid&amp;SysEx=ClaimId|ClaimNumber|PiRowId|EventNumber|EventId|DeptAssignedEid&amp;SysExMapCtl=ClaimId|ClaimNumber|SysFormPiRowId|EventNumber|EventId|DeptAssignedEid&amp;SysExChildControl=ClaimId|ClaimNumber|pirowid|EventNumber|EventId|DeptAssignedEid','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='medicalDisable']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>return displaymedicalguideline();
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return displaymedicalguideline();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_disabilityguidelines_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_disabilityguidelines_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_disabilityguidelines_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>

      <!--Non Occ Claim-->
      <xsl:when test="@type[.='nonOccPayments']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_planclasses_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
                    <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_planclasses_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_planclasses_active.png';</xsl:attribute>
          <xsl:attribute name="PostBackUrl">nonocc.aspx?SysFormPIdName=claimid&amp;SysFormName=nonocc&amp;SysCmd=0&amp;SysFormIdName=claimid&amp;SysEx=ClaimId|ClaimNumber|PiEid|Entity_Id|PiRowId&amp;SysExMapCtl=ClaimId|ClaimNumber|PiEid|Entity_Id|PiRowId</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormPIdName=claimid&amp;SysFormName=nonocc&amp;SysCmd=0&amp;SysFormIdName=claimid&amp;SysEx=ClaimId|ClaimNumber|PiEid|Entity_Id|PiRowId&amp;SysExMapCtl=ClaimId|ClaimNumber|PiEid|Entity_Id|PiRowId','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;if(!gotononoccpayments())return false;
          </xsl:attribute>
          <!--<xsl:attribute name="OnClick">if(!gotononoccpayments())return false;</xsl:attribute>-->
        </xsl:element>
      </xsl:when>

      <!--Defendant-->
      <xsl:when test="@type[.='defendantClaimHistory']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>return displayclaimhistory('defendant');
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return displayclaimhistory('defendant');</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_claimhistory_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_claimhistory_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_claimhistory_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>

      <!--Exposure Information-->
      <xsl:when test="@type[.='entityExpoRollUp']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>return SetRollUpAttribute();
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return SetRollUpAttribute();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_entityrollup_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_entityrollup_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_entityrollup_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>

      <!--Entity Maintenance-->
      <xsl:when test="@type[.='entityMMSEAData']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>return gotoEntityMMSEAData();
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return gotoEntityMMSEAData();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_MMSEA_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_MMSEA_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_MMSEA_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='entityPaymentHistory']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>return gotoEntityPaymentHistory();
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return gotoEntityPaymentHistory();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_paymenthistory_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_paymenthistory_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_paymenthistory_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='entityContracts']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_physician_contract_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_physician_contract_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_physician_contract_active.png';</xsl:attribute>
          <xsl:attribute name="RMXRef">/Instance/UI/FormVariables/SysExData/ContractCount</xsl:attribute>
          <xsl:attribute name="PostBackUrl">providercontract.aspx?SysFormName=providercontract&amp;SysCmd=1&amp;SysEx=EntityId|EntityTableId&amp;SysExMapCtl=EntityId|EntityTableIdText&amp;SysExChildControl=EntityId|EntityTableId</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormName=providercontract&amp;SysCmd=1&amp;SysEx=EntityId|EntityTableId&amp;SysExMapCtl=EntityId|EntityTableIdText&amp;SysExChildControl=EntityId|EntityTableId','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='entityJurisdiction']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_juris_license_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_juris_license_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_juris_license_active.png';</xsl:attribute>
          <xsl:attribute name="PostBackUrl">jurisdictionlicensecodes.aspx?SysFormName=jurisdictionlicensecodes&amp;SysCmd=1&amp;SysEx=EntityId|EntityTableIdText&amp;SysExMapCtl=EntityId|EntityTableIdText</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormName=jurisdictionlicensecodes&amp;SysCmd=1&amp;SysEx=EntityId|EntityTableIdText&amp;SysExMapCtl=EntityId|EntityTableIdText','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='entityExposure']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_exposure_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_exposure_active.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_exposure_active.png';</xsl:attribute>
          <xsl:attribute name="PostBackUrl">entityexposure.aspx?SysFormName=entityexposure&amp;SysCmd=1&amp;SysEx=EntityId|EntityTableId&amp;SysExMapCtl=EntityId|EntityTableIdText&amp;SysExChildControl=EntityId|EntityTableId</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormName=entityexposure&amp;SysCmd=1&amp;SysEx=EntityId|EntityTableId&amp;SysExMapCtl=EntityId|EntityTableIdText&amp;SysExChildControl=EntityId|EntityTableId','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <!--non-occ payments-->
      <xsl:when test="@type[.='collection']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>return nonocccollection();
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return nonocccollection();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_collection_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_collection_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_collection_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='printedpaym']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>return printedPayments();
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return printedPayments();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_printpayment_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
            <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_printpayment_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_printpayment_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='futurepaym']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>return futurePayments();
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return futurePayments();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_futurepayment_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
                    <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_futurepayment_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_futurepayment_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='calculatepaym']">
	  <script language="JavaScript">
    <xsl:attribute name="src">
      <xsl:value-of select="@includefilename"/>
    </xsl:attribute><![CDATA[{var i;}]]>
  </script>
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>return calcPayments();
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return calcPayments();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_calculatepayment_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
                    <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_calculatepayment_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_calculatepayment_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>

      <!--Organization Hierarchy-->
      <xsl:when test="@type[.='expsureInfo']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>return gotoExposure();
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return gotoExposure();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_exposure_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_exposure_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_exposure_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='selfInsured']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>return gotoSelfInsured();
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return gotoSelfInsured();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_selfinsured_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_selfinsured_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_selfinsured_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='orgHierarchyMMSEA']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>return gotoMMSEA();
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return gotoMMSEA();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_orghierMMSEA_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_orghierMMSEA_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_orghierMMSEA_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='jurisLicence']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_juris_license_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_juris_license_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_juris_license_active.png';</xsl:attribute>
          <xsl:attribute name="PostBackUrl">jurisdictionlicensecodes.aspx?SysFormName=jurisdictionlicensecodes&amp;SysCmd=1&amp;SysEx=EntityId|EntityTableIdText&amp;SysExMapCtl=EntityId|EntityTableIdText</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormName=jurisdictionlicensecodes&amp;SysCmd=1&amp;SysEx=EntityId|EntityTableIdText&amp;SysExMapCtl=EntityId|EntityTableIdText','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <!--Policy Tracking-->
      <xsl:when test="@type[.='clonePolicy']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>return ClonePolicy();
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return ClonePolicy();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:attribute name ="OnClick">ClonePolicy</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_policyclone_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_policyclone_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_policyclone_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='PolicyCoverages']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>

          <xsl:attribute name="src">../../Images/tb_policy_coverages_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_policy_coverages_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_policy_coverages_active.png';</xsl:attribute>
          <xsl:attribute name="RMXRef">Instance/Policy/PolicyXCvgTypeList/@committedcount</xsl:attribute>
          <xsl:attribute name="PostBackUrl">policycoverage.aspx?SysFormName=policycoverage&amp;SysCmd=1&amp;SysFormIdName=polcvgrowid&amp;SysEx=PolicyId&amp;SysExMapCtl=PolicyId</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormName=policycoverage&amp;SysCmd=1&amp;SysFormIdName=polcvgrowid&amp;SysEx=PolicyId&amp;SysExMapCtl=PolicyId','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='insurer']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>

          <xsl:attribute name="src">../../Images/tb_policyinsurer_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_policyinsurer_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_policyinsurer_active.png';</xsl:attribute>
          <xsl:attribute name="RMXRef">Instance/Policy/PolicyXInsurerList/@committedcount</xsl:attribute>
          <xsl:attribute name="PostBackUrl">policyinsurer.aspx?SysFormName=policyinsurer&amp;SysCmd=1&amp;SysEx=PolicyId&amp;SysExMapCtl=PolicyId</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormName=policyinsurer&amp;SysCmd=1&amp;SysEx=PolicyId&amp;SysExMapCtl=PolicyId','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='policyMCO']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>

          <xsl:attribute name="src">../../Images/tb_policymco_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_policymco_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_policymco_active.png';</xsl:attribute>
          <xsl:attribute name="RMXRef">Instance/Policy/PolicyXMcoList/@committedcount</xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="PostBackUrl">policymco.aspx?SysFormName=policymco&amp;SysCmd=1&amp;SysFilter=0&amp;SysFormIdName=polxmcorowid&amp;SysEx=PolicyId&amp;SysExMapCtl=PolicyId</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormName=policymco&amp;SysCmd=1&amp;SysFilter=0&amp;SysFormIdName=polxmcorowid&amp;SysEx=PolicyId&amp;SysExMapCtl=PolicyId','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <!--Policy Enhancement-->
      <xsl:when test="@type[.='convertToPolicy']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>return ConvertToPolicy();
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return ConvertToPolicy();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:choose>
            <xsl:when test="disabled[@value='true']">
              <xsl:attribute name="Enabled">False</xsl:attribute>
            </xsl:when>
            <xsl:when test="disabled[@value='false']">
              <xsl:attribute name="Enabled">True</xsl:attribute>
            </xsl:when>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_policyconvert_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_policyconvert_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_policyconvert_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='amendPolicy']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:choose>
            <xsl:when test="disabled[@value='true']">
              <xsl:attribute name="Enabled">False</xsl:attribute>
            </xsl:when>
            <xsl:when test="disabled[@value='false']">
              <xsl:attribute name="Enabled">True</xsl:attribute>
            </xsl:when>
          </xsl:choose>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return AmendPolicy();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_policyammend_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_policyammend_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_policyammend_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='renewPolicy']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:choose>
            <xsl:when test="@disabled">
              <xsl:attribute name="Enabled">False</xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="Enabled">False</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return RenewPolicy();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_policyrenew_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_policyrenew_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_policyrenew_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='cancelPolicy']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:choose>
            <xsl:when test="@disabled">
              <xsl:attribute name="Enabled">False</xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="Enabled">False</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return CancelPolicy();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_policycancel_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_policycancel_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_policycancel_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='reinstatePolicy']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:choose>
            <xsl:when test="@disabled">
              <xsl:attribute name="Enabled">False</xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="Enabled">False</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return ReinstatePolicy();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_policyreinstate_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_policyreinstate_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_policyreinstate_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='auditPolicy']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>return AuditPolicy();
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return AuditPolicy();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:choose>
            <xsl:when test="disabled[@value='true']">
              <xsl:attribute name="Enabled">False</xsl:attribute>
            </xsl:when>
            <xsl:when test="disabled[@value='false']">
              <xsl:attribute name="Enabled">True</xsl:attribute>
            </xsl:when>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_policyaudit_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_policyaudit_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_policyaudit_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='printPolicy']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>return PolicyPrint();
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return PolicyPrint();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:choose>
            <xsl:when test="disabled[@value='true']">
              <xsl:attribute name="Enabled">False</xsl:attribute>
            </xsl:when>
            <xsl:when test="disabled[@value='false']">
              <xsl:attribute name="Enabled">True</xsl:attribute>
            </xsl:when>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_policyprint_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
            <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_policyprint_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_policyprint_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='mco']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose>
            <xsl:when test="disabled[@value='true']">
              <xsl:attribute name="Enabled">False</xsl:attribute>
            </xsl:when>
            <xsl:when test="disabled[@value='false']">
              <xsl:attribute name="Enabled">True</xsl:attribute>
            </xsl:when>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_policymco_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_policymco_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_policymco_active.png';</xsl:attribute>
          <xsl:attribute name="RMXRef">Instance/PolicyEnh/PolicyXMcoEnhList/@committedcount</xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="PostBackUrl">Policymcoenh.aspx?SysFormPIdName=policyid&amp;SysFormName=policymcoenh&amp;SysCmd=1&amp;SysFilter=0&amp;SysFormIdName=polxmcoenhrowid&amp;SysEx=PolicyId&amp;SysExMapCtl=PolicyId</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormPIdName=policyid&amp;SysFormName=policymcoenh&amp;SysCmd=1&amp;SysFilter=0&amp;SysFormIdName=polxmcoenhrowid&amp;SysEx=PolicyId&amp;SysExMapCtl=PolicyId','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='propertyMco']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>

          <xsl:attribute name="src">../../Images/tb_policymco_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:choose>
            <xsl:when test="disabled[@value='true']">
              <xsl:attribute name="Enabled">False</xsl:attribute>
            </xsl:when>
            <xsl:when test="disabled[@value='false']">
              <xsl:attribute name="Enabled">True</xsl:attribute>
            </xsl:when>
          </xsl:choose>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_policymco_active.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_policymco_active.png';</xsl:attribute>
          <xsl:attribute name="RMXRef">Instance/PolicyEnh/PolicyXMcoEnhList/@committedcount</xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="PostBackUrl">Policymcoenh.aspx?SysFormPIdName=policyid&amp;SysFormName=policymcoenh&amp;SysCmd=1&amp;SysFilter=0&amp;SysFormIdName=polxmcoenhrowid&amp;SysEx=PolicyId&amp;SysExMapCtl=PolicyId</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormPIdName=policyid&amp;SysFormName=policymcoenh&amp;SysCmd=1&amp;SysFilter=0&amp;SysFormIdName=polxmcoenhrowid&amp;SysEx=PolicyId&amp;SysExMapCtl=PolicyId','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='WorkersMco']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_policymco_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:choose>
            <xsl:when test="disabled[@value='true']">
              <xsl:attribute name="Enabled">False</xsl:attribute>
            </xsl:when>
            <xsl:when test="disabled[@value='false']">
              <xsl:attribute name="Enabled">True</xsl:attribute>
            </xsl:when>
          </xsl:choose>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_policymco_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_policymco_active.png';</xsl:attribute>
          <xsl:attribute name="RMXRef">Instance/PolicyEnh/PolicyXMcoEnhList/@committedcount</xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="PostBackUrl">Policymcoenh.aspx?SysFormPIdName=policyid&amp;SysFormName=policymcoenh&amp;SysCmd=1&amp;SysFilter=0&amp;SysFormIdName=polxmcoenhrowid&amp;SysEx=PolicyId&amp;SysExMapCtl=PolicyId</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormPIdName=policyid&amp;SysFormName=policymcoenh&amp;SysCmd=1&amp;SysFilter=0&amp;SysFormIdName=polxmcoenhrowid&amp;SysEx=PolicyId&amp;SysExMapCtl=PolicyId','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <!--Unit-->
      <xsl:when test="@type[.='unitClaimHistory']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>return displayclaimhistory('unit');
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return displayclaimhistory('unit');</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_claimhistory_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_claimhistory_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_claimhistory_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>

      <!--Accidents-->
      <xsl:when test="@type[.='vehicleAccidents']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>return vehicleAccidentsList();
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return vehicleAccidentsList();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_vehicle_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_vehicle_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_vehicle_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='inspections']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_ofac_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_ofac_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_ofac_active.png';</xsl:attribute>
          <xsl:attribute name="RMXRef">Instance/Vehicle/VehicleXInspctList/@committedcount</xsl:attribute>
          <xsl:attribute name="PostBackUrl">vehicleinspections.aspx?SysFormPIdName=unitid&amp;SysFormName=vehicleinspections&amp;SysCmd=1&amp;SysFormIdName=unitinsprowid&amp;SysEx=UnitId&amp;SysExMapCtl=UnitId</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormPIdName=unitid&amp;SysFormName=vehicleinspections&amp;SysCmd=1&amp;SysFormIdName=unitinsprowid&amp;SysEx=UnitId&amp;SysExMapCtl=UnitId','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <!--Auto Claim Checks-->
      <xsl:when test="@type[.='selectBatch']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>return SelectBatch();
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return SelectBatch();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_autocheckselectbatch_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_autocheckselectbatch_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_autocheckselectbatch_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>

      <!--Leave-->
      <xsl:when test="@type[.='leaveHistory']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>return gotoLeaveHistory();
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return gotoLeaveHistory();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_leavehistory_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
            <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_leavehistory_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_leavehistory_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>

      <!--Osha-->
      <xsl:when test="@type[.='oshaPrint301']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>return Print301report();
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return Print301report();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_OSHA_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_OSHA_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_OSHA_active.png';</xsl:attribute>
          <xsl:attribute name="enableForNew">1</xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='oshaRecordabilityWizard2']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>return OSHARecordabilityWizard();
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return OSHARecordabilityWizard();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_OSHAwizard_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_OSHAwizard_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_OSHAwizard_active.png';</xsl:attribute>
          <xsl:attribute name="enableForNew">1</xsl:attribute>

        </xsl:element>
      </xsl:when>

      <!--Sentinel-->
      <xsl:when test="@type[.='sentinelReport']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>return getsentinelreport();
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return getsentinelreport();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_printreport_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_printreport_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_printreport_active.png';</xsl:attribute>
          <xsl:attribute name="enableForNew">1</xsl:attribute>

        </xsl:element>
      </xsl:when>

      <!--Plan-->
      <xsl:when test="@type[.='planClasses']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_planclasses_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_planclasses_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_planclasses_active.png';</xsl:attribute>
          <xsl:attribute name="RMXRef">/Instance/DisabilityPlan/ClassList/@committedcount</xsl:attribute>
          <xsl:attribute name="PostBackUrl">planclasses.aspx?SysFormPIdName=planid&amp;SysFormName=planclasses&amp;SysCmd=1&amp;SysFormIdName=classrowid&amp;SysEx=PlanId&amp;SysExMapCtl=PlanId</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(document.getElementById('PrefDayOfMCode')!=null) document.getElementById('PrefDayOfMCode').disabled = false;if(!(XFormHandler('SysFormPIdName=planid&amp;SysFormName=planclasses&amp;SysCmd=1&amp;SysFormIdName=classrowid&amp;SysEx=PlanId&amp;SysExMapCtl=PlanId','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <!--Physician-->
      <xsl:when test="@type[.='contracts']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_physician_contract_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_physician_contract_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_physician_contract_active.png';</xsl:attribute>
          <xsl:attribute name="RMXRef">/Instance/UI/FormVariables/SysExData/ContractCount</xsl:attribute>
          <xsl:attribute name="PostBackUrl">providercontract.aspx?SysFormName=providercontract&amp;SysCmd=1&amp;SysEx=EntityId|EntityTableId&amp;SysExMapCtl=EntityId|EntityTableId</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormName=providercontract&amp;SysCmd=1&amp;SysEx=EntityId|EntityTableId&amp;SysExMapCtl=EntityId|EntityTableId','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='privileges']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_staffprivileges_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_staffprivileges_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_staffprivileges_active.png';</xsl:attribute>
          <xsl:attribute name="RMXRef">/Instance/Physician/PrivilegeList/@committedcount</xsl:attribute>
          <xsl:attribute name="PostBackUrl">physicianprivilege.aspx?SysFormName=physicianprivilege&amp;SysEx=PhysEid&amp;SysExMapCtl=PhysEid&amp;SysCmd=1</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormName=physicianprivilege&amp;SysEx=PhysEid&amp;SysExMapCtl=PhysEid&amp;SysCmd=1','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='certifications']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_physiciancertification_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_physiciancertification_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_physiciancertification_active.png';</xsl:attribute>
          <xsl:attribute name="RMXRef">/Instance/Physician/CertificationList/@committedcount</xsl:attribute>
          <xsl:attribute name="PostBackUrl">physiciancertification.aspx?SysFormName=physiciancertification&amp;SysEx=PhysEid&amp;SysExMapCtl=PhysEid&amp;SysCmd=1</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormName=physiciancertification&amp;SysEx=PhysEid&amp;SysExMapCtl=PhysEid&amp;SysCmd=1','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='education']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_physeducation_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_physeducation_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_physeducation_active.png';</xsl:attribute>
          <xsl:attribute name="RMXRef">/Instance/Physician/EducationList/@committedcount</xsl:attribute>
          <xsl:attribute name="PostBackUrl">physicianeducation.aspx?SysFormName=physicianeducation&amp;SysEx=PhysEid&amp;SysExMapCtl=PhysEid&amp;SysCmd=1</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormName=physicianeducation&amp;SysEx=PhysEid&amp;SysExMapCtl=PhysEid&amp;SysCmd=1','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='previousHospitals']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_physician_prev_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_physician_prev_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_physician_prev_active.png';</xsl:attribute>
          <xsl:attribute name="RMXRef">/Instance/Physician/PrevHospitalList/@committedcount</xsl:attribute>
          <xsl:attribute name="PostBackUrl">physicianprevhospital.aspx?SysFormName=physicianprevhospital&amp;SysEx=PhysEid&amp;SysExMapCtl=PhysEid&amp;SysCmd=1</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormName=physicianprevhospital&amp;SysEx=PhysEid&amp;SysExMapCtl=PhysEid&amp;SysCmd=1','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <!--Event-->
      <xsl:when test="@type[.='sentinel']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_sentinel_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:choose>
            <xsl:when test="disabled[@value='true']">
              <xsl:attribute name="Enabled">False</xsl:attribute>
            </xsl:when>
            <xsl:when test="disabled[@value='false']">
              <xsl:attribute name="Enabled">True</xsl:attribute>
            </xsl:when>
          </xsl:choose>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_sentinel_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_sentinel_active.png';</xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="PostBackUrl">sentinel.aspx?SysFormName=sentinel&amp;SysEx=EventNumber|EventId&amp;SysExMapCtl=EventNumber|EventId&amp;SysCmd=1</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormName=sentinel&amp;SysEx=EventNumber|EventId&amp;SysExMapCtl=EventNumber|EventId&amp;SysCmd=1','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <!--Employee-->
      <xsl:when test="@type[.='violations']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>

          <xsl:attribute name="src">../../Images/tb_blockmodules_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_blockmodules_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_blockmodules_active.png';</xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="RMXRef">/Instance/Employee/EmpXViolationList/@committedcount</xsl:attribute>
          <xsl:attribute name="PostBackUrl">violation.aspx?SysFormName=violation&amp;SysCmd=1&amp;SysEx=EmployeeEid&amp;SysExMapCtl=EmployeeEid</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormName=violation&amp;SysCmd=1&amp;SysEx=EmployeeEid&amp;SysExMapCtl=EmployeeEid','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='dependants']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>

          <xsl:attribute name="src">../../Images/tb_selfinsured_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_selfinsured_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_selfinsured_active.png';</xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="RMXRef">/Instance/Employee/EmpXDependentList/@committedcount</xsl:attribute>
          <xsl:attribute name="PostBackUrl">dependent.aspx?SysFormName=dependent&amp;SysCmd=1&amp;SysEx=EmployeeEid|LastName&amp;SysExMapCtl=EmployeeEid|LastName</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormName=dependent&amp;SysCmd=1&amp;SysEx=EmployeeEid|LastName&amp;SysExMapCtl=EmployeeEid|LastName','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <!--staff-->
      <xsl:when test="@type[.='staffPrivileges']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>

          <xsl:attribute name="src">../../Images/tb_staffprivileges_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_staffprivileges_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_staffprivileges_active.png';</xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="RMXRef">/Instance/MedicalStaff/PrivilegeList/@committedcount</xsl:attribute>
          <xsl:attribute name="PostBackUrl">staffprivilege.aspx?SysFormName=staffprivilege&amp;SysCmd=1&amp;SysEx=StaffEid&amp;SysExMapCtl=StaffEid</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormName=staffprivilege&amp;SysCmd=1&amp;SysEx=StaffEid&amp;SysExMapCtl=StaffEid','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='staffCertifications']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>

          <xsl:attribute name="src">../../Images/tb_physiciancertification_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_physiciancertification_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_physiciancertification_active.png';</xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="RMXRef">/Instance/MedicalStaff/CertificationList/@committedcount</xsl:attribute>
          <xsl:attribute name="PostBackUrl">staffcertification.aspx?SysFormName=staffcertification&amp;SysCmd=1&amp;SysEx=StaffEid&amp;SysExMapCtl=StaffEid</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormName=staffcertification&amp;SysCmd=1&amp;SysEx=StaffEid&amp;SysExMapCtl=StaffEid','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <!--Patient-->
      <xsl:when test="@type[.='procedures']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>

          <xsl:attribute name="src">../../Images/tb_patientprocedure_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_patientprocedure_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_patientprocedure_active.png';</xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="RMXRef">/Instance/Patient/PatientXProcedureList/@committedcount</xsl:attribute>
          <xsl:attribute name="PostBackUrl">patientprocedure.aspx?SysFormName=patientprocedure&amp;SysEx=PatientId&amp;SysExMapCtl=PatientId&amp;SysCmd=1</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormName=patientprocedure&amp;SysEx=PatientId&amp;SysExMapCtl=PatientId&amp;SysCmd=1','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <!--People-->
      <xsl:when test="@type[.='contracts']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>

          <xsl:attribute name="src">../../Images/tb_physician_contract_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_physician_contract_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_physician_contract_active.png';</xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="RMXRef">/Instance/UI/FormVariables/SysExData/ContractCount</xsl:attribute>
          <xsl:attribute name="PostBackUrl">providercontract.aspx?SysFormName=providercontract&amp;SysCmd=1&amp;SysEx=EntityId|EntityTableId&amp;SysExMapCtl=EntityId|EntityTableIdText&amp;SysExChildControl=EntityId|EntityTableId</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormName=providercontract&amp;SysCmd=1&amp;SysEx=EntityId|EntityTableId&amp;SysExMapCtl=EntityId|EntityTableIdText&amp;SysExChildControl=EntityId|EntityTableId','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='jurisdictLicense']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>

          <xsl:attribute name="src">../../Images/tb_juris_license_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_juris_license_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_juris_license_active.png';</xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="PostBackUrl">jurisdictionlicensecodes.aspx?SysFormName=jurisdictionlicensecodes&amp;SysCmd=1&amp;SysEx=EntityId|EntityTableIdText&amp;SysExMapCtl=EntityId|EntityTableIdText</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormName=jurisdictionlicensecodes&amp;SysCmd=1&amp;SysEx=EntityId|EntityTableIdText&amp;SysExMapCtl=EntityId|EntityTableIdText','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <!--PiEmployee-->
      <xsl:when test="@type[.='restriction']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>

          <xsl:attribute name="src">../../Images/tb_workrestrictions_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_workrestrictions_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_workrestrictions_active.png';</xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="RMXRef">/Instance/PiEmployee/PiXRestrictList/@committedcount</xsl:attribute>
          <xsl:attribute name="PostBackUrl">pirestrictionlist.aspx?SysFormName=pirestrictionlist&amp;SysEx=PiRowId|EventId&amp;SysExMapCtl=PiRowId|EventId&amp;SysCmd=1&amp;SysViewType=controlsonly</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormName=pirestrictionlist&amp;SysEx=PiRowId|EventId&amp;SysExMapCtl=PiRowId|EventId&amp;SysCmd=1&amp;SysViewType=controlsonly','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='workLoss']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>

          <xsl:attribute name="src">../../Images/tb_workloss_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_workloss_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_workloss_active.png';</xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="RMXRef">/Instance/PiEmployee/PiXWorkLossList/@committedcount</xsl:attribute>
          <xsl:attribute name="PostBackUrl">piworklosslist.aspx?SysFormName=piworklosslist&amp;SysEx=PiRowId|EventId|DeptAssignedEid&amp;SysExMapCtl=PiRowId|EventId|DeptAssignedEid&amp;SysCmd=1&amp;SysViewType=controlsonly</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormName=piworklosslist&amp;SysEx=PiRowId|EventId|DeptAssignedEid&amp;SysExMapCtl=PiRowId|EventId|DeptAssignedEid&amp;SysCmd=1&amp;SysViewType=controlsonly','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <!--PI Patient-->
      <xsl:when test="@type[.='procedure']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_patientprocedure_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_patientprocedure_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_patientprocedure_active.png';</xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="RMXRef">/Instance/PiPatient/PiXProcedureList/@committedcount</xsl:attribute>
          <xsl:attribute name="PostBackUrl">piprocedure.aspx?SysFormName=piprocedure&amp;SysEx=PiRowId|EventId|PiEid|PatientId|ClaimNumber&amp;SysExMapCtl=PiRowId|EventId|PiEid|PatientId|ClaimNumber&amp;SysCmd=1</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormName=piprocedure&amp;SysEx=PiRowId|EventId|PiEid|PatientId|ClaimNumber&amp;SysExMapCtl=PiRowId|EventId|PiEid|PatientId|ClaimNumber&amp;SysCmd=1','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <!--PI Medstaff-->
      <xsl:when test="@type[.='medPrivileges']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_staffprivileges_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_staffprivileges_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_staffprivileges_active.png';</xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="RMXRef">/Instance/PiMedicalStaff/MedicalStaff/PrivilegeList/@committedcount</xsl:attribute>
          <xsl:attribute name="PostBackUrl">pimedstaffprivilege.aspx?SysFormName=pimedstaffprivilege&amp;SysEx=StaffEid|PiRowId|PiEid|EventId|DupSubTitle&amp;SysExMapCtl=StaffEid|PiRowId|PiEid|EventId|DupSubTitle&amp;SysCmd=1</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormName=pimedstaffprivilege&amp;SysEx=StaffEid|PiRowId|PiEid|EventId|DupSubTitle&amp;SysExMapCtl=StaffEid|PiRowId|PiEid|EventId|DupSubTitle&amp;SysCmd=1','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='medCertifications']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_physiciancertification_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_physiciancertification_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_physiciancertification_active.png';</xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="RMXRef">/Instance/PiMedicalStaff/MedicalStaff/CertificationList/@committedcount</xsl:attribute>
          <xsl:attribute name="PostBackUrl">pimedstaffcertification.aspx?SysFormName=pimedstaffcertification&amp;SysEx=StaffEid|PiRowId|PiEid|EventId|DupSubTitle&amp;SysExMapCtl=StaffEid|PiRowId|PiEid|EventId|DupSubTitle&amp;SysCmd=1</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormName=pimedstaffcertification&amp;SysEx=StaffEid|PiRowId|PiEid|EventId|DupSubTitle&amp;SysExMapCtl=StaffEid|PiRowId|PiEid|EventId|DupSubTitle&amp;SysCmd=1','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <!--PI Physician-->
      <xsl:when test="@type[.='physContracts']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_physician_contract_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_physician_contract_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_physician_contract_active.png';</xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="RMXRef">/Instance/UI/FormVariables/SysExData/ContractCount</xsl:attribute>
          <xsl:attribute name="PostBackUrl">providercontract.aspx?SysFormName=providercontract&amp;SysCmd=1&amp;SysEx=EntityId|EntityTableId&amp;SysExMapCtl=EntityId|EntityTableId</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormName=providercontract&amp;SysCmd=1&amp;SysEx=EntityId|EntityTableId&amp;SysExMapCtl=EntityId|EntityTableId','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='physPrivileges']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_staffprivileges_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_staffprivileges_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_staffprivileges_active.png';</xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="RMXRef">Instance/PiPhysician/Physician/PrivilegeList/@committedcount</xsl:attribute>
          <xsl:attribute name="PostBackUrl">piprivilege.aspx?SysFormName=piprivilege&amp;SysEx=PiRowId|EventId|PiEid|PhysEid|ClaimNumber&amp;SysExMapCtl=PiRowId|EventId|PiEid|PhysEid|ClaimNumber&amp;SysCmd=1</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormName=piprivilege&amp;SysEx=PiRowId|EventId|PiEid|PhysEid|ClaimNumber&amp;SysExMapCtl=PiRowId|EventId|PiEid|PhysEid|ClaimNumber&amp;SysCmd=1','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='physCertification']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_physiciancertification_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_physiciancertification_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_physiciancertification_active.png';</xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="RMXRef">Instance/PiPhysician/Physician/CertificationList/@committedcount</xsl:attribute>
          <xsl:attribute name="PostBackUrl">picertification.aspx?SysFormName=picertification&amp;SysEx=PiRowId|EventId|PiEid|PhysEid|ClaimNumber&amp;SysExMapCtl=PiRowId|EventId|PiEid|PhysEid|ClaimNumber&amp;SysCmd=1</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormName=picertification&amp;SysEx=PiRowId|EventId|PiEid|PhysEid|ClaimNumber&amp;SysExMapCtl=PiRowId|EventId|PiEid|PhysEid|ClaimNumber&amp;SysCmd=1','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='physEducation']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_physeducation_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_physeducation_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_physeducation_active.png';</xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="RMXRef">Instance/PiPhysician/Physician/EducationList/@committedcount</xsl:attribute>
          <xsl:attribute name="PostBackUrl">pieducation.aspx?SysFormName=pieducation&amp;SysEx=PiRowId|EventId|PiEid|PhysEid|ClaimNumber&amp;SysExMapCtl=PiRowId|EventId|PiEid|PhysEid|ClaimNumber&amp;SysCmd=1</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormName=pieducation&amp;SysEx=PiRowId|EventId|PiEid|PhysEid|ClaimNumber&amp;SysExMapCtl=PiRowId|EventId|PiEid|PhysEid|ClaimNumber&amp;SysCmd=1','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <xsl:when test="@type[.='physPrevHospitals']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_physician_prev_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_physician_prev_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_physician_prev_active.png';</xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="RMXRef">Instance/PiPhysician/Physician/PrevHospitalList/@committedcount</xsl:attribute>
          <xsl:attribute name="PostBackUrl">piprevhospital.aspx?SysFormName=piprevhospital&amp;SysEx=PiRowId|EventId|PiEid|PhysEid|ClaimNumber&amp;SysExMapCtl=PiRowId|EventId|PiEid|PhysEid|ClaimNumber&amp;SysCmd=1</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormName=piprevhospital&amp;SysEx=PiRowId|EventId|PiEid|PhysEid|ClaimNumber&amp;SysExMapCtl=PiRowId|EventId|PiEid|PhysEid|ClaimNumber&amp;SysCmd=1','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <!--Bank Account Sub-->
      <xsl:when test="@type[.='subAccountBalance']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@functionname">
              <xsl:attribute name="OnClientClick">
                <xsl:value-of select="@functionname"></xsl:value-of>return LoadDisbursementAccountBalance();
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="OnClientClick">return LoadDisbursementAccountBalance();</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:attribute name="src">../../Images/tb_acctbalance_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
            <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_acctbalance_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_acctbalance_active.png';</xsl:attribute>
        </xsl:element>
      </xsl:when>

      <!--Reinsurer-->
      <xsl:when test="@type[.='policyreinsurer']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_selfinsured_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:attribute name="AlternateText">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
             <xsl:attribute name="Title">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_selfinsured_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_selfinsured_active.png';</xsl:attribute>
          <xsl:attribute name="RMXRef">Instance/PolicyXInsurer/PolicyXReInsurerList/@committedcount</xsl:attribute>
          <xsl:attribute name="PostBackUrl">policyreinsurer.aspx?SysFormName=policyreinsurer&amp;SysCmd=1&amp;SysEx=InRowID&amp;SysExMapCtl=InRowID</xsl:attribute>
          <xsl:attribute name="OnClientClick">
            if(!(XFormHandler('SysFormName=policyreinsurer&amp;SysCmd=1&amp;SysEx=InRowID&amp;SysExMapCtl=InRowID','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
        </xsl:element>
      </xsl:when>

      <!--Common for all Back buttons-->
      <xsl:when test="@type[.='back']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_backrecord_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">BackToParent</xsl:attribute>
          <!--<xsl:attribute name="AlternateText">-->
          <xsl:attribute name="Text">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_backrecord_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_backrecord_active.png';</xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="OnClientClick">
            <xsl:if test="@onclick">
              <xsl:value-of select="@onclick"/>
            </xsl:if>if(!( XFormHandler('<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
          <xsl:attribute name="PostBackUrl">
            <xsl:value-of select="@pagetomove"/>?<xsl:value-of select="@param"/>
          </xsl:attribute>
          <xsl:if test="@onServerClick">
            <xsl:attribute name="OnClick">
              <xsl:value-of select="@onServerClick"/>
            </xsl:attribute>
			</xsl:if>
			<xsl:if test='@visible'>
                      <xsl:attribute name="visible">
                      <xsl:value-of select="@visible"/></xsl:attribute>
                  </xsl:if>
          
          <!--<xsl:attribute name="RMXRef">Instance/PiPhysician/Physician/PrevHospitalList/@committedcount</xsl:attribute>-->
          <!--<xsl:attribute name="PostBackUrl">?</xsl:attribute>
          <xsl:attribute name="OnClientClick">if(!(XFormHandler('@','1','<xsl:value-of select="@type"/>')))return false;</xsl:attribute>-->
        </xsl:element>
      </xsl:when>
      <!--Added this for Policy and Plan-->
      <xsl:when test="@type[.='backTo']">
        <xsl:element name="asp:ImageButton">
          <xsl:attribute name="runat">server</xsl:attribute>
          <xsl:attribute name="src">../../Images/tb_backrecord_active.png</xsl:attribute>
          <xsl:attribute name="width">28</xsl:attribute>
          <xsl:attribute name="height">28</xsl:attribute>
          <xsl:attribute name="border">0</xsl:attribute>
          <xsl:attribute name="id">btnBack</xsl:attribute>
          <!--<xsl:attribute name="AlternateText">-->
          <xsl:attribute name="Text">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="onMouseOver">this.src='../../Images/tb_backrecord_mo.png';</xsl:attribute>
          <xsl:attribute name="onMouseOut">this.src='../../Images/tb_backrecord_active.png';</xsl:attribute>
          <xsl:attribute name="ToolTip">
            <xsl:value-of select="@title"/>
          </xsl:attribute>
          <xsl:attribute name="OnClientClick">
            <xsl:if test="@onclick">
              <xsl:value-of select="@onclick"/>
            </xsl:if>if(!( XFormHandler('<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
          </xsl:attribute>
          <xsl:attribute name="PostBackUrl">
            <xsl:value-of select="@pagetomove"/>?<xsl:value-of select="@param"/>
          </xsl:attribute>
          <xsl:if test="@onServerClick">
            <xsl:attribute name="OnClick">
              <xsl:value-of select="@onServerClick"/>
            </xsl:attribute>
          </xsl:if>
        </xsl:element>
      </xsl:when>
      <!--End of MITS 28937-->
      <xsl:otherwise>
        Unknown Toolbar Button: <xsl:value-of select="@type"/>
      </xsl:otherwise>
    </xsl:choose>
  </div>
	<!--</td>-->
</xsl:template>

<!--Ramkumar MITS 33606-->
<xsl:template match="displaycolumn/control[@type='imagebutton'] | displaycolumn/control/control[@type='imagebutton']">
  <xsl:variable name="src">../../Images/</xsl:variable>
  <xsl:variable name="this">this.src='../../Images/</xsl:variable>
  <xsl:variable name="quote">'</xsl:variable>
  <div class="toolBarButton" runat="server">
    <xsl:attribute name="id">div_<xsl:value-of select="@name"/></xsl:attribute>
    <xsl:element name="asp:ImageButton">
      <xsl:attribute name="runat">server</xsl:attribute>
      <xsl:attribute name="src">
        <xsl:value-of select="concat($src,@activeimage)"/>
      </xsl:attribute>
      <xsl:attribute name="width">28</xsl:attribute>
      <xsl:attribute name="height">28</xsl:attribute>
      <xsl:attribute name="border">0</xsl:attribute>
      <xsl:attribute name="onMouseOver">
        <xsl:value-of select="concat($this,@moimage,$quote)"/>
      </xsl:attribute>
      <xsl:attribute name="onMouseOut">
        <xsl:value-of select="concat($this,@activeimage,$quote)"/>
      </xsl:attribute>
      <xsl:attribute name="id">
        <xsl:value-of select="@name"/>
      </xsl:attribute>
      <xsl:attribute name="ToolTip">
        <xsl:value-of select="@title"/>
      </xsl:attribute>
      <!--mona: added so that normal buttons of the screen can also show count of child screens like leave management in non occ-->
      <xsl:if test="@countref">
        <xsl:attribute name="RMXRef">
          <xsl:value-of select="@countref"/>
        </xsl:attribute>
      </xsl:if>
      <!--<xsl:attribute name="RMXRef"><xsl:value-of select="@ref"/></xsl:attribute>-->
      <xsl:attribute name="RMXType">
        <xsl:value-of select="@type"/>
      </xsl:attribute>
      <xsl:if test="(@PowerViewReadOnly[.='true'])">
        <xsl:attribute name="disabled">true</xsl:attribute>
        <xsl:attribute name="powerviewreadonly">true</xsl:attribute>
      </xsl:if>
      <xsl:attribute name="Text">
        <xsl:value-of select="@title"/>
      </xsl:attribute>
      <xsl:if test="@onServerClick">
        <xsl:attribute name="OnClick">
          <xsl:value-of select="@onServerClick"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:attribute name="OnClientClick">
        <xsl:choose>
          <xsl:when test="@functionname">if(!(<xsl:value-of select="@functionname"/>&amp;&amp;XFormHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;</xsl:when>
          <xsl:otherwise>if(!(XFormHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;</xsl:otherwise>
        </xsl:choose >
      </xsl:attribute>
      <xsl:attribute name="PostBackUrl">
        <xsl:value-of select="@pagetomove"/>?<xsl:value-of select="@param"/>
      </xsl:attribute>
      <xsl:if test='@visible'>
        <xsl:attribute name="visible">
          <xsl:value-of select="@visible"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="@disabled">
        <xsl:attribute name="Enabled">False</xsl:attribute>
      </xsl:if>
    </xsl:element>
  </div>
</xsl:template>

<xsl:template match="displaycolumn/control[@type='imagebuttonscript'] | displaycolumn/control/control[@type='imagebuttonscript']">
  <xsl:variable name="src">../../Images/</xsl:variable>
  <xsl:variable name="this">this.src='../../Images/</xsl:variable>
  <xsl:variable name="quote">'</xsl:variable>
  <script language="JavaScript">
    <xsl:attribute name="src">
      <xsl:value-of select="@includefilename"/>
    </xsl:attribute><![CDATA[{var i;}]]>
  </script>
  <div class="toolBarButton" runat="server">
    <xsl:attribute name="id">div_<xsl:value-of select="@name"/></xsl:attribute>
    <xsl:element name="asp:ImageButton">
      <xsl:attribute name="runat">server</xsl:attribute>
      <xsl:attribute name="src">
        <xsl:value-of select="concat($src,@activeimage)"/>
      </xsl:attribute>
      <xsl:attribute name="width">28</xsl:attribute>
      <xsl:attribute name="height">28</xsl:attribute>
      <xsl:attribute name="border">0</xsl:attribute>
      <xsl:attribute name="onMouseOver">
        <xsl:value-of select="concat($this,@moimage,$quote)"/>
      </xsl:attribute>
      <xsl:attribute name="onMouseOut">
        <xsl:value-of select="concat($this,@activeimage,$quote)"/>
      </xsl:attribute>
      <xsl:attribute name="id">
        <xsl:value-of select="@name"/>
      </xsl:attribute>
      <xsl:attribute name="RMXRef">
        <xsl:value-of select="@ref"/>
      </xsl:attribute>
      <xsl:choose>
        <xsl:when test="@buttontitle">
          <xsl:attribute name="Text">
            <xsl:value-of select="@buttontitle" />
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="Text">
            <xsl:value-of select="@title" />
          </xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:attribute name="ToolTip">
        <xsl:value-of select="@title | @buttontitle"/>
      </xsl:attribute>
      <xsl:attribute name="onClientClick">
        <xsl:value-of select="@functionname"/>
      </xsl:attribute>
      <xsl:if test="@onserverclick">
        <xsl:attribute name="onclick">
          <xsl:value-of select="@onserverclick"/>
        </xsl:attribute>
      </xsl:if>
    </xsl:element>
  </div>
</xsl:template>
  
<!--Ramkumar MITS 33606-->

<xsl:template match="error">
<li class="errtext">
	<xsl:if test="@number[.='10000']"><img src="../../Images/locked.gif" width="12" height="15" title="Security Message" border="0" />&amp;nbsp;&amp;nbsp;</xsl:if>
	<xsl:value-of select="text()"/>
</li>
</xsl:template>

<xsl:template match="dupes">
	<script language="JavaScript">setDataChanged(true);
	function DupeCallback(arg)
	{
		m_Wnd.close(); 
		if(arg==1){
			document.frmData.dupeoverride.value=1; 
			Navigate(5); 
		}
		return false; 
	}
	document.DupeCallback=DupeCallback;
	m_sDupes = new String("");
	m_sDupes=m_sDupes+('<html><head><title>Possible duplicate claim...</title><link rel="stylesheet" href="RMNet.css" type="text/css" />');
	m_sDupes=m_sDupes+('</head><body class="10pt">');
	m_sDupes=m_sDupes+('<div class="errtextheader">Claim was not saved, possible duplicate claim...</div>');
	m_sDupes=m_sDupes+('<table width="100%"><tr><td>');
	<xsl:choose>
	<xsl:when test="@formname[.='claimgc' or .='claimva']">
		m_sDupes=m_sDupes+('<table width="100%">');
		m_sDupes=m_sDupes+('<tr class="colheader">');
			m_sDupes=m_sDupes+('<td ><span class="required">Date of Event</span></td><td><input type="label" disabled=""><xsl:attribute name="value"><xsl:value-of select="@eventdate"/></xsl:attribute></input></td>');
			m_sDupes=m_sDupes+('<td ><span class="required">Claim Type</span></td><td><input type="label" disabled=""><xsl:attribute name="value"><xsl:value-of select="@claimtype"/></xsl:attribute></input></td>');
		m_sDupes=m_sDupes+('</tr>');
		m_sDupes=m_sDupes+('<tr class="colheader">');
			m_sDupes=m_sDupes+('<td ><span class="required">Date of Claim</span></td><td><input type="label" disabled=""><xsl:attribute name="value"><xsl:value-of select="@claimdate"/></xsl:attribute></input></td>');
			m_sDupes=m_sDupes+('<td ><span class="required">Department</span></td><td><input type="label" disabled=""><xsl:attribute name="value"><xsl:value-of select="@department"/></xsl:attribute></input></td>');
		m_sDupes=m_sDupes+('</tr>');
		m_sDupes=m_sDupes+('</table>');
		m_sDupes=m_sDupes+('<table width="100%"><tr class="ctrlgroup"><td class="colHeader">Event Number</td><td class="colHeader">Claim Number</td><td class="colHeader">Claim Status</td><td class="colHeader">Claimant</td><td class="colHeader">Event Description</td></tr>');
		<xsl:for-each select="claim">
				m_sDupes=m_sDupes+('<tr class="rowlight">');
				m_sDupes=m_sDupes+('<td><a class="blue"><xsl:attribute name="href">forms.asp?formname=event&amp;syscmd=0&amp;psid=3000&amp;sys_formidname=eventid&amp;eventid=<xsl:value-of select="@eventid"/></xsl:attribute>');
					m_sDupes=m_sDupes+('<xsl:value-of select="@eventnumber"/>');
					m_sDupes=m_sDupes+('</a>');
				m_sDupes=m_sDupes+('</td>');
				m_sDupes=m_sDupes+('<td><a class="blue"><xsl:attribute name="href">forms.asp?formname=<xsl:value-of select="//dupes/@formname"/>&amp;syscmd=0&amp;sys_formidname=claimid&amp;claimid=<xsl:value-of select="@claimid"/></xsl:attribute> ');
					m_sDupes=m_sDupes+('<xsl:value-of select="@claimnumber"/>');
					m_sDupes=m_sDupes+('</a>');
				m_sDupes=m_sDupes+('</td>');
				m_sDupes=m_sDupes+('<td><xsl:value-of select="@claimstatus"/></td>');
				m_sDupes=m_sDupes+('<td><xsl:value-of select="@claimant"/></td>');
				m_sDupes=m_sDupes+('<td><xsl:value-of select="@eventdescription"/></td>');
				m_sDupes=m_sDupes+('</tr>');
		</xsl:for-each>
		m_sDupes=m_sDupes+('</table><table><tr>');
		m_sDupes=m_sDupes+('<td><input type="button" class="button" value="Save (Ignore Duplication)" onClick="window.opener.document.DupeCallback(1); window.close(); return true;" /></td>');
		m_sDupes=m_sDupes+('<td><input type="button" class="button" value="Cancel" onClick="window.close();" /></td>');
		m_sDupes=m_sDupes+('</tr></table>');
	</xsl:when>
	<xsl:when test="@formname[.='claimwc']">
		m_sDupes=m_sDupes+('<table width="100%">');
		m_sDupes=m_sDupes+('<tr class="colheader">');
			m_sDupes=m_sDupes+('<td ><span class="required">Date of Event</span></td><td><input type="label" disabled=""><xsl:attribute name="value"><xsl:value-of select="@eventdate"/></xsl:attribute></input></td>');
			m_sDupes=m_sDupes+('<td ><span class="required">Department</span></td><td><input type="label" disabled=""><xsl:attribute name="value"><xsl:value-of select="@department"/></xsl:attribute></input></td>');
		m_sDupes=m_sDupes+('</tr>');
		m_sDupes=m_sDupes+('<tr class="colheader">');
			m_sDupes=m_sDupes+('<td ><span class="required">Jurisdiction</span></td><td><input type="label" disabled=""><xsl:attribute name="value"><xsl:value-of select="@juris"/></xsl:attribute></input></td>');
			m_sDupes=m_sDupes+('<td ><span class="required">Employee Name</span></td><td><input type="label" disabled=""><xsl:attribute name="value"><xsl:value-of select="@employee"/></xsl:attribute></input></td>');
		m_sDupes=m_sDupes+('</tr>');
		m_sDupes=m_sDupes+('<tr class="colheader">');
			m_sDupes=m_sDupes+('<td ><span class="required">Claim Type</span></td><td><input type="label" disabled=""><xsl:attribute name="value"><xsl:value-of select="@claimtype"/></xsl:attribute></input></td>');
			m_sDupes=m_sDupes+('<td ><span class="required">Employee SSN</span></td><td ><input type="label" disabled=""><xsl:attribute name="value"><xsl:value-of select="@taxid"/></xsl:attribute></input></td>');
		m_sDupes=m_sDupes+('</tr>');
		m_sDupes=m_sDupes+('</table>');
		m_sDupes=m_sDupes+('<table width="100%"><tr class="ctrlgroup"><td class="colHeader">Claim Date</td><td class="colHeader">Claim Number</td><td class="colHeader">Claim Status</td><td class="colHeader">Event Number</td><td class="colHeader">Event Description</td></tr>');
		<xsl:for-each select="claim">
				m_sDupes=m_sDupes+('<tr class="rowlight">');
				m_sDupes=m_sDupes+('<td><xsl:value-of select="@claimdate"/></td>');
				m_sDupes=m_sDupes+('<td><a class="blue"><xsl:attribute name="href">forms.asp?formname=<xsl:value-of select="//dupes/@formname"/>&amp;syscmd=0&amp;sys_formidname=claimid&amp;claimid=<xsl:value-of select="@claimid"/></xsl:attribute> ');
					m_sDupes=m_sDupes+('<xsl:value-of select="@claimnumber"/>');
					m_sDupes=m_sDupes+('</a>');
				m_sDupes=m_sDupes+('</td>');
				m_sDupes=m_sDupes+('<td><xsl:value-of select="@claimstatus"/></td>');
				m_sDupes=m_sDupes+('<td><a class="blue"><xsl:attribute name="href">forms.asp?formname=event&amp;syscmd=0&amp;psid=3000&amp;sys_formidname=eventid&amp;eventid=<xsl:value-of select="@eventid"/></xsl:attribute>');
					m_sDupes=m_sDupes+('<xsl:value-of select="@eventnumber"/>');
					m_sDupes=m_sDupes+('</a>');
				m_sDupes=m_sDupes+('</td>');
				m_sDupes=m_sDupes+('<td><xsl:value-of select="@eventdescription"/></td>');
				m_sDupes=m_sDupes+('</tr>');
		</xsl:for-each>
		m_sDupes=m_sDupes+('</table><table><tr>');
		m_sDupes=m_sDupes+('<td><input type="button" class="button" value="Save (Ignore Duplication)" onClick="window.opener.document.DupeCallback(1); window.close(); return true;" /></td>');
		m_sDupes=m_sDupes+('<td><input type="button" class="button" value="Cancel" onClick="window.close();" /></td>');
		m_sDupes=m_sDupes+('</tr></table>');
	</xsl:when>
	</xsl:choose>
	m_sDupes=m_sDupes+('</td></tr></table></body></html>');
	</script>
</xsl:template>
<xsl:template match="option"><xsl:copy-of select="."/></xsl:template>

  <xsl:template name="lastIndexOf">
    <!-- declare that it takes two parameters - the string and the char -->
    <xsl:param name="string" />
    <xsl:param name="char" />
    <xsl:variable name="origvalue" select="$string"/>
    <xsl:choose>
      <xsl:when test="contains($string, $char)">
        <xsl:call-template name="lastIndexOf">
          <xsl:with-param name="string"
                          select="substring-after($string, $char)" />
          <xsl:with-param name="char" select="$char" />
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="string-length($string)" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
