<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:asp="remove"  xmlns:cc1="remove" xmlns:uc="remove"  xmlns:dg="remove" xmlns:rmxforms="http://www.riskmaster.com/wizard" version="1.0">
  <xsl:output method="xml" encoding="ISO-8859-1" omit-xml-declaration="yes"/>
  <xsl:include href="controls.xsl"/>
<xsl:template match="/">

<html>
  <head runat="server"><title><xsl:apply-templates select="form"/></title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <script language="JavaScript"><xsl:attribute name="src">../../Scripts/form.js</xsl:attribute><![CDATA[{var i;}]]>
</script>
	<script language="JavaScript"><xsl:attribute name="src">../../Scripts/drift.js</xsl:attribute><![CDATA[{var i;}]]>
</script>
  <script language="JavaScript">
    <xsl:attribute name="src">../../Scripts/WaitDialog.js</xsl:attribute><![CDATA[{var i;}]]>
  </script>
  
	<script type="text/javascript">
		<xsl:attribute name="src">../../Scripts/zapatec/utils/zapatec.js</xsl:attribute><![CDATA[{var i;}]]>
</script>
	<script type="text/javascript">
		<xsl:attribute name="src">../../Scripts/zapatec/zpgrid/src/zpgrid.js</xsl:attribute><![CDATA[{var i;}]]>
	</script>
	<script type="text/javascript">
		<xsl:attribute name="src">../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js</xsl:attribute><![CDATA[{var i;}]]>
	</script>
	<script type="text/javascript">
		<xsl:attribute name="src">../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js</xsl:attribute><![CDATA[{var i;}]]>
	</script>
    <script type="text/javascript">
      <xsl:attribute name="src">../../Scripts/dhtml-div.js</xsl:attribute><![CDATA[{var i;}]]>
    </script>
    <script type="text/javascript">
      <xsl:attribute name="src">../../Scripts/dhtml-help-setup.js</xsl:attribute><![CDATA[{var i;}]]>
    </script>
	<script type="text/javascript">
		<xsl:attribute name="src">../../Scripts/zapatec/zpgrid/src/zpgrid-query.js</xsl:attribute><![CDATA[{var i;}]]>
	</script>
    <!--Deb ML-->
    <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script language="javascript" type="text/javascript">
      <xsl:attribute name="src">../../Scripts/jquery/jquery-1.8.0.js</xsl:attribute><![CDATA[{var i;}]]>
    </script>
    <script language="javascript" type="text/javascript">
      <xsl:attribute name="src">../../Scripts/jquery/ui/jquery.ui.core.js</xsl:attribute><![CDATA[{var i;}]]>
    </script>
    <script language="javascript" type="text/javascript">
      <xsl:attribute name="src">../../Scripts/jquery/ui/jquery.ui.datepicker.js</xsl:attribute><![CDATA[{var i;}]]>
    </script>
    <!--Deb ML-->

    <!--Add by kuladeep for rmA14.1 performance Start-->
  
    <script language="javascript" type="text/javascript">
      <xsl:attribute name="src">../../Scripts/jquery/json2.js</xsl:attribute><![CDATA[{var i;}]]>
    </script>
    <script language="javascript" type="text/javascript">
      <xsl:attribute name="src">../../Scripts/jquery/ui/minified/jquery-ui.min.js</xsl:attribute><![CDATA[{var i;}]]>
    </script>
    <link rel="stylesheet" href="../../Scripts/jquery/themes/base/jquery-ui.div.css"/>
    <!--Add by kuladeep for rmA14.1 performance End-->
	<xsl:if test="form/@includefilename"><script language="JavaScript"><xsl:attribute name="SRC"><xsl:value-of select="form/@includefilename"/></xsl:attribute><![CDATA[{var i;}]]></script></xsl:if>
  <xsl:if test="form/@includefilenamegrid"><script language="JavaScript"><xsl:attribute name="src"><xsl:value-of select="form/@includefilenamegrid"/></xsl:attribute><![CDATA[{var i;}]]></script>
    </xsl:if>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
  </head>
	 <body class="10pt">
		<xsl:attribute name="onload"><xsl:choose><xsl:when test="form/@onload"><xsl:value-of select="form/@onload"/></xsl:when><xsl:otherwise>pageLoaded();</xsl:otherwise></xsl:choose></xsl:attribute>
     <form name="frmData" id ="frmData" runat="server">
		<!--<xsl:attribute name="action"><xsl:choose><xsl:when test="form/@postpage"><xsl:value-of select="form/@postpage"/></xsl:when><xsl:otherwise>forms.asp</xsl:otherwise></xsl:choose></xsl:attribute>-->
       <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red"></asp:Label>
       <asp:HiddenField runat="server" ID="wsrp_rewrite_action_1" value=""/>
      <asp:TextBox style="display:none" runat="server" name="hTabName" id="hTabName"/>
       <asp:ScriptManager ID="SMgr" runat="server"></asp:ScriptManager>
      
		<xsl:if test="form/toolbar">
			<div id="toolbardrift" name="toolbardrift" class="toolbardrift" runat="server">
				<xsl:apply-templates select="form/toolbar/button" />
			</div>
	</xsl:if>
      <xsl:if test="form/@topbuttons[.='1']">
         <xsl:if test="form/button | form/buttonscript">
           <div class="formButtonGroup" runat="server">
           <xsl:for-each select="form/button">
             <div class="formButtonTopDown" style="padding-top:0px; padding-bottom:0px" runat="server">
               <xsl:attribute name="id">div_<xsl:value-of select="@name"/>_Top</xsl:attribute>
               <asp:Button class="button" runat="Server">
                 <xsl:choose>
                   <xsl:when test="@type='back'">
                     <xsl:attribute name="id">BackToParent</xsl:attribute>
                   </xsl:when>
                   <xsl:otherwise>
                     <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
                   </xsl:otherwise>
                 </xsl:choose>
                 <xsl:attribute name="Text"><xsl:value-of select="@title"/></xsl:attribute>
                 <xsl:if test="@countref">
                   <xsl:attribute name="RMXRef"><xsl:value-of select="@countref"/></xsl:attribute>
                 </xsl:if>
                 <!-- pmittal5 Mits 16284 05/06/09 - Added value from "onclick" attribute-->
                 <xsl:attribute name="OnClientClick"><xsl:if test="@onclick"><xsl:value-of select="@onclick"/></xsl:if>if(!( XFormHandler('<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;</xsl:attribute>
                 <xsl:attribute name="PostBackUrl">
                   <xsl:value-of select="@pagetomove"/>?<xsl:value-of select="@param"/>
                 </xsl:attribute>
                 <xsl:if test="@onServerClick">
                   <xsl:attribute name="OnClick">
                     <xsl:value-of select="@onServerClick"/>
                   </xsl:attribute>
                 </xsl:if>
                 <xsl:if test='@visible'>
                   <xsl:attribute name="visible"><xsl:value-of select="@visible"/></xsl:attribute>
                 </xsl:if>
               </asp:Button>
             </div>
           </xsl:for-each>
           <xsl:for-each select="form/buttonscript">
             <div class="formButton" style="padding-top:0px; padding-bottom:0px" runat="server">
               <xsl:attribute name="id">div_<xsl:value-of select="@name"/>_Top</xsl:attribute>
               <script language="JavaScript">
                 <xsl:attribute name="src">
                   <xsl:value-of select="@includefilename"/>
                 </xsl:attribute><![CDATA[{var i;}]]>
               </script>
               <asp:button class="button" runat="server">
                 <xsl:attribute name="id">
                   <xsl:value-of select="@name"/>
                 </xsl:attribute>
                 <xsl:attribute name="RMXRef">
                   <xsl:value-of select="@ref"/>
                 </xsl:attribute>
                 <xsl:choose>
                   <xsl:when test="@buttontitle">
                     <xsl:attribute name="Text">
                       <xsl:value-of select="@buttontitle" />
                     </xsl:attribute>
                   </xsl:when>
                   <xsl:otherwise>
                     <xsl:attribute name="Text">
                       <xsl:value-of select="@title" />
                     </xsl:attribute>
                   </xsl:otherwise>
                 </xsl:choose>
                 <xsl:if test='@width'>
                   <xsl:attribute name="width">
                     <xsl:value-of select="@width"/>
                   </xsl:attribute>
                 </xsl:if>
                 <xsl:attribute name="onClientClick">
                   <xsl:value-of select="@functionname"/>
                 </xsl:attribute>
                 <xsl:if test="@onserverclick">
                   <xsl:attribute name="onclick">
                     <xsl:value-of select="@onserverclick"/>
                   </xsl:attribute>
                 </xsl:if>
                  <xsl:if test='@visible'>
                <xsl:attribute name="visible"><xsl:value-of select="@visible"/></xsl:attribute>
              </xsl:if>
               </asp:button>
             </div>
           </xsl:for-each>
         </div>
        </xsl:if>
       </xsl:if>
       <br />
        <div class="msgheader" id="div_formtitle" runat="server">
         <asp:label id="formtitle" runat="server">
           <xsl:attribute name="Text"><xsl:value-of select="form/@title"/></xsl:attribute>
         </asp:label>
         <asp:label id="formsubtitle" runat="server">
           <xsl:attribute name="Text"><xsl:value-of select="form/@subtitle"/></xsl:attribute>
         </asp:label>
       </div>
        <div class="errtextheader" runat="server">
         <asp:label id="formdemotitle" runat="server"><xsl:attribute name="Text"><xsl:value-of select="form/demo/@title"/></xsl:attribute>
         </asp:label>
       </div>
		<xsl:if test="form/errors">
			<br />
			<div class="errtextheader">Following errors were reported:</div>
			<ul>
				<xsl:apply-templates select="form/errors/error" />	
			</ul>
		</xsl:if>
		<xsl:if test="form/alerts">
			<br />
			<div class="errtextheader">Following alerts were reported:</div>
			<ul>
				<xsl:apply-templates select="form/alerts/error" />	
			</ul>
		</xsl:if>
		<xsl:if test="form/dupes">
			<br />
			<xsl:apply-templates select="form/dupes" />	
		</xsl:if>
       <xsl:if test="contains(//form/@name , 'admintracking')">
         <xsl:apply-templates select="form/control" />
       </xsl:if><BR/>
		<xsl:for-each select="form/group">
			<div class="msgheader" runat="server" width="99%">
			<xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:value-of select="@title"/>
			</div>
      <!--Ramkumar MITS 33606-->
      <xsl:if test="displaycolumn/control[@type='imagebutton' or @type='imagebuttonscript'] | displaycolumn/control/control[@type='imagebutton' or @type='imagebuttonscript']">
        <div class="toolbardrift" name="toolbardrift" runat="server">
          <xsl:attribute name="id">TBD<xsl:value-of select="@name"/></xsl:attribute>
          <xsl:apply-templates select="displaycolumn/control[@type='imagebutton' or @type='imagebuttonscript'] | displaycolumn/control/control[@type='imagebutton' or @type='imagebuttonscript']" />
        </div>
        <BR/>
	    </xsl:if>
      <!--Ramkumar MITS 33606-->
      <xsl:choose>
        <xsl:when test="@name[.!= 'pvjurisgroup']">
          <table width="99%">
            <xsl:for-each select=".//displaycolumn/control[not(@type='hidden')and not(@type='id')and not(@type='imagebutton')  and not(@type='imagebuttonscript')]"><!--Ramkumar MITS 33606-->
              <!--<xsl:for-each select=".//displaycolumn/control">-->
              <!--nadim MITS 11369 for topdown layout sorting-->
              <!--<xsl:sort data-type="number" select="@tabindex" />-->
              <!--nadim MITS 11369 for topdown layout sorting-->
              <!--Code updated by pawan to byppass for spaces implementation for 11210 on topdownviews -->
              <xsl:if test="@type[.!='space']">
                <tr>
                  <td>
                    <xsl:apply-templates select="."/>
                  </td>
                </tr>
              </xsl:if>
              <!--Code updated by pawan to spaces implementation for 11210 on topdownviews -->
            </xsl:for-each>
          </table>
        </xsl:when>

        <xsl:otherwise>
          <div runat ="server" width="99%" id="Tabpvjurisgroup"></div>
          <table width="99%"></table>
        </xsl:otherwise>
      </xsl:choose>
    	</xsl:for-each>
       <div class="formButtonGroup" runat="server">
         <xsl:for-each select="form/button">
           <div class="formButtonTopDown" runat="server">
             <xsl:attribute name="id">div_<xsl:value-of select="@name"/>_Bottom</xsl:attribute>
             <asp:Button class="button" runat="Server">
               <xsl:choose>
                 <xsl:when test="@type='back'">
                   <xsl:attribute name="id">BackToParent_Bottom</xsl:attribute>
                 </xsl:when>
                 <xsl:otherwise>
                   <xsl:attribute name="id"><xsl:value-of select="@name"/>_Bottom</xsl:attribute>
                 </xsl:otherwise>
               </xsl:choose>
               <xsl:attribute name="Text">
                 <xsl:value-of select="@title"/>
               </xsl:attribute>
	       <xsl:if test='@visible'><xsl:attribute name="visible"><xsl:value-of select="@visible"/></xsl:attribute></xsl:if>
          
               <xsl:if test="@countref">
                 <xsl:attribute name="RMXRef">
                   <xsl:value-of select="@countref"/>
                 </xsl:attribute>
               </xsl:if>
               <xsl:attribute name="OnClientClick">
                 if(!( XFormHandler('<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')))return false;
               </xsl:attribute>
               <xsl:attribute name="PostBackUrl">
                 <xsl:value-of select="@pagetomove"/>?<xsl:value-of select="@param"/>
               </xsl:attribute>
               <xsl:if test="@onServerClick">
                 <xsl:attribute name="OnClick">
                   <xsl:value-of select="@onServerClick"/>
                 </xsl:attribute>
               </xsl:if>
             </asp:Button>
           </div>
         </xsl:for-each>
         <xsl:for-each select="form/buttonscript">
           <div class="formButton" runat="server">
             <xsl:attribute name="id">div_<xsl:value-of select="@name"/>_Bottom</xsl:attribute>
             <script language="JavaScript">
               <xsl:attribute name="src">
                 <xsl:value-of select="@includefilename"/>
               </xsl:attribute><![CDATA[{var i;}]]>
             </script>
             <asp:button class="button" runat="server">
               <xsl:attribute name="id"><xsl:value-of select="@name"/>_Bottom</xsl:attribute>
               <xsl:attribute name="RMXRef">
                 <xsl:value-of select="@ref"/>
               </xsl:attribute>
               <xsl:choose>
                 <xsl:when test="@buttontitle">
                   <xsl:attribute name="Text">
                     <xsl:value-of select="@buttontitle" />
                   </xsl:attribute>
                 </xsl:when>
                 <xsl:otherwise>
                   <xsl:attribute name="Text">
                     <xsl:value-of select="@title" />
                   </xsl:attribute>
                 </xsl:otherwise>
               </xsl:choose>
               <xsl:if test='@width'>
                 <xsl:attribute name="width">
                   <xsl:value-of select="@width"/>
                 </xsl:attribute>
               </xsl:if>
	        <xsl:if test='@visible'>
                 <xsl:attribute name="visible"><xsl:value-of select="@visible"/></xsl:attribute>
               </xsl:if>
               <xsl:attribute name="onClientClick">
                 <xsl:value-of select="@functionname"/>
               </xsl:attribute>
               <xsl:if test="@onserverclick">
                 <xsl:attribute name="onclick">
                   <xsl:value-of select="@onserverclick"/>
                 </xsl:attribute>
               </xsl:if>
             </asp:button>
           </div>
         </xsl:for-each>
       </div>
       <xsl:apply-templates select="//internal"/>
       <xsl:if test="not(contains(//form/@name , 'admintracking'))">
         <xsl:apply-templates select="form/control" />
       </xsl:if>
       <asp:TextBox style="display:none" runat="server" name="formname">
         <xsl:attribute name="Text">
           <xsl:for-each select="form">
             <xsl:value-of select="@name"/>
           </xsl:for-each>
         </xsl:attribute>
       </asp:TextBox>
       <asp:TextBox style="display:none" runat="server" name="SysRequired" id="SysRequired">
         <xsl:attribute name="Text">
           <xsl:for-each select=".//control">
             <xsl:if test="@required[.='yes']">
               <xsl:value-of select="@name"/><xsl:if test="@type[.='code' or .='codewithdetail']">_codelookup_cid</xsl:if><xsl:if test="@type[.='codelist']">_lst</xsl:if><xsl:if test="@type[.='entitylist']">_lst</xsl:if><xsl:if test="@type[.='orglist']">_lst</xsl:if><xsl:if test="@type[.='orgh']">_cid</xsl:if>|
             </xsl:if>
           </xsl:for-each>
         </xsl:attribute>
       </asp:TextBox>
       <asp:TextBox style="display:none" runat="server" name="SysBindingRequiredFields" id="SysBindingRequiredFields">
			<xsl:attribute name="Text">
				<xsl:for-each select=".//control">
					<xsl:if test="@bindingrequired[.='true']"><xsl:value-of select="@name"/>|</xsl:if>
				</xsl:for-each>
			</xsl:attribute>
		</asp:TextBox>
       <asp:TextBox style="display:none" runat="server" name="SysFocusFields">
         <xsl:attribute name="Text">
           <xsl:for-each select=".//control">
             <xsl:if test="@firstfield[.='1']">
               <xsl:value-of select="@name"/>|
             </xsl:if>
           </xsl:for-each>
         </xsl:attribute>
       </asp:TextBox>
       <!--Added Rakhi for 16453-->
	  <asp:TextBox style="display:none" runat="server" name="SysReadonlyFields" id="SysReadonlyFields">
		<xsl:attribute name="Text">
			<xsl:for-each select=".//control">
				<xsl:if test="@type[.='readonly']"><xsl:value-of select="@name"/>|</xsl:if>
			</xsl:for-each>
		</xsl:attribute>
	</asp:TextBox>
    <!--Added Rakhi for 16453-->
       <input type="hidden" id="hdSaveButtonClicked" />
       <!--asp:TextBox runat="server" id="SysInvisible" style="display:none" />
      <asp:TextBox runat="server" id="SysLookupClass" style="display:none"/>
      <asp:TextBox runat="server" id="SysLookupRecordId" style="display:none"/>
      <asp:TextBox runat="server" id="SysLookupAttachNodePath" style="display:none"/>
      <asp:TextBox runat="server" id="SysLookupResultConfig" style="display:none"/-->
       <asp:TextBox runat="server" id="txtScreenFlowStack" style="display:none"/>
       <input type="hidden" value="rmx-widget-handle-2" id="SysWindowId"/>
       <asp:TextBox runat="server" id="SysPageDataChanged" style="display:none"/>
       <asp:TextBox style="display:none" runat="server" name="SysIsServiceError" id="SysIsServiceError">
       </asp:TextBox>
       <asp:TextBox style="display:none" runat="server" name="SysIsFormSubmitted" id="SysIsFormSubmitted">
       </asp:TextBox>
      <xsl:for-each select="form/group">
        <xsl:for-each select=".//displaycolumn/control[@type='id'or type='hidden']">
          <!--<xsl:apply-templates select="displaycolumn/control[@type='id'or @type='hidden']"/>-->
      <xsl:if test="@type[.!='space']">
       
          <xsl:apply-templates select="."/>
        
      </xsl:if>  
      </xsl:for-each>
      </xsl:for-each>
	<!--</td>-->
	
	<!--<td valign="top">-->
    <xsl:if test="form/@goto[.='1']">
      <script language="JavaScript" src="goto.js"></script>
      <strong>Record Id: </strong>
      <asp:TextBox runat="server" size="2" name="txtgoto" OnKeyDown="txtgotoOnKeyDown();" onfocus="txtGotFocus(this);">
        <xsl:if test="form/@recordid[.!='0']">
          <xsl:attribute name="Text"><xsl:value-of select="form/@recordid"/></xsl:attribute>
        </xsl:if>
      </asp:TextBox>
      <input type="button" class="button" name="btnGoTo" value=" Go ">
        <xsl:attribute name="onClick">GoTo();</xsl:attribute>
      </input>
    </xsl:if>
       <!--<xsl:choose>-->
       <!--<xsl:when test="form/@formtype[.='GridPopup']">-->
       <!--Nitin MITS 15272 The Pleasewaitdialog gives error for Popups. There are some pages which are called as popup from diary list.
        So this change will fix all the issues.-->
       <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server"  CustomMessage="Loading"/>
       <!--</xsl:when>
        <xsl:otherwise>
          <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server"  CustomMessage=""/>
        </xsl:otherwise>-->
       <!--</xsl:choose>-->
		<xsl:if test="/form/@image"><img border="0" align="right"><xsl:attribute name="src">../../Images/<xsl:value-of select="/form/@image"/></xsl:attribute></img></xsl:if>
	<!--</td>
	
	</tr></table>--></form>
	</body>
</html>
</xsl:template>

<xsl:template match="form/button">
	<!--<input type="button" class="button"><xsl:attribute name="value"><xsl:value-of select="@title"/></xsl:attribute>
	    <xsl:attribute name="onClick">formHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')</xsl:attribute>
	</input>-->
  
  <!--<asp:Button class="button" runat="Server">
    <xsl:attribute name="Text">
      <xsl:value-of select="@title"/>
    </xsl:attribute>
    <xsl:attribute name="OnClientClick">
      XFormHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')
    </xsl:attribute>
    <xsl:if test="@onServerClick">
      <xsl:attribute name="OnClick">
        <xsl:value-of select="@onServerClick"/>
      </xsl:attribute>
    </xsl:if>
  </asp:Button>-->
</xsl:template>

<xsl:template match="option"><xsl:copy-of select="."/></xsl:template>

</xsl:stylesheet>