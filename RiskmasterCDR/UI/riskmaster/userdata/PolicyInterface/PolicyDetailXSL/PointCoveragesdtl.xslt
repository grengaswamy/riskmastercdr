﻿<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions">
  <xsl:template match="/">
    <FONT face="Ariel" size="2">
      <xsl:choose>
        <xsl:when test="normalize-space(//com.csc_IssueCd) = 'M'">
          <TABLE cellspacing="4" cellpadding="4" width="100%" >
            <TR bgcolor="#6fb3e7">
              <TD colspan="2">
                <B>Policy Coverage Detail</B>
              </TD>
            </TR>
            <TR >


              <TD >
                <B>
                  <u> Coverage Effective Date:   </u>
                </B>
                <xsl:value-of select="//com.csc_CoverageLossInfo/Coverage/EffectiveDt"/>
                <xsl:value-of select="'   '"/>

              </TD>
              <TD >
                <B>
                  <u> Coverage Expiry Date:   </u>
                </B>
                <xsl:value-of select="//com.csc_CoverageLossInfo/Coverage/ExpirationDt"/>


              </TD>
            </TR>
            <tr>


              <TD >
                <B>
                  <u> Transaction Date:   </u>
                </B>
                <xsl:value-of select="//com.csc_TrxnDt"/>

              </TD>
              <td>
                <B>
                  <u> Entry Date:   </u>
                </B>
                <xsl:value-of select="//com.csc_ManualEntryInd"/>


              </td>
            </tr>
            <tr>

              <TD >
                <B>
                  <u>Retro Date :   </u>
                </B>
                <xsl:value-of select="//com.csc_RetroDt"/>



              </TD>
              <TD >
                <B>
                  <u> Extend Date :   </u>
                </B>
                <xsl:value-of select="//com.csc_ExtendDt"/>



              </TD>

            </tr>
            <tr>
              <TD >
                <B>
                  <u> Accounting Date :   </u>
                </B>
                <xsl:value-of select="//  com.csc_AcctDt"/>



              </TD>

              <TD >
                <B>
                  <u> Product Line :   </u>
                </B>
                <xsl:value-of select="//com.csc_ProductLine"/>



              </TD>


            </tr>
            <tr>
              <TD >
                <B>
                  <u> Audit Frequency :   </u>
                </B>
                <xsl:value-of select="//com.csc_AuditFrequency"/>



              </TD>
              <TD >
                <B>
                  <u> Annual Statement :   </u>
                </B>
                <xsl:value-of select="//com.csc_AnnualStatement"/>



              </TD>

            </tr>
            <tr>
              <TD >
                <B>
                  <u> Audit Frequency :   </u>
                </B>
                <xsl:value-of select="//com.csc_AuditFrequency"/>



              </TD>
              <TD >
                <B>
                  <u> State :   </u>
                </B>
                <xsl:value-of select="//com.csc_CoverageLossInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RateState']/OtherId"/>



              </TD>

            </tr>
            <tr>
              <TD >
                <B>
                  <u> Audit Frequency :   </u>
                </B>
                <xsl:value-of select="//com.csc_AuditFrequency"/>



              </TD>
              <TD >
                <B>
                  <u> State :   </u>
                </B>
                <xsl:value-of select="//com.csc_CoverageLossInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RateState']/OtherId"/>



              </TD>

            </tr>
            <tr>
              <TD >
                <B>
                  <u> Territory :   </u>
                </B>
                <xsl:value-of select="//com.csc_CoverageLossInfo/Coverage/TerritoryCd"/>



              </TD>
              <TD >
                <B>
                  <u> Tax Location :   </u>
                </B>
                <xsl:value-of select="//com.csc_TaxLoc"/>



              </TD>

            </tr>
            <tr>
              <TD >
                <B>
                  <u> SubLine :   </u>
                </B>
                <xsl:value-of select="//com.csc_Subline"/>



              </TD>
              <TD >
                <B>
                  <u> Class :   </u>
                </B>
                <xsl:value-of select="//com.csc_ClassCd"/>

                <xsl:value-of select="'  '"/>
                <xsl:value-of select="//com.csc_ClassDesc"/>


              </TD>

            </tr>
            <tr>
              <TD >
                <B>
                  <u> Coverage :   </u>
                </B>
                <xsl:value-of select="//Coverage/CoverageCd"/>
                <xsl:value-of select="'  '"/>
                <xsl:value-of select="//CoverageDesc"/>


              </TD>
              <TD >
                <B>
                  <u> Commission :   </u>
                </B>
                <xsl:value-of select="//com.csc_Commission"/>


              </TD>

            </tr>
            <tr>
              <TD >
                <B>
                  <u> Exposure :   </u>
                </B>
                <xsl:value-of select="//com.csc_Exposure"/>

              </TD>
              <TD >
                <B>
                  <u> Occurence Limit :   </u>
                </B>
                <xsl:value-of select="//com.csc_CoverageLossInfo/Limit[LimitAppliesToCd='OccurenceLimit']/Amt"/>



              </TD>

            </tr>
            <tr>
              <TD >
                <B>
                  <u>Aggregate Limit :   </u>
                </B>
                <xsl:value-of select="//com.csc_CoverageLossInfo/Limit[LimitAppliesToCd='AggregateLimit']/Amt"/>

              </TD>
              <TD >
                <B>
                  <u> Deductible:   </u>
                </B>
                <xsl:value-of select="//Deductible/FormatCurrencyAmt/Amt"/>



              </TD>

            </tr>
            <tr>
              <TD >
                <B>
                  <u>Full Term Premium:   </u>
                </B>
                <xsl:value-of select="//CurrentTermAmt"/>

              </TD>
              <TD >
                <B>
                  <u> Original Premium:   </u>
                </B>
                <xsl:value-of select="//com.csc_OriginalPremium"/>



              </TD>

            </tr>
            <tr>
              <TD >
                <B>
                  <u>Written Premium:   </u>
                </B>
                <xsl:value-of select="//com.csc_WrittenPremium"/>

              </TD>
              <TD >
                <B>
                  <u>Total Premium:   </u>
                </B>
                <xsl:value-of select="//com.csc_TotalPremium"/>



              </TD>

            </tr>
            <tr>
              <td>
                <B>
                  <U> Annual Statement :  </U>
                </B>
                <xsl:value-of select="//com.csc_AnnualStatement"/>
              </td>
              <td>
                <B>
                  <U> Product Line : </U>
                </B>
                <xsl:value-of select="//com.csc_ProductLine"/>
              </td>
            </tr>
            <!--Ankit Start : Worked on MITS - 34333-->
            <TR>
              <TD>
                <B>
                  <u> Sub Line : </u>
                </B>
                <xsl:value-of select="//com.csc_Subline"/>
                <xsl:value-of select="'   '"/>
                <xsl:value-of select="//com.csc_SublineDesc"/>
              </TD>
	<!--Nitin Goel Start : Worked on MITS - Class Code for GC, MITS 36753,07/14/2014-->
	       <TD >
                  <B>
                    <u> Class Code:   </u>
                  </B>
                  <xsl:value-of select="//com.csc_ClassCd"/>
                  <xsl:value-of select="'   '"/>
                  <xsl:value-of select="//com.csc_ClassDesc"/>


                </TD>
		<!--Nitin End-->
            </TR>
            <!--Ankit End-->
          </TABLE>


        </xsl:when>
        <xsl:otherwise>
          <xsl:if test="//Policy/com.csc_BaseLOBLine ='WL'">
            <TABLE cellspacing="4" cellpadding="4"  width="100%">
              <TR bgcolor="#6fb3e7">
                <TD colspan="2">
                  <B>Policy Coverage Detail</B>
                </TD>
              </TR>
              <tr>
                <td>

                  <B>
                    <u>  Policy Status :  </u>
                  </B>
                  <xsl:value-of select="//com.csc_CoverageLossInfo/com.csc_RecordStatus"/>

                </td>

                <TD >
                  <B>
                    <u> Site Number/ Name:   </u>
                  </B>
                  <xsl:value-of select="//com.csc_CoverageLossInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNum']/OtherId"/>
                  <xsl:value-of select="'   '"/>
                  <xsl:value-of select="//com.csc_CoverageLossInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_SiteName']/OtherId"/>
                </TD>
              </tr>
              <TR >


                <TD >
                  <B>
                    <u> Class Code:   </u>
                  </B>
                  <xsl:value-of select="//com.csc_ClassCd"/>
                  <xsl:value-of select="'   '"/>
                  <xsl:value-of select="//com.csc_ClassDesc"/>


                </TD>
                <TD >
                  <B>
                    <u> Class Sequnece:   </u>
                  </B>
                  <xsl:value-of select="//com.csc_CoverageLossInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_ClassSeq']/OtherId"/>


                </TD>
              </TR>
              <tr>


                <TD >
                  <B>
                    <u> Estimated Payroll:   </u>
                  </B>
                  <xsl:value-of select="//com.csc_CoverageLossInfo/Coverage/com.csc_EstimatedPayroll"/>

                </TD>
                <td>
                  <B>
                    <u> Manual Premium:   </u>
                  </B>
                  <xsl:value-of select="//com.csc_CoverageLossInfo/Coverage/CurrentTermAmt"/>


                </td>
              </tr>
              <tr>

                <TD >
                  <B>
                    <u> RateOverride :   </u>
                  </B>
                  <xsl:value-of select="//com.csc_CoverageLossInfo/com.csc_RateOverideInd"/>



                </TD>
                <TD >
                  <B>
                    <u> Split Rate Override :   </u>
                  </B>
                  <xsl:value-of select="//com.csc_CoverageLossInfo/com.csc_SplitRateOverideInd"/>



                </TD>

              </tr>
              <tr>

                <TD >
                  <B>
                    <u> Rate :   </u>
                  </B>
                  <xsl:value-of select="//com.csc_CoverageLossInfo/com.csc_Rate"/>



                </TD>
                <TD >
                  <B>
                    <u> Split Rate :   </u>
                  </B>
                  <xsl:value-of select="//com.csc_CoverageLossInfo/com.csc_SplitRate"/>



                </TD>

              </tr>
              <tr>

                <TD >
                  <B>
                    <u> Coverage Code :   </u>
                  </B>
                  <xsl:value-of select="//CoverageCd"/>
                  <xsl:value-of select="'   '"/>
                  <xsl:value-of select="//CoverageDesc"/>



                </TD>

              </tr>
              <tr>
                <td >
                  <B>
                    <u> WC Deductible Amount :   </u>
                  </B>
                  <xsl:value-of select="//com.csc_CoverageLossInfo/Coverage/ExcessWorkCompDeductible/FormatCurrencyAmt/Amt"/>
                </td>
                <td >
                  <B>
                    <u> WC Deductible Aggregate :   </u>
                  </B>
                  <xsl:value-of select="//com.csc_CoverageLossInfo/Coverage/ExcessWorkCompDeductible/com.csc_AggrAmt"/>
                </td>
              </tr>
              <tr>
                <td>
                  <B>
                    <U> Annual Statement :</U> 
                  </B>
                  <xsl:value-of select="//com.csc_AnnualStatement"/>
                </td>
                <td>
                  <B>
                    <U> Product Line :</U> 
                  </B>
                  <xsl:value-of select="//com.csc_ProductLine"/>
                </td>
              </tr>
            </TABLE>
          </xsl:if>
       
          <xsl:if test="//com.csc_BaseLOBLine ='AL'">
            <TABLE cellspacing="4" cellpadding="4"  width="100%">
              <TR bgcolor="#6fb3e7">
                <TD colspan="2">
                  <B>Policy Coverage Detail</B>
                </TD>
              </TR>
              <tr>
                <td>

                  <B>
                    <u>  Policy Status :  </u>
                  </B>
                  <xsl:value-of select="//com.csc_RecordStatus"/>

                </td>

                <TD >
                  <B>
                    <u> Unit:   </u>
                  </B>
                  <xsl:value-of select="//com.csc_CoverageLossInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNum']/OtherId"/>

                </TD>
              </tr>
              <TR >


                <TD >
                  <B>
                    <u> Coverage:   </u>
                  </B>
                  <xsl:value-of select="//com.csc_CoverageLossInfo/Coverage/CoverageCd"/>
                  <xsl:value-of select="'   '"/>
                  <xsl:value-of select="//com.csc_CoverageLossInfo/Coverage/CoverageDesc"/>


                </TD>
                <TD >
                  <B>
                    <u> Limit:   </u>
                  </B>
                  <xsl:value-of select="//Coverage/Limit/FormatCurrencyAmt/Amt"/>


                </TD>
              </TR>
              <tr>


                <TD >
                  <B>
                    <u> Deductible:   </u>
                  </B>
                  <xsl:value-of select="//Coverage/Deductible/FormatCurrencyAmt/Amt"/>

                </TD>
                <td>
                  <B>
                    <u> Exposure:   </u>
                  </B>
                  <xsl:value-of select="//Coverage/com.csc_Exposure"/>


                </td>
              </tr>
              <tr>

                <TD >
                  <B>
                    <u> RateBook :   </u>
                  </B>
                  <xsl:value-of select="//com.csc_RateBook"/>



                </TD>
                <TD >
                  <B>
                    <u> Premium :   </u>
                  </B>
                  <xsl:value-of select="//CurrentTermAmt"/>
                </TD>

              </tr>
              <tr>
                <td>
                  <B>
                    <U> Annual Statement :  </U>
                  </B>
                  <xsl:value-of select="//com.csc_AnnualStatement"/>
                </td>
                <td>
                  <B>
                    <U> Product Line : </U>
                  </B>
                  <xsl:value-of select="//com.csc_ProductLine"/>
                </td>
              </tr>
              <!--Ankit Start : Worked on MITS - 34333-->
              <TR>
                <TD>
                  <B>
                    <u> Sub Line : </u>
                  </B>
                  <xsl:value-of select="//com.csc_Subline"/>
                  <xsl:value-of select="'   '"/>
                  <xsl:value-of select="//com.csc_SublineDesc"/>
                </TD>
		<!--Nitin Goel Start : Worked on MITS - Class Code for GC, MITS 36753,07/14/2014-->
	       <TD >
                  <B>
                    <u> Class Code:   </u>
                  </B>
                  <xsl:value-of select="//com.csc_ClassCd"/>
                  <xsl:value-of select="'   '"/>
                  <xsl:value-of select="//com.csc_ClassDesc"/>


                </TD>
		<!--Nitin End-->
              </TR>
              <!--Ankit End-->
            </TABLE>
          </xsl:if>


          <xsl:if test="//com.csc_BaseLOBLine ='PL'">
            <TABLE cellspacing="4" cellpadding="4"  width="100%">
              <TR bgcolor="#6fb3e7">
                <TD colspan="2">
                  <B>Policy Coverage Detail</B>
                </TD>
              </TR>
              <tr>
                <td>

                  <B>
                    <u>  Policy Status :  </u>
                  </B>
                  <xsl:value-of select="//com.csc_RecordStatus"/>

                </td>

                <TD >
                  <B>
                    <u> Unit:   </u>
                  </B>
                  <xsl:value-of select="//com.csc_CoverageLossInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNum']/OtherId"/>

                </TD>
              </tr>
              <TR >


                <TD >
                  <B>
                    <u> Coverage:   </u>
                  </B>
                  <xsl:value-of select="//com.csc_CoverageLossInfo/Coverage/CoverageCd"/>
                  <xsl:value-of select="'   '"/>
                  <xsl:value-of select="//com.csc_CoverageLossInfo/Coverage/CoverageDes"/>


                </TD>

              </TR>
              <tr>

                <TD >
                  <B>
                    <u> RateBook :   </u>
                  </B>
                  <xsl:value-of select="//com.csc_RateBook"/>



                </TD>
                <TD >
                  <B>
                    <u> Premium :   </u>
                  </B>
                  <xsl:value-of select="//CurrentTermAmt"/>



                </TD>

              </tr>
              <tr>

                <TD >
                  <B>
                    <u> Sequence :   </u>
                  </B>
                  <xsl:value-of select="//com.csc_CoverageLossInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_CoverageSeq']/OtherId"/>



                </TD>
                <TD >
                  <B>
                    <u> Premium :   </u>
                  </B>
                  <xsl:value-of select="//CurrentTermAmt"/>



                </TD>

              </tr>
              <tr>

                <TD >
                  <B>
                    <u> Limit :   </u>
                  </B>
                  <xsl:value-of select="//Limit/FormatCurrencyAmt/Amt"/>



                </TD>
                <TD >
                  <B>
                    <u> Limit Text  :   </u>
                  </B>
                  <xsl:value-of select="//Limit/LimitAppliesToCd"/>

                </TD>

              </tr>
              <tr>
                <td>
                  <B>
                    <U> Annual Statement :  </U>
                  </B>
                  <xsl:value-of select="//com.csc_AnnualStatement"/>
                </td>
                <td>
                  <B>
                    <U> Product Line : </U>
                  </B>
                  <xsl:value-of select="//com.csc_ProductLine"/>
                </td>
              </tr>
              <!--Ankit Start : Worked on MITS - 34333-->
              <TR>
                <TD>
                  <B>
                    <u> Sub Line : </u>
                  </B>
                  <xsl:value-of select="//com.csc_Subline"/>
                  <xsl:value-of select="'   '"/>
                  <xsl:value-of select="//com.csc_SublineDesc"/>
                </TD>
		<!--Nitin Goel Start : Worked on MITS - Class Code for GC, MITS 36753,07/14/2014-->
	       <TD >
                  <B>
                    <u> Class Code:   </u>
                  </B>
                  <xsl:value-of select="//com.csc_ClassCd"/>
                  <xsl:value-of select="'   '"/>
                  <xsl:value-of select="//com.csc_ClassDesc"/>


                </TD>
		<!--Nitin End-->
              </TR>
              <!--Ankit End-->
            </TABLE>
          </xsl:if>
          <xsl:if test="//com.csc_BaseLOBLine ='CL'">
            <TABLE cellspacing="4" cellpadding="4"  width="100%">
              <TR bgcolor="#6fb3e7">
                <TD colspan="2">
                  <B>Policy Coverage Detail</B>
                </TD>
              </TR>
              <tr>
                <td>

                  <B>
                    <u>  Policy Status :  </u>
                  </B>
                  <xsl:value-of select="//com.csc_RecordStatus"/>

                </td>


              </tr>
              <tr>
                <td>

                  <B>
                    <u> Product:  </u>
                  </B>
                  <xsl:value-of select="//CompanyProductCd"/>

                </td>


              </tr>
              <TR >


                <TD >
                  <B>
                    <u> Coverage:   </u>
                  </B>
                  <xsl:value-of select="//com.csc_CoverageLossInfo/Coverage/CoverageCd"/>


                </TD>
                <TD >
                  <B>
                    <u>  Sequnece:   </u>
                  </B>
                  <xsl:value-of select="//com.csc_CoverageLossInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_ClassSeq']/OtherId"/>


                </TD>
              </TR>
              <tr>


                <TD >
                  <B>
                    <u> RateBook:   </u>
                  </B>
                  <xsl:value-of select="//com.csc_RateBook"/>

                </TD>
                <td>
                  <B>
                    <u>  Premium:   </u>
                  </B>
                  <xsl:value-of select="//CurrentTermAmt"/>


                </td>
              </tr>
              <tr>

                <TD >
                  <B>
                    <u> Class Code :   </u>
                  </B>
                  <xsl:value-of select="//com.csc_ClassCd"/>
                  <xsl:value-of select="'  '"/>
                  <xsl:value-of select="//com.csc_ClassDesc"/>

                </TD>


              </tr>
              <tr>
                <td>
                  <B>
                    <U> Annual Statement :  </U>
                  </B>
                  <xsl:value-of select="//com.csc_AnnualStatement"/>
                </td>
                <td>
                  <B>
                    <U> Product Line : </U>
                  </B>
                  <xsl:value-of select="//com.csc_ProductLine"/>
                </td>
              </tr>
              <!--Ankit Start : Worked on MITS - 34333-->
              <TR>
                <TD>
                  <B>
                    <u> Sub Line : </u>
                  </B>
                  <xsl:value-of select="//com.csc_Subline"/>
                  <xsl:value-of select="'   '"/>
                  <xsl:value-of select="//com.csc_SublineDesc"/>
                </TD>
		<!--Nitin Goel Start : Worked on MITS - Class Code for GC, MITS 36753,07/14/2014-->
	       <TD >
                  <B>
                    <u> Class Code:   </u>
                  </B>
                  <xsl:value-of select="//com.csc_ClassCd"/>
                  <xsl:value-of select="'   '"/>
                  <xsl:value-of select="//com.csc_ClassDesc"/>


                </TD>
		<!--Nitin End-->
              </TR>
              <!--Ankit End-->
              <xsl:for-each select="//com.csc_AdditionalUserArea/*">
                <TR bgcolor="">
                  <TD>
                    <B>
                      <u>
                        <xsl:value-of select="name()"/> :
                      </u>
                    </B>
                  </TD>
                  <td>
                    <xsl:value-of select="node()"/>
                  </td>


                </TR>

              </xsl:for-each>
            </TABLE>
          </xsl:if>
     
        </xsl:otherwise>
      </xsl:choose>




    </FONT>

  </xsl:template>

</xsl:stylesheet>
