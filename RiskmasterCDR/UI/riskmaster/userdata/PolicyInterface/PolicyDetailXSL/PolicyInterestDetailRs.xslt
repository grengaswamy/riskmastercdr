﻿<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:template match="/">
    <FONT face="Ariel" size="2">

      <TABLE cellspacing="4" cellpadding="4" width="100%" >

        <TR bgcolor="#6fb3e7">

        
          <td colspan="2">
            <FONT face="Ariel" size="4">
              <B>
             Additional Interest Detail
              </B>
            </FONT>
          
          </td>

        </TR>
        <tr></tr>
        <TR >
          <TD >
            <B>
              <u>Interest Type: </u>
            </B>
            <xsl:value-of select="//InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd"/>
            <xsl:value-of select="'  '"/>
            <xsl:value-of select="//InsuredOrPrincipalInfo/InsuredOrPrincipalRoleDesc"/>
            
          </TD>
          <TD >
            <B>
              <u>Interest Location: </u>
            </B>
            <xsl:value-of select="//InsuredOrPrincipal/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Location']/OtherId"/>

          </TD>


        </TR>
        <tr>

          
          <TD >
            <B>
              <u>Interest Sequence: </u>
            </B>
            <xsl:value-of select="//InsuredOrPrincipal/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_SequenceNum']/OtherId"/>

          </TD>
          <TD >
            <B>
              <u>Name: </u>
            </B>
            <xsl:value-of select="//GeneralPartyInfo/NameInfo/CommlName/CommercialName"/>

          </TD>

        </tr>
        <tr>

          
          <TD >
            <B>
              <u>Address: </u>
            </B>
            <xsl:value-of select="//Addr/Addr2"/>

          </TD>
          <TD >
            <B>
              <u>Optional: </u>
            </B>
            <xsl:value-of select="//Addr/Addr1"/>

          </TD>
         
        </tr>
        <tr>
          <td>

            <B>
              <u>City: </u>
            </B>
            <xsl:value-of select="//Addr/City"/>

          </td>
          <td>

            <B>
              <u>State : </u>
            </B>
            <xsl:value-of select="//Addr/StateProvCd"/>

          </td>
          
        </tr>
        <tr>
          <td>

            <B>
              <u>Postal Code: </u>
            </B>
            <xsl:value-of select="//Addr/PostalCode"/>

          </td>
          
        </tr>
        <tr>
          
          <td>
            <B>
              <u>Effective Date: </u>
            </B>
            <xsl:value-of select="//ContractTerm/EffectiveDt"/>

          </td>
          <td>
            <B>
              <u>Expiration Date: </u>
            </B>
            <xsl:value-of select="//ContractTerm/ExpirationDt"/>

          </td>


        </tr>
        <tr>

          <td>
            <B>
              <u>FEIN/SSN : </u>
            </B>
            <xsl:value-of select="//GeneralPartyInfo/NameInfo/TaxIdentity[TaxIdTypeCd='FEIN/SSN']/TaxId"/>

          </td>
         
          <td>
            <B>
              <u>Tax Id: </u>
            </B>
            <xsl:value-of select="//GeneralPartyInfo/NameInfo/TaxIdentity[TaxIdTypeCd='TaxId']/TaxId"/>

          </td>

        </tr>
        <tr>

          <td>
            <B>
              <u>Loan Number :</u>
            </B>
            <xsl:value-of select="//com.csc_LoanAccNum"/>



          </td>
         


       
        </tr>
      </TABLE>
    </FONT>

  </xsl:template>
</xsl:stylesheet>
