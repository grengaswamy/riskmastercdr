﻿<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions">
  <xsl:template match="/">
    <FONT face="Ariel" size="2">
      <xsl:choose>
        <xsl:when test="normalize-space(/ACORD/ClaimsSvcRs/com.csc_PolicyUnitFetchRs/Policy/com.csc_IssueCd) = 'M'">
          <TABLE cellspacing="4" cellpadding="4" width="100%" >
            <TR bgcolor="#6fb3e7">
              <TD colspan="2">
                <B>Unit Information</B>
              </TD>
            </TR>
            <xsl:for-each select="//com.csc_InsuredNameIdInfo/ItemIdInfo/OtherIdentifier">
              <TR bgcolor="">
                <TD>
                  <B>
                    <xsl:value-of select="OtherIdTypeCd"/>
                  </B>
                </TD>
                <td>
                  <xsl:value-of select="OtherId"/>
                </td>
              </TR>
            </xsl:for-each>
            <TR bgcolor="">
              <TD >
                <B>Unit Premium....:</B>
              </TD>
              <TD >
                <xsl:value-of select="ACORD/ClaimsSvcRs/com.csc_PolicyUnitFetchRs/InsuredOrPrincipal/PremiumInfo/PremiumAmt"/>
              </TD>
            </TR>
          </TABLE>
        </xsl:when>
        <xsl:otherwise>
          <xsl:if test="//com.csc_BaseLOBLine ='WL'">
            <xsl:if test="//WorkCompLocInfo">
            <TABLE cellspacing="4" cellpadding="4"  width="100%">

              <TR bgcolor="#6fb3e7">


                <td  colspan="2">

                  <B>
                    Site Entry

                  </B>


                </td>

              </TR>
           
              <tr>
                <td>

                  <B>
                    <u>  Policy Status :                   </u>
                  </B>
                
                  <xsl:value-of select="//com.csc_UnitStatusDesc"/>
                  <BR></BR>
                </td>
               


              </tr>
              <TR >
                <TD >
                  <B>
                    <u>Site:   </u>
                  </B>
                  <xsl:value-of select="//ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId"/>


                </TD>

                <TD >
                  <B>
                    <u> Name:   </u>
                  </B>
                  <xsl:value-of select="//LocationName"/>

                </TD>

              </TR>
              <tr>

                <TD >
                  <B>
                    <u> Optional:   </u>
                  </B>
                  <xsl:value-of select="//com.csc_OptionalName"/>


                </TD>
                <!--dbisht6 start for mits 3588-->
              <TD >
                  <B>
                    <u>Insurance Line   </u>
                  </B>
                  <!--<xsl:value-of select="./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/WorkCompLocInfo/Location/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId"/>-->
                <xsl:value-of select="//ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId"/>


                </TD>
                <!--dbisht6 ends-->


              </tr>
              <tr>
                <td>
                  <B>
                    <u> Address:   </u>
                  </B>
                  <xsl:value-of select="//Addr1"/>
                  <xsl:value-of select="' '"/>
                  <xsl:value-of select="//Addr2"/>
                </td>
                <TD >
                  <B>
                    <u> City :   </u>
                  </B>
                  <xsl:value-of select="//City"/>



                </TD>

              </tr>
              <tr>


                <td>
                  <B>
                    <u> State :    </u>
                  </B>
                  <xsl:value-of select="//StateProvCd"/>

                </td>
                <TD >
                  <B>
                    <u>Postal Code:   </u>
                  </B>
                  <xsl:value-of select="//PostalCode"/>

                </TD>
              </tr>
              <tr>
                <td>

                  <B>
                    <u> Country Code:   </u>
                  </B>
                  <xsl:value-of select="//County"/>

                </td>

                <td>
                  <B>
                    <u>Phone Number:   </u>
                  </B>
                  <xsl:value-of select="//PhoneInfo"/>

                </td>
              </tr>
              <tr>

                <td>
                  <B>
                    <u> Contact:   </u>
                  </B>
                  <xsl:value-of select="//EmailInfo"/>

                </td>

                <td>
                  <B>
                    <u> Tax Location :    </u>
                  </B>
                  <xsl:value-of select="//TaxCd"/>

                </td>
              </tr>
              <tr>


                <td>
                  <B>
                    <u> Audit Basis :   </u>
                  </B>
                  <xsl:value-of select="//com.csc_Audit_Basis"/>



                </td>
                <td>
                  <B>
                    <u> Audit Type :    </u>
                  </B>
                  <xsl:value-of select="//com.csc_AuditType"/>



                </td>

              </tr>

              <tr>

                <td>
                  <B>
                    <u>  Auditor (Interim Audit) :     </u>
                  </B>
                  <xsl:value-of select="//com_csc_SiteAuditor_I"/>
                  <xsl:value-of select="' '"/>
                  <xsl:value-of select="//com_csc_SiteAuditorName_I"/>

                </td>



              </tr>
              <tr>

                <td>
                  <B>
                    <u>  Auditor (Check Audit):    </u>
                  </B>
                  <xsl:value-of select="//com_csc_SiteAuditor_C"/>
                  <xsl:value-of select="' '"/>
                  <xsl:value-of select="//com_csc_SiteAuditorName_C"/>

                </td>


              </tr>
              <tr>
                <td>
                  <B>
                    <u>Auditor (Final Audit): </u>
                  </B>
                  <xsl:value-of select="//com.csc_SiteAuditor"/>
                  <xsl:value-of select="' '"/>
                  <xsl:value-of select="//com.csc_SiteAuditorName"/>
                  
                </td>
              </tr>
              <tr>

                <td>
                  <B>
                    <u> Unemployment Number :    </u>
                  </B>
                  <xsl:value-of select="//TaxCodeInfo/TaxIdentity[TaxIdTypeCd='Unemployment']/TaxId"/>

                </td>
                <td>
                  <B>
                    <u>  Number Of Employees:    </u>
                  </B>
                  <xsl:value-of select="//NumEmployees"/>

                </td>


              </tr>
              <tr>

                <td>
                  <B>
                    <u> Federal Employer Identification Number (FEIN) :    </u>
                  </B>
                  <xsl:value-of select="//TaxCodeInfo/TaxIdentity[TaxIdTypeCd='FEIN']/TaxId"/>


                </td>


              </tr>
              <tr>

                <td>
                  <B>
                    <u> Standard Industrial Classification CODE (SIC) :     </u>
                  </B>
                  <xsl:value-of select="//SICCd"/>

                </td>

              </tr>
              <tr>

                <td>
                  <B>
                    <u> Governing Class Code :    </u>
                  </B>
                  <xsl:value-of select="//com.csc_PrimaryClassCd"/>

                </td>

                <td>
                  <B>
                    <u> Sequence No. :   </u>
                  </B>
                  <xsl:value-of select="//Location/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_SiteSequenceNum']/OtherId"/>

                </td>

              </tr>
              <tr>

                <td>
                  <B>
                    <u> Governing Class Description :    </u>
                  </B>
                  <xsl:value-of select="//com.csc_PrimaryClassDesc"/>
                </td>
              </tr>
            </TABLE>
            </xsl:if>
            <xsl:if test="//InsuredOrPrincipal">
              <TABLE cellspacing="4" cellpadding="4" width="100%" >
                <TR bgcolor="#6fb3e7">
                  <TD colspan="2">
                    <B>Unit Information</B>
                  </TD>
                </TR>
                <xsl:for-each select="//com.csc_InsuredNameIdInfo/ItemIdInfo/OtherIdentifier">
                  <TR bgcolor="">
                    <TD>
                      <B>
                        <xsl:value-of select="OtherIdTypeCd"/>
                      </B>
                    </TD>
                    <td>
                      <xsl:value-of select="OtherId"/>
                    </td>
                  </TR>
                </xsl:for-each>
                <TR bgcolor="">
                  <TD >
                    <B>Unit Premium....:</B>
                  </TD>
                  <TD >
                    <xsl:value-of select="ACORD/ClaimsSvcRs/com.csc_PolicyUnitFetchRs/InsuredOrPrincipal/PremiumInfo/PremiumAmt"/>
                  </TD>
                </TR>
              </TABLE>
            </xsl:if>
          </xsl:if>


          <xsl:if test="//com.csc_BaseLOBLine ='AL'">
            <xsl:if test="//AutoLossInfo">
            <TABLE cellspacing="4" cellpadding="4"  width="100%">

              <TR bgcolor="#6fb3e7">


                <td  colspan="2">

                  <B>
                    Vehicle Entry

                  </B>


                </td>

              </TR>
              <tr>
                <td>

                  <B>
                    <u>  Policy Status :  </u>
                  </B>
                  <xsl:value-of select="//com.csc_UnitStatus"/>

                </td>


              </tr>
              <TR >
                <TD >
                  <B>
                    <u> Insurance Line:   </u>
                  </B>
                  <xsl:value-of select="//VehInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId"/>

                </TD>

                <TD >
                  <B>
                    <u> Product:   </u>
                  </B>
                  <xsl:value-of select="//VehInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Product']/OtherId"/>

                </TD>

              </TR>
              <tr>

                <TD >
                  <B>
                    <u> Unit:   </u>
                  </B>
                  <xsl:value-of select="//VehInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId"/>


                </TD>
                <TD >
                  <B>
                    <u> Unit Premium:   </u>
                  </B>
                  <xsl:value-of select="//PremiumInfo/PremiumAmt"/>

                </TD>

              </tr>
              <tr>
                <td>
                  <B>
                    <u> Rate State:   </u>
                  </B>
                  <xsl:value-of select="//VehInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RateState']/OtherId"/>


                </td>
                <TD >
                  <B>
                    <u> Territory :   </u>
                  </B>
                  <xsl:value-of select="//com.csc_Territory"/>



                </TD>

              </tr>
              <tr>


                <td>
                  <B>
                    <u> Vehicle Year :    </u>
                  </B>
                  <xsl:value-of select="//ModelYear"/>

                </td>
                <TD >
                  <B>
                    <u>Purchase Date :   </u>
                  </B>
                  <xsl:value-of select="//com.csc_PurchaseDt"/>

                </TD>
              </tr>
              <tr>
                <td>

                  <B>
                    <u> Serial/VIN:   </u>
                  </B>
                  <xsl:value-of select="//com.csc_VehicleIdentificationNumber"/>

                </td>

                <td>
                  <B>
                    <u>Make/Model:   </u>
                  </B>
                  <xsl:value-of select="//Model"/>

                </td>
              </tr>
              <tr>

                <td>
                  <B>
                    <u>Type Vehicle:   </u>
                  </B>
                  <xsl:value-of select="//com.csc_VehicleTypeCd"/>

                </td>

                <td>
                  <B>
                    <u> Cost New:    </u>
                  </B>
                  <xsl:value-of select="//com.csc_CostNewAmt"/>

                </td>
              </tr>
              <tr>


                <td>
                  <B>
                    <u> Vehicle Symbol :   </u>
                  </B>
                  <xsl:value-of select="//com.csc_VehicleSymbol"/>



                </td>


              </tr>

              <tr>

                <td>
                  <B>
                    <u>  Airbag Discount :     </u>
                  </B>
                  <xsl:value-of select="//com.csc_AirbagDiscount"/>

                </td>

                <td>
                  <B>
                    <u>  Anti-Lock Brakes:    </u>
                  </B>
                  <xsl:value-of select="//com.csc_AntiLockBrakes"/>

                </td>

              </tr>
              <tr>

                <td>
                  <B>
                    <u> Anti-Theft Indicator:    </u>
                  </B>
                  <xsl:value-of select="//com.csc_AntiTheftInd"/>

                </td>


              </tr>

              <tr>

                <td>
                  <B>
                    <u> Body Type :    </u>
                  </B>
                  <xsl:value-of select="//com.csc_BodyType"/>

                </td>
                <td>
                  <B>
                    <u>  Daytime Running Lights:    </u>
                  </B>
                  <xsl:value-of select="//NumEmployees"/>

                </td>


              </tr>
              <tr>

                <td>
                  <B>
                    <u> Performance :    </u>
                  </B>
                  <xsl:value-of select="//TaxCodeInfo/TaxIdentity[TaxIdTypeCd='FEIN']/TaxId"/>


                </td>


              </tr>
              <tr>

                <td>
                  <B>
                    <u> Use Of Vehicle :    </u>
                  </B>
                  <xsl:value-of select="//com.csc_VehicleUse/ActionCd"/>

                </td>

              </tr>
              <tr>

                <td>
                  <B>
                    <u> Miles To Work :    </u>
                  </B>
                  <xsl:value-of select="//com.csc_MilesToWork"/>

                </td>

                <td>
                  <B>
                    <u> Days Per Week :   </u>
                  </B>
                  <xsl:value-of select="//com.csc_DaysPerWeek"/>

                </td>

              </tr>
              <tr>

                <td>
                  <B>
                    <u> Annual Miles :    </u>
                  </B>
                  <xsl:value-of select="//com.csc_AnnualMiles"/>
                </td>
                <td>
                  <B>
                    <u>Driver Assigned :    </u>
                  </B>
                  <xsl:value-of select="//com.csc_DriverAssigned"/>
                </td>
              </tr>
              <tr>

                <td>
                  <B>
                    <u> Primary Class :    </u>
                  </B>
                  <xsl:value-of select="//com.csc_PrimaryClassCd"/>
                </td>
                <td>
                  <B>
                    <u> Secondary Class  :    </u>
                  </B>
                  <xsl:value-of select="//com.csc_SecondaryClassCd"/>
                </td>
              </tr>
            </TABLE>
            </xsl:if>
            <xsl:if test="//InsuredOrPrincipal">
              <TABLE cellspacing="4" cellpadding="4" width="100%" >
                <TR bgcolor="#6fb3e7">
                  <TD colspan="2">
                    <B>Unit Information</B>
                  </TD>
                </TR>
                <xsl:for-each select="//com.csc_InsuredNameIdInfo/ItemIdInfo/OtherIdentifier">
                  <TR bgcolor="">
                    <TD>
                      <B>
                        <xsl:value-of select="OtherIdTypeCd"/>
                      </B>
                    </TD>
                    <td>
                      <xsl:value-of select="OtherId"/>
                    </td>
                  </TR>
                </xsl:for-each>
                <TR bgcolor="">
                  <TD >
                    <B>Unit Premium....:</B>
                  </TD>
                  <TD >
                    <xsl:value-of select="ACORD/ClaimsSvcRs/com.csc_PolicyUnitFetchRs/InsuredOrPrincipal/PremiumInfo/PremiumAmt"/>
                  </TD>
                </TR>
              </TABLE>
            </xsl:if>
          </xsl:if>

          <xsl:if test="//com.csc_BaseLOBLine ='PL'">
            <xsl:if test="//PropertyLossInfo">
            <TABLE cellspacing="4" cellpadding="4"  width="100%">

              <TR bgcolor="#6fb3e7">


                <td  colspan="2">

                  <B>
                    Property Entry

                  </B>


                </td>

              </TR>
              <tr>
                <td>

                  <B>
                    <u>  Policy Status :  </u>
                  </B>
                
                  <xsl:value-of select="//com.csc_UnitStatus"/>

                </td>


              </tr>
              <TR >
                <TD >
                  <B>
                    <u> Insurance Line:   </u>
                  </B>
                  <xsl:value-of select="//PropertyLossInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId"/>

                </TD>

                <TD >
                  <B>
                    <u> Product:   </u>
                  </B>
                  <xsl:value-of select="//PropertyLossInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Product']/OtherId"/>

                </TD>

              </TR>
              <tr>

                <TD >
                  <B>
                    <u> Unit:   </u>
                  </B>
                  <xsl:value-of select="//PropertyLossInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId"/>


                </TD>
                <TD >
                  <B>
                    <u> Rate Book:   </u>
                  </B>
                  <xsl:value-of select="//com.csc_RateBook"/>

                </TD>

              </tr>
              <tr>
                <td>
                  <B>
                    <u> Unit Premium:   </u>
                  </B>
                  <xsl:value-of select="//PremiumInfo/PremiumAmt"/>


                </td>
                <TD >
                  <B>
                    <u> Optional Coverage Premium :   </u>
                  </B>
                  <xsl:value-of select="//CurrentTermAmt"/>

                </TD>

              </tr>
              <tr>


                <td>
                  <B>
                    <u> State :    </u>
                  </B>
                  <xsl:value-of select="//com.csc_RateState"/>

                </td>
                <TD >
                  <B>
                    <u>Location Address :   </u>
                  </B>
                  <xsl:value-of select="//com.csc_UnitDesc"/>

                </TD>
              </tr>
              <tr>
                <td>

                  <B>
                    <u> Location City :   </u>
                  </B>
                  <xsl:value-of select="//com.csc_UnitCity"/>

                </td>

                <td>
                  <B>
                    <u>Postal Code :   </u>
                  </B>
                  <xsl:value-of select="//com.csc_Zipcode"/>

                </td>
              </tr>
              <tr>

                <td>
                  <B>
                    <u>Zone :   </u>
                  </B>
                  <xsl:value-of select="//com.csc_Territory"/>

                </td>

                <td>
                  <B>
                    <u> County:    </u>
                  </B>
                  <xsl:value-of select="//com.csc_County"/>

                </td>
              </tr>
              <tr>


                <td>
                  <B>
                    <u> Protection Class :   </u>
                  </B>
                  <xsl:value-of select="//com.csc_ProtectionClass"/>

                </td>


              </tr>

              <tr>

                <td>
                  <B>
                    <u>  Construction :     </u>
                  </B>
                  <xsl:value-of select="//com.csc_ConstructionCd"/>

                </td>

                <td>
                  <B>
                    <u>  Construction Year:    </u>
                  </B>
                  <xsl:value-of select="//com.csc_YearBuilt"/>

                </td>

              </tr>
              <tr>

                <td>
                  <B>
                    <u> Occupancy :    </u>
                  </B>
                  <xsl:value-of select="//com.csc_OccupancyCd"/>

                </td>


              </tr>

              <tr>

                <td>
                  <B>
                    <u> Deductible Amount :    </u>
                  </B>
                  <xsl:value-of select="//Deductible/DeductibleAmt"/>

                </td>
                <td>
                  <B>
                    <u>  Deductible Type :    </u>
                  </B>
                  <xsl:value-of select="//Deductible/DeductibleTypeCd"/>

                </td>


              </tr>
              <tr>

                <td>
                  <B>
                    <u> Number of Families :    </u>
                  </B>
                  <xsl:value-of select="//com.csc_NumberFamilies"/>

                </td>

              </tr>
              <tr>

                <td>
                  <B>
                    <u> Building Amount :    </u>
                  </B>
                  <xsl:value-of select="//LimitAmt"/>

                </td>

              </tr>
              <tr>

                <td>
                  <B>
                    <u> Fire Building Premium :    </u>
                  </B>
                  <xsl:value-of select="//CurrentTermAmt"/>

                </td>

                <td>
                  <B>
                    <u> Optional Coverage Building Contents Premium :   </u>
                  </B>
                  <xsl:value-of select="//com.csc_CoverageLossInfo/Coverage[7]/CurrentTermAmt"/>
                </td>

              </tr>
             
            </TABLE>
            </xsl:if>
            <xsl:if test="//InsuredOrPrincipal">
              <TABLE cellspacing="4" cellpadding="4" width="100%" >
                <TR bgcolor="#6fb3e7">
                  <TD colspan="2">
                    <B>Unit Information</B>
                  </TD>
                </TR>
                <xsl:for-each select="//com.csc_InsuredNameIdInfo/ItemIdInfo/OtherIdentifier">
                  <TR bgcolor="">
                    <TD>
                      <B>
                        <xsl:value-of select="OtherIdTypeCd"/>
                      </B>
                    </TD>
                    <td>
                      <xsl:value-of select="OtherId"/>
                    </td>
                  </TR>
                </xsl:for-each>
                <TR bgcolor="">
                  <TD >
                    <B>Unit Premium....:</B>
                  </TD>
                  <TD >
                    <xsl:value-of select="ACORD/ClaimsSvcRs/com.csc_PolicyUnitFetchRs/InsuredOrPrincipal/PremiumInfo/PremiumAmt"/>
                  </TD>
                </TR>
              </TABLE>
            </xsl:if>
          </xsl:if>


          <xsl:if test="//com.csc_BaseLOBLine ='CL'">
            <xsl:if test="//PropertyLossInfo">
              <TABLE cellspacing="4" cellpadding="4"  width="100%">

                <TR bgcolor="#6fb3e7">


                  <td  colspan="2">

                    <B>
                      Commercial Package Policy

                    </B>


                  </td>

                </TR>
                <tr>
                  <td>

                    <B>
                      <u>  Policy Status :  </u>
                    </B>

                    <xsl:value-of select="//com.csc_UnitStatus"/>

                  </td>


                </tr>
                <TR >
                  <TD >
                    <B>
                      <u> Insurance Line:   </u>
                    </B>
                    <xsl:value-of select="//PropertyLossInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId"/>

                  </TD>

                  <TD >
                    <B>
                      <u> Product:   </u>
                    </B>
                    <xsl:value-of select="//PropertyLossInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Product']/OtherId"/>

                  </TD>

                </TR>
                <tr>

                  <TD >
                    <B>
                      <u> Location:   </u>
                    </B>
                    <xsl:value-of select="//PropertyLossInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskLoc']/OtherId"/>


                  </TD>
                  <TD >
                    <B>
                      <u> Building:   </u>
                    </B>
                    <xsl:value-of select="//PropertyLossInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskSubLoc']/OtherId"/>

                  </TD>

                </tr>
                <tr>
                  <td>
                    <B>
                      <u>  Rate Book:   </u>
                    </B>
                    <xsl:value-of select="//com.csc_RateBook"/>


                  </td>
                  <td>
                    <B>
                      <u>  Premium:   </u>
                    </B>
                    <xsl:value-of select="//PremiumInfo/PremiumAmt"/>


                  </td>

                </tr>
                <tr>


                  <td>
                    <B>
                      <u> State :    </u>
                    </B>
                    <xsl:value-of select="//PropertyLossInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RateState']/OtherId"/>

                  </td>
                  <td>

                    <B>
                      <u> Class Code :   </u>
                    </B>

                    <xsl:value-of select="//com.csc_PrimaryClassCd"/>
                    <xsl:value-of select="'   '"/>
                    <xsl:value-of select="//com.csc_PrimaryClassDesc"/>
                  </td>
                </tr>
              
                  <tr>
                    <td>
                      <B>
                        <u>Desription :   </u>
                      </B>
                      <xsl:value-of select="//com.csc_UnitDescription"/>

                    </td>

                   </tr>
               
                <xsl:for-each select="//com.csc_AdditionalUserArea/*">
                  <TR bgcolor="">
                    <TD>
                      <B>
                        <u>
                          <xsl:value-of select="name()"/> :
                        </u>
                      </B>
                    </TD>
                   <td>
                      <xsl:value-of select="node()"/>
                  </td>

                  
               </TR>

                </xsl:for-each>
              
              </TABLE>
            </xsl:if>
            <xsl:if test="//AutoLossInfo">
              <TABLE cellspacing="4" cellpadding="4"  width="100%">

                <TR bgcolor="#6fb3e7">


                  <td  colspan="2">

                    <B>
                      Commercial Package Policy

                    </B>


                  </td>

                </TR>
                <tr>
                  <td>

                    <B>
                      <u>  Policy Status :  </u>
                    </B>

                    <xsl:value-of select="//com.csc_UnitStatus"/>

                  </td>


                </tr>
                <TR >
                  <TD >
                    <B>
                      <u> Insurance Line:   </u>
                    </B>
                    <xsl:value-of select="//AutoLossInfo/VehInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId"/>

                  </TD>

                  <TD >
                    <B>
                      <u> Product:   </u>
                    </B>
                    <xsl:value-of select="//AutoLossInfo/VehInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Product']/OtherId"/>

                  </TD>

                </TR>
                <tr>

                  <TD >
                    <B>
                      <u> Unit:   </u>
                    </B>
                    <xsl:value-of select="//AutoLossInfo/VehInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId"/>

                  </TD>
                </tr>
                <tr>
                  <td>
                    <B>
                      <u>  Rate Book:   </u>
                    </B>
                    <xsl:value-of select="//com.csc_RateBook"/>


                  </td>
                  <td>
                    <B>
                      <u>  Premium:   </u>
                    </B>
                    <xsl:value-of select="//PremiumInfo/PremiumAmt"/>


                  </td>

                </tr>
                <tr>


                  <td>
                    <B>
                      <u> State :    </u>
                    </B>
                    <xsl:value-of select="//AutoLossInfo/VehInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RateState']/OtherId"/>

                  </td>
                  <td>

                    <B>
                      <u> Class Code :   </u>
                    </B>

                    <xsl:value-of select="//com.csc_PrimaryClassCd"/>
                    <xsl:value-of select="'   '"/>
                    <xsl:value-of select="//com.csc_PrimaryClassDesc"/>
                  </td>
                </tr>

                <tr>
                  <td>
                    <B>
                      <u>Desription :   </u>
                    </B>
                    <xsl:value-of select="//Manufacturer"/>

                  </td>

                </tr>
                <tr>
                  <td>
                    <B>
                      <u>VIN Number  :   </u>
                    </B>
                    <xsl:value-of select="//com.csc_VehicleIdentificationNumber"/>

                  </td>
                  <td>
                    <B>
                      <u>Model Year  :   </u>
                    </B>
                    <xsl:value-of select="//ModelYear"/>

                  </td>

                </tr>

                <xsl:for-each select="//com.csc_AdditionalUserArea/*">
                  <TR bgcolor="">
                    <TD>
                      <B>
                        <u>
                          <xsl:value-of select="name()"/> :
                        </u>
                      </B>
                    </TD>
                    <td>
                      <xsl:value-of select="node()"/>
                    </td>


                  </TR>

                </xsl:for-each>

              </TABLE>
            </xsl:if>
            <xsl:if test="//InsuredOrPrincipal">
              <TABLE cellspacing="4" cellpadding="4" width="100%" >
                <TR bgcolor="#6fb3e7">
                  <TD colspan="2">
                    <B>Unit Information</B>
                  </TD>
                </TR>
                <xsl:for-each select="//com.csc_InsuredNameIdInfo/ItemIdInfo/OtherIdentifier">
                  <TR bgcolor="">
                    <TD>
                      <B>
                        <xsl:value-of select="OtherIdTypeCd"/>
                      </B>
                    </TD>
                    <td>
                      <xsl:value-of select="OtherId"/>
                    </td>
                  </TR>
                </xsl:for-each>
                <TR bgcolor="">
                  <TD >
                    <B>Unit Premium....:</B>
                  </TD>
                  <TD >
                    <xsl:value-of select="ACORD/ClaimsSvcRs/com.csc_PolicyUnitFetchRs/InsuredOrPrincipal/PremiumInfo/PremiumAmt"/>
                  </TD>
                </TR>
              </TABLE> 
            </xsl:if>
          </xsl:if>
         
          
        </xsl:otherwise>
      </xsl:choose>




    </FONT>

  </xsl:template>
  
</xsl:stylesheet>
