﻿<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:template match="/">
    <FONT face="Ariel" size="2">

      <TABLE cellspacing="4" cellpadding="4" width="100%" >

        <TR bgcolor="#6fb3e7">


          <td colspan="2">
            <FONT face="Ariel" size="4">
              <B>
                Driver Entry
              </B>
            </FONT>

          </td>

        </TR>
        <tr>

          <td>
            
          
            <B>
              <u>Status : </u>
            </B>
            <xsl:value-of select="//com.csc_DriverStatus"/>
            <BR></BR>
          </td>
          <td>


            <B>
              <u>Driver ID: </u>
            </B>
            <xsl:value-of select="//DriverInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_DriverId']/OtherId"/>
            <BR></BR>
          </td>
        </tr>
      
        <TR >
          <TD >
            <B>
              <u>Driver Name : </u>
            </B>
            <xsl:value-of select="//PersonName/GivenName"/>
            <xsl:value-of select="'  '"/>
            <xsl:value-of select="//PersonName/Surname"/>
            <xsl:value-of select="'  '"/>
            <xsl:value-of select="//PersonName/NameSuffix"/>
            <BR></BR>
          </TD>
        
        </TR>
        <tr>


          <TD >
            <B>
              <u>Date Of Birth : </u>
            </B>
            <xsl:value-of select="//BirthDt"/>
            <BR></BR>
          </TD>
          <TD >
            <B>
              <u>Gender : </u>
            </B>
            <xsl:value-of select="//GenderCd"/>
            <BR></BR>
          </TD>

        </tr>
        <tr>


          <TD >
            <B>
              <u>Marital Status : </u>
            </B>
            <xsl:value-of select="//MaritalStatusCd"/>
            <BR></BR>
          </TD>
          <TD >
            <B>
              <u>Relation to Insured : </u>
            </B>
            <xsl:value-of select="//com.csc_RelationToInsured"/>
            <BR></BR>
          </TD>

        </tr>
        <tr>
          <td>

            <B>
              <u>License State : </u>
            </B>
            <xsl:value-of select="//StateProvCd"/>

          </td>
          <td>

            <B>
              <u>License Number : </u>
            </B>
            <xsl:value-of select="//LicensePermitNumber"/>
            <BR></BR>
          </td>

        </tr>
        <tr>
          <td>

            <B>
              <u>Date Licensed : </u>
            </B>
            <xsl:value-of select="//LicensedDt"/>
            <BR></BR>
          </td>
          <td>
            <B>
              <u>MVR Indicator : </u>
            </B>
            <xsl:value-of select="//com.csc.MVRInd"/>
            <BR></BR>
          </td>
        </tr>
        
        <tr>

          <td>

            <B>
              <u>Driver Status : </u>
            </B>
            <xsl:value-of select="//com_csc_ExcludedDriverInd"/>
            <BR></BR>
          </td>
          <td>
            <B>
              <u>SR-22: </u>
            </B>
            <xsl:value-of select="//com.csc_UsrInd_6"/>
            <BR></BR>
          </td>


        </tr>
      
        <tr>

          <td>
            <B>
              <u>Principal Operator : </u>
            </B>
            <xsl:value-of select="//com.csc_PrincipalOperator_VH_1"/>
            <xsl:value-of select="'   '"/>
            <xsl:value-of select="//com.csc_PrincipalOperator_VH_2"/>
            <xsl:value-of select="'   '"/>
            <xsl:value-of select="//com.csc_PrincipalOperator_VH_3"/>
            <xsl:value-of select="'   '"/>
            <BR></BR>
          </td>

          <td>
            <B>
              <u>Part Time Operator : </u>
            </B>
            <xsl:value-of select="//com.csc_ParttimeOperator_VH_1"/>
            <xsl:value-of select="'   '"/>
            <xsl:value-of select="//com.csc_ParttimeOperator_VH_2"/>
            <xsl:value-of select="'   '"/>
            <xsl:value-of select="//com.csc_ParttimeOperator_VH_3"/>
            <xsl:value-of select="'   '"/>
            <BR></BR>
          </td>

        </tr>
        <tr>

          <td>
            <B>
              <u>Age :</u>
            </B>
            <xsl:value-of select="//Age"/>

            
          </td>
          <td>
            <B>
              <u>Driver Class :</u>
            </B>
            <xsl:value-of select="//com.csc.DriverClass"/>

           

          </td>




        </tr>
      </TABLE>
    </FONT>

  </xsl:template>
</xsl:stylesheet>
