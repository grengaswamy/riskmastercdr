<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/TR/WD-xsl">
<xsl:template match="/">
	<xsl:for-each select="data">
		<xsl:apply-templates />
	</xsl:for-each>
</xsl:template>

<xsl:template match="dir1">
<option><xsl:attribute name="value"><xsl:value-of select="@id"/></xsl:attribute><xsl:value-of select="text()"/></option>
<xsl:apply-templates />
</xsl:template>
<xsl:template match="dir2">
<option><xsl:if test="@def[.='1']"><xsl:attribute name="selected">1</xsl:attribute></xsl:if><xsl:attribute name="value"><xsl:value-of select="@id"/></xsl:attribute>&#160;&#160;&#160;<xsl:value-of select="text()"/></option>
<xsl:apply-templates />
</xsl:template>
<xsl:template match="dir3">
<option><xsl:if test="@def[.='1']"><xsl:attribute name="selected">1</xsl:attribute></xsl:if><xsl:attribute name="value"><xsl:value-of select="@id"/></xsl:attribute>&#160;&#160;&#160;&#160;&#160;&#160;<xsl:value-of select="text()"/></option>
<xsl:apply-templates />
</xsl:template>
<xsl:template match="dir4">
<option><xsl:if test="@def[.='1']"><xsl:attribute name="selected">1</xsl:attribute></xsl:if><xsl:attribute name="value"><xsl:value-of select="@id"/></xsl:attribute>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;<xsl:value-of select="text()"/></option>
<xsl:apply-templates />
</xsl:template>
<xsl:template match="dir5">
<option><xsl:if test="@def[.='1']"><xsl:attribute name="selected">1</xsl:attribute></xsl:if><xsl:attribute name="value"><xsl:value-of select="@id"/></xsl:attribute>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;<xsl:value-of select="text()"/></option>
<xsl:apply-templates />
</xsl:template>
<xsl:template match="dir6">
<option><xsl:if test="@def[.='1']"><xsl:attribute name="selected">1</xsl:attribute></xsl:if><xsl:attribute name="value"><xsl:value-of select="@id"/></xsl:attribute>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;<xsl:value-of select="text()"/></option>
<xsl:apply-templates />
</xsl:template>

</xsl:stylesheet>