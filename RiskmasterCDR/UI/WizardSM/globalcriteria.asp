<!-- #include file ="session.inc" -->
<!-- #include file ="generic.asp" -->

<html>
	<head>
		<link rel="stylesheet" href="RMNet.css" type="text/css"/>
		<title>SMD - [Global Criteria]</title>
		<script language="JavaScript">
			var tempStr;
			var gCriteria;

			function check(xmlStr)
			{
				var tempXML
				if(xmlStr.indexOf("aggcount") > -1)
				{
					tempXML = "COUNT(*)";
					return tempXML;
				}	
				else if(xmlStr.indexOf("agguniquecount") > -1)
				{
					tempXML = "COUNT" + " (DISTINCT " + window.document.frmData.hdGlobalCriteriaName.value + ")";
					return tempXML;
				}
				else if(xmlStr.indexOf("aggsum") > -1)
				{
					tempXML = "SUM" + " (" + window.document.frmData.hdGlobalCriteriaName.value + ")";
					return tempXML;
				}
				else if(xmlStr.indexOf("aggavg") > -1)
				{
					tempXML = "AVG" + " (" + window.document.frmData.hdGlobalCriteriaName.value + ")";
					return tempXML;
				}
				else if(xmlStr.indexOf("aggmin") > -1)
				{
					tempXML = "MIN" + " (" + window.document.frmData.hdGlobalCriteriaName.value + ")";
					return tempXML;
				}
				else if(xmlStr.indexOf("aggmax") > -1)
				{
					tempXML = "MAX" + " (" + window.document.frmData.hdGlobalCriteriaName.value + ")";
					return tempXML;
				}
			}

			function ok_click()
			{
				var temp =""
				temp = check(document.frmData.hdGlobalCriteriaXML.value);
				var connector;
				
				if(window.document.frmData.optConnector[0].checked == true)
					connector = "AND";
				else if (window.document.frmData.optConnector[1].checked == true)
					connector = "OR";
				
				//-------------------Validations
				if(document.frmData.cboOperator1.selectedIndex == 0 && (window.document.frmData.optConnector[0].checked == false && window.document.frmData.optConnector[1].checked == false))
				{
					alert('Please select an operator');
					return;
				}
				
				if(document.frmData.cboOperator1.selectedIndex == 0 && (window.document.frmData.optConnector[0].checked == true || window.document.frmData.optConnector[1].checked == true))
				{
					alert('Please select an operator');
					return;
				}

				if(document.frmData.cboOperator2.selectedIndex == 0 && (window.document.frmData.optConnector[0].checked == true || window.document.frmData.optConnector[1].checked == true))
				{
					alert('Please select an operator');
					return;
				}
				
				if((window.document.frmData.cboOperator2.options[window.document.frmData.cboOperator2.selectedIndex].text != '' || window.document.frmData.txtValue2.value != ''))
				{
					if(window.document.frmData.optConnector[0].checked == false && window.document.frmData.optConnector[1].checked == false)
					{
						alert('Please choose a connector');
						return;
					}
				}
				else if(window.document.frmData.cboOperator1.options[window.document.frmData.cboOperator1.selectedIndex].text == '')
				{
					alert('Please select an operator');
					return;
				}
				else if(window.document.frmData.txtValue1.value == '')
				{
					alert('Please enter a value');
					return;
				}
				else if(window.document.frmData.cboOperator1.options[window.document.frmData.cboOperator1.selectedIndex].text != '' && window.document.frmData.txtValue1.value != '' && (window.document.frmData.optConnector[0].checked == true || window.document.frmData.optConnector[1].checked == true))
				{
					if(window.document.frmData.cboOperator2.options[window.document.frmData.cboOperator2.selectedIndex].text == '')
					{	
						alert('Please select an operator');
						return;
					}
					else if(window.document.frmData.cboOperator2.options[window.document.frmData.cboOperator2.selectedIndex].text != '' && window.document.frmData.txtValue2.value == '')
					{
						alert('Please enter a value');
						return;
					}
				}
				
				if(window.document.frmData.cboOperator1.options[window.document.frmData.cboOperator1.selectedIndex].text != '' && window.document.frmData.txtValue1.value != '' && window.document.frmData.cboOperator2.options[window.document.frmData.cboOperator2.selectedIndex].text == '' && window.document.frmData.txtValue2.value == '')
					tempStr = "(" + temp + window.document.frmData.cboOperator1.options[window.document.frmData.cboOperator1.selectedIndex].text + unescape(window.document.frmData.txtValue1.value) + ")";
				else
				{
					tempStr = "(" + temp + window.document.frmData.cboOperator1.options[window.document.frmData.cboOperator1.selectedIndex].text;
					tempStr = tempStr + unescape(window.document.frmData.txtValue1.value) + " " +connector + " " ;
					tempStr = tempStr + temp
					tempStr = tempStr + window.document.frmData.cboOperator2.options[window.document.frmData.cboOperator2.selectedIndex].text;
					tempStr = tempStr + unescape(window.document.frmData.txtValue2.value) + ")"; 
				}
				
				gCriteria = tempStr;
				
				operatorList = selXMLOperator(document.frmData.cboOperator1.selectedIndex)
				
				dataList = unescape(document.frmData.txtValue1.value) + "^^^^^" + unescape(document.frmData.txtValue2.value);
				
				if(document.frmData.optConnector[0].checked == true)
					connector = "and";
				else if(document.frmData.optConnector[1].checked == true)
					connector = "or";
				else
					connector = "";
					
				if(connector !='')
					operatorList = operatorList + "," + selXMLOperator(document.frmData.cboOperator2.selectedIndex)

				window.opener.globalCriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,operatorList,dataList,connector,gCriteria);
				window.opener.displayCriteria();	
				window.opener.displayGlobalCriteria();
				window.opener.globalCriteriaChange();
				window.close();
			}

			function window_onload()
			{
				if(document.frmData.hdPGlobal.value !='')
				{
					tempArray = document.frmData.hdPGlobal.value.split("~~~~~");
					sConnector = tempArray[3];
					sOperator = tempArray[1].split(",");
					sData = tempArray[2].split("^^^^^"); 
					matchOperator(document.frmData.cboOperator1,sOperator[0]);
					document.frmData.txtValue1.value = sData[0];
					if(sConnector != "")
					{
						if(sConnector == "and")
							document.frmData.optConnector[0].checked = true;
						else
							document.frmData.optConnector[1].checked = true;
						matchOperator(document.frmData.cboOperator2,sOperator[1]);
						document.frmData.txtValue2.value = sData[1];
					}
				}
			}
				
			function matchOperator(pObj,pOperator)
			{
				if(pOperator == "eq")
				{
					pObj.selectedIndex = 1;
					return 1;
				}
				else if(pOperator == "le")
				{
					pObj.selectedIndex = 2;
					return 2;
				}
				else if(pOperator == "ge")
				{
					pObj.selectedIndex = 3;
					return 3;
				}
				else if(pOperator == "ne")
				{
					pObj.selectedIndex = 4;
					return 4;
				}
				else if(pOperator == "gt") //edit by rahul - mits 9681
				{
					pObj.selectedIndex = 5;
					return 5;
				}
				else if(pOperator == "lt")  //edit by rahul - mits 9681
				{
					pObj.selectedIndex = 6;
					return 6; 
				}
			}
				
			//-- fills global operatorList 
			function selXMLOperator(pIndex)
			{
				
				if(pIndex == 0)
					return "";
				
					
				if(pIndex == 1)
					return "eq";
				else if(pIndex == 2)
					return "le";
				else if(pIndex == 3)
					return "ge";
				else if(pIndex == 4)
					return "ne";
				else if(pIndex == 5)
					return "gt"; //edit by rahul - mits 9681
				else if(pIndex == 6)
					return "lt"; //edit by rahul - mits 9681
					
			}

			function delete_click()
			{
				if(document.frmData.txtValue1.value == "" )
				{
					window.close();
					return;
				}
				else
				{
					window.opener.deleteGlobalCriteria(document.frmData.hdGlobalIndex.value);
					window.opener.displayCriteria();	
					window.opener.displayGlobalCriteria();
					window.opener.globalCriteriaChange();
					//window.opener.displayCriteria();	
					window.close();
					return;
				}
			}
		</script>
	</head>
		
	<body onLoad="window_onload();">
	<form name="frmData">
		<table class="formsubtitle" border="0" width="100%" align="center">
			<tr>
				<td class="ctrlgroup">Global Criteria :</td>
			</tr>
		</table>					

		<table width="100%" border="0">
			<tr>
				<td><select size="1" id1=select1 name="cboOperator1">
				<option selected></option>
				<option>=</option>
				<option><=</option>
				<option>>=</option>
				<option><></option>
				<option>></option>
				<option><</option></select>
				</td>
				<td><input name="txtValue1" ></td>
			</tr>
			<tr>
				<td><input type="radio" name="optConnector">&nbsp;AND</td>
				<td><input type="radio" name="optConnector">&nbsp;OR</td>
			</tr>
			<tr>
				<td><select size="1" id1=select2 name="cboOperator2">
				<option selected></option>
				<option>=</option>
				<option><=</option>
				<option>>=</option>
				<option><></option>
				<option>></option>
				<option><</option></select>
				</td>
				<td><input name="txtValue2"></td>
			</tr>
				
			<tr><td colspan="2">&nbsp;</td></tr>
				
			<tr>
				<td align="center" colspan="2"><hr>
					<table width="100%">
						<tr>
							<td width="33%"><input class="button" type="button" name="ok" value="OK" onClick="ok_click()" style="width:100%"></td>
							<td width="34%"><input class="button" type="button" name="cancel" value="Cancel" onClick="window.close()" style="width:100%"></td>
							<td width="33%"><input class="button" type="button" name="btnDelete" value="Delete" onClick="delete_click()" style="width:100%"></td>
						</tr>
					</table>
				</td>
			</tr>
				
		</table>
		<input type="hidden" name="hdGlobalCriteriaXML" value='<%=Request.QueryString("a")%>'>
		<input type="hidden" name="hdPGlobal" value="<%=fixDoubleQuote(Request.QueryString("PGLOBAL"))%>">

		<input type="hidden" name="hdGlobalCriteriaName" value='<%=Request.QueryString("b")%>'>
		<input type="hidden" name="hdGlobalIndex" value='<%=Request.QueryString("INDEX")%>'>
	</form>
	</body>
</html>