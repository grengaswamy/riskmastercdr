<?xml version='1.0'?>
<!--<xsl:stylesheet xmlns:xsl="http://www.w3.org/TR/WD-xsl">-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">


<xsl:template match="/">

<html><head><title></title>
	<link rel="stylesheet" href="org.css" type="text/css"/>
	<script language="JavaScript" SRC="org.js"></script>
	<STYLE TYPE="text/css">
		<!--
			.indented
			   {
			   padding-left: 50pt;
			   padding-right: 50pt;
			   }
			-->
		</STYLE>

</head>
	 <body class="10pt" id="body1">
	<xsl:attribute name="bgcolor">
	 <xsl:value-of select="form/@bcolor"/>
	 </xsl:attribute>

	 <xsl:if test="form/body1/@req_func='yes'">
	<xsl:attribute name="onload"><xsl:value-of select="form/body1/@func"/></xsl:attribute>
	</xsl:if>
		<form name="frmData" method="post">
		<xsl:attribute name="action">
			<xsl:value-of select="form/@actionname"/>
		</xsl:attribute>
		
	<div class="formtitle" id="formtitle" align="center"><xsl:value-of select="form/@title"/></div>
	<table>
	<tr>
			<td align="center" class="errortext">
			<xsl:if  test="//control/org/error/@value[.='1']">
				<!--<table border="0" align="center" width="100%">
				<tr><td class="errortext" align="left">-->
					<xsl:value-of select="//control/org/error/@desc"/>
				<!--</td></tr>
				
				</table>-->
				</xsl:if>
			</td>
	</tr>
		<tr><td>	
		<table align="left" width="100%">
		<xsl:for-each select="form/group">
		
		<tr><td>
				<xsl:apply-templates select="control"/>
			</td>
		</tr>
		</xsl:for-each>
		</table>
		
	</td></tr><tr><td>
	<xsl:if test="form/@cid[.='orgtree']">
		<table align="left">
		<tr>
			<td colspan="2">
				<input type="checkbox" name="chkFilter" value="0" checked="checked"  onclick="setFilter()"/> 		Filter by 	Effective Date 
			</td>
		</tr>
		<tr><td colspan="2">
				<input name="chkTreeNode" type="checkbox" onclick="onChanged('chkTreeNode');"/>
			Search Within Selected Branch
		</td></tr>
		<tr><td colspan="2">
				<input name="chkAddr" type="checkbox"/>
			Search For Address
		</td></tr>

		<tr>
			<td colspan="2">
			Default Expansion Level For List :
			<select size="1" onchange="setOpenLevel();"><xsl:attribute name="name"><xsl:value-of select="form/levellist/@name"/></xsl:attribute>
			<xsl:for-each select="form/levellist/option">
				<xsl:choose>
					<xsl:when test="@selected[.='1']">
					<option selected="selected"><xsl:attribute name="value"><xsl:value-of select="@value"/></xsl:attribute><xsl:value-of select="text()"/></option>
					</xsl:when>
					<xsl:otherwise>
					<option><xsl:attribute name="value"><xsl:value-of select="@value"/></xsl:attribute><xsl:value-of select="text()"/></option>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
			</select>
			</td>
		</tr>
		</table>
	</xsl:if>
		<table align="center">
		<tr align="center">
		
		<td align="center">
		<div align="center">
		<xsl:for-each select="form/button">
			<input type="button" class="button"><xsl:attribute name="value"><xsl:value-of select="@title"/></xsl:attribute><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:if test="@disable[.='1']"><xsl:attribute name="disabled">disabled</xsl:attribute></xsl:if><xsl:attribute name="onClick">buttonAction('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>','<xsl:value-of select="@type"/>')</xsl:attribute></input><xsl:text>   </xsl:text>
		</xsl:for-each>
		</div>
		</td>
		
		</tr>
		</table>
	</td></tr>
	</table>
		</form>
		
	</body>
	
</html>
	

</xsl:template>
<xsl:template match="control">
<xsl:choose>
	<xsl:when test="@type[.='id']"><input type="hidden"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@value"/></xsl:attribute></input></xsl:when>
	<xsl:otherwise>
	<tr>
		<xsl:choose>
			<xsl:when test="@type[.='hidden']"><input type="hidden"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>	</xsl:when>
	<xsl:when test="@type[.='button']">
				<td colspan="2">
				<input type="button" class="button"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@title"/></xsl:attribute><xsl:attribute name="onClick">formHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')</xsl:attribute></input>
				</td>
			</xsl:when>
		<xsl:when test="@type[.='textlabel']">
				<td colspan="2">
					<xsl:value-of select="text()"/>
				</td>
			</xsl:when>
		<xsl:otherwise>
<td>
<xsl:choose>
		<xsl:when test="@isrequ[.='yes']"><b><u><xsl:value-of select="@title"/></u></b></xsl:when>
		<xsl:otherwise><xsl:if test="@type[.!='hidden']"><xsl:value-of select="@title"/></xsl:if></xsl:otherwise></xsl:choose>
	</td>
<td>
	<xsl:choose>
			<xsl:when test="@type[.='checkbox']">
			
			<input type="checkbox" value="1">
			<xsl:attribute name="onclick">
			onChanged('<xsl:value-of select="@name"/>');
			</xsl:attribute>
			<xsl:attribute name="name">	<xsl:value-of select="@name"/></xsl:attribute>
					<xsl:if test="@value[.='1']"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
					<xsl:if test="@lock[.='true']"><xsl:attribute name="disabled">disabled</xsl:attribute></xsl:if>
			</input>
			
		</xsl:when>

			<xsl:when test="@type[.='combobox']">
			<select size="1">
			<!--<xsl:attribute name="onchange">
			<xsl:text>setchanges('</xsl:text><xsl:value-of select="@name"/><xsl:text>')</xsl:text>
			</xsl:attribute>-->
			<xsl:if test="@lock[.='true']"><xsl:attribute name="disabled">disabled</xsl:attribute></xsl:if>
			<xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
				
				
				<xsl:call-template  name="combo_option">
				<xsl:with-param name="opt_name">
				<xsl:value-of select="@optionname"/>
				</xsl:with-param>
				</xsl:call-template>
				
			</select>
			</xsl:when>

			<xsl:when test="@type[.='text']">
			<input   type="text" size="30"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@value"/></xsl:attribute>
<xsl:if test="@lock[.='true']"><xsl:attribute name="disabled">disabled</xsl:attribute></xsl:if>
</input>
</xsl:when>
			
			<xsl:when test="@type[.='date']"><input type="text" size="30" readonly="readonly"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@value"/></xsl:attribute></input><input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of 	select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">selectDate('<xsl:value-of select="@name"/>')</xsl:attribute></input>
</xsl:when>
<xsl:when test="@type[.='orgsearch']">
	<tr>
		<td></td><td class="colheader3">Name</td><td class="colheader3">Parent</td><td class="colheader3">Address</td><td class="colheader3">City</td><td class="colheader3">State</td><td class="colheader3">Zip</td>
	</tr>
	<xsl:for-each select="search/result">
		<tr>
		<td><xsl:value-of select="@abr"/></td>
		<td><a href="#" style="text-decoration:none;">
		<xsl:attribute name="onclick">getTreeForEntity('<xsl:value-of select="@eid"/>','<xsl:value-of select="@searchin"/>');
		</xsl:attribute>
		<xsl:value-of select="@cName"/>
		</a></td>
		<td><xsl:value-of select="@pName"/></td>
		<td><xsl:value-of select="@addr"/></td>
		<td><xsl:value-of select="@city"/></td>
		<td><xsl:value-of select="@state"/></td>
		<td><xsl:value-of select="@zip"/></td>
		</tr>
	</xsl:for-each>
</xsl:when>
<xsl:when test="@type[.='orgTree']">
<div class="divScroll" style="width:650;">
<!--<div align="center"><img id="loadimage" width="445" height="105"	border="0"	src="nothing.gif" alt=""/></div>-->
<script language="javascript">
var m_level;
var m_name;
	 m_level='<xsl:value-of select="//form/group/control/@cmblevel"/>';	
	 m_name='<xsl:value-of select="//form/group/control/@srchstr"/>';	
	
</script>
<span>
	<xsl:for-each select="org">
	
	<xsl:call-template name="getchildren">
	<xsl:with-param name="level">
		<xsl:value-of select="//form/group/control/@num_level"/>		
	</xsl:with-param>
	</xsl:call-template>
	
	</xsl:for-each>
</span>
</div>
</xsl:when>
<xsl:otherwise>Unknown Element Type: <xsl:value-of select="@type"/>
	</xsl:otherwise>
	</xsl:choose>
	</td>
	</xsl:otherwise></xsl:choose>
	</tr>
	</xsl:otherwise>
	
</xsl:choose>
	</xsl:template>
	
	
<xsl:template name="getchildren">
	<xsl:param name="level"/>

			<dl>
			<xsl:choose>
			<xsl:when test="@ischild[.='1']">
			<a>
			<xsl:attribute name="name">
			<xsl:value-of select="@id"/>
			</xsl:attribute>
			</a>
				<!--changes for expand (+) sign disabling-->
		<xsl:choose>
			<xsl:when test="@isSelect[.='1']">
			<xsl:choose>
				<xsl:when test="@level[.=$level] or  $level ='0'">
				<!--disable + sign for level-->
				<xsl:choose>
				<xsl:when test="$level ='0'">
				<a class="link"><xsl:attribute name="href">javascript:getchild('<xsl:value-of 		select="@id"/>','<xsl:value-of select="@parent"/>','<xsl:value-of select="@ischild"/>','<xsl:value-of 		select="@code"/>')</xsl:attribute>+</a><xsl:text>      </xsl:text>
				</xsl:when>
				<xsl:otherwise>
				<a class="link">+</a><xsl:text>      </xsl:text>
				</xsl:otherwise>
				</xsl:choose>
			<a class="select"><xsl:attribute name="href">javascript:getOrg('<xsl:value-of 						select="@id"/>','<xsl:value-of select="@name"/>',false)</xsl:attribute><xsl:value-of select="@name"/>				</a>
				</xsl:when>
				<xsl:otherwise>
			<a class="link"><xsl:attribute name="href">javascript:getchild('<xsl:value-of 		select="@id"/>','<xsl:value-of select="@parent"/>','<xsl:value-of select="@ischild"/>','<xsl:value-of 		select="@code"/>')</xsl:attribute>+</a><xsl:text>      </xsl:text>
				<xsl:choose>
					<xsl:when test="@iscurrent[.='1']">
					<a class="clicked"><xsl:value-of select="@name"/></a>
					</xsl:when>
					<xsl:otherwise>
					<a class="disabled"><xsl:value-of select="@name"/></a>
					</xsl:otherwise>
				</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
			
			</xsl:when>
			<xsl:otherwise>
			<xsl:choose>
				<xsl:when test="@level[.=$level] or  $level ='0'">
				<!--changes for expand sign (+) disabling at this level-->
			<xsl:choose>
				<xsl:when test="$level ='0'">
				<a class="link"><xsl:attribute name="href">javascript:getchild('<xsl:value-of 		select="@id"/>','<xsl:value-of select="@parent"/>','<xsl:value-of select="@ischild"/>','<xsl:value-of 		select="@code"/>')</xsl:attribute>+</a><xsl:text>      </xsl:text>
				</xsl:when>
				<xsl:otherwise>
				<a class="link">+</a><xsl:text>      </xsl:text>
				</xsl:otherwise>
				</xsl:choose>
				<a class="select"><xsl:attribute name="href">javascript:getOrg('<xsl:value-of 						select="@id"/>','<xsl:value-of select="@name"/>',false)</xsl:attribute><xsl:value-of select="@name"/></a>
				</xsl:when>
				<xsl:otherwise>
			<a class="link"><xsl:attribute name="href">javascript:getchild('<xsl:value-of 		select="@id"/>','<xsl:value-of select="@parent"/>','<xsl:value-of select="@ischild"/>','<xsl:value-of 		select="@code"/>')</xsl:attribute>+</a><xsl:text>      </xsl:text>
				<xsl:choose>
					<xsl:when test="@iscurrent[.='1']">
					<a class="clicked"><xsl:value-of select="@name"/></a>
					</xsl:when>
					<xsl:otherwise>
					<a class="disabled"><xsl:value-of select="@name"/></a>
					</xsl:otherwise>
				</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
		</xsl:when>
		<xsl:when test="@ischild[.='0']">
		<a>
			<xsl:attribute name="name">
			<xsl:value-of select="@id"/>
			</xsl:attribute>
		</a>
				<!--changes for expand - sign disabling-->
		<xsl:choose>
			<xsl:when test="@isSelect[.='1']">
			<xsl:choose>
				<xsl:when test="@level[.=$level] or  $level ='0'">
				<!--disabling (-) sign at this level-->
				<xsl:choose>
				<xsl:when test="$level ='0'">
				<a class="link"><xsl:attribute name="href">javascript:getchild('<xsl:value-of 		select="@id"/>','<xsl:value-of select="@parent"/>','<xsl:value-of select="@ischild"/>','<xsl:value-of 		select="@code"/>')</xsl:attribute>-</a><xsl:text>      </xsl:text>
				</xsl:when>
				<xsl:otherwise>
				<a class="link">--</a><xsl:text>      </xsl:text>
				</xsl:otherwise>
				</xsl:choose>

				<a class="select"><xsl:attribute name="href">javascript:getOrg('<xsl:value-of 						select="@id"/>','<xsl:value-of select="@name"/>',false)</xsl:attribute><xsl:value-of select="@name"/></a>
				</xsl:when>
				<xsl:otherwise>
				<a class="link"><xsl:attribute name="href">javascript:getchild('<xsl:value-of 		select="@id"/>','<xsl:value-of select="@parent"/>','<xsl:value-of select="@ischild"/>','<xsl:value-of 		select="@code"/>')</xsl:attribute>--</a><xsl:text>      </xsl:text>
				<xsl:choose>
					<xsl:when test="@iscurrent[.='1']">
					<a class="clicked"><xsl:value-of select="@name"/></a>
					</xsl:when>
					<xsl:otherwise>
					<a class="disabled"><xsl:value-of select="@name"/></a>
					</xsl:otherwise>
				</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
			
			</xsl:when>
			<xsl:otherwise>
			<xsl:choose>
				<xsl:when test="@level[.=$level] or  $level ='0'">
				<!--disabling (-) sign at this level-->
				<xsl:choose>
				<xsl:when test="$level ='0'">
				<a class="link"><xsl:attribute name="href">javascript:getchild('<xsl:value-of 		select="@id"/>','<xsl:value-of select="@parent"/>','<xsl:value-of select="@ischild"/>','<xsl:value-of 		select="@code"/>')</xsl:attribute>--</a><xsl:text>      </xsl:text>
				</xsl:when>
				<xsl:otherwise>
				<a class="link">--</a><xsl:text>      </xsl:text>
				</xsl:otherwise>
				</xsl:choose>
				<a class="select"><xsl:attribute name="href">javascript:getOrg('<xsl:value-of 						select="@id"/>','<xsl:value-of select="@name"/>',false)</xsl:attribute><xsl:value-of select="@name"/>				</a>
				</xsl:when>
				<xsl:otherwise>
				<a class="link"><xsl:attribute name="href">javascript:getchild('<xsl:value-of 		select="@id"/>','<xsl:value-of select="@parent"/>','<xsl:value-of select="@ischild"/>','<xsl:value-of 		select="@code"/>')</xsl:attribute>--</a><xsl:text>      </xsl:text>
				<xsl:choose>
					<xsl:when test="@iscurrent[.='1']">
					<a class="clicked"><xsl:value-of select="@name"/></a>
					</xsl:when>
					<xsl:otherwise>
					<a class="disabled"><xsl:value-of select="@name"/></a>
					</xsl:otherwise>
				</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
			<xsl:choose>
			<xsl:when test="@isSelect[.='1']">
			<xsl:choose>
				<xsl:when test="@level[.=$level] or  $level ='0'">
				<a class="select"><xsl:attribute name="href">javascript:getOrg('<xsl:value-of 						select="@id"/>','<xsl:value-of select="@name"/>',false)</xsl:attribute><xsl:value-of select="@name"/>				</a>
				</xsl:when>
				<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="@iscurrent[.='1']">
					<a class="clicked"><xsl:value-of select="@name"/></a>
					</xsl:when>
					<xsl:otherwise>
					<a class="disabled"><xsl:value-of select="@name"/></a>
					</xsl:otherwise>
				</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
			
			</xsl:when>
			<xsl:otherwise>
			<xsl:choose>
				<xsl:when test="@level[.=$level] or  $level ='0'">
				<a class="select"><xsl:attribute name="href">javascript:getOrg('<xsl:value-of 						select="@id"/>','<xsl:value-of select="@name"/>',false)</xsl:attribute><xsl:value-of select="@name"/>				</a>
				</xsl:when>
				<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="@iscurrent[.='1']">
					<a class="clicked"><xsl:value-of select="@name"/></a>
					</xsl:when>
					<xsl:otherwise>
					<a class="disabled"><xsl:value-of select="@name"/></a>
					</xsl:otherwise>
				</xsl:choose>

				</xsl:otherwise>
			</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
			</xsl:otherwise>
			
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="@child_name[.='client']">
				<xsl:apply-templates select="client">
				<xsl:with-param name="level">
					<xsl:value-of select="$level"/>
				</xsl:with-param>
				</xsl:apply-templates>
				</xsl:when>
		
				<xsl:when test="@child_name[.='company']">
				<xsl:apply-templates select="company">
				<xsl:with-param name="level">
					<xsl:value-of select="$level"/>
				</xsl:with-param>
				</xsl:apply-templates>
				</xsl:when>
				
				<xsl:when test="@child_name[.='operation']">
				<xsl:apply-templates select="operation">
				<xsl:with-param name="level">
					<xsl:value-of select="$level"/>
				</xsl:with-param>
				</xsl:apply-templates>
				</xsl:when>
				
				<xsl:when test="@child_name[.='region']">
				<xsl:apply-templates select="region">
				<xsl:with-param name="level">
					<xsl:value-of select="$level"/>
				</xsl:with-param>
				</xsl:apply-templates>
				</xsl:when>
				
				<xsl:when test="@child_name[.='division']">
				<xsl:apply-templates select="division">
				<xsl:with-param name="level">
					<xsl:value-of select="$level"/>
				</xsl:with-param>
				</xsl:apply-templates>
				</xsl:when>
				
				<xsl:when test="@child_name[.='location']">
				<xsl:apply-templates select="location">
				<xsl:with-param name="level">
					<xsl:value-of select="$level"/>
				</xsl:with-param>
				</xsl:apply-templates>
				</xsl:when>
				
				<xsl:when test="@child_name[.='facility']">
				<xsl:apply-templates select="facility">
				<xsl:with-param name="level">
					<xsl:value-of select="$level"/>
				</xsl:with-param>
				</xsl:apply-templates>
				</xsl:when>
				
				<xsl:when test="@child_name[.='department']">
				<xsl:apply-templates select="department">
				<xsl:with-param name="level">
					<xsl:value-of select="$level"/>
				</xsl:with-param>
				</xsl:apply-templates>
				</xsl:when>
				<xsl:otherwise>
				</xsl:otherwise>
			</xsl:choose></dl>
				<!--</blockquote>-->
			
			<!--</xsl:otherwise>
			
			</xsl:choose>-->
</xsl:template>

<xsl:template match="company">
	<xsl:param name="level"/>

		
		<xsl:call-template name="getchildren">
		<xsl:with-param name="level">
				<xsl:value-of select='$level'/>
		</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
<xsl:template match="client">
	<xsl:param name="level"/>

		
		<xsl:call-template name="getchildren">
		<xsl:with-param name="level">
				<xsl:value-of select='$level'/>
		</xsl:with-param>
		</xsl:call-template>
	</xsl:template>

	<xsl:template match="operation">
	<xsl:param name="level"/>

		
		<xsl:call-template name="getchildren">
		<xsl:with-param name="level">
				<xsl:value-of select='$level'/>
		</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
<xsl:template match="region">
<xsl:param name="level"/>

		
		<xsl:call-template name="getchildren">
		<xsl:with-param name="level">
				<xsl:value-of select='$level'/>
		</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
<xsl:template match="division">
<xsl:param name="level"/>

		
		<xsl:call-template name="getchildren">
		<xsl:with-param name="level">
				<xsl:value-of select='$level'/>
		</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
<xsl:template match="location">
<xsl:param name="level"/>

		
		<xsl:call-template name="getchildren">
		<xsl:with-param name="level">
				<xsl:value-of select='$level'/>
		</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
<xsl:template match="facility">
<xsl:param name="level"/>

		
		<xsl:call-template name="getchildren">
		<xsl:with-param name="level">
				<xsl:value-of select='$level'/>
		</xsl:with-param>
		</xsl:call-template>
	</xsl:template>

<xsl:template match="department">
<xsl:param name="level"/>

		
		<xsl:call-template name="getchildren">
		<xsl:with-param name="level">
				<xsl:value-of select='$level'/>
		</xsl:with-param>
		</xsl:call-template>
	
	</xsl:template>
<xsl:template name="combo_option">
	<xsl:param name="opt_name"/>
	<xsl:choose>
		
			<xsl:when  test="@optionname[.='level']">
			<xsl:for-each select="level">
			
				<option>
				<xsl:if test="@selected[.='1']">
					<xsl:attribute name="selected">
					selected
					</xsl:attribute>
				</xsl:if>
				<xsl:attribute name="value">
				<xsl:value-of select="@id"/>
				</xsl:attribute>
				<xsl:value-of select="@name"/>
				</option>
			</xsl:for-each>
			
			</xsl:when>
	</xsl:choose>
</xsl:template>

</xsl:stylesheet>