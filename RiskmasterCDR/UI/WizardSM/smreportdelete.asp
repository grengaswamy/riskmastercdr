<!-- #include file ="sessionInitialize.asp" -->
<%Public objSession
initSession objSession
%> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html><head><title></title>
<!--[Rajeev 12/03/2002 CSS Consolidation] link rel="stylesheet" href="sm.css" type="text/css"/-->
<link rel="stylesheet" href="RMNet.css" type="text/css"/>
<script language="JavaScript">
var ns, ie, ieversion;
var browserName = navigator.appName;                   // detect browser 
var browserVersion = navigator.appVersion;
if (browserName == "Netscape")
{
	ie=0;
	ns=1;
}
else		//Assume IE
{
	ieversion=browserVersion.substr(browserVersion.search("MSIE ")+5,1);
	ie=1;
	ns=0;
}

function DeleteReport()
{
	var obj=eval("document.frmData.reportid[0]");
	if(obj==null)
	{
		var obj=eval("document.frmData.reportid");
		if(obj==null)
		{
			alert("No reports available.");
			return false;
		}
		if(!document.frmData.reportid.checked)
		{
			alert("Please select report to delete.");
			return false;
		}
		if(!self.confirm("Report(s) will be permanently deleted. Are you sure you want to delete selected Report(s)?"))
			return false;
		document.frmData.submit();
		return true;
	}
	var id=0;
	for(var i=0;i<document.frmData.reportid.length;i++)
	{
		if(document.frmData.reportid[i].checked)
		{
			id=document.frmData.reportid[i].value;
			break;
		}
	}
	if(id==0)
	{
		alert("Please select report to delete.");
		return false;
	}

	if(!self.confirm("Report(s) will be permanently deleted. Are you sure you want to delete selected Report(s)?"))
		return false;
	document.frmData.submit();
}
function onPageLoaded()
{
	if (ie)
	{
		if ((eval("document.all.divForms")!=null) && (ieversion>=6))
			divForms.style.height=window.frames.frameElement.Height*0.65;
	}
	else
	{
		var o_divforms;
		o_divforms=document.getElementById("divForms");
		if (o_divforms!=null)
		{
			o_divforms.style.height=window.frames.innerHeight*0.58;
			o_divforms.style.width=window.frames.innerWidth*0.995;
		}
	}
}
</script>
</head>
<body class="Margin0" onload="onPageLoaded()">
<form name="frmData" method="post" action="smreportdelete.asp">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr><td class="ctrlgroup2" colspan="3">Delete Available Reports</td></tr>

<tr>
<td class="colheader3" width="1"></td>
<td class="colheader3" align="center">Report Name</td>
<td class="colheader3" align="center">Report Description</td>
</tr>
</TABLE>
	<DIV id="divForms" class="divScroll">
		<TABLE border="0" width="100%" height="0">
<%
dim r
dim sSQL
dim henv
dim db
dim rs
dim vName, vDesc, lReportID, lCount
dim i

Set r = CreateObject("DTGRocket")
henv = r.DB_InitEnvironment()
db = r.DB_OpenDatabase(henv, Application("SM_DSN"), 0)


If LCase(Request.ServerVariables("REQUEST_METHOD"))="post" Then
	' Delete report
	If Request("reportid")<>"" Then
		Dim bDelAllReports
		bDelAllReports=False
		' MITS 9999 Ankur 10/15/2007
		' Admin users can delete all reports. Module security check is redundant
		If objSessionStr(SESSION_ADMINRIGHTS)="1" And Request("deleteall")="1" Then
			bDelAllReports=True
		End If
		
		If bDelAllReports Then
			sSQL="DELETE FROM SM_REPORTS_PERM WHERE REPORT_ID IN (" & Request("reportid") & ")"
			r.DB_SQLExecute db,sSQL
			lCount=0
		Else
			sSQL="DELETE FROM SM_REPORTS_PERM WHERE REPORT_ID IN (" & Request("reportid") & ") AND USER_ID=" & objSessionStr(SESSION_USERID)
			r.DB_SQLExecute db,sSQL
			sSQL="SELECT COUNT(*) FROM SM_REPORTS_PERM WHERE REPORT_ID IN (" & Request("reportid") & ")"
			rs = r.DB_CreateRecordset(db, sSQL, 3, 0)
			r.DB_GetData rs, 1, lCount
			r.DB_CloseRecordset CInt(rs), 2
		End If
		
		If lCount=0 Then
			sSQL="DELETE FROM SM_REPORTS WHERE REPORT_ID IN (" & Request("reportid") & ")"
			r.DB_SQLExecute db,sSQL
			sSQL="DELETE FROM SM_SCHEDULE WHERE REPORT_ID IN (" & Request("reportid") & ")"
			r.DB_SQLExecute db,sSQL
		End If
	End If
End If

i = 0

sSQL = "SELECT * FROM SM_REPORTS,SM_REPORTS_PERM WHERE "
sSQL = sSQL & "SM_REPORTS_PERM.USER_ID = " & objSessionStr("UserID") & " AND SM_REPORTS.REPORT_ID = SM_REPORTS_PERM.REPORT_ID"
sSQL = sSQL & " ORDER BY SM_REPORTS.REPORT_NAME"

rs = r.DB_CreateRecordset(db, sSQL, 3, 0)
while not r.DB_EOF(rs)
	r.DB_GetData rs, "REPORT_ID", lReportID
	r.DB_GetData rs, "REPORT_NAME", vName
	r.DB_GetData rs, "REPORT_DESC", vDesc
%>

<% if i mod 2 = 0 then %>
<tr class="rowlight1">
<% else %>
<tr class="rowdark1">
<% end if %>
<td width="1" nowrap=""><input type="checkbox" value="<%=lReportId%>" name="reportid"/></input></td>
<td width="30%">
<%If "" & vName="" Then Response.Write "Report Nr. " & lReportID Else Response.Write vName %>
</td>
<td><% = vDesc & "" %> </td>
</tr>

<%
	i = i + 1
	
	r.DB_MoveNext rs
wend
r.DB_CloseRecordset CInt(rs), 2
r.DB_CloseDatabase CInt(db)

r.DB_FreeEnvironment CLng(henv)

set r = Nothing
%>
</table>
</div>
<table>
<tr><td colspan="3" align="left" nowrap="1">
<!--MITS 9999 Ankur 10/15/2007: Admin users can delete all reports. Module security check is redundant-->
<!--Subhendu 12/17/2010 MITs 23182 : Removed the property checked="1" from Delete for all users checkbox -->
<%If objSessionStr(SESSION_ADMINRIGHTS)="1" Then%>
	<input type="checkbox" name="deleteall" value="1" /> Delete for all users &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<%End If%>
<input type="button" class="button" value="Delete Selected Report(s)" onClick="DeleteReport()" />
</td>
</table>
<div class="small">
This list shows the reports that are available for deletion.<br />
To delete one or more available reports, place a checkmark next to the report and click the "Delete Selected Report(s)" button.
<br>Note: This will not affect other users who also have access to a report.
<br>
*** For information on extended reporting features and report troubleshooting please see the <a href="smfaq.asp">Frequently Asked Questions (FAQ)</a>.
</div>
</form>
</body>
</html>
