<!-- #include file ="appconstantsconfig.inc" -->
<%Option Explicit%>
<HTML>
<HEAD>	
<link rel="stylesheet" href="RMNet.css" type="text/css"/>
<TITLE>RiskMaster Report Problem</TITLE>
</HEAD>
<BODY>
<h3>RiskMaster Report Problem</h3>

<P>The report requested could not be found.  There are two likely reasons for this:
<ol>
<li>An un-recoverable error was encountered while constructing the report output.  Please try running the report again. If you get this error again <%=ERR_CONTACT%></li>
<li>A common problem is that the data source you are currently using lacks a valid document path.  An administrator can verify this using the Security Management System.  Please verify that this data source has a valid document path before contacting technical support. </li>
</ol>
</P>
<form><input type="button" class="button" value="Back" onclick="if(window.opener != null &&window.opener!=self) self.close();else history.back();" /></form>
</BODY>
</HTML>
