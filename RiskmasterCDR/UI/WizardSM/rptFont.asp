<html>
	<head>
		<link rel="stylesheet" href="RMNet.css" type="text/css" />
		<title>Report Font Properties</title>

		<script language="JavaScript">
		
			function reportFontSubmit()
			{
				if (window.document.frmRptFont.select1.selectedIndex == -1)
					window.document.frmRptFont.select1.selectedIndex = 0;
				
				window.opener.frmData.hdRptFont.value=window.document.frmRptFont.select1.options[window.document.frmRptFont.select1.selectedIndex].text;
				
				if (window.document.frmRptFont.select2.options[window.document.frmRptFont.select2.selectedIndex].text=="Regular") 
				{
					window.opener.frmData.hdRptStyle1.value="0"
					window.opener.frmData.hdRptStyle2.value="0"
				}
				if (window.document.frmRptFont.select2.options[window.document.frmRptFont.select2.selectedIndex].text=="Italic")
				{
					window.opener.frmData.hdRptStyle1.value="0"
					window.opener.frmData.hdRptStyle2.value="1"
				}
				if (window.document.frmRptFont.select2.options[window.document.frmRptFont.select2.selectedIndex].text=="Bold") 
				{
					window.opener.frmData.hdRptStyle1.value="1"
					window.opener.frmData.hdRptStyle2.value="0"
				}
				if (window.document.frmRptFont.select2.options[window.document.frmRptFont.select2.selectedIndex].text=="Bold Italic") 
				{
					window.opener.frmData.hdRptStyle1.value="1"
					window.opener.frmData.hdRptStyle2.value="1"
				}
			
				window.opener.frmData.hdRptSize.value=window.document.frmRptFont.select3.options[window.document.frmRptFont.select3.selectedIndex].text + ".000000";
				window.close();
			}

			function body_onload()
			{
				for(i=0;i<document.getElementById("select1").length;i++)
				{
					if(window.opener.frmData.hdRptFont.value=="")
					{
						document.getElementById("select1").options(1).selected = true;
					}
					else if(document.getElementById("select1").options(i).text == window.opener.frmData.hdRptFont.value)
					{
						document.getElementById("select1").options(i).selected = true;
					}
				}
					
				if (window.document.frmRptFont.select1.selectedIndex == -1)
					window.document.frmRptFont.select1.selectedIndex = 0;
					
				for(i=0;i<document.getElementById("select3").length;i++)
				{
					if(window.opener.frmData.hdRptSize.value=="")
						document.getElementById("select3").options(1).selected = true;
					else if(document.getElementById("select3").options(i).text == window.opener.frmData.hdRptSize.value.substring(0,window.opener.frmData.hdRptSize.value.indexOf(".")))
						document.getElementById("select3").options(i).selected = true;
				}
				
				if(window.opener.frmData.hdRptStyle1.value=="" && window.opener.frmData.hdRptStyle2.value=="")
					document.getElementById("select2").options(1).selected = true;
				else if(window.opener.frmData.hdRptStyle1.value=="0" && window.opener.frmData.hdRptStyle2.value=="0")
					document.getElementById("select2").options(0).selected = true;
				else if(window.opener.frmData.hdRptStyle1.value=="0" && window.opener.frmData.hdRptStyle2.value=="1")
					document.getElementById("select2").options(1).selected = true;
				else if(window.opener.frmData.hdRptStyle1.value=="1" && window.opener.frmData.hdRptStyle2.value=="0")
					document.getElementById("select2").options(2).selected = true;
				else if(window.opener.frmData.hdRptStyle1.value=="1" && window.opener.frmData.hdRptStyle2.value=="1")
					document.getElementById("select2").options(3).selected = true;
			}

			function updateSerialNum()
			{
				window.opener.checkSerialNum(0);
			} 

		</script>
	</head>
	
	<body class="8pt" onblur1="window.focus()" onload="javascript: body_onload();">
	<form name="frmRptFont" action="post">
		<table class="formsubtitle" border="0" width="100%">
			<tr>
				<td class="ctrlgroup" >Report Font Properties</td>
			</tr>
		</table>

		<table border="0" width="80%" align="center">
			<tr>
				<td align="middle" style="width:30%">
					Font
				</td>
				<td align="middle" style="width:30%">
					Font Style
				</td>
				<td align="middle" style="width:30%">
					Size
				</td>
			</tr>
			<tr>
				<td align="middle">
					<SELECT id=select1 size=6 name=select1 onChange="updateSerialNum();"> 
						<OPTION>Courier New</OPTION>
						<OPTION>Arial</OPTION>
					</SELECT>
				</td>
				<td align="middle">
					<SELECT id=select2 size=6 name=select2 onChange="updateSerialNum();"> 
						<OPTION>Regular</OPTION>
						<OPTION>Italic</OPTION>
						<OPTION>Bold</OPTION>
						<OPTION>Bold Italic</OPTION>
					</SELECT>
				</td>
				<td align="middle">
					<SELECT id=select3 size=8 name=select3 style="width:30px" onChange="updateSerialNum();"> 
						<OPTION>8</OPTION>
						<OPTION>9</OPTION>
						<OPTION>10</OPTION>
						<OPTION>11</OPTION>
						<OPTION>12</OPTION>
						<OPTION>14</OPTION>
						<OPTION>16</OPTION>
						<OPTION>18</OPTION>
					</SELECT>
				</td>
			</tr>
		</table>
		<br>
		<hr>
		<table width="100%" border="0">
			<tr>
				<td width="20%">&nbsp;</td>
				<td width="60%" align="center">
					<input type="button" class="button" style="width:20%" name="up" value="OK" onClick="javascript:reportFontSubmit()">
					<input type="button" class="button" style="width:20%" name="up" value="Cancel" onClick="javascript:window.close()">
				</td>
				<td width="20%">&nbsp;</td>
			</tr>
		</table>
	</form>
	</body>
</HTML>
