<%'Option Explicit%>
<!-- #include file ="session.inc" -->
<!-- #include file ="generic.asp" -->

<html>
	<head>
		<link rel="stylesheet" href="RMNet.css" type="text/css"/>
<%

Dim objData, pageError
Set objData = Nothing

On Error Resume Next
Set objData = Server.CreateObject("InitSMList.CDataManager")
On Error Goto 0

If objData Is Nothing Then
	Call ShowError(pageError, "Call to CDataManager class failed.", True)
End If

objData.m_RMConnectStr = Application("SM_DSN")

Dim arrDateFormulae()
objData.GetDateFormulae arrDateFormulae

Set objData = Nothing

Dim sDateFormula, m
Err.Clear()
On Error Resume Next
m = -1
m = UBound(arrDateFormulae)
If Err.number <> 9 And m <> "" And m >= 0 Then
	For i = 0 To UBound(arrDateFormulae) 
		If i = 0 Then 
			sDateFormula = arrDateFormulae(0) 
		Else 
			sDateFormula = sDateFormula & "^^^^^" & arrDateFormulae(i) 
		End If 
	Next
End If
On Error Goto 0

%>
<html>
	<head>
		<link rel="stylesheet" href="RMNet.css" type="text/css"/>
		<title>SMD - [Relative Date]</title>
		<script language="JavaScript" SRC="SMD.js"></script>
		<script>
			var arrRelFormulaName = new Array;
			var arrRelFormulaValue = new Array;
			var arrRelDate;
			var bMode;
			bMode = '';
			var bChange = 0;
			function window_onload()
			{
				arrRelDate = window.document.frmData.hdRelativeDate.value.split("^^^^^");	
				if (arrRelDate.length > 0)
				{
					for(i=0;i<arrRelDate.length;i++)
					{
						var arrPos = FindCharacter(arrRelDate[i], '|', 2).split(",");
						var pos1 = arrPos[0];
						var pos2 = arrPos[1];
						arrRelFormulaName[i] = arrRelDate[i].substr(pos1,pos2-pos1-1);
						arrRelFormulaValue[i] = arrRelDate[i].substr(pos2,arrRelDate[i].length-pos2);
					}
					document.frmData.txtRelativeDate.value = arrRelFormulaName[0];
					document.frmData.txtRelativeDateFormula.value = arrRelFormulaValue[0];
				}
			}
		
			function ok_click()
			{
				var sRelativeDate;
				var bFound = 0;
				sRelativeDate = document.frmData.txtRelativeDate.value;
				sRelativeDateFormula = document.frmData.txtRelativeDateFormula.value;
				
				//-- Validate the formula
				if ((sRelativeDateFormula.substr(0,1) != "+" && sRelativeDateFormula.substr(0,1) != "-") ||(sRelativeDateFormula.substr(1,2) < 0 || sRelativeDateFormula.substr(1,2) > 9) || (sRelativeDateFormula.substr(2,3) != "M" && sRelativeDateFormula.substr(2,3) != "Y" && sRelativeDateFormula.substr(2,3) != "D"))
				{
					alert('Bad Relative Date Formula');
					return false;
				}
				
				if (bMode == 'ADD')
					add_item(sRelativeDate);
			
				window.opener.window.document.frmData.hdSubmit.value = "RELATIVEDATE"
				window.opener.window.document.frmData.hdRelDate.value = window.document.frmData.txtRelativeDate.value;
				window.opener.window.document.frmData.hdRelDateFormula.value = window.document.frmData.txtRelativeDateFormula.value;
				window.opener.window.document.frmData.hdRelMode.value = bMode;
			
				if (document.frmData.cboRelativeDate.length > 0)
					var sRelID = arrRelDate[document.frmData.cboRelativeDate.selectedIndex].substring(0,arrRelDate[document.frmData.cboRelativeDate.selectedIndex].indexOf("|"));
			
				window.opener.window.document.frmData.hdRelID.value = sRelID;
				window.opener.tabNavigation(window.document.frmData.hdTabId.value,window.document.frmData.hdTabId.value);
				window.close();		
			}
				
			function changeRelativeDate()
			{
				document.frmData.txtRelativeDate.value = arrRelFormulaName[document.frmData.cboRelativeDate.selectedIndex];
				document.frmData.txtRelativeDateFormula.value = arrRelFormulaValue[document.frmData.cboRelativeDate.selectedIndex];
				document.frmData.btnOK.disabled = false;
				bMode = 'UPDATE';
			}
		
			function add_item(txt)
			{
				var sNewItem;
				sNewItem = new Option(txt);
				if (navigator.appName == "Netscape")
					window.document.frmData.cboRelativeDate.add(sNewItem, null);
				else
					window.document.frmData.cboRelativeDate.add(sNewItem);
				
			}
		
			function changeRelativeDateName()
			{
				if (document.frmData.cboRelativeDate.length > 0)
				{
					if (document.frmData.txtRelativeDate.value != document.frmData.cboRelativeDate.options[document.frmData.cboRelativeDate.selectedIndex].text)
					{
						document.frmData.txtRelativeDateFormula.value = '';
						document.frmData.btnOK.disabled = true;
						bChange = 1;
						bMode = 'ADD';
					} 
				}
				else
					bMode = 'ADD';
			}
		
			function changeRelativeDateFormula()
			{
				if (document.frmData.cboRelativeDate.length > 0)
				{
					if (document.frmData.txtRelativeDateFormula.value != '')
						document.frmData.btnOK.disabled = false;
					if (bChange == 0)
						bMode = 'UPDATE';
				}
				else
						bMode = 'ADD';
			}
		
		</script>		
	</head>

	<body onLoad="window_onload();">
	<form name="frmData" topmargin="0" leftmargin="0">
	
		<table class="formsubtitle" border="0" width="100%" align1="center">
			<tr>
				<td class="ctrlgroup">Add/Edit Relative Date :</td>
			</tr>
		</table>
			
		<br>

		<table width="100%" border="0">
			<tr>
				<td width="100%" colspan=2 align="left">Select or Enter New Relative Date Description</td>

			</tr>
			<tr>
				<td align="left">
					<input size="10" name="txtRelativeDate" onKeyUp="changeRelativeDateName();">
					<select name="cboRelativeDate" style="WIDTH: 35%" height1="1" onChange="changeRelativeDate();">
					<% 
						Dim arrFormula
						If Err.number <> 9 And m <> "" And m >= 0 Then
							For i = 0 to UBound(arrDateFormulae)
								arrFormula = Split(arrDateFormulae(i), "|")
								If i=0 then 
									Response.Write "<option selected>" & arrFormula(1) & "</option>"
								Else
									Response.Write "<option>" & arrFormula(1) & "</option>"
								End If
							Next
						End If
					%>
					</select></td>
				<td><INPUT class=button style="WIDTH: 99%" type=button value="OK" name="btnOK" onClick="ok_click();"></td>
			</tr>
			<tr>
				<td align="left" width1="50%">Relative Date Formula</td>
				<td width1="50%" ><input type="button" class="button" style="WIDTH: 99%" name="btnCancel" value="Cancel" onClick="window.close()"></td>
			</tr>
			<tr>
				<td align="left" colspan="2" width1="50%">
					<input size="25" name="txtRelativeDateFormula" onKeyUp="changeRelativeDateFormula();">
				</td>
			</tr>
			<tr>
				<td align="left" colspan="2" width1="50%">
					(+,-)&nbsp;&nbsp;(0,1....9)&nbsp;&nbsp;(Y,M,D)&nbsp;&nbsp;&nbsp;i.e.&nbsp;-7D
				</td>
			</tr>
		</table>
		<input type="hidden" name="hdRelativeDate" value="<%=sDateFormula%>"> 
		<input type="hidden" name="hdTabId" value="<%=Request.QueryString("TABID")%>"> 
	</form>
	</body>
</html>