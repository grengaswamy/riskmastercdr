<!-- #include file ="session.inc" -->
<!-- #include file ="generic.asp" -->

<html>
	<head>
		<link rel="stylesheet" href="RMNet.css" type="text/css"/>
		<title>SMD - [Report Column Properties]</title>
<%

'''Dim bSubmitted
'''bSubmitted = Request.QueryString("d")
'''Response.Write bSubmitted
'''If Not bSubmitted Then
'''	Call ShowError(pageError, "Page could not be displayed", true)
'''End If

Dim checkStack,DuplicatePrint,IncludeField,SubTotal,RollUp,ColBreak0,ColBreak1,ColBreak2,ColBreak3,AggNone,AggCount,AggCountUnique,AggSum,AggAvg,AggMin,AggMax,ColSort0,ColSort1,ColSort2,DefaultLines,MaxLines,FiscYear,TopCode,Organization,LineOfExport
Dim DataType,i,org,lob,txtMaxLines,IncludeSelect, m

Dim xmlDoc, pageError
Set xmlDoc = Nothing
On Error Resume Next
Set xmlDoc = Server.CreateObject("Msxml2.DOMDocument")
On Error Goto 0

If xmlDoc Is Nothing Then
	Call ShowError(pageError, "XML Parser is not available.", True)
End If
xmlDoc.async = false

Dim arrP, arrTemp, sFieldName
'-- Rajeev -- 03/09/2004
'''arrP = Split(objSessionStr("ArrayP"), ",")
arrP = Split(objSessionStr("ArrayP"), "*!^")

arrTemp = Split(arrP(Request.QueryString("b")), ":")

sFieldName = Trim(arrTemp(1))

Dim myXML
myXML = Split(objSessionStr("ArrayPXML"), "</column>,")

Err.Clear()
On Error Resume Next
m = -1
m = UBound(myXML)
If Err.number <> 9 And m <> "" And m >= 0 Then
	For iCounter = 0 To UBound(myXML) - 1
		myXML(iCounter) = myXML(iCounter) & "</column>"
	Next
End If
On Error Goto 0

xmlDoc.loadXML myXML(Request.QueryString("b"))

tableId = xmlDoc.getElementsByTagName("column").item(0).attributes.item(1).value
fieldId = xmlDoc.getElementsByTagName("column").item(0).attributes.item(2).value

Dim objUtil

If Not GetValidObject("InitSMList.CUtility", objUtil) Then
	Call ShowError(pageError, "Call to CUtility class failed.", True)
End If    

Dim objFiscal
    
objUtil.GetFiscalYears objSessionStr(SESSION_DSN), objFiscal, False, True

DataType = Request.QueryString("c")

For i=0 To CInt(xmlDoc.getElementsByTagName("column").item(0).attributes.length)-1 

	If xmlDoc.getElementsByTagName("column").item(0).attributes.item(i).name="sort" Then
		If xmlDoc.getElementsByTagName("column").item(0).attributes.item(i).text="asc" then
			ColSort1 = "checked"
		Else xmlDoc.getElementsByTagName("column").item(0).attributes.item(i).text="desc" 
			ColSort2 = "checked"
		End If
	Else
		ColSort0 = "checked"
	End If
	'------------^^Aashish---------05/25/2003---- Code for optColBreak group------------------------
	'Manish C Jha 08/10/2009 - MITS 15151
	'Attribute's value should be compared instead of attribute's name. Reason behind: page break and simple break need to identified
	'separately. following are the attribute strings: Page Break: break="page" and simple break: break="break"
	If xmlDoc.getElementsByTagName("column").item(0).attributes.item(i).value="break" Then
		ColBreak1 = "checked"
	ElseIf xmlDoc.getElementsByTagName("column").item(0).attributes.item(i).value="page" Then
		ColBreak2 = "checked"
	ElseIf xmlDoc.getElementsByTagName("column").item(0).attributes.item(i).value="banner" Then
		ColBreak3 = "checked"
	Else
		ColBreak0 = "checked"
	End If
	
	If xmlDoc.getElementsByTagName("column").item(0).attributes.item(i).name="bannerstack" Then
		checkStack = "checked"
		ColBreak3 = "checked"
	End If 
	
	If xmlDoc.getElementsByTagName("column").item(0).attributes.item(i).name="stack" Then
		checkStack = "checked"
	End If 
	
	If xmlDoc.getElementsByTagName("column").item(0).attributes.item(i).name="noprint" Then
		IncludeField = ""
		IncludeSelect = "1"
	End If
	
	If xmlDoc.getElementsByTagName("column").item(0).attributes.item(i).name="subtotal" Then
		SubTotal = "checked"
	End If 
		
	If xmlDoc.getElementsByTagName("column").item(0).attributes.item(i).name="rollup" Then
		RollUp = "checked"
	End If 
	
	If xmlDoc.getElementsByTagName("column").item(0).attributes.item(i).name="duplicatecolumns" Then
		DuplicatePrint = "checked"
	End If

	If xmlDoc.getElementsByTagName("column").item(0).attributes.item(i).name="aggcount" Then
		AggCount = "checked"
	ElseIf xmlDoc.getElementsByTagName("column").item(0).attributes.item(i).name="agguniquecount" Then
		AggCountUnique = "checked"
	End If

	If xmlDoc.getElementsByTagName("column").item(0).attributes.item(i).name="maxlinesinrow" Then
		txtMaxLines = xmlDoc.getElementsByTagName("column").item(0).attributes.item(i).value
	End If

	'-------------------------Handling DataType 1----------------------------------------------------	
	If xmlDoc.getElementsByTagName("column").item(0).attributes.item(i).name="aggcount" Then
		AggCount = "checked"
	ElseIf xmlDoc.getElementsByTagName("column").item(0).attributes.item(i).name="agguniquecount" Then
		AggCountUnique = "checked"
	End If
	
	If xmlDoc.getElementsByTagName("column").item(0).attributes.item(i).name="aggsum" Then
		AggSum = "checked"
	End If
	
	If xmlDoc.getElementsByTagName("column").item(0).attributes.item(i).name="aggaverage" Then
		AggAvg = "checked"
	End If
	
	If xmlDoc.getElementsByTagName("column").item(0).attributes.item(i).name="aggmax" Then
		AggMax = "checked"
	End If
	
	If xmlDoc.getElementsByTagName("column").item(0).attributes.item(i).name="aggmin" Then
		AggMin = "checked"
	End If
	
	'----------------------------Handling DataType 2-------------------------------------------------
	If xmlDoc.getElementsByTagName("column").item(0).attributes.item(i).name="fiscalyear" Then
		FiscYear = "checked"
	End If

	If xmlDoc.getElementsByTagName("column").item(0).attributes.item(i).name="organization" Then
		org = xmlDoc.getElementsByTagName("column").item(0).attributes.item(i).value
	Else
		Organization = "Disabled"
	End If

	If xmlDoc.getElementsByTagName("column").item(0).attributes.item(i).name="lineofbusiness" Then
		lob = xmlDoc.getElementsByTagName("column").item(0).attributes.item(i).value
	Else 
		LineOfExport = "Disabled"
	End If

Next

TopCode = "Disabled"

If IncludeSelect <> "1" then
	IncludeField= "checked"
End If
		
Select Case DataType
	Case "0"
			If AggCount = "" And AggCountUnique = "" Then
				AggNone = "checked"
				AggCount = "disabled"
				AggCountUnique = "disabled"
			End If
		
			AggSum = "Disabled"
			AggAvg = "Disabled"
			AggMin = "Disabled"
			AggMax = "Disabled"
			DefaultLines = "Disabled"
			MaxLines = "Disabled"
			FiscYear = "Disabled"
			TopCode = "Disabled"
			
			Organization = "Disabled"
			LineOfExport = "Disabled"
		
	Case "1"
			If AggCount = "" And AggCountUnique = "" And AggSum = "" And AggAvg = "" And AggMax = "" And AggMin = "" Then
				AggNone = "checked"
				AggCount = "disabled"
				AggCountUnique = "disabled"
				AggSum = "disabled"
				AggAvg = "disabled"
				AggMax = "disabled"
				AggMin = "disabled"
			End If
			DefaultLines = "Disabled"
			MaxLines = "Disabled"
			FiscYear = "Disabled"
			TopCode = "Disabled"
			Organization = "Disabled"
			LineOfExport = "Disabled"
		
	Case "2"
			If AggCount = "" And AggCountUnique = "" Then
				AggNone = "checked"
				AggCount = "disabled"
				AggCountUnique = "disabled"
			End If
		
			AggSum = "disabled"
			AggAvg = "disabled"
			AggMax = "disabled"
			AggMin = "disabled"
			DefaultLines = "Disabled"
			MaxLines = "Disabled"
	
	Case "3"
			If AggCount = "" And AggCountUnique = "" Then
				AggNone = "checked"
				AggCount = "disabled"
				AggCountUnique = "disabled"
			End If
		
			AggSum = "Disabled"
			AggAvg = "Disabled"
			AggMin = "Disabled"
			AggMax = "Disabled"
			DefaultLines = "Disabled"
			MaxLines = "Disabled"
			FiscYear = "Disabled"
			TopCode = "Disabled"
			Organization = "Disabled"
			LineOfExport = "Disabled"
				
	Case "4"
			If AggCount = "" And AggCountUnique = "" Then
				AggNone = "checked"
				AggCount = "disabled"
				AggCountUnique = "disabled"
			End If
		
			AggSum = "Disabled"
			AggAvg = "Disabled"
			AggMin = "Disabled"
			AggMax = "Disabled"
			DefaultLines = "Disabled"
			MaxLines = "Disabled"
			FiscYear = "Disabled"
			TopCode = "Disabled"
			Organization = "Disabled"
			LineOfExport = "Disabled"
		
	Case "5"
			If AggCount = "" And AggCountUnique = "" Then
				AggNone = "checked"
				AggCount = "disabled"
				AggCountUnique = "disabled"
			End If
		
			AggSum = "Disabled"
			AggAvg = "Disabled"
			AggMin = "Disabled"
			AggMax = "Disabled"
			DefaultLines = "Disabled"
			MaxLines = "Disabled"
			FiscYear = "Disabled"
			TopCode = "Disabled"
			Organization = "Disabled"
			LineOfExport = "Disabled"

	Case "6"
			If AggCount = "" And AggCountUnique = "" Then
				AggNone = "checked"
				AggCount = "disabled"
				AggCountUnique = "disabled"
			End If
		
			AggSum = "Disabled"
			AggAvg = "Disabled"
			AggMin = "Disabled"
			AggMax = "Disabled"
			DefaultLines = "Disabled"
			MaxLines = "Disabled"
			FiscYear = "Disabled"
			TopCode = "Disabled"
			Organization = "Disabled"
			LineOfExport = "Disabled"

	Case "7"
			If AggCount = "" And AggCountUnique = "" Then
				AggNone = "checked"
				AggCount = "disabled"
				AggCountUnique = "disabled"
			End If
		
			AggSum = "Disabled"
			AggAvg = "Disabled"
			AggMin = "Disabled"
			AggMax = "Disabled"
			DefaultLines = "Disabled"
			MaxLines = "Disabled"
			FiscYear = "Disabled"
			TopCode = "Disabled"
			Organization = "Disabled"
			LineOfExport = "Disabled"

	Case "8"
			SubTotal="Disabled"
			ColBreak0="Disabled"
			ColBreak1="Disabled"
			ColBreak2="Disabled"
			
			ColSort0="Disabled"
			ColSort1="Disabled"
			ColSort2="Disabled"

			AggCountUnique = "Disabled"
			AggSum = "Disabled"
			AggAvg = "Disabled"
			AggMin = "Disabled"
			AggMax = "Disabled"
			FiscYear = "Disabled"
			Organization = "Disabled"
			LineOfExport = "Disabled"
			If AggCount = ""  Then
				AggNone = "checked"
				AggCount = "disabled"
			End If
			If txtMaxLines = "" Then
				DefaultLines = "checked"
				MaxLines= "Disabled"
			End If
	Case "9"
			If AggCount = "" And AggCountUnique = "" And AggSum = "" And AggAvg = "" And AggMax = "" And AggMin = "" Then
				AggNone = "checked"
				AggCount = "disabled"
				AggCountUnique = "disabled"
				AggSum = "disabled"
				AggAvg = "disabled"
				AggMax = "disabled"
				AggMin = "disabled"
			End If
			DefaultLines = "Disabled"
			MaxLines = "Disabled"
			FiscYear = "Disabled"
			TopCode = "Disabled"
			Organization = "Disabled"
			LineOfExport = "Disabled"
		
	Case "10"
			If AggCount = "" And AggCountUnique = "" Then
				AggNone = "checked"
				AggCount = "disabled"
				AggCountUnique = "disabled"
			End If
		
			AggSum = "Disabled"
			AggAvg = "Disabled"
			AggMin = "Disabled"
			AggMax = "Disabled"
			DefaultLines = "Disabled"
			MaxLines = "Disabled"
			FiscYear = "Disabled"
			TopCode = "Disabled"
			Organization = "Disabled"
			LineOfExport = "Disabled"
	
	Case "11"
			If AggCount = "" And AggCountUnique = "" And AggSum = "" And AggAvg = "" And AggMax = "" And AggMin = "" Then
				AggNone = "checked"
				AggCount = "disabled"
				AggCountUnique = "disabled"
				AggSum = "disabled"
				AggAvg = "disabled"
				AggMax = "disabled"
				AggMin = "disabled"
			End If
			DefaultLines = "Disabled"
			MaxLines = "Disabled"
			FiscYear = "Disabled"
			TopCode = "Disabled"
			Organization = "Disabled"
			LineOfExport = "Disabled"

End Select 
%>
<html>
	<head>
		<link rel="stylesheet" href="RMNet.css" type="text/css"/>
		<title>SMD - [Report Column Properties]</title>
		
		<script>
		
			function changeColAggCheckBox(pIndex)
			{			
				if(document.frmData.chkColAggCount.checked && pIndex == 0)
					window.opener.window.document.frmData.hdAggCount.value = "1";
				else if(!document.frmData.chkColAggCount.checked && pIndex == 0)
					window.opener.window.document.frmData.hdAggCount.value = "";
				
				if(document.frmData.chkColAggUnique.checked && pIndex == 1)
					window.opener.window.document.frmData.hdAggUniqueCount.value = "1";
				else if(!document.frmData.chkColAggUnique.checked && pIndex == 1)
					window.opener.window.document.frmData.hdAggUniqueCount.value = "";
					
				if(document.frmData.chkColAggSum.checked && pIndex == 2)
					window.opener.window.document.frmData.hdAggSum.value = "1";
				else if(!document.frmData.chkColAggSum.checked && pIndex == 2)
					window.opener.window.document.frmData.hdAggSum.value = "";
					
				if(document.frmData.chkColAggAvg.checked && pIndex == 3)
					window.opener.window.document.frmData.hdAggAvg.value = "1";
				else if(!document.frmData.chkColAggAvg.checked && pIndex == 3)
					window.opener.window.document.frmData.hdAggAvg.value = "";
					
				if(document.frmData.chkColAggMin.checked && pIndex == 4)
					window.opener.window.document.frmData.hdAggMin.value = "1";
				else if(!document.frmData.chkColAggMin.checked && pIndex == 4)
					window.opener.window.document.frmData.hdAggMin.value = "";
					
				if(document.frmData.chkColAggMax.checked && pIndex == 5)
					window.opener.window.document.frmData.hdAggMax.value = "1";
				else if(!document.frmData.chkColAggMax.checked && pIndex == 5)
					window.opener.window.document.frmData.hdAggMax.value = "";
			}
					
			function dialogModal()
			{
				var Nav4 = ((navigator.appName == "Netscape") && (parseInt(navigator.appVersion) >= 4));
				
				if (Nav4)
					window.focus();
				
				return true;
			}
			//^ Rajeev 05/20/2003--
			function PropertySubmit()
			{
				//Stack ChkBox
				if(window.document.frmData.hdDataType.value == "0" || window.document.frmData.hdDataType.value == "3" || window.document.frmData.hdDataType.value == "4" || window.document.frmData.hdDataType.value == "5" || window.document.frmData.hdDataType.value == "6" || window.document.frmData.hdDataType.value == "7" || window.document.frmData.hdDataType.value == "10")// || window.document.frmData.hdDataType.value == "12" || window.document.frmData.hdDataType.value == "13" )
					{
						if(window.document.frmData.chkStack.checked  == true && (window.document.frmData.optColBreak[1].checked == false && window.document.frmData.optColBreak[2].checked == false && window.document.frmData.optColBreak[3].checked ==false ))
						{
							window.opener.window.document.frmData.hdIsStackSelected.value = 1;
							if (window.document.frmData.optSort[0].checked == true)
							{
							window.opener.window.document.frmData.hdColumnSort.value = 1;
							}
						}
						else if(window.document.frmData.chkStack.checked  == true && window.document.frmData.optColBreak[3].checked == true )
						{
							window.opener.window.document.frmData.hdBannerStack.value = 1;
							if (window.document.frmData.optSort[0].checked == true)
							{
							window.opener.window.document.frmData.hdColumnSort.value = 1;
							}
							
						}
						
						// Include chkbox
						if	(window.document.frmData.chkInclude.checked  == false)
						{
							window.opener.window.document.frmData.hdIsIncludeSelected.value = 1;
						}
						
						//Print chkbox
						if	(window.document.frmData.chkSubTotal.checked  == true)
						{
							window.opener.window.document.frmData.hdIsPrintSelected.value = 1;
							
							if (window.document.frmData.optSort[0].checked == true)
							{
							window.opener.window.document.frmData.hdColumnSort.value = 1;
							}
							if (window.document.frmData.optColBreak[0].checked == true)
							{
							window.opener.window.document.frmData.hdColBreak.value = 1;
							}
						}
						
						//Roll up chkbox
						if	(window.document.frmData.chkRollUp.checked  == true)
						{
							window.opener.window.document.frmData.hdIsRollSelected.value = 1;
						}
						
						//column break option--------------------------------------------------------------------------------
				
						if	(window.document.frmData.optColBreak[1].checked == true)
						{
							window.opener.window.document.frmData.hdColBreak.value = 1;
							window.opener.window.document.frmData.hdColumnSort.value = 1;
						}
						if	(window.document.frmData.optColBreak[2].checked == true)
						{
							window.opener.window.document.frmData.hdColBreak.value = 2;
							window.opener.window.document.frmData.hdColumnSort.value = 1;
						}
						if	(window.document.frmData.optColBreak[3].checked == true && window.document.frmData.chkStack.checked == false)
						{
							window.opener.window.document.frmData.hdColBreak.value = 3;
							window.opener.window.document.frmData.hdColumnSort.value = 1;
						}
						//----------------------------------------------------------------------
						//Column Sort---------------------------------------------------------
						if	(window.document.frmData.optSort[1].checked == true)// && window.document.frmData.chkColAggUnique.checked  == false)
						{
							window.opener.window.document.frmData.hdColumnSort.value = 1;
						}
						if	(window.document.frmData.optSort[2].checked == true)// && window.document.frmData.chkColAggUnique.checked  == false)
						{
							window.opener.window.document.frmData.hdColumnSort.value = 2;
						}
				
						//--------------------------------------------------------------
						//--Col Aggragate---------------------------------------------------------------
						if	(window.document.frmData.chkColAggCount.checked  == true && window.document.frmData.chkColAggUnique.checked  == true)
						{
							alert("Can't select both");
							return false;
						}
						else if	(window.document.frmData.chkColAggCount.checked  == true)
						{
							window.opener.window.document.frmData.hdIsAggCountSelected.value = 1;
						}
				
						else if	(window.document.frmData.chkColAggUnique.checked  == true)
						{
							if (window.document.frmData.optSort[2].checked == true)
							{
								window.opener.window.document.frmData.hdColumnSort.value = 2;
								window.opener.window.document.frmData.hdIsAggUniqueSelected.value = 1;
							}
							else
							{	
								window.opener.window.document.frmData.hdColumnSort.value = 1;
								window.opener.window.document.frmData.hdIsAggUniqueSelected.value = 1;
							}															
						}
						//-----------------------------------------------------------------------------------
						//--Duplicate printing
						if	(window.document.frmData.chkDupPrint.checked  == true)
						{
							window.opener.window.document.frmData.hdIsSuppressSelected.value = 1;
						}
				}//End if	
				//-------------------------------------End of Case 0,3 etc etc...--------------------------------------			
				// For datatype 1
				if(window.document.frmData.hdDataType.value == "1"||window.document.frmData.hdDataType.value == "9"||window.document.frmData.hdDataType.value == "11")
					{
						if(window.document.frmData.chkStack.checked  == true)
						{
							window.opener.window.document.frmData.hdIsStackSelected.value = 1;
							if (window.document.frmData.optSort[0].checked == true)
							{
							window.opener.window.document.frmData.hdColumnSort.value = 1;
							}
						}
						// Include chkbox
						
						if	(window.document.frmData.chkInclude.checked  == false)
						{
							window.opener.window.document.frmData.hdIsIncludeSelected.value = 1;
						}
						//Print chkbox
						
						if	(window.document.frmData.chkSubTotal.checked  == true)
						{
							window.opener.window.document.frmData.hdIsPrintSelected.value = 1;
							
							if (window.document.frmData.optSort[0].checked == true)
							{
							window.opener.window.document.frmData.hdColumnSort.value = 1;
							}
							if (window.document.frmData.optColBreak[0].checked == true)
							{
							window.opener.window.document.frmData.hdColBreak.value = 1;
							}
						}
						//Roll up chkbox
						
						if	(window.document.frmData.chkRollUp.checked  == true)
						{
							window.opener.window.document.frmData.hdIsRollSelected.value = 1;
						}
						//column break option--------------------------------------------------------------------------------
				
						if	(window.document.frmData.optColBreak[1].checked == true)
						{
							window.opener.window.document.frmData.hdColBreak.value = 1;
							window.opener.window.document.frmData.hdColumnSort.value = 1;
						}
						if	(window.document.frmData.optColBreak[2].checked == true)
						{
							window.opener.window.document.frmData.hdColBreak.value = 2;
							window.opener.window.document.frmData.hdColumnSort.value = 1;
						}
						if	(window.document.frmData.optColBreak[3].checked == true)
						{
							window.opener.window.document.frmData.hdColBreak.value = 3;
							window.opener.window.document.frmData.hdColumnSort.value = 1;
						}
						//----------------------------------------------------------------------
						//Column Sort---------------------------------------------------------
						if	(window.document.frmData.optSort[1].checked == true)
						{
							window.opener.window.document.frmData.hdColumnSort.value = 1;
						}
						if	(window.document.frmData.optSort[2].checked == true)
						{
							window.opener.window.document.frmData.hdColumnSort.value = 2;
						}
				
						//--------------------------------------------------------------
						//--Col Aggragate---------------------------------------------------------------
						if	(window.document.frmData.chkColAggCount.checked  == true && window.document.frmData.chkColAggUnique.checked  == true)
						{
							alert("Can't select both");
							return false;
						}
						else if	(window.document.frmData.chkColAggCount.checked  == true)
						{
							window.opener.window.document.frmData.hdIsAggCountSelected.value = 1;
						}
				
						else if	(window.document.frmData.chkColAggUnique.checked  == true)
						{
							window.opener.window.document.frmData.hdIsAggUniqueSelected.value = 1;
							if (window.document.frmData.optSort[0].checked == true)
							{
								window.opener.window.document.frmData.hdColumnSort.value = 1;
							}
						}
						
						if(window.document.frmData.chkColAggSum.checked  == true)
							{
								window.opener.window.document.frmData.hdIsAggSumSelected.value = 1;
							}
						if(window.document.frmData.chkColAggAvg.checked  == true)
							{
								window.opener.window.document.frmData.hdIsAggAvgSelected.value = 1;
							}
						if(window.document.frmData.chkColAggMin.checked  == true)
							{
								window.opener.window.document.frmData.hdIsAggMinSelected.value = 1;
							}
						if(window.document.frmData.chkColAggMax.checked  == true)
							{
								window.opener.window.document.frmData.hdIsAggMaxSelected.value = 1;
							}
						
						//-----------------------------------------------------------------------------------
						//--Duplicate printing
						else if	(window.document.frmData.chkDupPrint.checked  == true)
						{
							window.opener.window.document.frmData.hdIsSuppressSelected.value = 1;
						}
				}		
				// For datatype 2
				if(window.document.frmData.hdDataType.value == "2")
					{
						if(window.document.frmData.chkStack.checked  == true)
						{
							window.opener.window.document.frmData.hdIsStackSelected.value = 1;
							if (window.document.frmData.optSort[0].checked == true)
							{
							window.opener.window.document.frmData.hdColumnSort.value = 1;
							}
						}
						// Include chkbox
						
						if	(window.document.frmData.chkInclude.checked  == false)
						{
							window.opener.window.document.frmData.hdIsIncludeSelected.value = 1;
						}
						//Print chkbox
						
						if	(window.document.frmData.chkSubTotal.checked  == true)
						{
							window.opener.window.document.frmData.hdIsPrintSelected.value = 1;
							
							if (window.document.frmData.optSort[0].checked == true)
							{
							window.opener.window.document.frmData.hdColumnSort.value = 1;
							}
							if (window.document.frmData.optColBreak[0].checked == true)
							{
							window.opener.window.document.frmData.hdColBreak.value = 1;
							}
						}
						//Roll up chkbox
						
						if	(window.document.frmData.chkRollUp.checked  == true)
						{
							window.opener.window.document.frmData.hdIsRollSelected.value = 1;
						}
						//column break option--------------------------------------------------------------------------------
				
						if	(window.document.frmData.optColBreak[1].checked == true)
						{
							window.opener.window.document.frmData.hdColBreak.value = 1;
							window.opener.window.document.frmData.hdColumnSort.value = 1;
						}
						if	(window.document.frmData.optColBreak[2].checked == true)
						{
							window.opener.window.document.frmData.hdColBreak.value = 2;
							window.opener.window.document.frmData.hdColumnSort.value = 1;
						}
						if	(window.document.frmData.optColBreak[3].checked == true)
						{
							window.opener.window.document.frmData.hdColBreak.value = 3;
							window.opener.window.document.frmData.hdColumnSort.value = 1;
						}
						//----------------------------------------------------------------------
						//Column Sort---------------------------------------------------------
						if	(window.document.frmData.optSort[1].checked == true)
						{
							window.opener.window.document.frmData.hdColumnSort.value = 1;
						}
						if	(window.document.frmData.optSort[2].checked == true)
						{
							window.opener.window.document.frmData.hdColumnSort.value = 2;
						}
				
						//--------------------------------------------------------------
						//--Col Aggragate---------------------------------------------------------------
						if	(window.document.frmData.chkColAggCount.checked  == true && window.document.frmData.chkColAggUnique.checked  == true)
						{
							alert("Can't select both");
							return false;
						}
						else if	(window.document.frmData.chkColAggCount.checked  == true)
						{
							window.opener.window.document.frmData.hdIsAggCountSelected.value = 1;
						}
				
						else if	(window.document.frmData.chkColAggUnique.checked  == true)
						{
							window.opener.window.document.frmData.hdIsAggUniqueSelected.value = 1;
							if (window.document.frmData.optSort[0].checked == true)
							{
							window.opener.window.document.frmData.hdColumnSort.value = 1;
							}
						}
						
						
						//-----------------------------------------------------------------------------------
						
						//--Duplicate printing
						if	(window.document.frmData.chkDupPrint.checked  == true)
						{
							window.opener.window.document.frmData.hdIsSuppressSelected.value = 1;
						}
						if	(window.document.frmData.chkFiscYear.checked  == true)
						{
							window.opener.window.document.frmData.hdIsFiscSelected.value = 1;
						}
					
						//----Organistion
						if (window.document.frmData.cboOrganization.selectedIndex == "0")
							window.opener.window.document.frmData.hdOrganization.value = 0;
						if (window.document.frmData.cboOrganization.selectedIndex == "1")
							window.opener.window.document.frmData.hdOrganization.value = 720;
						if (window.document.frmData.cboOrganization.selectedIndex == "2")
							window.opener.window.document.frmData.hdOrganization.value = 20;
						if (window.document.frmData.cboOrganization.selectedIndex == "3")
							window.opener.window.document.frmData.hdOrganization.value = 50;
						if (window.document.frmData.cboOrganization.selectedIndex == "4")
							window.opener.window.document.frmData.hdOrganization.value = 243;
						if (window.document.frmData.cboOrganization.selectedIndex == "5")
							window.opener.window.document.frmData.hdOrganization.value = 241;
						if (window.document.frmData.cboOrganization.selectedIndex == "6")
							window.opener.window.document.frmData.hdOrganization.value = 2;
						if (window.document.frmData.cboOrganization.selectedIndex == "7")
							window.opener.window.document.frmData.hdOrganization.value = 44;
						if (window.document.frmData.cboOrganization.selectedIndex == "8")
							window.opener.window.document.frmData.hdOrganization.value = 5;
						if (window.document.frmData.cboOrganization.selectedIndex == "9")
							window.opener.window.document.frmData.hdOrganization.value = 70;
						
						//----LOB
						if (window.document.frmData.cboLineOfBusiness.selectedIndex == "0")
							window.opener.window.document.frmData.hdLineOfBusiness.value = 0;
						if (window.document.frmData.cboLineOfBusiness.selectedIndex == "1")
							window.opener.window.document.frmData.hdLineOfBusiness.value = 241;
						if (window.document.frmData.cboLineOfBusiness.selectedIndex == "2")
							window.opener.window.document.frmData.hdLineOfBusiness.value = 2;
						if (window.document.frmData.cboLineOfBusiness.selectedIndex == "3")
							window.opener.window.document.frmData.hdLineOfBusiness.value = 68;
						if (window.document.frmData.cboLineOfBusiness.selectedIndex == "4")
							window.opener.window.document.frmData.hdLineOfBusiness.value = 5;
						if (window.document.frmData.cboLineOfBusiness.selectedIndex == "5")
							window.opener.window.document.frmData.hdLineOfBusiness.value = 29;
						if (window.document.frmData.cboLineOfBusiness.selectedIndex == "6")
							window.opener.window.document.frmData.hdLineOfBusiness.value = 4;
						if (window.document.frmData.cboLineOfBusiness.selectedIndex == "7")
							window.opener.window.document.frmData.hdLineOfBusiness.value = 243;
						if (window.document.frmData.cboLineOfBusiness.selectedIndex == "8")
							window.opener.window.document.frmData.hdLineOfBusiness.value = 242;
						if (window.document.frmData.cboLineOfBusiness.selectedIndex == "9")
							window.opener.window.document.frmData.hdLineOfBusiness.value = 844;
				}	
					
				//---------------------------------------------------------------------------------
				
				// For datatype 8
				if(window.document.frmData.hdDataType.value == "8")
					{
						if(window.document.frmData.chkStack.checked  == true)
						{
							window.opener.window.document.frmData.hdIsStackSelected.value = 1;
						}
						// Include chkbox
						
						if	(window.document.frmData.chkInclude.checked  == false)
						{
							window.opener.window.document.frmData.hdIsIncludeSelected.value = 1;
						}
						//Print chkbox
						
						if	(window.document.frmData.chkSubTotal.checked  == true)
						{
							window.opener.window.document.frmData.hdIsPrintSelected.value = 1;
						}
						//Roll up chkbox
						
						if	(window.document.frmData.chkRollUp.checked  == true)
						{
							window.opener.window.document.frmData.hdIsRollSelected.value = 1;
						}
						//column break option--------------------------------------------------------------------------------
				
						if	(window.document.frmData.optColBreak[3].checked == true)
						{
							window.opener.window.document.frmData.hdColBreak.value = 3;
						}
						
						//--Col Aggragate---------------------------------------------------------------
						
						if	(window.document.frmData.chkColAggCount.checked  == true)
						{
							window.opener.window.document.frmData.hdIsAggCountSelected.value = 1;
						}
				
						if(window.document.frmData.chkLines.checked  == false)
						{
							window.opener.window.document.frmData.hdDefaultLines.value = 1;
						}						
						
						if	(window.document.frmData.txtLinesPerRow.value == "" || isNaN(window.document.frmData.txtLinesPerRow.value) || (window.document.frmData.txtLinesPerRow.value >= 0 && window.document.frmData.txtLinesPerRow.value <1))
							{
								window.opener.window.document.frmData.hdDefaultLines.value = "";
							}
						else if(window.document.frmData.txtLinesPerRow.value < 0)
							{
								window.opener.window.document.frmData.hdMaxLines.value = "1";
							} 
						else
							window.opener.window.document.frmData.hdMaxLines.value = Math.floor(window.document.frmData.txtLinesPerRow.value);
						
				}							
						//-----------------------------------------------------------------------------------
				window.opener.window.document.frmData.hdSubmit.value = "PROPERTIES"
				window.opener.window.document.frmData.hdPropDataType.value = window.document.frmData.hdDataType.value;
				window.opener.window.document.frmData.hdSelectedIndex.value = window.document.frmData.hdSelIndex.value
				window.opener.tabNavigation(0,0);
				window.close();		
			}	
		
			function window_onLoad()
			{
				//----Organistion
				if (window.document.frmData.hdOrganization.value == 0)
					window.document.frmData.cboOrganization.selectedIndex = 0;
				if (window.document.frmData.hdOrganization.value == 720)
					window.document.frmData.cboOrganization.selectedIndex = 1;
				if (window.document.frmData.hdOrganization.value == 20)
					window.document.frmData.cboOrganization.selectedIndex = 2;
				if (window.document.frmData.hdOrganization.value == 50)
					window.document.frmData.cboOrganization.selectedIndex = 3;
				if (window.document.frmData.hdOrganization.value == 243)
					window.document.frmData.cboOrganization.selectedIndex = 4;
				if (window.document.frmData.hdOrganization.value == 241)
					window.document.frmData.cboOrganization.selectedIndex = 5;
				if (window.document.frmData.hdOrganization.value == 2)
					window.document.frmData.cboOrganization.selectedIndex = 6;
				if (window.document.frmData.hdOrganization.value == 44)
					window.document.frmData.cboOrganization.selectedIndex = 7;
				if (window.document.frmData.hdOrganization.value == 5)
					window.document.frmData.cboOrganization.selectedIndex = 8;
				if (window.document.frmData.hdOrganization.value == 70)
					window.document.frmData.cboOrganization.selectedIndex = 9;

				//LOB
				if (window.document.frmData.hdLineOfBusiness.value == 0)
					window.document.frmData.cboLineOfBusiness.selectedIndex = 0;
				if (window.document.frmData.hdLineOfBusiness.value == 241)
					window.document.frmData.cboLineOfBusiness.selectedIndex = 1;
				if (window.document.frmData.hdLineOfBusiness.value == 2)
					window.document.frmData.cboLineOfBusiness.selectedIndex = 2;
				if (window.document.frmData.hdLineOfBusiness.value == 68)
					window.document.frmData.cboLineOfBusiness.selectedIndex = 3;
				if (window.document.frmData.hdLineOfBusiness.value == 5)
					window.document.frmData.cboLineOfBusiness.selectedIndex = 4;
				if (window.document.frmData.hdLineOfBusiness.value == 29)
					window.document.frmData.cboLineOfBusiness.selectedIndex = 5;
				if (window.document.frmData.hdLineOfBusiness.value == 4)
					window.document.frmData.cboLineOfBusiness.selectedIndex = 6;
				if (window.document.frmData.hdLineOfBusiness.value == 243)
					window.document.frmData.cboLineOfBusiness.selectedIndex = 7;
				if (window.document.frmData.hdLineOfBusiness.value == 242)
					window.document.frmData.cboLineOfBusiness.selectedIndex = 8;
				if (window.document.frmData.hdLineOfBusiness.value == 844)
					window.document.frmData.cboLineOfBusiness.selectedIndex = 9;
			
				if (window.document.frmData.chkFiscYear.checked == true)
				{
					window.document.frmData.cboOrganization.disabled = false;
					window.document.frmData.cboLineOfBusiness.disabled = false;
				}	
			}
		
			function ColAggregate()
			{
				if(window.document.frmData.chkColAggNone.checked)
				{
					window.opener.window.document.frmData.hdAggCount.value = "";
					window.opener.window.document.frmData.hdAggUniqueCount.value = "";
					window.opener.window.document.frmData.hdAggSum.value = "";
					window.opener.window.document.frmData.hdAggAvg.value = "";
					window.opener.window.document.frmData.hdAggMin.value = "";
					window.opener.window.document.frmData.hdAggMax.value = "";
				}
			
				if(window.document.frmData.hdDataType.value == "0" || window.document.frmData.hdDataType.value == "2"|| window.document.frmData.hdDataType.value == "3" || window.document.frmData.hdDataType.value == "4" || window.document.frmData.hdDataType.value == "5" || window.document.frmData.hdDataType.value == "6" || window.document.frmData.hdDataType.value == "7" || window.document.frmData.hdDataType.value == "10" )//|| window.document.frmData.hdDataType.value == "12" || window.document.frmData.hdDataType.value == "13" )
				{
					if(window.document.frmData.chkColAggNone.checked == false)
					{
						window.document.frmData.chkColAggCount.disabled = false;
						window.document.frmData.chkColAggUnique.disabled = false;
					}
					else if(window.document.frmData.chkColAggNone.checked == true)
					{
						window.document.frmData.chkColAggCount.disabled = true;
						window.document.frmData.chkColAggUnique.disabled = true;
						window.document.frmData.chkColAggCount.checked = false;
						window.document.frmData.chkColAggUnique.checked = false;
					}
				}
				
				if(window.document.frmData.hdDataType.value == "1"||window.document.frmData.hdDataType.value == "9"||window.document.frmData.hdDataType.value == "11")
				{
					if(window.document.frmData.chkColAggNone.checked == false)
					{
						window.document.frmData.chkColAggCount.disabled = false;
						window.document.frmData.chkColAggUnique.disabled = false;
						window.document.frmData.chkColAggSum.disabled = false;
						window.document.frmData.chkColAggAvg.disabled = false;
						window.document.frmData.chkColAggMax.disabled = false;
						window.document.frmData.chkColAggMin.disabled = false;
					}
					else if(window.document.frmData.chkColAggNone.checked == true)
					{
						window.document.frmData.chkColAggCount.disabled = true;
						window.document.frmData.chkColAggUnique.disabled = true;
						window.document.frmData.chkColAggSum.disabled = true;
						window.document.frmData.chkColAggAvg.disabled = true;
						window.document.frmData.chkColAggMax.disabled = true;
						window.document.frmData.chkColAggMin.disabled = true;
						
						window.frmData.chkColAggCount.checked = false;
						window.frmData.chkColAggUnique.checked = false;
						window.frmData.chkColAggSum.checked = false;
						window.frmData.chkColAggAvg.checked = false;
						window.frmData.chkColAggMin.checked = false;
						window.frmData.chkColAggMax.checked = false;
					}
				}
				
				if(window.document.frmData.hdDataType.value == "8")
				{
					if (window.document.frmData.chkColAggNone.checked == false)
					{
						window.document.frmData.chkColAggCount.disabled = false;
						window.document.frmData.chkColAggCount.checked = false;
					}
					else if( window.document.frmData.chkColAggNone.checked == true )
					{
						window.document.frmData.chkColAggCount.disabled = true;
						window.document.frmData.chkColAggCount.checked = false;
					}
						
				}
			}
		
			function Toggle()
			{
				if (window.document.frmData.optColBreak[0].checked == true )
				{
					if (window.document.frmData.chkStack.checked == true)
					{
						window.document.frmData.chkStack.checked = false;
					}
				}
				if (window.document.frmData.optColBreak[0].checked == false)
				{
					if (window.document.frmData.chkStack.checked == false)
					{
						window.document.frmData.chkStack.checked = true;
					}
				}
			}
				
			function Toggle1()	
			{
				if (window.document.frmData.chkStack.checked == true && window.document.frmData.optColBreak[3].checked == false)
				{
					if (window.document.frmData.optColBreak[0].checked == true)
					{
						window.document.frmData.optColBreak[0].checked = false;
					}
				}
				if (window.document.frmData.chkStack.checked == false && window.document.frmData.optColBreak[3].checked == true)
				{
					window.document.frmData.optColBreak[3].checked = true;
					return true;
				}
				if (window.document.frmData.chkStack.checked == false)
				{
					if (window.document.frmData.optColBreak[0].checked == false)
					{
						window.document.frmData.optColBreak[0].checked = true;
					}
				}
			}
				
			function SelectFisc()
			{
				if (window.document.frmData.chkFiscYear.checked == true)
				{
					window.document.frmData.cboOrganization.disabled = false;
					window.document.frmData.cboLineOfBusiness.disabled = false;
				}
				if (window.document.frmData.chkFiscYear.checked == false)
				{
					window.document.frmData.cboOrganization.disabled = true;
					window.document.frmData.cboLineOfBusiness.disabled = true;
				}

			}
		
			function LineNo()
			{
				if(window.document.frmData.chkLines.checked == false)
				{
					window.document.frmData.txtLinesPerRow.disabled = false;
				}
				if(window.document.frmData.chkLines.checked == true)
				{
					window.document.frmData.txtLinesPerRow.disabled = true;
					window.document.frmData.txtLinesPerRow.value = "";
				}
			}
			
			function changeOrganization()
			{
			}
			
			//^^Rajeev 05/20/2003----
			
		</script>
	</head>
  
	<body class="8pt" onLoad="window_onLoad();">
	<form name="frmData" topmargin="0" leftmargin="0">
		<table align="center" border="0" width="100%" bordercolor="#336699" cellpadding="0" cellspacing="0" class1="singleborder">
			<tr>
				<td>
					<table border="0" width="100%">
						<tr><td width="33%" nowrap>&nbsp;<b><%= sFieldName%></b></td>
							<td width="33%" nowrap><input type="checkbox" value="1" name="chkStack" <%= checkStack%> onClick="Toggle1()" >Stacked</td>
							<td width="33%" nowrap><input type="checkbox" value="" name="chkDupPrint" <%=DuplicatePrint%>>Suppress Duplicate Printing</td>
						</tr>
	
						<tr><td nowrap><input type="checkbox" value="" name="chkInclude" <%=IncludeField %>>Include Field In Report</td>
							<td nowrap><input type="checkbox" value="" name="chkSubTotal"<%= SubTotal %>>Print Sub Total</td>
							<td align="left" nowrap><input type="checkbox" value="" name="chkRollUp" <%=RollUp%> >Roll Up</td>
						</tr>
					</table>
	
					<table class="formsubtitle" border="0" width="98%">
						<tr>
							<td class="ctrlgroup">Column Break</td>
						</tr>
					</table>
					
					<table border="0" width="100%">
						<tr>
							<td width="25%"><input type="radio" value="" name="optColBreak" <%=ColBreak0%> onClick="Toggle()" >None</td>
							<td width="25%"><input type="radio" value="" name="optColBreak" <%=ColBreak1%> >Break</td>
							<td width="25%"><input type="radio" value="" name="optColBreak" <%=ColBreak2%>>Page</td>
							<td width="25%"><input type="radio" value="" name="optColBreak" <%=ColBreak3%>>Banner</td>
						</tr>
					</table>
	
					<table class="formsubtitle" border="0" width="98%">
						<tr>
							<td class="ctrlgroup">Column Aggregate</td>
						</tr>
					</table>
	
					<table width="100%" border="0">
						<tr><td width="25%"><input type="checkbox" value="" name="chkColAggNone" <%=AggNone%> onClick="ColAggregate();">None</td>
							<td width="25%"><input type="checkbox" value="" name="chkColAggCount"<%=AggCount%> onClick="changeColAggCheckBox(0);">Count</td>
							<td width="50%" colspan="2"><input type="checkbox" value="" name="chkColAggUnique" <%=AggCountUnique%> onClick="changeColAggCheckBox(1);">Unique Count</td>
						</tr>
						<tr>
							<td width="25%"><input type="checkbox" value="" name="chkColAggSum" <%=AggSum%> onClick="changeColAggCheckBox(2);">Sum</td>
							<td width="25%"><input type="checkbox" value="" name="chkColAggAvg" <%=AggAvg%> onClick="changeColAggCheckBox(3);">Average</td>
							<td width="25%"><input type="checkbox" value="" name="chkColAggMin" <%=AggMin%> onClick="changeColAggCheckBox(4);">Min</td>
							<td width="25%"><input type="checkbox" value="" name="chkColAggMax" <%=AggMax%> onClick="changeColAggCheckBox(5);">Max</td>
						</tr>
					</table>
	
					<table class="formsubtitle" border="0" width="98%">
						<tr>
							<td class="ctrlgroup">Column Sort</td>
						</tr>
					</table>

					<table border="0" width="100%">
						<tr>
							<td width="25%"><input type="radio" value="" name="optSort" <%=ColSort0%>>None</td>
							<td width="25%"><input type="radio" value="" name="optSort" <%=ColSort1%>>Ascending</td>
							<td width="50%"><input type="radio" value="" name="optSort" <%=ColSort2%>>Descending</td>
						</tr>
					</table>
	
					<table class="formsubtitle" border="0" width="98%">
						<tr>
							<td class="ctrlgroup">Free Text Properties</td>
						</tr>
					</table>

					<table width="100%" border="0">
						<tr>
							<td width="50%"><input type="checkbox" value="" name="chkLines" <%=DefaultLines%> onClick="LineNo()" >Default Lines Per Row</td>
							<td align="left">Max Lines Per Row&nbsp;<input type="text" size="21" name="txtLinesPerRow" <%=MaxLines%> value="<%=txtMaxLines%>"></td>
						</tr>
					</table>

				<hr width="96%">

					<table width="100%" border="0">
						<tr>
							<td width="55%" align="left"><input type="checkbox" value="" name="chkFiscYear" <%=FiscYear%> onClick="SelectFisc()">Fiscal Year</td>
							<td width="45%" align="left"><input type="checkbox" value="" name="chkTopCode" <%=TopCode%>>Top Code Only</td>
						</tr>
						<tr>
							<td align="left">&nbsp;Organization:&nbsp;
								<select name="cboOrganization" style="WIDTH:70%" height="1" <%=Organization%> onChange="changeOrganization();">
									<%
									Dim s_temp, arrOrganisation(), bFound, iIndex
									iIndex = 0
									For i = 1 To objFiscal.Count
										If objFiscal.item(i).m_ENTITY_NAME = "" Then
											s_temp = "Entire Organization"
										Else
										    s_temp = objFiscal.item(i).m_ENTITY_NAME
										End If
										Redim Preserve arrOrganisation(iIndex)
										bFound = False
										Err.Clear()
										On Error Resume Next
										m = -1
										m = UBound(arrOrganisation)
										If Err.number <> 9 And m <> "" And m >= 0 Then
											For j = 0 To Ubound(arrOrganisation)
												If s_temp = arrOrganisation(j) Then
													bFound = True
													Exit For
												End If
											Next
										End If
										On Error Goto 0
										
										If Not bFound Then 
											arrOrganisation(iIndex) = s_temp
											iIndex = iIndex + 1
										End If
									Next
									
									If Err.number <> 9 And m <> "" And m >= 0 Then
										For i = 0 To Ubound(arrOrganisation) - 1
											Response.Write "<option>" & arrOrganisation(i) & "</option>"
										Next
									End If
									
									%>								
								</select>
							</td>
							<td align="left">Line Of Business:&nbsp;&nbsp;
								<select name="cboLineOfBusiness" style="WIDTH:50%" height="1" <%=LineOfExport%>>
									<%
									Dim arrLOB()
									iIndex = 0
									Redim arrLOB(iIndex)
									For i = 1 To objFiscal.Count
										If objFiscal.item(i).m_CODE_DESC = "" Then
											s_temp = "All Lines"
										Else
											s_temp = Trim(objFiscal.item(i).m_CODE_DESC)
										End If
																		
										bFound = False
										Err.Clear()
										On Error Resume Next
										m = -1
										m = UBound(arrLOB)
										If Err.number <> 9 And m <> "" And m >= 0 Then
											For j = 0 To Ubound(arrLOB)
												If s_temp = arrLOB(j) Then
													bFound = True
													Exit For
												End If
											Next
										End If
										On Error Goto 0
																		
										If Not bFound Then 
											arrLOB(iIndex) = s_temp
											iIndex = iIndex + 1
											Redim Preserve arrLOB(iIndex)
										End If
									Next
									
									If Err.number <> 9 And m <> "" And m >= 0 Then							
										For i = 0 To Ubound(arrLOB) - 1
											Response.Write "<option>" & arrLOB(i) & "</option>"									
										Next
									End If									
									%>
								</select>
							</td>
						</tr>
					</table>
				<hr width="96%">

					<table width="100%" border="0">
						<tr>
							<td width="30%"></td>
							<td>
								<table width="100%" border="0">
									<tr>
										<td width="33%"><input class="button" type="button" name="btnOk" value="OK"   onClick ="PropertySubmit()" style="width:100%"></td>
										<td width="34%"><input class="button" type="button" name="btnCancel" value="Cancel" onClick="window.close()" style="width:100%"></td>
										<td width="33%"><input class="button" type="button" name="btnHelp" value="Help" style="width:100%"></td>
									</tr>
								</table>
							</td>
							<td width="30%"></td>
						</tr>
					</table>
				</td>
			</td>
		</table>

		<input type="hidden" name="hdDataType" value="<%=DataType%>">
		<input type="hidden" name="hdArrayTF" value="<%=objSessionStr("ArrayTF")%>" >
		<input type="hidden" name="hdSelFields" value="<%=objSessionStr("ArrayP")%>">
		<input type="hidden" name="hdArrayPFields" value='<%=objSessionStr("ArrayPFields")%>'>			
		<input type="hidden" name="hdLineOfBusiness" value="<%=lob%>">			
		<input type="hidden" name="hdOrganization" value="<%=org%>">
		<input type="hidden" name="hdDefaultLines" value="">
		<input type="hidden" name="hdSelIndex" value="<%=Request.QueryString("b")%>">

	</form>
	</body>
</html>
