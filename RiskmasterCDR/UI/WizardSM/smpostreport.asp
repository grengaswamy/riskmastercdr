<!-- #include file ="SessionInitialize.asp" -->
<!-- #include file ="dcom.inc" -->
<%Public objSession
initSession objSession
%> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
	<title>SORTMASTER Report Post</title>
	<!--[Rajeev 11/15/2002 CSS Consolidation] link rel="stylesheet" href="forms.css" type="text/css"/-->
	<link rel="stylesheet" href="RMNet.css" type="text/css"/>
	<script language="JavaScript">
		var m_FormSubmitted=false;
		function onExit()
		{
			//if(self.parent == null || self.parent == self)
			//	self.history.go(-2);
			
			//abisht MITS 11222
			if(eval(document.frmData.serverName) != null )
			{
			    var serverName = document.frmData.serverName.value;
			    var index = serverName.indexOf('mdi.html');
			    serverName = serverName.substring(0,index);
			}
			
			if(eval(document.frmData.txtNameHidden) != null )
			{
			    var txtNameHidden = document.frmData.txtNameHidden.value;
			}
			
			if(eval(document.frmData.txtNameHidden) != null &&  eval(document.frmData.serverName) != null )
			{
			    switch(txtNameHidden)
			    {
			       case 'OSHA 300 Report':
			            location.href = serverName + "home?pg=riskmaster/Reports/PostOsha300Report";
			            break;
			       case 'OSHA 301 Report':
			            location.href = serverName + "home?pg=riskmaster/Reports/PostOsha301Report";
			            break;
			       case 'OSHA 300A Report':
			            location.href = serverName + "home?pg=riskmaster/Reports/PostOsha300AReport";
			            break;
			       case 'OSHA Sharps Log':
			            location.href = serverName + "home?pg=riskmaster/Reports/PostOshaSharpsLogReport";
			            break;
			    }
			}
			
			return true;
		}
		
			//Set Focus to the first field
		function pageLoaded()
		{
			 var i;

			 for (i=0;i<document.frmData.length;i++)			
				{	 				
				 if((document.frmData.item(i).type=="text") || (document.frmData.item(i).type=="select-one")|| (document.frmData.item(i).type=="textarea"))
					{
						document.frmData.item(i).focus();
						break;
					}
				}
		}		
		function getSelUsers()
		{
			var sUsers=new String();
			for(var i=0;i<document.frmData.users.options.length;i++)
			{
				if (document.frmData.users.options[i].selected)
				{
					var sName=new String(document.frmData.users.options[i].value);
					if(sUsers!="")
						sUsers=sUsers + ",";
					sUsers=sUsers + sName;
				}
			}
			return sUsers;
		}
		
		function UserSubmit()
		{
		
			if(m_FormSubmitted)
			{
				alert("You already submitted this form. Please wait for server to respond.");
				return false;
			}
			
			if(document.frmData.txtName.value=="")
			{
				alert("Please enter report name.");
				return false;
			}
			
			if(document.frmData.txtFileName)
				if(document.frmData.txtFileName.value=="")
				{
					alert("Please select report file to upload.");
					return false;
				}
			
			if(getSelUsers()=="")
			{
				alert("Please select at least one user allowed to use this report.");
				return false;
			}
			var sUsers=getSelUsers();
			document.frmData.userlist.value = sUsers;
			
			m_FormSubmitted=true;			
			return true;
		}
		
		 	
		
		function pageLoad(sFieldname)
		{
		
			var objCntrlname=eval("document.frmData."+sFieldname);
			if (objCntrlname !=null)
			{
				objCntrlname.focus();
			}
			
			for(var i=0;i<document.frmData.users.options.length;i++)
			{
				document.frmData.users.options[i].selected = true;
			}
			return true;
		}
	</script>
</head>

<%
' Attempt to run SMRConvert wrapped for DCOM
Function SMRConvert(sPath)
	'Forward Conversion File IFF DCOM makes this necessary.
	
	Dim sRemotePath, objFS, fso, sOrigPath, sResultPath
	sOrigPath = sPath
	Set fso = CreateObject("Scripting.FileSystemObject")
	If USE_DCOM Then
		'Use objFS to get temp path local to remote machine...
		Set objFS = CreateObject("CSCFileStorage.CCSCFileStorage")
		sRemotePath = objFS.sGetTempPath()
		Set objFS = Nothing
		If Right(sRemotePath,1) <> "\" Then sRemotePath = sRemotePath & "\"
		sPath = sRemotePath & fso.GetFileName(sPath)
		ForwardFileToDCOMServer(sOrigPath) ' Drops file into Temp folder of DCOM Box
	End If
	
	'Attempt Conversion
	Set objSM = CreateObject("SMEngine")
	objSM.InitEngine objSessionStr("DSN")
	SMRConvert =  objSM.SMRConvert(sPath , sPath)  
	
	If SMRConvert and USE_DCOM Then ' Fetch the Results back here.
		On Error Resume Next
		fso.DeleteFile sOrigPath	
		sResultPath = PullFileFromDCOMServer(sPath)
		fso.MoveFile sResultPath, sOrigPath 
	End If
 sPath = sOrigPath
End Function

' try to convert the report from old SM to new XML format
Function SMToXML(sSMText)
Dim bError, sPath, objStream
	
	'Prepare Temp Conversion File 
	Set fso = CreateObject("Scripting.FileSystemObject")
	sPath = Application(APP_USERDIRECTORYHOME)& "\" & fso.GetTempName()
	Set objStream = fso.CreateTextFile(sPath)
	objStream.Write(sSMText)
	objStream.Close
	Set objStream = Nothing
	
	'Attempt Conversion
	'Note: Modified to Use wrapper function that incorporates DCOM File Forwarding.
	if not SMRConvert(sPath) then 
		sError = "File you are trying to post is not a valid " & REPORTAPP_TITLE & " report."
		SMToXML = ""
	else
		Set objStream = fso.OpenTextFile(sPath)
		SMToXML = objStream.ReadAll()
		objStream.Close
		Set objStream = nothing
	end if
	
	'Clean up
	On Error Resume Next
	fso.DeleteFile sPath, true
	On Error Goto 0
	Set fso = nothing
End Function

Sub SubmitPosted()	
	Dim objXML, bError, sXML
	Set objXML = CreateObject("MSXML.DOMDocument")
	bError = False
	sXML = Request("xml")
	
	On Error Resume Next
	'Verify Received XML Parses
	bError = (objXML.loadXML(sXML)= false)
	If Err.number <> 0 then bError = True
	
	If bError then 'Try a conversion
		bError = false
		bError = (objXML.loadXML(SMToXML(sXML))= false) 
		If Err.number <> 0 or bError then
			'Conversion Failed
			sError = "File you are trying to post is not a valid " & REPORTAPP_TITLE & " report."
			Exit Sub
		end if
	End If
	On Error Goto 0
	'Write report to database
	If not bError Then
		Set r = CreateObject("SMInterface.SMI")
		'Safeway Atul
		r.postReport Request("txtName"), Request("txtDescription"), objXML.xml, FetchUserArray(), Application("SM_DSN"), Request("chkPrivacycase")
	else
		sError="Your request could not be completed due to error: " & objUpload.ErrNumber & " - " & objUpload.ErrDesc & ". If this error persists "
		sError=sError & ERR_CONTACT
	end if
End Sub


' parse user list
Function FetchUserArray()
	Dim sUserList, vArrUsers, bOk,f
	If not objUpload is nothing then
		sUserList=objUpload("userlist")
	else
		sUserList = Request("userlist")
	End If

	If sUserList="" Then 
		Response.Redirect "smreportsel.asp"
		Response.End
	End If

	vArrUsers=Split(sUserList,",")
	If Not IsArray(vArrUsers) Then
		Response.Redirect "smreportsel.asp"
		Response.End
	End If

	bOk=False
	For f=0 to UBound(vArrUsers)
		If Not IsNumeric(vArrUsers(f)) Then	
			vArrUsers(f)=0
		Else
			bOk=True
		End If
	Next

	If not bOk Then
		Response.Redirect "smreportsel.asp"
		Response.End
	End if

	FetchUserArray = vArrUsers
End Function

Sub SubmitUploaded()
	Dim s
	dim xml
	dim sPath, sXMLPath
	dim bError
	dim r, henv, db, rs, lUserID, sFirstName, sLastName, sLoginName
	dim vTmp
	
	sError=""
	boolShowForm=True
	Set objUpload=nothing
	Set objUpload=CreateObject("IISUpload.CUpload")
	s=objUpload.Upload()
	If objUpload.ErrNumber = 0 And objUpload.FileName<>"" Then
		sPath=objUpload.FullFileName
		' ... try to load report into DOM - display error if DOM complains
		set xml = CreateObject("MSXML.DOMDocument")
		bError = False
		'on error resume next
		bError = (xml.load(sPath) = False)
		if Err.number <> 0 then bError = True
		'on error goto 0
		If Not bError then
			' Write report to database
			Set r = CreateObject("SMInterface.SMI")
			'Safeway Atul
			r.postReport objUpload("txtName") & "", objUpload("txtDescription") & "", xml.xml, FetchUserArray(), Application("SM_DSN"), Request.QueryString("chkPrivacycase")
		else
		' try to convert the report from old SM to new XML format
			if SMRConvert(sPath) then
				bError = (xml.load(sPath) = False)
				if Not bError then
					' Write report to database
					set r = CreateObject("SMInterface.SMI")
					'Safeway Atul
					r.postReport objUpload("txtName") & "", objUpload("txtDescription") & "", xml.xml, FetchUserArray(), Application("SM_DSN"), Request.QueryString("chkPrivacycase")
				else
					sError = "File you are trying to post is not a valid " & REPORTAPP_TITLE & " report."
				end if
			else ' Error - report probably is not XML
				sError = "File you are trying to post is not a valid " & REPORTAPP_TITLE & " report."
			end if
		end if
			
		' delete uploaded file
		on error resume next
		fso.DeleteFile sPath, True
		on error goto 0
	else	
		' Error while uploading
		If objUpload.ErrNumber=0 Then
			sError="Please select file to upload."
		ElseIf objUpload.ErrNumber=60003 Then
			' File is too big
			sError="File you are trying to upload is too big. If you want to increase the file size you "
			sError=sError & "can upload, get more disk space and many other benefits please sign up for our "
			sError=sError & "Premium Services."
		Else
			' Report what happend
			sError="Your request could not be completed due to error: " & objUpload.ErrNumber & " - " & objUpload.ErrDesc & ". If this error persists "
			sError=sError & ERR_CONTACT
		End If
	End If
	
End Sub
%>
<%
Dim boolShowForm,sError,objUpload,fso,bShowExit,bLoadGroupUsers,db1,rs1
'Initialize Page Level Object(s)
boolShowForm = True
Set fso = CreateObject("Scripting.FileSystemObject")
Set objUpload = nothing
If LCase(Request.ServerVariables("REQUEST_METHOD"))="post" Then
	boolShowForm = false
	bShowExit = false
	If Len(Request("xml"))= 0 Then
		SubmitUploaded()
	else
		SubmitPosted() 'Came from admin menu - need exit button.
		bShowExit = True
	end if
	' Check for errors
	If len(sError) > 0 then
		boolShowForm = True   'pass through & display error			
		bShowExit=False
	Else %>
		
		<center>
		<b>Report Posted Successfully!</b>
		</br>
		<!-- PJS MITS 12070, 04-22-2008 - Commented code and changed on click to history.back to get SM World report working -->
		<%If bShowExit Then Response.Write "</br></br><input class=""button"" type=""button"" value=""    Ok    "" onclick=""history.back();""/>"
		%></center><%
	End if
End If
bLoadGroupUsers=ObjSessionStr(SESSION_USERSBYGROUP) 'changed by ravi on 06-2004
'Response.End
If boolShowForm Then %>
<body class="10pt" onload="pageLoaded()">
<form method="post" action="smpostreport.asp" name="frmData" <%If Len(objSessionStr("reportxml"))= 0 Then%> enctype="multipart/form-data" <%End If%> onsubmit="return UserSubmit()">

<table width="100%">
	<tr><td colspan="2" class="msgheader">Reports Administration</td></tr>
	<tr><td colspan="2" class="ctrlgroup">Post New Report</td></tr>
	<%If sError<>"" Then%>
	<tr><td colspan="2" class="errortext"><%=sError%></td></tr>
	<%End If
	'Bury the "Can't read from Request collection after binary read" errors.
	On Error Resume Next
	%>
	<tr>
		<td nowrap="" class="datatd"><b>Report Name:</b></td>
		<td class="datatd"><input class="button" type="text" name="txtName" value="<%=Request("txtName")%>" size="48"/></td>
	</tr>

	<tr>
		<td nowrap="" class="datatd"><b>Report Description:</b></td>
		<td class="datatd"><input class="button" type="text" name="txtDescription" value="<%=Request("txtDescription")%>" size="48"/></td>
	</tr>
	<%If Len(objSessionStr("reportxml"))= 0 Then%>
	<tr>
		<td nowrap="" class="datatd"><b>Report File Name:</b></td>
		<td class="datatd"><input class="button" type="file" name="txtFileName"  size="35"/></td>
	</tr>
	<%Else%>
	<input type="hidden" name="xml" value='<%=objSessionStr("reportxml")%>' />
	<%objSession("reportxml") = "" 'clear it out - now passed via postback.
	End If
	On Error Goto 0
	%>
	<tr>
		<td nowrap="" class="datatd"><b>Users:</b></td>
		<td class="datatd">
		<i>Note: To select multiple users, hold down Ctrl key while clicking on user.</i><br />
		<select name="users" multiple="">
		<%
		Dim bLoadAllUsers
		bLoadAllUsers=True
		If objSessionStr(SESSION_MODULESECURITY)="1" Then
			If objSessionStr(SESSION_ADMINRIGHTS)<>"1" Then
				bLoadAllUsers=False
			End If
		End If
		
		If bLoadAllUsers Then
			'bLoadGroupUsers=ObjSessionStr(SESSION_USERSBYGROUP) 'changed by ravi on 06-2004
			if cbool(bLoadGroupUsers)=false then
				set r = CreateObject("DTGRocket")
				henv = r.DB_InitEnvironment()
				db = r.DB_OpenDatabase(henv, Application(APP_SECURITYDSN), 0)
				rs = r.DB_CreateRecordset(db, "SELECT USER_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME,USER_DETAILS_TABLE.LOGIN_NAME FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_DETAILS_TABLE.DSNID = " & objSessionStr("DSNID") & " AND USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID ORDER BY USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME", 3, 0)
				while not r.DB_EOF(rs)
				
					r.DB_GetData rs, 1, lUserID
					r.DB_GetData rs, 2, sLastName
					r.DB_GetData rs, 3, sFirstName
					r.DB_GetData rs, 4, sLoginName
					
					%>
					<option value="<% = lUserID %>"><% = sFirstName & " " & sLastName & " (" & sLoginName & ")" %></option>
					<%
			
					r.DB_MoveNext rs
				wend
				r.DB_CloseRecordset CInt(rs), 2
				r.DB_CloseDatabase CInt(db)
				r.DB_FreeEnvironment CLng(henv)
			else
			'*************modified by ravi on24 june 04 to filter out user lists according to group
				set r = CreateObject("DTGRocket")
				henv = r.DB_InitEnvironment()
				db1 = r.DB_OpenDatabase(henv, ObjSessionStr(SESSION_DSN), 0)
				rs1 = r.DB_CreateRecordset(db1, "SELECT USER_ID FROM USER_MEMBERSHIP WHERE GROUP_ID=" & objSessionStr(SESSION_GROUPID), 3, 0)
				
				while not r.DB_EOF(rs1)
					r.DB_GetData rs1, 1, lUserID
				
					db = r.DB_OpenDatabase(henv,Application(APP_SECURITYDSN) , 0)
					rs = r.DB_CreateRecordset(db, "SELECT USER_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME,USER_DETAILS_TABLE.LOGIN_NAME FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = " & lUserID & " AND USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID ORDER BY USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME", 3, 0)
					if not r.DB_EOF(rs) then
						r.DB_GetData rs, 1, lUserID
						r.DB_GetData rs, 2, sLastName
						r.DB_GetData rs, 3, sFirstName
						r.DB_GetData rs, 4, sLoginName
					end if	
					%>
					<option value="<% = lUserID %>"><% = sFirstName & " " & sLastName & " (" & sLoginName & ")" %></option>
					<%
			
					r.DB_MoveNext rs1
					r.DB_CloseRecordset CInt(rs), 2
					r.DB_CloseDatabase CInt(db)
				wend
				r.DB_CloseRecordset CInt(rs), 2
				r.DB_CloseRecordset CInt(rs1), 2
				r.DB_CloseDatabase CInt(db)
				r.DB_CloseDatabase CInt(db1)
				r.DB_FreeEnvironment CLng(henv)
				set r =nothing
			end if
			'*********************end*******************************
		Else
			Response.Write "<option value=""" & objSessionStr(SESSION_USERID) & """>" & objSessionStr(SESSION_FIRSTNAME) & " " & objSessionStr(SESSION_LASTNAME) & " (" & objSessionStr(SESSION_LOGINNAME) & ")</option>"
		
		
		End If
		%>
		</select>
		</td>
	</tr>
	<!--Safeway Atul - OSHA Privacy case-->
	<tr>
		<td nowrap="" class="datatd"><b>Privacy case:</b></td>
		<td class="datatd"><input type="checkbox" name="chkPrivacycase" <%=chkPrivacycase%> /></td>
	</tr>
</table>
<p class="small">
Use this page to add a new report to the Available Reports list.<br />
Be sure to give access to at least one user in the "Users" list.
<br>
*** For information on extended reporting features and report troubleshooting please see the <a href="smfaq.asp">Frequently Asked Questions (FAQ)</a>.</p>
	<input type="hidden" name="userlist" value=""/>
	<!-- PJS MITS 12070, 04-22-2008 - Commented code to get SM World report working -->
	<input type="submit" class="button" name="btnSave" value="  Save  " />
	<!--Anjaneya MITS 11680 -->
	<!--<input type="button" class="button" name="cmdBack" value="Back" onclick="history.back();"/>-->
</form>
<%End If
Set fso = nothing
Set objUpload=nothing%>
</body>
</html>
