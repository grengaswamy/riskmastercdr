<%Option Explicit%>
<!-- #include file ="session.inc" -->
<!-- #include file ="customdocscode.inc" -->
<%Public objSession
initSession objSession
%> 
<%
Const mc_iPerPageMax=500
Dim m_sTableName
Dim m_objDomCodes, objStyle
Dim m_lRecordCount, m_lPageCount, m_lPageNumber, m_lLOB, m_sFormName, m_sTriggerDate
Dim sDSN, sSQL
Dim i, m_sToday,lLob
Dim m_lDeptEid, m_lFacEid, m_lLocEid, m_lDivEid, m_lRegEid, m_lOprEid, m_lComEid, m_lCliEid
Dim m_sNow, m_bCustom, m_sParentForm
Dim m_lParentId
Dim m_lClaimID  'PJS MITS 7594, 10-17-2006
Dim m_lClaimType  'PJS MITS 7594, 10-17-2006

m_sTableName=Trim(Request("code"))

m_lLOB=Trim(Request("lob"))

sDSN=objSessionStr(SESSION_DSN)   'SECURITY_DSN when tablename=rm_sys_users


'-- ParentId to track the LOBs when m_lLOB is blank, MITS 5679
m_lParentId = Clng("0" & Request("ParentId"))
m_sParentForm = Request("ParentName")
m_lClaimID = Request("ClaimId")   'PJS MITS 7594, 10-17-2006


If m_lLOB = "" Then
	If m_sParentForm = "claim" Then
		GetLOB m_lParentId,sDSN
	End If
End If

m_lRecordCount=CLng("0" & Request("RecordCount"))
m_lPageCount=CLng("0" & Request("PageCount"))
m_lPageNumber=CLng("0" & Request("PageNumber"))
if m_lPageNumber=0 then m_lPageNumber=1
set m_objDomCodes = nothing
m_sFormName=Request("formname")
m_sTriggerDate=Request("triggerdate")
m_sNow=Now
m_sToday=Year(m_sNow)
m_bCustom=Request("custom")

'Response.Write "<br>m_sTableName" & m_sTableName
'Response.Write "<br>m_lLOB" & m_lLOB
'Response.Write "<br>sDSN" & sDSN
'Response.Write "<br>m_lRecordCount" & m_lRecordCount
'Response.Write "<br>m_lPageCount" & m_lPageCount
'Response.Write "<br>m_lPageNumber" & m_lPageNumber
'Response.Write "<br>m_sFormName" & m_sFormName
'Response.Write "<br>m_sTriggerDate" & m_sTriggerDate
'Response.Write "<br>m_sNow" & m_sNow
'Response.Write "<br>m_bCustom" & m_bCustom


If m_bCustom="" Then
	m_bCustom=0
End If

If Month(m_sNow) < 10 Then
	m_sToday = m_sToday & "0" & Month(m_sNow)
Else
	m_sToday = m_sToday & Month(m_sNow)
End If

If Day(m_sNow) < 10 Then
	m_sToday = m_sToday & "0" & Day(m_sNow)
Else
	m_sToday = m_sToday & Day(m_sNow)
End If

m_lDeptEid=Request("orgeid")

If m_lDeptEid>0 Then
	GetOrgEid m_lDeptEid, sDSN
End If

'Dim m_bDebug, m_bEnd
'm_bDebug=false
'm_bEnd=false
'if m_bDebug Then
	'Response.Write "<br>m_sTableName=" & m_sTableName
	'Response.Write "<br>m_lLOB=" & m_lLOB
	'Response.Write "<br>m_lRecordCount=" & m_lRecordCount
	'Response.Write "<br>m_lPageCount=" & m_lPageCount
	'Response.Write "<br>m_lPageNumber=" & m_lPageNumber
'end if
Function sDBDateFormat(sDate,sFormat) 
    
    Dim vTmp
    sDBDateFormat = ""
    If Trim(sDate) <> "" Then
        vTmp = DateSerial(cint(Left(sDate, 4)), cint(Mid(sDate, 5, 2)), cint(Mid(sDate, 7, 2)))
        sDBDateFormat = cdate(vTmp)
    End If
End Function

If m_sTableName="" Then Response.End
If lcase(m_sTableName) = "states" then

	sSQL = "SELECT STATES.STATE_ROW_ID, STATES.STATE_ID, STATES.STATE_NAME"
	sSQL = sSQL & " FROM STATES WHERE STATE_ROW_ID > 0 ORDER BY STATES.STATE_ID"
'added by ravi on 16th july 03 for TandE
elseif lcase(m_sTableName)="cust_rate" then
	 sSQL = "SELECT RATE_TABLE_ID, EFFECTIVE_DATE, EXPIRATION_DATE, TABLE_NAME FROM CUST_RATE"
    sSQL = sSQL & " WHERE CUST_RATE.CUSTOMER_EID = " & m_lLOB
	
elseif (lcase(m_sTableName)= "event_type" or lcase(m_sTableName)= "claim_type") Then
	Dim sLOBSQL, sLimitsSQL 
	Dim sLimits, vArray, vArray2
	
	if lcase(m_sTableName)="event_type" Then
		sLimits=objSessionStr(SESSION_EVENT_TYPE_LIMITS)
	elseif lcase(m_sTableName)="claim_type" Then
		sLimits=objSessionStr(SESSION_CLAIM_TYPE_LIMITS)
	end if
			
	If IsNumeric(m_lLOB) Then
		sLOBSQL = " AND (CODES.LINE_OF_BUS_CODE = " & m_lLOB & " OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0) "
	Else
		sLOBSQL = ""
	End If
			
	sLimitsSQL=""
	If Len(Trim(sLimits)) > 0 Then
		vArray=Split(sLimits, "|")
		For i = 0 To UBound(vArray)
		    vArray2 = Split(vArray(i), ",")
		    If CBool(vArray2(1)) = false then
				sLimitsSQL=sLimitsSQL & "," & vArray2(0)	        
		    End If
		Next
		if Len(Trim(sLimitsSQL)) > 0 then sLimitsSQL = " AND CODES.CODE_ID NOT IN (" & Mid(sLimitsSQL, 2) & ") "
	else
		sLimitsSQL=""
	end if
	
	'if m_bDebug Then
		'Response.write "<br>sLimitsSQL=" & sLimitsSQL
		'Response.Write "<br>sLOBSQL=" & sLOBSQL
	'end if
	
	sSQL = "SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC, C2.SHORT_CODE"
	sSQL = sSQL & " FROM CODES,CODES_TEXT,GLOSSARY, CODES C2"
	sSQL = sSQL & " WHERE GLOSSARY.SYSTEM_TABLE_NAME='" & UCase(m_sTableName) & "'"
	sSQL = sSQL & " AND C2.CODE_ID=CODES.RELATED_CODE_ID"
	sSQL = sSQL & " AND GLOSSARY.TABLE_ID=CODES.TABLE_ID"
	sSQL = sSQL & " AND CODES.CODE_ID=CODES_TEXT.CODE_ID"
	sSQL = sSQL & " AND CODES_TEXT.LANGUAGE_CODE=1033"
	sSQL = sSQL & " AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL)"
	sSQL = sSQL & sLOBSQL & sLimitsSQL

	If m_lDeptEid > 0 Then
		sSQL = sSQL & "	AND (CODES.ORG_GROUP_EID=" & m_lDeptEid
		sSQL = sSQL & "	OR CODES.ORG_GROUP_EID=" & m_lFacEid
		sSQL = sSQL & "	OR CODES.ORG_GROUP_EID=" & m_lLocEid
		sSQL = sSQL & "	OR CODES.ORG_GROUP_EID=" & m_lDivEid
		sSQL = sSQL & "	OR CODES.ORG_GROUP_EID=" & m_lRegEid
		sSQL = sSQL & "	OR CODES.ORG_GROUP_EID=" & m_lOprEid
		sSQL = sSQL & "	OR CODES.ORG_GROUP_EID=" & m_lComEid
		sSQL = sSQL & "	OR CODES.ORG_GROUP_EID=" & m_lCliEid
		sSQL = sSQL & "	OR CODES.ORG_GROUP_EID IS NULL "
		sSQL = sSQL & "	OR CODES.ORG_GROUP_EID=0) "
	Else
		sSQL = sSQL & "	AND (CODES.ORG_GROUP_EID IS NULL "
		sSQL = sSQL & "	OR CODES.ORG_GROUP_EID=0) "
	End If

	If m_sFormName=""  Then
		sSQL = sSQL & "	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) "
		'Mukul Added 6/23/2006;MITS 6274;It will take all the codes which have code effective trigger not equal to system date
		sSQL = sSQL & "	OR (CODES.TRIGGER_DATE_FIELD <>'SYSTEM_DATE') "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" & m_sToday & "' AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" & m_sToday & "') "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" & m_sToday & "' AND CODES.EFF_END_DATE>='" & m_sToday & "')) "
	ElseIf LCase(m_sFormName)="event" Then
		sSQL = sSQL & "	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" & m_sTriggerDate & "' AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" & m_sTriggerDate & "') "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" & m_sTriggerDate & "' AND CODES.EFF_END_DATE>='" & m_sTriggerDate & "') "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & "	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" & m_sToday & "' AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & "	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" & m_sToday & "') "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" & m_sToday & "' AND CODES.EFF_END_DATE>='" & m_sToday & "'))"
	'ElseIf LCase(m_sFormName)="claimgc" Or LCase(m_sFormName)="claimwc" Or LCase(m_sFormName)="claimva" Or LCase(m_sFormName)="claimdi" Then
	'pmittal5 MITS:11404 1/30/2008
	ElseIf LCase(m_sFormName)="claimgc" Or LCase(m_sFormName)="claimwc" Or LCase(m_sFormName)="claimva" Or LCase(m_sFormName)="claimdi" Or LCase(m_sFormName)="claim_supp" Then
		sSQL = sSQL & "	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE<='" & m_sTriggerDate & "' AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" & m_sTriggerDate & "') "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE<='" & m_sTriggerDate & "' AND CODES.EFF_END_DATE>='" & m_sTriggerDate & "') "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & "	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" & m_sToday & "' AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & "	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" & m_sToday & "') "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" & m_sToday & "' AND CODES.EFF_END_DATE>='" & m_sToday & "'))"
	ElseIf LCase(m_sFormName)="policy" Then
		sSQL = sSQL & "	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE<='" & m_sTriggerDate & "' AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" & m_sTriggerDate & "') "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE<='" & m_sTriggerDate & "' AND CODES.EFF_END_DATE>='" & m_sTriggerDate & "') "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & "	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" & m_sToday & "' AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & "	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" & m_sToday & "') "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" & m_sToday & "' AND CODES.EFF_END_DATE>='" & m_sToday & "'))"
	ElseIf LCase(m_sFormName)="funds" Then
		sSQL = sSQL & "	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE<='" & m_sTriggerDate & "' AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" & m_sTriggerDate & "') "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE<='" & m_sTriggerDate & "' AND CODES.EFF_END_DATE>='" & m_sTriggerDate & "') "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & "	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" & m_sToday & "' AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & "	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" & m_sToday & "') "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" & m_sToday & "' AND CODES.EFF_END_DATE>='" & m_sToday & "'))"
	End If
	
	sSQL = sSQL & " ORDER BY CODES.SHORT_CODE"

ElseIf Left(LCase(m_sTableName),9) = "glossary_" then
	sSQL = "SELECT GLOSSARY.TABLE_ID, GLOSSARY.SYSTEM_TABLE_NAME, GLOSSARY_TEXT.TABLE_NAME, NULL"
	sSQL = sSQL & " FROM GLOSSARY, GLOSSARY_TEXT"
	sSQL = sSQL & " WHERE GLOSSARY.TABLE_ID=GLOSSARY_TEXT.TABLE_ID"
	sSQL = sSQL & " AND GLOSSARY_TEXT.LANGUAGE_CODE=1033"
	sSQL = sSQL & " AND GLOSSARY.GLOSSARY_TYPE_CODE=" & Mid(m_sTableName,10)
		
ElseIf lcase(m_sTableName)="account" Then
	sSQL = "SELECT ACCOUNT_ID, NULL, ACCOUNT_NAME "
	sSQL = sSQL & " FROM ACCOUNT"
	sSQL = sSQL & " ORDER BY ACCOUNT_ID "
		
ElseIf LCase(m_sTableName) = "rm_sys_users" then
	sDSN = Application(APP_SECURITYDSN)
	sSQL = "SELECT USER_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME,USER_DETAILS_TABLE.LOGIN_NAME"
	sSQL = sSQL & " FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID"
	sSQL = sSQL & " AND USER_DETAILS_TABLE.DSNID = " & objSessionStr(SESSION_DSNID)
	sSQL = sSQL & " ORDER BY USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME"
elseif m_sTableName="TRANS_TYPES" then
	lLob=objSessionStr(SESSION_LOB)
	'PJS MITS 7594, 10-17-2006 - Retrieved ClaimType for Checking Reserve by ClaimType option
	if  m_lClaimID <> 0 then
		GetClaimType m_lClaimID, sDSN 
	end if		
	
	sSQL = "SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC,C2.SHORT_CODE"
	sSQL = sSQL & " FROM CODES, CODES_TEXT, GLOSSARY, CODES C2 "
	sSQL = sSQL & " WHERE GLOSSARY.SYSTEM_TABLE_NAME = '" & UCase(m_sTableName) & "' "
	if(lLob="" or lLob="0") then
		sSQL = sSQL & " AND C2.CODE_ID=CODES.RELATED_CODE_ID"
	else
	    'PJS MITS 7594, 10-17-2006 - Reserve By Claim Type option is checked
		if bReservesByClaimType (lLob) then
			sSQL = sSQL & " AND C2.CODE_ID=CODES.RELATED_CODE_ID AND C2.CODE_ID IN(SELECT RESERVE_TYPE_CODE FROM SYS_CLM_TYPE_RES WHERE LINE_OF_BUS_CODE=" & lLob & " AND CLAIM_TYPE_CODE = " & m_lClaimType  & ")"
	    else
			sSQL = sSQL & " AND C2.CODE_ID=CODES.RELATED_CODE_ID AND C2.CODE_ID IN(SELECT RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES WHERE LINE_OF_BUS_CODE=" & lLob & ")"
		end if	
	end if
	sSQL = sSQL & " AND GLOSSARY.TABLE_ID = CODES.TABLE_ID "
	sSQL = sSQL & " AND CODES.CODE_ID = CODES_TEXT.CODE_ID "
	sSQL = sSQL & " AND CODES_TEXT.LANGUAGE_CODE = 1033 "
	sSQL = sSQL & " AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) "
	
	'Added by Nishith Gupta 10/13/2006 MITS 7785
	'To enable the trigger date checking for the Transaction Types codes 
	sSQL = sSQL & "	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) "
	sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) "
	sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" & m_sToday & "' AND CODES.EFF_END_DATE IS NULL) "
	sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" & m_sToday & "') "
	sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" & m_sToday & "' AND CODES.EFF_END_DATE>='" & m_sToday & "') "
	ssql = ssql & " OR (CODES.TRIGGER_DATE_FIELD IS NOT NULL AND CODES.TRIGGER_DATE_FIELD != 'SYSTEM_DATE')) "
	'End of Code

	'PJS 7594 -commented - taking 0 also as LOB but we want 241,242,243 etc.
	'If IsNumeric(lLob) Then
	if Not(lLob="" or lLob="0") then
		If GetIsLOBRequired("TRANS_TYPES", sDSN) Then
				sSQL = sSQL & " AND (CODES.LINE_OF_BUS_CODE = " & lLob & " OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0) "
		End If
	End If
	sSQL = sSQL & " ORDER BY CODES.SHORT_CODE"
	
Else    'fetch all codes in the table
	sSQL = "SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC,C2.SHORT_CODE"
	sSQL = sSQL & " FROM CODES, CODES_TEXT, GLOSSARY, CODES C2 "
	sSQL = sSQL & " WHERE GLOSSARY.SYSTEM_TABLE_NAME = '" & UCase(m_sTableName) & "' "
	If bCustomButtons("getcode.asp?" & m_sTableName) And m_bCustom=0 Then
		sSQL = sSQL & CustomButtons_GetCodeFilter()
	End If
	sSQL = sSQL & " AND C2.CODE_ID=CODES.RELATED_CODE_ID"
	sSQL = sSQL & " AND GLOSSARY.TABLE_ID = CODES.TABLE_ID "
	sSQL = sSQL & " AND CODES.CODE_ID = CODES_TEXT.CODE_ID "
	sSQL = sSQL & " AND CODES_TEXT.LANGUAGE_CODE = 1033 "
	sSQL = sSQL & " AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) "

	If m_lDeptEid > 0 Then
		sSQL = sSQL & "	AND (CODES.ORG_GROUP_EID=" & m_lDeptEid
		sSQL = sSQL & "	OR CODES.ORG_GROUP_EID=" & m_lFacEid
		sSQL = sSQL & "	OR CODES.ORG_GROUP_EID=" & m_lLocEid
		sSQL = sSQL & "	OR CODES.ORG_GROUP_EID=" & m_lDivEid
		sSQL = sSQL & "	OR CODES.ORG_GROUP_EID=" & m_lRegEid
		sSQL = sSQL & "	OR CODES.ORG_GROUP_EID=" & m_lOprEid
		sSQL = sSQL & "	OR CODES.ORG_GROUP_EID=" & m_lComEid
		sSQL = sSQL & "	OR CODES.ORG_GROUP_EID=" & m_lCliEid
		sSQL = sSQL & "	OR CODES.ORG_GROUP_EID IS NULL "
		sSQL = sSQL & "	OR CODES.ORG_GROUP_EID=0) "
	Else
		sSQL = sSQL & "	AND (CODES.ORG_GROUP_EID IS NULL "
		sSQL = sSQL & "	OR CODES.ORG_GROUP_EID=0) "
	End If
    
    'PJS MITS 6976, 09/22/2006 -added below- checked effective dates for claimant type codes also.
	If m_sFormName="" Or LCase(m_sFormName)="claimant" Then
		sSQL = sSQL & "	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) "
		'Mukul Added 6/23/2006;MITS 6274;It will take all the codes which have code effective trigger not equal to system date
		sSQL = sSQL & "	OR (CODES.TRIGGER_DATE_FIELD <>'SYSTEM_DATE') "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" & m_sToday & "' AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" & m_sToday & "') "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" & m_sToday & "' AND CODES.EFF_END_DATE>='" & m_sToday & "')) "
	ElseIf LCase(m_sFormName)="event" Then
		sSQL = sSQL & "	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" & m_sTriggerDate & "' AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" & m_sTriggerDate & "') "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" & m_sTriggerDate & "' AND CODES.EFF_END_DATE>='" & m_sTriggerDate & "') "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & "	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" & m_sToday & "' AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & "	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" & m_sToday & "') "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" & m_sToday & "' AND CODES.EFF_END_DATE>='" & m_sToday & "'))"
	ElseIf LCase(m_sFormName)="claimgc" Or LCase(m_sFormName)="claimwc" Or LCase(m_sFormName)="claimva" Or LCase(m_sFormName)="claimdi" Then
		sSQL = sSQL & "	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE<='" & m_sTriggerDate & "' AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" & m_sTriggerDate & "') "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE<='" & m_sTriggerDate & "' AND CODES.EFF_END_DATE>='" & m_sTriggerDate & "') "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & "	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" & m_sToday & "' AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & "	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" & m_sToday & "') "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" & m_sToday & "' AND CODES.EFF_END_DATE>='" & m_sToday & "'))"
	ElseIf LCase(m_sFormName)="policy" Then
		sSQL = sSQL & "	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE<='" & m_sTriggerDate & "' AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" & m_sTriggerDate & "') "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE<='" & m_sTriggerDate & "' AND CODES.EFF_END_DATE>='" & m_sTriggerDate & "') "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & "	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" & m_sToday & "' AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & "	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" & m_sToday & "') "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" & m_sToday & "' AND CODES.EFF_END_DATE>='" & m_sToday & "'))"
	ElseIf LCase(m_sFormName)="funds" Then
		sSQL = sSQL & "	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE<='" & m_sTriggerDate & "' AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" & m_sTriggerDate & "') "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE<='" & m_sTriggerDate & "' AND CODES.EFF_END_DATE>='" & m_sTriggerDate & "') "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & "	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" & m_sToday & "' AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & "	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" & m_sToday & "') "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" & m_sToday & "' AND CODES.EFF_END_DATE>='" & m_sToday & "'))"
	ElseIf LCase(m_sFormName)="employee" Then
		sSQL = sSQL & "	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE<='" & m_sTriggerDate & "' AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" & m_sTriggerDate & "') "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='FUNDS.TRANS_DATE' AND CODES.EFF_START_DATE<='" & m_sTriggerDate & "' AND CODES.EFF_END_DATE>='" & m_sTriggerDate & "') "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & "	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" & m_sToday & "' AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & "	OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" & m_sToday & "') "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" & m_sToday & "' AND CODES.EFF_END_DATE>='" & m_sToday & "'))"
	'--PJS, MITS 7536  - 12/13/2006 - Added code for System date filtering
	Else
	    sSQL = sSQL & "	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) "
		sSQL = sSQL & "	OR (CODES.TRIGGER_DATE_FIELD <>'SYSTEM_DATE') "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" & m_sToday & "' AND CODES.EFF_END_DATE IS NULL) "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" & m_sToday & "') "
		sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" & m_sToday & "' AND CODES.EFF_END_DATE>='" & m_sToday & "')) "
    '--PJS, MITS 7536  - 12/13/2006 - till here
	End If
	
	'-- Aashish Bhateja 
	'-- MITS 5679; 05/26/2005
	If IsNumeric(m_lLOB) Then
		sLOBSQL = " AND (CODES.LINE_OF_BUS_CODE = " & m_lLOB & " OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0) "
	Else
		sLOBSQL = ""
	End If
	sSQL = sSQL & sLOBSQL
	
	sSQL = sSQL & " ORDER BY CODES.SHORT_CODE"
End if
'if m_bDebug Then
	'Response.write "<br>sSQL=" & sSQL
	'Response.Write "<br>sDSN=" & sDSN
'end if

GetCodes sDSN, sSQL

If Not m_objDomCodes is nothing Then
	Set objStyle=CreateObject("Microsoft.XMLDOM")
	objStyle.Load Application(APP_DATAPATH) & "getcode.xsl"
	Response.Write m_objDomCodes.transformNode(objStyle.documentElement)
	Response.End
Else
	Response.Write "Error occured while retrieving codes. " & ERR_CONTACT
	Response.End
End If

Response.End

Sub GetCodes(sDSN, sSQL)
	Dim CodeDesc, CodeID, ShortCode, ParentCode, TriggerDateField, EffStartDate, EffEndDate
	Dim TriggerTable, TriggerField
	Dim iStart, iLength, lStartAt, lCounter 
	Dim objXMLRoot, objXMLElem
	Dim r, henv, db, rs, sCountSQL
	
	set r = CreateObject("DTGRocket")
	henv = r.DB_InitEnvironment()
	db = r.DB_OpenDatabase(henv, sDSN, 0)
	
	if m_lRecordCount=0 Then
		'if user hit next/previous/first/last link record count was passed in
		iStart = InStr(1, sSQL, "FROM")
		iLength = InStr(1, sSQL, "ORDER BY") - iStart
		if iLength > 0 Then
			sCountSQL = "SELECT COUNT(*) " & Mid(sSQL, iStart, iLength)
		else
			sCountSQL = "SELECT COUNT(*) " & Mid(sSQL, iStart)
		end if
		
		'If m_bDebug Then
			'Response.write "<br>sCountSQL=" & sCountSQL
			'if m_bEnd Then 
				'r.DB_FreeEnvironment CLng(henv)
				'set r = Nothing
				'Response.end
			'end if
		'end if

		rs=r.DB_CreateRecordset(db, sCountSQL, 3, 0)
		r.DB_GetData rs,1,m_lRecordCount
		r.DB_CloseRecordset CInt(rs), 2
	End If
	
	If m_lRecordCount = 0 Then
		r.DB_CloseDatabase cint(db)
		r.DB_FreeEnvironment cint(henv)
		Response.Write "No Codes Available"
		Response.End
	End If

	rs = r.DB_CreateRecordset(db, sSQL, 3, 0)
	
	m_lPageCount = int(m_lRecordCount/mc_iPerPageMax)
	if (m_lRecordCount Mod mc_iPerPageMax) <> 0 then m_lPageCount = m_lPageCount + 1
	if m_lPageNumber=1 then
		lStartAt=1
	Else
		lStartAt = ((m_lPageNumber - 1) * mc_iPerPageMax) + 1
	End if

	'move to the current page
	lCounter = 1
	If lStartAt > 1 Then
		Do While lCounter < lStartAt
			lCounter = lCounter+1
			r.DB_MoveNext rs
		Loop
	End If
	
	'If m_bDebug Then
		'Response.Write "<br>m_lRecordCount=" & m_lRecordCount
		'Response.Write "<br>m_lPageCount=" & m_lPageCount
		'Response.Write "<br>m_lPageNumber=" & m_lPageNumber
		'Response.Write "<br>lStartAt=" & lStartAt
		'Response.Write "<br>lCounter=" & lCounter
		'if m_bEnd Then Response.end
	'end if
	
	Set m_objDomCodes=CreateObject("Microsoft.XMLDOM")
	Set objXMLRoot = m_objDomCodes.appendChild(m_objDomCodes.createElement("codes"))
				
	lcounter=0
	If LCase(m_sTableName) = "rm_sys_users" then
		Dim sFirstName
		Do Until r.DB_Eof(rs) Or lCounter >= mc_iPerPageMax 	
			r.DB_GetData rs, 1, CodeID    'user id
			r.DB_GetData rs, 2, CodeDesc   'lastname
			r.DB_GetData rs, 3, sFirstName
			r.DB_GetData rs, 4, ShortCode   'loginname
			
			if sFirstName & "" <> "" then CodeDesc = sFirstName & " " & CodeDesc
			CodeDesc = Replace(CodeDesc & "", Chr(34), "''")		
			Set objXMLElem = m_objDomCodes.createElement("code")
			objXMLElem.Text = CodeDesc & ""
			objXMLElem.setAttribute "id", CodeID
			objXMLElem.setAttribute "shortcode", ShortCode & ""
			objXMLRoot.appendChild objXMLElem
			Set objXMLElem=Nothing
			lCounter = lCounter + 1
			r.DB_MoveNext rs
		Loop
	elseif LCase(m_sTableName) = "cust_rate" Then
	dim efDate,exDate
		Do Until r.DB_Eof(rs) Or lCounter >= mc_iPerPageMax 	
			r.DB_GetData rs, 1, CodeID
			r.DB_GetData rs, 4, ShortCode
			r.DB_GetData rs, 4, CodeDesc
			r.DB_GetData rs, 2, efDate
			r.DB_GetData rs, 3, exDate
			CodeDesc = Replace(CodeDesc & "", Chr(34), "''") & " - Effective Date " & sDBDateFormat(efdate,"short date") & " Expiration Date " & sDBDateFormat(exDate,"short date")
			Set objXMLElem = m_objDomCodes.createElement("code")
			objXMLElem.Text = CodeDesc & ""
			objXMLElem.setAttribute "id", CodeID & ""
			objXMLElem.setAttribute "shortcode", ShortCode & ""
			objXMLRoot.appendChild objXMLElem
			Set objXMLElem=Nothing
			lCounter=lCounter+1
			r.DB_MoveNext(rs)
		Loop	
	elseif LCase(m_sTableName) = "states" Then
		Do Until r.DB_Eof(rs) Or lCounter >= mc_iPerPageMax 	
			r.DB_GetData rs, 1, CodeID
			r.DB_GetData rs, 2, ShortCode
			r.DB_GetData rs, 3, CodeDesc
			CodeDesc = Replace(CodeDesc & "", Chr(34), "''")
			Set objXMLElem = m_objDomCodes.createElement("code")
			objXMLElem.Text = CodeDesc & ""
			objXMLElem.setAttribute "id", CodeID & ""
			objXMLElem.setAttribute "shortcode", ShortCode & ""
			objXMLRoot.appendChild objXMLElem
			Set objXMLElem=Nothing
			lCounter=lCounter+1
			r.DB_MoveNext(rs)
		Loop	
	else
		Do Until r.DB_Eof(rs) Or lCounter >= mc_iPerPageMax 	
			r.DB_GetData rs, 1, CodeID
			r.DB_GetData rs, 2, ShortCode
			r.DB_GetData rs, 3, CodeDesc
			r.DB_GetData rs, 4, ParentCode 'changed by ravi on april 8th 03 to get parent code

			CodeDesc = Replace(CodeDesc & "", Chr(34), "''")
			Set objXMLElem = m_objDomCodes.createElement("code")
			objXMLElem.Text = CodeDesc & ""
			objXMLElem.setAttribute "id", CodeID & ""
			objXMLElem.setAttribute "shortcode", ShortCode & ""
			objXMLElem.setAttribute "parentcode", Replace(ParentCode & "", Chr(34), "") 'changed by ravi on april 8th 03 to get parent code
			objXMLRoot.appendChild objXMLElem
			Set objXMLElem=Nothing
			lCounter=lCounter+1
			r.DB_MoveNext(rs)
		Loop
	end if
	
	objXMLRoot.setAttribute "recordcount", m_lRecordCount
	objXMLRoot.setAttribute "pagecount", m_lPageCount
	objXMLRoot.setAttribute "tablename", m_sTableName
	objXMLRoot.setAttribute "triggerdate", m_sTriggerDate
	objXMLRoot.setAttribute "FormName", m_sFormName
	
	'note:  getcode.xsl will add pagination links if m_lPageCount != 1
			
	If m_lPageCount > 1 then
						
		If m_lPageNumber = 1 Then 
			objXMLRoot.removeAttribute "previouspage" 
		Else 
			objXMLRoot.setAttribute "previouspage", m_lPageNumber - 1
		End If
				
		If m_lPageNumber > 1 Then 
			objXMLRoot.setAttribute "firstpage", "1" 
		Else 
			objXMLRoot.removeAttribute "firstpage"
		End If
				
		If m_lPageNumber = m_lPageCount Then 
			objXMLRoot.removeAttribute "nextpage" 
		Else 
			objXMLRoot.setAttribute "nextpage", m_lPageNumber + 1
		End If
				
		objXMLRoot.setAttribute "thispage", m_lPageNumber
		objXMLRoot.setAttribute "pagecount", m_lPageCount
		If m_lPageNumber < m_lPageCount Then 
			objXMLRoot.setAttribute "lastpage", m_lPageCount 
		Else 
			objXMLRoot.removeAttribute "lastpage"
		End if
			
	End if
		
	'If m_bDebug Then m_objDomCodes.save "C:\Riskmaster.Net.6\TestFiles\codes.xml"
	
	r.DB_CloseRecordset cint(rs), 2
	r.DB_CloseDatabase cint(db)
	r.DB_FreeEnvironment cint(henv)
	set r = nothing
End Sub

Sub GetOrgEid(lDeptEid, sDSN)
	Dim r, henv, db
	Dim sSQL, rs
	
	Set r = CreateObject("DTGRocket")
	henv = r.DB_InitEnvironment()
	db = r.DB_OpenDatabase(henv, sDSN, 0)
	
	sSQL = "SELECT FACILITY_EID, LOCATION_EID, DIVISION_EID, REGION_EID, OPERATION_EID, COMPANY_EID, CLIENT_EID FROM ORG_HIERARCHY WHERE DEPARTMENT_EID=" & lDeptEid
	rs=r.DB_CreateRecordset(db, sSQL, 3, 0)

	r.DB_GetData rs,1,m_lFacEid
	r.DB_GetData rs,2,m_lLocEid
	r.DB_GetData rs,3,m_lDivEid
	r.DB_GetData rs,4,m_lRegEid
	r.DB_GetData rs,5,m_lOprEid
	r.DB_GetData rs,6,m_lComEid
	r.DB_GetData rs,7,m_lCliEid

	r.DB_CloseRecordset cint(rs), 2
	r.DB_CloseDatabase cint(db)
	r.DB_FreeEnvironment cint(henv)
	Set r = Nothing
End Sub

Sub GetLOB(lClaimId, sDSN)
	Dim r, henv, db
	Dim sSQL, rs
	
	Set r = CreateObject("DTGRocket")
	henv = r.DB_InitEnvironment()
	db = r.DB_OpenDatabase(henv, sDSN, 0)
	
	sSQL = "SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID=" & lClaimId
	rs=r.DB_CreateRecordset(db, sSQL, 3, 0)

	r.DB_GetData rs,1,m_lLOB

	r.DB_CloseRecordset cint(rs), 2
	r.DB_CloseDatabase cint(db)
	r.DB_FreeEnvironment cint(henv)
	Set r = Nothing
End Sub

function GetIsLOBRequired(sTableName, sDSN)
	Dim r, henv, db
	Dim sSQL, rs, sLOBRequired
	
	Set r = CreateObject("DTGRocket")
	henv = r.DB_InitEnvironment()
	db = r.DB_OpenDatabase(henv, sDSN, 0)
	
	sSQL = "SELECT LINE_OF_BUS_FLAG FROM GLOSSARY WHERE SYSTEM_TABLE_NAME='" & sTableName & "'"
	rs=r.DB_CreateRecordset(db, sSQL, 3, 0)
	If Not r.DB_Eof(rs) Then
		r.DB_GetData rs,1,sLOBRequired
		sLOBRequired = sLOBRequired & ""
	End If
	
	r.DB_CloseRecordset cint(rs), 2
	r.DB_CloseDatabase cint(db)
	r.DB_FreeEnvironment cint(henv)
	Set r = Nothing
	
	If (sLOBRequired <> 0) AND (sLOBRequired <> "") Then
		GetIsLOBRequired = True
	End If
End Function

'PJS MITS 7594,10-17-2006 - Check Reserve By Claim type option is set in utilities or not
function bReservesByClaimType(lLOB) 
	Dim r, henv, db
	Dim sSQL, rs, sReservesByClaimType
	
	Set r = CreateObject("DTGRocket")
	henv = r.DB_InitEnvironment()
	db = r.DB_OpenDatabase(henv, sDSN, 0)
	
	sSQL = "SELECT RES_BY_CLM_TYPE FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE='" & lLOB & "'"
	rs=r.DB_CreateRecordset(db, sSQL, 3, 0)
	If Not r.DB_Eof(rs) Then
		r.DB_GetData rs,1,sReservesByClaimType
		If sReservesByClaimType <> 0 Then
		   bReservesByClaimType = True
		Else
		   bReservesByClaimType = False
		End if
	End If
	
	r.DB_CloseRecordset cint(rs), 2
	r.DB_CloseDatabase cint(db)
	r.DB_FreeEnvironment cint(henv)
	Set r = Nothing
End Function	

'PJS MITS 7594,10-17-2006 - Get claim type for the claim	
Sub GetClaimType(lClaimId, sDSN)
	Dim r, henv, db
	Dim sSQL, rs
	
	Set r = CreateObject("DTGRocket")
	henv = r.DB_InitEnvironment()
	db = r.DB_OpenDatabase(henv, sDSN, 0)
	
	sSQL = "SELECT CLAIM_TYPE_CODE FROM CLAIM WHERE CLAIM_ID = " & lClaimId
	rs=r.DB_CreateRecordset(db, sSQL, 3, 0)

	r.DB_GetData rs,1,m_lClaimType 

	r.DB_CloseRecordset cint(rs), 2
	r.DB_CloseDatabase cint(db)
	r.DB_FreeEnvironment cint(henv)
	Set r = Nothing
End Sub
%>