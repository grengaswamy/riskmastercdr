<% Option Explicit %>

<!-- #include file ="session.inc" -->
<!-- #include file ="criteriautils.inc" -->

<%Public objSession
initSession objSession
%> 

<%
Function OutputDateTime(sDTTM)
	Dim sDate, sTime
	If Len(sDTTM) <> 14 Then
		OutputDateTime = ""
		Exit Function
	End If

	sDate = FormatDateTime(DateSerial(left(sDTTM, 4), mid(sDTTM, 5, 2), mid(sDTTM, 7, 2)), vbShortDate)
	sTime = FormatDateTime(TimeSerial(mid(sDTTM, 9, 2), mid(sDTTM, 11, 2), right(sDTTM, 2)), vbLongTime)

	OutputDateTime = sDate & " " & sTime
End Function
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<META HTTP-EQUIV="Refresh" CONTENT="3">

<html>
	<head>
		<title>Preview Status</title>
		<!--[Rajeev 11/15/2002 CSS Consolidation] link rel="stylesheet" href="sm.css" type="text/css"/-->
		<link rel="stylesheet" href="RMNet.css" type="text/css" />
		<script language="JavaScript" SRC="smqueue.js"></script>
		<script language="JavaScript">
		var ns, ie, ieversion;
		var browserName = navigator.appName;                   // detect browser 
		var browserVersion = navigator.appVersion;
		if (browserName == "Netscape")
		{
			ie=0;
			ns=1;
		}
		else		//Assume IE
		{
			ieversion=browserVersion.substr(browserVersion.search("MSIE ")+5,1);
			ie=1;
			ns=0;
		}


		function onLoadDoc()
		{
			self.onblur=closewnd;
		}
		function closewnd()
		{
			window.close();
		}

		</script>
	</head>
	<body onload="onLoadDoc()">

	<form name="frmData">
		<input type="hidden" name="jobs" value="" />
		<table bgcolor="white" border="0" width="100%" height="0">
	<%
	Dim r
	Dim sSQL
	Dim henv
	Dim db, db2
	Dim rs, rs2
	Dim vStartDTTM, bComplete, bAssigned, vAssignedDTTM, lJobID, vOutputPathURL, vMsg, bErrorFlag
	Dim b
	
	If objSessionStr("PreviewJobID") = "0" Then
		Response.Write "<script>closewnd();</script>"
		Response.End
	End If
	If objSessionStr("PreviewJobID") <> "-1" And objSessionStr("PreviewJobID") <> "" Then

		sSQL = "SELECT JOB_ID,OUTPUT_PATH_URL,OUTPUT_PATH,ASSIGNED,ASSIGNED_DTTM,COMPLETE,ERROR_FLAG FROM SM_JOBS WHERE "
		sSQL = sSQL & " SM_JOBS.JOB_ID = " & objSessionStr("PreviewJobID") & " AND SM_JOBS.USER_ID = " & objSessionStr("UserID") & " AND (ARCHIVED = 0 OR ARCHIVED IS NULL)"
		sSQL = sSQL & " ORDER BY JOB_ID DESC"
		' sSQL = sSQL & " ORDER BY ASSIGNED DESC, COMPLETE DESC,COMPLETE_DTTM DESC"

		Set r = CreateObject("DTGRocket")

		henv = r.DB_InitEnvironment()

		db = r.DB_OpenDatabase(henv, Application("SM_DSN"), 0)
		db2 = r.DB_OpenDatabase(henv, Application("SM_DSN"), 0)

		rs = r.DB_CreateRecordset(db, sSQL, 3, 0)
		If Not r.DB_EOF(rs) Then
			r.DB_GetData rs, "JOB_ID", lJobID
			r.DB_GetData rs, "OUTPUT_PATH_URL", vOutputPathURL
			r.DB_GetData rs, "ASSIGNED", bAssigned
			r.DB_GetData rs, "ASSIGNED_DTTM", vAssignedDTTM
			r.DB_GetData rs, "COMPLETE", bComplete
			r.DB_GetData rs, "ERROR_FLAG", bErrorFlag
	%>

			<tr>
				<td valign="middle" width="30%" align="left"><img src="img/anglobe.gif"></td>
				<td class1="td8" width="*" align="left">
			<%	If Not bAssigned Then 
			%>
					<font color="6e6e6e" size="3"><b>Waiting...</b></font>
			<%	ElseIf Not bComplete Then 
			%>
					<font color="blue" size="3"><b>Running</b> (Started @ <% = OutputDateTime(vAssignedDTTM) %>)
					<br />
			<% 
					rs2 = r.DB_CreateRecordset(db2, "SELECT MSG FROM SM_JOB_LOG WHERE JOB_ID = " & lJobID & " AND MSG_TYPE = 'progress' ORDER BY LOG_ID DESC", 3, 0)
					If Not r.DB_EOF(rs2) Then
						r.DB_GetData rs2, 1, vMsg
						vMsg = vMsg & ""
						vMsg = Replace(vMsg, vbCrLf, "<br />")
						vMsg = Replace(vMsg, vbCr, "<br />")
						vMsg = Replace(vMsg, vbLf, "<br />")
						Response.Write vMsg
					End If
					r.DB_CloseRecordset CInt(rs2), 2
			%>
					</font>
			<%	Else' complete
					If bErrorFlag = -1 Then 
			%>
						<font color="red"><b>Error Occurred</b>
			<%
						rs2 = r.DB_CreateRecordset(db2, "SELECT MSG FROM SM_JOB_LOG WHERE JOB_ID = " & lJobID & " AND MSG_TYPE = 'error' ORDER BY LOG_ID DESC", 3, 0)
						If Not r.DB_EOF(rs2) Then
							r.DB_GetData rs2, 1, vMsg
							vMsg = vMsg & ""
							vMsg = Replace(vMsg, vbCrLf, "<br />")
							vMsg = Replace(vMsg, vbCr, "<br />")
							vMsg = Replace(vMsg, vbLf, "<br />")
							Response.Write vMsg
						End If
						r.DB_CloseRecordset CInt(rs2), 2
						Response.Write "<br />" & vMsg
						Response.End 
			%>
						</font>
			<%		Else
						Response.Write "<script>closewnd();</script>"
						Response.End
					End If
		
				End If 
	%>
			</tr>
	<%
		End If

		r.DB_CloseRecordset CInt(rs), 2
		r.DB_CloseDatabase CInt(db)
		r.DB_CloseDatabase CInt(db2)

		r.DB_FreeEnvironment CLng(henv)

		Set r = Nothing
			
	ElseIf objSessionStr("PreviewJobID") = "-1" Then
		
		Response.Write "<table align=""center"" width=""100%"" border=""0"">"_
						&"<tr>"_
							&"<td width=""30%"" align=""left"" valign=""middle""><img src=""img/anglobe.gif""></td>"_
							&"<td width=""*"" valign=""middle"" align=""left""><font color=""6e6e6e"" size=""3""><b>Waiting...</b></font></td>"_
						&"</tr>"_
					&"</table>"
	Else
		Response.Write "<script>closewnd();</script>"
		Response.Redirect "smwdPreviewStatus.asp"
	End If
	%>
		</table>
		<input type="hidden" name="hdJOBID" value="<%=objSessionStr("PreviewJobID")%>" />
	</form>
	</body>
</html>