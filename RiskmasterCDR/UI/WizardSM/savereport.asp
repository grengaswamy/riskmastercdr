<html>
	<head>
		<link rel="stylesheet" href="RMNet.css" type="text/css" />
		<title>Save Report</title>

		<script language="JavaScript" SRC="SMD.js"></script>
		<script>
			function onCancel()
			{
				if (m_FLAG !="TRUE")   //Tanu: added for MITS 9812
				{
					window.opener.window.document.frmData.hdPost.value = "";
				}
				window.close();
			}
		</script>
	</head>
  
  	<body class="8pt" onunload="onCancel();"> 
		<form name="frmData">
		<table class="formsubtitle" border="0" width="100%">
			<tr>
				<td class="ctrlgroup">Save Report</td>
			</tr>
		</table>
		<br>
		<table border="0" width="100%" align="center">
			<tr>
				<td align="right" width="40%">&nbsp;&nbsp;Report Name:&nbsp;</td>
				<td><input name="txtFileName" type="text" size="40%"></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td align="right" width1="50%">&nbsp;&nbsp;Report Description:&nbsp;</td>
				<td><input name="txtFileDesc" type="text" size="40%" ></td>
			</tr>
		</table>
		<br>
		

			<hr>
			
		<table width="100%" border="0">
			<tr>
				<td width="20%">&nbsp;</td>
				<td width="60%" align="center">
					<input type="button" class="button" style="width:20%" name="up" value="OK" onClick="ReportName('<%=Request.QueryString("TABID")%>', '<%=Request.QueryString("POST")%>')">
					<input type="button" class="button" style="width:20%" name="up" value="Cancel" onClick="onCancel();">
				</td>
				<td width="20%">&nbsp;</td>
			</tr>
		</table>
		</form>
	</body>
</html>