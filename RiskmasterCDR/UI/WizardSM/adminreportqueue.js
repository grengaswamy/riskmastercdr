// Author: Denis Basaric, 03/28/2000
// Last Modified by: J. Partin 7/10/2000    Hacked for use in SM Report Queue

function getSelJobs()
{
	var sJobs=new String();
	for(var i=0;i<document.frmData.length;i++)
	{
		var objElem=document.frmData.elements[i];
		var sName=new String(objElem.name);
		if(sName.substring(0,6)=="seljob" && objElem.checked)
		{
			if(sJobs!="")
				sJobs=sJobs + ",";
			sJobs=sJobs + sName.substring(6,sName.length);
		}
	}
	return sJobs;
}

function DeleteJob()
{
	var sJobs=getSelJobs();
	if(sJobs=="")
	{
		alert("Please select a report job you would like to delete.");
		return true;
	}
	var ret = confirm('Are you sure you want to delete selected Report(s)?');
	if(ret == false)
	{
		document.location="adminreportqueue.asp"
		return false;
	}
	document.location="adminreportdeletejob.asp?jobs=" + escape(sJobs);
	return true;
}

 
