<% OPTION EXPLICIT %>
<!-- #include file ="session.inc" -->
<!-- #include file ="dbutil.inc" -->
<!-- #include file ="generic.asp" -->

<html>
	<head>
		<title>Report Criteria</title>
		<link rel="stylesheet" href="RMNet.css" type="text/css" />
<SCRIPT LANGUAGE="VBScript" RUNAT=SERVER>
dim smi
dim lReportID, sReportName
Dim sXML, objXML, objNode, sWhere
Dim lJobID

lReportID = CLng(Request("reportid"))
sReportName = Request("REPORTNAME")

'Fetch this Report XML
OpenSMDatabase
sWhere = "SM_REPORTS_PERM.USER_ID = " & objSessionStr("UserID") & " AND SM_REPORTS.REPORT_ID = SM_REPORTS_PERM.REPORT_ID AND SM_REPORTS.REPORT_ID = " & lReportID
sXML = GetSingleSMValue("REPORT_XML","SM_REPORTS,SM_REPORTS_PERM",sWhere)
CloseSMDatabase
If sXML = "" Then
	Response.Write "Error: Selected report could not be found on the server."
	Response.End
end if

'Load this Report XML 
Set objXML = CreateObject("Microsoft.XMLDOM")
if (objXML.loadXML(sXML) = false) then
	Response.Write "Error: Selected report could not be loaded.  Please try uploading it again."
	Response.End
end if

'Determine what type of report this is. (And what engine to run.)
Set objNode = objXML.selectSingleNode("/report")
If IsEmpty(objNode) or objNode is nothing then
	Response.Write "Error: Selected report does not have a top level report element with the 'type' attribute.  Please try uploading it again."
	Response.End
end if	
Select Case objNode.getAttribute("type")
	Case "1","2","3","4","5"	' Known Custom Reports (OSHA & Exec Summary)
		objSession("reportcriteriaxml") = sXML
		Response.Redirect "CriteriaEdit.asp?reportid=" & cstr(lreportID)
		Response.End
	Case Else	' Assume SortMaster
		' Generate criteria page
		'''Set smi = CreateObject("SMInterface.SMI")
		Dim DOCUMENT_PATH
		dim smwdi
		dim errorHTML
		dim sHTTP
		dim sPath, l, pageError, lDocPathType

		'Neelima
		DOCUMENT_PATH= objSessionStr(SESSION_DOCPATH)	
		errorHTML = ""
		
		Set smwdi = Nothing

		On Error Resume Next
		set smwdi = CreateObject("InitSMList.CUtility")
		On Error Goto 0

		If smwdi Is Nothing Then
			Call ShowError(pageError, "Call to CUtility class failed.", True)
		End If
		

		if ucase(Request.ServerVariables("HTTPS")) = "ON" then	sHTTP = "https://" else	sHTTP = "http://"

		sPath=Request.ServerVariables("SCRIPT_NAME")
		l=InStrRev(sPath,"/")
		If l>0 Then
			sPath=Left(sPath,l)
		Else
			sPath="/"
		End If
						
		lDocPathType = objSessionStr(SESSION_DOCPATHTYPE)
		If smwdi.runReportQueued2(objSessionStr("LoginName"), objSessionStr("UserID"), objSessionStr("DSN"), Application("SM_DSN"),DOCUMENT_PATH, sHTTP & Request.ServerVariables("SERVER_NAME") & sPath & "smrepserve.asp", errorHTML,lReportID, lJobID, Request.QueryString("OUTPUTTYPE"), lDocPathType) then
			
			'-- Preview Functionality -- Delete the posted report from SM_REPORT
			Call smwdi.DeletePostedReport(Application("SM_DSN"),lReportID)

			objSession("PreviewJobID") = lJobID
			Response.Redirect "smwdPreview.asp?JOBID=" & lJobID & "&TABID=" & Request.QueryString("TABID")
		Else
			'-- Preview Functionality -- Delete the posted report from SM_REPORT
			Call smwdi.DeletePostedReport(Application("SM_DSN"),lReportID)
		End If	

		Set smwdi = Nothing
End Select


</SCRIPT>
</head>
</html>