<!-- #include file ="sessionInitialize.asp" -->
<%Public objSession
initSession objSession
%> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html><head><title></title>
<link rel="stylesheet" href="RMNet.css" type="text/css" />
<script language="JavaScript">
var ns, ie, ieversion;
var browserName = navigator.appName;                   // detect browser 
var browserVersion = navigator.appVersion;
if (browserName == "Netscape")
{
	ie=0;
	ns=1;
}
else		//Assume IE
{
	ieversion=browserVersion.substr(browserVersion.search("MSIE ")+5,1);
	ie=1;
	ns=0;
}
function ViewReport(Id)
{
	var obj=eval("document.frmData.reportid");
		if(obj==null)
		{
			alert("No reports available.");
			return false;
		}
		obj.value=Id;
		document.frmData.submit();
		return true;
}

function onPageLoaded()
{
	if (ie)
	{
		if ((eval("document.all.divForms")!=null) && (ieversion>=6))
			{
				divForms.style.height=window.frames.frameElement.Height*0.75;
			}
	}
	else
	{
		var o_divforms;
		o_divforms=document.getElementById("divForms");
		if (o_divforms!=null)
		{
			o_divforms.style.height=window.frames.innerHeight*0.72;
			o_divforms.style.width=window.frames.innerWidth*0.995;
		}
	}
}
</script>
</head>
<body class="Margin0" onload="onPageLoaded()">
<form name="frmData" method="post" action="smreportfetch.asp" ID="Form1">
<table width="100%" border="0" cellspacing="0" cellpadding="4" ID="Table1">
<tr><td class="msgheader" colspan="8">Reports Administration</td></tr>
<tr><td class="ctrlgroup" colspan="8">View Available Reports</td></tr>
</table>
<div id="divForms" class="divScroll">
<table width="100%" border="0" cellspacing="0" cellpadding="4" ID="Table1">
<tr>
<td class="colheader3"></td>
<td class="colheader3">Report Name</td>
<td class="colheader3">Report Description</td>
</tr>

<%
dim r
dim sSQL
dim henv
dim db
dim rs
dim vName, vDesc, lReportID, lCount
dim i

Set r = CreateObject("DTGRocket")
henv = r.DB_InitEnvironment()
db = r.DB_OpenDatabase(henv, Application("SM_DSN"), 0)


If LCase(Request.ServerVariables("REQUEST_METHOD"))="post" Then
	' View report
	If Request("reportid")<>"" Then
		sSQL = "SELECT REPORT_XML FROM SM_REPORTS WHERE REPORT_ID = "  & Request("reportid")
		rs = r.DB_CreateRecordset(db, sSQL, 3, 0)
		vName = "Report XML Could Not be Found"
		If Not r.DB_EOF(rs) Then r.DB_GetData rs, "REPORT_XML", vName
		Response.Clear
		Response.ContentType = "text\xml"
		Response.Write vName
		r.DB_CloseRecordset CInt(rs), 2
		r.DB_CloseDatabase CInt(db)
		r.DB_FreeEnvironment CLng(henv)
		Response.End
	End If
End If
i = 0

sSQL = "SELECT * FROM SM_REPORTS,SM_REPORTS_PERM WHERE "
sSQL = sSQL & "SM_REPORTS_PERM.USER_ID = " & objSessionStr("UserID") & " AND SM_REPORTS.REPORT_ID = SM_REPORTS_PERM.REPORT_ID"
sSQL = sSQL & " ORDER BY SM_REPORTS.REPORT_NAME"

rs = r.DB_CreateRecordset(db, sSQL, 3, 0)
while not r.DB_EOF(rs)
	r.DB_GetData rs, "REPORT_ID", lReportID
	r.DB_GetData rs, "REPORT_NAME", vName
	r.DB_GetData rs, "REPORT_DESC", vDesc
%>

<% if i mod 2 = 0 then %>
<tr class="rowlight1">
<% else %>
<tr class="rowdark1">
<% end if %>
<td width="1" nowrap=""></td>
<td>
<a href="#" onClick="ViewReport(<%=lReportID%>)"> <%If "" & vName="" Then Response.Write "Report Nr. " & lReportID Else Response.Write vName %></a>
</td>
<td><% = vDesc & "" %> </td>
</tr>

<%
	i = i + 1
	
	r.DB_MoveNext rs
wend
r.DB_CloseRecordset CInt(rs), 2
r.DB_CloseDatabase CInt(db)

r.DB_FreeEnvironment CLng(henv)

set r = Nothing
%>
</table>
</div>
<input type="hidden" name="reportid" value="-1" ID="Hidden1"/>
</form>
<!--<input type="button" class="button" value="  Back  "  onClick="history.back();"  name="cmdBack"/>-->
<br>
This list shows the reports that are available for viewing.<br />
*** For information on extended reporting features and report troubleshooting please see the <a href="smfaq.htm">Frequently Asked Questions (FAQ)</a>.
</body>
</html>
