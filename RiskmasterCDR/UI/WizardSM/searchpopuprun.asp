<!-- #include file ="session.inc" -->
<%Public objSession
initSession objSession
%> 
<% 
Dim objData
Dim s

Set objData=CreateObject("SearchLib.CSearchResults")
objData.UserId=objSessionStr(SESSION_USERID)  'tkr 1/2003 enforce user permissions on searches
objData.GroupId=objSessionStr(SESSION_GROUPID)

'tkr 4/2003 no caching on web server
s="" & objData.SearchRun(objSessionStr("DSN"), objSessionStr("UserID"), Application("APP_DATA_PATH") & "searchpopupresults.xsl",Application(APP_SESSIONDSN),objSessionStr(SESSION_SID),,,, "" & Request("sys_ex"))
If Len(s) > 0 Then
	Response.ContentType = "text/HTML"
	Response.Write s
	Response.End
end if

' search did not return results - fall through'
%>

<html>
<head>
	<link rel="stylesheet" href="rmnet.css" type="text/css"/>
	<title><%=APP_TITLE%></title>
</head>

<body><form name="frmData">

<table width="550">
	<tr>
		<td>
      <P><FONT face=Arial size=4>Your search produced no 
      results.</FONT></P></td>
	<tr>
		<td><font face="Arial,Verdana,Helvetica,sans-serif" size="-1">
		<p>&nbsp;
		     
		</p>
      <P>
		Please hit the Back button and 
      refine your criteria selections.
		<br></P>
		<input type="button" value="Back" class="button" onclick="self.history.back();" name="btnBack" id="btnBack"></input>
		</font>
		</td>
	</tr>
</table>

</form>
<script language="JavaScript">
document.frmData.btnBack.focus();
</script>
</body>
</html>
