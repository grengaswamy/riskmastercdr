<!-- #include file ="criteriautilsconfig.inc" -->
<script language="VBScript" runat="server">
' Navigation Constants
Public Const SysCmdNavigationNone = 0
Public Const SysCmdNavigationFirst = 1
Public Const SysCmdNavigationPrev = 2
Public Const SysCmdNavigationNext = 3
Public Const SysCmdNavigationLast = 4
Public Const SysCmdSave = 5
Public Const SysCmdDelete =6

Public Const ID_FLDTYPE = "id"
Public Const RADIO_FLDTYPE = "radio"
Public Const TEXT_FLDTYPE = "text"
Public Const CODE_FLDTYPE = "code"
Public Const ORGH_FLDTYPE = "orgh"
Public Const DATE_FLDTYPE = "date"
Public Const TIME_FLDTYPE = "time"
Public Const MEMO_FLDTYPE = "memo"
Public Const BOOL_FLDTYPE = "checkbox"
Public Const ZIP_FLDTYPE = "zip"
Public Const NUMERIC_FLDTYPE = "numeric"
Public Const CODELIST_FLDTYPE = "codelist"
Public Const ENTITYLIST_FLDTYPE = "entitylist"
Public Const LOOKUP_FLDTYPE = "lookup"
Public Const EIDLOOKUP_FLDTYPE = "eidlookup"
Public Const POLICYLOOKUP_FLDTYPE = "policylookup"
Public Const POLICYSEARCH_FLDTYPE = "policysearch"
Public Const POLICYNUMBER_FLDTYPE = "policynumberlookup"
Public Const COMBOBOX_FLDTYPE = "combobox"
Public Const ACCOUNTLIST_FLDTYPE = "accountlist"
'Field types added by BB to support Exec Summary Criteria Screen.
Public Const MULTICLAIM_FLDTYPE = "multiclaimnumberlookup"
Public Const MULTIEVENT_FLDTYPE = "multieventnumberlookup"
Public Const MULTIORGH_FLDTYPE = "multiorgh"

Public Function SendFile(sFullName, bInline)
	Dim l,sExt, sName, sContentType, objSendFile
	sName = Right(sFullName,Len(sFullName) - InstrRev(sFullName,"\")) 
	Response.Clear()
	' Create object
	If USE_DCOM Then
		Set objSendFile=CreateObject("SendFileLocal.DTGSendFileLocal")
	Else
		Set objSendFile=CreateObject("DTGSendFile.1")
	End If
	' Default Content-Type
	sContentType="application/x-octetstream"
	If  bInline Then
		Response.AddHeader "Content-Disposition","inline; filename=" & sName
		' Get File extension if any
		l=InstrRev(sName,".")
		If l>0 Then
			sExt=LCase(Right(sName,Len(sName)-l))
			sContentType=objMimeTypes.LookupValue(sExt)
			If sContentType="" Then
				sContentType="application/x-octetstream"
			End If
		End If
	Else
		Response.AddHeader "Content-Disposition","attachment; filename=" & sName
	End If
	objSendFile.SendFileWin sFullName,sContentType
	Set objSendFile=Nothing
	Response.End

	End Function

	Sub SetValue(xmlElem)
	Dim sType
	Dim sName
	Dim sTmp1, sTmp2
	Dim vValue
	Dim objNode, vArr, c, l

	
	vValue = "" & Request(xmlElem.getAttribute("name"))
	sType = LCase(xmlElem.getAttribute("type"))
	
	If sType=CODELIST_FLDTYPE Or sType=ENTITYLIST_FLDTYPE or sType=MULTICLAIM_FLDTYPE or sType=MULTIEVENT_FLDTYPE or sType=MULTIORGH_FLDTYPE Then
	If xmlElem.hasChildNodes Then RemoveChildNodes xmlElem
	' List Field Types have the ID's stored is comma separated hidden label _lst
	sName = xmlElem.getAttribute("name") & "_lst"
		If "" & Request(sName)<>"" Then
			' Add It as Child Nodes
			vArr=Split(Request(sName),",")
			If IsArray(vArr) Then
				c=UBound(vArr)
				For l=0 To c
					If IsNumeric(vArr(l)) Then
						Set objNode=xmlElem.ownerDocument.createElement("option")
						objNode.setAttribute "value",vArr(l)
						xmlElem.appendChild objNode
						Set objNode=Nothing
					End If
				Next
			End If
		End If
	'ElseIf sType = CODE_FLDTYPE Or sType = ORGH_FLDTYPE Or sType=LOOKUP_FLDTYPE Or sType=EIDLOOKUP_FLDTYPE Or sType=POLICYLOOKUP_FLDTYPE or sType = POLICYSEARCH_FLDTYPE Then
	ElseIf sType = CODE_FLDTYPE Or sType = ORGH_FLDTYPE Or sType=LOOKUP_FLDTYPE Or sType=EIDLOOKUP_FLDTYPE Or sType=POLICYLOOKUP_FLDTYPE or sType = POLICYSEARCH_FLDTYPE or sType = POLICYNUMBER_FLDTYPE Then
	   sName = xmlElem.getAttribute("name")
	   If IsNumeric(Request(sName & "_cid")) Then
			xmlElem.setAttribute "codeid", "" & CLng(Request(sName & "_cid"))
			xmlElem.Text = vValue
		Else
			xmlElem.setAttribute "codeid", "0"
			xmlElem.Text=""
		End If
	ElseIf sType = BOOL_FLDTYPE Then
		sName = xmlElem.getAttribute("name")
		If clng(vValue) = 1 Then
			xmlElem.setAttribute "checked",""
		End if	
	ElseIf sType=COMBOBOX_FLDTYPE Then
		sName = xmlElem.getAttribute("name")
		If IsNumeric(Request(sName)) And sName<>"" Then
			xmlElem.setAttribute "codeid", "" & CLng(Request(sName))
		Else
			xmlElem.setAttribute "codeid", Request(sName)
		End If
	ElseIf sType=ACCOUNTLIST_FLDTYPE Then
	   sName = xmlElem.getAttribute("name")
	   If IsNumeric(Request(sName & "_cid")) Then
			xmlElem.setAttribute "codeid", "" & CLng(Request(sName & "_cid"))
			xmlElem.Text = vValue
		Else
			xmlElem.setAttribute "codeid", "0"
			xmlElem.Text=""
		End If
	ElseIf sType=RADIO_FLDTYPE Then
		sName = xmlElem.getAttribute("name")
		If vValue = xmlElem.getAttribute("value") Then
			xmlElem.setAttribute "checked",""
		End if	
	ElseIf sType=DATE_FLDTYPE or sType = TEXT_FLDTYPE or sType = NUMERIC_FLDTYPE Then
		xmlElem.setAttribute "value", vValue
	Else
	   xmlElem.Text = vValue
	End If
End Sub

' This sub is a place to insert non-standard per-job Request("paramname")
' values into the XML tags of the current report.
' Example:  optional outputfilename parameter.
Public Sub ApplyPostedXMLJobParams(ByRef sXML)
	Dim objXML 'as DomDocument
	Set objXML = CreateObject("Microsoft.XMLDOM")
	If Request("outputfilename") & "" <> "" Then
		If (objXML.loadXML(sXML) <> false) Then	
			objXML.documentElement.setAttribute "outputfilename", Request("outputfilename") & ""
      sXML = objXML.xml
      End If
      End If
      End Sub

      Public Sub CleanXMLDefaults(objXML)
      Dim ControlNodes, objTmpElt
      Set ControlNodes = objXML.getElementsByTagName("control")
      'Clean off all "codeid" and  "checked" elts this should be a blank template.
      For Each objTmpElt In ControlNodes
      If Not objTmpElt.Attributes.getNamedItem("checked") Is Nothing Then objTmpElt.Attributes.removeNamedItem "checked"
      If Not objTmpElt.Attributes.getNamedItem("codeid") Is Nothing Then  objTmpElt.Attributes.removeNamedItem "codeid"
      Next
      End Sub

      ' Transfer the Posted Form Elements to coresponding
      ' XML tags.
      Public Sub RequestToXML(objXMLDoc)
      Dim objXMLNode
      Dim objTmpNode
      Dim objXMLList
      Dim objXMLList1'	Subhendu 09/01/2010: MITS 21877
      Dim objXMLNode1'	Subhendu 09/01/2010: MITS 21877
      Dim sType
      Dim Tmp'Subhendu 09/01/2010: MITS 21877
      Dim lTemp 'As Long
      Dim sWhere 'Add by kuladeep for update the schedule mits:27574
      CleanXMLDefaults(objXMLDoc)
      'Subhendu 09/01/2010:MITS 21877 Report scheduling Issue.
      If not IsEmpty(Request("reportid")) then lTemp = Clng(Request("reportid"))
      'OpenSMDatabase
      sWhere = "SM_REPORTS.REPORT_ID = " & lTemp
			Tmp = GetSingleSMValue("PRIVACY_CASE","SM_REPORTS,SM_REPORTS_PERM",sWhere)
			'CloseSMDatabase
			Set objXMLList1=objXMLDoc.getElementsByTagName("report")
			For Each objXMLNode1 in objXMLList1
				if Tmp=0 Then'Request("chkPrivacycase")="on" then'sPrivacyCase = 0 then
					objXMLNode1.setAttribute "PrivacyCaseFlag", "0"
				else
					objXMLNode1.setAttribute "PrivacyCaseFlag", "1"
				end if
			Next
			
			
			Set objXMLList=objXMLDoc.getElementsByTagName("control")
			For Each objXMLNode in objXMLList
			sType=LCase(objXMLNode.getAttribute("type"))
			If sType=CODELIST_FLDTYPE Or sType=ENTITYLIST_FLDTYPE or sType=MULTICLAIM_FLDTYPE or sType=MULTIEVENT_FLDTYPE or sType=MULTIORGH_FLDTYPE Then
			If Not IsEmpty(Request(objXMLNode.getAttribute("name") & "_lst")) Then
				SetValue objXMLNode
			End If
		ElseIf Not IsEmpty(Request(objXMLNode.getAttribute("name"))) Then
			SetValue objXMLNode
		End If
	Next
		'HACK So that if report type is set internally it will be correctly noted
		' (special case since it's not a "control" but hidden as an "internal")
		If Not IsEmpty(Request("reporttype")) Then
			Set objXMLNode = objXMLDoc.selectSingleNode("//internal[@name = 'reporttype']")
			objXMLNode.setAttribute "value", Request("reporttype")
			Set objTmpNode = objXMLDoc.selectSingleNode("//report")
			objTmpNode.setAttribute "type", Request("reporttype")
		End If
   	
	Set objXMLList=Nothing
	Set objXMLNode=Nothing

End Sub

' Remove All Child Nodes from XML Elemenet
Public Sub RemoveChildNodes(xmlElem)
	Dim objChildNode
	
	If xmlElem Is Nothing Then Exit Sub
	
	For Each objChildNode in xmlElem.childNodes
		xmlElem.removeChild objChildNode
	Next
	
End Sub
</script>