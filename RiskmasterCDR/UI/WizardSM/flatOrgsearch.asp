<!-- #include file ="session.inc" -->

<%Public objSession
initSession objSession
Dim sLookupType, sLookupString,sLOBSQL,lLOB,i
Dim sSQL
Public objRocket, lEnv, iDB, iRS, sDSN
Dim  sFind,isOptdisabled,lLevel
dim objOrgHierarchy,strOrgEnt 

sDSN = objSessionStr(SESSION_DSN) 
'm_rm_space pankaj 14, jun 04. Trimmed before search 
sFind = trim(Request("find") & "")
if sFind="" then
	sFind=Request.QueryString("find") & "" 'by ravi
end if
sLookupString=Replace("" & sFind,"'","''")
sLookupString=Replace("" & sLookupString,"%","")
sLookupString=sLookupString & "%"

sLookupType="" & Request("type")
isOptdisabled=0

lLOB = "" & Request("lob")

Set objRocket = CreateObject("DTGRocket")
lEnv = objRocket.DB_InitEnvironment()
iDB = objRocket.DB_OpenDatabase(lEnv, sDSN, 0)


If sLookupType="code.orgh" Then
	'changed by ravi to make it competeble with org hierarchy
	'------------------------------------
	
	if objSessionStr("lob")<>"" then 'used to set the level of access
		lLevel=clng(objSessionStr("lob"))
	end if
	
	if Request.QueryString("entityTable")<>"" and isnumeric(Request.QueryString("entityTable")) then
		lLOB=clng(Request.QueryString("entityTable"))		
		select case lLOB
			case 1012
				strOrgEnt="A.DEPARTMENT_EID"
			case 1011
				strOrgEnt="A.FACILITY_EID"
			case 1010
				strOrgEnt="A.LOCATION_EID"
			case 1009
				strOrgEnt="A.DIVISION_EID"
			case 1008
				strOrgEnt="A.REGION_EID"
			case 1007
				strOrgEnt="A.OPERATION_EID"
			case 1006
				strOrgEnt="A.COMPANY_EID"
			case 1005
				strOrgEnt="A.CLIENT_EID"
		end select
		if sLookupString<>"" then		
				'M_RM_SEARCH Pankaj 25 May,2004 case in-sensitive search for oracle
				IF objRocket.DB_GetDBMake(iDB)=4 THEN ' 4 is for oracle
						set objOrgHierarchy=CreateObject("orgHierarchy.orgDisplay")				
						if  objOrgHierarchy.isSearchCaseInSens(objRocket,iDB) then
								sSQL="SELECT DISTINCT ENTITY_ID,ABBREVIATION,LAST_NAME FROM ENTITY,ORG_HIERARCHY A WHERE ENTITY_TABLE_ID=" & clng(lLOB) & " AND DELETED_FLAG=0 AND ENTITY_ID=" & strOrgEnt  & " AND (UPPER(ABBREVIATION) LIKE '" & UCASE(sLookupString) & "' OR UPPER(LAST_NAME) LIKE '" & UCASE(sLookupString) & "') ORDER BY ABBREVIATION"
						else
								sSQL="SELECT DISTINCT ENTITY_ID,ABBREVIATION,LAST_NAME FROM ENTITY,ORG_HIERARCHY A WHERE ENTITY_TABLE_ID=" & clng(lLOB) & " AND DELETED_FLAG=0 AND ENTITY_ID=" & strOrgEnt  & " AND (ABBREVIATION LIKE '" & sLookupString & "' OR LAST_NAME LIKE '" & sLookupString & "') ORDER BY ABBREVIATION"
						end if
						set objOrgHierarchy=nothing
				else	
						sSQL="SELECT DISTINCT ENTITY_ID,ABBREVIATION,LAST_NAME FROM ENTITY,ORG_HIERARCHY A WHERE ENTITY_TABLE_ID=" & clng(lLOB) & " AND DELETED_FLAG=0 AND ENTITY_ID=" & strOrgEnt  & " AND (ABBREVIATION LIKE '" & sLookupString & "' OR LAST_NAME LIKE '" & sLookupString & "') ORDER BY ABBREVIATION"
				end if		
		else
			sSQL="SELECT DISTINCT ENTITY_ID,ABBREVIATION,LAST_NAME FROM ENTITY,ORG_HIERARCHY A WHERE ENTITY_TABLE_ID=" & clng(lLOB) & " AND DELETED_FLAG=0 AND ENTITY_ID=" & strOrgEnt  & " ORDER BY ABBREVIATION"
		end if
		select case lLevel
			case 8
			lLevel=1012
			case 7
			lLevel=1011
			case 6
			lLevel=1010
			case 5
			lLevel=1009
			case 4
			lLevel=1008
			case 3
			lLevel=1007
			case 2
			lLevel=1006
			case 1
			lLevel=1005
		end select 
		
		if lLOB<>lLevel and lLevel<>"0" then ' changed on 9/30/03
			isOptdisabled=1
		end if
	else
		sSQL="SELECT DISTINCT ENTITY_ID,ABBREVIATION,LAST_NAME FROM ENTITY,ORG_HIERARCHY A  WHERE ENTITY_TABLE_ID=1012 AND DELETED_FLAG=0 AND ENTITY_ID=A.DEPARTMENT_EID AND (ABBREVIATION LIKE '" & sLookupString & "' OR LAST_NAME LIKE '" & sLookupString & "') ORDER BY ABBREVIATION"
	end if
	'-----------------------------------------------------
	'sSQL="SELECT ENTITY_ID,ABBREVIATION,LAST_NAME FROM ENTITY WHERE ENTITY_TABLE_ID=1012 AND ABBREVIATION LIKE '" & sLookupString & "' OR LAST_NAME LIKE '%" & sLookupString & "' ORDER BY ABBREVIATION"

End If


iRS=objRocket.DB_CreateRecordset(iDB, sSQL, 3, 0)

If objRocket.DB_Eof(iRS) Then
	' Display could not find data matching the criteria
	WriteDataNotFound
Else
	WriteData
End If

objRocket.DB_CloseRecordset CInt(iRS), 2
objRocket.DB_CloseDatabase CInt(iDB)
objRocket.DB_FreeEnvironment CLng(lEnv)
Set objRocket=Nothing

%>

<%Sub WriteDataNotFound()%>
<html><head>
<link rel="stylesheet" href="RMNet.css" type="text/css" />
<script language="JavaScript">
function handleOnload()
{
	
	//self.parent.frames["frmhierachy"].location.href='blank.htm';
	self.parent.frames["frmhierachy_1"].location.href='searchorg.asp?level=<%=lLOB%>&str=<%=sFind%>';
	
	return true;
}

</script>
</head>
<body onload="return handleOnload();">
<table align="center" width="100%" border="0">
	<tr>
	
		<td width="100%" valign="middle" align="center">
			<font face="Verdana, Arial" size="4">
				<b>Zero records match the criteria</b>
			</font>
				
		</td>
	</tr>
	
</table>
</body>
</html>
<%End Sub%>

<%Sub WriteData()
Dim sOption
Dim sId, sText, v, i, c

objRocket.DB_GetData iRS,1,sId
sId="" & sId
sText=""
c=objRocket.DB_ColumnCount(iRS)
For i=2 To c
	objRocket.DB_GetData iRS,i,v
	sText=sText & v & " "
Next

sOption="<option value=""" & sId & """ selected="""">" & sText & "</option>" & vbCrLf

objRocket.DB_MoveNext iRS

%>
<html>
<head><link rel="stylesheet" href="RMNet.css" type="text/css" />
<title>Quick Lookup Results</title>
<script language="JavaScript" SRC="org.js"></script>
<script language="JavaScript">
function handleOnload()
{
	
	self.parent.frames["frmhierachy_1"].location.href='searchorg.asp?level=<%=lLOB%>&str=<%=sFind%>';
	
	return true;
}
function handleUnload()
{
	self.parent.frames['frmhierachy'].location.href='blank.htm';

}
function FormSubmit(num)
{
	if(num=='1')
	{
		alert('not valid level');
		return ;
	}
	if(document.frmData.optResults.selectedIndex<0)
	{
		alert("Please select the record.");
	}
	else
	{
		var sId, sText,x
		sId=document.frmData.optResults.options[document.frmData.optResults.selectedIndex].value;
		sText=document.frmData.optResults.options[document.frmData.optResults.selectedIndex].text;
		getOrg(sId,sText,false);
		
	}
	return false;
}

</script>
</head>
<body onload="handleOnload();">
<table align="center" width="100%" border="0">
	<tr>
		<td width="100%" valign="middle" align="center">
			<font face="Verdana, Arial" size="2">
			<b>Record(s) matching the criteria</b>
			</font>
		</td>
	</tr>
	<tr>
		<td width="100%" valign="middle" align="center">
			<form name="frmData" onSubmit="return FormSubmit()" action="" >
				<input type="hidden" name="lookuptype" value="<%=Request("type")%>" />
				<%if isOptdisabled=1 then%>
				<select name="optResults" size="10" style="width: 100%" ondblclick="FormSubmit('1');">
				<%else%>
				<select name="optResults" size="10" style="width: 100%" ondblclick="FormSubmit('0');">
				<%end if
				If sOption<>"" Then Response.Write sOption
				
				iCount=0
				Do While Not objRocket.DB_Eof(iRS) 
					objRocket.DB_GetData iRS,1,sId
					sId="" & sId
					sText=""
					c=objRocket.DB_ColumnCount(iRS)
					For i=2 To c
						objRocket.DB_GetData iRS,i,v
						sText=sText & v & " "
					Next
					Response.Write "<option value=""" & sId & """>" & sText & "</option>" & vbCrLf
					iCount=iCount+1
					objRocket.DB_MoveNext iRS
				Loop
				%>
				</select>
				</form>
		</td>
	</tr>
</table>
</body>
</html>
<%End Sub%>

