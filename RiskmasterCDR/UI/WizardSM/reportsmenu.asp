<% OPTION EXPLICIT %>

<%
' Check security context
Dim lSecurityId, objSecurityTable, bSortmasterAccess, bSortmasternetAccess, bDCCnetAccess, bConfigurationAccess

bSortmasterAccess=True
bSortmasternetAccess=True
bConfigurationAccess=True
bDCCnetAccess=True
'If objSessionStr(SESSION_MODULESECURITY)="1" Then
'	lSecurityId=50000	
'	Set objSecurityTable=Application(APP_PERMCACHE & objSessionStr(SESSION_DSNID))
'	If Not objSecurityTable.KeyExists(objSessionStr(SESSION_GROUPID) & "-" & lSecurityId) Then
'		bSortmasterAccess=False
'	End If
'	For SortMaster
'	lSecurityId=358000
'	If Not objSecurityTable.KeyExists(objSessionStr(SESSION_GROUPID) & "-" & lSecurityId) Then
'		bSortmasternetAccess=False
'	End If
'End If
%>

<html>
<head>
	<title>Riskmaster</title>
   <link rel="stylesheet" href="RMNet.css" type="text/css" />
	<script language="JavaScript" SRC="SMD.js"></script>
	<script language="JavaScript" SRC="Menus.js"></script>
</head>

<body class="margin0" ONLOAD="onPageLoaded()">

<table border="0" cellspacing="0" cellpadding="0">
	<tr>
			<td class="headerNavy8" height="15" width="120" background="img/navigationmid2.gif">&nbsp;Reports</td>			
	</tr>
	<tr>
		<td class="td8" height="20" width="120" VALIGN="middle">
			<% If bSortmasternetAccess Then %>
				<img src="img/ball.gif"><a class="LightBold" href="#" ONCLICK="return linkClick(this,'smreportsel.asp','docframe');">Available Reports</a>
			<% Else %>
				<img src="img/ball.gif"><a class="LightBold" href="#" ONCLICK="return linkClick(this,'ad.asp','docframe');">Available Reports</a>
			<% End If %>
		</td>
	</tr>
	<tr>
		<td class="td8" height="20" width="120" VALIGN="middle">
			<% If bSortmasternetAccess Then %>
				<img src="img/ball.gif"><a class="LightBold" href="#" ONCLICK="return linkClick(this,'smrepqueue.asp','docframe');">Job Queue</a>
			<% Else %>
				<img src="img/ball.gif"><a class="LightBold" href="#" ONCLICK="return linkClick(this,'ad.asp','docframe');">Job Queue</a>
			<% End If %>
		</td>
	</tr>
	<tr>
		<td class="td8" height="20" width="120" VALIGN="middle">
			<% If bSortmasternetAccess Then %>
				<img src="img/ball.gif"><a class="LightBold" href="#" ONCLICK="return linkClick(this,'smpostreport.asp','docframe');">Post New Report</a>
			<% Else %>
				<img src="img/ball.gif"><a class="LightBold" href="#" ONCLICK="return linkClick(this,'ad.asp','docframe');">Post New Report</a>
			<% End If %>
		</td>
	</tr>
	<tr>
		<td class="td8" height="20" width="120" VALIGN="middle">
			<% If bSortmasternetAccess Then %>
				<img src="img/ball.gif"><a class="LightBold" href="#" ONCLICK="return linkClick(this,'smreportdelete.asp','docframe');">Delete Report</a>
			<% Else %>
				<img src="img/ball.gif"><a class="LightBold" href="#" ONCLICK="return linkClick(this,'ad.asp','docframe');">Delete Report</a>
			<% End If %>
		</td>
	</tr>
	<tr>
		<td class="td8" height="20" width="120" VALIGN="middle">
			<% If bSortmasternetAccess Then %>
				<img src="img/ball.gif"><a class="LightBold" href="#" ONCLICK="return linkClick(this,'schedulereport.asp','docframe');">Schedule Reports</a>
			<% Else %>
				<img src="img/ball.gif"><a class="LightBold" href="#" ONCLICK="return linkClick(this,'ad.asp','docframe');">Schedule Reports</a>
			<% End If %>
		</td>
	</tr>
	<tr>
		<td class="td8" height="20" width="120" VALIGN="middle">
			<% If bSortmasternetAccess Then %>
				<img src="img/ball.gif"><a class="LightBold" href="#" ONCLICK="return linkClick(this,'schedulelist.asp','docframe');">View Scheduled Reports</a>
			<% Else %>
				<img src="img/ball.gif"><a class="LightBold" href="#" ONCLICK="return linkClick(this,'ad.asp','docframe');">View Scheduled Reports</a>
			<% End If %>
		</td>
	</tr>
</table>

<table border="0" cellspacing="0" cellpadding="0">
	<tr>
			<td class="headerNavy8" height="15" width="120" background="img/navigationmid2.gif">&nbsp;SM</td>			
	</tr>
	<tr>
		<td class="td8" height="20" width="120" VALIGN="middle">
			<% If bSortmasternetAccess Then %>
				<img src="img/ball.gif"><a class="LightBold" href="#" ONCLICK="return linkClick(this,'rptfields.asp?MODE=0','docframe');">Designer</a>
			<% Else %>
				<img src="img/ball.gif"><a class="LightBold" href="#" ONCLICK="return linkClick(this,'ad.asp','docframe');">Designer</a>
			<% End If %>
		</td>
	</tr>
	<tr>
		<td class="td8" height="20" width="120">
			<% If bSortmasternetAccess Then %>
				<img src="img/ball.gif"><a class="LightBold" href="#" ONCLICK="return linkClick(this,'smopenreport.asp','docframe');">Draft Reports</a>
			<% Else %>
				<img src="img/ball.gif"><a class="LightBold" href="#" ONCLICK="return linkClick(this,'ad.asp','docframe');">Draft Reports</a>
			<% End If %>
		</td>
	</tr>
	<tr>
		<td class="td8" height="20" width="120">
			<% If bSortmasternetAccess Then %>
				<img src="img/ball.gif"><a class="LightBold" href="#" ONCLICK="return linkClick(this,'postreport.asp','docframe');">Post Draft Reports</a>
			<% Else %>
				<img src="img/ball.gif"><a class="LightBold" href="#" ONCLICK="return linkClick(this,'ad.asp','docframe');">Post Draft Reports</a>
			<% End If %>
		</td>
	</tr>
	<% '-- SMWD Rajeev 03/24/2003 %>
</table>
</body>
</html>