<script language="VBScript" runat="server">

'*******************************************************
'*******************************************************
' This module of DB Routines contains both Sortmaster 
' and RiskMaster db access methods and variables.
' The Sortmaster specific things have "SM" in the name.
'
'Note: All DB Routines.
' objSession MUST EXIST AND BE INITIALIZED
'********************************************************
'********************************************************
Dim objRocket, hEnv, hDB
Dim objRocketSec, hEnvSec, hDBSec
Dim objRocketSess, hEnvSess, hDBSess
Sub OpenDatabase()

	If IsEmpty(objRocket) then Set objRocket = nothing
	If objRocket Is Nothing Then
		Set objRocket = CreateObject("DTGRocket")
		hEnv = objRocket.DB_InitEnvironment()
		hDB = objRocket.DB_OpenDatabase(henv, "" & objSessionStr(SESSION_DSN), 0)
	End If
End Sub
Sub CloseDatabase()
	If IsEmpty(objRocket) then Set objRocket = nothing
	If Not objRocket Is Nothing Then
		objRocket.DB_CloseDatabase CInt(hDB)
		objRocket.DB_FreeEnvironment CLng(hEnv)
		Set objRocket = Nothing
	End If
End Sub
Sub OpenSecDatabase()

	If IsEmpty(objRocketSec) then Set objRocketSec = nothing
	If objRocketSec Is Nothing Then
		Set objRocketSec = CreateObject("DTGRocket")
		hEnvSec = objRocketSec.DB_InitEnvironment()
		hDBSec = objRocketSec.DB_OpenDatabase(hEnvSec, "" & Application(APP_SECURITYDSN), 0)
	End If
End Sub
Sub CloseSecDatabase()
	If IsEmpty(objRocketSec) then Set objRocketSec = nothing
	If Not objRocketSec Is Nothing Then
		objRocketSec.DB_CloseDatabase CInt(hDBSec)
		objRocketSec.DB_FreeEnvironment CLng(hEnvSec)
		Set objRocketSec = Nothing
	End If
End Sub
Sub OpenSessDatabase()

	If IsEmpty(objRocketSess) then Set objRocketSess = nothing
	If objRocketSess Is Nothing Then
		Set objRocketSess = CreateObject("DTGRocket")
		hEnvSess = objRocketSess.DB_InitEnvironment()
		hDBSess = objRocketSess.DB_OpenDatabase(hEnvSess, "" & Application(APP_SESSIONDSN), 0)
	End If
End Sub
Sub CloseSessDatabase()
	If IsEmpty(objRocketSess) then Set objRocketSess = nothing
	If Not objRocketSess Is Nothing Then
		objRocketSess.DB_CloseDatabase CInt(hDBSess)
		objRocketSess.DB_FreeEnvironment CLng(hEnvSess)
		Set objRocketSess = Nothing
	End If
End Sub
Function GetSingleValue(sFieldName, sTableName, sCriteria ) 
    Dim rs 'as Integer
    Dim SQL 'as String
    Dim v 'as Variant
    Dim CleanUpFlag 'as Boolean
    
    If IsEmpty(objRocket) Then
		Set objRocket = nothing
		CleanUpFlag = true
		OpenDatabase
	End If
    If objRocket Is nothing Then
		CleanUpFlag = true
		OpenDatabase
	End If

    If sFieldName = "" Or sTableName = "" Then Exit Function
    SQL = "SELECT " & sFieldName & " FROM " & sTableName 
    If sCriteria <> "" Then SQL = SQL & " WHERE " & sCriteria
    On Error Resume Next
    rs = objRocket.DB_CreateRecordset(hDb, UCase(SQL), 3, 0)
    If Err.Number <>0 Then
		Response.Write SQL
		Response.End
    End If
    On Error GoTo 0
    If Not objRocket.DB_EOF(cint(rs)) Then objRocket.DB_GetData rs, 1, v
    GetSingleValue = v
    objRocket.DB_CloseRecordset Cint(rs),1
    If CleanUpFlag Then CloseDatabase
End Function
'01/01/07   Rem UMESH
Function GetSingleSessValue(sFieldName, sTableName, sCriteria ) 
    Dim rs 'as Integer
    Dim SQL 'as String
    Dim v 'as Variant
    Dim CleanUpFlag 'as Boolean
    If IsEmpty(objRocketSess) Then
		Set objRocketSess = nothing
		CleanUpFlag = true
		 OpenSessDatabase
	End If
    If objRocketSess Is nothing Then
		CleanUpFlag = true
		 OpenSessDatabase
	End If
    If sFieldName = "" Or sTableName = "" Then Exit Function
    SQL = "SELECT " & sFieldName & " FROM " & sTableName 
    If sCriteria <> "" Then SQL = SQL & " WHERE " & sCriteria
    On Error Resume Next
    rs = objRocketSess.DB_CreateRecordset(hDBSess, SQL, 3, 0)
    If Err.Number <>0 Then
		Response.Write SQL
		Response.End
    End If
    On Error GoTo 0
    If Not objRocketSess.DB_EOF(cint(rs)) Then objRocketSess.DB_GetData rs, 1, v
    GetSingleSessValue = v
    objRocketSess.DB_CloseRecordset Cint(rs),1
    If CleanUpFlag Then CloseSessDatabase
End Function
'END   Rem UMESH

'BSB 01.06.2005 Fix for my earlier laziness using + to concatenate field values into a single 
' output field and using GetSingleValue -- Fails in Oracle on trans.asp
'Example: sResultPattern for Codes:  "{1} - {2}"
'SQL for Codes: "SELECT CODES.SHORT_CODE, CODES_TEXT.CODE_DESC  FROM CODES,CODES_TEXT WHERE CODES.CODE_ID=... AND CODES.CODE_ID=CODES_TEXT.CODE_ID"
'Example: sResultPattern for States: "{1} {2}"
'SQL for States: "SELECT STATE_ID, STATE_NAME FROM STATES  WHERE STATE_ROW_ID=..."

Function GetPatternedValueString(sResultPattern,SQL) 
    Dim rs 'as Integer
    'Dim SQL 'as String
    Dim v 'as Variant
    Dim CleanUpFlag 'as Boolean
    Dim i 'As Integer 
    
    If IsEmpty(objRocket) Then
		Set objRocket = Nothing
		CleanUpFlag = true
		OpenDatabase
	End If
    If objRocket Is Nothing Then
		CleanUpFlag = true
		OpenDatabase
	End If
    
    rs = objRocket.DB_CreateRecordset(hDb, UCase(SQL), 3, 0)
    If Not objRocket.DB_EOF(cint(rs)) Then 
		For i=1 To objRocket.DB_ColumnCount(cint(rs))
			objRocket.DB_GetData rs, i, v
			sResultPattern = Replace(sResultPattern,"{" & i & "}", v)
		Next
    End If
    GetPatternedValueString = sResultPattern
    
    objRocket.DB_CloseRecordset Cint(rs),1
    If CleanUpFlag Then CloseDatabase
End Function

Function lGetNextUID(sTableName, iDB)
	Dim rs
	Dim sSQL
	Dim lNextUID
	Dim lOrigUID
	Dim lRows
	Dim lCollisionRetryCount
	Dim lErrRetryCount
	Dim sSaveError
	 
   Do 
		rs = objRocket.DB_CreateRecordset(iDB, "SELECT NEXT_UNIQUE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = '" & sTableName & "'", 3, 0)
		If objRocket.DB_EOF(rs) Then
			objRocket.DB_CloseRecordset Cint(rs), 2			
			Err.Raise 32001, "lGetNextUID(" & sTableName & ")", "Specified table does not exist in glossary."
			lGetNextUID = 0
		End If
	       
		objRocket.DB_GetData rs, "NEXT_UNIQUE_ID", lNextUID
		objRocket.DB_CloseRecordset CInt(rs), 2
	
		' Compute next id
		lOrigUID = lNextUID
			
		If lOrigUID Then lNextUID = lNextUID + 1 Else lNextUID = 2
			   
		' try to reserve id (searched update)
		sSQL = "UPDATE GLOSSARY SET NEXT_UNIQUE_ID = " & lNextUID & " WHERE SYSTEM_TABLE_NAME = '" & sTableName & "'"
			
		' only add searched clause if no chance of a null originally in row (only if no records ever saved against table)   JP 12/7/96
		If lOrigUID Then sSQL = sSQL & " AND NEXT_UNIQUE_ID = " & lOrigUID
	
		' Try update
		On Error Resume Next
		lRows = objRocket.DB_SQLExecute(iDB, sSQL)
			
		If Err.Number <> 0 Then
			sSaveError = Err.Description
				
			lErrRetryCount = lErrRetryCount + 1
				
			If lErrRetryCount >= 5 Then
				On Error GoTo 0
				Response.Write "lGetNextUID(" & sTableName & "), Could not save Record, please try and resubmit your application."
				Exit Function
			End If
		Else
			' if success, return
			If lRows = 1 Then
				lGetNextUID = lNextUID - 1
				Exit Function   ' success
			Else
				lCollisionRetryCount = lCollisionRetryCount + 1   ' collided with another user - try again (up to 1000 times)
			End If
		End If
			
		On Error Goto 0
	        
	Loop While (lErrRetryCount < 5) And (lCollisionRetryCount < 1000)
	     
	If lCollisionRetryCount >= 1000 Then
		Response.Write "GlobalFunctions.lGetNextUID(" & sTableName & "), Collision timeout. Server load too high. Please wait and try again."
		Exit Function
	End If
	
	lGetNextUID = lNextUID
End Function

'*******************************************************
'*******************************************************
'Note: All SortMaster DB Routines have "SM" in the name.
' They are run off of variables with "SM" in them as well.
'(Begin SM Specific Code)
'********************************************************
'********************************************************
Dim objSMRocket, hSMEnv, hSMDB
Sub OpenSMDatabase()
	If IsEmpty(objSMRocket) then Set objSMRocket = nothing
	If objSMRocket Is Nothing Then
		Set objSMRocket = CreateObject("DTGRocket")
		hSMEnv = objSMRocket.DB_InitEnvironment()
		hSMDB = objSMRocket.DB_OpenDatabase(hSMEnv, Application(APP_SMDSN), 0)
	End If
End Sub
Sub CloseSMDatabase()
	If IsEmpty(objSMRocket) then Set objSMRocket = nothing
	If Not objSMRocket Is Nothing Then
		objSMRocket.DB_CloseDatabase CInt(hSMDB)
		objSMRocket.DB_FreeEnvironment CLng(hSMEnv)
		Set objSMRocket = Nothing
	End If
End Sub
Function GetSingleSMValue(sFieldName, sTableName, sCriteria ) 
    Dim rs 'as Integer
    Dim SQL 'as String
    Dim v 'as Variant
    Dim CleanUpFlag 'as Boolean
    
    If IsEmpty(objSMRocket) Then
		Set objSMRocket = nothing
		CleanUpFlag = true
		OpenSMDatabase
	End If
    If objSMRocket Is nothing Then
		CleanUpFlag = true
		OpenSMDatabase
	End If

    If sFieldName = "" Or sTableName = "" Then Exit Function
    SQL = "SELECT " & sFieldName & " FROM " & sTableName 
    If sCriteria <> "" Then SQL = SQL & " WHERE " & sCriteria
    rs = objSMRocket.DB_CreateRecordset(hSMDb, UCase(SQL), 3, 0)
    If Not objSMRocket.DB_EOF(cint(rs)) Then objSMRocket.DB_GetData rs, 1, v
    GetSingleSMValue = v
    objSMRocket.DB_CloseRecordset Cint(rs),1
    If CleanUpFlag Then CloseSMDatabase
End Function
Function lGetNextSMUID(sTableName, iDB)
	Dim rs
	Dim sSQL
	Dim lNextUID
	Dim lOrigUID
	Dim lRows
	Dim lCollisionRetryCount
	Dim lErrRetryCount
	Dim sSaveError
	
	Do 
		rs = objSMRocket.DB_CreateRecordset(iDB, "SELECT NEXT_ID FROM SM_IDS WHERE TABLE_NAME = '" & sTableName & "'", 3, 0)
		If objSMRocket.DB_EOF(rs) Then
			objSMRocket.DB_CloseRecordset Cint(rs), 2			
			Err.Raise 32001, "lGetNextUID(" & sTableName & ")", "Specified table does not exist in glossary."
			lGetNextSMUID = 0
		End If
	    
		objSMRocket.DB_GetData rs, "NEXT_ID", lNextUID
		objSMRocket.DB_CloseRecordset CInt(rs), 2

		' Compute next id
		lOrigUID = lNextUID
		
		If lOrigUID Then lNextUID = lNextUID + 1 Else lNextUID = 2
			
		' try to reserve id (searched update)
		sSQL = "UPDATE SM_IDS SET NEXT_ID = " & lNextUID & " WHERE TABLE_NAME = '" & sTableName & "'"
		
		' only add searched clause if no chance of a null originally in row (only if no records ever saved against table)   JP 12/7/96
		If lOrigUID Then sSQL = sSQL & " AND NEXT_ID = " & lOrigUID

		' Try update
		On Error Resume Next
		lRows = objSMRocket.DB_SQLExecute(iDB, sSQL)
		
		If Err.Number <> 0 Then
			sSaveError = Err.Description
			
			lErrRetryCount = lErrRetryCount + 1
			
			If lErrRetryCount >= 5 Then
				On Error GoTo 0
				Response.Write "lGetNextUID(" & sTableName & "), Could not save Record, please try and resubmit your application."
				Exit Function
			End If
		Else
			' if success, return
			If lRows = 1 Then
				lGetNextSMUID = lNextUID - 1
				Exit Function   ' success
			Else
				lCollisionRetryCount = lCollisionRetryCount + 1   ' collided with another user - try again (up to 1000 times)
			End If
		End If
		
		On Error Goto 0
	    
	Loop While (lErrRetryCount < 5) And (lCollisionRetryCount < 1000)
	    
	If lCollisionRetryCount >= 1000 Then
		Response.Write "GlobalFunctions.lGetNextSMUID(" & sTableName & "), Collision timeout. Server load too high. Please wait and try again."
		Exit Function
	End If

	lGetNextSMUID = lNextUID
End Function

Function GetSMData(rs, Idx) 'As Variant
	Dim v
	On Error Resume Next
	objSMRocket.DB_GetData rs, Idx, v
	GetSMData = v
End Function

'******************************************************************
'End Sortmaster Specific Routines
'******************************************************************

Function GetData(rs, Idx) 'As Variant
	Dim v
	On Error Resume Next
	objRocket.DB_GetData rs, Idx, v
	GetData = v
End Function

Function GetDBDate(sDate)
	Dim vDate
	GetDBDate=""
	
	If Not IsDate(sDate) Then Exit Function
	
	vDate=CDate(sDate)
	
	GetDBDate=Year(vDate)
	If Len("" & Month(vDate))=1 Then GetDBDate=GetDBDate & "0"
	GetDBDate=GetDBDate & Month(vDate)
	
	If Len("" & Day(vDate))=1 Then GetDBDate=GetDBDate & "0"
	GetDBDate=GetDBDate & Day(vDate)
End Function

Function GetDBTime(sTime)
	Dim vTime
	GetDBTime=""
	
	If sTime="" Or Not IsDate(sTime) Then Exit Function
	
	On Error Resume Next
	vTime=CDate(sTime)
	If Err.Number>0 Then Exit Function
	On Error Goto 0
	
	If Len("" & Hour(vTime))=1 Then GetDBTime=GetDBTime & "0"
	GetDBTime=GetDBTime & Hour(vTime)
	
	If Len("" & Minute(vTime))=1 Then GetDBTime=GetDBTime & "0"
	GetDBTime=GetDBTime & Minute(vTime)
	
	If Len("" & Second(vTime))=1 Then GetDBTime=GetDBTime & "0"
	GetDBTime=GetDBTime & Second(vTime)
	
End Function

Function FormatDBDateTime(sDateTime)
Dim sDate, sTime

sTime=""
sDate=""

If Len(sDateTime) <> 14 And Len(sDateTime) <> 8 Then
	FormatDBDateTime = ""
	Exit Function
End If

sDate = FormatDateTime(DateSerial(Left(sDateTime, 4), Mid(sDateTime, 5, 2), Mid(sDateTime, 7, 2)), vbShortDate)

If Len(sDateTime) = 14 Then
	sTime = FormatDateTime(TimeSerial(Mid(sDateTime, 9, 2), Mid(sDateTime, 11, 2), right(sDateTime, 2)), vbLongTime)
End If

FormatDBDateTime = sDate & " " & sTime
End Function

Function FormatDBTime(sTime)

If Len(sTime) <> 6  Then
	FormatDBTime = ""
	Exit Function
End If

FormatDBTime = FormatDateTime(TimeSerial(Left(sTime, 2), Mid(sTime, 3, 2), right(sTime, 2)), vbLongTime)

End Function

'tkr 2/2003 to make outer joins.  copied from SearchLib.vbp, SearchResults.bas
'example:
	'Dim sFrom, sWhere, sSelect, sSQL
    'sWhere = "CLAIM_ID = 546 AND CLAIMANT_EID = 0 AND UNIT_ID = 0 AND RESERVE_TYPE_CODE = 376"
    'sFrom = "RESERVE_HISTORY"
    'joinItEx sFrom, sWhere, "RESERVE_HISTORY", "CODES", "RES_STATUS_CODE", "CODE_ID", DBMS_IS_SQLSRVR
    'sSelect = "SELECT * "
    'sSQL = sSelect & sFrom & " WHERE " & sWhere
    'yields: SELECT * FROM RESERVE_HISTORY,CODES WHERE CLAIM_ID = 546 AND CLAIMANT_EID = 0 AND UNIT_ID = 0 AND RESERVE_TYPE_CODE = 376 AND RESERVE_HISTORY.RES_STATUS_CODE *= CODES.CODE_ID 
Function joinItEx(sFrom, sWhere, sTemp1, sTemp2, sField1, sField2, iDB)
    If iDB = DBMS_IS_ACCESS Then
        sFrom = "(" & sFrom & " LEFT JOIN " & sTemp2
        sFrom = sFrom & " ON " & sTemp1 & "." & sField1 & " = " & sTemp2 & "." & sField2 & ")"
    ElseIf iDB = DBMS_IS_SQLSRVR Or iDB = DBMS_IS_SYBASE Then
        sFrom = sFrom & "," & sTemp2
        If sWhere <> "" Then sWhere = sWhere & " AND "
        sWhere = sWhere & sTemp1 & "." & sField1 & " *= " & sTemp2 & "." & sField2
    ElseIf iDB = DBMS_IS_INFORMIX Then
        sFrom = sFrom & ",OUTER " & sTemp2
        If sWhere <> "" Then sWhere = sWhere & " AND "
        sWhere = sWhere & sTemp1 & "." & sField1 & " = " & sTemp2 & "." & sField2
    ElseIf iDB = DBMS_IS_ORACLE Then  ' Oracle Support JP 4/14/98
        sFrom = sFrom & "," & sTemp2
        If sWhere <> "" Then sWhere = sWhere & " AND "
        sWhere = sWhere & sTemp1 & "." & sField1 & " = " & sTemp2 & "." & sField2 & "(+)"
    ElseIf iDB = DBMS_IS_DB2 Then     ' JP 2/8/2000 DB2 support
        sFrom = "(" & sFrom & " LEFT OUTER JOIN " & sTemp2
        sFrom = sFrom & " ON " & sTemp1 & "." & sField1 & " = " & sTemp2 & "." & sField2 & ")"
    End If
End Function

'tkr 2/2003 outer joins where join field has same name in both tables.  copied from SearchLib.vbp, SearchResults.bas
Sub joinIt(sFrom, sWhere, sTemp1, sTemp2, sField, iDB)
    If iDB = DBMS_IS_ACCESS Then
        sFrom = "(" & sFrom & " LEFT JOIN " & sTemp2
        sFrom = sFrom & " ON " & sTemp1 & "." & sField & " = " & sTemp2 & "." & sField & ")"
    ElseIf iDB = DBMS_IS_SQLSRVR Or iDB = DBMS_IS_SYBASE Then
        sFrom = sFrom & "," & sTemp2
        If sWhere <> "" Then sWhere = sWhere & " AND "
        sWhere = sWhere & sTemp1 & "." & sField & " *= " & sTemp2 & "." & sField
    ElseIf iDB = DBMS_IS_INFORMIX Then
        sFrom = sFrom & ",OUTER " & sTemp2
        If sWhere <> "" Then sWhere = sWhere & " AND "
        sWhere = sWhere & sTemp1 & "." & sField & " = " & sTemp2 & "." & sField
    ElseIf iDB = DBMS_IS_ORACLE Then  ' Oracle Support JP 4/14/98
        sFrom = sFrom & "," & sTemp2
        If sWhere <> "" Then sWhere = sWhere & " AND "
        sWhere = sWhere & sTemp1 & "." & sField & " = " & sTemp2 & "." & sField & "(+)"
    ElseIf iDB = DBMS_IS_DB2 Then     ' JP 2/8/2000 DB2 Support
        sFrom = "(" & sFrom & " LEFT OUTER JOIN " & sTemp2
        sFrom = sFrom & " ON " & sTemp1 & "." & sField & " = " & sTemp2 & "." & sField & ")"
    End If
End Sub


</script>