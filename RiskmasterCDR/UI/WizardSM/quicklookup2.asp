<%Option Explicit%>
<!-- #include file ="session.inc" -->
<!--<script language="JavaScript" src="bb_debug.js"></script>-->
<%Public objSession
initSession objSession
' Added filtering for Claim Status
' Modified Deleted flag checks to catch "-1" or other non-zero true values.
Dim sLookupType, sLookupString,sLOBSQL,lLOB,sLimits,sLimitsSQL,vArray,vArray2,i,blnOracle
Dim sSQL
Public objRocket, lEnv, iDB, iRS, sDSN
Dim bCreatable, sFind
Const ListLimit=100
Dim sIndex, iCount
Dim sId
'DF MITS 13372
Dim m_lClaimID
Dim m_lClaimType
dim m_sNow, m_sToday
m_lClaimID=Request("claimid")

'DF MITS 13372
m_sNow=Now
m_sToday=Year(m_sNow)

sIndex=cStr("" & Request("index"))

If sIndex="" Then sIndex=" "
sDSN = objSessionStr(SESSION_DSN)  'tkr 12/2002 some codes are rm_sys_users
sFind = Request("find") & ""
if sFind="" then
	sFind=Request.QueryString("find") & "" 'by ravi
end if
sLookupString=Replace("" & sFind,"'","''")
sLookupString=Replace("" & sLookupString,"%","")
sLookupString=sLookupString & "%"

Set objRocket = CreateObject("DTGRocket")
lEnv = objRocket.DB_InitEnvironment()
iDB = objRocket.DB_OpenDatabase(lEnv, sDSN, 0)

sSQL="SELECT ORACLE_CASE_INS FROM SYS_PARMS"
iRS=objRocket.DB_CreateRecordset(iDB, sSQL, 3, 0)
if not objRocket.DB_EOF(iRS) then
	objRocket.DB_GetData iRS,"ORACLE_CASE_INS",blnOracle
	if not isnull(blnOracle) then
		if (blnOracle=1 or blnOracle=-1) and objRocket.DB_GetDBMake(iDB)=4 then
			blnOracle=1
			sLookupString=ucase(sLookupString)
		else
			blnOracle=0
		end if
	else
		blnOracle=0
	end if
end if
objRocket.DB_CloseRecordset cint(iRS),2

sLookupType="" & Request("type")
If sLookupType="" Then
	sLookupType=Request.QueryString("lookuptype") & ""
End If

'tkr 2/2003 expand creatable functionality to allow new record even when lookup matches
bCreatable = (Request("creatable") = "1")

' Hack - Filter by LOB if Code is CLAIM_TYPE and LOB param is available. 
' BSB 06.03.2002
lLOB = "" & Request("lob")

If IsNumeric(lLOB) Then
	sLOBSQL = " AND (CODES.LINE_OF_BUS_CODE = " & lLOB & " OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0) "
Else
	sLOBSQL = ""
End If

If UCase(sLookupType)="EMPLOYEES" Then
	if blnOracle=1 then
		sSQL="SELECT EMPLOYEE.EMPLOYEE_EID, EMPLOYEE.EMPLOYEE_NUMBER, ENTITY.LAST_NAME, ENTITY.FIRST_NAME FROM EMPLOYEE,ENTITY WHERE EMPLOYEE.EMPLOYEE_EID=ENTITY.ENTITY_ID AND (ENTITY.DELETED_FLAG = 0 OR ENTITY.DELETED_FLAG IS NULL) AND UPPER(EMPLOYEE.EMPLOYEE_NUMBER) LIKE '" & sLookupString & "' AND UPPER(EMPLOYEE.EMPLOYEE_NUMBER) >= '" & ucase(sIndex) & "' ORDER BY EMPLOYEE.EMPLOYEE_NUMBER"
	else
		sSQL="SELECT EMPLOYEE.EMPLOYEE_EID, EMPLOYEE.EMPLOYEE_NUMBER, ENTITY.LAST_NAME, ENTITY.FIRST_NAME FROM EMPLOYEE,ENTITY WHERE EMPLOYEE.EMPLOYEE_EID=ENTITY.ENTITY_ID AND (ENTITY.DELETED_FLAG = 0 OR ENTITY.DELETED_FLAG IS NULL) AND EMPLOYEE.EMPLOYEE_NUMBER LIKE '" & sLookupString & "' AND EMPLOYEE.EMPLOYEE_NUMBER >= '" & sIndex & "' ORDER BY EMPLOYEE.EMPLOYEE_NUMBER"
	end if
ElseIf UCase(sLookupType)="CODE.ORGH" Then
	if blnOracle=1 then
		sSQL="SELECT ENTITY_ID,ABBREVIATION,LAST_NAME FROM ENTITY WHERE ENTITY_TABLE_ID=1012 AND UPPER(ABBREVIATION) LIKE '" & sLookupString & "' AND UPPER(ABBREVIATION) >= '" & ucase(sIndex) & "' ORDER BY ABBREVIATION"
	else
		sSQL="SELECT ENTITY_ID,ABBREVIATION,LAST_NAME FROM ENTITY WHERE ENTITY_TABLE_ID=1012 AND ABBREVIATION LIKE '" & sLookupString & "' AND ABBREVIATION >= '" & sIndex & "' ORDER BY ABBREVIATION"
	end if
ElseIf UCase(sLookupType)="CODE.STATES" Then
	if blnOracle=1 then
		sSQL="SELECT STATE_ROW_ID, STATE_ID, STATE_NAME FROM STATES WHERE (DELETED_FLAG = 0 OR DELETED_FLAG IS NULL) AND UPPER(STATE_ID) LIKE '" & sLookupString & "' AND UPPER(STATE_ID) >= '" & ucase(sIndex) & "'"
	else
		sSQL="SELECT STATE_ROW_ID, STATE_ID, STATE_NAME FROM STATES WHERE (DELETED_FLAG = 0 OR DELETED_FLAG IS NULL) AND STATE_ID LIKE '" & sLookupString & "' AND STATE_ID >= '" & sIndex & "'"
	end if
ElseIf UCase(sLookupType)="CODE.RM_SYS_USERS" Then
	sDSN = Application(APP_SECURITYDSN)
	objRocket.DB_CloseDatabase CInt(iDB)
	iDB = objRocket.DB_OpenDatabase(lEnv, sDSN, 0)
	if blnOracle=1 then
		sSQL = "SELECT USER_TABLE.USER_ID,USER_DETAILS_TABLE.LOGIN_NAME,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME"
		sSQL = sSQL & " FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID"
		sSQL = sSQL & " AND USER_DETAILS_TABLE.DSNID = " & objSessionStr(SESSION_DSNID)
		sSQL = sSQL & " AND (UPPER(USER_DETAILS_TABLE.LOGIN_NAME) LIKE '" & sLookupString & "' OR UPPER(USER_DETAILS_TABLE.LOGIN_NAME) LIKE '" & Replace(sLookupString, "%", "*") & "')"
		sSQL = sSQL & " AND UPPER(USER_DETAILS_TABLE.LOGIN_NAME) >= '" & ucase(sIndex) & "'"
		sSQL = sSQL & " ORDER BY USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME"
	else
		sSQL = "SELECT USER_TABLE.USER_ID,USER_DETAILS_TABLE.LOGIN_NAME,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME"
		sSQL = sSQL & " FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID"
		sSQL = sSQL & " AND USER_DETAILS_TABLE.DSNID = " & objSessionStr(SESSION_DSNID)
		sSQL = sSQL & " AND (USER_DETAILS_TABLE.LOGIN_NAME LIKE '" & sLookupString & "' OR USER_DETAILS_TABLE.LOGIN_NAME LIKE '" & Replace(sLookupString, "%", "*") & "')"
		sSQL = sSQL & " AND USER_DETAILS_TABLE.LOGIN_NAME >= '" & sIndex & "'"
		sSQL = sSQL & " ORDER BY USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME"
	
	end if
ElseIf UCase(sLookupType)="CODE.CLAIM_TYPE" Then
	'TR 8/2002 filter if limits applied
	sLimits=objSessionStr(SESSION_CLAIM_TYPE_LIMITS)
	sLimitsSQL=""
	If Len(Trim(sLimits)) > 0 Then
		vArray=Split(sLimits, "|")
		For i = 0 To UBound(vArray)
		    vArray2 = Split(vArray(i), ",")
		    If lLOB = vArray2(3) And CBool(vArray2(1)) = false then
				sLimitsSQL=sLimitsSQL & "," & vArray2(0)	        
		    End If
		Next
		if Len(Trim(sLimitsSQL)) > 0 then sLimitsSQL = " AND CODES.CODE_ID NOT IN (" & Mid(sLimitsSQL, 2) & ") "
	else
		sLimitsSQL=""
	end if
	if blnOracle=1 then
		sSQL="SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME)='" & ucase(Mid(sLookupType,6)) & "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND UPPER(CODES.SHORT_CODE) LIKE '" & sLookupString & "' " & sLOBSQL & sLimitsSQL & " AND UPPER(CODES.SHORT_CODE) >= '" & sIndex & "' ORDER BY CODES.SHORT_CODE"
	else
		sSQL="SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='" & Mid(sLookupType,6) & "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND CODES.SHORT_CODE LIKE '" & sLookupString & "' " & sLOBSQL & sLimitsSQL & " AND CODES.SHORT_CODE >= '" & sIndex & "' ORDER BY CODES.SHORT_CODE"
	end if
ElseIf UCase(sLookupType)="CODE.EVENT_TYPE" Then
	'TR 8/2002 filter if limits applied
	sLimits=objSessionStr(SESSION_EVENT_TYPE_LIMITS)
	sLimitsSQL=""
	If Len(Trim(sLimits)) > 0 Then
		vArray=Split(sLimits, "|")
		For i = 0 To UBound(vArray)
		    vArray2 = Split(vArray(i), ",")
		    If CBool(vArray2(1)) = false then
				sLimitsSQL=sLimitsSQL & "," & vArray2(0)	        
		    End If
		Next
		if Len(Trim(sLimitsSQL)) > 0 then sLimitsSQL = " AND CODES.CODE_ID NOT IN (" & Mid(sLimitsSQL, 2) & ") "
	else
		sLimitsSQL=""
	end if
	if blnOracle=1 then
		sSQL="SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME)='" & ucase(Mid(sLookupType,6)) & "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND UPPER(CODES.SHORT_CODE) LIKE '" & sLookupString & "' " & sLOBSQL & sLimitsSQL & "AND UPPER(CODES.SHORT_CODE) >= '" & ucase(sIndex) & "' ORDER BY CODES.SHORT_CODE"
	else
		sSQL="SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='" & Mid(sLookupType,6) & "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND CODES.SHORT_CODE LIKE '" & sLookupString & "' " & sLOBSQL & sLimitsSQL & "AND CODES.SHORT_CODE >= '" & sIndex & "' ORDER BY CODES.SHORT_CODE"
	end if
'DF MITS 13372 10/1/2008
ElseIf UCase(sLookupType)="CODE.TRANS_TYPES" Then
	lLob=objSessionStr(SESSION_LOB)
	if  m_lClaimID <> 0 then
		GetClaimType m_lClaimID, sDSN 
	end if		
	if blnOracle=1 then
		'sSQL="SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME)='" & ucase(Mid(sLookupType,6)) & "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND UPPER(CODES.SHORT_CODE) LIKE '" & sLookupString & "' " & sLOBSQL & "AND UPPER(CODES.SHORT_CODE) >= '" & ucase(sIndex) & "' ORDER BY CODES.SHORT_CODE"
		'sSQL="SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY,CODES C2 WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME)='" & ucase(Mid(sLookupType,6)) & "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND UPPER(CODES.SHORT_CODE) LIKE '" & ucase(sLookupString) & "' AND UPPER(CODES.SHORT_CODE) >= '" & ucase(sIndex) & "'"
		sSQL="SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY,CODES C2 WHERE GLOSSARY.SYSTEM_TABLE_NAME='" & ucase(Mid(sLookupType,6)) & "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND CODES.SHORT_CODE LIKE '" & ucase(sLookupString) & "' AND CODES.SHORT_CODE >= '" & ucase(sIndex) & "'"
		if(lLob="" or lLob="0") then
			sSQL = sSQL & " AND UPPER(C2.CODE_ID) = UPPER(CODES.RELATED_CODE_ID)"
		else
			if bReservesByClaimType (lLob) then
				sSQL = sSQL & " AND C2.CODE_ID=CODES.RELATED_CODE_ID AND UPPER(C2.CODE_ID) IN(SELECT RESERVE_TYPE_CODE FROM SYS_CLM_TYPE_RES WHERE UPPER(LINE_OF_BUS_CODE) = " & lLob & " AND CLAIM_TYPE_CODE = " & m_lClaimType  & ")"
	   	 	else
				sSQL = sSQL & " AND C2.CODE_ID=CODES.RELATED_CODE_ID AND UPPER(C2.CODE_ID) IN(SELECT RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES WHERE UPPER(LINE_OF_BUS_CODE) = " & lLob & ")"
			end if	
		end if
	else
		'sSQL="SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='" & Mid(sLookupType,6) & "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND CODES.SHORT_CODE LIKE '" & sLookupString & "' " & sLOBSQL & "AND CODES.SHORT_CODE >= '" & sIndex & "' ORDER BY CODES.SHORT_CODE"
		sSQL="SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY,CODES C2 WHERE GLOSSARY.SYSTEM_TABLE_NAME='" & Mid(sLookupType,6) & "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND CODES.SHORT_CODE LIKE '" & sLookupString & "' AND CODES.SHORT_CODE >= '" & sIndex & "'"
		if(lLob="" or lLob="0") then
			sSQL = sSQL & " AND C2.CODE_ID=CODES.RELATED_CODE_ID"
		else
			if bReservesByClaimType (lLob) then
				sSQL = sSQL & " AND C2.CODE_ID=CODES.RELATED_CODE_ID AND C2.CODE_ID IN(SELECT RESERVE_TYPE_CODE FROM SYS_CLM_TYPE_RES WHERE LINE_OF_BUS_CODE=" & lLob & " AND CLAIM_TYPE_CODE = " & m_lClaimType  & ")"
	   	 	else
				sSQL = sSQL & " AND C2.CODE_ID=CODES.RELATED_CODE_ID AND C2.CODE_ID IN(SELECT RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES WHERE LINE_OF_BUS_CODE=" & lLob & ")"
			end if	
		end if
	end if
	'DF MITS 13372 10/15/2008
	sSQL = sSQL & "	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) "
	sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) "
	sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" & m_sToday & "' AND CODES.EFF_END_DATE IS NULL) "
	sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" & m_sToday & "') "
	sSQL = sSQL & " OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" & m_sToday & "' AND CODES.EFF_END_DATE>='" & m_sToday & "') "
	ssql = ssql & " OR (CODES.TRIGGER_DATE_FIELD IS NOT NULL AND CODES.TRIGGER_DATE_FIELD != 'SYSTEM_DATE')) "

	sSQL = sSQL & " ORDER BY CODES.SHORT_CODE"
ElseIf UCase(Left(sLookupType,5))="CODE." Then
	if blnOracle=1 then
		sSQL="SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME)='" & ucase(Mid(sLookupType,6)) & "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND UPPER(CODES.SHORT_CODE) LIKE '" & sLookupString & "' " & sLOBSQL & "AND UPPER(CODES.SHORT_CODE) >= '" & ucase(sIndex) & "' ORDER BY CODES.SHORT_CODE"
	else
		sSQL="SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='" & Mid(sLookupType,6) & "' AND GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE=1033 AND  (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND CODES.SHORT_CODE LIKE '" & sLookupString & "' " & sLOBSQL & "AND CODES.SHORT_CODE >= '" & sIndex & "' ORDER BY CODES.SHORT_CODE"
	end if
	
ElseIf UCase(Left(sLookupType,6))="ENTITY" Then
	If Instr(sLookupType,".")>0 Then
		' Specific Search for Entity Type
		if blnOracle=1 then
			sSQL="SELECT ENTITY_ID, LAST_NAME, FIRST_NAME FROM ENTITY,GLOSSARY WHERE UPPER(GLOSSARY.SYSTEM_TABLE_NAME)='" & ucase(Mid(sLookupType,8)) & "' AND GLOSSARY.TABLE_ID=ENTITY.ENTITY_TABLE_ID AND (ENTITY.DELETED_FLAG = 0 OR ENTITY.DELETED_FLAG IS NULL) AND UPPER(LAST_NAME) LIKE '" & sLookupString & "' AND UPPER(LAST_NAME) >= '" & ucase(sIndex) & "' ORDER BY LAST_NAME"
		else
			sSQL="SELECT ENTITY_ID, LAST_NAME, FIRST_NAME FROM ENTITY,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='" & Mid(sLookupType,8) & "' AND GLOSSARY.TABLE_ID=ENTITY.ENTITY_TABLE_ID AND (ENTITY.DELETED_FLAG = 0 OR ENTITY.DELETED_FLAG IS NULL) AND LAST_NAME LIKE '" & sLookupString & "' AND LAST_NAME >= '" & sIndex & "' ORDER BY LAST_NAME"
		end if
	Else
		' All Entities search
		if blnOracle=1 then
			sSQL="SELECT ENTITY_ID, LAST_NAME, FIRST_NAME FROM ENTITY WHERE (ENTITY.DELETED_FLAG = 0 OR ENTITY.DELETED_FLAG IS NULL) AND UPPER(LAST_NAME) LIKE '" & sLookupString & "' AND UPPER(LAST_NAME) >= '" & ucase(sIndex) & "' ORDER BY LAST_NAME"
		else
			sSQL="SELECT ENTITY_ID, LAST_NAME, FIRST_NAME FROM ENTITY WHERE (ENTITY.DELETED_FLAG = 0 OR ENTITY.DELETED_FLAG IS NULL) AND LAST_NAME LIKE '" & sLookupString & "' AND LAST_NAME >= '" & sIndex & "' ORDER BY LAST_NAME"
		end if
	End If
ElseIf UCase(sLookupType)="VEHICLE" Then
	if blnOracle=1 then
		sSQL="SELECT UNIT_ID, VIN, VEHICLE_MAKE, VEHICLE_MODEL, VEHICLE_YEAR FROM VEHICLE WHERE UPPER(VIN) LIKE '" & sLookupString & "' AND UPPER(VIN) >= '" & ucase(sIndex) & "' ORDER BY VIN"
	else
		sSQL="SELECT UNIT_ID, VIN, VEHICLE_MAKE, VEHICLE_MODEL, VEHICLE_YEAR FROM VEHICLE WHERE VIN LIKE '" & sLookupString & "' AND VIN >= '" & sIndex & "' ORDER BY VIN"
	end if
ElseIf UCase(sLookupType)="CLAIM" Then
	if blnOracle=1 then
		sSQL="SELECT CLAIM_ID, CLAIM_NUMBER, EVENT_NUMBER FROM CLAIM WHERE UPPER(CLAIM_NUMBER) LIKE '" & sLookupString & "' AND UPPER(CLAIM_NUMBER) >= '" & ucase(sIndex) & "' ORDER BY CLAIM_NUMBER"
	else
		sSQL="SELECT CLAIM_ID, CLAIM_NUMBER, EVENT_NUMBER FROM CLAIM WHERE CLAIM_NUMBER LIKE '" & sLookupString & "' AND CLAIM_NUMBER >= '" & sIndex & "' ORDER BY CLAIM_NUMBER"
	end if
ElseIf UCase(sLookupType)="EVENT" Then
	if blnOracle=1 then
		sSQL="SELECT EVENT_ID, EVENT_NUMBER, CODES_TEXT.CODE_DESC FROM EVENT,CODES_TEXT WHERE CODES_TEXT.CODE_ID=EVENT.EVENT_TYPE_CODE AND UPPER(EVENT_NUMBER) LIKE '" & sLookupString & "' AND UPPER(EVENT_NUMBER) >= '" & sIndex & "' ORDER BY EVENT_NUMBER"
	else
		sSQL="SELECT EVENT_ID, EVENT_NUMBER, CODES_TEXT.CODE_DESC FROM EVENT,CODES_TEXT WHERE CODES_TEXT.CODE_ID=EVENT.EVENT_TYPE_CODE AND EVENT_NUMBER LIKE '" & sLookupString & "' AND EVENT_NUMBER >= '" & sIndex & "' ORDER BY EVENT_NUMBER"
	end if
'ElseIf sLookupType="policy" Then
'	sSQL="SELECT POLICY_ID, POLICY_NUMBER, POLICY_NAME FROM POLICY WHERE POLICY_NAME LIKE '" & sLookupString & "' ORDER BY POLICY_NAME"
Else
	Response.Write "Unknown Lookup Type."
	Response.End
End If

iRS=objRocket.DB_CreateRecordset(iDB, sSQL, 3, 0)
If objRocket.DB_Eof(iRS) Then
	' Display could not find data matching the criteria
	WriteDataNotFound
Else
	WriteData
End If

objRocket.DB_CloseRecordset CInt(iRS), 2
objRocket.DB_CloseDatabase CInt(iDB)
objRocket.DB_FreeEnvironment CLng(lEnv)
Set objRocket=Nothing

%>

<%Sub WriteDataNotFound()%>
<html><head>
<link rel="stylesheet" href="RMNet.css" type="text/css" />
<script language="JavaScript">
function handleUnload()
{
	if(window.opener!=null)
	{
		window.opener.m_LookupBusy=false;
		window.opener.document.onCodeClose();
	}
	return true;
}
function haveProperty(obj, sPropName)
{
	for(p in obj)
	{
		if(p==sPropName)
		{
			return true;
		}
	}
	return false;
}

/*BSB 01.09.2003 Added FormSubmit in order to Clear out any existing 
 auto-fill junk.
*/
function ClearAutoFilled()
{
		var sId, sText,junk;
		sId="-10";
		
		sText=document.frmData.resulttext;
		if(document.frmData.lookuptype.value.substring(0,5)=="code.")
		{
			window.opener.document.codeSelected(sText,sId);
			
		}
		else
		{
			
			if(haveProperty(window.opener,"lookupCallback"))
			{
				if(window.opener.lookupCallback!=null)
				{
					eval("window.opener."+window.opener.lookupCallback+"("+sId+")");
					return false;
				}
			}
			
			window.opener.document.entitySelected(sId);
		}
	return false;
	}
	
function handleOnload()
{
	document.frmData.cmdClose.focus();
	return true;
}
</script>
</head>
<body onload="return handleOnload();" onUnload="handleUnload();">
<table align="center" width="100%" border="0">
	<tr>
		<td width="100%" valign="middle" align="center">
			<font face="Verdana, Arial" size="4">
				<b>Zero records match the criteria '<%=sFind%>' </b>
			</font>
				<%If bCreatable Then%>
					<p><p>
					<b>Choose &quot;Add New&quot; to add a new record.<b><br>
					<!--changed by ravi on march-07-03-->
					<script language="javascript">
					if(window.opener.getCurrentID()!='0')
						{document.write('<b>Choose &quot;Edit&quot; to edit this record.<b>');
						}
					</script>
					<!---end-->
					
				<%End If%>
		</td>
	</tr>
	<tr>
		<td width="100%" valign="middle" align="center">
			<form name="frmData" action>
				&nbsp;<br />&nbsp;<br />
					<input type="hidden" name="lookuptype" value="<%=sLookupType%>">
					<input type="hidden" name="resultid" value="<%=sId%>">
					<input type="hidden" name="resulttext" value="<%=sFind%>">
				<%If bCreatable Then%>
					<!--changed by ravi on march-07-03-->
					<script language="javascript">
					if(window.opener.getCurrentID()!='0')
						{document.write('<input class="button" type="button" name="cmdEdit" onClick="window.opener.RequestRestore(window.document.frmData.resulttext.value);window.close();" value=" Edit " />');
						}
					</script>
					<!---end-->
					<input class="button" type="button" name="cmdKeep" onClick="window.opener.RequestRestore(window.document.frmData.resulttext.value);ClearAutoFilled();window.close();" value=" Add New " />
					<input class="button" type="button" name="cmdClose" onClick="window.opener.RequestCancel();window.close();" value=" Cancel " />
				<%Else%>
					<input class="button" type="button" name="cmdClose" onClick="window.opener.RequestCancel();window.close();" value=" Close " />
				<%End If%>
			</form>
		</td>
	</tr>
</table>
</body>
</html>
<%End Sub%>

<%Sub WriteData()
Dim sOption
Dim sText, v, i, c

objRocket.DB_GetData iRS,1,sId
sId="" & sId
sText=""
'----------added by ravi on 24th jan
if Ucase(Left(sLookupType,6))="ENTITY" then

	dim compLastName,flgSingleRecord
	flgSingleRecord=0
	objRocket.DB_GetData iRS,"LAST_NAME",compLastName
	
end if
'-------end----------------
'sLookupString
c=objRocket.DB_ColumnCount(iRS)
For i=2 To c
	objRocket.DB_GetData iRS,i,v
	If UCase(objRocket.DB_ColumnName(iRS, i))="BIRTH_DATE" Then
		If Not IsNull(v) Then
		sText=sText & " " & Mid(v,5,2) & "/" & Right(v,2) & "/" & Left(v,4)
		End If
	Else
	sText=sText & v & " "
	end if
Next

sOption="<option value=""" & sId & """ selected="""">" & sText & "</option>" & vbCrLf

objRocket.DB_MoveNext iRS
'If objRocket.DB_Eof(iRS) then///changed by ravi on 24th jan------------
if Left(sLookupType,6)="entity" then
	If objRocket.DB_Eof(iRS) and sLookupString=compLastName and not bCreatable Then ' changede by ravi
	
		' There was only one record, perfect match
		WriteOneRecordMatch sId, sText
		Exit Sub
	elseif objRocket.DB_Eof(iRS) and sLookupString<>compLastName Then
		flgSingleRecord=1
	End If
else
	If objRocket.DB_Eof(iRS) and not bCreatable then
		' There was only one record, perfect match
		WriteOneRecordMatch sId, sText
		Exit Sub
	end if
end if
'------------end--------------
%>
<html>
<head><link rel="stylesheet" href="RMNet.css" type="text/css" />
<title>Quick Lookup Results</title>
<script language="JavaScript">
function FormSubmit()
{
	if(document.frmData.optResults.selectedIndex<0)
	{
		alert("Please select the record.");
	}
	else
	{
		var sId, sText
		sId=document.frmData.optResults.options[document.frmData.optResults.selectedIndex].value;
		sText=document.frmData.optResults.options[document.frmData.optResults.selectedIndex].text;
		//alert(document.frmData.optResults.options[document.frmData.optResults.selectedIndex].value+"  "+document.frmData.optResults.options[document.frmData.optResults.selectedIndex].text);
		//opener.document.QuickLookup(document.frmData.optResults.options[document.frmData.optResults.selectedIndex].value,document.frmData.optResults.options[document.frmData.optResults.selectedIndex].text);
		if(document.frmData.lookuptype.value.substring(0,5)=="code.")
		{
			window.opener.document.codeSelected(sText,sId);
			self.close();
		}
		else
		{
			if(haveProperty(window.opener,"lookupCallback"))
			{
				if(window.opener.lookupCallback!=null)
				{
					eval("window.opener."+window.opener.lookupCallback+"("+sId+")");
					self.close();
					return false;
				}
			}
			window.opener.document.entitySelected(sId);
			self.close();
		}
	}
	return false;
}
function ResultsKeyDown()
{
	if(event.keyCode==13)
	{
		FormSubmit();
	}
	return true;
}
function selCode(sCodeText,lCodeId)
{
	if(lCodeId!=0)
	{
		// Put the code on the calling form+close this one
		
	}
	return false;
}
function handleUnload()
{

	if(window.opener!=null)
	{
		window.opener.m_LookupBusy=false;
		window.opener.document.onCodeClose();
	}
	return true;
}
function haveProperty(obj, sPropName)
{
	for(p in obj)
	{
		if(p==sPropName)
			return true;
	}
	return false;
}

/*BSB 01.09.2003 Added FormSubmit in order to Clear out any existing 
 auto-fill junk.
*/
function ClearAutoFilled()
{
		var sId, sText,junk;
		sId="-10";
		sText=document.frmData.resulttext;
		if(document.frmData.lookuptype.value.substring(0,5)=="code.")
		{
			window.opener.document.codeSelected(sText,sId);
		}
		else
		{
			if(haveProperty(window.opener,"lookupCallback"))
			{
				if(window.opener.lookupCallback!=null)
				{
					eval("window.opener."+window.opener.lookupCallback+"("+sId+")");
					return false;
				}
			}
			window.opener.document.entitySelected(sId);
		}
	return false;
	}
	
function DoNext()
{
	document.frmData.action="quicklookup1.asp";
	document.frmData.submit();
}
</script>
</head>
<body onload="document.frmData.optResults.focus();" onUnload="handleUnload()">
<table align="center" width="100%" border="0">
	<tr>
		<td width="100%" valign="middle" align="center">
			<font face="Verdana, Arial" size="2">
			<b>Record(s) matching the criteria '<%=sFind%>'</b>
			</font>
		</td>
	</tr>
	<tr>
		<td width="100%" valign="middle" align="center">
			<form name="frmData" onSubmit="return FormSubmit()">
				<input type="hidden" name="lookuptype" value="<%=sLookupType%>" />
				<select name="optResults" size="10" style="width: 100%" onkeydown="ResultsKeyDown();" ondblclick="document.frmData.cmdSubmit.click();">
<%				If sOption<>"" Then Response.Write sOption
				iCount=0
				Do While Not objRocket.DB_Eof(iRS) And iCount<=ListLimit-1
					If iCount<ListLimit-1 Then
						objRocket.DB_GetData iRS,1,sId
						sId="" & sId
						sText=""
						c=objRocket.DB_ColumnCount(iRS)
						For i=2 To c
							objRocket.DB_GetData iRS,i,v
							If UCase(objRocket.DB_ColumnName(iRS, i))="BIRTH_DATE" Then
								If Not IsNull(v) Then
									sText=sText & " " & Mid(v,5,2) & "/" & Right(v,2) & "/" & Left(v,4)
								End If
							Else
							sText=sText & v & " "
							end if
						Next
						Response.Write "<option value=""" & sId & """>" & sText & "</option>" & vbCrLf
						iCount=iCount+1
						objRocket.DB_MoveNext iRS
					Else
						objRocket.DB_GetData iRS,2,v
						sIndex="" & v
						iCount = ListLimit+1
					End If
				Loop
%>
				</select>
				<%If bCreatable Then%>
				<font face="Verdana, Arial" size="2">
					<br>
					<b>Choose a record, or press 'Add New' to add a new record.</b>
				</font>
				<%End If%>
				<br />
				<input class="button" type="submit" name="cmdSubmit" value="Choose Highlighted" />
<%				If icount = ListLimit+1 Then %>
					<input class="button" type="button" name="btnNext" onClick="DoNext();" value="Next <%=ListLimit%>">
					<input type="hidden" name="index" value="<%=sIndex%>">
					<input type="hidden" name="find" value="<%=sFind%>">
<%				End if %>
				<%If bCreatable Then%>
					<input type="hidden" name="resultid" value="<%=sId%>">
					<input type="hidden" name="resulttext" value="<%=sFind%>">
					<input class="button" type="button" name="cmdKeep" onClick="window.opener.RequestRestore(window.document.frmData.resulttext.value);ClearAutoFilled();window.close();" value=" Add New " />
				<% End If%>
				<input class="button" type="button" name="cmdCancel" value="Cancel" onClick="window.opener.RequestCancel();self.close();" />
			</form>
		</td>
	</tr>
</table>
</body>
</html>
<%End Sub%>

<%Sub WriteOneRecordMatch(sId, sText)%>
<html>
<link rel="stylesheet" href="RMNet.css" type="text/css" />
<head><title>Quick Lookup Results</title>
<script language="JavaScript">
function TransferData()
{
	var sId, sText
	sId=document.frmData.resultid.value;
	sText=document.frmData.resulttext.value;
	if(document.frmData.lookuptype.value.substring(0,5)=="code.")
	{
		window.opener.document.codeSelected(sText,sId);
	}
	else
	{
		if(haveProperty(window.opener,"lookupCallback"))
		{
			if(window.opener.lookupCallback!=null)
			{
				eval("window.opener."+window.opener.lookupCallback+"("+sId+")");
				return false;
			}
		}
		window.opener.document.entitySelected(sId);
	}
	return false;
}

function handleUnload()
{
	if(window.opener!=null)
	{
		window.opener.m_LookupBusy=false;
		window.opener.document.onCodeClose();
	}
	return true;
}
function haveProperty(obj, sPropName)
{
	for(p in obj)
	{
		if(p==sPropName)
			return true;
	}
	return false;
}
</script>
</head>
<body onload="TransferData();" onUnload="handleUnload()">
<br />&nbsp;<br />&nbsp;<br />&nbsp;<br />&nbsp;
<!--<table align="center" width="100%" border="0">
	<tr>	
		<td valign="center"><img src="img/anglobe.gif"></td>
		<td width="100%" valign="middle" align="center"><font fae="Arial, Helvetica" size="7"><b>Please wait...</b></font></td>
	</tr>
</table>-->
<form name="frmData">
	<input type="hidden" name="lookuptype" value="<%=sLookupType%>" />
	<input type="hidden" name="resultid" value="<%=sId%>" />
	<input type="hidden" name="resulttext" value="<%=sText%>" />
</form>
</body>
</html>
<%
End Sub
'DF MITS 13372 10/2/2008
function bReservesByClaimType(lLOB) 
	Dim r, henv, db
	Dim sSQL, rs, sReservesByClaimType
	
	Set r = CreateObject("DTGRocket")
	henv = r.DB_InitEnvironment()
	db = r.DB_OpenDatabase(henv, sDSN, 0)
	
	sSQL = "SELECT RES_BY_CLM_TYPE FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE='" & lLOB & "'"
	rs=r.DB_CreateRecordset(db, sSQL, 3, 0)
	If Not r.DB_Eof(rs) Then
		r.DB_GetData rs,1,sReservesByClaimType
		If sReservesByClaimType <> 0 Then
		   bReservesByClaimType = True
		Else
		   bReservesByClaimType = False
		End if
	End If
	
	r.DB_CloseRecordset cint(rs), 2
	r.DB_CloseDatabase cint(db)
	r.DB_FreeEnvironment cint(henv)
	Set r = Nothing
End Function	
'DF MITS 13372 10/2/2008
Sub GetClaimType(lClaimId, sDSN)
	Dim r, henv, db
	Dim sSQL, rs
	
	Set r = CreateObject("DTGRocket")
	henv = r.DB_InitEnvironment()
	db = r.DB_OpenDatabase(henv, sDSN, 0)
	
	sSQL = "SELECT CLAIM_TYPE_CODE FROM CLAIM WHERE CLAIM_ID = " & lClaimId
	rs=r.DB_CreateRecordset(db, sSQL, 3, 0)

	r.DB_GetData rs,1,m_lClaimType 

	r.DB_CloseRecordset cint(rs), 2
	r.DB_CloseDatabase cint(db)
	r.DB_FreeEnvironment cint(henv)
	Set r = Nothing
End Sub

%>
