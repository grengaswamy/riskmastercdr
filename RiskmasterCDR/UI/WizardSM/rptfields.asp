<%'Option Explicit%>
<% ' Neelima%>
<!-- #include file ="SessionInitialize.asp" -->
<!-- #include file ="generic.asp" -->
<!-- #include file ="customutil.inc" -->


<html>
	<head>
		<title>Report Fields</title>
		<link rel="stylesheet" href="RMNet.css" type="text/css" />
		<script language="JavaScript" SRC="SMD.js"></script>
		<script>
			function derived_click()
			{
				if(document.frmData.lstselfields.length > 0)
					openDerivedField1(true, 0);
				else
					openDerivedField1(false, 0);
			}
			
			function UpdateSmFieldsList()
			{
				document.frmData.hdSubmit.value = "START_TABLE"
				tabNavigation(0,0);
			}
			
			function onDown()
			{
				if ((window.document.frmData.lstUsers.length >0) && (window.document.frmData.lstselfields.length >0) )
				{
					
					document.frmData.hdSubmit.value='DOWN';
					tabNavigation(0,0);
				}
				else
					return false;
			
			}
			
			function onUp()
			{
				document.frmData.hdSubmit.value='UP';
				tabNavigation(0,0);
			}
			
			function window_onLoad()
			{
				if (document.frmData.lstselfields.length <= 0)
					document.frmData.btnDel.disabled = true;
				
				if (document.frmData.lstUsers.length >= 0 && document.frmData.lstselfields.length > 0 )
					document.frmData.btnDnlvl.disabled = false;
					
				if ((document.frmData.hdStackTableID.value == '') || (document.frmData.hdStackTableID.value.indexOf(",") == -1))
					document.frmData.btnUplvl.disabled = true;
				
				//Code for selecting the last list item on body onload------------>>Rajeev-23/05/30
				if(document.frmData.lstselfields.length > 0)
				{
					document.frmData.lstselfields.selectedIndex = 0;
				}
					
				if(document.frmData.lstselfields.length > 1 )
				{
					document.frmData.btnDown.disabled = false;
				}
	
				if(document.frmData.lstselfields.length > 0)
					window.document.frmData.cboTables.disabled = true;

				if(document.frmData.lstselfields.length <= 0 ||document.frmData.lstUsers.length <= 0)
				{
					window.document.frmData.btnDnlvl.disabled = true;
				}
				else
				{
					window.document.frmData.btnDnlvl.disabled = false;
				}
				
				if(document.frmData.lstselfields.length > 0)
				{
					window.document.frmData.btnProperties.disabled = false;
					
				}
				else
				{
					window.document.frmData.btnProperties.disabled = true;
				}
				
				if(document.frmData.lstTableFields.length <= 0)
				{
					window.document.frmData.btnAdd.disabled = true;
				}
				else
				{
					window.document.frmData.btnAdd.disabled = false;
				}
				
				if (window.document.frmData.hdOpenProp.value!='-2')
				{
					var tempValue = window.document.frmData.hdOpenProp.value;
					window.document.frmData.hdOpenProp.value = '-2';
					window.setTimeout(window.open('properties.asp?b='+tf[tempValue]+'&c='+arrFieldType[tf[tempValue]],'prop','resizable=yes,Width=650,Height=525,top='+(screen.availHeight-525)/2+',left='+(screen.availWidth-650)/2+''),1)
				}
				if (window.document.frmData.hdCValue.value!='')
				{				  
				  var wnd = window.open('properties.asp?b='+window.document.frmData.hdBValue.value +'&c='+window.document.frmData.hdCValue.value,'XprogressWnd','Width=650,Height=525,top='+(screen.availHeight-525)/2+',left='+(screen.availWidth-650)/2+'')				  
				  wnd.resizeTo(625,525);
				  window.document.frmData.hdBValue.value=''
				  window.document.frmData.hdCValue.value=''				  
				}
			}	
			
			function btnUp_click()
			{
				if(window.document.frmData.lstselfields.selectedIndex != window.document.frmData.lstselfields.length-1)
				{
					window.document.frmData.btnDown.disabled = false;
				}
				
				if(window.document.frmData.lstselfields.selectedIndex == 0)
				{
					window.document.frmData.btnUp.disabled = true;
				}
				
			}	
			
			function btnDown_click()
			{
				if(window.document.frmData.lstselfields.selectedIndex != 0)
				{
					window.document.frmData.btnUp.disabled = false;
				}
				
				if(window.document.frmData.lstselfields.selectedIndex == window.document.frmData.lstselfields.length-1)
				{
					window.document.frmData.btnDown.disabled = true;
				}
			}
			function scrollBehavior()
			{
				if(window.document.frmData.lstselfields.selectedIndex >= 15 && window.event.keyCode == 40)
					document.all.divSelectedField.doScroll("scrollbarDown");
				else if(window.event.keyCode == 38)
					document.all.divSelectedField.doScroll("scrollbarUp");
				else if(window.event.keyCode == 34)
					document.all.divSelectedField.doScroll("scrollbarPageDown");
				else if(window.event.keyCode == 33)
					document.all.divSelectedField.doScroll("scrollbarPageUp");
					
			}
			
			function openDerivedField2(pText,pBtnClick,pTabID)
			{
				window.document.frmData.hdDerFieldName.value = pText;
				window.open('derivedfields_2.asp?usercolname='+pText+'&UpdateMode='+pBtnClick+'&TABID='+pTabID,'der2','resizable=yes,Width=575,Height=465,top='+(screen.availHeight-465)/2+',left='+(screen.availWidth-575)/2+'');
			}	
	
			function editDerivedField2(pText,pBtnClick,pTabID)
			{
				window.document.frmData.hdDerFieldName.value = pText;
				window.open('derivedfields_2.asp?usercolname='+pText+'&UpdateMode='+pBtnClick+'&TABID='+pTabID,'der2','resizable=yes,Width=575,Height=465,top='+(screen.availHeight-465)/2+',left='+(screen.availWidth-575)/2+'');
			}	
			
			function openGScript(pTabID)
			{
				window.open('globalscript.asp?TABID='+pTabID,'glob','resizable=yes,Width=500,Height=225,top='+(screen.availHeight-225)/2+',left='+(screen.availWidth-500)/2+'');				
			}			
		</script>
	</head>
<%
Dim iStart, id, TName, strTBStack, objData, lTableId
Dim n, j, a, nSize, TableName, TableID, z, iCounter, sGlobalScript

Dim objAggregate, objLabel

Dim summary, detail, both
detail = ""
summary = ""
both = ""

'Neelima
If Not CBool(Application(APP_APPSTART)) then
	AppStart
end if

'^-- Rajeev
'-- Starting Table combo box shows the tables in sorted order
'-- The selected index of Starting Table combo box can be used to get the table information e.g. name or id
'-- Collect selected index
If Request.Form("cboTables") <> "" Then
	iStart = Request.Form("cboTables")
End if

'-- Navigated from Documentsmenu.asp in RMNET Or New Toolbar Button in SMD
'-- Querystring 'MODE=0' is passed
'-- Custom session can't be initialized with blank
'-- Initialize all the custom session variables as soon as SMD is opened first time
If Request.QueryString("MODE") <> "" Then 
	objSession("REPORTNAME") = "-1"
	objSession("MODE1") = Request.QueryString("MODE")
	objSession("Save") = "0"			'-- Tracks whether report has been saved in database
	objSession("SaveFinal") = "0"		'-- Suppress Save Report Dialog window
	objSession("ColLength") = "-1"		'-- Tracks no. of report columns selected
	objSession("ArrayP") = "-1"			'-- Stores usercolname attribute of a column tag in XML
	objSession("ColumnsInvolved") = "-1" '-- Keep the track of columns involved in a derived field
	objSession("ArrayFieldType") = "-1"	
	objSession("ArrayColProp") = "-1"	
	objSession("ArrayPFields") = "-1"	'-- Stores unique name of columns neglecting no. of instances e.g. City1, City2
	objSession("ArrayTF") = "-1"		'-- Maintains report column order
	objSession("CurrentLevel") = "-1"	'-- Tracks the current level pressing DOWN/UP level buttons
	objSession("TableIDStack") = "-1"	'-- Maintains the tables ids selected using DOWN level
	objSession("TableNameStack") = "-1"	'-- Maintains the tables names selected using DOWN level
	objSession("ArrayPXML") = "-1"		'-- Maintains column tags of columnlist tag in XML
	objSession("ArrayCurrentFields") = "-1"
	both= "checked"
	objSession("ArrayPCriteria") = "-1" '-- Blank out the criteria in session
	objSession("ArrayPGlobalCriteria") = "-1" '-- Blank out the criteria in session
	objSession("LOB") = "-1"	
	objSession("PreviewJobID") = "-1"
	objSession("OPENPROPERTY") = "-2"
	'-- Delete the temporary report if the user clicks on the New button/Designer link
	Call CleanUp()
End If

'-- objSession("MODE1") tracks the mode : new, open, edit etc.
'-- New mode -- no report id available by now
If objSessionStr("MODE1") = "0" Then 
	objSession("REPORTID") = "-1"
	objSession("REPORTNAME") = "-1"
End If

'-- Navigated from smopenreport.asp
'-- Initialize all the custom session variables as soon as existing report is opened
'-- Report id is passed as querystring
If Request.QueryString("REPORTID") <> "" Then 
	objSession("REPORTID") = Request.QueryString("REPORTID")
	objSession("Save") = "1"
	objSession("SaveFinal") = "1" 
	objSession("MODE1") = "1"
	'''objSession("CurrentLevel") = "-1"
	objSession("ArrayP") = "-1"
	objSession("ColumnsInvolved") = "-1" '-- Keep the track of columns involved in a derived field
	objSession("ArrayFieldType") = "-1"
	objSession("ArrayColProp") = "-1"	
	objSession("ArrayPFields") = "-1"
	objSession("ArrayTF") = "-1"
	objSession("TableIDStack") = "-1"
	objSession("TableNameStack") = "-1"
	objSession("ArrayPXML") = "-1"
	objSession("ArrayCurrentFields") = "-1"
	objSession("ArrayPCriteria") = "-1" '-- Blank out the criteria in session
	objSession("ArrayPGlobalCriteria") = "-1" '-- Blank out the criteria in session
	objSession("LOB") = "-1"
	objSession("PreviewJobID") = "-1"
	objSession("OPENPROPERTY") = "-2"

End If
'^^-- Rajeev

'-- Rajeev 05/14/2003 -- Component access
Dim m_objSMList, pageError

If Not GetValidObject("InitSMList.CInitSM", m_objSMList) Then
	Call ShowError(pageError, "Call to CInitSM class failed.", True)
End If

m_objSMList.m_RMConnectStr = objSessionStr(SESSION_DSN)
m_objSMList.m_SMConnectStr = Application(APP_SMDDSN)


'-- Populating Starting Table combo box
Dim arr(), sorted_arr()

If objSessionStr(SESSION_MODULESECURITY) = "1" Then
	If Not m_objSMList.GetSMTableList(Application(APP_PERMCACHE & objSessionStr(SESSION_DSNID)), objSessionStr(SESSION_GROUPID), arr) Then
		'''Response.Write "Starting Tables couldn't be obtained...." & "<BR>"
		'''Response.Write "Please contact your web administrator...."
	End If
Else
	If Not m_objSMList.GetSMTableList(Nothing, "", arr) Then
		'''Response.Write "Starting Tables couldn't be obtained...." & "<BR>"
		'''Response.Write "Please contact your web administrator...."
	End If
End If

'-- Error -- if couldn't get array of tables
If Not IsValidArray(arr) Then 
	Call ShowError(pageError, "There are no Starting Tables available.", False)
Else
	'-- arr stores as table name and id seperated by comma
	Dim iCount, temp
	'- Extract only table names from arr and fill in sorted_arr
	For iCount = 0 To UBound(arr)
	    ReDim Preserve sorted_arr(iCount)
	    temp = Split(arr(iCount), ",")
	    sorted_arr(iCount) = temp(0)
	Next

	'-- Sort the table names to display in Starting Table combo box			
	m_objSMList.Sort sorted_arr
	

	'-- Find table id from arr for selected Starting Table
	Dim x
	For iCount = 0 To UBound(arr)
	    temp = Split(arr(iCount), ",")
	    If StrComp(sorted_arr(CInt(iStart)), temp(0)) = 0 Then
	        x = temp(1)
	       	If objSessionStr("MODE1") = "0" Or Request.Form("hdSubmit") = "START_TABLE" Or Request.Form("hdSubmit") = "DEL_FIELDS" Then
	 	        objSession("CurrentLevel") = sorted_arr(CInt(iStart)) '-- Set Current Level on change of Starting Table
	       		objSession("TableIDStack") = x
	       		objSession("TableNameStack") = sorted_arr(CInt(iStart))
			End IF
	        Exit For
	    End If
	Next
End If

Dim arrFields(), sorted_arrFields(), arrTables(), sorted_arrTables() 
Dim iIndex, bFound

'^-- Rajeev 05/09/2003 -- DOWN/UP level functionality
If Request.Form("hdSubmit") = "DOWN" Then 

 	'-- Find table id of the selected related table 
	If Not m_objSMList.GetTableIDFromSMTableList(Request.Form("lstUsers"), lTableID) Then
		'''Response.Write "Table Id couldn't be found for DOWN Leveling...." & "<BR>"
		'''Response.Write "Please contact your web administrator...."
	End If
	
	Dim tempArr
	
	If lTableID = "" Then
		'Modified by: Ankur Saxena 'MITS 6979 05/18/2006
		'Reason for change: A parameter has been added to GetTableIDFromSUPP_DICT(). This parameter will
		'                   be used to send table ID's stack.
		If m_objSMList.GetTableIDFromSUPP_DICT(Request.Form("lstUsers"), lTableID, objSession("TableIDStack")) Then
			 
			tempArr = Split(objSession("TableIDStack"), ",")
			If IsValidArray(tempArr) Then
				If (tempArr(UBound(tempArr)) = 155 Or _
					tempArr(UBound(tempArr)) = 156 Or _
					tempArr(UBound(tempArr)) = 157 Or _
					tempArr(UBound(tempArr)) = 158 Or _
					tempArr(UBound(tempArr)) = 159 Or _
					tempArr(UBound(tempArr)) = 160 Or _
					tempArr(UBound(tempArr)) = 161 Or _
					tempArr(UBound(tempArr)) = 162 Or _
					tempArr(UBound(tempArr)) = 163 Or _
					tempArr(UBound(tempArr)) = 164 Or _
					tempArr(UBound(tempArr)) = 165 Or _
					tempArr(UBound(tempArr)) = 166 Or _
					tempArr(UBound(tempArr)) = 167 Or _
					tempArr(UBound(tempArr)) = 168 Or _
					tempArr(UBound(tempArr)) = 169 Or _
					tempArr(UBound(tempArr)) = 170) Then  '-- "ENTITY_SUPP"

					m_objSMList.m_SuppTableList = tempArr(UBound(tempArr))
				End If
			End If
			m_objSMList.m_SuppTableName = Trim(Request.Form("lstUsers"))
			objSession("TableIDStack") = objSessionStr("TableIDStack") & "," & (lTableID+20000)
		Else
		End If
	Else
		objSession("TableIDStack") = objSessionStr("TableIDStack") & "," & lTableID
	End If

	objSession("TableNameStack") = objSessionStr("TableNameStack") & "," & Request.Form("lstUsers")
	objSession("CurrentLevel") = Request.Form("lstUsers")
	
	strTBStack=objSessionStr("TableIDStack")
	If InStr(1, strTBStack, ",") = 0 Then strTBStack = ""

End If

If Request.Form("hdSubmit") = "UP" Then 

 	'-- Find table id from stack 
	id = Split(Request.Form("hdStackTableID"), ",")
	TName = Split(Request.Form("hdStackTableName"), ",")
	
	If IsValidArray(TName) Then
		objSession("CurrentLevel") = TName(UBound(TName)-1)
	End If
			
	objSession("TableIDStack") = Left(Request.Form("hdStackTableID"), InStrRev(Request.Form("hdStackTableID"), ",")-1)
	objSession("TableNameStack") = Left(Request.Form("hdStackTableName"), InStrRev(Request.Form("hdStackTableName"), ",")-1)
	strTBStack=objSessionStr("TableIDStack")
	
	If InStr(1, strTBStack, ",") = 0 Then strTBStack = ""
						
End If
'^^ Rajeev 05/09/2003 -- Down / Up functionality
'---------------------------------------------------------------------

'-- Create an object of MSXML Dom Document
Dim xmlDoc 

If Not GetValidObject("Msxml2.DOMDocument", xmlDoc) Then
	Call ShowError(pageError, "XML Parser is not available.", True)
End If

xmlDoc.async = False

Dim sXML
Dim m_myArray, m_myOrder
Dim m_DateRetrieved, m_DateFormatted, m_AsOf
	
'^-- Rajeev 05/15/2003 -- Retrieve report from database	
'-- User opens an existing report or continues using saved report

If objSessionStr("REPORTID") <> "-1" And objSessionStr("Save") = "1" And objSessionStr("SaveFinal") = "1" Then
	
	If Not GetValidObject("InitSMList.CDataManager", objData) Then
		Call ShowError(pageError, "Call to CDataManager class failed.", True)
	End If
	
	objData.m_RMConnectStr = Application("SM_DSN")
	objData.ReportID = objSessionStr("REPORTID")
	objData.GetReport

	sXML = objData.XML
	objSession("REPORTNAME") = objData.REPORTNAME
	
	xmlDoc.loadXML sXML

	Set objData = Nothing
	
	'-- Parse XML for populating Starting Table on GUI

	If Request.QueryString("REPORTID") <> "" Then

		'-- Check permissions and absence of fields
		Dim sErr
		If objSessionStr(SESSION_MODULESECURITY) = "1" Then
			If Not m_objSMList.ValidateReport(Application(APP_PERMCACHE & objSessionStr(SESSION_DSNID)), objSessionStr(SESSION_GROUPID),xmlDoc,sErr) Then
				Call ShowError(pageError, "Permission check error.", True)
			End If
		Else
			If Not m_objSMList.ValidateReport(Nothing, "",xmlDoc,sErr) Then
				Call ShowError(pageError, "Permission check error.", True)
			End If
		End If
		
		If sErr <> "" Then
			Call ShowError(pageError, sErr, True)
		End If

		'-- No report column is found, show for first starting table
		If xmlDoc.getElementsByTagName("columnlist").item(0).childNodes.length = 0 Then
			objSession("CurrentLevel") = sorted_arr(0)
			m_objSMList.GetTableIDFromSMTableList sorted_arr(0), TableID
			
			objSession("TableIDStack") = TableID
			objSession("TableNameStack") = sorted_arr(0)
		Else	
			m_objSMList.GetTableNameFromSMTableList xmlDoc.getElementsByTagName("columnlist").item(0).childNodes(0).Attributes(1).text, TableName
			objSession("CurrentLevel") = TableName
			objSession("TableIDStack") = xmlDoc.getElementsByTagName("columnlist").item(0).childNodes(0).Attributes(1).text
			objSession("TableNameStack") = TableName
		End If
	End If
	
	If Not MaintainColumnsAndSessions(xmlDoc) Then
		'''Response.Write "There is no column to process...." & "<BR>"
		'''Response.Write "Please contact your web administrator....."
	End If

End if

'-- User continues tabbing but has not clicked on Save yet or Selects Starting Table
If objSessionStr("REPORTID") <> "-1"  And objSessionStr("Save") = "0" And objSessionStr("SaveFinal") = "0" Then

	If Not GetValidObject("InitSMList.CDataManager", objData) Then
		Call ShowError(pageError, "Call to CDataManager class failed.", True)
	End If
	objData.m_RMConnectStr = Application("SM_DSN")
	objData.TempReportID = objSessionStr("REPORTID")
	objData.GetTempReport

	sXML = objData.XML
	objSession("REPORTNAME") = objData.REPORTNAME

	xmlDoc.loadXML sXML

	Set objData = Nothing
		
	If Not MaintainColumnsAndSessions(xmlDoc) Then
		'''Response.Write "There is no column to process...." & "<BR>"
		'''Response.Write "Please contact your web administrator....."
	End If

End If
'^^-- Rajeev 05/15/2003 -- Retrieve report from database	

IF Request.Form("hdSubmit") <> "UP" AND Request.Form("hdSubmit") <> "DOWN" Then '-- Also handles Open report

	If Not m_objSMList.GetTableIDFromSMTableList(objSessionStr("CurrentLevel"), lTableID) Then
		'''Response.Write "Table Id couldn't be obtained...." & "<BR>"
		'''Response.Write "Please contact your web administrator...."
	End If

	If lTableID = "" Then
		'Modified by: Ankur Saxena 'MITS 6979 05/18/2006
		'Reason for change: A parameter has been added to GetTableIDFromSUPP_DICT(). This parameter will
		'                   be used to send table ID's stack.
		If m_objSMList.GetTableIDFromSUPP_DICT(objSessionStr("CurrentLevel"), lTableID, objSession("TableIDStack")) Then

			tempArr = Split(objSession("TableIDStack"), ",")
			
			If IsValidArray(tempArr) Then
				If UBound(tempArr) > 0 Then
					If (tempArr(UBound(tempArr) - 1) = 155 Or _
						tempArr(UBound(tempArr) - 1) = 156 Or _
						tempArr(UBound(tempArr) - 1) = 157 Or _
						tempArr(UBound(tempArr) - 1) = 158 Or _
						tempArr(UBound(tempArr) - 1) = 159 Or _
						tempArr(UBound(tempArr) - 1) = 160 Or _
						tempArr(UBound(tempArr) - 1) = 161 Or _
						tempArr(UBound(tempArr) - 1) = 162 Or _
						tempArr(UBound(tempArr) - 1) = 163 Or _
						tempArr(UBound(tempArr) - 1) = 164 Or _
						tempArr(UBound(tempArr) - 1) = 165 Or _
						tempArr(UBound(tempArr) - 1) = 166 Or _
						tempArr(UBound(tempArr) - 1) = 167 Or _
						tempArr(UBound(tempArr) - 1) = 168 Or _
						tempArr(UBound(tempArr) - 1) = 169 Or _
						tempArr(UBound(tempArr) - 1) = 170) Then  '-- "ENTITY_SUPP"

						m_objSMList.m_SuppTableList = tempArr(UBound(tempArr) - 1)
					End If
				End If
			End If
			m_objSMList.m_SuppTableName = Trim(objSessionStr("CurrentLevel"))
		Else
		End If
	End If

	strTBStack = objSessionStr("TableIDStack")
	
	If InStr(1, strTBStack, ",") = 0 Then strTBStack = ""

	If objSessionStr(SESSION_MODULESECURITY)="1" Then
		If Not m_objSMList.GetSMFieldList(Application(APP_PERMCACHE & objSessionStr(SESSION_DSNID)) , objSessionStr(SESSION_GROUPID),  lTableID , arrFields, arrTables,strTBStack) Then
			'''Response.Write "Fields couldn't be obtained...." & "<BR>"
			'''Response.Write "Please contact your web administrator...."
		End If
	Else
		If Not m_objSMList.GetSMFieldList(Nothing, "", lTableID , arrFields, arrTables,strTBStack) Then
			'''Response.Write "Fields couldn't be obtained...." & "<BR>"
			'''Response.Write "Please contact your web administrator...."
		End if		
	End IF

	Call GetRelatedFields()
	
	Call GetRelatedTables()
	
End If

If Not IsValidArray(arrFields) Then 
	'''Call ShowError(pageError, "There are no fields available.", False)
Else
	objSession("ArrayCurrentFields") = "-1"
	
	For iCount = 0 To UBound(arrFields)
		If objSessionStr("ArrayCurrentFields") = "-1" Then
			objSession("ArrayCurrentFields") = arrFields(iCount)
		Else
			objSession("ArrayCurrentFields") = objSessionStr("ArrayCurrentFields") & "*!^" & arrFields(iCount)
		End IF
	Next
End If

Dim rptName, rptDesc

'-- If page has been submitted
If LCase(Request.ServerVariables("REQUEST_METHOD")) = "post" Then

	Call SubmitReport()
		
End If
dim str 
Set m_objSMList = Nothing
%>

	<body class="10pt" leftmargin="0" onLoad="pageLoaded();window_onLoad();">
	<form name="frmData" method="post" topmargin="0" bottommargin="0" action="<%= Request.ServerVariables("SCRIPT_NAME") %>">
	<%If Request("ERROR") <> "" Then
		Response.Write "<center><b><font color=red>Error saving Derived Field: " & Request("ERROR") & "</font></b></center>"
	End If
	%>
	<%
	If Request("MSG") <> "" Then
		Response.Write Request("MSG")
	End If
	%>
	<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
		<tr height="4%">
			<td width="30%">
				<table border="0" class="toolbar" height="90%" cellpadding="0" cellspacing="0">
					<tr>
						<td align="center" valign="middle" height="32" width="28"><a class="LightBold" href="#" onClick="newReport();return false;"><img src="img/new.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/new2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/new.gif';this.style.zoom='100%'" title="New" /></a></td>
						<td align="center" valign="middle" height="32" width="28"><a class="LightBold" href="#" onClick= "uploadReport();return false;"> <img src="img/open.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/open2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/open.gif';this.style.zoom='100%'" title="Open Local Report"/></a></td>
						<td align="center" valign="middle" height="32" width="28"><a class="LightBold" href="#" onClick="saveReport(0,0);return false;"><img src="img/smwd_save.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/smwd_save2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/smwd_save.gif';this.style.zoom='100%'" title="Save" /></a></td>
						<td align="center" valign="middle" height="32" width="28"><a class="LightBold" href="#" onClick="saveReport(0,3);return false;"><img src="img/saveas.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/saveas2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/saveas.gif';this.style.zoom='100%'" title="Save As..." /></a></td>
						<td align="center" valign="middle" height="32" width="28"><a class="LightBold" href="#" onClick="saveReport(0,1);return false;"><img src="img/savepost.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/savepost2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/savepost.gif';this.style.zoom='100%'" title="Save and Post" /></a></td>
						<td align="center" valign="middle" height="32" width="28"><a class="LightBold" href="#" onClick="openPreview('pdf');saveReport(0,2);return false;"><img src="img/adobe_prev1.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/adobe_prev2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/adobe_prev1.gif';this.style.zoom='100%'"  title="Preview in PDF" /></a></td>
						<td align="center" valign="middle" height="32" width="28"><a class="LightBold" href="#" onClick="openPreview('xls');saveReport(0,2);return false;"><img src="img/excel_prev1.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/excel_prev2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/excel_prev1.gif';this.style.zoom='100%'"  title="Preview in XLS" /></a></td>
						<td align="center" valign="middle" height="32" width="28"><a class="LightBold" href="#" onClick="derived_click();return false;"><img src="img/derfield.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/derfield2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/derfield.gif';this.style.zoom='100%'" title="Derived Fields" /></a></td>
						<td align="center" valign="middle" height="32" width="28"><a class="LightBold" href="#" onClick="openRelDate(0);return false;"><img src="img/rel_date.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/rel_date2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/rel_date.gif';this.style.zoom='100%'" title="Relative Date" /></a></td>
					</tr>
				</table>
			</td>
			<% If objSession("REPORTNAME") <> "-1" Then %> 
				<td width="70%" class="msgheader" align="right"><% Response.Write "<b><font>" & "Report Name : " & LEFT(objSession("REPORTNAME"), 50) & "</font></b>" %>&nbsp;&nbsp;&nbsp;</td>
			<% End If %>
		</tr>
		<tr height="6%">
			<td colspan="2">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td id="tool_rptfields" width="24%" class="Selected"><a class="Selected" href="#"><span style="{text-decoration:none}">Report Fields</span></a></td>
						<td style="border-bottom:none;border-left-style:solid;border-left-color:#999999;border-width:2px;border-top-style:solid;border-top-width:2px;border-top-color:#FFFFFF;">&nbsp;&nbsp;</td>

						<td id="tool_rptcriteria" width="24%" class="NotSelected">
							<a class="NotSelected1" href="javascript:tabNavigation(1,0)"><span style="{text-decoration:none}">Report Criteria</span></a>
						</td>
						<td style="border-bottom:none;border-left-style:solid;border-left-color:#999999;border-width:2px;border-top-style:solid;border-top-width:2px;border-top-color:#FFFFFF;">&nbsp;&nbsp;</td>

						<td id="tool_rptoptions" width="24%" class="NotSelected">
							<a class="NotSelected1" href="javascript:tabNavigation(2,0)"><span style="{text-decoration:none}">Report Options</span></a>
						</td>
						<td style="border-bottom:none;border-left-style:solid;border-left-color:#999999;border-width:2px;border-top-style:solid;border-top-width:2px;border-top-color:#FFFFFF;">&nbsp;&nbsp;</td>

						<td id="tool_graphoptions" width="24%" class="NotSelected">
							<a class="NotSelected1" href="javascript:tabNavigation(3,0)"><span style="{text-decoration:none}">Graph Options</span></a>
						</td>
						<td style="border-bottom:none;border-left-style:solid;border-left-color:#999999;border-width:2px;border-top-style:solid;border-top-width:2px;border-top-color:#FFFFFF;">&nbsp;&nbsp;</td>

						<!--td id="tool_hier" width="25%" class="NotSelected">
							<a class="NotSelected1" href1="javascript:tabNavigation(4,0);"><span style="{text-decoration:none}">Column Hierarchy</span></a>
						</td>
						<td style="border-bottom:none;border-left-style:solid;border-left-color:#999999;border-width:2px;border-top-style:solid;border-top-width:2px;border-top-color:#FFFFFF;">&nbsp;&nbsp;</td-->
					</tr>
				</table>
			</td>
		</tr>
		<tr height="90%"><td colspan="2">
			<table id="reportfields" width="100%" height="100%" class="background" border="0" bgcolor="white" cellspacing1="0" cellpadding1="0">
				<tr class="background" VALIGN="top">
					<td>
						<table  border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="white" height="100%">
							<tr>
								<td width="34%">&nbsp;&nbsp;Starting Table:</td>
								<td width="10%">&nbsp;</td>
								<td width="20%" align="left">&nbsp;</td>
								<td width="36%" align="left">&nbsp;</td>
							</tr>
							<tr>
								<td width1="20%">&nbsp;
									<select name="cboTables" style="width:95%" size="1" onChange="UpdateSmFieldsList()" onChange1="changeselection()">
									<% 
									
									a = Split(objSessionStr("TableNameStack"), ",")
									
									If IsValidArray(sorted_arr) And IsValidArray(a) Then 
										For iCount = 0 to ubound(sorted_arr)%>
											<option value="<%=iCount%>" <%if sorted_arr(iCount) = a(0) then Response.Write "selected"%>><%=sorted_arr(iCount)%></option>
										<% Next
									End If
									
									%>
									</select>
								</td>
								<td width1="10%">&nbsp;</td>
								<td width1="15%"><input type="checkbox"  name="chkAsOf" <%=m_AsOf%>>As Of :</td>	
								<td width1="35%">&nbsp;<input name="txtDtofevnt" maxlength="10" size="10" onBlur1="return IsDate(this)" onBlur="dateLostFocus('txtDtofevnt')" value ='<%=m_DateFormatted %>'>&nbsp;<input type="button" class="button" value="..." name="btnDtofevnt" onClick="selectDate('txtDtofevnt')"></td>	
							</tr>
							<tr>
								<td width1="20%">&nbsp;&nbsp;Current Level:</td>
								<td width1="10%">&nbsp;</td>
								<td width1="15%">&nbsp;Report Type :</td>
								<td width1="35%" align="left">
									<input type="radio" size="5%" name="optReportType" <%=detail%> value="0">Detail
									<input type="radio" size="5%" name="optReportType" <%=summary%> value="1">Summary
									<input type="radio" size="5%" name="optReportType" <%=both %> value="2">Both
								</td>
							</tr>
							<tr>
								<td width1="20%" colspan="2">&nbsp;
									<input type="text" size="42" name="txtCurrentLevel" disabled style="background-color:lightgrey" value="<%=objSessionStr("CurrentLevel")%>">
									<!--input type="button" class="button" value="..." name="btnCurrentLevel"-->
								</td>
								<td width1="15%">&nbsp;Report Columns :</td>
								<td width1="35%">								
									<table border="0" cellspacing1="0" cellpadding1="0">
										<tr>
											<td><input class="button" disabled type="button" style1="width:25%" name="btnUp" value="   Up   " onclick="trackLevelup();btnUp_click();"></td>
											<td><input class="button" disabled type="button" style1="width:25%" name="btnDown" value="  Down  " onclick="trackLevel();btnDown_click();"></td>
											<td><input class="button" disabled1 type="button" style1="width:35%" name="btnProperties" value="Properties..." onClick="openProperty()"></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td width1="20%" colspan="4">&nbsp;&nbsp;Fields :</td>
							</tr>						
							<tr>
								<td width1="40%">
									<table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td>&nbsp;
												<select name="lstTableFields" style="width:95%" multiple size="12" ondblClick="AddFields()">
												
													<%
														If IsValidArray(sorted_arrFields) Then
															for iCount = 0 to UBound(sorted_arrFields)
															  if iCount=0 then 
																Response.Write "<option selected>" & sorted_arrFields(iCount) & "</option>"
															  else
															     response.write "<option>" & sorted_arrFields(iCount) & "</option>"
															  end if
															next
														End If
													 %>
												</select>
											</td>
										</tr>
										
										<tr>
											<td>&nbsp;&nbsp;Related Tables :</td>
										</tr>
										<tr>
											<td>&nbsp;
												<select name="lstUsers" style="width:95%" size="3" ondblClick="onDown();">
													<%  
														If IsValidArray(sorted_arrTables) Then
															for iCount = 0 to UBound(sorted_arrTables)
																  if iCount=0 then 
																	Response.Write "<option selected>" & sorted_arrTables(iCount) & "</option>"
																  else
																     response.write "<option>" & sorted_arrTables(iCount) & "</option>"
																  end if
															next
														End If
													%>
												</select>
											</td>
										</tr>
									</table>
								</td>
								
								<td width1="10%" align="center">
									<input class="button" type="button" size="8" style="width:70%"   value="Add &gt;&gt;" name="btnAdd" onclick="AddFields()"><br><br>
									<input class="button" type="button" size="8" style="width:70%" value="&lt;&lt; Del " name="btnDel" onclick="DelFields()">
								</td>
								<td width1="50%" colspan="2">
								<%
								If objSessionStr("MODE1") = "1" Then 
									If IsValidArray(m_myOrder) Then
										If UBound(m_myOrder) <= 15 Then
											nSize = 16
										Else
											nSize = UBound(m_myOrder) + 1
										End If
									Else
										nSize = 15
									End If
								Else 
									 nSize = 15
								End IF
								%>
								<div id="divSelectedField" style="position:relative;left:0;top:0;width:450px;height:262px;overflow:scroll">
									<select style="width:700px;" size="<%=nSize%>"  name="lstselfields" onClick="selFields()" ondblClick="openProperty()" onkeydown="scrollBehavior()">
									
									<%
									If objSessionStr("MODE1")= "1" And IsArray(m_myOrder) Then
										If IsValidArray(m_myOrder) Then
											For iCount = 0 To UBound(m_myOrder)
												For j = 0 To UBound(m_myArray)
													If CInt(m_myOrder(iCount)) = j Then
														Response.Write "<Option>" & m_myArray(j) & "</Option>"
												  End IF
												Next
											Next
										End If
									End If
									%>
									</select>
								</div>
								</td>
							</tr>
							<tr>
								<td width="100%" align="center" colspan="4"><font size="1">&nbsp;</font></td>
							</tr>
							<tr>
								<td align="center">
									<table>
										<tr>
											<td align="center">
												<input class="button" type="button" style1="width:25%" value="Down Level" name="btnDnlvl" onclick="onDown();">
											</td>
											<td align="center">
												<input class="button" disabled1 type="button" style1="width:25%" value="   Up Level   " name="btnUplvl" onclick="onUp();">
											</td>
										</tr>
									</table>
								</td>
								<td width1="10%">&nbsp;</td>
								<td width1="15%">&nbsp;</td>
								<td width1="35%">&nbsp;</td>
							</tr>
							<tr>
								<td width="100%" align="center" colspan="4"><font size="1">&nbsp;</font></td>
							</tr>
						</table>
					</td>
				</tr>
		</table>
			<input type="hidden" name="hdMode" value="<%=Request.QueryString("MODE")%>">
			<input type="hidden" name="hdMode1" value="<%=objSessionStr("MODE1")%>">
		
			<input type="hidden" name="hdTabID" value="">
			<input type="hidden" name="hdSave" value="<%=objSessionStr("Save")%>">
			<input type="hidden" name="hdStackTableID" value="<%=objSessionStr("TableIDStack")%>">
			<input type="hidden" name="hdStackTableName" value="<%=objSessionStr("TableNameStack")%>">
			<input type="hidden" name="hdNextField"  value="">
		
			<input type="hidden" name="hdAsOfSelected" value="">
			<input type="hidden" name="hdSubmit" value="">
			<input type="hidden" name="hdTabPressed"  value="0">
			<input type="hidden" name="hdSelFields" value="<%=objSessionStr("ArrayP")%>">			
			<input type="hidden" name="hdArrayPFields" value="<%=objSessionStr("ArrayPFields")%>">			
			<input type="hidden" name="hdSaveFinal" value="<%=objSessionStr("SaveFinal")%>">
			<input type="hidden" name="hdRptName" value="" >
			<input type="hidden" name="hdRptDesc" value="" >
			<input type="hidden" name="hdSelFieldsLength" value="<%=objSessionStr("ColLength")%>" >
			<input type="hidden" name="hdArrayTF" value="<%=objSessionStr("ArrayTF")%>" >
			<input type="hidden" name="hdSelFieldsXML" value="<%=fixDoubleQuote(objSessionStr("ArrayPXML"))%>">
			<input type="hidden" name="hdArrayCurrentFields" value="<%=fixDoubleQuote(objSessionStr("ArrayCurrentFields"))%>">
			<input type="hidden" name="hdDataType" value="<%=objSessionStr("ArrayFieldType")%>">
			<input type="hidden" name="hdColProp" value="<%=objSessionStr("ArrayColProp")%>">
			<input type="hidden" name="hdPropDataType" value="">
			<input type="hidden" name="hdDate" value="">
			<input type="hidden" name="hdMaxFieldNumber" value="<%'='objSessionStr("MaxFieldNumber")%>">
			<input type="hidden" name="hdOpenProp" value="<%=objSession("OPENPROPERTY")%>">
		
			<!------------------  Hidden Variables used for Property Box---------------------------------->
			<input type="hidden" name="hdIsStackSelected" value="">
			<input type="hidden" name="hdIsIncludeSelected" value="">
			<input type="hidden" name="hdIsPrintSelected" value="">
			<input type="hidden" name="hdIsRollSelected" value="">
			<input type="hidden" name="hdColBreak" value="">
			<input type="hidden" name="hdBannerStack" value="">
			<input type="hidden" name="hdIsAggNoneSelected" value="">
			<input type="hidden" name="hdIsAggCountSelected" value="">
			<input type="hidden" name="hdIsAggUniqueSelected" value="">
			<input type="hidden" name="hdIsAggSumSelected" value="">
			<input type="hidden" name="hdIsAggAvgSelected" value="">
			<input type="hidden" name="hdIsAggMinSelected" value="">
			<input type="hidden" name="hdIsAggMaxSelected" value="">
			<input type="hidden" name="hdColumnSort" value="">
			<input type="hidden" name="hdIsSuppressSelected" value="">
			<input type="hidden" name="hdIsDefaultSelected" value="">
			<input type="hidden" name="hdMaxLines" value="">
			<input type="hidden" name="hdIsFiscSelected" value="">
			<input type="hidden" name="hdIsTopSelected" value="">
			<input type="hidden" name="hdOrganization" value="">
			<input type="hidden" name="hdLineOfBusiness" value="">
			<input type="hidden" name="hdDefaultLines" value="">
				
			<input type="hidden" name="hdSelectedIndex" value="">
		
			<input type="hidden" name="hdAggCount" value="">
			<input type="hidden" name="hdAggUniqueCount" value="">
			<input type="hidden" name="hdAggSum" value="">
			<input type="hidden" name="hdAggAvg" value="">
			<input type="hidden" name="hdAggMin" value="">
			<input type="hidden" name="hdAggMax" value="">
		
			<!--input type="hidden" name="hdSelOrg" value=""-->

			<input type="hidden" name="hdDerFieldType" value="">
			<input type="hidden" name="hdDerFieldName" value="">
			<input type="hidden" name="hdDerFieldWidth" value="">
			<input type="hidden" name="hdDerDetailFormula" value="">
			<input type="hidden" name="hdDerIndex" value=""> 
			<input type="hidden" name="hdDerAddEdit" value=""> 
			<input type="hidden" name="hdDerAggregate" value=""> 
			<input type="hidden" name="hdDerAggregateFormula" value="">
			<input type="hidden" name="hdDerColumnInvolved" value="<%=fixDoubleQuote(objSessionStr("ColumnsInvolved"))%>"> 
			<input type="hidden" name="hdDerError" value='<%=sError%>'> 
			<input type="hidden" name="hdDerGScript" value="<%=sGlobalScript%>">
		
			<input type="hidden" name="hdRelDate" value=""> 
			<input type="hidden" name="hdRelDateFormula" value=""> 
			<input type="hidden" name="hdRelativeDate" value=""> 
			<input type="hidden" name="hdRelMode" value=""> 
			<input type="hidden" name="hdRelID" value=""> 
		
			<input type="hidden" name="hdPost" value="0">
			<input type="hidden" name="hdOutputType" value="0">
			<input type="hidden" name="hdPreview" value="0">
			<input type="hidden" name="hdUpload" value="<%=Request.QueryString("UPLOAD")%>">
			<input type="hidden" name="hdBValue" value="<%=Request.QueryString("b")%>">
			<input type="hidden" name="hdCValue" value="<%=Request.QueryString("c")%>">
		</form>
	</body>
</html>
<%

Function GenerateXMLReport()

	'-- Recreate the base XML template
	Dim objXML 
	Set objXML = Server.CreateObject("Msxml2.DOMDocument")
	objXML.async = false
	On Error Resume Next
	'--Load the XML template

	objXML.loadXML "<report reporttype=""multiquery"" reportdetailtype=""detailsummary"" orientation=""portrait"" papersize=""9"" selecttype=""all"">" _
				& "<reporttitle justify=""left"" color=""0"" fontname=""Courier New"" fontsize=""14.000000"" bold=""1"" italic=""0"" underline=""0"" strikeout=""0"">" _
				& "<subtitle></subtitle>" _
				& "</reporttitle>" _
				& "<headerleft></headerleft>" _
				& "<headercenter>" & Server.HTMLEncode("&") & "r</headercenter>"_
				& "<headerright>" & Server.HTMLEncode("&") & "t</headerright>"_
				& "<footerleft></footerleft>"_
				& "<footercenter>Page " & Server.HTMLEncode("&") & "p of " & Server.HTMLEncode("&") & "P</footercenter>"_
				& "<footerright></footerright>"_
				& "<reportfont fontname=""Courier New"" fontsize=""10.000000"" bold=""0"" italic=""0"" underline=""0"" strikeout=""0""/>"_
				& "<htmlsettings htmlnumlinesperpage=""0"" htmlpageformat=""single"" htmlpagebreak=""0"" htmlexportgraph=""0"" htmlgraphsamefile=""1"" htmlgraphtopofpage=""1"">"_
				& "<htmldefpath></htmldefpath>"_
				& "</htmlsettings>"_
				& "<columns>"_
				& "<columnorder>"_
				& "</columnorder>"_
				& "<columnlist>"_
				& "</columnlist>"_
				& "</columns>"_
				& "<criterialist/>"_
				& "<havinglist/>"_
				& "</report>"
	
	Dim iCount, myOrder2, myArray2XML, nSelectedIndex, j, colValXML, colOrder, z, xmlProp, k
	
	Set z = objXML.getElementsByTagName("report").item(0)
	
	If Request.Form("hdAsOfSelected") = "1"  Then
		If Trim(Request.Form("hdDate")) <> "" Then z.setAttribute "evaldate",Request.Form("hdDate")
	Else
		If Not z.getAttributeNode("evaldate") Is Nothing Then z.removeAttribute("evaldate")
	End if
		
	If Request.Form("optReportType") = "0" Then
		z.setAttribute "reportdetailtype", "detail"
		detail = "checked"
	ElseIf Request.Form("optReportType") = "1" Then
		z.setAttribute "reportdetailtype", "summary"
		summary = "checked"
	ElseIf Request.Form("optReportType") = "2" Then
		z.setAttribute "reportdetailtype", "detailsummary"
		both = "checked"
	End If
		
	'-- Create columnlist and columnorder tags
	'-- columnorder tag is used to maintain the order of report column display
	
	For iCount = 0 To CInt(objSessionStr("ColLength")) - 1
		Set y = objXML.getElementsByTagName("columnlist").item(0).appendChild(objXML.createElement("column"))
		Set y = objXML.getElementsByTagName("columnorder").item(0).appendChild(objXML.createElement("column"))
	Next

	'-- Retrieve the JavaScripts arrays
	'-- hdSelFields corresponds to  p array to maintain the column name display
	'-- hdSelFieldsXML corresponds to pXML array to maintain the columnlist tag in XML
	'-- hdArrayTF corresponds to tf array to maintain  the column order
		
	colValXML = Request.Form("hdSelFieldsXML")
	colOrder = Request.Form("hdArrayTF")

	myArray2XML = Split(colValXML, "</column>,")
	If IsValidArray(myArray2XML) Then
		For iCounter = 0 To UBound(myArray2XML) - 1
			myArray2XML(iCounter) = myArray2XML(iCounter) & "</column>"
		Next
	
		myOrder2 = Split(colOrder, ",")

		'-- If page is submitted from properties.asp
		If Request.Form("hdSubmit") = "PROPERTIES" Then
			nSelectedIndex = CInt(Request.Form("hdSelectedIndex"))
		End If
	
		'-- Initialize pXML array	
		objSession("ArrayPXML") = "-1"

		'-- Set attributes to column tag of columnlist tag
		For j = 0  To CInt(objSessionStr("ColLength")) - 1
			
			Set z = objXML.getElementsByTagName("columnlist").item(0).childNodes(j)
					
			'-- Extend XML template from pXML array
			Set xmlProp = Server.CreateObject("Msxml2.DOMDocument")
			xmlProp.async = false

			xmlProp.loadXML myArray2XML(j)
			
			'-- Set attributes
			For iCount = 0 To xmlProp.getElementsByTagName("column").item(0).attributes.length - 1
				If iCount = 5 And xmlProp.getElementsByTagName("column").item(0).attributes.item(iCount).text = "" Then
					Dim FieldString,DefFieldWidth
					FieldString = xmlProp.getElementsByTagName("column").item(0).attributes.item(1).text & "," & xmlProp.getElementsByTagName("column").item(0).attributes.item(2).text
					If Not m_objSMList.GetDefaultFieldWidth(FieldString,DefFieldWidth) Then
						Call ShowError(pageError, "Couldn't fetch the default column width.", False)
					End If
					z.setAttribute xmlProp.getElementsByTagName("column").item(0).attributes.item(iCount).name, DefFieldWidth
				Else
					z.setAttribute xmlProp.getElementsByTagName("column").item(0).attributes.item(iCount).name, xmlProp.getElementsByTagName("column").item(0).attributes.item(iCount).text
				End If
			Next

			If Request.Form("hdSubmit") = "PROPERTIES" Then
				
			'^Rajeev 05/20/2003 -- 
					
				If (Cint(nSelectedIndex) = j) Then

					'-- No Print
					If Request.Form("hdIsIncludeSelected") = "1" Then 
						z.setAttribute "noprint","1"
					Else
						If Not z.getAttributeNode("noprint") Is Nothing Then z.removeAttribute("noprint")
					End If
					'-- Sort
					If Request.Form("hdColumnSort") = "1" Then 
						z.setAttribute "sort","asc"
					Elseif Request.Form("hdColumnSort") = "2" Then
						z.setAttribute "sort","desc"
					Else
						If Not z.getAttributeNode("sort") Is Nothing Then z.removeAttribute("sort")			
					End If
					'-- Break
					
					If Request.Form("hdColBreak") = "1" Then 
						z.setAttribute "break","break"
						If Not z.getAttributeNode("page") Is Nothing Then z.removeAttribute("page")
						If Not z.getAttributeNode("banner") Is Nothing Then z.removeAttribute("banner")
					Elseif Request.Form("hdColBreak") = "2" Then
						'Manish C Jha 08/10/2009 - MITS 15151
						'Reason for change: Break Mapping was incorrect
						z.setAttribute "break","page"
						If Not z.getAttributeNode("page") Is Nothing Then z.removeAttribute("page")
						If Not z.getAttributeNode("banner") Is Nothing Then z.removeAttribute("banner")
					Elseif Request.Form("hdColBreak") = "3" Then
						z.setAttribute "banner","banner"
						If Not z.getAttributeNode("page") Is Nothing Then z.removeAttribute("page")
						If Not z.getAttributeNode("break") Is Nothing Then z.removeAttribute("break")
					Else
						If Not z.getAttributeNode("page") Is Nothing Then z.removeAttribute("page")
						If Not z.getAttributeNode("break") Is Nothing Then z.removeAttribute("break")
						If Not z.getAttributeNode("banner") Is Nothing Then z.removeAttribute("banner")
					End If
					'-- Stack
					If Request.Form("hdIsStackSelected") = "1" Then
						z.setAttribute "stack","stack"
					Else
						If Not z.getAttributeNode("stack") Is Nothing Then z.removeAttribute("stack")
					End if
						
					If Request.Form("hdBannerStack") = "1" Then
						z.setAttribute "bannerstack","bannerstack"
					Else
						If Not z.getAttributeNode("bannerstack") Is Nothing Then z.removeAttribute("bannerstack")
					End If
					'-- Creating subtotal attribute along with dependent attributes-----applicable for all except DataType 8.
					'-- Sub Total
					If Request.Form("hdIsPrintSelected") = "1" Then
						z.setAttribute "subtotal","1"
					Else
						If Not z.getAttributeNode("subtotal") Is Nothing Then z.removeAttribute("subtotal")
					End If
					'-- Duplicate Columns
					If Request.Form("hdIsSuppressSelected") = "1" Then 
						z.setAttribute "duplicatecolumns","1"
					Else
						If Not z.getAttributeNode("duplicatecolumns") Is Nothing Then z.removeAttribute("duplicatecolumns")
					End If
					'-- Roll Up
					If Request.Form("hdIsRollSelected") = "1" Then 
						z.setAttribute "rollup","1"
					Else
						If Not z.getAttributeNode("rollup") Is Nothing Then z.removeAttribute("rollup")
					End If
					'-- Fiscal Year
					If Request.Form("hdIsFiscSelected") = "1" Then 
						z.setAttribute "fiscalyear","1"
						z.setAttribute "organization",Request.Form("hdOrganization")
						z.setAttribute "lineofbusiness",Request.Form("hdLineOfBusiness")
					Else
						If Not z.getAttributeNode("fiscalyear") Is Nothing Then z.removeAttribute("fiscalyear")
						If Not z.getAttributeNode("organization") Is Nothing Then z.removeAttribute("organization")
						If Not z.getAttributeNode("lineofbusiness") Is Nothing Then z.removeAttribute("lineofbusiness")
					End If
					'-- Max lines
					If Request.Form("hdDefaultLines") = "1"  Then 
						z.setAttribute "maxlinesinrow",Request.Form("hdMaxLines")
					Else
						If Not z.getAttributeNode("maxlinesinrow") Is Nothing Then z.removeAttribute("maxlinesinrow")			
					End If				
					
					If Request.Form("hdIsAggUniqueSelected") = "1" Then
						z.setAttribute "agguniquecount","1"
						If Not z.getAttributeNode("aggcount") Is Nothing Then z.removeAttribute("aggcount")
					ElseIf Request.Form("hdIsAggCountSelected") = "1" Then 
						z.setAttribute "aggcount","1"
						If Not z.getAttributeNode("agguniquecount") Is Nothing Then z.removeAttribute("agguniquecount")
					Else
						If Not z.getAttributeNode("agguniquecount") Is Nothing Then z.removeAttribute("agguniquecount")
						If Not z.getAttributeNode("aggcount") Is Nothing Then z.removeAttribute("aggcount")
					End If
					
					If Request.Form("hdIsAggSumSelected") = "1" Then 
						z.setAttribute "aggsum","1"
					Else
						If Not z.getAttributeNode("aggsum") Is Nothing Then z.removeAttribute("aggsum")
					End If				

					If Request.Form("hdIsAggAvgSelected") = "1" Then 
						z.setAttribute "aggaverage","1"
					Else
						If Not z.getAttributeNode("aggaverage") Is Nothing Then z.removeAttribute("aggaverage")
					End If
					
					If Request.Form("hdIsAggMinSelected") = "1" Then 
						z.setAttribute "aggmin","1"
					Else
						If Not z.getAttributeNode("aggmin") Is Nothing Then z.removeAttribute("aggmin")
					End If							
					
					If Request.Form("hdIsAggMaxSelected") = "1" Then 
						z.setAttribute "aggmax","1"
					Else
						If Not z.getAttributeNode("aggmax") Is Nothing Then z.removeAttribute("aggmax")
					End If				

				End If
				'^^Rajeev 05/20/2003 -- 
			End If

			'-- Prepare Aggregate labels
				If Request.Form("hdSubmit") = "PROPERTIES" Then
					If Cint(nSelectedIndex) = j Then
						If Request.Form("hdIsAggCountSelected") = "1" _
							Or Request.Form("hdIsAggUniqueSelected") = "1" _
							Or Request.Form("hdIsAggSumSelected") = "1" _
							Or Request.Form("hdIsAggAvgSelected") = "1" _
							Or Request.Form("hdIsAggMinSelected") = "1" _
							Or Request.Form("hdIsAggMaxSelected") = "1" Then
								
							Set objAggregate = z.appendChild(objXML.createElement("aggregatelabels"))
							For iCount = 0 To 6
								Set objLabel = objAggregate.appendChild(objXML.createElement("aggregatelabel"))
							Next
									
							If Request.Form("hdAggUniqueCount") = "1" Then z.getElementsByTagName("aggregatelabels").item(0).childNodes.item(0).appendChild(objXML.createTextNode("Records:"))
							If Request.Form("hdAggCount") = "1" Then z.getElementsByTagName("aggregatelabels").item(0).childNodes.item(1).appendChild(objXML.createTextNode("Records:"))
							If Request.Form("hdAggAvg") = "1" Then z.getElementsByTagName("aggregatelabels").item(0).childNodes.item(3).appendChild(objXML.createTextNode("Avg:"))
							If Request.Form("hdAggMin") = "1" Then z.getElementsByTagName("aggregatelabels").item(0).childNodes.item(4).appendChild(objXML.createTextNode("Min:"))
							If Request.Form("hdAggMax") = "1" Then z.getElementsByTagName("aggregatelabels").item(0).childNodes.item(5).appendChild(objXML.createTextNode("Max:"))
						End If
					End If
				Else
					If Not z.getAttributeNode("aggcount") Is Nothing _
						Or Not z.getAttributeNode("agguniquecount") Is Nothing _
						Or Not z.getAttributeNode("aggsum") Is Nothing _
						Or Not z.getAttributeNode("aggaverage") Is Nothing _
						Or Not z.getAttributeNode("aggmin") Is Nothing _
						Or Not z.getAttributeNode("aggmax")Is Nothing Then
									
						Set objAggregate = z.appendChild(objXML.createElement("aggregatelabels"))
						
						For iCount = 0 To 6
							Set objLabel = objAggregate.appendChild(objXML.createElement("aggregatelabel"))						
							'''z.getElementsByTagName("aggregatelabels").item(j).childNodes.item(iCount).appendChild(objXML.createTextNode( xmlProp.getElementsByTagName("aggregatelabels").item(j).childNodes.item(iCount).text))
							If Not xmlProp.getElementsByTagName("aggregatelabels").item(0) Is Nothing Then
								objLabel.appendChild(objXML.createTextNode( xmlProp.getElementsByTagName("aggregatelabels").item(0).childNodes.item(iCount).text))
							End If

						Next
					End If		
				End If
				
			'-- Set parenttables tag
			Set x = xmlProp.getElementsByTagName("parenttables").item(0) ''.appendChild(objXML.createElement("column"))
			If Not x Is Nothing Then z.appendChild(x)
				
			Set xmlProp = Nothing

			'-- Set attributes in XML template from properties.asp

			'-- Update pXML array with new added attributes
			If objSessionStr("ArrayPXML") = "-1" Then
				objSession("ArrayPXML") = Trim(objXML.getElementsByTagName("columnlist").item(0).childNodes.item(j).xml)
			Else
				objSession("ArrayPXML") = Trim(objSessionStr("ArrayPXML")) & "," & Trim(objXML.getElementsByTagName("columnlist").item(0).childNodes.item(j).xml)
			End If

		Next
	End If		

	'-- Maintain the tf array in XML template				
	For k=0 to CInt(objSessionStr("ColLength")) - 1
		objXML.getElementsByTagName("columnorder").item(0).childNodes.item(k).text=myOrder2(k)
	Next

	If Request.Form("hdSubmit") = "RELATIVEDATE" And Request.Form("hdRelMode") = "ADD" Then
		
		If Request.Form("hdRelDate") <> "" And Request.Form("hdRelDateFormula") <> "" Then
			Dim objData
			If Not GetValidObject("InitSMList.CDataManager", objData) Then
				Call ShowError(pageError, "Call to CDataManager class failed.", True)
			End If
			objData.m_RMConnectStr = Application("SM_DSN")
		
			objData.AddDateFormula Request.Form("hdRelDate"), Request.Form("hdRelDateFormula")
			Set objData = Nothing
		End If
		
	ElseIf Request.Form("hdSubmit") = "RELATIVEDATE" And Request.Form("hdRelMode") = "UPDATE" Then
		If Request.Form("hdRelDate") <> "" And Request.Form("hdRelDateFormula") <> "" Then
			If Not GetValidObject("InitSMList.CDataManager", objData) Then
				Call ShowError(pageError, "Call to CDataManager class failed.", True)
			End If
			objData.m_RMConnectStr = Application("SM_DSN")
		
			'--Start MITS 6708
			'--Ankur Saxena 06/15/2006
			'--First parameter was wrong
			objData.UpdateDateFormula CInt(Request.Form("hdRelID")),Request.Form("hdRelDateFormula")
			Set objData = Nothing
			'--End MITS 6708
		End If
	
	End If
	
	'-- Update global sXML variable with the latest XML template
	GenerateXMLReport = objXML.XML
	
	Set objXML = Nothing
	
End Function

Function GetRelatedTables()

	Dim arrStack
	arrStack = Split(objSessionStr("TableIDStack"), ",")

	If IsValidArray(arrTables) And IsValidArray(arrStack) Then
		'-- Extract only tables that are not on stack yet
		iIndex = 0
		For iCount = 0 To UBound(arrTables)
			bFound = False
			temp = split(arrTables(iCount), ",")
			For j = 0 To UBound(arrStack)
				'''If ( (CLng(arrStack(j)) = CLng(&H80000000 + temp(1))) OR (CLng(arrStack(j)+20000) = CLng(&H80000000 + temp(1)))) Then
				If m_objSMList.m_SuppTableName <> "" Then
					'If ( (CLng(arrStack(j)) = CLng(&H80000000 + temp(1))) OR (CLng(arrStack(j)+20000) = CLng(&H80000000 + temp(1)))) Then
					'	bFound = True
					'	Exit For
					'End If
				Else
					If CLng(arrStack(j)) = CLng(&H80000000 + temp(1))Then
						bFound = True
						Exit For
					End If
				End If
		    Next
		    If Not bFound Then
				Redim Preserve sorted_arrTables(iIndex)
				sorted_arrTables(iIndex) = temp(0)
				iIndex = iIndex + 1
			End If
		Next
					
		If iIndex > 0 Then m_objSMList.Sort sorted_arrTables
	End If
	
End Function

Function GetRelatedFields()

	'-- arrFields contains field name and id seperated by |
	'-- Extract only field names from arrFields in sorted_arrFields
	
	If IsValidArray(arrFields) Then
		'-- Sort the array
		m_objSMList.Sort arrFields
				
		'-- Extract field names
		For iCount = 0 To UBound(arrFields)
		    ReDim Preserve sorted_arrFields(iCount)
		    temp = Split(arrFields(iCount), "|")
		    sorted_arrFields(iCount) = temp(0)
		Next
	End If
		
End Function

Function MaintainColumnsAndSessions(ByRef pobjXML)

	MaintainColumnsAndSessions = False
	On Error Resume Next
	Dim objTemp
	'-- Retrieving the date
	Set objTemp = pobjXML.getElementsByTagName("report").item(0).getAttributeNode("evaldate")
	If Not objTemp Is Nothing Then
		m_DateRetrieved = objTemp.value		
		'mjha3 : 12/23/2008 : MITS 13913 : As of date (m_DateFormatted) changed into mm/dd/yyyy format on page.
		'm_DateFormatted = Right(m_DateRetrieved,2)& "/" & Mid(m_DateRetrieved,5,2) & "/" & Left(m_DateRetrieved,4)
		m_DateFormatted = Mid(m_DateRetrieved,5,2)& "/" & Right(m_DateRetrieved,2) & "/" & Left(m_DateRetrieved,4)
		m_AsOf = "checked"
	End If
	Set objTemp = Nothing

	'-- Retrieving the report detail type	
	Set objTemp = pobjXML.getElementsByTagName("report").item(0).getAttributeNode("reportdetailtype")
	If Not objTemp Is Nothing Then
		If objTemp.value = "detail" Then
			detail = "checked"
		ElseIf objTemp.value = "summary" Then
			summary = "checked"
		ElseIf objTemp.value = "detailsummary" Then
			both = "checked"
		Else
			both = "checked"
		End If
	End If
	Set objTemp = Nothing
	
	If Not pobjXML.getElementsByTagName("report").item(0).getAttributeNode("globalscript") Is Nothing Then
		sGlobalScript = pobjXML.getElementsByTagName("report").item(0).getAttributeNode("globalscript").value
	End If
	
	'-- Parse XML for populating Report Columns on GUI
	Dim n
	n = pobjXML.getElementsByTagName("columnlist").item(0).childNodes.length

	objSession("ColLength") = n

	objSession("ArrayPFields") = "-1"
	objSession("ArrayFieldType") = "-1"
	objSession("ArrayColProp") = "-1"	

	'-- For Relative Date
	Set k = pobjXML.getElementsByTagName("datelist").item(0)
	
	
	If Not k Is Nothing Then
		l = k.childNodes.length
	End If
	
	Dim iSelected, iCount
	iSelected = 1
	
	Dim matchString
	
	For iCount = 0 To n - 1

		'-- For preserve arrray--start
		'-- Outer loop for the newly selected fields(for arranging numberwise e.g. F1,F2)
		Redim Preserve arrTemp(iCount)
		Redim Preserve strProp(iCount)

		Dim sTableName, sFieldName, nFieldType
		
		'-- if derived field
		If InStr(1, pobjXML.getElementsByTagName("columnlist").item(0).childNodes.item(iCount).attributes.item(1).text, "-") > 0 Then
			sTableName = "Derived"
		Else
			'-- Retrieve table name for given table id from the SM Table collection
			'''If Not m_objSMList.GetTableNameFromSMTableList( CLng(pobjXML.getElementsByTagName("columnlist").item(0).childNodes.item(iCount).attributes.item(1).text), sTableName) Then
			If Not m_objSMList.GetSMTableName( CLng(pobjXML.getElementsByTagName("columnlist").item(0).childNodes.item(iCount).attributes.item(1).text), sTableName) Then
				MaintainColumnsAndSessions = False
				Exit Function
			End If
		End If


		'-- if derived field
		If InStr(1, pobjXML.getElementsByTagName("columnlist").item(0).childNodes.item(iCount).attributes.item(1).text, "-") > 0 Then
			sFieldName = pobjXML.getElementsByTagName("columnlist").item(0).childNodes.item(iCount).childNodes.item(0).attributes.item(1).text
		Else
			'-- Retrieve field name for given table id & field id from the SM Table collection
			If Not m_objSMList.GetSMFieldName(  CInt(pobjXML.getElementsByTagName("columnlist").item(0).childNodes.item(iCount).attributes.item(1).text), CInt(pobjXML.getElementsByTagName("columnlist").item(0).childNodes.item(iCount).attributes.item(2).text), sFieldName) Then
				MaintainColumnsAndSessions = False
				Exit Function
			End If
		End If
		
		'-- Format as TableName : FieldName

		matchString =  Trim( sTableName) & " : " & Trim( sFieldName)
		
		arrTemp(iCount) =  Trim( sTableName) & " : " & Trim( sFieldName)
		
		'-- if derived field
		If InStr(1, pobjXML.getElementsByTagName("columnlist").item(0).childNodes.item(iCount).attributes.item(1).text, "-") Then
			If pobjXML.getElementsByTagName("columnlist").item(0).childNodes.item(iCount).childNodes.item(0).attributes(0).text = "text" _
				Or pobjXML.getElementsByTagName("columnlist").item(0).childNodes.item(iCount).childNodes.item(0).attributes(0).text = "time" _
				Or pobjXML.getElementsByTagName("columnlist").item(0).childNodes.item(iCount).childNodes.item(0).attributes(0).text = "datetime" Then
				nFieldType = 0
			ElseIf pobjXML.getElementsByTagName("columnlist").item(0).childNodes.item(iCount).childNodes.item(0).attributes(0).text = "formattednumeric" _
				Or pobjXML.getElementsByTagName("columnlist").item(0).childNodes.item(iCount).childNodes.item(0).attributes(0).text = "numeric" _
				Or pobjXML.getElementsByTagName("columnlist").item(0).childNodes.item(iCount).childNodes.item(0).attributes(0).text = "currency" Then
				nFieldType = 1
			ElseIf pobjXML.getElementsByTagName("columnlist").item(0).childNodes.item(iCount).childNodes.item(0).attributes(0).text = "date" Then
				nFieldType = 2
			End If
		Else
			'-- Retrieve field type for given table id & field id from the SM Table collection
			nFieldType = m_objSMList.GetFieldType( CInt(pobjXML.getElementsByTagName("columnlist").item(0).childNodes.item(iCount).attributes.item(1).text), CInt(pobjXML.getElementsByTagName("columnlist").item(0).childNodes.item(iCount).attributes.item(2).text))
		End If

		'-- If couldn't retrieve correct field type
		If nFieldType = -1 Then
			MaintainColumnsAndSessions = False
			Exit Function
		End If

		'-- Maintain session for field type				
		If objSessionStr("ArrayFieldType") = "-1" Then
			objSession("ArrayFieldType") = nFieldType
		Else
			objSession("ArrayFieldType") = objSessionStr("ArrayFieldType") & "," & nFieldType
		End IF
		
		'-- Properties Display		
		Set objTemp = pobjXML.getElementsByTagName("columnlist").item(0).childNodes.item(iCount)
		If Not objTemp.getAttributeNode("subtotal") Is Nothing Then
			strProp(iCount) = "Sub" 
		End if

		'Manish C Jha 08/10/2009 - MITS 15151
		'Reason for Change: "Break" node value will be either "break" or "page". Assigning correct value.
		If Not objTemp.getAttributeNode("break") Is Nothing Then
			If objTemp.getAttributeNode("break").value="break" Then
			strProp(iCount) = strProp(iCount) & "Break" 
			Else
				strProp(iCount) = strProp(iCount) & "Page"
			End if
		End if
		
		If Not objTemp.getAttributeNode("page") Is Nothing Then
			strProp(iCount) = strProp(iCount) & "Page" 
		End if
		
		If Not objTemp.getAttributeNode("banner") Is Nothing Then
			strProp(iCount) = strProp(iCount) & "Banner" 
		End if
		
		If Not objTemp.getAttributeNode("bannerstack") Is Nothing Then
			strProp(iCount) = strProp(iCount) & "Banner Stacked"  
		End if
		
		If Not objTemp.getAttributeNode("stack") Is Nothing Then
			strProp(iCount) = strProp(iCount) & "Stacked" 
		End if
		
		If Not objTemp.getAttributeNode("aggcount") Is Nothing Then
			strProp(iCount) = strProp(iCount) & "Count" 
		End if
		
		If Not objTemp.getAttributeNode("agguniquecount") Is Nothing Then
			strProp(iCount) = strProp(iCount) & "Count*" 
		End if
		
		If Not objTemp.getAttributeNode("aggsum") Is Nothing Then
			strProp(iCount) = strProp(iCount) & "Sum" 
		End if
		
		If Not objTemp.getAttributeNode("aggaverage") Is Nothing Then
			strProp(iCount) = strProp(iCount) & "Avg" 
		End if
		
		If Not objTemp.getAttributeNode("aggmin") Is Nothing Then
			strProp(iCount) = strProp(iCount) & "Min" 
		End if
		
		If Not objTemp.getAttributeNode("aggmax") Is Nothing Then
			strProp(iCount) = strProp(iCount) & "Max" 
		End if
		
		If Not objTemp.getAttributeNode("sort") Is Nothing Then
			If objTemp.getAttribute("sort") = "asc" Then
				strProp(iCount) = strProp(iCount) & "Asc"
			Else
				strProp(iCount) = strProp(iCount) & "Desc"
			End If 
		End if
		
		If Not objTemp.getAttributeNode("noprint") Is Nothing Then
			strProp(iCount) = strProp(iCount) & "Excl" 
		End if
		
		If Not objTemp.getAttributeNode("duplicatecolumns") Is Nothing Then
			strProp(iCount) = strProp(iCount) & "No Dup" 
		End if
		
		If Not objTemp.getAttributeNode("fiscalyear") Is Nothing Then
			strProp(iCount) = strProp(iCount) & "Fisc" 
		End if
		
		If Not objTemp.getAttributeNode("rollup") Is Nothing Then
			strProp(iCount) = strProp(iCount) & "Roll Up" 
		End if

		'-- Maintain session for column properties
		If objSessionStr("ArrayColProp") = "-1" Then
			objSession("ArrayColProp") = strProp(iCount)
		Else
			objSession("ArrayColProp") = objSessionStr("ArrayColProp") & "," & strProp(iCount)
		End If

		'-- Maintain session for unique column names as C i.e. discarding numbering e.g. C1, C2		
		If objSessionStr("ArrayPFields") = "-1" Then
			objSession("ArrayPFields") = arrTemp(iCount)
		Else
			Dim myPFields, bFound, iCounter
'-- Rajeev -- 03/09/2004
'''			myPFields = Split(objSessionStr("ArrayPFields"), ",")
			myPFields = Split(objSessionStr("ArrayPFields"), "*!^")
			bFound = False
			If IsValidArray(myPFields) Then
				For iCounter = 0 To UBound(myPFields)
					If arrTemp(iCount) = myPFields(iCounter) Then
						bFound = True
						Exit For
					End If
				Next
			End If
			
'-- Rajeev -- 03/09/2004
'''			If Not bFound Then objSession("ArrayPFields") = objSessionStr("ArrayPFields") & "," & arrTemp(iCount) 'Trim(sTableName) & " : " & Trim(sFieldName) 'Trim(sFieldName)'xmlDoc.getElementsByTagName("columnlist").item(0).childNodes.item(iCount).attributes.item(3).text
			If Not bFound Then objSession("ArrayPFields") = objSessionStr("ArrayPFields") & "*!^" & arrTemp(iCount)
		End If				
		
		'-- Number the columns with same name e.g. C1, C2
		'-- Excludes newly added fields that already existed

		Dim j, iNum, iNum1
		
		For j = 0 To UBound(arrTemp) - iSelected
			 '-- First time renaming
			If Trim(matchString) = Trim(arrTemp(j)) Then
				iNum = 1
				arrTemp(j) = Trim(matchString) & iNum
				iNum = iNum + 1
				arrTemp(iCount)=Trim(matchString) & iNum
				Exit For
			'-- Finding last index of matching substrings 
			ElseIf Trim(matchString) = Left(arrTemp(j), Len(matchString)) And (Right(Left(arrTemp(j),Len(matchString)+1),1) <> " ") Then
			'''ElseIf Trim(matchString) = Left(arrTemp(j), Len(matchString)) And (Right(Left(arrTemp(j),Len(matchString)+1),1) <> " ") Then
				iNum1 = 1
				Dim k
				For k = 0 To UBound(arrTemp)-iSelected
					If (Trim(matchString) = Left(arrTemp(k), Len(matchString))) And (Right(Left(arrTemp(j),Len(matchString)+1),1) <> " ") Then iNum1 = iNum1 + 1
				Next
				arrTemp(iCount) = Trim(matchString) & iNum1
				Exit For
				
			'-- New unmatched field.
			Else
				arrTemp(UBound(arrTemp)) = Trim(matchString)
			End IF
		Next
	
	Next '-- for preserve array--end


	objSession("ArrayP") = "-1"
	objSession("ArrayTF") = "-1"
	objSession("ArrayPXML") = "-1"

	If n > 0 Then
		
		Redim m_myArray(CInt(objSessionStr("ColLength"))-1)
		Redim m_myOrder(CInt(objSessionStr("ColLength"))-1)

		For iCount = 0 To CInt(objSessionStr("ColLength")) - 1
			m_myArray(iCount) = arrTemp(iCount)
			m_myOrder(iCount) = pobjXML.getElementsByTagName("columnorder").item(0).childNodes.item(iCount).text

			If objSessionStr("ArrayP") = "-1" Then
				objSession("ArrayP") = m_myArray(iCount)
				m_myArray(iCount) = m_myArray(iCount) & " " & strProp(iCount) '-- Update m_myArray(iCount)
				objSession("ArrayTF") = m_myOrder(iCount)
				objSession("ArrayPXML") = Trim(pobjXML.getElementsByTagName("columnlist").item(0).childNodes.item(iCount).xml)
			Else
'-- Rajeev -- 03/09/2004			
'''				objSession("ArrayP") = objSessionStr("ArrayP") & "," & m_myArray(iCount)
				objSession("ArrayP") = objSessionStr("ArrayP") & "*!^" & m_myArray(iCount)
				m_myArray(iCount) = m_myArray(iCount) & " " & strProp(iCount) '-- Update m_myArray(iCount)
				objSession("ArrayTF") = objSessionStr("ArrayTF") & "," & m_myOrder(iCount)
				objSession("ArrayPXML") = Trim(objSessionStr("ArrayPXML")) & "," & Trim(pobjXML.getElementsByTagName("columnlist").item(0).childNodes.item(iCount).xml)
			End IF
		Next
	End If

	objSession("ColumnsInvolved") = "-1" 
	'-- Maintaining the values for columns involved
	Dim sColumnInvolved, arrXML
	sColumnInvolved = ""
	
	If objSessionStr("ArrayPXML") <> "" And objSessionStr("ArrayPXML") <> "-1" Then
		arrXML = Split(objSessionStr("ArrayPXML"), "</column>,")
	
		For iCounter = 0 To UBound(arrXML) - 1
			arrXML(iCounter) = arrXML(iCounter) & "</column>"
		Next
	
		For i = 0 To UBound(arrXML)
			xmlDoc.loadXML arrXML(i)
			
			'-- Fetching DataType for derived fields
			Set a1 = xmlDoc.getElementsByTagName("column").item(0).childNodes.item(0) 
			Set a2 = xmlDoc.getElementsByTagName("derived").item(0)
			If Not a2 Is Nothing Then
				'-- Handling for columns involved
				Set a3 = a1.getElementsByTagName("detailformula").item(0)
				If objSessionStr("ColumnsInvolved") = "-1" Then
					objSession("ColumnsInvolved") = a3.text
				Else
					objSession("ColumnsInvolved") = objSessionStr("ColumnsInvolved")  &  a3.text
				End If
			End If		
		Next
	End If
	
	MaintainColumnsAndSessions = True
	
End Function

Function SaveReport()

		SaveReport = False

	'-- User copies the report
	If objSessionStr("MODE1") = "1" And objSessionStr("Save") = "3" then
		rptName = Request.Form("hdRptName")
		rptDesc = Request.Form("hdRptDesc")
		
    	sXML = UpdateXMLReport()

		If Not GetValidObject("InitSMList.CDataManager", objData) Then
			Call ShowError(pageError, "Call to CDataManager class failed.", True)
		End If
		objData.m_RMConnectStr = Application("SM_DSN")
		objData.ReportName = rptName
		objData.ReportDesc = rptDesc
		objData.XML = sXML

		objData.AddReport objSessionStr("UserID")

		objSession("REPORTID") = objData.ReportID
		objSession("REPORTNAME") = objData.REPORTNAME
		
		Response.Redirect "rptFields.asp?REPORTID=" & objData.ReportID

		Set objData = Nothing
	End If
	
	'-- User uses tabbing first time but doesn't click on Save button
	'-- Save Report in temporary table
	If objSessionStr("MODE1") = "0" And objSessionStr("Save") = "0" Then		

		rptName = "-1"

    	sXML = GenerateXMLReport 
    	
    	'-- Create unique report name
    	Dim sGUID
    	sGUID = m_objSMList.GetGUID() 

		Dim objData
		If Not GetValidObject("InitSMList.CDataManager", objData) Then
			Call ShowError(pageError, "Call to CDataManager class failed.", True)
		End If
		objData.m_RMConnectStr = Application("SM_DSN")
		objData.TempReportID = sGUID
		objData.ReportName = rptName
		objData.XML = sXML
		objData.AddTempReport 
		Set objData = Nothing
		
		'-- Store this GUID as ReportID in custom session
		objSession("REPORTID") = sGUID
		
		'-- Come into update mode now
		objSession("MODE1") = "1"
		
	'----------------------------------------------------------------------
	'-- User continues using tabbing but hasn't clicked on Save button
	ElseIf objSessionStr("MODE1") = "1" And objSessionStr("Save") = "0" Then

		rptName = "-1"

    	sXML = UpdateXMLReport()

		If Not GetValidObject("InitSMList.CDataManager", objData) Then
			Call ShowError(pageError, "Call to CDataManager class failed.", True)
		End If
		objData.m_RMConnectStr = Application("SM_DSN")
		objData.TempReportID = objSessionStr("REPORTID")
		objData.ReportName = rptName
		objData.XML = sXML
		objData.UpdateTempReport
		Set objData = Nothing
    	
	'----------------------------------------------------------------------
	'-- User first time clicks on Save button without tabbing
	ElseIf objSessionStr("MODE1") = "0" And objSessionStr("Save") = "1" Then
		'-- Here we needn't worry about PROPERTIES setting...
		
		rptName = Replace(Request.Form("hdRptName"), "'", "''")
		rptDesc = Replace(Request.Form("hdRptDesc"), "'", "''")

		Set objXML = Server.CreateObject("MSXML2.DOMDOCUMENT")
		objXML.loadXML "<report reporttype=""multiquery"" reportdetailtype=""detailsummary"" orientation=""portrait"" papersize=""9"" selecttype=""all"">" _
					& "<reporttitle justify=""left"" color=""0"" fontname=""Courier New"" fontsize=""14.000000"" bold=""1"" italic=""0"" underline=""0"" strikeout=""0"">" _
					& "<subtitle></subtitle>" _
					& "</reporttitle>" _
					& "<headerleft></headerleft>" _
					& "<headercenter>" & Server.HTMLEncode("&") & "r</headercenter>"_
					& "<headerright>" & Server.HTMLEncode("&") & "t</headerright>"_
					& "<footerleft></footerleft>"_
					& "<footercenter>Page " & Server.HTMLEncode("&") & "p of " & Server.HTMLEncode("&") & "P</footercenter>"_
					& "<footerright></footerright>"_
					& "<reportfont fontname=""Courier New"" fontsize=""10.000000"" bold=""0"" italic=""0"" underline=""0"" strikeout=""0""/>"_
					& "<htmlsettings htmlnumlinesperpage=""0"" htmlpageformat=""single"" htmlpagebreak=""0"" htmlexportgraph=""0"" htmlgraphsamefile=""1"" htmlgraphtopofpage=""1"">"_
					& "<htmldefpath></htmldefpath>"_
					& "</htmlsettings>"_
					& "<columns>"_
					& "<columnorder>"_
					& "</columnorder>"_
					& "<columnlist>"_
					& "</columnlist>"_
					& "</columns>"_
					& "<criterialist/>"_
					& "<havinglist/>"_
					& "</report>"
					
	'-------Code for As of Checkbox and DatePicker-----------
		Set z = objXML.getElementsByTagName("report").item(0)

		If Request.Form("hdAsOfSelected") = "1"  Then
			z.setAttribute "evaldate", Request.Form("hdDate")
		Else
			If Not z.getAttributeNode("evaldate") Is Nothing Then z.removeAttribute("evaldate")
		End if
		
		If Request.Form("optReportType") = "0" Then
			z.setAttribute "reportdetailtype", "detail"
			detail = "checked"
		ElseIf Request.Form("optReportType") = "1" Then
			z.setAttribute "reportdetailtype", "summary"
			summary = "checked"
		ElseIf Request.Form("optReportType") = "2" Then
			z.setAttribute "reportdetailtype", "detailsummary"
			both = "checked"
		End If
		
	'-- Code for Option Buttons
		For iCount = 0 To CInt(objSessionStr("ColLength")) - 1
			Set y = objXML.getElementsByTagName("columnlist").item(0).appendChild(objXML.createElement("column"))
			Set y = objXML.getElementsByTagName("columnorder").item(0).appendChild(objXML.createElement("column"))
		Next
			
		colValXML = Request.Form("hdSelFieldsXML")
		colOrder = Request.Form("hdArrayTF")
	
		myArray2XML = Split(colValXML, "</column>,")
		
		If IsValidArray(myArray2XML) Then
			For iCounter = 0 To UBound(myArray2XML) - 1
				myArray2XML(iCounter) = myArray2XML(iCounter) & "</column>"
			Next
		
			myOrder2 = Split(colOrder, ",")

			'-- Initialize pXML array	
			objSession("ArrayPXML") = "-1"

			'-- Set attributes to column tag of columnlist tag

			For j = 0  To CInt(objSessionStr("ColLength")) - 1
				
				Set z = objXML.getElementsByTagName("columnlist").item(0).childNodes(j)
						
				'-- Set default attributes in XML template from pXML array
				Set xmlProp = Server.CreateObject("Msxml2.DOMDocument")
				xmlProp.async = false

				xmlProp.loadXML myArray2XML(j)
				
				For iCount = 0 To xmlProp.getElementsByTagName("column").item(0).attributes.length - 1
					z.setAttribute xmlProp.getElementsByTagName("column").item(0).attributes.item(iCount).name,xmlProp.getElementsByTagName("column").item(0).attributes.item(iCount).text
				Next

				Set x = xmlProp.getElementsByTagName("parenttables").item(0)
				If Not x Is Nothing Then z.appendChild(x)
	
				Set xmlProp = Nothing

				'-- Set attributes in XML template from properties.asp

				'-- Update pXML array with new added attributes
				If objSessionStr("ArrayPXML") = "-1" Then
					objSession("ArrayPXML") = Trim(objXML.getElementsByTagName("columnlist").item(0).childNodes.item(j).xml)
				Else
					objSession("ArrayPXML") = Trim(objSessionStr("ArrayPXML")) & "," & Trim(objXML.getElementsByTagName("columnlist").item(0).childNodes.item(j).xml)
				End If

			Next
		End If		

		'-- Maintain the tf array in XML template				
		For k=0 to CInt(objSessionStr("ColLength")) - 1
			objXML.getElementsByTagName("columnorder").item(0).childNodes.item(k).text=myOrder2(k)
		Next
	
		'-- Update global sXML variable with the latest XML template
		sXML = objXML.XML
	
		Set objXML = Nothing


		If Not GetValidObject("InitSMList.CDataManager", objData) Then
			Call ShowError(pageError, "Call to CDataManager class failed.", True)
		End If
		objData.m_RMConnectStr = Application("SM_DSN")
		objData.ReportName = rptName
		objData.ReportDesc = rptDesc
		objData.XML = sXML
		objData.AddReport objSessionStr("UserID")
		
		objSession("REPORTID") = objData.ReportID
		objSession("REPORTNAME") = objData.REPORTNAME
		
		Set objData = Nothing

		'-- Come into update mode now
		objSession("MODE1") = "1"
		'-- Suppress Save dialog window
		objSession("SaveFinal") = "1" 

	'----------------------------------------------------------------------
	'-- User continues using tabbing but has clicked on Save button first time
	ElseIf objSessionStr("MODE1") = "1" And objSessionStr("Save") = "1" And objSessionStr("SaveFinal") = "0" Then

		rptName = Request.Form("hdRptName")
		rptDesc = Request.Form("hdRptDesc")
		
    	sXML = UpdateXMLReport()

		'-- Delete corresponding temporary report from database
		If Not GetValidObject("InitSMList.CDataManager", objData) Then
			Call ShowError(pageError, "Call to CDataManager class failed.", True)
		End If
		objData.m_RMConnectStr = Application("SM_DSN")
		objData.TempReportID = objSessionStr("REPORTID")
		objData.ReportName = rptName
		objData.ReportDesc = rptDesc
		objData.XML = sXML
		objData.DeleteTempReport
		objData.AddReport objSessionStr("UserID")

		objSession("REPORTID") = objData.ReportID
		objSession("REPORTNAME") = objData.REPORTNAME

		Set objData = Nothing
	
		'-- Suppress save dilog window for saving next time	
		objSession("SaveFinal") = "1" 
				
	'----------------------------------------------------------------------
	'-- User has already clicked on Save button and again clicks save button or uses tabbing or any other SUBMIT point
	ElseIf objSessionStr("MODE1") = "1" And objSessionStr("Save") = "1" And objSessionStr("SaveFinal") = "1" then
	
		'-- Recreate the base XML template
		sXML = UpdateXMLReport()

		If Not GetValidObject("InitSMList.CDataManager", objData) Then
			Call ShowError(pageError, "Call to CDataManager class failed.", True)
		End If
		objData.m_RMConnectStr = Application("SM_DSN")
		objData.ReportID = objSessionStr("REPORTID")
		objData.XML = sXML
		objData.UpdateReport
		Set objData = Nothing
			
	End If 
	
	SaveReport = True

End Function

Function RefreshSessionVariables()

	If Request.Form("hdOPenProp") <> "-2" And Request.Form("hdOPenProp") <> "" then
		objSession("OPENPROPERTY") = Request.Form("hdOPenProp")
	Else
		objSession("OPENPROPERTY") = "-2"
	End If

	If Request.Form("hdSave") <> "-1" And Request.Form("hdSave") <> "" then
		objSession("Save") = Request.Form("hdSave")
	Else
		objSession("Save") = "-1"
	End If
	
	If Request.Form("hdSaveFinal") <> "-1" And Request.Form("hdSaveFinal") <> "" Then
		objSession("SaveFinal") = Request.Form("hdSaveFinal")
	Else
		objSession("SaveFinal") = "0"
	End If

	If Request.Form("hdSelFieldsLength") <> "-1" And Request.Form("hdSelFieldsLength") <> "" Then
		objSession("ColLength") = Request.Form("hdSelFieldsLength")
	Else 
		objSession("ColLength") = "-1"
	End If
	
	If Request.Form("hdArrayTF") <> "-1" And Request.Form("hdArrayTF") <> "" Then
		objSession("ArrayTF") = Request.Form("hdArrayTF")
	Else 
		objSession("ArrayTF") = "-1"
	End If
	
	If Request.Form("hdSelFields") <> "-1" And Request.Form("hdSelFields") <> "" Then
		objSession("ArrayP") = Request.Form("hdSelFields")
	Else 
		objSession("ArrayP") = "-1"
	End If
	
	If Request.Form("hdDerColumnInvolved") <> "-1" And Request.Form("hdDerColumnInvolved") <> "" Then
		objSession("ColumnsInvolved") = Request.Form("hdDerColumnInvolved")
	Else 
		objSession("ColumnsInvolved") = "-1"
	End If

	
	If Request.Form("hdDataType") <> "-1" And Request.Form("hdDataType") <> "" Then
		objSession("ArrayFieldType") = Request.Form("hdDataType")
	Else 
		objSession("ArrayFieldType") = "-1"
	End If
	

	If Request.Form("hdArrayPFields") <> "-1" And Request.Form("hdArrayPFields") <> "" Then
		objSession("ArrayPFields") = Request.Form("hdArrayPFields")
	Else 
		objSession("ArrayPFields") = "-1"
	End If
	
	On Error Resume Next	
	If Request.Form("hdSelFieldsXML") <> "-1" And Request.Form("hdSelFieldsXML") <> "" Then
		objSession("ArrayPXML") = Request.Form("hdSelFieldsXML")
	Else
		objSession("ArrayPXML") = "-1"
	End If

	If Request.Form("hdDerError") <> "-1" And Request.Form("hdDerError") <> "" Then
		sError = Request.Form("hdDerError")
	Else
		sError = ""
	End If
	
End Function

Function UpdateXMLReport()

	Dim myOrder2, myArray2XML, nSelectedIndex, j, colValXML, colOrder, z, xmlProp, k, y, v
	Dim AsOfYMD				'Tanu: Added for MITS 6778, MITS 6778 changes were commented in sminterface.dll for MITS 12914
	On Error Resume Next
	Dim objXML 
	Set objXML = Server.CreateObject("Msxml2.DOMDocument")
	objXML.async = false

	'--Load the existing XML
	objXML.loadXML sXML

	Set z = objXML.getElementsByTagName("report").item(0)

	If Request.Form("hdAsOfSelected") = "1"  Then
		'Tanu: Added for MITS 6778, MITS 6778 changes were commented in sminterface.dll for MITS 12914
		'Reason: As Of Date gets changed after posting in RM.NET.Changing As Of Date in "YYYYMMDD" format 
		'before its value get saved in REPORT_XML of SMWD_REPORT table
		AsOfYMD=mid(Request.Form("hdDate"),1,4) & mid(Request.Form("hdDate"),7,2) & mid(Request.Form("hdDate"),5,2)
		If Trim(Request.Form("hdDate")) <> "" Then z.setAttribute "evaldate", AsOfYMD
		'If Trim(Request.Form("hdDate")) <> "" Then z.setAttribute "evaldate", Request.Form("hdDate")
	Else
		Set z = objXML.getElementsByTagName("report").item(0)
		If Not z.getAttributeNode("evaldate") Is Nothing Then z.removeAttribute("evaldate")
	End if
			
	If Request.Form("optReportType") = "0" Then
		z.setAttribute "reportdetailtype", "detail"
		detail = "checked"
	ElseIf Request.Form("optReportType") = "1" Then
		z.setAttribute "reportdetailtype", "summary"
		summary = "checked"
	ElseIf Request.Form("optReportType") = "2" Then
		z.setAttribute "reportdetailtype", "detailsummary"
		both = "checked"
	End If

	Dim script
	Dim evobj

	If Request.Form("hdDerGScript") <> "" Then
		If Not GetValidObject("DTGScript.ScriptEngine", script) Then
			Call ShowError(pageError, "Call to DTG ScriptEngine class failed.", True)
		End If

		If Not GetValidObject("InitSMList.CEventSink", evobj) Then
			Call ShowError(pageError, "Call to CEventSink class failed.", True)
		End If
		
		script.StartEngine

		script.RegisterGlobalObject "", evobj, False

		On Error Resume Next
		script.AddScript Request.Form("hdDerGScript")
		
		sError = script.LastError
		
		If sError = "" Then
			z.setAttribute "globalscript", Request.Form("hdDerGScript")
		Else
			script.StopEngine
			Set evobj = Nothing
			Set script = Nothing
		End If
	Else
		If Not z.getAttributeNode("globalscript") Is Nothing Then z.removeAttribute("globalscript")
	End If
	
	'-- Create columnlist and columnorder tags
	'-- columnorder tag is used to maintain the order of report column display
		
	Set y = objXML.selectNodes("//columnlist//column")
	y.removeAll
	
	Set y = objXML.selectNodes("//columnorder//column")
	y.removeAll

	For iCount = 0 To CInt(objSessionStr("ColLength")) - 1
		Set y = objXML.getElementsByTagName("columnlist").item(0).appendChild(objXML.createElement("column"))
		Set y = objXML.getElementsByTagName("columnorder").item(0).appendChild(objXML.createElement("column"))
	Next

	'-- Retrieve the JavaScripts arrays
	'-- hdSelFields corresponds to  p array to maintain the column name display
	'-- hdSelFieldsXML corresponds to pXML array to maintain the columnlist tag in XML
	'-- hdArrayTF corresponds to tf array to maintain  the column order
			
	colValXML = Request.Form("hdSelFieldsXML")
	colOrder = Request.Form("hdArrayTF")

	'-- If page is submitted from properties.asp
	If Request.Form("hdSubmit") = "PROPERTIES" Then
		nSelectedIndex = CInt(Request.Form("hdSelectedIndex"))
	End If
	
	'-- Initialize pXML array	
	objSession("ArrayPXML") = "-1"

	Dim arrFieldNumbers(), iCounter
	iCounter = 0

	If CInt(objSessionStr("ColLength")) = 0 Then
		Set y = objXML.selectNodes("//criterialist//criteria")
		If Not y Is Nothing Then y.removeAll
		
		Set y = objXML.selectNodes("//havinglist//having")
		If Not y Is Nothing Then y.removeAll
	End If

	myArray2XML = Split(colValXML, "</column>,")
	myOrder2 = Split(colOrder, ",")

	If IsValidArray(myArray2XML) And IsValidArray(myOrder2) Then
		For iCounter = 0 To UBound(myArray2XML) - 1
			myArray2XML(iCounter) = myArray2XML(iCounter) & "</column>"            
		Next

		'-- Set attributes to column tag of columnlist tag
		For j = 0  To CInt(objSessionStr("ColLength")) - 1

			Set z = objXML.getElementsByTagName("columnlist").item(0).childNodes(j)

			'-- Extend XML template from pXML array
			Set xmlProp = Server.CreateObject("Msxml2.DOMDocument")
			xmlProp.async = false

			'--Rajeev -- 09/30/2003 -- Fix for & corrupting the XML in ArrayPXML
			'''xmlProp.loadXML replace( myArray2XML(j), "&", "&amp;")
			'Vsoni5 : MITS 19805 : encrypting '<' and '>' symbole for derrived field formula.
                        myArray2XML(j) = replace( myArray2XML(j), "&", "&amp;")
			
                        dim posStart, posEnd
			dim strDerFormula, strCol
                       
			posStart = InStr(1,myArray2XML(j),"<detailformula>")
			
			If posStart > 0 Then
			    posStart = posStart + 15
			    posEnd = InStr(posStart,myArray2XML(j),"</detailformula>")
			    strDerFormula = Mid(myArray2XML(j),posStart,posEnd-posStart)
			    strDerFormula = Replace(strDerFormula,"<", "&lt;")
			    strDerFormula = Replace(strDerFormula,">", "&gt;")
			    
			   strCol = Left(myArray2XML(j),posStart-1) & strDerFormula & Right(myArray2XML(j), Len(myArray2XML(j))- posEnd+1)
			   myArray2XML(j) = strCol
			End If
			
                        xmlProp.loadXML myArray2XML(j)
            
			Redim Preserve arrFieldNumbers(j)
			arrFieldNumbers(j) = xmlProp.getElementsByTagName("column").item(0).attributes.item(0).text

			'-- Set attributes
			For iCount = 0 To xmlProp.getElementsByTagName("column").item(0).attributes.length - 1
				If iCount = 5 And xmlProp.getElementsByTagName("column").item(0).attributes.item(iCount).text = "" Then
					Dim FieldString, DefFieldWidth
					FieldString = xmlProp.getElementsByTagName("column").item(0).attributes.item(1).text & "," & xmlProp.getElementsByTagName("column").item(0).attributes.item(2).text
					If Not m_objSMList.GetDefaultFieldWidth(FieldString,DefFieldWidth) Then
						Call ShowError(pageError, "Couldn't fetch the default column width.", False)
					End If
					z.setAttribute xmlProp.getElementsByTagName("column").item(0).attributes.item(iCount).name, DefFieldWidth
				Else
					z.setAttribute xmlProp.getElementsByTagName("column").item(0).attributes.item(iCount).name, xmlProp.getElementsByTagName("column").item(0).attributes.item(iCount).text
				End If
			Next

			'-- Set Derived tag
			Set x = xmlProp.getElementsByTagName("derived").item(0)
			If Not x Is Nothing Then z.appendChild(x)


			If Request.Form("hdSubmit") = "PROPERTIES" Then

			'^Rajeev 05/20/2003 -- 
						
				If (Cint(nSelectedIndex) = j) Then

					'-- No Print
					If Request.Form("hdIsIncludeSelected") = "1" Then 
						z.setAttribute "noprint","1"
					Else
						If Not z.getAttributeNode("noprint") Is Nothing Then z.removeAttribute("noprint")
					End If
					'-- Sort
					If Request.Form("hdColumnSort") = "1" Then 
						z.setAttribute "sort","asc"
					Elseif Request.Form("hdColumnSort") = "2" Then
						z.setAttribute "sort","desc"
					Else
						If Not z.getAttributeNode("sort") Is Nothing Then z.removeAttribute("sort")			
					End If
					'-- Break
						
					If Request.Form("hdColBreak") = "1" Then 
						z.setAttribute "break","break"
						If Not z.getAttributeNode("page") Is Nothing Then z.removeAttribute("page")
						If Not z.getAttributeNode("banner") Is Nothing Then z.removeAttribute("banner")
					Elseif Request.Form("hdColBreak") = "2" Then
						'Manish C Jha 08/10/2009 - MITS 15151
						'Reason for change: Break Mapping was incorrect
						z.setAttribute "break","page"
						If Not z.getAttributeNode("page") Is Nothing Then z.removeAttribute("page")
						If Not z.getAttributeNode("banner") Is Nothing Then z.removeAttribute("banner")
					Elseif Request.Form("hdColBreak") = "3" Then
						z.setAttribute "banner","banner"
						If Not z.getAttributeNode("page") Is Nothing Then z.removeAttribute("page")
						If Not z.getAttributeNode("break") Is Nothing Then z.removeAttribute("break")
					Else
						If Not z.getAttributeNode("page") Is Nothing Then z.removeAttribute("page")
						If Not z.getAttributeNode("break") Is Nothing Then z.removeAttribute("break")
						If Not z.getAttributeNode("banner") Is Nothing Then z.removeAttribute("banner")
					End If
					'-- Stack
					If Request.Form("hdIsStackSelected") = "1" Then
						z.setAttribute "stack","stack"
					Else
						If Not z.getAttributeNode("stack") Is Nothing Then z.removeAttribute("stack")
					End if
							
					If Request.Form("hdBannerStack") = "1" Then
						z.setAttribute "bannerstack","bannerstack"
					Else
						If Not z.getAttributeNode("bannerstack") Is Nothing Then z.removeAttribute("bannerstack")
					End If
					'-- Creating subtotal attribute along with dependent attributes-----applicable for all except DataType 8.
					'-- Sub Total
					If Request.Form("hdIsPrintSelected") = "1" Then
						z.setAttribute "subtotal","1"
					Else
						If Not z.getAttributeNode("subtotal") Is Nothing Then z.removeAttribute("subtotal")
					End If
					'-- Duplicate Columns
					If Request.Form("hdIsSuppressSelected") = "1" Then 
						z.setAttribute "duplicatecolumns","1"
					Else
						If Not z.getAttributeNode("duplicatecolumns") Is Nothing Then z.removeAttribute("duplicatecolumns")
					End If
					'-- Roll Up
					If Request.Form("hdIsRollSelected") = "1" Then 
						z.setAttribute "rollup","1"
					Else
						If Not z.getAttributeNode("rollup") Is Nothing Then z.removeAttribute("rollup")
					End If
					'-- Fiscal Year
					If Request.Form("hdIsFiscSelected") = "1" Then 
						z.setAttribute "fiscalyear","1"
						z.setAttribute "organization",Request.Form("hdOrganization")
						z.setAttribute "lineofbusiness",Request.Form("hdLineOfBusiness")
					Else
						If Not z.getAttributeNode("fiscalyear") Is Nothing Then z.removeAttribute("fiscalyear")
						If Not z.getAttributeNode("organization") Is Nothing Then z.removeAttribute("organization")
						If Not z.getAttributeNode("lineofbusiness") Is Nothing Then z.removeAttribute("lineofbusiness")
					End If
					'-- Max lines
					If Request.Form("hdDefaultLines") = "1"  Then 
						z.setAttribute "maxlinesinrow",Request.Form("hdMaxLines")
					Else
						If Not z.getAttributeNode("maxlinesinrow") Is Nothing Then z.removeAttribute("maxlinesinrow")			
					End If					
						
					If Request.Form("hdIsAggUniqueSelected") = "1" Then
						z.setAttribute "agguniquecount","1"
						If Not z.getAttributeNode("aggcount") Is Nothing Then z.removeAttribute("aggcount")
					ElseIf Request.Form("hdIsAggCountSelected") = "1" Then 
						z.setAttribute "aggcount","1"
						If Not z.getAttributeNode("agguniquecount") Is Nothing Then z.removeAttribute("agguniquecount")
					Else
						If Not z.getAttributeNode("agguniquecount") Is Nothing Then z.removeAttribute("agguniquecount")
						If Not z.getAttributeNode("aggcount") Is Nothing Then z.removeAttribute("aggcount")
					End If
						
					If Request.Form("hdIsAggSumSelected") = "1" Then 
						z.setAttribute "aggsum","1"
					Else
						If Not z.getAttributeNode("aggsum") Is Nothing Then z.removeAttribute("aggsum")
					End If				

					If Request.Form("hdIsAggAvgSelected") = "1" Then 
						z.setAttribute "aggaverage","1"
					Else
						If Not z.getAttributeNode("aggaverage") Is Nothing Then z.removeAttribute("aggaverage")
					End If
						
					If Request.Form("hdIsAggMinSelected") = "1" Then 
						z.setAttribute "aggmin","1"
					Else
						If Not z.getAttributeNode("aggmin") Is Nothing Then z.removeAttribute("aggmin")
					End If							
						
					If Request.Form("hdIsAggMaxSelected") = "1" Then 
						z.setAttribute "aggmax","1"
					Else
						If Not z.getAttributeNode("aggmax") Is Nothing Then z.removeAttribute("aggmax")
					End If										
				End If
				'^^Rajeev 05/20/2003 -- 
			End If

			'-- Prepare Aggregate labels
			If Request.Form("hdSubmit") = "PROPERTIES" Then
				If Cint(nSelectedIndex) = j Then
					If Request.Form("hdIsAggCountSelected") = "1" _
						Or Request.Form("hdIsAggUniqueSelected") = "1" _
						Or Request.Form("hdIsAggSumSelected") = "1" _
						Or Request.Form("hdIsAggAvgSelected") = "1" _
						Or Request.Form("hdIsAggMinSelected") = "1" _
						Or Request.Form("hdIsAggMaxSelected") = "1" Then
								
						Set objAggregate = z.appendChild(objXML.createElement("aggregatelabels"))
						For iCount = 0 To 6
							Set objLabel = objAggregate.appendChild(objXML.createElement("aggregatelabel"))
						Next
									
						If Request.Form("hdAggUniqueCount") = "1" Then z.getElementsByTagName("aggregatelabels").item(0).childNodes.item(0).appendChild(objXML.createTextNode("Records:"))
						If Request.Form("hdAggCount") = "1" Then z.getElementsByTagName("aggregatelabels").item(0).childNodes.item(1).appendChild(objXML.createTextNode("Records:"))
						If Request.Form("hdAggAvg") = "1" Then z.getElementsByTagName("aggregatelabels").item(0).childNodes.item(3).appendChild(objXML.createTextNode("Avg:"))
						If Request.Form("hdAggMin") = "1" Then z.getElementsByTagName("aggregatelabels").item(0).childNodes.item(4).appendChild(objXML.createTextNode("Min:"))
						If Request.Form("hdAggMax") = "1" Then z.getElementsByTagName("aggregatelabels").item(0).childNodes.item(5).appendChild(objXML.createTextNode("Max:"))
					End If
				End If
			ElseIf Request.Form("hdUpload") = "UPLOAD" Then '-- Rajeev -- 02/18/2004 -- After Report Uploading
				If Not z.getAttributeNode("aggcount") Is Nothing _
					Or Not z.getAttributeNode("agguniquecount") Is Nothing _
					Or Not z.getAttributeNode("aggsum") Is Nothing _
					Or Not z.getAttributeNode("aggaverage") Is Nothing _
					Or Not z.getAttributeNode("aggmin") Is Nothing _
					Or Not z.getAttributeNode("aggmax")Is Nothing Then
									
					Set objAggregate = z.appendChild(objXML.createElement("aggregatelabels"))

					Set objLabel = objAggregate.appendChild(objXML.createElement("aggregatelabel"))
					If Not z.getAttributeNode("agguniquecount") Is Nothing Then objLabel.appendChild(objXML.createTextNode("Records:"))
					
					Set objLabel = objAggregate.appendChild(objXML.createElement("aggregatelabel"))
					If Not z.getAttributeNode("aggcount") Is Nothing Then objLabel.appendChild(objXML.createTextNode("Records:"))

					Set objLabel = objAggregate.appendChild(objXML.createElement("aggregatelabel"))

					Set objLabel = objAggregate.appendChild(objXML.createElement("aggregatelabel"))
					If Not z.getAttributeNode("aggaverage") Is Nothing Then objLabel.appendChild(objXML.createTextNode("Avg:"))

					Set objLabel = objAggregate.appendChild(objXML.createElement("aggregatelabel"))
					If Not z.getAttributeNode("aggmin") Is Nothing Then objLabel.appendChild(objXML.createTextNode("Min:"))

					Set objLabel = objAggregate.appendChild(objXML.createElement("aggregatelabel"))
					If Not z.getAttributeNode("aggmax") Is Nothing Then objLabel.appendChild(objXML.createTextNode("Max:"))

					Set objLabel = objAggregate.appendChild(objXML.createElement("aggregatelabel"))
					
				End If		
			Else '-- Retain
				If Not z.getAttributeNode("aggcount") Is Nothing _
					Or Not z.getAttributeNode("agguniquecount") Is Nothing _
					Or Not z.getAttributeNode("aggsum") Is Nothing _
					Or Not z.getAttributeNode("aggaverage") Is Nothing _
					Or Not z.getAttributeNode("aggmin") Is Nothing _
					Or Not z.getAttributeNode("aggmax")Is Nothing Then
									
					Set objAggregate = z.appendChild(objXML.createElement("aggregatelabels"))

					For iCount = 0 To 6
						Set objLabel = objAggregate.appendChild(objXML.createElement("aggregatelabel"))

						If Not xmlProp.getElementsByTagName("aggregatelabels").item(0) Is Nothing Then
							objLabel.appendChild(objXML.createTextNode( xmlProp.getElementsByTagName("aggregatelabels").item(0).childNodes.item(iCount).text))
						End If
					Next

				End If		
			End If

			'-- Set parenttables tag
			Set x = xmlProp.getElementsByTagName("parenttables").item(0) ''.appendChild(objXML.createElement("column"))
			If Not x Is Nothing Then z.appendChild(x)
					
			Set xmlProp = Nothing

			'-- Set attributes in XML template from properties.asp

			'-- Update pXML array with new added attributes
			If objSessionStr("ArrayPXML") = "-1" Then
				objSession("ArrayPXML") = Trim(objXML.getElementsByTagName("columnlist").item(0).childNodes.item(j).xml)
			Else
				objSession("ArrayPXML") = Trim(objSessionStr("ArrayPXML")) & "," & Trim(objXML.getElementsByTagName("columnlist").item(0).childNodes.item(j).xml)
			End If

		Next
	
		'--Start MITS 6708
		'--Ankur Saxena 06/15/2006
		'--Relative date code commented here and moved to the bottom because this functionality is 
		'--independant of the other functionality causing problem in adding and updating the relative dates.
		'--If Request.Form("hdSubmit") = "RELATIVEDATE" And Request.Form("hdRelMode") = "ADD" Then
		'--	If Request.Form("hdRelDate") <> "" And Request.Form("hdRelDateFormula") <> "" Then
		'--		Dim objData
		'--		If Not GetValidObject("InitSMList.CDataManager", objData) Then
		'--			Call ShowError(pageError, "Call to CDataManager class failed.", True)
		'--		End If
		'--		
		'--		objData.m_RMConnectStr = Application("SM_DSN")
		'--	
		'--		objData.AddDateFormula Request.Form("hdRelDate"), Request.Form("hdRelDateFormula")
		'--		Set objData = Nothing
		'--	End If
		'--	
		'--ElseIf Request.Form("hdSubmit") = "RELATIVEDATE" And Request.Form("hdRelMode") = "UPDATE" Then
		'--	If Request.Form("hdRelDate") <> "" And Request.Form("hdRelDateFormula") <> "" Then
		'--		If Not GetValidObject("InitSMList.CDataManager", objData) Then
		'--			Call ShowError(pageError, "Call to CDataManager class failed.", True)
		'--		End If
		'--		objData.m_RMConnectStr = Application("SM_DSN")
		'--		
		'--		objData.UpdateDateFormula CInt(Request.Form("hdRelID")),Request.Form("hdRelDateFormula")
		'--		Set objData = Nothing
		'--	End If
		'--
		'--End If
		'--End MITS 6708
	
		'-- j counter is now having total no. of columns till now i.e. before adding derived field
		Dim objDerived, objDetailFormula, objAggregateFormula, a1, a2, a3
	
		'-- Add mode
		If Request.Form("hdSubmit") = "DERIVED" And Request.Form("hdDerAddEdit") = "Add" Then
			'-- Add column tag in columnorder tag and its value
			Set z = objXML.getElementsByTagName("columnorder").item(0).appendChild(objXML.createElement("column"))
				z.appendChild(objXML.createTextNode(objXML.getElementsByTagName("columnorder").item(0).childNodes.length - 1))
			
			a4 = -1
			For k = objXML.getElementsByTagName("columnlist").item(0).childNodes.length - 1 To 0 Step -1
				Set a1 = objXML.getElementsByTagName("columnlist").item(0).childNodes.item(k)
				Set a2 = a1.getElementsByTagName("derived").item(0)
				
				If a4 = -1 Then	a4 = Cstr(CInt(a1.getAttributeNode("fieldnumber").value) + 1)
				
				If Not a2 Is Nothing Then
					a3 = a1.getAttributeNode("tableid").value
					a3 = CStr(CInt(a3) - 1)
					Exit For 
				Else
					a3 = "-1"
				End If
			Next

			If Not GetValidObject("DTGScript.ScriptEngine", script) Then
				Call ShowError(pageError, "Call to DTG ScriptEngine class failed.", True)
			End If
			If Not GetValidObject("InitSMList.CEventSink", evobj) Then
				Call ShowError(pageError, "Call to CEventSink class failed.", True)
			End If
			
			script.StartEngine

			script.RegisterGlobalObject "", evobj, False

			On Error Resume Next
			script.AddScript "function " & Request.Form("hdDerFieldName") & "()" & Chr(13) & " end function"

			sError = script.LastError

			If sError = "" Then
				'-- Add column in columnlist tag and its children
				Set z = objXML.getElementsByTagName("columnlist").item(0).appendChild(objXML.createElement("column"))
					z.setAttribute "fieldnumber", a4 
					z.setAttribute "tableid", a3
					z.setAttribute "fieldid", a3
					z.setAttribute "usercolname", Trim(Request.Form("hdDerFieldName"))
					z.setAttribute "colwidthunits", "chars"
					z.setAttribute "colwidth", Trim(Request.Form("hdDerFieldWidth"))
					z.setAttribute "justify", "left"
					z.setAttribute "color", "0"
				
					script.AddScript "function " & Request.Form("hdDerFieldName") & "()" & Chr(13) & Request.Form("hdDerDetailFormula") & Chr(13) & Chr(13) & " end function"

					sError = script.LastError
			
					Set objDerived = z.appendChild(objXML.createElement("derived"))
						objDerived.setAttribute "fieldtype", Request.Form("hdDerFieldType")
						objDerived.setAttribute "fieldname", Request.Form("hdDerFieldName")
						objDerived.setAttribute "defaultfieldwidth", Request.Form("hdDerFieldWidth")
						Set objDetailFormula = objDerived.appendChild(objXML.createElement("detailformula"))
					
					If sError = "" Then	
						objDetailFormula.appendChild(objXML.createTextNode(Request.Form("hdDerDetailFormula")))			
						
						'-- Check for Aggregate Formula
						If Request.Form("hdDerAggregate") = "sameformula" Or Request.Form("hdDerAggregate") = "differentformula" Then
							Set objAggregateFormula = objDerived.appendChild(objXML.createElement("aggregateformula"))
								objDerived.setAttribute "aggregatecalcmethod", Request.Form("hdDerAggregate")
						End If

						script.AddScript "function " & Request.Form("hdDerFieldName") & "()" & Chr(13) & Request.Form("hdDerAggregateFormula") & Chr(13) & Chr(13) & " end function"

						sError = script.LastError
						
						If sError = "" Then	
							objAggregateFormula.appendChild(objXML.createTextNode(Request.Form("hdDerAggregateFormula")))
						Else
							script.StopEngine
			
							Set evobj = Nothing
							Set script = Nothing 
						End If

					Else
						script.StopEngine
			
						Set evobj = Nothing
						Set script = Nothing 
					End If
			Else
				script.StopEngine
			
				Set evobj = Nothing
				Set script = Nothing 
			
			End If

			'-- Set parenttables tag
			z.appendChild(objXML.createElement("parenttables"))

			'-- Update pXML array with new added derived field and its attributes and children
			objSession("ArrayPXML") = Trim(objSessionStr("ArrayPXML")) & "," & Trim(objXML.getElementsByTagName("columnlist").item(0).childNodes.item(objXML.getElementsByTagName("columnlist").item(0).childNodes.length - 1).xml)
'-- Rajeev -- 03/09/2004			
'''			objSession("ArrayP") = Trim(objSessionStr("ArrayP")) & "," & "Derived : " & Request.Form("hdDerFieldName")
			objSession("ArrayP") = Trim(objSessionStr("ArrayP")) & "*!^" & "Derived : " & Request.Form("hdDerFieldName")
		ElseIf Request.Form("hdSubmit") = "DERIVED" And Request.Form("hdDerAddEdit") = "Edit" Then '-- Edit mode
			
			Set z = objXML.getElementsByTagName("columnlist").item(0).childNodes.item(CInt(Request.Form("hdDerIndex")))

			z.setAttribute "colwidth", Trim(Request.Form("hdDerFieldWidth"))

			Set objDerived = z.getElementsByTagName("derived").item(0)

				objDerived.setAttribute "fieldtype", Request.Form("hdDerFieldType")
				objDerived.setAttribute "fieldname", Request.Form("hdDerFieldName")
				objDerived.setAttribute "defaultfieldwidth", Request.Form("hdDerFieldWidth")
			
			'-- Check for Aggregate Formula
			Dim objTempAgg, p, sAggFormula
			Set objTempAgg = objDerived.getElementsByTagName("aggregateformula").item(0)
			
			If Not objTempAgg Is Nothing Then
				sAggFormula = objTempAgg.text 
			Else
				sAggFormula = ""
			End If
			
			Set p = z.childNodes.item(0).getAttributeNode("aggregatecalcmethod")

			If Not p Is Nothing Then 
				objDerived.removeAttribute("aggregatecalcmethod")
			End If

			If Not objTempAgg Is Nothing Then
				objDerived.removeChild(objTempAgg)
			End If
			
			If Not GetValidObject("DTGScript.ScriptEngine", script) Then
				Call ShowError(pageError, "Call to DTG ScriptEngine class failed.", True)
			End If
			If Not GetValidObject("InitSMList.CEventSink", evobj) Then
				Call ShowError(pageError, "Call to CEventSink class failed.", True)
			End If

			script.StartEngine

			script.RegisterGlobalObject "ScriptEvent", evobj, False
			
			On Error Resume Next
			script.AddScript "function " & Request.Form("hdDerFieldName") & "()" & Chr(13) & Request.Form("hdDerDetailFormula") & Chr(13) & Chr(13) & " end function"
   
			sError = script.LastError

			If sError = "" Then

				Set objDetailFormula = objDerived.getElementsByTagName("detailformula").item(0)
					objDetailFormula.text = Request.Form("hdDerDetailFormula")

				'-- Check for Aggregate Formula
				If Request.Form("hdDerAggregate") = "sameformula" Or Request.Form("hdDerAggregate") = "differentformula" Then
					Set objAggregateFormula = objDerived.appendChild(objXML.createElement("aggregateformula"))
						objDerived.setAttribute "aggregatecalcmethod", Request.Form("hdDerAggregate")
				End If
						
				script.AddScript "function " & Request.Form("hdDerFieldName") & "()" & Chr(13) & Request.Form("hdDerAggregateFormula") & Chr(13) & Chr(13) & " end function"
   
				sError = script.LastError
					
				Set objAggregateFormula = objDerived.getElementsByTagName("aggregateformula").item(0)
					
				If sError = "" Then
					objAggregateFormula.text = Request.Form("hdDerAggregateFormula")
				Else
					objAggregateFormula.text = sAggFormula
				End If
			Else
				'-- Check for Aggregate Formula
				If Request.Form("hdDerAggregate") = "sameformula" Or Request.Form("hdDerAggregate") = "differentformula" Then
					Set objAggregateFormula = objDerived.appendChild(objXML.createElement("aggregateformula"))
						objDerived.setAttribute "aggregatecalcmethod", Request.Form("hdDerAggregate")
				End If
				Set objAggregateFormula = objDerived.getElementsByTagName("aggregateformula").item(0)
					
				objAggregateFormula.text = sAggFormula	
			
			End If

			script.StopEngine
			
			Set evobj = Nothing
			Set script = Nothing 

			objSession("ArrayPXML") = "-1"
			
			For j = 0  To objXML.getElementsByTagName("columnlist").item(0).childNodes.length - 1
				If objSessionStr("ArrayPXML") = "-1" Then
					objSession("ArrayPXML") = Trim(objXML.getElementsByTagName("columnlist").item(0).childNodes.item(j).xml)
				Else
					objSession("ArrayPXML") = Trim(objSessionStr("ArrayPXML")) & "," & Trim(objXML.getElementsByTagName("columnlist").item(0).childNodes.item(j).xml)
				End If
			Next

		ElseIf Request.Form("hdDerAddEdit") = "Delete" Then '-- Delete Derived Field
				Set z = objXML.getElementsByTagName("columnlist").item(0)
					z.removeChild z.childNodes.item(CInt(Request.Form("hdDerIndex")))
			
			objSession("ArrayPXML") = "-1"
			For j = 0  To CInt(z.childNodes.length) - 1
				If objSessionStr("ArrayPXML") = "-1" Then
					objSession("ArrayPXML") = Trim(objXML.getElementsByTagName("columnlist").item(0).childNodes.item(j).xml)
				Else
					objSession("ArrayPXML") = Trim(objSessionStr("ArrayPXML")) & "," & Trim(objXML.getElementsByTagName("columnlist").item(0).childNodes.item(j).xml)
				End If
			Next
			
				
			'-- Updating columns involved 
			Dim arrXML, b1, b2, b3
			Set xmlDoc = Server.CreateObject("Msxml2.DOMDocument")
			xmlDoc.async = false
			
			arrXML = Split(objSessionStr("ArrayPXML"), "</column>,")
			
			If IsValidArray(arrXML) Then
				For iCounter = 0 To UBound(arrXML) - 1
					arrXML(iCounter) = arrXML(iCounter) & "</column>"
				Next
			
				Dim sColumnInvolved
				sColumnInvolved = "-1"
				For i = 0 To UBound(arrXML)
					xmlDoc.loadXML arrXML(i)
					'-- Fetching DataType for derived fields
					Set b1 = xmlDoc.getElementsByTagName("column").item(0).childNodes.item(0) 
					Set b2 = xmlDoc.getElementsByTagName("derived").item(0)
					If Not b2 Is Nothing Then
						'-- Handling for columns involved
							Set b3 = b1.getElementsByTagName("detailformula").item(0)
							If sColumnInvolved = "-1" Then
								sColumnInvolved = b3.text
							Else
								sColumnInvolved = sColumnInvolved  & b3.text
							End If
					End If		
				Next
			End If
			
			objSession("ColumnsInvolved") = "-1"
			If Not (sColumnInvolved = "-1" Or sColumnInvolved = "") Then
				objSession("ColumnsInvolved") = sColumnInvolved
			End If
			
		End If
	
		'-- Maintain the tf array in XML template				
		For k=0 To CInt(objSessionStr("ColLength")) - 1
			objXML.getElementsByTagName("columnorder").item(0).childNodes.item(k).text=myOrder2(k)
		Next

		Set y = objXML.getElementsByTagName("criterialist").item(0)
		v = y.ChildNodes.length
		While v > 0
			Set z = y.ChildNodes(v-1).getAttributeNode("fieldnumber")
			bFound = False
			If IsValidArray(arrFieldNumbers) Then
				For n = 0 To UBound(arrFieldNumbers)
					If z.value = arrFieldNumbers(n) Then 
						bFound = True
						Exit For
					End If
				Next
			End If
			If Not bFound Then
				y.removeChild y.childNodes.item(v-1)
			End If
			v = v - 1
		Wend

		Set y = objXML.getElementsByTagName("havinglist").item(0)
	
		v = y.ChildNodes.length

		While v > 0
			Set z = y.ChildNodes(v-1).getAttributeNode("fieldnumber")		
			bFound = False
			If IsValidArray(arrFieldNumbers) Then
				For n = 0 To UBound(arrFieldNumbers)
					If z.value = arrFieldNumbers(n) Then 
						bFound = True
						Exit For
					End If
				Next
			End If
			
			If Not bFound Then
				y.removeChild y.childNodes.item(v-1)
			End If
	
			v = v - 1
		Wend	
	End If

	'--Start MITS 6708
	'--Ankur Saxena 06/15/2006
	'--Add or update date formula, it is independant of the above functionality. Moved from above if statement to here.
	Dim objData
	If Request.Form("hdSubmit") = "RELATIVEDATE" And Request.Form("hdRelMode") = "ADD" Then
		If Request.Form("hdRelDate") <> "" And Request.Form("hdRelDateFormula") <> "" Then
			If Not GetValidObject("InitSMList.CDataManager", objData) Then
				Call ShowError(pageError, "Call to CDataManager class failed.", True)
			End If
				
			objData.m_RMConnectStr = Application("SM_DSN")
			objData.AddDateFormula Request.Form("hdRelDate"), Request.Form("hdRelDateFormula")
			Set objData = Nothing
		End If
	ElseIf Request.Form("hdSubmit") = "RELATIVEDATE" And Request.Form("hdRelMode") = "UPDATE" Then
		If Request.Form("hdRelDate") <> "" And Request.Form("hdRelDateFormula") <> "" Then
			If Not GetValidObject("InitSMList.CDataManager", objData) Then
				Call ShowError(pageError, "Call to CDataManager class failed.", True)
			End If
			
			objData.m_RMConnectStr = Application("SM_DSN")
			objData.UpdateDateFormula CInt(Request.Form("hdRelID")),Request.Form("hdRelDateFormula")
			Set objData = Nothing
		End If
	End If
	'--End MITS 6708

	'-- Update global sXML variable with the latest XML template
	UpdateXMLReport = objXML.XML

	Set objXML = Nothing

End Function

%>
