<html>
	<head>
		<link rel="stylesheet" href="RMNet.css" type="text/css" />
		<title>OLE Criteria Parameters</title>
		<script>
			function window_onload()
			{
				if(window.opener.window.document.frmData.hdOLECritName.value != '' && window.opener.window.document.frmData.hdOLECritDesc.value != '')
				{
					window.document.frmData.txtOLEName.value = window.opener.window.document.frmData.hdOLECritName.value;
					window.document.frmData.txtOLEDesc.value = window.opener.window.document.frmData.hdOLECritDesc.value;
				}
				if(window.opener.window.document.frmData.hdOLECritName.value == '')
					window.document.frmData.txtOLEName.value = window.opener.window.document.frmData.hdFieldName.value;
				if(window.opener.window.document.frmData.hdOLECritDesc.value == '')
					window.document.frmData.txtOLEDesc.value = window.opener.window.document.frmData.hdFieldName.value;
			}
			function on_OK()
			{			
				window.opener.window.document.frmData.hdOLECritName.value = window.document.frmData.txtOLEName.value;
				window.opener.window.document.frmData.hdOLECritDesc.value = window.document.frmData.txtOLEDesc.value;
				
				window.close();
			}
		</script>
	</head>
  
	<body class="8pt" onLoad="window_onload();">
		<form name="frmData">
		<table class="formsubtitle" border="0" width="100%">
			<tr>
				<td class="ctrlgroup">OLE Criteria Parameters</td>
			</tr>
		</table>
		<br>
		<table border="0" width="100%" align="center">
			<tr>
				<td width="10%">&nbsp;</td>
				<td align="right" width="20%">&nbsp;&nbsp;Name:&nbsp;</td>
				<td><input name="txtOLEName" type="text" size="30">&nbsp;&nbsp;&nbsp;</td>
				<td width="10%">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;</td>
			</tr>
			<tr>
				<td width="10%">&nbsp;</td>
				<td align="right" width="20%">&nbsp;&nbsp;Description:&nbsp;</td>
				<td><input name="txtOLEDesc" type="text" size="30" >&nbsp;&nbsp;&nbsp;</td>
				<td width="10%">&nbsp;</td>
			</tr>
		</table>
		<br>
			<hr>
		<table width="100%" border="0">
			<tr>
				<td width="20%">&nbsp;</td>
				<td width="60%" align="center">
					<input type="button" class="button" style1="width:40%" name="up" value="    OK    "onClick="on_OK();">
					<input type="button" class="button" style1="width:20%" name="up" value="  Cancel  " onClick="window.close()">
				</td>
				<td width="20%">&nbsp;</td>
			</tr>
		</table>
		</form>
	</body>
</html>