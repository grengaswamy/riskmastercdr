<% OPTION EXPLICIT %>
<!-- #include file ="session.inc" -->
<!-- #include file ="generic.asp" -->
<%'Public objSession
'initSession objSession
%> 
<%
Dim DOCUMENT_PATH
dim smi
dim errorHTML
dim sHTTP
dim sPath, l, lDocPathType

DOCUMENT_PATH= objSessionStr(SESSION_DOCPATH)

errorHTML = ""

set smi = CreateObject("SMInterface.SMI")

if ucase(Request.ServerVariables("HTTPS")) = "ON" then	sHTTP = "https://" else	sHTTP = "http://"

sPath=Request.ServerVariables("SCRIPT_NAME")
l=InStrRev(sPath,"/")
If l>0 Then
	sPath=Left(sPath,l)
Else
	sPath="/"
End If

lDocPathType = objSessionStr(SESSION_DOCPATHTYPE)
If smi.runReportQueued2(objSessionStr("LoginName"), objSessionStr("UserID"), objSessionStr("DSN"), Application("SM_DSN"),DOCUMENT_PATH, sHTTP & Request.ServerVariables("SERVER_NAME") & sPath & "smrepserve.asp", lDocPathType, errorHTML) then
	Response.Redirect "smrepqueue.asp"
Else
%>
<html><head><title></title>
<!--[Rajeev 11/19/2002 CSS Consolidation] link rel="stylesheet" href="sm.css" type="text/css" /-->
<link rel="stylesheet" href="RMNet.css" type="text/css" />
</head>
<body>
<b><font color="red">There were errors in the submitted report criteria. Please hit the Back button on your browser to correct the mistakes and submit again.<br/><br/></font></b>
<%
	Response.Write errorHTML
%>
</body></html>
<%
end if

set smi = Nothing

Response.End

%>