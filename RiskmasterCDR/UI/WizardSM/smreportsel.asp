<!-- #include file ="SessionInitialize.asp" -->
<%Public objSession
initSession objSession
%> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html><head><title></title>
<!--[Rajeev 11/15/2002 CSS Consolidation] link rel="stylesheet" href="sm.css" type="text/css"/-->
<link rel="stylesheet" href="RMNet.css" type="text/css" />
<script>
var ns, ie, ieversion;
var browserName = navigator.appName;                   // detect browser 
var browserVersion = navigator.appVersion;
if (browserName == "Netscape")
{
	ie=0;
	ns=1;
}
else		//Assume IE
{
	ieversion=browserVersion.substr(browserVersion.search("MSIE ")+5,1);
	ie=1;
	ns=0;
}

function onPageLoaded()
{
	
}
</script>
</head>
<body class="Margin0" onload="onPageLoaded()">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr><td class="ctrlgroup2" colspan="8">Available Reports</td></tr>
<tr>
<td class="colheader3" width="30%">&nbsp;Report Name</td>
<td class="colheader3" width="70%">&nbsp;Report Description</td>
</tr>
</table>

	<DIV id="divForms" class="divScroll">
		<TABLE border="0" width="100%" height="0">
<%
dim r
dim sSQL
dim henv
dim db
dim rs
dim vName, vDesc, lReportID, sReportXML
dim i

i = 0


sSQL = "SELECT * FROM SM_REPORTS,SM_REPORTS_PERM WHERE "
sSQL = sSQL & "SM_REPORTS_PERM.USER_ID = " & objSessionStr("UserID") & " AND SM_REPORTS.REPORT_ID = SM_REPORTS_PERM.REPORT_ID"
sSQL = sSQL & " ORDER BY SM_REPORTS.REPORT_NAME"

set r = CreateObject("DTGRocket")

henv = r.DB_InitEnvironment()
db = r.DB_OpenDatabase(henv, Application("SM_DSN"), 0)

rs = r.DB_CreateRecordset(db, sSQL, 3, 0)
while not r.DB_EOF(rs)
		r.DB_GetData rs, "REPORT_ID", lReportID
		r.DB_GetData rs, "REPORT_NAME", vName
		r.DB_GetData rs, "REPORT_DESC", vDesc
		'Safeway Atul - OSHA Privacy case
		r.DB_GetData rs, "PRIVACY_CASE", bPrivacyCase
		'Safeway Code End
		%>

		<% if i mod 2 = 0 then %>
		<tr class="rowlight1">
		<% else %>
		<tr class="rowdark1">
		<% end if %>
		<td width="30%">
		<!--Safeway Atul for OSHA Privacy case-->
		<a class="Blue" href="smi.asp?reportid=<%=lReportID%>&PrivacyCaseFlag=<%=bPrivacyCase%>"><% If "" & vName="" Then Response.Write "Report Nr. " & lReportID Else Response.Write vName %>
		</a></td>
		<td width="70%"><% = vDesc & "" %> </td>
		</tr>

		<%
			i = i + 1
	r.DB_MoveNext rs
wend
r.DB_CloseRecordset CInt(rs), 2
r.DB_CloseDatabase CInt(db)

r.DB_FreeEnvironment CLng(henv)

set r = Nothing
%>
		</TABLE>
	</DIV>
<font class="small">
You have access to the following reports which have been posted to the system.  
<br />
To begin running an available report, click on the desired report name.
<br>*** For information on extended reporting features and report troubleshooting please see the <a href="smfaq.asp">Frequently Asked Questions (FAQ)</a>.
</font>
</body>
</html>
