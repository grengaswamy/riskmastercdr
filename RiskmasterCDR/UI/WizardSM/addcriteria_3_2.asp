<% Option Explicit %>
<!-- #include file ="session.inc" -->
<!-- #include file ="generic.asp" -->
<html>
	<head>
		<link rel="stylesheet" href="RMNet.css" type="text/css"/>

<%
Dim objData, pageError
Set objData = Nothing

On Error Resume Next
Set objData = Server.CreateObject("InitSMList.CDataManager")
On Error Goto 0

If objData Is Nothing Then
	Call ShowError(pageError, "Call to CDataManager class failed.", True)
End If

objData.m_RMConnectStr = Application("SM_DSN")

Dim arrDateFormulae()
objData.GetDateFormulae arrDateFormulae

Set objData = Nothing

Dim sDateFormula, i, m

Err.Clear()
On Error Resume Next
m = -1
m = UBound(arrDateFormulae)
On Error Goto 0

If Err.number <> 9 And m <> "" And m >=0 Then
	For i = 0 To UBound(arrDateFormulae) 
		If i = 0 Then 
			sDateFormula = arrDateFormulae(0) 
		Else 
			sDateFormula = sDateFormula & "^^^^^" & arrDateFormulae(i) 
		End If 
	Next
End If

%>
<html>
	<head>
		<link rel="stylesheet" href="RMNet.css" type="text/css"/>
		<title>SMD - [Criteria]</title>
		<script language="JavaScript" SRC="SMD.js"></script>
		<script language="JavaScript">
			var btnClick;
			btnClick = ""
			var trackChange;
			trackChange = 1;
			var txtNull;
			var txtEnter;
			var pAttributes;
			var operatorList;
			operatorList = '';
			var connectorList;
			connectorList = '';
			var arrData = new Array;
			arrData.length = 0;
			var pOperator = new Array;
			pOperator.length = 0;
			var arrRelFormulaName = new Array;
			var arrRelFormulaValue = new Array;
			
			function or_click()
			{	
				var valFrom, valTo;
				
				if(trimIt(window.document.frmData.txtDtofevnt1.value) != '')
				{
					dtArrayFrom = window.document.frmData.txtDtofevnt1.value.split("/");
					dtValFrom = dtArrayFrom[2] + dtArrayFrom[0] + dtArrayFrom[1];
				}
				else
					dtValFrom = '';
					
				if(trimIt(window.document.frmData.txtDtofevnt2.value) != '')
				{
					dtArrayTo = window.document.frmData.txtDtofevnt2.value.split("/");
					dtValTo = dtArrayTo[2] + dtArrayTo[0] + dtArrayTo[1];
				}
				else
					dtValTo = '';

			   	valFrom = dtValFrom;
			   	valTo = dtValTo;
			   	
				//-- Code to handle change
				if(document.frmData.btnDelete.disabled == false && document.frmData.btnOr.disabled == false)
				{
					var tempArray = arrData[document.frmData.lstCriteria.selectedIndex].split("!!!!!");
					if((formatDate(tempArray[0]) == document.frmData.txtDtofevnt1.value) && (formatDate(tempArray[1]) == document.frmData.txtDtofevnt2.value) )
					{
						document.frmData.txtDtofevnt1.value = '';
						document.frmData.txtDtofevnt2.value = '';
						document.frmData.btnDelete.disabled = true;
						btnClick="OR";
						return;
					}
					else
					{
						check(1);
						btnClick="OR";
						return;
					}
				}
							
				if(document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == false &&  (document.frmData.txtDtofevnt1.value != '' || document.frmData.txtDtofevnt2.value != '') && trackChange == 1)
				{
					document.frmData.txtDtofevnt1.value = "";
					document.frmData.txtDtofevnt2.value = "";
					document.frmData.btnDelete.disabled = true;
				}
				
				if(document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)
					check(1);
				else
				{
					if((trackChange == 0) && (document.frmData.txtDtofevnt1.value == '' && document.frmData.txtDtofevnt2.value == ''))
					{
						arrData[arrData.length] = valFrom + "!!!!!" + valTo;
						check(0);
					}
					else if(trackChange == 0 && (document.frmData.txtDtofevnt1.value != '' || document.frmData.txtDtofevnt2.value != '') && document.frmData.btnDelete.disabled == false) //Selecting subquery criteria in edit mode.
						check(1);
					else if (document.frmData.txtDtofevnt1.value != '' || document.frmData.txtDtofevnt2.value != '')
					{
						arrData[arrData.length] = valFrom + "!!!!!" + valTo;
						check(0);
					}
				}

				document.frmData.txtDtofevnt1.value = "";
				document.frmData.txtDtofevnt2.value = "";
				document.frmData.btnChange.disabled = true;
				document.frmData.btnDelete.disabled = true;
				trackChange = 1;
				btnClick = "OR";
			}
	
			function Subquery()
			{
				trackChange = 0;
				if(btnClick == "OR")
					btnClick = "OR";
				else
					btnClick = "";	
			}
	
			function check(pAdd)
			{
				var txt, index, FieldName;
				var oOption,selConnector;
				FieldName = window.document.frmData.hdFieldName.value;
				var valFrom;
				var valTo;
				var oOption;	
				if(window.document.frmData.txtDtofevnt1.value != '')
				{
					dtArrayFrom = window.document.frmData.txtDtofevnt1.value.split("/");
					dtValFrom = dtArrayFrom[2] + dtArrayFrom[0] + dtArrayFrom[1];
				}
				else
					dtValFrom = '';
					
				if(window.document.frmData.txtDtofevnt2.value != '')
				{
					dtArrayTo = window.document.frmData.txtDtofevnt2.value.split("/");
					dtValTo = dtArrayTo[2] + dtArrayTo[0] + dtArrayTo[1];
				}
				else
					dtValTo = '';
			   	
			   	valFrom = dtValFrom;
			   	valTo = dtValTo;
				if(document.frmData.chkOLE.checked)
				{
					valFrom = 'OLECRIT';
					valTo = 'OLECRIT';
				}
			   
			   	valRange = "'" + valFrom + "'" + " AND " + "'" + valTo + "'";
				valExclude = "BETWEEN";
		
				if(document.frmData.lstCriteria.length==0 && (document.frmData.txtDtofevnt1.value!="" || document.frmData.txtDtofevnt2.value!="")) //If the value being entered is the 1st one and text box is not null.
				{
					if(pAdd == 0)
						txtEnter = btnClick + " " +"(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
					else
						txtEnter = "(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
				}
				else if((document.frmData.txtDtofevnt1.value == '' && document.frmData.txtDtofevnt2.value == '') && (trackChange == 0))
				{
					if(pAdd == 0)
						txtEnter = btnClick + " " +"(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
					else
						txtEnter = "(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";

					trackChange=1;
				}	
				else if(trackChange== 0 && (document.frmData.txtDtofevnt1.value != '' || document.frmData.txtDtofevnt2.value != '') && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == false  )//If the value being entered is not the 1st one and text box is null.
				{
					if(pAdd == 0)
						txtEnter = btnClick + " " +"(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
					else
						txtEnter = "(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
				
					trackChange=1;
				}
				else if(trackChange== 0 && (document.frmData.txtDtofevnt1.value == '' && document.frmData.txtDtofevnt2.value == '') && document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)//If the value being entered is not the 1st one and text box is null.
				{
					trackChange=1;
					if(pAdd == 0)
						txtEnter = btnClick + "(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
					else
						txtEnter = "(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
				}
				else if(trackChange == 1 && (document.frmData.txtDtofevnt1.value == '' && document.frmData.txtDtofevnt2.value == '') && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == true && document.frmData.chkOLE.checked == false) //If the text box value is null and check box value is not changed.
				{
					return;				
				}
				else if(trackChange == 1 && (document.frmData.txtDtofevnt1.value == '' && document.frmData.txtDtofevnt2.value == '') && document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)//If the text box value is null and check box value is not changed.
				{
					if(pAdd == 0)
						txtEnter = btnClick + " " + "(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
					else
						txtEnter = "(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
				}
				else
				{
					if(pAdd == 0)
						txtEnter = btnClick + " " + "(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
					else
						txtEnter = "(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
				}
	
				if(pAdd == 0) //-- Add mode
					add_item(txtEnter);
				else //-- Edit mode
				{	
					var selLength = document.frmData.lstCriteria.length;
					
					if(selLength == 0) //-- Need to add
					{
						if((document.frmData.txtDtofevnt1.value != "" || document.frmData.txtDtofevnt2.value != "" ) || trackChange == 0)
						{
							arrData[arrData.length] = valFrom + "!!!!!" + valTo;
						}
						add_item(txtEnter);
					}
					else
					{
						var selTxt = document.frmData.lstCriteria.options[document.frmData.lstCriteria.selectedIndex].text;
						if(selTxt.substring(0,2) == "OR")
							selConnector = "OR ";
						else
							selConnector = "";
						
						document.frmData.lstCriteria.options[document.frmData.lstCriteria.selectedIndex].text = selConnector + txtEnter;
						arrData[document.frmData.lstCriteria.selectedIndex] = valFrom + "!!!!!" + valTo;
					}
				}

				if(document.frmData.chkExclude.checked == true)
				{
					for(i=0;i<document.frmData.lstCriteria.length;i++)
					{
						var sCriteria = document.frmData.lstCriteria.options[i].text.substring(0,document.frmData.lstCriteria.options[i].text.indexOf("'"));
						iIndex = sCriteria.indexOf("NOT BETWEEN");
						if(iIndex == -1)
							document.frmData.lstCriteria.options[i].text = document.frmData.lstCriteria.options[i].text.replace("BETWEEN","NOT BETWEEN");
					}
					txtEnter = btnClick + "(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
				}
				else
				{
					for(i=0;i<document.frmData.lstCriteria.length;i++)
					{
						document.frmData.lstCriteria.options[i].text = document.frmData.lstCriteria.options[i].text.replace("NOT BETWEEN","BETWEEN");
					}
					txtEnter = btnClick + "(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
				}
			}
	
			function ok_click()
			{
				var valFrom, valTo, oOption;
				oOption = '';
				
				if(window.document.frmData.txtDtofevnt1.value != '')
				{
					dtArrayFrom = window.document.frmData.txtDtofevnt1.value.split("/");
					dtValFrom = dtArrayFrom[2] + dtArrayFrom[0] + dtArrayFrom[1];
				}
				else
					dtValFrom = '';

				if(window.document.frmData.txtDtofevnt2.value != '')
				{
					dtArrayTo = window.document.frmData.txtDtofevnt2.value.split("/");
					dtValTo = dtArrayTo[2] + dtArrayTo[0] + dtArrayTo[1];
				}
				else
					dtValTo = '';

			   	valFrom = dtValFrom;
			   	valTo = dtValTo;
				
				if(document.frmData.chkOLE.checked)
				{
					valFrom = 'OLECRIT';
					valTo = 'OLECRIT';
				}
				
				valRange = "'" + valFrom + "'" + " AND " + "'" + valTo + "'";
				
				if(document.frmData.chkExclude.checked == true)
					valExclude = " NOT BETWEEN ";
				else
					valExclude = " BETWEEN ";
				
				var rangeExclude;
				
				if(document.frmData.chkExclude.checked == true)
					rangeExclude = "excluderange";
				else
					rangeExclude = "range";		
				
				//-- Initialize
				connectorList = '';
				dataList = '';
				operatorList = 'BETWEEN';

				var enterKey = String.fromCharCode(13, 10);

				//-- Check for sub query
				if(document.frmData.chkSubquery.checked == true)
					hasDatadefcrit = 1;
				else
					hasDatadefcrit = 0;

				//-- Rajeev -- 03/25/2004 -- Check for hiding in RMNet
				if(document.frmData.chkHide.checked == true)
					hideInRMNet = 1;
				else
					hideInRMNet = 0;

				//-- Rajeev -- 03/25/2004 -- fill RMNet Label
				document.frmData.hdSelRMNetLabel.value = document.frmData.txtRMNetLabel.value;
				
				//-- Handling OLE Criteria(Name & Description)
				if(window.document.frmData.hdOLECritName.value == '')
					window.document.frmData.hdOLECritName.value = window.document.frmData.hdFieldName.value;
				if(window.document.frmData.hdOLECritDesc.value == '')
					window.document.frmData.hdOLECritDesc.value = window.document.frmData.hdFieldName.value;
				
				//-- fill pAttributes
				if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
				{
					if(window.opener.window.document.frmData.lstSelcriteria.length > 0)
						pAttributes = document.frmData.hdTableId.value + "|" + document.frmData.hdFieldId.value + "|" + rangeExclude + "|" + "and" + "|" + hasDatadefcrit + "|" + hideInRMNet + "|" + document.frmData.hdSelRMNetLabel.value;
					else
						pAttributes = document.frmData.hdTableId.value + "|" + document.frmData.hdFieldId.value + "|" + rangeExclude + "|" + "none" + "|" + hasDatadefcrit + "|" + hideInRMNet + "|" + document.frmData.hdSelRMNetLabel.value;
				}
				else
				{
					var tempArray = window.document.frmData.hdSelCriteriaItem.value.split("~~~~~");
					var arrAttribute = tempArray[7].split("|");
					
					pAttributes = document.frmData.hdTableId.value + "|" + document.frmData.hdFieldId.value + "|" + rangeExclude + "|" + arrAttribute[4] + "|" + hasDatadefcrit + "|" + hideInRMNet + "|" + document.frmData.hdSelRMNetLabel.value;
				}
				
				//-- No data case -- just close down
				if(document.frmData.lstCriteria.length == 0 && (document.frmData.txtDtofevnt1.value == '' && document.frmData.txtDtofevnt2.value == '') && trackChange == 0 )
				{	
					//-- Handle OLE Criteria	
					if(window.document.frmData.chkOLE.checked && window.document.frmData.hdSelCriteriaItem.value == '')
					{

						dataList = 'OLECRIT' + "!!!!!" + 'OLECRIT';
						connectorList = 'none';
						connectorList = connectorList + '|and';
						txtEnter = "(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
						if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
						{
							if(window.opener.window.document.frmData.lstSelcriteria.length > 0)
								txtEnter = "AND" + " " + txtEnter;
						}
						else
						{	
							if(window.document.frmData.hdSelCriteriaIndex.selectedIndex > 0)
								txtEnter = "AND" + " " + txtEnter;			
						}
		
						window.opener.criteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,operatorList,dataList,connectorList,document.frmData.hdFieldName.value,document.frmData.hdFieldType.value,document.frmData.hdDBTableId.value,pAttributes,(txtEnter+enterKey));			
						if(window.document.frmData.chkOLE.checked)
							window.opener.OLEcriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,window.document.frmData.hdOLECritName.value +'^^^^^'+window.document.frmData.hdOLECritDesc.value ,'', true);
						else
							window.opener.OLEcriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,window.document.frmData.hdOLECritName.value +'^^^^^'+window.document.frmData.hdOLECritDesc.value ,'', false);
						
						if(window.document.frmData.cboDay1.selectedIndex > 1 && window.document.frmData.cboDay2.selectedIndex < 2)
							window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(arrRelFormulaValue[document.frmData.cboDay1.selectedIndex-2]) + "," + "",'', true);
						else if(window.document.frmData.cboDay1.selectedIndex < 2 && window.document.frmData.cboDay2.selectedIndex > 1)
							window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, "" + "," + unescape(arrRelFormulaValue[document.frmData.cboDay2.selectedIndex-2]),'', true);
						else if(window.document.frmData.cboDay1.selectedIndex > 1 && window.document.frmData.cboDay2.selectedIndex > 1)
							window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(arrRelFormulaValue[document.frmData.cboDay1.selectedIndex-2]) + "," + unescape(arrRelFormulaValue[document.frmData.cboDay2.selectedIndex-2]),'', true);
						else
							window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, "" + "," + "",'', true);
						
						window.opener.displayCriteria();
						window.opener.displayGlobalCriteria();
						window.opener.changeFieldName(window.document.frmData.hdSelCriteriaIndex.value);
						window.close();
						return;
					}
					else if(window.document.frmData.chkOLE.checked && window.document.frmData.hdSelCriteriaItem.value != '')
					{
						window.close();
						return;
					}
				
					//-- Connector list
					if(btnClick == '')
						connectorList = 'none';
					else
						connectorList = btnClick;

					connectorList = connectorList + '|and';
					
					arrData[arrData.length] = valFrom + "!!!!!" + valTo;
					check(0);

					//-- fill datalist -- only one data item
					dataList = arrData[0];	
					
					//-- maintain outer main connector
					txtEnter = "(" + txtEnter + ")";
					
					if(document.frmData.chkExclude.checked == true)
						txtEnter = btnClick + "(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
					else
						txtEnter = btnClick + "(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
				
					if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
					{
						if(window.opener.window.document.frmData.lstSelcriteria.length > 0)
						{
							if(txtEnter != '')
								txtEnter = "AND" + " " + "(" + txtEnter + ")";
						}
						else
							txtEnter = "(" + txtEnter + ")";
					}
					else
					{
						if(window.document.frmData.hdSelCriteriaIndex.value > 0)
						{
							if(txtEnter != '')
								txtEnter = "AND" + " " + txtEnter;
						}
					}
					
					window.opener.criteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,operatorList,dataList,connectorList,document.frmData.hdFieldName.value,document.frmData.hdFieldType.value,document.frmData.hdDBTableId.value,pAttributes,(txtEnter+enterKey));			
					if(window.document.frmData.chkOLE.checked)
						window.opener.OLEcriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,window.document.frmData.hdOLECritName.value +'^^^^^'+window.document.frmData.hdOLECritDesc.value ,'', true);
					else
						window.opener.OLEcriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,window.document.frmData.hdOLECritName.value +'^^^^^'+window.document.frmData.hdOLECritDesc.value ,'', false);
					//-- Handling Relative Date
					if(window.document.frmData.cboDay1.selectedIndex > 1 && window.document.frmData.cboDay2.selectedIndex < 2)
						window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(arrRelFormulaValue[document.frmData.cboDay1.selectedIndex-2]) + "," + "",'', true);
					else if(window.document.frmData.cboDay1.selectedIndex < 2 && window.document.frmData.cboDay2.selectedIndex > 1)
						window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, "" + "," + unescape(arrRelFormulaValue[document.frmData.cboDay2.selectedIndex-2]),'', true);
					else if(window.document.frmData.cboDay1.selectedIndex > 1 && window.document.frmData.cboDay2.selectedIndex > 1)
						window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(arrRelFormulaValue[document.frmData.cboDay1.selectedIndex-2]) + "," + unescape(arrRelFormulaValue[document.frmData.cboDay2.selectedIndex-2]),'', true);
					else
						window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, "" + "," + "",'', true);
				
					window.opener.displayCriteria();
					window.opener.displayGlobalCriteria();
					window.opener.changeFieldName(window.document.frmData.hdSelCriteriaIndex.value);
					window.opener.ButtonState();
						
					window.close();
					return;		
				
				}
				else if(document.frmData.lstCriteria.length == 0 && (document.frmData.txtDtofevnt1.value == '' && document.frmData.txtDtofevnt2.value == '') && trackChange == 1)
				{	
					if(window.document.frmData.chkOLE.checked)
					{
						dataList = 'OLECRIT' + "!!!!!" + 'OLECRIT';
						connectorList = 'none';
						connectorList = connectorList + '|and';
						txtEnter = "(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
					
						if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
						{
							if(window.opener.window.document.frmData.lstSelcriteria.length > 0)
								txtEnter = "AND" + " " + txtEnter;
						}
						else
						{	
							if(window.document.frmData.hdSelCriteriaIndex.selectedIndex > 0)
								txtEnter = "AND" + " " + txtEnter;			
						}
		
						window.opener.criteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,operatorList,dataList,connectorList,document.frmData.hdFieldName.value,document.frmData.hdFieldType.value,document.frmData.hdDBTableId.value,pAttributes,(txtEnter+enterKey));			
						if(window.document.frmData.chkOLE.checked)
							window.opener.OLEcriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,window.document.frmData.hdOLECritName.value +'^^^^^'+window.document.frmData.hdOLECritDesc.value ,'', true);
						else
							window.opener.OLEcriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,window.document.frmData.hdOLECritName.value +'^^^^^'+window.document.frmData.hdOLECritDesc.value ,'', false);
						//-- Handling Relative Date
						if(window.document.frmData.cboDay1.selectedIndex > 1 && window.document.frmData.cboDay2.selectedIndex < 2)
							window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(arrRelFormulaValue[document.frmData.cboDay1.selectedIndex-2]) + "," + "",'', true);
						else if(window.document.frmData.cboDay1.selectedIndex < 2 && window.document.frmData.cboDay2.selectedIndex > 1)
							window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, "" + "," + unescape(arrRelFormulaValue[document.frmData.cboDay2.selectedIndex-2]),'', true);
						else if(window.document.frmData.cboDay1.selectedIndex > 1 && window.document.frmData.cboDay2.selectedIndex > 1)
							window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(arrRelFormulaValue[document.frmData.cboDay1.selectedIndex-2]) + "," + unescape(arrRelFormulaValue[document.frmData.cboDay2.selectedIndex-2]),'', true);
						else
							window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, "" + "," + "",'', true);
						window.opener.displayCriteria();
						window.opener.displayGlobalCriteria();
						window.opener.changeFieldName(window.document.frmData.hdSelCriteriaIndex.value);
				
						window.close();
						return;
					}
					else
					{
						if(document.frmData.hdSelCriteriaIndex.value == -1)
						{
							window.close();
							return;
						}
						else
						{
							window.opener.deleteClick();
							window.close();
							return;
						}						
						//window.close();
						//return;
					}
				}
				
				if(document.frmData.lstCriteria.length == 0 && (document.frmData.txtDtofevnt1.value != '' || document.frmData.txtDtofevnt2.value != ''))
				{
					//-- Add mode -- Edit not applicable
					//-- Handle the OLE criteria
					if(window.document.frmData.chkOLE.checked)
					{
						dataList = 'OLECRIT' + "!!!!!" + 'OLECRIT';
						connectorList = 'none';
						connectorList = connectorList + '|and';
						
						txtEnter = "(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
						if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
						{
							if(window.opener.window.document.frmData.lstSelcriteria.length > 0)
								txtEnter = "AND" + " " + txtEnter;
						}
						else
						{	
							if(window.document.frmData.hdSelCriteriaIndex.selectedIndex > 0)
								txtEnter = "AND" + " " + txtEnter;			
						}
		
						window.opener.criteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,operatorList,dataList,connectorList,document.frmData.hdFieldName.value,document.frmData.hdFieldType.value,document.frmData.hdDBTableId.value,pAttributes,(txtEnter+enterKey));			
						if(window.document.frmData.chkOLE.checked)
							window.opener.OLEcriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,window.document.frmData.hdOLECritName.value +'^^^^^'+window.document.frmData.hdOLECritDesc.value ,'', true);
						else
							window.opener.OLEcriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,window.document.frmData.hdOLECritName.value +'^^^^^'+window.document.frmData.hdOLECritDesc.value ,'', false);
						//-- Handling Relative Date
						if(window.document.frmData.cboDay1.selectedIndex > 1 && window.document.frmData.cboDay2.selectedIndex < 2)
							window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(arrRelFormulaValue[document.frmData.cboDay1.selectedIndex-2]) + "," + "",'', true);
						else if(window.document.frmData.cboDay1.selectedIndex < 2 && window.document.frmData.cboDay2.selectedIndex > 1)
							window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, "" + "," + unescape(arrRelFormulaValue[document.frmData.cboDay2.selectedIndex-2]),'', true);
						else if(window.document.frmData.cboDay1.selectedIndex > 1 && window.document.frmData.cboDay2.selectedIndex > 1)
							window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(arrRelFormulaValue[document.frmData.cboDay1.selectedIndex-2]) + "," + unescape(arrRelFormulaValue[document.frmData.cboDay2.selectedIndex-2]),'', true);
						else
							window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, "" + "," + "",'', true);
						window.opener.displayCriteria();
						window.opener.displayGlobalCriteria();
						window.opener.changeFieldName(window.document.frmData.hdSelCriteriaIndex.value);
						window.close();
						return;
					}

					//-- Connector list
					if(btnClick == '')
						connectorList = 'none';
					else
						connectorList = btnClick;

					connectorList = connectorList + '|and';

					if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
					{
						arrData[arrData.length] = valFrom + "!!!!!" + valTo;
						check(0);
					}
					else
					{
						check(1);
					}
						
					//-- fill datalist -- only one data item
					dataList = arrData[0];
	
					//-- maintain outer main connector
					txtEnter = "(" + txtEnter + ")";
					
					if(document.frmData.chkExclude.checked == true)
						txtEnter = btnClick + "(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
					else
						txtEnter = btnClick + "(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
					
					if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
					{
						if(window.opener.window.document.frmData.lstSelcriteria.length > 0)
							txtEnter = "AND" + " " + "(" + txtEnter + ")";
						else
							txtEnter = "(" + txtEnter + ")";
					}
					else
					{
						if(window.document.frmData.hdSelCriteriaIndex.value > 0)
							txtEnter = "AND" + " " + txtEnter;
					}
					
					window.opener.criteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,operatorList,dataList,connectorList,document.frmData.hdFieldName.value,document.frmData.hdFieldType.value,document.frmData.hdDBTableId.value,pAttributes,(txtEnter+enterKey));			
					if(window.document.frmData.chkOLE.checked)
						window.opener.OLEcriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,window.document.frmData.hdOLECritName.value +'^^^^^'+window.document.frmData.hdOLECritDesc.value ,'', true);
					else
						window.opener.OLEcriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,window.document.frmData.hdOLECritName.value +'^^^^^'+window.document.frmData.hdOLECritDesc.value ,'', false);
					//-- Handling Relative Date
					if(window.document.frmData.cboDay1.selectedIndex > 1 && window.document.frmData.cboDay2.selectedIndex < 2)
						window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(arrRelFormulaValue[document.frmData.cboDay1.selectedIndex-2]) + "," + "",'', true);
					else if(window.document.frmData.cboDay1.selectedIndex < 2 && window.document.frmData.cboDay2.selectedIndex > 1)
						window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, "" + "," + unescape(arrRelFormulaValue[document.frmData.cboDay2.selectedIndex-2]),'', true);
					else if(window.document.frmData.cboDay1.selectedIndex > 1 && window.document.frmData.cboDay2.selectedIndex > 1)
						window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(arrRelFormulaValue[document.frmData.cboDay1.selectedIndex-2]) + "," + unescape(arrRelFormulaValue[document.frmData.cboDay2.selectedIndex-2]),'', true);
					else
						window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, "" + "," + "",'', true);
					window.opener.displayCriteria();
					window.opener.displayGlobalCriteria();
					window.opener.changeFieldName(window.document.frmData.hdSelCriteriaIndex.value);
					window.opener.ButtonState();
						
					window.close();
					return;		

				}
				else if((document.frmData.lstCriteria.length > 0) && (document.frmData.txtDtofevnt1.value != '' || document.frmData.txtDtofevnt2.value != '') && document.frmData.btnDelete.disabled == true && document.frmData.btnChange.disabled == true)
				{
					if(document.frmData.chkOLE.checked)
					{
						for(i=0;i<document.frmData.lstCriteria.length;i++)
						{
							if(document.frmData.lstCriteria.options[i].text.substring(0,2) == "OR")
								connectorList = connectorList + "," + "or|and";
							else
								connectorList = connectorList + "," + "none|and";
						}
	
						//-- append AND/OR as per the AND/OR button buffer
						connectorList = connectorList + "," + btnClick + '|and';
				
						//-- seperate out initial comma
						connectorList = connectorList.substring(1,connectorList.length);
				
				
						//arrData[arrData.length] = valFrom + "!!!!!" + valTo;
						
						//check(1);
						
						if(window.document.frmData.chkOLE.checked == false)
						{
							check(1);
						}

							
						for(i=0;i<arrData.length;i++)
							dataList = dataList + "^^^^^" + arrData[i];	

						//-- seperate out initial comma
						dataList = dataList.substring(5,dataList.length);

						oOption = '';
						
						
						for(i=0;i<document.frmData.lstCriteria.length;i++)
							oOption = oOption + document.frmData.lstCriteria.options[i].text;
			
						//-- maintain outer main connector
						if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
						{
							if(window.opener.window.document.frmData.lstSelcriteria.length > 0)
								oOption = "AND" + " ( " + oOption + ")";
							else
								oOption = "(" + oOption + ")";
						}
						else
						{
							if(window.document.frmData.hdSelCriteriaIndex.value > 0)
								oOption = "AND" + " ( " + oOption + ")";
							else
								oOption = "(" + oOption + ")";
						}
						//---------------------
						if(window.document.frmData.chkOLE.checked && document.frmData.hdSelCriteriaItem.value == '')
						{
							dataList = dataList + "^^^^^" + 'OLECRIT' + "!!!!!" + 'OLECRIT';
								
							//-- append AND/OR as per the AND/OR button buffer
							connectorList = connectorList + "," + btnClick + "|" + "and";
							//connectorList = connectorList + "," + btnClick;
							oOption = oOption + " " + btnClick + " (" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
						}
			
						else if(window.document.frmData.chkOLE.checked && document.frmData.hdSelCriteriaItem.value != '')
							check(1);
						//---------------------	
							
						window.opener.criteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,operatorList,dataList,connectorList,document.frmData.hdFieldName.value,document.frmData.hdFieldType.value,document.frmData.hdDBTableId.value,pAttributes,(oOption+enterKey));			
						if(window.document.frmData.chkOLE.checked)
							window.opener.OLEcriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,window.document.frmData.hdOLECritName.value +'^^^^^'+window.document.frmData.hdOLECritDesc.value ,'', true);
						else
							window.opener.OLEcriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,window.document.frmData.hdOLECritName.value +'^^^^^'+window.document.frmData.hdOLECritDesc.value ,'', false);
						//-- Handling Relative Date
						if(window.document.frmData.cboDay1.selectedIndex > 1 && window.document.frmData.cboDay2.selectedIndex < 2)
							window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(arrRelFormulaValue[document.frmData.cboDay1.selectedIndex-2]) + "," + "",'', true);
						else if(window.document.frmData.cboDay1.selectedIndex < 2 && window.document.frmData.cboDay2.selectedIndex > 1)
							window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, "" + "," + unescape(arrRelFormulaValue[document.frmData.cboDay2.selectedIndex-2]),'', true);
						else if(window.document.frmData.cboDay1.selectedIndex > 1 && window.document.frmData.cboDay2.selectedIndex > 1)
							window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(arrRelFormulaValue[document.frmData.cboDay1.selectedIndex-2]) + "," + unescape(arrRelFormulaValue[document.frmData.cboDay2.selectedIndex-2]),'', true);
						else
							window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, "" + "," + "",'', true);
						window.opener.displayCriteria();
						window.opener.displayGlobalCriteria();
						window.opener.changeFieldName(window.document.frmData.hdSelCriteriaIndex.value);
						window.close();
						return;				
					}				
					
					//-- Add/Edit mode -- extended criteria will be appended the criteria of selected item in list box
					//-- fill connectorlist
					for(i=0;i<document.frmData.lstCriteria.length;i++)
					{
						if(document.frmData.lstCriteria.options[i].text.substring(0,2) == "OR")
							connectorList = connectorList + "," + "or|and";
						else
							connectorList = connectorList + "," + "none|and";
					}
					
					//-- append AND/OR as per the AND/OR button buffer
					connectorList = connectorList + "," + btnClick + '|and';
					
					//-- seperate out initial comma
					connectorList = connectorList.substring(1,connectorList.length);

					//-- fill datalist

					arrData[arrData.length] = valFrom + "!!!!!" + valTo;
					check(0);
					
					for(i=0;i<arrData.length;i++)
						dataList = dataList + "^^^^^" + arrData[i];	

					//-- seperate out initial comma
					dataList = dataList.substring(5,dataList.length);

					//-- build combined critera to show on parent window's list box
					for(i=0;i<document.frmData.lstCriteria.length;i++)
						oOption = oOption + document.frmData.lstCriteria.options[i].text;

				}
				else if((document.frmData.lstCriteria.length > 0 && (document.frmData.txtDtofevnt1.value != '' || document.frmData.txtDtofevnt2.value != '') && document.frmData.btnDelete.disabled == false && document.frmData.btnChange.disabled == true))
				{
					//-- fill connectorlist
					for(i=0;i<document.frmData.lstCriteria.length;i++)
					{
						if(document.frmData.lstCriteria.options[i].text.substring(0,2) == "OR")
							connectorList = connectorList + "," + "or|and";
						else
							connectorList = connectorList + "," + "none|and";
					}
					//-- seperate out initial comma
					connectorList = connectorList.substring(1,connectorList.length);

					//-- fill datalist
					check(1);
					for(i=0;i<arrData.length;i++)
						dataList = dataList + "^^^^^" + arrData[i];	

					//-- seperate out initial comma
					dataList = dataList.substring(5,dataList.length);
					
					//-- build combined critera to show on parent window's list box
					for(i=0;i<document.frmData.lstCriteria.length;i++)
						oOption = oOption + document.frmData.lstCriteria.options[i].text;

				}		
				else if(document.frmData.lstCriteria.length > 0 && (document.frmData.txtDtofevnt1.value != '' || document.frmData.txtDtofevnt2.value != '') && document.frmData.btnDelete.disabled == false && document.frmData.btnChange.disabled == false)
				{
					//-- Add/Edit mode -- extended criteria will be appended the criteria of selected item in list box
					
					//-- fill connectorlist
					for(i=0;i<document.frmData.lstCriteria.length;i++)
					{
						if(document.frmData.lstCriteria.options[i].text.substring(0,2) == "OR")
							connectorList = connectorList + "," + "or|and";
						else
							connectorList = connectorList + "," + "none|and";
					}
					//-- seperate out initial comma
					connectorList = connectorList.substring(1,connectorList.length);

					//-- fill datalist
					check(1);
					
					for(i=0;i<arrData.length;i++)
						dataList = dataList + "^^^^^" + arrData[i];	

					//-- seperate out initial comma
					dataList = dataList.substring(5,dataList.length);

					//-- build combined critera to show on parent window's list box
					for(i=0;i<document.frmData.lstCriteria.length;i++)
						oOption = oOption + document.frmData.lstCriteria.options[i].text;

				}
				else if(document.frmData.lstCriteria.length > 0 && (document.frmData.txtDtofevnt1.value != '' || document.frmData.txtDtofevnt2.value != '') && document.frmData.btnDelete.disabled == true && document.frmData.btnChange.disabled == false)
				{
					//-- Add/Edit mode -- extended criteria will be appended the criteria of selected item in list box
					//-- fill connectorlist
					for(i=0;i<document.frmData.lstCriteria.length;i++)
					{
						if(document.frmData.lstCriteria.options[i].text.substring(0,2) == "OR")
							connectorList = connectorList + "," + "or|and";
						else
							connectorList = connectorList + "," + "none|and";
					}
					//-- seperate out initial comma
					connectorList = connectorList.substring(1,connectorList.length);

					check(1);
					
					for(i=0;i<arrData.length;i++)
						dataList = dataList + "^^^^^" + arrData[i];	

					//-- seperate out initial comma
					dataList = dataList.substring(5,dataList.length);
					

					//-- build combined critera to show on parent window's list box
					for(i=0;i<document.frmData.lstCriteria.length;i++)
						oOption = oOption + document.frmData.lstCriteria.options[i].text;

				}		
				else if(document.frmData.lstCriteria.length > 0 && (document.frmData.txtDtofevnt1.value == '' && document.frmData.txtDtofevnt2.value == '') && trackChange == 0)
				{
					//-- fill connectorlist
					for(i=0;i<document.frmData.lstCriteria.length;i++)
					{
						if(document.frmData.lstCriteria.options[i].text.substring(0,2) == "OR")
							connectorList = connectorList + "," + "or|and";
						else
							connectorList = connectorList + "," + "none|and";
					}
					//-- append AND/OR as per the AND/OR button buffer
					connectorList = connectorList + "," + btnClick + '|and';
					
					//-- seperate out initial comma
					connectorList = connectorList.substring(1,connectorList.length);

					if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
					{
						//-- fill datalist
						arrData[arrData.length] = valFrom + "!!!!!" + valTo;
						check(0);
					}
					else
					{
						//-- fill datalist
						check(1);			
					}			
					for(i=0;i<arrData.length;i++)
						dataList = dataList + "^^^^^" + arrData[i];	

					//-- seperate out initial comma
					dataList = dataList.substring(5,dataList.length);

					//-- build combined critera to show on parent window's list box
					for(i=0;i<document.frmData.lstCriteria.length;i++)
						oOption = oOption + document.frmData.lstCriteria.options[i].text;
						
					//-- handle OLE Criteria
					if(window.document.frmData.chkOLE.checked && document.frmData.hdSelCriteriaItem.value == '')
					{
						dataList = dataList + "^^^^^" + 'OLECRIT' + "!!!!!" + 'OLECRIT';
							
						//-- append AND/OR as per the AND/OR button buffer
						//connectorList = connectorList + "," + btnClick;
						connectorList = connectorList + "," + btnClick + "|" + "and";
						oOption = oOption + " " + btnClick + " (" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
					}
					else if(window.document.frmData.chkOLE.checked && document.frmData.hdSelCriteriaItem.value != '')
						check(1);
				}				
				else if(document.frmData.lstCriteria.length > 0 && (document.frmData.txtDtofevnt1.value == '' && document.frmData.txtDtofevnt2.value == '') && trackChange == 1)
				{
					//-- fill connectorlist
					for(i=0;i<document.frmData.lstCriteria.length;i++)
					{
						if(document.frmData.lstCriteria.options[i].text.substring(0,2) == "OR")
							connectorList = connectorList + "," + "or|and";
						else
							connectorList = connectorList + "," + "none|and";
					}
					//-- seperate out initial comma
					connectorList = connectorList.substring(1,connectorList.length);

					//-- fill datalist
					//check(1);
					if(window.document.frmData.chkOLE.checked == false)
					{
						check(1);
					}
			
					
					for(i=0;i<arrData.length;i++)
						dataList = dataList + "^^^^^" + arrData[i];	

					//-- seperate out initial comma
					dataList = dataList.substring(5,dataList.length);
					
					//-- build combined critera to show on parent window's list box
					for(i=0;i<document.frmData.lstCriteria.length;i++)
						oOption = oOption + document.frmData.lstCriteria.options[i].text;
					//-- handle OLE Criteria
					if(window.document.frmData.chkOLE.checked && document.frmData.hdSelCriteriaItem.value == '')
					{
						dataList = dataList + "^^^^^" + 'OLECRIT' + "!!!!!" + 'OLECRIT';
							
						//-- append AND/OR as per the AND/OR button buffer
						connectorList = connectorList + "," + btnClick + "|" + "and";
						//connectorList = connectorList + "," + btnClick;
						oOption = oOption + " " + btnClick + " (" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
					}
			
					else if(window.document.frmData.chkOLE.checked && document.frmData.hdSelCriteriaItem.value != '')
						check(1);

				}
							
				//-- append outer parenthesis 
				if((trimIt(oOption).substring(0,3) == "AND" && trimIt((oOption).substring(3,trimIt(oOption).length)).indexOf("AND") > 0) || (trimIt(oOption).substring(0,3) == "AND" && trimIt((oOption).substring(3,trimIt(oOption).length)).indexOf("OR") > 0))
					oOption = "(" +  oOption + ")" ;
				else if((trimIt(oOption).substring(0,2) == "OR" && trimIt((oOption).substring(2,trimIt(oOption).length)).indexOf("OR") > 0) || (trimIt(oOption).substring(0,2) == "AND" && trimIt((oOption).substring(2,trimIt(oOption).length)).indexOf("AND") > 0))
					oOption = "(" +  oOption + ")" ;
				else if((trimIt(oOption).substring(0,3) != "AND" && trimIt(oOption).indexOf("AND") > 0) || (trimIt(oOption).substring(0,3) != "AND" && trimIt(oOption).indexOf("OR") > 0 ))
					oOption = "(" + oOption + ")";
				else if((trimIt(oOption).substring(0,2) != "OR" && trimIt(oOption).indexOf("AND")) > 0 || (trimIt(oOption).substring(0,2) != "OR" && trimIt(oOption).indexOf("OR") > 0 ))
					oOption = "(" + oOption + ")";

				if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
				{
					if(window.opener.window.document.frmData.lstSelcriteria.length > 0)
						oOption = "AND" + " " + oOption;
				}
				else
				{	
					if(window.document.frmData.hdSelCriteriaIndex.selectedIndex > 0)
						oOption = "AND" + " " + oOption;			
				}
				
				//-- code to handle the outer connector while editing
				if(document.frmData.hdSelCriteriaIndex.value != -1)		
				{	
					var arrPAttributes = pAttributes.split("|");
					
					if(arrPAttributes[3] != "none")
						oOption = arrPAttributes[3].toUpperCase() + " " + oOption;
				}
				
				window.opener.criteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,operatorList,dataList,connectorList,document.frmData.hdFieldName.value,document.frmData.hdFieldType.value,document.frmData.hdDBTableId.value,pAttributes,(oOption+enterKey));			
				if(window.document.frmData.chkOLE.checked)
					window.opener.OLEcriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,window.document.frmData.hdOLECritName.value +'^^^^^'+window.document.frmData.hdOLECritDesc.value ,'', true);
				else
					window.opener.OLEcriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,window.document.frmData.hdOLECritName.value +'^^^^^'+window.document.frmData.hdOLECritDesc.value ,'', false);
				//-- Handling Relative Date
				if(window.document.frmData.cboDay1.selectedIndex > 1 && window.document.frmData.cboDay2.selectedIndex < 2)
					window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(arrRelFormulaValue[document.frmData.cboDay1.selectedIndex-2]) + "," + "",'', true);
				else if(window.document.frmData.cboDay1.selectedIndex < 2 && window.document.frmData.cboDay2.selectedIndex > 1)
					window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, "" + "," + unescape(arrRelFormulaValue[document.frmData.cboDay2.selectedIndex-2]),'', true);
				else if(window.document.frmData.cboDay1.selectedIndex > 1 && window.document.frmData.cboDay2.selectedIndex > 1)
					window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(arrRelFormulaValue[document.frmData.cboDay1.selectedIndex-2]) + "," + unescape(arrRelFormulaValue[document.frmData.cboDay2.selectedIndex-2]),'', true);
				else
					window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, "" + "," + "",'', true);
				window.opener.displayCriteria();
				window.opener.displayGlobalCriteria();
				window.opener.changeFieldName(window.document.frmData.hdSelCriteriaIndex.value);
				window.opener.ButtonState();

				if(document.frmData.hdSelCriteriaIndex.value != -1)		
					window.opener.window.document.frmData.lstSelcriteria.selectedIndex = window.document.frmData.hdSelCriteriaIndex.value;
					
				window.close();
			}		
	
			function add_item(txt)
			{
				var sNewItem;
				sNewItem = new Option(txt);
				if (navigator.appName == "Netscape")
					window.document.frmData.lstCriteria.add(sNewItem, null);
				else
					window.document.frmData.lstCriteria.add(sNewItem);
			}
			
			function add_item_relative_date1(txt)
			{
				var sNewItem;
				sNewItem = new Option(txt);
				if (navigator.appName == "Netscape")
				{
					window.document.frmData.cboDay1.add(sNewItem, null);
				}	
				else
				{
					window.document.frmData.cboDay1.add(sNewItem);
				}
			}

			function add_item_relative_date2(txt)
			{
				var sNewItem;
				sNewItem = new Option(txt);
				if (navigator.appName == "Netscape")
				{
					window.document.frmData.cboDay2.add(sNewItem, null);
				}	
				else
				{
					window.document.frmData.cboDay2.add(sNewItem);
				}
			}
		
			function delete_click()
			{
				var txt;
				if(document.frmData.lstCriteria.length>1)
				{
					txt=document.frmData.lstCriteria.options[1].text
					pattern="(";
					index=txt.indexOf(pattern);

				}

				if(document.frmData.lstCriteria.selectedIndex != -1)
				{
					if(document.frmData.lstCriteria.selectedIndex=="0" && document.frmData.lstCriteria.length>1)//If 1st element is being deleted and there more than 1 elements.
					{
						arrData[0] = null;
						document.frmData.lstCriteria.options[0]=null;
						document.frmData.lstCriteria.options[0].text=txt.substring(index,txt.length);
						document.frmData.lstCriteria.selectedIndex= 0;
					}
					else if(document.frmData.lstCriteria.selectedIndex=="0" && document.frmData.lstCriteria.length == 1)//If 1st element is being deleted and its the only element.
					{
						arrData[0] = null;
						document.frmData.lstCriteria.options[0]=null;
					}
					else if(document.frmData.lstCriteria.selectedIndex == document.frmData.lstCriteria.length-1)
					{
						arrData[document.frmData.lstCriteria.selectedIndex] = null;
						document.frmData.lstCriteria.options[document.frmData.lstCriteria.selectedIndex]=null;	
						document.frmData.lstCriteria.selectedIndex=document.frmData.lstCriteria.length-1;
					}
					else
					{
						var selIndx = document.frmData.lstCriteria.selectedIndex
						arrData[document.frmData.lstCriteria.selectedIndex] = null;
						document.frmData.lstCriteria.options[document.frmData.lstCriteria.selectedIndex]=null;
						document.frmData.lstCriteria.selectedIndex = selIndx; 
					}

					adjust_pArray(arrData);

					if(document.frmData.lstCriteria.selectedIndex == -1)
					{
						document.frmData.btnDelete.disabled = true;
						document.frmData.txtDtofevnt1.value = '';
						document.frmData.txtDtofevnt2.value = '';
					}
					else
					{
						var splitData = arrData[document.frmData.lstCriteria.selectedIndex].split('!!!!!');
						document.frmData.txtDtofevnt1.value = formatDate(splitData[0]);
						document.frmData.txtDtofevnt2.value = formatDate(splitData[1]);
					}
					
				}
				else
				{
					//-- Check if the selected item that is being edited has been deleted or not.
					if(document.frmData.lstCriteria.length == 0)
					{
						document.frmData.hdSelCriteriaItem.value = '';
						window.opener.deleteClick();
						document.frmData.hdSelCriteriaIndex.value = -1;
					}
					return;
				}
				//-- Check if the selected item that is being edited has been deleted or not.
				if(document.frmData.lstCriteria.length == 0)
				{
					document.frmData.hdSelCriteriaItem.value = '';
					window.opener.deleteClick();
					document.frmData.hdSelCriteriaIndex.value = -1;
				}
				return;			
			}
	
			function change_click()
			{
				check(1);
			}
	
			function window_onLoad()
			{
				// Start Naresh MITS 7836 09/11/2006
				var objCheckBox=eval("document.frmData.chkHide");
				objCheckBox.style.display='none';
				var objTextBox=eval("document.frmData.txtRMNetLabel");
				objTextBox.style.display='none';
				// End Naresh MITS 7836 09/11/2006
				//-- Handling Relative Date
				var arrRelDate = window.document.frmData.hdRelativeDate.value.split("^^^^^");	
				if (arrRelDate.length > 0 && window.document.frmData.hdRelativeDate.value != -1 && window.document.frmData.hdRelativeDate.value != "")
				{
					for(i=0;i<arrRelDate.length;i++)
					{
						var arrPos = FindCharacter(arrRelDate[i], '|', 2).split(",");
						var pos1 = arrPos[0];
						var pos2 = arrPos[1];
						arrRelFormulaName[i] = arrRelDate[i].substr(pos1,pos2-pos1-1);
						arrRelFormulaValue[i] = arrRelDate[i].substr(pos2,arrRelDate[i].length-pos2);
						add_item_relative_date1(arrRelFormulaName[i])			
						add_item_relative_date2(arrRelFormulaName[i])			
					}
				}
			
				if(window.document.frmData.hdSelSubQuery.value == 1)
					window.document.frmData.chkSubquery.checked = true;

				if(window.document.frmData.hdSelHide.value == 1)
					window.document.frmData.chkHide.checked = true;

				if(window.document.frmData.hdSelRMNetLabel.value != '')
					window.document.frmData.txtRMNetLabel.value = window.document.frmData.hdSelRMNetLabel.value;
					
				//-- Handling OLE Criteria(Name & Description)
				if(window.document.frmData.hdSelOLECriteriaItem.value == '' || window.document.frmData.hdSelOLECriteriaItem.value == '`````')
				{
					if(window.document.frmData.hdOLECritName.value == '')
						window.document.frmData.hdOLECritName.value = window.document.frmData.hdFieldName.value;
					if(window.document.frmData.hdOLECritDesc.value == '')
						window.document.frmData.hdOLECritDesc.value = window.document.frmData.hdFieldName.value;
				}
				else
				{
					var tempArrayOLE,tempDataArray;
					tempArrayOLE = window.document.frmData.hdSelOLECriteriaItem.value.split("~~~~~");
					tempDataArray = tempArrayOLE[1].split("^^^^^");
					window.document.frmData.hdOLECritName.value = tempDataArray[0];
					window.document.frmData.hdOLECritDesc.value = tempDataArray[1];
				}
				
				if(document.frmData.hdSelCriteriaItem.value != '')
				{
					formatCriteria();
					var arrPCriteria, arrOperators;;
					arrPCriteria = document.frmData.hdSelCriteriaItem.value.split("~~~~~");
					
					operatorList = arrPCriteria[1]; //-- BETWEEN
					pAttributes = arrPCriteria[7];
					arrData = arrPCriteria[2].split("^^^^^");
					var arrTemp = arrData[0].split('!!!!!')
					document.frmData.txtDtofevnt1.value =  formatDate(arrTemp[0]);
					document.frmData.txtDtofevnt2.value = formatDate(arrTemp[1]);
					
					if(document.frmData.hdSelCriteriaItem.value.indexOf('OLECRIT') > -1)
					{
						document.frmData.chkOLE.checked = true;
						document.frmData.btnDelete.disabled = true;
						document.frmData.txtDtofevnt1.value = '';
						document.frmData.txtDtofevnt2.value = '';
						OLEEnable();
					}
				}
				
				if(window.document.frmData.hdSelCriteria.value != '')
				{
					document.frmData.lstCriteria.selectedIndex = 0;
					document.frmData.btnDelete.disabled = false;
					if(document.frmData.hdSelCriteriaItem.value.indexOf('OLECRIT') != -1)
						document.frmData.btnDelete.disabled = true;
					else
						document.frmData.btnDelete.disabled = false;
				}		
			}	
	
			function formatCriteria()
			{
				var sEditCriteria = window.document.frmData.hdSelCriteriaItem.value;
				var lOperator,lData,lConnector,sFieldName,sCriteria,tempArray, valExclude; 
				tempArray = sEditCriteria.split("~~~~~");
				
				lData = tempArray[2].split("^^^^^");
				lConnector = tempArray[3].split(",");
				sFieldName = tempArray[4];
				
				arrAttribute = tempArray[7].split('|');
				
				if (arrAttribute[3] != 'range')
					{
						document.frmData.chkExclude.checked = true;
						valExclude = " NOT BETWEEN ";
					}
				else
					valExclude = " BETWEEN ";
				
				var dataArray, connectorArray;
				for(iCount=0;iCount<lData.length;iCount++)
				{
					connectorArray = lConnector[iCount].split('|')
					dataArray =	lData[iCount].split('!!!!!');
					if(connectorArray[0] != "none")
						sCriteria = connectorArray[0].toUpperCase() + " " +"("+sFieldName+valExclude+"'"+dataArray[0]+"' AND '"+ dataArray[1] +"')";
					else
						sCriteria = "("+sFieldName+valExclude+"'"+dataArray[0]+"' AND '"+ dataArray[1] +"')";
							
					add_item(sCriteria);
				}
			}		
	
			function OLEEnable()
			{
				if(window.document.frmData.chkOLE.checked == true)
				{
					window.document.frmData.cboDay2.disabled = true;
					window.document.frmData.btnOr.disabled = true;
					window.document.frmData.btnDelete.disabled = true;
					window.document.frmData.btnChange.disabled = true;
					//window.document.frmData.lstCriteria.disabled = true;
					window.document.frmData.txtDtofevnt1.disabled = true;
					window.document.frmData.txtDtofevnt2.disabled = true;
					window.document.frmData.btnDtofevnt1.disabled = true;
					window.document.frmData.btnDtofevnt2.disabled = true;
					window.document.frmData.btnOLE.disabled = false;
					
				}
				else
				{
					window.document.frmData.cboDay2.disabled = false;
					window.document.frmData.btnOr.disabled = false;
					
					if(window.document.frmData.lstCriteria.selectedIndex > -1)
						window.document.frmData.btnDelete.disabled = false;
					
					window.document.frmData.txtDtofevnt1.disabled = false;
					window.document.frmData.txtDtofevnt2.disabled = false;
					window.document.frmData.btnDtofevnt1.disabled = false;
					window.document.frmData.btnDtofevnt2.disabled = false;
					window.document.frmData.btnOLE.disabled = true;
				}
			}
			
			function selectDay1()
			{
				var today = new Date();
				var dateString = '';
				var temp;
				
				temp = (today.getMonth()+1).toString();
				
				if (temp.length == 1)
				{
					temp = "0"+ temp;
				}
				
				dateString = temp;
				
				temp = (today.getDate()).toString();
				
				if (temp.length == 1)
				{
					temp = "0"+ temp;
				}
				
				dateString = dateString + "/"+ temp;
				
				temp = today.getFullYear().toString();
				
				dateString = dateString + "/"+ temp;
				
				if (window.document.frmData.cboDay1.selectedIndex == 1)
				{
					window.document.frmData.txtDtofevnt1.disabled = true;
					window.document.frmData.btnDtofevnt1.disabled = true;
					window.document.frmData.txtDtofevnt1.value = dateString;
				}
				else if (window.document.frmData.cboDay1.selectedIndex == 0)
				{
					window.document.frmData.txtDtofevnt1.disabled = false;
					window.document.frmData.btnDtofevnt1.disabled = false;
				}
				else //-- Handling relative date
				{
					var sDate = new Date();
					var sRelDateFormula = arrRelFormulaValue[window.document.frmData.cboDay1.selectedIndex-2];
					if (sRelDateFormula.substring(sRelDateFormula.length-1,sRelDateFormula.length) == 'M')
					{
						sDate.setMonth(sDate.getMonth() + parseInt(sRelDateFormula.substring(0,2)));
						
						temp = (sDate.getMonth()+1).toString();
						
						if (temp.length == 1)
						{
							temp = "0"+ temp;
						}
		
						dateString = temp;
		
						temp = (sDate.getDate()).toString();
		
						if (temp.length == 1)
						{
							temp = "0"+ temp;
						}
		
						dateString = dateString + "/"+ temp;
		
						temp = sDate.getFullYear().toString();
		
						dateString = dateString + "/"+ temp;
					}
					else if (sRelDateFormula.substring(sRelDateFormula.length-1,sRelDateFormula.length) == 'D')
					{
						sDate.setDate(sDate.getDate() + parseInt(sRelDateFormula.substring(0,2)));
						
						temp = (sDate.getMonth()+1).toString();
						
						if (temp.length == 1)
						{
							temp = "0"+ temp;
						}
		
						dateString = temp;
		
						temp = (sDate.getDate()).toString();
		
						if (temp.length == 1)
						{
							temp = "0"+ temp;
						}
		
						dateString = dateString + "/"+ temp;
		
						temp = sDate.getFullYear().toString();
		
						dateString = dateString + "/"+ temp;
					}
					else if (sRelDateFormula.substring(sRelDateFormula.length-1,sRelDateFormula.length) == 'Y')
					{
						sDate.setFullYear(sDate.getFullYear() + parseInt(sRelDateFormula.substring(0,2)));
						
						temp = (sDate.getMonth()+1).toString();
						
						if (temp.length == 1)
						{
							temp = "0"+ temp;
						}
		
						dateString = temp;
		
						temp = (sDate.getDate()).toString();
		
						if (temp.length == 1)
						{
							temp = "0"+ temp;
						}
		
						dateString = dateString + "/"+ temp;
		
						temp = sDate.getFullYear().toString();
		
						dateString = dateString + "/"+ temp;
					}
					window.document.frmData.txtDtofevnt1.disabled = true;
					window.document.frmData.btnDtofevnt1.disabled = true;
					window.document.frmData.txtDtofevnt1.value = dateString;
				}
			}
			
			function selectDay2()
			{
				var today = new Date();
				var dateString = '';
				var temp;
				
				temp = (today.getMonth()+1).toString();
				
				if (temp.length == 1)
				{
					temp = "0"+ temp;
				}
				
				dateString = temp;
				
				temp = (today.getDate()).toString();
				
				if (temp.length == 1)
				{
					temp = "0"+ temp;
				}
				
				dateString = dateString + "/"+ temp;
				
				temp = today.getFullYear().toString();
				
				dateString = dateString + "/"+ temp;
				
				if (window.document.frmData.cboDay2.selectedIndex == 1)
				{
					window.document.frmData.txtDtofevnt2.disabled = true;
					window.document.frmData.btnDtofevnt2.disabled = true;
					window.document.frmData.txtDtofevnt2.value = dateString;
				}
				else if (window.document.frmData.cboDay2.selectedIndex == 0)
				{
					window.document.frmData.txtDtofevnt2.disabled = false;
					window.document.frmData.btnDtofevnt2.disabled = false;
				}
				else //-- Handling relative date
				{
					var sDate = new Date();
					var sRelDateFormula = arrRelFormulaValue[window.document.frmData.cboDay2.selectedIndex-2];
					if (sRelDateFormula.substring(sRelDateFormula.length-1,sRelDateFormula.length) == 'M')
					{
						sDate.setMonth(sDate.getMonth() + parseInt(sRelDateFormula.substring(0,2)));
						
						temp = (sDate.getMonth()+1).toString();
						
						if (temp.length == 1)
						{
							temp = "0"+ temp;
						}
		
						dateString = temp;
		
						temp = (sDate.getDate()).toString();
		
						if (temp.length == 1)
						{
							temp = "0"+ temp;
						}
		
						dateString = dateString + "/"+ temp;
		
						temp = sDate.getFullYear().toString();
		
						dateString = dateString + "/"+ temp;
					}
					else if (sRelDateFormula.substring(sRelDateFormula.length-1,sRelDateFormula.length) == 'D')
					{
						sDate.setDate(sDate.getDate() + parseInt(sRelDateFormula.substring(0,2)));
						
						temp = (sDate.getMonth()+1).toString();
						
						if (temp.length == 1)
						{
							temp = "0"+ temp;
						}
		
						dateString = temp;
		
						temp = (sDate.getDate()).toString();
		
						if (temp.length == 1)
						{
							temp = "0"+ temp;
						}
		
						dateString = dateString + "/"+ temp;
		
						temp = sDate.getFullYear().toString();
		
						dateString = dateString + "/"+ temp;
					}
					else if (sRelDateFormula.substring(sRelDateFormula.length-1,sRelDateFormula.length) == 'Y')
					{
						sDate.setFullYear(sDate.getFullYear() + parseInt(sRelDateFormula.substring(0,2)));
						
						temp = (sDate.getMonth()+1).toString();
						
						if (temp.length == 1)
						{
							temp = "0"+ temp;
						}
		
						dateString = temp;
		
						temp = (sDate.getDate()).toString();
		
						if (temp.length == 1)
						{
							temp = "0"+ temp;
						}
		
						dateString = dateString + "/"+ temp;
		
						temp = sDate.getFullYear().toString();
		
						dateString = dateString + "/"+ temp;
					}
					window.document.frmData.txtDtofevnt2.disabled = true;
					window.document.frmData.btnDtofevnt2.disabled = true;
					window.document.frmData.txtDtofevnt2.value = dateString;
				}
			}

			function ButtonEnable()
			{
				var selIndex;
				window.document.frmData.btnChange.disabled = true;
				if(window.document.frmData.lstCriteria.selectedIndex >= 0)
					window.document.frmData.btnDelete.disabled = false; 
				else
					window.document.frmData.btnDelete.disabled = true; 
				
				if(document.frmData.lstCriteria.selectedIndex >= 0)
				{
					var splitData = arrData[document.frmData.lstCriteria.selectedIndex].split('!!!!!');
					
					document.frmData.txtDtofevnt1.value = formatDate(splitData[0]);
					document.frmData.txtDtofevnt2.value = formatDate(splitData[1]);
				}
					
				oOption = '';	
				for(i=0;i<document.frmData.lstCriteria.length;i++)
					oOption = oOption + document.frmData.lstCriteria.options[i].text;
		
				
				if(oOption.indexOf("OLECRIT") > -1)
				{
					document.frmData.txtDtofevnt1.value = '';
					document.frmData.txtDtofevnt2.value = '';
				}
			}
					
			function EnableChange()
			{
			if(window.document.frmData.lstCriteria.selectedIndex !=-1 && window.document.frmData.chkOLE.checked==false)
				window.document.frmData.btnChange.disabled = false;
			if(window.document.frmData.lstCriteria.selectedIndex >= 0 )
				window.document.frmData.btnChange.disabled = false;
			else
				window.document.frmData.btnChange.disabled = true;
			}
	
		</script>
	</head>

	<body onLoad="window_onLoad();pageInitialize();">
	<form name="frmData" topmargin="0" leftmargin="0" onSubmit="return false;">
		<table class="formsubtitle" border="0" width="100%" align="center">
			<tr>
				<td class="ctrlgroup">Criteria :</td>
			</tr>
		</table>					
		<table width="100%" border="0">
			<tr><td colspan="3"><b><%=Response.Write(Request.QueryString("FIELDNAME"))%></b></td></tr>
			<tr>
				<td width="25%"></td>
				<td width="25%" align="right">From :&nbsp;<input type="text" size="10" name="txtDtofevnt1" onKeyDown="EnableChange()"  onBlur1="return IsDate(this)" onBlur="dateLostFocus('txtDtofevnt1')">&nbsp;<input type="button" class="button" value="..." name="btnDtofevnt1" onClick="selectDate('txtDtofevnt1')"></td>
				<td width="30%">&nbsp;<select name="cboDay1" style="WIDTH:70%" height="1" onChange="selectDay1();EnableChange()" onClick="selectDay1();EnableChange()">
									<option>DATE</option>
									<option>TODAY</option>
								</select>
				</td>
				<td width="20%"><input type="checkbox" value="" name="chkSubquery" onclick="Subquery()">Subquery Criteria</td>
			</tr>
			<tr>
				<td><input type="checkbox" value="" name="chkExclude" onChange="Subquery()">Exclude Range</td>
				<td width1="5%" align="right">To :&nbsp;<input type="text" size="10" name="txtDtofevnt2" onKeyDown="EnableChange()"  onBlur1="return IsDate(this)" onBlur="dateLostFocus('txtDtofevnt2')">&nbsp;<input type="button" class="button" value="..." name="btnDtofevnt2" onClick="selectDate('txtDtofevnt2')"></td>
				<td width1="28%">&nbsp;<select name="cboDay2" style="WIDTH:70%" height="1" onChange="selectDay2();EnableChange()" onClick="selectDay2();EnableChange()">
									<option>DATE</option>
									<option>TODAY</option>
								</select>
				</td>
				<td>&nbsp;<input class="button" type="button" style1="WIDTH: 25%" name="btnChange" value="Change" disabled></td>
			</tr>
			<tr>
				<td>&nbsp;<input class="button" type="button" style1="WIDTH: 100%" name="btnOr" value="    OR    " onclick="or_click()"></td>
				<td align="right"><input class="button" type="button" style1="WIDTH: 25%" name="btnDelete" value=" Delete " disabled onclick="delete_click()">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td align="center"><input type="checkbox" value="" name="chkOLE" onclick="OLEEnable()">OLE Criteria:&nbsp;</td>
				<td>&nbsp;<input class="button" type="button" style1="WIDTH: 25%" name="btnOLE" value="OLE Param" onClick="window.open('OLEParam.asp','OLEcri','resizable=yes,Width=400,Height=220,top='+(screen.availHeight-220)/2+',left='+(screen.availWidth-400)/2+'')"  disabled></td>
			</tr>
			<tr>
				<td colspan="4">
					<select name="lstCriteria" style="WIDTH:100%" size="15" onChange="ButtonEnable()">
						
					</select>
				</td>
			</tr>
			<tr><!-- Rajeev -- 03/25/2004 -- Added feature of RMNet labelling-->
				<td>
					<!-- Start Naresh MITS 7836 09/11/2006-->
					<input type="checkbox" name="chkHide"><!--Hide in RMNet-->
					<!-- End Naresh MITS 7836 09/11/2006-->
				</td>
				<!-- Start Naresh MITS 7836 09/11/2006-->
				<td colspan="3" align="right"><!--RMNet label:-->
				<!-- End Naresh MITS 7836 09/11/2006-->
					<input type="text" name="txtRMNetLabel" value="" onChange1="window.close()">
				</td>
			</tr>
		</table>
		<hr>
		<table border="0" width="100%" align="center" cellspacing="0" cellpadding="0">
			<tr>
				<td align="middle">
					<input class="button" type="button" style1="WIDTH: 30%" name="ok" value="    OK    " onClick="ok_click()">
					<input class="button" type="button" style1="WIDTH: 30%" name="cancel" value="Cancel" onClick="window.close()">
					<input class="button" type="button" style1="WIDTH: 30%" name="help" value="  Help  ">
				</td>
			</tr>
		</table>
		
		<input type="hidden" name="hdFieldName" value="<%=Request.QueryString("FIELDNAME")%>">
		<input type="hidden" name="hdTableId" value="<%=Request.QueryString("TABLEID")%>">
		<input type="hidden" name="hdFieldId" value="<%=Request.QueryString("FIELDID")%>">
		<input type="hidden" name="hdFieldType" value="<%=Request.QueryString("FIELDTYPE")%>">
		<input type="hidden" name="hdDBTableId" value="<%=Request.QueryString("DBTABLEID")%>">
		<input type="hidden" name="hdSelCriteria" value="<%=fixDoubleQuote(Request.QueryString("CRITERIA"))%>">
		<input type="hidden" name="hdSelCriteriaIndex" value="<%=Request.QueryString("CRITERIAINDEX")%>">
		<input type="hidden" name="hdSelSubQuery" value="<%=Request.QueryString("SUBQUERY")%>">
		<input type="hidden" name="hdSelHide" value="<%=Request.QueryString("HIDEINRMNET")%>">
		<input type="hidden" name="hdSelRMNetLabel" value="<%=fixDoubleQuote(Request.QueryString("RMNETLABEL"))%>">
		<input type="hidden" name="hdSelCriteriaItem" value="<%=fixDoubleQuote(Request.QueryString("PCRITERIA"))%>">
		<input type="hidden" name="hdSelOLECriteriaItem" value="<%=fixDoubleQuote(Request.QueryString("POLECRITERIA"))%>">
		<input type="hidden" name="hdOLECritName" value="">
		<input type="hidden" name="hdOLECritDesc" value="">
		<input type="hidden" name="hdRelativeDate" value="<%=fixDoubleQuote(sDateFormula)%>">
	
	</form>
	</body>
</html>