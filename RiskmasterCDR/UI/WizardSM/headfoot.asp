<html>
	<head>
		<link rel="stylesheet" href="RMNet.css" type="text/css"/>
		<title>SMD - [Headers & Footers]</title>

		<script language="JavaScript" SRC="form.js"></script>
	
		<script>
			function headFootSubmit()
			{
				window.opener.frmData.hdHeaderLeft.value = window.document.frmHeadFoot.txtHeaderleft.value;
				window.opener.frmData.hdHeaderCenter.value = window.document.frmHeadFoot.txtHeadercenter.value;
				window.opener.frmData.hdHeaderRight.value = window.document.frmHeadFoot.txtHeaderright.value;
				window.opener.frmData.hdFooterLeft.value = window.document.frmHeadFoot.txtFooterleft.value;
				window.opener.frmData.hdFooterCenter.value = window.document.frmHeadFoot.txtFootercenter.value;
				window.opener.frmData.hdFooterRight.value = window.document.frmHeadFoot.txtFooterright.value;
				window.close();
			}

			function body_onload()
			{
				window.document.frmHeadFoot.txtHeaderleft.value = window.opener.frmData.hdHeaderLeft.value;
				window.document.frmHeadFoot.txtHeadercenter.value = window.opener.frmData.hdHeaderCenter.value;
				window.document.frmHeadFoot.txtHeaderright.value = window.opener.frmData.hdHeaderRight.value;
				window.document.frmHeadFoot.txtFooterleft.value = window.opener.frmData.hdFooterLeft.value; 
				window.document.frmHeadFoot.txtFootercenter.value = window.opener.frmData.hdFooterCenter.value;
				window.document.frmHeadFoot.txtFooterright.value = window.opener.frmData.hdFooterRight.value;
			}
			
			function updateSerialNum()
			{
				window.opener.checkSerialNum(0);
			} 
		
		</script>
	</head>
  
	<body class="8pt" onLoad="body_onload()" onBlur1="window.focus()">
	<form name="frmHeadFoot" action="post">
		<table class="formsubtitle" border="0" width="100%">
			<tr>
				<td class="ctrlgroup">Header</td>
			</tr>
		</table>

		<table width="100%" border="0">
			<tr>
				<td align="left">Left</td>
				<td align="center">Center</td>
				<td align="right">Right</td>
			</tr>
			<tr>
				<td align="left"><input type="text" value="" name="txtHeaderleft" onChange="updateSerialNum();"></td>
				<td align="center"><input type="text" value="&r" name="txtHeadercenter" onChange="updateSerialNum();"></td>
				<td align="right"><input type="text" value="&t" name="txtHeaderright" onChange="updateSerialNum();"></td>
			</tr>
		</table>
	<br>
		<table class="formsubtitle" border="0" width="100%">
			<tr>
				<td class="ctrlgroup">Footer</td>
			</tr>
		</table>

		<table width="100%" border="0">
			<tr>
				<td align="left">Left</td>
				<td align="center">Center</td>
				<td align="right">Right</td>
			</tr>
			<tr>
				<td align="left"><input type="text" value="" name="txtFooterleft" onChange="updateSerialNum();"></td>
				<td align="center"><input type="text" value="Page &p of &P" name="txtFootercenter" onChange="updateSerialNum();"></td>
				<td align="right"><input type="text" value="" name="txtFooterright" onChange="updateSerialNum();"></td>
			</tr>
		</table>
	<br>
	<hr>

		<table width="100%" border="0">
			<tr>
				<td width="20%">&nbsp;</td>
				<td width="60%" align="center">
					<input type="button" class="button" style="width:25%" name="up" value="OK" onClick="headFootSubmit();">
					<input type="button" class="button" style="width:25%" name="up" value="Cancel" onClick="javascript:window.close()">
					<input type="button" class="button" style="width:25%" name="up" value="Help">
				</td>
				<td width="20%">&nbsp;</td>
			</tr>
		</table>
		</form>
	</body>
</html>