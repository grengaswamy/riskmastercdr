<% 'Option Explicit %>
<!-- #include file ="session.inc" -->
<!-- #include file ="generic.asp" -->

<html>
	<head>
		<link rel="stylesheet" href="RMNet.css" type="text/css"/>
<%
Dim myArrayP, pageError
'-- Rajeev -- 03/09/2004
'''myArrayP = Split(objSessionStr("ArrayP"), ",")
myArrayP = Split(objSessionStr("ArrayP"), "*!^")

Dim count, matchString, arrTemp(), index, m
count = 0
index = 0
matchString = Request.QueryString("usercolname")

If Request.QueryString("UpdateMode") = "Add" Then
	Err.Clear()
	On Error Resume Next
	m = -1
	m = UBound(myArrayP)
	If Not (Err.number <> 9 And m <> "" And m >=0) Then
		Call ShowError(pageError, "No field names are available.", False)
	Else
		For i = 0 To UBound(myArrayP)
			myArrayP(i) = Right(myArrayP(i),Len(myArrayP(i))-InStr(myArrayP(i),":"))
		Next
	End If

	Dim sArrayP 
	If Err.number <> 9 And m <> "" And m >=0 Then
		For i = 0 To UBound(myArrayP) 
			If i = 0 Then 
				sArrayP = myArrayP(0) 
			Else 
'-- Rajeev -- 03/09/2004			
'''				sArrayP = sArrayP & "," & myArrayP(i) 
				sArrayP = sArrayP & "*!^" & myArrayP(i) 
			End If 
		Next
	End If
	On Error Goto 0
	
ElseIf Request.QueryString("UpdateMode") = "Edit" Then
	Err.Clear()
	On Error Resume Next
	m = -1
	m = UBound(myArrayP)
	count = 0
	If Not (Err.number <> 9 And m <> "" And m >=0) Then
		Call ShowError(pageError, "No field names are available.", False)
	Else
		ReDim arrTemp(m-1)	'-- Rajeev -- 03/09/2004 -- Edit will exculde matching element
		For i = 0 To UBound(myArrayP)
			If Not InStr(1,Trim(myArrayP(i)),Trim(matchString)) = 0 And Len(Trim(Right(myArrayP(i),Len(myArrayP(i))-InStr(myArrayP(i),":")))) = Len(Trim(matchString)) Then
				index = i
			Else
'-- Rajeev -- 03/09/2004			
'''				ReDim  Preserve arrTemp(count)
				arrTemp(count) = Right(myArrayP(i),Len(myArrayP(i))-InStr(myArrayP(i),":"))
				count = count + 1
			End If
		Next
	End If
	
	m = -1
	m = UBound(arrTemp)
	
	If Err.number <> 9 And m <> "" And m >=0 Then
		i = 0
		For i = 0 To UBound(arrTemp)
			If i = 0 Then 
				sArrayP = arrTemp(0) 
			Else 
'-- Rajeev -- 03/09/2004			
'''				sArrayP = sArrayP & "," & arrTemp(i) 
				sArrayP = sArrayP & "*!^" & arrTemp(i) 
			End If 
		Next
	End If
	On Error Goto 0

End If 

Dim xmlDoc, tableId, fieldId, arrXML

Set xmlDoc = Nothing
On Error Resume Next
Set xmlDoc = Server.CreateObject("Msxml2.DOMDocument")
On Error Goto 0

If xmlDoc Is Nothing Then
	Call ShowError(pageError, "XML Parser is not available.", True)
End If

xmlDoc.async = false


Dim m_objSMList, a1, a2, a3, k1, sDerFieldType
Set m_objSMList = Nothing

On Error Resume Next

Set m_objSMList = Server.CreateObject("InitSMList.CInitSM")
On Error Goto 0

If m_objSMList Is Nothing Then
	Call ShowError(pageError, "Call to CInitSM class failed.", True)
End If

m_objSMList.m_RMConnectStr = objSessionStr(SESSION_DSN)
m_objSMList.m_SMConnectStr = Application(APP_SMDDSN)

'''m_objSMList.InitSMList

Dim arrSMTables()

If objSessionStr(SESSION_MODULESECURITY) = "1" Then
	If Not m_objSMList.GetSMTableList(Application(APP_PERMCACHE & objSessionStr(SESSION_DSNID)), objSessionStr(SESSION_GROUPID), arrSMTables) Then
	End If
Else
	If Not m_objSMList.GetSMTableList(Nothing, "", arrSMTables) Then
	End If
End If

Dim iCounter

If objSessionStr("ArrayPXML") <> "-1" Then
	arrXML = Split(objSessionStr("ArrayPXML"), "</column>,")
	Err.Clear()
	On Error Resume Next
	m = -1
	m = UBound(arrXML)
	If Err.number <> 9 And m <> "" And m >=0 Then
		For iCounter = 0 To UBound(arrXML) - 1
			arrXML(iCounter) = arrXML(iCounter) & "</column>"
		Next
	End If
	On Error Goto 0
	
	Dim arrFieldType(), sColumnInvolved
	sColumnInvolved = ""

    'Vsoni5 : 03/30/2010. Variables added.
    dim posStart, posEnd
	dim strDerFormula, strCol
	If Err.number <> 9 And m <> "" And m >=0 Then
		i = 0
		For i = 0 To UBound(arrXML)
			xmlDoc.loadXML replace(arrXML(i), "&", "&amp;")           
			
			'Vsoni5 MITS 19805 : Encrypting '<' and '>' symboles for derrived field editing.
			posStart = InStr(1,arrXML(i),"<detailformula>")
			
			If posStart > 0 Then
			    posStart = posStart + 15
			    posEnd = InStr(posStart,arrXML(i),"</detailformula>")
			    strDerFormula = Mid(arrXML(i),posStart,posEnd-posStart)
			    strDerFormula = Replace(strDerFormula,"<", "&lt;")
			    strDerFormula = Replace(strDerFormula,">", "&gt;")
			    
			   strCol = Left(arrXML(i),posStart-1) & strDerFormula & Right(arrXML(i), Len(arrXML(i))- posEnd+1)
			   arrXML(i) = strCol
			End If
			
            xmlDoc.loadXML arrXML(i)
                        
			'-- Fetching DataType for derived fields
			Set a1 = xmlDoc.getElementsByTagName("column").item(0).childNodes.item(0) 
			Set a2 = xmlDoc.getElementsByTagName("derived").item(0)
			If Not a2 Is Nothing Then
				If sDerFieldType = "" Then
					sDerFieldType = xmlDoc.getElementsByTagName("derived").item(0).attributes.item(0).text
				Else
					sDerFieldType = sDerFieldType & "," & xmlDoc.getElementsByTagName("derived").item(0).attributes.item(0).text
				End If
				
				'-- Handling for columns involved
				If i <> index Then  '-- Getting all the detail formulae other than the current index
					Set a3 = a1.getElementsByTagName("detailformula").item(0)
					If sColumnInvolved = "" Then
						sColumnInvolved = a3.text
					Else
						sColumnInvolved = sColumnInvolved  & a3.text
					End If
				End If
			End If	
			tableId = xmlDoc.getElementsByTagName("column").item(0).attributes.item(1).value
			fieldId = xmlDoc.getElementsByTagName("column").item(0).attributes.item(2).value
			Redim Preserve arrFieldType(i)
			arrFieldType(i) = m_objSMList.GetFieldType(CLng(tableId),CInt(fieldId))
		Next
	End If

	Dim sFieldType
	
	Err.Clear()
	On Error Resume Next
	m = -1
	m = UBound(arrFieldType)
	If Err.number <> 9 And m <> "" And m >= 0 Then
		i = 0
		For i = 0 To UBound(arrFieldType) 
			If i = 0 Then 
				sFieldType = arrFieldType(0) 
			Else 
				sFieldType = sFieldType & "," & arrFieldType(i) 
			End If 
		Next
	End If
	On Error Goto 0
End If

Set m_objSMList = Nothing

Dim objLocal, arrOperators(), arrDataType(), i, arrArithmeticFunction(), arrArithmeticString()
Dim arrStrFunction(), arrStrString(), arrDateTimeFunction(), arrDateTimeString()
Dim arrDataAccessFunction(), arrDataAccessString(), arrSystemWideParamFunction(), arrSystemWideParamString()
Dim arrGCParamFunction(), arrGCParamString(), arrWCParamFunction(), arrWCParamString()
Dim arrVAParamFunction(), arrVAParamString(), arrMiscFunction(), arrMiscString()

Set objLocal = Nothing

On Error Resume Next
Set objLocal = Server.CreateObject("InitSMList.CLocalize") 
On Error Goto 0

If objLocal Is Nothing Then
	Call ShowError(pageError, "Call to CLocalize class failed.", True)
End If

'-- Rajeev -- 03/09/2004
objLocal.GetComboItems arrOperators, "IDD_MULTIQ_DRVEDIT", "IDC_CMBFUNCTIONS", "0|" & Application(APP_DATAPATH)

Err.Clear()
On Error Resume Next
Dim mOperators, mError
mOperators = -1
mOperators = UBound(arrOperators)
If Not (Err.number <> 9 And mOperators <> "" And mOperators >=0) Then
	Call ShowError(pageError, "No operators are available.", False)
End If
On Error Goto 0

'-- Rajeev -- 03/09/2004
objLocal.GetComboItems arrDataType, "IDD_MULTIQ_DRVEDIT", "IDC_CMBDATATYPE", "0|" & Application(APP_DATAPATH)

Err.Clear()
On Error Resume Next
Dim mDatatype
mDatatype = -1
mDatatype = UBound(arrDataType)
If Not (Err.number <> 9 And mDatatype <> "" And mDatatype >=0) Then
	Call ShowError(pageError, "No data types are available.", False)
End If
On Error Goto 0

objLocal.GetListItems arrArithmeticFunction, arrArithmeticString, 0, "0|" & Application(APP_DATAPATH)
Dim a, sArithmeticString
Err.Clear()
On Error Resume Next
m = -1
m = UBound(arrArithmeticFunction)
If Err.number <> 9 And m <> "" And m >= 0 Then
	For i = 0 To UBound(arrArithmeticFunction) 
		If i = 0 Then 
			a = arrArithmeticFunction(0) 
		Else 
			a = a & "|||||" & arrArithmeticFunction(i) 
		End If 
	Next
End If

m = -1
m = UBound(arrArithmeticString)
If Err.number <> 9 And m <> "" And m >= 0 Then
	For i = 0 To UBound(arrArithmeticString) 
		If i = 0 Then 
			sArithmeticString = arrArithmeticString(0) 
		Else 
			sArithmeticString = sArithmeticString & "|||||" & arrArithmeticString(i) 
		End If 
	Next
End If
On Error Goto 0

objLocal.GetListItems arrStrFunction, arrStrString, 1, "0|" & Application(APP_DATAPATH)
Dim b, sStrString
Err.Clear()
On Error Resume Next
m = -1
m = UBound(arrStrFunction)
If Err.number <> 9 And m <> "" And m >= 0 Then
	For i = 0 To UBound(arrStrFunction) 
		If i = 0 Then 
			b = arrStrFunction(0) 
		Else 
			b = b & "|||||" & arrStrFunction(i) 
		End If 
	Next
End If

m = -1
m = UBound(arrStrString)
If Err.number <> 9 And m <> "" And m >= 0 Then
	For i = 0 To UBound(arrStrString) 
		If i = 0 Then 
			sStrString = arrStrString(0) 
		Else 
			sStrString = sStrString & "|||||" & arrStrString(i) 
		End If 
	Next
End If
On Error Goto 0

objLocal.GetListItems arrDateTimeFunction, arrDateTimeString, 2, "0|" & Application(APP_DATAPATH)
Dim c, sDateTimeString
Err.Clear()
On Error Resume Next
m = -1
m = UBound(arrDateTimeFunction)
If Err.number <> 9 And m <> "" And m >= 0 Then
	For i = 0 To UBound(arrDateTimeFunction) 
		If i = 0 Then 
			c = arrDateTimeFunction(0) 
		Else 
			c = c & "|||||" & arrDateTimeFunction(i) 
		End If 
	Next
End If

m = -1
m = UBound(arrDateTimeString)
If Err.number <> 9 And m <> "" And m >= 0 Then
	For i = 0 To UBound(arrDateTimeString) 
		If i = 0 Then 
			sDateTimeString = arrDateTimeString(0) 
		Else 
			sDateTimeString = sDateTimeString & "|||||" & arrDateTimeString(i) 
		End If 
	Next
End If
On Error Goto 0

objLocal.GetListItems arrDataAccessFunction, arrDataAccessString, 3, "0|" & Application(APP_DATAPATH)
Dim d, sDataAccessString
Err.Clear()
On Error Resume Next
m = -1
m = UBound(arrDataAccessFunction)
If Err.number <> 9 And m <> "" And m >= 0 Then
	For i = 0 To UBound(arrDataAccessFunction) 
		If i = 0 Then 
			d = arrDataAccessFunction(0) 
		Else 
			d = d & "|||||" & arrDataAccessFunction(i) 
		End If 
	Next
End If

m = -1
m = UBound(arrDataAccessString)
If Err.number <> 9 And m <> "" And m >= 0 Then
	For i = 0 To UBound(arrDataAccessString) 
		If i = 0 Then 
			sDataAccessString = arrDataAccessString(0) 
		Else 
			sDataAccessString = sDataAccessString & "|||||" & arrDataAccessString(i) 
		End If 
	Next
End If
On Error Goto 0

objLocal.GetListItems arrSystemWideParamFunction, arrSystemWideParamString, 4, "0|" & Application(APP_DATAPATH)
Dim e, sSystemWideParamString
Err.Clear()
On Error Resume Next
m = -1
m = UBound(arrSystemWideParamFunction)
If Err.number <> 9 And m <> "" And m >= 0 Then
	For i = 0 To UBound(arrSystemWideParamFunction) 
		If i = 0 Then 
			e = arrSystemWideParamFunction(0) 
		Else 
			e = e & "|||||" & arrSystemWideParamFunction(i) 
		End If 
	Next
End If

m = -1
m = UBound(arrSystemWideParamString)
If Err.number <> 9 And m <> "" And m >= 0 Then
	For i = 0 To UBound(arrSystemWideParamString) 
		If i = 0 Then 
			sSystemWideParamString = arrSystemWideParamString(0) 
		Else 
			sSystemWideParamString = sSystemWideParamString & "|||||" & arrSystemWideParamString(i) 
		End If 
	Next
End If
On Error Goto 0

objLocal.GetListItems arrGCParamFunction, arrGCParamString, 5, "0|" & Application(APP_DATAPATH)
Dim f, sGCParamString 
Err.Clear()
On Error Resume Next
m = -1
m = UBound(arrGCParamFunction)
If Err.number <> 9 And m <> "" And m >= 0 Then
	For i = 0 To UBound(arrGCParamFunction) 
		If i = 0 Then 
			f = arrGCParamFunction(0) 
		Else 
			f = f & "|||||" & arrGCParamFunction(i) 
		End If 
	Next
End If

m = -1
m = UBound(arrGCParamString)
If Err.number <> 9 And m <> "" And m >= 0 Then
	For i = 0 To UBound(arrGCParamString) 
		If i = 0 Then 
			sGCParamString = arrGCParamString(0) 
		Else 
			sGCParamString = sGCParamString & "|||||" & arrGCParamString(i) 
		End If 
	Next
End If
On Error Goto 0


objLocal.GetListItems arrWCParamFunction, arrWCParamString, 6, "0|" & Application(APP_DATAPATH)
Dim g, sWCParamString 
Err.Clear()
On Error Resume Next
m = -1
m = UBound(arrWCParamFunction)
If Err.number <> 9 And m <> "" And m >= 0 Then
	For i = 0 To UBound(arrWCParamFunction) 
		If i = 0 Then 
			g = arrWCParamFunction(0) 
		Else 
			g = g & "|||||" & arrWCParamFunction(i) 
		End If 
	Next
End If

m = -1
m = UBound(arrWCParamString)
If Err.number <> 9 And m <> "" And m >= 0 Then
	For i = 0 To UBound(arrWCParamString) 
		If i = 0 Then 
			sWCParamString = arrWCParamString(0) 
		Else 
			sWCParamString = sWCParamString & "|||||" & arrWCParamString(i) 
		End If 
	Next
End If
On Error Goto 0

objLocal.GetListItems arrVAParamFunction, arrVAParamString, 7, "0|" & Application(APP_DATAPATH)
Dim h, sVAParamString 

Err.Clear()
On Error Resume Next
m = -1
m = UBound(arrVAParamFunction)
If Err.number <> 9 And m <> "" And m >= 0 Then
	For i = 0 To UBound(arrVAParamFunction) 
		If i = 0 Then 
			h = arrVAParamFunction(0) 
		Else 
			h = h & "|||||" & arrVAParamFunction(i) 
		End If 
	Next
End If

m = -1
m = UBound(arrVAParamString)
If Err.number <> 9 And m <> "" And m >= 0 Then
	For i = 0 To UBound(arrVAParamString) 
		If i = 0 Then 
			sVAParamString = arrVAParamString(0) 
		Else 
			sVAParamString = sVAParamString & "|||||" & arrVAParamString(i) 
		End If 
	Next
End If
On Error Goto 0

objLocal.GetListItems arrMiscFunction, arrMiscString, 8, "0|" & Application(APP_DATAPATH)
Dim k, sMiscString
Err.Clear()
On Error Resume Next
m = -1
m = UBound(arrMiscFunction)
If Err.number <> 9 And m <> "" And m >= 0 Then
	For i = 0 To UBound(arrMiscFunction) 
		If i = 0 Then 
			k = arrMiscFunction(0) 
		Else 
			k = k & "|||||" & arrMiscFunction(i) 
		End If 
	Next
End If

m = -1
m = UBound(arrMiscString)
If Err.number <> 9 And m <> "" And m >= 0 Then
	For i = 0 To UBound(arrMiscString) 
		If i = 0 Then 
			sMiscString = arrMiscString(0) 
		Else 
			sMiscString = sMiscString & "|||||" & arrMiscString(i) 
		End If 
	Next
End If
On Error Goto 0

%>
<html>
	<head>
		<link rel="stylesheet" href="RMNet.css" type="text/css"/>
		<title>SMD - [Add/Edit Derived Fields]</title>
		<script language="JavaScript" SRC="SMD.js"></script>
		<script>
			var arrDataTypeValue = new Array;
			arrDataTypeValue.length = 0;
			var arrFieldName, sSelAggregateCalcMethodAttribute;
			sSelAggregateCalcMethodAttribute = '';
			var trackListBox = '';
			
			function window_onLoad()
			{
				arrDataType = document.frmData.hdFieldType.value.split(",");
//-- Rajeev -- 03/09/2004				
//				arrFieldName = document.frmData.hdFieldName.value.split(",");
				arrFieldName = document.frmData.hdFieldName.value.split("*!^");
				
				//-- In edit case the date type of the selected field is not required for display
				if (document.frmData.hdAddEdit.value == 'Edit')
				{
					arrDataType[document.frmData.hdIndex.value] = null;
					adjust_pArray(arrDataType);
				}
				
				for(i=0;i<arrDataType.length;i++)
				{
					if (arrDataType[i] == -1)
						arrDataTypeValue[arrDataTypeValue.length] = -1;
					else
						mapDataType(arrDataType[i]);
				}
				//-- Checking for derived field
				var arrXML = document.frmData.hdSelFieldsXML.value.split("</column>,");
				for(iCounter=0;iCounter<arrXML.length-1;iCounter++)
					arrXML[iCounter] = arrXML[iCounter] + "</column>"

				if (document.frmData.hdAddEdit.value == 'Edit')
				{
					if (document.frmData.hdIndex.value != 0)
					{
						var sSelectedDerField = arrXML[document.frmData.hdIndex.value];
						arrXML[document.frmData.hdIndex.value] = null;
					}
					adjust_pArray(arrXML)
				}
				
				
				for(i=0;i<arrXML.length;i++)
				{
					if (arrXML[i].indexOf("<derived") == -1)  
					{
						populateFields(i);
					}
					else
					{
						var arrPos, iFieldNumber;
						var sFieldType = '';
						var sTemp = arrXML[i].substring(arrXML[i].indexOf("<derived"),arrXML[i].length);
						arrPos = FindCharacter(sTemp, '"', 2).split(',');
						sFieldTypeAttribute = sTemp.substr(parseInt(arrPos[0]),parseInt(arrPos[1])-1-parseInt(arrPos[0]));
						sFieldName = getTypeName(sFieldTypeAttribute);
						arrDataTypeValue[i] = sFieldName;
						populateFields(i);
					}
				}
				//Extracting the required info. from selected derived field
				if (document.frmData.hdAddEdit.value == "Edit" && document.frmData.hdIndex.value != 0)
				{
					sSelectedDerField = sSelectedDerField.substring(sSelectedDerField.indexOf("<derived"),sSelectedDerField.length);
					arrPos = FindCharacter(sSelectedDerField, '"', 2).split(',');
					var pos1 = arrPos[0];
					var pos2 = arrPos[1];
					arrPos = FindCharacter(sSelectedDerField, '"', 5).split(',');
					var pos5 = arrPos[1];
					arrPos = FindCharacter(sSelectedDerField, '"', 6).split(',');
					var pos6 = arrPos[1];
					arrPos = FindCharacter(sSelectedDerField, '"', 7).split(',');
					var pos7 = arrPos[1];
					arrPos = FindCharacter(sSelectedDerField, '"', 8).split(',');
					var pos8 = arrPos[1]; 
					
					sSelFieldTypeAttribute = sSelectedDerField.substr(parseInt(pos1),parseInt(pos2)-1-parseInt(pos1));
					sSelFieldWidthAttribute = sSelectedDerField.substr(parseInt(pos5),parseInt(pos6)-1-parseInt(pos5));
					sSelAggregateCalcMethodAttribute = sSelectedDerField.substr(parseInt(pos7),parseInt(pos8)-1-parseInt(pos7));
					
					if (sSelectedDerField.indexOf("<detailformula/>") > -1)
						sSelDetailFormula = '';
					else
						sSelDetailFormula = sSelectedDerField.substring(sSelectedDerField.indexOf("<detailformula>")+15,sSelectedDerField.indexOf("</detailformula>"))
					//Vsoni5 MITS 19805 : Decrypting '<' and '>' symboles for derrived field editing.
					sSelDetailFormula = (sSelDetailFormula.replace(/&lt;/gi,"<")).replace(/&gt;/gi,">");
					if (trimIt(sSelAggregateCalcMethodAttribute) == 'sameformula' || trimIt(sSelAggregateCalcMethodAttribute) == 'differentformula')
					{
						if (sSelectedDerField.indexOf("<aggregateformula/>") > -1)
							sSelAggregateFormula = '';
						else
							sSelAggregateFormula = sSelectedDerField.substring(sSelectedDerField.indexOf("<aggregateformula>")+18,sSelectedDerField.indexOf("</aggregateformula>"))
					}
					
					if (trimIt(sSelAggregateCalcMethodAttribute) == 'sameformula')
					{	
						document.frmData.optAggregate[1].checked = true;
						document.frmData.txtAggregateFormula.disabled = true;
					}
					else if (trimIt(sSelAggregateCalcMethodAttribute) == 'differentformula')
						document.frmData.optAggregate[2].checked = true;
					else
					{	
						document.frmData.optAggregate[0].checked = true;
						document.frmData.txtAggregateFormula.disabled = true;
						sSelAggregateFormula = '';
					}
					
					sSelFieldName = getTypeName(sSelFieldTypeAttribute);
					sSelFieldTypeIndex = getTypeIndex(trimIt(sSelFieldName));
					document.frmData.cboDataType.selectedIndex = sSelFieldTypeIndex;
					document.frmData.txtWidth.value = sSelFieldWidthAttribute;
//-- Rajeev -- 03/09/2004					
//					document.frmData.txtDetailFormula.value = sSelDetailFormula;
					document.frmData.txtDetailFormula.value = sSelDetailFormula.replace("~^~^~","'");
					document.frmData.txtAggregateFormula.value = sSelAggregateFormula;
					document.frmData.txtAggregateFormula.value = sSelAggregateFormula.replace("~^~^~","'");
					
					changeDataType(sSelFieldWidthAttribute);
				}				
				var index = document.frmData.cboOperators.selectedIndex;
				populateListbox(index);
				
				//Handling DataType & Width
				if (document.frmData.hdAddEdit.value == "Add")
				{
					changeDataType(-1);
					document.frmData.optAggregate[0].checked = true;
					document.frmData.txtAggregateFormula.disabled = true;
					
				}
			}
			
			function getTypeName(pAttribute)
			{
				if (pAttribute == 'text')
					return "Text"
				else if (pAttribute == 'numeric')
					return "Numeric"
				else if (pAttribute == 'formattednumeric')
					return "Formatted Numeric"
				else if (pAttribute == 'currency')
					return "Currency"
				else if (pAttribute == 'date')
					return "Date"
				else if (pAttribute == 'time')
					return "Time"
				else if (pAttribute == 'datetime')
					return "DateTime"
			}
			
			function getTypeIndex(pFieldTypeName)
			{
				if (pFieldTypeName == 'Text')
					return "0"
				else if (pFieldTypeName == 'Numeric')
					return "1"
				else if (pFieldTypeName == 'Formatted Numeric')
					return "2"
				else if (pFieldTypeName == 'Currency')
					return "3"
				else if (pFieldTypeName == 'Date')
					return "4"
				else if (pFieldTypeName == 'Time')
					return "5"
				else if (pFieldTypeName == 'DateTime')
					return "6"
			}
			
			function dialogModal()
			{
				var Nav4 = ((navigator.appName == "Netscape") && (parseInt(navigator.appVersion) >= 4));
				
				if (Nav4)
					window.focus();
				
				return true;
			}
			
			function add_item(pObj,txt)
			{
				var sNewItem;
				sNewItem = new Option(txt);
				if (navigator.appName == "Netscape")
					pObj.add(sNewItem, null);
				else
					pObj.add(sNewItem);
			}
			
			function changeFunction()		
			{
				var index = document.frmData.cboOperators.selectedIndex;
				populateListbox(index);
			}
			
			function populateListbox(pIndex)
			{
				var tempArray = new Array;
				switch(pIndex)
				{
				case 0:
					tempArray = document.frmData.hdArithmeticFunction.value.split("|||||");
					document.frmData.lstOperators.length = 0;
					for(i=0;i<tempArray.length;i++)
					{
						add_item(document.frmData.lstOperators,tempArray[i]);
					}
					break;
				case 1:
					tempArray = document.frmData.hdStrFunction.value.split("|||||");
					document.frmData.lstOperators.length = 0;
					for(i=0;i<tempArray.length;i++)
					{
						add_item(document.frmData.lstOperators,tempArray[i]);
					}
					tempArray.length = 0;
					break;
				case 2:
					tempArray = document.frmData.hdDateTimeFunction.value.split("|||||");
					document.frmData.lstOperators.length = 0;
					for(i=0;i<tempArray.length;i++)
					{
						add_item(document.frmData.lstOperators,tempArray[i]);
					}
					break;
				case 3:
					tempArray = document.frmData.hdDataAccessFunction.value.split("|||||");
					document.frmData.lstOperators.length = 0;
					for(i=0;i<tempArray.length;i++)
					{
						add_item(document.frmData.lstOperators,tempArray[i]);
					}
					break;
				case 4:
					tempArray = document.frmData.hdSystemWideParamFunction.value.split("|||||");
					document.frmData.lstOperators.length = 0;
					for(i=0;i<tempArray.length;i++)
					{
						add_item(document.frmData.lstOperators,tempArray[i]);
					}
					break;
				case 5:
					tempArray = document.frmData.hdGCParamFunction.value.split("|||||");
					document.frmData.lstOperators.length = 0;
					for(i=0;i<tempArray.length;i++)
					{
						add_item(document.frmData.lstOperators,tempArray[i]);
					}
					break;
				case 6:
					tempArray = document.frmData.hdWCParamFunction.value.split("|||||");
					document.frmData.lstOperators.length = 0;
					for(i=0;i<tempArray.length;i++)
					{
						add_item(document.frmData.lstOperators,tempArray[i]);
					}
					break;
				case 7:
					tempArray = document.frmData.hdVAParamFunction.value.split("|||||");
					document.frmData.lstOperators.length = 0;
					for(i=0;i<tempArray.length;i++)
					{
						add_item(document.frmData.lstOperators,tempArray[i]);
					}
					break;
				case 8:
					tempArray = document.frmData.hdMiscFunction.value.split("|||||");
					document.frmData.lstOperators.length = 0;
					for(i=0;i<tempArray.length;i++)
					{
						add_item(document.frmData.lstOperators,tempArray[i]);
					}
					break;
				}

			}
			
			function populateDetailFormula(pIndex,pObj)
			{
				var tempArray = new Array;
				var position = '';
				position = document.frmData.lstOperators.selectedIndex;
				switch(pIndex)
				{
				case 0:
					tempArray = document.frmData.hdArithmeticString.value.split("|||||");
					
					//-- Rajeev -- 09/30/2003 -- Fix for integer division
					if (position == 6)
						tempArray[position] = '\\';
					
					if (pObj.value == '')
						pObj.value = tempArray[position]; 
					else
						pObj.value = pObj.value + " " + tempArray[position];
					break;
				case 1:
					tempArray = document.frmData.hdStrString.value.split("|||||");
					if (pObj.value == '')
						pObj.value = tempArray[position];
					else
						pObj.value = pObj.value + " " + tempArray[position];
					break;
				case 2:
					tempArray = document.frmData.hdDateTimeString.value.split("|||||");
					if (pObj.value == '')
						pObj.value = tempArray[position];
					else
						pObj.value = pObj.value + " " + tempArray[position];
					break;
				case 3:
					tempArray = document.frmData.hdDataAccessString.value.split("|||||");
					tempArray[position] = tempArray[position].replace('()','("")');
					if (pObj.value == '')
						pObj.value = tempArray[position];
					else
						pObj.value = pObj.value + " " + tempArray[position];
					break;
				case 4:
					tempArray = document.frmData.hdSystemWideParamString.value.split("|||||");
					if (pObj.value == '')
						pObj.value = tempArray[position];
					else
						pObj.value = pObj.value + " " + tempArray[position];
					break;
				case 5:
					tempArray = document.frmData.hdGCParamString.value.split("|||||");
					tempArray[position] = tempArray[position].replace('(GC)','("GC")');
					if (pObj.value == '')
						pObj.value = tempArray[position];
					else
						pObj.value = pObj.value + " " + tempArray[position];
					break;
				case 6:
					tempArray = document.frmData.hdWCParamString.value.split("|||||");
					tempArray[position] = tempArray[position].replace('(WC)','("WC")');
					if (pObj.value == '')
						pObj.value = tempArray[position];
					else
						pObj.value = pObj.value + " " + tempArray[position];
					break;
				case 7:
					tempArray = document.frmData.hdVAParamString.value.split("|||||");
					tempArray[position] = tempArray[position].replace('(VA)','("VA")');
					if (pObj.value == '')
						pObj.value = tempArray[position];
					else
						pObj.value = pObj.value + " " + tempArray[position];
					break;
				case 8:
					tempArray = document.frmData.hdMiscString.value.split("|||||");
					if (pObj.value == '')
						pObj.value = tempArray[position];
					else
						pObj.value = pObj.value + " " + tempArray[position];
					break;
				}
			}
			
			function clickFunctionListbox()
			{
				latest = document.activeElement.name;
				backTrack = latest;
				var index = document.frmData.cboOperators.selectedIndex;
				
				if (trackListBox == 'detail' || trackListBox == '')
					Obj = document.frmData.txtDetailFormula;
				else if (trackListBox == 'aggregate')
					Obj = document.frmData.txtAggregateFormula;
				
				populateDetailFormula(index,Obj);
			}
			
			function mapDataType(pDataType)
			{
				//return false;
				switch(parseInt(pDataType))
				{
				case 0:
					arrDataTypeValue[arrDataTypeValue.length] = "Text";
					break;
				case 1:
					arrDataTypeValue[arrDataTypeValue.length] = "Number";
					break;
				case 2:
					arrDataTypeValue[arrDataTypeValue.length] = "Date";
					break;
				case 3:
					arrDataTypeValue[arrDataTypeValue.length] = "Code";
					break;
				case 4:
					arrDataTypeValue[arrDataTypeValue.length] = "State";
					break;
				case 5:
					arrDataTypeValue[arrDataTypeValue.length] = "Boolean";
					break;
				case 6:
					arrDataTypeValue[arrDataTypeValue.length] = "Time";
					break;
				case 7:
					arrDataTypeValue[arrDataTypeValue.length] = "Date/Time";
					break;
				case 8:
					arrDataTypeValue[arrDataTypeValue.length] = "Memo";
					break;
				case 9:
					arrDataTypeValue[arrDataTypeValue.length] = "Count";
					break;
				case 10:
					arrDataTypeValue[arrDataTypeValue.length] = "Code Desc";
					break;
				case 11:
					arrDataTypeValue[arrDataTypeValue.length] = "Unformatted Count";
				}

				return;
			}
			
			function populateFields(pIndex)
			{
				var fieldValue;
				fieldValue = arrFieldName[pIndex] + ":" + arrDataTypeValue[pIndex];
				add_item(window.document.frmData.lstFields,fieldValue);				
			}
			
			function clickFieldListbox()
			{
				if (trackListBox == 'detail' || trackListBox == '')
					Obj = document.frmData.txtDetailFormula;
				else if (trackListBox == 'aggregate')
					Obj = document.frmData.txtAggregateFormula;
				
				if (document.frmData.txtDetailFormula.value == '')
					Obj.value = unescape('Current("' + trimIt(arrFieldName[document.frmData.lstFields.selectedIndex]) + '")');
				else
					Obj.value = unescape(Obj.value + ' ' + 'Current("' + trimIt(arrFieldName[document.frmData.lstFields.selectedIndex]) + '")');
			}
			
			function changeDataType(pWidth)
			{
				if (document.frmData.cboDataType.selectedIndex == 0)
				{
					document.frmData.txtWidth.disabled = false;
					if (document.frmData.hdAddEdit.value == "Add")
						document.frmData.txtWidth.value = 4;
					else if(document.frmData.cboDataType.selectedIndex == 0 && pWidth == -1)
						document.frmData.txtWidth.value = 4;
					else
						document.frmData.txtWidth.value = pWidth;
				}
				else
				{
					document.frmData.txtWidth.disabled = true;
					document.frmData.txtWidth.value = '';
				}
			}
			
			function done_click()
			{
				
				if ((trimIt(sSelAggregateCalcMethodAttribute) == 'differentformula') && (document.frmData.txtAggregateFormula.value == ''))
				{
					alert("Please enter a formula for the aggregate rows.");
					return false;
				}
				
				//-- Check if a non existent field has been added to detail formula(Current & Previous functions)
				var arrValues = new Array;
				var bFound;
				arrValues.length = 0;
				var sTemp = trimIt(document.frmData.txtDetailFormula.value);
				var indexCurrent = sTemp.indexOf('Current("');
				var indexPrevious = sTemp.indexOf('Previous("');
				
				if (indexCurrent > -1)
				{
					bFound = 1;
					increment = 9;
					index = indexCurrent;
				}
				else if (indexPrevious > -1)
				{
					bFound = 1;
					increment = 10;
					index = indexPrevious;
				}
				else
					bFound = 0;
					
				while (bFound == 1)
				{
					sTemp = sTemp.substr(index+increment,sTemp.length);
					arrValues[arrValues.length] = sTemp.substr(0,sTemp.indexOf('"'))
					
					indexCurrent = sTemp.indexOf('Current("');
					indexPrevious = sTemp.indexOf('Previous("');
					
					if (indexCurrent > -1)
					{
						bFound = 1;
						increment = 9;
						index = indexCurrent;
					}
					else if (indexPrevious > -1)
					{
						bFound = 1;
						increment = 10;
						index = indexPrevious;
					}
					else
						bFound = 0;
				}
				
				bFound = 0;
				
				for(i=0;i<arrValues.length;i++)
				{
					for(j=0;j<arrFieldName.length;j++)
					{
						if (trimIt(arrValues[i]) == trimIt(arrFieldName[j]))
						{
							bFound = 1;
							break;
						}
						else
							bFound = 0;
					}
					
					if (bFound == 0)
					{
						alert('Formula has reference to a column that was not added.' + String.fromCharCode(13, 10) + 'Please exit this window,add the column and recreate the derived field.');
						return false;
					}
				}
				
				if (document.frmData.txtAggregateFormula.value.indexOf('Previous("') > -1)
				{
					alert('Formula for aggregate rows contains reference to Previous Object.' + String.fromCharCode(13, 10) + 'Please reenter the formula without a reference to Previous Object');
					return false;
				}
				
				var sFieldType; 
				if (window.document.frmData.cboDataType.selectedIndex == 0)
					sFieldType = "text";
				else if(window.document.frmData.cboDataType.selectedIndex == 1)
					sFieldType = "numeric";
				else if(window.document.frmData.cboDataType.selectedIndex == 2)
					sFieldType = "formattednumeric";
				else if(window.document.frmData.cboDataType.selectedIndex == 3)
					sFieldType = "currency";
				else if(window.document.frmData.cboDataType.selectedIndex == 4)
					sFieldType = "date";
				else if(window.document.frmData.cboDataType.selectedIndex == 5)
					sFieldType = "time";
				else if(window.document.frmData.cboDataType.selectedIndex == 6)
					sFieldType = "datetime";
				window.opener.window.document.frmData.hdDerFieldType.value = sFieldType;
				
				if (document.frmData.cboDataType.selectedIndex == 0)
					window.opener.window.document.frmData.hdDerFieldWidth.value = document.frmData.txtWidth.value;
				else
					window.opener.window.document.frmData.hdDerFieldWidth.value = 4;
				
				window.opener.window.document.frmData.hdDerDetailFormula.value = document.frmData.txtDetailFormula.value;
				window.opener.window.document.frmData.hdSubmit.value = "DERIVED"
				window.opener.window.document.frmData.hdDerAddEdit.value = document.frmData.hdAddEdit.value;
				
				if (document.frmData.hdAddEdit.value == "Add")
					window.opener.window.document.frmData.hdDerIndex.value = -1;
				else
					window.opener.window.document.frmData.hdDerIndex.value = document.frmData.hdIndex.value;
				
				//-- Check for Aggregate Formula
				if (document.frmData.optAggregate[0].checked == true)
				{
					window.opener.window.document.frmData.hdDerAggregate.value = 'Standard';
					window.opener.window.document.frmData.hdDerAggregateFormula.value = '';
				}
				else if (document.frmData.optAggregate[1].checked == true)
				{
					window.opener.window.document.frmData.hdDerAggregate.value = 'sameformula';
					window.opener.window.document.frmData.hdDerAggregateFormula.value = document.frmData.txtDetailFormula.value;
				}
				else if (document.frmData.optAggregate[2].checked == true)
				{
					window.opener.window.document.frmData.hdDerAggregate.value = 'differentformula';
					window.opener.window.document.frmData.hdDerAggregateFormula.value = document.frmData.txtAggregateFormula.value;
				}
				
				//-- Check if column is involved in derived field or not
				window.opener.window.document.frmData.hdDerColumnInvolved.value = window.document.frmData.hdColumnInvolved.value + '  ' + document.frmData.txtDetailFormula.value;
				window.opener.tabNavigation(window.document.frmData.hdTabID.value,window.document.frmData.hdTabID.value);
				window.close();		
			}
			
			function selStandardFormula()
			{
				sSelAggregateCalcMethodAttribute = 'Standard';
				document.frmData.txtAggregateFormula.value = '';
				document.frmData.txtAggregateFormula.disabled = true; 
			}
			
			function selSameFormula()
			{
				sSelAggregateCalcMethodAttribute = 'sameformula';
				document.frmData.txtAggregateFormula.value = document.frmData.txtDetailFormula.value;
				document.frmData.txtAggregateFormula.disabled = true; 
			}
			
			function selSeparateFormula()
			{
				sSelAggregateCalcMethodAttribute = 'differentformula';
				document.frmData.txtAggregateFormula.value = document.frmData.txtDetailFormula.value;
				document.frmData.txtAggregateFormula.disabled = false; 
			}
			
			function activateAggregateFormula()
			{
				trackListBox = 'aggregate';
			}
			
			function activateDetailFormula()
			{
				trackListBox = 'detail';
			}
			
		</script>		
	</head>

	<body onLoad="window_onLoad();">
	<form name="frmData" topmargin="0" leftmargin="0" onSubmit="return false;">
		<table class="formsubtitle" border="0" width="100%" align="center">
			<tr>
				<td class="ctrlgroup" width="33%">Add/Edit Derived Fields :</td>
			</tr>
		</table>
			
		<table width="100%" border="0" cellspacing="0">
			<tr>
				<td width="50%"><b>Fields</b></td>
				<td width="50%"><b>Functions/Operators</b></td>
			</tr>
			<tr>
				<td rowspan="2">
					<select name="lstFields" multiple size ="6" style="width:100%" ondblClick="clickFieldListbox();">
					</select>
				</td>
				<td>
					<select name="cboOperators" height="1" style="width:100%" onChange="changeFunction();" >
					<% 
						If Err.number <> 9 And mOperators <> "" And mOperators >= 0 Then
							For i = 0 To UBound(arrOperators)
								If i=0 then 
									Response.Write "<option selected>" & arrOperators(i) & "</option>"
								Else
									Response.Write "<option>" & arrOperators(i) & "</option>"
								End If
							Next
						End If
					%>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					<select name="lstOperators" multiple size="4" style="width:100%" ondblClick="clickFunctionListbox();">
					</select>
				</td>
			</tr>
						
			<tr>
				<td>Datatype:&nbsp;
					<select name="cboDataType" height="1" style1 ="WIDTH: 35%" onChange="changeDataType(-1);">
					<% 
						If Err.number <> 9 And mDatatype <> "" And mDatatype >= 0 Then
							For i = 0 To UBound(arrDataType)
								If i=0 Then 
									Response.Write "<option selected>" & arrDataType(i) & "</option>"
								Else
									Response.Write "<option>" & arrDataType(i) & "</option>"
								End If
							Next
						End If
					%>
					</select>&nbsp;&nbsp;Width:&nbsp;
					<input size="16" name="txtWidth">
				</td>
			</tr>
						
			<tr>
				<td colspan="2"><b>Detail Formula</b></td>
			</tr>
			<tr>
				<td colspan="2"><textarea name=txtDetailFormula rows=4 cols1=85 style="width:100%" onFocus="activateDetailFormula();"></textarea>
				</td>
			</tr>
		</table>
		
		<table class="formsubtitle" border="0" width="100%" align="center">
			<tr>
				<td class="ctrlgroup" width="33%">Advanced &gt;&gt;  : Aggregate</td>
			</tr>
		</table>
			
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td><input type="radio" value="" name="optAggregate" onClick="selStandardFormula();">Standard</td>
				<td><input type="radio" value="" name="optAggregate" onClick="selSameFormula();">Same Formula</td>
				<td><input type="radio" value="" name="optAggregate" onClick="selSeparateFormula();">Separate Formulae</td>
			</tr>
			<tr>
				<td colspan="3"><b>Aggregate Formula</b></td>
			</tr>
			<tr>
				<td colspan="3"><textarea name=txtAggregateFormula rows=4 cols1=85 style="width:100%" onFocus="activateAggregateFormula();"></textarea></td>
			</tr>
		</table>
		<hr>
	
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="20%">&nbsp;</td>
				<td width="60%" align="middle">
					<input type="button" class="button" name="btnDone" value="Done" style1="width:25%" onClick="done_click();">
					<input type="button" class="button" name="btnCancel" value="Cancel" style1="width:25%" onClick="window.close()">
					<input type="button" class="button" name="btnHelp" value="Help" style1="width:25%">
				</td>
				<td width="20%">&nbsp;</td>
			</tr>
		</table>
		<input type="hidden" name="hdArithmeticFunction" value="<%=a%>"> 
		<input type="hidden" name="hdArithmeticString" value="<%=sArithmeticString%>"> 
		<input type="hidden" name="hdStrFunction" value="<%=b%>"> 
		<input type="hidden" name="hdStrString" value="<%=sStrString%>"> 
		<input type="hidden" name="hdDateTimeFunction" value="<%=c%>"> 
		<input type="hidden" name="hdDateTimeString" value="<%=sDateTimeString%>"> 
		<input type="hidden" name="hdDataAccessFunction" value="<%=d%>"> 
		<input type="hidden" name="hdDataAccessString" value="<%=sDataAccessString%>"> 
		<input type="hidden" name="hdSystemWideParamFunction" value="<%=e%>"> 
		<input type="hidden" name="hdSystemWideParamString" value="<%=sSystemWideParamString%>"> 
		<input type="hidden" name="hdGCParamFunction" value="<%=f%>"> 
		<input type="hidden" name="hdGCParamString" value="<%=sGCParamString%>"> 
		<input type="hidden" name="hdWCParamFunction" value="<%=g%>"> 
		<input type="hidden" name="hdWCParamString" value="<%=sWCParamString%>"> 
		<input type="hidden" name="hdVAParamFunction" value="<%=h%>"> 
		<input type="hidden" name="hdVAParamString" value="<%=sVAParamString%>"> 
		<input type="hidden" name="hdMiscFunction" value="<%=k%>"> 
		<input type="hidden" name="hdMiscString" value="<%=sMiscString%>"> 
		<input type="hidden" name="hdFieldType" value="<%=sFieldType%>"> 
		<input type="hidden" name="hdFieldName" value="<%=sArrayP%>"> 
		<input type="hidden" name="hdDerivedFieldName" value="<%=fixDoubleQuote(Request.QueryString("usercolname"))%>"> 
		<input type="hidden" name="hdDerivedFieldType" value="<%=sDerFieldType%>"> 
		<input type="hidden" name="hdSelFieldsXML" value='<%=tokenSingleQuote(objSessionStr("ArrayPXML"))%>'>
		<input type="hidden" name="hdIndex" value="<%=index%>"> 
		<input type="hidden" name="hdAddEdit" value="<%=Request.QueryString("UpdateMode")%>"> 
		<input type="hidden" name="hdColumnInvolved" value='<%=sColumnInvolved%>'>

		<input type="hidden" name="hdTabID" value="<%=Request.QueryString("TABID")%>">

	</form>
	</body>
</html>