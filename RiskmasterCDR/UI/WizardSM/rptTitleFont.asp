<html>
	<head>
		<link rel="stylesheet" href="RMNet.css" type="text/css" />
		<title>Title Font Properties</title>
		<script language="JavaScript">
			function reportTitleFontSubmit()
			{
				if (window.document.frmData.select1.selectedIndex == -1)
					window.document.frmData.select1.selectedIndex = 0;
					
				window.opener.frmData.hdRptTitleFont.value=window.document.frmData.select1.options[window.document.frmData.select1.selectedIndex].text ;
				window.opener.frmData.hdRptTitleSize.value=window.document.frmData.select3.options[window.document.frmData.select3.selectedIndex].text + ".000000";
				if (window.document.frmData.select2.options[window.document.frmData.select2.selectedIndex].text=="Regular")
				{
					window.opener.frmData.hdRptTitleStyle1.value="0"
					window.opener.frmData.hdRptTitleStyle2.value="0"
				}
				if (window.document.frmData.select2.options[window.document.frmData.select2.selectedIndex].text=="Italic")
				{
					window.opener.frmData.hdRptTitleStyle1.value="0"
					window.opener.frmData.hdRptTitleStyle2.value="1"
				}
				if (window.document.frmData.select2.options[window.document.frmData.select2.selectedIndex].text=="Bold") 
				{
					window.opener.frmData.hdRptTitleStyle1.value="1"
					window.opener.frmData.hdRptTitleStyle2.value="0"
				}
				if (window.document.frmData.select2.options[window.document.frmData.select2.selectedIndex].text=="Bold Italic") 
				{
					window.opener.frmData.hdRptTitleStyle1.value="1"
					window.opener.frmData.hdRptTitleStyle2.value="1"
				}

				if (window.document.frmData.chkStrikeOut.checked)
					window.opener.frmData.hdStrikeOut.value = "1"
				else
					window.opener.frmData.hdStrikeOut.value = "0"
				
				if (window.document.frmData.chkUnderLine.checked)
					window.opener.frmData.hdUnderLine.value = "1"
				else
					window.opener.frmData.hdUnderLine.value = "0"
					
				window.opener.frmData.hdTitleFontColor.value = window.document.frmData.hdColor.value;
				
				window.opener.frmData.hdTitleFontRColor.value = window.document.frmData.hdRColor.value;
				window.opener.frmData.hdTitleFontGColor.value = window.document.frmData.hdGColor.value;
				window.opener.frmData.hdTitleFontBColor.value = window.document.frmData.hdBColor.value;
				
				window.close();
			}

			function body_onload()
			{
				for(i=0;i<document.getElementById("select1").length;i++)
				{
					if(window.opener.frmData.hdRptTitleFont.value == "")
					{
					document.getElementById("select1").options(0).selected = true;
					break;
					}		
					else if(document.getElementById("select1").options(i).text == window.opener.frmData.hdRptTitleFont.value)
					{
					document.getElementById("select1").options(i).selected = true;
					break;
					}
				}
				
				if (window.document.frmData.select1.selectedIndex == -1)
					window.document.frmData.select1.selectedIndex = 0;
					
				for(i=0;i<document.getElementById("select3").length;i++)
				{
					if(window.opener.frmData.hdRptTitleSize.value == "")
						document.getElementById("select3").options(0).selected = true;
					else if(document.getElementById("select3").options(i).text == window.opener.frmData.hdRptTitleSize.value.substring(0,window.opener.frmData.hdRptTitleSize.value.indexOf(".")))
						document.getElementById("select3").options(i).selected = true;
				}
				
				if(window.opener.frmData.hdRptTitleStyle1.value=="" && window.opener.frmData.hdRptTitleStyle2.value=="")
					document.getElementById("select2").options(0).selected = true;
				
				if(window.opener.frmData.hdRptTitleStyle1.value=="0" && window.opener.frmData.hdRptTitleStyle2.value=="0")
					document.getElementById("select2").options(0).selected = true;
				
				if(window.opener.frmData.hdRptTitleStyle1.value=="0" && window.opener.frmData.hdRptTitleStyle2.value=="1")
					document.getElementById("select2").options(1).selected = true;
				
				if(window.opener.frmData.hdRptTitleStyle1.value=="1" && window.opener.frmData.hdRptTitleStyle2.value=="0")
					document.getElementById("select2").options(2).selected = true;
				
				if(window.opener.frmData.hdRptTitleStyle1.value=="1" && window.opener.frmData.hdRptTitleStyle2.value=="1")
					document.getElementById("select2").options(3).selected = true;
				
				if(window.opener.frmData.hdStrikeOut.value == "1")
					document.frmData.chkStrikeOut.checked = true;
				else
					document.frmData.chkStrikeOut.checked = false;
					
				if(window.opener.frmData.hdUnderLine.value == "1")
					document.frmData.chkUnderLine.checked = true;
				else
					document.frmData.chkUnderLine.checked = false;
				
				window.document.frmData.hdColor.value = window.opener.frmData.hdTitleFontColor.value;
				
				if (window.opener.frmData.hdTitleFontRColor.value != '')
				{
				window.document.frmData.hdRColor.value = window.opener.frmData.hdTitleFontRColor.value;
				window.document.frmData.hdGColor.value = window.opener.frmData.hdTitleFontGColor.value;
				window.document.frmData.hdBColor.value = window.opener.frmData.hdTitleFontBColor.value;
				}
				
				//-- Handling the color sample
				hexcode = toHex(parseInt(window.document.frmData.hdColor.value))
				hexb = hexcode.substring(0,2);
				hexg = hexcode.substring(2,4);
				hexr = hexcode.substring(6,4);
				
				if (hexr == "" && hexg == "")
					document.frmData.txtSample.style.color = toDec(hexb + '00' + '00');
				else if (hexr == "")
					document.frmData.txtSample.style.color = toDec(hexg + hexb + '00');
				else
					document.frmData.txtSample.style.color = toDec(hexr + hexg + hexb);
					
				changeFontSize();
				changeFontStyle();
				changeFontType();
				underline_click();
				strikeout_click();
			}

			function changeColor(sColor,pSampleColor)
			{
				document.frmData.txtSample.style.color = pSampleColor;
				return;
			}

			function updateSerialNum()
			{
				window.opener.checkSerialNum(0);
			}

			function underline_click()
			{
				if (document.frmData.chkUnderLine.checked)
					document.frmData.txtSample.style.textDecorationUnderline = true;
				else 
					document.frmData.txtSample.style.textDecorationUnderline = false;
			} 

			function strikeout_click()
			{
				if (document.frmData.chkStrikeOut.checked)
					document.frmData.txtSample.style.textDecorationLineThrough = true;
				else 
					document.frmData.txtSample.style.textDecorationLineThrough = false;
			} 

			function changeFontSize()
			{
				if (document.frmData.select3.selectedIndex == 0)
					document.frmData.txtSample.style.fontSize = 8;
				else if (document.frmData.select3.selectedIndex == 1)
					document.frmData.txtSample.style.fontSize = 9;
				else if (document.frmData.select3.selectedIndex == 2)
					document.frmData.txtSample.style.fontSize = 10;
				else if (document.frmData.select3.selectedIndex == 3)
					document.frmData.txtSample.style.fontSize = 11;
				else if (document.frmData.select3.selectedIndex == 4)
					document.frmData.txtSample.style.fontSize = 12;
				else if (document.frmData.select3.selectedIndex == 5)
					document.frmData.txtSample.style.fontSize = 14;
				else if (document.frmData.select3.selectedIndex == 6)
					document.frmData.txtSample.style.fontSize = 16;
				else if (document.frmData.select3.selectedIndex == 7)
					document.frmData.txtSample.style.fontSize = 18;
			}

			function changeFontStyle()
			{
				if (document.frmData.select2.selectedIndex == 0)
				{
					document.frmData.txtSample.style.fontStyle = "normal";
					document.frmData.txtSample.style.fontWeight = "";
				}
				else if (document.frmData.select2.selectedIndex == 1)
				{
					document.frmData.txtSample.style.fontStyle = "italic";
					document.frmData.txtSample.style.fontWeight = "";
				}
				else if (document.frmData.select2.selectedIndex == 2)
				{
					document.frmData.txtSample.style.fontStyle = "";
					document.frmData.txtSample.style.fontWeight = "bolder";
				}
				else if (document.frmData.select2.selectedIndex == 3)
				{
					document.frmData.txtSample.style.fontStyle = "italic";
					document.frmData.txtSample.style.fontWeight = "bolder";
				}
			}

			function changeFontType()
			{
				if (document.frmData.select1.selectedIndex == 0)
					document.frmData.txtSample.style.fontFamily = "couriernew";
				else if (document.frmData.select1.selectedIndex == 1)
					document.frmData.txtSample.style.fontFamily = "arial";
			}

			function toHex(decimal_number)
			//feed it a decimal number and it spits back a 2 digit hex number
			{
				var prefix = (decimal_number < 16) ? '0' : '';
				hex_number = decimal_number.toString(16);
				hex_number = hex_number.toUpperCase();
				return prefix + hex_number;
			}

			function toDec(p_HexCode) 
			{ 
				return parseInt(p_HexCode,16); 
			}

		</script>
	</head>
	
	<body class="8pt" onload="JavaScript:body_onload()">
	<form name="frmData" method="post"> 
		<table class="formsubtitle" border="0" width="100%">
			<tr>
				<td class="ctrlgroup" >Font</td>
			</tr>
		</table>
		
		<table border="0" width="80%" align="center">
			<tr>
				<td align="middle" style="width:30%">
					Font
				</td>
				<td align="middle" style="width:30%">
					Font Style
				</td>
				<td align="middle" style="width:30%">
					Size
				</td>
			</tr>
			<tr>
				<td align="middle">
					<SELECT size=6 name=select1 onChange="updateSerialNum();" onClick="changeFontType()"> 
						<OPTION value="0">Courier New</OPTION>
						<OPTION value="1">Arial</OPTION>
					</SELECT>
				</td>
				<td align="middle">
					<SELECT size=6 name=select2 onChange="updateSerialNum();" onClick="changeFontStyle()"> 
						<OPTION>Regular</OPTION>
						<OPTION>Italic</OPTION>
						<OPTION>Bold</OPTION>
						<OPTION>Bold Italic</OPTION>
					</SELECT>
				</td>
				<td align="middle">
					<SELECT size=6 name=select3 onChange="updateSerialNum();" onClick="changeFontSize();"> 
						<OPTION>8</OPTION>
						<OPTION>9</OPTION>
						<OPTION>10</OPTION>
						<OPTION>11</OPTION>
						<OPTION>12</OPTION>
						<OPTION>14</OPTION>
						<OPTION>16</OPTION>
						<OPTION>18</OPTION>
					</SELECT>
				</td>
			</tr>
		</table>
		<br>
		<hr>
		<table class="formsubtitle" border="0" width="100%">
			<tr>
				<td class="ctrlgroup">Effects</td>
			</tr>
		</table>

		<table width="60%" border="0" align="center">
			<tr height="34%">
				<td align="center" colspan="5" rowspan="2"><input type="text" width="100%" name="txtSample"  value="SAMPLE TEXT" readonly style="BORDER-RIGHT: red 1px solid;BORDER-LEFT: red 1px solid;BORDER-TOP: red 1px solid;BORDER-BOTTOM: red 1px solid;TEXT-ALIGN:center" ></td>
			</tr>
			<tr height="33%">
				<td></td>
			</tr>
			<tr height="33%">
				<td align="middle">
					<INPUT  type=checkbox name=chkStrikeOut onClick="updateSerialNum();strikeout_click();">
				</td>
				<td>
					Strikeout
				</td>
				<td align="middle">
					<INPUT  type=checkbox name=chkUnderLine onClick="updateSerialNum();underline_click();">
				</td>
				<td>
					Underline
				</td>
				<td align="middle">
					<INPUT id=button1 type=button value=" Color " name=button1 class="button" style="width:100%" onClick="window.open('colorpicker.asp?CHECKWINDOW='+'rpttitlefont','colpick','resizable=yes,Width=340,Height=360,top='+(screen.availHeight-360)/2+',left='+(screen.availWidth-340)/2+'')">
				</td>
			</tr>
		</table>
		<br>
		<hr>
		<table width="100%" border="0">
			<tr>
				<td width="20%">&nbsp;</td>
				<td width="60%" align="center">
					<input type="button" class="button" style="width:20%" name="up" value="OK" onClick="reportTitleFontSubmit()">
					<input type="button" class="button" style="width:20%" name="up" value="Cancel" onClick="javascript:window.close()">
				</td>
				<td width="20%">&nbsp;</td>
			</tr>
		</table>
		
		<input type=hidden name="hdColor" value="">
		<input type=hidden name="hdRColor" value="">
		<input type=hidden name="hdGColor" value="">
		<input type=hidden name="hdBColor" value="">
	</form>
	</body>
</html>
