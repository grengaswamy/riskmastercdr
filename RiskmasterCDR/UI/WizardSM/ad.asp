<!-- #include file ="appconstantsconfig.inc" -->
<%Option Explicit%>
<html>
<head>
	<title>Access Denied</title>
	<!--[Rajeev 11/15/2002 CSS Consolidation] link rel="stylesheet" href="forms.css" type="text/css"/-->
	<link rel="stylesheet" href="RMNet.css" type="text/css"/>
	<script language="JavaScript" SRC="form.js"></script>

<SCRIPT LANGUAGE="JAVASCRIPT">
function CheckHistory()
{
	if (self.history.length > 0)
		self.history.back();
	else
		window.close();
		
	return true;
}
</SCRIPT>
</head>
<!--[Rajeev 12/20/2002 -- MITS#3899 [Added onLoad()]-->
<body class="10pt" onLoad="CloseProgressWindow();">

<br /><br /><br /><br />
<h3 align="center"><font color="red">Access Denied</font></h3>
<p align="center">
	You don't have sufficient rights to access requested resource.
	<br><%=ERR_CONTACT%>
</p>

	<CENTER>
	<input name="btnBack" type="button" class="button" value="  Back  " onClick="return CheckHistory()" />
	</CENTER>
</body>
</html>