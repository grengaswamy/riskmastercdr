<% Option Explicit %>
<!-- #include file ="session.inc" -->
<!-- #include file ="generic.asp" -->

<html>
	<head>
		<title>SMD - [Criteria]</title>
		<link rel="stylesheet" href="RMNet.css" type="text/css"/>
<%
Dim objData, m, pageError
Set objData = Nothing
On Error Resume Next
Set objData = Server.CreateObject("InitSMList.CDataManager")
objData.m_RMConnectStr = Application("SM_DSN")

On Error Goto 0

If objData Is Nothing Then
	Call ShowError(pageError, "Call to CDataManager class failed.", True)
End If


Dim arrDateFormulae()
objData.GetDateFormulae arrDateFormulae

Set objData = Nothing

Dim sDateFormula, i

Err.Clear()
On Error Resume Next
m = -1
m = UBound(arrDateFormulae)
On Error Goto 0

If Err.number <> 9 And m <> "" And m >=0 Then
	For i = 0 To UBound(arrDateFormulae) 
		If i = 0 Then 
			sDateFormula = arrDateFormulae(0) 
		Else 
			sDateFormula = sDateFormula & "^^^^^" & arrDateFormulae(i) 
		End If 
	Next
End If


%>

		
		
		<script language="JavaScript" SRC="SMD.js"></script>
		<script language="JavaScript">
			var btnClick;
			btnClick = ""
			var trackSubquery;
			trackSubquery = 1;
			var txtNull;
			var txtEnter;
			var pAttributes;
			var operatorList;
			operatorList = '';
			var connectorList;
			connectorList = '';
			var arrData = new Array;
			arrData.length = 0;
			var pOperator = new Array;
			pOperator.length = 0;
			var arrRelFormulaName = new Array;
			var arrRelFormulaValue = new Array;
			//-- When AND / OR Button is clicked
			function AND_OR_Click(pType)
			{	
				var txtVal;
				if(trimIt(window.document.frmData.txtDtofevnt.value) != '')
				{
					dtArray = window.document.frmData.txtDtofevnt.value.split("/");
					dtVal = dtArray[2] + dtArray[0] + dtArray[1];
				}
				else
					dtVal = '';
					
				txtVal = dtVal;
				
				//-- Code to handle change
				if(document.frmData.btnDelete.disabled == false && document.frmData.btnAnd.disabled == false && document.frmData.btnOr.disabled == false)
				{
					if(formatDate(arrData[document.frmData.lstCriteria.selectedIndex]) == document.frmData.txtDtofevnt.value)
					{
						document.frmData.btnDelete.disabled = true;
						document.frmData.txtDtofevnt.value = '';
						btnClick=pType;
						return;
					}
					else
					{
						check(1);
						btnClick=pType;
						return;
					}
				}
					
				if(document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == false && trimIt(document.frmData.txtDtofevnt.value) != '' && trackSubquery == 1)
				{
					document.frmData.txtDtofevnt.value = '';
					document.frmData.btnDelete.disabled = true;
				}

				if(document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)
					check(1);
				else
				{
					if((document.frmData.cboOperators.selectedIndex == 14 || document.frmData.cboOperators.selectedIndex == 15) && trackSubquery == 0)
					{
						arrData[arrData.length] = '';
						check(0);
					}
					else if(trackSubquery == 0 && trimIt(document.frmData.txtDtofevnt.value) == '')
					{
						arrData[arrData.length] = txtVal;
						check(0);
					}
					else if(trackSubquery == 0 && trimIt(document.frmData.txtDtofevnt.value) != '' && document.frmData.btnDelete.disabled == false) //Selecting subquery criteria in edit mode.
						check(1);
					else if(trimIt(document.frmData.txtDtofevnt.value) != "")// || trackSubquery == 0)
					{
						arrData[arrData.length] = txtVal;
						check(0);
					}
					
				}
				document.frmData.txtDtofevnt.value="";
				document.frmData.btnChange.disabled = true;
				document.frmData.btnDelete.disabled = true;
				trackSubquery = 1;
				btnClick=pType;
			}	
	
			function Subquery()
			{
				trackSubquery=0;
				if(btnClick=="OR")
					btnClick="OR";
				else if(btnClick=="AND")
					btnClick="AND";		
				else
					btnClick="";	
			}
	
			function add_item(txt)
			{
				var sNewItem;
				sNewItem = new Option(txt);
				if (navigator.appName == "Netscape")
					window.document.frmData.lstCriteria.add(sNewItem, null);
				else
					window.document.frmData.lstCriteria.add(sNewItem);
			}
	
			function add_item_relative_date(txt)
			{
				var sNewItem;
				sNewItem = new Option(txt);
				if (navigator.appName == "Netscape")
					window.document.frmData.cboDay.add(sNewItem, null);
				else
					window.document.frmData.cboDay.add(sNewItem);
			}

			function check(pAdd)
			{
				var txt, index, FieldName,txtVal;
				var oOption,selConnector;
				index = document.frmData.cboOperators.selectedIndex;
				FieldName = window.document.frmData.hdFieldName.value;
				
				if(window.document.frmData.txtDtofevnt.value != '')
				{
					dtArray = window.document.frmData.txtDtofevnt.value.split("/");
					dtVal = dtArray[2] + dtArray[0] + dtArray[1];
				}
				else
					dtVal = '';
					
				txtVal = dtVal;
		
				if(document.frmData.chkOLE.checked && (document.frmData.cboOperators.selectedIndex != 14 && document.frmData.cboOperators.selectedIndex != 15))
					txtVal = 'OLECRIT';
		
		
				switch(index)
				{
					case 0:	txt="<";
					
							if(pAdd == 0)
							{
								pOperator[pOperator.length] = 0;
								operatorList = operatorList + "," + "lt";
							}

							if(trackSubquery == 1 && window.document.frmData.txtDtofevnt.value =="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == true && document.frmData.chkOLE.checked == false)//If the text box value is null and check box value is not changed.
								return;				

							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+txtVal+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+txtVal+"'"+")";
						
							break;
												
					case 1:	txt="<=";
					
							if(pAdd == 0)
							{
								pOperator[pOperator.length] = 1;
								operatorList = operatorList + "," + "le";
							}

							if(trackSubquery == 1 && window.document.frmData.txtDtofevnt.value =="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == true && document.frmData.chkOLE.checked == false)//If the text box value is null and check box value is not changed.
								return;				

							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+txtVal+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+txtVal+"'"+")";
						
							break;	
											
					case 2:	txt="=";							
					
							if(pAdd == 0)
							{
								pOperator[pOperator.length] = 2;
								operatorList = operatorList + "," + "eq";
							}

							if(trackSubquery == 1 && window.document.frmData.txtDtofevnt.value =="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == true && document.frmData.chkOLE.checked == false)//If the text box value is null and check box value is not changed.
								return;				

							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+txtVal+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+txtVal+"'"+")";
						
							break;					
					
					case 3:	txt=">=";
					
							if(pAdd == 0)
							{
								pOperator[pOperator.length] = 3;
								operatorList = operatorList + "," + "ge";
							}

							if(trackSubquery == 1 && window.document.frmData.txtDtofevnt.value =="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == true && document.frmData.chkOLE.checked == false)//If the text box value is null and check box value is not changed.
								return;				

							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+txtVal+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+txtVal+"'"+")";
						
							break;
												
					case 4:	txt=">";
					
							if(pAdd == 0)
							{
								pOperator[pOperator.length] = 4;
								operatorList = operatorList + "," + "gt";
							}

							if(trackSubquery == 1 && window.document.frmData.txtDtofevnt.value =="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == true && document.frmData.chkOLE.checked == false)//If the text box value is null and check box value is not changed.
								return;				

							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+txtVal+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+txtVal+"'"+")";
						
							break;					
					
					case 5:	txt="<>";
					
							if(pAdd == 0)
							{
								pOperator[pOperator.length] = 5;
								operatorList = operatorList + "," + "ne";
							}

							if(trackSubquery == 1 && window.document.frmData.txtDtofevnt.value =="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == true && document.frmData.chkOLE.checked == false)//If the text box value is null and check box value is not changed.
								return;				

							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+txtVal+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+txtVal+"'"+")";
						
							break;

					case 6: txt=" LIKE ";
							
							if(pAdd == 0)
							{
								pOperator[pOperator.length] = 6;
								operatorList = operatorList + "," + "contains";
							}

							if(trackSubquery == 1 && window.document.frmData.txtDtofevnt.value =="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == true && document.frmData.chkOLE.checked == false)//If the text box value is null and check box value is not changed.
								return;				

							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+"%"+txtVal+"%"+"'"+")";						
							else
								txtEnter = "("+FieldName+txt+"'"+"%"+txtVal+"%"+"'"+")";
						
							break;

					case 7: txt=" LIKE ";
							
							if(pAdd == 0)
							{
							pOperator[pOperator.length] = 7;
							operatorList = operatorList + "," + "like";
							}
							
							if(trackSubquery == 1 && window.document.frmData.txtDtofevnt.value =="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == true && document.frmData.chkOLE.checked == false)//If the text box value is null and check box value is not changed.
								return;				

							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+txtVal+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+txtVal+"'"+")";
							
							break;

					case 8:	txt=" NOT LIKE ";
									
							if(pAdd == 0)
							{
								pOperator[pOperator.length] = 8;
								operatorList = operatorList + "," + "notcontains";
							}
									
							if(trackSubquery == 1 && window.document.frmData.txtDtofevnt.value =="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == true && document.frmData.chkOLE.checked == false)//If the text box value is null and check box value is not changed.
								return;				
									
							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+"%"+txtVal+"%"+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+"%"+txtVal+"%"+"'"+")";
									
							break;
	
					case 9:	txt=" NOT LIKE ";
							
							if(pAdd == 0)
							{
								pOperator[pOperator.length] = 9;
								operatorList = operatorList + "," + "notlike";
							}
							
							if(trackSubquery == 1 && window.document.frmData.txtDtofevnt.value =="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == true && document.frmData.chkOLE.checked == false)//If the text box value is null and check box value is not changed.
								return;				

							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+txtVal+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+txtVal+"'"+")";
							
							break;
				
					case 10: txt=" LIKE ";
							
							if(pAdd == 0)
							{
								pOperator[pOperator.length] = 10;
								operatorList = operatorList + "," + "beginswith";
							}
							
							if(trackSubquery == 1 && window.document.frmData.txtDtofevnt.value =="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == true && document.frmData.chkOLE.checked == false)//If the text box value is null and check box value is not changed.
								return;				

							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+txtVal+"%"+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+txtVal+"%"+"'"+")";

							break;				
				
					case 11: txt=" NOT LIKE ";
							
							if(pAdd == 0)
							{
								pOperator[pOperator.length] = 11;
								operatorList = operatorList + "," + "notbeginswith"; //Ankur Saxena MITS 9315 05/07/2007 - wrong operator was used.
							}
							
							if(trackSubquery == 1 && window.document.frmData.txtDtofevnt.value =="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == true && document.frmData.chkOLE.checked == false)//If the text box value is null and check box value is not changed.
								return;				

							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+txtVal+"%"+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+txtVal+"%"+"'"+")";
							
							break;
				
					case 12: txt=" LIKE ";
							
							if(pAdd == 0)
							{
								pOperator[pOperator.length] = 12;
								operatorList = operatorList + "," + "endswith";
							}
								
							if(trackSubquery == 1 && window.document.frmData.txtDtofevnt.value =="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == true && document.frmData.chkOLE.checked == false)//If the text box value is null and check box value is not changed.
								return;				

							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+"%"+txtVal+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+"%"+txtVal+"'"+")";

							break;
				
					case 13: txt=" NOT LIKE ";
						
							if(pAdd == 0)
							{
								pOperator[pOperator.length] = 13;
								operatorList = operatorList + "," + "notendswith"; //Ankur Saxena MITS 9315 05/07/2007 - wrong operator was used.
							}

							if(trackSubquery == 1 && window.document.frmData.txtDtofevnt.value =="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == true && document.frmData.chkOLE.checked == false)//If the text box value is null and check box value is not changed.
								return;				

							if(pAdd == 0)
								txtEnter=btnClick+ " " + "("+FieldName+txt+"'"+"%"+txtVal+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+"%"+txtVal+"'"+")";
										
							break;
				
					case 14: txt=" IS NULL ";
							
							if(pAdd == 0)
							{
								pOperator[pOperator.length] = 14;
								operatorList = operatorList + "," + "isnull";
							}
							
							if(trackSubquery == 1 && window.document.frmData.txtDtofevnt.value =="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == true && document.frmData.chkOLE.checked == false)//If the text box value is null and check box value is not changed.
								return;				

							if(pAdd == 0)
								txtEnter=btnClick+ " " + "("+FieldName+txt+")";
							else
								txtEnter = "("+FieldName+txt+")";
							
							break;
				
					case 15:
							txt=" IS NOT NULL ";
						
							if(pAdd == 0)
							{
								pOperator[pOperator.length] = 15;
								operatorList = operatorList + "," + "isnotnull";
							}
							
							if(trackSubquery == 1 && window.document.frmData.txtDtofevnt.value =="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == true && document.frmData.chkOLE.checked == false)//If the text box value is null and check box value is not changed.
								return;				

							if(pAdd == 0)
								txtEnter=btnClick+ " " + "("+FieldName+txt+")";
							else
								txtEnter = "("+FieldName+txt+")";
				}// End Of Swith
		
				if(trackSubquery == 0)
					trackSubquery=1;
						
				if(trackSubquery== 0 && window.document.frmData.txtDtofevnt.value !="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == false  )//If the value being entered is not the 1st one and text box is null.
					return;

				if(pAdd == 0) //-- Add mode
					add_item(txtEnter);
				else //-- Edit mode
				{	
					var selLength = document.frmData.lstCriteria.length;
					
					if(selLength == 0) //-- Need to add
					{
						if(document.frmData.cboOperators.selectedIndex == 14 || document.frmData.cboOperators.selectedIndex == 15)
							arrData[arrData.length] = '';
						else if(window.document.frmData.txtDtofevnt.value  != "" || trackSubquery == 0)
							arrData[arrData.length] = txtVal;

						add_item(txtEnter);
					}
					else
					{
						var selTxt = document.frmData.lstCriteria.options[document.frmData.lstCriteria.selectedIndex].text;
						if(selTxt.substring(0,2) == "OR")
							selConnector = "OR ";
						else if(selTxt.substring(0,3) == "AND")
							selConnector = "AND ";
						else
							selConnector = "";
							
						document.frmData.lstCriteria.options[document.frmData.lstCriteria.selectedIndex].text = selConnector + txtEnter;
						arrData[document.frmData.lstCriteria.selectedIndex] = txtVal;
						pOperator[document.frmData.lstCriteria.selectedIndex] = document.frmData.cboOperators.selectedIndex;
					}
				}	
			}//End Check		
	
			function delete_click()
			{

				var txt;
				if(document.frmData.lstCriteria.length>1)
				{
					txt=document.frmData.lstCriteria.options[1].text
					pattern="(";
					index=txt.indexOf(pattern);
				}

				if(document.frmData.lstCriteria.selectedIndex != -1)
				{
					if(document.frmData.lstCriteria.selectedIndex=="0" && document.frmData.lstCriteria.length>1)//If 1st element is being deleted and there more than 1 elements.
					{
						arrData[0] = null;
						pOperator[0] = null;
						document.frmData.lstCriteria.options[0]=null;
						document.frmData.lstCriteria.options[0].text=txt.substring(index,txt.length);
						document.frmData.lstCriteria.selectedIndex="0";
					}
					else if(document.frmData.lstCriteria.selectedIndex=="0" && document.frmData.lstCriteria.length=="1")//If 1st element is being deleted and its the only element.
					{
						arrData[0] = null;
						pOperator[0] = null;
						document.frmData.lstCriteria.options[0]=null;
					}
					else if(document.frmData.lstCriteria.selectedIndex == document.frmData.lstCriteria.length-1)
					{
						arrData[document.frmData.lstCriteria.selectedIndex] = null;
						pOperator[document.frmData.lstCriteria.selectedIndex] = null;
						document.frmData.lstCriteria.options[document.frmData.lstCriteria.selectedIndex]=null;	
						document.frmData.lstCriteria.selectedIndex=document.frmData.lstCriteria.length-1;
					}
					else
					{
						var selIndx = document.frmData.lstCriteria.selectedIndex
						arrData[document.frmData.lstCriteria.selectedIndex] = null;
						pOperator[document.frmData.lstCriteria.selectedIndex] = null;
						document.frmData.lstCriteria.options[document.frmData.lstCriteria.selectedIndex]=null;
						document.frmData.lstCriteria.selectedIndex = selIndx; 
					}
					
					adjust_pArray(arrData);
					adjust_pArray(pOperator);			
					
					if(document.frmData.lstCriteria.selectedIndex == -1)
					{
						document.frmData.btnDelete.disabled = true;
						document.frmData.txtDtofevnt.value = '';
					}
					else
					{
						document.frmData.txtDtofevnt.value = formatDate(arrData[document.frmData.lstCriteria.selectedIndex]);
						document.frmData.cboOperators.selectedIndex = pOperator[document.frmData.lstCriteria.selectedIndex];
					}
					
					if(document.frmData.lstCriteria.length == 0)
						operatorList = '';
				}
				else
				{
					//-- Check if the selected item that is being edited has been deleted or not.
					if(document.frmData.lstCriteria.length == 0)
					{
						document.frmData.hdSelCriteriaItem.value = '';
						window.opener.deleteClick();
						document.frmData.hdSelCriteriaIndex.value = -1;
					}
					return;
				}
					//-- Check if the selected item that is being edited has been deleted or not.
					if(document.frmData.lstCriteria.length == 0)
					{
						document.frmData.hdSelCriteriaItem.value = '';
						window.opener.deleteClick();
						document.frmData.hdSelCriteriaIndex.value = -1;
					}
			}
	
			function matchOperator(pOperator)
			{
				if(pOperator == "lt")
				{
					document.frmData.cboOperators.selectedIndex = 0;
					return 0;
				}
				else if(pOperator == "le")
				{
					document.frmData.cboOperators.selectedIndex = 1;
					return 1;
				}
				else if(pOperator == "eq")
				{
					document.frmData.cboOperators.selectedIndex = 2;
					return 2;
				}
				else if(pOperator == "ge")
				{
					document.frmData.cboOperators.selectedIndex = 3;
					return 3;
				}
				else if(pOperator == "gt")
				{
					document.frmData.cboOperators.selectedIndex = 4;
					return 4;
				}
				else if(pOperator == "ne")
				{
					document.frmData.cboOperators.selectedIndex = 5;
					return 5;
				}
				else if(pOperator == "contains")
				{
					document.frmData.cboOperators.selectedIndex = 6;
					return 6;
				}
				else if(pOperator == "like")
				{
					document.frmData.cboOperators.selectedIndex = 7;
					return 7;
				}
				else if(pOperator == "notcontains")
				{
					document.frmData.cboOperators.selectedIndex = 8;
					return 8;
				}
				else if(pOperator == "notlike")
				{
					document.frmData.cboOperators.selectedIndex = 9;
					return 9;
				}
				else if(pOperator == "beginswith")
				{
					document.frmData.cboOperators.selectedIndex = 10;
					return 10;
				}
				else if(pOperator == "notbeginswith") //Ankur Saxena MITS 9315 05/07/2007 - wrong operator was used.
				{
					document.frmData.cboOperators.selectedIndex = 11;
					return 11;
				}
				else if(pOperator == "endswith")
				{
					document.frmData.cboOperators.selectedIndex = 12;
					return 12;
				}
				else if(pOperator == "notendswith") //Ankur Saxena MITS 9315 05/07/2007 - wrong operator was used.
				{
					document.frmData.cboOperators.selectedIndex = 13;
					return 13;
				}
				else if(pOperator == "isnull")
				{
					document.frmData.cboOperators.selectedIndex = 14;
					return 14;
				}
				else if(pOperator == "isnotnull")
				{
					document.frmData.cboOperators.selectedIndex = 15;
					return 15;
				}
			}
	
			//-- fills global operatorList 
			function selXMLOperator(pIndex)
			{
				if(pIndex == 0)
					operatorList = operatorList + "," + "lt";
				else if(pIndex == 1)
					operatorList = operatorList + "," + "le";
				else if(pIndex == 2)
					operatorList = operatorList + "," + "eq";
				else if(pIndex == 3)
					operatorList = operatorList + "," + "ge";
				else if(pIndex == 4)
					operatorList = operatorList + "," + "gt";
				else if(pIndex == 5)
					operatorList = operatorList + "," + "ne";
				else if(pIndex == 6)
					operatorList = operatorList + "," + "contains";
				else if(pIndex == 7)
					operatorList = operatorList + "," + "like";
				else if(pIndex == 8)
					operatorList = operatorList + "," + "notcontains";
				else if(pIndex == 9)
					operatorList = operatorList + "," + "notlike";
				else if(pIndex == 10)
					operatorList = operatorList + "," + "beginswith";
				else if(pIndex == 11)
					operatorList = operatorList + "," + "notbeginswith"; //Ankur Saxena MITS 9315 05/07/2007 - wrong operator was used.
				else if(pIndex == 12)
					operatorList = operatorList + "," + "endswith";
				else if(pIndex == 13)
					operatorList = operatorList + "," + "notendswith"; //Ankur Saxena MITS 9315 05/07/2007 - wrong operator was used.
				else if(pIndex == 14)
					operatorList = operatorList + "," + "isnull";
				else if(pIndex == 15)
					operatorList = operatorList + "," + "isnotnull";
			}
	
			function change_click()
			{
				var FieldName,oper;
				if(document.frmData.cboOperators.selectedIndex == 0)
					oper = "<";
				else if(document.frmData.cboOperators.selectedIndex == 1)
					oper = "<=";
				else if(document.frmData.cboOperators.selectedIndex == 2)
					oper = "=";
				else if(document.frmData.cboOperators.selectedIndex == 3)
					oper = ">=";
				else if(document.frmData.cboOperators.selectedIndex == 4)
					oper = ">";
				else if(document.frmData.cboOperators.selectedIndex == 5)
					oper = "<>";
				else if(document.frmData.cboOperators.selectedIndex == 6)
					oper = " LIKE ";
				else if(document.frmData.cboOperators.selectedIndex == 7)
					oper = " LIKE ";
				else if(document.frmData.cboOperators.selectedIndex == 8)
					oper = " NOT LIKE ";
				else if(document.frmData.cboOperators.selectedIndex == 9)
					oper = " NOT LIKE ";
				else if(document.frmData.cboOperators.selectedIndex == 10)
					oper = " LIKE ";
				else if(document.frmData.cboOperators.selectedIndex == 11)
					oper = " NOT LIKE ";
				else if(document.frmData.cboOperators.selectedIndex == 12)
					oper = " LIKE ";
				else if(document.frmData.cboOperators.selectedIndex == 13)
					oper = " NOT LIKE ";
				else if(document.frmData.cboOperators.selectedIndex == 14)
					oper = " IS NULL ";
				else if(document.frmData.cboOperators.selectedIndex == 15)
					oper = " IS NOT NULL ";
				FieldName = window.document.frmData.hdFieldName.value;
				//btnClick = '';
				check(1);
				
			}

			function getOperator(p_Index) //-- document.frmData.cboOperators.selectedIndex
			{
				if(p_Index == 0)
					return "<";
				else if(p_Index == 1)
					return "<=";
				else if(p_Index == 2)
					return "=";
				else if(p_Index == 3)
					return ">=";
				else if(p_Index == 4)
					return ">";
				else if(p_Index == 5)
					return "<>";
				else if(p_Index == 6)
					return " LIKE ";
				else if(p_Index == 7)
					return " LIKE ";
				else if(p_Index == 8)
					return " NOT LIKE ";
				else if(p_Index == 9)
					return " NOT LIKE ";
				else if(p_Index == 10)
					return " LIKE ";
				else if(p_Index == 11)
					return " NOT LIKE ";
				else if(p_Index == 12)
					return " LIKE ";
				else if(p_Index == 13)
					return " NOT LIKE ";
				else if(p_Index == 14)
					return " IS NULL ";
				else if(p_Index == 15)
					return " IS NOT NULL ";		
			}	
		
			function ok_click()
			{
				var txtVal;
				//-- Change date format		
				
				if(window.document.frmData.txtDtofevnt.value != '')
				{
					dtArray = window.document.frmData.txtDtofevnt.value.split("/");
					dtVal = dtArray[2] + dtArray[0] + dtArray[1];
				}
				else
					dtVal = '';
					
				txtVal = dtVal;
						
				//-- Initialize
				connectorList = '';
				dataList = '';

				var oOption,pcount;
				oOption="";
				var enterKey = String.fromCharCode(13, 10);

				//-- Check for sub query
				if(document.frmData.chkSubquery.checked == true)
					hasDatadefcrit = 1;
				else
					hasDatadefcrit = 0;

				//-- Rajeev -- 03/25/2004 -- Check for hiding in RMNet
				if(document.frmData.chkHide.checked == true)
					hideInRMNet = 1;
				else
					hideInRMNet = 0;

				//-- Rajeev -- 03/25/2004 -- fill RMNet Label
				document.frmData.hdSelRMNetLabel.value = document.frmData.txtRMNetLabel.value;
					
				if(window.document.frmData.hdOLECritName.value == '')
					window.document.frmData.hdOLECritName.value = window.document.frmData.hdFieldName.value;
				
				if(window.document.frmData.hdOLECritDesc.value == '')
					window.document.frmData.hdOLECritDesc.value = window.document.frmData.hdFieldName.value;

				//-- fill pAttributes
				if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
				{
					if(window.opener.window.document.frmData.lstSelcriteria.length > 0)
						pAttributes = document.frmData.hdTableId.value + "|" + document.frmData.hdFieldId.value + "|" + "data" + "|" + "AND" + "|" + hasDatadefcrit + "|" + hideInRMNet + "|" + document.frmData.hdSelRMNetLabel.value;
					else
						pAttributes = document.frmData.hdTableId.value + "|" + document.frmData.hdFieldId.value + "|" + "data" + "|" + "none" + "|" + hasDatadefcrit + "|" + hideInRMNet + "|" + document.frmData.hdSelRMNetLabel.value;
				}
				else
				{
					var tempArray = window.document.frmData.hdSelCriteriaItem.value.split("~~~~~");
					var arrAttribute = tempArray[7].split("|");
					
					pAttributes = document.frmData.hdTableId.value + "|" + document.frmData.hdFieldId.value + "|" + "data" + "|" + arrAttribute[4] + "|" + hasDatadefcrit + "|" + hideInRMNet + "|" + document.frmData.hdSelRMNetLabel.value;
				}

				//-- No data case -- just close down
				if(document.frmData.lstCriteria.length == 0 && txtVal == '' && trackSubquery == 0 )
				{	
					if(document.frmData.cboOperators.selectedIndex == 14 || document.frmData.cboOperators.selectedIndex == 15)
					{
						arrData[arrData.length] = '';
						check(0);
					}
					else
					{	
						if(window.document.frmData.chkOLE.checked)
						{
							selXMLOperator(document.frmData.cboOperators.selectedIndex);
							//-- seperate out initial comma
							if(operatorList.substring(0,1) == ",")
								operatorList = operatorList.substring(1,operatorList.length);
							if(document.frmData.cboOperators.selectedIndex != 14 && document.frmData.cboOperators.selectedIndex != 15)
								dataList = 'OLECRIT';
							
							connectorList = 'none';
							
							txtEnter = "(" + document.frmData.hdFieldName.value + getOperator(document.frmData.cboOperators.selectedIndex) + "'" + dataList + "'" + ")";
							
							if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
							{
								if(window.opener.window.document.frmData.lstSelcriteria.length > 0)
									txtEnter = "AND" + " " + txtEnter;
							}
							else
							{	
								if(window.document.frmData.hdSelCriteriaIndex.selectedIndex > 0)
									txtEnter = "AND" + " " + txtEnter;			
							}
				
							window.opener.criteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,operatorList,dataList,connectorList,document.frmData.hdFieldName.value,document.frmData.hdFieldType.value,document.frmData.hdDBTableId.value,pAttributes,(txtEnter+enterKey));			
							if(window.document.frmData.chkOLE.checked)
								window.opener.OLEcriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(window.document.frmData.hdOLECritName.value) +'^^^^^'+ unescape(window.document.frmData.hdOLECritDesc.value) ,'', true);
							else
								window.opener.OLEcriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(window.document.frmData.hdOLECritName.value) +'^^^^^'+ unescape(window.document.frmData.hdOLECritDesc.value) ,'', false);
							
							if(window.document.frmData.cboDay.selectedIndex > 1)
								window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(arrRelFormulaValue[document.frmData.cboDay.selectedIndex-2]),'', true);
							else
								window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(arrRelFormulaValue[document.frmData.cboDay.selectedIndex-2]),'', false);
							
							window.opener.displayCriteria();
							window.opener.displayGlobalCriteria();
							window.opener.changeFieldName(window.document.frmData.hdSelCriteriaIndex.value);
							window.close();
							return;
						}
					}
					
					//-- Connector list
					if(btnClick == '')
						connectorList = 'none';
					else
						connectorList = btnClick;

					if(document.frmData.cboOperators.selectedIndex == 14 || document.frmData.cboOperators.selectedIndex == 15)
					{
						arrData[arrData.length] = '';
						check(0);
					}
					else
					{
						arrData[arrData.length] = txtVal;
						check(0);
					}

					//-- fill datalist -- only one data item
					dataList = arrData[0];	

					//-- Operator list
					//-- seperate out initial comma
					if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
					{
						if(operatorList.substring(0,1) == ",")
							operatorList = operatorList.substring(1,operatorList.length);
					}
					else
					{
						operatorList = '';
						selXMLOperator(document.frmData.cboOperators.selectedIndex);
						//-- seperate out initial comma
						if(operatorList.substring(0,1) == ",")
							operatorList = operatorList.substring(1,operatorList.length);
					}
					
					//-- maintain outer main connector
					if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
					{
						if(window.opener.window.document.frmData.lstSelcriteria.length > 0)
						{
							if(txtEnter != '')
								txtEnter = "AND" + " " + txtEnter;
						}
					}
					else
					{
						if(window.document.frmData.hdSelCriteriaIndex.value > 0)
						{
							if(txtEnter != '')
								txtEnter = "AND" + " " + txtEnter;
						}
					}
					
					window.opener.criteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,operatorList,dataList,connectorList,document.frmData.hdFieldName.value,document.frmData.hdFieldType.value,document.frmData.hdDBTableId.value,pAttributes,(txtEnter+enterKey));			
					if(window.document.frmData.chkOLE.checked)
						window.opener.OLEcriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(window.document.frmData.hdOLECritName.value) +'^^^^^'+ unescape(window.document.frmData.hdOLECritDesc.value) ,'', true);
					else
						window.opener.OLEcriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(window.document.frmData.hdOLECritName.value) +'^^^^^'+ unescape(window.document.frmData.hdOLECritDesc.value) ,'', false);
					if(window.document.frmData.cboDay.selectedIndex > 1)
						window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(arrRelFormulaValue[document.frmData.cboDay.selectedIndex-2]),'', true);
					else
						window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(arrRelFormulaValue[document.frmData.cboDay.selectedIndex-2]),'', false);
					window.opener.displayCriteria();
					window.opener.displayGlobalCriteria();
					window.opener.changeFieldName(window.document.frmData.hdSelCriteriaIndex.value);
					window.opener.ButtonState();
						
					window.close();
					return;		
				
				}
				else if(document.frmData.lstCriteria.length == 0 && txtVal == '' && trackSubquery == 1)
				{	
					if(document.frmData.cboOperators.selectedIndex == 14 || document.frmData.cboOperators.selectedIndex == 15)
					{
						arrData[arrData.length] = '';
						check(0);
					}
					else
					{			
						if(window.document.frmData.chkOLE.checked)
						{
							selXMLOperator(document.frmData.cboOperators.selectedIndex);
							//-- seperate out initial comma
							if(operatorList.substring(0,1) == ",")
								operatorList = operatorList.substring(1,operatorList.length);
							if(document.frmData.cboOperators.selectedIndex != 14 && document.frmData.cboOperators.selectedIndex != 15)
								dataList = 'OLECRIT';
							connectorList = 'none';
							
							txtEnter = "(" + document.frmData.hdFieldName.value + getOperator(document.frmData.cboOperators.selectedIndex) + "'" + dataList + "'" + ")";
							
							if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
							{
								if(window.opener.window.document.frmData.lstSelcriteria.length > 0)
									txtEnter = "AND" + " " + txtEnter;
							}
							else
							{	
								if(window.document.frmData.hdSelCriteriaIndex.selectedIndex > 0)
									txtEnter = "AND" + " " + txtEnter;			
							}
				
							window.opener.criteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,operatorList,dataList,connectorList,document.frmData.hdFieldName.value,document.frmData.hdFieldType.value,document.frmData.hdDBTableId.value,pAttributes,(txtEnter+enterKey));			
							
							if(window.document.frmData.chkOLE.checked)
								window.opener.OLEcriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(window.document.frmData.hdOLECritName.value) +'^^^^^'+ unescape(window.document.frmData.hdOLECritDesc.value) ,'', true);
							else
								window.opener.OLEcriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(window.document.frmData.hdOLECritName.value) +'^^^^^'+ unescape(window.document.frmData.hdOLECritDesc.value) ,'', false);
							
							if(window.document.frmData.cboDay.selectedIndex > 1)
								window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(arrRelFormulaValue[document.frmData.cboDay.selectedIndex-2]),'', true);
							else
								window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(arrRelFormulaValue[document.frmData.cboDay.selectedIndex-2]),'', false);
							window.opener.displayCriteria();
							window.opener.displayGlobalCriteria();
							window.opener.changeFieldName(window.document.frmData.hdSelCriteriaIndex.value);

							window.close();
							return;
						}
						else
						{
							if(document.frmData.hdSelCriteriaIndex.value == -1)
							{
								window.close();
								return;
							}
							else
							{
								window.opener.deleteClick();
								window.close();
								return;
							}							
							//window.close();
							//return;
						}
					}
				}
	
				if(document.frmData.lstCriteria.length == 0 && txtVal != '')
				{
					//-- Add mode -- Edit not applicable
					//-- Handle the OLE criteria
					if(window.document.frmData.chkOLE.checked)
					{
						selXMLOperator(document.frmData.cboOperators.selectedIndex);
						//-- seperate out initial comma
						if(operatorList.substring(0,1) == ",")
							operatorList = operatorList.substring(1,operatorList.length);
						if(document.frmData.cboOperators.selectedIndex != 14 && document.frmData.cboOperators.selectedIndex != 15)
							dataList = 'OLECRIT';
						connectorList = 'none';
							
						txtEnter = "(" + document.frmData.hdFieldName.value + getOperator(document.frmData.cboOperators.selectedIndex) + "'" + dataList + "'" + ")";
							
						if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
						{
							if(window.opener.window.document.frmData.lstSelcriteria.length > 0)
								txtEnter = "AND" + " " + txtEnter;
						}
						else
						{	
							if(window.document.frmData.hdSelCriteriaIndex.selectedIndex > 0)
								txtEnter = "AND" + " " + txtEnter;			
						}
				
						window.opener.criteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,operatorList,dataList,connectorList,document.frmData.hdFieldName.value,document.frmData.hdFieldType.value,document.frmData.hdDBTableId.value,pAttributes,(txtEnter+enterKey));			
						if(window.document.frmData.chkOLE.checked)
							window.opener.OLEcriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(window.document.frmData.hdOLECritName.value) +'^^^^^'+ unescape(window.document.frmData.hdOLECritDesc.value) ,'', true);
						else
							window.opener.OLEcriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(window.document.frmData.hdOLECritName.value) +'^^^^^'+ unescape(window.document.frmData.hdOLECritDesc.value) ,'', false);
						if(window.document.frmData.cboDay.selectedIndex > 1)
							window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(arrRelFormulaValue[document.frmData.cboDay.selectedIndex-2]),'', true);
						else
							window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(arrRelFormulaValue[document.frmData.cboDay.selectedIndex-2]),'', false);
						window.opener.displayCriteria();
						window.opener.displayGlobalCriteria();
						window.opener.changeFieldName(window.document.frmData.hdSelCriteriaIndex.value);
						window.close();
						return;
					}
				
					//-- Connector list
					if(btnClick == '')
						connectorList = 'none';
					else
						connectorList = btnClick;
					
					if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
					{
						if(document.frmData.cboOperators.selectedIndex == 14 || document.frmData.cboOperators.selectedIndex == 15)
						{
							arrData[arrData.length] = '';
							check(0);
						}
						else
						{
							arrData[arrData.length] = txtVal;
							check(0);
						}
					}
					else
					{
						if(document.frmData.cboOperators.selectedIndex == 14 || document.frmData.cboOperators.selectedIndex == 15)
							check(1);
						else
							check(1);
					}
						
					//-- fill datalist -- only one data item
					dataList = arrData[0];	
				
					if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
					{
						if(operatorList.substring(0,1) == ",")
							operatorList = operatorList.substring(1,operatorList.length);
					}
					else
					{
						operatorList = '';
						selXMLOperator(document.frmData.cboOperators.selectedIndex);
						//-- seperate out initial comma
						if(operatorList.substring(0,1) == ",")
							operatorList = operatorList.substring(1,operatorList.length);
					}
	
					//-- maintain outer main connector
					if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
					{
						if(window.opener.window.document.frmData.lstSelcriteria.length > 0)
							txtEnter = "AND" + " " + txtEnter;
					}
					else
					{
						if(window.document.frmData.hdSelCriteriaIndex.value > 0)
							txtEnter = "AND" + " " + txtEnter;
					}
					
					window.opener.criteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,operatorList,dataList,connectorList,document.frmData.hdFieldName.value,document.frmData.hdFieldType.value,document.frmData.hdDBTableId.value,pAttributes,(txtEnter+enterKey));
					if(window.document.frmData.chkOLE.checked)
						window.opener.OLEcriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(window.document.frmData.hdOLECritName.value) +'^^^^^'+ unescape(window.document.frmData.hdOLECritDesc.value) ,'', true);
					else
						window.opener.OLEcriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(window.document.frmData.hdOLECritName.value) +'^^^^^'+ unescape(window.document.frmData.hdOLECritDesc.value) ,'', false);
					if(window.document.frmData.cboDay.selectedIndex > 1)
						window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(arrRelFormulaValue[document.frmData.cboDay.selectedIndex-2]),'', true);
					else
						window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(arrRelFormulaValue[document.frmData.cboDay.selectedIndex-2]),'', false);
					window.opener.displayCriteria();
					window.opener.displayGlobalCriteria();
					window.opener.changeFieldName(window.document.frmData.hdSelCriteriaIndex.value);
					window.opener.ButtonState();
						
					window.close();
					return;		
				}
				else if(document.frmData.lstCriteria.length > 0 && txtVal != '' && document.frmData.btnDelete.disabled == true && document.frmData.btnChange.disabled == true)
				{
					if(document.frmData.chkOLE.checked)
					{
						for(i=0;i<document.frmData.lstCriteria.length;i++)
						{
							if(document.frmData.lstCriteria.options[i].text.substring(0,3) == "AND")
								connectorList = connectorList + "," + "AND";
							else if(document.frmData.lstCriteria.options[i].text.substring(0,2) == "OR")
								connectorList = connectorList + "," + "OR";
							else
								connectorList = connectorList + "," + "none";
						}
					
						//-- seperate out initial comma
						connectorList = connectorList.substring(1,connectorList.length);

						//-- append AND/OR as per the AND/OR button buffer
						if(btnClick == '')
						{
							window.close();
							return;
						}
							
						connectorList = connectorList + "," + btnClick;
						
						//-- fill datalist
						if(document.frmData.cboOperators.selectedIndex == 14 || document.frmData.cboOperators.selectedIndex == 15)
						{
							arrData[arrData.length] = '';
							check(0);
						}
						else
						{
							arrData[arrData.length] = 'OLECRIT';
							check(0);
						}
							
						//-- rebuild operator list
						operatorList = '';
					
						for(i=0;i<arrData.length;i++)
							dataList = dataList + "^^^^^" + arrData[i];	

						//-- seperate out initial comma
						dataList = dataList.substring(5,dataList.length);
					
						//-- eleminate last operator inserted by check(0)		
						for(i=0;i<pOperator.length;i++)
							selXMLOperator(pOperator[i]);
						//-- seperate out initial comma
						if(operatorList.substring(0,1) == ",")
							operatorList = operatorList.substring(1,operatorList.length);

						oOption = '';
					
						for(i=0;i<document.frmData.lstCriteria.length;i++)
							oOption = oOption + document.frmData.lstCriteria.options[i].text;
					
						//-- maintain outer main connector
						if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
						{
							if(window.opener.window.document.frmData.lstSelcriteria.length > 0)
								oOption = "AND" + " ( " + oOption + ")";
							else
								oOption = "(" + oOption + ")";
						}
						else
						{
							if(window.document.frmData.hdSelCriteriaIndex.value > 0)
								oOption = "AND" + " ( " + oOption + ")";
							else
								oOption = "(" + oOption + ")";
						}
				
						window.opener.criteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,operatorList,dataList,connectorList,document.frmData.hdFieldName.value,document.frmData.hdFieldType.value,document.frmData.hdDBTableId.value,pAttributes,(oOption+enterKey));			
						if(window.document.frmData.chkOLE.checked)
							window.opener.OLEcriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(window.document.frmData.hdOLECritName.value) +'^^^^^'+ unescape(window.document.frmData.hdOLECritDesc.value) ,'', true);
						else
							window.opener.OLEcriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(window.document.frmData.hdOLECritName.value) +'^^^^^'+ unescape(window.document.frmData.hdOLECritDesc.value) ,'', false);
						if(window.document.frmData.cboDay.selectedIndex > 1)
							window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(arrRelFormulaValue[document.frmData.cboDay.selectedIndex-2]),'', true);
						else
							window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(arrRelFormulaValue[document.frmData.cboDay.selectedIndex-2]),'', false);
						window.opener.displayCriteria();
						window.opener.displayGlobalCriteria();
						window.opener.changeFieldName(window.document.frmData.hdSelCriteriaIndex.value);
						window.close();
						return;				
					}
					//-- Add/Edit mode -- extended criteria will be appended the criteria of selected item in list box
					
					//-- fill connectorlist
					for(i=0;i<document.frmData.lstCriteria.length;i++)
					{
						if(document.frmData.lstCriteria.options[i].text.substring(0,3) == "AND")
							connectorList = connectorList + "," + "AND";
						else if(document.frmData.lstCriteria.options[i].text.substring(0,2) == "OR")
							connectorList = connectorList + "," + "OR";
						else
							connectorList = connectorList + "," + "none";
					}
					//-- append AND/OR as per the AND/OR button buffer
					connectorList = connectorList + "," + btnClick;
					
					//-- seperate out initial comma
					connectorList = connectorList.substring(1,connectorList.length);

					//-- fill datalist
					if(document.frmData.cboOperators.selectedIndex == 14 || document.frmData.cboOperators.selectedIndex == 15)
					{
						arrData[arrData.length] = '';
						check(0);
					}
					else
					{
						arrData[arrData.length] = txtVal;
						check(0);
					}
					
					for(i=0;i<arrData.length;i++)
						dataList = dataList + "^^^^^" + arrData[i];	

					//-- seperate out initial comma
					dataList = dataList.substring(5,dataList.length);

					//-- rebuild operator list
					operatorList = '';
					for(i=0;i<pOperator.length;i++)
						selXMLOperator(pOperator[i]);
					//-- seperate out initial comma
					if(operatorList.substring(0,1) == ",")
						operatorList = operatorList.substring(1,operatorList.length);
					

					for(i=0;i<document.frmData.lstCriteria.length;i++)
						oOption = oOption + document.frmData.lstCriteria.options[i].text;

					//-- maintain outer main connector
					if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
					{
						if(window.opener.window.document.frmData.lstSelcriteria.length > 0)
							oOption = "AND" + " ( " + oOption + ")";
						else
							oOption = "(" + oOption + ")";
					}
					else
					{
						if(window.document.frmData.hdSelCriteriaIndex.value > 0)
							oOption = "AND" + " ( " + oOption + ")";
						else
							oOption = "(" + oOption + ")";
					}
										
					window.opener.criteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,operatorList,dataList,connectorList,document.frmData.hdFieldName.value,document.frmData.hdFieldType.value,document.frmData.hdDBTableId.value,pAttributes,(oOption+enterKey));			
					if(window.document.frmData.chkOLE.checked)
						window.opener.OLEcriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(window.document.frmData.hdOLECritName.value) +'^^^^^'+ unescape(window.document.frmData.hdOLECritDesc.value) ,'', true);
					else
						window.opener.OLEcriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(window.document.frmData.hdOLECritName.value) +'^^^^^'+ unescape(window.document.frmData.hdOLECritDesc.value) ,'', false);
					if(window.document.frmData.cboDay.selectedIndex > 1)
						window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(arrRelFormulaValue[document.frmData.cboDay.selectedIndex-2]),'', true);
					else
						window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(arrRelFormulaValue[document.frmData.cboDay.selectedIndex-2]),'', false);
					window.opener.displayCriteria();
					window.opener.displayGlobalCriteria();
					window.opener.changeFieldName(window.document.frmData.hdSelCriteriaIndex.value);
					window.close();
					return;		
				}
				else if((document.frmData.lstCriteria.length > 0 && txtVal != '' && document.frmData.btnDelete.disabled == false && document.frmData.btnChange.disabled == true))
				{
					//-- fill connectorlist
					for(i=0;i<document.frmData.lstCriteria.length;i++)
					{
						if(document.frmData.lstCriteria.options[i].text.substring(0,3) == "AND")
							connectorList = connectorList + "," + "AND";
						else if(document.frmData.lstCriteria.options[i].text.substring(0,2) == "OR")
							connectorList = connectorList + "," + "OR";
						else
							connectorList = connectorList + "," + "none";
					}
					
					//-- seperate out initial comma
					connectorList = connectorList.substring(1,connectorList.length);

					//-- fill datalist
					if(document.frmData.cboOperators.selectedIndex == 14 || document.frmData.cboOperators.selectedIndex == 15)
						check(1);
					else
						check(1);
					
					for(i=0;i<arrData.length;i++)
						dataList = dataList + "^^^^^" + arrData[i];	

					//-- seperate out initial comma
					dataList = dataList.substring(5,dataList.length);
					
					//-- rebuild operator list
					operatorList = '';
					for(i=0;i<pOperator.length;i++)
						selXMLOperator(pOperator[i]);
					//-- seperate out initial comma
					if(operatorList.substring(0,1) == ",")
						operatorList = operatorList.substring(1,operatorList.length);
						
					//-- build combined critera to show on parent window's list box
					for(i=0;i<document.frmData.lstCriteria.length;i++)
						oOption = oOption + document.frmData.lstCriteria.options[i].text;
				}		
				else if(document.frmData.lstCriteria.length > 0 && txtVal != '' && document.frmData.btnDelete.disabled == false && document.frmData.btnChange.disabled == false)
				{
					//-- Add/Edit mode -- extended criteria will be appended the criteria of selected item in list box
					//-- fill connectorlist
					for(i=0;i<document.frmData.lstCriteria.length;i++)
					{
						if(document.frmData.lstCriteria.options[i].text.substring(0,3) == "AND")
							connectorList = connectorList + "," + "AND";
						else if(document.frmData.lstCriteria.options[i].text.substring(0,2) == "OR")
							connectorList = connectorList + "," + "OR";
						else
							connectorList = connectorList + "," + "none";
					}
					
					//-- seperate out initial comma
					connectorList = connectorList.substring(1,connectorList.length);

					//-- fill datalist
					check(1);
					
					for(i=0;i<arrData.length;i++)
						dataList = dataList + "^^^^^" + arrData[i];	

					//-- seperate out initial comma
					dataList = dataList.substring(5,dataList.length);
					
					//-- rebuild operator list
					operatorList = '';
					for(i=0;i<pOperator.length;i++)
						selXMLOperator(pOperator[i]);
					//-- seperate out initial comma
					if(operatorList.substring(0,1) == ",")
						operatorList = operatorList.substring(1,operatorList.length);

					//-- build combined critera to show on parent window's list box
					for(i=0;i<document.frmData.lstCriteria.length;i++)
						oOption = oOption + document.frmData.lstCriteria.options[i].text;

				}
				else if(document.frmData.lstCriteria.length > 0 && txtVal != '' && document.frmData.btnDelete.disabled == true && document.frmData.btnChange.disabled == false)
				{
					//-- Add/Edit mode -- extended criteria will be appended the criteria of selected item in list box
					//-- fill connectorlist
					for(i=0;i<document.frmData.lstCriteria.length;i++)
					{
						if(document.frmData.lstCriteria.options[i].text.substring(0,3) == "AND")
							connectorList = connectorList + "," + "AND";
						else if(document.frmData.lstCriteria.options[i].text.substring(0,2) == "OR")
							connectorList = connectorList + "," + "OR";
						else
							connectorList = connectorList + "," + "none";
					}
					
					//-- seperate out initial comma
					connectorList = connectorList.substring(1,connectorList.length);

					//-- fill datalist
					check(1);
					
					for(i=0;i<arrData.length;i++)
						dataList = dataList + "^^^^^" + arrData[i];	

					//-- seperate out initial comma
					dataList = dataList.substring(5,dataList.length);
					
					//-- rebuild operator list
					operatorList = '';
					for(i=0;i<pOperator.length;i++)
						selXMLOperator(pOperator[i]);
					//-- seperate out initial comma
					if(operatorList.substring(0,1) == ",")
						operatorList = operatorList.substring(1,operatorList.length);

					//-- build combined critera to show on parent window's list box
					for(i=0;i<document.frmData.lstCriteria.length;i++)
						oOption = oOption + document.frmData.lstCriteria.options[i].text;

				}		
				else if(document.frmData.lstCriteria.length > 0 && txtVal == '' && trackSubquery == 0)
				{
					//-- fill connectorlist
					for(i=0;i<document.frmData.lstCriteria.length;i++)
					{
						if(document.frmData.lstCriteria.options[i].text.substring(0,3) == "AND")
							connectorList = connectorList + "," + "AND";
						else if(document.frmData.lstCriteria.options[i].text.substring(0,2) == "OR")
							connectorList = connectorList + "," + "OR";
						else
							connectorList = connectorList + "," + "none";
					}
					//-- append AND/OR as per the AND/OR button buffer
					connectorList = connectorList + "," + btnClick;
					
					//-- seperate out initial comma
					connectorList = connectorList.substring(1,connectorList.length);
					
					if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
					{
						//-- fill datalist
						if(document.frmData.cboOperators.selectedIndex == 14 || document.frmData.cboOperators.selectedIndex == 15)
						{
							arrData[arrData.length] = '';
							check(0);
						}
						else
						{
							arrData[arrData.length] = txtVal;
							check(0);
						}
					}
					else
					{
						//-- fill datalist
						check(1);
					}			
					for(i=0;i<arrData.length;i++)
						dataList = dataList + "^^^^^" + arrData[i];	

					//-- seperate out initial comma
					dataList = dataList.substring(5,dataList.length);
					
					//-- rebuild operator list
					operatorList = '';
					for(i=0;i<pOperator.length;i++)
						selXMLOperator(pOperator[i]);
					//-- seperate out initial comma
					if(operatorList.substring(0,1) == ",")
						operatorList = operatorList.substring(1,operatorList.length);

					//-- build combined critera to show on parent window's list box
					for(i=0;i<document.frmData.lstCriteria.length;i++)
						oOption = oOption + document.frmData.lstCriteria.options[i].text;
					
					//-- handle OLE Criteria
					if(window.document.frmData.chkOLE.checked && document.frmData.hdSelCriteriaItem.value == '')
					{
						selXMLOperator(document.frmData.cboOperators.selectedIndex);
						//-- seperate out initial comma
						if(operatorList.substring(0,1) == ",")
							operatorList = operatorList.substring(1,operatorList.length);
						if(document.frmData.cboOperators.selectedIndex != 14 && document.frmData.cboOperators.selectedIndex != 15)
							dataList = dataList + "^^^^^" + 'OLECRIT';
							
						//-- append AND/OR as per the AND/OR button buffer
						connectorList = connectorList + "," + btnClick;
						if(document.frmData.cboOperators.selectedIndex != 14 && document.frmData.cboOperators.selectedIndex != 15)
							oOption = oOption + " " + btnClick + " (" + document.frmData.hdFieldName.value + getOperator(document.frmData.cboOperators.selectedIndex) + "'" + 'OLECRIT' + "'" + ")";
						else
							oOption = oOption + " " + btnClick + " (" + document.frmData.hdFieldName.value + getOperator(document.frmData.cboOperators.selectedIndex);
					}
					else if(window.document.frmData.chkOLE.checked && document.frmData.hdSelCriteriaItem.value != '')
					{
						check(1);
					}		
				}				
				else if(document.frmData.lstCriteria.length > 0 && txtVal == '' && trackSubquery == 1)
				{
			
					//-- fill connectorlist
					for(i=0;i<document.frmData.lstCriteria.length;i++)
					{
						if(document.frmData.lstCriteria.options[i].text.substring(0,3) == "AND")
							connectorList = connectorList + "," + "AND";
						else if(document.frmData.lstCriteria.options[i].text.substring(0,2) == "OR")
							connectorList = connectorList + "," + "OR";
						else
							connectorList = connectorList + "," + "none";
					}
					//-- separate out initial comma
					connectorList = connectorList.substring(1,connectorList.length);

			
					//-- fill datalist
					if(window.document.frmData.chkOLE.checked == false)
					{
						check(1);
					}
								
					
					
					for(i=0;i<arrData.length;i++)
						dataList = dataList + "^^^^^" + arrData[i];	

					//-- seperate out initial comma
					dataList = dataList.substring(5,dataList.length);
					
					//-- rebuild operator list
					operatorList = '';
					for(i=0;i<pOperator.length;i++)
						selXMLOperator(pOperator[i]);
					//-- seperate out initial comma
					if(operatorList.substring(0,1) == ",")
						operatorList = operatorList.substring(1,operatorList.length);
					
					//-- build combined critera to show on parent window's list box
					for(i=0;i<document.frmData.lstCriteria.length;i++)
						oOption = oOption + document.frmData.lstCriteria.options[i].text;
					
					
					//-- handle OLE Criteria
					if(window.document.frmData.chkOLE.checked && document.frmData.hdSelCriteriaItem.value == '')
					{
					
						selXMLOperator(document.frmData.cboOperators.selectedIndex);
						//-- seperate out initial comma
						if(operatorList.substring(0,1) == ",")
							operatorList = operatorList.substring(1,operatorList.length);
						if(document.frmData.cboOperators.selectedIndex != 14 && document.frmData.cboOperators.selectedIndex != 15)
							dataList = dataList + "^^^^^" + 'OLECRIT';
							
						//-- append AND/OR as per the AND/OR button buffer
						connectorList = connectorList + "," + btnClick;
					
						if(document.frmData.cboOperators.selectedIndex != 14 && document.frmData.cboOperators.selectedIndex != 15)
							oOption = oOption + " " + btnClick + " (" + document.frmData.hdFieldName.value + getOperator(document.frmData.cboOperators.selectedIndex) + "'" + 'OLECRIT' + "'" + ")";
						else
							oOption = oOption + " " + btnClick + " (" + document.frmData.hdFieldName.value + getOperator(document.frmData.cboOperators.selectedIndex);
					}
					else if(window.document.frmData.chkOLE.checked && document.frmData.hdSelCriteriaItem.value != '')
					{
						check(1);
					}
				}			
				//-- append outer parenthesis 

				if((trimIt(oOption).substring(0,3) == "AND" && trimIt((oOption).substring(3,trimIt(oOption).length)).indexOf("AND") > 0) || (trimIt(oOption).substring(0,3) == "AND" && trimIt((oOption).substring(3,trimIt(oOption).length)).indexOf("OR") > 0))
					oOption = "(" +  oOption + ")" ;
				else if((trimIt(oOption).substring(0,2) == "OR" && trimIt((oOption).substring(2,trimIt(oOption).length)).indexOf("OR") > 0) || (trimIt(oOption).substring(0,2) == "AND" && trimIt((oOption).substring(2,trimIt(oOption).length)).indexOf("AND") > 0))
					oOption = "(" +  oOption + ")" ;
				else if((trimIt(oOption).substring(0,3) != "AND" && trimIt(oOption).indexOf("AND") > 0) || (trimIt(oOption).substring(0,3) != "AND" && trimIt(oOption).indexOf("OR") > 0 ))
					oOption = "(" + oOption + ")";
				else if((trimIt(oOption).substring(0,2) != "OR" && trimIt(oOption).indexOf("AND")) > 0 || (trimIt(oOption).substring(0,2) != "OR" && trimIt(oOption).indexOf("OR") > 0 ))
					oOption = "(" + oOption + ")";


				if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
				{
					if(window.opener.window.document.frmData.lstSelcriteria.length > 0)
						oOption = "AND" + " " + oOption;
				}
				else
				{	
					if(window.document.frmData.hdSelCriteriaIndex.selectedIndex > 0)
						oOption = "AND" + " " + oOption;			
				}
				
				//-- code to handle the outer connector while editing
				if(document.frmData.hdSelCriteriaIndex.value != -1)		
				{	
					var arrPAttributes = pAttributes.split("|");
					
					if(arrPAttributes[3] != "none")
						oOption = arrPAttributes[3].toUpperCase() + " " + oOption;
				}

				window.opener.criteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,operatorList,dataList,connectorList,document.frmData.hdFieldName.value,document.frmData.hdFieldType.value,document.frmData.hdDBTableId.value,pAttributes,(oOption+enterKey));			
				
				if(window.document.frmData.chkOLE.checked)
					window.opener.OLEcriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(window.document.frmData.hdOLECritName.value) +'^^^^^'+ unescape(window.document.frmData.hdOLECritDesc.value) ,'', true);
				else
					window.opener.OLEcriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(window.document.frmData.hdOLECritName.value) +'^^^^^'+ unescape(window.document.frmData.hdOLECritDesc.value) ,'', false);
				if(window.document.frmData.cboDay.selectedIndex > 1)
					window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(arrRelFormulaValue[document.frmData.cboDay.selectedIndex-2]),'', true);
				else
					window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value, unescape(arrRelFormulaValue[document.frmData.cboDay.selectedIndex-2]),'', false);
				window.opener.displayCriteria();
				window.opener.displayGlobalCriteria();
				window.opener.changeFieldName(window.document.frmData.hdSelCriteriaIndex.value);
				window.opener.ButtonState();

				if(document.frmData.hdSelCriteriaIndex.value != -1)		
					window.opener.window.document.frmData.lstSelcriteria.selectedIndex = window.document.frmData.hdSelCriteriaIndex.value;

				window.close();
			}
	
			function selOperators(index)
			{
				return 	pOperator[index];
			}

			function OLEEnable()
			{
				if(window.document.frmData.chkOLE.checked == true)
				{
					window.document.frmData.cboDay.disabled = true;
					window.document.frmData.btnAnd.disabled = true;
					window.document.frmData.btnOr.disabled = true;
					window.document.frmData.btnDelete.disabled = true;
					window.document.frmData.btnChange.disabled = true;
					//window.document.frmData.lstCriteria.disabled = true;
					window.document.frmData.txtDtofevnt.disabled = true;
					window.document.frmData.btnDtofevnt.disabled = true;
					window.document.frmData.btnOLE.disabled = false;		
				}
				else
				{
					window.document.frmData.cboDay.disabled = false;
					window.document.frmData.btnAnd.disabled = false;
					window.document.frmData.btnOr.disabled = false;
					
					if(window.document.frmData.lstCriteria.selectedIndex > -1)
						window.document.frmData.btnDelete.disabled = false;
					
					window.document.frmData.txtDtofevnt.disabled = false;
					window.document.frmData.btnDtofevnt.disabled = false;
					window.document.frmData.btnOLE.disabled = true;
				}
			}

			function selectDay()
			{
				var today = new Date();
				var dateString;
				var temp;
				
				temp = (today.getMonth()+1).toString();
				
				if (temp.length == 1)
				{
					temp = "0"+ temp;
				}
				
				dateString = temp;
				
				temp = (today.getDate()).toString();
				
				if (temp.length == 1)
				{
					temp = "0"+ temp;
				}
				
				dateString = dateString + "/"+ temp;
				
				temp = today.getFullYear().toString();
				
				dateString = dateString + "/"+ temp;
				
				if (window.document.frmData.cboDay.selectedIndex == 1)
				{
					window.document.frmData.txtDtofevnt.disabled = true;
					window.document.frmData.btnDtofevnt.disabled = true;
					window.document.frmData.txtDtofevnt.value = dateString;
				}
				else if (window.document.frmData.cboDay.selectedIndex == 0)
				{
					window.document.frmData.txtDtofevnt.disabled = false;
					window.document.frmData.btnDtofevnt.disabled = false;
				}
				else //-- Handling relative date
				{
					var sDate = new Date();
					var sRelDateFormula = arrRelFormulaValue[window.document.frmData.cboDay.selectedIndex-2];
					if (sRelDateFormula.substring(sRelDateFormula.length-1,sRelDateFormula.length) == 'M')
					{
						sDate.setMonth(sDate.getMonth() + parseInt(sRelDateFormula.substring(0,2)));
						
						temp = (sDate.getMonth()+1).toString();
						
						if (temp.length == 1)
						{
							temp = "0"+ temp;
						}
					
						dateString = temp;
					
						temp = (sDate.getDate()).toString();
					
						if (temp.length == 1)
						{
							temp = "0"+ temp;
						}
					
						dateString = dateString + "/"+ temp;
					
						temp = sDate.getFullYear().toString();
					
						dateString = dateString + "/"+ temp;
					}
					else if (sRelDateFormula.substring(sRelDateFormula.length-1,sRelDateFormula.length) == 'D')
					{
						sDate.setDate(sDate.getDate() + parseInt(sRelDateFormula.substring(0,2)));
						
						temp = (sDate.getMonth()+1).toString();
						
						if (temp.length == 1)
						{
							temp = "0"+ temp;
						}
					
						dateString = temp;
					
						temp = (sDate.getDate()).toString();
					
						if (temp.length == 1)
						{
							temp = "0"+ temp;
						}
					
						dateString = dateString + "/"+ temp;
					
						temp = sDate.getFullYear().toString();
					
						dateString = dateString + "/"+ temp;
					}
					else if (sRelDateFormula.substring(sRelDateFormula.length-1,sRelDateFormula.length) == 'Y')
					{
						sDate.setFullYear(sDate.getFullYear() + parseInt(sRelDateFormula.substring(0,2)));
						
						temp = (sDate.getMonth()+1).toString();
						
						if (temp.length == 1)
						{
							temp = "0"+ temp;
						}
					
						dateString = temp;
					
						temp = (sDate.getDate()).toString();
					
						if (temp.length == 1)
						{
							temp = "0"+ temp;
						}
					
						dateString = dateString + "/"+ temp;
					
						temp = sDate.getFullYear().toString();
					
						dateString = dateString + "/"+ temp;
					}
					window.document.frmData.txtDtofevnt.disabled = true;
					window.document.frmData.btnDtofevnt.disabled = true;
					window.document.frmData.txtDtofevnt.value = dateString;
				}
			}
	
			function ButtonEnable()
			{
				var selIndex;
				window.document.frmData.btnChange.disabled = true;
				
				if(window.document.frmData.lstCriteria.selectedIndex >= 0)
					window.document.frmData.btnDelete.disabled = false; 
				else
					window.document.frmData.btnDelete.disabled = true; 
				
				if(document.frmData.lstCriteria.selectedIndex >= 0)
					document.frmData.txtDtofevnt.value = formatDate(arrData[document.frmData.lstCriteria.selectedIndex]);
				
				selIndex = selOperators(document.frmData.lstCriteria.selectedIndex);
				document.frmData.cboOperators.selectedIndex = selIndex;
				matchOperator(pOperator[document.frmData.lstCriteria.selectedIndex]);
	
				oOption = '';
				for(i=0;i<document.frmData.lstCriteria.length;i++)
					oOption = oOption + document.frmData.lstCriteria.options[i].text;
					
				if(oOption.indexOf("OLECRIT") > -1)
					document.frmData.txtDtofevnt.value = '';
			}
	
			function EnableChange()
			{
				if(window.document.frmData.lstCriteria.selectedIndex >= 0 )
					window.document.frmData.btnChange.disabled = false;
				else
					window.document.frmData.btnChange.disabled = true;
			}
	
			function HideFields()
			{
				if((window.document.frmData.cboOperators.selectedIndex==window.document.frmData.cboOperators.length-1)||(window.document.frmData.cboOperators.selectedIndex==window.document.frmData.cboOperators.length-2))
				{
					window.document.frmData.cboDay.style.display = "none";
					window.document.frmData.txtDtofevnt.style.display = "none";
					window.document.frmData.btnDtofevnt.style.display = "none";
					window.document.frmData.lblValue.style.display = "none";
				}
				else 
				{
					window.document.frmData.cboDay.style.display = "";
					window.document.frmData.txtDtofevnt.style.display = "";
					window.document.frmData.btnDtofevnt.style.display = "";
					window.document.frmData.lblValue.style.display = "";
				}
			}	
	
			function window_onLoad()
			{
				// Start Naresh MITS 7836 09/11/2006
				var objCheckBox=eval("document.frmData.chkHide");
				objCheckBox.style.display='none';
				var objTextBox=eval("document.frmData.txtRMNetLabel");
				objTextBox.style.display='none';
				// End Naresh MITS 7836 09/11/2006
				//-- Handling Relative Date
				var arrRelDate = window.document.frmData.hdRelativeDate.value.split("^^^^^");	
				
				if (arrRelDate.length > 0 && window.document.frmData.hdRelativeDate.value != -1 && window.document.frmData.hdRelativeDate.value != "")
				{
					for(i=0;i<arrRelDate.length;i++)
					{
						var arrPos = FindCharacter(arrRelDate[i], '|', 2).split(",");
						var pos1 = arrPos[0];
						var pos2 = arrPos[1];
						arrRelFormulaName[i] = arrRelDate[i].substr(pos1,pos2-pos1-1);
						arrRelFormulaValue[i] = arrRelDate[i].substr(pos2,arrRelDate[i].length-pos2);
						add_item_relative_date(arrRelFormulaName[i])			
					}
				}
				
				if(window.document.frmData.hdSelSubQuery.value == 1)
					window.document.frmData.chkSubquery.checked = true;

				if(window.document.frmData.hdSelHide.value == 1)
					window.document.frmData.chkHide.checked = true;

				if(window.document.frmData.hdSelRMNetLabel.value != '')
					window.document.frmData.txtRMNetLabel.value = window.document.frmData.hdSelRMNetLabel.value;
				
				//-- Handling OLE Criteria(Name & Description)
				if(window.document.frmData.hdSelOLECriteriaItem.value == '' || window.document.frmData.hdSelOLECriteriaItem.value == '`````')
				{
					if(window.document.frmData.hdOLECritName.value == '')
						window.document.frmData.hdOLECritName.value = window.document.frmData.hdFieldName.value;
					if(window.document.frmData.hdOLECritDesc.value == '')
						window.document.frmData.hdOLECritDesc.value = window.document.frmData.hdFieldName.value;
				}
				else
				{
					var tempArrayOLE,tempDataArray;
					tempArrayOLE = window.document.frmData.hdSelOLECriteriaItem.value.split("~~~~~");
					tempDataArray = tempArrayOLE[1].split("^^^^^");
					window.document.frmData.hdOLECritName.value = tempDataArray[0];
					window.document.frmData.hdOLECritDesc.value = tempDataArray[1];
				}
								
				var today = new Date();
				var dateString;
				var temp;
				
				temp = (today.getMonth()+1).toString();
				
				if (temp.length == 1)
				{
					temp = "0"+ temp;
				}
				
				dateString = temp;
				
				temp = (today.getDate()).toString();
				
				if (temp.length == 1)
				{
					temp = "0"+ temp;
				}
				
				dateString = dateString + "/"+ temp;
				
				temp = today.getFullYear().toString();
				
				dateString = dateString + "/"+ temp;
				
				if(dateString == document.frmData.txtDtofevnt.value)
				{
					document.frmData.cboDay.selectedIndex = 1;
				}
				
				if(document.frmData.hdSelCriteriaItem.value != '')
				{
					formatCriteria();
						
					var arrPCriteria, arrOperators;;
					arrPCriteria = document.frmData.hdSelCriteriaItem.value.split("~~~~~");
					arrOperators =  arrPCriteria[1].split(",");
					
					pOperator.length = 0
					for(i=0;i<arrOperators.length;i++)
						pOperator[i] = matchOperator(arrOperators[i])
					
					operatorList = arrPCriteria[1];
					pAttributes = arrPCriteria[7];
					matchOperator(pOperator[0]);
					arrData = arrPCriteria[2].split("^^^^^");
					document.frmData.txtDtofevnt.value = formatDate(arrData[0]);
					document.frmData.cboOperators.selectedIndex = pOperator[0];					
					
					if(document.frmData.hdSelCriteriaItem.value.indexOf('OLECRIT') > -1)
					{
						document.frmData.chkOLE.checked = true;
						document.frmData.btnDelete.disabled = true;
						document.frmData.txtDtofevnt.value = '';
						OLEEnable();
					}
				}
				
				if(window.document.frmData.hdSelCriteria.value != '')
				{
					document.frmData.lstCriteria.selectedIndex = 0;
					
					if(document.frmData.hdSelCriteriaItem.value.indexOf('OLECRIT') != -1)
						document.frmData.btnDelete.disabled = true;
					else
						document.frmData.btnDelete.disabled = false;
				}
			}	

			function formatCriteria()
			{
				var sEditCriteria = window.document.frmData.hdSelCriteriaItem.value;
				var lOperator,lData,lConnector,sFieldName,sCriteria,tempArray; 
				tempArray = sEditCriteria.split("~~~~~");
				
				 
				lOperator = tempArray[1].split(",");
				lData = tempArray[2].split("^^^^^");
				lConnector = tempArray[3].split(",");
				sFieldName = tempArray[4];
				for(iCount=0;iCount<lOperator.length;iCount++)
				{	
					index = matchOperator(lOperator[iCount]);

					switch(index)
					{
						case 0:
							txt = "<";
							
							if(lConnector[iCount] != "none")
								sCriteria = lConnector[iCount].toUpperCase() + " " +"("+sFieldName+txt+"'"+lData[iCount]+"'"+")";
							else
								sCriteria = "("+sFieldName+txt+"'"+lData[iCount]+"'"+")";
							add_item(sCriteria);
							break;

						case 1:
							txt="<=";
							
							if(lConnector[iCount] != "none")
								sCriteria = lConnector[iCount].toUpperCase() + " " +"("+sFieldName+txt+"'"+lData[iCount]+"'"+")";
							else
								sCriteria = "("+sFieldName+txt+"'"+lData[iCount]+"'"+")";
							add_item(sCriteria);
							break;


						case 2:
							txt="=";
							
							if(lConnector[iCount] != "none")
								sCriteria = lConnector[iCount].toUpperCase() + " " +"("+sFieldName+txt+"'"+lData[iCount]+"'"+")";
							else
								sCriteria = "("+sFieldName+txt+"'"+lData[iCount]+"'"+")";
							add_item(sCriteria);
							break;


						case 3:
							txt=">=";
							
							if(lConnector[iCount] != "none")
								sCriteria = lConnector[iCount].toUpperCase() + " " +"("+sFieldName+txt+"'"+lData[iCount]+"'"+")";
							else
								sCriteria = "("+sFieldName+txt+"'"+lData[iCount]+"'"+")";
							add_item(sCriteria);
							break;

						case 4:
							txt=">";
							
							if(lConnector[iCount] != "none")
								sCriteria = lConnector[iCount].toUpperCase() + " " +"("+sFieldName+txt+"'"+lData[iCount]+"'"+")";
							else
								sCriteria = "("+sFieldName+txt+"'"+lData[iCount]+"'"+")";
							add_item(sCriteria);
							break;

						case 5:
							txt="<>";
						
							if(lConnector[iCount] != "none")
								sCriteria = lConnector[iCount].toUpperCase() + " " +"("+sFieldName+txt+"'"+lData[iCount]+"'"+")";
							else
								sCriteria = "("+sFieldName+txt+"'"+lData[iCount]+"'"+")";
							add_item(sCriteria);
							break;
				
						case 6:
							txt=" LIKE ";
							
							if(lConnector[iCount] != "none")
								sCriteria = lConnector[iCount].toUpperCase() + " " +"("+sFieldName+txt+"'"+"%"+lData[iCount]+"%"+"'"+")";
							else
								sCriteria = "("+sFieldName+txt+"'"+"%"+lData[iCount]+"%"+"'"+")";
							add_item(sCriteria);
							break;

						case 7:
							txt=" LIKE ";
							if(lConnector[iCount] != "none")
								sCriteria = lConnector[iCount].toUpperCase() + " " +"("+sFieldName+txt+"'"+lData[iCount]+"'"+")";
							else
								sCriteria = "("+sFieldName+txt+"'"+lData[iCount]+"'"+")";
							add_item(sCriteria);
							break;

						case 8:
							txt=" NOT LIKE ";
							if(lConnector[iCount] != "none")
								sCriteria = lConnector[iCount].toUpperCase() + " " +"("+sFieldName+txt + "'"+"%"+lData[iCount]+"%"+"'"+")";
							else
								sCriteria = "("+sFieldName+txt + "'"+"%"+lData[iCount]+"%"+"'"+")";
							add_item(sCriteria);
							break;
			
						case 9:
							txt=" NOT LIKE ";
							if(lConnector[iCount] != "none")
								sCriteria = lConnector[iCount].toUpperCase() + " " +"("+sFieldName+txt+"'"+lData[iCount]+"'"+")";
							else
								sCriteria = "("+sFieldName+txt+"'"+lData[iCount]+"'"+")";
							add_item(sCriteria);
							break;
				
						case 10:
							txt=" LIKE ";
							if(lConnector[iCount] != "none")
								sCriteria = lConnector[iCount].toUpperCase() + " " +"("+sFieldName+txt+"'"+lData[iCount]+"%"+"'"+")";
							else
								sCriteria = "("+sFieldName+txt+"'"+lData[iCount]+"%"+"'"+")";
							add_item(sCriteria);
							break;
							
						case 11:
							txt=" NOT LIKE ";
							if(lConnector[iCount] != "none")
								sCriteria = lConnector[iCount].toUpperCase() + " " +"("+sFieldName+txt+"'"+lData[iCount]+"%"+"'"+")";
							else
								sCriteria = "("+sFieldName+txt+"'"+lData[iCount]+"%"+"'"+")";
							add_item(sCriteria);
							break;
							
						case 12:
							txt=" LIKE ";
							if(lConnector[iCount] != "none")
								sCriteria = lConnector[iCount].toUpperCase() + " " +"("+sFieldName+txt+"'"+"%"+lData[iCount]+"'"+")";
							else
								sCriteria = "("+sFieldName+txt+"'"+"%"+lData[iCount]+"'"+")";
							add_item(sCriteria);
							break;
							
						case 13:
							txt=" NOT LIKE ";
							if(lConnector[iCount] != "none")
								sCriteria = lConnector[iCount].toUpperCase() + " " +"("+sFieldName+txt+"'"+"%"+lData[iCount]+"'"+")";
							else
								sCriteria = "("+sFieldName+txt+"'"+"%"+lData[iCount]+"'"+")";
							add_item(sCriteria);
							break;
							
						case 14:
							txt=" IS NULL ";
							if(lConnector[iCount] != "none")
								sCriteria = lConnector[iCount].toUpperCase() + " " +"("+sFieldName+txt+")";
							else
								sCriteria = "("+sFieldName+txt+")";
							add_item(sCriteria);
							break;
							
						case 15:
							txt=" IS NOT NULL ";
						
							if(lConnector[iCount] != "none")
								sCriteria = lConnector[iCount].toUpperCase() + " " +"("+sFieldName+txt+")";
							else
								sCriteria = "("+sFieldName+txt+")";
							add_item(sCriteria);
							break;

					}//End For
				}
			
			}//End formatCriteria

			function ChangeCriteria()
			{
				var strDate;
				var strFieldName;
				var strOperator;
				var arrOperator = new Array();
				arrOperator[0] = "<";
				arrOperator[1] = "<=";
				arrOperator[2] = "=";
				arrOperator[3] = ">=";
				arrOperator[4] = ">";
				arrOperator[5] = "<>";
				arrOperator[6] = "LIKE";
				arrOperator[7] = "NOT LIKE";
				arrOperator[8] = "NOT LIKE";
				arrOperator[9] = "LIKE";
				arrOperator[10] = "NOT LIKE";
				arrOperator[11] = "LIKE";
				arrOperator[12] = "NOT LIKE";
				arrOperator[13] = "IS NULL";
				arrOperator[14] = "IS NOT NULL";
	
				strFieldName = window.document.frmData.hdFieldName.value;
				strOperator = arrOperator[window.document.frmData.lstCriteria.selectedIndex];
				var arrDate = new Array;
				if(window.document.frmData.txtDtofevnt.value != "")
				{ 
				strDate = window.document.frmData.txtDtofevnt.value;
				arrDate = strDate.split("/");
				strDate = arrDate[2]+arrDate[0]+arrDate[1];
				if(window.document.frmData.lstCriteria.selectedIndex == 6 || window.document.frmData.lstCriteria.selectedIndex == 8)
					strDate = "%"+strDate+"%";
				if(window.document.frmData.lstCriteria.selectedIndex == 7 ||window.document.frmData.lstCriteria.selectedIndex == 9)
					strDate = strDate;
				if(window.document.frmData.lstCriteria.selectedIndex == 9 || window.document.frmData.lstCriteria.selectedIndex == 10)
					strDate = strDate+"%";
				if(window.document.frmData.lstCriteria.selectedIndex == 11 || window.document.frmData.lstCriteria.selectedIndex == 12)
					strDate = "%"+strDate;
				if(window.document.frmData.lstCriteria.selectedIndex == 13 ||window.document.frmData.lstCriteria.selectedIndex == 14)
					strDate = "";
				}
			}
		</script>
	</head>
	
	<body onLoad="window_onLoad();pageInitialize();">
	<form name="frmData" topmargin="0" leftmargin="0" onSubmit="return false;">
		<table class="formsubtitle" border="0" width="100%" align="center">
			<tr>
				<td class="ctrlgroup" width="33%">Criteria :</td>
			</tr>
		</table>					

		<table width="100%" border="0">
			<tr><td colspan="4"><b><%=Response.Write(Request.QueryString("FIELDNAME"))%></b></td></tr>
			<tr height1="8%">
				<td width="25%"></td>
				<td width="30%">
					<select name="cboOperators" style1="WIDTH: 80%" height="1" onchange="Subquery();EnableChange();HideFields();">
						<option><(Less Than)</option>
						<option><=(Less Than or Equal)</option>
						<option>=(Equal To)</option>
						<option>>=(Greater Than or Equal)</option>
						<option>>(Greater Than)</option>
						<option><>(Not Equal To)</option>
						<option>Contains</option>
						<option>Like</option>
						<option>Does Not Contain</option>
						<option>Is Not Like</option>
						<option>Begins With</option>
						<option>Does Not Begin With</option>
						<option>Ends With</option>
						<option>Does Not End With</option>
						<option>Is Null</option>
						<option>Is Not Null</option>
					</select>
				</td>
				<td width="20%">&nbsp;<select name="cboDay" style="WIDTH:70%" height="1" onChange="selectDay();EnableChange()" onClick="selectDay();EnableChange()">
									<option>DATE</option>
									<option>TODAY</option>
								</select>
				</td>
				<td width="25%"><input type="checkbox" value1="" name="chkSubquery" onclick="Subquery()" >Subquery Criteria</td>
			</tr>
			<tr height1="8%" >
				<td><input type="text" name="lblValue" Value="Value:" style="border:0" readonly></td>
				<td><input type="text" name="txtDtofevnt" size="15" onKeyDown="EnableChange();" onBlur1="return IsDate(this)" onBlur="dateLostFocus('txtDtofevnt')">&nbsp;<input type="button" class="button" value="..." name="btnDtofevnt" onClick="selectDate('txtDtofevnt')"></td>
				<td>&nbsp;<input class="button" type="button" style1="WIDTH: 25%" name="btnChange" value="Change" disabled onClick="ChangeCriteria();change_click();"></td>
				<td><input type="checkbox" value1="" name="chkOLE" onclick="OLEEnable()" >OLE Criteria</td>
			</tr>
			<tr height1="8%">
				<td><input class="button" type="button" style1="WIDTH: 25%" name="btnAnd" value="   AND   " onclick="AND_OR_Click('AND')"></td>
				<td><input class="button" type="button" style1="WIDTH: 25%" name="btnOr" value="     OR     " onclick="AND_OR_Click('OR')"></td>
				<td>&nbsp;<input class="button" type="button" style1="WIDTH: 25%" name="btnDelete" value=" Delete " onclick="delete_click()" disabled></td>
				<td>&nbsp;<input class="button" type="button" style1="WIDTH: 25%" name="btnOLE" value=" OLE Params" onClick="window.open('OLEParam.asp','OLEcri','resizable=yes,Width=400,Height=220,top='+(screen.availHeight-220)/2+',left='+(screen.availWidth-400)/2+'')" disabled></td>
				
			</tr>
			<tr height1="68%">
				<td colspan="4">
					<select  name="lstCriteria" style="WIDTH:100%" size="15"  onChange="ButtonEnable()">
					</select>
				</td>
			</tr>
			<tr><!-- Rajeev -- 03/25/2004 -- Added feature of RMNet labelling-->
				<td>
					<!-- Start Naresh MITS 7836 09/11/2006-->
					<input type="checkbox" name="chkHide"><!--Hide in RMNet-->
					<!-- End Naresh MITS 7836 09/11/2006-->
				</td>
				<!-- Start Naresh MITS 7836 09/11/2006-->
				<td colspan="3" align="right"><!--RMNet label:-->
				<!-- End Naresh MITS 7836 09/11/2006-->
					<input type="text" name="txtRMNetLabel" value="" onChange1="window.close()">
				</td>
			</tr>
		</table>
		<hr>
		<table border="0" width="100%" align="center" cellspacing="0" cellpadding="0">
			<tr>
				<td align="middle">
					<input class="button" type="button" style1="WIDTH: 30%" name="ok" value="    OK    " onClick="ok_click()">
					<input class="button" type="button" style1="WIDTH: 30%" name="cancel" value="Cancel" onClick="window.close()">
					<input class="button" type="button" style1="WIDTH: 30%" name="help" value="  Help  ">
				</td>
			</tr>
		</table>
		<input type="hidden" name="hdFieldName" value="<%=Request.QueryString("FIELDNAME")%>">
		<input type="hidden" name="hdTableId" value="<%=Request.QueryString("TABLEID")%>">
		<input type="hidden" name="hdFieldId" value="<%=Request.QueryString("FIELDID")%>">
		<input type="hidden" name="hdFieldType" value="<%=Request.QueryString("FIELDTYPE")%>">
		<input type="hidden" name="hdDBTableId" value="<%=Request.QueryString("DBTABLEID")%>">
		<input type="hidden" name="hdSelCriteria" value="<%=fixDoubleQuote(Request.QueryString("CRITERIA"))%>">
		<input type="hidden" name="hdSelCriteriaIndex" value="<%=Request.QueryString("CRITERIAINDEX")%>">
		<input type="hidden" name="hdSelSubQuery" value="<%=Request.QueryString("SUBQUERY")%>">
		<input type="hidden" name="hdSelHide" value="<%=Request.QueryString("HIDEINRMNET")%>">
		<input type="hidden" name="hdSelRMNetLabel" value="<%=fixDoubleQuote(Request.QueryString("RMNETLABEL"))%>">
		<input type="hidden" name="hdSelCriteriaItem" value="<%=fixDoubleQuote(Request.QueryString("PCRITERIA"))%>">
		<input type="hidden" name="hdSelOLECriteriaItem" value="<%=fixDoubleQuote(Request.QueryString("POLECRITERIA"))%>">
		<input type="hidden" name="hdSelDateFormulaItem" value="<%=fixDoubleQuote(Request.QueryString("PDATEFORMULA"))%>">
		<input type="hidden" name="hdOLECritName" value="">
		<input type="hidden" name="hdOLECritDesc" value="">
		<input type="hidden" name="hdRelativeDate" value="<%=fixDoubleQuote(sDateFormula)%>">
		
	</form>
	</body>
</html>