<!-- #include file ="SessionInitialize.asp" -->
<!-- #include file ="criteriautils.inc" -->
<!-- #include file ="dbutil.inc" -->
<%Public objSession
initSession objSession
%> 
<%
function OutputDateTime(sDTTM)
dim sDate, sTime

if len(sDTTM) <> 14 then
	OutputDateTime = ""
	exit function
end if

sDate = FormatDateTime(DateSerial(left(sDTTM, 4), mid(sDTTM, 5, 2), mid(sDTTM, 7, 2)), vbShortDate)
sTime = FormatDateTime(TimeSerial(mid(sDTTM, 9, 2), mid(sDTTM, 11, 2), right(sDTTM, 2)), vbLongTime)

OutputDateTime = sDate & " " & sTime
end function
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<META HTTP-EQUIV="Refresh" CONTENT="60">

<html><head><title>Job Queue</title>
<!--[Rajeev 11/15/2002 CSS Consolidation] link rel="stylesheet" href="sm.css" type="text/css"/-->
<link rel="stylesheet" href="RMNet.css" type="text/css" />
<script language="JavaScript" SRC="smqueue.js"></script>
<script language="JavaScript">
var ns, ie, ieversion;
var browserName = navigator.appName;                   // detect browser 
var browserVersion = navigator.appVersion;
if (browserName == "Netscape")
{
	ie=0;
	ns=1;
}
else		//Assume IE
{
	ieversion=browserVersion.substr(browserVersion.search("MSIE ")+5,1);
	ie=1;
	ns=0;
}

function viewFile(vURL)
{
	setTimeout('document.location = "' + vURL + '"', 100);
}

function onPageLoaded()
{
	
}
</script>
</head>
<body onload="onPageLoaded()">

<form name="frmData">
<input type="hidden" name="jobs" value="" />
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr><td class="ctrlgroup2" colspan="8">Job Queue</td></tr>
<tr>
<td class="colheader3" width="25%">Job Name</td>
<td class="colheader3" width="30%">Job Description</td>
<td class="colheader3" width="25%">Status</td>
</tr>
</table>
	<DIV id="divForms" class="divScroll">
		<TABLE bgcolor="white" border="0" width="100%" height="0">

<%
dim r
dim sSQL
dim h_env
dim db, db2
dim rs, rs2
dim vName, vDesc, vStartDTTM, bComplete, vCompleteDTTM, bAssigned, vAssignedDTTM, lJobID, vOutputPathURL, vMsg, bErrorFlag
dim b
dim i, pos

i = 0

sSQL = "SELECT JOB_ID,JOB_NAME,JOB_DESC,OUTPUT_PATH_URL,START_DTTM,ASSIGNED,ASSIGNED_DTTM,COMPLETE,COMPLETE_DTTM,ERROR_FLAG FROM SM_JOBS WHERE "
sSQL = sSQL & "SM_JOBS.USER_ID = " & objSessionStr("UserID") & " AND (ARCHIVED = 0 OR ARCHIVED IS NULL)"
sSQL = sSQL & " ORDER BY JOB_ID DESC"
' sSQL = sSQL & " ORDER BY ASSIGNED DESC, COMPLETE DESC,COMPLETE_DTTM DESC"

set r = CreateObject("DTGRocket")

h_env = r.DB_InitEnvironment()

db = r.DB_OpenDatabase(h_env, Application("SM_DSN"), 0)
db2 = r.DB_OpenDatabase(h_env, Application("SM_DSN"), 0)

rs = r.DB_CreateRecordset(db, sSQL, 3, 0)
while not r.DB_EOF(rs)
	r.DB_GetData rs, "JOB_ID", lJobID
	r.DB_GetData rs, "JOB_NAME", vName
	r.DB_GetData rs, "JOB_DESC", vDesc
	r.DB_GetData rs, "OUTPUT_PATH_URL", vOutputPathURL
	r.DB_GetData rs, "START_DTTM", vStartDTTM
	r.DB_GetData rs, "ASSIGNED", bAssigned
	r.DB_GetData rs, "ASSIGNED_DTTM", vAssignedDTTM
	r.DB_GetData rs, "COMPLETE", bComplete
	r.DB_GetData rs, "COMPLETE_DTTM", vCompleteDTTM
	r.DB_GetData rs, "ERROR_FLAG", bErrorFlag
	
	If bComplete And vOutputPathURL<>"" Then
		' Adjust for right server name etc.
		pos=InstrRev(vOutputPathURL,"/")
		vOutputPathURL=Mid(vOutputPathURL,pos+1)
		'If pos>0 And False Then
		'	Dim s
		'	s=Request.ServerVariables("SCRIPT_NAME")
		'	If InstrRev(s,"/")>0 Then s=Left(s,InstrRev(s,"/")-1)
		'	s=Request.ServerVariables("SERVER_NAME") & s & Mid(vOutputPathURL,pos)
		'	If UCase(Request.ServerVariables("HTTPS"))="ON" Then
		'		s="https://" & s
		'	Else
		'		s="http://" & s
		'	End if
		'	vOutputPathURL=s
		'End If
	End If
%>

<% if i mod 2 = 0 then %>
<tr class="rowlight1">
<% else %>
<tr class="rowdark1">
<% end if %>

<td width="25%">
<input name="seljob<% = lJobID %>" type="checkbox"></input>&nbsp;

<% if bComplete then %>
<a class="td8" href="<%= vOutputPathURL %>"><% = vName %></a>
<% else %>
<% = vName %>
<% end if %>
</td>

<td class="td8" width="25%">
<% = vDesc %>
</td>

<td class="td8" width="50%">
<% if Not bAssigned then %>
<font color="6e6e6e"><b>Waiting</b></font>
<% elseif Not bComplete then %>
<font color="blue"><b>
Running</b> (Started @ <% = OutputDateTime(vAssignedDTTM) %>)
<br />
<% 
	rs2 = r.DB_CreateRecordset(db2, "SELECT MSG FROM SM_JOB_LOG WHERE JOB_ID = " & lJobID & " AND MSG_TYPE = 'progress' ORDER BY LOG_ID DESC", 3, 0)
	if Not r.DB_EOF(rs2) then
		r.DB_GetData rs2, 1, vMsg
		vMsg = vMsg & ""
		vMsg = Replace(vMsg, vbCrLf, "<br />")
		vMsg = Replace(vMsg, vbCr, "<br />")
		vMsg = Replace(vMsg, vbLf, "<br />")
		Response.Write vMsg
	end if
	r.DB_CloseRecordset CInt(rs2), 2

%>
</font>
<% else ' complete %>
	<% if bErrorFlag = -1 then %>
		<font color="red"><b>
		Error Occurred</b>
<%
		rs2 = r.DB_CreateRecordset(db2, "SELECT MSG FROM SM_JOB_LOG WHERE JOB_ID = " & lJobID & " AND MSG_TYPE = 'error' ORDER BY LOG_ID DESC", 3, 0)
		if Not r.DB_EOF(rs2) then
			r.DB_GetData rs2, 1, vMsg
			vMsg = vMsg & ""
			vMsg = Replace(vMsg, vbCrLf, "<br />")
			vMsg = Replace(vMsg, vbCr, "<br />")
			vMsg = Replace(vMsg, vbLf, "<br />")
			Response.Write vMsg
		end if
		r.DB_CloseRecordset CInt(rs2), 2
		Response.Write "<br />" & vMsg
%>
		</font>
		</td>
	<% else %>
		<font color="green"><b>
		Complete</b> (Started @ <% = OutputDateTime(vAssignedDTTM) %> and Completed @ <% = OutputDateTime(vCompleteDTTM) %>)
		</font>
		</td>
	<% end if %>
<% end if %>

</tr>

<%
	i = i + 1
	r.DB_MoveNext rs
wend
r.DB_CloseRecordset CInt(rs), 2
r.DB_CloseDatabase CInt(db)
r.DB_CloseDatabase CInt(db2)

r.DB_FreeEnvironment CLng(h_env)

set r = Nothing
'01/04/07 REM   UMESH
		Dim	 objectXML , sXML
		Dim  bArchive, bEmail, bDelete
		bArchive = bARCHIVE_Q
		bEmail = bEMAIL_Q
		bDelete = bDELETE_Q
		
		OpenSessDatabase
		sWhere = "FILENAME='customize_reports'"
		sXML = GetSingleSessValue("CONTENT","CUSTOMIZE",sWhere)
		CloseSessDatabase
		if sXML<>"" then
			Set objectXML = CreateObject("Microsoft.XMLDOM")
			if (objectXML.loadXML(sXML) = false) then
			Response.Write "Error: Selected Customize File could not be loaded.  Please try uploading it again."
			Response.End
			end if
			
			bArchive = objectXML.SelectSingleNode("//RMAdminSettings/ReportMenu/ReportQueue/Archive").text
			if bArchive ="" then bArchive="0"
			bEmail = objectXML.SelectSingleNode("//RMAdminSettings/ReportMenu/ReportQueue/Email").text
			if bEmail ="" then bEmail="0"
			bDelete = objectXML.SelectSingleNode("//RMAdminSettings/ReportMenu/ReportQueue/Delete").text
			if bDelete ="" then bDelete="0"
		end if
		
	'End REM   UMESH	
%>
</TABLE>
</DIV>
<font class="small">This page shows current reporting activity and allows you to manage report output.
<br />To view report results, click on a job name.
<br />To Archive, Email or Delete one or more jobs, place a checkmark next to the target jobs and click the desired button.
<br />*** For information on extended reporting features and report troubleshooting please see the <a href="smfaq.htm">Frequently Asked Questions (FAQ)</a>.
</font>
<table border="0">
<tr>
<td><input type="button" class="button" name="cmdAll" value="Select All" onclick="for(var i = 0; i<document.frmData.elements.length;i++) if(document.frmData.elements[i].type=='checkbox') document.frmData.elements[i].checked = true;"/></td>
<%If bArchive THEN %>
<td><input type="button" class="button" name="cmdMove" value="   Archive   " onClick="ArchiveJob()" /></td>
<%End If%>
<%If bEmail THEN %>
<td><input type="button" class="button" name="cmdEmail" value="  E-Mail  " onClick="EMailJob()" /></td>
<%End If%>
<%If bDelete THEN %>
<td><input type="button" class="button" name="cmdDelete" value="  Delete  " onClick="DeleteJob()" /></td>
<%End If%>
<td><input type="button" class="button" name="cmdRefresh" value="  Refresh  " onClick="window.location='smrepqueue.asp'" /></td>
</tr>
</table>
</form>
</body>
</html>
