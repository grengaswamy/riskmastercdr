
<script language="VBScript" runat="server">
'custom exec summary report code


Public Sub CustomBeforeTransform(objXML)
  Dim objNode, objectXML ,sExsummName
  Dim henv,sSQL,r,db,sXML
  sSQL = "Select CONTENT from CUSTOMIZE where FILENAME='customize_custom'"
  set r = CreateObject("DTGRocket")
  henv = r.DB_InitEnvironment()
  db = r.DB_OpenDatabase(henv,""& Application(APP_SESSIONDSN), 0)
  rs = r.DB_CreateRecordset(db,sSQL, 3, 0)
  If Not r.DB_EOF(cint(rs)) Then r.DB_GetData rs, 1, sXML
  r.DB_CloseRecordset CInt(rs), 1
  r.DB_CloseDatabase CInt(db)
	if sXML<>"" then
      Set objectXML = CreateObject("Microsoft.XMLDOM")
      if (objectXML.loadXML(sXML) = false) then
        Response.Write "Error: Selected Customize File could not be loaded.  Please try uploading it again."
        Response.End
      end if
      sExsummName=objectXML.SelectSingleNode("//RMAdminSettings/Caption/ExecutiveSummary").text
      Set objNode = objXML.selectSingleNode("report")
      objNode.setAttribute "title", sExsummName

      set objNode=objXML.selectSingleNode("//control[@name='recordtype']")
      objNode.removeChild objNode.lastChild

      Set objNode = objXML.selectSingleNode("//control[@name='events']")
      objNode.parentNode.removeChild objNode
    end if
End Sub



Public Sub CustomGetName(sName)
  Dim  objectXML,sExsummName
  Dim henv,sSQL,r,db,sXML
  sSQL = "Select CONTENT from CUSTOMIZE where FILENAME='customize_custom'"
  set r = CreateObject("DTGRocket")
  henv = r.DB_InitEnvironment()
  db = r.DB_OpenDatabase(henv, Application(APP_SESSIONDSN), 0)
  rs = r.DB_CreateRecordset(db, sSQL, 3, 0)
  If Not r.DB_EOF(cint(rs)) Then r.DB_GetData rs, 1, sXML
  r.DB_CloseRecordset CInt(rs), 1
  r.DB_CloseDatabase CInt(db)

 
  if sXML<>"" then
    Set objectXML = CreateObject("Microsoft.XMLDOM")
    if (objectXML.loadXML(sXML) = false) then
      Response.Write "Error: Selected Customize File could not be loaded.  Please try uploading it again."
      Response.End
    end if
    sExsummName=objectXML.SelectSingleNode("//RMAdminSettings/Caption/ExecutiveSummary").text
    sName=sExsummName
  end if
End Sub
</script>