<!-- #include file ="session.inc" -->
<!-- #include file ="generic.asp" -->

<html>
	<head>
		<link rel="stylesheet" href="RMNet.css" type="text/css"/>
		<title>SMD - [Criteria]</title>
		<script language="JavaScript" SRC="SMD.js"></script>

		<script language="JavaScript">
	
			var btnClick;
			btnClick = ""
			var trackChange;
			trackChange = 1;
			var txtNull;
			var txtEnter;
			var pAttributes;
			var operatorList;
			operatorList = '';
			var connectorList;
			connectorList = '';
			var arrData = new Array;
			arrData.length = 0;
			var pOperator = new Array;
			pOperator.length = 0;
	
			function or_click()
			{
				if(document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == false &&  (document.frmData.txtFrom.value != '' || document.frmData.txtTo.value != '') && trackChange == 1)
				{
					document.frmData.txtFrom.value = "";
					document.frmData.txtTo.value = "";
					document.frmData.btnDelete.disabled = true;
				}
				
				if(document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)
					check(1);
				else
				{
					if(trackChange == 0 && document.frmData.txtFrom.value == '' && document.frmData.txtTo.value == '')
					{
						arrData[arrData.length] = document.frmData.txtFrom.value + "!!!!!" + document.frmData.txtTo.value;
						check(0);
					}
					else if(trackChange == 0 && (document.frmData.txtFrom.value != "" || document.frmData.txtTo.value != "") && document.frmData.btnDelete.disabled == false) //Selecting subquery criteria in edit mode.
						check(1);
					else if (document.frmData.txtFrom.value != "" || document.frmData.txtTo.value != "")
					{
						arrData[arrData.length] = document.frmData.txtFrom.value + "!!!!!" + document.frmData.txtTo.value;
						check(0);
					}
				}

				document.frmData.txtFrom.value = "";
				document.frmData.txtTo.value = "";
				document.frmData.btnChange.disabled = true;
				document.frmData.btnDelete.disabled = true;
				trackChange = 1;
				btnClick = "OR";
			}	
	
			function Subquery()
			{
				trackChange = 0;
				if(btnClick == "OR")
					btnClick = "OR";
				else
					btnClick = "";	
			}
	
			function check(pAdd)
			{
				var txt, index, FieldName;
				var oOption,selConnector;
				FieldName = window.document.frmData.hdFieldName.value;

				var valFrom, valTo, valRange, valExclude;
			   	
			   	valFrom = window.document.frmData.txtFrom.value;
			   	valTo = window.document.frmData.txtTo.value;
			   	valRange = "'" + valFrom + "' AND '" + valTo + "'";
			   	
				valExclude = "BETWEEN"

				if(document.frmData.lstCriteria.length==0 && (document.frmData.txtFrom.value!="" || document.frmData.txtTo.value!="")) //If the value being entered is the 1st one and text box is not null.
				{
					if(pAdd == 0)
						txtEnter = btnClick + " " +"(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
					else
						txtEnter = "(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
				}
				else if(valFrom == '' && valTo == '' && trackChange == 0)
				{
					if(pAdd == 0)
						txtEnter = btnClick + " " +"(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
					else
						txtEnter = "(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";

					trackChange=1;
				}	
				else if(trackChange== 0 && (valFrom != '' || valTo != '') && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == false  )//If the value being entered is not the 1st one and text box is null.
				{
					if(pAdd == 0)
						txtEnter = btnClick + " " +"(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
					else
						txtEnter = "(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
				
					trackChange=1;
				}
				else if(trackChange== 0 && (valFrom == '' && valTo == '') && document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)//If the value being entered is not the 1st one and text box is null.
				{
					trackChange=1;
					if(pAdd == 0)
						txtEnter = btnClick + "(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
					else
						txtEnter = "(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
				}
				else if(trackChange == 1 && (valFrom == '' && valTo == '') && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == true)//If the text box value is null and check box value is not changed.
				{
					return;				
				}
				else if(trackChange == 1 && (valFrom == '' && valTo == '') && document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)//If the text box value is null and check box value is not changed.
				{
					if(pAdd == 0)
						txtEnter = btnClick + " " + "(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
					else
						txtEnter = "(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
				}
				else
				{
					if(pAdd == 0)
						txtEnter = btnClick + " " + "(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
					else
						txtEnter = "(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
				}
	
				if(pAdd == 0) //-- Add mode
					add_item(txtEnter);
				else //-- Edit mode
				{	
					var selLength = document.frmData.lstCriteria.length;
					
					if(selLength == 0) //-- Need to add
					{
						if((document.frmData.txtFrom.value != "" || document.frmData.txtTo.value != "" ) || trackChange == 0)
						{
							arrData[arrData.length] = document.frmData.txtFrom.value + "!!!!!" + document.frmData.txtTo.value;
							//check(0);
						}
						add_item(txtEnter);
					}
					else
					{
						var selTxt = document.frmData.lstCriteria.options[document.frmData.lstCriteria.selectedIndex].text;
						if(selTxt.substring(0,2) == "OR")
							selConnector = "OR ";
						else
							selConnector = "";
						
						document.frmData.lstCriteria.options[document.frmData.lstCriteria.selectedIndex].text = selConnector + txtEnter;
						arrData[document.frmData.lstCriteria.selectedIndex] = document.frmData.txtFrom.value + "!!!!!" + document.frmData.txtTo.value;
					}
				}

				if(document.frmData.chkExclude.checked == true)
				{
					for(i=0;i<document.frmData.lstCriteria.length;i++)
					{
						var sCriteria = document.frmData.lstCriteria.options[i].text.substring(0,document.frmData.lstCriteria.options[i].text.indexOf("'"));
						iIndex = sCriteria.indexOf("NOT BETWEEN");
						if(iIndex == -1)
							document.frmData.lstCriteria.options[i].text = document.frmData.lstCriteria.options[i].text.replace("BETWEEN","NOT BETWEEN");
					}
					txtEnter = btnClick + "(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
				}
				else
				{
					for(i=0;i<document.frmData.lstCriteria.length;i++)
					{
						document.frmData.lstCriteria.options[i].text = document.frmData.lstCriteria.options[i].text.replace("NOT BETWEEN","BETWEEN");
					}
					txtEnter = btnClick + "(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
				}
			}//End Check

			function ok_click()
			{
				var valFrom, valTo, valRange, oOption, pcount;
				
				oOption = '';	
			   	valFrom = window.document.frmData.txtFrom.value;
			   	valTo = window.document.frmData.txtTo.value;
				valRange = "'" + valFrom + "' AND '" + valTo + "'";
				
				if(document.frmData.chkExclude.checked == true)
					valExclude = "NOT BETWEEN";
				else
					valExclude = "BETWEEN";
				
				var rangeExclude;
				
				if(document.frmData.chkExclude.checked == true)
					rangeExclude = "excluderange";
				else
					rangeExclude = "range";		
				
				//-- Initialize
				connectorList = '';
				dataList = '';
				operatorList = 'BETWEEN';

				var enterKey = String.fromCharCode(13, 10);

				//-- Check for sub query
				if(document.frmData.chkSubquery.checked == true)
					hasDatadefcrit = 1;
				else
					hasDatadefcrit = 0;

				//-- Rajeev -- 03/25/2004 -- Check for hiding in RMNet
				if(document.frmData.chkHide.checked == true)
					hideInRMNet = 1;
				else
					hideInRMNet = 0;

				//-- Rajeev -- 03/25/2004 -- fill RMNet Label
				document.frmData.hdSelRMNetLabel.value = document.frmData.txtRMNetLabel.value;

				//-- fill pAttributes
				
				if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
				{
					if(window.opener.window.document.frmData.lstSelcriteria.length > 0)
						pAttributes = document.frmData.hdTableId.value + "|" + document.frmData.hdFieldId.value + "|" + rangeExclude + "|" + "and" + "|" + hasDatadefcrit + "|" + hideInRMNet + "|" + document.frmData.hdSelRMNetLabel.value;
					else
						pAttributes = document.frmData.hdTableId.value + "|" + document.frmData.hdFieldId.value + "|" + rangeExclude + "|" + "none" + "|" + hasDatadefcrit + "|" + hideInRMNet + "|" + document.frmData.hdSelRMNetLabel.value;
				}
				else
				{
					var tempArray = window.document.frmData.hdSelCriteriaItem.value.split("~~~~~");
					var arrAttribute = tempArray[7].split("|");
					
					pAttributes = document.frmData.hdTableId.value + "|" + document.frmData.hdFieldId.value + "|" + rangeExclude + "|" + arrAttribute[4] + "|" + hasDatadefcrit + "|" + hideInRMNet + "|" + document.frmData.hdSelRMNetLabel.value;
				}
				
				//-- No data case -- just close down
				if(document.frmData.lstCriteria.length == 0 && (valFrom == '' && valTo == '') && trackChange == 0 )
				{	
					//-- Connector list
					if(btnClick == '')
						connectorList = 'none';
					else
						connectorList = btnClick;

					connectorList = connectorList + '|and';
					
					arrData[arrData.length] = valFrom + "!!!!!" + valTo;
					check(0);

					//-- fill datalist -- only one data item
					dataList = arrData[0];	
					
					//-- maintain outer main connector
					
					txtEnter = "(" + txtEnter + ")";
					
					if(document.frmData.chkExclude.checked == true)
						txtEnter = btnClick + "(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
					else
						txtEnter = btnClick + "(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
				
					if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
					{
						if(window.opener.window.document.frmData.lstSelcriteria.length > 0)
						{
							if(txtEnter != '')
								txtEnter = "AND" + " " + "(" + txtEnter + ")";
						}
							else
								txtEnter = "(" + txtEnter + ")";
					}
					else
					{
						if(window.document.frmData.hdSelCriteriaIndex.value > 0)
						{
							if(txtEnter != '')
								txtEnter = "AND" + " " + txtEnter;
						}
					}
					
					window.opener.criteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,operatorList,dataList,connectorList,document.frmData.hdFieldName.value,document.frmData.hdFieldType.value,document.frmData.hdDBTableId.value,pAttributes,(txtEnter+enterKey));			
					window.opener.OLEcriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,'','', false);
					window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,'','', false);
					window.opener.displayCriteria();
					window.opener.displayGlobalCriteria();
					window.opener.changeFieldName(window.document.frmData.hdSelCriteriaIndex.value);
					window.opener.ButtonState();
						
					window.close();
					return;		
				}
				else if(document.frmData.lstCriteria.length == 0 && (valFrom == '' && valTo == '') && trackChange == 1)
				{	
					if(document.frmData.hdSelCriteriaIndex.value == -1)
					{
						window.close();
						return;
					}
					else
					{
						window.opener.deleteClick();
						window.close();
						return;
					}
				}
				
				if(document.frmData.lstCriteria.length == 0 && (valFrom != '' || valTo != ''))
				{
					//-- Add mode -- Edit not applicable

					//-- Connector list
					if(btnClick == '')
						connectorList = 'none';
					else
						connectorList = btnClick;

					connectorList = connectorList + '|and';

					if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
					{
						arrData[arrData.length] = valFrom + "!!!!!" + valTo;
						check(0);
					}
					else
					{
						check(1);
					}
						
					//-- fill datalist -- only one data item
					dataList = arrData[0];
	
					//-- maintain outer main connector
					txtEnter = "(" + txtEnter + ")";
					
					if(document.frmData.chkExclude.checked == true)
						txtEnter = btnClick + "(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
					else
						txtEnter = btnClick + "(" + document.frmData.hdFieldName.value + " " + valExclude + " " + valRange + ")";
					
					if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
					{
						if(window.opener.window.document.frmData.lstSelcriteria.length > 0)
							txtEnter = "AND" + " " + "(" + txtEnter + ")";
						else
							txtEnter = "(" + txtEnter + ")";
					}
					else
					{
						if(window.document.frmData.hdSelCriteriaIndex.value > 0)
							txtEnter = "AND" + " " + txtEnter;
					}
					
					window.opener.criteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,operatorList,dataList,connectorList,document.frmData.hdFieldName.value,document.frmData.hdFieldType.value,document.frmData.hdDBTableId.value,pAttributes,(txtEnter+enterKey));			
					window.opener.OLEcriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,'','', false);
					window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,'','', false);
					window.opener.displayCriteria();
					window.opener.displayGlobalCriteria();
					window.opener.changeFieldName(window.document.frmData.hdSelCriteriaIndex.value);
					window.opener.ButtonState();
						
					window.close();
					return;		

				}
				else if(document.frmData.lstCriteria.length > 0 && (valFrom != '' || valTo != '') && document.frmData.btnDelete.disabled == true && document.frmData.btnChange.disabled == true)
				{
					//-- Add/Edit mode -- extended criteria will be appended the criteria of selected item in list box
					//-- fill connectorlist
					for(i=0;i<document.frmData.lstCriteria.length;i++)
					{
						if(document.frmData.lstCriteria.options[i].text.substring(0,2) == "OR")
							connectorList = connectorList + "," + "or|and";
						else
							connectorList = connectorList + "," + "none|and";
					}
					//-- append AND/OR as per the AND/OR button buffer
					connectorList = connectorList + "," + btnClick + '|and';
					
					//-- seperate out initial comma
					connectorList = connectorList.substring(1,connectorList.length);

					//-- fill datalist

					arrData[arrData.length] = valFrom + "!!!!!" + valTo;
					check(0);
					
					for(i=0;i<arrData.length;i++)
						dataList = dataList + "^^^^^" + arrData[i];	

					//-- seperate out initial comma
					dataList = dataList.substring(5,dataList.length);

					//-- build combined critera to show on parent window's list box
					for(i=0;i<document.frmData.lstCriteria.length;i++)
						oOption = oOption + document.frmData.lstCriteria.options[i].text;

				}
				else if((document.frmData.lstCriteria.length > 0 && (valFrom != '' || valTo != '') && document.frmData.btnDelete.disabled == false && document.frmData.btnChange.disabled == true))
				{
					//-- fill connectorlist
					for(i=0;i<document.frmData.lstCriteria.length;i++)
					{
						if(document.frmData.lstCriteria.options[i].text.substring(0,2) == "OR")
							connectorList = connectorList + "," + "or|and";
						else
							connectorList = connectorList + "," + "none|and";
					}
					//-- seperate out initial comma
					connectorList = connectorList.substring(1,connectorList.length);

					//-- fill datalist
					check(1);
					
					for(i=0;i<arrData.length;i++)
						dataList = dataList + "^^^^^" + arrData[i];	

					//-- seperate out initial comma
					dataList = dataList.substring(5,dataList.length);
					
					//-- build combined critera to show on parent window's list box
					for(i=0;i<document.frmData.lstCriteria.length;i++)
						oOption = oOption + document.frmData.lstCriteria.options[i].text;
				}		
				else if(document.frmData.lstCriteria.length > 0 && (valFrom != '' || valTo != '') && document.frmData.btnDelete.disabled == false && document.frmData.btnChange.disabled == false)
				{
					//-- Add/Edit mode -- extended criteria will be appended the criteria of selected item in list box
					//-- fill connectorlist
					for(i=0;i<document.frmData.lstCriteria.length;i++)
					{
						if(document.frmData.lstCriteria.options[i].text.substring(0,2) == "OR")
							connectorList = connectorList + "," + "or|and";
						else
							connectorList = connectorList + "," + "none|and";
					}
					//-- seperate out initial comma
					connectorList = connectorList.substring(1,connectorList.length);

					//-- fill datalist
					check(1);
					
					for(i=0;i<arrData.length;i++)
						dataList = dataList + "^^^^^" + arrData[i];	

					//-- seperate out initial comma
					dataList = dataList.substring(5,dataList.length);

					//-- build combined critera to show on parent window's list box
					for(i=0;i<document.frmData.lstCriteria.length;i++)
						oOption = oOption + document.frmData.lstCriteria.options[i].text;
				}
				else if(document.frmData.lstCriteria.length > 0 && (valFrom != '' || valTo != '') && document.frmData.btnDelete.disabled == true && document.frmData.btnChange.disabled == false)
				{
					//-- Add/Edit mode -- extended criteria will be appended the criteria of selected item in list box
					//-- fill connectorlist
					for(i=0;i<document.frmData.lstCriteria.length;i++)
					{
						if(document.frmData.lstCriteria.options[i].text.substring(0,2) == "OR")
							connectorList = connectorList + "," + "or|and";
						else
							connectorList = connectorList + "," + "none|and";
					}
					//-- seperate out initial comma
					connectorList = connectorList.substring(1,connectorList.length);

					check(1);
					
					for(i=0;i<arrData.length;i++)
						dataList = dataList + "^^^^^" + arrData[i];	

					//-- seperate out initial comma
					dataList = dataList.substring(5,dataList.length);

					//-- build combined critera to show on parent window's list box
					for(i=0;i<document.frmData.lstCriteria.length;i++)
						oOption = oOption + document.frmData.lstCriteria.options[i].text;
				}		
				else if(document.frmData.lstCriteria.length > 0 && (valFrom == '' && valTo == '') && trackChange == 0)
				{
					//-- fill connectorlist
					for(i=0;i<document.frmData.lstCriteria.length;i++)
					{
						if(document.frmData.lstCriteria.options[i].text.substring(0,2) == "OR")
							connectorList = connectorList + "," + "or|and";
						else
							connectorList = connectorList + "," + "none|and";
					}
					//-- append AND/OR as per the AND/OR button buffer
					connectorList = connectorList + "," + btnClick + '|and';
					
					//-- seperate out initial comma
					connectorList = connectorList.substring(1,connectorList.length);

					if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
					{
						//-- fill datalist
						arrData[arrData.length] = valFrom + "!!!!!" + valTo;
						check(0);
					}
					else
					{
						//-- fill datalist
						check(1);			
					}			
					for(i=0;i<arrData.length;i++)
						dataList = dataList + "^^^^^" + arrData[i];	

					//-- seperate out initial comma
					dataList = dataList.substring(5,dataList.length);

					//-- build combined critera to show on parent window's list box
					for(i=0;i<document.frmData.lstCriteria.length;i++)
						oOption = oOption + document.frmData.lstCriteria.options[i].text;
				
				}				
				else if(document.frmData.lstCriteria.length > 0 && (valFrom == '' && valTo == '') && trackChange == 1)
				{
					//-- fill connectorlist
					for(i=0;i<document.frmData.lstCriteria.length;i++)
					{
						if(document.frmData.lstCriteria.options[i].text.substring(0,2) == "OR")
							connectorList = connectorList + "," + "or|and";
						else
							connectorList = connectorList + "," + "none|and";
					}
					//-- seperate out initial comma
					connectorList = connectorList.substring(1,connectorList.length);

					//-- fill datalist
					check(1);
					
					for(i=0;i<arrData.length;i++)
						dataList = dataList + "^^^^^" + arrData[i];	

					//-- seperate out initial comma
					dataList = dataList.substring(5,dataList.length);
					
					//-- build combined critera to show on parent window's list box
					for(i=0;i<document.frmData.lstCriteria.length;i++)
						oOption = oOption + document.frmData.lstCriteria.options[i].text;
				}
							
				//-- append outer parenthesis 
				if((trimIt(oOption).substring(0,3) == "AND" && trimIt((oOption).substring(3,trimIt(oOption).length)).indexOf("AND") > 0) || (trimIt(oOption).substring(0,3) == "AND" && trimIt((oOption).substring(3,trimIt(oOption).length)).indexOf("OR") > 0))
					oOption = "(" +  oOption + ")" ;
				else if((trimIt(oOption).substring(0,2) == "OR" && trimIt((oOption).substring(2,trimIt(oOption).length)).indexOf("OR") > 0) || (trimIt(oOption).substring(0,2) == "AND" && trimIt((oOption).substring(2,trimIt(oOption).length)).indexOf("AND") > 0))
					oOption = "(" +  oOption + ")" ;
				else if((trimIt(oOption).substring(0,3) != "AND" && trimIt(oOption).indexOf("AND") > 0) || (trimIt(oOption).substring(0,3) != "AND" && trimIt(oOption).indexOf("OR") > 0 ))
					oOption = "(" + oOption + ")";
				else if((trimIt(oOption).substring(0,2) != "OR" && trimIt(oOption).indexOf("AND")) > 0 || (trimIt(oOption).substring(0,2) != "OR" && trimIt(oOption).indexOf("OR") > 0 ))
					oOption = "(" + oOption + ")";

				if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
				{
					if(window.opener.window.document.frmData.lstSelcriteria.length > 0)
						oOption = "AND" + " " + oOption;
				}
				else
				{	
					if(window.document.frmData.hdSelCriteriaIndex.selectedIndex > 0)
						oOption = "AND" + " " + oOption;			
				}
				
				//-- code to handle the outer connector while editing
				if(document.frmData.hdSelCriteriaIndex.value != -1)		
				{	
					var arrPAttributes = pAttributes.split("|");
					
					if(arrPAttributes[3] != "none")
						oOption = arrPAttributes[3].toUpperCase() + " " + oOption;
				}
				
				window.opener.criteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,operatorList,dataList,connectorList,document.frmData.hdFieldName.value,document.frmData.hdFieldType.value,document.frmData.hdDBTableId.value,pAttributes,(oOption+enterKey));			
				window.opener.OLEcriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,'','', false);
				window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,'','', false);
				window.opener.displayCriteria();
				window.opener.displayGlobalCriteria();
				window.opener.changeFieldName(window.document.frmData.hdSelCriteriaIndex.value);
				window.opener.ButtonState();

				if(document.frmData.hdSelCriteriaIndex.value != -1)		
					window.opener.window.document.frmData.lstSelcriteria.selectedIndex = window.document.frmData.hdSelCriteriaIndex.value;
	
				window.close();
			}
	
			function add_item(txt)
			{
				var sNewItem;
				sNewItem = new Option(txt);
				if (navigator.appName == "Netscape")
					window.document.frmData.lstCriteria.add(sNewItem, null);
				else
					window.document.frmData.lstCriteria.add(sNewItem);
			}
	
			function delete_click()
			{
				var txt;
				if(document.frmData.lstCriteria.length>1)
					{
						txt=document.frmData.lstCriteria.options[1].text
						pattern="(";
						index=txt.indexOf(pattern);
					}

				if(document.frmData.lstCriteria.selectedIndex != -1)
				{
					if(document.frmData.lstCriteria.selectedIndex=="0" && document.frmData.lstCriteria.length>1)//If 1st element is being deleted and there more than 1 elements.
					{
						arrData[0] = null;
						document.frmData.lstCriteria.options[0]=null;
						document.frmData.lstCriteria.options[0].text=txt.substring(index,txt.length);
						document.frmData.lstCriteria.selectedIndex= 0;
					}
					else if(document.frmData.lstCriteria.selectedIndex=="0" && document.frmData.lstCriteria.length == 1)//If 1st element is being deleted and its the only element.
					{
						arrData[0] = null;
						document.frmData.lstCriteria.options[0]=null;
					}
					else if(document.frmData.lstCriteria.selectedIndex == document.frmData.lstCriteria.length-1)
					{
						arrData[document.frmData.lstCriteria.selectedIndex] = null;
						document.frmData.lstCriteria.options[document.frmData.lstCriteria.selectedIndex]=null;	
						document.frmData.lstCriteria.selectedIndex=document.frmData.lstCriteria.length-1;
					}
					else
					{
						var selIndx = document.frmData.lstCriteria.selectedIndex
						arrData[document.frmData.lstCriteria.selectedIndex] = null;
						document.frmData.lstCriteria.options[document.frmData.lstCriteria.selectedIndex]=null;
						document.frmData.lstCriteria.selectedIndex = selIndx; 
					}
					
					adjust_pArray(arrData);

					if(document.frmData.lstCriteria.selectedIndex == -1)
					{
						document.frmData.btnDelete.disabled = true;
						document.frmData.txtFrom.value = '';
						document.frmData.txtTo.value = '';
					}
					else
					{
						var splitData = arrData[document.frmData.lstCriteria.selectedIndex].split('!!!!!');
						document.frmData.txtFrom.value = splitData[0];
						document.frmData.txtTo.value = splitData[1];
					}
				}
				else
				{
					return;
				}
			}
	
			function change_click()
			{
				check(1);
			}

			function ButtonEnable()
			{
				var selIndex;
				window.document.frmData.btnChange.disabled = true;
				if(window.document.frmData.lstCriteria.selectedIndex >= 0)
					window.document.frmData.btnDelete.disabled = false; 
				else
					window.document.frmData.btnDelete.disabled = true; 
				
				if(document.frmData.lstCriteria.selectedIndex >= 0)
				{
					var splitData = arrData[document.frmData.lstCriteria.selectedIndex].split('!!!!!');
					document.frmData.txtFrom.value = splitData[0];
					document.frmData.txtTo.value = splitData[1];
				}
			}
	
			function EnableChange()
			{
				if(window.document.frmData.lstCriteria.selectedIndex >= 0 )
					window.document.frmData.btnChange.disabled = false;
				else
					window.document.frmData.btnChange.disabled = true;
			}
	
			function window_onLoad()
			{
				// Start Naresh MITS 7836 09/11/2006
				var objCheckBox=eval("document.frmData.chkHide");
				objCheckBox.style.display='none';
				var objTextBox=eval("document.frmData.txtRMNetLabel");
				objTextBox.style.display='none';
				// End Naresh MITS 7836 09/11/2006
				if(window.document.frmData.hdSelSubQuery.value == 1)
					window.document.frmData.chkSubquery.checked = true;

				if(window.document.frmData.hdSelHide.value == 1)
					window.document.frmData.chkHide.checked = true;

				if(window.document.frmData.hdSelRMNetLabel.value != '')
					window.document.frmData.txtRMNetLabel.value = window.document.frmData.hdSelRMNetLabel.value;
				
				if(document.frmData.hdSelCriteriaItem.value != '')
				{
					formatCriteria();
					var arrPCriteria, arrOperators;;
					arrPCriteria = document.frmData.hdSelCriteriaItem.value.split("~~~~~");
					
					operatorList = arrPCriteria[1]; //-- BETWEEN
					pAttributes = arrPCriteria[7];
					arrData = arrPCriteria[2].split("^^^^^");
					var arrTemp = arrData[0].split('!!!!!')
					document.frmData.txtFrom.value = arrTemp[0];
					document.frmData.txtTo.value = arrTemp[1];
				}
				
				if(window.document.frmData.hdSelCriteria.value != '')
				{
					//add_item(window.document.frmData.hdSelCriteria.value);
					document.frmData.lstCriteria.selectedIndex = 0;
					document.frmData.btnDelete.disabled = false;
				}		
			}		

			function formatCriteria()
			{
				var sEditCriteria = window.document.frmData.hdSelCriteriaItem.value;
				var lOperator,lData,lConnector,sFieldName,sCriteria,tempArray, valExclude; 
				tempArray = sEditCriteria.split("~~~~~");
				
				 
				//lOperator = tempArray[1].split(",");
				lData = tempArray[2].split("^^^^^");
				lConnector = tempArray[3].split(",");
				sFieldName = tempArray[4];

				arrAttribute = tempArray[7].split('|');
				
				if (arrAttribute[3] != 'range')
					{
						document.frmData.chkExclude.checked = true;
						valExclude = " NOT BETWEEN ";
					}
				else
					valExclude = " BETWEEN ";
				
				var dataArray, connectorArray;
				for(iCount=0;iCount<lData.length;iCount++)
				{
					connectorArray = lConnector[iCount].split('|')
					dataArray =	lData[iCount].split('!!!!!');
					if(connectorArray[0] != "none")
						sCriteria = connectorArray[0].toUpperCase() + " " +"("+sFieldName+valExclude+"'"+dataArray[0]+"' AND '"+ dataArray[1] +"')";
					else
						sCriteria = "("+sFieldName+valExclude+"'"+dataArray[0]+"' AND '"+ dataArray[1] +"')";
							
					add_item(sCriteria);
				}
	
			}//End formatCriteria		

		</script>
	</head>

	<body onLoad="window_onLoad();">
	<form name="frmData" topmargin="0" leftmargin="0" onSubmit1="return false;">
		<table class="formsubtitle" border="0" width="100%" align="center">
			<tr>
				<td class="ctrlgroup">Criteria :</td>
			</tr>
		</table>					

		<table width="100%" border="0">
			<tr><td colspan="4"><b><%=Response.Write(Request.QueryString("FIELDNAME"))%></b></td></tr>
			<tr>
				<td width="33%"></td>
				<td width="5%" align="right">From :</td>
				<td width="28%"><input size="25"  name="txtFrom" onKeyDown="EnableChange()"></td>
				<td width="33%"><input type="checkbox" value="" name="chkSubquery" onChange="Subquery()">Subquery Criteria</td>
			</tr>
			<tr>
				<td><input type="checkbox" value="" name="chkExclude" onChange="Subquery()">Exclude Range</td>
				<td align="right">To :
				<td><input size="25" name="txtTo" onKeyDown="EnableChange()"></td></td>
				<td>
				&nbsp;<input class="button" type="button" style1="WIDTH: 25%" name="btnChange" value="Change" disabled onClick="change_click();">
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><input class="button" type="button" style1="WIDTH: 100%" name="btnOr" value="    OR    " onClick="or_click()"></td>
				<td>&nbsp;</td>
				<td>
					&nbsp;<input class="button" type="button" style1="WIDTH: 25%" name="btnDelete" value=" Delete " onClick ="delete_click()" disabled>
				</td>
			</tr>
			<tr>
				<td colspan="4">
				<select name="lstCriteria" style="WIDTH:100%" size="15" onChange="ButtonEnable()" onClick="ButtonEnable();">
				</select>
				</td>
			</tr>
			<tr><!-- Rajeev -- 03/25/2004 -- Added feature of RMNet labelling-->
				<td>
				<!-- Start Naresh MITS 7836 09/11/2006-->
					<input type="checkbox" name="chkHide"><!--Hide in RMNet-->
				<!-- End Naresh MITS 7836 09/11/2006-->
				</td>
				<!-- Start Naresh MITS 7836 09/11/2006-->
				<td colspan="3" align="right"><!--RMNet label:-->
				<!-- End Naresh MITS 7836 09/11/2006-->
					<input type="text" name="txtRMNetLabel" value="" onChange1="window.close()">
				</td>
			</tr>
		</table>
		<hr>
		<table border="0" width="100%" align="center" cellspacing="0" cellpadding="0">
			<tr>
				<td align="middle">
					<input class="button" type="button" style1="WIDTH: 30%" name="ok" value="    OK    " onClick="ok_click()">
					<input class="button" type="button" style1="WIDTH: 30%" name="cancel" value="Cancel" onClick="window.close()">
					<input class="button" type="button" style1="WIDTH: 30%" name="help" value="  Help  ">
				</td>
			</tr>
		</table>
		<input type="hidden" name="hdFieldName" value="<%=Request.QueryString("FIELDNAME")%>">
		<input type="hidden" name="hdTableId" value="<%=Request.QueryString("TABLEID")%>">
		<input type="hidden" name="hdFieldId" value="<%=Request.QueryString("FIELDID")%>">
		<input type="hidden" name="hdFieldType" value="<%=Request.QueryString("FIELDTYPE")%>">
		<input type="hidden" name="hdDBTableId" value="<%=Request.QueryString("DBTABLEID")%>">
		<input type="hidden" name="hdSelCriteria" value="<%=fixDoubleQuote(Request.QueryString("CRITERIA"))%>">
		<input type="hidden" name="hdSelCriteriaIndex" value="<%=Request.QueryString("CRITERIAINDEX")%>">
		<input type="hidden" name="hdSelSubQuery" value="<%=Request.QueryString("SUBQUERY")%>">
		<input type="hidden" name="hdSelHide" value="<%=Request.QueryString("HIDEINRMNET")%>">
		<input type="hidden" name="hdSelRMNetLabel" value="<%=fixDoubleQuote(Request.QueryString("RMNETLABEL"))%>">
		<input type="hidden" name="hdSelCriteriaItem" value="<%=fixDoubleQuote(Request.QueryString("PCRITERIA"))%>">
	</form>
	</body>
</html>