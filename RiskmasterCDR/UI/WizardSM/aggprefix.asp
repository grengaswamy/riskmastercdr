<html>
	<head>
		<link rel="stylesheet" href="RMNet.css" type="text/css" />
		<title>Prefix For Aggregate Rows</title>
		<script>
			var arrAggregateLabels = new Array;
			var gAggPrefix = '';
			var arrAggPrefix = new Array;
			var latest = 0;
			var backTrack = 0;
			function window_onload()
			{
				gAggPrefix = window.document.frmData.hdAggLabel.value;
				arrAggPrefix = window.document.frmData.hdAggLabel.value.split("^^^^^");
				for(i=0;i<arrAggPrefix.length;i++)
				{
					var temp = arrAggPrefix[i].split("!!!!!");
					arrAggregateLabels[i] = temp[1];
				}
				
				if (window.document.frmData.hdAggPrefix.value != '')
				{
					var arrProperties = window.document.frmData.hdAggPrefix.value.split("^^^^^");
					for(iCount=0;iCount<arrProperties.length;iCount++)
					{
						add_item(arrProperties[iCount]);
					}
				}
				
				document.frmData.lstProperties.selectedIndex = 0;
				
				if(document.frmData.lstProperties.options[0].text == "Unique Count")
					document.frmData.txtAggPrefix.value = arrAggregateLabels[0];
				else if(document.frmData.lstProperties.options[0].text == "Count")
					document.frmData.txtAggPrefix.value = arrAggregateLabels[1];
				else if(document.frmData.lstProperties.options[0].text == "Sum")
					document.frmData.txtAggPrefix.value = arrAggregateLabels[2];
				else if(document.frmData.lstProperties.options[0].text == "Average")
					document.frmData.txtAggPrefix.value = arrAggregateLabels[3];
				else if(document.frmData.lstProperties.options[0].text == "Minimum")
					document.frmData.txtAggPrefix.value = arrAggregateLabels[4];
				else if(document.frmData.lstProperties.options[0].text == "Maximum")
					document.frmData.txtAggPrefix.value = arrAggregateLabels[5];
				
				changeAggregateLabel();	
			}
		
			function changeAggregateLabel()
			{
				latest = document.frmData.lstProperties.selectedIndex;
				
				//-- Check the value at backTrack
				if(document.frmData.lstProperties.options[backTrack].text == "Unique Count")
				{
					arrAggregateLabels[0] = document.frmData.txtAggPrefix.value;
					index = 0;
				}
				else if(document.frmData.lstProperties.options[backTrack].text == "Count")
				{
					arrAggregateLabels[1] = document.frmData.txtAggPrefix.value;
					index = 1;
				}
				else if(document.frmData.lstProperties.options[backTrack].text == "Sum")
				{
					arrAggregateLabels[2] = document.frmData.txtAggPrefix.value;
					index = 2;
				}
				else if(document.frmData.lstProperties.options[backTrack].text == "Average")
				{
					arrAggregateLabels[3] = document.frmData.txtAggPrefix.value;
					index = 3;
				}
				else if(document.frmData.lstProperties.options[backTrack].text == "Minimum")
				{
					arrAggregateLabels[4] = document.frmData.txtAggPrefix.value;
					index = 4;
				}
				else if(document.frmData.lstProperties.options[backTrack].text == "Maximum")
				{
					arrAggregateLabels[5] = document.frmData.txtAggPrefix.value;
					index = 5;
				}
				
				changeAggregateLabelValue(index);
				if(document.frmData.lstProperties.options[document.frmData.lstProperties.selectedIndex].text == "Unique Count")
					document.frmData.txtAggPrefix.value = arrAggregateLabels[0];
				else if(document.frmData.lstProperties.options[document.frmData.lstProperties.selectedIndex].text == "Count")
					document.frmData.txtAggPrefix.value = arrAggregateLabels[1];
				else if(document.frmData.lstProperties.options[document.frmData.lstProperties.selectedIndex].text == "Sum")
					document.frmData.txtAggPrefix.value = arrAggregateLabels[2];
				else if(document.frmData.lstProperties.options[document.frmData.lstProperties.selectedIndex].text == "Average")
					document.frmData.txtAggPrefix.value = arrAggregateLabels[3];
				else if(document.frmData.lstProperties.options[document.frmData.lstProperties.selectedIndex].text == "Minimum")
					document.frmData.txtAggPrefix.value = arrAggregateLabels[4];
				else if(document.frmData.lstProperties.options[document.frmData.lstProperties.selectedIndex].text == "Maximum")
					document.frmData.txtAggPrefix.value = arrAggregateLabels[5];
				
				backTrack = latest;
			}
		
			function add_item(txt)
			{
				var sNewItem;
				sNewItem = new Option(txt);
				if (navigator.appName == "Netscape")
					window.document.frmData.lstProperties.add(sNewItem, null);
				else
					window.document.frmData.lstProperties.add(sNewItem);
			}
		
			function change_click()
			{
				var tempStr = '';
				var index = '';
				
				if(document.frmData.lstProperties.options[document.frmData.lstProperties.selectedIndex].text == "Unique Count")
				{
					arrAggregateLabels[0] = document.frmData.txtAggPrefix.value;
					index = 0;
				}
				else if(document.frmData.lstProperties.options[document.frmData.lstProperties.selectedIndex].text == "Count")
				{
					arrAggregateLabels[1] = document.frmData.txtAggPrefix.value;
					index = 1;
				}
				else if(document.frmData.lstProperties.options[document.frmData.lstProperties.selectedIndex].text == "Sum")
				{
					arrAggregateLabels[2] = document.frmData.txtAggPrefix.value;
					index = 2;
				}
				else if(document.frmData.lstProperties.options[document.frmData.lstProperties.selectedIndex].text == "Average")
				{
					arrAggregateLabels[3] = document.frmData.txtAggPrefix.value;
					index = 3;
				}
				else if(document.frmData.lstProperties.options[document.frmData.lstProperties.selectedIndex].text == "Minimum")
				{
					arrAggregateLabels[4] = document.frmData.txtAggPrefix.value;
					index = 4;
				}
				else if(document.frmData.lstProperties.options[document.frmData.lstProperties.selectedIndex].text == "Maximum")
				{
					arrAggregateLabels[5] = document.frmData.txtAggPrefix.value;
					index = 5;
				}
				
				changeAggregateLabelValue(index);
			}
		
			function changeAggregateLabelValue(pIndex)
			{
				var tempArray = gAggPrefix.split('^^^^^');
				var iCount;
				var tempStr = '';
								
				for(iCount=0;iCount<tempArray.length;iCount++)
				{				
					if(iCount == pIndex)
					{
						if(tempStr == '')
							tempStr =  (iCount + "!!!!!" + arrAggregateLabels[pIndex])
						else
							tempStr =  tempStr + '^^^^^' + (iCount + "!!!!!" + arrAggregateLabels[pIndex]);
					}
					else
					{
						if(tempStr == '')
							tempStr =  tempArray[iCount];
						else
							tempStr =  tempStr + '^^^^^' + tempArray[iCount];
					}
					gAggPrefix = tempStr;
				}
			}
		
			function done_click()
			{
				
				if(document.frmData.lstProperties.options[document.frmData.lstProperties.selectedIndex].text == "Unique Count")
					temp = arrAggregateLabels[0];
				else if(document.frmData.lstProperties.options[document.frmData.lstProperties.selectedIndex].text == "Count")
					temp = arrAggregateLabels[1];
				else if(document.frmData.lstProperties.options[document.frmData.lstProperties.selectedIndex].text == "Sum")
					temp = arrAggregateLabels[2];
				else if(document.frmData.lstProperties.options[document.frmData.lstProperties.selectedIndex].text == "Average")
					temp = arrAggregateLabels[3];
				else if(document.frmData.lstProperties.options[document.frmData.lstProperties.selectedIndex].text == "Minimum")
					temp = arrAggregateLabels[4];
				else if(document.frmData.lstProperties.options[document.frmData.lstProperties.selectedIndex].text == "Maximum")
					temp = arrAggregateLabels[5];
				
				if(temp == document.frmData.txtAggPrefix.value)
					{
						window.opener.updateAggPrefix(gAggPrefix);
						window.close();
					}
				else
				{
					var bCheck = window.confirm('Do you want to save changes?',"");
					if(bCheck == true)
					{
						change_click();
						window.opener.updateAggPrefix(gAggPrefix);
						window.close();
					}
					else
					{
						window.opener.updateAggPrefix(gAggPrefix);
						window.close();
					}
				}
			}
		
			function updateSerialNum()
			{
				window.opener.checkSerialNumForColumnProperties(0);
			} 
		
		</script>
	</head>
  
	<body class="8pt" onLoad="window_onload()">
		<form name="frmData">
			<table class="formsubtitle" border="0" width="100%">
				<tr>
					<td class="ctrlgroup">Prefix for aggregate rows</td>
				</tr>
			</table>
			<br>
		
			<table border="0" width="100%" align="center">
				<tr>
					<td rowspan="3" width="40%">
						<select name="lstProperties" style="WIDTH:100%" size="6" onChange="changeAggregateLabel();updateSerialNum();" >
						</select>
					</td>
					<td valign="top"><input name="txtAggPrefix" type="text" size="30" style="WIDTH:100%"  onChange="updateSerialNum();"></td>
				</tr>
				
				<tr>
					<td align="center"><input type="button" class="button" style="width:40%" name="btnChange" value="Change" onClick="change_click();"></td>
				</tr>
				
				<tr>
					<td align="center"><input type="button" class="button" style="width:40%" name="btnDone" value="Done" onClick="done_click();"></td>
				</tr>
			</table>
		
			<input type="hidden" name="hdAggPrefix" value="<%=Request.QueryString("a")%>">
			<input type="hidden" name="hdAggLabel" value="<%=Request.QueryString("b")%>">
		
		</form>
	</body>
</html>