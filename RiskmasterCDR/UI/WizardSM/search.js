// Author: Denis Basaric, 11/01/1999
// Last Modified by: Denis Basaric, 01/16/2002
var m_codeWindow=null;
var m_sFieldName="";
var m_Wnd=null;

var NS4 = (document.layers);
var IE4 = (document.all);
var m_LookupTextChanged=false;
var m_FormSubmitted=false;
var m_DataChanged=false;
var m_LookupBusy = false;
var m_LookupCancelFieldList="";
function OnSubmitForm()
{
	if(m_FormSubmitted)
	{
		alert("You already submitted this form. Please wait for server to respond.");
		return false;
	}
	
	m_FormSubmitted=true;
	return true;
}

function pageLoaded()
{
	var obj=eval("document.frmSearch.setasdefault");
	if(obj!=null)
	{
		obj.checked=(document.frmSearch.defaultviewid.value==document.frmSearch.viewid.value && document.frmSearch.defaultviewid.value!=0 && document.frmSearch.defaultviewid.value!="")
	}
	
	 var i;
	//Set focus to first field 
	 for (i=0;i<document.frmSearch.length;i++)			
		{	 				
	 		   if (document.frmSearch[i].type=="text")
			{
				document.frmSearch[i].focus();
				break;
			}
		}
	document.dateSelected=dateSelected;
	document.codeSelected=codeSelected;
	document.onCodeClose=onCodeClose;
	document.entitySelected=entitySelected;
	//document.RequestCancel=RequestCancel;
	self.onfocus=onWindowFocus;
	
	return true;
}

// Hides code popup window
function onWindowFocus()
{
	if(m_codeWindow!=null)
		m_codeWindow.close();
	m_codeWindow=null;
	
	return true;
}

function onCodeClose()
{
	m_codeWindow=null;
	
	return true;
}
function getOrgLevel(enttable)
{
	var orglevel=8;
	switch (enttable)
	{
	case 1005:
		orglevel=1;
		break;
	case 1006:
		orglevel=2;
		break;
	case 1007:
		orglevel=3;
		break;
	case 1008:
		orglevel=4;
		break;
	case 1009:
		orglevel=5;
		break;
	case 1010:
		orglevel=6;
		break;
	case 1011:
		orglevel=7;
		break;
	case 1012:
		orglevel=8;
		break;
	}
	return orglevel;
}
function selectCode(sCodeTable,sFieldName,ent_table,tableid)
{
	if(m_codeWindow!=null)
		m_codeWindow.close();
		
	m_sFieldName=sFieldName;
	
	if(m_LookupTextChanged)
	{
		var objFormElem=eval('document.frmSearch.'+sFieldName);
		var sFind=objFormElem.value;
		
		objFormElem.value="";
		
		var objFormElem=eval('document.frmSearch.'+sFieldName+"code");
		objFormElem.value="0";
		
		//Track the Fields to restore in case of cancel.
		m_LookupCancelFieldList=sFieldName + "|"+sFieldName+"code";
		//alert('quicklookup1.asp?type=code.'+sCodeTable+"&find="+sFind);
		m_codeWindow=window.open('quicklookup1.asp?type=code.'+sCodeTable+"&find="+sFind,'codeWnd',
			'width=500,height=290'+',top='+(screen.availHeight-290)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
		m_LookupTextChanged=false;
	}
	else
	{//changed by ravi on 20th march 04
		if(sCodeTable=='orgh')
			{
			if(parseInt(ent_table)>=1005 && parseInt(ent_table)<=1012 || parseInt(ent_table)==0)
				{
					var orglevel;
					var objFormElem=eval('document.frmSearch.'+sFieldName+'code');
					var sFind=objFormElem.value;
					
					orglevel=getOrgLevel(parseInt(ent_table));
				
					//orgfor=orgfor.value;
					//alert(orglevel);
					//if(m_codeWindow!=null)
					//	m_codeWindow.close();
					if(sFind=='0')
					sFind='';
					m_codeWindow=window.open('org.asp?lob='+orglevel+'&searchOrgId='+sFind,'Table','width=500,height=290'+',top='+(screen.availHeight-290)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
				}
				else
				{
					lookupData(sFieldName,tableid,4,sFieldName,3);
				}
			}	
			else
			{
			//--------end-----------
				m_codeWindow=window.open('getcode.asp?code='+sCodeTable,'codeWnd',
					'width=500,height=300'+',top='+(screen.availHeight-300)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
			}
		}
	return false;
}

function codeSelected(sCodeText,lCodeId)
{
	var objCtrl=null;
	if(m_codeWindow!=null)
		m_codeWindow.close();
	objCtrl=eval('document.frmSearch.'+m_sFieldName);
	if(objCtrl!=null)
	{
		if(objCtrl.type=="textarea")
		{
			objCtrl.value=objCtrl.value + sCodeText + "\n";
		}
		else if(objCtrl.type=="select-one" || objCtrl.type=="select-multiple")
		{
			var bAdd=true;
			for(var i=0;i<objCtrl.length;i++)
			{
				if(objCtrl.options[i].value==lCodeId)
					bAdd=false;
			}
			if(bAdd)
			{
				var objOption = new Option(sCodeText, lCodeId, false, false);
				objCtrl.options[objCtrl.length] = objOption;
				objCtrl=null;
				objCtrl=eval("document.frmSearch." + m_sFieldName+"_lst");
				if(objCtrl!=null)
				{
					if(objCtrl.value!="" && objCtrl.value.substring(objCtrl.value.length-1,1)!=",")
						objCtrl.value=objCtrl.value+",";
					objCtrl.value=objCtrl.value+lCodeId;
				}
			}
		}
		else
		{
			objCtrl.value=sCodeText;
			objCtrl.codeText=sCodeText;
			objCtrl=eval('document.frmSearch.'+m_sFieldName+"code");
			objCtrl.value=lCodeId;			
		}

	}
	
	m_sFieldName="";
	m_codeWindow=null;
	return true;
}

function lookupData(sFieldName, sTableId, sViewId, sFieldMark, lookupType)
{
	if(m_codeWindow!=null)
		m_codeWindow.close();
	
	m_sFieldName=sFieldMark;
	m_LookupType=lookupType;
	
	self.lookupCallback="entitySelected";
	
	m_codeWindow=window.open('searchpopup.asp?viewid='+sViewId+'&tableid='+sTableId,'searchWnd',
	'width=550,height=450'+',top='+(screen.availHeight-450)/2+',left='+(screen.availWidth-550)/2+',resizable=yes,scrollbars=yes');
	
		
	return false;
}

function entitySelected(sEntityId)
{
	
	if(m_codeWindow!=null)
		m_codeWindow.close();
	if(sEntityId!=""){
		if(sEntityId!="-1")	{
		
		self.setTimeout("m_Wnd=window.open('getentitydata.asp?entityid="+sEntityId+
			"','progressWnd','width=400,height=150,top="+((screen.availHeight-150)/2)+
			",left="+((screen.availWidth-400)/2)+"');",100);
		document.entityLoaded=entitySelected;
		}	
		else{
			var objCtrl;
			objCtrl=eval("document.frmSearch." + m_sFieldName);
			
			if(objCtrl.type=="select-one" || objCtrl.type=="select-multiple")
				{
				
				var sEntityName, sEntityId;
				var sEntityName, sEntityId;
				obj=eval("m_Wnd.document.frmData.lastfirstname");
				if(obj!=null)
					sEntityName=obj.value;
				obj=null;				
				obj=eval("m_Wnd.document.frmData.entityid");
				if(obj!=null)
					sEntityId=obj.value;
						
				obj2=eval("document.frmSearch." + m_sFieldName);
				if(obj2!=null){
					var bAdd=true;
					
					for(var i=0;i<obj2.length;i++){
						if(obj2.options[i].value==sEntityId)
						bAdd=false;
					}
					
					if(bAdd){
						var objOption = new Option(sEntityName, sEntityId, false, false);
						obj2.options[obj2.length] = objOption;
						obj2=null;
						obj2=eval("document.frmSearch." + m_sFieldName+"_lst");
						
						if(obj2!=null){
						if(obj2.value!="" && obj2.value.substring(obj2.value.length-1,1)!=",")
							obj2.value=obj2.value+",";
						obj2.value=obj2.value+sEntityId;
						}
						}
					}
				}
			else
				{
					//alert(eval("m_Wnd.document.frmData.entityid").value);
					//alert(objCtrl.name);
					objCtrl.value=eval("m_Wnd.document.frmData.lastfirstname").value;
					objCtrl=eval("document.frmSearch." + m_sFieldName + "code");
					objCtrl.value=eval("m_Wnd.document.frmData.entityid").value;
				}		
			if(m_Wnd!=null)
			m_Wnd.close();
			m_Wnd=null;
			m_sFieldName="";
			m_codeWindow=null;
			return true;
		}	
	}

}

function deleteSelCode(sFieldName)
{
	var objCtrl=null;
	if(m_codeWindow!=null)
		m_codeWindow.close();
	objCtrl=eval('document.frmSearch.'+sFieldName);
	if(objCtrl==null)
		return false;
	if(objCtrl.selectedIndex<0)
		return false;
	
	var bRepeat=true;
	while(bRepeat)
	{
		bRepeat=false;
		for(var i=0;i<objCtrl.length;i++)
		{
			// remove selected elements
			if(objCtrl.options[i].selected)
			{
				objCtrl.options[i]=null;
				bRepeat=true;
				break;
			}
		}
	}
	// Now create ids list
	var sId="";
	for(var i=0;i<objCtrl.length;i++)
	{
			if(sId!="")
				sId=sId+",";
			sId=sId+objCtrl.options[i].value;
	}
	objCtrl=null;
	objCtrl=eval("document.frmSearch." + sFieldName+"_lst");
	if(objCtrl!=null)
		objCtrl.value=sId;

	return true;
}

function codeLostFocus(sCtrlName)
{
	var objFormElem=eval('document.frmSearch.'+sCtrlName);
	if(objFormElem.value=="")
	{
		objFormElem.codeText="";
		objFormElem.codeId=0;
	}
	else
	{
		if(m_LookupTextChanged)
		{
			
			var objFormElemButton=eval('document.frmSearch.'+sCtrlName+"btn");
			
			objFormElemButton.click()
		}
		else
		{
			if(haveProperty(objFormElem,"codeText"))
				objFormElem.value=objFormElem.codeText;
			else
				objFormElem.value="";
		}
	}
	
	return true;
}

function haveProperty(obj, sPropName)
{
	for(p in obj)
	{
		if(p==sPropName)
			return true;
	}
	return false;
}

function dateLostFocus(sCtrlName)
{
	var sDateSeparator;
	var iDayPos=0, iMonthPos=0;
	var d=new Date(1999,11,22);
	var s=d.toLocaleString();
	var sRet="";
	var objFormElem=eval('document.frmSearch.'+sCtrlName);
	var sDate=new String(objFormElem.value);
	var iMonth=0, iDay=0, iYear=0;
	var monthDays = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	if(sDate=="")
		return "";
	iDayPos=s.indexOf("22");
	iMonthPos=s.indexOf("11");
	//if(IE4)
	//	sDateSeparator=s.charAt(iDayPos+2);
	//else
		sDateSeparator="/";
	var sArr=sDate.split(sDateSeparator);
	if(sArr.length==3)
	{
		sArr[0]=new String(parseInt(sArr[0],10));
		sArr[1]=new String(parseInt(sArr[1],10));
		sArr[2]=new String(parseInt(sArr[2],10));
		// Classic leap year calculation
		if (((parseInt(sArr[2],10) % 4 == 0) && (parseInt(sArr[2],10) % 100 != 0)) || (parseInt(sArr[2],10) % 400 == 0))
			monthDays[1] = 29;
		if(iDayPos<iMonthPos)
		{
			// Date should be as dd/mm/yyyy
			if(parseInt(sArr[1],10)<1 || parseInt(sArr[1],10)>12 ||  parseInt(sArr[0],10)<0 || parseInt(sArr[0],10)>monthDays[parseInt(sArr[1],10)-1])
				objFormElem.value="";
		}
		else
		{
			// Date is something like mm/dd/yyyy
			if(parseInt(sArr[0],10)<1 || parseInt(sArr[0],10)>12 ||  parseInt(sArr[1],10)<0 || parseInt(sArr[1],10)>monthDays[parseInt(sArr[0],10)-1])
				objFormElem.value="";
		}
		// Check the year
		if(parseInt(sArr[2],10)<10 || (sArr[2].length!=4 && sArr[2].length!=2))
			objFormElem.value="";
		// If date has been accepted
		if(objFormElem.value!="")
		{
			// Format the date
			if(sArr[0].length==1)
				sArr[0]="0" + sArr[0];
			if(sArr[1].length==1)
				sArr[1]="0" + sArr[1];
			if(sArr[2].length==2)
				sArr[2]="19"+sArr[2];
			if(iDayPos<iMonthPos)
				objFormElem.value=formatDate(sArr[2] + sArr[1] + sArr[0]);
			else
				objFormElem.value=formatDate(sArr[2] + sArr[0] + sArr[1]);
		}
	}
	else
		objFormElem.value="";
	return true;
}

function timeLostFocus(sCtrlName)
{
	var objFormElem=eval('document.frmSearch.'+sCtrlName);
	var sTime=new String(objFormElem.value);
	if(sTime=="")
		return true;
	var sArr=sTime.split(":");
	if(sArr.length!=2)
	{
		objFormElem.value="";
		return true;
	}
	sArr[0]=new String(parseInt(sArr[0],10));
	sArr[1]=new String(parseInt(sArr[1],10));
	if(parseInt(sArr[0],10)<0 || parseInt(sArr[0],10)>23 || parseInt(sArr[1],10)<0 || parseInt(sArr[1],10)>59)
	{
		objFormElem.value="";
		return true;
	}
	if(sArr[0].length==1)
		sArr[0]="0" + sArr[0];
	if(sArr[1].length==1)
		sArr[1]="0" + sArr[1];
	objFormElem.value=formatTime(sArr[0]+sArr[1]);		
	
	return true;
}

function formatDate(sParamDate)
{
	var sDateSeparator;
	var iDayPos=0, iMonthPos=0;
	var d=new Date(1999,11,22);
	var s=d.toLocaleString();
	var sRet="";
	var sDate=new String(sParamDate);
	if(sDate=="")
		return "";
	iDayPos=s.indexOf("22");
	iMonthPos=s.indexOf("11");
	//if(IE4)
	//	sDateSeparator=s.charAt(iDayPos+2);
	//else
		sDateSeparator="/";
	if(iDayPos<iMonthPos)
		sRet=sDate.substr(6,2) + sDateSeparator + sDate.substr(4,2) + sDateSeparator + sDate.substr(0,4);
	else
		sRet=sDate.substr(4,2) + sDateSeparator + sDate.substr(6,2) + sDateSeparator + sDate.substr(0,4);
	return sRet;
}

function formatTime(sParamTime)
{
	if(sParamTime=="")
		return "";
	var sTime=new String(sParamTime);
	return sTime.substr(0,2) + ":" + sTime.substr(2,2);
}

function handleUnload()
{
	if(window.opener!=null)
		window.opener.document.onCodeClose();
	
	return true;
}

function OnViewChange(sPage)
{
	if(document.frmSearch.cboViews.selectedIndex > 0)
	{
		var sTableId='';
		var sType='';
		if (sPage=='searchpopup.asp')  //tkr 1/2003 hack to allow choice of custom searches in popup
			sTableId='&tableid='+document.frmSearch.tablerestrict.value;
		if(document.frmSearch.catid)
			sType="&type="+document.frmSearch.catid.value
		var sUrl=sPage+"?viewid="+document.frmSearch.cboViews.options[document.frmSearch.cboViews.selectedIndex].value+sTableId+sType;
		window.location.href=sUrl;
	}
}

function replace(sSource, sSearchFor, sReplaceWith)
{
	var arr = new Array();
	arr=sSource.split(sSearchFor);
	return arr.join(sReplaceWith);
}

function SetDefaultView()
{
	var iViewId=document.frmSearch.viewid.value;
	if (iViewId=="")
		return false;
	var iType=document.frmSearch.catid.value;
	var bChecked = (document.frmSearch.setasdefault.checked==true);
		
	document.location="search.asp?setdefault="+bChecked+"&viewid="+iViewId+"&type="+iType;
	return false;
}
function SetDefaultViewSearchGen()
{
	var sys_ex='';
	var iViewId=document.frmSearch.viewid.value;
	if (iViewId=="")
		return false;
	var iType=document.frmSearch.catid.value;
	var bChecked = (document.frmSearch.setasdefault.checked==true);
	if(eval("document.frmSearch.sys_ex"))
		sys_ex="&formname="+document.frmSearch.sys_ex.value;
		
	document.location="searchgen.asp?setdefault="+bChecked+"&viewid="+iViewId+"&type="+iType+sys_ex;
	return false;
}
function selectDate(sFieldName)
{
	if(m_codeWindow!=null)
		m_codeWindow.close();
		
	m_sFieldName=sFieldName;
	
	m_codeWindow=window.open('calendar.html','codeWnd',
		'width=230,height=230'+',top='+(screen.availHeight-230)/2+',left='+(screen.availWidth-230)/2+',resizable=yes,scrollbars=no');
	return false;
}
function dateSelected(sDay, sMonth, sYear)
{
	var objCtrl=null;
	if(m_codeWindow!=null)
		m_codeWindow.close();
	objCtrl=eval('document.frmSearch.'+m_sFieldName);
	if(objCtrl!=null)
	{
		sDay=new String(sDay);
		if(sDay.length==1)
			sDay="0"+sDay;
		sMonth=new String(sMonth);
		if(sMonth.length==1)
			sMonth="0"+sMonth;
		sYear=new String(sYear);
			
		objCtrl.value=formatDate(sYear+sMonth+sDay);
	}
	m_sFieldName="";
	m_codeWindow=null;
	return true;
}
function ssnLostFocus(objCtrl)
{
	// ###-##-####
	/* 
	if(objCtrl.value.length==0)
			return false;
	var sValue=new String(objCtrl.value);
	sValue=stripNonDigits(sValue);
	if(sValue.length!=9)
	{
		alert("Please enter valid SSN number.");
		objCtrl.value="";
		objCtrl.focus();
		return false;
	}
	objCtrl.value=sValue.substr(0,3)+"-"+sValue.substr(3,2)+"-"+sValue.substr(5,4);
	return true;
	*/
	if(objCtrl.value.length==0)
			return false;
	var sValue=new String(objCtrl.value);
	//tkr 4/2003 do not enforce format if user using wildcard
	if(sValue.indexOf('*')!=-1)
		return true;
	var sCheck=new String();
	//check is 9 digits
	sCheck=stripNonDigits(sValue);
	if(sCheck.length!=9)
	{
		alert("Please enter valid SSN (###-##-####) or TaxID (##-#######).");
		objCtrl.value="";
		objCtrl.focus();
		return false;
	}
	//user entered SSN with dashes
	if(sValue.length==11)
	{
		if(sValue.charAt(3)!="-" || sValue.charAt(6)!="-")
		{
			alert("Please enter valid SSN (###-##-####) or TaxID (##-#######).");
			objCtrl.value="";
			objCtrl.focus();
			return false;
		}
		return true;	
	}		
	
	//user entered a tax id with dashes
	if(sValue.length==10)
	{
		if(sValue.charAt(2)!="-")
		{
			alert("Please enter valid SSN (###-##-####) or TaxID (##-#######).");
			objCtrl.value="";
			objCtrl.focus();
			return false;
		}
		return true;
	}
	//fall through:  if user did not add dashes, default to SSN ###-##-####
	objCtrl.value=sCheck.substr(0,3)+"-"+sCheck.substr(3,2)+"-"+sCheck.substr(5,4);
	return true;
	
}
function lookupTextChanged(objField)
{
//	alert("lookupTextChanged");
	if (m_LookupBusy)
	{
		window.alert("The application is busy looking up data for a previous field.\nPlease wait for the results before modifying additional fields.");
		objField.value= objField.cancelledvalue;
		return false;
	}
	setDataChanged(true);
	m_LookupTextChanged=true;
	return true;
}
function setDataChanged(b)
{
	m_DataChanged=b;
	return b;
}

function stripNonDigits(str) {
	return str.replace(/[^0-9]/g,"")
}
function RequestCancel()
{
	var i;
	var obj;
	var arrElements;
	//alert("Cancel Requested: "+m_LookupCancelFieldList+"\nbusy?"+window.m_LookupBusy);
	if(self.document.frmSearch == null)
		return false;
		
	arrElements=String(m_LookupCancelFieldList).split("|");
	
	for(i=0;i<arrElements.length;i++)
	{
		obj=null;
		
		if (arrElements[i]!="")
		{
			obj = eval('window.document.frmSearch.'+arrElements[i]);
			if (obj!=null)
				waitFor("(!window.m_LookupBusy)",'window.DoCancel("'+obj.name+'");');
		}
	}
	return false;
}
function DoCancel(sName)
{
	var obj=eval("document.frmSearch."+sName);
	
	obj.value="";
	return true;
}
function waitFor(cond,block)
{
	if(eval(cond))
		eval(block);
	else
		self.setTimeout("waitFor('" +cond+"','"+block+"');",10);
	return true;
}