<%'Option Explicit%>
<!-- #include file ="session.inc" -->
<!-- #include file ="generic.asp" -->
<html>
	<head>
		<title>Report Criteria</title>
		<link rel="stylesheet" href="RMNet.css" type="text/css" />
		<script language="JavaScript" SRC="SMD.js"></script>

<%
Dim pageError

Dim myArrayP, z, iCounter, mArrayP
'-- Rajeev -- 03/09/2004
'''myArrayP = Split(objSessionStr("ArrayP"), ",")
myArrayP = Split(objSessionStr("ArrayP"), "*!^")

mArrayP = IsValidArray(myArrayP)

If Not mArrayP Then
	Call ShowError(pageError, "No fields are available.", False)
Else
	For i = 0 To UBound(myArrayP)
		myArrayP(i) = Right(myArrayP(i),Len(myArrayP(i))-InStr(myArrayP(i),":"))
	Next
End If



'-- Create an object of MSXML DOM Document
Dim xmlDoc 

If Not GetValidObject("Msxml2.DOMDocument", xmlDoc) Then
	Call ShowError(pageError, "XML Parser is not available.", True)
End If

xmlDoc.async = false

Dim objUtil

If Not GetValidObject("InitSMList.CUtility", objUtil) Then
	Call ShowError(pageError, "Call to CUtility class failed.", True)
End If

Dim sXML, objData, sGlobalScript

'^ Rajeev 06/05/2003 -- Retrieve report from database
'-- User opens an existing report or continues using saved report
If objSessionStr("REPORTID") <> "-1" And objSessionStr("Save") = "1" And objSessionStr("SaveFinal") = "1" Then

	If Not GetValidObject("InitSMList.CDataManager", objData) Then
		Call ShowError(pageError, "Call to CDataManager class failed.", True)
	End If

	objData.m_RMConnectStr = Application("SM_DSN")
	objData.ReportID = objSessionStr("REPORTID")
	objData.GetReport

	sXML = objData.XML
	
	xmlDoc.loadXML sXML

	Set objData = Nothing
	
	'-- Parse XML for populating criteria on GUI
	'-- Populate pCriteria array
	Call DisplayCriteria()
	Call DisplayGlobalCriteria()
	
End if

'-- User continues tabbing but has not clicked on Save yet
If objSessionStr("REPORTID") <> "-1"  And objSessionStr("Save") = "0" And objSessionStr("SaveFinal") = "0" Then
	
	If Not GetValidObject("InitSMList.CDataManager", objData) Then
		Call ShowError(pageError, "Call to CDataManager class failed.", True)
	End If
	
	objData.m_RMConnectStr = Application("SM_DSN")
	objData.TempReportID = objSessionStr("REPORTID")
	objData.GetTempReport

	sXML = objData.XML
	xmlDoc.loadXML sXML

	Set objData = Nothing

	'-- Parse XML for populating criteria on GUI
	Call DisplayCriteria()
	Call DisplayGlobalCriteria()
	
End If
'^^ Rajeev 06/05/2003 -- Retrieve report from database	

If LCase(Request.ServerVariables("REQUEST_METHOD")) = "post" Then

	Call SubmitReport()

End If
%>
		<script language="JavaScript">
			var pCriteria = new Array;
			pCriteria.length = 0;
			var pGlobal = new Array;
			pGlobal.length = 0;
			var pOLECriteria = new Array;
			pOLECriteria.length = 0;
			var pDateFormula = new Array;
			pDateFormula.length = 0;
			var tempOperators;
		
			function openCriteria(selFieldType,SelCriteria, SelDBTableID, selTableId, selFieldId)
			{
				if (SelCriteria == 0)
				{
					if(selFieldType == 3 || selFieldType == 4 ||selFieldType == 5 )
						window.open('addcriteria_2_3.asp?CRITERIAINDEX=-1&FIELDNAME='+window.document.frmData.hdSelFieldCriteria.value+'&TABLEID='+selTableId+'&FIELDID='+selFieldId+'&FIELDTYPE='+selFieldType+'&DBTABLEID='+SelDBTableID+'&CRITERIA=&SUBQUERY=0','addcr1','resizable=yes,Width=600,Height=425,top='+(screen.availHeight-425)/2+',left='+(screen.availWidth-600)/2+'')
					else if(selFieldType == 2)
						window.open('addcriteria_2_2.asp?CRITERIAINDEX=-1&FIELDNAME='+window.document.frmData.hdSelFieldCriteria.value+'&TABLEID='+selTableId+'&FIELDID='+selFieldId+'&FIELDTYPE='+selFieldType+'&DBTABLEID='+SelDBTableID+'&CRITERIA=&SUBQUERY=0','addcr1','resizable=yes,Width=700,Height=475,top='+(screen.availHeight-475)/2+',left='+(screen.availWidth-700)/2+'')
					else
						window.open('addcriteria_2_1.asp?CRITERIAINDEX=-1&FIELDNAME='+window.document.frmData.hdSelFieldCriteria.value+'&TABLEID='+selTableId+'&FIELDID='+selFieldId+'&FIELDTYPE='+selFieldType+'&DBTABLEID='+SelDBTableID+'&CRITERIA=&SUBQUERY=0','addcr1','resizable=yes,Width=600,Height=480,top='+(screen.availHeight-480)/2+',left='+(screen.availWidth-600)/2+'')
				}
				else if (SelCriteria == 1)
				{
					if(selFieldType == 2)
						window.open('addcriteria_3_2.asp?CRITERIAINDEX=-1&FIELDNAME='+window.document.frmData.hdSelFieldCriteria.value+'&TABLEID='+selTableId+'&FIELDID='+selFieldId+'&FIELDTYPE='+selFieldType+'&DBTABLEID='+SelDBTableID+'&CRITERIA=&SUBQUERY=0','addcr1','resizable=yes,Width=700,Height=475,top='+(screen.availHeight-475)/2+',left='+(screen.availWidth-700)/2+'')
					else
						window.open('addcriteria_3_1.asp?CRITERIAINDEX=-1&FIELDNAME='+window.document.frmData.hdSelFieldCriteria.value+'&TABLEID='+selTableId+'&FIELDID='+selFieldId+'&FIELDTYPE='+selFieldType+'&DBTABLEID='+SelDBTableID+'&CRITERIA=&SUBQUERY=0','addcr1','resizable=yes,Width=600,Height=480,top='+(screen.availHeight-480)/2+',left='+(screen.availWidth-600)/2+'')
				}
			}
		
			function editCriteria()
			{
				if( document.frmData.lstSelcriteria.selectedIndex == -1)
					return;
		
				var selFieldType,selWindowType, selCriteria, selDBTableID, selTableId, selFieldId, selFieldName, selSubQuery, selHide, selRMNetLabel;
				
				var selfieldIndex = document.frmData.cboSelfield.selectedIndex;
				var selCriteriaIndex = document.frmData.lstSelcriteria.selectedIndex;
				
				for(i=0;i<pCriteria.length;i++)
				{
					if(selfieldIndex == parseInt(pCriteria[i].substring(0,pCriteria[i].indexOf("~~~~~"))))
					{
						var temp = pCriteria[i+selCriteriaIndex];
						var tempArr,arrSelAttributes,windowType;
						tempArr = temp.split("~~~~~");
						selFieldType = tempArr[5];
						selDBTableID = tempArr[6];
						arrSelAttributes = tempArr[7].split("|");
						windowType = arrSelAttributes[3];
						selTableId = arrSelAttributes[1];
						selFieldId = arrSelAttributes[2];
																
						if(windowType == "data" || windowType == "codelist" ||windowType == "excludecodelist")
							selWindowType = 0;
						else
							selWindowType = 1;
						
						selCriteria = document.frmData.lstSelcriteria.options[selCriteriaIndex].text;
						selSubQuery = arrSelAttributes[5];
						selHide = arrSelAttributes[6];
						if(arrSelAttributes[7] != null)
							selRMNetLabel = arrSelAttributes[7];
						else
							selRMNetLabel = '';
							
						selFieldName = tempArr[4]; 
						break;
					}
				}
				
				
				if (selWindowType == 0)
				{
					if(selFieldType == 3 || selFieldType == 4 ||selFieldType == 5 )
						window.open('addcriteria_2_3.asp?CRITERIAINDEX='+selCriteriaIndex+'&FIELDNAME='+selFieldName+'&TABLEID='+selTableId+'&FIELDID='+selFieldId+'&FIELDTYPE='+selFieldType+'&DBTABLEID='+selDBTableID+'&CRITERIA='+ escape(selCriteria) +'&SUBQUERY='+selSubQuery+'&HIDEINRMNET='+selHide+'&RMNETLABEL='+selRMNetLabel+'&PCRITERIA='+ escape(pCriteria[i+selCriteriaIndex]),'addcr1','resizable=yes,Width=600,Height=425,top='+(screen.availHeight-425)/2+',left='+(screen.availWidth-600)/2+'')
					else if(selFieldType == 2)
						window.open('addcriteria_2_2.asp?CRITERIAINDEX='+selCriteriaIndex+'&FIELDNAME='+selFieldName+'&TABLEID='+selTableId+'&FIELDID='+selFieldId+'&FIELDTYPE='+selFieldType+'&DBTABLEID='+selDBTableID+'&CRITERIA='+ escape(selCriteria) +'&SUBQUERY='+selSubQuery+'&HIDEINRMNET='+selHide+'&RMNETLABEL='+selRMNetLabel+'&PCRITERIA='+ escape(pCriteria[i+selCriteriaIndex])+'&POLECRITERIA='+escape(pOLECriteria[i+selCriteriaIndex])+'&PDATEFORMULA='+escape(pDateFormula[i+selCriteriaIndex]) ,'addcr1','resizable=yes,Width=700,Height=475,top='+(screen.availHeight-475)/2+',left='+(screen.availWidth-700)/2+'')
					else
						window.open('addcriteria_2_1.asp?CRITERIAINDEX='+selCriteriaIndex+'&FIELDNAME='+selFieldName+'&TABLEID='+selTableId+'&FIELDID='+selFieldId+'&FIELDTYPE='+selFieldType+'&DBTABLEID='+selDBTableID+'&CRITERIA='+ escape(selCriteria) +'&SUBQUERY='+selSubQuery+'&HIDEINRMNET='+selHide+'&RMNETLABEL='+selRMNetLabel+'&PCRITERIA='+ escape(pCriteria[i+selCriteriaIndex]),'addcr1','resizable=yes,Width=600,Height=480,top='+(screen.availHeight-480)/2+',left='+(screen.availWidth-600)/2+'')
				}
				else if (selWindowType == 1)
				{
					if(selFieldType == 2)
						window.open('addcriteria_3_2.asp?CRITERIAINDEX='+selCriteriaIndex+'&FIELDNAME='+selFieldName+'&TABLEID='+selTableId+'&FIELDID='+selFieldId+'&FIELDTYPE='+selFieldType+'&DBTABLEID='+selDBTableID+'&CRITERIA='+ escape(selCriteria) +'&SUBQUERY='+selSubQuery+'&HIDEINRMNET='+selHide+'&RMNETLABEL='+selRMNetLabel+'&PCRITERIA='+ escape(pCriteria[i+selCriteriaIndex])+'&POLECRITERIA='+escape(pOLECriteria[i+selCriteriaIndex]),'addcr1','resizable=yes,Width=700,Height=475,top='+(screen.availHeight-475)/2+',left='+(screen.availWidth-700)/2+'')
					else
						window.open('addcriteria_3_1.asp?CRITERIAINDEX='+selCriteriaIndex+'&FIELDNAME='+selFieldName+'&TABLEID='+selTableId+'&FIELDID='+selFieldId+'&FIELDTYPE='+selFieldType+'&DBTABLEID='+selDBTableID+'&CRITERIA='+ escape(selCriteria) +'&SUBQUERY='+selSubQuery+'&HIDEINRMNET='+selHide+'&RMNETLABEL='+selRMNetLabel+'&PCRITERIA='+ escape(pCriteria[i+selCriteriaIndex]),'addcr1','resizable=yes,Width=600,Height=480,top='+(screen.availHeight-480)/2+',left='+(screen.availWidth-600)/2+'')
				}
			}
		
			function openGlobalCriteria()
			{
				if(document.frmData.txtGlobal.value == '')
					window.open('globalcriteria.asp?a='+pXML[window.document.frmData.cboSelfield.selectedIndex]+'&b='+window.document.frmData.cboSelfield.options[window.document.frmData.cboSelfield.selectedIndex].text,'globcri','resizable=yes,Width=325,Height=200,top='+(screen.availHeight-200)/2+',left='+(screen.availWidth-325)/2+'');
				else // Edit mode
				{
					var selfieldIndex = document.frmData.cboSelfield.selectedIndex;
						
					for(i=0;i<pGlobal.length;i++)
					{
						if(selfieldIndex == parseInt(pGlobal[i].substring(0,pGlobal[i].indexOf("~~~~~"))))
						{
							break;
						}
					}
					window.open('globalcriteria.asp?INDEX='+i+'&a='+pXML[window.document.frmData.cboSelfield.selectedIndex]+'&b='+window.document.frmData.cboSelfield.options[window.document.frmData.cboSelfield.selectedIndex].text+'&PGLOBAL='+pGlobal[i],'globcri','resizable=yes,Width=325,Height=200,top='+(screen.availHeight-200)/2+',left='+(screen.availWidth-325)/2+'');
				}
			}	
		
			function deleteGlobalCriteria(p_Index)
			{
				pGlobal[p_Index] = p_Index + "~~~~~" + '' + "~~~~~" + '' + "~~~~~" + '' + "~~~~~" + '' + "~~~~~" + '' + "`````";
			}			

			function changeFieldName(p_selCriteriaIndex)
			{
				var index,sNewItem,oOption,tempIndex,val,selIndex;
				var test = String.fromCharCode(13, 10);
				var arrOption = new Array;

				for(i=0,n=0;i<pCriteria.length;i++)
				{
					selIndex = pCriteria[i].substring(0,pCriteria[i].indexOf("~~~~~"));
					if((selIndex != '') && (selIndex == document.frmData.cboSelfield.selectedIndex))
					{
						var tempArray;
						tempArray = pCriteria[i].split("~~~~~")
						arrOption[n] = tempArray[10].substring(0,tempArray[10].indexOf("`````"));
						if(arrOption[n].substring(arrOption[n].length-2,arrOption[n].length) == test)
						{
							arrOption[n] = arrOption[n].substring(0,arrOption[n].length-2);
						}
						n = n + 1;
					}
				}
				
				if(navigator.appName == "Netscape")
				{
					sNewItem = new Option(oOption);
					window.document.frmData.lstSelcriteria.add(sNewItem, null);
				}
				else
				{
					if(window.document.frmData.lstSelcriteria.length > 0)
						window.document.frmData.lstSelcriteria.length = 0;
						for(i=0;i<arrOption.length;i++)
						{
							sNewItem = window.document.createElement("OPTION");
							sNewItem.text = arrOption[i];//"ABC";
							window.document.frmData.lstSelcriteria.add(sNewItem);
						}
				}

				var temp = document.frmData.cboSelfield.selectedIndex;
				
				if(document.frmData.cboSelfield.length > 0)
				{
					if(pXML[temp].indexOf("aggcount") > -1 || pXML[temp].indexOf("aggsum") > -1 || pXML[temp].indexOf("aggmin") > -1 || pXML[temp].indexOf("aggmax") > -1 || pXML[temp].indexOf("aggavg") > -1 )
						document.frmData.btnGlobal.disabled = false;
					else
						document.frmData.btnGlobal.disabled = true;
				}
				
				if(window.document.frmData.lstSelcriteria.length > 0 && p_selCriteriaIndex < window.document.frmData.lstSelcriteria.length)
				{
					if(p_selCriteriaIndex == -1)
						window.document.frmData.lstSelcriteria.selectedIndex = window.document.frmData.lstSelcriteria.length - 1;
					else
						window.document.frmData.lstSelcriteria.selectedIndex = p_selCriteriaIndex;
				}
				else
					window.document.frmData.lstSelcriteria.selectedIndex = window.document.frmData.lstSelcriteria.length - 1;
				
				ButtonEnable();

				if(document.frmData.lstSelcriteria.length > 0)
				{
					document.frmData.lstSelcriteria.selectedIndex = 0;
					
					var tempIndex = pCriteriaIndex();

					//-- handle Parameter/ Label
					var tempArray = pCriteria[tempIndex].split("~~~~~");
					if(tempArray[8] == 1)
					{
						document.frmData.chkParameter.checked = true;
						document.frmData.txtLabel.readOnly = false; 
						document.frmData.txtLabel.value = tempArray[9];
					}
					else
					{
						document.frmData.chkParameter.checked = false;
						document.frmData.txtLabel.readOnly = true;
						document.frmData.txtLabel.value = '';
					}				
				}			
				else
				{
					document.frmData.chkParameter.checked = false;
					document.frmData.txtLabel.readOnly = true;
					document.frmData.txtLabel.value = '';
				}
			}

			function pCriteriaIndex() //-- returns -1 if failed
			{
				//-- Maintain Index
				var selfieldIndex = document.frmData.cboSelfield.selectedIndex;
				var selfieldName = document.frmData.cboSelfield.options[selfieldIndex].text;
				var selCriteriaIndex = document.frmData.lstSelcriteria.selectedIndex;
				var iIndex = 0;
				var iFound = -1;
				for(i=0;i<pCriteria.length;i++)
				{
					if(selfieldIndex == parseInt(pCriteria[i].substring(0,pCriteria[i].indexOf("~~~~~"))))
					{
						if(iFound == -1)
						{
							iFound = i;
						}
									
						if(iIndex == selCriteriaIndex)
						{					
							break;
						}
						iIndex++;
					}
				}

				return parseInt(iFound)+parseInt(iIndex);		
			}

			function globalCriteriaChange()
			{
				document.frmData.txtGlobal.value = '';
					
				var arrOption = new Array;
				for(i=0,n=0;i<pGlobal.length;i++)
				{
					var tempArray;
					tempArray = pGlobal[i].split("~~~~~")
					selIndex = pGlobal[i].substring(0,pGlobal[i].indexOf("~~~~~"));
					if(parseInt(selIndex) == document.frmData.cboSelfield.selectedIndex)
					{
						document.frmData.txtGlobal.value = tempArray[5].substring(0,tempArray[5].indexOf("`````"));
						break;
					}
				}			
			}
		
			function criteriaArray(columnindex,p_selCriteriaIndex,operators,dataList,connectorList,fieldName,fieldType,DBTableID,pAttributes,criteria)
			{
				var arrPos, iFieldNumber;
				arrPos = FindCharacter(pXML[columnindex], '"', 2).split(',');
				iFieldNumber = parseInt(pXML[columnindex].substr(parseInt(arrPos[0]),parseInt(arrPos[1])-1-parseInt(arrPos[0])));
				
				pAttributes = iFieldNumber + '|' + pAttributes
				
				var tempIndex = pCriteriaIndex();
								
				if(p_selCriteriaIndex == -1)
				{
					bParameter = 0;
					sParameter = '';
					pCriteria[pCriteria.length] = columnindex + "~~~~~" + operators + "~~~~~" + dataList + "~~~~~" + connectorList +  "~~~~~" + fieldName + "~~~~~" + fieldType + "~~~~~" + DBTableID + "~~~~~" + pAttributes + "~~~~~" + bParameter + "~~~~~" + sParameter + "~~~~~" + criteria + "`````";
				}
				else
				{
					var tempArray = pCriteria[tempIndex].split("~~~~~");
					bParameter = tempArray[8];
					sParameter = tempArray[9];
					pCriteria[tempIndex] = columnindex + "~~~~~" + operators + "~~~~~" + dataList + "~~~~~" + connectorList +  "~~~~~" + fieldName + "~~~~~" + fieldType + "~~~~~" + DBTableID + "~~~~~" + pAttributes + "~~~~~" + bParameter + "~~~~~" + sParameter + "~~~~~" + criteria + "`````";
				}
				
				var arrTemp = new Array();
				for(i=0;i<pCriteria.length;i++)
				{
					arrTemp[i] = pCriteria[i];
				}	

				for(i=0;i<pCriteria.length;i++)
				{
					if(parseInt(pCriteria[i].substring(0,pCriteria[i].indexOf("~~~~~"))) > parseInt(pCriteria[pCriteria.length-1].substring(0,pCriteria[pCriteria.length-1].indexOf("~~~~~"))))
					{
						pCriteria[i] = pCriteria[pCriteria.length-1];
						for(j=i+1;j<arrTemp.length;j++)
						{
							pCriteria[j] = arrTemp[j-1];
						}
						break;				
					}
				}			
			}

			function OLEcriteriaArray(p_columnIndex,p_selCriteriaIndex, p_DataList, p_Attributes, p_Flag)
			{
				var arrPos, iFieldNumber;
				arrPos = FindCharacter(pXML[p_columnIndex], '"', 2).split(',');
				iFieldNumber = parseInt(pXML[p_columnIndex].substr(parseInt(arrPos[0]),parseInt(arrPos[1])-1-parseInt(arrPos[0])));
				
				p_Attributes = iFieldNumber // + '|' + pAttributes
				
				var tempIndex = pCriteriaIndex();

				if(p_selCriteriaIndex == -1)
				{
					if(p_Flag)
						pOLECriteria[pOLECriteria.length] = p_columnIndex + "~~~~~" + p_DataList + "~~~~~" + p_Attributes + "`````";
					else
						pOLECriteria[pOLECriteria.length] = "`````";
				}
				else
				{
					if(p_Flag)
						pOLECriteria[tempIndex] = p_columnIndex + "~~~~~" + p_DataList + "~~~~~" + p_Attributes + "`````";
					else
						pOLECriteria[tempIndex] = "`````";
				}
				
				var arrTemp = new Array();
				for(i=0;i<pOLECriteria.length;i++)
				{
					arrTemp[i] = pOLECriteria[i];
				}	

				for(i=0;i<pOLECriteria.length;i++)
				{
					if(parseInt(pOLECriteria[i].substring(0,pOLECriteria[i].indexOf("~~~~~"))) > parseInt(pOLECriteria[pOLECriteria.length-1].substring(0,pOLECriteria[pOLECriteria.length-1].indexOf("~~~~~"))))
					{
						pOLECriteria[i] = pOLECriteria[pOLECriteria.length-1];
						for(j=i+1;j<arrTemp.length;j++)
						{
							pOLECriteria[j] = arrTemp[j-1];
						}
						break;				
					}
				}			
			}
		
			function dateFormulaArray(p_columnIndex,p_selCriteriaIndex, p_DateFormula, p_Attributes, p_Flag)
			{
				var arrPos, iFieldNumber;
				arrPos = FindCharacter(pXML[p_columnIndex], '"', 2).split(',');
				iFieldNumber = parseInt(pXML[p_columnIndex].substr(parseInt(arrPos[0]),parseInt(arrPos[1])-1-parseInt(arrPos[0])));
				
				p_Attributes = iFieldNumber
				
				var tempIndex = pCriteriaIndex();

				if(p_selCriteriaIndex == -1)
				{
					if(p_Flag)
						pDateFormula[pDateFormula.length] = p_columnIndex + "~~~~~" + p_DateFormula + "~~~~~" + p_Attributes + "`````";
					else
						pDateFormula[pDateFormula.length] = "`````";
				}
				else
				{
					if(p_Flag)
						pDateFormula[tempIndex] = p_columnIndex + "~~~~~" + p_DateFormula + "~~~~~" + p_Attributes + "`````";
					else
						pDateFormula[tempIndex] = "`````";
				}
				
				var arrTemp = new Array();
				for(i=0;i<pDateFormula.length;i++)
				{
					arrTemp[i] = pDateFormula[i];
				}	

				for(i=0;i<pDateFormula.length;i++)
				{
					if(parseInt(pDateFormula[i].substring(0,pDateFormula[i].indexOf("~~~~~"))) > parseInt(pDateFormula[pDateFormula.length-1].substring(0,pDateFormula[pDateFormula.length-1].indexOf("~~~~~"))))
					{
						pDateFormula[i] = pDateFormula[pDateFormula.length-1];
						for(j=i+1;j<arrTemp.length;j++)
						{
							pDateFormula[j] = arrTemp[j-1];
						}
						break;				
					}
				}			
			}		

			function globalCriteriaArray(columnindex,operatorList,dataList,connector,globalCriteria)
			{
				var arrPos, iFieldNumber;
				arrPos = FindCharacter(pXML[columnindex], '"', 2).split(',');
				iFieldNumber = parseInt(pXML[columnindex].substr(parseInt(arrPos[0]),parseInt(arrPos[1])-1-parseInt(arrPos[0])));
				
				pAttributes = iFieldNumber
				
				if((pGlobal.length-1) < columnindex)
					pGlobal[pGlobal.length] = columnindex + "~~~~~" + operatorList + "~~~~~" + dataList + "~~~~~" + connector + "~~~~~" + pAttributes + "~~~~~" + globalCriteria + "`````";
				else
					pGlobal[columnindex] = columnindex + "~~~~~" + operatorList + "~~~~~" + dataList + "~~~~~" + connector + "~~~~~" + pAttributes + "~~~~~" + globalCriteria + "`````";

				var arrTemp = new Array();
				for(i=0;i<pGlobal.length;i++)
				{
					arrTemp[i] = pGlobal[i];
				}	

				for(i=0;i<pGlobal.length;i++)
				{
					if(parseInt(pGlobal[i].substring(0,pGlobal[i].indexOf("~~~~~"))) > parseInt(pGlobal[pGlobal.length-1].substring(0,pGlobal[pGlobal.length-1].indexOf("~~~~~"))))
					{
						pGlobal[i] = pGlobal[pGlobal.length-1];
						for(j=i+1;j<arrTemp.length;j++)
						{
							pGlobal[j] = arrTemp[j-1];
						}
						break;				
					}
				}			
			}
		
			function displayGlobalCriteria()	//display Global Criteria in text area
			{
				var test = String.fromCharCode(13, 10);
				if(pGlobal.length == 0)
				{
					return;
				}
				
				for(j=0;j<document.frmData.cboSelfield.length;j++)
				{
					for(i=0,n=0;i<pGlobal.length;i++)
					{
						var tempArray;

						tempArray = pGlobal[i].split("~~~~~")
						selIndex = pGlobal[i].substring(0,pGlobal[i].indexOf("~~~~~"));
						var selOperator = tempArray[1];
						
						if(selOperator != '')
						{
							if(parseInt(selIndex) == j)
							{
								if(document.frmData.txtRptcriteria.value == "")
									document.frmData.txtRptcriteria.value = "Global Criteria(e)" + test + document.frmData.cboSelfield.options[j].text + ": " + tempArray[5].substring(0,tempArray[5].indexOf("`````"));	
								else if(document.frmData.txtRptcriteria.value.indexOf("Global Criteria(e)") == -1)
									document.frmData.txtRptcriteria.value = document.frmData.txtRptcriteria.value + test + "Global Criteria(e)" + test + document.frmData.cboSelfield.options[j].text + ": " + tempArray[5].substring(0,tempArray[5].indexOf("`````"));	
								else
									document.frmData.txtRptcriteria.value = document.frmData.txtRptcriteria.value + test + document.frmData.cboSelfield.options[j].text + ": " + tempArray[5].substring(0,tempArray[5].indexOf("`````"));	
							}
						}
					}
				}			
			}
				
			function displayCriteria()		//display Normal Criteria in text area
			{
				if(pCriteria.length == 0)
				{
					document.frmData.txtRptcriteria.value = '';
					return;
				}
				var test = String.fromCharCode(13, 10);
				var check;
				var bFound;
				bFound = -1;
				var arrDisplay = new Array;
				var temp;
				temp = '';
				check = document.frmData.cboSelfield.selectedIndex;
				for(j=0;j<document.frmData.cboSelfield.length;j++)
				{
				for(i=0,n=0;i<pCriteria.length;i++)
				{
					if(j == pCriteria[i].substring(0,pCriteria[i].indexOf("~~~~~")))
					{
						var tempArray = new Array;
						tempArray = pCriteria[i].split("~~~~~");
						bFound = j;
						if(n == 0)
							arrDisplay[n] = document.frmData.cboSelfield.options[j].text + ":" + tempArray[10].substring(0,tempArray[10].indexOf("`````"));
						else
							arrDisplay[n] = tempArray[10].substring(0,tempArray[10].indexOf("`````"));

						n = n + 1;
					}
				}
				if(bFound == j)
				{
					arrDisplay.length = n;
					for(k=0;k<arrDisplay.length;k++)
					{
						if(arrDisplay[k].indexOf(test) == -1)
							temp = temp + arrDisplay[k] + test;
						else
							temp = temp + arrDisplay[k];
					}
					document.frmData.txtRptcriteria.value = temp;	
				}
				}
			}

			function ButtonEnable()
			{
				if(document.frmData.lstSelcriteria.length > 0)	
				{
					document.frmData.btnEdit.disabled = false;
					document.frmData.btnDelete.disabled = false;
					document.frmData.chkParameter.disabled = false; 
					document.frmData.btnToggle.disabled = false;
				}
				else
				{
				document.frmData.btnEdit.disabled = true;
				document.frmData.btnDelete.disabled = true;
				document.frmData.chkParameter.disabled = true; 
				document.frmData.btnToggle.disabled = true;
				}
				
				if(document.frmData.lstSelcriteria.selectedIndex == 0)		
					document.frmData.btnToggle.disabled = true;
			}

			function deleteClick()
			{
				var txt, i;
				var selfieldName,selfieldIndex,pattern;
				var arrOperators = new Array;
				var arrDisplay = new Array;
				var test = String.fromCharCode(13, 10);
				
				selfieldIndex = document.frmData.cboSelfield.selectedIndex;
				selfieldName = document.frmData.cboSelfield.options[selfieldIndex].text;
				selCriteriaIndex = document.frmData.lstSelcriteria.selectedIndex;
				
				var iIndex = 0;
				var iFound = -1;
				for(i=0;i<pCriteria.length;i++)
				{
					if(selfieldIndex == parseInt(pCriteria[i].substring(0,pCriteria[i].indexOf("~~~~~"))))
					{
						if(iFound == -1)
						{
							iFound = i;
						}
							
						if(iIndex == selCriteriaIndex)
						{					
							pCriteria[parseInt(iFound)+parseInt(iIndex)] = null;
							pOLECriteria[parseInt(iFound)+parseInt(iIndex)] = null;
							pDateFormula[parseInt(iFound)+parseInt(iIndex)] = null;
							
							//adjust_pCriteria();
							//adjust_pOLECriteria();
							adjust_pArray(pCriteria);
							adjust_pArray(pOLECriteria);
							adjust_pArray(pDateFormula);
							
							break;
						}
						iIndex++;
					}
				}

				changeFieldName(document.frmData.lstSelcriteria.selectedIndex);
				
				if(document.frmData.lstSelcriteria.length > 0) 
				{
					var tempArr,temp, tempIndex;
					tempIndex = parseInt(iFound)+parseInt(iIndex);//i-1;
						
					if(tempIndex >= pCriteria.length)
						tempIndex = pCriteria.length - 1;
						
					temp = pCriteria[tempIndex];
					tempArr = temp.split("~~~~~")
					oldAttributes = tempArr[7];
					arrAttributes = tempArr[7].split("|");
					tempArr[7] = '';
					for(j=0;j<arrAttributes.length;j++)
					{
						if (tempArr[7] == '')
							tempArr[7] =  arrAttributes[j];
						else
						{
							if(j == 4)
								tempArr[7] =  tempArr[7] + '|' + 'none';
							else
								tempArr[7] =  tempArr[7] + '|' + arrAttributes[j];
						}
					}
					
					pCriteria[tempIndex] = pCriteria[tempIndex].replace(oldAttributes,tempArr[7]);

					var tempArray = new Array;
					tempArray = pCriteria[tempIndex].split("~~~~~")
					a = tempArray[10].substring(0,tempArray[10].indexOf("`````"));
					if(selCriteriaIndex == 0)
					{
						
					if(a.substring(0,3) == "AND")
						{	
							a = a.substring(3,a.length)
							pCriteria[tempIndex] = pCriteria[tempIndex].replace(tempArray[10],a) + "`````";
						}
					else if(a.substring(0,2) == "OR")
						{	
							a = a.substring(2,a.length)
							pCriteria[tempIndex] = pCriteria[tempIndex].replace(tempArray[10],a) + "`````";
						}
					}
					
					if(document.frmData.lstSelcriteria.length == 1)		
						document.frmData.btnToggle.disabled = true;
				}
				else
				{
					document.frmData.btnEdit.disabled = true;
					document.frmData.btnDelete.disabled = true;
					document.frmData.btnToggle.disabled = true;
					document.frmData.chkParameter.disabled = true; 
				}	
		
				changeFieldName(document.frmData.lstSelcriteria.selectedIndex);
				displayCriteria();
				displayGlobalCriteria();
				
				return true;
			}
		
			function ButtonState()
			{
				if(window.document.frmData.lstSelcriteria.length > 0)
				{
					window.document.frmData.btnEdit.disabled = false;
					window.document.frmData.btnDelete.disabled = false;
					window.document.frmData.chkParameter.disabled = false;
					
					var tempIndex = pCriteriaIndex();

					tempArray = pCriteria[tempIndex].split("~~~~~");
					if(tempArray[8] == 1)
					{
						document.frmData.txtLabel.value = tempArray[9];
						window.document.frmData.chkParameter.checked = true;
						document.frmData.txtLabel.readOnly = false;
					}
					else
					{
						document.frmData.txtLabel.value = '';
						window.document.frmData.chkParameter.checked = false;
						document.frmData.txtLabel.readOnly = true;
					}
				}
				else
				{
					window.document.frmData.btnEdit.disabled = true;
					window.document.frmData.btnDelete.disabled = true;
					window.document.frmData.chkParameter.disabled = true;
				}
				
				if(window.document.frmData.lstSelcriteria.selectedIndex > 0)
				{
					window.document.frmData.btnToggle.disabled = false;
				}
				else
				{
					window.document.frmData.btnToggle.disabled = true;
				}
				
			}

			function EnableTextBox()
			{
				var tempStr = '';
				
				var tempIndex = pCriteriaIndex();
				
				tempArray = pCriteria[tempIndex].split("~~~~~");

				if(window.document.frmData.chkParameter.checked == true)
				{
					window.document.frmData.txtLabel.readOnly = false;
					if(tempArray[9] != '')
						document.frmData.txtLabel.value = tempArray[9];
					else
					{
						document.frmData.txtLabel.value = tempArray[4];
						for(i=0;i<tempArray.length;i++)
						{
							if (tempStr == '')
								tempStr =  tempArray[i];
							else
							{
								if(i == 8)
									tempStr =  tempStr + '~~~~~' + '1';
								else if(i == 9)
									tempStr =  tempStr + '~~~~~' + unescape(document.frmData.txtLabel.value);
								else
									tempStr =  tempStr + '~~~~~' + tempArray[i];
							}
						}
						pCriteria[tempIndex] = tempStr;
					}
				}
				else
				{	
					document.frmData.txtLabel.value = '';
					for(i=0;i<tempArray.length;i++)
					{
						if (tempStr == '')
							tempStr =  tempArray[i];
						else
						{
							if(i == 8)
								tempStr =  tempStr + '~~~~~' + '0';
							else if(i == 9)
								tempStr =  tempStr + '~~~~~' + '';
							else
								tempStr =  tempStr + '~~~~~' + tempArray[i];
						}
					}
					pCriteria[tempIndex] = tempStr;
					window.document.frmData.txtLabel.readOnly = true;
				}
			}
		
			function page_onLoad()
			{
				if(document.frmData.cboSelfield.length == 0)
					document.frmData.btnAdd.disabled = true;		
				
				if(pXML.length != 0)
					if(pXML[0].indexOf("aggcount") > -1 || pXML[0].indexOf("aggsum") > -1 || pXML[0].indexOf("aggmin") > -1 || pXML[0].indexOf("aggmax") > -1 || pXML[0].indexOf("aggavg") > -1 )
						document.frmData.btnGlobal.disabled = false;		
				 		
			}

			function toggle()
			{
				var tempStr;
				var listItem
				listItem = window.document.frmData.lstSelcriteria.options[window.document.frmData.lstSelcriteria.selectedIndex].text;
				var arrTemp = new Array();
				var arrAttributes;
				var arrTemp1 = new Array();
				var firstPos;
				var restString;
				arrTemp = listItem.split("(")
				tempStr = arrTemp[0]; 
				firstPos = listItem.indexOf("(")
				restString = listItem.substr(firstPos,(listItem.length-firstPos))

				if (tempStr.indexOf(" ")>-1)
				{
					arrTemp1 = tempStr.split(" ");
					tempStr = arrTemp1[0];
				}
				else
					tempStr = arrTemp[0];

				if (tempStr == "AND")
					tempStr = "OR";
				else if (tempStr == "OR")
					tempStr = "AND";

				window.document.frmData.lstSelcriteria.options[window.document.frmData.lstSelcriteria.selectedIndex].text = tempStr + restString;
		
				var tempIndex = pCriteriaIndex();
		
				var temp;
				var tempArr = new Array;
				temp = pCriteria[tempIndex]
		
				tempArr = temp.split("~~~~~")
				tempArr[10] = tempStr + restString;
				tempArr[10] = "~~~~~" + tempArr[10];
				oldAttributes = tempArr[7];
				arrAttributes = tempArr[7].split("|");
				tempArr[7] = '';
				for(i=0;i<arrAttributes.length;i++)
					if (tempArr[7] == '')
						tempArr[7] =  arrAttributes[i];
					else
					{
						if(i == 4)
							tempArr[7] =  tempArr[7] + '|' + tempStr;
						else
							tempArr[7] =  tempArr[7] + '|' + arrAttributes[i];
					}
		
				tempStr = "~~~~~" + listItem; 
				pCriteria[tempIndex] = temp.replace(tempStr,tempArr[10])
				pCriteria[tempIndex] = pCriteria[tempIndex].replace(oldAttributes,tempArr[7])
				displayCriteria();
				window.document.frmData.lstSelcriteria.selectedIndex = window.document.frmData.lstSelcriteria.selectedIndex
				displayGlobalCriteria();
			}
		
			function updateParameter()
			{
				var tempIndex = pCriteriaIndex();
				var	tempArray = pCriteria[tempIndex].split("~~~~~");
				var tempStr = '';
				for(i=0;i<tempArray.length;i++)
				{
					if (tempStr == '')
						tempStr =  tempArray[i];
					else
					{
						if(i == 9)
							tempStr =  tempStr + '~~~~~' + unescape(document.frmData.txtLabel.value);
						else
							tempStr =  tempStr + '~~~~~' + tempArray[i];
					}
				}
				pCriteria[tempIndex] = tempStr;
			}
		
			function window_onLoad()
			{
				if(document.frmData.hdPCriteria.value != '-1')
				{
					pCriteria = document.frmData.hdPCriteria.value.split('`````,');
					for(i=0;i<pCriteria.length-1;i++)
						pCriteria[i] = pCriteria[i] + '`````';
				}

				if(document.frmData.hdPOLECriteria.value != '-1')
				{
					pOLECriteria = document.frmData.hdPOLECriteria.value.split('`````,');
					for(i=0;i<pOLECriteria.length-1;i++)
						pOLECriteria[i] = pOLECriteria[i] + '`````';
				}
		
				if(document.frmData.hdDateFormula.value != '-1')
				{
					pDateFormula = document.frmData.hdDateFormula.value.split('`````,');
					for(i=0;i<pDateFormula.length-1;i++)
						pDateFormula[i] = pDateFormula[i] + '`````';
				}
				
				if(document.frmData.hdPGlobalCriteria.value != '-1')
				{
					pGlobal = document.frmData.hdPGlobalCriteria.value.split('`````,');
					for(i=0;i<pGlobal.length-1;i++)
						pGlobal[i] = pGlobal[i] + '`````';
				}
				
				changeFieldName(-1);

				globalCriteriaChange();
				ButtonState();
				displayCriteria();
				displayGlobalCriteria();
				
				if(document.frmData.lstSelcriteria.length > 0)
				{
					document.frmData.lstSelcriteria.selectedIndex = 0;
					//-- handle Parameter/ Label
					tempArray = pCriteria[0].split("~~~~~");
					if(tempArray[10] == 1)
					{
						document.frmData.chkParameter.checked = true;
						document.frmData.txtLabel.readOnly = false;
						document.frmData.txtLabel.value = tempArray[9];
					}
				}			
				document.frmData.btnToggle.disabled = true;
						
				return true;
			}

			function openDerivedField2(pText,pBtnClick,pTabID)
			{
				window.document.frmData.hdDerFieldName.value = pText;
				window.open('derivedfields_2.asp?usercolname='+pText+'&UpdateMode='+pBtnClick+'&TABID='+pTabID,'der2','resizable=yes,Width=575,Height=465,top='+(screen.availHeight-465)/2+',left='+(screen.availWidth-575)/2+'');
			}	
	
			function editDerivedField2(pText,pBtnClick,pTabID)
			{
				window.document.frmData.hdDerFieldName.value = pText;
				window.open('derivedfields_2.asp?usercolname='+pText+'&UpdateMode='+pBtnClick+'&TABID='+pTabID,'der2','resizable=yes,Width=575,Height=465,top='+(screen.availHeight-465)/2+',left='+(screen.availWidth-575)/2+'');
			}	

			function derived_click()
			{
				if(document.frmData.hdSelFields.value != -1)
					openDerivedField1(true, 1);
				else
					openDerivedField1(false, 1);
			}

			function openGScript(pTabID)
			{
				window.open('globalscript.asp?TABID='+pTabID,'glob','resizable=yes,Width=500,Height=225,top='+(screen.availHeight-225)/2+',left='+(screen.availWidth-500)/2+'');				
			}			
		
		</script>
	</head>
	
	<body class="10pt" leftmargin="0" onLoad="pageLoaded();page_onLoad();window_onLoad();">
	<form name="frmData" method="post" topmargin="0" bottommargin="0">
	<%	If Request("ERROR") <> "" Then
			Call ShowError(pageError, "Error saving Derived Field: " & Request("ERROR"), False)
		End If
	%>

	<%	If Request("MSG") <> "" Then
			Response.Write Request("MSG")
		End If
	%>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
			<tr height="4%">
				<td width="30%">
					<table border="0" class="toolbar" height="90%" cellpadding="0" cellspacing="0">
						<tr>
							<td align="center" valign="middle" height="32" width="28"><a class="LightBold" href="#" onClick="newReport();return false;"><img src="img/new.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/new2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/new.gif';this.style.zoom='100%'" title="New" /></a></td>
							<td align="center" valign="middle" height="32" width="28"><a class="LightBold" href="#" onClick= "uploadReport();return false;"> <img src="img/open.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/open2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/open.gif';this.style.zoom='100%'" title="Open"/></a></td>
							<td align="center" valign="middle" height="32" width="28"><a class="LightBold" href="#" onClick="saveReport(1,0);return false;"><img src="img/smwd_save.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/smwd_save2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/smwd_save.gif';this.style.zoom='100%'" title="Save" /></a></td>
							<td align="center" valign="middle" height="32" width="28"><a class="LightBold" href="#" onClick="saveReport(1,3);return false;"><img src="img/saveas.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/saveas2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/saveas.gif';this.style.zoom='100%'" title="Save As..." /></a></td>
							<td align="center" valign="middle" height="32" width="28"><a class="LightBold" href="#" onClick="saveReport(1,1);return false;"><img src="img/savepost.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/savepost2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/savepost.gif';this.style.zoom='100%'" title="Save and Post" /></a></td>
							<td align="center" valign="middle" height="32" width="28"><a class="LightBold" href="#" onClick="openPreview('pdf');saveReport(1,2);return false;"><img src="img/adobe_prev1.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/adobe_prev2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/adobe_prev1.gif';this.style.zoom='100%'"  title="Preview in PDF" /></a></td>
							<td align="center" valign="middle" height="32" width="28"><a class="LightBold" href="#" onClick="openPreview('xls');saveReport(1,2);return false;"><img src="img/excel_prev1.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/excel_prev2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/excel_prev1.gif';this.style.zoom='100%'"  title="Preview in XLS" /></a></td>
							<td align="center" valign="middle" height="32" width="28"><a class="LightBold" href="#" onClick="derived_click();return false;"><img src="img/derfield.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/derfield2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/derfield.gif';this.style.zoom='100%'" title="Derived Fields" /></a></td>
							<td align="center" valign="middle" height="32" width="28"><a class="LightBold" href="#" onClick="openRelDate(1);return false;"><img src="img/rel_date.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/rel_date2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/rel_date.gif';this.style.zoom='100%'" title="Relative Date" /></a></td>
						</tr>
					</table>
				</td>
				<% If objSession("REPORTNAME") <> "-1" Then %> 
					<td width="70%" class="msgheader" align="right"><% Response.Write "<b><font>" & "Report Name : " & LEFT(objSession("REPORTNAME"), 50) & "</font></b>" %>&nbsp;&nbsp;&nbsp;</td>
				<% End If %>
			</tr>
			<tr height="6%">
				<td colspan="2">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td id="tool_rptfields" width="24%" class="NotSelected">
								<a class="NotSelected1" href="JavaScript:tabNavigation(0,1)"><span style="{text-decoration:none}">Report Fields</span></a>
							</td>
							<td style="border-bottom:none;border-left-style:solid;border-left-color:#999999;border-width:2px;border-top-style:solid;border-top-width:2px;border-top-color:#FFFFFF;">&nbsp;&nbsp;</td>

							<td id="tool_rptcriteria" width="24%" class="Selected">
								<a class="Selected" href="#"><span style="{text-decoration:none}">Report Criteria</span></a>
							</td>
							<td style="border-bottom:none;border-left-style:solid;border-left-color:#999999;border-width:2px;border-top-style:solid;border-top-width:2px;border-top-color:#FFFFFF;">&nbsp;&nbsp;</td>

							<td id="tool_rptoptions" width="24%" class="NotSelected">
								<a class="NotSelected1" href="JavaScript:tabNavigation(2,1)"><span style="{text-decoration:none}">Report Options</span></a>
							</td>
							<td style="border-bottom:none;border-left-style:solid;border-left-color:#999999;border-width:2px;border-top-style:solid;border-top-width:2px;border-top-color:#FFFFFF;">&nbsp;&nbsp;</td>

							<td id="tool_graphoptions" width="24%" class="NotSelected">
								<a class="NotSelected1" href="JavaScript:tabNavigation(3,1)"><span style="{text-decoration:none}">Graph Options</span></a>
							</td>
							<td style="border-bottom:none;border-left-style:solid;border-left-color:#999999;border-width:2px;border-top-style:solid;border-top-width:2px;border-top-color:#FFFFFF;">&nbsp;&nbsp;</td>

							<!--td id="tool_hier" width="25%" class="NotSelected">
								<a class="NotSelected1" href1="JavaScript:tabNavigation(4,1)"><span style="{text-decoration:none}">Column Hierarchy</span></a>
							</td>
							<td style="border-bottom:none;border-left-style:solid;border-left-color:#999999;border-width:2px;border-top-style:solid;border-top-width:2px;border-top-color:#FFFFFF;">&nbsp;&nbsp;</td-->
						</tr>
					</table>
				</td>
			</tr>
			<tr height="90%">
				<td colspan="2">	
					<table id="reportcriteria" style1="display:none" width="100%" class="background" border="0" height="100%" bgcolor="white">
						<tr class="background" valign="top">
							<td>
								<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%" bgcolor="white">
									<tr>
										<td>
											<table class="formsubtitle" border="0" width="100%" bgcolor="white" height="10%">
												<tr>
													<td>&nbsp;</td>
												</tr>
												<tr>
													<td class="ctrlgroup">&nbsp;&nbsp;Column Criteria</td>
												</tr>
											</table>					
											<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="white">
												<tr>
													<td width="100%" align="center" colspan="3">&nbsp;</td>
												</tr>
												<tr>
													<td width="45%" align="left">&nbsp;&nbsp;Column : 
														<select size="1" style="width:70%" id1=select1 name="cboSelfield" onChange="changeFieldName(0);globalCriteriaChange();">
														<%	If objSessionStr("ArrayP") <> "-1" Then
																Dim i
																If mArrayP Then	
																	For i = 0 To UBound(myArrayP)
																		If i=0 Then 
																			Response.Write "<option selected>" & myArrayP(i) & "</option>"
																		Else
																			Response.Write "<option>" & myArrayP(i) & "</option>"
																		End If
																	Next
																End If	
															End If
														%>
														</select>
													</td>
													<td width="15%" align="left"><input type="checkbox" disabled  name="chkParameter" onClick="EnableTextBox()" >&nbsp;Parameter</td>
													<td width="40%" align="left">Prompt label for this parameter :&nbsp;<input type="text" size=17 ReadOnly  name=txtLabel onChange="updateParameter();"></td>
												</tr>
												<tr>
													<td width="100%" align="center" colspan="3">&nbsp;</td>
												</tr>
												<tr>
													<td width="100%" align="left" colspan="3">&nbsp;&nbsp;Criteria</td>				
												</tr>
												<tr>
													<td width="100%" align="left" colspan="3">&nbsp;
														<select size="4" style="width:98%" name="lstSelcriteria" onChange="ButtonState();" onDblClick="editCriteria();">
														</select>
													</td>
												</tr>
												<tr>
													<td width="100%" align="center" colspan="3">&nbsp;</td>
												</tr>
												<tr>
													<td width="100%" align="left" colspan="3">&nbsp;&nbsp;Global Criteria</td>				
												</tr>
												<tr>
													<td width="80%" align="center" colspan="2">&nbsp;<input type="text" readonly style="width:98%;background-color:silver" id1=text2 name="txtGlobal"></td>
													<td width="20%" align="left"><input class="button" type="button" value="Set Global Criteria" onClick="openGlobalCriteria();" id1=button1 name="btnGlobal" disabled></td>
												</tr>																										
												<tr>
													<td width="100%" align="center" colspan="3">&nbsp;</td>
												</tr>
											</table>
											<table class="formsubtitle" border="0" width="100%" bgcolor="white">
												<tr>
													<td class="ctrlgroup">&nbsp;&nbsp;Report Criteria (for all fields)</td>
												</tr>
												<tr>
													<td  colspan="5">&nbsp;</td>
												</tr>
											</table>					
											<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="white">
												<tr>
													<td width="100%" align="left">&nbsp;&nbsp;<textarea readonly rows="4" style="width:98%;background-color:silver" id1=textarea1 name="txtRptcriteria" wrap="off"></textarea></td>
												</tr>
											</table>
											<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="white">
												<tr>
													<td  colspan="5">&nbsp;</td>
												</tr>
												<tr>																														
													<td width="20%" align="center"><input class="button" type="button" value="Add Criteria" onClick="window.open('addcriteria_1.asp?a='+window.document.frmData.cboSelfield.selectedIndex+'&b='+window.document.frmData.cboSelfield.options[window.document.frmData.cboSelfield.selectedIndex].text+'&c='+escape(pXML[window.document.frmData.cboSelfield.selectedIndex]),'addcri_1','resizable=yes,Width=450,Height=200,top='+(screen.availHeight-200)/2+',left='+(screen.availWidth-450)/2+'')" id1=button2 name="btnAdd"></td>
													<td width="20%" align="center"><input class="button" type="button" value="Toggle AND/OR"  id1=button3 name="btnToggle" disabled onClick="toggle()"></td>				
													<td width="19%" align="center">&nbsp;<input class="button" type="button" value="DISTINCT Rows"  onclick="test();" id1=button4 name="btnDistinct" disabled></td>
													<td width="20%" align="center"><input class="button" type="button" value="Edit Criteria" id1=button5 name="btnEdit" onClick="editCriteria();"></td>				
													<td width="20%" align="center"><input class="button" type="button" value="Delete Criteria" name="btnDelete" onClick="deleteClick()" disabled></td>				
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>							
		</table>

		<input type="hidden" name="hdMode1" value="<%=objSessionStr("MODE1")%>">

		<input type="hidden" name="hdTabID" value="">
		<input type="hidden" name="hdSave" value="<%=objSessionStr("Save")%>">
		<input type="hidden" name="hdSaveFinal" value="<%=objSessionStr("SaveFinal")%>">
		<input type="hidden" name="hdRptName" value="" >
		<input type="hidden" name="hdRptDesc" value="" >
		<input type="hidden" name="hdSelFields" value="<%=objSessionStr("ArrayP")%>">
		<input type="hidden" name="hdSelFieldsLength" value="" >
		<input type="hidden" name="hdArrayTF" value="">
		<input type="hidden" name="hdArrayPFields" value="">			
		<input type="hidden" name="hdDataType" value="">
		<input type="hidden" name="hdColProp" value=">">
		<input type="hidden" name="hdArrayCurrentFields" value=''>
		<input type="hidden" name="hdSelFieldsXML" value="<%=fixDoubleQuote(objSessionStr("ArrayPXML"))%>">
		<input type="hidden" name="hdPCriteria" value="<%=fixDoubleQuote(objSessionStr("ArrayPCriteria"))%>">
		<input type="hidden" name="hdPOLECriteria" value="<%=fixDoubleQuote(objSessionStr("ArrayPOLECriteria"))%>">
		<input type="hidden" name="hdDateFormula" value="<%=fixDoubleQuote(objSessionStr("ArrayDateFormula"))%>">
		<input type="hidden" name="hdPGlobalCriteria" value="<%=fixDoubleQuote(objSessionStr("ArrayPGlobalCriteria"))%>">
		<input type="hidden" name="hdSelFieldCriteria" value="">

		<input type="hidden" name="hdSubmit" value="">

		<input type="hidden" name="hdDerFieldType" value="">
		<input type="hidden" name="hdDerFieldName" value="">
		<input type="hidden" name="hdDerFieldWidth" value="">
		<input type="hidden" name="hdDerDetailFormula" value="">
		<input type="hidden" name="hdDerIndex" value=""> 
		<input type="hidden" name="hdDerAddEdit" value=""> 
		<input type="hidden" name="hdDerAggregate" value=""> 
		<input type="hidden" name="hdDerAggregateFormula" value="">
		<input type="hidden" name="hdDerColumnInvolved" value="<%=fixDoubleQuote(objSessionStr("ColumnsInvolved"))%>"> 
		<input type="hidden" name="hdDerError" value='<%=sError%>'> 
		<input type="hidden" name="hdDerGScript" value="<%=sGlobalScript%>">
		
		<input type="hidden" name="hdRelDate" value=""> 
		<input type="hidden" name="hdRelDateFormula" value=""> 
		<input type="hidden" name="hdRelativeDate" value=""> 
		<input type="hidden" name="hdRelMode" value=""> 
		<input type="hidden" name="hdRelID" value="">
		
		<input type="hidden" name="hdPost" value="0"> 
		<input type="hidden" name="hdOutputType" value="0">
		<input type="hidden" name="hdPreview" value="0">
	</form>
	</body>
</html>

<%

Function SaveReport()

	'-- objSession("MODE1") will always be 1 here
	'-- User continues using tabbing but hasn't clicked on Save button
	If objSessionStr("Save") = "0" Then
	
		rptName = "-1"
		sXML = UpdateXMLReport()
		
		If Not GetValidObject("InitSMList.CDataManager", objData) Then
			Call ShowError(pageError, "Call to CDataManager class failed.", True)
		End If
		
		objData.m_RMConnectStr = Application("SM_DSN")
		objData.TempReportID = objSessionStr("REPORTID")
		objData.ReportName = rptNameok
		objData.XML = sXML
		objData.UpdateTempReport
		
		Set objData = Nothing

	'-- User copies the report
	ElseIf objSessionStr("MODE1") = "1" And objSessionStr("Save") = "3" Then
		rptName = Request.Form("hdRptName")
		rptDesc = Request.Form("hdRptDesc")
				
		sXML = UpdateXMLReport()

		'-- Delete corresponding temporary report from database
		If Not GetValidObject("InitSMList.CDataManager", objData) Then
			Call ShowError(pageError, "Call to CDataManager class failed.", True)
		End If
		
		objData.m_RMConnectStr = Application("SM_DSN")
		objData.TempReportID = objSessionStr("REPORTID")
		objData.ReportName = rptName
		objData.ReportDesc = rptDesc
		objData.XML = sXML
		objData.DeleteTempReport
		objData.AddReport objSessionStr("UserID")

		objSession("REPORTID") = objData.ReportID
		objSession("REPORTNAME") = objData.REPORTNAME

		Set objData = Nothing
			
		objSession("Save") = "1"
		objSession("SaveFinal") = "1" 

		Set objData = Nothing
					
	'-- User continues using tabbing but has clicked on Save button first time
	ElseIf objSessionStr("Save") = "1" And objSessionStr("SaveFinal") = "0" Then

		rptName=Request.Form("hdRptName")
		rptDesc=Request.Form("hdRptDesc")

		sXML = UpdateXMLReport()

		'-- Delete corresponding temporary report from database
		If Not GetValidObject("InitSMList.CDataManager", objData) Then
			Call ShowError(pageError, "Call to CDataManager class failed.", True)
		End If
		
		objData.m_RMConnectStr = Application("SM_DSN")
		objData.TempReportID = objSessionStr("REPORTID")
		objData.ReportName = rptName
		objData.ReportDesc = rptDesc
		objData.XML = sXML
		objData.DeleteTempReport
		objData.AddReport objSessionStr("UserID")

		objSession("REPORTID") = objData.ReportID
		objSession("REPORTNAME") = objData.REPORTNAME

		Set objData = Nothing
			
		objSession("SaveFinal") = "1" 
				
	'-- User has already clicked on Save button and again clicks save button or uses tabbing
	ElseIf objSessionStr("Save") = "1" And objSessionStr("SaveFinal") = "1" Then

		sXML = UpdateXMLReport()
		
		If Not GetValidObject("InitSMList.CDataManager", objData) Then
			Call ShowError(pageError, "Call to CDataManager class failed.", True)
		End If
		
		objData.m_RMConnectStr = Application("SM_DSN")
		objData.ReportID = objSessionStr("REPORTID")
		objData.XML = sXML
		objData.UpdateReport
		Set objData = Nothing
		
	End If 


End Function


Function UpdateXMLReport()

	'-- Add/Delete XML tags for Criteria

	Dim objXML, arrCriteria, arrOLECriteria, arrTemp, sTemp, arrGlobalCriteria, bParameter, sLabel, arrDateFormulaValues
	Dim iCount1, iCount2, m
	Dim arrOperators, arrData, arrConnectors, arrAttributes, sFieldName, arrCriteriaParts, arrOLECriteriaParts, arrDateFormula, arrDateFormulaParts, sDateFormula
	On Error Resume Next
	Set objXML = Server.CreateObject("Msxml2.DOMDocument")
	objXML.async = false

	'--Load the existing XML

	objXML.loadXML sXML


	Set z = objXML.getElementsByTagName("report").item(0)
	If Request.Form("hdDerGScript") <> "" Then
	
		If Not GetValidObject("DTGScript.ScriptEngine", script) Then
			Call ShowError(pageError, "Call to DTG ScriptEngine class failed.", True)
		End If

		If Not GetValidObject("InitSMList.CEventSink", evobj) Then
			Call ShowError(pageError, "Call to CEventSink class failed.", True)
		End If

		script.StartEngine

		script.RegisterGlobalObject "", evobj, False

		On Error Resume Next
		script.AddScript Request.Form("hdDerGScript")
		
		sError = script.LastError
		On Error Goto 0
		
		If sError = "" Then
			z.setAttribute "globalscript", Request.Form("hdDerGScript")
		Else
			script.StopEngine
			Set evobj = Nothing
			Set script = Nothing
		End If
	Else
		If Not z.getAttributeNode("globalscript") Is Nothing Then z.removeAttribute("globalscript")
	End If
	
	Dim y
	Set y = objXML.selectNodes("//criterialist//criteria")
	y.removeAll

	Set y = objXML.selectNodes("//havinglist//having")
	y.removeAll

	If (objSessionStr("ArrayPCriteria") <> "" And objSessionStr("ArrayPCriteria") <> "-1") Then

		arrCriteria = Split(objSessionStr("ArrayPCriteria"), "`````,")
		
		arrOLECriteria = Split(objSessionStr("ArrayPOLECriteria"), "`````,")
		
		arrDateFormula = Split(objSessionStr("ArrayDateFormula"), "`````,")

		If IsValidArray(arrCriteria) Then
			If IsValidArray(arrOLECriteria) Then
				If IsValidArray(arrDateFormula) Then

					If UBound(arrCriteria) = 0 Then
						arrCriteria(0) = Replace(arrCriteria(0), "`````", "")
						arrOLECriteria(0) = Replace(arrOLECriteria(0), "`````", "")
						arrDateFormula(0) = Replace(arrDateFormula(0), "`````", "")
					Else
						For i = 0 To UBound(arrCriteria)
							If Right(arrCriteria(i), 5) = "`````" Then	arrCriteria(i) = Left(arrCriteria(i), Len(arrCriteria(i))-5 )
							If Right(arrOLECriteria(i), 5) = "`````" Then	arrOLECriteria(i) = Left(arrOLECriteria(i), Len(arrOLECriteria(i))-5 )
							If Right(arrDateFormula(i), 5) = "`````" Then	arrDateFormula(i) = Left(arrDateFormula(i), Len(arrDateFormula(i))-5 )
						Next
					End If

					For iCount1 = 0 To UBound(arrCriteria)
						arrCriteriaParts = Split(arrCriteria(iCount1), "~~~~~")
						
						'-- Date Formula handling
						arrDateFormulaParts = Split(arrDateFormula(iCount1), "~~~~~")
												
						If IsValidArray(arrCriteriaParts) Then
							arrOperators = Split(arrCriteriaParts(1), ",")
							If arrCriteriaParts(2) <> "" Then
								arrData = Split(arrCriteriaParts(2), "^^^^^")
							Else
								Redim arrData(0)
								arrData(0) = ""
							End If
						End If
						
						arrConnectors = Split(arrCriteriaParts(3), ",")

						'-- handle range data
						If IsValidArray(arrOperators) And IsValidArray(arrData) Then
							If Trim(arrOperators(0)) = "BETWEEN" Then
								For i = 0 To UBound(arrData)
									Redim Preserve arrRangeFrom(i)
									Redim Preserve arrRangeTo(i)
									Redim Preserve arrConnector1(i)
									Redim Preserve arrConnector2(i)
									arrTemp = Split(arrData(i), "!!!!!")
									arrRangeFrom(i) = arrTemp(0)
									arrRangeTo(i) = arrTemp(1)
									
									arrTemp = Split(arrConnectors(i), "|")
									arrConnector1(i) = arrTemp(0)
									arrConnector2(i) = arrTemp(1)
								Next
							End If
						End If
						
						sFieldName = arrCriteriaParts(4)

						arrAttributes = Split(arrCriteriaParts(7), "|")
						
						Dim x, a, z, t, k, y1, y2, sDateFormulaValue1, sDateFormulaValue2
						
						If IsValidArray(arrDateFormulaParts) Then
							arrDateFormulaValues = Split(arrDateFormulaParts(1), ",")							
							If IsValidArray(arrDateFormulaValues) And UBound(arrDateFormulaValues) > 0 Then
								sDateFormulaValue1 = arrDateFormulaValues(0)
								sDateFormulaValue2 = arrDateFormulaValues(1)
							End If
						End If
						
						bParameter = arrCriteriaParts(8)
						
						sLabel = arrCriteriaParts(9)

						
						Set x = objXML.getElementsByTagName("criterialist").item(0).appendChild(objXML.createElement("criteria"))
						
						Set a = objXML.getElementsByTagName("criteria").item(iCount1)
								a.setAttribute "fieldnumber", arrAttributes(0)
								a.setAttribute "tableid", arrAttributes(1) 
								a.setAttribute "fieldid", arrAttributes(2)  
								a.setAttribute "type", arrAttributes(3)
								a.setAttribute "connector", LCase(arrAttributes(4))
								a.setAttribute "hasdatadefcrit", arrAttributes(5)
								a.setAttribute "hideinRMNet", arrAttributes(6)
								If arrAttributes(7) <> "" Then a.setAttribute "rmnetlabel", arrAttributes(7)

						Set y = x.appendChild(objXML.createElement("filters"))
						
						If IsValidArray(arrData) And IsValidArray(arrOperators) And IsValidArray(arrConnectors) Then
							For iCount2 = 0 To UBound(arrData)
								If Trim(arrOperators(0)) <> "BETWEEN" And Trim(arrOperators(0)) <> "IN" Then
									Set z =	y.appendChild(objXML.createElement("filter"))
										z.setAttribute "operator", arrOperators(iCount2)
										z.setAttribute "connector", LCase(arrConnectors(iCount2))
									Set t =	z.appendChild(objXML.createElement("data"))	
									Set k = t.appendChild(objXML.createTextNode(arrData(iCount2)))

									If IsValidArray(arrDateFormulaParts) Then
										If arrDateFormulaParts(1) <> "" Then
											sDateFormula = arrDateFormulaParts(1)
										Else
											sDateFormula = ""
										End If
										
										If sDateFormula <> "" Then
											If arrDateFormulaParts(2) = objXML.getElementsByTagName("criterialist").item(0).childNodes(iCount1).getAttributeNode("fieldnumber").value then
												Set x = objXML.getElementsByTagName("criterialist").item(0).childNodes(iCount1)
																
												Set z =	x.getElementsByTagName("filter").item(0)
												Set t1 = z.appendChild(objXML.createElement("dateformula"))	
												Set t2 = t1.appendChild(objXML.createTextNode(sDateFormula))	
											End If
										End If
									End If	
									
								ElseIf Trim(arrOperators(0)) = "BETWEEN" Then'-- Handle range data
									'-- From data
									Set z =	y.appendChild(objXML.createElement("filter"))
										z.setAttribute "operator", "ge"
										z.setAttribute "connector", LCase(arrConnector1(iCount2))
									Set t =	z.appendChild(objXML.createElement("data"))	
									Set k = t.appendChild(objXML.createTextNode(arrRangeFrom(iCount2)))
									If sDateFormulaValue1 <> "" Then
										Set g = z.appendChild(objXML.createElement("dateformula"))	
										Set g1 = g.appendChild(objXML.createTextNode(sDateFormulaValue1))	
									End If
									'-- To data
									Set z =	y.appendChild(objXML.createElement("filter"))
										z.setAttribute "operator", "le"
										z.setAttribute "connector", LCase(arrConnector2(iCount2))
									Set t =	z.appendChild(objXML.createElement("data"))	
									Set k = t.appendChild(objXML.createTextNode(arrRangeTo(iCount2)))	
									If sDateFormulaValue2 <> "" Then
										Set g2 = z.appendChild(objXML.createElement("dateformula"))	
										Set g3 = g2.appendChild(objXML.createTextNode(sDateFormulaValue2))	
									End If
								Else '-- Handle codelist data
									Set z =	y.appendChild(objXML.createElement("filter"))
										z.setAttribute "operator", "eq"
										z.setAttribute "connector", LCase(arrConnectors(iCount2))
									Set t =	z.appendChild(objXML.createElement("data"))	
									Set k = t.appendChild(objXML.createTextNode(arrData(iCount2)))				
								End If
							Next
						End If
			
						'-- OLE Criteria handling
						arrOLECriteriaParts = Split(arrOLECriteria(iCount1), "~~~~~")
							
						If IsValidArray(arrOLECriteriaParts) Then
							If arrOLECriteriaParts(1) <> "" Then
								arrOLEData = Split(arrOLECriteriaParts(1), "^^^^^")
							Else
								Redim arrOLEData(0)
								arrOLEData(0) = ""
							End If

							If arrOLECriteriaParts(2) = objXML.getElementsByTagName("criterialist").item(0).childNodes(iCount1).getAttributeNode("fieldnumber").value then
								Set x = objXML.getElementsByTagName("criterialist").item(0).childNodes(iCount1)
												
								Set z =	x.insertBefore(objXML.createElement("olecriteria"),x.getElementsByTagName("filters").Item(0))
								Set t1 = z.appendChild(objXML.createElement("olecritname"))	
								Set t2 = t1.appendChild(objXML.createTextNode(arrOLEData(0)))	
								Set t1 = z.appendChild(objXML.createElement("olecritdesc"))	
								Set t2 = t1.appendChild(objXML.createTextNode(arrOLEData(1)))	
							End If
						End If	
			
						'-- Handle for Parameter and Label
						If bParameter = 1 Then
							Set z =	x.insertBefore(objXML.createElement("parameter"),x.getElementsByTagName("filters").Item(0))
							Set t = z.appendChild(objXML.createTextNode(sLabel))	
						End If
			
					Next
				End If
			End If
		End If

	End If

	If (objSessionStr("ArrayPGlobalCriteria") <> "" And objSessionStr("ArrayPGlobalCriteria") <> "-1") Then

		arrGlobalCriteria = Split(objSessionStr("ArrayPGlobalCriteria"), "`````,")	

		If IsValidArray(arrGlobalCriteria) Then
			If UBound(arrGlobalCriteria) = 0 Then
				arrGlobalCriteria(0) = Replace(arrGlobalCriteria(0), "`````", "")
			End If
			Dim arrGlobalCriteriaParts, arrGlobalOperators, arrGlobalData, GlobalConnector, GlobalAttribute
			'-- Handle Global Criteria
			Dim iCounter
			iCounter = 0
			For iCount1 = 0 To UBound(arrGlobalCriteria)
				arrGlobalCriteriaParts = Split(arrGlobalCriteria(iCount1), "~~~~~")
				arrGlobalOperators = Split(arrGlobalCriteriaParts(1), ",")
		
			    If IsValidArray(arrGlobalOperators) And IsValidArray(arrGlobalCriteriaParts) Then
					arrGlobalData = Split(arrGlobalCriteriaParts(2), "^^^^^")

					GlobalConnector = arrGlobalCriteriaParts(3) '-- either blank, or, and

					GlobalAttribute = arrGlobalCriteriaParts(4) '-- fieldnumber
				
					Set x = objXML.getElementsByTagName("havinglist").item(0).appendChild(objXML.createElement("having"))
				
					Set a = objXML.getElementsByTagName("having").item(iCounter)
							a.setAttribute "fieldnumber", GlobalAttribute
							a.setAttribute "hideinRMNet", "0" '-- Need to be handled
				
					Set y1 = x.appendChild(objXML.createElement("value1"))
							 y1.setAttribute "operator", arrGlobalOperators(0)
					Set k = y1.appendChild(objXML.createTextNode(arrGlobalData(0)))			
				
					If GlobalConnector <> "" Then
						Set y2 = x.appendChild(objXML.createElement("value2"))
								 y2.setAttribute "operator", arrGlobalOperators(1)
								 y2.setAttribute "connector", GlobalConnector
				
						Set k = y2.appendChild(objXML.createTextNode(arrGlobalData(1)))			
					End If
					iCounter = iCounter + 1
				End If
			Next
		End If
	End If

	If Request.Form("hdSubmit") = "RELATIVEDATE" And Request.Form("hdRelMode") = "ADD" Then
				
		If Request.Form("hdRelDate") <> "" And Request.Form("hdRelDateFormula") <> "" Then
			Dim objData

			If Not GetValidObject("InitSMList.CDataManager", objData) Then
				Call ShowError(pageError, "Call to CDataManager class failed.", True)
			End If
				
			objData.m_RMConnectStr = Application("SM_DSN")
		
			objData.AddDateFormula Request.Form("hdRelDate"), Request.Form("hdRelDateFormula")
			Set objData = Nothing
		End If
		
	ElseIf Request.Form("hdSubmit") = "RELATIVEDATE" And Request.Form("hdRelMode") = "UPDATE" Then

		If Request.Form("hdRelDate") <> "" And Request.Form("hdRelDateFormula") <> "" Then

			If Not GetValidObject("InitSMList.CDataManager", objData) Then
				Call ShowError(pageError, "Call to CDataManager class failed.", True)
			End If

			objData.m_RMConnectStr = Application("SM_DSN")
						
			objData.UpdateDateFormula CInt(Request.Form("hdRelID")),Request.Form("hdRelDateFormula")
			Set objData = Nothing
		End If
	
	End If

	Dim objDerived, objDetailFormula, objAggregateFormula, a1, a2, a3
	
	'-- Add mode
	If Request.Form("hdSubmit") = "DERIVED" And Request.Form("hdDerAddEdit") = "Add" Then

		If Not GetValidObject("DTGScript.ScriptEngine", script) Then
			Call ShowError(pageError, "Call to DTG ScriptEngine class failed.", True)
		End If
	
		If Not GetValidObject("InitSMList.CEventSink", evobj) Then
			Call ShowError(pageError, "Call to CEventSink class failed.", True)
		End If

		script.StartEngine

		script.RegisterGlobalObject "", evobj, False

		On Error Resume Next
		script.AddScript "function " & Request.Form("hdDerFieldName") & "()" & Chr(13) & " end function"
		sError = script.LastError
		If sError = "" Then
			script.AddScript "function " & Request.Form("hdDerFieldName") & "()" & Chr(13) & Request.Form("hdDerDetailFormula") & Chr(13) & Chr(13) & " end function"
			sError = script.LastError
			If sError = "" Then
				script.AddScript "function " & Request.Form("hdDerFieldName") & "()" & Chr(13) & Request.Form("hdDerAggregateFormula") & Chr(13) & Chr(13) & " end function"
				sError = script.LastError
				If sError = "" Then
					'-- Add column tag in columnorder tag and its value
					Set z = objXML.getElementsByTagName("columnorder").item(0).appendChild(objXML.createElement("column"))
						z.appendChild(objXML.createTextNode(objXML.getElementsByTagName("columnorder").item(0).childNodes.length - 1))
					For k = objXML.getElementsByTagName("columnlist").item(0).childNodes.length - 1 To 0 Step -1
						Set a1 = objXML.getElementsByTagName("columnlist").item(0).childNodes.item(k)
						Set a2 = a1.getElementsByTagName("derived").item(0)
						
						a4 = Cstr(CInt(a1.getAttributeNode("fieldnumber").value) + 1)
						
						If Not a2 Is Nothing Then
							a3 = a1.getAttributeNode("tableid").value
							a3 = CStr(CInt(a3) - 1)
							Exit For 
						Else
							a3 = "-1"
						End If
					
					Next

					'-- Add column in columnlist tag and its children
					Set z = objXML.getElementsByTagName("columnlist").item(0).appendChild(objXML.createElement("column"))
						z.setAttribute "fieldnumber", a4
						z.setAttribute "tableid", a3
						z.setAttribute "fieldid", a3
						z.setAttribute "usercolname", Request.Form("hdDerFieldName")
						z.setAttribute "colwidthunits", "chars"
						z.setAttribute "colwidth", Request.Form("hdDerFieldWidth")
						z.setAttribute "justify", "left"
						z.setAttribute "color", "0"
			
		
					Set objDerived = z.appendChild(objXML.createElement("derived"))
						objDerived.setAttribute "fieldtype", Request.Form("hdDerFieldType")
						objDerived.setAttribute "fieldname", Request.Form("hdDerFieldName")
						objDerived.setAttribute "defaultfieldwidth", Request.Form("hdDerFieldWidth")
						Set objDetailFormula = objDerived.appendChild(objXML.createElement("detailformula"))
						
					objDetailFormula.appendChild(objXML.createTextNode(Request.Form("hdDerDetailFormula")))			
							
					'-- Check for Aggregate Formula
					If Request.Form("hdDerAggregate") = "sameformula" Or Request.Form("hdDerAggregate") = "differentformula" Then
						Set objAggregateFormula = objDerived.appendChild(objXML.createElement("aggregateformula"))
							objDerived.setAttribute "aggregatecalcmethod", Request.Form("hdDerAggregate")
					End If
				
					objAggregateFormula.appendChild(objXML.createTextNode(Request.Form("hdDerAggregateFormula")))

					'-- Set parenttables tag
					z.appendChild(objXML.createElement("parenttables"))

					'-- Update p and pXML array with new added derived field and its attributes and children
					objSession("ArrayPXML") = Trim(objSessionStr("ArrayPXML")) & "," & Trim(objXML.getElementsByTagName("columnlist").item(0).childNodes.item(objXML.getElementsByTagName("columnlist").item(0).childNodes.length - 1).xml)
'-- Rajeev -- 03/09/2004					
'''					objSession("ArrayP") = Trim(objSessionStr("ArrayP")) & "," & "Derived : " & Request.Form("hdDerFieldName")
					objSession("ArrayP") = Trim(objSessionStr("ArrayP")) & "*!^" & "Derived : " & Request.Form("hdDerFieldName")
				Else
					script.StopEngine
		
					Set evobj = Nothing
					Set script = Nothing 
				End If
				
			Else
				script.StopEngine
		
				Set evobj = Nothing
				Set script = Nothing 
			End If
		Else
			script.StopEngine
		
			Set evobj = Nothing
			Set script = Nothing 
		End If

	ElseIf Request.Form("hdSubmit") = "DERIVED" And Request.Form("hdDerAddEdit") = "Edit" Then '-- Edit mode
		
		Set z = objXML.getElementsByTagName("columnlist").item(0).childNodes.item(CInt(Request.Form("hdDerIndex")))

		z.setAttribute "colwidth", Request.Form("hdDerFieldWidth")

		Set objDerived = z.getElementsByTagName("derived").item(0)

			objDerived.setAttribute "fieldtype", Request.Form("hdDerFieldType")
			objDerived.setAttribute "fieldname", Request.Form("hdDerFieldName")
			objDerived.setAttribute "defaultfieldwidth", Request.Form("hdDerFieldWidth")
		
		'-- Check for Aggregate Formula
		Dim objTempAgg, p, sAggFormula
		Set objTempAgg = objDerived.getElementsByTagName("aggregateformula").item(0)
		
		If Not objTempAgg Is Nothing Then
			sAggFormula = objTempAgg.text 
		Else
			sAggFormula = ""
		End If
		
		Set p = z.childNodes.item(0).getAttributeNode("aggregatecalcmethod")

		If Not p Is Nothing Then 
			objDerived.removeAttribute("aggregatecalcmethod")
		End If

		If Not objTempAgg Is Nothing Then
			objDerived.removeChild(objTempAgg)
		End If
		
		If Not GetValidObject("DTGScript.ScriptEngine", script) Then
			Call ShowError(pageError, "Call to DTG ScriptEngine class failed.", True)
		End If
	
		If Not GetValidObject("InitSMList.CEventSink", evobj) Then
			Call ShowError(pageError, "Call to CEventSink class failed.", True)
		End If

		script.StartEngine

		script.RegisterGlobalObject "ScriptEvent", evobj, False
		
		On Error Resume Next
		script.AddScript "function " & Request.Form("hdDerFieldName") & "()" & Chr(13) & Request.Form("hdDerDetailFormula") & Chr(13) & Chr(13) & " end function"
   
		sError = script.LastError
		
		If sError = "" Then
			Set objDetailFormula = objDerived.getElementsByTagName("detailformula").item(0)
				objDetailFormula.text = Request.Form("hdDerDetailFormula")

			'-- Check for Aggregate Formula
			If Request.Form("hdDerAggregate") = "sameformula" Or Request.Form("hdDerAggregate") = "differentformula" Then
				Set objAggregateFormula = objDerived.appendChild(objXML.createElement("aggregateformula"))
					objDerived.setAttribute "aggregatecalcmethod", Request.Form("hdDerAggregate")
			End If
					
			script.AddScript "function " & Request.Form("hdDerFieldName") & "()" & Chr(13) & Request.Form("hdDerAggregateFormula") & Chr(13) & Chr(13) & " end function"
   
			sError = script.LastError
				
			Set objAggregateFormula = objDerived.getElementsByTagName("aggregateformula").item(0)
				
			If sError = "" Then
				objAggregateFormula.text = Request.Form("hdDerAggregateFormula")
			Else
				objAggregateFormula.text = sAggFormula
			End If
		Else
			'-- Check for Aggregate Formula
			If Request.Form("hdDerAggregate") = "sameformula" Or Request.Form("hdDerAggregate") = "differentformula" Then
				Set objAggregateFormula = objDerived.appendChild(objXML.createElement("aggregateformula"))
					objDerived.setAttribute "aggregatecalcmethod", Request.Form("hdDerAggregate")
			End If
			Set objAggregateFormula = objDerived.getElementsByTagName("aggregateformula").item(0)
				
			objAggregateFormula.text = sAggFormula	
		
		End If

		script.StopEngine
		
		Set evobj = Nothing
		Set script = Nothing 

		objSession("ArrayPXML") = "-1"
		
		For j = 0  To objXML.getElementsByTagName("columnlist").item(0).childNodes.length - 1
			If objSessionStr("ArrayPXML") = "-1" Then
				objSession("ArrayPXML") = Trim(objXML.getElementsByTagName("columnlist").item(0).childNodes.item(j).xml)
			Else
				objSession("ArrayPXML") = Trim(objSessionStr("ArrayPXML")) & "," & Trim(objXML.getElementsByTagName("columnlist").item(0).childNodes.item(j).xml)
			End If
		Next
		
	ElseIf Request.Form("hdDerAddEdit") = "Delete" Then '-- Delete Derived Field

		Set z = objXML.getElementsByTagName("columnlist").item(0)
			z.removeChild z.childNodes.item(CInt(Request.Form("hdDerIndex")))
		
		Set z = objXML.getElementsByTagName("columnorder").item(0)
			z.removeChild z.childNodes.item(CInt(Request.Form("hdDerIndex")))

		
		objSession("ArrayPXML") = "-1"
		Dim arrColOrder()
		For j = 0  To CInt(z.childNodes.length) - 1
			If objSessionStr("ArrayPXML") = "-1" Then
				objSession("ArrayPXML") = Trim(objXML.getElementsByTagName("columnlist").item(0).childNodes.item(j).xml)
			Else
				objSession("ArrayPXML") = Trim(objSessionStr("ArrayPXML")) & "," & Trim(objXML.getElementsByTagName("columnlist").item(0).childNodes.item(j).xml)
			End If
			
			Redim Preserve arrColOrder(j)
			If CInt(Request.Form("hdDerIndex")) < Cint(Trim(objXML.getElementsByTagName("columnorder").item(0).childNodes.item(j).text)) Then
				arrColOrder(j) = Cint(Trim(objXML.getElementsByTagName("columnorder").item(0).childNodes.item(j).text)) - 1
			Else
				arrColOrder(j) = Cint(Trim(objXML.getElementsByTagName("columnorder").item(0).childNodes.item(j).text))
			End If
		Next

		Set z = objXML.selectNodes("//columns//columnorder")
		z.removeAll
			
		Set z = objXML.getElementsByTagName("columns").item(0)
		
		Set z1 = z.insertBefore(objXML.createElement("columnorder"), z.getElementsByTagName("columnlist").item(0))
		
		If IsValidArray(arrColOrder) Then
			For j = 0  To UBound(arrColOrder)
				Set z2 = z1.appendChild(objXML.createElement("column"))
					z2.appendChild(objXML.createTextNode(arrColOrder(j)))
			Next			
		End If
		
		Dim myArrayP
'-- Rajeev -- 03/09/2004					
'''		myArrayP = Split(objSessionStr("ArrayP"), ",")
		myArrayP = Split(objSessionStr("ArrayP"), "*!^")

		objSession("ArrayP") = "-1"

		If IsValidArray(myArrayP) Then
			For j = 0  To  UBound(myArrayP)
				If j <> CInt(Request.Form("hdDerIndex")) Then
					If objSessionStr("ArrayP") = "-1" Then
						objSession("ArrayP") = myArrayP(j)
					Else
'-- Rajeev -- 03/09/2004					
'''						objSession("ArrayP") = Trim(objSessionStr("ArrayP")) & "," & myArrayP(j)
						objSession("ArrayP") = Trim(objSessionStr("ArrayP")) & "*!^" & myArrayP(j)
					End If
				End If
			Next
		End If
						
		'-- Updating columns involved 
		Dim arrXML, b1, b2, b3
		Set xmlDoc = Server.CreateObject("Msxml2.DOMDocument")
		xmlDoc.async = false
		'''--- arrXML = Split(objSessionStr("ArrayPXML"),",")
		
		arrXML = Split(objSessionStr("ArrayPXML"), "</column>,")
		
		If IsValidArray(arrXML) Then
			For iCounter = 0 To UBound(arrXML) - 1
				arrXML(iCounter) = arrXML(iCounter) & "</column>"
			Next
		
			Dim sColumnInvolved
			sColumnInvolved = "-1"
			For i = 0 To UBound(arrXML)
				xmlDoc.loadXML replace( arrXML(i), "&", "&amp;")
							
				'-- Fetching DataType for derived fields
				Set b1 = xmlDoc.getElementsByTagName("column").item(0).childNodes.item(0) 
				Set b2 = xmlDoc.getElementsByTagName("derived").item(0)
				If Not b2 Is Nothing Then
					'-- Handling for columns involved
						Set b3 = b1.getElementsByTagName("detailformula").item(0)
						If sColumnInvolved = "-1" Then
							sColumnInvolved = b3.text
						Else
							sColumnInvolved = sColumnInvolved  & b3.text
						End If
				End If		
			Next
		End If
		
		objSession("ColumnsInvolved") = "-1"
		If Not (sColumnInvolved = "-1" Or sColumnInvolved = "") Then
			objSession("ColumnsInvolved") = sColumnInvolved
		End If
		
	End If	

	UpdateXMLReport = objXML.XML

	Set objXML = Nothing	
	
End Function

Function DisplayCriteria()
	On Error Resume Next
	If Not xmlDoc.getElementsByTagName("report").item(0).getAttributeNode("globalscript") Is Nothing Then
		sGlobalScript = xmlDoc.getElementsByTagName("report").item(0).getAttributeNode("globalscript").value
	End If

	Dim objCriteriaList
	Set objCriteriaList = xmlDoc.getElementsByTagName("criterialist").item(0)  
	
	Dim iCount1, iCount2, objCriteria, sOperators, sData, bParameter, sLabel
	Dim sFieldName, sConnectors, sAttributes, sCriteria, iFieldType, iDBTableID 
	Dim arrOperators(), arrData(), arrConnectors(), arrAttributes()
	Dim arrDateFormula(), sDateFormula

	Dim m_objSMList

	If Not GetValidObject("InitSMList.CInitSM", m_objSMList) Then
		Call ShowError(pageError, "Call to CInitSM class failed.", True)
	End If
		
	m_objSMList.m_RMConnectStr = objSessionStr(SESSION_DSN)
	m_objSMList.m_SMConnectStr = Application(APP_SMDDSN)

	objSession("ArrayPCriteria") = "-1"
	objSession("ArrayPOLECriteria") = "-1"
	objSession("ArrayDateFormula") = "-1"

	Dim objXML 
	Set objXML = Server.CreateObject("Msxml2.DOMDocument")
	objXML.async = false
	
	Dim arrPXML, m

	If objSessionStr("ArrayPXML") <> "-1" And objSessionStr("ArrayPXML") <> "" Then
		arrPXML = Split(objSessionStr("ArrayPXML"), "</column>,")
		If IsValidArray(arrPXML) Then
			For iCounter = 0 To UBound(arrPXML) - 1
				arrPXML(iCounter) = arrPXML(iCounter) & "</column>"
			Next

			Dim arrFieldNumbers(), a, iCount
			For iCount = 0 To UBound(arrPXML)
				objXML.LoadXML arrPXML(iCount)
				Set a = objXML.getElementsByTagName("column").Item(0)
				Redim Preserve arrFieldNumbers(CInt(iCount))
				arrFieldNumbers(iCount) = a.Attributes(0).Text
			Next
		End If
	End If
	
	Dim sType
	Dim arrOLEData1()

	For iCount1 = 0 To objCriteriaList.childNodes.length - 1
		Set objCriteria = objCriteriaList.childNodes(iCount1)
		
		sType = objCriteria.getAttributeNode("type").value

		If Trim(sType) = "data" Then
			objUtil.GetAssembledCriteria objCriteria.xml, arrOperators, arrData, arrConnectors, arrAttributes
			objUtil.GetDateFormula  objCriteria.xml, arrDateFormula
		ElseIf Trim(sType) = "codelist" or Trim(sType) = "excludecodelist" Then
			Redim arrOperators(0)
			arrOperators(0) = "IN"
			objUtil.GetAssembledCriteriaForCodes objCriteria.xml, arrData, arrConnectors, arrAttributes
		Else
			Redim arrOperators(0)
			arrOperators(0) = "BETWEEN"
			objUtil.GetAssembledCriteriaForRange objCriteria.xml, arrData, arrConnectors, arrAttributes
			objUtil.GetDateFormulaForRange  objCriteria.xml, arrDateFormula
		End If

		sOperators = ""
		sData = ""
		sConnectors = ""
		sAttributes = ""
		sCriteria = ""
		bParameter = "0"
		sLabel = ""
		sDateFormula = ""
		
		If IsValidArray(arrData) And IsValidArray(arrOperators) And IsValidArray(arrConnectors) Then
			For iCount2 = 0 To UBound(arrData)
				If Trim(arrOperators(0)) <> "BETWEEN" And Trim(arrOperators(0)) <> "IN" Then
					If sOperators = "" Then
						sOperators = arrOperators(iCount2)
						sData = arrData(iCount2)
						sConnectors = arrConnectors(iCount2)
						If IsValidArray(arrDateFormula) Then sDateFormula = arrDateFormula(iCount2)
					Else
						sOperators = sOperators & "," & arrOperators(iCount2)
						sData = sData & "^^^^^" & arrData(iCount2)
						sConnectors = sConnectors & "," & arrConnectors(iCount2)
						If IsValidArray(arrDateFormula) Then sDateFormula = sDateFormula & "," & arrDateFormula(iCount2)
					End If
				ElseIf Trim(arrOperators(0)) = "BETWEEN" Then'-- handle range data
					If sOperators = "" Then
						If Trim(sType) = "range" Then
							sOperators = " BETWEEN "
						Else
							sOperators = " NOT BETWEEN "
						End If
						sData = arrData(iCount2)
						sConnectors = arrConnectors(iCount2)
						If IsValidArray(arrDateFormula) Then sDateFormula = arrDateFormula(iCount2)
					Else
						sData = sData & "^^^^^" & arrData(iCount2)
						sConnectors = sConnectors & "," & arrConnectors(iCount2)
						If IsValidArray(arrDateFormula) Then sDateFormula = sDateFormula & "," & arrDateFormula(iCount2)
					End If
				Else '-- handle code data
					If sOperators = "" Then
						If Trim(sType) = "codelist" Then
							sOperators = " IN "
						Else
							sOperators = " NOT IN "
						End If
						sData = arrData(iCount2)
						sConnectors = arrConnectors(iCount2)
					Else
						sData = sData & "^^^^^" & arrData(iCount2)
						sConnectors = sConnectors & "," & arrConnectors(iCount2)
					End If				
				End If
			Next
		End If
		
		If IsValidArray(arrAttributes) Then
			For iCount2 = 0 To UBound(arrAttributes) '-- Need to handle last attribute (HideInRMNet)
				If sAttributes = "" Then
					sAttributes = arrAttributes(iCount2)
				Else
					sAttributes = sAttributes & "|" & arrAttributes(iCount2)
				End If
			Next
		End If

		Set z = objCriteria.getElementsByTagName("parameter").Item(0)
		If Not z Is Nothing Then
			bParameter = 1
			sLabel = z.text
		Else
			bParameter = 0
			sLabel = ""
		End If
		
		'-- If not derived field
		If Instr(1, arrAttributes(1), "-") = 0 Then
			m_objSMList.GetSMFieldInfo arrAttributes(1), arrAttributes(2), sFieldName, iFieldType, iDBTableID
		Else
			For iCount = 0 To UBound(arrPXML)
				objXML.LoadXML arrPXML(iCount)
				Set a = objXML.getElementsByTagName("column").Item(0)
				If a.attributes(0).value = arrAttributes(0) Then
 					sFieldName = a.attributes(3).value
					If objXML.getElementsByTagName("column").item(0).childNodes.item(0).attributes(0).text = "text" _
						Or objXML.getElementsByTagName("column").item(0).childNodes.item(0).attributes(0).text = "time" _
						Or objXML.getElementsByTagName("column").item(0).childNodes.item(0).attributes(0).text = "datetime" Then
						iFieldType = 0
					ElseIf objXML.getElementsByTagName("column").item(0).childNodes.item(0).attributes(0).text = "formattednumeric" _
						Or objXML.getElementsByTagName("column").item(0).childNodes.item(0).attributes(0).text = "numeric" _
						Or objXML.getElementsByTagName("column").item(0).childNodes.item(0).attributes(0).text = "currency" Then
						iFieldType = 1
					ElseIf objXML.getElementsByTagName("column").item(0).childNodes.item(0).attributes(0).text = "date" Then
						iFieldType = 2
					End If
					Exit For
				End If
			Next
			iDBTableID = arrAttributes(1)
		End If

		If IsValidArray(arrData) And IsValidArray(arrOperators) And IsValidArray(arrConnectors) Then
			For iCount2 = 0 To UBound(arrData)
				If Trim(arrOperators(0)) <> "BETWEEN" And Trim(arrOperators(0)) <> "IN" Then
					If UBound(arrData) = 0 Then
						If Trim(LCase(arrConnectors(iCount2))) = "none" Then
							sCriteria = "(" & sFieldName & MapOperatorWithString(arrOperators(iCount2), arrData(iCount2)) & ")"
						Else
							sCriteria = UCase(arrConnectors(iCount2)) & " (" & sFieldName & MapOperatorWithString(arrOperators(iCount2), arrData(iCount2)) & ")"
						End If
					Else
						If Trim(LCase(arrConnectors(iCount2))) = "none" Then
							sCriteria = "(" & sFieldName & MapOperatorWithString(arrOperators(iCount2), arrData(iCount2)) & ")"
						Else
							sCriteria = sCriteria & " " & UCase(arrConnectors(iCount2)) & " (" & sFieldName & MapOperatorWithString(arrOperators(iCount2), arrData(iCount2)) & ")"
						End If
					End If
				ElseIf Trim(arrOperators(0)) = "BETWEEN" Then'-- handle range data
					If UBound(arrData) = 0 Then
						arrTemp1 = Split(arrConnectors(iCount2), "|")
						arrTemp2 = Split(arrData(iCount2), "!!!!!")
	
						If Trim(LCase(arrTemp1(0))) = "none" Then
							sCriteria = "((" & sFieldName & sOperators & "'" & arrTemp2(0) & "' AND '" & arrTemp2(1) & "'))"
						Else
							sCriteria = "( " & UCase(arrTemp1(0)) & " (" & sFieldName & sOperators & "'" & arrTemp2(0) & "' AND '" & arrTemp2(1) & "'))"
						End If
					Else
						arrTemp1 = Split(arrConnectors(iCount2), "|")
						arrTemp2 = Split(arrData(iCount2), "!!!!!")
	
						If Trim(LCase(arrTemp1(0))) = "none" Then
							sCriteria = "((" & sFieldName & sOperators & "'" & arrTemp2(0) & "' AND '" & arrTemp2(1) & "')"
						Else
							sCriteria = sCriteria & " " & UCase(arrTemp1(0)) & " (" & sFieldName & sOperators & "'" & arrTemp2(0) & "' AND '" & arrTemp2(1) & "')"
						End If
					End If			
				Else '-- handle code data
					If UBound(arrData) = 0 Then
						If Trim(arrData(iCount2)) = "TRUE" Then
							sCriteria = "(" & sFieldName & sOperators & "(1,-1))"
						ElseIf Trim(arrData(iCount2)) = "FALSE" Then
							sCriteria = "(" & sFieldName & sOperators & "(0))"
						Else
							sCriteria = "(" & sFieldName & sOperators & "('" &  arrData(iCount2) & "'))"
						End If
					Else
						If sCriteria = "" Then
							If Trim(arrData(iCount2)) = "TRUE" Or Trim(arrData(iCount2)) = "FALSE" Then
								sCriteria = "(" & sFieldName & sOperators & "(1,-1"
							Else
								sCriteria = "(" & sFieldName & sOperators & "('" & arrData(iCount2) & "'"
							End If
						Else
							If Trim(arrData(iCount2)) = "TRUE" Or Trim(arrData(iCount2)) = "FALSE" Then
								sCriteria = sCriteria & ",0"
							Else
								sCriteria = sCriteria & ",'" & arrData(iCount2) & "'"
							End If
						End If
					End If		
				End If			
			Next
		End If

		If IsValidArray(arrOperators) And IsValidArray(arrData) Then
			If Trim(arrOperators(0)) = "BETWEEN" Then
				If UBound(arrData) > 0 Then	sCriteria = sCriteria & ")"
				If Trim(LCase(arrAttributes(4))) <> "none" Then sCriteria = Trim(UCase(arrAttributes(4))) & " " & sCriteria
				sOperators = "BETWEEN"
			End If
		
			If Trim(arrOperators(0)) = "IN" Then
				If UBound(arrData) > 0 Then	sCriteria = sCriteria & "))"
				If Trim(LCase(arrAttributes(4))) <> "none" Then sCriteria = Trim(UCase(arrAttributes(4))) & " " & sCriteria
				sOperators = "IN"
			End If
		End If

		'-- Apply parenthesis for value data
		If IsValidArray(arrOperators) And IsValidArray(arrAttributes) Then
			If Trim(arrOperators(0)) <> "BETWEEN" And Trim(arrOperators(0)) <> "IN" Then
				If UBound(arrData) > 0 Then
					If Trim(LCase(arrAttributes(4))) <> "none" Then
						sCriteria = Trim(UCase(arrAttributes(4))) & " (" & sCriteria & ")"
					Else
						sCriteria = "(" & sCriteria & ")"
					End If
				Else
					If Trim(LCase(arrAttributes(4))) <> "none" Then
						sCriteria = Trim(UCase(arrAttributes(4))) & " " & sCriteria
					End If
				End If
			End If
		End If

		Dim iIndex
		If IsValidArray(arrFieldNumbers)Then
			For iCount = 0 To UBound(arrFieldNumbers)
				If CInt(arrAttributes(0)) = CInt(arrFieldNumbers(iCount)) Then
					iIndex = iCount
					Exit For
				End If
			Next
		End If
		
		If objSessionStr("ArrayPCriteria") = "-1" Then
			objSession("ArrayPCriteria") = iIndex & "~~~~~" & sOperators & "~~~~~" & sData & "~~~~~" & sConnectors &  "~~~~~" & sFieldName & "~~~~~" & iFieldType & "~~~~~" & iDBTableID & "~~~~~" & sAttributes & "~~~~~" & bParameter & "~~~~~" & sLabel & "~~~~~" & sCriteria & "`````"
		Else
			objSession("ArrayPCriteria") = objSessionStr("ArrayPCriteria") & "," & iIndex & "~~~~~" & sOperators & "~~~~~" & sData & "~~~~~" & sConnectors &  "~~~~~" & sFieldName & "~~~~~" & iFieldType & "~~~~~" & iDBTableID & "~~~~~" & sAttributes & "~~~~~" & bParameter & "~~~~~" & sLabel & "~~~~~" & sCriteria & "`````"
		End If

		'-- Handle OLE Criteria
		Dim objOLECriteria
		Set objOLECriteria = objCriteria.getElementsByTagName("olecriteria").Item(0)
		If Not objOLECriteria Is Nothing Then
			objUtil.GetAssembledOLECriteria objCriteria.xml, arrOLEData1, arrAttributes
			
			If IsValidArray(arrOLEData1) Then
				sOLEData = arrOLEData1(0) & "^^^^^" & arrOLEData1(1)
				If objSessionStr("ArrayPOLECriteria") = "-1" Then
					objSession("ArrayPOLECriteria") = iIndex & "~~~~~" & sOLEData & "~~~~~" & arrAttributes(0) & "`````"
				Else
					objSession("ArrayPOLECriteria") = objSessionStr("ArrayPOLECriteria") & "," & iIndex & "~~~~~" & sOLEData & "~~~~~" & arrAttributes(0) & "`````"
				End If
			End If
		Else
			If objSessionStr("ArrayPOLECriteria") = "-1" Then
				objSession("ArrayPOLECriteria") = "`````"
			Else
				objSession("ArrayPOLECriteria") = objSessionStr("ArrayPOLECriteria") & "," & "`````"
			End If
		End If

		If objSessionStr("ArrayDateFormula") = "-1" Then
			objSession("ArrayDateFormula") = iIndex & "~~~~~" & sDateFormula & "~~~~~" & arrAttributes(0) & "`````"
		Else
			objSession("ArrayDateFormula") = objSessionStr("ArrayDateFormula") & "," & iIndex & "~~~~~" & sDateFormula & "~~~~~" & arrAttributes(0) & "`````"
		End If
		
	Next

	Set m_objSMList = Nothing
		
	Set objXML = Nothing

End Function

Function DisplayGlobalCriteria()
	On Error Resume Next
	Dim objHavingList
	Set objHavingList = xmlDoc.getElementsByTagName("havinglist").item(0)  
	
	Dim iCount1, iCount2, objHaving, sOperators, sData
	Dim sFieldName, sConnectors, sAttributes, sCriteria, iFieldType
	Dim arrOperators(), arrData(), GlobalConnector, arrAttributes()

	Dim objXML 
	Set objXML = Server.CreateObject("Msxml2.DOMDocument")
	objXML.async = false
	
	Dim arrPXML, m
	arrPXML = Split(objSessionStr("ArrayPXML"), "</column>,")

	If IsValidArray(arrPXML) Then
		For iCounter = 0 To UBound(arrPXML) - 1
			arrPXML(iCounter) = arrPXML(iCounter) & "</column>"
		Next	
	
		Dim arrFieldNumbers(), a, iCount
		For iCount = 0 To UBound(arrPXML)
			objXML.LoadXML arrPXML(iCount)
			Set a = Nothing
			Set a = objXML.getElementsByTagName("column").Item(0)
			If Not a Is Nothing Then
				Redim Preserve arrFieldNumbers(CInt(iCount))
				arrFieldNumbers(iCount) = a.Attributes(0).Text
			End If
		Next
	
		Set objXML = Nothing
			
		objSession("ArrayPGlobalCriteria") = "-1"

		Dim iCounter
		iCounter = 0
		For iCount1 = 0 To UBound(arrPXML)
			Set objHaving = objHavingList.childNodes(iCounter)
			
			If Not objHaving Is Nothing Then
				objUtil.GetAssembledGlobalCriteria objHaving.xml, arrOperators, arrData, GlobalConnector, arrAttributes

				sOperators = ""
				sData = ""
				sConnectors = ""
				sAttributes = ""
				sCriteria = ""

				Dim iIndex

				If arrFieldNumbers(iCount1) <> arrAttributes(0) Then
					iIndex = iCount1
				Else
					sOperators = arrOperators(0)
					sData = arrData(0)
					sConnectors = ""
			
					If GlobalConnector <> "" Then			
						sOperators = sOperators & "," & arrOperators(1)
						sData = sData & "^^^^^" & arrData(1)
						sConnectors = GlobalConnector
					End If			
			
					sAttributes = arrAttributes(0)'-- Need to handle last attribute (HideInRMNet)

					If IsValidArray(arrFieldNumbers) Then
						For iCount = 0 To UBound(arrFieldNumbers)
							If CInt(arrAttributes(0)) = CInt(arrFieldNumbers(iCount)) Then
								iIndex = iCount
								Exit For
							End If
						Next
					End If

					Dim tempXML, tempStr
					tempXML = arrPXML(iIndex)
			
					If InStr(1, tempXML, "aggcount") > 0 Then
						tempStr = "COUNT(*)"
					ElseIf InStr(1, tempXML, "agguniquecount") > 0 Then
						tempStr = "COUNT" & " (DISTINCT " & myArrayP(iIndex) & ")"
					ElseIf InStr(1, tempXML, "aggsum") > 0 Then
						tempStr = "SUM" & " (" & myArrayP(iIndex) & ")"
					ElseIf InStr(1, tempXML, "aggavg") > 0 Then
						tempStr = "AVG" & " (" & myArrayP(iIndex) & ")"
					ElseIf InStr(1, tempXML, "aggmin") > 0 Then
						tempStr = "MIN" & " (" & myArrayP(iIndex) & ")"
					ElseIf InStr(1, tempXML, "aggmax") > 0 Then
						tempStr = "MAX" & " (" & myArrayP(iIndex) & ")"
					Else
						tempStr = myArrayP(iIndex)
					End If
							
					If GlobalConnector = "" Then
						sCriteria = "( " & tempStr & MapOperator(arrOperators(0)) & arrData(0) & " )"
					Else
						sCriteria = "( " & tempStr & MapOperator(arrOperators(0)) & arrData(0) & " " & UCase(GlobalConnector) &  " " & tempStr & MapOperator(arrOperators(1)) & arrData(1) & " )"
					End If
					iCounter = iCounter + 1
				End If
			
				If objSessionStr("ArrayPGlobalCriteria") = "-1" Then
					objSession("ArrayPGlobalCriteria") = iIndex & "~~~~~" & sOperators & "~~~~~" & sData & "~~~~~" & sConnectors &  "~~~~~" & sAttributes & "~~~~~" & sCriteria & "`````"
				Else
					objSession("ArrayPGlobalCriteria") = objSessionStr("ArrayPGlobalCriteria") & "," & iIndex & "~~~~~" & sOperators & "~~~~~" & sData & "~~~~~" & sConnectors &  "~~~~~" & sAttributes & "~~~~~" & sCriteria & "`````"
				End If
			End If
		Next
	End If

	
End Function


Function MapOperatorWithString( p_Operator, p_Data)

	If Trim(p_Operator) = "lt" Then
		MapOperatorWithString = "<'" & p_Data & "'"
	ElseIf Trim(p_Operator) = "le" Then
		MapOperatorWithString = "<='" & p_Data & "'"
	ElseIf Trim(p_Operator) = "eq" Then
		MapOperatorWithString = "='" & p_Data & "'"
	ElseIf Trim(p_Operator) = "ge" Then
		MapOperatorWithString = ">='" & p_Data & "'"
	ElseIf Trim(p_Operator) = "gt" Then
		MapOperatorWithString = ">'" & p_Data & "'"
	ElseIf Trim(p_Operator) = "ne" Then
		MapOperatorWithString = "<>'" & p_Data & "'"
	ElseIf Trim(p_Operator) = "contains" Then
		MapOperatorWithString = " LIKE '%" & p_Data & "%'"
	ElseIf Trim(p_Operator) = "like" Then
		MapOperatorWithString = " LIKE '" & p_Data & "'"
	ElseIf Trim(p_Operator) = "notcontains" Then
		MapOperatorWithString = " NOT LIKE '%" & p_Data & "%'"
	ElseIf Trim(p_Operator) = "notlike" Then
		MapOperatorWithString = " NOT LIKE '" & p_Data & "'"
	ElseIf Trim(p_Operator) = "beginswith" Then
		MapOperatorWithString = " LIKE '" & p_Data & "%'"
	ElseIf Trim(p_Operator) = "notbeginswith" Then '--Ankur Saxena MITS 9315 05/07/2007 - wrong operator was used.
		MapOperatorWithString = " NOT LIKE '" & p_Data & "%'"
	ElseIf Trim(p_Operator) = "endswith" Then
		MapOperatorWithString = " LIKE '%" & p_Data & "'"
	ElseIf Trim(p_Operator) = "notendswith" Then '--Ankur Saxena MITS 9315 05/07/2007 - wrong operator was used.
		MapOperatorWithString = " NOT LIKE '%" & p_Data & "'"
	ElseIf Trim(p_Operator) = "isnull" Then
		MapOperatorWithString = " IS NULL"
	ElseIf Trim(p_Operator) = "isnotnull" Then
		MapOperatorWithString = " IS NOT NULL"
	End If

End Function

Function MapOperator( p_Operator)

	If Trim(p_Operator) = "lt" Then
		MapOperator = "<"
	ElseIf Trim(p_Operator) = "le" Then
		MapOperator = "<="
	ElseIf Trim(p_Operator) = "eq" Then
		MapOperator = "="
	ElseIf Trim(p_Operator) = "ge" Then
		MapOperator = ">="
	ElseIf Trim(p_Operator) = "gt" Then
		MapOperator = ">"
	ElseIf Trim(p_Operator) = "ne" Then
		MapOperator = "<>"
	End If

End Function

Function RefreshSessionVariables()

	If Request.Form("hdSave") <> "-1" And Request.Form("hdSave") <> "" then
		objSession("Save") = Request.Form("hdSave")
	Else
		objSession("Save") = "-1"
	End If
	
	If Request.Form("hdSaveFinal") <> "-1" And Request.Form("hdSaveFinal") <> "" Then
		objSession("SaveFinal") = Request.Form("hdSaveFinal")
	Else
		objSession("SaveFinal") = "0"
	End If

	If Request.Form("hdArrayTF") <> "-1" And Request.Form("hdArrayTF") <> "" Then
		objSession("ArrayTF") = Request.Form("hdArrayTF")
	Else 
		objSession("ArrayTF") = "-1"
	End If
	
	If Request.Form("hdSelFields") <> "-1" And Request.Form("hdSelFields") <> "" Then
		objSession("ArrayP") = Request.Form("hdSelFields")
	Else 
		objSession("ArrayP") = "-1"
	End If
	
	If Request.Form("hdSelFieldsXML") <> "-1" And Request.Form("hdSelFieldsXML") <> "" Then
		objSession("ArrayPXML") = Request.Form("hdSelFieldsXML")
	Else
		objSession("ArrayPXML") = "-1"
	End If
	If Request.Form("hdPCriteria") <> "-1" And Request.Form("hdPCriteria") <> "" Then
		objSession("ArrayPCriteria") = Request.Form("hdPCriteria")
	Else
		objSession("ArrayPCriteria") = "-1"
	End If	
	If Request.Form("hdPOLECriteria") <> "-1" And Request.Form("hdPOLECriteria") <> "" Then
		objSession("ArrayPOLECriteria") = Request.Form("hdPOLECriteria")
	Else
		objSession("ArrayPOLECriteria") = "-1"
	End If	
	If Request.Form("hdDateFormula") <> "-1" And Request.Form("hdDateFormula") <> "" Then
		objSession("ArrayDateFormula") = Request.Form("hdDateFormula")
	Else
		objSession("ArrayDateFormula") = "-1"
	End If	
	If Request.Form("hdPGlobalCriteria") <> "-1" And Request.Form("hdPGlobalCriteria") <> "" Then
		objSession("ArrayPGlobalCriteria") = Request.Form("hdPGlobalCriteria")
	Else
		objSession("ArrayPGlobalCriteria") = "-1"
	End If

	If Request.Form("hdDerError") <> "-1" And Request.Form("hdDerError") <> "" Then
		sError = Request.Form("hdDerError")
	Else
		sError = ""
	End If
		
End Function

Function GetFieldNames(ByRef pArrFields, ByRef pArrFieldNames, ByRef pArrFieldType, ByRef pArrDBTableID, ByRef pArrFieldID) 
	'-- arrFields contains field name and id seperated by | 
	'-- Extract only field names from arrFields in sorted_arrFields 

	If IsValidArray(pArrFields) Then 
		'-- Extract field names 
		For iCount = 0 To UBound(pArrFields) 
			ReDim Preserve pArrFieldNames(iCount)
			ReDim Preserve pArrFieldType(iCount)
			ReDim Preserve pArrDBTableID(iCount)
			ReDim Preserve pArrFieldID(iCount)

			temp = Split(pArrFields(iCount), "|")
			pArrFieldNames(iCount) = temp(0)
			pArrFieldType(iCount) = temp(1)
			pArrDBTableID(iCount) = temp(2) 
			pArrFieldID(iCount) = temp(3)
		Next
	End IF 
End Function

%>