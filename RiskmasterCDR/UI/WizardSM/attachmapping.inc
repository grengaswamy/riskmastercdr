<script language="VBScript" runat="server">
' Author: Denis Basaric, 11/2000

' Maps form name to the table name that is used for attachments
Public Function MapFormName(ByVal sFormName)
	MapFormName=""
	sFormName=LCase(Trim(sFormName))
	If sFormName="" Then
		Exit Function
	End If
	If sFormName = "claimgc" or sFormName="claimva" or sFormName="claimwc" Or sFormName="claimdi" Or sFormName="claim" Then
      ' Claim object
      MapFormName = "CLAIM"
   'tkr 4/2003 conform to rmworld
   ElseIf sFormName = "piemployee" Or sFormName = "pimedstaff" Or sFormName = "piother" Or sFormName = "pipatient" Or sFormName="piqpatient" Or sFormName = "piphysician" Or sFormName = "piwitness" Then
	  MapFormName="PERSON_INVOLVED"
   ElseIf sFormName = "funds" Then
      ' Funds
      MapFormName = "FUNDS"
   ElseIf sFormName = "deposit" Then
      ' Funds
      MapFormName = "FUNDS_DEPOSIT"
   ElseIf sFormName = "adjuster" Then
      ' Claim Adjuster
      MapFormName = "CLAIM_ADJUSTER"
   ElseIf sFormName = "adjusterdatedtext" Then
      ' Adjuster Dated Text
      MapFormName = "ADJUST_DATED_TEXT"
   ElseIf sFormName = "claimant" Then
      ' Claimant
      MapFormName = "CLAIMANT"
   ElseIf sFormName = "defendant" Then
      ' Defendant
      MapFormName = "DEFENDANT"
   ElseIf sFormName = "expert" Then
      ' Expert
      MapFormName = "EXPERT"
   ElseIf sFormName = "litigation" Then
      ' Claim Litigation
      MapFormName = "CLAIM_X_LITIGATION"
   ElseIf sFormName = "unit" Then
      ' Claim Unit Involved
      MapFormName = "UNIT_X_CLAIM"
   ElseIf sFormName = "vehicle" Then
      ' Vehicle
      MapFormName = "VEHICLE"
   ElseIf sFormName = "event" Then
      ' Event
      MapFormName = "EVENT"
   ElseIf sFormName = "entity" or sFormName = "people" or sFormName="entitymaint" Then
      ' Entity
      MapFormName = "ENTITY"
   ElseIf sFormName = "employee" Then
      ' Employee
      MapFormName = "EMPLOYEE"
   ElseIf sFormName = "policy" Then
      ' Policy
      MapFormName = "POLICY"
   ElseIf sFormName = "patient" Then
      ' Patient
      MapFormName = "PATIENT"
   ElseIf sFormName = "physician" Then
      ' Physician
      MapFormName = "PHYSICIAN"
   ElseIf sFormName = "medicalstaff" or sFormName = "staff" Then
      ' Medical Staff
      MapFormName = "MED_STAFF"
   ElseIf sFormName = "concomitant" Then
      ' Concom Product
      MapFormName = "EV_X_CONCOM_PROD"
   ElseIf sFormName = "dependent" Then
      ' Dependent
      MapFormName = "EMP_X_DEPENDENT"
   ElseIf sFormName = "pidependent" Then
      ' PI Dependent
      MapFormName = "PI_X_DEPENDENT"
   ElseIf sFormName = "eventdatedtext" Then
      ' Event Dated text
      MapFormName = "EVENT_X_DATED_TEXT"
   ElseIf sFormName = "fallinfo" Then
      ' Fall Indicator
      MapFormName = "FALL_INDICATOR"
   ElseIf sFormName = "piworkloss" Then
      ' Pi X Work Loss
      MapFormName = "PI_X_WORK_LOSS"
   ElseIf sFormName = "medwatch" Then
      ' Medwatch
      MapFormName = "EVENT_X_MEDWATCH"
   ElseIf sFormName = "medwatchtest" Then
      ' Medwatch Test
      MapFormName = "EVENT_X_MEDW_TEST"
   ElseIf sFormName = "osha" Then
      ' Event X OSHA
      MapFormName = "EVENT_X_OSHA"
   ElseIf sFormName = "policycoverage" Then
      ' Policy Coverage
      MapFormName = "POLICY_X_CVG_TYPE"
   ElseIf sFormName = "pirestriction" Then
      ' Pi X Restricted Days
      MapFormName = "PI_X_RESTRICT"
   ElseIf sFormName = "admtable" Then
      'MapFormName = frmA.ADMTable.TableName
   ElseIf Request("at")="1" Then
   	MapFormName=sFormName
   End If
End Function

' Maps table name to the form name
Public Function MapTableName(sTableName, ByRef sPrimaryId)

	sTableName=UCase(sTableName)
	MapTableName=""
	If sTableName="" Then
		Exit Function
	End If
	If sTableName = "CLAIM" Then
      ' Claim object
      MapTableName = "claim"
      sPrimaryId="claimid"
   ElseIf sTableName = "FUNDS" Then
      ' Funds
      MapTableName = "funds"
      sPrimaryId="transid"
   ElseIf sTableName = "FUNDS_DEPOSIT" Then
      ' Funds
      MapTableName = "deposit"
      sPrimaryId="depositid"
   ElseIf sTableName = "CLAIM_ADJUSTER" Then
      ' Claim Adjuster
      MapTableName = "adjuster"
      sPrimaryId="adjrowid"
   ElseIf sTableName = "ADJUST_DATED_TEXT" Then
      ' Adjuster Dated Text
      MapTableName = "adjusterdatedtext"
      sPrimaryId="adjdttextrowid"
   ElseIf sTableName = "CLAIMANT" Then
      ' Claimant
      MapTableName = "claimant"
      sPrimaryId="clmntrowid"
   ElseIf sTableName = "DEFENDANT" Then
      ' Defendant
      MapTableName = "defendant"
      sPrimaryId="defendantrowid"
   ElseIf sTableName = "EXPERT" Then
      ' Expert
      MapTableName = "expert"
      sPrimaryId="expertrowid"
   ElseIf sTableName = "CLAIM_X_LITIGATION" Then
      ' Claim Litigation
      MapTableName = "litigation"
      sPrimaryId="litigationrowid"
   ElseIf sTableName = "UNIT_X_CLAIM" Then
      ' Claim Unit Involved
      MapTableName = "unit"
      sPrimaryId="unitrowid"
   ElseIf sTableName = "VEHICLE" Then
      ' Vehicle
      MapTableName = "vehicle"
      sPrimaryId="unitid"
   ElseIf sTableName = "EVENT" Then
      ' Event
      MapTableName = "event"
      sPrimaryId="eventid"
   ElseIf sTableName = "ENTITY" Then
      ' Entity
      MapTableName = "entity"
      sPrimaryId="entityid"
   ElseIf sTableName = "EMPLOYEE" Then
      ' Employee
      MapTableName = "employee"
      sPrimaryId="employeeeid"
   ElseIf sTableName = "POLICY" Then
      ' Policy
      MapTableName = "policy"
      sPrimaryId="policyid"
   ElseIf sTableName = "PATIENT" Then
      ' Patient
      MapTableName = "patient"
      sPrimaryId="patientid"
   ElseIf sTableName = "PHYSICIAN" Then
      ' Physician
      MapTableName = "physician"
      sPrimaryId="physeid"
   ElseIf sTableName = "MED_STAFF" Then
      ' Medical Staff
      MapTableName = "staff"
      sPrimaryId="staffeid"
   ElseIf sTableName = "EV_X_CONCOM_PROD" Then
      ' Concom Product
      MapTableName = "concomitant"
      sPrimaryId="evconcomrowid"
   ElseIf sTableName = "EMP_X_DEPENDENT" Then
      ' Dependent
      MapTableName = "dependent"
      sPrimaryId="empdeprowid"
   ElseIf sTableName = "PI_X_DEPENDENT" Then
      ' Dependent
      MapTableName = "pidependent"
      sPrimaryId="pideprowid"
   ElseIf sTableName = "EVENT_X_DATED_TEXT" Then
      ' Event Dated text
      MapTableName = "eventdatedtext"
      sPrimaryId="evdtrowid"
   ElseIf sTableName = "FALL_INDICATOR" Then
      ' Fall Indicator
      MapTableName = "fallinfo"
      sPrimaryId="eventid"
   ElseIf sTableName = "PI_X_WORK_LOSS" Then   	
      ' Pi X Work Loss
      MapTableName = "piworkloss"
      sPrimaryId="piwlrowid"
   ElseIf sTableName = "EVENT_X_MEDWATCH" Then
      ' Medwatch
      MapTableName = "medwatch"
      sPrimaryId="eventid"
   ElseIf sTableName = "EVENT_X_MEDW_TEST" Then
      ' Medwatch Test
      MapTableName = "medwatchtest"
      sPrimaryId="evmwtestrowid"
   ElseIf sTableName = "EVENT_X_OSHA" Then
      ' Event X OSHA
      MapTableName = "osha"
      sPrimaryId="eventid"
   ElseIf sTableName = "POLICY_X_CVG_TYPE" Then
      ' Policy Coverage
      MapTableName = "policycoverage"
      sPrimaryId="polcvgrowid"
   ElseIf sTableName = "PI_X_RESTRICT" Then
      ' Pi X Restricted Days
      MapTableName = "pirestriction"
      sPrimaryId="pirestrictrowid"
   ElseIf sTableName = "EMPLOYEE_INVOLVED" Then
      ' Employee Involved
      MapTableName = "piemployee"
      sPrimaryId="pirowid"
   ElseIf sTableName = "OTHER_INVOLVED" Then
      ' Other Person Involved
      MapTableName = "piother"
      sPrimaryId="pirowid"
   ElseIf sTableName = "PATIENT_INVOLVED" Then
      ' Old Patient Involved
      MapTableName = "pipatient"
      sPrimaryId="pirowid"
   ElseIf sTableName = "WITNESS_INVOLVED" Then
      ' Witness Involved
      MapTableName = "piwitness"
      sPrimaryId="pirowid"
   ElseIf sTableName = "admtable" Then
      'MapTableName = frmA.ADMTable.TableName
   ElseIf sTableName = "PERSON_INVOLVED" Then   'tkr 4/2003 rmworld uses this for any person involved
	  MapTableName = Request("pixmlformname")
	  sPrimaryId="pirowid"
   ElseIf Request("at")="1" Then
   	MapTableName=sTableName
   	sPrimaryId=sTableName & "_id"
   End If
End Function
</script>