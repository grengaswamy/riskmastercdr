<script language="VBScript" runat="server">
' Author: Denis Basaric, 08/2000

' Variables Stored in Session object
Public Const SESSION_DSN="DSN"
Public Const SESSION_DSNID="DSNID"
Public Const SESSION_DOCPATH="DocPath"
Public Const SESSION_DOCPATHTYPE="DocPathType"
Public Const SESSION_SEARCHRESULTPATH="SearchResultsPath"
Public Const SESSION_CODESPATH="CodesPath"
Public Const SESSION_LOGINNAME="LoginName"
Public Const SESSION_USERID="UserID"
Public Const SESSION_GROUPID="GroupID"
Public Const SESSION_LASTNAME="LastName"
Public Const SESSION_FIRSTNAME="FirstName"
Public Const SESSION_TITLE="Title"
Public Const SESSION_PHONE1="Phone1"
Public Const SESSION_EMAIL="EMail"
Public Const SESSION_HOMEPATH="HomePath"
Public Const SESSION_AUTHENTICATED="Authenticated"
Public Const SESSION_ORGSECURITY="OrgSecurity"
Public Const SESSION_ORGNEEDREFRESH="OrgNeedRefresh"		' When Org. Security is enabled after user logs in we have to recreate the org. hierarchy html file on first request
Public Const SESSION_ADMINRIGHTS="AdminRights"
Public Const SESSION_WPAREFRESHED="WpaRefreshed"
Public Const SESSION_WPAHISTREFRESHED="WpaHistRefreshed"
Public Const SESSION_MODULESECURITY="ModuleSecurity"
Public Const SESSION_DBNAME="DBName"
Public Const SESSION_HIDEDOCEMAILTOUSERS="HideDocEmailTo"
Public Const SESSION_QUICKENTRY="QuickEntry"
Public Const SESSION_QUICKENTRYVIEWNAME="QuickEntryViewName"
Public Const SESSION_QUICKENTRYHOMEPAGE="QuickEntryHomePage"
Public Const SESSION_QUICKENTRYPAGEMENU="QuickEntryPageMenu"
Public Const SESSION_QUEST="Quest"
Public Const SESSION_FROIBYPI="FroiByPI"
Public Const SESSION_SID="SessionId"
public Const SESSION_USERSBYGROUP="usersbygroup"
'**********ravi on 15 sep 04**************88
'**********to store LOB in session that we need in getcode.asp page
'**********to filter out trans type according to claim*******88
'***********there was no other option except this thisng**********8
Public Const SESSION_LOB="LOB"

'******************end************************
Public Const SESSION_EVENT_TYPE_LIMITS="EventTypeLimits"
Public Const SESSION_CLAIM_TYPE_LIMITS="ClaimTypeLimits"

'* Aditya -- 07/21/2004 -- Integration of Domain Authentication
Public Const SESSION_DOMAIN_AUTHENTICATED_USER="DomainAuthenticatedUser"
'** Aditya -- 07/21/2004 -- Integration of Domain Authentication

Public Const SESSION_NCCINSTALLED="NCCClaimsInstalled"

' Variables Stored in Application Object
Public Const APP_SECURITYDSN="SECURITY_DSN"
Public Const APP_SESSIONDSN="SESSION_DSN"
Public Const APP_SMDSN="SM_DSN"
Public Const APP_SMDDSN="SMD_DSN"
Public Const APP_SMTPSERVER="SMTP_SERVER"
Public Const APP_DATAPATH="APP_DATA_PATH"
Public Const APP_PATH="APP_PATH"
Public Const APP_BINARIESPATH="BINARIES_PATH"
Public Const APP_RISKSERVERNAME="RISK_SERVER_NAME"
Public Const APP_USERDIRECTORYHOME="USER_DIRECTORY_HOME"

Public Const APP_PERMCACHE="permissions"
Public Const APP_PERMLASTCHANGED="PermLastChanged"

Public Const APP_CUSTOMDICTIONARY="CustomDict"

Public Const APP_LASTCACHEDELETE="LastCacheDelete"
Public Const APP_APPSTART="AppStart"

Public Const RMO_ACCESS = 0
Public Const RMO_VIEW = 1
Public Const RMO_UPDATE = 2
Public Const RMO_CREATE = 3
Public Const RMO_DELETE = 4
Public Const RMO_IMPORT = 5
Public Const RMO_ATTACHACCESS = 6

Public Const USE_DCOM=False
Public Const APP_DCOM_SERVER="DCOM_SERVER"
Public Const APP_MAX_WAIT = 600
Public Const CSCStoreFileSystem="0"
Public Const CSCStoreDatabase="1"

Public Const DBMS_IS_ACCESS = 0
Public Const DBMS_IS_SQLSRVR = 1
Public Const DBMS_IS_SYBASE = 2
Public Const DBMS_IS_INFORMIX = 3
Public Const DBMS_IS_ORACLE = 4
Public Const DBMS_IS_ODBC = 5
Public Const DBMS_IS_DB2 = 6

'* Aditya -- 07/21/2004 -- Integration of Domain Authentication
Public Const DOMAIN_NOT_ENABLED_FOR_RM = 0
Public Const DOMAIN_ENABLED_FOR_RM = 1

Public Const NO_PASSWORD_FLAG = 1
Public Const NO_DSN_FLAG = 2
'** Aditya -- 07/21/2004 -- Integration of Domain Authentication

'*AP -- 12/31/2004 -- Add/Edit Org hierarchy
Public Const GLOSSARY_TYPE_CODE_ORGANIZATION = 5
'* End

</script>