<script language="VBScript" runat="server">
Public Const CUST_NAME=""

Public Const APP_TITLE ="Riskmaster"
Public Const DEFAULT_APPTITLE = "Riskmaster"

Public Const APP_COPYRIGHT ="� 2005 by CSC, All Rights Reserved."
Public Const DEFAULT_APPCOPYRIGHT ="&copy 2005 by CSC, All Rights Reserved."

Public Const REPORTAPP_TITLE ="Sortmaster"
Public Const DEFAULT_REPORTAPPTITLE = "Sortmaster"

Public Const ERR_CONTACT ="For assistance please consult the System Administrator."
Public Const DEFAULT_ERRCONTACT = "For assistance please consult the System Administrator."

Public Const OVERRIDE_DOCPATH=""

Public Const EXISTREC_ALERT ="You are working on a new record and this functionality is available for existing records only. Please save the data and try again."      'comment by tkr
Public Const DEFAULT_EXISTRECALERT = "You are working on a new record and this functionality is available for existing records only. Please save the data and try again."

Public Const TEXTML_COLS="30"
Public Const TEXTML_ROWS="5"
Public Const DEFAULT_TEXTMLCOLS=30
Public Const DEFAULT_TEXTMLROWS=5

Public Const FREECODE_COLS="30"
Public Const FREECODE_ROWS="8"
Public Const DEFAULT_FREECODECOLS=30
Public Const DEFAULT_FREECODEROWS=8

Public Const READONLYMEMO_COLS="30"
Public Const READONLYMEMO_ROWS="5"
Public Const DEFAULT_READONLYMEMOCOLS=30
Public Const DEFAULT_READONLYMEMOROWS=5

Public Const MEMO_COLS="30"
Public Const MEMO_ROWS="5"
Public Const DEFAULT_MEMOCOLS=30
Public Const DEFAULT_MEMOROWS=5

'all boolean settings = "-1" or "0"
Public Const bSOUNDEX="0"
Public Const bSHOW_NAME="0"
Public Const bSHOW_LOGIN="-1"
Public Const bSHOW_ADMINPAGE="-1"
Public Const bSHOW_DOCUMENTS="-1"
Public Const bSHOW_SEARCH="-1"
Public Const bSHOW_FILES="-1"
Public Const bSHOW_DIARIES="-1"
Public Const bSHOW_REPORTS="-1"
Public Const bSHOW_LOGOUT="-1"
Public Const bSHOW_HELP="-1"
Public Const bSEARCH_CLAIMS="-1"
Public Const bSEARCH_EVENTS="-1"
Public Const bSEARCH_EMP="-1"
Public Const bSEARCH_ENT="-1"
Public Const bSEARCH_VEH="-1"
Public Const bSEARCH_POL="-1"
Public Const bSEARCH_FUNDS="-1"
Public Const bSEARCH_PAT="-1"
Public Const bSEARCH_PHY="-1"
Public Const bADD_PAYM="-1"
Public Const bADD_COLL="-1"
Public Const bOFAC_CHECK="-1"
</script>