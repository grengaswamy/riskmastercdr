<% 'Option Explicit %>
<!-- #include file ="session.inc" -->
<!-- #include file ="generic.asp" -->
<html>
	<head>
		<title>Graph Options</title>
		<link rel="stylesheet" href="RMNet.css" type="text/css" />
<%
Dim objLocal, arrFore(), arrBack(), arrType(), arrStyle(), arrPerspective(), pageError
Dim sGraphType, sGraphStyle, i, sForeGround, sBackGround, sGraphTitle, sLeftTitle
Dim sGraphStyleIndex, sSummary, sDetail, sVertical, sHorizontal, sGridStyle
Dim sLabels, sXaxis, sYaxis, sBottomTitle, sPerspective

Dim sGlobalScript

If Not GetValidObject("InitSMList.CLocalize", objLocal) Then
	Call ShowError(pageError, "Call to CLocalize class failed.", True)
End If

objLocal.GetComboItems arrFore, "IDD_GRAPH", "IDC_CBCOLORFORE", "0|" & Application(APP_DATAPATH)

Dim mFore, mError

mFore = IsValidArray(arrFore)

If Not mFore Then
	Call ShowError(pageError, "No foreground colors are available.", False)
End If

objLocal.GetComboItems arrBack, "IDD_GRAPH", "IDC_CBCOLORBACK", "0|" & Application(APP_DATAPATH)

Dim mBack

mBack = IsValidArray(arrBack)

If Not mBack Then
	Call ShowError(pageError, "No background colors are available.", False)
End If

objLocal.GetComboItems arrPerspective, "IDD_GRAPH", "IDC_CMBPER", "0|" & Application(APP_DATAPATH)

Dim mPerspective

mPerspective = IsValidArray(arrPerspective)
If Not mPerspective Then
	Call ShowError(pageError, "No perspective options are available.", False)
End If

objLocal.GetGraphItems arrType, arrStyle, "IDD_GRAPH", "IDC_GPH_TYPE", "0|" & Application(APP_DATAPATH)

mType = IsValidArray(arrType)

If Not mType Then
	Call ShowError(pageError, "No graph types are available.", False)
End If

If IsValidArray(arrStyle) Then
	For i = 0 To UBound(arrStyle) 
		If i = 0 Then 
			sGraphStyle = arrStyle(0) 
		Else 
			sGraphStyle = sGraphStyle & "," & arrStyle(i) 
		End If 
	Next
End If

Dim rptName, rptDesc

'-- Create an object of MSXML Dom Document
Dim xmlDoc 

If Not GetValidObject("Msxml2.DOMDocument", xmlDoc) Then
	Call ShowError(pageError, "XML Parser is not available.", True)
End If

xmlDoc.async = false

Dim sXML, objData

'^ Rajeev -- Retrieve report from database
'-- User opens an existing report or continues using saved report
If objSessionStr("REPORTID") <> "-1" And objSessionStr("Save") = "1" And objSessionStr("SaveFinal") = "1" Then

	If Not GetValidObject("InitSMList.CDataManager", objData) Then
		Call ShowError(pageError, "Call to CDataManager class failed.", True)
	End If

	objData.m_RMConnectStr = Application("SM_DSN")
	objData.ReportID = objSessionStr("REPORTID")
	objData.GetReport

	sXML = objData.XML
		
	xmlDoc.loadXML sXML

	Set objData = Nothing
	
	'-- Parse XML for populating on GUI
	
	Call DisplayGraphOptions()
	
End If

'-- User continues tabbing but has not clicked on Save yet
If objSessionStr("REPORTID") <> "-1"  And objSessionStr("Save") = "0" And objSessionStr("SaveFinal") = "0" Then
	
	If Not GetValidObject("InitSMList.CDataManager", objData) Then
		Call ShowError(pageError, "Call to CDataManager class failed.", True)
	End If
	objData.m_RMConnectStr = Application("SM_DSN")
	objData.TempReportID = objSessionStr("REPORTID")
	objData.GetTempReport

	sXML = objData.XML
	xmlDoc.loadXML sXML

	Set objData = Nothing
	'-- Parse XML for populating on GUI
	Call DisplayGraphOptions()

End If
'^^ Rajeev 06/05/2003 -- Retrieve report from database	

If LCase(Request.ServerVariables("REQUEST_METHOD")) = "post" Then

	Call SubmitReport()

End If
%> 
<html>
	<head>
		<title>Graph Options</title>
		<link rel="stylesheet" href="RMNet.css" type="text/css" />
		<script language="JavaScript" src="SMD.js"></script>
		<script language="JavaScript">
		
			function window_onLoad()
			{
				//-- Handle the index of GraphType
				if (document.frmData.cboGraphType.selectedIndex == -1)
				{
					document.frmData.cboGraphType.selectedIndex = 0;
					window.document.frmData.hdGraphType.value = 0;
				}
				else
				{
					document.frmData.cboGraphType.selectedIndex = document.frmData.hdGraphType.value;
				}
				
				if ((document.frmData.optGraphData[0].checked == false) && (document.frmData.optGraphData[1].checked == false))
					document.frmData.optGraphData[0].checked = true;

				//-- Handle the index of Graph type & style
				var arrGraphStyle = document.frmData.hdGraphStyle.value.split(",");
				var arrGraphType = new Array;
				
				for(i=0,n=0;i<arrGraphStyle.length;i++)
				{
					var tempArray;
					tempArray = arrGraphStyle[i].split("|")
					selIndex = arrGraphStyle[i].substring(0,arrGraphStyle[i].indexOf("|"));
					
					if(parseInt(selIndex) == document.frmData.cboGraphType.selectedIndex)
					{
						arrGraphType[n] = tempArray[1];
						n = n + 1;
					}
				}			
				
				if(window.document.frmData.cboGraphStyle.length > 0)
					window.document.frmData.cboGraphStyle.length = 0;
				
				for(i=0;i<arrGraphType.length;i++)
				{
					var sNewItem = window.document.createElement("OPTION");
					sNewItem.text = arrGraphType[i];//"ABC";
					window.document.frmData.cboGraphStyle.add(sNewItem);
				}
				
				//-- Handle the index of GraphStyle
				if (document.frmData.cboGraphStyle.selectedIndex == -1)
				{
					document.frmData.cboGraphStyle.selectedIndex = 0;
					window.document.frmData.hdGraphStyleIndex.value = 0;
				}
				else
					document.frmData.cboGraphStyle.selectedIndex = document.frmData.hdGraphStyleIndex.value;
					
				//-- Handle the index of Foreground & Background
				if(document.frmData.hdForeGround.value != '')
					document.frmData.cboForegnd.selectedIndex = document.frmData.hdForeGround.value;
				else
					document.frmData.cboForegnd.selectedIndex =  document.frmData.cboForegnd.length - 1;
				
				if(document.frmData.hdBackGround.value != '')
					document.frmData.cboBackgnd.selectedIndex = document.frmData.hdBackGround.value;
				else
					document.frmData.cboBackgnd.selectedIndex =  document.frmData.cboBackgnd.length - 1;
		
				//-- Handle Perspective
				if(document.frmData.hdPerspective.value != '')
					document.frmData.cboPerspective.selectedIndex = document.frmData.hdPerspective.value;
				else
					document.frmData.cboPerspective.selectedIndex =  0;
					
				if(window.document.frmData.cboGraphType.selectedIndex == 4)
				{
					window.document.frmData.cboPerspective.disabled = false;
					if(window.document.frmData.cboPerspective.selectedIndex == 0)
					{
						window.document.frmData.cboPerspective.selectedIndex = 5;
						window.document.frmData.hdPerspective.value = document.frmData.cboPerspective.selectedIndex;
					}
				}

				 changeGraphData();
			}
		
			function changeGraphType()
			{
				//-- For saving value in XML
				window.document.frmData.hdGraphType.value = document.frmData.cboGraphType.selectedIndex;
				
				if(document.frmData.cboGraphType.selectedIndex > 0)
				{
					document.frmData.chkXaxis.checked = true;
					document.frmData.chkYaxis.checked = true;
					changeLabels();
				}
				else if (document.frmData.cboGraphType.selectedIndex == 0)
				{
					document.frmData.chkXaxis.checked = false;
					document.frmData.chkYaxis.checked = false;
				}
				
				var arrGraphStyle = document.frmData.hdGraphStyle.value.split(",");
				var arrGraphType = new Array;
				
				for(i=0,n=0;i<arrGraphStyle.length;i++)
				{
					var tempArray;
					tempArray = arrGraphStyle[i].split("|")
					selIndex = arrGraphStyle[i].substring(0,arrGraphStyle[i].indexOf("|"));
					
					if(parseInt(selIndex) == document.frmData.cboGraphType.selectedIndex)
					{
						arrGraphType[n] = tempArray[1];
						n = n + 1;
					}
				}			
				
				if(window.document.frmData.cboGraphStyle.length > 0)
					window.document.frmData.cboGraphStyle.length = 0;
				
				for(i=0;i<arrGraphType.length;i++)
				{
					var sNewItem = window.document.createElement("OPTION");
					sNewItem.text = arrGraphType[i];//"ABC";
					window.document.frmData.cboGraphStyle.add(sNewItem);
				}
				
				if(i == arrGraphType.length)
					window.document.frmData.hdGraphStyleIndex.value = document.frmData.cboGraphStyle.selectedIndex;
			}
		
			function changeGraphData()
			{
				//-- For saving value in XML
				if (document.frmData.cboGraphStyle.selectedIndex == -1)
					window.document.frmData.hdGraphStyleIndex.value = 0;
				else
					window.document.frmData.hdGraphStyleIndex.value = document.frmData.cboGraphStyle.selectedIndex;
				
				if (window.document.frmData.cboGraphType.selectedIndex == 0)
				{
					window.document.frmData.optGraphData[1].disabled = false;			
					window.document.frmData.optGraphData[0].disabled = false;			
					window.document.frmData.optGraphData[0].checked = true;
				}
				
				if (window.document.frmData.cboGraphType.selectedIndex == 1 || window.document.frmData.cboGraphType.selectedIndex == 2)
				{
					window.document.frmData.optGraphData[1].disabled = true;			
					window.document.frmData.optGraphData[0].disabled = false;			
					window.document.frmData.optGraphData[0].checked = true;
				}
				
				if (window.document.frmData.cboGraphType.selectedIndex == 3 || window.document.frmData.cboGraphType.selectedIndex == 4)
				{
					if(window.document.frmData.cboGraphType.selectedIndex == 4)
					{
						window.document.frmData.cboPerspective.disabled = false;
						if (window.document.frmData.cboPerspective.selectedIndex != 5)
						{
							window.document.frmData.cboPerspective.selectedIndex = 5;
							window.document.frmData.hdPerspective.value = document.frmData.cboPerspective.selectedIndex;
						}
					}
						
					if(window.document.frmData.cboGraphStyle.selectedIndex <= 1)
					{
						window.document.frmData.optGraphData[1].disabled = true;			
						window.document.frmData.optGraphData[0].disabled = false;			
						window.document.frmData.optGraphData[0].checked = true;
					}
					else
					{
						window.document.frmData.optGraphData[1].disabled = false;			
						window.document.frmData.optGraphData[0].disabled = true;			
						window.document.frmData.optGraphData[1].checked = true;
					}
				}
				
				if (window.document.frmData.cboGraphType.selectedIndex == 5)
				{
					window.document.frmData.optGraphData[1].disabled = false;			
					window.document.frmData.optGraphData[0].disabled = true;			
					window.document.frmData.optGraphData[1].checked = true;
				}
				
				if (window.document.frmData.cboGraphType.selectedIndex == 6 || window.document.frmData.cboGraphType.selectedIndex == 7 || window.document.frmData.cboGraphType.selectedIndex == 8)
				{
					window.document.frmData.optGraphData[1].disabled = false;			
					window.document.frmData.optGraphData[0].disabled = false;			
					window.document.frmData.optGraphData[0].checked = true;
				}
				
				if(window.document.frmData.cboGraphType.selectedIndex == 4)
				{
					window.document.frmData.cboPerspective.disabled = false;
					if (window.document.frmData.cboPerspective.selectedIndex == 0)
					{
						window.document.frmData.cboPerspective.selectedIndex = 5;
						window.document.frmData.hdPerspective.value = document.frmData.cboPerspective.selectedIndex;
					}
				}
				else
					window.document.frmData.cboPerspective.disabled = true;
		
			}
		
			function changeGridStyle()
			{
				if(window.document.frmData.chkHorizontal.checked == true && window.document.frmData.chkVertical.checked == false)
					window.document.frmData.hdGridStyle.value = 1;
				else if(window.document.frmData.chkHorizontal.checked == false && window.document.frmData.chkVertical.checked == true)
					window.document.frmData.hdGridStyle.value = 2;
				else if(window.document.frmData.chkHorizontal.checked == true && window.document.frmData.chkVertical.checked == true)
					window.document.frmData.hdGridStyle.value = 3;
				else 
					window.document.frmData.hdGridStyle.value = 0;
			}
		
			function changeLabels()
			{
				if(window.document.frmData.chkXaxis.checked == true && window.document.frmData.chkYaxis.checked == false)
					window.document.frmData.hdLabels.value = 1;
				else if(window.document.frmData.chkXaxis.checked == false && window.document.frmData.chkYaxis.checked == true)
					window.document.frmData.hdLabels.value = 2;
				else if(window.document.frmData.chkXaxis.checked == true && window.document.frmData.chkYaxis.checked == true)
					window.document.frmData.hdLabels.value = 3;
				else 
					window.document.frmData.hdLabels.value = 0;
			}
		
			function changeForeGround()
			{
				window.document.frmData.hdForeGround.value = document.frmData.cboForegnd.selectedIndex;
			}

			function changeBackGround()
			{
				window.document.frmData.hdBackGround.value = document.frmData.cboBackgnd.selectedIndex;
			}
		
			function changePerspective()
			{
				window.document.frmData.hdPerspective.value = document.frmData.cboPerspective.selectedIndex;
			}
		
			function openDerivedField2(pText,pBtnClick,pTabID)
			{
				window.document.frmData.hdDerFieldName.value = pText;
				window.open('derivedfields_2.asp?usercolname='+pText+'&UpdateMode='+pBtnClick+'&TABID='+pTabID,'der2','resizable=yes,Width=575,Height=465,top='+(screen.availHeight-465)/2+',left='+(screen.availWidth-575)/2+'');
			}	
	
			function editDerivedField2(pText,pBtnClick,pTabID)
			{
				window.document.frmData.hdDerFieldName.value = pText;
				window.open('derivedfields_2.asp?usercolname='+pText+'&UpdateMode='+pBtnClick+'&TABID='+pTabID,'der2','resizable=yes,Width=575,Height=465,top='+(screen.availHeight-465)/2+',left='+(screen.availWidth-575)/2+'');
			}	
		

			function derived_click()
			{
				if(document.frmData.hdSelFields.value != -1)
					openDerivedField1(true, 3);
				else
					openDerivedField1(false, 3);
			}

			function openGScript(pTabID)
			{
				window.open('globalscript.asp?TABID='+pTabID,'glob','resizable=yes,Width=500,Height=225,top='+(screen.availHeight-225)/2+',left='+(screen.availWidth-500)/2+'');				
			}			

		</script>
	</head>
	
	<body class="10pt" leftmargin="0" onLoad="pageLoaded();window_onLoad();">
	<form name="frmData" method="post" topmargin="0" bottommargin="0">
	<%If Request("ERROR") <> "" Then
		Response.Write "<center><b><font color=red>Error saving Derived Field: " & Request("ERROR") & "</font></b></center>"
	End If
	%>
	
	<%If Request("MSG") <> "" Then
		Response.Write Request("MSG")
	End If
	%>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
			<tr height="4%">
				<td>
					<table border="0" class="toolbar" height="90%" cellpadding="0" cellspacing="0">
						<tr>
							<td align="center" valign="middle" height="32" width="28"><a class="LightBold" href="#" onClick="newReport();return false;"><img src="img/new.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/new2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/new.gif';this.style.zoom='100%'" title="New" /></a></td>
							<td align="center" valign="middle" height="32" width="28"><a class="LightBold" href="#" onClick= "uploadReport();return false;"> <img src="img/open.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/open2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/open.gif';this.style.zoom='100%'" title="Open"/></a></td>
							<td align="center" valign="middle" height="32" width="28"><a class="LightBold" href="#" onClick="saveReport(3,0);return false;"><img src="img/smwd_save.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/smwd_save2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/smwd_save.gif';this.style.zoom='100%'" title="Save" /></a></td>
							<td align="center" valign="middle" height="32" width="28"><a class="LightBold" href="#" onClick="saveReport(3,1);return false;"><img src="img/savepost.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/savepost2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/savepost.gif';this.style.zoom='100%'" title="Save and Post" /></a></td>
							<td align="center" valign="middle" height="32" width="28"><a class="LightBold" href="#" onClick="openPreview('pdf');saveReport(3,2);return false;"><img src="img/adobe_prev1.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/adobe_prev2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/adobe_prev1.gif';this.style.zoom='100%'"  title="Preview in PDF" /></a></td>
							<td align="center" valign="middle" height="32" width="28"><a class="LightBold" href="#" onClick="openPreview('xls');saveReport(3,2);return false;"><img src="img/excel_prev1.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/excel_prev2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/excel_prev1.gif';this.style.zoom='100%'"  title="Preview in XLS" /></a></td>
							<td align="center" valign="middle" height="32" width="28"><a class="LightBold" href="#" onClick="derived_click();return false;"><img src="img/derfield.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/derfield2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/derfield.gif';this.style.zoom='100%'" title="Derived Fields" /></a></td>
							<td align="center" valign="middle" height="32" width="28"><a class="LightBold" href="#" onClick="openRelDate(3);return false;"><img src="img/rel_date.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/rel_date2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/rel_date.gif';this.style.zoom='100%'" title="Relative Date" /></a></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr height="6%">
				<td>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td id="tool_rptfields" width="24%" class="NotSelected"><a class="NotSelected1" href="javascript:tabNavigation(0,3)"><span style="{text-decoration:none}">Report Fields</span></a></td>
							<td style="border-bottom:none;border-left-style:solid;border-left-color:#999999;border-width:2px;border-top-style:solid;border-top-width:2px;border-top-color:#FFFFFF;">&nbsp;&nbsp;</td>

							<td id="tool_rptcriteria" width="24%" class="NotSelected">
								<a class="NotSelected1" href="javascript:tabNavigation(1,3)"><span style="{text-decoration:none}">Report Criteria</span></a>
							</td>
							<td style="border-bottom:none;border-left-style:solid;border-left-color:#999999;border-width:2px;border-top-style:solid;border-top-width:2px;border-top-color:#FFFFFF;">&nbsp;&nbsp;</td>

							<td id="tool_rptoptions" width="24%" class="NotSelected">
								<a class="NotSelected1" href="javascript:tabNavigation(2,3)"><span style="{text-decoration:none}">Report Options</span></a>
							</td>
							<td style="border-bottom:none;border-left-style:solid;border-left-color:#999999;border-width:2px;border-top-style:solid;border-top-width:2px;border-top-color:#FFFFFF;">&nbsp;&nbsp;</td>

							<td id="tool_graphoptions" width="24%" class="Selected">
								<a class="Selected" href="#"><span style="{text-decoration:none}">Graph Options</span></a>
							</td>
							<td style="border-bottom:none;border-left-style:solid;border-left-color:#999999;border-width:2px;border-top-style:solid;border-top-width:2px;border-top-color:#FFFFFF;">&nbsp;&nbsp;</td>

							<!--td id="tool_hier" width="25%" class="NotSelected">
								<a class="NotSelected1" href1="javascript:tabNavigation('4')"><span style="{text-decoration:none}">Column Hierarchy</span></a>
							</td>
							<td style="border-bottom:none;border-left-style:solid;border-left-color:#999999;border-width:2px;border-top-style:solid;border-top-width:2px;border-top-color:#FFFFFF;">&nbsp;&nbsp;</td-->
						</tr>
					</table>
				</td>
			</tr>
			<tr height="90%">
				<td>
					<table id="graphoptions" width="100%" class="background" border="0" height="100%">
						<tr class="background" VALIGN="top">
							<td>
								<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="white">
									<tr>
										<td>
											<table class="formsubtitle" border="0" width="100%" height="10%" bgcolor="white">
												<tr>
													<td>&nbsp;</td>
												</tr>
												<tr>
													<td class="ctrlgroup">&nbsp;&nbsp;Graph Options</td>
												</tr>
											</table>					
											<table border="0" cellpadding="0" cellspacing="0" width="100%" height1="92%" bgcolor="white">
												<tr>
													<td colspan="3">&nbsp;</td>
												</tr>
												<tr>
													<td width="20%" align="left">&nbsp;&nbsp;Graph Type:</td>
													<td width="20%" align="left">Graph Style:</td>
													<td width="60%" align="left">Perspective:</td>
												</tr>
												<tr>
													<td width="20%" align="left">&nbsp;
														<select size="1" style="width:90%"  name="cboGraphType" onChange="changeGraphType();changeGraphData();">
														<% 
															If mType Then
																For i = 0 to UBound(arrType)
																	If i=0 then 
																		Response.Write "<option selected>" & arrType(i) & "</option>"
																	Else
																		Response.Write "<option>" & arrType(i) & "</option>"
																	End If
																Next
															End If
														%>
														</select>
													</td>
													<td width="20%" align="left">
														<select size="1" style="width:90%" name="cboGraphStyle" onChange="changeGraphData();">
														</select>
													</td>
													<td width="60%" align="left">
														<select size="1" style="width:20%" name="cboPerspective" disabled onChange="changePerspective();">
														<% 
															If mPerspective Then
																For i = 0 to UBound(arrPerspective)
																	If i=0 then 
																		Response.Write "<option selected>" & arrPerspective(i) & "</option>"
																	Else
																		Response.Write "<option>" & arrPerspective(i) & "</option>"
																	End If
																Next
															End If
														%>
														</select>
													</td>
												</tr>
												<tr>
													<td width="100%" align="center" colspan="3">&nbsp;</td>
												</tr>
												<tr>
													<td width="20%" align="left">&nbsp;&nbsp;Graph Data :</td>
													<td width="20%" align="left"><input type="radio" name="optGraphData" value="0"  onClick="changeGraphData();" <%=sSummary %>>&nbsp;Summary</td>
													<td width="60%" align="left"><input type="radio" name="optGraphData" value="1" onClick1="changeGraphData();" <%=sDetail %>>&nbsp;Detail</td>
												</tr>
												<tr>
													<td width="100%" align="center" colspan="3">&nbsp;</td>
												</tr>
												<tr>
													<td width="20%" align="left">&nbsp;&nbsp;Grid :</td>
													<td width="20%" align="left"><input type="checkbox" name="chkVertical" onClick="changeGridStyle();"  <%=sVertical%>>&nbsp;Vertical</td>
													<td width="60%" align="left"><input type="checkbox" name="chkHorizontal" onClick="changeGridStyle();"<%=sHorizontal%>>&nbsp;Horizontal</td>
												</tr>
												<tr>
													<td width="100%" align="center" colspan="3">&nbsp;</td>
												</tr>
												<tr>
													<td width="20%" align="left">&nbsp;&nbsp;Labels :</td>
													<td width="20%" align="left"><input type="checkbox"  name="chkXaxis" onClick="changeLabels();"  <%=sXaxis%>>&nbsp;X-Axis</td>
													<td width="60%" align="left"><input type="checkbox"  name="chkYaxis" onClick="changeLabels();"  <%=sYaxis%>>&nbsp;Y-Axis</td>
												</tr>
												<tr>
													<td width="100%" align="center" colspan="3">&nbsp;</td>
												</tr>
												<tr>
													<td width="20%" align="left">&nbsp;&nbsp;Colors :</td>
													<td width="25%" align="left">Foreground&nbsp;
														<select size="1" style="width:50%" name="cboForegnd" onChange="changeForeGround();">
														<% 
															If mFore Then
																For i = 0 To UBound(arrFore)
																	If i=0 Then 
																		Response.Write "<option selected>" & arrFore(i) & "</option>"
																	Else
																		Response.Write "<option>" & arrFore(i) & "</option>"
																	End If
																Next
															End If
														%>
														</select>
													</td>
													<td width="55%" align="left">&nbsp;Background&nbsp;
														<select size="1" style="width:23%" name="cboBackgnd" onChange="changeBackGround();">
														<% 
															If mBack Then
																For i = 0 To UBound(arrBack)
																	If i=0 Then 
																		Response.Write "<option selected>" & arrBack(i) & "</option>"
																	Else
																		Response.Write "<option>" & arrBack(i) & "</option>"
																	End If
																Next
															End If
														%>
														</select>
													</td>
												</tr>
												<tr>
													<td width="100%" align="center" colspan="3">&nbsp;</td>
												</tr>
												<tr>
													<td width="20%" align="left">&nbsp;&nbsp;Graph Title :&nbsp;</td>
													<td width="80%" align="left" colspan="2"><input type="text" size="100" style1="width:80%"  name="txtGraphTitle" value="<%=fixDoubleQuote(sGraphTitle)%>"></td>
												</tr>
												<tr>
													<td width="100%" align="center" colspan="3">&nbsp;</td>
												</tr>
												<tr>
													<td width="20%" align="left">&nbsp;&nbsp;Left Title :&nbsp;</td>
													<td width="80%" align="left" colspan="2"><input type="text" size="100" style1="width:80%"  name="txtLeftTitle" value="<%=fixDoubleQuote(sLeftTitle)%>"></td>
												</tr>
												<tr>
													<td width="100%" align="center" colspan="3">&nbsp;</td>
												</tr>
												<tr>
													<td width="20%" align="left">&nbsp;&nbsp;Bottom Title :&nbsp;</td>
													<td width="80%" align="left" colspan="2"><input type="text" size="100" style1="width:80%"  name="txtBottomTitle" value="<%=fixDoubleQuote(sBottomTitle)%>"></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		
		<input type="hidden" name="hdNextField"  value="">
		<input type="hidden" name="hdTabID" value="">
		<input type="hidden" name="hdSave" value="<%=objSessionStr("Save")%>">
		<input type="hidden" name="hdSaveFinal" value="<%=objSessionStr("SaveFinal")%>">
		<input type="hidden" name="hdRptName" value="" >
		<input type="hidden" name="hdRptDesc" value="" >
		<input type="hidden" name="hdSelFields" value="<%=objSessionStr("ArrayP")%>">
		<input type="hidden" name="hdSelFieldsLength" value="" >
		<input type="hidden" name="hdArrayTF" value="">
		<input type="hidden" name="hdArrayPFields" value="">			
		<input type="hidden" name="hdDataType" value="">
		<input type="hidden" name="hdColProp" value="">
		<input type="hidden" name="hdArrayCurrentFields" value=''>
		<input type="hidden" name="hdSelFieldsXML" value="<%=fixDoubleQuote(objSessionStr("ArrayPXML"))%>">
		
		<input type="hidden" name="hdGraphType" value="<%=sGraphType%>">
		<input type="hidden" name="hdGraphStyleIndex" value="<%=sGraphStyleIndex%>">
		<input type="hidden" name="hdGridStyle" value="<%=sGridStyle%>">
		<input type="hidden" name="hdLabels" value="<%=sLabels%>">
		<input type="hidden" name="hdForeGround" value="<%=sForeGround%>">
		<input type="hidden" name="hdBackGround" value="<%=sBackGround%>">
		<input type="hidden" name="hdPerspective" value="<%=sPerspective%>">
		
		<input type="hidden" name="hdGraphStyle" value='<%=sGraphStyle%>'>
		
		<input type="hidden" name="hdSubmit" value="">

		<input type="hidden" name="hdDerFieldType" value="">
		<input type="hidden" name="hdDerFieldName" value="">
		<input type="hidden" name="hdDerFieldWidth" value="">
		<input type="hidden" name="hdDerDetailFormula" value="">
		<input type="hidden" name="hdDerIndex" value=""> 
		<input type="hidden" name="hdDerAddEdit" value=""> 
		<input type="hidden" name="hdDerAggregate" value=""> 
		<input type="hidden" name="hdDerAggregateFormula" value="">
		<input type="hidden" name="hdDerColumnInvolved" value="<%=fixDoubleQuote(objSessionStr("ColumnsInvolved"))%>"> 
		<input type="hidden" name="hdDerError" value='<%=sError%>'> 
		<input type="hidden" name="hdDerGScript" value="<%=sGlobalScript%>">
		
		<input type="hidden" name="hdRelDate" value=""> 
		<input type="hidden" name="hdRelDateFormula" value=""> 
		<input type="hidden" name="hdRelativeDate" value=""> 
		<input type="hidden" name="hdRelMode" value=""> 
		<input type="hidden" name="hdRelID" value=""> 

		<input type="hidden" name="hdPost" value="0"> 
		<input type="hidden" name="hdOutputType" value="0">
		<input type="hidden" name="hdPreview" value="0">
	</form>
	</body>
</html>

<%
Function SaveReport()

	'-- objSession("MODE1") will always be 1 here
	'-- User continues using tabbing but hasn't clicked on Save button
	If objSessionStr("Save") = "0" Then
	
		rptName = "Temp"
		sXML = UpdateXMLReport()
		
		If Not GetValidObject("InitSMList.CDataManager", objData) Then
			Call ShowError(pageError, "Call to CDataManager class failed.", True)
		End If
		objData.m_RMConnectStr = Application("SM_DSN")
		objData.TempReportID = objSessionStr("REPORTID")
		objData.ReportName = rptName
		objData.XML = sXML
		objData.UpdateTempReport
		
		Set objData = Nothing
					
	'-- User continues using tabbing but has clicked on Save button first time
	ElseIf objSessionStr("Save") = "1" And objSessionStr("SaveFinal") = "0" Then

		rptName=Request.Form("hdRptName")
		rptDesc=Request.Form("hdRptDesc")

		sXML = UpdateXMLReport()

		'-- Delete corresponding temporary report from database
		If Not GetValidObject("InitSMList.CDataManager", objData) Then
			Call ShowError(pageError, "Call to CDataManager class failed.", True)
		End If
		objData.m_RMConnectStr = Application("SM_DSN")
		objData.TempReportID = objSessionStr("REPORTID")
		objData.ReportName = rptName
		objData.ReportDesc = rptDesc
		objData.XML = sXML
		objData.DeleteTempReport
		objData.AddReport objSessionStr("UserID")

		objSession("REPORTID") = objData.ReportID
		Set objData = Nothing
			
		objSession("SaveFinal") = "1" 
				
	'-- User has already clicked on Save button and again clicks save button or uses tabbing
	ElseIf objSessionStr("Save") = "1" And objSessionStr("SaveFinal") = "1" Then

		sXML = UpdateXMLReport()
		
		If Not GetValidObject("InitSMList.CDataManager", objData) Then
			Call ShowError(pageError, "Call to CDataManager class failed.", True)
		End If
		objData.m_RMConnectStr = Application("SM_DSN")
		objData.ReportID = objSessionStr("REPORTID")
		objData.XML = sXML
		objData.UpdateReport
		Set objData = Nothing
		
	End If 

End Function


Function UpdateXMLReport()

	'-- Add/Delete XML tags/attributes for Options

	Dim objXML, x, z, a, t1, y, sGraphData, t2
	On Error Resume Next
	Set objXML = Nothing
	On Error Resume Next
	Set objXML = Server.CreateObject("Msxml2.DOMDocument")
	On Error Goto 0

	If objXML Is Nothing Then
		Call ShowError(pageError, "XML Parser is not available.", True)
	End If

	objXML.async = false

	'--Load the existing XML

	objXML.loadXML sXML

	Set z = objXML.getElementsByTagName("report").item(0)
	
	If Request.Form("hdDerGScript") <> "" Then

		If Not GetValidObject("DTGScript.ScriptEngine", script) Then
			Call ShowError(pageError, "Call to DTG ScriptEngine class failed.", True)
		End If

		If Not GetValidObject("InitSMList.CEventSink", evobj) Then
			Call ShowError(pageError, "Call to CEventSink class failed.", True)
		End If
				
		script.StartEngine

		script.RegisterGlobalObject "", evobj, False

		On Error Resume Next
		script.AddScript Request.Form("hdDerGScript")
		
		sError = script.LastError

		If sError = "" Then
			z.setAttribute "globalscript", Request.Form("hdDerGScript")
		Else
			script.StopEngine
			Set evobj = Nothing
			Set script = Nothing
		End If
	Else
		If Not z.getAttributeNode("globalscript") Is Nothing Then z.removeAttribute("globalscript")
	End If

	
	Set y = objXML.getElementsByTagName("graphsettings")
	y.removeAll
	
	If Request.Form("optGraphData") = "0" Then
		sGraphData = "4"
	ElseIf Request.Form("optGraphData") = "1" Then
		sGraphData = "3"
	End if

	'--Creating graph settings tag
	If Request.Form("hdGraphType") <> "" And Request.Form("hdGraphType") <> "0" Then
		Set x = objXML.getElementsByTagName("report").item(0)
		Set z =	x.insertBefore(objXML.createElement("graphsettings"),x.getElementsByTagName("reportfont").Item(0))
		Set a = objXML.getElementsByTagName("graphsettings").item(0)
				a.setAttribute "graphtype", Request.Form("hdGraphType")
				a.setAttribute "graphstyle", Request.Form("hdGraphStyleIndex")
				a.setAttribute "gridstyle", Request.Form("hdGridStyle")
				a.setAttribute "labels", Request.Form("hdLabels")
				a.setAttribute "graphfilter", sGraphData
				a.setAttribute "background", Request.Form("hdBackGround")
				a.setAttribute "foreground", Request.Form("hdForeGround") 
				a.setAttribute "perspective", Request.Form("hdPerspective") 
				a.setAttribute "rotation", "" 
				a.setAttribute "elevation", "" 
				
		Set t1 = z.appendChild(objXML.createElement("graphtitle"))	
		Set t2 = t1.appendChild(objXML.createTextNode(Request.Form("txtGraphTitle")))	
		Set t1 = z.appendChild(objXML.createElement("graphlefttitle"))	
		Set t2 = t1.appendChild(objXML.createTextNode(Request.Form("txtLeftTitle")))
		Set t1 = z.appendChild(objXML.createElement("graphbottomtitle"))	
		Set t2 = t1.appendChild(objXML.createTextNode(Request.Form("txtBottomTitle")))
		
		sGraphType = ""
		sGraphStyleIndex = ""
		sGraphType = objXML.getElementsByTagName("graphsettings").item(0).attributes.item(0).text
		sGraphStyleIndex = objXML.getElementsByTagName("graphsettings").item(0).attributes.item(1).text
	
		
		sForeGround = objXML.getElementsByTagName("graphsettings").item(0).attributes.item(6).text
		sBackGround = objXML.getElementsByTagName("graphsettings").item(0).attributes.item(5).text
		
		sGridStyle = objXML.getElementsByTagName("graphsettings").item(0).attributes.item(2).text
		sVertical = ""
		sHorizontal = ""
		If sGridStyle = "3" Then
			sVertical = "checked"
			sHorizontal = "checked"
		ElseIf sGridStyle = "2" Then 
			sVertical = "checked"
			sHorizontal = ""
		ElseIf sGridStyle = "1" Then
			sVertical = ""
			sHorizontal = "checked"
		End If 
		
		sLabels = objXML.getElementsByTagName("graphsettings").item(0).attributes.item(3).text
		sYaxis = ""
		sXaxis = ""
		If sLabels = "3" Then
			sYaxis = "checked"
			sXaxis = "checked"
		ElseIf sLabels = "2" Then 
			sYaxis = "checked"
			sXaxis = ""
		ElseIf sLabels = "1" Then
			sYaxis = ""
			sXaxis = "checked"
		End If 

		'-- Graph Data
		sSummary = "" 
		sDetail = ""
		If objXML.getElementsByTagName("graphsettings").item(0).attributes.item(4).value = "4" Then
			sSummary = "checked" 
			sDetail = ""
		ElseIf objXML.getElementsByTagName("graphsettings").item(0).attributes.item(4).value = "3" Then
			sDetail = "checked"
			sSummary = "" 
		End if
		
		sPerspective = objXML.getElementsByTagName("graphsettings").item(0).attributes.item(7).text
		
		sGraphTitle = Request.Form("txtGraphTitle")
		sLeftTitle = Request.Form("txtLeftTitle")
		sBottomTitle = Request.Form("txtBottomTitle")
	End If	

	If Request.Form("hdSubmit") = "RELATIVEDATE" And Request.Form("hdRelMode") = "ADD" Then
				
		If Request.Form("hdRelDate") <> "" And Request.Form("hdRelDateFormula") <> "" Then
			Dim objData

			If Not GetValidObject("InitSMList.CDataManager", objData) Then
				Call ShowError(pageError, "Call to CDataManager class failed.", True)
			End If
			
			objData.m_RMConnectStr = Application("SM_DSN")
		
			objData.AddDateFormula Request.Form("hdRelDate"), Request.Form("hdRelDateFormula")
			Set objData = Nothing
		End If
		
	ElseIf Request.Form("hdSubmit") = "RELATIVEDATE" And Request.Form("hdRelMode") = "UPDATE" Then

		If Request.Form("hdRelDate") <> "" And Request.Form("hdRelDateFormula") <> "" Then

			If Not GetValidObject("InitSMList.CDataManager", objData) Then
				Call ShowError(pageError, "Call to CDataManager class failed.", True)
			End If
			
			objData.m_RMConnectStr = Application("SM_DSN")
						
			objData.UpdateDateFormula CInt(Request.Form("hdRelID")),Request.Form("hdRelDateFormula")
			Set objData = Nothing
		End If
	
	End If

	Dim objDerived, objDetailFormula, objAggregateFormula, a1, a2, a3
	
	'-- Add mode
	If Request.Form("hdSubmit") = "DERIVED" And Request.Form("hdDerAddEdit") = "Add" Then

		If Not GetValidObject("DTGScript.ScriptEngine", script) Then
			Call ShowError(pageError, "Call to DTG ScriptEngine class failed.", True)
		End If
		
		If Not GetValidObject("InitSMList.CEventSink", evobj) Then
			Call ShowError(pageError, "Call to CEventSink class failed.", True)
		End If

		script.StartEngine

		script.RegisterGlobalObject "", evobj, False

		On Error Resume Next
		script.AddScript "function " & Request.Form("hdDerFieldName") & "()" & Chr(13) & " end function"
		sError = script.LastError
		If sError = "" Then
			script.AddScript "function " & Request.Form("hdDerFieldName") & "()" & Chr(13) & Request.Form("hdDerDetailFormula") & Chr(13) & Chr(13) & " end function"
			sError = script.LastError
			If sError = "" Then
				script.AddScript "function " & Request.Form("hdDerFieldName") & "()" & Chr(13) & Request.Form("hdDerAggregateFormula") & Chr(13) & Chr(13) & " end function"
				sError = script.LastError
				If sError = "" Then
					'-- Add column tag in columnorder tag and its value
					Set z = objXML.getElementsByTagName("columnorder").item(0).appendChild(objXML.createElement("column"))
						z.appendChild(objXML.createTextNode(objXML.getElementsByTagName("columnorder").item(0).childNodes.length - 1))
					For k = objXML.getElementsByTagName("columnlist").item(0).childNodes.length - 1 To 0 Step -1
						Set a1 = objXML.getElementsByTagName("columnlist").item(0).childNodes.item(k)
						Set a2 = a1.getElementsByTagName("derived").item(0)
						
						a4 = Cstr(CInt(a1.getAttributeNode("fieldnumber").value) + 1)
						
						If Not a2 Is Nothing Then
							a3 = a1.getAttributeNode("tableid").value
							a3 = CStr(CInt(a3) - 1)
							Exit For 
						Else
							a3 = "-1"
						End If
					
					Next

					'-- Add column in columnlist tag and its children
					Set z = objXML.getElementsByTagName("columnlist").item(0).appendChild(objXML.createElement("column"))
						z.setAttribute "fieldnumber", a4
						z.setAttribute "tableid", a3
						z.setAttribute "fieldid", a3
						z.setAttribute "usercolname", Request.Form("hdDerFieldName")
						z.setAttribute "colwidthunits", "chars"
						z.setAttribute "colwidth", Request.Form("hdDerFieldWidth")
						z.setAttribute "justify", "left"
						z.setAttribute "color", "0"
			
		
					Set objDerived = z.appendChild(objXML.createElement("derived"))
						objDerived.setAttribute "fieldtype", Request.Form("hdDerFieldType")
						objDerived.setAttribute "fieldname", Request.Form("hdDerFieldName")
						objDerived.setAttribute "defaultfieldwidth", Request.Form("hdDerFieldWidth")
						Set objDetailFormula = objDerived.appendChild(objXML.createElement("detailformula"))
						
					objDetailFormula.appendChild(objXML.createTextNode(Request.Form("hdDerDetailFormula")))			
							
					'-- Check for Aggregate Formula
					If Request.Form("hdDerAggregate") = "sameformula" Or Request.Form("hdDerAggregate") = "differentformula" Then
						Set objAggregateFormula = objDerived.appendChild(objXML.createElement("aggregateformula"))
							objDerived.setAttribute "aggregatecalcmethod", Request.Form("hdDerAggregate")
					End If
				
					objAggregateFormula.appendChild(objXML.createTextNode(Request.Form("hdDerAggregateFormula")))

					'-- Set parenttables tag
					z.appendChild(objXML.createElement("parenttables"))

					'-- Update p and pXML array with new added derived field and its attributes and children
					objSession("ArrayPXML") = Trim(objSessionStr("ArrayPXML")) & "," & Trim(objXML.getElementsByTagName("columnlist").item(0).childNodes.item(objXML.getElementsByTagName("columnlist").item(0).childNodes.length - 1).xml)
'-- Rajeev -- 03/09/2004					
'''					objSession("ArrayP") = Trim(objSessionStr("ArrayP")) & "," & "Derived : " & Request.Form("hdDerFieldName")
					objSession("ArrayP") = Trim(objSessionStr("ArrayP")) & "*!^" & "Derived : " & Request.Form("hdDerFieldName")
				Else
					script.StopEngine
		
					Set evobj = Nothing
					Set script = Nothing 
				End If
				
			Else
				script.StopEngine
		
				Set evobj = Nothing
				Set script = Nothing 
			End If
		Else
			script.StopEngine
		
			Set evobj = Nothing
			Set script = Nothing 
		End If

	ElseIf Request.Form("hdSubmit") = "DERIVED" And Request.Form("hdDerAddEdit") = "Edit" Then '-- Edit mode
		
		Set z = objXML.getElementsByTagName("columnlist").item(0).childNodes.item(CInt(Request.Form("hdDerIndex")))

		z.setAttribute "colwidth", Request.Form("hdDerFieldWidth")

		Set objDerived = z.getElementsByTagName("derived").item(0)

			objDerived.setAttribute "fieldtype", Request.Form("hdDerFieldType")
			objDerived.setAttribute "fieldname", Request.Form("hdDerFieldName")
			objDerived.setAttribute "defaultfieldwidth", Request.Form("hdDerFieldWidth")
		
		'-- Check for Aggregate Formula
		Dim objTempAgg, p, sAggFormula
		Set objTempAgg = objDerived.getElementsByTagName("aggregateformula").item(0)
		
		If Not objTempAgg Is Nothing Then
			sAggFormula = objTempAgg.text 
		Else
			sAggFormula = ""
		End If
		
		Set p = z.childNodes.item(0).getAttributeNode("aggregatecalcmethod")

		If Not p Is Nothing Then 
			objDerived.removeAttribute("aggregatecalcmethod")
		End If

		If Not objTempAgg Is Nothing Then
			''objTempAgg.removeAll
			objDerived.removeChild(objTempAgg)
		End If
		
		If Not GetValidObject("DTGScript.ScriptEngine", script) Then
			Call ShowError(pageError, "Call to DTG ScriptEngine class failed.", True)
		End If
		
		If Not GetValidObject("InitSMList.CEventSink", evobj) Then
			Call ShowError(pageError, "Call to CEventSink class failed.", True)
		End If

		script.StartEngine

		script.RegisterGlobalObject "ScriptEvent", evobj, False
		
		On Error Resume Next
		script.AddScript "function " & Request.Form("hdDerFieldName") & "()" & Chr(13) & Request.Form("hdDerDetailFormula") & Chr(13) & Chr(13) & " end function"
   
		sError = script.LastError
		
		If sError = "" Then
			Set objDetailFormula = objDerived.getElementsByTagName("detailformula").item(0)
				objDetailFormula.text = Request.Form("hdDerDetailFormula")

			'-- Check for Aggregate Formula
			If Request.Form("hdDerAggregate") = "sameformula" Or Request.Form("hdDerAggregate") = "differentformula" Then
				Set objAggregateFormula = objDerived.appendChild(objXML.createElement("aggregateformula"))
					objDerived.setAttribute "aggregatecalcmethod", Request.Form("hdDerAggregate")
			End If
					
			script.AddScript "function " & Request.Form("hdDerFieldName") & "()" & Chr(13) & Request.Form("hdDerAggregateFormula") & Chr(13) & Chr(13) & " end function"
   
			sError = script.LastError
				
			Set objAggregateFormula = objDerived.getElementsByTagName("aggregateformula").item(0)
				
			If sError = "" Then
				objAggregateFormula.text = Request.Form("hdDerAggregateFormula")
			Else
				objAggregateFormula.text = sAggFormula
			End If
		Else
			'-- Check for Aggregate Formula
			If Request.Form("hdDerAggregate") = "sameformula" Or Request.Form("hdDerAggregate") = "differentformula" Then
				Set objAggregateFormula = objDerived.appendChild(objXML.createElement("aggregateformula"))
					objDerived.setAttribute "aggregatecalcmethod", Request.Form("hdDerAggregate")
			End If
			Set objAggregateFormula = objDerived.getElementsByTagName("aggregateformula").item(0)
				
			objAggregateFormula.text = sAggFormula	
		
		End If

		script.StopEngine
		
		Set evobj = Nothing
		Set script = Nothing 

		objSession("ArrayPXML") = "-1"
		
		For j = 0  To objXML.getElementsByTagName("columnlist").item(0).childNodes.length - 1
			If objSessionStr("ArrayPXML") = "-1" Then
				objSession("ArrayPXML") = Trim(objXML.getElementsByTagName("columnlist").item(0).childNodes.item(j).xml)
			Else
				objSession("ArrayPXML") = Trim(objSessionStr("ArrayPXML")) & "," & Trim(objXML.getElementsByTagName("columnlist").item(0).childNodes.item(j).xml)
			End If
		Next
		
	ElseIf Request.Form("hdDerAddEdit") = "Delete" Then '-- Delete Derived Field

		Set z = objXML.getElementsByTagName("columnlist").item(0)
			z.removeChild z.childNodes.item(CInt(Request.Form("hdDerIndex")))
		
		Set z = objXML.getElementsByTagName("columnorder").item(0)
			z.removeChild z.childNodes.item(CInt(Request.Form("hdDerIndex")))

		
		objSession("ArrayPXML") = "-1"
		Dim arrColOrder()
		For j = 0  To CInt(z.childNodes.length) - 1
			If objSessionStr("ArrayPXML") = "-1" Then
				objSession("ArrayPXML") = Trim(objXML.getElementsByTagName("columnlist").item(0).childNodes.item(j).xml)
			Else
				objSession("ArrayPXML") = Trim(objSessionStr("ArrayPXML")) & "," & Trim(objXML.getElementsByTagName("columnlist").item(0).childNodes.item(j).xml)
			End If
			
			Redim Preserve arrColOrder(j)
			If CInt(Request.Form("hdDerIndex")) < Cint(Trim(objXML.getElementsByTagName("columnorder").item(0).childNodes.item(j).text)) Then
				arrColOrder(j) = Cint(Trim(objXML.getElementsByTagName("columnorder").item(0).childNodes.item(j).text)) - 1
			Else
				arrColOrder(j) = Cint(Trim(objXML.getElementsByTagName("columnorder").item(0).childNodes.item(j).text))
			End If
		Next

		Set z = objXML.selectNodes("//columns//columnorder")
		z.removeAll
			
		Set z = objXML.getElementsByTagName("columns").item(0)
		
		Set z1 = z.insertBefore(objXML.createElement("columnorder"), z.getElementsByTagName("columnlist").item(0))
		
		If IsValidArray(arrColOrder) Then
			For j = 0  To UBound(arrColOrder)
				Set z2 = z1.appendChild(objXML.createElement("column"))
					z2.appendChild(objXML.createTextNode(arrColOrder(j)))
			Next			
		End If
		
		Dim myArrayP
'-- Rajeev -- 03/09/2004		
'''		myArrayP = Split(objSessionStr("ArrayP"), ",")
		myArrayP = Split(objSessionStr("ArrayP"), "*!^")

		objSession("ArrayP") = "-1"

		If IsValidArray(myArrayP) Then
			For j = 0  To  UBound(myArrayP)
				If j <> CInt(Request.Form("hdDerIndex")) Then
					If objSessionStr("ArrayP") = "-1" Then
						objSession("ArrayP") = myArrayP(j)
					Else
'-- Rajeev -- 03/09/2004					
'''						objSession("ArrayP") = Trim(objSessionStr("ArrayP")) & "," & myArrayP(j)
						objSession("ArrayP") = Trim(objSessionStr("ArrayP")) & "*!^" & myArrayP(j)
					End If
				End If
			Next
		End If
						
		'-- Updating columns involved 
		Dim arrXML, b1, b2, b3
		Set xmlDoc = Nothing
		On Error Resume Next
		Set xmlDoc = Server.CreateObject("Msxml2.DOMDocument")
		On Error Goto 0

		If xmlDoc Is Nothing Then
			Call ShowError(pageError, "XML Parser is not available.", True)
		End If
		xmlDoc.async = false
		'''--- arrXML = Split(objSessionStr("ArrayPXML"),",")
		
		arrXML = Split(objSessionStr("ArrayPXML"), "</column>,")
		
		If IsValidArray(arrXML) Then
			For iCounter = 0 To UBound(arrXML) - 1
				arrXML(iCounter) = arrXML(iCounter) & "</column>"
			Next
		
			Dim sColumnInvolved
			sColumnInvolved = "-1"
		
			For i = 0 To UBound(arrXML)
				xmlDoc.loadXML arrXML(i)
				'-- Fetching DataType for derived fields
				Set b1 = xmlDoc.getElementsByTagName("column").item(0).childNodes.item(0) 
				Set b2 = xmlDoc.getElementsByTagName("derived").item(0)
				If Not b2 Is Nothing Then
					'-- Handling for columns involved
						Set b3 = b1.getElementsByTagName("detailformula").item(0)
						If sColumnInvolved = "-1" Then
							sColumnInvolved = b3.text
						Else
							sColumnInvolved = sColumnInvolved  & b3.text
						End If
				End If		
			Next
		End If
		
		objSession("ColumnsInvolved") = "-1"
		If Not (sColumnInvolved = "-1" Or sColumnInvolved = "") Then
			objSession("ColumnsInvolved") = sColumnInvolved
		End If
		
	End If
	
	UpdateXMLReport = objXML.XML
	
	Set objXML = Nothing	

End Function

Function DisplayGraphOptions()
	On Error Resume Next
	If Not xmlDoc.getElementsByTagName("report").item(0).getAttributeNode("globalscript") Is Nothing Then
		sGlobalScript = xmlDoc.getElementsByTagName("report").item(0).getAttributeNode("globalscript").value
	End If
	
	Dim a
	Set a = xmlDoc.getElementsByTagName("graphsettings").item(0) ''.appendChild(objXML.createElement("column"))
	If Not a Is Nothing Then 
		'-- Graph type & style
		sGraphType = ""
		sGraphStyleIndex = ""
		sGraphType = xmlDoc.getElementsByTagName("graphsettings").item(0).attributes.item(0).text
		sGraphStyleIndex = xmlDoc.getElementsByTagName("graphsettings").item(0).attributes.item(1).text
		'-- Graph Data
		sSummary = "" 
		sDetail = ""
		If xmlDoc.getElementsByTagName("graphsettings").item(0).attributes.item(4).value = "4" Then
			sSummary = "checked" 
			sDetail = ""
		ElseIf xmlDoc.getElementsByTagName("graphsettings").item(0).attributes.item(4).value = "3" Then
			sDetail = "checked"
			sSummary = "" 
		End if
	
		'-- Grid Data
		sVertical = ""
		sHorizontal = ""
		sGridStyle = xmlDoc.getElementsByTagName("graphsettings").item(0).attributes.item(2).text
		If sGridStyle = "3" Then
			sVertical = "checked"
			sHorizontal = "checked"
		ElseIf sGridStyle = "2" Then 
			sVertical = "checked"
			sHorizontal = ""
		ElseIf sGridStyle = "1" Then
			sVertical = ""
			sHorizontal = "checked"
		End If 
	
		'-- Labels
		sYaxis = ""
		sXaxis = ""
		sLabels = xmlDoc.getElementsByTagName("graphsettings").item(0).attributes.item(3).text
		If sLabels = "3" Then
			sYaxis = "checked"
			sXaxis = "checked"
		ElseIf sLabels = "2" Then 
			sYaxis = "checked"
			sXaxis = ""
		ElseIf sLabels = "1" Then
			sYaxis = ""
			sXaxis = "checked"
		End If 
		
		sPerspective = xmlDoc.getElementsByTagName("graphsettings").item(0).attributes.item(7).text
		
		'-- Foreground & background display
		sForeGround = xmlDoc.getElementsByTagName("graphsettings").item(0).attributes.item(6).text
		sBackGround = xmlDoc.getElementsByTagName("graphsettings").item(0).attributes.item(5).text
	
		'-- Graph,left and bottom title display
		sGraphTitle = xmlDoc.getElementsByTagName("graphsettings").item(0).childNodes(0).text 
		sLeftTitle = xmlDoc.getElementsByTagName("graphsettings").item(0).childNodes(1).text 
		sBottomTitle = xmlDoc.getElementsByTagName("graphsettings").item(0).childNodes(2).text 
	End If
End Function

Function RefreshSessionVariables()

	If Request.Form("hdSave") <> "-1" And Request.Form("hdSave") <> "" then
		objSession("Save") = Request.Form("hdSave")
	Else
		objSession("Save") = "-1"
	End If
	
	If Request.Form("hdSaveFinal") <> "-1" And Request.Form("hdSaveFinal") <> "" Then
		objSession("SaveFinal") = Request.Form("hdSaveFinal")
	Else
		objSession("SaveFinal") = "0"
	End If

	If Request.Form("hdArrayTF") <> "-1" And Request.Form("hdArrayTF") <> "" Then
		objSession("ArrayTF") = Request.Form("hdArrayTF")
	Else 
		objSession("ArrayTF") = "-1"
	End If
	
	If Request.Form("hdSelFields") <> "-1" And Request.Form("hdSelFields") <> "" Then
		objSession("ArrayP") = Request.Form("hdSelFields")
	Else 
		objSession("ArrayP") = "-1"
	End If
	
	If Request.Form("hdSelFieldsXML") <> "-1" And Request.Form("hdSelFieldsXML") <> "" Then
		objSession("ArrayPXML") = Request.Form("hdSelFieldsXML")
	Else
		objSession("ArrayPXML") = "-1"
	End If

	If Request.Form("hdDerColumnInvolved") <> "-1" And Request.Form("hdDerColumnInvolved") <> "" Then
		objSession("ColumnsInvolved") = Request.Form("hdDerColumnInvolved")
	Else 
		objSession("ColumnsInvolved") = "-1"
	End If

	If Request.Form("hdDerError") <> "-1" And Request.Form("hdDerError") <> "" Then
		sError = Request.Form("hdDerError")
	Else
		sError = ""
	End If
		
End Function

%>

