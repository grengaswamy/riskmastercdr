<!-- #include file ="session.inc" -->
<!-- #include file ="generic.asp" -->
<%
'-------------------------------------------------------------------------------------------
'-- Author	: Rajeev Chauhan
'-- Dated	: 09-Jul-2003
'-------------------------------------------------------------------------------------------

%> 
<html>
	<head>
		<title>Open Local Report</title>
		<link rel="stylesheet" href="RMNet.css" type="text/css"/>
		<script language="JavaScript" SRC="SMD.js"></script>
		<script language="JavaScript">
			var m_FormSubmitted=false;
			//-- Set Focus to the first field
			function pageLoaded()
			{
				 var i;

				 for (i=0;i<document.frmData.length;i++)			
					{	 				
					 if((document.frmData.item(i).type=="text") || (document.frmData.item(i).type=="select-one")|| (document.frmData.item(i).type=="textarea"))
						{
							document.frmData.item(i).focus();
							break;
						}
					}
			}
					
			function getSelUsers()
			{
				var sUsers=new String();
				for(var i=0;i<document.frmData.users.options.length;i++)
				{
					if (document.frmData.users.options[i].selected)
					{
						var sName=new String(document.frmData.users.options[i].value);
						if(sUsers!="")
							sUsers=sUsers + ",";
						sUsers=sUsers + sName;
					}
				}
				return sUsers;
			}
		
			function UserSubmit()
			{
		
				if(m_FormSubmitted)
				{
					alert("You already submitted this form. Please wait for server to respond.");
					return false;
				}
				
				if(document.frmData.txtName.value=="")
				{
					alert("Please enter report name.");
					return false;
				}
				
				if(document.frmData.txtFileName)
					if(document.frmData.txtFileName.value=="")
					{
						alert("Please select report file to open.");
						return false;
					}
				if(getSelUsers()=="")
				{
					alert("Please select at least one user allowed to use this report.");
					return false;
				}
				var sUsers=getSelUsers();
				document.frmData.userlist.value = sUsers;
				
				m_FormSubmitted=true;			
				return true;
			}
		</script>
	</head>

<%

'-- Delete the temporary report if the user opens a local report
Call CleanUp()

'-- Try to convert the report from old SM to new XML format
Dim pageError
dim ReportID
Function SMToXML(sSMText)
	Dim objSM, bError, sPath, objStream
	
	'-- Prepare Temp Conversion File 
	Set fso = CreateObject("Scripting.FileSystemObject")
	sPath = Application(APP_USERDIRECTORYHOME)& "\" & fso.GetTempName()
	Set objStream = fso.CreateTextFile(sPath)
	objStream.Write(sSMText)
	objStream.Close
	Set objStream = Nothing
	
	'-- Attempt Conversion
	Set objSM = CreateObject("SMEngine")
	objSM.InitEngine objSessionStr("DSN")
	If Not objSM.SMRConvert(sPath , sPath) Then 
		sError = "File you are trying to open is not a valid SORTMASTER report."
		SMToXML = ""
	Else
		Set objStream = fso.OpenTextFile(sPath)
		SMToXML = objStream.ReadAll()
		objStream.Close
		Set objStream = Nothing
	End If
	Set objSM = Nothing
	
	'-- Clean up
	On Error Resume Next
	fso.DeleteFile sPath, True
	On Error Goto 0
	Set fso = Nothing
End Function

Sub SubmitUploaded()
	Dim s
	Dim xml
	Dim sPath, sXMLPath
	Dim bError
	Dim objUtil
	Dim vTmp
	Dim objSM
	
	sError = ""
	boolShowForm = True
	
	Set objUpload = Nothing
	On Error Resume Next
	Set objUpload = Server.CreateObject("IISUpload.CUpload")
	On Error Goto 0

	If objUpload Is Nothing Then
		Call ShowError(pageError, "Call to CUpload class failed.", True)
	End If
	
	s = objUpload.Upload()
	
	If objUpload.ErrNumber = 0 And objUpload.FileName <> "" Then
		sPath = objUpload.FullFileName
		'-- Try to load report into DOM - display error if DOM complains

		Set xml = CreateObject("MSXML.DOMDocument")
		bError = False
		bError = (xml.load(sPath) = False)
		If Err.number <> 0 Then bError = True
		If Not bError Then
			'-- Write report to database
			Set objUtil = Nothing

			On Error Resume Next
			Set objUtil = Server.CreateObject("InitSMList.CUtility")
			On Error Goto 0

			If objUtil Is Nothing Then
				Call ShowError(pageError, "Call to CUtility class failed.", True)
			End If


			If Not objUtil.UploadReport(Application("SM_DSN"), objUpload("txtName") & "", objUpload("txtDescription") & "", xml.xml, FetchUserArray(), ReportID) Then
				sError = "Report couldn't be opened..."
			End If
		Else
		'-- Try to convert the report from old SM to new XML format
			Set objSM = Server.CreateObject("SMEngine")
			
			objSM.InitEngine objSessionStr("DSN")
						
			If objSM.SMRConvert(sPath, sPath) Then
				bError = (xml.load(sPath) = False)
				If Not bError Then
					'-- Write report to database
					Set objUtil = Nothing

					On Error Resume Next
					Set objUtil = Server.CreateObject("InitSMList.CUtility")
					On Error Goto 0

					If objUtil Is Nothing Then
						Call ShowError(pageError, "Call to CUtility class failed.", True)
					End If
					If Not objUtil.UploadReport (Application("SM_DSN"), objUpload("txtName") & "", objUpload("txtDescription") & "", xml.xml, FetchUserArray(), ReportID) Then
						sError = "Report couldn't be opened..."
					End If
					
				Else
					sError = "File you are trying to open is not a valid SORTMASTER report."
				End If
			Else ' Error - report probably is not XML
				sError = "File you are trying to open is not a valid SORTMASTER report."
			End If
			Set objSM = Nothing
		End If
			
		'-- Delete uploaded file
		On Error Resume Next
		fso.DeleteFile sPath, True
		On Error Goto 0
	Else	
		'-- Error while uploading
		If objUpload.ErrNumber = 0 Then
			sError="Please select file to open."
		ElseIf objUpload.ErrNumber = 60003 Then
			' File is too big
			sError = "File you are trying to open is too big. If you want to increase the file size you "
			sError = sError & "can open, get more disk space and many other benefits please sign up for our "
			sError = sError & "Premium Services."
		Else
			' Report what happend
			sError = "Your request could not be completed due to error: " & objUpload.ErrNumber & " - " & objUpload.ErrDesc & ". If this errors persist "
			sError = sError & "please contact System Administrator for assistance."
		End If
	End If
		
End Sub

'-- Parse user list
Function FetchUserArray()
	Dim sUserList, vArrUsers, bOk,f
	If not objUpload is nothing then
		sUserList = objUpload("userlist")
	else
		sUserList = Request("userlist")
	End If

	vArrUsers = Split(sUserList,",")

	FetchUserArray = vArrUsers
End Function

%>

<%
Dim objUpload,fso
''Dim sError
Set fso = CreateObject("Scripting.FileSystemObject")
Set objUpload = Nothing

If LCase(Request.ServerVariables("REQUEST_METHOD")) = "post" Then
	If Len(Request("xml")) = 0 Then
		SubmitUploaded()
	Else
		'-- no report xml
	End If
	
	If Len(sError) > 0 Then
		Call ShowError(pageError, sError, False)
	Else
%>
		<center><b><font color=blue>Report Opened Successfully!</font></b></br></center>
<%
	
		Response.Redirect "rptfields.asp?UPLOAD=UPLOAD&REPORTID=" & ReportID & "&MSG=" & "<center><b><font color=blue>Report Opened Successfully.</font></b></br></center>"
	End if
End If
%>
	<body class="10pt" onload="pageLoaded()">
	<form method="post" action="uploadreport.asp"   name="frmData" <%If Len(objSessionStr("reportxml"))= 0 Then%> enctype="multipart/form-data" <%End If%> onsubmit="return UserSubmit()">
		
					<table class="singleborder">
						<tr><td  colspan="2" class="ctrlgroup">Open Local Report</td></tr>
						<%'''If sError<>"" Then%>
						<!--tr><td colspan="2" class="errortext"><%=sError%></td></tr-->
						<%'''End If
						'Bury the "Can't read from Request collection after binary read" errors.
						On Error Resume Next
						%>
						<tr>
							<td nowrap="" class="datatd"><b>Report Name:</b></td>
							<td class="datatd"><input type="text" name="txtName" value="<%=Request("txtName")%>" size="48"/></td>
						</tr>

						<tr>
							<td nowrap="" class="datatd"><b>Report Description:</b></td>
							<td class="datatd"><input type="text" name="txtDescription" value="<%=Request("txtDescription")%>" size="48"/></td>
						</tr>
						<%If Len(objSessionStr("reportxml")) = 0 Then%>
						<tr>
							<td nowrap="" class="datatd"><b>Report File Name:</b></td>
							<td class="datatd"><input type="file" name="txtFileName"  size="35"/></td>
						</tr>
						<%Else%>
						<input type="hidden" name="xml" value='<%=objSessionStr("reportxml")%>' />
						<%objSession("reportxml") = "" 'clear it out - now passed via postback.
						End If
						On Error Goto 0
						%>
						<tr>
							<td nowrap="" class="datatd"><b>Users:</b></td>
							<td class="datatd">
							<i>Note: To select multiple users, hold down Ctrl key while clicking on user.</i><br />
							<select name="users" multiple="">
							<%
							Dim bLoadAllUsers, arrUserIDs(), arrFirstNames(), arrLastNames(), arrLoginNames()
							bLoadAllUsers=True
							If objSessionStr(SESSION_MODULESECURITY)="1" Then
								If objSessionStr(SESSION_ADMINRIGHTS)<>"1" Then
									bLoadAllUsers=False
								End If
							End If
		
							If bLoadAllUsers Then
								Dim objUtil, m
								Set objUtil = Nothing

								On Error Resume Next
								Set objUtil = Server.CreateObject("InitSMList.CUtility")
								On Error Goto 0

								If objUtil Is Nothing Then
									Call ShowError(pageError, "Call to CUtility class failed.", True)
								End If

								objUtil.LoadAllUsers Application(APP_SECURITYDSN), objSessionStr("DSNID"), arrUserIDs, arrFirstNames, arrLastNames, arrLoginNames
										
								Err.Clear()
								On Error Resume Next
								m = -1
								m = UBound(arrUserIDs)
								If Err.number <> 9 And m <> "" And m >= 0 Then
									For iCount = 0 To UBound(arrUserIDs)
										%>
										<option value="<% = arrUserIDs(iCount) %>"><% = arrFirstNames(iCount) & " " & arrLastNames(iCount) & " (" & arrLoginNames(iCount) & ")" %></option>
										<%
									Next
								End If
								On Error Goto 0
							Else
								Response.Write "<option value=""" & objSessionStr(SESSION_USERID) & """>" & objSessionStr(SESSION_FIRSTNAME) & " " & objSessionStr(SESSION_LASTNAME) & " (" & objSessionStr(SESSION_LOGINNAME) & ")</option>"
							End If
							%>								
							</select>
							</td>
						</tr>
						<tr>
							<td colspan="2" align="center" class="datatd">
								<br><center>
								
							</td>
						</tr>
					</table>
					
				
				<p class="small">
				Use this page to add a local report and open it for editing.<br />
				Be sure to give access to at least one user in the "Users" list.
				<br>
				*** For information on extended reporting features and report troubleshooting please see the <a href="smfaq.asp">Frequently Asked Questions (FAQ)</a>.</p>
					<input type="submit" class="button" name="btnSave" value="  Save  " />
								<input type="hidden" name="userlist" value=""/></center>
				</form>
	</body>
<%
Set fso = Nothing
Set objUpload = Nothing
%>
</html>