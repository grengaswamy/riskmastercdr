<% option explicit %>
<!-- #include file ="session.inc" -->
<!-- #include file ="generic.asp" -->

<html>
	<head>
		<link rel="stylesheet" href="RMNet.css" type="text/css"/>
		<title>SMD - [Criteria]</title>
		<script language="JavaScript" SRC="SMD.js"></script>
		<script language="JavaScript">

			var btnClick;
			btnClick = "";
			
			var trackSubquery;
			trackSubquery = 1;
			
			var txtNull;
			var txtEnter;
			var pAttributes;
			var operatorList;
			operatorList = '';
			
			var connectorList;
			connectorList = '';
			
			var arrData = new Array;
			
			arrData.length = 0;
			
			var pOperator = new Array;
			pOperator.length = 0;
			 
			function and_click()
			{	
				if(document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == false && document.frmData.txtValue.value != '' && trackSubquery == 1)
				{
					document.frmData.txtValue.value = '';
					document.frmData.btnDelete.disabled = true;
				}
				
				if(document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)
					check(1);
				else
				{
					if((document.frmData.cboOperators.selectedIndex == 14 || document.frmData.cboOperators.selectedIndex == 15) && trackSubquery == 0)
					{
						arrData[arrData.length] = '';
						check(0);
					}

					else if(trackSubquery == 0 && document.frmData.txtValue.value == '')
					{
						arrData[arrData.length] = document.frmData.txtValue.value;
						check(0);
					}
					else if(trackSubquery == 0 && document.frmData.txtValue.value != '' && document.frmData.btnDelete.disabled == false) //Selecting subquery criteria in edit mode.
						check(1);
					else if(document.frmData.txtValue.value != "")// || trackSubquery == 0)
					{
						arrData[arrData.length] = document.frmData.txtValue.value;
						check(0);
					}
				}
				document.frmData.txtValue.value="";
				document.frmData.btnChange.disabled = true;
				document.frmData.btnDelete.disabled = true;
				trackSubquery = 1;
				btnClick="AND"
			}
			
			function or_click()
			{	
				if(document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == false && document.frmData.txtValue.value != '' && trackSubquery == 1)
				{
					document.frmData.txtValue.value = '';
					document.frmData.btnDelete.disabled = true;
				}
				
				if(document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)
					check(1);
				else
				{
					if((document.frmData.cboOperators.selectedIndex == 14 || document.frmData.cboOperators.selectedIndex == 15) && trackSubquery == 0)
					{
						arrData[arrData.length] = '';
						check(0);
					}

					else if(trackSubquery == 0 && document.frmData.txtValue.value == '')
					{
						arrData[arrData.length] = document.frmData.txtValue.value;
						check(0);
					}
					else if(trackSubquery == 0 && document.frmData.txtValue.value != '' && document.frmData.btnDelete.disabled == false) //Selecting subquery criteria in edit mode.
						check(1);
					else if(document.frmData.txtValue.value != "")// || trackSubquery == 0)
					{
						arrData[arrData.length] = document.frmData.txtValue.value;
						check(0);
					}
				}
				
				document.frmData.txtValue.value="";
				document.frmData.btnChange.disabled = true;
				document.frmData.btnDelete.disabled = true;
				trackSubquery = 1;
				btnClick="OR"
			}
			
			function Subquery()
			{
				trackSubquery=0;
				if(btnClick=="OR")
					btnClick="OR"
				else if(btnClick=="AND")
					btnClick="AND"		
				else
					btnClick=""	
			}
			
			function add_item(txt)
			{
				var sNewItem;
				sNewItem = new Option(txt);
				if (navigator.appName == "Netscape")
					window.document.frmData.oSelect.add(sNewItem, null);
				else
					window.document.frmData.oSelect.add(sNewItem);
			}
			
			function check(pAdd)
			{
				var txt, index, FieldName;
				var oOption,selConnector;
				index = document.frmData.cboOperators.selectedIndex;
				FieldName = window.document.frmData.hdFieldName.value;
				
				switch(index)
				{
					case 0:
						txt="<";
						
						if(pAdd == 0)
						{
							pOperator[pOperator.length] = 0;
							operatorList = operatorList + "," + "lt";
						}
						
						if(document.frmData.oSelect.length==0 && document.frmData.txtValue.value!="") //If the value being entered is the 1st one and text box is not null.
						{
							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
						}
						else if(trackSubquery == 0 && document.frmData.txtValue.value=="")//If the value being entered is the 1st one and text box is null.
						{
							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";

							trackSubquery=1;
						}
						else if(trackSubquery== 0 && document.frmData.txtValue.value!="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == false  )//If the value being entered is not the 1st one and text box is null.
						{
							trackSubquery=1;
							return;//break;
							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
						}				
						else if(trackSubquery== 0 && document.frmData.txtValue.value!="" && document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)//If the value being entered is not the 1st one and text box is null.
						{
							trackSubquery=1;
							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
						}
						else if(trackSubquery == 1 && document.frmData.txtValue.value=="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == true)//If the text box value is null and check box value is not changed.
						{
							return;				
						}
						else if(trackSubquery == 1 && document.frmData.txtValue.value=="" && document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)//If the text box value is null and check box value is not changed.
						{
							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							break;				
						}
						else
						{
							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
						}
						
						break;

					case 1:
						txt="<=";
						
						if(pAdd == 0)
						{
							pOperator[pOperator.length] = 1;
							operatorList = operatorList + "," + "le";
						}
						
						if(document.frmData.oSelect.length==0 && document.frmData.txtValue.value!="") //If the value being entered is the 1st one and text box is not null.
						{
							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
						}
						else if(trackSubquery == 0 && document.frmData.txtValue.value=="" && document.frmData.oSelect.length==0)//If the value being entered is the 1st one and text box is null.
						{
							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";

							trackSubquery=1;
						}
						else if(trackSubquery== 0 && document.frmData.txtValue.value!="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == false  )//If the value being entered is not the 1st one and text box is null.
						{
							trackSubquery=1;
							return;//break;
							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
						}				
						else if(trackSubquery== 0 && document.frmData.txtValue.value!="" && document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)//If the value being entered is not the 1st one and text box is null.
						{
							trackSubquery=1;
							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
						}
						else if(trackSubquery == 1 && document.frmData.txtValue.value=="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == true)//If the text box value is null and check box value is not changed.
						{
							return;				
						}
						else if(trackSubquery == 1 && document.frmData.txtValue.value=="" && document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)//If the text box value is null and check box value is not changed.
						{
							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							break;				
						}
						else
						{
							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
						}
						
						break;
				
					case 2:
						txt="=";
						
						if(pAdd == 0)
						{
							pOperator[pOperator.length] = 2;
							operatorList = operatorList + "," + "eq";
						}
						
						if(document.frmData.oSelect.length==0 && document.frmData.txtValue.value!="") //If the value being entered is the 1st one and text box is not null.
						{
							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
						}
						else if(trackSubquery == 0 && document.frmData.txtValue.value=="" && document.frmData.oSelect.length==0)//If the value being entered is the 1st one and text box is null.
						{
							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";

							trackSubquery=1;
						}
						else if(trackSubquery== 0 && document.frmData.txtValue.value!="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == false  )//If the value being entered is not the 1st one and text box is null.
						{
							trackSubquery=1;
							return;//break;
							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
						}				
						else if(trackSubquery== 0 && document.frmData.txtValue.value!="" && document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)//If the value being entered is not the 1st one and text box is null.
						{
							trackSubquery=1;
							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
						}
						else if(trackSubquery == 1 && document.frmData.txtValue.value=="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == true)//If the text box value is null and check box value is not changed.
						{
							return;				
						}
						else if(trackSubquery == 1 && document.frmData.txtValue.value=="" && document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)//If the text box value is null and check box value is not changed.
						{
							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							break;				
						}
						else
						{
							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
						}
						
						break;

					case 3:
						txt=">=";
						
						if(pAdd == 0)
						{
							pOperator[pOperator.length] = 3;
							operatorList = operatorList + "," + "ge";
						}
						
						if(document.frmData.oSelect.length==0 && document.frmData.txtValue.value!="") //If the value being entered is the 1st one and text box is not null.
						{
							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
						}
						else if(trackSubquery == 0 && document.frmData.txtValue.value=="" && document.frmData.oSelect.length==0)//If the value being entered is the 1st one and text box is null.
						{
							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";

							trackSubquery=1;
						}
						else if(trackSubquery== 0 && document.frmData.txtValue.value!="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == false  )//If the value being entered is not the 1st one and text box is null.
						{
							trackSubquery=1;
							return;//break;
							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
						}				
						else if(trackSubquery== 0 && document.frmData.txtValue.value!="" && document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)//If the value being entered is not the 1st one and text box is null.
						{
							trackSubquery=1;
							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
						}
						else if(trackSubquery == 1 && document.frmData.txtValue.value=="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == true)//If the text box value is null and check box value is not changed.
						{
							return;				
						}
						else if(trackSubquery == 1 && document.frmData.txtValue.value=="" && document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)//If the text box value is null and check box value is not changed.
						{
							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							break;				
						}
						else
						{
							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
						}
						
						break;

					case 4:
						txt=">";
						
						if(pAdd == 0)
						{
							pOperator[pOperator.length] = 4;
							operatorList = operatorList + "," + "gt";
						}
						
						if(document.frmData.oSelect.length==0 && document.frmData.txtValue.value!="") //If the value being entered is the 1st one and text box is not null.
						{
							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
						}
						else if(trackSubquery == 0 && document.frmData.txtValue.value=="" && document.frmData.oSelect.length==0)//If the value being entered is the 1st one and text box is null.
						{
							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";

							trackSubquery=1;
						}
						else if(trackSubquery== 0 && document.frmData.txtValue.value!="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == false  )//If the value being entered is not the 1st one and text box is null.
						{
							trackSubquery=1;
							return;//break;
							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
						}				
						else if(trackSubquery== 0 && document.frmData.txtValue.value!="" && document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)//If the value being entered is not the 1st one and text box is null.
						{
							trackSubquery=1;
							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
						}
						else if(trackSubquery == 1 && document.frmData.txtValue.value=="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == true)//If the text box value is null and check box value is not changed.
						{
							return;				
						}
						else if(trackSubquery == 1 && document.frmData.txtValue.value=="" && document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)//If the text box value is null and check box value is not changed.
						{
							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							break;				
						}
						else
						{
							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
						}
						
						break;

					case 5:
						txt="<>";
					
						if(pAdd == 0)
						{
							pOperator[pOperator.length] = 5;
							operatorList = operatorList + "," + "ne";
						}
						
						if(document.frmData.oSelect.length==0 && document.frmData.txtValue.value!="") //If the value being entered is the 1st one and text box is not null.
						{
							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
						}
						else if(trackSubquery == 0 && document.frmData.txtValue.value=="" && document.frmData.oSelect.length==0)//If the value being entered is the 1st one and text box is null.
						{
							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";

							trackSubquery=1;
						}
						else if(trackSubquery== 0 && document.frmData.txtValue.value!="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == false  )//If the value being entered is not the 1st one and text box is null.
						{
							trackSubquery=1;
							return;//break;
							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
						}				
						else if(trackSubquery== 0 && document.frmData.txtValue.value!="" && document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)//If the value being entered is not the 1st one and text box is null.
						{
							trackSubquery=1;
							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
						}
						else if(trackSubquery == 1 && document.frmData.txtValue.value=="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == true)//If the text box value is null and check box value is not changed.
						{
							return;				
						}
						else if(trackSubquery == 1 && document.frmData.txtValue.value=="" && document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)//If the text box value is null and check box value is not changed.
						{
							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							break;				
						}
						else
						{
							if(pAdd == 0)
								txtEnter = btnClick + " " +"("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
						}
						
						break;
				
					case 6:
						txt=" LIKE ";
						
						if(pAdd == 0)
						{
							pOperator[pOperator.length] = 6;
							operatorList = operatorList + "," + "contains";
						}
						
						if(document.frmData.oSelect.length==0 && document.frmData.txtValue.value!="") //If the value being entered is the 1st one and text box is not null.
						{
							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"%"+"'"+")";						
							else
								txtEnter = "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"%"+"'"+")";
						}
						else if(trackSubquery=="0" && document.frmData.txtValue.value=="" && document.frmData.oSelect.length==0)//If the value being entered is the 1st one and text box is null.
						{
							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"%"+"'"+")";						
							else
								txtEnter = "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"%"+"'"+")";

							trackSubquery=1;
						}
						else if(trackSubquery== 0 && document.frmData.txtValue.value!="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == false  )//If the value being entered is not the 1st one and text box is null.
						{
							trackSubquery=1;
							return;//break;

						}				
						else if(trackSubquery== 0 && document.frmData.txtValue.value!="" && document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)//If the value being entered is not the 1st one and text box is null.
						{
							trackSubquery=1;
							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"%"+"'"+")";						
							else
								txtEnter = "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"%"+"'"+")";
						}				
						else if(trackSubquery == 1 && document.frmData.txtValue.value=="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == true)//If the text box value is null and check box value is not changed.
						{
							return;				
						}
						else if(trackSubquery == 1 && document.frmData.txtValue.value=="" && document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)//If the text box value is null and check box value is not changed.
						{
							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"%"+"'"+")";						
							else
								txtEnter = "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"%"+"'"+")";
							break;				
						}
						else
						{
							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"%"+"'"+")";						
							else
								txtEnter = "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"%"+"'"+")";
						}
						
						break;

					case 7:
						txt=" LIKE ";
						
						if(pAdd == 0)
						{
						pOperator[pOperator.length] = 7;
						operatorList = operatorList + "," + "like";
						}
						
						if(document.frmData.oSelect.length==0 && document.frmData.txtValue.value!="") //If the value being entered is the 1st one and text box is not null.
						{
							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
						}
						else if(trackSubquery=="0" && document.frmData.txtValue.value=="" && document.frmData.oSelect.length==0)//If the value being entered is the 1st one and text box is null.
						{
							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";

							trackSubquery=1;
						}
						else if(trackSubquery== 0 && document.frmData.txtValue.value!="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == false  )//If the value being entered is not the 1st one and text box is null.
						{
							trackSubquery=1;
							return;//break;

						}				
						else if(trackSubquery== 0 && document.frmData.txtValue.value!="" && document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)//If the value being entered is not the 1st one and text box is null.
						{
							trackSubquery=1;
							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
						}						
						else if(trackSubquery == 1 && document.frmData.txtValue.value=="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == true)//If the text box value is null and check box value is not changed.
						{
							return;				
						}
						else if(trackSubquery == 1 && document.frmData.txtValue.value=="" && document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)//If the text box value is null and check box value is not changed.
						{
							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";

							break;				
						}
						else
						{
							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
						}
						
						break;

					case 8:
						txt=" NOT LIKE ";
						
						if(pAdd == 0)
						{
							pOperator[pOperator.length] = 8;
							operatorList = operatorList + "," + "notcontains";
						}
						if(document.frmData.oSelect.length==0 && document.frmData.txtValue.value!="") //If the value being entered is the 1st one and text box is not null.
						{
							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"%"+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"%"+"'"+")";
						}
						else if(trackSubquery=="0" && document.frmData.txtValue.value=="" && document.frmData.oSelect.length==0)//If the value being entered is the 1st one and text box is null.
						{
							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"%"+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"%"+"'"+")";

							trackSubquery=1;
						}
						else if(trackSubquery== 0 && document.frmData.txtValue.value!="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == false  )//If the value being entered is not the 1st one and text box is null.
						{
							trackSubquery=1;
							return;//break;

						}				
						else if(trackSubquery== 0 && document.frmData.txtValue.value!="" && document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)//If the value being entered is not the 1st one and text box is null.
						{
							trackSubquery=1;
							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"%"+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"%"+"'"+")";
						}							
						else if(trackSubquery == 1 && document.frmData.txtValue.value=="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == true)//If the text box value is null and check box value is not changed.
						{
							return;				
						}
						else if(trackSubquery == 1 && document.frmData.txtValue.value=="" && document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)//If the text box value is null and check box value is not changed.
						{
							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"%"+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"%"+"'"+")";

							break;				
						}
						else
						{
							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"%"+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"%"+"'"+")";
						}
						
						break;
			
					case 9:
						txt=" NOT LIKE ";
						
						if(pAdd == 0)
						{
							pOperator[pOperator.length] = 9;
							operatorList = operatorList + "," + "notlike";
						}
						
						if(document.frmData.oSelect.length==0 && document.frmData.txtValue.value!="") //If the value being entered is the 1st one and text box is not null.
						{
							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
						}
						else if(trackSubquery=="0" && document.frmData.txtValue.value=="" && document.frmData.oSelect.length==0)//If the value being entered is the 1st one and text box is null.
						{
							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";

							trackSubquery=1;
						}
						else if(trackSubquery== 0 && document.frmData.txtValue.value!="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == false  )//If the value being entered is not the 1st one and text box is null.
						{
							trackSubquery=1;
							return;//break;

						}				
						else if(trackSubquery== 0 && document.frmData.txtValue.value!="" && document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)//If the value being entered is not the 1st one and text box is null.
						{
							trackSubquery=1;
							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
						}						
						else if(trackSubquery == 1 && document.frmData.txtValue.value=="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == true)//If the text box value is null and check box value is not changed.
						{
							return;				
						}
						else if(trackSubquery == 1 && document.frmData.txtValue.value=="" && document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)//If the text box value is null and check box value is not changed.
						{
							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";

							break;				
						}
						else
						{
							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"'"+")";
						}
						
						break;
						
					case 10:
						txt=" LIKE ";
						
						if(pAdd == 0)
						{
							pOperator[pOperator.length] = 10;
							operatorList = operatorList + "," + "beginswith";
						}
						if(document.frmData.oSelect.length==0 && document.frmData.txtValue.value!="") //If the value being entered is the 1st one and text box is not null.
						{
							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+document.frmData.txtValue.value+"%"+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"%"+"'"+")";
						}
						else if(trackSubquery=="0" && document.frmData.txtValue.value=="" && document.frmData.oSelect.length==0)//If the value being entered is the 1st one and text box is null.
						{
							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+document.frmData.txtValue.value+"%"+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"%"+"'"+")";

							trackSubquery=1;
						}
						else if(trackSubquery== 0 && document.frmData.txtValue.value!="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == false  )//If the value being entered is not the 1st one and text box is null.
						{
							trackSubquery=1;
							return;//break;

						}				
						else if(trackSubquery== 0 && document.frmData.txtValue.value!="" && document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)//If the value being entered is not the 1st one and text box is null.
						{
							trackSubquery=1;
							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+document.frmData.txtValue.value+"%"+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"%"+"'"+")";
						}					
						else if(trackSubquery == 1 && document.frmData.txtValue.value=="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == true)//If the text box value is null and check box value is not changed.
						{
							return;				
						}
						else if(trackSubquery == 1 && document.frmData.txtValue.value=="" && document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)//If the text box value is null and check box value is not changed.
						{
							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+document.frmData.txtValue.value+"%"+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"%"+"'"+")";

							break;				
						}
						else
						{
							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+document.frmData.txtValue.value+"%"+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"%"+"'"+")";
						}
						
						break;
						
					case 11:
						txt=" NOT LIKE ";
						
						if(pAdd == 0)
						{
							pOperator[pOperator.length] = 11;
							operatorList = operatorList + "," + "notbeginswith";	//Ankur Saxena MITS 9315 05/07/2007 - wrong operator was used.
						}
						if(document.frmData.oSelect.length==0 && document.frmData.txtValue.value!="") //If the value being entered is the 1st one and text box is not null.
						{
							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+document.frmData.txtValue.value+"%"+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"%"+"'"+")";
						}
						else if(trackSubquery=="0" && document.frmData.txtValue.value=="" && document.frmData.oSelect.length==0)//If the value being entered is the 1st one and text box is null.
						{
							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+document.frmData.txtValue.value+"%"+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"%"+"'"+")";

							trackSubquery=1;
						}
						else if(trackSubquery== 0 && document.frmData.txtValue.value!="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == false  )//If the value being entered is not the 1st one and text box is null.
						{
							trackSubquery=1;
							return;//break;

						}				
						else if(trackSubquery== 0 && document.frmData.txtValue.value!="" && document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)//If the value being entered is not the 1st one and text box is null.
						{
							trackSubquery=1;
							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+document.frmData.txtValue.value+"%"+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"%"+"'"+")";
						}					
						else if(trackSubquery == 1 && document.frmData.txtValue.value=="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == true)//If the text box value is null and check box value is not changed.
						{
							return;				
						}
						else if(trackSubquery == 1 && document.frmData.txtValue.value=="" && document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)//If the text box value is null and check box value is not changed.
						{
							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+document.frmData.txtValue.value+"%"+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"%"+"'"+")";

							break;				
						}
						else
						{
							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+document.frmData.txtValue.value+"%"+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+document.frmData.txtValue.value+"%"+"'"+")";
						}
						
						break;
						
					case 12:
						txt=" LIKE ";
					
						if(pAdd == 0)
						{
							pOperator[pOperator.length] = 12;
							operatorList = operatorList + "," + "endswith";
						}
						
						if(document.frmData.oSelect.length==0 && document.frmData.txtValue.value!="") //If the value being entered is the 1st one and text box is not null.
						{

							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"'"+")";
						}
						else if(trackSubquery=="0" && document.frmData.txtValue.value=="" && document.frmData.oSelect.length==0)//If the value being entered is the 1st one and text box is null.
						{
							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"'"+")";

							trackSubquery=1;
						}
						else if(trackSubquery== 0 && document.frmData.txtValue.value!="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == false  )//If the value being entered is not the 1st one and text box is null.
						{
							trackSubquery=1;
							return;//break;

						}				
						else if(trackSubquery== 0 && document.frmData.txtValue.value!="" && document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)//If the value being entered is not the 1st one and text box is null.
						{
							trackSubquery=1;
							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"'"+")";
						}								
						else if(trackSubquery == 1 && document.frmData.txtValue.value=="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == true)//If the text box value is null and check box value is not changed.
						{
							return;				
						}
						else if(trackSubquery == 1 && document.frmData.txtValue.value=="" && document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)//If the text box value is null and check box value is not changed.
						{
							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"'"+")";

							break;				
						}
						else
						{
							if(pAdd == 0)
								txtEnter = btnClick+ " " + "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"'"+")";
						}
						
						break;
						
					case 13:
						txt=" NOT LIKE ";
					
						if(pAdd == 0)
						{
							pOperator[pOperator.length] = 13;
							operatorList = operatorList + "," + "notendswith"; //Ankur Saxena MITS 9315 05/07/2007 - wrong operator was used.
						}
						if(document.frmData.oSelect.length==0 && document.frmData.txtValue.value!="") //If the value being entered is the 1st one and text box is not null.
						{
							if(pAdd == 0)
								txtEnter=btnClick+ " " + "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"'"+")";
						}
						else if(trackSubquery=="0" && document.frmData.txtValue.value=="" && document.frmData.oSelect.length==0)//If the value being entered is the 1st one and text box is null.
						{
							if(pAdd == 0)
								txtEnter=btnClick+ " " + "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"'"+")";

							trackSubquery=1;
						}				
						else if(trackSubquery== 0 && document.frmData.txtValue.value!="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == false  )//If the value being entered is not the 1st one and text box is null.
						{
							trackSubquery=1;
							return;//break;

						}				
						else if(trackSubquery== 0 && document.frmData.txtValue.value!="" && document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)//If the value being entered is not the 1st one and text box is null.
						{
							trackSubquery=1;
							if(pAdd == 0)
								txtEnter=btnClick+ " " + "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"'"+")";
						}							
						else if(trackSubquery == 1 && document.frmData.txtValue.value=="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == true)//If the text box value is null and check box value is not changed.
						{
							return;				
						}
						else if(trackSubquery == 1 && document.frmData.txtValue.value=="" && document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)//If the text box value is null and check box value is not changed.
						{
							if(pAdd == 0)
								txtEnter=btnClick+ " " + "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"'"+")";

							break;				
						}
						else
						{
							if(pAdd == 0)
								txtEnter=btnClick+ " " + "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"'"+")";
							else
								txtEnter = "("+FieldName+txt+"'"+"%"+document.frmData.txtValue.value+"'"+")";
						}
									
						break;
						
					case 14:
						txt=" IS NULL ";
						
						if(pAdd == 0)
						{
							pOperator[pOperator.length] = 14;
							operatorList = operatorList + "," + "isnull";
						}
						if(document.frmData.oSelect.length==0 && document.frmData.txtValue.value!="") //If the value being entered is the 1st one and text box is not null.
						{
							if(pAdd == 0)
								txtEnter=btnClick+ " " + "("+FieldName+txt+")";
							else
								txtEnter = "("+FieldName+txt+")";
						}
						else if(trackSubquery=="0" && document.frmData.txtValue.value=="" && document.frmData.oSelect.length==0)//If the value being entered is the 1st one and text box is null.
						{
							if(pAdd == 0)
								txtEnter=btnClick+ " " + "("+FieldName+txt+")";
							else
								txtEnter = "("+FieldName+txt+")";
							
							trackSubquery=1;
						}
						else if(trackSubquery== 0 && document.frmData.txtValue.value!="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == false  )//If the value being entered is not the 1st one and text box is null.
						{
							trackSubquery=1;
							return;//break;

						}				
						else if(trackSubquery== 0 && document.frmData.txtValue.value!="" && document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)//If the value being entered is not the 1st one and text box is null.
						{
							trackSubquery=1;
							if(pAdd == 0)
								txtEnter=btnClick+ " " + "("+FieldName+txt+")";
							else
								txtEnter = "("+FieldName+txt+")";
						}							
						else if(trackSubquery == 1 && document.frmData.txtValue.value=="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == true)//If the text box value is null and check box value is not changed.
						{
							return;				
						}
						else if(trackSubquery == 1 && document.frmData.txtValue.value=="" && document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)//If the text box value is null and check box value is not changed.
						{
							if(pAdd == 0)
								txtEnter=btnClick+ " " + "("+FieldName+txt+")";
							else
								txtEnter = "("+FieldName+txt+")";

							break;				
						}
						else
						{
							if(pAdd == 0)
								txtEnter=btnClick+ " " + "("+FieldName+txt+")";
							else
								txtEnter = "("+FieldName+txt+")";
						}
						
						break;
						
					case 15:
						txt=" IS NOT NULL ";
					
						if(pAdd == 0)
						{
							pOperator[pOperator.length] = 15;
							operatorList = operatorList + "," + "isnotnull";
						}
						if(document.frmData.oSelect.length==0 && document.frmData.txtValue.value!="") //If the value being entered is the 1st one and text box is not null.
						{
							if(pAdd == 0)
								txtEnter=btnClick+ " " + "("+FieldName+txt+")";
							else
								txtEnter = "("+FieldName+txt+")";
						}
						else if(trackSubquery=="0" && document.frmData.txtValue.value=="" && document.frmData.oSelect.length==0)//If the value being entered is the 1st one and text box is null.
						{
							if(pAdd == 0)
								txtEnter=btnClick+ " " + "("+FieldName+txt+")";
							else
								txtEnter = "("+FieldName+txt+")";
							
							trackSubquery=1;
						}
						else if(trackSubquery== 0 && document.frmData.txtValue.value!="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == false  )//If the value being entered is not the 1st one and text box is null.
						{
							trackSubquery=1;
							return;//break;

						}				
						else if(trackSubquery== 0 && document.frmData.txtValue.value!="" && document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)//If the value being entered is not the 1st one and text box is null.
						{
							trackSubquery=1;
							if(pAdd == 0)
								txtEnter=btnClick+ " " + "("+FieldName+txt+")";
							else
								txtEnter = "("+FieldName+txt+")";
								
						}							
						else if(trackSubquery == 1 && document.frmData.txtValue.value=="" && document.frmData.btnChange.disabled == true && document.frmData.btnDelete.disabled == true)//If the text box value is null and check box value is not changed.
						{
							return;				
						}
						else if(trackSubquery == 1 && document.frmData.txtValue.value=="" && document.frmData.btnChange.disabled == false && document.frmData.btnDelete.disabled == false)//If the text box value is null and check box value is not changed.
						{
							if(pAdd == 0)
								txtEnter=btnClick+ " " + "("+FieldName+txt+")";
							else
								txtEnter = "("+FieldName+txt+")";

							break;				
						}
						else
						{
							if(pAdd == 0)
								txtEnter=btnClick+ " " + "("+FieldName+txt+")";
							else
								txtEnter = "("+FieldName+txt+")";
						}
				}
				
				if(pAdd == 0) //-- Add mode
					add_item(txtEnter);
				else //-- Edit mode
				{	
					var selLength = document.frmData.oSelect.length;
					
					if(selLength == 0) //-- Need to add
					{
						if(document.frmData.cboOperators.selectedIndex == 14 || document.frmData.cboOperators.selectedIndex == 15)
						{
							arrData[arrData.length] = '';
							//check(0);
						}
						else if(document.frmData.txtValue.value != "" || trackSubquery == 0)
						{
							arrData[arrData.length] = document.frmData.txtValue.value;
							//check(0);
						}
						add_item(txtEnter);
					}
					else
					{
					var selTxt = document.frmData.oSelect.options[document.frmData.oSelect.selectedIndex].text;
					if(selTxt.substring(0,2) == "OR")
						selConnector = "OR ";
					else if(selTxt.substring(0,3) == "AND")
						selConnector = "AND ";
					else
						selConnector = "";
						
					document.frmData.oSelect.options[document.frmData.oSelect.selectedIndex].text = selConnector + txtEnter;
					arrData[document.frmData.oSelect.selectedIndex] = document.frmData.txtValue.value;
					pOperator[document.frmData.oSelect.selectedIndex] = document.frmData.cboOperators.selectedIndex;
					}
				}	
			}//End Check
			
			function delete_click()
			{
				var txt;
				if(document.frmData.oSelect.length>1)
				{
					txt=document.frmData.oSelect.options[1].text
					pattern="(";
					index=txt.indexOf(pattern);
				}

				if(document.frmData.oSelect.selectedIndex != -1)
				{
					if(document.frmData.oSelect.selectedIndex=="0" && document.frmData.oSelect.length>1)//If 1st element is being deleted and there more than 1 elements.
					{
						arrData[0] = null;
						pOperator[0] = null;
						document.frmData.oSelect.options[0]=null;
						document.frmData.oSelect.options[0].text=txt.substring(index,txt.length);
						document.frmData.oSelect.selectedIndex="0";
					}
					else if(document.frmData.oSelect.selectedIndex=="0" && document.frmData.oSelect.length=="1")//If 1st element is being deleted and its the only element.
					{
						arrData[0] = null;
						pOperator[0] = null;
						document.frmData.oSelect.options[0]=null;
					}
					else if(document.frmData.oSelect.selectedIndex == document.frmData.oSelect.length-1)
					{
						arrData[document.frmData.oSelect.selectedIndex] = null;
						pOperator[document.frmData.oSelect.selectedIndex] = null;
						document.frmData.oSelect.options[document.frmData.oSelect.selectedIndex]=null;	
						document.frmData.oSelect.selectedIndex=document.frmData.oSelect.length-1;
					}
					else
					{
						var selIndx = document.frmData.oSelect.selectedIndex
						arrData[document.frmData.oSelect.selectedIndex] = null;
						pOperator[document.frmData.oSelect.selectedIndex] = null;
						document.frmData.oSelect.options[document.frmData.oSelect.selectedIndex]=null;
						document.frmData.oSelect.selectedIndex = selIndx; 
					}
					
					adjust_pArray(arrData);	
					adjust_pArray(pOperator);		
					
					if(document.frmData.oSelect.selectedIndex == -1)
					{
						document.frmData.btnDelete.disabled = true;
						document.frmData.txtValue.value = '';
					}
					else
					{
						document.frmData.txtValue.value = arrData[document.frmData.oSelect.selectedIndex];
						document.frmData.cboOperators.selectedIndex = pOperator[document.frmData.oSelect.selectedIndex];
					}
					
					if(document.frmData.oSelect.length == 0)
						operatorList = '';
				}
				else
				{
					return;
				}
			}
									
			function matchOperator(pOperator)
			{
				if(pOperator == "lt")
				{
					document.frmData.cboOperators.selectedIndex = 0;
					return 0;
				}
				else if(pOperator == "le")
				{
					document.frmData.cboOperators.selectedIndex = 1;
					return 1;
				}
				else if(pOperator == "eq")
				{
					document.frmData.cboOperators.selectedIndex = 2;
					return 2;
				}
				else if(pOperator == "ge")
				{
					document.frmData.cboOperators.selectedIndex = 3;
					return 3;
				}
				else if(pOperator == "gt")
				{
					document.frmData.cboOperators.selectedIndex = 4;
					return 4;
				}
				else if(pOperator == "ne")
				{
					document.frmData.cboOperators.selectedIndex = 5;
					return 5;
				}
				else if(pOperator == "contains")
				{
					document.frmData.cboOperators.selectedIndex = 6;
					return 6;
				}
				else if(pOperator == "like")
				{
					document.frmData.cboOperators.selectedIndex = 7;
					return 7;
				}
				else if(pOperator == "notcontains")
				{
					document.frmData.cboOperators.selectedIndex = 8;
					return 8;
				}
				else if(pOperator == "notlike")
				{
					document.frmData.cboOperators.selectedIndex = 9;
					return 9;
				}
				else if(pOperator == "beginswith")
				{
					document.frmData.cboOperators.selectedIndex = 10;
					return 10;
				}
				else if(pOperator == "notbeginswith")	//Ankur Saxena MITS 9315 05/07/2007 - wrong operator was used.
				{
					document.frmData.cboOperators.selectedIndex = 11;
					return 11;
				}
				else if(pOperator == "endswith")
				{
					document.frmData.cboOperators.selectedIndex = 12;
					return 12;
				}
				else if(pOperator == "notendswith") //Ankur Saxena MITS 9315 05/07/2007 - wrong operator was used.
				{
					document.frmData.cboOperators.selectedIndex = 13;
					return 13;
				}
				else if(pOperator == "isnull")
				{
					document.frmData.cboOperators.selectedIndex = 14;
					return 14;
				}
				else if(pOperator == "isnotnull")
				{
					document.frmData.cboOperators.selectedIndex = 15;
					return 15;
				}
			}
			
			//-- fills global operatorList 
			function selXMLOperator(pIndex)
			{
				if(pIndex == 0)
					operatorList = operatorList + "," + "lt";
				else if(pIndex == 1)
					operatorList = operatorList + "," + "le";
				else if(pIndex == 2)
					operatorList = operatorList + "," + "eq";
				else if(pIndex == 3)
					operatorList = operatorList + "," + "ge";
				else if(pIndex == 4)
					operatorList = operatorList + "," + "gt";
				else if(pIndex == 5)
					operatorList = operatorList + "," + "ne";
				else if(pIndex == 6)
					operatorList = operatorList + "," + "contains";
				else if(pIndex == 7)
					operatorList = operatorList + "," + "like";
				else if(pIndex == 8)
					operatorList = operatorList + "," + "notcontains";
				else if(pIndex == 9)
					operatorList = operatorList + "," + "notlike";
				else if(pIndex == 10)
					operatorList = operatorList + "," + "beginswith";
				else if(pIndex == 11)
					operatorList = operatorList + "," + "notbeginswith"; //Ankur Saxena MITS 9315 05/07/2007 - wrong operator was used.
				else if(pIndex == 12)
					operatorList = operatorList + "," + "endswith";
				else if(pIndex == 13)
					operatorList = operatorList + "," + "notendswith"; //Ankur Saxena MITS 9315 05/07/2007 - wrong operator was used.
				else if(pIndex == 14)
					operatorList = operatorList + "," + "isnull";
				else if(pIndex == 15)
					operatorList = operatorList + "," + "isnotnull";
			}
			
			function change_click()
			{
				var FieldName,oper;
				if(document.frmData.cboOperators.selectedIndex == 0)
					oper = "<";
				else if(document.frmData.cboOperators.selectedIndex == 1)
					oper = "<=";
				else if(document.frmData.cboOperators.selectedIndex == 2)
					oper = "=";
				else if(document.frmData.cboOperators.selectedIndex == 3)
					oper = ">=";
				else if(document.frmData.cboOperators.selectedIndex == 4)
					oper = ">";
				else if(document.frmData.cboOperators.selectedIndex == 5)
					oper = "<>";
				else if(document.frmData.cboOperators.selectedIndex == 6)
					oper = " LIKE ";
				else if(document.frmData.cboOperators.selectedIndex == 7)
					oper = " LIKE ";
				else if(document.frmData.cboOperators.selectedIndex == 8)
					oper = " NOT LIKE ";
				else if(document.frmData.cboOperators.selectedIndex == 9)
					oper = " NOT LIKE ";
				else if(document.frmData.cboOperators.selectedIndex == 10)
					oper = " LIKE ";
				else if(document.frmData.cboOperators.selectedIndex == 11)
					oper = " NOT LIKE ";
				else if(document.frmData.cboOperators.selectedIndex == 12)
					oper = " LIKE ";
				else if(document.frmData.cboOperators.selectedIndex == 13)
					oper = " NOT LIKE ";
				else if(document.frmData.cboOperators.selectedIndex == 14)
					oper = " IS NULL ";
				else if(document.frmData.cboOperators.selectedIndex == 15)
					oper = " IS NOT NULL ";
				FieldName = window.document.frmData.hdFieldName.value;
				//btnClick = '';
				check(1);
				
			}
			
			function ok_click()
			{
				//-- Initialize
				connectorList = '';
				dataList = '';

				var oOption,pcount;
				oOption="";
				var enterKey = String.fromCharCode(13, 10);

				//-- Check for sub query
				if(document.frmData.chkSubquery.checked == true)
					hasDatadefcrit = 1;
				else
					hasDatadefcrit = 0;

				//-- Rajeev -- 03/25/2004 -- Check for hiding in RMNet
				if(document.frmData.chkHide.checked == true)
					hideInRMNet = 1;
				else
					hideInRMNet = 0;

				//-- Rajeev -- 03/25/2004 -- fill RMNet Label
				document.frmData.hdSelRMNetLabel.value = document.frmData.txtRMNetLabel.value;
				 
				//-- fill pAttributes

				if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
				{
					if(window.opener.window.document.frmData.lstSelcriteria.length > 0)
						pAttributes = document.frmData.hdTableId.value + "|" + document.frmData.hdFieldId.value + "|" + "data" + "|" + "AND" + "|" + hasDatadefcrit + "|" + hideInRMNet + "|" + document.frmData.hdSelRMNetLabel.value;
					else
						pAttributes = document.frmData.hdTableId.value + "|" + document.frmData.hdFieldId.value + "|" + "data" + "|" + "none" + "|" + hasDatadefcrit + "|" + hideInRMNet + "|" + document.frmData.hdSelRMNetLabel.value;
				}
				else
				{
					var tempArray = window.document.frmData.hdSelCriteriaItem.value.split("~~~~~");
					var arrAttribute = tempArray[7].split("|");
					
					pAttributes = document.frmData.hdTableId.value + "|" + document.frmData.hdFieldId.value + "|" + "data" + "|" + arrAttribute[4] + "|" + hasDatadefcrit + "|" + hideInRMNet + "|" + document.frmData.hdSelRMNetLabel.value;
				}

				//-- No data case -- just close down
				if(document.frmData.oSelect.length == 0 && document.frmData.txtValue.value == '' && trackSubquery == 0 )
				{	
					//-- Connector list
					if(btnClick == '')
						connectorList = 'none';
					else
						connectorList = btnClick;

					if(document.frmData.cboOperators.selectedIndex == 14 || document.frmData.cboOperators.selectedIndex == 15)
					{
						arrData[arrData.length] = '';
						check(0);
					}
					else
					{
						arrData[arrData.length] = document.frmData.txtValue.value;
						check(0);
					}

					//-- fill datalist -- only one data item
					dataList = arrData[0];	

					//-- Operator list
					//-- seperate out initial comma
					if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
						operatorList = operatorList.substring(1,operatorList.length);
					else
					{
						operatorList = '';
						selXMLOperator(document.frmData.cboOperators.selectedIndex);
						//-- seperate out initial comma
						operatorList = operatorList.substring(1,operatorList.length);
					}
					
					//-- maintain outer main connector
					if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
					{
						if(window.opener.window.document.frmData.lstSelcriteria.length > 0)
						{
							if(txtEnter != '')
								txtEnter = "AND" + " " + txtEnter;
						}
					}
					else
					{
						if(window.document.frmData.hdSelCriteriaIndex.value > 0)
						{
							if(txtEnter != '')
								txtEnter = "AND" + " " + txtEnter;
						}
					}
					window.opener.criteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,operatorList,dataList,connectorList,document.frmData.hdFieldName.value,document.frmData.hdFieldType.value,document.frmData.hdDBTableId.value,pAttributes,(txtEnter+enterKey));			
					window.opener.OLEcriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,'','', false);
					window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,'','', false);
					window.opener.displayCriteria();
					window.opener.displayGlobalCriteria();
					window.opener.changeFieldName(window.document.frmData.hdSelCriteriaIndex.value);
					window.opener.ButtonState();
						
					window.close();
					return;		
				
				}
				else if(document.frmData.oSelect.length == 0 && document.frmData.txtValue.value == '' && trackSubquery == 1)
				{	
					if(document.frmData.hdSelCriteriaIndex.value == -1)
					{
						window.close();
						return;
					}
					else
					{
						window.opener.deleteClick();
						window.close();
						return;
					
					}
				}
			
				if(document.frmData.oSelect.length == 0 && document.frmData.txtValue.value != '')
				{
					//-- Add mode -- Edit not applicable
					//-- Connector list
					if(btnClick == '')
						connectorList = 'none';
					else
						connectorList = btnClick;
					
					if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
					{
						if(document.frmData.cboOperators.selectedIndex == 14 || document.frmData.cboOperators.selectedIndex == 15)
						{
							arrData[arrData.length] = '';
							check(0);
						}
						else
						{
							arrData[arrData.length] = document.frmData.txtValue.value;
							check(0);
						}
					}
					else
					{
						if(document.frmData.cboOperators.selectedIndex == 14 || document.frmData.cboOperators.selectedIndex == 15)
							check(1);
						else
							check(1);
					}
						
					//-- fill datalist -- only one data item
					dataList = arrData[0];	
				
					if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
						operatorList = operatorList.substring(1,operatorList.length);
					else
					{
						operatorList = '';
						selXMLOperator(document.frmData.cboOperators.selectedIndex);
						//-- seperate out initial comma
						operatorList = operatorList.substring(1,operatorList.length);
					}
			
					//-- maintain outer main connector
					if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
					{
						if(window.opener.window.document.frmData.lstSelcriteria.length > 0)
							txtEnter = "AND" + " " + txtEnter;
					}
					else
					{
						if(window.document.frmData.hdSelCriteriaIndex.value > 0)
							txtEnter = "AND" + " " + txtEnter;
					}
					
					window.opener.criteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,operatorList,dataList,connectorList,document.frmData.hdFieldName.value,document.frmData.hdFieldType.value,document.frmData.hdDBTableId.value,pAttributes,(txtEnter+enterKey));			
					window.opener.OLEcriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,'','', false);
					window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,'','', false);
					window.opener.displayCriteria();
					window.opener.displayGlobalCriteria();
					window.opener.changeFieldName(window.document.frmData.hdSelCriteriaIndex.value);
					window.opener.ButtonState();
						
					window.close();
					return;		

				}
				else if(document.frmData.oSelect.length > 0 && document.frmData.txtValue.value != '' && document.frmData.btnDelete.disabled == true && document.frmData.btnChange.disabled == true)
				{
					//-- Add/Edit mode -- extended criteria will be appended the criteria of selected item in list box
					//-- fill connectorlist
					for(i=0;i<document.frmData.oSelect.length;i++)
					{
						if(document.frmData.oSelect.options[i].text.substring(0,3) == "AND")
							connectorList = connectorList + "," + "AND";
						else if(document.frmData.oSelect.options[i].text.substring(0,2) == "OR")
							connectorList = connectorList + "," + "OR";
						else
							connectorList = connectorList + "," + "none";
					}
					//-- append AND/OR as per the AND/OR button buffer
					connectorList = connectorList + "," + btnClick;
					
					//-- seperate out initial comma
					connectorList = connectorList.substring(1,connectorList.length);

					//-- fill datalist
					if(document.frmData.cboOperators.selectedIndex == 14 || document.frmData.cboOperators.selectedIndex == 15)
					{
						arrData[arrData.length] = '';
						check(0);
					}
					else
					{
						arrData[arrData.length] = document.frmData.txtValue.value;
						check(0);
					}
					
					for(i=0;i<arrData.length;i++)
						dataList = dataList + "^^^^^" + arrData[i];	

					//-- seperate out initial comma
					dataList = dataList.substring(5,dataList.length);

					//-- rebuild operator list
					operatorList = '';
					for(i=0;i<pOperator.length;i++)
						selXMLOperator(pOperator[i]);
					//-- seperate out initial comma
					if(operatorList.substring(0,1) == ",")
						operatorList = operatorList.substring(1,operatorList.length);
					

					//-- build combined critera to show on parent window's list box
					for(i=0;i<document.frmData.oSelect.length;i++)
						oOption = oOption + document.frmData.oSelect.options[i].text;

				}
				else if((document.frmData.oSelect.length > 0 && document.frmData.txtValue.value != '' && document.frmData.btnDelete.disabled == false && document.frmData.btnChange.disabled == true))
				{
					//-- fill connectorlist
					for(i=0;i<document.frmData.oSelect.length;i++)
					{
						if(document.frmData.oSelect.options[i].text.substring(0,3) == "AND")
							connectorList = connectorList + "," + "AND";
						else if(document.frmData.oSelect.options[i].text.substring(0,2) == "OR")
							connectorList = connectorList + "," + "OR";
						else
							connectorList = connectorList + "," + "none";
					}
					
					//-- seperate out initial comma
					connectorList = connectorList.substring(1,connectorList.length);

					//-- fill datalist
					if(document.frmData.cboOperators.selectedIndex == 14 || document.frmData.cboOperators.selectedIndex == 15)
					{
						//arrData[arrData.length] = '';
						check(1);
					}
					else
					{
						//arrData[arrData.length] = document.frmData.txtValue.value;
						check(1);
					}
					
					for(i=0;i<arrData.length;i++)
						dataList = dataList + "^^^^^" + arrData[i];	

					//-- seperate out initial comma
					dataList = dataList.substring(5,dataList.length);
					
					//-- eleminate last operator inserted by check(0)
					///////////operatorList = operatorList.substring(0,operatorList.lastIndexOf(','));

					//-- rebuild operator list
					operatorList = '';
					for(i=0;i<pOperator.length;i++)
						selXMLOperator(pOperator[i]);
					//-- seperate out initial comma
					if(operatorList.substring(0,1) == ",")
						operatorList = operatorList.substring(1,operatorList.length);
						
						
					//-- build combined critera to show on parent window's list box
					for(i=0;i<document.frmData.oSelect.length;i++)
						oOption = oOption + document.frmData.oSelect.options[i].text;

				}		
				else if(document.frmData.oSelect.length > 0 && document.frmData.txtValue.value != '' && document.frmData.btnDelete.disabled == false && document.frmData.btnChange.disabled == false)
				{
					//-- Add/Edit mode -- extended criteria will be appended the criteria of selected item in list box
					//-- fill connectorlist
					for(i=0;i<document.frmData.oSelect.length;i++)
					{
						if(document.frmData.oSelect.options[i].text.substring(0,3) == "AND")
							connectorList = connectorList + "," + "AND";
						else if(document.frmData.oSelect.options[i].text.substring(0,2) == "OR")
							connectorList = connectorList + "," + "OR";
						else
							connectorList = connectorList + "," + "none";
					}
					
					//-- seperate out initial comma
					connectorList = connectorList.substring(1,connectorList.length);

					//-- fill datalist
					if(document.frmData.cboOperators.selectedIndex == 14 || document.frmData.cboOperators.selectedIndex == 15)
						check(1);
					else
						check(1);
					
					for(i=0;i<arrData.length;i++)
						dataList = dataList + "^^^^^" + arrData[i];	

					//-- seperate out initial comma
					dataList = dataList.substring(5,dataList.length);
					
					//-- rebuild operator list
					operatorList = '';
					for(i=0;i<pOperator.length;i++)
						selXMLOperator(pOperator[i]);
					//-- seperate out initial comma
					if(operatorList.substring(0,1) == ",")
						operatorList = operatorList.substring(1,operatorList.length);

					//-- build combined critera to show on parent window's list box
					for(i=0;i<document.frmData.oSelect.length;i++)
						oOption = oOption + document.frmData.oSelect.options[i].text;

				}
				
				else if(document.frmData.oSelect.length > 0 && document.frmData.txtValue.value != '' && document.frmData.btnDelete.disabled == true && document.frmData.btnChange.disabled == false)
				{
					//-- Add/Edit mode -- extended criteria will be appended the criteria of selected item in list box
					//-- fill connectorlist
					for(i=0;i<document.frmData.oSelect.length;i++)
					{
						if(document.frmData.oSelect.options[i].text.substring(0,3) == "AND")
							connectorList = connectorList + "," + "AND";
						else if(document.frmData.oSelect.options[i].text.substring(0,2) == "OR")
							connectorList = connectorList + "," + "OR";
						else
							connectorList = connectorList + "," + "none";
					}
					
					//-- seperate out initial comma
					connectorList = connectorList.substring(1,connectorList.length);

					//-- fill datalist
					if(document.frmData.cboOperators.selectedIndex == 14 || document.frmData.cboOperators.selectedIndex == 15)
						check(1);
					else
						check(1);
					
					for(i=0;i<arrData.length;i++)
						dataList = dataList + "^^^^^" + arrData[i];	

					//-- seperate out initial comma
					dataList = dataList.substring(5,dataList.length);
					
					//-- rebuild operator list
					operatorList = '';
					for(i=0;i<pOperator.length;i++)
						selXMLOperator(pOperator[i]);
					//-- seperate out initial comma
					if(operatorList.substring(0,1) == ",")
						operatorList = operatorList.substring(1,operatorList.length);

					//-- build combined critera to show on parent window's list box
					for(i=0;i<document.frmData.oSelect.length;i++)
						oOption = oOption + document.frmData.oSelect.options[i].text;

				}		
				
				else if(document.frmData.oSelect.length > 0 && document.frmData.txtValue.value == '' && trackSubquery == 0)
				{
					//-- fill connectorlist
					for(i=0;i<document.frmData.oSelect.length;i++)
					{
						if(document.frmData.oSelect.options[i].text.substring(0,3) == "AND")
							connectorList = connectorList + "," + "AND";
						else if(document.frmData.oSelect.options[i].text.substring(0,2) == "OR")
							connectorList = connectorList + "," + "OR";
						else
							connectorList = connectorList + "," + "none";
					}
					//-- append AND/OR as per the AND/OR button buffer
					connectorList = connectorList + "," + btnClick;
					
					//-- seperate out initial comma
					connectorList = connectorList.substring(1,connectorList.length);
					
					if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
					{
						//-- fill datalist
						if(document.frmData.cboOperators.selectedIndex == 14 || document.frmData.cboOperators.selectedIndex == 15)
						{
							arrData[arrData.length] = '';
							check(0);
						}
						else
						{
							arrData[arrData.length] = document.frmData.txtValue.value;
							check(0);
						}
					}
					else
					{
						//-- fill datalist
						if(document.frmData.cboOperators.selectedIndex == 14 || document.frmData.cboOperators.selectedIndex == 15)
							check(1);
						else
							check(1);
					}			
					for(i=0;i<arrData.length;i++)
						dataList = dataList + "^^^^^" + arrData[i];	

					//-- seperate out initial comma
					dataList = dataList.substring(5,dataList.length);
					
					//-- rebuild operator list
					operatorList = '';
					for(i=0;i<pOperator.length;i++)
						selXMLOperator(pOperator[i]);
					//-- seperate out initial comma
					if(operatorList.substring(0,1) == ",")
						operatorList = operatorList.substring(1,operatorList.length);

					//-- build combined critera to show on parent window's list box
					for(i=0;i<document.frmData.oSelect.length;i++)
						oOption = oOption + document.frmData.oSelect.options[i].text;
				
				}				
				else if(document.frmData.oSelect.length > 0 && document.frmData.txtValue.value == '' && trackSubquery == 1)
				{
					//-- fill connectorlist
					for(i=0;i<document.frmData.oSelect.length;i++)
					{
						if(document.frmData.oSelect.options[i].text.substring(0,3) == "AND")
							connectorList = connectorList + "," + "AND";
						else if(document.frmData.oSelect.options[i].text.substring(0,2) == "OR")
							connectorList = connectorList + "," + "OR";
						else
							connectorList = connectorList + "," + "none";
					}
					
					//-- seperate out initial comma
					connectorList = connectorList.substring(1,connectorList.length);

					//-- fill datalist
					if(document.frmData.cboOperators.selectedIndex == 14 || document.frmData.cboOperators.selectedIndex == 15)
						check(1);
					else
						check(1);					
					
					for(i=0;i<arrData.length;i++)
						dataList = dataList + "^^^^^" + arrData[i];	

					//-- seperate out initial comma
					dataList = dataList.substring(5,dataList.length);
					
					//-- rebuild operator list
					operatorList = '';
					for(i=0;i<pOperator.length;i++)
						selXMLOperator(pOperator[i]);
					//-- seperate out initial comma
					if(operatorList.substring(0,1) == ",")
						operatorList = operatorList.substring(1,operatorList.length);
						
					//-- build combined critera to show on parent window's list box
					for(i=0;i<document.frmData.oSelect.length;i++)
						oOption = oOption + document.frmData.oSelect.options[i].text;
					
				}			
				//-- append outer parenthesis 

				if((trimIt(oOption).substring(0,3) == "AND" && trimIt((oOption).substring(3,trimIt(oOption).length)).indexOf("AND") > 0) || (trimIt(oOption).substring(0,3) == "AND" && trimIt((oOption).substring(3,trimIt(oOption).length)).indexOf("OR") > 0))
					oOption = "(" +  oOption + ")" ;
				else if((trimIt(oOption).substring(0,2) == "OR" && trimIt((oOption).substring(2,trimIt(oOption).length)).indexOf("OR") > 0) || (trimIt(oOption).substring(0,2) == "AND" && trimIt((oOption).substring(2,trimIt(oOption).length)).indexOf("AND") > 0))
					oOption = "(" +  oOption + ")" ;
				else if((trimIt(oOption).substring(0,3) != "AND" && trimIt(oOption).indexOf("AND") > 0) || (trimIt(oOption).substring(0,3) != "AND" && trimIt(oOption).indexOf("OR") > 0 ))
					oOption = "(" + oOption + ")";
				else if((trimIt(oOption).substring(0,2) != "OR" && trimIt(oOption).indexOf("AND")) > 0 || (trimIt(oOption).substring(0,2) != "OR" && trimIt(oOption).indexOf("OR") > 0 ))
					oOption = "(" + oOption + ")";

				if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
				{
					if(window.opener.window.document.frmData.lstSelcriteria.length > 0)
						oOption = "AND" + " " + oOption;
				}
				else
				{	
					if(window.document.frmData.hdSelCriteriaIndex.selectedIndex > 0)
						oOption = "AND" + " " + oOption;			
				}
				
				//-- code to handle the outer connector while editing
				if(document.frmData.hdSelCriteriaIndex.value != -1)		
				{	
					var arrPAttributes = pAttributes.split("|");
					
					if(arrPAttributes[3] != "none")
						oOption = arrPAttributes[3].toUpperCase() + " " + oOption;
				}
				window.opener.criteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,operatorList,dataList,connectorList,document.frmData.hdFieldName.value,document.frmData.hdFieldType.value,document.frmData.hdDBTableId.value,pAttributes,(oOption+enterKey));			
				window.opener.OLEcriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,'','', false);
				window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,'','', false);
				window.opener.displayCriteria();
				window.opener.displayGlobalCriteria();
				window.opener.changeFieldName(window.document.frmData.hdSelCriteriaIndex.value);
				window.opener.ButtonState();

				if(document.frmData.hdSelCriteriaIndex.value != -1)		
					window.opener.window.document.frmData.lstSelcriteria.selectedIndex = window.document.frmData.hdSelCriteriaIndex.value;
			
				window.close();
			}
			
			function selOperators(index)
			{
				return 	pOperator[index];
			}
			
			function ButtonEnable()
			{
				var selIndex;
				window.document.frmData.btnChange.disabled = true;

				if(window.document.frmData.oSelect.selectedIndex >= 0)
					window.document.frmData.btnDelete.disabled = false; 
				else
					window.document.frmData.btnDelete.disabled = true; 
						
				if(document.frmData.oSelect.selectedIndex >= 0)
					document.frmData.txtValue.value = arrData[document.frmData.oSelect.selectedIndex];
				
				selIndex = selOperators(document.frmData.oSelect.selectedIndex);
				document.frmData.cboOperators.selectedIndex = selIndex;
				matchOperator(pOperator[document.frmData.oSelect.selectedIndex]);
			}
			
			function EnableChange()
			{
				if(window.document.frmData.oSelect.selectedIndex >= 0 )
					window.document.frmData.btnChange.disabled = false;
				else
					window.document.frmData.btnChange.disabled = true;
			}
			
			function HideFields()
			{
				if((window.document.frmData.cboOperators.selectedIndex==window.document.frmData.cboOperators.length-1)||(window.document.frmData.cboOperators.selectedIndex==window.document.frmData.cboOperators.length-2))
				{
					window.document.frmData.txtValue.style.display = "none";
					window.document.frmData.lblValue.style.display = "none";
				}	
				else 
				{
					window.document.frmData.txtValue.style.display = "";
					window.document.frmData.lblValue.style.display = "";
				}
			}	
				
			function window_onLoad()
			{
				// Start Naresh MITS 7836 09/11/2006
				var objCheckBox=eval("document.frmData.chkHide");
				objCheckBox.style.display='none';
				var objTextBox=eval("document.frmData.txtRMNetLabel");
				objTextBox.style.display='none';
				// End Naresh MITS 7836 09/11/2006
				if(window.document.frmData.hdSelSubQuery.value == 1)
					window.document.frmData.chkSubquery.checked = true;

				if(window.document.frmData.hdSelHide.value == 1)
					window.document.frmData.chkHide.checked = true;

				if(window.document.frmData.hdSelRMNetLabel.value != '')
					window.document.frmData.txtRMNetLabel.value = window.document.frmData.hdSelRMNetLabel.value;
				
				if(document.frmData.hdSelCriteriaItem.value != '')
				{
					formatCriteria();
					var arrPCriteria, arrOperators;
					arrPCriteria = document.frmData.hdSelCriteriaItem.value.split("~~~~~");
					arrOperators =  arrPCriteria[1].split(",");
					
					pOperator.length = 0
					for(i=0;i<arrOperators.length;i++)
						pOperator[i] = matchOperator(arrOperators[i])
					
					operatorList = arrPCriteria[1];
					pAttributes = arrPCriteria[7];
					matchOperator(pOperator[0]);
					arrData = arrPCriteria[2].split("^^^^^");
					document.frmData.txtValue.value = arrData[0];
					document.frmData.cboOperators.selectedIndex = pOperator[0];					
				}
				
				if(window.document.frmData.hdSelCriteria.value != '')
				{
					document.frmData.oSelect.selectedIndex = 0;
					document.frmData.btnDelete.disabled = false;	
				}
		}	

		function formatCriteria()
			{
				var sEditCriteria = window.document.frmData.hdSelCriteriaItem.value;
				var lOperator,lData,lConnector,sFieldName,sCriteria,tempArray; 
				tempArray = sEditCriteria.split("~~~~~");
				 
				lOperator = tempArray[1].split(",");
				lData = tempArray[2].split("^^^^^");
				lConnector = tempArray[3].split(",");
				sFieldName = tempArray[4];
				for(iCount=0;iCount<lOperator.length;iCount++)
				{	
					index = matchOperator(lOperator[iCount]);

				switch(index)
				{
					case 0:
						txt = "<";
						
						if(lConnector[iCount] != "none")
							sCriteria = lConnector[iCount].toUpperCase() + " " +"("+sFieldName+txt+"'"+lData[iCount]+"'"+")";
						else
							sCriteria = "("+sFieldName+txt+"'"+lData[iCount]+"'"+")";
						add_item(sCriteria);
						break;

					case 1:
						txt="<=";
						
						if(lConnector[iCount] != "none")
							sCriteria = lConnector[iCount].toUpperCase() + " " +"("+sFieldName+txt+"'"+lData[iCount]+"'"+")";
						else
							sCriteria = "("+sFieldName+txt+"'"+lData[iCount]+"'"+")";
						add_item(sCriteria);
						break;

					case 2:
						txt="=";
						
						if(lConnector[iCount] != "none")
							sCriteria = lConnector[iCount].toUpperCase() + " " +"("+sFieldName+txt+"'"+lData[iCount]+"'"+")";
						else
							sCriteria = "("+sFieldName+txt+"'"+lData[iCount]+"'"+")";
						add_item(sCriteria);
						break;

					case 3:
						txt=">=";
						
						if(lConnector[iCount] != "none")
							sCriteria = lConnector[iCount].toUpperCase() + " " +"("+sFieldName+txt+"'"+lData[iCount]+"'"+")";
						else
							sCriteria = "("+sFieldName+txt+"'"+lData[iCount]+"'"+")";
						add_item(sCriteria);
						break;

					case 4:
						txt=">";
						
						if(lConnector[iCount] != "none")
							sCriteria = lConnector[iCount].toUpperCase() + " " +"("+sFieldName+txt+"'"+lData[iCount]+"'"+")";
						else
							sCriteria = "("+sFieldName+txt+"'"+lData[iCount]+"'"+")";
						add_item(sCriteria);
						break;

					case 5:
						txt="<>";
					
						if(lConnector[iCount] != "none")
							sCriteria = lConnector[iCount].toUpperCase() + " " +"("+sFieldName+txt+"'"+lData[iCount]+"'"+")";
						else
							sCriteria = "("+sFieldName+txt+"'"+lData[iCount]+"'"+")";
						add_item(sCriteria);
						break;

					case 6:
						txt=" LIKE ";
						
						if(lConnector[iCount] != "none")
							sCriteria = lConnector[iCount].toUpperCase() + " " +"("+sFieldName+txt+"'"+"%"+lData[iCount]+"%"+"'"+")";
						else
							sCriteria = "("+sFieldName+txt+"'"+"%"+lData[iCount]+"%"+"'"+")";
						add_item(sCriteria);
						break;

					case 7:
						txt=" LIKE ";
						if(lConnector[iCount] != "none")
							sCriteria = lConnector[iCount].toUpperCase() + " " +"("+sFieldName+txt+"'"+lData[iCount]+"'"+")";
						else
							sCriteria = "("+sFieldName+txt+"'"+lData[iCount]+"'"+")";
						add_item(sCriteria);
						break;

					case 8:
						txt=" NOT LIKE ";
						if(lConnector[iCount] != "none")
							sCriteria = lConnector[iCount].toUpperCase() + " " +"("+sFieldName+txt + "'"+"%"+lData[iCount]+"%"+"'"+")";
						else
							sCriteria = "("+sFieldName+txt + "'"+"%"+lData[iCount]+"%"+"'"+")";
						add_item(sCriteria);
						break;
			
					case 9:
						txt=" NOT LIKE ";
						if(lConnector[iCount] != "none")
							sCriteria = lConnector[iCount].toUpperCase() + " " +"("+sFieldName+txt+"'"+lData[iCount]+"'"+")";
						else
							sCriteria = "("+sFieldName+txt+"'"+lData[iCount]+"'"+")";
						add_item(sCriteria);
						break;
				
				
					case 10:
						txt=" LIKE ";
						if(lConnector[iCount] != "none")
							sCriteria = lConnector[iCount].toUpperCase() + " " +"("+sFieldName+txt+"'"+lData[iCount]+"%"+"'"+")";
						else
							sCriteria = "("+sFieldName+txt+"'"+lData[iCount]+"%"+"'"+")";
						add_item(sCriteria);
						break;
						
					case 11:
						txt=" NOT LIKE ";
						if(lConnector[iCount] != "none")
							sCriteria = lConnector[iCount].toUpperCase() + " " +"("+sFieldName+txt+"'"+lData[iCount]+"%"+"'"+")";
						else
							sCriteria = "("+sFieldName+txt+"'"+lData[iCount]+"%"+"'"+")";
						add_item(sCriteria);
						break;
						
					case 12:
						txt=" LIKE ";
						if(lConnector[iCount] != "none")
							sCriteria = lConnector[iCount].toUpperCase() + " " +"("+sFieldName+txt+"'"+"%"+lData[iCount]+"'"+")";
						else
							sCriteria = "("+sFieldName+txt+"'"+"%"+lData[iCount]+"'"+")";
						add_item(sCriteria);
						break;
						
					case 13:
						txt=" NOT LIKE ";
						if(lConnector[iCount] != "none")
							sCriteria = lConnector[iCount].toUpperCase() + " " +"("+sFieldName+txt+"'"+"%"+lData[iCount]+"'"+")";
						else
							sCriteria = "("+sFieldName+txt+"'"+"%"+lData[iCount]+"'"+")";
						add_item(sCriteria);
						break;
						
					case 14:
						txt=" IS NULL ";
						if(lConnector[iCount] != "none")
							sCriteria = lConnector[iCount].toUpperCase() + " " +"("+sFieldName+txt+")";
						else
							sCriteria = "("+sFieldName+txt+")";
						add_item(sCriteria);
						break;
						
					case 15:
						txt=" IS NOT NULL ";
					
						if(lConnector[iCount] != "none")
							sCriteria = lConnector[iCount].toUpperCase() + " " +"("+sFieldName+txt+")";
						else
							sCriteria = "("+sFieldName+txt+")";
						add_item(sCriteria);
						break;

					}//End For
				}
			
			}//End formatCriteria

		</script>
	</head>
	
	<body onLoad="window_onLoad();">
	<form name="frmData" topmargin="0" leftmargin="0" onSubmit="return false;">
		<table class="formsubtitle" border="0" width="100%" align="center">
			<tr>
				<td class="ctrlgroup" width="33%">Criteria :</td>
			</tr>
		</table>					
		<table width="100%" border="0">
			<tr><td colspan="3"><b><%=Response.Write(Request.QueryString("FIELDNAME"))%></b></td></tr>
			<tr height="8%">
				<td width="33%"></td>
				<td width="33%">
					<select name="cboOperators" style="WIDTH: 97%" height="1" onchange="Subquery();EnableChange();HideFields()">
						<option><(Less Than)</option>
						<option><=(Less Than or Equal)</option>
						<option>=(Equal To)</option>
						<option>>=(Greater Than or Equal)</option>
						<option>>(Greater Than)</option>
						<option><>(Not Equal To)</option>
						<option>Contains</option>
						<option>Like</option>
						<option>Does Not Contain</option>
						<option>Is Not Like</option>
						<option>Begins With</option>
						<option>Does Not Begin With</option>
						<option>Ends With</option>
						<option>Does Not End With</option>
						<option>Is Null</option>
						<option>Is Not Null</option>
					</select>
				</td>
				<td width="33%"><input type="checkbox" value1="" name="chkSubquery" onclick="Subquery()" >Subquery Criteria</td>
			</tr>
			<tr height="8%">
			<span name="hideSpan" style="visibilty:true">
				<td><input type="text" name="lblValue" Value="Value:" style="border:0" readonly></td>
				
				<td><input size="26" name="txtValue" onKeyDown="EnableChange()"></td>
				</span>
				<td>&nbsp;<input class="button" type="button" style1="WIDTH: 25%" name="btnChange" value="Change" onclick="change_click()" disabled></td>
			</tr>
			<tr height="8%">
				<td><input class="button" type="button" style1="WIDTH: 25%" name="btnAnd" value="   AND   " onclick="and_click()"></td>
				<td><input class="button" type="button" style1="WIDTH: 25%" name="btnOr" value="     OR     " onclick="or_click()"></td>
				<td>&nbsp;<input class="button" type="button" style1="WIDTH: 25%" name="btnDelete" value=" Delete " disabled onclick="delete_click()"></td>
			</tr>
			<tr height="68%">
				<td colspan="3">
					<select id="oSelect" name="lstCriteria" style="WIDTH:100%" size="15" onChange="ButtonEnable()" onClick="ButtonEnable();">
						
					</select>
				</td>
			</tr>
			<tr height="8%"><!-- Rajeev -- 03/25/2004 -- Added feature of RMNet labelling-->
				<td>
					<!-- Start Naresh MITS 7836 09/11/2006-->
					<input type="checkbox" name="chkHide"><!--Hide in RMNet-->
					<!-- End Naresh MITS 7836 09/11/2006-->
				</td>
				<!-- Start Naresh MITS 7836 09/11/2006-->
				<td colspan="2" align="right"><!--RMNet label:-->
				<!-- End Naresh MITS 7836 09/11/2006-->
					<input type="text" name="txtRMNetLabel" value="" onChange1="window.close()">
				</td>
			</tr>
		</table>
		<hr>
		<table border="0" width="100%" align="center" cellspacing="0" cellpadding="0">
			<tr>
				<td align="middle">
					<input class="button" type="button" style1="WIDTH: 30%" name="ok" value="    OK    " onClick="ok_click()">
					<input class="button" type="button" style1="WIDTH: 30%" name="cancel" value="Cancel" onClick="window.close()">
					<input class="button" type="button" style1="WIDTH: 30%" name="help" value="  Help  ">
				</td>
			</tr>
		</table>
		<input type="hidden" name="hdFieldName" value="<%=Request.QueryString("FIELDNAME")%>">
		<input type="hidden" name="hdTableId" value="<%=Request.QueryString("TABLEID")%>">
		<input type="hidden" name="hdFieldId" value="<%=Request.QueryString("FIELDID")%>">
		<input type="hidden" name="hdFieldType" value="<%=Request.QueryString("FIELDTYPE")%>">
		<input type="hidden" name="hdDBTableId" value="<%=Request.QueryString("DBTABLEID")%>">
		<input type="hidden" name="hdSelCriteria" value="<%=fixDoubleQuote(Request.QueryString("CRITERIA"))%>">
		<input type="hidden" name="hdSelCriteriaIndex" value="<%=Request.QueryString("CRITERIAINDEX")%>">
		<input type="hidden" name="hdSelSubQuery" value="<%=Request.QueryString("SUBQUERY")%>">
		<input type="hidden" name="hdSelHide" value="<%=Request.QueryString("HIDEINRMNET")%>">
		<input type="hidden" name="hdSelRMNetLabel" value="<%=fixDoubleQuote(Request.QueryString("RMNETLABEL"))%>">
		<input type="hidden" name="hdSelCriteriaItem" value="<%=fixDoubleQuote(Request.QueryString("PCRITERIA"))%>">
	</form>
	</body>
</html>