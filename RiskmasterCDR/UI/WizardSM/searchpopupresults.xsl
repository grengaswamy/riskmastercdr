<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/TR/WD-xsl">
<xsl:template match="/">

<html><head><title></title>
	<link rel="stylesheet" href="RMNet.css" type="text/css"/>
	<script language="JavaScript" SRC="searchpopupresults.js"></script>
</head>
	<body class="10pt">
		<table width="100%" cellspacing="0" cellpadding="0" border="0">
		<xsl:apply-templates select="results"/>
		</table>

		<table width="100%" border="0" cellspacing="0" cellpadding="4">
				<tr><td class="ctrlgroup1" nowrap=""><xsl:attribute name="colspan"><xsl:eval>documentElement.selectNodes("/results/columns/column").length + 1 </xsl:eval></xsl:attribute>Search</td></tr>
				<xsl:apply-templates select="results/columns" />
				<xsl:apply-templates select="results/data/row[index() $ge$ /results/@rowstart and index() $le$ /results/@rowend]" />
				<tr><td class="ctrlgroup1" nowrap=""><xsl:attribute name="colspan"><xsl:eval>documentElement.selectNodes("/results/columns/column").length + 1 </xsl:eval></xsl:attribute>&#160;</td></tr>
		</table>
		
		<table width="100%" cellspacing="0" cellpadding="0" border="0">
		<xsl:apply-templates select="results"/>
		</table>

		<form name="frmInfo">
		<input type="hidden" name="searchcat"><xsl:attribute name="value"><xsl:value-of select="/results/@searchcat"/></xsl:attribute></input>
		<input type="hidden" name="admtable"><xsl:attribute name="value"><xsl:value-of select="/results/@admtable"/></xsl:attribute></input>
		</form>
	</body>
</html>
</xsl:template>

<xsl:template match="results">
		<tr>
		<td nowrap=""><xsl:eval>Number(this.getAttribute("rowstart")) + 1 </xsl:eval> - <xsl:eval>Number(this.getAttribute("rowend")) + 1</xsl:eval> of <xsl:value-of select="@totalrows"/> matches<br/></td>
		<td align="right" width="50%" nowrap="">
			Page <xsl:value-of select="@pageno"/> of <xsl:value-of select="@totalpages"/>
			
			<xsl:choose>
				<xsl:when test="@pageno[. $gt$ 1]">
					<span class="pagescrolldec">&lt;&lt; </span>
					<a class="pagescroll" onClick="window.onunload=null;"><xsl:attribute name="href">searchpopupresults.asp?page=1</xsl:attribute>First</a>
					<a class="pagescroll" onClick="window.onunload=null;"><xsl:attribute name="href">searchpopupresults.asp?page=<xsl:eval>Number(this.getAttribute("pageno")) - 1 </xsl:eval></xsl:attribute>Previous</a>
				</xsl:when>
				<xsl:otherwise>
					<span class="disabled">&lt;&lt; First Previous</span>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when expr="Number(getAttribute('pageno')) &lt; Number(getAttribute('totalpages'))">
					<a class="pagescroll" onClick="window.onunload=null;"><xsl:attribute name="href" onClick="window.onunload=null;">searchpopupresults.asp?page=<xsl:eval>Number(this.getAttribute("pageno")) + 1 </xsl:eval></xsl:attribute>Next</a>
					<a class="pagescroll" onClick="window.onunload=null;"><xsl:attribute name="href">searchpopupresults.asp?page=<xsl:value-of select="@totalpages"/></xsl:attribute>Last</a>
					<span class="pagescrolldec">&gt;&gt;</span>
				</xsl:when>
				<xsl:otherwise>
					<span class="disabled">Next Last &gt;&gt;</span>
				</xsl:otherwise>
			</xsl:choose>
		</td>
		</tr>
</xsl:template>

<xsl:template match="columns">
	<xsl:for-each select="column">
		<td nowrap="" class="colheader2"><xsl:value-of select="text()"/></td>
	</xsl:for-each>
</xsl:template>

<xsl:template match="row">
	<xsl:if expr="childNumber(this) % 2 == 0">
		<tr class="rowdark1">
		
		<xsl:for-each select="field[0]">
			<td nowrap=""><a class="Blue" href="#"><xsl:attribute name="onClick">selRecord(<xsl:value-of select="ancestor(row)/@pid"/>)</xsl:attribute>
				<xsl:choose>
					<xsl:when test="text()">
						<xsl:value-of select="text()"/>
						<xsl:if test="text()[.='']">*</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						*
					</xsl:otherwise>
				</xsl:choose>
			</a></td>
		</xsl:for-each>
		
		<xsl:for-each select="field[index() $gt$ 0]">
			<td nowrap=""><xsl:value-of select="text()"/>&#160;</td>
		</xsl:for-each>
		</tr>
	</xsl:if>
	
	<xsl:if expr="childNumber(this) % 2 != 0">
		<tr class="rowlight1" >
		<xsl:for-each select="field[0]">
			<td nowrap=""><a class="Blue" href="#"><xsl:attribute name="onClick">selRecord(<xsl:value-of select="ancestor(row)/@pid"/>)</xsl:attribute>
				<xsl:choose>
					<xsl:when test="text()">
						<xsl:value-of select="text()"/>
						<xsl:if test="text()[.='']">*</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						*
					</xsl:otherwise>
				</xsl:choose>
			</a></td>
		</xsl:for-each>

		<xsl:for-each select="field[index() $gt$ 0]">
			<td nowrap=""><xsl:value-of select="text()"/>&#160;</td>
		</xsl:for-each>
		</tr>
	</xsl:if>
</xsl:template>

</xsl:stylesheet>
