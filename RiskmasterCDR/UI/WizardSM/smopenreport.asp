<% 'By neelima %>
<!-- #include file ="SessionInitialize.asp" -->
<!-- #include file ="generic.asp" -->

<%
'-------------------------------------------------------------------------------------------
'-- Author	: Rajeev Chauhan
'-- Dated	: 12-Jun-2003
'-------------------------------------------------------------------------------------------

%> 

<%
	'-- Aashish Bhateja
	'-- Date : 02/25/2005
	'-- SMNet Enhancements : Providing delete functionality.
		
	If Request("hdDelete") <> "" Then
		Dim objDataManager, bReturn, sReports
		Set objDataManager = Nothing

		On Error Resume Next
		Set objDataManager = Server.CreateObject("InitSMList.CDataManager")
		On Error Goto 0
		
		If objDataManager Is Nothing Then
			Call ShowError(pageError, "Call to CDataManager class failed.", True)
		End If
		
		sReports = Request("hdReports")

		objDataManager.m_RMConnectStr = Application("SM_DSN")
		bReturn = objDataManager.DeleteReport(sReports)
		
		If Not bReturn Then
			Call ShowError(pageError, "Call to CDataManager class failed.", False)
		End If
	End If
	
	'-- Aashish Bhateja
	'-- Date : 03/11/2005
	'-- SMNet Enhancements : Cloning a report.
	If Request("hdSave") <> "" Then
		If Not GetValidObject("InitSMList.CDataManager", objData) Then
			Call ShowError(pageError, "Call to CDataManager class failed.", True)
		End If
	
		'-- Get the report
		objData.m_RMConnectStr = Application("SM_DSN")
		objData.ReportID = Request("hdReports")
		objData.GetReport

		sXML = objData.XML
		
		'-- Add a new report which is a copy of the above report.
		objData.ReportName = Request.Form("hdRptName")
		objData.ReportDesc = Request.Form("hdRptDesc")
		objData.XML = sXML

		objData.AddReport objSessionStr("UserID")

		objSession("REPORTID") = objData.ReportID
		objSession("REPORTNAME") = objData.REPORTNAME
		
		Response.Redirect "rptFields.asp?REPORTID=" & objData.ReportID

		Set objData = Nothing
	End If

%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>	<head>
		<title>Draft Report</title>
		<link rel="stylesheet" href="RMNet.css" type="text/css" />
		<script language="JavaScript" SRC="SMD.js"></script>
		<script>
			function window_onLoad()
			{
			
			}
			var ns, ie, ieversion;
			var browserName = navigator.appName;                   // detect browser 
			var browserVersion = navigator.appVersion;
			if (browserName == "Netscape")
			{
				ie=0;
				ns=1;
			}
			else		//Assume IE
			{
				ieversion=browserVersion.substr(browserVersion.search("MSIE ")+5,1);
				ie=1;
				ns=0;
			}

			function onPageLoaded()
			{
				if (ie)
				{
					if ((eval("document.all.divForms")!=null) && (ieversion>=6))
						divForms.style.height=window.frames.frameElement.Height*0.75;
				}
				else
				{
					var o_divforms;
					o_divforms=document.getElementById("divForms");
					if (o_divforms!=null)
					{
						o_divforms.style.height=window.frames.innerHeight*0.72;
						o_divforms.style.width=window.frames.innerWidth*0.995;
					}
				}
			}
		</script>
	</head>

	<body class="10pt" onload1="onPageLoaded()">
	<form name="frmData" method="post" topmargin="0" bottommargin="0" action="<%= Request.ServerVariables("SCRIPT_NAME") %>">
		<table width="100%" border="0" cellspacing="0" cellpadding="4">
			<table width="100%" border="0" cellspacing="0" cellpadding="4">
			<tr>
				<td colspan="8"></td>
			</tr>
			<tr>
				<td class="ctrlgroup2" colspan="8">Draft Reports</td>
			</tr>
			<tr>
				<td width="5%" class="colheader3">&nbsp;</td>
				<td width="45%" class="colheader3">Report Name</td>
				<td width="50%" class="colheader3">Report Description</td>
			</tr>
		</table>
			<DIV id="divForms" class="divScroll">
			<table width=100% border="0" cellspacing="0" cellpadding="4">
			<%
			'-- Delete the temporary report if the user drafts a report
			Call CleanUp()
					
			Dim i
			i = 0

			Dim objUtil, arrReportIDs(), arrNames(), arrDescs(), pageError
			'-- Component call : results returned in array format
			Set objUtil = Nothing

			On Error Resume Next
			Set objUtil = Server.CreateObject("InitSMList.CUtility")
			On Error Goto 0

			If objUtil Is Nothing Then
				Call ShowError(pageError, "Call to CUtility class failed.", True)
			End If
			objUtil.GetReports Application("SM_DSN"), objSessionStr("UserID"), arrReportIDs, arrNames, arrDescs

			On Error Resume Next
			err.Clear() 
			Dim m
			m = -1
			m = UBound(arrReportIDs)

%>

<%			If Err.number <> 9 And m <> "" And m >= 0 Then
				On Error Goto 0
				For i = 0 To UBound(arrReportIDs)
				%>

				<% If i Mod 2 = 0 Then %>
				<tr class="rowlight1">
				<% Else %>
				<tr class="rowdark1">
				<% End If %>
				<td width="5%">
					<input name="chkReport<% = arrReportIDs(i)%>" type="checkbox"></input>
				</td>
				<td width="45%">
				<a class="Blue" href="rptfields.asp?REPORTID=<%=arrReportIDs(i)%>"><% If "" & arrNames(i)="" Then Response.Write "Report Nr. " & arrReportIDs(i) Else Response.Write LEFT(arrNames(i),50) %></a></td>
				<td width="50%"><%If "" & arrDescs(i)="" Then Response.Write "Not Specified" Else Response.Write LEFT(arrDescs(i),50) %></td>
				</tr>
				<%
				Next
			End If
			Set objUtil = Nothing
			%>
			<tr>
				<td class="ctrlgroup2" colspan="8">&nbsp;</td>
			</tr>
		</TABLE>
		</DIV>
		<input type="button" class="button" name="cmdClone" value="  Clone  " onClick="CloneReport(5,3)" />
		<input type="button" class="button" name="cmdDelete" value="  Delete  " onClick="DeleteReport()" />
		<input type="hidden" name="hdSave" value="">
		<input type="hidden" name="hdRptName" value="" >
		<input type="hidden" name="hdRptDesc" value="" >
		<input type="hidden" name="hdPreview" value="0">
		<input type="hidden" name="hdTabID" value="">
		<input type="hidden" name="hdPost" value="0">
		<input type="hidden" name="hdDelete" value="">
		<input type="hidden" name="hdReports" value="">
	</form>
	</body>
</html>

