<script language="VBScript" runat="server">
' Author: Denis Basaric, 07/2000

' Navigation Constants
Public Const SysCmdNavigationNone = 0
Public Const SysCmdNavigationFirst = 1
Public Const SysCmdNavigationPrev = 2
Public Const SysCmdNavigationNext = 3
Public Const SysCmdNavigationLast = 4
Public Const SysCmdSave = 5
Public Const SysCmdDelete = 6

Public Const ID_FLDTYPE = "id"
Public Const TEXT_FLDTYPE = "text"
Public Const CODE_FLDTYPE = "code"
Public Const CODEDETAIL_FLDTYPE = "codewithdetail"
Public Const RADIO_FLDTYPE = "radio"
Public Const ORGH_FLDTYPE = "orgh"
Public Const DATE_FLDTYPE = "date"
Public Const TIME_FLDTYPE = "time"
Public Const MEMO_FLDTYPE = "memo"
Public Const BOOL_FLDTYPE = "checkbox"
Public Const ZIP_FLDTYPE = "zip"
Public Const NUMERIC_FLDTYPE = "numeric"
Public Const CODELIST_FLDTYPE = "codelist"
Public Const ENTITYLIST_FLDTYPE = "entitylist"
Public Const LOOKUP_FLDTYPE = "lookup"
Public Const EIDLOOKUP_FLDTYPE = "eidlookup"
Public Const POLICYLOOKUP_FLDTYPE = "policylookup"
Public Const PLANLOOKUP_FLDTYPE = "planlookup"
Public Const POLICYSEARCH_FLDTYPE = "policysearch"
Public Const POLICYNUMBER_FLDTYPE = "policynumberlookup"
Public Const PLANNUMBER_FLDTYPE = "plannumberlookup"
Public Const COMBOBOX_FLDTYPE = "combobox"
Public Const ACCOUNTLIST_FLDTYPE = "accountlist"
Public const SUBTABLE_CONTROL = "subtable1"

Sub SetValue(xmlElem)
	Dim sType 
	Dim sName 
	Dim sTmp1, sTmp2 
	Dim vValue
	Dim objNode, vArr, c, l
	
	vValue = "" & Request(xmlElem.getAttribute("name"))
	sType = LCase(xmlElem.getAttribute("type"))
	
	If sType=CODELIST_FLDTYPE Or sType=ENTITYLIST_FLDTYPE Then
		If xmlElem.hasChildNodes Then RemoveChildNodes xmlElem
		' List Field Types have the ID's stored is comma separated hidden label _lst
		sName = xmlElem.getAttribute("name") & "_lst"
		If "" & Request(sName)<>"" Then
			' Add It as Child Nodes
			vArr=Split(Request(sName),",")
			If IsArray(vArr) Then
				c=UBound(vArr)
				For l=0 To c
					If IsNumeric(vArr(l)) Then
						Set objNode=xmlElem.ownerDocument.createElement("option")
						objNode.setAttribute "value",vArr(l)
						xmlElem.appendChild objNode
						Set objNode=Nothing
					End If
				Next
			End If
		End If
	'ElseIf sType = CODE_FLDTYPE Or sType = ORGH_FLDTYPE Or sType=LOOKUP_FLDTYPE Or sType=EIDLOOKUP_FLDTYPE Or sType=POLICYLOOKUP_FLDTYPE or sType = POLICYSEARCH_FLDTYPE Then
	ElseIf sType = CODE_FLDTYPE Or sType =  CODEDETAIL_FLDTYPE Or sType = ORGH_FLDTYPE Or sType=LOOKUP_FLDTYPE Or sType=EIDLOOKUP_FLDTYPE Or sType=POLICYLOOKUP_FLDTYPE Or sType = PLANLOOKUP_FLDTYPE or sType = POLICYSEARCH_FLDTYPE or sType = POLICYNUMBER_FLDTYPE Or sType = PLANNUMBER_FLDTYPE Then
	   sName = xmlElem.getAttribute("name")
	   If IsNumeric(Request(sName & "_cid")) Then
			xmlElem.setAttribute "codeid", "" & CLng(Request(sName & "_cid"))
			xmlElem.Text = vValue
		Else
			xmlElem.setAttribute "codeid", "0"
			xmlElem.Text=""
		End If
	ElseIf sType =BOOL_FLDTYPE Then
		If vValue="0" Or vValue="" Then
			xmlElem.Text = "0"
		Else
			xmlElem.Text = "1"
		End If
	ElseIf sType=COMBOBOX_FLDTYPE Then
		sName = xmlElem.getAttribute("name")
		If IsNumeric(Request(sName)) And sName<>"" Then
			xmlElem.setAttribute "codeid", "" & CLng(Request(sName))
		Else
			xmlElem.setAttribute "codeid", Request(sName)
		End If
	ElseIf sType=ACCOUNTLIST_FLDTYPE Then
	   sName = xmlElem.getAttribute("name")
	   If IsNumeric(Request(sName & "_cid")) Then
			xmlElem.setAttribute "codeid", "" & CLng(Request(sName & "_cid"))
			xmlElem.Text = vValue
		Else
			xmlElem.setAttribute "codeid", "0"
			xmlElem.Text=""
		End If
	ElseIf sType=RADIO_FLDTYPE Then
		If vValue = xmlElem.getAttribute("value") Then
			xmlElem.setAttribute "checked",""
			'changed by ravi on 26th march 03
			xmlElem.text=vValue
			'----end------
		Else 'Fix BSB 01.31.2004
			xmlElem.removeAttribute "checked"
			xmlElem.text = ""
		End if

	ElseIf sType = SUBTABLE_CONTROL then

		sName = xmlElem.getAttribute("name")
		Dim xmlColumnsNode,xmlHeaderNode, iRow, objCellNode, nRows
		Set xmlColumnsNode = xmlElem.selectSingleNode("header")
		nRows = 0
		if Trim(request(sName & "_rows")) > "" and isNumeric(Trim(request(sName & "_rows"))) then
			nRows = CInt(request(sName & "_rows"))
		end if
		
		For iRow = 1 to nRows

			Set xmlHeaderNode  = xmlColumnsNode.selectSingleNode("column")
			Dim objTempRowNode
			Set objTempRowNode = xmlElem.ownerDocument.createElement("row")
			
			While Not xmlHeaderNode Is Nothing

				Set objCellNode = xmlElem.ownerDocument.createElement("cell")
				objCellNode.setAttribute "idvalue" ,  request(sName & "_" & xmlHeaderNode.getAttribute("name") & "_" & iRow)
				objTempRowNode.appendChild objCellNode 
				Set objCellNode = Nothing
				set xmlHeaderNode = xmlHeaderNode.nextSibling

			Wend

			xmlElem.appendChild objTempRowNode ' append the row to the subtable1 control element
			Set objTempRowNode = Nothing

		Next
		Set xmlColumnsNode = Nothing
	Else
	   xmlElem.Text = vValue
	End If

End Sub

' Transfer the Posted Form Elements to coresponding
' XML tags.
Public Sub RequestToXML(objXMLDoc)
	Dim objXMLNode
	Dim objXMLList
	Dim sType
	Dim sName
	
	Set objXMLList=objXMLDoc.getElementsByTagName("control")
	For Each objXMLNode in objXMLList
		sType=LCase(objXMLNode.getAttribute("type"))
		sName = trim(objXMLNode.getAttribute("name"))
		If sType=CODELIST_FLDTYPE Or sType=ENTITYLIST_FLDTYPE Then
			If Not IsEmpty(Request(objXMLNode.getAttribute("name") & "_lst")) Then
				SetValue objXMLNode
			End If
		ElseIf sType = SUBTABLE_CONTROL then ' control is a table i.e. it'll always be empty
			
			if not IsEmpty(Request(objXMLNode.getAttribute("name") & "_rows")) then
				SetValue objXMLNode
			end if
		ElseIf Not IsEmpty(Request(objXMLNode.getAttribute("name"))) Then
			SetValue objXMLNode
		End If
		
		dim strGRPValue
		strGRPValue = trim(request(sName & "_GroupAssoc"))
		if strGRPValue > "" then
			objXMLNode.setAttribute "GroupAssoc", strGRPValue
		end if

	Next
	
	Set objXMLList=Nothing
	Set objXMLNode=Nothing

End Sub


'^Rajeev 01/16/2003-- MITS#3899
' Refresh the transferred Posted Form Elements values in coresponding XML tags.
Public Sub RefreshXMLNodes(objXMLDoc)
	Dim objXMLNode
	Dim objXMLList
	Dim sType

	Set objXMLList=objXMLDoc.getElementsByTagName("control")
	For Each objXMLNode in objXMLList
		objXMLNode.Text=""
	Next
	
	Set objXMLList=Nothing
	Set objXMLNode=Nothing

End Sub
'^^Rajeev 01/16/2003-- MITS#3899

' Transfer Only ID Values to the parameters
Public Sub RequestToParam(objParams, bParentOnly)
	Dim vArr
	Dim c, l
	
	' Parent Id's are comma delimited, can only one or none
	If Request("sys_formpidname")<>"" Then
		vArr=Split(Request("sys_formpidname"),",")
		If IsArray(vArr) Then
			c=UBound(vArr)
			For l=0 To c
				On Error Resume Next
				objParams.Add vArr(l),Request(vArr(l))
				On Error Goto 0
			Next
		End If
	End If
	
	' Add Extra data passed between forms
	If Request("sys_ex")<>"" Then
		vArr=Split(Request("sys_ex"),",")
		If IsArray(vArr) Then
			c=UBound(vArr)
			For l=0 To c
				On Error Resume Next
				'Hack - If bParentOnly then we are trying to create a new 
				' record and want to avoid passing the current formid value
				' into FDM since it will then just navigate to the existing record.
				' In some unique cases we are passing the formid value around
				' in sys_ex to get proper supp screen navigation. 
				' BSB 11.25.2002
				If bParentOnly Then
					If LCase(vArr(l)) <> LCase(Request("sys_formidname") & "") Then
						objParams.Add vArr(l),Request(vArr(l))
					End If
				Else
					objParams.Add vArr(l),Request(vArr(l))
				End If
				On Error Goto 0
			Next
		End If
	End If
	
	If Not bParentOnly Then
		' Id's for this forms are too comma delimited, can be only one
		If Request("sys_formidname")<>"" Then
			vArr=Split(Request("sys_formidname"),",")
			If IsArray(vArr) Then
				c=UBound(vArr)
				For l=0 To c
					On Error Resume Next
					objParams.Add vArr(l),Request(vArr(l))
					On Error Goto 0
				Next
			End If
		End If
	End If
	
	'tkr 5/2003 extra parameters in querystring
	If Request("sys_param")<>"" Then
		vArr=Split(Request("sys_param"),",")
		If IsArray(vArr) Then
			c=UBound(vArr)
			For l=0 To c
				On Error Resume Next
     			objParams.Add vArr(l),Request(vArr(l))
				On Error Goto 0
			Next
		End If
	End If
	
End Sub

' Get All Parent related Data from Request object and put it into the XML form definition
' Used when user issues the AddNew command on the child forms...
Public Sub ParentDataToForm(objXMLDoc)
	Dim vArr
	Dim c, l
	Dim objXMLElem
	
	' Parent Id's are comma delimited, can only one or none
	If Request("sys_formpidname")<>"" Then
		vArr=Split(Request("sys_formpidname"),",")
		If IsArray(vArr) Then
			c=UBound(vArr)
			For l=0 To c
				Set objXMLElem=objXMLDoc.selectSingleNode("//control[@name='" & LCase(vArr(l)) & "']")
				objXMLElem.Text=Request(vArr(l))
			Next
		End If
	End If
	
	' Additional data passed between forms
	If Request("sys_ex")<>"" Then
		vArr=Split(Request("sys_ex"),",")
		If IsArray(vArr) Then
			c=UBound(vArr)
			For l=0 To c
				Set objXMLElem=objXMLDoc.selectSingleNode("//control[@name='" & LCase(vArr(l)) & "']")
				If Not (objXMLElem Is Nothing) Then
					objXMLElem.Text=Request(vArr(l))
				End If
			Next
		End If
	End If
End Sub

' Remove All Child Nodes from XML Elemenet
Public Sub RemoveChildNodes(xmlElem)
	Dim objChildNode
	
	If xmlElem Is Nothing Then Exit Sub
	
	For Each objChildNode in xmlElem.childNodes
		xmlElem.removeChild objChildNode
	Next
	
End Sub

Public Function IsNewRecord(objXMLForm)
	Dim objXMLNode, sName, l
	IsNewRecord=False
	Set objXMLNode=objXMLForm.selectSingleNode("//internal[@name='sys_formidname']")
	If objXMLNode Is Nothing Then Exit Function
	sName="" & objXMLNode.getAttribute("value")
	If sName="" Then Exit Function
	Set objXMLNode=objXMLForm.selectSingleNode("//internal[@name='" & sName & "']")
	If objXMLNode Is Nothing Then Exit Function
	l=0
	If IsNumeric("" & objXMLNode.Text) And "" & objXMLNode.Text<>"" Then
		l=CLng("" & objXMLNode.Text)
	End If
	
	If l=0 Then IsNewRecord=True
End Function

Public Function IsNewRecordRequest()
	Dim sName, l
	IsNewRecordRequest=false
	
	sName="" & Request("sys_formidname")
	If sName="" Then Exit Function
	
	l=0
	If IsNumeric(Request(sName)) And Request(sName)<>"" Then l=CLng("" & Request(sName))
	
	if l=0 Then IsNewRecordRequest=true
	
End Function

Public Sub SystemDataToForm(objXMLDoc)
	Dim vArr
	Dim c, l
	Dim objXMLElem
	
	If objXMLDoc.xml="" Then Exit Sub
	
	' Parent Id's are comma delimited
	If Request("sys_formpidname")<>"" Then
		vArr=Split(Request("sys_formpidname"),",")
		If IsArray(vArr) Then
			c=UBound(vArr)
			For l=0 To c
				Set objXMLElem=objXMLDoc.selectSingleNode("//control[@name='" & LCase(vArr(l)) & "']")
				objXMLElem.Text=Request(vArr(l))
			Next
		End If
	End If
	
	' Id's are comma delimited
	If Request("sys_formidname")<>"" Then
		vArr=Split(Request("sys_formidname"),",")
		If IsArray(vArr) Then
			c=UBound(vArr)
			For l=0 To c
				Set objXMLElem=objXMLDoc.selectSingleNode("//control[@name='" & LCase(vArr(l)) & "']")
				If objXMLElem Is Nothing Then Response.Write "Control Name not found: " & LCase(vArr(l)): Response.End
				objXMLElem.Text=Request(vArr(l))
			Next
		End If
	End If
	
	' Additional data that are passed between forms
	If Request("sys_ex")<>"" Then
		vArr=Split(Request("sys_ex"),",")
		If IsArray(vArr) Then
			c=UBound(vArr)
			For l=0 To c
				Set objXMLElem=objXMLDoc.selectSingleNode("//control[@name='" & LCase(vArr(l)) & "']")
				If objXMLElem Is Nothing Then
					'Response.Write "SystemDataToForm Field not found. Control name:" & LCase(vArr(l))
					'Response.Write objXMLDoc.xml
					'Response.End
				Else
					objXMLElem.Text=Request(vArr(l))
				End If
			Next
		End If
	End If
	
	If Request("tpf")<>"" Then
		Set objXMLElem=objXMLDoc.selectSingleNode("//control[@name='tpf']")
		If Not objXMLElem is Nothing Then objXMLElem.Text=Request("tpf")
	End If
	
	If Request("tpfid")<>"" Then
		Set objXMLElem=objXMLDoc.selectSingleNode("//control[@name='tpfid']")
		If Not objXMLElem is Nothing Then objXMLElem.Text=Request("tpfid")
	End If

End Sub
'this proc is added to filter out the controls which r not visible regarding permissions
Public sub filterXmlControlsbySecurity(controlids,groupid)
	dim aryIds,objControls,xelm,i,objparent
	set objparent=objXML.selectSingleNode("//group[@id='" & groupid & "']")
	set objControls=objXML.selectNodes("//group[@id='" & groupid & "']/control")
	aryIds=split(controlids,",")
	
	if not objControls is nothing then
	
	for each xelm in objControls 
	'Response.Write xelm.getAttribute("id")
		for i=lbound(aryIds) to ubound(aryIds)
		
		if xelm.getAttribute("id")=aryIds(i) then
			'Response.Write xelm.getAttribute("id")
			objparent.removechild xelm
			
			
		end if
		next
		
	next'
	'Response.End
	end if
	
end sub

</script>