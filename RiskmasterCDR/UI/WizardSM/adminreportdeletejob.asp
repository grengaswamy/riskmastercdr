<!-- #include file ="sessionInitialize.asp" -->

<%Public objSession
initSession objSession
%> 
<script language="vbscript" runat="server">
Sub CancelRequest()
	Response.Redirect "adminreportqueue.asp"
	Response.End
End Sub
</script>

<%
Dim sJobList
Dim vArrJob, f, bOk
Dim sMoveTo
Dim objDocHandler
dim objNewDoc
dim r, henv, db, rs
dim sCompleteFilename, sFilename, iPos, sJobName, sJobDesc
dim fso

sJobList=Request("jobs")
If sJobList="" Then
	CancelRequest
End If

Set objDocHandler=CreateObject("RMDoc.CDocHandler")	
objDocHandler.DSN = objSessionStr("DSN")
objDocHandler.UserId=objSessionStr(SESSION_USERID)
objDocHandler.UserLoginName=objSessionStr(SESSION_LOGINNAME)
objDocHandler.AppDataPath = Application(APP_DATAPATH)
objDocHandler.UserDataPath=Application(APP_USERDIRECTORYHOME)

	set r= CreateObject("DTGRocket")
	henv = r.DB_InitEnvironment()
	db = r.DB_OpenDatabase(henv, Application("SM_DSN"), 0)

	' Loop and try to delete report output files
	Set fso = CreateObject("Scripting.FileSystemObject")
	rs = r.DB_CreateRecordset(db, "SELECT * FROM SM_JOBS WHERE COMPLETE <> 0 AND JOB_ID IN (" & sJobList & ")", 3, 0)
	while Not r.DB_EOF(rs)
		r.DB_GetData rs, "OUTPUT_PATH", sCompleteFilename
			
		if sCompleteFilename & "" <> "none" then
			on error resume next
			fso.DeleteFile sCompleteFilename, True
			on error goto 0
		end if
			
		r.DB_MoveNext rs
	wend
	r.DB_CloseRecordset CInt(rs), 2
	set fso = Nothing
				
	' Mark all the jobs as ARCHIVED so they don't show up in the report job list anymore
	r.DB_SQLExecute db, "DELETE FROM SM_JOBS WHERE JOB_ID IN (" & sJobList & ")"
		
	r.DB_CloseDatabase CInt(db)
	r.DB_FreeEnvironment CLng(henv)
		
	set r = Nothing
	Response.Redirect "adminreportqueue.asp"
	Response.End
 

Response.ExpiresAbsolute=DateAdd("h",-24,Now)
%>


