<!-- #include file ="customexecsummcode.inc" -->
<script language="VBScript" runat="server">
'**********************************************
' Begin Custom Report Queuing Code
' The following functions are provided to 
' integrate with the CriteriaEdit.asp page
' supporting custom report queuing.
' Author: Brian Battah 
' 3/8/02
'**********************************************

Public Const EXECUTIVESUMMARY = 350000
Public Const EXECUTIVESUMMARY_CONFIGURATION = 350001
Public Const CONFIGURATION_EVENTS = 351000
Public Const CONFIGURATION_CLAIMS_ALL = 352000
Public Const CONFIGURATION_CLAIMS_GC = 353000
Public Const CONFIGURATION_CLAIMS_VA = 354000
Public Const CONFIGURATION_CLAIMS_WC = 355000
Public Const CONFIGURATION_CLAIMS_DI = 356000




Function GenerateExecCriteriaHTML()
	Dim  objResult, objNode, objFrm, objOpt, i, objElt, objXML
	
	Set objResult = CreateObject("Microsoft.XMLDOM")
	
	Set objXML = CreateObject("Microsoft.XMLDOM")
	objXML.load Application(APP_DATAPATH) & "/executivesummary.xml" 'Pull a blank template	
	
	Set objXSL = CreateObject("Microsoft.XMLDOM")
	objXSL.load Application(APP_DATAPATH) & "/executivesummary.xsl" 'Pull a blank template	
	
	CleanXMLDefaults objXML
	CustomBeforeTransform(objXML)   'tkr 6/2003 add customization

	Response.ContentType = "text/html"
	'****************************************
	' Apply standard form XSL against criteria
	'*****************************************
	objXML.transformNodeToObject objXSL.documentElement,objResult

	'*****************************************************
	' Make some final adjustments to XSL output
	'*****************************************************
	'Fill LOB box from DB after transformation
	Set objElt = nothing
	Set objElt = objResult.selectSingleNode("//select[@name='lob']")
	If not objElt is nothing then GenerateLOB objElt
	objElt.setAttribute "onchange", "return onLOBChange();"
	
	'Add some friendly Defaults
	'Default to standard filtering record selection
	Set objElt = nothing
	Set objElt = objResult.selectSingleNode("//input[@name='filtermethod' && @value='standard']")
	If not objElt is nothing then objElt.setAttribute "checked","" 
	
	'Default to relative date range
	Set objElt = nothing
	Set objElt = objResult.selectSingleNode("//input[@name='datemethod' && @value='relative']")
	If not objElt is nothing then objElt.setAttribute "checked","" 

	'Default to no delta tracking
	Set objElt = nothing
	Set objElt = objResult.selectSingleNode("//input[@name='deltatrackingmethod' && @value='none']")
	If not objElt is nothing then objElt.setAttribute "checked","" 

	'Default Date scalar to 1
	Set objElt = nothing
	Set objElt = objResult.selectSingleNode("//input[@name='reviewperiodscalar']")
	If not objElt is nothing then objElt.setAttribute "value","1" 
	
	'Default Date Unit to Month
	Set objElt = nothing
	Set objElt = objResult.selectSingleNode("//select/option[@value='m']")
	If not objElt is nothing then objElt.setAttribute "selected","" 

	'Set "ReportType"  to Executive Summary
	'Note: The "//report" node has been XSL traslated away...
	'Set objNode = objResult.selectSingleNode("//report")
	'objNode.setAttribute "type","2"
	Set objElt = objResult.selectSingleNode("//input[@name = 'reporttype']")
	objElt.setAttribute "value","2"

	GenerateExecCriteriaHTML = objResult.xml
End Function

'********************************
' Test the Criteria by applying 
' them to the Engine and looking for errors.
'********************************
Function ApplyExecCriteria(ByRef sXML,ByRef errorHTML)
		ApplyExecCriteria = false
		Dim objMgr 'As ExecutiveSummary.CManager 
		Dim pEngine 'as IReportEngine
		Dim objErr 'as CError
		Dim sHTML 'As String
		Dim objXML 'As DOMDocument
		Dim e 'As CError
		
		Set objXML = CreateObject("Microsoft.XMLDOM")
		objXML.load Application(APP_DATAPATH) & "/executivesummary.xml" 'Pull a blank template

		Set objMgr = CreateObject("ExecutiveSummary.CManager")
		objMgr.DSN = objSessionStr(SESSION_DSN)
		objMgr.Init
		Set pEngine = objMgr.GetIReportEngine
		RequestToXML objXML 				'Fill the template
		AppendConfigXML objXML 
		
		On Error Resume Next
		Set pEngine.CriteriaXMLDom = objXML 'Apply the filled template
		sXML = pEngine.CriteriaXMLDom.xml
		On Error Goto 0
		For each e in objMgr.Errors
			errorHTML = errorHTML & "<li>" & e.Source & " : " & e.Description & "</li>"
		Next 
		If objMgr.Errors.Count = 0 Then ApplyExecCriteria = true
End Function

Public Sub AppendConfigXML(objXML)
	Dim objXMLTemp, sFileName
	sFileName = Application(APP_USERDIRECTORYHOME)
    If sFileName <> "" And Right(sFileName, 1) <> "\" Then sFileName = sFileName & "\"
    sFileName = sFileName & "execsummconfig_" & objSessionStr(SESSION_DSNID) & "_" & objSessionStr(SESSION_USERID) & ".xml"
    sFileName = LCase(sFileName)
	Set objXMLTemp = CreateObject("Microsoft.XMLDOM")
	If objXMLTemp.load(sFileName) Then 
		objXML.documentElement.insertBefore objXMLTemp.documentElement,objXML.documentElement.firstChild
	End If
End Sub

Public Sub GenerateLOB(objElt)
	Dim objOption 'As IXMLElement
	Dim objNodeList 'as IXMLNodeList
	Dim SQL 'As String
	Dim rs 'as long
	Dim i 'as long
	Set objNodeList = objElt.childNodes
	For i = 1 to objNodeList.length
		objElt.removeChild( objNodeList.item(i))
	next
	OpenDatabase
	SQL = "SELECT CODES_TEXT.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID AND TABLE_ID = 1027" 
	rs = objRocket.DB_CreateRecordset(hDb, SQL,3,0)
	While not objRocket.DB_EOF(rs)
		Set objOption = objElt.ownerDocument.createElement("option")
		objOption.setAttribute "value",GetData(rs,1)
		objOption.text = Cstr(GetData(rs,3)) & " (" & Cstr(GetData(rs,2)) & ")"
		objElt.appendChild objOption
		objRocket.DB_MoveNext CInt(rs)
	Wend
	objRocket.DB_CloseRecordset Cint(rs),1
	CloseDatabase
End Sub

Public Function bIsEnabled(secid)
	Dim  objSecurityTable
	bIsEnabled = True
	If objSessionStr(SESSION_MODULESECURITY)="1" Then
    	Set objSecurityTable=Application(APP_PERMCACHE & objSessionStr(SESSION_DSNID))
		If Not objSecurityTable.KeyExists(objSessionStr(SESSION_GROUPID) & "-" & secid) Then
			bIsEnabled = False
		End If
	End If
	Set objSecurityTable = Nothing
End Function


Public Function bExecSummEnabled()
	Dim  objSecurityTable
	bExecSummEnabled = True
	If objSessionStr(SESSION_MODULESECURITY)="1" Then
    	Set objSecurityTable=Application(APP_PERMCACHE & objSessionStr(SESSION_DSNID))
		If Not objSecurityTable.KeyExists(objSessionStr(SESSION_GROUPID) & "-" & EXECUTIVESUMMARY) Then
			bExecSummEnabled = False
		End If
	End If
	Set objSecurityTable = Nothing
End Function

Public Function bExecSummConfigEnabled()
	Dim  objSecurityTable
	bExecSummConfigEnabled = True
	If objSessionStr(SESSION_MODULESECURITY)="1" Then
    	Set objSecurityTable=Application(APP_PERMCACHE & objSessionStr(SESSION_DSNID))
		If Not objSecurityTable.KeyExists(objSessionStr(SESSION_GROUPID) & "-" & EXECUTIVESUMMARY_CONFIGURATION) Then
			bExecSummConfigEnabled = False
		End If
	End If
	Set objSecurityTable = Nothing
End Function

Public Function bEventsTabEnabled()
	Dim  objSecurityTable
	bEventsTabEnabled = True
	If objSessionStr(SESSION_MODULESECURITY)="1" Then
    	Set objSecurityTable=Application(APP_PERMCACHE & objSessionStr(SESSION_DSNID))
		If Not objSecurityTable.KeyExists(objSessionStr(SESSION_GROUPID) & "-" & CONFIGURATION_EVENTS) Then
			bEventsTabEnabled = False
		End If
	End If
	Set objSecurityTable = Nothing
End Function

Public Function bClaimsAllTabEnabled()
	Dim  objSecurityTable
	bClaimsAllTabEnabled = True
	If objSessionStr(SESSION_MODULESECURITY)="1" Then
    	Set objSecurityTable=Application(APP_PERMCACHE & objSessionStr(SESSION_DSNID))
		If Not objSecurityTable.KeyExists(objSessionStr(SESSION_GROUPID) & "-" & CONFIGURATION_CLAIMS_ALL) Then
			bClaimsAllTabEnabled = False
		End If
	End If
	Set objSecurityTable = Nothing
End Function

Public Function bClaimsGCTabEnabled()
	Dim  objSecurityTable
	bClaimsGCTabEnabled = True
	If objSessionStr(SESSION_MODULESECURITY)="1" Then
    	Set objSecurityTable=Application(APP_PERMCACHE & objSessionStr(SESSION_DSNID))
		If Not objSecurityTable.KeyExists(objSessionStr(SESSION_GROUPID) & "-" & CONFIGURATION_CLAIMS_GC) Then
			bClaimsGCTabEnabled = False
		End If
	End If
	Set objSecurityTable = Nothing
End Function

Public Function bClaimsVATabEnabled()
	Dim  objSecurityTable
	bClaimsVATabEnabled = True
	If objSessionStr(SESSION_MODULESECURITY)="1" Then
    	Set objSecurityTable=Application(APP_PERMCACHE & objSessionStr(SESSION_DSNID))
		If Not objSecurityTable.KeyExists(objSessionStr(SESSION_GROUPID) & "-" & CONFIGURATION_CLAIMS_VA) Then
			bClaimsVATabEnabled = False
		End If
	End If
	Set objSecurityTable = Nothing
End Function

Public Function bClaimsWCTabEnabled()
	Dim  objSecurityTable
	bClaimsWCTabEnabled = True
	If objSessionStr(SESSION_MODULESECURITY)="1" Then
    	Set objSecurityTable=Application(APP_PERMCACHE & objSessionStr(SESSION_DSNID))
		If Not objSecurityTable.KeyExists(objSessionStr(SESSION_GROUPID) & "-" & CONFIGURATION_CLAIMS_WC) Then
			bClaimsWCTabEnabled = False
		End If
	End If
	Set objSecurityTable = Nothing
End Function

Public Function bClaimsDITabEnabled()
	Dim  objSecurityTable
	bClaimsDITabEnabled = True
	If objSessionStr(SESSION_MODULESECURITY)="1" Then
    	Set objSecurityTable=Application(APP_PERMCACHE & objSessionStr(SESSION_DSNID))
		If Not objSecurityTable.KeyExists(objSessionStr(SESSION_GROUPID) & "-" & CONFIGURATION_CLAIMS_DI) Then
			bClaimsDITabEnabled = False
		End If
	End If
	Set objSecurityTable = Nothing
End Function

	Public Function initCustomEs(lclaimID,leventID)				
		set CustomesObj =  CreateObject("CustomexecSumm.CustomRMExecSummary")
		CustomesObj.FilePath=Application(APP_USERDIRECTORYHOME)
	 	CustomesObj.DSNID=objSessionStr(SESSION_DSNID)
	 	CustomesObj.USERID=objSessionStr(SESSION_USERID)
	 	CustomesObj.GROUPID=objSessionStr(SESSION_GROUPID)
	 	CustomesObj.DocType="http"
	 	If Not bIsEsAllowed() then
	 		CustomesObj.ConfigType=1   
	 	Else
	 		CustomesObj.ConfigType=2
	 	End IF		
	 		
		If lclaimid <> "" then
			CustomesObj.ClaimExecSumm objSessionStr("DSN"), lclaimID
		Else	
			CustomesObj.EventExecSumm objSessionStr("DSN"), leventID
		End if
			   
		set CustomesObj = Nothing	 		 			
	End Function	
</script>