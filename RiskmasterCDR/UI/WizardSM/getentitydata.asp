<!-- #include file ="session.inc" -->
<!-- #include file ="initdata.inc" -->

<%
Option Explicit
Public objSession
initSession objSession
%> 
<html><body onload="opener.document.entityLoaded('-1')">
<table align="center" width="100%" border="0">
	<tr>
		<td valign="center"><img src="img/anglobe.gif"></td>
		<td width="100%" valign="middle" align="center"><font name="Verdana,Arial" size="16"><b>Please wait...</b></font></td>
	</tr>
</table>
<%
Response.Flush
Dim objData
Dim s
Dim objXML, objXMLStyle
Dim sType, lEntityId

'tkr 9/2003.  if searching for patient will get patient_id, need get the entityid
sType=Request("type")
If sType="patient" then
	%>
	<!-- #include file ="dbutil.inc" -->
	<%
	lEntityId=GetSingleValue("PATIENT_EID", "PATIENT", "PATIENT_ID=" & Request("entityid"))
Else
	lEntityId=Request("entityid")
End If


Set objXMLStyle=CreateObject("Microsoft.XMLDOM")
objXMLStyle.Load Application("APP_DATA_PATH") & "formhidden.xsl"

Set objXML=CreateObject("Microsoft.XMLDOM")
objXML.Load Application("APP_DATA_PATH") & "entity.xml"

Set objData=CreateObject("FormDataManager.CData")
initCData(objData)

objData.Params.Add "entityid",lEntityId
objData.GetXMLData "entity",objXML,0

Response.Write objXML.transformNode(objXMLStyle.documentElement)
Set objXML=Nothing
Set objXMLStyle=Nothing
%>

</body></html>