<% option explicit %>
<!-- #include file ="session.inc" -->
<!-- #include file ="generic.asp" -->
<html>
	<head>
		<link rel="stylesheet" href="RMNet.css" type="text/css"/>

<%
Dim objCode,iCount,DBTableID,fieldType, pageError
Set objCode = Nothing

On Error Resume Next
Set objCode = Server.CreateObject("InitSMList.CRMCodeList")
On Error Goto 0

If objCode Is Nothing Then
	Call ShowError(pageError, "Call to CRMCodeList class failed.", True)
End If

objCode.m_RMConnectStr = objSessionStr(SESSION_DSN)

Dim arrCodes() 
DBTableID = Request.QueryString("DBTABLEID")
fieldType = Request.QueryString("FIELDTYPE")

objCode.GetCodeList CLng(DBTableID), 0, arrCodes

Dim m

'-- Rajeev -- 01/05/2004 -- Don't require to show as an error
'''If fieldType = "3" Then							
'''	If Not (Err.number <> 9 And m <> "" And m >=0) Then
'''		Call ShowError(pageError, "There are no codes available.", False)
'''	End If
'''End If
'''On Error Goto 0
%>
<html>
	<head>
		<link rel="stylesheet" href="RMNet.css" type="text/css"/>
		<title>SMD - [Criteria]</title>
		<script language="JavaScript" SRC="SMD.js"></script>

		<script language="JavaScript">
	
			function ok_click()
			{
				var txt = new Array;
				var txtCode = new Array;
				var arrcodeList = new Array;
				var temp;
				temp = '';
				var codeList;
				codeList = '';
				var oOption;
				if(document.frmData.lstCriteria.selectedIndex == -1)
				{
					window.close();
					return;
				}
				operatorList = 'IN';
				
				//-- Check for sub query
				if(document.frmData.chkSubquery.checked == true)
					hasDatadefcrit = 1;
				else
					hasDatadefcrit = 0;

				//-- Rajeev -- 03/25/2004 -- Check for hiding in RMNet
				if(document.frmData.chkHide.checked == true)
					hideInRMNet = 1;
				else
					hideInRMNet = 0;

				//-- Rajeev -- 03/25/2004 -- fill RMNet Label
				document.frmData.hdSelRMNetLabel.value = document.frmData.txtRMNetLabel.value;
				
				if(document.frmData.chkExcludeCodes.checked == true)
					codesExclude = "excludecodelist";
				else
					codesExclude = "codelist";		
				
				//-- fill pAttributes

				if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
				{
					if(window.opener.window.document.frmData.lstSelcriteria.length > 0)
						pAttributes = document.frmData.hdTableId.value + "|" + document.frmData.hdFieldId.value + "|" + codesExclude + "|" + "AND" + "|" + hasDatadefcrit + "|" + hideInRMNet + "|" + document.frmData.hdSelRMNetLabel.value;
					else
						pAttributes = document.frmData.hdTableId.value + "|" + document.frmData.hdFieldId.value + "|" + codesExclude + "|" + "none" + "|" + hasDatadefcrit + "|" + hideInRMNet + "|" + document.frmData.hdSelRMNetLabel.value;
				}
				else
				{
					var tempArray = window.document.frmData.hdSelCriteriaItem.value.split("~~~~~");
					var arrAttribute = tempArray[7].split("|");
					
					pAttributes = document.frmData.hdTableId.value + "|" + document.frmData.hdFieldId.value + "|" + codesExclude + "|" + arrAttribute[4] + "|" + hasDatadefcrit + "|" + hideInRMNet + "|" + document.frmData.hdSelRMNetLabel.value;
				}
					
			   	for(k = 0,selected = 0;k < document.frmData.lstCriteria.length;k++)
			   	{
			   		if(document.frmData.lstCriteria.item(k).selected)
			   		{
			   			txt[selected] = document.frmData.lstCriteria.item(k).text;
			   			selected = selected + 1;
			   		}
			   	}
			   	
			   	for(i=0;i<txt.length;i++)
			   	{
			   		txtCode = txt[i].split(" ");
			   		if(temp == '')
			   			temp = txtCode[0];
			   		else
			   			temp = temp + "," + txtCode[0];
			   	}
			   	
			   	arrcodeList = temp.split(",");
			   
			   	for(i=0;i<arrcodeList.length;i++)
			   	{
			   		if(codeList == '')
			   		{
			   			codeList = "'" + arrcodeList[0] + "'";
			   			dataList = arrcodeList[0];
			   			connectorList = 'none';
			   		}
			   		else
			   		{
			   			codeList = codeList + "," + "'" + arrcodeList[i] + "'";
			   			dataList = dataList + "^^^^^" + arrcodeList[i];
			   			connectorList = connectorList + ',' + 'or';
			   		}
			   	}
			   			
				if(document.frmData.chkExcludeCodes.checked == true)
					exlcodeVal = " NOT IN "
				else
					exlcodeVal = " IN "
				
				oOption = "(" + document.frmData.hdFieldName.value + " " + exlcodeVal + " " + "(" + codeList + ")" + ")" ;
				
				if(window.document.frmData.hdFieldType.value == 5)
				{
					oOption = oOption.replace("'TRUE'","1,-1");
					oOption = oOption.replace("'FALSE'","0");
				}
	
				var enterKey = String.fromCharCode(13, 10);
				
				if(document.frmData.hdSelCriteriaIndex.value == -1) //-- Add mode
				{
					if(window.opener.window.document.frmData.lstSelcriteria.length > 0)
						oOption = "AND" + " " + oOption;
				}
				else
				{	
					if(window.document.frmData.hdSelCriteriaIndex.selectedIndex > 0)
						oOption = "AND" + " " + oOption;			
				}

				//-- code to handle the outer connector while editing
				if(document.frmData.hdSelCriteriaIndex.value != -1)		
				{	
					var arrPAttributes = pAttributes.split("|");
						
					if(arrPAttributes[3] != "none")
						oOption = arrPAttributes[3].toUpperCase() + " " + oOption;
				}
				window.opener.criteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,operatorList,dataList,connectorList,document.frmData.hdFieldName.value,document.frmData.hdFieldType.value,document.frmData.hdDBTableId.value,pAttributes,(oOption+enterKey));			
				window.opener.OLEcriteriaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,'','', false);
				window.opener.dateFormulaArray(window.opener.window.document.frmData.cboSelfield.selectedIndex,window.document.frmData.hdSelCriteriaIndex.value,'','', false);
				window.opener.displayCriteria();
				window.opener.displayGlobalCriteria();
				window.opener.changeFieldName(window.document.frmData.hdSelCriteriaIndex.value);
				window.opener.ButtonState();

				if(document.frmData.hdSelCriteriaIndex.value != -1)		
					window.opener.window.document.frmData.lstSelcriteria.selectedIndex = window.document.frmData.hdSelCriteriaIndex.value;
				window.close();
			}
	
			function Subquery()
			{
				trackChange = 0;
			}

			function window_onLoad()
			{
				// Start Naresh MITS 7836 09/11/2006
				var objCheckBox=eval("document.frmData.chkHide");
				objCheckBox.style.display='none';
				var objTextBox=eval("document.frmData.txtRMNetLabel");
				objTextBox.style.display='none';
				// End Naresh MITS 7836 09/11/2006
				if(window.document.frmData.hdSelSubQuery.value == 1)
					window.document.frmData.chkSubquery.checked = true;

				if(window.document.frmData.hdSelHide.value == 1)
					window.document.frmData.chkHide.checked = true;

				if(window.document.frmData.hdSelRMNetLabel.value != '')
					window.document.frmData.txtRMNetLabel.value = window.document.frmData.hdSelRMNetLabel.value;
				
				if(document.frmData.hdSelCriteriaItem.value != '')
				{
					var arrPCriteria;
					arrPCriteria = document.frmData.hdSelCriteriaItem.value.split("~~~~~");
					arrData = arrPCriteria[2].split("^^^^^");
					
					var temp = '';
					var arrIndex = new Array;
			   		for(i=0;i<document.frmData.lstCriteria.length;i++)
			   		{
			   			txtCode =  document.frmData.lstCriteria.options[i].text.split(" ");
			   			if(temp == '')
			   				temp = txtCode[0];
			   			else
			   				temp = temp + "," + txtCode[0];
			   		}
			   		if (temp != '')
			   		{
			   			arrCodes = temp.split(",");
			   		
						for(j=0,k=0;j<arrData.length;j++)
						{	
							for(i=0;i<arrCodes.length;i++)				
							{
								if(arrData[j] == arrCodes[i])
								{	
									document.frmData.lstCriteria.options[i].selected = true;
									break;
								}
							}
						}
					}
					
					pAttributes = arrPCriteria[7];
					arrAttributes = pAttributes.split("|");
					bSubquery = arrAttributes[arrAttributes.length-1];
					sType = arrAttributes[3];
					
					if(sType == 'excludecodelist')
						document.frmData.chkExcludeCodes.checked = true;
					else
						document.frmData.chkExcludeCodes.checked = false;
						
					//if(bSubquery == 1)
					//	document.frmData.chkSubquery.checked = true;
					//else
					//	document.frmData.chkSubquery.checked = false;
				}
			}
				
		</script>
	</head>
	
	<body onLoad="window_onLoad();">
	<form name="frmData" topmargin="0" leftmargin="0" onSubmit="return false;">
		<table class="formsubtitle" border="0" width="100%" align="center">
			<tr>
				<td class="ctrlgroup" width="33%">Criteria :</td>
			</tr>
		</table>					

		<table width="100%" border="0">
			<tr><td colspan="3"><b><%=Response.Write(Request.QueryString("FIELDNAME"))%></b></td></tr>
			<tr>
				<td width="33%"></td>
				<td width="33%"><input type="checkbox" value1="" name="chkSubquery" onChange="Subquery()">Subquery Criteria</td>
				<td width="33%"><input type="checkbox" value1="" name="chkExcludeCodes" onChange="Subquery()" >Exclude Codes</td>
			</tr>
			<tr>
				<td colspan="3">
					<select  name="lstCriteria" style="WIDTH:100%" size="15" multiple>
					<%
						If fieldType = "3" Or fieldType = "4" Then							
							'''If Err.number <> 9 And m <> "" And m >=0 Then
							If IsValidArray(arrCodes) Then
								For iCount=0 to UBound(arrCodes)
									Response.Write "<option>" & arrCodes(iCount) & "</option>"
								Next
							End If
						End If

						If fieldType = "5" Then
							Response.Write "<option>TRUE Yes</option><option>FALSE No</option>"
						End If
					%>	
					</select>
				</td>
			</tr>
			<tr><!-- Rajeev -- 03/25/2004 -- Added feature of RMNet labelling-->
				<td>
					<!-- Start Naresh MITS 7836 09/11/2006-->
					<input type="checkbox" name="chkHide"><!--Hide in RMNet-->
					<!-- End Naresh MITS 7836 09/11/2006-->
				</td>
				<!-- Start Naresh MITS 7836 09/11/2006-->
				<td colspan="2" align="right"><!--RMNet label:-->
					<!-- End Naresh MITS 7836 09/11/2006-->
					<input type="text" name="txtRMNetLabel" value="" onChange1="window.close()">
				</td>
			</tr>
		</table>
		<hr>
		<table border="0" width="100%" align="center" cellspacing="0" cellpadding="0">
			<tr>
				<td align="middle">
					<input class="button" type="button" style1="WIDTH: 30%" name="ok" value="    OK    " onClick="ok_click()">
					<input class="button" type="button" style1="WIDTH: 30%" name="cancel" value="Cancel" onClick="window.close()">
					<input class="button" type="button" style1="WIDTH: 30%" name="help" value="  Help  ">
				</td>
			</tr>
		</table>
		
		<input type="hidden" name="hdFieldName" value="<%=Request.QueryString("FIELDNAME")%>">
		<input type="hidden" name="hdTableId" value="<%=Request.QueryString("TABLEID")%>">
		<input type="hidden" name="hdFieldId" value="<%=Request.QueryString("FIELDID")%>">
		<input type="hidden" name="hdFieldType" value="<%=Request.QueryString("FIELDTYPE")%>">
		<input type="hidden" name="hdDBTableId" value="<%=Request.QueryString("DBTABLEID")%>">
		<input type="hidden" name="hdSelCriteria" value="<%=fixDoubleQuote(Request.QueryString("CRITERIA"))%>">
		<input type="hidden" name="hdSelCriteriaIndex" value="<%=Request.QueryString("CRITERIAINDEX")%>">
		<input type="hidden" name="hdSelSubQuery" value="<%=Request.QueryString("SUBQUERY")%>">
		<input type="hidden" name="hdSelHide" value="<%=Request.QueryString("HIDEINRMNET")%>">
		<input type="hidden" name="hdSelRMNetLabel" value="<%=fixDoubleQuote(Request.QueryString("RMNETLABEL"))%>">
		<input type="hidden" name="hdSelCriteriaItem" value="<%=fixDoubleQuote(Request.QueryString("PCRITERIA"))%>">
	
	</form>
	</body>
</html>