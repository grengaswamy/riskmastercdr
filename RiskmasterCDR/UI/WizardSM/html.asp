<html>
	<head>
		<link rel="stylesheet" href="RMNet.css" type="text/css" />
		<title>SMD - [HTML Options]</title>

		<!--<script language="JavaScript" SRC="form.js"></script>-->
		<script language="JavaScript">
			function SelectType()
			{
				if (window.document.frmData.optPage[1].checked == true)
				{
					window.document.frmData.txtLinesPP.disabled = false;
					window.document.frmData.chkPageBreak.disabled = false;
				}
				else if (window.document.frmData.optPage[0].checked == true)
				{
					window.document.frmData.txtLinesPP.disabled = true;
					window.document.frmData.txtLinesPP.value = '';
					window.document.frmData.chkPageBreak.disabled = true;
					window.document.frmData.chkPageBreak.checked = false;
				}
			}
			
			function Enable()
			{
				if (window.document.frmData.chkExport.checked == true)
				{
					window.document.frmData.optExport[0].disabled = false;
					window.document.frmData.optExport[1].disabled = false;
					window.document.frmData.optGraph[0].disabled = false;
					window.document.frmData.optGraph[1].disabled = false;
				}
				else
				{
					window.document.frmData.optExport[0].disabled = true;
					window.document.frmData.optExport[1].disabled = true;
					window.document.frmData.optGraph[0].disabled = true;
					window.document.frmData.optGraph[1].disabled = true;
					
					window.document.frmData.optExport[0].checked = true;
					window.document.frmData.optExport[1].checked = false;
					window.document.frmData.optGraph[0].checked = true;
					window.document.frmData.optGraph[1].checked = false;
				}
			}
			
			function HTMLSubmit()
			{
				if(window.document.frmData.optPage[1].checked == true && (isNaN(window.document.frmData.txtLinesPP.value) || (window.document.frmData.txtLinesPP.value < 1 || window.document.frmData.txtLinesPP.value > 32767)))
				{
					alert('Invalid number of lines per page specified.Please enter a value between 1 and 32767');
					window.document.frmData.txtLinesPP.focus();
					return;
				}
				if (window.document.frmData.optPage[0].checked == true)
					window.opener.frmData.hdHtmlPageFormat.value = "single";
				else
					window.opener.frmData.hdHtmlPageFormat.value = "multiple";
					
				if (window.document.frmData.txtLinesPP.value != '')
					window.opener.frmData.hdHtmlNumLinesPerPage.value = parseInt(window.document.frmData.txtLinesPP.value);
				else
					window.opener.frmData.hdHtmlNumLinesPerPage.value = 0;
	
				if (window.document.frmData.chkPageBreak.checked == true)
					window.opener.frmData.hdHtmlPageBreak.value = 1;
				else
					window.opener.frmData.hdHtmlPageBreak.value = 0;
	
				if (window.document.frmData.chkExport.checked == true)
					window.opener.frmData.hdHtmlExportGraph.value = 1;
				else
					window.opener.frmData.hdHtmlExportGraph.value = 0;
				
				
				if (window.document.frmData.optExport[0].checked == true)
					window.opener.frmData.hdHtmlGraphSameFile.value = 1;
				else
					window.opener.frmData.hdHtmlGraphSameFile.value = 0;
				
				if (window.document.frmData.optGraph[0].checked == true)
					window.opener.frmData.hdHtmlGraphTopOfPage.value = 1;
				else
					window.opener.frmData.hdHtmlGraphTopOfPage.value = 0;
					
				window.close();			
		}
		
		function body_onload()
		{
			if (window.opener.frmData.hdHtmlPageFormat.value == "single")
				window.document.frmData.optPage[0].checked = true;
			else
				window.document.frmData.optPage[1].checked = true;
			
			window.document.frmData.txtLinesPP.value = window.opener.frmData.hdHtmlNumLinesPerPage.value;
			
			if (window.opener.frmData.hdHtmlPageBreak.value == 1)
				window.document.frmData.chkPageBreak.checked = true;
			else
				window.document.frmData.chkPageBreak.checked = false;
				
			if (window.opener.frmData.hdHtmlExportGraph.value == 1)
				window.document.frmData.chkExport.checked = true;
			else
				window.document.frmData.chkExport.checked = false;
				
			if (window.opener.frmData.hdHtmlGraphSameFile.value == 1)
				window.document.frmData.optExport[0].checked = true;
			else
				window.document.frmData.optExport[1].checked = true;
				
			if (window.opener.frmData.hdHtmlGraphTopOfPage.value == 1)
				window.document.frmData.optGraph[0].checked = true;
			else
				window.document.frmData.optGraph[1].checked = true;
		
			SelectType();
			Enable();
				
		}
		
		function updateSerialNum()
		{
			window.opener.checkSerialNum(0);
		} 

		</script>
	</head>
  
	<body class="8pt" onLoad="body_onload()" onBlur1="window.focus()" >
		<form name="frmData">
		<table class="formsubtitle" border="0" width="100%">
			<tr>
				<td class="ctrlgroup">Page Format</td>
			</tr>
		</table>

		<table border="0" width="100%">
			<tr>
				<td align="left" width="50%">
				<input type="radio" name="optPage"  value="0" onClick="SelectType();updateSerialNum();">Single Page</td>
				<td><input type="radio" name="optPage"    value="1" onClick="SelectType();updateSerialNum();">Multiple Pages</td>
			</tr>
			<tr>
				<td align="left">&nbsp;&nbsp;Lines Per Page:&nbsp;<input type="text" size="25%" value="" maxlength=5 name="txtLinesPP" disabled onChange="updateSerialNum();"></td>
				<td><input type="checkbox" value="" name="chkPageBreak" disabled onChange="updateSerialNum();">New File On Page Break</td>
			</tr>
		</table>

		<table class="formsubtitle" border="0" width="100%">
			<tr>
				<td class="ctrlgroup">Export</td>
			</tr>
		</table>	

		<table width="100%" border="0">
			<tr>
				<td><input type="checkbox" value="1" name="chkExport" colspan="3" onClick="Enable();updateSerialNum();">Export Graph</td>
			</tr>
			<tr></tr>
			<tr>
				<td width="33%">&nbsp;&nbsp;Export To:</td>
				<td width="33%"><input type="radio" value="0"  name="optExport" disabled onClick="updateSerialNum();">Same File</td>
				<td width="33%"><input type="radio" value="1"  name="optExport" disabled onClick="updateSerialNum();">Different File</td>
			</tr>
		</table>

		<table width="100%" border="0">
			<tr>
				<td width="33%">&nbsp;&nbsp;Graph Location:</td>
				<td width="33%"><input type="radio"  name="optGraph"  disabled onClick="updateSerialNum();">Top</td>
				<td width="33%"><input type="radio"  name="optGraph"  disabled onClick="updateSerialNum();">Bottom</td>
			</tr>
		</table>
	<hr>
		<table width="100%" border="0">
			<tr>
				<td width="20%">&nbsp;</td>
				<td width="60%" align="center">
					<input type="button" class="button" style="width:20%" name="up" value="OK" onClick="HTMLSubmit()">
					<input type="button" class="button" style="width:20%" name="up" value="Cancel" onClick="javascript:window.close()">
				</td>
				<td width="20%">&nbsp;</td>
			</tr>
		</table>
		</form>
	</body>
</html>