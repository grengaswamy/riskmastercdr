<script language="VBScript" runat="server">
'DCOM.inc
'Author Brian Battah
'Supports Chubb DCOM Implementation


'Uses Modified C++ in DTGSendFile and DTGSendFileLocal
'in concert to pull a Dcom'd file (FullFileName)
'from the dcom server box to the web-server box.
'Author: Brian Battah
Function PullFileFromDCOMServer(FullFileName)
	Dim oSendLocal 'As SENDFILELOCALLib.DTGSendFileLocal 'Object
	Dim oSend 'As SENDFILELib.DTGSendFile 'Object
	Dim fso 'As Scripting.FileSystemObject
	Dim v 'As Variant (Holds SafeArray o' bytes)
	Dim cbRead 'As Variant (holds num o' bytes read)
	Dim cbTotal 'As Variant (holds num o' bytes to read)
	Dim cbSaved 'As Variant (holds num o' bytes saved)
	Dim SaveAsFileName 'As String
	Dim bAppend 'As Boolean (Choose to over-write\append to existing file content.)
	Dim size 
	
	Set fso = CreateObject("Scripting.FileSystemObject")
	Set oSend = CreateObject("DTGSendFile.1")
	Set oSendLocal = CreateObject("SendFileLocal.DTGSendFileLocal")
	
	SaveAsFileName = fso.GetSpecialFolder(2) & "\" & fso.GetFileName(FullFileName)
	
	'Have the remote objSendFile give open up the file from the DCOM 
	'box file system and hand it back to us in chunks so we can 
	' temporarily store and pass it out from the web-server box.
	On Error Resume Next
	size = oSend.OpenFile(FullFileName)
	If Err.number <> 0 Then
		Response.Write Err.Description & " File " & FullFileName & " was not found."
		Response.End
	End If
	On Error Goto 0
	bAppend = False
	Do
   	v = oSend.ReadFile(8192, cbRead)
		If cbRead <> 0 Then
			oSendLocal.SaveFile SaveAsFileName,v,(bAppend)
		End If
	bAppend = True
	Loop While cbRead > 0
	oSend.CloseFile
	
	'Clean Up DCOM Server Scratch File
	On Error Resume Next
	oSend.KillFile FullFileName
	On Error Goto 0
	PullFileFromDCOMServer = SaveAsFileName
End Function

'Uses Modified C++ in DTGSendFile and DTGSendFileLocal
'in concert to push an uploaded file (FullFileName)
'from the web server box to the dcom box.
'Author: Brian Battah
Function ForwardFileToDCOMServer(FullFileName)
	Dim oSendLocal 'As SENDFILELOCALLib.DTGSendFileLocal 'Object
	Dim oSend 'As SENDFILELib.DTGSendFile 'Object
	Dim fso 'As Scripting.FileSystemObject
	Dim v 'As Variant (Holds SafeArray o' bytes)
	Dim cbRead 'As Variant (holds num o' bytes read)
	Dim cbTotal 'As Variant (holds num o' bytes to read)
	Dim cbSaved 'As Variant (holds num o' bytes saved)
	
	Set fso = CreateObject("Scripting.FileSystemObject")
	Set oSend = CreateObject("DTGSendFile.1")
	Set oSendLocal = CreateObject("SendFileLocal.DTGSendFileLocal")
	
	cbTotal = fso.GetFile(FullFileName).Size
	oSendLocal.OpenFile FullFileName
	v = oSendLocal.ReadFile(clng(cbTotal), cbRead)
	oSendLocal.CloseFile
	
	cbSaved = oSend.SaveFile(FullFileName,clng(cbTotal),v)
	ForwardFileToDCOMServer = ((cbTotal=cbRead) and (cbTotal=cbSaved))
End Function
</script>