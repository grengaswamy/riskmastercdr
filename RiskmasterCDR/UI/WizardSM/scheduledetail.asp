<!-- #include file ="session.inc" -->
<!-- #include file ="dbutil.inc" -->
<!-- #include file ="customcriteria.inc" -->
<%Option Explicit

Public objSession
initSession objSession
Response.ExpiresAbsolute=DateAdd("h",-24,Now)

Dim lScheduleType
Dim hRS
Dim sSQL, v, lReportId
Dim sDataPath, sFormFile
'Dim objXML 'Declared elsewhere BB 03/18/02
Dim objXMLStyle
Dim objForm
Dim lSysCommand
Dim bViewOnly
Dim lScheduleId, bTypeChanged,lnewTypeReport
bViewOnly=Request("viewonly")
lScheduleId=Request("scheduleid")
bTypeChanged="0" & Request("typechanged")
lScheduleType=Request("scheduletype")
'Add by kuladeep for editable report mits:27574 Start
Dim  sXMLData,sReportXML1,RptDataNode 
Dim asofdate,enforce180dayrule,printsofterrlog,eventbasedflag,printoshadescflag,columnesource
Dim columnfsource,sortorder,allentitiesflag,allentitiesflag2,selectedentities
Dim datemethod,yearofreport,datemethod2,begindate,enddate,reportTypeId
Dim byoshaestablishmentflag,byoshaestablishmentflag2,usereportlevel,reportlevel,establishmentnameprefix,allselecteddata
'Add by kuladeep for editable report mits:27574 End

'Report 301
Dim preparertitle,preparername,preparerphone,primarylocationflag,preparersignaturedate

Function IIf(bBool, bTrue, bFalse)
	if bBool then IIf = bTrue else IIf = bFalse
End Function

If lScheduleId=0 Then
	Response.Write "Invalid Parameters. ScheduleId not found."
	Response.End
End If

If LCase(Request.ServerVariables("REQUEST_METHOD"))="post" Then

	Dim sPath, sHTTP, objSMI, l, errorHTML, sURL

	'Set objSMI = CreateObject("SMInterface.SMI")

	If ucase(Request.ServerVariables("HTTPS")) = "ON" then
		sHTTP = "https://"
	else
		sHTTP = "http://"
	End If
	
	sPath=Request.ServerVariables("SCRIPT_NAME")
	l=InStrRev(sPath,"/")
	If l>0 Then
		sPath=Left(sPath,l)
	Else
		sPath="/"
	End If
	
	sURL = sHTTP & Request.ServerVariables("SERVER_NAME") & sPath & "smrepserve.asp?DPT=" & objSessionStr(SESSION_DOCPATHTYPE)& ""
	If SaveScheduledReport( objSessionStr(SESSION_LOGINNAME), objSessionStr(SESSION_USERID), objSessionStr(SESSION_DSN), Application(APP_SMDSN), objSessionStr(SESSION_DOCPATH), sURL, errorHTML) Then
		Response.Redirect "schedulelist.asp"
	Else 'Handle form submission error.%>
<html>
<head>
    <title></title>
    <!--[Rajeev 11/27/2002 CSS Consolidation] link rel="stylesheet" href="sm.css" type="text/css" /-->
    <link rel="stylesheet" href="RMNet.css" type="text/css" />
</head>
<body class="10pt">
    <b><font color="red">There were errors in the submitted report criteria. Please hit
        the Back button on your browser to correct the mistakes and submit again.<br />
        <br />
    </font></b>
    <%="<ul>" & errorHTML & "</ul>"%>
</body>
</html>
<%	End If	
	Response.End
	' Replaced 3/18/02 BB
	'objSMI.SaveScheduledReport objSessionStr(SESSION_LOGINNAME), objSessionStr(SESSION_USERID), objSessionStr(SESSION_DSN), Application(APP_SMDSN), objSessionStr(SESSION_DOCPATH), sHTTP & Request.ServerVariables("SERVER_NAME") & sPath & "smrepserve.asp?file="
	'set objSMI=nothing
	'Response.Redirect "schedulelist.asp"
	'Response.End
End If

'Sub OpenDatabase()
'	If objRocket Is Nothing Then
'		Set objRocket = CreateObject("DTGRocket")
'		hEnv = objRocket.DB_InitEnvironment()
'		hDB = objRocket.DB_OpenDatabase(henv, Application(APP_SMDSN), 0)
'	End If
'End Sub
'Sub CloseDatabase()
'	If Not objRocket Is Nothing Then
'		objRocket.DB_CloseDatabase CInt(hDB)
'		objRocket.DB_FreeEnvironment CLng(hEnv)
'		Set objRocket = Nothing
'	End If
'End Sub

OpenSMDatabase

sSQL="SELECT SM_SCHEDULE.*, SM_REPORTS.REPORT_NAME FROM SM_SCHEDULE,SM_REPORTS WHERE SM_SCHEDULE.REPORT_ID=SM_REPORTS.REPORT_ID AND SCHEDULE_ID=" & Request("scheduleid")
hRS = objSMRocket.DB_CreateRecordset(hSMDB, sSQL, 3, 0)
If objSMRocket.DB_Eof(hRS) Then
	objSMRocket.DB_CloseRecordset CInt(hRS), 2
	Response.Write "Invalid Schedule Id"
	Response.End
End If

objSMRocket.DB_GetData hRS, "REPORT_ID", lReportId
	'If bTypeChanged<>1 Then
        If lScheduleType > 0 Then
		    'objSMRocket.DB_GetData hRS, "SCHEDULE_TYPE", lScheduleType
        else
            objSMRocket.DB_GetData hRS, "SCHEDULE_TYPE", lScheduleType
        End If
        ' Start:Add by kuladeep for editable report mits:27574
        objSMRocket.DB_GetData hRS, "REPORT_XML",sReportXML1 
        Set sXMLData = CreateObject("Microsoft.XMLDOM")
            sXMLData.loadXML sReportXML1 
            lnewTypeReport="0"     
           'For Suporting Old report so we put condition for check old/new report
           For Each RptDataNode In sXMLData.selectNodes("/report/form/group")
               If(RptDataNode.getAttribute("title") = "OSHA Information [By OSHA Establishment]") Then

                         If(RptDataNode.selectNodes("control").[0].getAttribute("id")="byoshaestablishmentflag1") Then
                              lnewTypeReport = "1"
                          End If                              
               End If        
           Next 

           If(lnewTypeReport="1") Then
            reportTypeId = sXMLData.selectsingleNode("/report/@type").value
            'For checking report type
            If(reportTypeId="1") Then' Regarding Report type 1

            For Each RptDataNode In sXMLData.selectNodes("/report/form/group")
                 
                 If(RptDataNode.getAttribute("title") = "OSHA Information [By OSHA Establishment]") Then

                     If(RptDataNode.selectNodes("control").[0].getAttribute("name")="byoshaestablishmentflag") Then
                            byoshaestablishmentflag = RptDataNode.selectNodes("control").[0].getAttribute("checked")
                             If(byoshaestablishmentflag = "") Then
                             byoshaestablishmentflag=true
                             End IF

                      End If               
                 End If

                 If(RptDataNode.getAttribute("title") = "OSHA Information [By Organizational Hierarchy]") Then
                    
                     If(RptDataNode.selectNodes("control").[0].getAttribute("name")="byoshaestablishmentflag") Then
                            byoshaestablishmentflag2 = RptDataNode.selectNodes("control").[0].getAttribute("checked")
                             If(byoshaestablishmentflag2 = "") Then
                             byoshaestablishmentflag2=true
                             End IF
                      End If
                     If(RptDataNode.selectNodes("control").[1].getAttribute("name")="usereportlevel") Then
                            usereportlevel = RptDataNode.selectNodes("control").[1].getAttribute("codeid")
                      End If
                     If(RptDataNode.selectNodes("control").[2].getAttribute("name")="reportlevel") Then
                            reportlevel = RptDataNode.selectNodes("control").[2].getAttribute("codeid")
                      End If
                        If(RptDataNode.selectNodes("control").[3].getAttribute("name")="establishmentnameprefix") Then
                            establishmentnameprefix = RptDataNode.selectNodes("control").[3].getAttribute("codeid")
                        End If  
                  End If            

                  If(RptDataNode.getAttribute("name") = "Date Information") Then
                    
                         If(RptDataNode.selectNodes("control").[0].getAttribute("name")="datemethod") Then
                            datemethod = RptDataNode.selectNodes("control").[0].getAttribute("checked")
                            If(datemethod = "") Then
                             datemethod=true
                             End IF
                         End If
                         If(RptDataNode.selectNodes("control").[1].getAttribute("name")="yearofreport") Then
                            yearofreport = RptDataNode.selectNodes("control").[1].getAttribute("codeid")
                         End If
                         If(RptDataNode.selectNodes("control").[2].getAttribute("name")="datemethod") Then
                            datemethod2 = RptDataNode.selectNodes("control").[2].getAttribute("checked")
                            If(datemethod2 = "") Then
                             datemethod2=true
                             End IF
                         End If
                         If(RptDataNode.selectNodes("control").Length > 3) Then
                         If(RptDataNode.selectNodes("control").[3].getAttribute("name")="begindate") Then
                            begindate = RptDataNode.selectNodes("control").[3].getAttribute("value")
                             If "" & begindate<>"" Then
                                begindate = FormatDateTime(DateSerial(Left(begindate,4),Mid(begindate,5,2),Mid(begindate,7,2)),vbGeneralDate) 
                            End If
                         End If
                         End If
                         If(RptDataNode.selectNodes("control").Length > 3) Then
                         If(RptDataNode.selectNodes("control").[4].getAttribute("name")="enddate") Then
                            enddate = RptDataNode.selectNodes("control").[4].getAttribute("value")
                            If "" & enddate<>"" Then
                               enddate = FormatDateTime(DateSerial(Left(enddate,4),Mid(enddate,5,2),Mid(enddate,7,2)),vbGeneralDate)
                            End If 
                         End If
                         End If
                     End If

                 if(RptDataNode.getAttribute("name") = "Data Options") Then
                    
                     If(RptDataNode.selectNodes("control").[0].getAttribute("name")="asofdate") Then
                        asofdate = RptDataNode.selectNodes("control").[0].getAttribute("value")
                        If "" & asofdate<>"" Then
                            asofdate = FormatDateTime(DateSerial(Left(asofdate,4),Mid(asofdate,5,2),Mid(asofdate,7,2)),vbGeneralDate) 
                        End If
                     End If
                     If(RptDataNode.selectNodes("control").[1].getAttribute("name")="enforce180dayrule") Then
                        enforce180dayrule = RptDataNode.selectNodes("control").[1].getAttribute("checked")
                       If(enforce180dayrule = "") Then
                             enforce180dayrule =true
                             End IF
                     End If
                     If(RptDataNode.selectNodes("control").[2].getAttribute("name")="printsofterrlog") Then
                        printsofterrlog = RptDataNode.selectNodes("control").[2].getAttribute("checked")
                         If(printsofterrlog = "") Then
                             printsofterrlog =true
                             End IF
                     End If
                     If(RptDataNode.selectNodes("control").[3].getAttribute("name")="eventbasedflag") Then
                        eventbasedflag = RptDataNode.selectNodes("control").[3].getAttribute("checked")
                        If(eventbasedflag = "") Then
                             eventbasedflag =true
                             End IF
                     End If
                     If(RptDataNode.selectNodes("control").[4].getAttribute("name")="printoshadescflag") Then
                        printoshadescflag = RptDataNode.selectNodes("control").[4].getAttribute("checked")
                         If(printoshadescflag = "") Then
                             printoshadescflag =true
                             End IF
                     End If
                     If(RptDataNode.selectNodes("control").[5].getAttribute("name")="columnesource") Then
                        columnesource = RptDataNode.selectNodes("control").[5].getAttribute("codeid")
                     End If
                     If(RptDataNode.selectNodes("control").[6].getAttribute("name")="columnfsource") Then
                        columnfsource=RptDataNode.selectNodes("control").[6].getAttribute("codeid")
                     End If
                    If(RptDataNode.selectNodes("control").[7].getAttribute("name")="sortorder") Then
                        sortorder=RptDataNode.selectNodes("control").[7].getAttribute("codeid")
                    End If
                    If(RptDataNode.selectNodes("control").[8].getAttribute("name")="allentitiesflag") Then
                        allentitiesflag=RptDataNode.selectNodes("control").[8].getAttribute("checked")
                         If(allentitiesflag="")Then
                            allentitiesflag=true
                         End If
                    End If

                   If(RptDataNode.selectNodes("control").[9].getAttribute("name")="allentitiesflag")Then
                         allentitiesflag2=RptDataNode.selectNodes("control").[9].getAttribute("checked")
                          If(allentitiesflag2="")Then
                            allentitiesflag2=true
                         End If
                   End If
                   

                    If(RptDataNode.selectNodes("control").[10].getAttribute("name")="selectedentities") Then
                         selectedentities=RptDataNode.selectNodes("control").[10].getAttribute("value")

                          Dim CtlRptNode1
                          Dim valueCodelistRpt1

                          For Each CtlRptNode1 In RptDataNode.selectNodes("control").[10].selectNodes("option")                                             
                             If "" & CtlRptNode1.text<>"" Then
                                valueCodelistRpt1 = valueCodelistRpt1 +"||"+ CtlRptNode1.getAttribute("value")+"#"+CtlRptNode1.text
                             End If
                         Next
                         allselecteddata = valueCodelistRpt1

                    End If


                 End If
            next 

       End If


        If(reportTypeId="4") Then ' Regarding Report type 2

               For Each RptDataNode In sXMLData.selectNodes("/report/form/group")
                 
                 If(RptDataNode.getAttribute("title") = "Preparer Information") Then

                     If(RptDataNode.selectNodes("control").[0].getAttribute("name")="preparertitle") Then
                            preparertitle = RptDataNode.selectNodes("control").[0].getAttribute("value")
                     End If  
                      If(RptDataNode.selectNodes("control").[1].getAttribute("name")="preparername") Then
                            preparername = RptDataNode.selectNodes("control").[1].getAttribute("value")
                     End If  
                      If(RptDataNode.selectNodes("control").[2].getAttribute("name")="preparerphone") Then
                            preparerphone = RptDataNode.selectNodes("control").[2].getAttribute("value")
                     End If               
                 End If


                 If(RptDataNode.getAttribute("title") = "OSHA Information [By OSHA Establishment]") Then

                     If(RptDataNode.selectNodes("control").[0].getAttribute("name")="byoshaestablishmentflag") Then
                            byoshaestablishmentflag = RptDataNode.selectNodes("control").[0].getAttribute("checked")
                             If(byoshaestablishmentflag = "") Then
                             byoshaestablishmentflag=true
                             End IF

                      End If               
                 End If

                 If(RptDataNode.getAttribute("title") = "OSHA Information [By Organizational Hierarchy]") Then
                    
                     If(RptDataNode.selectNodes("control").[0].getAttribute("name")="byoshaestablishmentflag") Then
                            byoshaestablishmentflag2 = RptDataNode.selectNodes("control").[0].getAttribute("checked")
                             If(byoshaestablishmentflag2 = "") Then
                             byoshaestablishmentflag2=true
                             End IF
                      End If
                     If(RptDataNode.selectNodes("control").[1].getAttribute("name")="usereportlevel") Then
                            usereportlevel = RptDataNode.selectNodes("control").[1].getAttribute("codeid")
                      End If
                     If(RptDataNode.selectNodes("control").[2].getAttribute("name")="reportlevel") Then
                            reportlevel = RptDataNode.selectNodes("control").[2].getAttribute("codeid")
                      End If
                    
                  End If            

                  If(RptDataNode.getAttribute("name") = "Date Information") Then
                    
                      If(RptDataNode.selectNodes("control").[0].getAttribute("name")="yearofreport") Then
                            yearofreport = RptDataNode.selectNodes("control").[0].getAttribute("codeid")
                         End If

                         If(RptDataNode.selectNodes("control").[1].getAttribute("name")="datemethod") Then
                            datemethod = RptDataNode.selectNodes("control").[1].getAttribute("checked")
                            If(datemethod = "") Then
                             datemethod=true
                             End IF
                         End If
                       
                         If(RptDataNode.selectNodes("control").[2].getAttribute("name")="datemethod") Then
                            datemethod2 = RptDataNode.selectNodes("control").[2].getAttribute("checked")
                            If(datemethod2 = "") Then
                             datemethod2=true
                             End IF
                         End If
                         If(RptDataNode.selectNodes("control").Length > 3) Then
                         If(RptDataNode.selectNodes("control").[3].getAttribute("name")="begindate") Then
                            begindate = RptDataNode.selectNodes("control").[3].getAttribute("value")
                             If "" & begindate<>"" Then
                                begindate = FormatDateTime(DateSerial(Left(begindate,4),Mid(begindate,5,2),Mid(begindate,7,2)),vbGeneralDate) 
                            End If
                         End If
                         End If
                         If(RptDataNode.selectNodes("control").Length > 4) Then
                         If(RptDataNode.selectNodes("control").[4].getAttribute("name")="enddate") Then
                            enddate = RptDataNode.selectNodes("control").[4].getAttribute("value")
                            If "" & enddate<>"" Then
                               enddate = FormatDateTime(DateSerial(Left(enddate,4),Mid(enddate,5,2),Mid(enddate,7,2)),vbGeneralDate)
                            End If 
                         End If
                         End If
                     End If

                 if(RptDataNode.getAttribute("name") = "Data Options") Then
                    
                     If(RptDataNode.selectNodes("control").[0].getAttribute("name")="printsofterrlog") Then
                        printsofterrlog = RptDataNode.selectNodes("control").[0].getAttribute("checked")
                       If(printsofterrlog = "") Then
                             printsofterrlog =true
                             End IF
                     End If

                     If(RptDataNode.selectNodes("control").[1].getAttribute("name")="eventbasedflag") Then
                        eventbasedflag = RptDataNode.selectNodes("control").[1].getAttribute("checked")
                        If(eventbasedflag = "") Then
                             eventbasedflag =true
                             End IF
                     End If


                      If(RptDataNode.selectNodes("control").[2].getAttribute("name")="printoshadescflag") Then
                        printoshadescflag = RptDataNode.selectNodes("control").[2].getAttribute("checked")
                         If(printoshadescflag = "") Then
                             printoshadescflag =true
                             End IF
                     End If

                     If(RptDataNode.selectNodes("control").[3].getAttribute("name")="primarylocationflag") Then
                        primarylocationflag = RptDataNode.selectNodes("control").[3].getAttribute("checked")
                         If(primarylocationflag = "") Then
                             primarylocationflag =true
                             End IF
                     End If
                     
                    

                    If(RptDataNode.selectNodes("control").[4].getAttribute("name")="allentitiesflag") Then
                        allentitiesflag=RptDataNode.selectNodes("control").[4].getAttribute("checked")
                       If(allentitiesflag="")Then
                            allentitiesflag=true
                         End If
                    End If

                   If(RptDataNode.selectNodes("control").[5].getAttribute("name")="allentitiesflag")Then
                         allentitiesflag2=RptDataNode.selectNodes("control").[5].getAttribute("checked")
                        If(allentitiesflag2="")Then
                            allentitiesflag2=true
                         End If
                   End If
                   

                    If(RptDataNode.selectNodes("control").[6].getAttribute("name")="selectedentities") Then
                        selectedentities=RptDataNode.selectNodes("control").[6].getAttribute("value")

                          Dim CtlRptNode2
                          Dim valueCodelistRpt2
                          For Each CtlRptNode2 In RptDataNode.selectNodes("control").[6].selectNodes("option")                                             
                            If "" & CtlRptNode2.text<>"" Then
                                valueCodelistRpt2 = valueCodelistRpt2 +"||"+ CtlRptNode2.getAttribute("value")+"#"+CtlRptNode2.text
                            End If
                        Next
                        allselecteddata = valueCodelistRpt2

                    End If


                 End If
            next 
            
        End If



         If(reportTypeId="3") Then  ' Regarding Report type 3 300A

            For Each RptDataNode In sXMLData.selectNodes("/report/form/group")

                 If(RptDataNode.getAttribute("title") = "Preparer Information") Then

                         If(RptDataNode.selectNodes("control").[0].getAttribute("name")="preparertitle") Then
                                preparertitle = RptDataNode.selectNodes("control").[0].getAttribute("value")
                         End If  
                         If(RptDataNode.selectNodes("control").[1].getAttribute("name")="preparername") Then
                                preparername = RptDataNode.selectNodes("control").[1].getAttribute("value")
                         End If  
                         If(RptDataNode.selectNodes("control").[2].getAttribute("name")="preparerphone") Then
                                preparerphone = RptDataNode.selectNodes("control").[2].getAttribute("value")
                         End If   
                         If(RptDataNode.selectNodes("control").[3].getAttribute("name")="preparersignaturedate") Then
                            preparersignaturedate = RptDataNode.selectNodes("control").[3].getAttribute("value")
                           ' If "" & preparersignaturedate<>"" Then
                               ' preparersignaturedate = FormatDateTime(DateSerial(Left(preparersignaturedate,4),Mid(preparersignaturedate,5,2),Mid(preparersignaturedate,7,2)),vbGeneralDate) 
                            'End If
                         End If  
                                 
                   End If
 
                 If(RptDataNode.getAttribute("title") = "OSHA Information [By OSHA Establishment]") Then

                     If(RptDataNode.selectNodes("control").[0].getAttribute("name")="byoshaestablishmentflag") Then
                            byoshaestablishmentflag = RptDataNode.selectNodes("control").[0].getAttribute("checked")
                             If(byoshaestablishmentflag = "") Then
                             byoshaestablishmentflag=true
                             End IF

                      End If               
                 End If

                 If(RptDataNode.getAttribute("title") = "OSHA Information [By Organizational Hierarchy]") Then
                    
                     If(RptDataNode.selectNodes("control").[0].getAttribute("name")="byoshaestablishmentflag") Then
                            byoshaestablishmentflag2 = RptDataNode.selectNodes("control").[0].getAttribute("checked")
                             If(byoshaestablishmentflag2 = "") Then
                             byoshaestablishmentflag2=true
                             End IF
                      End If
                     If(RptDataNode.selectNodes("control").[1].getAttribute("name")="usereportlevel") Then
                            usereportlevel = RptDataNode.selectNodes("control").[1].getAttribute("codeid")
                      End If
                     If(RptDataNode.selectNodes("control").[2].getAttribute("name")="reportlevel") Then
                            reportlevel = RptDataNode.selectNodes("control").[2].getAttribute("codeid")
                      End If
                        If(RptDataNode.selectNodes("control").[3].getAttribute("name")="establishmentnameprefix") Then
                            establishmentnameprefix = RptDataNode.selectNodes("control").[3].getAttribute("codeid")
                        End If
                  End If            

                  If(RptDataNode.getAttribute("name") = "Date Information") Then
                    
                         If(RptDataNode.selectNodes("control").[0].getAttribute("name")="yearofreport") Then
                            yearofreport = RptDataNode.selectNodes("control").[0].getAttribute("codeid")
                         End If
                         If(RptDataNode.selectNodes("control").[1].getAttribute("name")="datemethod") Then
                            datemethod = RptDataNode.selectNodes("control").[1].getAttribute("checked")
                            If(datemethod = "") Then
                             datemethod=true
                             End IF
                         End If
                        
                         If(RptDataNode.selectNodes("control").[2].getAttribute("name")="datemethod") Then
                            datemethod2 = RptDataNode.selectNodes("control").[2].getAttribute("checked")
                            If(datemethod2 = "") Then
                             datemethod2=true
                             End IF
                         End If
                         If(RptDataNode.selectNodes("control").Length > 4) Then
                         If(RptDataNode.selectNodes("control").[3].getAttribute("name")="begindate") Then
                            begindate = RptDataNode.selectNodes("control").[3].getAttribute("value")
                             If "" & begindate<>"" Then
                                begindate = FormatDateTime(DateSerial(Left(begindate,4),Mid(begindate,5,2),Mid(begindate,7,2)),vbGeneralDate) 
                            End If
                         End If
                         End If
                         If(RptDataNode.selectNodes("control").Length > 4) Then
                         If(RptDataNode.selectNodes("control").[4].getAttribute("name")="enddate") Then
                            enddate = RptDataNode.selectNodes("control").[4].getAttribute("value")
                            If "" & enddate<>"" Then
                               enddate = FormatDateTime(DateSerial(Left(enddate,4),Mid(enddate,5,2),Mid(enddate,7,2)),vbGeneralDate)
                            End If 
                         End If
                         End If
                     End If

                 if(RptDataNode.getAttribute("name") = "Data Options") Then
                    
                     If(RptDataNode.selectNodes("control").[0].getAttribute("name")="asofdate") Then
                        asofdate = RptDataNode.selectNodes("control").[0].getAttribute("value")
                        If "" & asofdate<>"" Then
                            asofdate = FormatDateTime(DateSerial(Left(asofdate,4),Mid(asofdate,5,2),Mid(asofdate,7,2)),vbGeneralDate) 
                        End If
                     End If
                     If(RptDataNode.selectNodes("control").[1].getAttribute("name")="enforce180dayrule") Then
                        enforce180dayrule = RptDataNode.selectNodes("control").[1].getAttribute("checked")
                       If(enforce180dayrule = "") Then
                             enforce180dayrule =true
                             End IF
                     End If
                     If(RptDataNode.selectNodes("control").[2].getAttribute("name")="printsofterrlog") Then
                        printsofterrlog = RptDataNode.selectNodes("control").[2].getAttribute("checked")
                         If(printsofterrlog = "") Then
                             printsofterrlog =true
                             End IF
                     End If
                     If(RptDataNode.selectNodes("control").[3].getAttribute("name")="eventbasedflag") Then
                        eventbasedflag = RptDataNode.selectNodes("control").[3].getAttribute("checked")
                        If(eventbasedflag = "") Then
                             eventbasedflag =true
                             End IF
                     End If
                     If(RptDataNode.selectNodes("control").[4].getAttribute("name")="printoshadescflag") Then
                        printoshadescflag = RptDataNode.selectNodes("control").[4].getAttribute("checked")
                         If(printoshadescflag = "") Then
                             printoshadescflag =true
                             End IF
                     End If

                     If(RptDataNode.selectNodes("control").[5].getAttribute("name")="primarylocationflag") Then
                        primarylocationflag = RptDataNode.selectNodes("control").[5].getAttribute("checked")
                         If(primarylocationflag = "") Then
                             primarylocationflag =true
                             End IF
                     End If

                    If(RptDataNode.selectNodes("control").[6].getAttribute("name")="allentitiesflag") Then
                        allentitiesflag=RptDataNode.selectNodes("control").[6].getAttribute("checked")
                        If(allentitiesflag="")Then
                            allentitiesflag=true
                         End If
                    End If

                   If(RptDataNode.selectNodes("control").[7].getAttribute("name")="allentitiesflag")Then
                         allentitiesflag2=RptDataNode.selectNodes("control").[7].getAttribute("checked")
                         If(allentitiesflag2="")Then
                            allentitiesflag2=true
                         End If
                   End If           
                    If(RptDataNode.selectNodes("control").[8].getAttribute("name")="selectedentities") Then
                         selectedentities=RptDataNode.selectNodes("control").[8].getAttribute("value")

                          Dim CtlRptNode3
                          Dim valueCodelistRpt3
                          For Each CtlRptNode3 In RptDataNode.selectNodes("control").[8].selectNodes("option")                                             
                             If "" & CtlRptNode3.text<>"" Then
                                valueCodelistRpt3 = valueCodelistRpt3 +"||"+ CtlRptNode3.getAttribute("value")+"#"+CtlRptNode3.text
                             End If
                         Next
                         allselecteddata = valueCodelistRpt3

                    End If
                 End If
            next 

       End If

       End if
        ' End :Add by kuladeep for editable report mits:27574
	'End If
%>
<html>
<head>
    <title>Scheduled Report Details</title>
    <!--[Rajeev 11/27/2002 CSS Consolidation] link rel="stylesheet" href="forms.css" type="text/css" /-->
    <link rel="stylesheet" href="RMNet.css" type="text/css" />
    <script language="JavaScript" src="oshareport.js"></script>
    <!--Add by kuladeep for editable report mits:27574 -->
    <script language="JavaScript" src="form.js"></script>
    <script language="JavaScript" src="smi.js"></script>
    <script>

        function pageLoaded() {
            var i; m_sFieldName = "selectedentities"; //Add by kuladeep for editable report mits:27574
            document.onCodeClose = onCodeClose;
            document.dateSelected = dateSelected;
            for (i = 0; i < document.frmData.length; i++) {

                if ((document.frmData.item(i).type == "text") || (document.frmData.item(i).type == "select-one") || (document.frmData.item(i).type == "textarea")) {
                    document.frmData.item(i).focus();
                    break;
                }
            }
            //Start:Add by kuladeep regarding populate report data mits:27574

            //Report filling for 300 Report data

            if (document.frmData.reportTypeId != null) {
                if (document.frmData.reportTypeId.value == "1") {

                    if (document.frmData.allselecteddata != null) {
                        if (document.frmData.allselecteddata.value != '') {
                            var entity_Array = document.frmData.allselecteddata.value.split("||");
                            for (var i = 1; i < entity_Array.length; i++) {
                                var codevalue_Array = entity_Array[i].split("#");
                                codeSelected(codevalue_Array[1], codevalue_Array[0], false);
                            }
                        }
                    }

                    if (document.frmData.hidasofdate != null) {
                        if (document.frmData.hidasofdate.value != '') {
                            if (document.frmData.asofdate != null)
                                document.frmData.asofdate.value = document.frmData.hidasofdate.value;

                        }
                    }
                    if (document.frmData.hidbyoshaestablishmentflag1 != null) {
                        if (document.frmData.hidbyoshaestablishmentflag1.value == "True") {
                            if (document.getElementById('byoshaestablishmentflag1') != null)
                                document.getElementById('byoshaestablishmentflag1').checked = true;
                            //For making read only control

                            if (document.frmData.hidusereportlevel.value != '') {
                                if (document.getElementById("usereportlevel") != null)
                                    document.getElementById("usereportlevel").disabled = true;
                            }
                            if (document.frmData.hidreportlevel.value != '') {
                                if (document.getElementById("reportlevel") != null)
                                    document.getElementById("reportlevel").disabled = true;
                            }
                            if (document.frmData.hidestablishmentnameprefix.value != '') {
                                if (document.getElementById("establishmentnameprefix") != null)
                                    document.getElementById("establishmentnameprefix").disabled = true;

                            }
                        }
                    }

                    if (document.frmData.hidbyoshaestablishmentflag2 != null) {
                        if (document.frmData.hidbyoshaestablishmentflag2.value == "True") {
                            if (document.getElementById('byoshaestablishmentflag2') != null)
                                document.getElementById('byoshaestablishmentflag2').checked = true;
                        }
                    }

                    if (document.frmData.hiddatemethod != null) {
                        if (document.frmData.hiddatemethod.value == "True") {
                            if (document.getElementById('datemethod1') != null)
                                document.getElementById('datemethod1').checked = true;
                        }
                    }
                    if (document.frmData.hiddatemethod2 != null) {
                        if (document.frmData.hiddatemethod2.value == "True") {
                            if (document.getElementById('datemethod2') != null)
                                document.getElementById('datemethod2').checked = true;

                            if (document.frmData.hidbegindate.value != '') {
                                if (document.frmData.begindate != null)
                                    document.frmData.begindate.value = document.frmData.hidbegindate.value;

                            }
                            if (document.frmData.hidenddate.value != '') {
                                if (document.frmData.enddate != null)
                                    document.frmData.enddate.value = document.frmData.hidenddate.value;

                            }
                        }
                    }

                    if (document.frmData.hidenforce180dayrule.value == "True") {
                        if (document.getElementById('enforce180dayrule') != null)
                            document.getElementById('enforce180dayrule').checked = true;
                    }
                    else {
                        if (document.getElementById('enforce180dayrule') != null)
                            document.getElementById('enforce180dayrule').checked = false;
                    }

                    if (document.frmData.hidprintsofterrlog.value == "True") {
                        if (document.getElementById('printsofterrlog') != null)
                            document.getElementById('printsofterrlog').checked = true;
                    }
                    else {
                        if (document.getElementById('printsofterrlog') != null)
                            document.getElementById('printsofterrlog').checked = false;
                    }

                    if (document.frmData.hideventbasedflag.value == "True") {
                        if (document.getElementById('eventbasedflag') != null)
                            document.getElementById('eventbasedflag').checked = true;
                    }
                    else {
                        if (document.getElementById('eventbasedflag') != null)
                            document.getElementById('eventbasedflag').checked = false;
                    }

                    if (document.frmData.hidprintoshadescflag.value == "True") {
                        if (document.getElementById('printoshadescflag') != null)
                            document.getElementById('printoshadescflag').checked = true;
                    }
                    else {
                        if (document.getElementById('printoshadescflag') != null)
                            document.getElementById('printoshadescflag').checked = false;
                    }

                    if (document.frmData.hidyearofreport.value != '') {
                        if (document.getElementById("yearofreport") != null)
                            document.getElementById("yearofreport").value = document.frmData.hidyearofreport.value;
                    }


                    if (document.frmData.hidcolumnesource.value != '') {
                        if (document.getElementById("columnesource") != null)
                            document.getElementById("columnesource").value = document.frmData.hidcolumnesource.value;
                    }
                    if (document.frmData.hidcolumnfsource.value != '') {
                        if (document.getElementById("columnfsource") != null)
                            document.getElementById("columnfsource").value = document.frmData.hidcolumnfsource.value;
                    }
                    if (document.frmData.hidsortorder.value != '') {
                        if (document.getElementById("sortorder") != null)
                            document.getElementById("sortorder").value = document.frmData.hidsortorder.value;
                    }


                    if (document.frmData.hidusereportlevel.value != '') {
                        if (document.getElementById("usereportlevel") != null)
                            document.getElementById("usereportlevel").value = document.frmData.hidusereportlevel.value;
                    }
                    if (document.frmData.hidreportlevel.value != '') {
                        if (document.getElementById("reportlevel") != null)
                            document.getElementById("reportlevel").value = document.frmData.hidreportlevel.value;
                    }
                    if (document.frmData.hidestablishmentnameprefix.value != '') {
                        if (document.getElementById("establishmentnameprefix") != null)
                            document.getElementById("establishmentnameprefix").value = document.frmData.hidestablishmentnameprefix.value;
                    }

                   if(document.getElementById("hidallentitiesflag").value != null)
                   {
                      if(document.getElementById("hidallentitiesflag").value=="True")
                       document.getElementsByName("allentitiesflag")[0].checked = true;
                   }
                   if(document.getElementById("hidallentitiesflag2").value != null)
                   {
                     if(document.getElementById("hidallentitiesflag2").value=="True")
                       document.getElementsByName("allentitiesflag")[1].checked = true;
                   }
        

                }
            }



            //Report filling for 301 Report data

            if (document.frmData.reportTypeId != null) {
                if (document.frmData.reportTypeId.value == "4") {

                    if (document.frmData.allselecteddata != null) {
                       if (document.frmData.allselecteddata.value != '') {
                           var entity_Array = document.frmData.allselecteddata.value.split("||");
                           for (var i = 1; i < entity_Array.length; i++) {
                               var codevalue_Array = entity_Array[i].split("#");
                               codeSelected(codevalue_Array[1], codevalue_Array[0], false);
                           }
                       }
                   }

                    if (document.frmData.hidpreparertitle != null) {
                        if (document.frmData.hidpreparertitle.value != '') {
                            if (document.frmData.preparertitle != null)
                                document.frmData.preparertitle.value = document.frmData.hidpreparertitle.value;

                        }
                    }
                    if (document.frmData.hidpreparername != null) {
                        if (document.frmData.hidpreparername.value != '') {
                            if (document.frmData.preparername != null)
                                document.frmData.preparername.value = document.frmData.hidpreparername.value;

                        }
                    }
                    if (document.frmData.hidpreparerphone != null) {
                        if (document.frmData.hidpreparerphone.value != '') {
                            if (document.frmData.preparerphone != null)
                                document.frmData.preparerphone.value = document.frmData.hidpreparerphone.value;

                        }
                    }

                    if (document.frmData.hidbyoshaestablishmentflag1 != null) {
                        if (document.frmData.hidbyoshaestablishmentflag1.value == "True") {
                            if (document.getElementById('byoshaestablishmentflag1') != null)
                                document.getElementById('byoshaestablishmentflag1').checked = true;

                            if (document.frmData.hidusereportlevel.value != '') {
                                if (document.getElementById("usereportlevel") != null)
                                    document.getElementById("usereportlevel").disabled = true;
                            }
                            if (document.frmData.hidreportlevel.value != '') {
                                if (document.getElementById("reportlevel") != null)
                                    document.getElementById("reportlevel").disabled = true;
                            }
                            if (document.frmData.hidestablishmentnameprefix.value != '') {
                                if (document.getElementById("establishmentnameprefix") != null)
                                    document.getElementById("establishmentnameprefix").disabled = true;

                            }
                        }
                    }

                    if (document.frmData.hidbyoshaestablishmentflag2 != null) {
                        if (document.frmData.hidbyoshaestablishmentflag2.value == "True") {
                            if (document.getElementById('byoshaestablishmentflag2') != null)
                                document.getElementById('byoshaestablishmentflag2').checked = true;
                        }
                    }

                    if (document.frmData.hiddatemethod != null) {
                        if (document.frmData.hiddatemethod.value == "True") {
                            if (document.getElementById('datemethod1') != null)
                                document.getElementById('datemethod1').checked = true;
                        }
                    }

                    if (document.frmData.hiddatemethod2 != null) {
                        if (document.frmData.hiddatemethod2.value == "True") {
                            if (document.getElementById('datemethod2') != null)
                                document.getElementById('datemethod2').checked = true;

                            if (document.frmData.hidbegindate.value != '') {
                                if (document.frmData.begindate != null)
                                    document.frmData.begindate.value = document.frmData.hidbegindate.value;

                            }
                            if (document.frmData.hidenddate.value != '') {
                                if (document.frmData.enddate != null)
                                    document.frmData.enddate.value = document.frmData.hidenddate.value;

                            }
                        }
                    }


                    if (document.frmData.hidprintsofterrlog.value == "True") {
                        if (document.getElementById('printsofterrlog') != null)
                            document.getElementById('printsofterrlog').checked = true;
                    }
                    else {

                        if (document.getElementById('printsofterrlog') != null)
                            document.getElementById('printsofterrlog').checked = false ;
                        
                    }

                    if (document.frmData.hideventbasedflag.value == "True") {
                        if (document.getElementById('eventbasedflag') != null)
                            document.getElementById('eventbasedflag').checked = true;
                    }
                    else {

                        if (document.getElementById('eventbasedflag') != null)
                            document.getElementById('eventbasedflag').checked = false;

                    }

                    if (document.frmData.hidprintoshadescflag.value == "True") {
                        if (document.getElementById('printoshadescflag') != null)
                            document.getElementById('printoshadescflag').checked = true;
                    }
                    else {
                        if (document.getElementById('printoshadescflag') != null)
                            document.getElementById('printoshadescflag').checked = false;
                    }

                    if (document.frmData.hidprimarylocationflag.value == "True") {
                        if (document.getElementById('primarylocationflag') != null)
                            document.getElementById('primarylocationflag').checked = true;
                    }
                    else {

                        if (document.getElementById('primarylocationflag') != null)
                            document.getElementById('primarylocationflag').checked = false;
                        
                    }    

                    if (document.frmData.hidyearofreport.value != '') {
                        if (document.getElementById("yearofreport") != null)
                            document.getElementById("yearofreport").value = document.frmData.hidyearofreport.value;
                    }

                    if (document.frmData.hidusereportlevel.value != '') {
                        if (document.getElementById("usereportlevel") != null)
                            document.getElementById("usereportlevel").value = document.frmData.hidusereportlevel.value;
                    }
                    if (document.frmData.hidreportlevel.value != '') {
                        if (document.getElementById("reportlevel") != null)
                            document.getElementById("reportlevel").value = document.frmData.hidreportlevel.value;
                    }
                   
                    if (document.frmData.hidestablishmentnameprefix.value != '') {
                        if (document.getElementById("establishmentnameprefix") != null)
                            document.getElementById("establishmentnameprefix").value = document.frmData.hidestablishmentnameprefix.value;
                    }

                   if(document.getElementById("hidallentitiesflag").value != null)
                   {
                      if(document.getElementById("hidallentitiesflag").value=="True")
                       document.getElementsByName("allentitiesflag")[0].checked = true;
                   }
                   if(document.getElementById("hidallentitiesflag2").value != null)
                   {
                     if(document.getElementById("hidallentitiesflag2").value=="True")
                       document.getElementsByName("allentitiesflag")[1].checked = true;
                   }

                }
            }


             //Report filling for 300A Report data

            if (document.frmData.reportTypeId != null) {
                if (document.frmData.reportTypeId.value == "3") {

                    if (document.frmData.hidpreparertitle != null) {
                        if (document.frmData.hidpreparertitle.value != '') {
                            if (document.frmData.preparertitle != null)
                                document.frmData.preparertitle.value = document.frmData.hidpreparertitle.value;

                        }
                    }
                    if (document.frmData.hidpreparername != null) {
                        if (document.frmData.hidpreparername.value != '') {
                            if (document.frmData.preparername != null)
                                document.frmData.preparername.value = document.frmData.hidpreparername.value;

                        }
                    }
                    if (document.frmData.hidpreparerphone != null) {
                        if (document.frmData.hidpreparerphone.value != '') {
                            if (document.frmData.preparerphone != null)
                                document.frmData.preparerphone.value = document.frmData.hidpreparerphone.value;

                        }
                    }
                    if (document.frmData.hidpreparersignaturedate != null) {
                        if (document.frmData.hidpreparersignaturedate.value != '') {
                            if (document.frmData.preparersignaturedate != null)
                                document.frmData.preparersignaturedate.value = document.frmData.hidpreparersignaturedate.value;

                        }
                    }

                    if (document.frmData.hidpreparersignaturedate != null) {
                        if (document.frmData.hidpreparersignaturedate.value != '') {
                            if (document.frmData.preparersignaturedate != null)
                                document.frmData.preparersignaturedate.value = document.frmData.hidpreparersignaturedate.value;

                        }
                    }


                    if (document.frmData.allselecteddata != null) {
                        if (document.frmData.allselecteddata.value != '') {
                            var entity_Array = document.frmData.allselecteddata.value.split("||");
                            for (var i = 1; i < entity_Array.length; i++) {
                                var codevalue_Array = entity_Array[i].split("#");
                                codeSelected(codevalue_Array[1], codevalue_Array[0], false);
                            }
                        }
                    }

                    if (document.frmData.hidasofdate != null) {
                        if (document.frmData.hidasofdate.value != '') {
                            if (document.frmData.asofdate != null)
                                document.frmData.asofdate.value = document.frmData.hidasofdate.value;

                        }
                    }

                    if (document.frmData.hidbyoshaestablishmentflag1 != null) {
                        if (document.frmData.hidbyoshaestablishmentflag1.value == "True") {
                            if (document.getElementById('byoshaestablishmentflag1') != null)
                                document.getElementById('byoshaestablishmentflag1').checked = true;

                            if (document.frmData.hidusereportlevel.value != '') {
                                if (document.getElementById("usereportlevel") != null)
                                    document.getElementById("usereportlevel").disabled = true;
                            }
                            if (document.frmData.hidreportlevel.value != '') {
                                if (document.getElementById("reportlevel") != null)
                                    document.getElementById("reportlevel").disabled = true;
                            }
                            if (document.frmData.hidestablishmentnameprefix.value != '') {
                                if (document.getElementById("establishmentnameprefix") != null)
                                    document.getElementById("establishmentnameprefix").disabled = true;

                            }
                        }
                    }

                    if (document.frmData.hidbyoshaestablishmentflag2 != null) {
                        if (document.frmData.hidbyoshaestablishmentflag2.value == "True") {
                            if (document.getElementById('byoshaestablishmentflag2') != null)
                                document.getElementById('byoshaestablishmentflag2').checked = true;
                        }
                    }

                    if (document.frmData.hiddatemethod != null) {
                        if (document.frmData.hiddatemethod.value == "True") {
                            if (document.getElementById('datemethod1') != null)
                                document.getElementById('datemethod1').checked = true;
                        }
                    }
                    if (document.frmData.hiddatemethod2 != null) {
                        if (document.frmData.hiddatemethod2.value == "True") {
                            if (document.getElementById('datemethod2') != null)
                                document.getElementById('datemethod2').checked = true;

                            if (document.frmData.hidbegindate.value != '') {
                                if (document.frmData.begindate != null)
                                    document.frmData.begindate.value = document.frmData.hidbegindate.value;

                            }
                            if (document.frmData.hidenddate.value != '') {
                                if (document.frmData.enddate != null)
                                    document.frmData.enddate.value = document.frmData.hidenddate.value;

                            }
                        }
                    }

                    if (document.frmData.hidenforce180dayrule.value == "True") {
                        if (document.getElementById('enforce180dayrule') != null)
                            document.getElementById('enforce180dayrule').checked = true;
                    }
                    else {
                        if (document.getElementById('enforce180dayrule') != null)
                            document.getElementById('enforce180dayrule').checked = false;
                    }

                    if (document.frmData.hidprintsofterrlog.value == "True") {
                        if (document.getElementById('printsofterrlog') != null)
                            document.getElementById('printsofterrlog').checked = true;
                    }
                    else {
                        if (document.getElementById('printsofterrlog') != null)
                            document.getElementById('printsofterrlog').checked = false;
                    }

                    if (document.frmData.hideventbasedflag.value == "True") {
                        if (document.getElementById('eventbasedflag') != null)
                            document.getElementById('eventbasedflag').checked = true;
                    }
                    else {
                        if (document.getElementById('eventbasedflag') != null)
                            document.getElementById('eventbasedflag').checked = false;
                    }

                    if (document.frmData.hidprintoshadescflag.value == "True") {
                        if (document.getElementById('printoshadescflag') != null)
                            document.getElementById('printoshadescflag').checked = true;
                    }
                    else {
                        if (document.getElementById('printoshadescflag') != null)
                            document.getElementById('printoshadescflag').checked = false;
                    }

                    if (document.frmData.hidprimarylocationflag.value == "True") {
                        if (document.getElementById('primarylocationflag') != null)
                            document.getElementById('primarylocationflag').checked = true;
                    }
                    else {

                        if (document.getElementById('primarylocationflag') != null)
                            document.getElementById('primarylocationflag').checked = false;

                    }   

                    if (document.frmData.hidyearofreport.value != '') {
                        if (document.getElementById("yearofreport") != null)
                            document.getElementById("yearofreport").value = document.frmData.hidyearofreport.value;
                    }


                    if (document.frmData.hidusereportlevel.value != '') {
                        if (document.getElementById("usereportlevel") != null)
                            document.getElementById("usereportlevel").value = document.frmData.hidusereportlevel.value;
                    }
                    if (document.frmData.hidreportlevel.value != '') {
                        if (document.getElementById("reportlevel") != null)
                            document.getElementById("reportlevel").value = document.frmData.hidreportlevel.value;
                    }
                    if (document.frmData.hidestablishmentnameprefix.value != '') {
                        if (document.getElementById("establishmentnameprefix") != null)
                            document.getElementById("establishmentnameprefix").value = document.frmData.hidestablishmentnameprefix.value;
                    }

                   if(document.getElementById("hidallentitiesflag").value != null)
                   {
                      if(document.getElementById("hidallentitiesflag").value=="True")
                       document.getElementsByName("allentitiesflag")[0].checked = true;
                   }
                   if(document.getElementById("hidallentitiesflag2").value != null)
                   {
                     if(document.getElementById("hidallentitiesflag2").value=="True")
                       document.getElementsByName("allentitiesflag")[1].checked = true;
                   }

                }
            }

            //End:Add by kuladeep for editable report mits:27574

        }



        function PreSaveCheck() {
            /////		document.frmData.hidscheduletype.value=document.frmData.scheduletype.value;
            window.setTimeout("window.location.href='scheduledetail.asp?scheduleid=" + "<%=lScheduleId%>" + "&viewonly=0&scheduletype=" + document.frmData.scheduletype.value + "'", 100);
            document.frmData.submit();
            return true;
        }

        function ScheduleTypeChanged() {
            /////		document.frmData.hidscheduletype.value=document.frmData.scheduletype.value;
            window.setTimeout("window.location.href='scheduledetail.asp?scheduleid=" + "<%=lScheduleId%>" + "&viewonly=0&typechanged=1&reportid=" + document.frmData.reportid.value + " &scheduletype=" + document.frmData.scheduletype.value + "'", 100);
            return true;
        }

        function onClickEdit() {
            window.setTimeout("window.location.href='scheduledetail.asp?scheduleid=" + "<%=lScheduleId%>" + "&viewonly=0'", 100);
            return true;
        }
    </script>
    <!--[Rajeev 11/27/2002 CSS Consolidation] STYLE Commented-->
    <!--style type="text/css">
	input.button
	{
		background-color: #330099;
		color: #FFFFFF;
		font-weight: bold;
		font-family : Arial, Helvetica, sans-serif;
		font-size : 9pt;
	}
</style-->
</head>
<body class="10pt" onload="pageLoaded()">
    <form name="frmData" method="post" action="scheduledetail.asp?editscheduleonly=1"
    id="frmData">
    <input type="hidden" name="reportid" value="<%=lReportId%>" id="Hidden1" />
    <input type="hidden" name="scheduleid" value="<%=lScheduleId%>" id="Hidden2" />
    <h3>
        Scheduled Report Details</h3>
    <table border="0" cellpadding="4" cellspacing="0" id="Table1">
        <tr>
            <td class="ctrlgroup2">
                Schedule Type:
            </td>
            <td class="datatd1">
                <b>
                    <%
				If bViewOnly=1 Then
					If lScheduleType=1 Then Response.Write "Daily" Else Response.Write "Monthly"
				Else
					response.write "<select name=""scheduletype"" size=""1"" ONCHANGE=""return ScheduleTypeChanged()"" >"
					If lScheduleType=1 Then
						Response.write "<option SELECTED value=""1"">Daily  </option>"
						Response.write "<option value=""2"">Monthly</option> </select>"
					Else
						Response.write "<option value=""1"">Daily  </option>"
						Response.write "<option SELECTED value=""2"">Monthly</option> </select>"
					End If
				End If
                    %></b>
            </td>
        </tr>
        <tr>
            <td class="ctrlgroup2">
                Report Name:
            </td>
            <td class="datatd">
                <%
			objSMRocket.DB_GetData hRS, "REPORT_NAME", v
			If v & "" = "" Then v="Report Nr. " & lReportId
			Response.Write v
                %>
            </td>
        </tr>
        <tr>
            <td class="ctrlgroup2">
                Next Run:
            </td>
            <td class="datatd1">
                <%
			objSMRocket.DB_GetData hRS, "NEXT_RUN_DATE", v
			If bViewOnly=1 Then
				If "" & v <> "" Then Response.Write FormatDateTime(DateSerial(Left(v,4),Mid(v,5,2),Mid(v,7,2)),vbGeneralDate) & " "
				objSMRocket.DB_GetData hRS, "START_TIME", v
				If "" & v<>"" Then Response.Write FormatDateTime(TimeSerial(Left(v,2),Mid(v,3,2),Mid(v,5,2)),vbLongTime)
			Else
				Response.Write "<input type=""text"" name=""startdate"" value=""" &  FormatDateTime(DateSerial(Left(v,4),Mid(v,5,2),Mid(v,7,2)),vbGeneralDate) & " " & """>"
			End If
                %>
            </td>
        </tr>
        <%	If bViewOnly=0 Then %>
        <tr>
            <td class="ctrlgroup2">
                Next Run Time:
            </td>
            <td class="datatd">
                <%
				objSMRocket.DB_GetData hRS, "START_TIME", v
				Response.write "<input type=""text""  name=""starttime"" value=""" & FormatDateTime(TimeSerial(Left(v,2),Mid(v,3,2),Mid(v,5,2)),vbLongTime) & " " & """>"
                %>
            </td>
        </tr>
        <% End If %>
        <tr>
            <td valign="top" class="ctrlgroup2">
                Report runs every:
            </td>
            <td class="datatd">
                <%
				If bViewOnly=1 Then
					If lScheduleType=1 Then
						objSMRocket.DB_GetData hRS, "MON_RUN", v
						If v=1 Then Response.Write "Monday <br />"
						objSMRocket.DB_GetData hRS, "TUE_RUN", v
						If v=1 Then Response.Write "Tuesday <br />"
						objSMRocket.DB_GetData hRS, "WED_RUN", v
						If v=1 Then Response.Write "Wednesday <br />"
						objSMRocket.DB_GetData hRS, "THU_RUN", v
						If v=1 Then Response.Write "Thursday <br />"
						objSMRocket.DB_GetData hRS, "FRI_RUN", v
						If v=1 Then Response.Write "Friday <br />"
						objSMRocket.DB_GetData hRS, "SAT_RUN", v
						If v=1 Then Response.Write "Saturday <br />"
						objSMRocket.DB_GetData hRS, "SUN_RUN", v
						If v=1 Then Response.Write "Sunday <br />"
					Else
						objSMRocket.DB_GetData hRS, "JAN_RUN", v
						If v=1 Then Response.Write "January <br />"
						objSMRocket.DB_GetData hRS, "FEB_RUN", v
						If v=1 Then Response.Write "February <br />"
						objSMRocket.DB_GetData hRS, "MAR_RUN", v
						If v=1 Then Response.Write "March <br />"
						objSMRocket.DB_GetData hRS, "APR_RUN", v
						If v=1 Then Response.Write "April <br />"
						objSMRocket.DB_GetData hRS, "MAY_RUN", v
						If v=1 Then Response.Write "May <br />"
						objSMRocket.DB_GetData hRS, "JUN_RUN", v
						If v=1 Then Response.Write "June <br />"
						objSMRocket.DB_GetData hRS, "JUL_RUN", v
						If v=1 Then Response.Write "July <br />"
						objSMRocket.DB_GetData hRS, "AUG_RUN", v
						If v=1 Then Response.Write "August <br />"
						objSMRocket.DB_GetData hRS, "SEP_RUN", v
						If v=1 Then Response.Write "September <br />"
						objSMRocket.DB_GetData hRS, "OCT_RUN", v
						If v=1 Then Response.Write "October <br />"
						objSMRocket.DB_GetData hRS, "NOV_RUN", v
						If v=1 Then Response.Write "November <br />"
						objSMRocket.DB_GetData hRS, "DEC_RUN", v
						If v=1 Then Response.Write "December <br />"
					End If
				Else
					If lScheduleType=1 Then
						objSMRocket.DB_GetData hRS, "MON_RUN", v
						If v=1 Then
							Response.Write "<input type=""Checkbox"" name=""MON_RUN"" checked=""1"" value=""1"">Monday</input>"
						Else
							Response.Write "<input type=""Checkbox"" name=""MON_RUN"" value=""1"">Monday</input>"
						End If

						objSMRocket.DB_GetData hRS, "TUE_RUN", v
						If v=1 Then
							Response.Write "<input type=""Checkbox"" name=""TUE_RUN"" checked=""1"" value=""1"">Tuesday</input>"
						Else
							Response.Write "<input type=""Checkbox"" name=""TUE_RUN"" value=""1"">Tuesday</input>"
						End If
						
						objSMRocket.DB_GetData hRS, "WED_RUN", v
						If v=1 Then
							Response.Write "<input type=""Checkbox"" name=""WED_RUN"" checked=""1"" value=""1"">Wednesday</input>"
						Else
							Response.Write "<input type=""Checkbox"" name=""WED_RUN"" value=""1"">Wednesday</input>"
						End If

						objSMRocket.DB_GetData hRS, "THU_RUN", v
						If v=1 Then
							Response.Write "<input type=""Checkbox"" name=""THU_RUN"" checked=""1"" value=""1"">Thursday</input>"
						Else
							Response.Write "<input type=""Checkbox"" name=""THU_RUN"" value=""1"">Thursday</input>"
						End If

						objSMRocket.DB_GetData hRS, "FRI_RUN", v
						If v=1 Then
							Response.Write "<input type=""Checkbox"" name=""FRI_RUN"" checked=""1"" value=""1"">Friday</input>"
						Else
							Response.Write "<input type=""Checkbox"" name=""FRI_RUN"" value=""1"">Friday</input>"
						End If

						objSMRocket.DB_GetData hRS, "SAT_RUN", v
						If v=1 Then
							Response.Write "<input type=""Checkbox"" name=""SAT_RUN"" checked=""1"" value=""1"">Saturday</input>"
						Else
							Response.Write "<input type=""Checkbox"" name=""SAT_RUN"" value=""1"">Saturday</input>"
						End If

						objSMRocket.DB_GetData hRS, "SUN_RUN", v
						If v=1 Then
							Response.Write "<input type=""Checkbox"" name=""SUN_RUN"" checked=""1"" value=""1"">Sunday</input>"
						Else
							Response.Write "<input type=""Checkbox"" name=""SUN_RUN"" value=""1"">Sunday</input>"
						End If

					Else
						objSMRocket.DB_GetData hRS, "JAN_RUN", v
						If v=1 Then
							Response.Write "<input type=""Checkbox"" name=""JAN_RUN"" checked=""1"" value=""1"">January</input>"
						Else
							Response.Write "<input type=""Checkbox"" name=""JAN_RUN"" value=""1"">January</input>"
						End If
						
						objSMRocket.DB_GetData hRS, "FEB_RUN", v
						If v=1 Then
							Response.Write "<input type=""Checkbox"" name=""FEB_RUN"" checked=""1"" value=""1"">February</input>"
						Else
							Response.Write "<input type=""Checkbox"" name=""FEB_RUN"" value=""1"">February</input>"
						End If

						objSMRocket.DB_GetData hRS, "MAR_RUN", v
						If v=1 Then
							Response.Write "<input type=""Checkbox"" name=""MAR_RUN"" checked=""1"" value=""1"">March</input>"
						Else
							Response.Write "<input type=""Checkbox"" name=""MAR_RUN"" value=""1"">March</input>"
						End If

						objSMRocket.DB_GetData hRS, "APR_RUN", v
						If v=1 Then
							Response.Write "<input type=""Checkbox"" name=""APR_RUN"" checked=""1"" value=""1"">April</input>"
						Else
							Response.Write "<input type=""Checkbox"" name=""APR_RUN"" value=""1"">April</input>"
						End If

						objSMRocket.DB_GetData hRS, "MAY_RUN", v
						If v=1 Then
							Response.Write "<input type=""Checkbox"" name=""MAY_RUN"" checked=""1"" value=""1"">May</input>"
						Else
							Response.Write "<input type=""Checkbox"" name=""MAY_RUN"" value=""1"">May</input>"
						End If

						objSMRocket.DB_GetData hRS, "JUN_RUN", v
						If v=1 Then
							Response.Write "<input type=""Checkbox""  name=""JUN_RUN"" checked=""1"" value=""1"">June</input>"
						Else
							Response.Write "<input type=""Checkbox"" name=""JUN_RUN"" value=""1"">June</input>"
						End If

						objSMRocket.DB_GetData hRS, "JUL_RUN", v
						If v=1 Then
							Response.Write "<input type=""Checkbox"" name=""JUL_RUN"" checked=""1"" value=""1"">July</input>"
						Else
							Response.Write "<input type=""Checkbox"" name=""JUL_RUN"" value=""1"">July</input>"
						End If

						objSMRocket.DB_GetData hRS, "AUG_RUN", v
						If v=1 Then
							Response.Write "<input type=""Checkbox"" name=""AUG_RUN"" checked=""1"" value=""1"">August</input>"
						Else
							Response.Write "<input type=""Checkbox"" name=""AUG_RUN"" value=""1"">August</input>"
						End If

						objSMRocket.DB_GetData hRS, "SEP_RUN", v
						If v=1 Then
							Response.Write "<input type=""Checkbox"" name=""SEP_RUN"" checked=""1"" value=""1"">September</input>"
						Else
							Response.Write "<input type=""Checkbox"" name=""SEP_RUN"" value=""1"">September</input>"
						End If

						objSMRocket.DB_GetData hRS, "OCT_RUN", v
						If v=1 Then
							Response.Write "<input type=""Checkbox"" name=""OCT_RUN"" checked=""1"" value=""1"">October</input>"
						Else
							Response.Write "<input type=""Checkbox"" name=""OCT_RUN"" value=""1"">October</input>"
						End If

						objSMRocket.DB_GetData hRS, "NOV_RUN", v
						If v=1 Then
							Response.Write "<input type=""Checkbox"" name=""NOV_RUN"" checked=""1"" value=""1"">November</input>"
						Else
							Response.Write "<input type=""Checkbox"" name=""NOV_RUN"" value=""1"">November</input>"
						End If

						objSMRocket.DB_GetData hRS, "DEC_RUN", v
						If v=1 Then
							Response.Write "<input type=""Checkbox"" name=""DEC_RUN"" checked=""1"" value=""1"">December</input>"
						Else
							Response.Write "<input type=""Checkbox"" name=""DEC_RUN"" value=""1"">December</input>"
						End If
					End If
				End If
                %>
            </td>
        </tr>
        <tr>
            <td valign="top" class="ctrlgroup2">
                Output Type:
            </td>
            <td class="datatd">
                <%objSMRocket.DB_GetData hRS, "OUTPUT_TYPE", v:v=LCase(v & "")%>
                <%If bViewOnly=0 Then 'Ask the engine what outputs are supported. BB 03/18/02%>
                <%=GenerateAvailFormats(lReportId)%>
                <!--		<select name="outputtype" height="1" ID="Select1">
			<option value="pdf"  <%=iif(v="pdf" Or v="","selected=""""","")%>>PDF</option>
			<option value="html"  <%=iif(v="html","selected=""""","")%>>HTML</option>
			<option value="xml"  <%=iif(v="xml","selected=""""","")%>>XML</option>
		</select>
-->
                <% 
		'Highlight the current selection.
		Response.Write "<script language='javascript'>var i;for( i=0; i < document.frmData.outputtype.options.length;i++)if (document.frmData.outputtype.options[i].value == '" & v & "' )document.frmData.outputtype.options[i].selected=true;</script>"
		Else
			Response.Write v
		End If
                %>
            </td>
        </tr>
        <tr>
            <td valign="top" class="ctrlgroup2">
                Notification Type:
            </td>
            <td class="datatd">
                <%objSMRocket.DB_GetData hRS, "NOTIFICATION_TYPE", v:v=v & ""%>
                <%If bViewOnly=0 Then%>
                <select name="notifytype" height="1" id="Select2">
                    <%If CBool(bNOTIFY_LINK) Then %>
                    <option value="notifylink" <%=iif(v="notifylink","selected=""""","")%>>
                        <%=NOTIFYLINK_TEXT%></option>
                    <% End If%>
                    <%If CBool(bNOTIFY_EMBED) Then %>
                    <option value="notifyembed" <%=iif(v="notifyembed","selected=""""","")%>>
                        <%=NOTIFYEMBED_TEXT%></option>
                    <% End If%>
                    <%If CBool(bNOTIFY_ONLY) Then %>
                    <option value="notifyonly" <%=iif(v="notifyonly","selected=""""","")%>>
                        <%=NOTIFYONLY_TEXT%></option>
                    <% End If%>
                    <option value="none" <%=iif(v="none" Or v="","selected=""""","")%>>
                        <%=NOTIFYNONE_TEXT%></option>
                </select>
                <%Else
			Response.Write v
		End If
                %>
            </td>
        </tr>
        <tr>
            <td valign="top" class="ctrlgroup2">
                E-Mail To:
            </td>
            <td class="datatd">
                <%objSMRocket.DB_GetData hRS, "NOTIFY_EMAIL", v:v=v & ""%>
                <%If bViewOnly=0 Then%>
                <textarea cols="40" rows="4" name="notifyemail" id="Textarea1"><%=v%></textarea><br />
                <i>(Note: separate multiple email addresses with semicolons)</i>
                <%
		Else
			Response.Write v
		End If
                %>
            </td>
        </tr>
     <!-- Start:Add by kuladeep for update the schedule mits:27574-->
        <tr>
            <td>
            </td>
        </tr>
                <% if reportTypeId = 1 or reportTypeId = 4 or reportTypeId = 3 Then %>
                    <%=GenerateCriteriaHTML() %> 
                 <% End If  %>

                    
        <tr>
            <td>
                <input type="hidden" name="allselecteddata" value="<%=allselecteddata%>"></input>
                 <input type="hidden" name="hidbyoshaestablishmentflag1" value="<%=byoshaestablishmentflag%>"></input>
                 <input type="hidden" name="hidbyoshaestablishmentflag2" value="<%=byoshaestablishmentflag2%>"></input>
                 <input type="hidden" name="hidusereportlevel" value="<%=usereportlevel%>"></input>
                 <input type="hidden" name="hidreportlevel" value="<%=reportlevel%>"></input>
                 <input type="hidden" name="hidestablishmentnameprefix" value="<%=establishmentnameprefix%>"></input>
                 <input type="hidden" name="hiddatemethod" value="<%=datemethod%>"></input>
                 <input type="hidden" name="hidyearofreport" value="<%=yearofreport%>"></input>
                 <input type="hidden" name="hiddatemethod2" value="<%=datemethod2%>"></input>
                 <input type="hidden" name="hidbegindate" value="<%=begindate%>"></input>
                 <input type="hidden" name="hidenddate" value="<%=enddate%>"></input>
                 <input type="hidden" name="hidasofdate" value="<%=asofdate%>"></input>
                 <input type="hidden" name="hidenforce180dayrule" value="<%=enforce180dayrule%>"></input>
                 <input type="hidden" name="hidprintsofterrlog" value="<%=printsofterrlog%>"></input>
                 <input type="hidden" name="hideventbasedflag" value="<%=eventbasedflag%>"></input>
                 <input type="hidden" name="hidprintoshadescflag" value="<%=printoshadescflag%>"></input>
                 <input type="hidden" name="hidcolumnesource" value="<%=columnesource%>"></input>
                 <input type="hidden" name="hidcolumnfsource" value="<%=columnfsource%>"></input>
                 <input type="hidden" name="hidsortorder" value="<%=sortorder%>"></input>
                 <input type="hidden" name="hidallentitiesflag" value="<%=allentitiesflag%>"></input>
                 <input type="hidden" name="hidallentitiesflag2" value="<%=allentitiesflag2%>"></input>
                 <input type="hidden" name="reportTypeId" value="<%=reportTypeId%>"></input>
                <input type="hidden" name="hidpreparertitle" value="<%=preparertitle%>"></input>
                <input type="hidden" name="hidpreparername" value="<%=preparername%>"></input>
                <input type="hidden" name="hidpreparerphone" value="<%=preparerphone%>"></input>
                <input type="hidden" name="hidprimarylocationflag" value="<%=primarylocationflag%>"></input>
                <input type="hidden" name="hidpreparersignaturedate" value="<%=preparersignaturedate%>"></input>              
            </td>
        </tr>

        <!-- End:Add by kuladeep for update the schedule mits:27574-->
        <tr>
            <td colspan="2">
                <%			If bViewOnly=0 Then %>
                <input type="submit" class="button" value="  Save  " id="Submit1" name="Submit1" />&nbsp;&nbsp;
                <%			Else %>
                <input type="button" class="button" onclick="return onClickEdit()" value=" Edit "
                    id="Button1" name="Button1" />&nbsp;&nbsp;
                <%			End If %>
                <input type="button" class="button" value="  Back  " onclick="document.location.href='schedulelist.asp'"
                    id="Button2" name="Button2" />
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
<%
objSMRocket.DB_CloseRecordset CInt(hRS), 2
CloseSMDatabase
%>
