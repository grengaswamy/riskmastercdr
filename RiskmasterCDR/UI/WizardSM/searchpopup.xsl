<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/TR/WD-xsl">
<xsl:template match="/">

<html><head><title><xsl:value-of select="search/@name"/></title>
	<script language="JavaScript" SRC="search.js"></script>
	<link rel="stylesheet" href="RMNet.css" type="text/css"/>
</head>
	<body onload="pageLoaded();">
		<form name="frmSearch" action="searchpopuprun.asp" method="post">
		<table border="0" cellspacing="0" cellpadding="2">
				<tr><td colspan="4" class="ctrlgroup"><xsl:apply-templates select="search"/></td></tr>
				<tr><td><BR/></td></tr>
				<tr><td colspan="3" class="ctrlgroup">Search Criteria</td></tr>
				<xsl:apply-templates select="search/searchfields/field" />
		</table>
		<BR/>
		<table border="0">
				<tr><td colspan="3" class="ctrlgroup">Sort By</td></tr>
				<xsl:apply-templates select="search/displayfields" />
		</table>
		<input type="hidden" name="viewid"><xsl:attribute name="value"><xsl:value-of select="search/@id"/></xsl:attribute></input>
		<input type="hidden" name="dsnid"><xsl:attribute name="value"><xsl:value-of select="search/@dsnid"/></xsl:attribute></input>
		<input type="hidden" name="tablerestrict"><xsl:attribute name="value"><xsl:value-of select="search/@tablerestrict"/></xsl:attribute></input>
		<input type="hidden" name="sys_ex" ><xsl:attribute name="value"><xsl:value-of select="/search/@sys_ex"/></xsl:attribute></input>
		<input type="hidden" name="defaultviewid" ><xsl:attribute name="value"><xsl:value-of select="/search/@defaultviewid"/></xsl:attribute></input>
		<br/>
		<input type="checkbox" name="soundex" value="1"><xsl:if test="search/@soundex[.='-1']"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>Use Sound-Alike (Soundex) Match on Last Names</input>
		<br/>
		<xsl:if test="search/@catid[.!='nosubmit']"><input type="submit" value="Submit Query" class="button"></input></xsl:if>
		
	</form>
	</body>
</html>
</xsl:template>

<xsl:template match="search">
	<tr>
		<td colspan="2" class="ctrlgroup">
			<xsl:value-of select="@name"/>
		</td>
		<td colspan="2" class="ctrlgroup" align="right">
			<xsl:apply-templates select="views"/>
		</td>
	</tr>
</xsl:template>

<xsl:template match="views">
	<select name="cboViews" size="1" onchange="OnViewChange('searchpopup.asp')">
		<option />
		<xsl:for-each select="view">
		<option><xsl:attribute name="value"><xsl:value-of select="@id"/></xsl:attribute><xsl:value-of select="@name"/></option>
		</xsl:for-each>
	</select>
</xsl:template>

<xsl:template match="displayfields">
	<TR>
	
	<TD colSpan="1"><select name="orderby1" size="1">
		<option value=""></option>
		
	<xsl:for-each select="field">
		<option>
			<xsl:attribute name="value"><xsl:value-of select="@id"/></xsl:attribute><xsl:value-of select="text()"/>
		</option>
	</xsl:for-each>

	</select></TD>

	<TD colSpan="1"><select name="orderby2" size="1">
		<option value=""></option>
	
	<xsl:for-each select="field">
		<option>
			<xsl:attribute name="value"><xsl:value-of select="@id"/></xsl:attribute><xsl:value-of select="text()"/>
		</option>
	</xsl:for-each>

	</select></TD>

	<TD colSpan="1"><select name="orderby3" size="1">
		<option value=""></option>

	<xsl:for-each select="field">
		<option>
			<xsl:attribute name="value"><xsl:value-of select="@id"/></xsl:attribute><xsl:value-of select="text()"/>
		</option>
	</xsl:for-each>

	</select></TD>
	
	</TR>

</xsl:template>

<xsl:template match="searchfields/field">
	<tr>
		<td>
			<xsl:value-of select="text()"/>:
		</td>
		<xsl:choose>
			<xsl:when test="@type[.='text']">
				<td>
				<select size="1"><xsl:attribute name="name"><xsl:value-of select="@id"/>op</xsl:attribute>
					<option value="=">=</option><option value="&lt;&gt;">&lt;&gt;</option>
					<option value="&gt;">&gt;</option><option value="&gt;=">&gt;=</option>
					<option value="&lt;">&lt;</option><option value="&lt;=">&lt;=</option>
				</select>
				</td>
				<td>
				<input type="text" size="50"><xsl:attribute name="name"><xsl:value-of select="@id"/></xsl:attribute></input>
				</td>
			</xsl:when>
			<xsl:when test="@type[.='ssn']">
				<td>
				<select size="1"><xsl:attribute name="name"><xsl:value-of select="@id"/>op</xsl:attribute>
					<option value="=">=</option><option value="&lt;&gt;">&lt;&gt;</option>
					<option value="&gt;">&gt;</option><option value="&gt;=">&gt;=</option>
					<option value="&lt;">&lt;</option><option value="&lt;=">&lt;=</option>
				</select>
				</td>
				<td>
				<input type="text" size="50" onblur="ssnLostFocus(this);"><xsl:attribute name="name"><xsl:value-of select="@id"/></xsl:attribute></input>
				</td>
			</xsl:when>
			<xsl:when test="@type[.='textml']">
				<td>
				<select size="1"><xsl:attribute name="name"><xsl:value-of select="@id"/>op</xsl:attribute>
					<option value="=">=</option>
				</select>
				</td>
				<td>
				<xsl:element name="textarea" xml:space="preserve">
					<xsl:attribute name="cols"><xsl:value-of select="@cols"/></xsl:attribute>
					<xsl:attribute name="rows"><xsl:value-of select="@rows"/></xsl:attribute>
					<xsl:attribute name="name"><xsl:value-of select="@id"/></xsl:attribute>
				</xsl:element>
				</td>
			</xsl:when>
			<xsl:when test="@type[.='code']">
				<td>
				<select size="1"><xsl:attribute name="name"><xsl:value-of select="@id"/>op</xsl:attribute>
					<option value="=">=</option><option value="&lt;&gt;">&lt;&gt;</option>
				</select>
				</td>
				<td>
				<!--<input type="text" size="30" onblur="codeLostFocus(this.name);">-->
          <input type="text" size="30" onblur="codeLostFocus(this.name);" onchange="lookupTextChanged(this);">
					<xsl:attribute name="name"><xsl:value-of select="@id"/></xsl:attribute>
					<xsl:choose>
						<xsl:when test="@name[.='LINE_OF_BUS_CODE']">
							<xsl:choose>
								<xsl:when test="/search/@tablerestrict[.='gc']">
									<xsl:attribute name="value">GC General Claims</xsl:attribute>
								</xsl:when>
								<xsl:when test="/search/@tablerestrict[.='va']">
									<xsl:attribute name="value">VA Vehicle Accident Claims</xsl:attribute>
								</xsl:when>
								<xsl:when test="/search/@tablerestrict[.='wc']">
									<xsl:attribute name="value">WC Workers' Compensation</xsl:attribute>
								</xsl:when>
							</xsl:choose>
						</xsl:when>
					</xsl:choose>
				</input>
				
				<input type="button" value="..." class="button">
				<xsl:attribute name="name"><xsl:value-of select="@id"/>btn</xsl:attribute>
				<xsl:attribute name="onClick">selectCode('<xsl:value-of select="@codetable"/>','<xsl:value-of select="@id"/>')</xsl:attribute>
				</input>
				
				<xsl:choose>
					<xsl:when test="@name[.='LINE_OF_BUS_CODE']">
						<xsl:choose>
							<xsl:when test="/search/@tablerestrict[.='gc']">
								<input type="hidden" value="241"><xsl:attribute name="name"><xsl:value-of select="@id"/>code</xsl:attribute></input>
							</xsl:when>
							<xsl:when test="/search/@tablerestrict[.='va']">
								<input type="hidden" value="242"><xsl:attribute name="name"><xsl:value-of select="@id"/>code</xsl:attribute></input>
							</xsl:when>
							<xsl:when test="/search/@tablerestrict[.='wc']">
								<input type="hidden" value="243"><xsl:attribute name="name"><xsl:value-of select="@id"/>code</xsl:attribute></input>
							</xsl:when>
							<xsl:when test="/search/@tablerestrict[.='di']">
								<input type="hidden" value="844"><xsl:attribute name="name"><xsl:value-of select="@id"/>code</xsl:attribute></input>
							</xsl:when>
							<xsl:otherwise>
								<input type="hidden" value="0"><xsl:attribute name="name"><xsl:value-of select="@id"/>code</xsl:attribute></input>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<input type="hidden" value="0"><xsl:attribute name="name"><xsl:value-of select="@id"/>code</xsl:attribute></input>
					</xsl:otherwise>					
				</xsl:choose>
				</td>
			</xsl:when>
			<xsl:when test="@type[.='date']">
				<td>
				<select size="1"><xsl:attribute name="name"><xsl:value-of select="@id"/>op</xsl:attribute>
					<option value="=">=</option><option value="&lt;&gt;">&lt;&gt;</option>
					<option value="&gt;">&gt;</option><option value="&gt;=">&gt;=</option>
					<option value="&lt;">&lt;</option><option value="&lt;=">&lt;=</option>
					<option selected="" value="between">Between</option>
				</select>
				</td>
				<td>
				<input type="text" size="15" onblur="dateLostFocus(this.name);"><xsl:attribute name="name"><xsl:value-of select="@id"/>start</xsl:attribute></input>
				<input type="text" size="15" onblur="dateLostFocus(this.name);"><xsl:attribute name="name"><xsl:value-of select="@id"/>end</xsl:attribute></input>
				</td>
			</xsl:when>
			<xsl:when test="@type[.='time']">
				<td>
				<select size="1"><xsl:attribute name="name"><xsl:value-of select="@id"/>op</xsl:attribute>
					<option value="=">=</option><option value="&lt;&gt;">&lt;&gt;</option>
					<option value="&gt;">&gt;</option><option value="&gt;=">&gt;=</option>
					<option value="&lt;">&lt;</option><option value="&lt;=">&lt;=</option>
					<option value="between">Between</option>
				</select>
				</td>
				<td>
				<input type="text" size="15" onblur="timeLostFocus(this.name);"><xsl:attribute name="name"><xsl:value-of select="@id"/>start</xsl:attribute></input>
				<input type="text" size="15" onblur="timeLostFocus(this.name);"><xsl:attribute name="name"><xsl:value-of select="@id"/>end</xsl:attribute></input>
				</td>
			</xsl:when>
			<xsl:when test="@type[.='orgh']">
				<td>
				<select size="1"><xsl:attribute name="name"><xsl:value-of select="@id"/>op</xsl:attribute>
					<option value="=">=</option><option value="&lt;&gt;">&lt;&gt;</option>
				</select>
				</td>
				<td>
				<input type="text" size="30" onblur="codeLostFocus(this.name);"><xsl:attribute name="name"><xsl:value-of select="@id"/></xsl:attribute></input><input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@id"/>btn</xsl:attribute><xsl:attribute name="onClick">selectCode('orgh','<xsl:value-of select="@id"/>')</xsl:attribute></input>
				<input type="hidden" value="0"><xsl:attribute name="name"><xsl:value-of select="@id"/>code</xsl:attribute></input>
				</td>
			</xsl:when>
			<xsl:when test="@type[.='checkbox']">
				<td>
				<select size="1"><xsl:attribute name="name"><xsl:value-of select="@id"/>op</xsl:attribute>
					<option value="=">=</option><option value="&lt;&gt;">&lt;&gt;</option>
				</select>
				</td>
				<td>
				<input type="checkbox"><xsl:attribute name="name"><xsl:value-of select="@id"/></xsl:attribute></input>
				</td>
			</xsl:when>
			<xsl:when test="@type[.='numeric']">
				<td>
				<select size="1"><xsl:attribute name="name"><xsl:value-of select="@id"/>op</xsl:attribute>
					<option value="=">=</option><option value="&lt;&gt;">&lt;&gt;</option>
					<option value="&gt;">&gt;</option><option value="&gt;=">&gt;=</option>
					<option value="&lt;">&lt;</option><option value="&lt;=">&lt;=</option>
				</select>
				</td>
				<td>
				<input type="text" size="30"><xsl:attribute name="name"><xsl:value-of select="@id"/></xsl:attribute></input>
				</td>
			</xsl:when>
			<xsl:when test="@type[.='currency']">
				<td>
				<select size="1"><xsl:attribute name="name"><xsl:value-of select="@id"/>op</xsl:attribute>
					<option value="=">=</option><option value="&lt;&gt;">&lt;&gt;</option>
					<option value="&gt;">&gt;</option><option value="&gt;=">&gt;=</option>
					<option value="&lt;">&lt;</option><option value="&lt;=">&lt;=</option>
				</select>
				</td>
				<td>
				<input type="text" size="30"><xsl:attribute name="name"><xsl:value-of select="@id"/></xsl:attribute></input>
				</td>
			</xsl:when>
			<xsl:when test="@type[.='tablelist']">
				<td>
				<select size="1"><xsl:attribute name="name"><xsl:value-of select="@id"/>op</xsl:attribute>
					<option value="=">=</option><option value="&lt;&gt;">&lt;&gt;</option>
				</select>
				</td>
				<td>
				<select size="1"><xsl:attribute name="name"><xsl:value-of select="@id"/></xsl:attribute>
					<option value=""></option>
					<xsl:for-each select="tablevalue">
						<option><xsl:attribute name="value"><xsl:value-of select="@tableid"/></xsl:attribute>
							<xsl:value-of select="text()"/>
						</option>
					</xsl:for-each>
				</select>
				</td>
			</xsl:when>
			<xsl:when test="@type[.='codelist']">
				<td>
				<select size="1"><xsl:attribute name="name"><xsl:value-of select="@id"/>op</xsl:attribute>
					<option value="=">in</option>
				</select>
				</td>
				<td>
				<select size="3"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
				<xsl:apply-templates select="option">
					<xsl:template><xsl:copy><xsl:apply-templates select="@* | * | comment() | pi() | text()"/></xsl:copy></xsl:template>
				</xsl:apply-templates>
				</select> 
				<input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">selectCode('<xsl:value-of select="@codetable"/>','<xsl:value-of select="@name"/>')</xsl:attribute></input>
				<input type="button" class="button" value=" - "><xsl:attribute name="name"><xsl:value-of select="@name"/>btndel</xsl:attribute><xsl:attribute name="onClick">deleteSelCode('<xsl:value-of select="@name"/>')</xsl:attribute></input>
				<input type="hidden"><xsl:attribute name="name"><xsl:value-of select="@name"/>_lst</xsl:attribute>
	        	<xsl:attribute name="value"><xsl:for-each select="option"><xsl:value-of select="@value"/><xsl:if test="context()[not(end())]">,</xsl:if></xsl:for-each></xsl:attribute>
				</input>
				</td>
	        </xsl:when>
			<xsl:when test="@type[.='entitylist']">
				<td>
				<select size="1"><xsl:attribute name="name"><xsl:value-of select="@id"/>op</xsl:attribute>
					<option value="=">in</option>
				</select>
				</td>
				<td>
				<select size="3"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
				<xsl:apply-templates select="option">
					<xsl:template><xsl:copy><xsl:apply-templates select="@* | * | comment() | pi() | text()"/></xsl:copy></xsl:template>
				</xsl:apply-templates>
				</select> 
				<input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">lookupData('<xsl:value-of select="@name"/>','<xsl:value-of select="@tableid"/>',4,'<xsl:value-of select="@name"/>',3)</xsl:attribute></input>
				<input type="button" class="button" value=" - "><xsl:attribute name="name"><xsl:value-of select="@name"/>btndel</xsl:attribute><xsl:attribute name="onClick">deleteSelCode('<xsl:value-of select="@name"/>')</xsl:attribute></input>
				<input type="hidden"><xsl:attribute name="name"><xsl:value-of select="@name"/>_lst</xsl:attribute>
	        	<xsl:attribute name="value"><xsl:for-each select="option"><xsl:value-of select="@value"/><xsl:if test="context()[not(end())]">,</xsl:if></xsl:for-each></xsl:attribute>
				</input>
				</td>
			</xsl:when>
			<xsl:otherwise>
				<td>
				Unknown Element Type: <xsl:value-of select="@type"/>
				</td>
			</xsl:otherwise>
		</xsl:choose>
	</tr>
</xsl:template>

</xsl:stylesheet>
