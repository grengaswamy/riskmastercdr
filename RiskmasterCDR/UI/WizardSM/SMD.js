// Author:		Rajeev Chauhan, CSC India
// Co-author:	Aashish Bhateja, CSC India

	var m_sFieldName = "";
	var m_codeWindow=null;
	var m_DataChanged=false;
	var m_FLAG;						//Tanu: added for MITS 9812
	
	var p = new Array;
	var arrCurrentFields = new Array;
	var pFields = new Array;
	var pXML = new Array;
	var pcount;
	var t = new Array;
	var tf = new Array;
	var c = new Array;
	var pindex = new Array;
	var arrFieldType = new Array;
	var pColProp = new Array;

	var g_curlevel = 1;

	var arr1 = new Array;
	var bSubmitted = "false";

	arr1[0] = 'Clm_Accident(Code)';
	arr1[1] = 'Clm_Claim Date';
	arr1[2] = 'Clm_Claim Number';
			
	arr1[3] = 'Claimants';
	arr1[4] = 'Defendants';
			
	arr1[5] = 'Bank_Addr1';
	arr1[6] = 'Bank_Addr2';
	arr1[7] = 'Bank_City';

	arr1[8] = 'Bank Accounts';
	arr1[9] = 'Bank - Supplementals';
	
	function saveReport(pTabID, pPost)
	{
		window.document.frmData.hdPost.value = pPost;
			
		if (window.document.frmData.hdSaveFinal.value != "1" || pPost == '3') // Save As...
			{	
				if((pPost == '3') && (window.document.frmData.hdMode1.value == '0'))
					{
 					window.document.frmData.hdPost.value = '0'; // force normal save
 					pPost = '0';
 					}

				window.open('savereport.asp?TABID='+pTabID+'&POST='+pPost, 'saverep', 'resizable=yes,Width=460,Height=210,top=' + (screen.availHeight-210)/2 + ',left=' + (screen.availWidth-460)/2 + '')
			}
		else
			tabNavigation(pTabID, pTabID);
	}			

	function ReportName(pTabId, pPost)
	{
		if (trimIt(window.document.frmData.txtFileName.value) == "")
		{
			alert("Please enter the Report Name");
			window.document.frmData.txtFileName.focus();
			return false;
		}
		else
		{
			window.opener.window.document.frmData.hdRptName.value = window.document.frmData.txtFileName.value;
			window.opener.window.document.frmData.hdRptDesc.value = window.document.frmData.txtFileDesc.value;
						
			if(pPost == '3')
				window.opener.window.document.frmData.hdSave.value = pPost;
			else
				window.opener.window.document.frmData.hdSave.value = "1";
				
			window.opener.window_onLoad();
			window.opener.tabNavigation(pTabId, pTabId);

			if (window.opener.window.document.frmData.hdPreview.value == "1")
			{
				var wnd = window.open('smwdPreviewStatus.asp','wndProgress','resizable=yes,width=400,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-400)/2);
			}
			m_FLAG="TRUE";
			window.close();
		}
	}
	
	function Window_OnSubmit(pPageID) // Page to be submitted
	{
		switch(parseInt(pPageID))
		{
			case 0:	
					//-- Rajeev -- 03/09/2004
					//window.document.frmData.hdSelFields.value = p;
					window.document.frmData.hdDataType.value = arrFieldType;
					window.document.frmData.hdColProp.value = pColProp;
					//-- Rajeev -- 03/09/2004
					//window.document.frmData.hdArrayPFields.value = pFields;
					window.document.frmData.hdArrayTF.value = tf;
					window.document.frmData.hdSelFieldsLength.value = p.length;
					window.document.frmData.hdSelFieldsXML.value = pXML;

					window.document.frmData.hdSelFields.value = '';
					for(i=0;i<p.length;i++)
						if(window.document.frmData.hdSelFields.value == '')
							window.document.frmData.hdSelFields.value = p[i];
						else
							window.document.frmData.hdSelFields.value = window.document.frmData.hdSelFields.value + '*!^' + p[i];
											
					window.document.frmData.hdArrayPFields.value = '';
					for(i=0;i<pFields.length;i++)
						if(window.document.frmData.hdArrayPFields.value == '')
							window.document.frmData.hdArrayPFields.value = pFields[i];
						else
							window.document.frmData.hdArrayPFields.value = window.document.frmData.hdArrayPFields.value + '*!^' + pFields[i];
					
										
					if (window.document.frmData.chkAsOf.checked && window.document.frmData.txtDtofevnt.value != '')
					{
						window.document.frmData.hdAsOfSelected.value = "1";
						var arr;
						var temp;
						arr = window.document.frmData.txtDtofevnt.value.split("/");
						temp = arr[2] + arr[1] + arr[0];
						window.document.frmData.hdDate.value = temp;
					}
					break;
					
			case 1:
					if (pCriteria.length == 0)
						window.document.frmData.hdPCriteria.value = -1;
					else
						window.document.frmData.hdPCriteria.value = pCriteria;
						
					if (pOLECriteria.length == 0)
						window.document.frmData.hdPOLECriteria.value = -1;
					else
						window.document.frmData.hdPOLECriteria.value = pOLECriteria;
					
					if (pDateFormula.length == 0)
						window.document.frmData.hdDateFormula.value = -1;
					else
						window.document.frmData.hdDateFormula.value = pDateFormula;
						
					if (pGlobal.length == 0)
						window.document.frmData.hdPGlobalCriteria.value = -1;
					else
						window.document.frmData.hdPGlobalCriteria.value = pGlobal;
											
					break;
					
			case 2:			
					if (pOptions.length == 0)
						window.document.frmData.hdPOptions.value = '-1';
					else
						window.document.frmData.hdPOptions.value = pOptions;
												
					break;
					
			case 3:
					break;
			case 4:
					break;
			case 5:
					break;
		}
	}
		
	function tabNavigation(pTabID, pPageID)
	{
		Window_OnSubmit(pPageID);
		window.document.frmData.hdTabID.value = pTabID;
		window.document.frmData.submit();
		//bSubmitted = "true";
	}	

	function pageInitialize()
	{	
		document.dateSelected = dateSelected;
		document.onCodeClose=onCodeClose;
	}

	function pageLoaded()
	{	
		document.dateSelected = dateSelected;
		document.onCodeClose=onCodeClose;

		window.document.frmData.hdPost.value = "0";
	
		if (document.frmData.hdArrayTF.value != '-1')
			tf = document.frmData.hdArrayTF.value.split(',')
	
		if (document.frmData.hdArrayPFields.value != '-1') // Initialization with custom session
			pFields = document.frmData.hdArrayPFields.value.split('*!^')
//-- Rajeev -- 03/09/2004
//			pFields = document.frmData.hdArrayPFields.value.split(',')


		if (document.frmData.hdArrayCurrentFields.value != '-1') // Initialization with custom session
			arrCurrentFields = document.frmData.hdArrayCurrentFields.value.split('*!^')
//-- Rajeev -- 03/09/2004
//			arrCurrentFields = document.frmData.hdArrayCurrentFields.value.split(',')
					
		if (document.frmData.hdSelFields.value != '-1')
		{
//-- Rajeev -- 03/09/2004
//			p = document.frmData.hdSelFields.value.split(',')
			p = document.frmData.hdSelFields.value.split('*!^')
			
			pXML = document.frmData.hdSelFieldsXML.value.split("</column>,");
			for (iCounter = 0;iCounter<pXML.length-1;iCounter++)
				pXML[iCounter] = pXML[iCounter] + "</column>";

			arrFieldType = document.frmData.hdDataType.value.split(',')
			
			pColProp = document.frmData.hdColProp.value.split(',')
			
			for (i=0;i<p.length;i++)
				pindex[i] = i;
		}
		return true;
	}

	function CriteriaLoaded()
	{
		document.dateSelected = dateSelected;
		return true;
	}
							
	function changeselection()
	{
		document.frmData.txtCurrentLevel.value = document.frmData.cboTables.options[window.document.frmData.cboTables.selectedIndex].text;
				
		if (window.document.frmData.cboTables.selectedIndex == 0)
		{
			document.frmData.lstTableFields.options[0].text = arr1[0];
			document.frmData.lstTableFields.options[1].text = arr1[1];
			document.frmData.lstTableFields.options[2].text = arr1[2];
			document.frmData.lstUsers.options[0].text = arr1[3];
			document.frmData.lstUsers.options[1].text = arr1[4];					
		}
		else
		{
			document.frmData.lstTableFields.options[0].text = arr1[5];
			document.frmData.lstTableFields.options[1].text = arr1[6];
			document.frmData.lstTableFields.options[2].text = arr1[7];
			document.frmData.lstUsers.options[0].text = arr1[8];
			document.frmData.lstUsers.options[1].text = arr1[9];
		}	

		document.frmData.lstTableFields.selectedIndex = 0;						
		document.frmData.lstUsers.selectedIndex = 0;
	}
						
   function AddFields()
   {
		var k,selected;
		for(k = 0,selected = 0;k < document.frmData.lstTableFields.length;k++)
			{
				if(document.frmData.lstTableFields.item(k).selected)
				{
					selected = selected + 1;
				}
			}
		if (document.frmData.lstselfields.length >= 15 || selected > 15)
		{
			if(selected > 15)
				document.frmData.lstselfields.size = document.frmData.lstselfields.size + (selected-15);
			else
				document.frmData.lstselfields.size = document.frmData.lstselfields.size + selected;
				//document.frmData.lstselfields.height = document.frmData.lstselfields.height + selected;
		}
			
	   	pcount = document.frmData.lstselfields.length;

	   	var txt = new Array;
	   	var cStr;
	   	var oOption;

	   	//-- For selection of fields
	   	for (k=0,selected=0;k<document.frmData.lstTableFields.length;k++)
	   	{
	   		if (document.frmData.lstTableFields.item(k).selected)
	   		{
	   			cStr = document.frmData.lstTableFields.item(k).text;

	   			//^-- Rajeev 05/26/2003 -- z format is : fieldnumber|fieldid|usercolname|tableid|fieldtype
	   			z = arrCurrentFields[k];
	   			y = z.split('|');
	   			
	   			cTemp = document.frmData.txtCurrentLevel.value + ' : ' + cStr;

	   			oOption = new Option(cTemp ,cTemp);
	   			document.frmData.lstselfields.options[document.frmData.lstselfields.options.length] = oOption;

	   			//-- Gives the name of items to be newly added to lstSelfields
	   			txt[selected] = document.frmData.txtCurrentLevel.value + ' : ' + document.frmData.lstTableFields.item(k).text;

	   			//-- Gives the no. of items to be newly added to lstSelfields
	   			selected = selected + 1;

	   			p[pcount] = cTemp;
	   			bFound = 0;
	   			for (n=0;n<pFields.length;n++)
	   			{
	   				if (pFields[n].toLowerCase() == cTemp.toLowerCase())
	   				{
	   					bFound = 1;
	   					break;
	   				}
	   			}
	   			if (bFound == 0)
	   				pFields[pFields.length] = cTemp;
									
	   			arrParentID = document.frmData.hdStackTableID.value.split(',');
	   			
	   			str = '';
	   			if (arrParentID.length == 1)
	   				str = '<parenttables />';
	   			else
	   			{
	   				for (i=arrParentID.length - 1;i > 0;i--)
	   				{
	   					if (str.length == 0)
	   						str = '<parenttables><parenttable>' + arrParentID[i - 1] + '</parenttable>';
	   					else
	   						str = str + '<parenttable>' + arrParentID[i - 1] + '</parenttable>';
	   				}
	   				str = str + '</parenttables>';
	   			}

	   			var iFieldNumber = 1;
	   			if (pcount != 0)
				{
					var arrPos;
					arrPos = FindCharacter(pXML[pXML.length-1], '"', 2).split(',');
					iFieldNumber = parseInt(pXML[pcount-1].substr(parseInt(arrPos[0]),parseInt(arrPos[1])-1-parseInt(arrPos[0]))) + 1;
				}
				
	   			pXML[pcount] = '<column fieldnumber="'+iFieldNumber+'" tableid="'+y[3]+'" fieldid="'+y[1]+'" usercolname="'+y[2]+'" colwidthunits="chars" colwidth="" justify="left" color="0" >' + str + '</column>';
	   			pindex[pcount] = pcount;
	   			tf[pcount] = pcount;
	   			arrFieldType[pcount] = y[4];
	   			pColProp[pcount] = '';
	   			pcount = pcount + 1;
	   		}
	   	}

	   	//-- Gives the no. of items in the column list.
	   	len = document.frmData.lstselfields.length;

	   	//for preserve arrray--start
        //-- Outer loop for the newly selected fields(for arranging numberwise e.g. F1,F2)
	    for (k=0;k<selected;k++)
	   	{				
	   		for (j=0;j<len-selected;j++)	//-- Excludes newly added fields that already existed
	   		{
	   			if (txt[k].toLowerCase() == p[j].toLowerCase())//-- First time renaming
	   			{
	   				var count = 1;
	   				p[j] = txt[k] + count;
	   				count = count + 1;
	   				p[k+len-selected] = txt[k] + count;
	   				break;
	   			}
	   			//-- Finding last index of matching substrings 
	   			else if ((txt[k] == p[j].substring(0,txt[k].length)) && (p[j].charCodeAt(txt[k].length) != 32))
	   			{
	   				for (i=0,n=1;i<len-selected;i++)
	   				{
	   					if ((txt[k] == p[i].substring(0,txt[k].length)) && (p[i].charCodeAt(txt[k].length) != 32))
	   						n = n + 1;
	   					else
	   						continue;
	   				}
	   				p[k+len-selected] = txt[k] + n;		
	   				break;
	   			}
	   			else //-- New unmatched field.
	   				p[len-1] = txt[k];
	   		}//for preserve array--end
	   	}	
		//-- Alternate Code to maintain the multiple occurences of a field in lstselfields(Using array tf[])
		for (i=0;i<tf.length;i++)
		{
	   		for (j = 0;j < pindex.length;j++)
	   		{
	   			if (tf[i] == pindex[j])
	   				document.frmData.lstselfields.options[i].text = p[pindex[j]] + pColProp[j];
	   		}
	    }
	   	document.frmData.lstselfields.selectedIndex = document.frmData.lstselfields.options.length - 1;
		if (document.frmData.lstselfields.length > 0)
			document.frmData.btnProperties.disabled = false;
		else
			document.frmData.btnProperties.disabled = true ;

	   	if (document.frmData.lstselfields.selectedIndex > 0)
	   		document.frmData.btnUp.disabled = false;
	   	else
	   		document.frmData.btnUp.disabled = true;

	   	if (document.frmData.lstselfields.selectedIndex < document.frmData.lstselfields.length-1 )
	   		document.frmData.btnDown.disabled = false;
	   	else
	   		document.frmData.btnDown.disabled = true;

		if (window.document.frmData.lstselfields.length == 1)
		{
			window.document.frmData.btnUp.disabled = true;
			window.document.frmData.btnDown.disabled = true;
		}
	   	
	   	if (document.frmData.lstselfields.length > 0)
	   		document.frmData.btnDel.disabled = false;

	   	if (document.frmData.lstselfields.length > 0)
	   	{
	   		document.frmData.cboTables.disabled = true;

	   		if (document.frmData.lstUsers.length > 0)
	   			document.frmData.btnDnlvl.disabled = false;
	   	}
	}
							
	function selFields()
	{
		if (document.frmData.lstselfields.selectedIndex > 0)
			document.frmData.btnUp.disabled = false;
		else
			document.frmData.btnUp.disabled = true;
					
		if (document.frmData.lstselfields.selectedIndex < document.frmData.lstselfields.length - 1)
			document.frmData.btnDown.disabled = false;
		else
			document.frmData.btnDown.disabled = true;
	}
			
	function DelFields()
	{
		var instCount, instCount1;
		var c = new Array;
		selIndex = document.frmData.lstselfields.selectedIndex;
		selTxt = trimIt(document.frmData.lstselfields.options[selIndex].text.substring(0,(trimIt(document.frmData.lstselfields.options[selIndex].text).length-trimIt(pColProp[tf[selIndex]]).length)));

		//-- Check for derived field
		if (selTxt.substring(0,9) == "Derived :")
		{
			alert('This is a Derived Column.To delete it select Derived Fields from tools menu');
			return;
		}
		
		//-- Check for involved column
		if (document.frmData.hdDerColumnInvolved.value.indexOf(selTxt.substring(selTxt.indexOf(":")+1,selTxt.length)) > -1)
		{
			alert("Column Involved in a Derived Field.Can't delete it.");
			return;
		}
		
		selLength = document.frmData.lstselfields.length;
		var matchString;
		var matchString1;
		instCount = 0;
		instCount1 = 0;
		var instance = 0;

		for (i=0;i<pFields.length;i++)
		{
			if (instance == 0)
			{
				if (pFields[i] != null)
				{
					matchString = pFields[i];
					if (trimIt(matchString) == trimIt(selTxt.substring(0,matchString.length)))
						for (j=0;j<selLength;j++)
						{
							if (instance == 0)
								instance = selTxt.substring(pFields[i].length,document.frmData.lstselfields.options[j].text.length)	//Gives the instance of the selected column			
							if (matchString == document.frmData.lstselfields.options[j].text.substring(0,matchString.length))
								instCount = instCount+1;		
						}
				}
			}
			else
				break;
		}
		
		if (trimIt(selTxt) == trimIt(p[0]) && document.frmData.lstselfields.length > 1)  //1st element selected(Case 1)
			alert('To delete this column delete other columns first')
		else if (trimIt(document.frmData.lstselfields.options[0].text.substring(0,(trimIt(document.frmData.lstselfields.options[0].text).length-trimIt(pColProp[0]).length))) == p[0] && document.frmData.lstselfields.length == 1)  //1st element selected(Case 2)
		{
			document.frmData.lstselfields.options[selIndex] = null;
			p[0] = null;
			arrFieldType[0] = null;
			pColProp[0] = null;
			pXML[0] = null;
			adjust_tf();
			adjust_pArray(p);
			adjust_pArray(arrFieldType);
			adjust_pArray(pColProp);
			adjust_pArray(pXML);
			selIndex = 0;
		}
		else if (selIndex == document.frmData.lstselfields.length - 1 && instCount <= 1)  
		{
			for (j=0;j<p.length;j++)
			{
				if(trimIt(p[j]) == selTxt)
				{
				
					p[j] = null;
					arrFieldType[j] = null;
					pColProp[j] = null;
					pXML[j] = null;
					
					adjust_tf();
					
					adjust_pArray(p);					
					adjust_pArray(arrFieldType);					
					adjust_pArray(pColProp);					
					adjust_pArray(pXML);
										
					break;
				}
			}
			document.frmData.lstselfields.options[selIndex] = null;
			document.frmData.lstselfields.selectedIndex = selLength - 2;
		}
		else if (instCount > 1)
		{
			for (i=0;i<pFields.length;i++)
			{
				matchString1 = pFields[i];
				for (k=0;k<p.length;k++)
				{
					if (p[k] == selTxt)
						break;
				}
				
				if (p[k] != null)
					found = p[k].substring(matchString1.length,p[k].length);
				else
					break;	
				
				if (matchString1 == p[k].substring(0,matchString1.length))
				{	
					for (j=0;j<p.length;j++)
					{
						if (p[j] != null && matchString1==p[j].substring(0,matchString1.length))
						{
							if (parseInt(p[j].substring(matchString1.length,p[j].length))== parseInt(found))
							{
								p[j] = null;
								arrFieldType[j] = null;
								pColProp[j] = null;
								pXML[j] = null;
								continue;
							}
							
							if (parseInt(p[j].substring(matchString1.length,p[j].length)) > parseInt(found))
								if (instCount != 2)
									p[j] = matchString1+(parseInt(p[j].substring(matchString1.length,p[j].length))-1);
								else
									p[j] = matchString1;
													
							if (parseInt(p[j].substring(matchString1.length,p[j].length)) < parseInt(found) && instCount == 2)
									p[j] = matchString1;									
						}
					}
				}
			}
			adjust_tf();
			adjust_pArray(p);
			adjust_pArray(arrFieldType);
			adjust_pArray(pColProp);
			adjust_pArray(pXML);
			
			for (i=0;i<tf.length;i++)
			{
				for (j=0;j<pindex.length;j++)
				{
					if (tf[i] == pindex[j])
						document.frmData.lstselfields.options[i].text = p[pindex[j]] + pColProp[j];
				}
			}

			for (i=tf.length;i<document.frmData.lstselfields.length;i++)
				document.frmData.lstselfields.options[i] = null;

			if (selIndex != selLength - 1)
				document.frmData.lstselfields.selectedIndex = selIndex;		
			else
				document.frmData.lstselfields.selectedIndex = selLength - 2;
		}
		else
		{
			for (j=0;j<p.length;j++)
			{
				if (p[j] == selTxt)
				{
					p[j] = null;
					arrFieldType[j] = null;
					pColProp[j] = null;
					pXML[j] = null;
					
					adjust_tf();					
					adjust_pArray(pXML);
					adjust_pArray(p);
					adjust_pArray(arrFieldType);
					adjust_pArray(pColProp);

					break;
				}							
			}
			document.frmData.lstselfields.options[selIndex] = null;
			document.frmData.lstselfields.selectedIndex = selIndex;
		}

		if (document.frmData.lstselfields.length <= 0)
			document.frmData.btnDel.disabled = true;
		
		if (document.frmData.lstselfields.length <= 0)
			document.frmData.cboTables.disabled = false;	
		
		if (document.frmData.lstselfields.length <= 0)
		{
			document.frmData.btnProperties.disabled = true;
			document.frmData.btnDnlvl.disabled = true;
		}
		
		adjust_pFields();
		
		if (document.frmData.lstselfields.length == 0)
		{
			document.frmData.hdSubmit.value = "DEL_FIELDS"
			tabNavigation(0, 0);
		}
		
		if (document.frmData.lstselfields.length == 1)
		{
			window.document.frmData.btnUp.disabled = true;
			window.document.frmData.btnDown.disabled = true;	
		}
		
		if(document.frmData.lstselfields.selectedIndex == 1 && document.frmData.lstselfields.length > 1)
		{
			window.document.frmData.btnUp.disabled = true;
			window.document.frmData.btnDown.disabled = false;
		}

		if(document.frmData.lstselfields.selectedIndex == document.frmData.lstselfields.length-1 && document.frmData.lstselfields.length > 1)
		{
			window.document.frmData.btnUp.disabled = false;
			window.document.frmData.btnDown.disabled = true;
		}
	}
					
	function trackLevelup()
	{
		var c = new Array;
		var len = document.frmData.lstselfields.length;

		for (i=0;i<len;i++)
			c[i] = trimIt(document.frmData.lstselfields.options[i].text.substring(0,(trimIt(document.frmData.lstselfields.options[i].text).length-trimIt(pColProp[tf[i]]).length)));

		if (document.frmData.lstselfields.selectedIndex > 0)
		{
			temp = document.frmData.lstselfields.options[window.document.frmData.lstselfields.selectedIndex].text;
			document.frmData.lstselfields.options[window.document.frmData.lstselfields.selectedIndex].text = document.frmData.lstselfields.options[window.document.frmData.lstselfields.selectedIndex-1].text;
			document.frmData.lstselfields.options[window.document.frmData.lstselfields.selectedIndex-1].text = temp;
			
			temp = c[window.document.frmData.lstselfields.selectedIndex];
			c[window.document.frmData.lstselfields.selectedIndex] = c[window.document.frmData.lstselfields.selectedIndex-1]
			c[window.document.frmData.lstselfields.selectedIndex-1] = temp;
		}
		
		window.document.frmData.lstselfields.selectedIndex = window.document.frmData.lstselfields.selectedIndex-1;
		
		for (j=0;j<len;j++)
		{
			for (i=0;i<len;i++)
			{
				if (c[j] == p[i])
					tf[j] = i;
			}
		}
	}
	
	function trackLevel()
	{
		var a = new Array;
		var b = new Array;
		var len = document.frmData.lstselfields.length;

		for (i=0;i<len;i++)
			c[i] = trimIt(document.frmData.lstselfields.options[i].text.substring(0,(trimIt(document.frmData.lstselfields.options[i].text).length-trimIt(pColProp[tf[i]]).length)));
		
		if (document.frmData.lstselfields.selectedIndex != len - 1)
		{
			temp = document.frmData.lstselfields.options[window.document.frmData.lstselfields.selectedIndex].text;
			document.frmData.lstselfields.options[window.document.frmData.lstselfields.selectedIndex].text = document.frmData.lstselfields.options[window.document.frmData.lstselfields.selectedIndex+1].text;
			document.frmData.lstselfields.options[window.document.frmData.lstselfields.selectedIndex+1].text = temp;
			
			temp = c[window.document.frmData.lstselfields.selectedIndex];
			c[window.document.frmData.lstselfields.selectedIndex] = c[window.document.frmData.lstselfields.selectedIndex+1]
			c[window.document.frmData.lstselfields.selectedIndex+1] = temp;
		}
		
		window.document.frmData.lstselfields.selectedIndex = window.document.frmData.lstselfields.selectedIndex + 1;
				
		for (j=0;j<len;j++)
		{
			for (i=0;i<len;i++)
			{
				if (c[j] == p[i])
					tf[j] = i;
			}
		}
	}
			
	function changelevel(varUpOrDown)
	{
		if (varUpOrDown == 0)
			g_curlevel = g_curlevel - 1;
		else
			g_curlevel = g_curlevel + 1;

		if (g_curlevel == 1)
		{
			document.frmData.btnUplvl.disabled = true;
			document.frmData.btnDnlvl.disabled = false;
		}
		else
		{
			document.frmData.btnUplvl.disabled = false;
			document.frmData.btnDnlvl.disabled = true;
		}
	}			

	function selectDate(sFieldName)
	{
		m_sFieldName=sFieldName;
		
		//window.open('calendar1.html','_new','menubar=no,toolbar=no,width=250,height=250,top='+(screen.availHeight-250)/2+',left='+(screen.availWidth-250)/2+'')		
		m_codeWindow=window.open('calendar.html','codeWnd','width=230,height=230'+',top='+(screen.availHeight-230)/2+',left='+(screen.availWidth-230)/2+',resizable=yes,scrollbars=no');
		return false;
	}
	
	function dateSelected(sDay, sMonth, sYear)
	{
		var objCtrl = null;

		if(m_codeWindow!=null)
			m_codeWindow.close();
		
		objCtrl = eval('document.frmData.' + m_sFieldName);
		
		if (objCtrl != null)
		{
			sDay = new String(sDay);	
			if (sDay.length == 1)
				sDay = "0" + sDay;
			
			sMonth = new String(sMonth);
			
			if(sMonth.length == 1)
				sMonth = "0" + sMonth;
			
			sYear = new String(sYear);
			objCtrl.value = formatDate(sYear + sMonth + sDay);
		}
		m_sFieldName = "";
		return true;
	}		

	function formatDate(sParamDate)
	{
		var sDateSeparator;
		var iDayPos = 0, iMonthPos = 0;
		var d = new Date(1999,11,22);
		var s = d.toLocaleString();
		var sRet = "";
		var sDate = new String(sParamDate);
		
		if (sDate == "")
			return "";
		
		iDayPos = s.indexOf("22");
		iMonthPos = s.indexOf("11");
		sDateSeparator = "/";
		
		if (iDayPos<iMonthPos)
			sRet = sDate.substr(6,2) + sDateSeparator + sDate.substr(4,2) + sDateSeparator + sDate.substr(0,4);
		else
			sRet = sDate.substr(4,2) + sDateSeparator + sDate.substr(6,2) + sDateSeparator + sDate.substr(0,4);
		return sRet;
	}
			
	function trimIt(aString) 
	{
		// RETURNS INPUT ARGUMENT WITHOUT ANY LEADING OR TRAILING SPACES
		return (aString.replace(/^ +/, '')).replace(/ +$/, '');
	}
			
	function IsDate(pObj)
	{
		var ret;
		ret = true;
		
		if (trimIt(pObj.value) == '')
		{
			pObj.value = '';
			return false;
		}
		
		var month,year,day;
		var dtCh = "/";
		var pos1 = pObj.value.indexOf(dtCh);
		var pos2 = pObj.value.indexOf(dtCh,pos1 + 1);
		var strMonth = pObj.value.substring(0,pos1);
		var strDay = pObj.value.substring(pos1 + 1,pos2);
		var strYear = pObj.value.substring(pos2 + 1);
		
		if (strDay.charAt(0) == "0" && strDay.length > 1) 
			day = strDay.substring(1);
		
		if (strMonth.charAt(0) == "0" && strMonth.length > 1) 
			month = strMonth.substring(1);
		
		month = parseInt(month);
		day = parseInt(day);
		year = parseInt(strYear);
		
		var errFlag = 0;
		if (pos1 == -1 || pos2 == -1)
		{
			alert("Please enter a valid date in 'mm/dd/yyyy' format");
			pObj.value = '';
			pObj.focus();
			return false;
		}

		strMonth = trimIt(strMonth);
		strDay = trimIt(strDay);
		strYear = trimIt(strYear);
		
		if (strMonth.length == 0 || strDay.length == 0 || strYear.length == 0)
		{
			alert("Please enter a valid date in 'mm/dd/yyyy' format");
			pObj.value = '';
			pObj.focus();
			return false;
		}
		
		if (strMonth.length < 1 || month < 1 || month > 12 || isNaN(strMonth))
		{
			alert("Please enter a valid date in 'mm/dd/yyyy' format");
			pObj.value = '';
			pObj.focus();
			return false;
		}
		
		if (strDay.length < 1 || day < 1 || day > 31 || isNaN(strDay))
		{
			alert("Please enter a valid date in 'mm/dd/yyyy' format");
			pObj.value = '';
			pObj.focus();
			return false;
		}
		
		if (strYear.length != 4 || year == 0  || isNaN(strYear))
		{
			alert("Please enter a valid date in 'mm/dd/yyyy' format");
			pObj.value = '';
			pObj.focus();
			return false;
		}
		
		if (strMonth.search(" ") >= 0)
			strMonth = strMonth.replace(" ","");
		
		if (strDay.search(" ") >= 0)
			strDay = strDay.replace(" ","");
		
		pObj.value = strMonth + '/' + strDay + '/' + strYear;

		return true;
	}		
				
	function openProperty()
	{			
		if (document.frmData.lstselfields.length > 0)
		{
			window.document.frmData.hdOpenProp.value=window.document.frmData.lstselfields.selectedIndex;
			window.document.frmData.hdBValue.value = tf[window.document.frmData.lstselfields.selectedIndex];
			window.document.frmData.hdCValue.value = arrFieldType[tf[window.document.frmData.lstselfields.selectedIndex]];
			tabNavigation(0,0);
			window.open('progress.html','XprogressWnd',
					'width=400,height=150,top='+(screen.availHeight-425)/2+',left='+(screen.availWidth-650)/2+'');
			//window.setTimeout(window.open('properties.asp?b='+tf[window.document.frmData.lstselfields.selectedIndex]+'&c='+arrFieldType[tf[window.document.frmData.lstselfields.selectedIndex]],'prop','Width=650,Height=425,top='+(screen.availHeight-425)/2+',left='+(screen.availWidth-650)/2+''),100)
		}
		else
			return false;		

	}
	
	function openRelDate(pTabID)
	{
		window.open('relativedate.asp?TABID='+pTabID,'reldate','resizable=yes,Width=450,Height=200,top='+(screen.availHeight-200)/2+',left='+(screen.availWidth-450)/2+'')				
	}

	function openReport()
	{
		// Netscape hack
		var iNav = 0,iDoc = 0;
		for (var i=0;i<self.parent.frames.length;i++)
		{
			if (self.parent.frames[i].name == "nav")
				iNav = i;
			else if (self.parent.frames[i].name == "docframe")
				iDoc = i;
		}
		window.setTimeout("self.parent.frames["+iDoc+"].location.href='smopenreport.asp'",10);
	}
			
	function newReport()
	{
		// Netscape hack
		var iNav = 0,iDoc = 0;
		//Done by neelima
		//for (var i=0;i<self.parent.frames.length;i++)
		//{
		//	if (self.parent.frames[i].name == "nav")
		//		iNav = i;
		//	else if (self.parent.frames[i].name == "docframe")
		//		iDoc = i;
		//}
		window.setTimeout("window.location.href='rptfields.asp?MODE=0'",10);
	}
	
	function uploadReport()
	{
		// Netscape hack
		var iNav = 0,iDoc = 0;
		//Done by neelima
		//for (var i=0;i<self.parent.frames.length;i++)
		//{
		//	if (self.parent.frames[i].name == "nav")
		//		iNav = i;
		//	else if (self.parent.frames[i].name == "docframe")
		//		iDoc = i;
		//}
		//window.setTimeout("self.parent.frames["+iDoc+"].location.href='uploadreport.asp'",10);
		window.setTimeout("window.location.href='uploadreport.asp'",10);
	}
			
	function postReport()
	{
		// Netscape hack
		var iNav = 0,iDoc = 0;
		for (var i=0;i<self.parent.frames.length;i++)
		{
			if (self.parent.frames[i].name == "nav")
				iNav = i;
			else if (self.parent.frames[i].name == "docframe")
				iDoc = i;
		}
		window.setTimeout("self.parent.frames["+iDoc+"].location.href='postreport.asp'",10);
	}
	
	function openDerivedField1(pAllowOpen, pTabID)
	{
		if (pAllowOpen == true)
		{
			if (pTabID == 0)
				tabNavigation(0,0);
				
			window.open('derivedfields_1.asp?TABID='+pTabID,'derfld_1','resizable=yes,Width=450,Height=350,top='+(screen.availHeight-350)/2+',left='+(screen.availWidth-450)/2+'');				
		}
	}
	
	function FindCharacter(p_sString, p_cChar, p_nTH) // Starting from 1 to nth
	{
		var iStart = -1;
		var iEnd = -1;
		var iTH = 0;
		
		if (p_sString.length > 0)
		{
		    var iIndex;
		    for (iIndex=0;iIndex<p_sString.length;iIndex++)
		    {
		        if (iStart == -1)
		        {
		            if (p_sString.substr(iIndex, 1) == p_cChar)
		            {
						iStart = iIndex + 1;
						iEnd = iIndex + 1;
						iTH++;
					}
				}
		        else
		        {
					if (p_nTH > iTH)
					{
						if (p_sString.substr(iIndex, 1) == p_cChar)
						{
							iEnd = iIndex + 1;
							iTH++;
						}
					}
					else
						break;
				}
		    }
		}
		return (iStart + ',' + iEnd);
	}
	
	function adjust_pArray(pArray)
	{		
		var n = 0;
		var adjust = new Array;
		for (m=0;m<pArray.length;m++)
		{
			if (pArray[m] == null)
				continue;
			else
			{
				adjust[n] = pArray[m];
				n++;
			}
		}
				
		for (i=0;i<adjust.length;i++)	
			pArray[i] = adjust[i];
		
		pArray.length = pArray.length - 1;
		return true;
	}
	
	function adjust_tf()
	{
		for (i=0;i<p.length;i++)
		{
			if (p[i] == null)
			{
				k = i;
				break;						
			}
		}
		
		for (j=0;j<p.length;j++)  //This for loop is to create an array with null indices marked
		{
			if (tf[j] < k)
				tf[j] = tf[j];
			else if (tf[j] > k) 
				tf[j] = tf[j] - 1;
			else if (tf[j] == null)
				tf[j] = null;
			else
				tf[j] = null;
		}
		
		var n = 0;
		var t = new Array;
		for (m=0;m<tf.length;m++)//This loop creates the contracted version of above array with nulls removed
		{					
			if (tf[m] != null)
			{
				t[n] = tf[m];
				n++;
			}					
		}
		
		for (i=0;i<t.length;i++)	
			tf[i] = t[i];

		tf.length = tf.length - 1;
		return true;		
	}	
	
	function adjust_pFields()
	{	
		var bFound;
		var m = 0, n = 0;
				
		for (m=0;m<pFields.length;m++)
		{
			bFound = 0;
			for (n=0;n<p.length;n++)
			{
				if (pFields[m] != null)
				{
					if (pFields[m] == p[n].substring(0,pFields[m].length))
					{
						bFound = 1;
						break;
					}
				}
			}
			
			if (bFound == 0)
			{
				pFields[m] = null;
				n = 0;
				var adjust = new Array;
			
				for (m=0;m<pFields.length;m++)//This loop creates the contracted version of above array with nulls removed
				{
					if (pFields[m] == null)
						continue;
					else
					{
						adjust[n] = pFields[m];
						n++;
					}
				}

				for (i=0;i<adjust.length;i++)	
					pFields[i] = adjust[i];
				
				pFields.length = pFields.length - 1;
				break;
			}
		}
		return true;
	}		

	function fixDoubleQuote(p_String)
	{
	    //return escape(p_String)
	    return p_String.replace('"','&quot;');
	}
	
	function onCodeClose()
	{
		m_codeWindow=null;
		return true;
	}	

	function dateLostFocus(sCtrlName)
	{
		var sDateSeparator;
		var iDayPos=0, iMonthPos=0;
		var d=new Date(1999,11,22);
		var s=d.toLocaleString();
		var sRet="";
		var objFormElem=eval('document.frmData.'+sCtrlName);
		var sDate=new String(objFormElem.value);
		var iMonth=0, iDay=0, iYear=0;
		var monthDays = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
		if(sDate=="")
			return "";
		iDayPos=s.indexOf("22");
		iMonthPos=s.indexOf("11");
		//if(IE4)
		//	sDateSeparator=s.charAt(iDayPos+2);
		//else
			sDateSeparator="/";
		var sArr=sDate.split(sDateSeparator);
		if(sArr.length==3)
		{
			//-- Rajeev -- 02/20/2004 -- Fix to check for digits
			if(isNaN(sArr[0]/2) || isNaN(sArr[1]/2) || isNaN(sArr[2]/2))
				{
					objFormElem.value="";
					return true;
				}
				
			sArr[0]=new String(parseInt(sArr[0],10));
			sArr[1]=new String(parseInt(sArr[1],10));
			sArr[2]=new String(parseInt(sArr[2],10));


			// Classic leap year calculation
			if (((parseInt(sArr[2],10) % 4 == 0) && (parseInt(sArr[2],10) % 100 != 0)) || (parseInt(sArr[2],10) % 400 == 0))
				monthDays[1] = 29;
			if(iDayPos<iMonthPos)
			{
				// Date should be as dd/mm/yyyy
				if(parseInt(sArr[1],10)<1 || parseInt(sArr[1],10)>12 ||  parseInt(sArr[0],10)<0 || parseInt(sArr[0],10)>monthDays[parseInt(sArr[1],10)-1])
					objFormElem.value="";
			}
			else
			{
				// Date is something like mm/dd/yyyy
				if(parseInt(sArr[0],10)<1 || parseInt(sArr[0],10)>12 ||  parseInt(sArr[1],10)<0 || parseInt(sArr[1],10)>monthDays[parseInt(sArr[0],10)-1])
					objFormElem.value="";
			}

			// Check the year
			if(parseInt(sArr[2],10)<10 || (sArr[2].length!=4 && sArr[2].length!=2))
				objFormElem.value="";
			// If date has been accepted
			if(objFormElem.value!="")
			{
				// Format the date
				if(sArr[0].length==1)
					sArr[0]="0" + sArr[0];
				if(sArr[1].length==1)
					sArr[1]="0" + sArr[1];
				if(sArr[2].length==2)
					sArr[2]="19"+sArr[2];
				if(iDayPos<iMonthPos)
					objFormElem.value=formatDate(sArr[2] + sArr[1] + sArr[0]);
				else
					objFormElem.value=formatDate(sArr[2] + sArr[0] + sArr[1]);
			}
		}
		else
			objFormElem.value="";
		return true;
	}

	function Navigate()
	{
		//document.frmData.sysCmd.value=direction;
		//if(direction=='')  //tkr need single quotes for sysCmdQueue value
		//	direction="''";
		if(OnFormSubmit())
			self.setTimeout('document.frmData.submit();',100);

		return true;
	}

	function OnFormSubmit()
	{
		var ret=true;
		
		//if(!m_DataChanged && document.frmData.sysCmd.value=='5')
		//	return false;
			
		//if(document.frmData.sysCmd.value=='5')
		//	ret=validateForm();
		//else if(m_DataChanged)
		//	{
		//	if(ConfirmSave())
		//	 {
		//		if(!validateForm())
		//			return false;
		//		else
		//			{
		//				if(sCmdQueue==null)
		//					sCmdQueue='';
		//				document.frmData.sysCmdConfirmSave.value=1;
		//				document.frmData.sysCmdQueue.value=sCmdQueue;
		//				self.setTimeout('document.frmData.submit();',200);
		//				return false;
		//			}
		//	}	

				var wnd=window.open('progress.html','progressWnd',
					'width=400,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-400)/2);
				self.parent.wndProgress=wnd;
				
				return true;
		//	}
		
		self.onerror=scripterror;
		if(ret && self.parent!=null && self.parent!=self && !OPERA)
		{
			var wnd=window.open('progress.html','progressWnd',
				'width=400,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-400)/2);
			self.parent.wndProgress=wnd;
		}
		self.onerror=null;
		
		return ret;
	}

	function openPreview(pOutputType)
	{
		window.document.frmData.hdPreview.value = "1";
		window.document.frmData.hdOutputType.value = pOutputType
		
		if (window.document.frmData.hdSaveFinal.value == "1")
		{
			var wnd = window.open('smwdPreviewStatus.asp','wndProgress','resizable=yes,width=400,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-400)/2);
		}
	}
	
	function CloneReport(pTabID,pPost)
	{
		var sReports = getSelReports();
		var iIndex;
		
		if(sReports == "")
		{
			alert("Please select a report you would like to clone.");
			return true;
		}
		
		iIndex = sReports.indexOf(',');
		if (iIndex != '-1')
		{
			alert("Please clone only one report at a time.");
			return true;
		}
		
		document.frmData.hdReports.value = sReports;
		window.open('savereport.asp?TABID='+pTabID+'&POST='+pPost, 'saverep', 'resizable=yes,Width=460,Height=210,top=' + (screen.availHeight-210)/2 + ',left=' + (screen.availWidth-460)/2 + '');
	}
	
	function DeleteReport()
	{
		var sReports = getSelReports();
		
		if(sReports == "")
		{
			alert("Please select report/s you would like to delete.");
			return true;
		}
		
		if(!self.confirm("Are you sure you want to delete the selected report/s?"))
			return false;
		
		document.frmData.hdDelete.value = 1;
		document.frmData.hdReports.value = sReports;
		document.frmData.submit();
	}
	
	function getSelReports()
	{
		var sJobs = new String();
		for(var i=0;i<document.frmData.length;i++)
		{
			var objElem = document.frmData.elements[i];
			var sName = new String(objElem.name);
			if(sName.substring(0,9) == "chkReport" && objElem.checked)
			{
				if(sJobs != "")
					sJobs = sJobs + ",";
				sJobs = sJobs + sName.substring(9,sName.length);
			}
		}
		return sJobs;
	}