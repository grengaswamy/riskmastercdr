<!-- #include file ="session.inc" -->
<!-- #include file ="generic.asp" -->
<%
'Public objSession
'initSession objSession
%>
<html>
	<head>
		<link rel="stylesheet" href="RMNet.css" type="text/css"/>
		<title>SMD - [Derived Fields]</title>
		<script language="JavaScript" SRC="SMD.js"></script>
		<script>
			var arrDerFieldName = new Array;
			arrDerFieldName.length = 0;
			var UpdateMode;
			UpdateMode = '';
			function dialogModal()
			{
				var Nav4 = ((navigator.appName == "Netscape") && (parseInt(navigator.appVersion) >= 4));
				
				if (Nav4)
					window.focus();
				
				return true;
			}	
			
			function changeTextValue()
			{
				var sValue = '';
				sValue = document.frmData.txtDerivedField.value;
				if(arrDerFieldName.length == 0)
				{
					if (document.frmData.txtDerivedField.value.length == 0)
					{
						document.frmData.btnAdd.disabled = true;
						document.frmData.btnDelete.disabled = true;
						document.frmData.btnDone.disabled = false;
						document.frmData.btnEdit.disabled = true;
						document.frmData.btnGlobal.disabled = false;
					}
					else
					{
						document.frmData.btnAdd.disabled = false;
						document.frmData.btnDelete.disabled = true;
						document.frmData.btnDone.disabled = false;
						document.frmData.btnEdit.disabled = true;
						document.frmData.btnGlobal.disabled = false;
					}				
				}
				else
				{
					for(i=0;i<arrDerFieldName.length;i++)
					{
						if (trimIt(sValue) == trimIt(arrDerFieldName[i]))
						{
							document.frmData.btnAdd.disabled = true;
							document.frmData.btnDelete.disabled = false;
							document.frmData.btnDone.disabled = false;
							document.frmData.btnEdit.disabled = false;
							document.frmData.btnGlobal.disabled = false;
							break;
						}
						else
						{
							document.frmData.btnAdd.disabled = false;
							document.frmData.btnDelete.disabled = true;
							document.frmData.btnDone.disabled = false;
							document.frmData.btnEdit.disabled = true;
							document.frmData.btnGlobal.disabled = false;
						}
						
						if (document.frmData.txtDerivedField.value.length == 0)
						{
							document.frmData.btnAdd.disabled = true;
							document.frmData.btnDelete.disabled = true;
							document.frmData.btnDone.disabled = false;
							document.frmData.btnEdit.disabled = true;
							document.frmData.btnGlobal.disabled = false;
						}
						else
						{
							document.frmData.btnAdd.disabled = false;
							document.frmData.btnDelete.disabled = true;
							document.frmData.btnDone.disabled = false;
							document.frmData.btnEdit.disabled = true;
							document.frmData.btnGlobal.disabled = false;
						}
					}
		
				}
			}
			
			function window_onLoad()
			{
//-- Rajeev -- 03/09/2004			
//				var arrTemp = document.frmData.hdDerivedFieldName.value.split(",");
				var arrTemp = document.frmData.hdDerivedFieldName.value.split("*!^");
				for(i=0,n=0;i<arrTemp.length;i++)
				{
					if (arrTemp[i].indexOf("Derived") > -1)  
					{
						arrDerFieldName[n] = arrTemp[i].substring(arrTemp[i].indexOf(":")+1,arrTemp[i].length);
						add_item(trimIt(arrDerFieldName[n]));
						n = n + 1;
					} 
				}
				
				if (document.frmData.lstDerivedFields.length > 0)
					document.frmData.txtDerivedField.value = trimIt(document.frmData.lstDerivedFields.options[0].text);
				
				if (document.frmData.txtDerivedField.value != '')
				{
					document.frmData.btnAdd.disabled = true;
					document.frmData.btnDelete.disabled = false;
					document.frmData.btnDone.disabled = false;
					document.frmData.btnEdit.disabled = false;
					document.frmData.btnGlobal.disabled = false;
				}
				
				if (document.frmData.lstDerivedFields.length > 0)
				{
					document.frmData.lstDerivedFields.selectedIndex = 0;
					document.frmData.txtDerivedField.value = trimIt(arrDerFieldName[0]);
				}
				
			}
			
			function add_item(txt)
			{
				var sNewItem;
				sNewItem = new Option(txt);
				if (navigator.appName == "Netscape")
					window.document.frmData.lstDerivedFields.add(sNewItem, null);
				else
					window.document.frmData.lstDerivedFields.add(sNewItem);
			}
				
			function changeFieldSelection()
			{
				document.frmData.txtDerivedField.value = trimIt(arrDerFieldName[document.frmData.lstDerivedFields.selectedIndex]);
			}
			
			function add_click()
			{
				UpdateMode = "Add";
				window.opener.openDerivedField2(document.frmData.txtDerivedField.value,UpdateMode,window.document.frmData.hdTabID.value);
				window.close(); 
			}
		
			function edit_click()
			{
				UpdateMode = "Edit";
				window.opener.editDerivedField2(document.frmData.txtDerivedField.value,UpdateMode,window.document.frmData.hdTabID.value);
				window.close(); 
			}
			
			function global_click()
			{
				window.opener.openGScript(window.document.frmData.hdTabID.value);
				window.close(); 
			}

			function delete_click()
			{

//-- Rajeev -- 03/09/2004			
//				var arrTemp = document.frmData.hdDerivedFieldName.value.split(",");
				var arrTemp = document.frmData.hdDerivedFieldName.value.split("*!^");
				
				var index = '';
				for(i=0;i<arrTemp.length;i++)
				{
					if (trimIt(document.frmData.txtDerivedField.value) == trimIt(arrTemp[i].substring(arrTemp[i].indexOf(":")+1,arrTemp[i].length)))  
					{
						index = i;
						break;
					} 
				}
				UpdateMode = "Delete";
				window.opener.window.document.frmData.hdDerAddEdit.value = UpdateMode;
				window.opener.window.document.frmData.hdDerIndex.value = index;
				window.opener.tabNavigation(window.document.frmData.hdTabID.value,window.document.frmData.hdTabID.value);
				window.close(); 
			}			
			
			function done_click()
			{
				window.opener.window.document.frmData.hdDerGScript.value = window.document.frmData.hdDerGScript.value;
				window.opener.window.document.frmData.hdSubmit.value = "DERIVED";
				window.opener.tabNavigation(window.document.frmData.hdTabID.value,window.document.frmData.hdTabID.value);
				window.close(); 
			}
			
		</script>		
	</head>

	<body onLoad="window_onLoad()">
	<form name="frmData" topmargin="0" leftmargin="0" onSubmit="return false;">
		<table class="formsubtitle" border="0" width="98%" align="center">
			<tr>
				<td class="ctrlgroup">Derived Fields :</td>
			</tr>
		</table>
		<br>
		<table border="0" width="100%" align="center">
			<tr>
				<td align="right" width="40%"><b>Derived Field :</b></td>
				<td nowrap><input name="txtDerivedField" size="25" onKeyUp="changeTextValue();" ></td>
			</tr>
											
			<tr>
				<td align="center" colspan="2">
					<table>
						<tr>
							<td><input type="button" name="btnAdd"  disabled value="  Add   "   class="button" onClick="add_click()"></td>
							<td><input type="button" name="btnEdit"  disabled value=" Edit   "  class="button" onClick="edit_click()"></td>
							<td><input type="button" name="btnDelete"  disabled value="Delete" class="button" onClick="delete_click()"></td>
							<td><input type="button" name="btnGlobal"  value="Global" class="button" onClick="global_click()"></td>
							<td><input type="button" name="btnDone"  value=" Done " class="button" onClick="done_click()"></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td align="center" colspan="2">
					<select name="lstDerivedFields" size="12" style="width:98%" onChange="changeFieldSelection();" ondblClick="edit_click()">
					</select>
				</td>
			</tr>
		</table>
		<input type="hidden" name="hdDerivedFieldName" value="<%=objSessionStr("ArrayP")%>">
		<input type="hidden" name="hdTabID" value="<%=Request.QueryString("TABID")%>">
		<input type="hidden" name="hdDerGScript" value="">
	</form>
	</body>
</html>