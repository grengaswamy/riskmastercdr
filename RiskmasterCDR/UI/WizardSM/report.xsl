<?xml version='1.0'?>
<!--<xsl:stylesheet xmlns:xsl="http://www.w3.org/TR/WD-xsl">-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">


<xsl:template match="/">

<html><head><title></title>
	<link rel="stylesheet" href="xml/css/RMNet.css" type="text/css"/>
	<script language="JavaScript" SRC="smi.js"></script>
		
</head>
	 <body class="10pt">
	 <xsl:attribute name="bgcolor">
	 <xsl:value-of select="form/@bcolor"/>
	 </xsl:attribute>
	<!-- <xsl:if test="form/body1/@req_func='yes'">
	<xsl:attribute name="onload">javascript:<xsl:value-of select="form/body1/@func"/></xsl:attribute>
	</xsl:if>-->
		<form name="frmcodelist" method="post">
		<xsl:attribute name="action">
			<xsl:value-of select="form/@actionname"/>
		</xsl:attribute>
		
	<div class="formtitle" id="formtitle" align="center"><xsl:value-of select="form/@title"/></div>
			
		<table align="center">
				
		<tr><td>
	</td></tr>
		<tr>
		<td align="center">
		<xsl:for-each select="form/group">
			<xsl:apply-templates select="control"/>
		</xsl:for-each>
		<input type="hidden" name="textboxname" value=""/>
		
			</td>
		</tr>
		<tr>
		<td>
		
		<xsl:for-each select="form/button">
			<input type="button" class="button"><xsl:attribute name="value"><xsl:text>     </xsl:text><xsl:value-of select="@title"/><xsl:text>   </xsl:text></xsl:attribute><xsl:if test="@disable[.='1']"><xsl:attribute name="disabled">disabled</xsl:attribute></xsl:if>
		<xsl:attribute name="onClick">collect_codes('<xsl:value-of select="@action"/>');</xsl:attribute>
		</input>
		</xsl:for-each>
		</td>
		</tr>
		</table>
		</form>
		
	</body>
	
</html>
</xsl:template>
<xsl:template match="control">
	<xsl:choose>
	<xsl:when test="@type[.='listbox']">
			<select size="10" multiple="multiple">
			<xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
				
				<option value="0">nothing</option>
				<xsl:call-template  name="combo_option">
				<xsl:with-param name="opt_name">
				<xsl:value-of select="@optionname"/>
				</xsl:with-param>
				</xsl:call-template>
			</select>
	</xsl:when>
	</xsl:choose>
</xsl:template>
	
	<xsl:template name="combo_option">
	<xsl:param name="opt_name"/>
	<xsl:choose>
		<xsl:when  test="@optionname[.='option']">
			<xsl:for-each select="option">
					<option>
						<xsl:attribute name="value">
							<xsl:value-of select="@code"/><xsl:text>|</xsl:text><xsl:value-of select="@Description"/>
						</xsl:attribute>
						<xsl:value-of select="@code"/><xsl:text>   -   </xsl:text><xsl:value-of select="@Description"/>
					</option>
			</xsl:for-each>
		</xsl:when>
	</xsl:choose>
	</xsl:template>

</xsl:stylesheet>