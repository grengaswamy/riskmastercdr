<%	'-----------------------------------------------------
	'Author	:	Rajeev Chauhan, CSC India
	'Dated	:	Aug. 29, 2003
	'-----------------------------------------------------

%>

<%
Public objSession, sError

'-- Initialize custom session
InitSession objSession
%>

<%
'-- Posts report and returns latest report id from SM_REPORTS table
Public Function PostReport(ByRef pReportID, ByRef pReportName)
	Dim objUtil, lReportID, sReportName, pageError
	PostReport = false
	Set objUtil = Nothing

	On Error Resume Next
	Set objUtil = Server.CreateObject("InitSMList.CUtility")
	On Error Goto 0

	If objUtil Is Nothing Then
		Call ShowError(pageError, "Call to CUtility class failed.", True)
	End If

	'-- SMWD Component call : records replicated from SMWD tables to SM tables
	If Not objUtil.PostReports(Application("SM_DSN"), objSessionStr("REPORTID"), objSessionStr("UserID")) Then
		Response.Write "Error in posting: Can't test run...."
		Response.End
	Else
		objUtil.GetLastPostedReport  Application("SM_DSN"), objSessionStr("UserID"), lReportID, sReportName
		pReportID = lReportID
		pReportName = sReportName
	End If
	
	Set objUtil = Nothing
	PostReport = True
	
End Function

'-- Used in HTML inputs to fix double quote entry
Public Function fixDoubleQuote(p_String)
    fixDoubleQuote = Replace(p_String, """", "&quot;")
End Function

Public Function tokenSingleQuote(p_String)
    tokenSingleQuote = Replace(p_String, "'", "~^~^~")
End Function

'-- Tabbing redirection
Function ChangeTab(pTabID, pError)
	Dim link
	link = ""
	Select Case pTabID
		Case "0"
			link="rptfields.asp"
			if Request.Form("hdBValue")<> "" then
				Response.Redirect link & "?ERROR=" & pError & "&b=" + Request.Form("hdBValue") & "&c=" & Request.Form("hdCValue")
			end if
		Case "1"
			link="rptcriteria.asp"
		Case "2"
			link="rptoptions.asp"
		Case "3"
			link="graphoptions.asp"
		Case "4"
			link="colhierarchy.asp"
	End Select
	If link <> "" Then Response.Redirect link & "?ERROR=" & pError
End Function

'-- If page has been submitted, call this function
'-- This function is dependent on local functions in the caller page
Function SubmitReport()

	Call RefreshSessionVariables()
	
	Call SaveReport()
	
	If Request.Form("hdPost") = "1" Then Response.Redirect "PostReport.asp?REPORTID=" & objSessionStr("REPORTID") & "&TABID=" & Request.Form("hdTabID")
	
	Dim lReportID, sReportName, sOutputType, sTabId
	
	sTabId = Request.Form("hdTabID") 
	sOutputType = Request.Form("hdOutputType") 
	
	If Request.Form("hdPost") = "2" Then
		If Request.Form("hdTabID") = "0" Then Call SaveReport()
		PostReport lReportID, sReportName
		Response.Redirect "smwdi.asp?REPORTID=" & lReportID & "&REPORTNAME=" & sReportName & "&OUTPUTTYPE=" & sOutputType & "&TABID=" & sTabId
	End If

	Call ChangeTab(Request.Form("hdTabID"), sError) '-- sError should be defined public to the page

End Function

Public Function ShowError(ByRef sPageError, Byval sErrMsg, Byval bBlockPage)
	If sErrMsg <> "" Then
		If sPageError <> "" Then
			Response.Write "<Ul><Li class=""errtext"">" & sErrMsg & "</Li></Ul>"
		Else
			sPageError = "Following errors were reported:"
			Response.Write "<div class=""errtextheader"">" & sPageError & "</div><Ul><Li class=""errtext"">" & sErrMsg & "</Li></Ul>"
		End If
	End If
	If bBlockPage Then Response.End
End Function

Public Function IsValidArray(ByRef arr)
	IsValidArray = False
	Dim m
	Err.Clear()
	On Error Resume Next
	m = -1
	m = UBound(arr)
	On Error GoTo 0
	If Err.number <> 9 And m <> "" And m >=0 Then IsValidArray = True
End Function

Public Function GetValidObject(ByVal ProgID, ByRef obj)
	GetValidObject = False
	Set obj = Nothing
	On Error Resume Next
	Set obj = Server.CreateObject(ProgID)
	On Error Goto 0
	If Not obj Is Nothing Then GetValidObject = True
End Function

'-- This function can be appended in case more clean-ups are needed.
'-- variable pageError should be declared in the calling page.
Public Function CleanUp()

	'-- Delete the temporary report
	If 	Len(objSessionStr("REPORTID")) = 32 Then
		Dim objDataManager
		If Not GetValidObject("InitSMList.CDataManager", objDataManager) Then
			Call ShowError(pageError, "Call to CDataManager class failed.", True)
		End If
		objDataManager.m_RMConnectStr = Application("SM_DSN")
		objDataManager.TempReportID = objSessionStr("REPORTID")
		objDataManager.DeleteTempReport
		Set objDataManager = Nothing
	End If 
	
End Function
%>
