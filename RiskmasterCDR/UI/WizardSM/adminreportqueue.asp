<!-- #include file ="sessionInitialize.asp" -->
<%Public objSession
initSession objSession
%> 
<%
Sub LoadUsers(objDict)
	dim r, h_env, db, rs, sSQL
	dim sLoginName, sLastName, sFirstName

	Set r = CreateObject("DTGRocket")	
	h_env = r.DB_InitEnvironment()
	db = r.DB_OpenDatabase(h_env, Application(APP_SECURITYDSN), 0)
	
	objDict.RemoveAll
	
	' Query security db for users
	sSQL = "SELECT DISTINCT USER_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME,USER_DETAILS_TABLE.LOGIN_NAME"
	sSQL = sSQL & " FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID"
	sSQL = sSQL & " ORDER BY USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME"
	rs = r.DB_CreateRecordset(db, sSQL, 3, 0)
	Do While not r.DB_EOF(rs)
		r.DB_GetData rs, "USER_ID", lUserID
		r.DB_GetData rs, "LAST_NAME", sLastName
		r.DB_GetData rs, "FIRST_NAME", sFirstName
		r.DB_GetData rs, "LOGIN_NAME", sLoginName
		On Error Resume Next
		objDict.Add lUserId, sLoginName & " (" & Trim(sFirstName & " " & sLastName) & ")"
		On Error Goto 0	
		r.DB_MoveNext rs
	Loop
	
	r.DB_CloseRecordset CInt(rs), 2	
	' Shut down database connection
	r.DB_CloseDatabase CInt(db)
	r.DB_FreeEnvironment CLng(h_env)
	set r = Nothing
End Sub

function OutputDateTime(sDTTM)
dim sDate, sTime

if len(sDTTM) <> 14 then
	OutputDateTime = ""
	exit function
end if

sDate = FormatDateTime(DateSerial(left(sDTTM, 4), mid(sDTTM, 5, 2), mid(sDTTM, 7, 2)), vbShortDate)
sTime = FormatDateTime(TimeSerial(mid(sDTTM, 9, 2), mid(sDTTM, 11, 2), right(sDTTM, 2)), vbLongTime)

OutputDateTime = sDate & " " & sTime
end function
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<META HTTP-EQUIV="Refresh" CONTENT="300">
<html><head><title>RISKMASTER's System Administrator Report Queue</title>
<link rel="stylesheet" href="RMNet.css" type="text/css" />
<script language="JavaScript" SRC="adminreportqueue.js"></script>
<script language="JavaScript">
var ns, ie, ieversion;
var browserName = navigator.appName;                   // detect browser 
var browserVersion = navigator.appVersion;
if (browserName == "Netscape")
{
	ie=0;
	ns=1;
}
else		//Assume IE
{
	ieversion=browserVersion.substr(browserVersion.search("MSIE ")+5,1);
	ie=1;
	ns=0;
}

function viewFile(vURL)
{
	setTimeout('document.location = "' + vURL + '"', 100);
	return true;
}

function onPageLoaded()
{
	if (ie)
	{
		if ((eval("document.all.divForms")!=null) && (ieversion>=6))
			divForms.style.height=window.frames.frameElement.Height*0.77;
	}
	else
	{
		var o_divforms;
		o_divforms=document.getElementById("divForms");
		if (o_divforms!=null)
		{
			o_divforms.style.height=window.frames.innerHeight*0.58;
			o_divforms.style.width=window.frames.innerWidth*0.995;
		}
	}
}
</script>
</head>
<body onload="onPageLoaded()">

<form name="frmData">
<input type="hidden" name="jobs" value="" />
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr><td class="msgheader" colspan="8">Reports Administration</td></tr>
<tr><td class="ctrlgroup2" colspan="8">&nbsp;Report Jobs All Users</td></tr>
</table>
<div id="divForms" class="divScroll">
<table width="100%" height="150" border="0" cellspacing="0" cellpadding="1">
<tr>
<td class="colheader3" width="15%">Job Name</td>
<td class="colheader3" width="15%">Job Description</td>
<td class="colheader3" width="15%">Start Date/Time</td>
<td class="colheader3" width="15%">Status</td>
<td class="colheader3" width="15%">User</td>
<td class="colheader3" width="15%">Type</td>
<td class="colheader3" width="15%">Path</td>
</tr>

<%
dim r
dim sSQL
dim h_env
dim db, db2
dim rs, rs2
dim vName, vDesc, vStartDTTM, bComplete, vCompleteDTTM, bAssigned, vAssignedDTTM, lJobID, vOutputPathURL, vMsg, bErrorFlag, luserId
Dim sOutputType, sOutputPath
dim b
dim i, pos
dim pos1, pos2, pos3 'vsoni5 : 02/15/2008 : MITS 19763
Dim objDict
Dim objFSO, bExist

Set objDict = CreateObject("Scripting.Dictionary")
LoadUsers objDict

Set objFSO=CreateObject("Scripting.FileSystemObject")

i = 0

sSQL = "SELECT JOB_ID,JOB_NAME,JOB_DESC,OUTPUT_PATH_URL,START_DTTM,ASSIGNED,ASSIGNED_DTTM,COMPLETE,COMPLETE_DTTM,ERROR_FLAG,USER_ID, OUTPUT_TYPE,OUTPUT_PATH FROM SM_JOBS WHERE "
sSQL = sSQL & "(ARCHIVED = 0 OR ARCHIVED IS NULL)"
sSQL = sSQL & " ORDER BY JOB_ID DESC"
' sSQL = sSQL & " ORDER BY ASSIGNED DESC, COMPLETE DESC,COMPLETE_DTTM DESC"

set r = CreateObject("DTGRocket")

h_env = r.DB_InitEnvironment()

db = r.DB_OpenDatabase(h_env, Application("SM_DSN"), 0)
db2 = r.DB_OpenDatabase(h_env, Application("SM_DSN"), 0)

rs = r.DB_CreateRecordset(db, sSQL, 3, 0)
while not r.DB_EOF(rs)
	r.DB_GetData rs, "JOB_ID", lJobID
	r.DB_GetData rs, "JOB_NAME", vName
	r.DB_GetData rs, "JOB_DESC", vDesc
	r.DB_GetData rs, "OUTPUT_PATH_URL", vOutputPathURL
	r.DB_GetData rs, "START_DTTM", vStartDTTM
	r.DB_GetData rs, "ASSIGNED", bAssigned
	r.DB_GetData rs, "ASSIGNED_DTTM", vAssignedDTTM
	r.DB_GetData rs, "COMPLETE", bComplete
	r.DB_GetData rs, "COMPLETE_DTTM", vCompleteDTTM
	r.DB_GetData rs, "ERROR_FLAG", bErrorFlag
	r.DB_GetData rs, "USER_ID", lUserId
	r.DB_GetData rs, "OUTPUT_TYPE", sOutputType
	r.DB_GetData rs, "OUTPUT_PATH", sOutputPath
	
	'vsoni5 : 02/15/2008 : MITS 19763
	'Remove Database path and credentials from Output Path(sOutputPath)	
	'------------------------------------------------------------------
	pos1 = InStr(1,sOutputPath,"UID=")
	If pos1<> 0 Then		
		pos2 = InStrRev(sOutputPath, ";")
		If pos2 <> 0 Then			
			sOutputPath = Right(sOutputPath, Len(sOutputPath) - pos2)
		End If		
		' remove "\" from file name
		pos3 = InStr(sOutputPath,"\")
		If pos3 <> 0 Then
			sOutputPath = Right(sOutputPath, Len(sOutputPath) - pos3)
		End If
	End If	
	'------------------------------------------------------------------
				
	If bComplete And vOutputPathURL<>"" Then
		' Adjust for right server name etc.
		pos=InstrRev(vOutputPathURL,"/")
		vOutputPathURL=Mid(vOutputPathURL,pos+1)
		'If pos>0 And False Then
		'	Dim s
		'	s=Request.ServerVariables("SCRIPT_NAME")
		'	If InstrRev(s,"/")>0 Then s=Left(s,InstrRev(s,"/")-1)
		'	s=Request.ServerVariables("SERVER_NAME") & s & Mid(vOutputPathURL,pos)
		'	If UCase(Request.ServerVariables("HTTPS"))="ON" Then
		'		s="https://" & s
		'	Else
		'		s="http://" & s
		'	End if
		'	vOutputPathURL=s
		'End If
	End If
%>

<% if i mod 2 = 0 then %>
<tr class="rowlight1">
<% else %>
<tr class="rowdark1">
<% end if %>

<td width="15%">
<input name="seljob<% = lJobID %>" type="checkbox"></input>&nbsp;

<% if bComplete then %>
<a class="td8" href="<%= vOutputPathURL %>"><% = vName %></a>
<% else %>
<% = vName %>
<% end if %>
</td>

<td class="td8" width="15%">
<% = vDesc %>
</td>

<td class="td8" width="15%">
<% if vStartDTTM & "" <> "" then %>
	Run @ <% = OutputDateTime(vStartDTTM) %>
<% else %>
	Run Immediately
<% end if %>
</td>

<td width="15%">
<% if Not bAssigned then %>
<font color="6e6e6e"><b>Waiting</b></font>
<% elseif Not bComplete then %>
<font color="blue"><b>
Running</b> (Started @ <% = OutputDateTime(vAssignedDTTM) %>)
<br />
<% 
	rs2 = r.DB_CreateRecordset(db2, "SELECT MSG FROM SM_JOB_LOG WHERE JOB_ID = " & lJobID & " AND MSG_TYPE = 'progress' ORDER BY LOG_ID DESC", 3, 0)
	if Not r.DB_EOF(rs2) then
		r.DB_GetData rs2, 1, vMsg
		vMsg = vMsg & ""
		vMsg = Replace(vMsg, vbCrLf, "<br />")
		vMsg = Replace(vMsg, vbCr, "<br />")
		vMsg = Replace(vMsg, vbLf, "<br />")
		Response.Write vMsg
	end if
	r.DB_CloseRecordset CInt(rs2), 2

%>
</font>
<% else ' complete %>
	<% if bErrorFlag = -1 then %>
		<font color="red"><b>
		Error Occurred</b>
<%
		rs2 = r.DB_CreateRecordset(db2, "SELECT MSG FROM SM_JOB_LOG WHERE JOB_ID = " & lJobID & " AND MSG_TYPE = 'error' ORDER BY LOG_ID DESC", 3, 0)
		if Not r.DB_EOF(rs2) then
			r.DB_GetData rs2, 1, vMsg
			vMsg = vMsg & ""
			vMsg = Replace(vMsg, vbCrLf, "<br />")
			vMsg = Replace(vMsg, vbCr, "<br />")
			vMsg = Replace(vMsg, vbLf, "<br />")
			Response.Write vMsg
		end if
		r.DB_CloseRecordset CInt(rs2), 2
		Response.Write "<br />" & vMsg
%>
		</font>
		</td>
	<% else %>
		<font color="green"><b>
		Complete</b> (Started @ <% = OutputDateTime(vAssignedDTTM) %> and Completed @ <% = OutputDateTime(vCompleteDTTM) %>)
		</font>
		</td>
	<% end if %>
<% end if %>
<td width="15%"><%
On Error Resume Next
Response.Write objDict.Item(lUserId)
On Error Goto 0
%></td>
<td width="15%"><%=sOutputType%></td>
<td width="15%"><%
		If sOutputPath<>"" Then
			bExist=False
			On Error Resume Next
			bExist=objFSO.FileExists(sOutputPath)
			On Error Goto 0
			If bExist Then
				Response.Write sOutputPath
			Else
				Response.Write "<font color=""red"">" & sOutputPath & "</font>"
			End If
		End if
	  %></td>
</tr>

<%
	i = i + 1
	r.DB_MoveNext rs
wend
r.DB_CloseRecordset CInt(rs), 2
r.DB_CloseDatabase CInt(db)
r.DB_CloseDatabase CInt(db2)

r.DB_FreeEnvironment CLng(h_env)

set r = Nothing
%>


</table>
</div>
</form>
<table border="0"><form name="frmCmd">
<tr>
<td><input type="button" class="button" name="cmdAll" value="Select All" onclick="for(var i = 0; i<document.frmData.elements.length;i++) if(document.frmData.elements[i].type=='checkbox') document.frmData.elements[i].checked = true;"/></td>
<td><input type="button" class="button" name="cmdDelete" value="  Delete  " onClick="DeleteJob()" /></td>
<td><input type="button" class="button" name="cmdRefresh" value="  Refresh  " onClick="window.location='adminreportqueue.asp'" /></td>
</tr></form>
</table>
</body>
</html>
