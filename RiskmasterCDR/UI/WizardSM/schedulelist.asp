<!-- #include file ="SessionInitialize.asp" -->
<%
' Author: Denis Basaric
Public objSession
initSession objSession
ExpirePage
Dim lScheduleType
Public objRocket, hEnv, hDB, hRS
Dim sSQL, v, lScheduleId, lReportId, sClass
Dim lCount, sAction
Dim sOrderBy, bChecked
Dim vArrScheduleIds
Function IsChecked(lScheduleId)
	Dim Count 'As long
	IsChecked=false
	'Build Selected Schedule Id Array if necessary.
	If Not IsArray(vArrScheduleIds) Then vArrScheduleIds = Split(Replace(Request("scheduleids")," ",""),",")
	'If no results then nothing is selected and we can say "not checked"
	If Not IsArray(vArrScheduleIds) Then Exit Function
	For Count = LBound(vArrScheduleIds) to Ubound(vArrScheduleIds)
		If lScheduleId =  Clng("0" & vArrScheduleIds(Count)) Then IsChecked=true : Exit Function
	Next	
	
End Function 

lCount=0
sAction=""

Set objRocket=Nothing

Sub OpenDatabase()
	If objRocket Is Nothing Then
		Set objRocket = CreateObject("DTGRocket")
		hEnv = objRocket.DB_InitEnvironment()
		hDB = objRocket.DB_OpenDatabase(henv, Application(APP_SMDSN), 0)
	End If
End Sub
Sub CloseDatabase()
	If Not objRocket Is Nothing Then
		objRocket.DB_CloseDatabase CInt(hDB)
		objRocket.DB_FreeEnvironment CLng(hEnv)
		Set objRocket = Nothing
	End If
End Sub

If LCase(Request.ServerVariables("REQUEST_METHOD"))="post" And Request("scheduleids")<>"" Then
	If LCase(Request("action"))="delete" Then
		sSQL="DELETE FROM SM_SCHEDULE WHERE SCHEDULE_ID IN (" & Request("scheduleids") & ")"
		OpenDatabase
		objRocket.DB_SQLExecute hDB,sSQL
	End If
End If

%>
<html>
<head>
	<title>Scheduled Reports</title>
	<!-- [ Rajeev 11/12/2002 -- CSS Consolidation] link rel="stylesheet" href="forms.css" type="text/css"/-->
	<link rel="stylesheet" href="RMNet.css" type="text/css"/>
<script language="JavaScript">
var ns, ie, ieversion;
var browserName = navigator.appName;                   // detect browser 
var browserVersion = navigator.appVersion;
if (browserName == "Netscape")
{
	ie=0;
	ns=1;
}
else		//Assume IE
{
	ieversion=browserVersion.substr(browserVersion.search("MSIE ")+5,1);
	ie=1;
	ns=0;
}

function ValidateDelete()
{
	if(document.frmData.schedulecount.value==0)
		return false;
	if(document.frmData.schedulecount.value==1)
	{
		if(document.frmData.scheduleids.checked)
		{
			document.frmData.action.value="delete";
			return self.confirm("Are you sure you want to delete selected schedule(s)?");
		}
		return false;
	}
	
	for(var i=0;i<document.frmData.scheduleids.length;i++)
	{
		if(document.frmData.scheduleids[i].checked)
		{
			document.frmData.action.value="delete";
			return self.confirm("Are you sure you want to delete selected schedule(s)?");
		}
	}
	alert("Please select the schedule you would like to delete.");
	return false;
}

function onClickEdit()
{
	var selected, selectedindex;

	selected=0;	
	selectedindex=0;
	
	if(document.frmData.schedulecount.value==0)
		return false;

	for(var i=0;i<document.frmData.scheduleids.length;i++)
	{
		if(document.frmData.scheduleids[i].checked)
		{
			selected=selected + 1;
			selectedindex = i;
		}
	}

	if(selected==1)
	{
		if(document.frmData.scheduleids[selectedindex].checked)
		{
			window.setTimeout("window.location.href='scheduledetail.asp?scheduleid=" + document.frmData.scheduleids[selectedindex].value + "&viewonly=0'",100);
			return true;
		}
		return false;
	}
	else
	{
		if (selected==0)
		{
			if(document.frmData.scheduleids.checked)
			{
				window.setTimeout("window.location.href='scheduledetail.asp?scheduleid=" + document.frmData.scheduleid.value + "&viewonly=0'",100);
				return true;
			}
		}
		else
			alert("Cannot Edit multiple schedules.");
			
		return false;
	}
}

function sortByColumn(sColumnName)
{
	document.frmData.orderby.value=sColumnName;
	window.setTimeout("document.frmData.submit();",100);
}

function onPageLoaded()
{
	
}
</script>
</head>
<body class="10pt" onload="onPageLoaded()">
<form name="frmData" method="post" action="schedulelist.asp">
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr class="ctrlgroup"><td colspan="5">
	<p align="center" class="smallnote">Clicking on a column name will sort the list.</p></td></tr>
	<tr class="ctrlgroup">
		<td><a href="#" onclick="sortByColumn('report_name');" class="tablink" >Report Name</a></td>
		<td><a href="#" onclick="sortByColumn('schedule_type');" class="tablink" >Schedule Type</a></td>
		<td><a href="#" onclick="sortByColumn('next_run_date,start_time');" class="tablink" >Next Run</a></td>
		<td><a href="#" onclick="sortByColumn('last_run_dttm');" class="tablink" >Last Run</a></td>
	<td></TD>
	</tr>
</table>

<DIV id="divForms" class="divScroll">
	<TABLE border="0" width="100%" height="0">
<%

OpenDatabase

sSQL="SELECT SCHEDULE_ID, SM_SCHEDULE.REPORT_ID, SM_REPORTS.REPORT_NAME, SCHEDULE_TYPE, LAST_RUN_DTTM, NEXT_RUN_DATE, START_TIME FROM SM_SCHEDULE,SM_REPORTS "
sSQL = sSQL & " WHERE SM_SCHEDULE.REPORT_ID=SM_REPORTS.REPORT_ID AND SM_SCHEDULE.USER_ID=" & objSessionStr(SESSION_USERID) 
If UCASE(Request("orderby")) & "" = "" Then 
	sSQL = sSQL & " ORDER BY SM_REPORTS.REPORT_NAME" ' NEXT_RUN_DATE"
Else
	sSQL = sSQL & " ORDER BY " & uCase(Request("orderby")) & ""
End If


hRS = objRocket.DB_CreateRecordset(hDB, sSQL, 3, 0)

If objRocket.DB_Eof(hRS) Then
	Response.Write "<tr><td colspan=""4"" align=""center""><br /><br /><br /><br /><i>There are no Reports scheduled to run</i><br /><br /><br /><br /><br /></td></tr>"
End If

sClass="datatd"
Do While Not objRocket.DB_Eof(hRS)
	objRocket.DB_GetData hRS, "SCHEDULE_ID", v
	lScheduleId=CLng(v)
	bChecked = IsChecked(lScheduleId)

	objRocket.DB_GetData hRS, "REPORT_ID", v
	lReportId=CLng(v)
	objRocket.DB_GetData hRS, "REPORT_NAME", v
	If "" & v="" Then v="Report Nr. " & lReportId
	Response.Write "<tr class=""" & sClass & """>"
	Response.Write "<td><input type=""checkbox"" " 
	If bChecked  Then Response.Write "checked" 'Add reportid by kuladeep for update the schedule report mits:27574
	Response.Write " name=""scheduleids"" value=""" & lScheduleId & """ /> <a class=""LightBold"" href=""scheduledetail.asp?scheduleid=" & lScheduleId & "&viewonly=0&reportid=" & lReportId & """>" & v & "</a></td>"
	objRocket.DB_GetData hRS, "SCHEDULE_TYPE", v
	If v=1 Then Response.Write "<td>Daily</td>" Else Response.Write "<td>Monthly</td>"
	objRocket.DB_GetData hRS, "NEXT_RUN_DATE", v
	Response.Write "<td>"
	if "" & v<>"" Then
		Response.Write FormatDateTime(DateSerial(Left(v,4),Mid(v,5,2),Mid(v,7,2)),vbGeneralDate) & " "
		objRocket.DB_GetData hRS, "START_TIME", v
		If "" & v<>"" Then Response.Write FormatDateTime(TimeSerial(Left(v,2),Mid(v,3,2),Mid(v,5,2)),vbLongTime)
	End If
	Response.Write "</td>"
	objRocket.DB_GetData hRS, "LAST_RUN_DTTM", v
	
	Response.Write "<td>"
	If "" & v<>"" Then
		Response.Write FormatDateTime(DateSerial(Left(v,4),Mid(v,5,2),Mid(v,7,2)),vbGeneralDate) & " " & FormatDateTime(TimeSerial(Mid(v,9,2),Mid(v,11,2),Mid(v,13,2)),vbLongTime)
	End If
	Response.Write "</td>"
	
	'Response.Write "<td>"
	'Response.Write "<a href=""scheduledetail.asp?scheduleid=" & lScheduleid & "&viewonly=0""" & ">Edit</a>"
	'Response.Write "</tr>"	
	objRocket.DB_MoveNext hRS
	If sClass="datatd" Then sClass="datatd1" Else sClass="datatd"
	lCount=lCount+1
Loop
objRocket.DB_CloseRecordset CInt(hRS), 2

CloseDatabase
%>
</table>
</div>
<%If lCount>0 Then%>
	<input type="submit" class="button" ONCLICK="return ValidateDelete()" value=" Delete " />
<%End If%>

<input type="hidden" name="orderby" value=""/>
<input type="hidden" name="schedulecount" value="<%=lCount%>" />
<input type="hidden" name="action" value="<%=sAction%>" />
<input type="hidden" name="scheduleid" value="<%=lScheduleId%>" />
<div class="small">
This list shows the reports that are scheduled to run on a recurring basis.  
<br />
To edit the schedule for a report, click on the report name.<br />To delete one or more scheduled reports, place a checkmark next to the report and click the "Delete" button.
<br>
*** For information on extended reporting features and report troubleshooting please see the <a href="smfaq.asp">Frequently Asked Questions (FAQ)</a>.
</div>
</form>
</body>
</html>