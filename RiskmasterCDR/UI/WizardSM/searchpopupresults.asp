<!-- #include file ="session.inc" -->
<%Public objSession
initSession objSession
%> 
<% Response.Buffer = True %>
<SCRIPT LANGUAGE="VBScript" RUNAT=SERVER>
Dim objData
dim sResults

Set objData=CreateObject("SearchLib.CSearchResults")

'tkr 4/2003 no caching on web server
sResults = objData.GetSearchResults(Request("page"), objSessionStr("UserID"), Application("APP_DATA_PATH") & "searchpopupresults.xsl",Application(APP_SESSIONDSN),objSessionStr(SESSION_SID)) 
'sResults = objData.getResultPage(Request("page"), objSessionStr("UserID"), objSessionStr("SearchResultsPath"), Application("APP_DATA_PATH") & "searchpopupresults.xsl")

Response.ContentType = "text/HTML"
Response.Write sResults

Response.End

Set objData=Nothing
</SCRIPT>
