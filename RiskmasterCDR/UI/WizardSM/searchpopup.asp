<!-- #include file ="session.inc" -->
<%Public objSession
initSession objSession
%> 
<% Response.Buffer = True %>
<SCRIPT LANGUAGE="VBScript" RUNAT=SERVER>
Dim objData
Dim sResults
Dim lViewId, sTableId
Dim sCategory

Set objData=CreateObject("SearchLib.CSearchGen")

lViewId=Request("viewid")
sCategory=Trim(Request("cat") & "")

If IsNumeric(lViewId) And lViewId<>"" Then
	lViewId=CLng(lViewId)
Else
	lViewId=0
End If

sTableId=Trim(Request("tableid") & "")
If Len(sCategory) <> 0 Then
	Dim objTmp
	Set objTmp=CreateObject("SearchLib.CSearchResults")
	lViewID = objTmp.getBestSearch(objSessionStr("DSN"), sCategory, "")
	Set objTmp=Nothing
End If

If lViewId = 0 THen   'tkr 1/2003 enforce user permissions on searches
	Response.Write "No search has been defined for this data entry form, or the user lacks permission to use any defined search in this category."
	Response.End
end if

objData.UserId=objSession(SESSION_USERID)  'tkr 1/2003 enforce user permissions on searches
objData.GroupId=objSession(SESSION_GROUPID)
If CBool(bSOUNDEX) Then objData.Settings="soundex=" & bSOUNDEX
If lViewId=4 And Len(sTableId) > 0 And Len(sCategory)=0 Then lViewID = objData.getBestEntitySearch(objSessionStr("DSN"), sTableId, lViewId)
sResults = objData.CreateSearchViewEx(objSessionStr("DSN"), lViewId, Application("APP_DATA_PATH") & "searchpopup.xsl",sTableId,Request("sys_ex"))
set objData=nothing

Response.ContentType = "text/HTML"
Response.Write sResults
Response.End

Set objData=Nothing
</SCRIPT>
