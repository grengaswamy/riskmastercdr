﻿<%@ page title="" language="C#" masterpagefile="~/RMXPortalTemplate.master" autoeventwireup="true"
    codefile="Login.aspx.cs" inherits="Login" %>

<asp:content id="cPortalLoginHeader" runat="server" contentplaceholderid="cphPortalHeader">
    <div class="portalheader">
        <asp:image imageurl="~/App_Themes/RMXPortal/images/weblogox4.gif" runat="server" />
    </div>
</asp:content>
<asp:content id="Content3" contentplaceholderid="cphPortalContent" runat="Server">
    <asp:login id="Login1" runat="server" onauthenticate="OnAuthenticate">
        <layouttemplate>
            <table border="0" cellpadding="1">
                <tr>
                    <td>
                        <table border="0" cellpadding="0">
                            <tr>
                                <td align="center" colspan="2">
                                    Log In
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:label id="UserNameLabel" runat="server" associatedcontrolid="UserName">User Name:</asp:label>
                                </td>
                                <td>
                                    <asp:textbox id="UserName" runat="server"></asp:textbox>
                                    <asp:requiredfieldvalidator id="UserNameRequired" runat="server" controltovalidate="UserName"
                                        errormessage="User Name is required." tooltip="User Name is required." validationgroup="Login1">*
                                    </asp:requiredfieldvalidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:label id="PasswordLabel" runat="server" associatedcontrolid="Password">
                    Password:</asp:label>
                                </td>
                                <td>
                                    <asp:textbox id="Password" runat="server" textmode="Password"></asp:textbox>
                                    <asp:requiredfieldvalidator id="PasswordRequired" runat="server" controltovalidate="Password"
                                        errormessage="Password is required." tooltip="Password is required." validationgroup="Login1">*
                                    </asp:requiredfieldvalidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:dropdownlist id="ddlSSOProviders" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:checkbox id="RememberMe" runat="server" text="Remember me next time." />
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2" style="color: red">
                                    <asp:literal id="FailureText" runat="server" enableviewstate="False">
                                    </asp:literal>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" colspan="2">
                                    <asp:button id="LoginButton" runat="server" commandname="Login" text="Log In" validationgroup="Login1" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </layouttemplate>
    </asp:login>
    <br />
    <br />
    <asp:loginname id="LoginName1" runat="server" />
    <asp:loginstatus id="LoginStatus1" runat="server" />
    <br />
    <br />
</asp:content>
