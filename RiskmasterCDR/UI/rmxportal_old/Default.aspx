<%@ page title="" language="C#" masterpagefile="~/RMXPortalTemplate.master" autoeventwireup="true"
    codefile="Default.aspx.cs" inherits="_Default" %>

<asp:content id="cphHeaderHomeContent" contentplaceholderid="head" runat="server">

    <script language="JavaScript" type="text/javascript">
        function ReSizeiFrame() {
            var x, y;
            if (self.innerHeight) // all except Explorer
            {
                x = self.innerWidth;
                y = self.innerHeight;
            }
            else if (document.documentElement && document.documentElement.clientHeight)
            // Explorer 6 Strict Mode
            {
                x = document.documentElement.clientWidth;
                y = document.documentElement.clientHeight;
            }
            else if (document.body) // other Explorers
            {
                x = document.body.clientWidth;
                y = document.body.clientHeight;
            }

            try {
                var objElts = document.getElementById("HomeFrame");
                if (objElts.id != null) {
                    objElts.style.height = y - 107;
                    objElts.style.width = x - 45;
                }
            }
            catch (e) {
            }
        }


    </script>

</asp:content>
<%--<body bgcolor="#0d4c7d" onresize="ReSizeiFrame();">--%>
<asp:content id="cphPortalHomeContent" contentplaceholderid="cphPortalContent" runat="server">
    <div class="portalbody">
        <asp:loginstatus id="logStatus" runat="server" />
        <asp:loginname id="logName" runat="server" />
        <asp:loginview id="loginView" runat="server" />
        <asp:label id="loginInfo" runat="server" />
    </div>
</asp:content>
