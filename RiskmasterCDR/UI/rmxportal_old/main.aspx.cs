﻿using System;
using System.Text;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.Sql;
using System.Data.SqlClient;

namespace Riskmaster.RmxPortal
{
    public partial class _main : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //SqlDataSource sds = new SqlDataSource("server=20.198.58.52;database=QA_R4_SECURITY;uid=sa;password=rmserver_123;", "select * from portlet_info");
            StringBuilder sb ;//= new StringBuilder();
            string sName = "", sUrl = "",sId="";
            try
            {
                SqlConnection sdc = new SqlConnection("server=20.198.58.52;database=QA_R4_SECURITY;uid=sa;password=rmserver_123;");
                sdc.Open();
                SqlDataAdapter sda = new SqlDataAdapter("SELECT * FROM PORTLET_INFO", sdc);
                DataSet dsPortlets = new DataSet();
                sda.Fill(dsPortlets);
                sdc.Close(); 
                //lblStatus.Text = dsPortlets.Tables[0].Rows.Count.ToString();
                int i = 0;
                sb = new StringBuilder("<div id='rmxportal-container' ><ul>");//height='300px'
                //sb.Append("<div id='rmxportal-container' ><ul>");
                for (i = 0; i < dsPortlets.Tables[0].Rows.Count;i++ )
                {
                    sName = dsPortlets.Tables[0].Rows[i]["name"].ToString();
                   // sUrl = dsPortlets.Tables[0].Rows[i]["url"].ToString();
                    //sId = "rmxR5Portlet_"+i.ToString();
                    sb.Append(string.Format("<li><a href='#rmxR5Portlet_{0}'><span>{1}</span></a></li>", i, sName));
                    
                    //add <li><a href="#fragment-1"><span>t 1</span></a> </li>
                    //add </ul>         <div id="fragment-1"  >
                }
                sb.Append("</ul>");
                //add </div>
                for (i = 0; i < dsPortlets.Tables[0].Rows.Count; i++)
                {
                    //sName = dsPortlets.Tables[0].Rows[i]["name"].ToString();
                    sUrl = dsPortlets.Tables[0].Rows[i]["url"].ToString();
                    //sId = "rmxR5Portlet_" + i.ToString(); //height='400px'<br/>
                    sb.Append(string.Format("<div border='none' id='rmxR5Portlet_{0}'><iframe border='none' id='rmxR5Portlet_{0}' height='600px'width='1113px' src='{1}'></iframe></div>", i, sUrl));
                }

                sb.Append("</div>");

                TabsPlaceHolder.Text = sb.ToString();                    
            }
            catch (Exception ex)
            {
                lblStatus.Text = ex.Message.ToString();
            }
            

        }        
    }
}
