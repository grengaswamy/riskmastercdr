﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.IdentityModel;
using System.IdentityModel.Selectors;
using System.Text;
using System.Web.Security;

// NOTE: If you change the class name "AuthenticationService" here, you must also update the reference to "AuthenticationService" in Web.config.
//public class AuthenticationService : UserNamePasswordValidator, IAuthenticationService
public class AuthenticationService: IAuthenticationService
{
    //public AuthenticationService(string strProviderType, string strProviderConnection)
    //{
    //    ProviderType = strProviderType;
    //    ProviderConnection = strProviderConnection;

    //} // constructor2


    public string ProviderType { get; set; }

    public string ProviderConnection { get; set; }

    public string UserName { get; set; }

    public string Password { get; set; }

    

    //public override void Validate(string userName, string password)
    //{
    //    const string CSC_USERNAME = "csc";
    //    const string CSC_PASSWORD = "csc";

    //    if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
    //    {
    //        throw new ArgumentNullException();
    //    }

    //    if (!userName.Equals(CSC_USERNAME) && !password.Equals(CSC_PASSWORD))
    //    {
    //        throw new FaultException("Unknown Username or Incorrect Password");
    //    }
    //}

    #region IAuthenticationService Members



    public bool AuthenticateUser(string strMembershipProviderName)
    {
        MembershipProvider membProvider = Membership.Providers[strMembershipProviderName];
        bool blnIsAuthenticated = false;


        if (string.IsNullOrEmpty(this.UserName) || string.IsNullOrEmpty(this.Password))
        {
            throw new FaultException("Username or password credentials were not provided.");
        } // if
        else
        {
            blnIsAuthenticated = membProvider.ValidateUser(this.UserName, this.Password);
        } // else

        return blnIsAuthenticated;
    }

    #endregion
}
