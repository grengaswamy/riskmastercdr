using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Data.Odbc;
using Riskmaster.Security.Encryption;
using Resources;

/// <summary>
/// Summary description for RiskmasterMembershipProvider
/// </summary>
public class RiskmasterMembershipProvider : MembershipProvider
{
    private string m_strMembershipProviderName = "RiskmasterMembershipProvider";
    private string m_strDBConnString = ConfigurationManager.ConnectionStrings["RMXSecurity"].ConnectionString;

    public RiskmasterMembershipProvider()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    //public RiskmasterMembershipProvider(string strDbConnString)
    //{
    //    m_strDBConnString = strDbConnString;
    //}

    //public string ConnectionString
    //{
    //    get
    //    {
    //        return m_strDBConnString;
    //    }
    //    set
    //    {
    //        m_strDBConnString = value;
    //    }
    //}


    public override string ApplicationName
    {
        get
        {
            return m_strMembershipProviderName;
        }
        set
        {
            m_strMembershipProviderName = value;
        }
    }

    public override bool ChangePassword(string username, string oldPassword, string newPassword)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public override bool DeleteUser(string username, bool deleteAllRelatedData)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public override bool EnablePasswordReset
    {
        get { throw new Exception("The method or operation is not implemented."); }
    }

    public override bool EnablePasswordRetrieval
    {
        get { throw new Exception("The method or operation is not implemented."); }
    }

    public override MembershipUserCollection FindUsersByEmail(string emailToMatch,
                       int pageIndex, int pageSize, out int totalRecords)
    {
        MembershipUserCollection users = new MembershipUserCollection();
        try
        {
            OdbcConnection conn = new OdbcConnection(m_strDBConnString);
            conn.Open();
            OdbcCommand cmd = new OdbcCommand
               ("SELECT LOGIN_NAME FROM SMS_USERS WHERE LOGIN_NAME LIKE '"
                + emailToMatch + "%'", conn);
            OdbcDataReader rd = cmd.ExecuteReader();
            if (rd.HasRows)
            {
                while (rd.Read())
                {
                    MembershipUser user =
                                 new MembershipUser(this.ApplicationName,
                                    rd["LOGIN_NAME"].ToString(), null, rd["LOGIN_NAME"].ToString(),
                            null, string.Empty, true, false,
                                    DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now,
                                    DateTime.Now);
                    users.Add(user);
                }
            }
        }
        catch (Exception ex)
        {
            //-- throw exception
        }
        finally
        {
            //-- clean up and close connection
        }
        totalRecords = users.Count;
        return users;
    }


    public override MembershipUserCollection FindUsersByName
               (string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
    {
        MembershipUserCollection users = new MembershipUserCollection();
        try
        {
            OdbcConnection conn = new OdbcConnection(m_strDBConnString);
            conn.Open();
            OdbcCommand cmd = new OdbcCommand
               ("SELECT LOGIN_NAME FROM SMS_USERS WHERE LOGIN_NAME LIKE '"
                + usernameToMatch + "%'", conn);
            OdbcDataReader rd = cmd.ExecuteReader();
            if (rd.HasRows)
            {
                while (rd.Read())
                {
                    MembershipUser user =
                                 new MembershipUser(this.ApplicationName,
                                    rd["LOGIN_NAME"].ToString(), null, rd["LOGIN_NAME"].ToString(),
                            null, string.Empty, true, false,
                                    DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now,
                                    DateTime.Now);
                    users.Add(user);
                }
            }
        }
        catch (Exception ex)
        {
            //-- throw exception
        }
        finally
        {
            //-- clean up and close connection
        }
        totalRecords = users.Count;
        return users;
    }

    public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public override int GetNumberOfUsersOnline()
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public override string GetPassword(string username, string answer)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public override MembershipUser GetUser(string username, bool userIsOnline)
    {
        MembershipUser user = null;
        try
        {
            OdbcConnection conn = new OdbcConnection(m_strDBConnString);
            conn.Open();
            OdbcCommand cmd = new OdbcCommand
               ("SELECT LOGIN_NAME FROM SMS_USERS WHERE LOGIN_NAME = ?", conn);
            cmd.Parameters.AddWithValue("@username", username);
            OdbcDataReader rd = cmd.ExecuteReader();
            if (rd.HasRows)
            {
                rd.Read();
                user = new MembershipUser(this.ApplicationName,
                                    rd["LOGIN_NAME"].ToString(), null, rd["LOGIN_NAME"].ToString(),
                            null, string.Empty, true, false,
                                    DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now,
                                    DateTime.Now);
            }
        }
        catch (Exception ex)
        {
            //-- throw exception
        }
        finally
        {
            //-- clean up and close connection
        }
        return user;
    }

    public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public override string GetUserNameByEmail(string email)
    {
        string user = string.Empty;
        try
        {
            OdbcConnection conn = new OdbcConnection(m_strDBConnString);
            conn.Open();
            OdbcCommand cmd = new OdbcCommand
               ("SELECT LOGIN_NAME FROM SMS_USERS WHERE LOGIN_NAME= ?", conn);
            cmd.Parameters.AddWithValue("@email", email);
            OdbcDataReader rd = cmd.ExecuteReader();
            if (rd.HasRows)
            {
                rd.Read();
                user = rd["LOGIN_NAME"].ToString();
            }
        }
        catch (Exception ex)
        {
            //-- throw exception
        }
        finally
        {
            //-- clean up and close connection
        }
        return user;
    }

    public override int MaxInvalidPasswordAttempts
    {
        get { throw new Exception("The method or operation is not implemented."); }
    }

    public override int MinRequiredNonAlphanumericCharacters
    {
        get { throw new Exception("The method or operation is not implemented."); }
    }

    public override int MinRequiredPasswordLength
    {
        get { throw new Exception("The method or operation is not implemented."); }
    }

    public override int PasswordAttemptWindow
    {
        get { throw new Exception("The method or operation is not implemented."); }
    }

    public override MembershipPasswordFormat PasswordFormat
    {
        get { throw new Exception("The method or operation is not implemented."); }
    }

    public override string PasswordStrengthRegularExpression
    {
        get { throw new Exception("The method or operation is not implemented."); }
    }

    public override bool RequiresQuestionAndAnswer
    {
        get { throw new Exception("The method or operation is not implemented."); }
    }

    public override bool RequiresUniqueEmail
    {
        get { throw new Exception("The method or operation is not implemented."); }
    }

    public override string ResetPassword(string username, string answer)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public override bool UnlockUser(string userName)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public override void UpdateUser(MembershipUser user)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    /// <summary>
    /// Validates that a user is a member of the Security Database
    /// </summary>
    /// <param name="username"></param>
    /// <param name="password"></param>
    /// <returns></returns>
    public override bool ValidateUser(string username, string password)
    {
        bool blnIsValidUser = false;
        string strResult = string.Empty;
        int intUserID = 0;
        DTGCrypt32 objCrypto = new DTGCrypt32();

        string strEncPassword = objCrypto.EncryptString(password, RMXPortal.AUTH_CODE);

        using (OdbcConnection odbcConn = new OdbcConnection(m_strDBConnString))
        {
            //Open the connection string
            odbcConn.Open();


            string strCmd = "SELECT USER_ID FROM SMS_USERS WHERE LOGIN_NAME= ? AND PASSWORD = ?";
            OdbcCommand odbcCmd = new OdbcCommand(strCmd, odbcConn);

            OdbcParameter odbcParamUID = new OdbcParameter("@UID", username);
            OdbcParameter odbcParamPWD = new OdbcParameter("@PWD", strEncPassword);

            odbcCmd.Parameters.Add(odbcParamUID);
            odbcCmd.Parameters.Add(odbcParamPWD);

            OdbcDataReader odbcReader = odbcCmd.ExecuteReader();

            //Verify that the OdbcReader has rows
            if (odbcReader.HasRows)
            {
                strResult = odbcReader[RMXPortal.VALID_USER].ToString();
            } // if

            odbcReader.Close();
        } // using


        bool blnSuccess = Int32.TryParse(strResult, out intUserID);

        //Verify that a result was returned
        if (blnSuccess && (intUserID > 0 || intUserID.ToString().Equals(RMXPortal.CSC_USER)))
        {
            blnIsValidUser = true;
        } // if

        //if (dstUserCredentials.Tables[0].Rows.Count != 0)
        //{
        //    blnIsValidUser = true;
        //}//if

        //return the boolean indicating whether or not
        //the user has been authenticated successfully
        return blnIsValidUser;
    }
}
