/**
 * Created by nsharma202 on 12/4/2014.
 */
app.filter('rmaTimeFilter', [function () {
    'use strict';
    return function (time) {
        if (typeof time != 'undefined') {
            return time.match(/^[0-9]{0,2}:[0-9]{0,2}/g) + ' ' + time.match(/(AM|PM)$/g);
        }
//            if (!angular.isUndefined(time))
//                console.log(time.match(/^[0-9]{0,2}:[0-9]{0,2}/g) + ' ' + time.match(/(AM|PM)$/g));
//            return time.match(/^[0-9]{0,2}:[0-9]{0,2}/g) + ' ' + time.match(/(AM|PM)$/g);
//        }
    }
}]);
