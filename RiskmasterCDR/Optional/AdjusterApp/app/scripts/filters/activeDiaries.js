/**
 * Created by nsharma202 on 11/17/2014.
 */

app.filter('active',[function () {

    return function (items,filterOn) {
        if (filterOn === false) {
            return items;
        }
        if ((filterOn || angular.isUndefined(filterOn)) && angular.isArray(items)){
            var filtered=[];
            var filterDate=new Date();
            angular.forEach(items,function(item){
                var itemDate=new Date(item.completeDate);
                if(itemDate<=filterDate){
                    filtered.push(item);
                }
            });
            return filtered;
        }
    };
}]);
