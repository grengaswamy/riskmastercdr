/**
 * Created by nsharma202 on 1/29/2015.
 */
app.filter('parseUrl',['SERVER',function(SERVER){
  return function(url){
    if(!angular.isUndefined(url) && url){
      var parse_url = /^(?:([A-Za-z]+):)?(\/{0,3})([0-9.\-A-Za-z]+)(?::(\d+))?(?:\/([^?#]*))?(?:\?([^#]*))?(?:#(.*))?$/;
      var result = parse_url.exec(url);
      return 'http://'+result[3];
    }
    else return SERVER;
  };
}]);
