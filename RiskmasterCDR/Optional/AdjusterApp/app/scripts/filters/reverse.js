/**
 * Created by nsharma202 on 12/17/2014.
 */
app.filter('reverse', function() {
    return function(items) {
        return items.slice().reverse();
    };
});
