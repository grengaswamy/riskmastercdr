﻿app.factory('Data',['Event','Task','Claim','Code','localStorageService','$filter',
  function(Event,Task,Claim,Code,localStorageService,$filter){
'use strict';
    var welcomeDiv='true';
    var loginDiv='false';
    var homeDiv='false';
    var totalClaims='';
    var totalEvents='';
    var totalTasks='';
    var lastSyncTime='';
    if(!localStorageService.get('sessionId')){
        localStorageService.set('progressBarStatus',0);
    }
    var Data= {
        load : function(){
            localStorageService.set('progressBarStatus',0);
            Event.loadEvents();
            Claim.loadClaims();
            Task.loadTasks();
            Code.requestCodes();
        },
        reset: function(){
             welcomeDiv='true';
             loginDiv='false';
             homeDiv='false';
             localStorageService.set('progressBarStatus',0);
             totalClaims='';
             totalEvents='';
             totalTasks='';
             lastSyncTime='';
        },
        sync:function(lastSynced){
            Task.loadTasks(lastSynced);
        },
        setWelcomeStatus: function(w){
            welcomeDiv=w;
        },
        getWelcomeStatus: function(){
            return welcomeDiv;
        },
        setLoginStatus: function(l){
            loginDiv=l;
        },
        getLoginStatus:function(){
            return loginDiv;
        },
        setHomeStatus:function(h){
            homeDiv=h;
        },
        getHomeStatus: function(){
            return homeDiv;
        },
        setProgressBar:function(){
            localStorageService.set('progressBarStatus',0);
        },
        setProgressBarStatus: function(v){
            var value=parseInt(localStorageService.get('progressBarStatus'));
            value+=v;
            localStorageService.set('progressBarStatus',value);
        },
        getProgressBarStatus: function(){
            return parseInt(localStorageService.get('progressBarStatus'));
        },
        addPin : function(pin){
            if(localStorageService.get('pins')===null){
                var pins=[];
                pins.push(pin);
                localStorageService.set('pins',pins);
            }
            else{
                var pins=localStorageService.get('pins');
                pins.push(pin)
                localStorageService.set('pins',pins);
            }
        },
        removePin: function(item){
          var pins=localStorageService.get('pins');
          for(var i=0;i<pins.length;i++){
              if(pins[i].link==item.link){
                  pins.splice(i,1);
              }
          }
          localStorageService.set('pins',pins);
        },
        checkPin:function(item){
            var pins=localStorageService.get('pins');
            if(pins===null) return false;
            for(var i=0;i<pins.length;i++){
                if(pins[i].link==item.link){
                    return true;
                }
            }
            return false;
        },
        getPins:function(){
            //if(localStorageService.get('pins')===null){
            //    return [];
            //}
            return localStorageService.get('pins')  || [];
        },
        setLastSyncTime: function(){
            lastSyncTime=$filter('date')(new Date(), 'yyyyMMddHHmmss', 'UTC')
            localStorageService.set('lastSyncTime',lastSyncTime);
        },
        getLastSyncTime: function(){
          return localStorageService.get('lastSyncTime')||new Date();
        }
    }
    return Data;
  }]);
