'use strict';
app.factory('Task', ['$http', '$q', 'Auth', 'Event', 'Claim', 'BASE_URL', 'localStorageService',
  function ($http, $q, Auth, Event, Claim, BASE_URL, localStorageService) {
    var raw = [];
    var diaries = [];
    var error = 'false';
    var errorMessage = '';
    var tasksPromise = $q.defer();
    var syncError = 'false';
    var syncPromise = $q.defer();

    if (!localStorageService.get('sessionId')) {
      localStorageService.set('tasks', '');
      localStorageService.set('tasks.loading', 'true');
    }
    else {
      localStorageService.get('tasks.loading');
    }

    var format = function (raw) {
      //Sorting claims & events
      diaries = localStorageService.get('tasks') || [];
      angular.forEach(raw, function (diary) {
        var temp = {
          'entryId': '',
          'taskSubject': '',
          'entryNotes': '',
          'workActivity': '',
          'completeDate': '',
          'completeTime': '',
          'attachments': '',
          'linked': '',
          'type': ''
        };
        temp.entryId = diary.entryId;
        temp.taskSubject = diary.name;
        temp.entryNotes = diary.entryNotes;
        temp.workActivity = diary.workActivity;
        temp.completeDate = diary.completeDate;
        temp.completeTime = diary.completeTime;
        temp.attachments = diary.attachPrompt;
        var link = '';

        if (diary.attachPrompt.indexOf('EVENT') > -1) {
          temp.type = 'E';
          link = temp.attachments.replace('EVENT ', '');
          temp.linked = link;
        }
        else if (diary.attachPrompt.indexOf('CLAIM') > -1) {
          temp.type = 'C';
          link = temp.attachments.replace('CLAIM ', '');
          temp.linked = link;
        }
        else {
          temp.type = 'O';
        }
        diaries.push(temp);
      });
      localStorageService.set('tasks', diaries);
      console.log('formatted tasks');
      console.log(diaries);
    };

    //Required by 14.3
    var formatOlderRma = function (raw) {
      diaries = localStorageService.get('tasks') || [];
      //Sorting claims & events
      angular.forEach(raw, function (diary) {
        var temp = {
          'taskSubject': '',
          'entryNotes': '',
          'workActivity': '',
          'completeDate': '',
          'completeTime': '',
          'attachments': '',
          'linked': '',
          'type': ''
        };
        temp.taskSubject = diary.tasksubject;
        temp.entryNotes = diary.EntryNotes;
        temp.workActivity = diary.work_activity;
        temp.completeDate = diary.complete_date;
        temp.completeTime = diary.CompleteTime;
        temp.attachments = diary.attachprompt;
        var link = '';
        if (diary.attachprompt.indexOf('EVENT') > -1) {
          temp.type = 'E';
          link = temp.attachments.replace('EVENT ', '');
          temp.linked = link;
        }
        else if (diary.attachprompt.indexOf('CLAIM') > -1) {
          temp.type = 'C';
          link = temp.attachments.replace('CLAIM ', '');
          temp.linked = link;
        }
        else {
          temp.type = 'O';
        }
        diaries.push(temp);

      });
      console.log('formatted tasks');
      localStorageService.set('tasks', diaries);
      console.log(diaries);
    }

    //Required by 14.3
    var handleOlderRMA = function (data) {
      if (data.ResultMessage.MsgStatus.MsgStatusCd == 'Error') {
        console.log('error fetching tasks list');
        console.log(data);
        error = 'true';
        errorMessage = 'Error';
        localStorageService.set('tasks.loading', 'false');
        tasksPromise.reject(0);
      }
      else {
        error = 'false';
        console.log('tasks success 14.3 ');
        if (data.ResultMessage.Document.diaries.diary === undefined) {
          //no diary
        }
        else if (data.ResultMessage.Document.diaries.diary.length === undefined) {
          //here we have only one diary
          raw.push(data.ResultMessage.Document.diaries.diary);
        }
        else {
          angular.forEach(data.ResultMessage.Document.diaries.diary, function (diary) {
            raw.push(diary);
          });
        }
        console.log('tasks');
        console.log(raw);
        formatOlderRma(raw);
        localStorageService.set('tasks.loading', 'false');
        tasksPromise.resolve(diaries.length);
      }
    };

    //Check Task if already present in the list
    var checkPresence = function (newDiaries) {
      diaries = localStorageService.get('tasks');

//        Get last entryId of last element from tasks list
      var last = parseInt(diaries[diaries.length - 1].entryId);
//        Get entryId of first element of fresh task list
      //var fresh=parseInt(newDiaries[0].entryId);
      for (var i = 0; i < newDiaries.length; i += 1) {
        if (parseInt(newDiaries[i].entryId) > last) {
          return i;
        }
      }

//        if(last < fresh){
//            return true;
//        }
//        else {
//            return false;
//        }
    }

    var Task = {
      reset: function () {
        raw = [];
        localStorageService.set('tasks.loading', 'true');
        error = 'false';
        errorMessage = '';
        syncError = 'false';
        tasksPromise = $q.defer();
        localStorageService.set('tasks', '');
      },
      requestTasks: function (lastSynced) {
        if (lastSynced === undefined) {
          return $http({
            method: 'GET',
            url: Auth.getServer() + BASE_URL + '/Tasks',
            headers: {'session': Auth.getSession(), 'user': Auth.getUsername(), 'ClientId': Auth.getClientId()}
          });
        }
        else {
          return $http({
            method: 'GET',
            url: Auth.getServer() + BASE_URL + '/Tasks/' + lastSynced,
            headers: {'session': Auth.getSession(), 'user': Auth.getUsername(), 'ClientId': Auth.getClientId()}
          });
        }
      },
      loadTasks: function (lastSynced) {
        console.log('loading tasks');
        raw = [];
        Task.requestTasks(lastSynced)
          .success(function (data) {
            //14.3 backward compatibility check
            if (!(data.ResultMessage === undefined)) {
              handleOlderRMA(data);
            }
            else {
              //15.1 onwards
              if (data.Diaries === undefined || data.Diaries.length === undefined) {
                console.log('error fetching tasks list');
                console.log(data);
                error = 'true';
                errorMessage = 'Error'
                localStorageService.set('tasks.loading', 'false');
                tasksPromise.reject(0);
              }
              else {
                error = 'false';
                console.log('tasks success');
                console.log(data);
                raw = data.Diaries;
                console.log('tasks');
                console.log(raw);
                format(raw);
                tasksPromise.resolve(diaries.length);
                localStorageService.set('tasks.loading', 'false');
              }
            }
          })
          .error(function (error) {
            console.log('error fetching tasksList');
            console.log(error);
            error = 'true';
            localStorageService.set('tasks.loading', 'false');
            tasksPromise.reject(0);
          });
      },
      sync: function (lastSynced) {
        raw = [];
        Task.requestTasks(lastSynced)
          .success(function (data) {
            console.log(data);
            if (!data.hasOwnProperty('Diaries')) {
              console.log('error fetching new tasks');
              syncError = 'true';
              syncPromise.reject('try again');
            }
            else {
              syncError = 'false';
              console.log('syncing tasks success');
              if (data['Diaries'] === null) {
                syncPromise.resolve(0);
              }
              else {
                angular.forEach(data.Diaries, function (diary) {
                  raw.push(diary);
                });
              }
              console.log('new tasks');
              console.log(raw);
              format(raw);
              syncPromise.resolve(raw.length);
//                        if(raw.length && checkPresence(raw)){
//                            format(raw);
//                            syncPromise.resolve(raw.length);
//                        }
//                        else{
//                            syncPromise.resolve(0);
//                        }
            }
          })
          .error(function (data) {
            console.log('error fetching new tasks');
            console.log(data);
            syncError = 'true';
            syncPromise.reject('Not able to reach server');
          });
        return syncPromise.promise;
      },
      getTasks: function () {
        diaries = localStorageService.get('tasks') || [];
        return diaries;
      },
      find: function (id) {
        console.log('inside find: ' + id);
        var temp = [];
        diaries = localStorageService.get('tasks') || [];
        for (var i = 0; i < diaries.length; i++) {
          if (diaries[i]['linked'] == id) {
            temp.push(diaries[i]);
          }
        }
        return temp;
      },
      getLoading: function () {
        return localStorageService.get('tasks.loading');
      },
      getError: function () {
        return error;
      },
      getErrorMessage: function () {
        return errorMessage;
      },
      getTotalTasks: function () {
        return tasksPromise.promise;
      },
      addTask: function (t) {
        diaries = localStorageService.get('tasks') || [];
        diaries.push(t);
        localStorageService.set('tasks', diaries);
      },
      getUpdatedTaskCount: function () {
        diaries = localStorageService.get('tasks') || [];
        return diaries.length;
      }
    };
    return Task;
  }]);

