﻿'use strict';
app.factory('Note',['$http','Auth','BASE_URL',function($http,Auth,BASE_URL){

    var Note = {
        requestNotes : function(){
            $http({
                method : 'GET',
                url: Auth.getServer()+ BASE_URL+'/Notes',
                headers : {'session': Auth.getSession(),'ClientId':Auth.getClientId()}

            })
            .success(function(data){
                console.log(data);
            })
            .error(function(error){
                console.log('error requesting codes');
                console.log(error);
            });
        },
        postNote : function(note){
           return $http({
                method : 'POST',
                url : Auth.getServer()+BASE_URL +'/Notes',
                data : note,
                headers : {'session' : Auth.getSession(), 'Content-Type' : 'json','ClientId':Auth.getClientId()}
             });
        }

    };
    return Note;
}]);
