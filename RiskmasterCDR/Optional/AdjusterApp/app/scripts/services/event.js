'use strict';
app.factory('Event',['$rootScope','$q','$http','Auth','BASE_URL','localStorageService',
    function($rootScope,$q,$http,Auth,BASE_URL,localStorageService){
    var events=[];
    var raw='';
    var eventAttributes=[];
    var columns=[];
    //var loading='true';
    var error='false';
    var errorMessage='';
    var eventPromise=$q.defer();
    if(!localStorageService.get('sessionId')){
        localStorageService.set('events','');
        localStorageService.set('events.loading','true');
    }
    else {
        localStorageService.get('events.loading');
    }
    var Event ={
        reset : function(){
            events=[];
            raw='';
            eventAttributes=[];
            columns=[];
            error='false';
            errorMessage='';
            eventPromise=$q.defer();
            localStorageService.set('events','');
            localStorageService.set('events.loading','true');
        },
        requestEvents : function(){
            return  $http({
                method : 'GET',
                url: Auth.getServer()+ BASE_URL+'/Events',
                headers : {'session': Auth.getSession(),'ClientId':Auth.getClientId()}

            });
        },
        loadEvents : function(){
            console.log('Loading events');
            events=[];
            Event.requestEvents()
                .success(function(data){
                    if(data.ResultMessage.MsgStatus.MsgStatusCd=='Error'){
                        console.log("error fetching events");
                        console.log(data);
                        error='true';
                        errorMessage=data.ResultMessage.MsgStatus.ExtendedStatus;
                        eventPromise.resolve(0);
                        localStorageService.set('events.loading','false');
                    }
                    else {

                        error='false';
                        console.log('event success');
                        raw=data;
                        //getting column heads
                        angular.forEach(raw.ResultMessage.Document.results.columns.column, function(column){
                            eventAttributes.push(column.text);
                            columns.push(column);
                        });

                        //formatting data

                        //console.log(raw.ResultMessage.Document.results.data.row);
                        if(Array.isArray(raw.ResultMessage.Document.results.data.row)){
                          angular.forEach(raw.ResultMessage.Document.results.data.row, function(row){
                            console.log(row);
                            var temp={};
                            angular.forEach(row.field, function(v, j){
                              temp[eventAttributes[j]]=v;
                            });
                            events.push(temp);
                          });
                        }
                        else {
                          var temp={};
                          angular.forEach(raw.ResultMessage.Document.results.data.row.field, function(v, j){
                            temp[eventAttributes[j]]=v;
                          });
                          events.push(temp);
                        }
                        localStorageService.set('events.loading','false');
                        eventPromise.resolve(events.length);
                        console.log('FORMATTED Events');
                        console.log(events);
                        localStorageService.set('events',events);
                    }
                })
                .error(function(error){
                    console.log('error !: Service Not reachable');
                    console.log(error);
                    error=true;
                    errorMessage='server not reachable';
                    eventPromise.resolve(0);
                    //loading=false;
                    localStorageService.set('events.loading','false');
                });

        },
        setEvents: function(data){
            localStorageService.set('events',data);
        },
        getEvents:function(){
            return localStorageService.get('events') || [];
        },
        find: function(id){
            events=localStorageService.get('events') || [];
            for(var i=0;i<events.length;i++){
                var event=events[i];
                if(event['Event Number']==id){
                    return event;
                }
            }
            return null;
        },
        getLoading : function() {
            return localStorageService.get('events.loading');
        },
        getError : function() {
            return error;
        },
        getErrorMessage : function() {
            return errorMessage;
        },
        getTotalEvents : function() {
            return eventPromise.promise;
        },
        getTotalEventsStatic : function() {
          if(localStorageService.get('events'))
             return (localStorageService.get('events')).length
          else return 0;
        }
    };
    return Event;
}]);
