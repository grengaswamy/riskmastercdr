/**
 * Created by nsharma202 on 12/5/2014.
 */
app.factory('Sync',['Claim','Event','Task','$q',function(Claim,Event,Task,$q){
    'use strict';
    var a=[];
    var b=[];
    var result=$q.defer();
    var differ= $q.defer();
    var requestClaims=function(){
        var columns=[];
        var claimsNew=[];
        Claim.requestClaims()
            .success(function(data){
                var claimAttributes=[];
                if(data.ResultMessage.MsgStatus.MsgStatusCd=='Error'){
                    console.log('error fetching claims list');
                    console.log(data);
                    result.reject('sorry dude');
                }
                else{

                    //getting column heads
                    angular.forEach(data.ResultMessage.Document.results.columns.column, function(column){
                        claimAttributes.push(column.text);
                        columns.push(column);
                    });

                    //formatting data
                    angular.forEach(data.ResultMessage.Document.results.data.row, function(row){
                        var temp={};
                        angular.forEach(row.field, function(v, j){
                            temp[claimAttributes[j]]=v;

                        });
                        temp['pid']=row.pid;
                        claimsNew.push(temp);
                    });
                    console.log(claimsNew);
                    result.resolve(claimsNew);
                }
            })
            .error(function(){
                console.log('error reaching server');
                result.reject('sorry dude');
            });
        return result.promise;
    };
    var Sync= {
        getFreshClaims: function(){
            a=Claim.getClaims();
            requestClaims().then(function(data){
                b=data;
                var onlyInA = a.filter(function(current){
                    return b.filter(function(current_b){
                        return current_b['Claim Number'] == current['Claim Number']
                    }).length == 0
                });

                var onlyInB = b.filter(function(current){
                    return a.filter(function(current_a){
                        return current_a['Claim Number'] == current['Claim Number']
                    }).length == 0
                });
                differ.resolve(onlyInA.concat(onlyInB));
            });
            return differ.promise;
        }
    };
    return Sync;
}]);


//            var onlyInA = a.filter(function(current){
//                return b.filter(function(current_b){
//                    return current_b.value == current.value && current_b.display == current.display
//                }).length == 0
//            });
//
//            var onlyInB = b.filter(function(current){
//                return a.filter(function(current_a){
//                    return current_a.value == current.value && current_a.display == current.display
//                }).length == 0
//            });
