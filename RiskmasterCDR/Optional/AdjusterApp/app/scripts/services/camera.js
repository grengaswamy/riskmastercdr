﻿'use strict';
app.factory('CameraService',['$window',function($window) {
    var hasUserMedia = function() {
        return !!getUserMedia();
    }

    var getUserMedia = function() {
        navigator.getUserMedia = ($window.navigator.getUserMedia ||
            $window.navigator.webkitGetUserMedia ||
            $window.navigator.mozGetUserMedia ||
            $window.navigator.msGetUserMedia);
        return navigator.getUserMedia;
    }

    return {
        hasUserMedia: hasUserMedia(),
        getUserMedia: getUserMedia()
    }


}])
