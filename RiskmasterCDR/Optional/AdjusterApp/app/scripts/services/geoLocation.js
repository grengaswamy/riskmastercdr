﻿'use strict';
app.factory('GeoLocation',[ function(){
    var GeoLocation = {
        getCodes : function(address){
            var geocoder=new google.maps.Geocoder();
            geocoder.geocode({'address':address}, function(results,status){
                if(status== google.maps.GeocoderStatus.OK){
                    return results[0].geometry.location;
                }
                else
                {
                    return "not found";
                }
            });
        }
    };

    return GeoLocation;

}]);