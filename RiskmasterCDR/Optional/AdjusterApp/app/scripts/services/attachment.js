'use strict';
app.factory('Attachment',['$http','Auth','BASE_URL','$q',function($http,Auth,BASE_URL,$q){
    var status='';
    var fileUploadPromise = $q.defer();
    var win = function (r) {
        status=true;
        fileUploadPromise.resolve(r);
        console.log("Code = " + r.responseCode);
        console.log("Response = " + r.response);
        console.log("Sent = " + r.bytesSent);
    }
    var fail = function (error) {
        status=false;
        fileUploadPromise.resolve(error);
        alert("An error has occurred: Code = " + error.code);
        console.log("upload error source " + error.source);
        console.log("upload error target " + error.target);
    }
    var Attachment = {
        requestClaimAttachment: function (attachment) {
            return  $http({
                method: 'POST',
                data : attachment,
                url: Auth.getServer() + BASE_URL + '/Attachment',
                headers: {'session': Auth.getSession(),'ClientId':Auth.getClientId(),'Content-Type': 'json'}

            });
        },
        fileUpload : function(fileURL,attachmentModel){
            var options = new FileUploadOptions();
            options.fileKey = "file";
            options.fileName = fileURL.substr(fileURL.lastIndexOf('/') + 1);
            options.mimeType = attachmentModel.type;
            var headers={'session': Auth.getSession(),'ClientId':Auth.getClientId()};
            var params = {
                FileName:'',Title:'', Subject:'', Notes:'', FileContents:'', Claimnumber:''
            };
            params.FileName=attachmentModel.fileName;
            params.Title=attachmentModel.title;
            params.Subject=attachmentModel.subject;
            params.Notes=attachmentModel.notes;
            params.Claimnumber=attachmentModel.claimNumber;
            options.params = params;
            options.headers=headers;
            var ft = new FileTransfer();
            ft.onprogress = function(progressEvent) {
                if (progressEvent.lengthComputable) {
                    loadingStatus.setPercentage(progressEvent.loaded / progressEvent.total);
                } else {
                    loadingStatus.increment();
                }
            };
            ft.upload(fileURL, encodeURI(Auth.getServer() + BASE_URL + '/fileUpload'), win, fail, options);
            return fileUploadPromise;
        },
        readFile : function(fileUrl){
          var reader = new FileReader();
          var readPromise=$q.defer();
            reader.readAsDataURL(fileUrl);
            reader.onloadend=function(){
                readPromise.resolve(reader.result);
            };
            return readPromise.promise;
        }
    };
    return Attachment;
}]);

