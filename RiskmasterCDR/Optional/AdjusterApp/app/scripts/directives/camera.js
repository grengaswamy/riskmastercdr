app.directive('camera',['CameraService',function(CameraService) {
    return {
        restrict: 'EA',
        replace: true,
        transclude: true,
        scope: {
            attachPic : "&"
        },
        controller: function($scope, $q, $timeout) {
            this.takeSnapshot = function() {
                var canvas  = document.querySelector('canvas'),
                    ctx     = canvas.getContext('2d'),
                    videoElement = document.querySelector('video'),
                    d       = $q.defer();

                canvas.width = $scope.w;
                canvas.height = $scope.h;

                $timeout(function() {
                    ctx.fillRect(0, 0, $scope.w, $scope.h);
                    ctx.drawImage(videoElement, 0, 0, $scope.w, $scope.h);
                    d.resolve(canvas.toDataURL());
                },0);
                return d.promise;
            }
        },
        template: '<div class="camera col-xs-12"> <video class="camera col-xs-12" autoplay="" /><br/> <div ng-transclude class="col-xs-12 text-center"></div> </div>',
        link: function($scope, element, attrs) {
            var w = attrs.width || 320,
                h = attrs.height || 200;


            // Check the availability of getUserMedia across supported browsers
            if (!CameraService.hasUserMedia) {
                onFailure({code:-1, msg: 'Browser does not support getUserMedia.'});
                return;
            }

            var userMedia = CameraService.getUserMedia;
            var  videoElement = document.querySelector('video');


            var onSuccess = function(stream) {
                if (navigator.mozGetUserMedia) { //sssssss
                    videoElement.mozSrcObject = stream;
                } else {
                    var vendorURL = window.URL || window.webkitURL;
                    videoElement.src = window.URL.createObjectURL(stream);
                }
                // Just to make sure it autoplays
                videoElement.play();

            }

            // If there is an error
            var onFailure = function(err) {
                console.error(err);
            }
            // Make the request for the media
            navigator.getUserMedia({
                video: true

            }, onSuccess, onFailure);

            $scope.w = w;
            $scope.h = h;
        }
    }
}]);
