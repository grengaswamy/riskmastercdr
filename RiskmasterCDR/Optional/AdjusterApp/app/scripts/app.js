/* global app:true */
/* exported app */
'use strict';
/**
 * @ngdoc overview
 * @name yoRiskyApp
 * @description
 * # yoRiskyApp
 *
 * Main module of the application.
 */
var app = angular.module('yoRiskyApp', [
  'ngAnimate',
  'ngCookies',
  'ngResource',
  'ngRoute',
  'ngSanitize',
  'ngTouch',
  'LocalStorageModule',
  'angular-loading-bar'
])
  .config(['localStorageServiceProvider', function (localStorageServiceProvider) {
    localStorageServiceProvider.setPrefix('csc.riskmaster.mobile');
  }])
  .config(['$routeProvider', '$httpProvider', '$locationProvider', 'CLIENT_ID',
    function ($routeProvider, $httpProvider, $locationProvider, CLIENT_ID) {
    $httpProvider.defaults.useXDomain = true;
    //$httpProvider.defaults.cache = true;
    $httpProvider.defaults.headers.common['ClientId'] = CLIENT_ID;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    $routeProvider
      .when('/', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl'
      })
      .when('/home', {
        templateUrl: 'views/home.html',
        controller: 'HomeCtrl'
      })
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl'
      })
      .when('/events', {
        templateUrl: 'views/events.html',
        controller: 'EventsCtrl'
      })
      .when('/events/:eventId', {
        templateUrl: 'views/event.html',
        controller: 'EventCtrl'
      })
      .when('/claims', {
        templateUrl: 'views/claims.html',
        controller: 'ClaimsCtrl'
      })
      .when('/claims/:claimId', {
        templateUrl: 'views/claim.html',
        controller: 'ClaimCtrl'
      })
      .when('/tasks', {
        templateUrl: 'views/tasks.html',
        controller: 'TasksCtrl'
      })
      .when('/feedback', {
        templateUrl: 'views/feedback.html',
        controller: 'FeedbackCtrl'
      })
      .when('/settings', {
        templateUrl: 'views/settings.html',
        controller: 'SettingsCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html'
      })
      .when('/peekUsers', {
        templateUrl: 'views/peekUsers.html',
        controller: 'PeekUsersCtrl'
      })
      .when('/peekDiaries/:user', {
        templateUrl: '/views/peekDiaries.html',
        controller: 'PeekDiariesCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  }])
  .config(['cfpLoadingBarProvider', function (cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeSpinner = false;
  }])
  .run(['$rootScope', function ($rootScope) {
    var height = $(window).height();
    $('.view').css('height', height);

    //Errors
    $rootScope.sending=false;
    $rootScope.attachError=false;
    $rootScope.noteError=false;
    $rootScope.taskError=false;
    $rootScope.attachError=false;
    $rootScope.server=false;
    $rootScope.attachSuccess=false;
    $rootScope.noteAdded=false;
    $rootScope.taskAdded=false;

  }])
  .constant('SERVER', 'http://ec2-54-186-161-57.us-west-2.compute.amazonaws.com')
  .constant('BASE_URL', '/RMService/Mobile')
  .constant('CLIENT_ID', '0');


