﻿
app.controller('EventsCtrl',['$scope','Auth','Event','$location','localStorageService',
    function($scope,Auth,Event,$location,localStorageService){
        'use strict';
        $scope.formattedEvents=Event.getEvents();
        $scope.loading=Event.getLoading();
        $scope.errorMessage=Event.getErrorMessage();
        $scope.error=Event.getError();

        Event.getTotalEvents().then(function(l){
            $scope.totalEvents=l;
            $scope.loading=Event.getLoading();
        } );

        $scope.left=function(){
            $location.path('/home');
        };
    }
]);
