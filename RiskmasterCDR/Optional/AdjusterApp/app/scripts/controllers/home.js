/**
 * Created by nsharma202 on 8/12/2014.
 */

app.controller('HomeCtrl', ['$scope', '$q', '$location', '$rootScope', 'Data', 'Auth', 'Event', 'Claim', 'Task',
  function ($scope, $q, $location, $rootScope, Data, Auth, Event, Claim, Task) {
    'use strict';

    if (Auth.getSession() == null) {
      $location.path('/login');
    }
    $rootScope.welcomeDiv = Data.getWelcomeStatus();
    $rootScope.homeDiv = Data.getHomeStatus();

//        Hide & Show Pinned Items
    $scope.toggleValue = true;
    $scope.toggle = function () {
      $scope.toggleValue = !$scope.toggleValue;
    };
    $scope.pins = Data.getPins();
    $scope.progressBarStatus = Data.getProgressBarStatus();

    $rootScope.sideMenu = false;
    $rootScope.username = Auth.getUsername();

    $rootScope.syncInProgress = false;
    $rootScope.syncFailed = false;
    $rootScope.syncSuccess = false;
    $rootScope.syncNewEntriesCount = 0;

    $rootScope.$watch('syncNewEntriesCount', function (count) {
      $rootScope.syncNewEntriesCount = count;
    });

    if ($scope.progressBarStatus == 0) {

      Data.setLastSyncTime();
      $scope.lastUpdated = Data.getLastSyncTime();

      Event.getTotalEvents().then(function (l) {
        $scope.totalEvents = l;
        $scope.eventsReady = Event.getLoading();
        $scope.progressBarStatus += 30;
        Data.setProgressBarStatus(30);
        console.log($scope.progressBarStatus);
      }, function () {
        $scope.totalEvents = 0;
        $scope.eventsReady = Event.getLoading();
        $scope.progressBarStatus += 30;
        console.log($scope.progressBarStatus);
        Data.setProgressBarStatus(30);
      });

      Claim.getTotalClaims().then(function (c) {
        $scope.totalClaims = c;
        $scope.claimsReady = Claim.getLoading();
        $scope.progressBarStatus += 30;
        Data.setProgressBarStatus(30);
        console.log($scope.progressBarStatus);
      }, function (failed) {
        $scope.totalClaims = 0;
        $scope.claimsReady = Claim.getLoading();
        $scope.progressBarStatus += 30;
        Data.setProgressBarStatus(30);
        console.log($scope.progressBarStatus);
      });

      Task.getTotalTasks().then(function (t) {
        $scope.totalTasks = t;
        $scope.tasksReady = Task.getLoading();
        $scope.progressBarStatus += 40;
        Data.setProgressBarStatus(40);
        console.log($scope.progressBarStatus);
      }, function (failed) {
        $scope.totalTasks = 0;
        $scope.tasksReady = Task.getLoading();
        $scope.progressBarStatus += 40;
        Data.setProgressBarStatus(40);
        console.log($scope.progressBarStatus);
      });

    }
    else {

      $scope.lastUpdated = Data.getLastSyncTime();
      $scope.totalEvents = Event.getTotalEventsStatic();
      $scope.totalClaims = Claim.getTotalClaimsStatic();
      $scope.totalTasks = Task.getUpdatedTaskCount();
      $scope.$watch(function () {
          return Task.getUpdatedTaskCount();
        },
        function (newValue) {
          $scope.totalTasks = newValue;
        });
    }

  }]);


