
app.controller('FeedbackCtrl',['$scope','$timeout', function($scope,$timeout){
    'use strict';
    $scope.user={'feedback':''};
    $scope.toggle='false';
    var email='nsharma202@csc.com,asharma329@csc.com';
    $scope.submit = function() {
        $scope.feedbackForm.submitted=false;

        if ($scope.feedbackForm.$valid) {
            sendIt();
            $scope.feedbackForm.$setPristine();
        } else {
            $scope.feedbackForm.submitted = true;
        }
    };
    var sendIt=function(){
        var subject='Riskmaster Adjuster App : User feedback';
        var link = "mailto:"+ email
            + "?subject=" + encodeURI(subject)
        + "&body=" + encodeURI($scope.user.feedback)+encodeURI("\n\n");
        window.location.href = link;
        $scope.toggle=true;
        $scope.user={'feedback':''};
        $timeout(toggleValue,5000,true);
    };
    var toggleValue=function(){
        $scope.toggle=false;
    };
    $scope.cancel=function(){
        $scope.user={'name':'', 'email':'','feedback':''};
    };
}]);
