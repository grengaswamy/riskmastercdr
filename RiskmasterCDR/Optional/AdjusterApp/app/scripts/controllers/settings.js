
app.controller('SettingsCtrl',['$scope','$location','$rootScope','Auth','Claim','Code','Data','Task','Event','$timeout',
    function($scope,$location,$rootScope,Auth,Claim,Code,Data,Task,Event,$timeout){
    'use strict';
    $scope.password={ currentPassword:'',newPassword:'',confirmPassword:''};
    $scope.passwordError=false;
    var resetPasswordError=function(){
      $scope.passwordError=false;
    };
    $scope.validate=function(){
        $scope.passwordForm.submitted=false;
        if ($scope.passwordForm.$valid) {
            if($scope.password.currentPassword===$scope.password.newPassword){
                $scope.passwordForm.same=true;
                $scope.passwordForm.submitted = true;
            }
            else if($scope.password.newPassword!==$scope.password.confirmPassword){
                $scope.passwordForm.mismatch=true;
                $scope.passwordForm.submitted = true;
            }
            else {
                $scope.passwordForm.$setPristine();
                changePassword();
            }
        } else {
            $scope.passwordForm.submitted = true;
        }
    };
    var changePassword=function(){

        $scope.error=false;
        $scope.errorMessage='';
        var user = { UserName: '', Password: '', NewPassword: '', DsnName : ''};
        user.UserName=Auth.getUsername();
        user.NewPassword=$scope.password.newPassword;
        user.Password=$scope.password.currentPassword;


        Auth.changePassword(user).then(function(){
            console.log("Your password is changed successfully.");
            reset();
        }, function(err){
            console.log(err);
            $scope.passwordError=true;
            $timeout(resetPasswordError,2000,true);
            $scope.errorMessage="something went wrong. Pls try again.";
            $scope.password={ currentPassword:'',newPassword:'',confirmPassword:''};

        });
    };

    $scope.changeDSN=function(){
      $location.path('/login');
    };

    var reset=function(){
        Auth.logout(Auth.getSession())
            .success(function(){
                console.log("bye bye");
                //clearing services
                Auth.reset();
                Claim.reset();
                Code.reset();
                Data.reset();
                Event.reset();
                Task.reset();

                $rootScope.welcomeDiv='true';
                $rootScope.homeDiv='false';
                $rootScope.sideMenu='false';
                $location.path('/');
            })
            .error(function(){
                console.log("error logging out");
            })
    };


}]);