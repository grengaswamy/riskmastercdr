﻿
app.controller('ClaimsCtrl',['$scope','Auth','Claim','$location',
    function($scope,Auth,Claim,$location) {
        'use strict';
        $scope.formattedClaims=Claim.getClaims();
        $scope.loading = Claim.getLoading();
        $scope.errorMessage=Claim.getErrorMessage();
        $scope.error=Claim.getError();

        Claim.getTotalClaims().then(function(c){
            $scope.totalClaims=c;
            $scope.loading=Claim.getLoading();
        });

        $scope.left=function(){
            $location.path('/home');
        };

    }
]);

