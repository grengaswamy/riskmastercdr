﻿app.controller('TasksCtrl',['$scope','Auth','Task','$location','Diary','Code','$timeout','$rootScope',
    function($scope,Auth,Task,$location,Diary,Code,$timeout,$rootScope) {
        'use strict';
        $scope.loading = Task.getLoading();
        $scope.errorMessage=Task.getErrorMessage();
        $scope.error=Task.getError();
        $scope.formattedTasks=Task.getTasks();
        $scope.formattedTasks.reverse();
        Task.getTotalTasks().then(function(t){
            $scope.totalTasks=t;
            $scope.loading=Task.getLoading();
        });

        $scope.left=function(){
            $location.path('/home');
        };

      //Add Task
      $scope.showMyTask = function(){
        $('#myTasks').modal('show');
        $('.modal-backdrop').removeClass('modal-backdrop');
      };
      $scope.toggle='false';
      var toggleValue=function(){
        $scope.toggle=false;
      };
      $rootScope.taskError='false';
      $scope.taskErrorMessage='';
      $rootScope.taskAdded='false';
      var taskAddedToggle=function(){
        $rootScope.taskAdded=false;
      };
      $scope.taskEntered='';
      $scope.task={ name:'', type: '', dueDate :'', dueTime : '', description : '', recordId:'', table:'', activity:''};
      $scope.taskTypes=Code.getWpaActivities();

      //Check for validation
      $scope.submitTask = function() {

        $scope.task_form.submitted=false;
        console.log($scope.task_form.$valid);
        if ($scope.task_form.$valid) {
          $('#myTasks').modal('hide');
          postTask();
          $scope.task_form.$setPristine();
        } else {
          $scope.task_form.submitted = true;
        }
      };
      //Post a task
      var postTask=function(){
        $rootScope.sending=true;
        $rootScope.taskError='false';
        $rootScope.taskAdded='false';
        $scope.task.type=$scope.taskEntered.Id;
        $scope.task.activity=$scope.taskEntered.Id+' | '+$scope.taskEntered.ShortCode+' '+$scope.taskEntered.Desc;
        console.log($scope.task);
        Diary.postDiary($scope.task)
          .success(function(data,status){
            $rootScope.sending=false;
            console.log(data);
            if(data.ResultMessage.MsgStatus.MsgStatusCd=='Error'){
              $rootScope.taskError='true';
              $scope.taskErrorMessage=data.ResultMessage.MsgStatus.ExtendedStatus;
              console.log('error posting task');
              console.log(data);
              console.log(status);
            }
            else {
              console.log('task added');
              $rootScope.taskAdded='true';
              $timeout(taskAddedToggle,5000,true);
              console.log(data);
              var temp={ 'taskSubject':'', 'entryNotes':'','workActivity':'','completeDate':'','completeTime':'', 'attachments':'','linked':'','type':''};
              temp.taskSubject=$scope.task.name;
              temp.entryNotes=$scope.task.description;
              temp.completeDate=$scope.task.dueDate;
              temp.completeTime=$scope.task.dueTime;
              temp.workActivity=$scope.task.activity;
              $scope.formattedTasks.push(temp);
              Task.addTask(temp);
              $scope.task={ name:'', type: '', dueDate :'', dueTime : '', description : '', recordId:'', table:'', activity:''};
            }
          })
          .error(function(data,status){
            console.log('error creating task');
            $rootScope.sending=false;
            $scope.message='error creating task';
            $rootScope.taskError=true;
            $scope.taskErrorMessage='error creating task';
            console.log(data);
            console.log(status);
          })
      };

      $scope.peek=function(){
        $location.path('/peekUsers');
      }
    }
]);
