
app.controller('SideMenuCtrl',['$scope','$rootScope','$location','Auth','Claim','Event','Task','Code','Data','$route',
    function($scope,$rootScope,$location,Auth,Claim,Event,Task,Code,Data,$route){
        'use strict';
        $scope.hideSideMenu=function(){
            $('body').removeClass('show-nav');
        };
        $rootScope.fullName=Auth.getFullName();
        $rootScope.right=function(){
            $rootScope.syncInProgress=false;
            $rootScope.syncFailed=false;
            $rootScope.syncSuccess=false;
        };

        $rootScope.reload=function() {
            $rootScope.syncInProgress=true;
            var lastSynced = Data.getLastSyncTime();
            //lastSynced = lastSynced.replace(/-|T|:/g, "").match(/^[0-9]{14}/g);
            console.log("last time stamp " + lastSynced);
            Task.sync(lastSynced)
                .then(function(length){
                    if(!isNaN(length)){
                        $rootScope.syncNewEntriesCount=length;
                    }
                    Data.setLastSyncTime();
                    $rootScope.syncInProgress=false;
                    $rootScope.syncSuccess=true;
                },function(error){
                    $rootScope.syncInProgress=false;
                    $rootScope.syncFailed=true;
                })  ;
        };
        $rootScope.fullReload=function(){
            var lastSynced=Data.getLastSyncTime();
            lastSynced=lastSynced.replace(/-|T|:/g,"").match(/^[0-9]{14}/g);
            console.log("last time stamp" +lastSynced);

            Claim.reset();
            Code.reset();
            Data.reset();
            Event.reset();
            Task.reset();
            var dsn=Auth.getCurrentDSN();
            $scope.dsnLoading='true';
            $scope.selectedDSN=dsn;

            console.log("setting up home");
            console.log("selected DSN  : "+$scope.selectedDSN.Key);
            $scope.user=Auth.getUser();
            console.log($scope.user);
            $scope.user.DsnName=$scope.selectedDSN.Key;
            $scope.user.UserName=Auth.getUsername();
            Auth.getUserSessionID($scope.user)
                .success(function(data){

                    Auth.setSessionID(data);
                    console.log("sessionDone going to /home");
                    console.log(Auth.getSession());
                    $scope.dsnLoading='false';
                    Data.setProgressBar();
                    console.log("progressBar "+Data.getProgressBarStatus());
                    Data.load();
                    Data.setWelcomeStatus(false);
                    Data.setHomeStatus(true);

                    $route.reload();
                    $location.path('/home');
                })
                .error(function(error){
                    $scope.dsnError='true';
                    $scope.message="error getting session";
                    console.log("error getting session");
                    console.log(error);
                    $scope.loginDiv=true;
                    $scope.homeDiv=false;
                    Data.setLoginStatus(true);
                    Data.setHomeStatus(false);
                });
        };
        $rootScope.logout= function(){
            $rootScope.welcomeDiv='true';
            $rootScope.homeDiv='false';
            $rootScope.sideMenu='false';
            Auth.setIsAuthenticated(null);
            $route.reload();
            $location.path('/');
            Auth.logout(Auth.getSession())
                .success(function(){
                    console.log("bye bye");
                    Auth.reset();
                    Claim.reset();
                    Code.reset();
                    Data.reset();
                    Event.reset();
                    Task.reset();
                })
                .error(function(){
                    console.log("error logging out");
                });
        };

    }]);
