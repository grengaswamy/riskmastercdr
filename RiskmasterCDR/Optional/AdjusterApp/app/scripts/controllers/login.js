
app.controller('LoginCtrl',['$scope','$route','$location','Auth','$rootScope','Data','Claim','Task','Code','Event','$filter','$timeout',
    function($scope,$route,$location,Auth,$rootScope,Data,Claim,Task,Code,Event,$filter,$timeout){
        'use strict';

        $scope.user = { UserName: '', Password: '', NewPassword: '', DsnName : ''};
        $scope.userDSNs=Auth.getUserDSNs();
        $scope.DSNs=[];
        $scope.selectedDSN='';
        $scope.loginMsg='';
        $scope.server={name:''};
        $scope.error='false';
        $scope.dsnError='false';
        $scope.message='';
        $scope.loginLoading='false';
        $scope.dsnLoading='false';
        $rootScope.sideMenu='false';
        $scope.login_form= {username:'',password:'',submitted:'false'};

//        Validation Check
        $scope.loginForm = function() {
            if ($scope.login_form.$valid) {
                // Submit as normal
              $scope.error='false';
              $scope.loginLoading='true';
              //$timeout(login,1500);
              login();
            } else {
                $scope.login_form.submitted = true;
            }
        };

        var login= function(){
            //$scope.error='false';
            //$scope.loginLoading='true';
            window.scrollTo(0,0);
            var filteredUrl=$filter('parseUrl')($scope.server.name);
            Auth.setServer(filteredUrl);
            console.log('server : '+Auth.getServer());
            Auth.login($scope.user)
                .success(function(login){
                    console.log('Auth:' +login);
                    if(login=='true'){
                        Auth.setUser($scope.user);
                        Auth.requestDSNs($scope.user)
                            .success(function(data){
                                $scope.error='false';
                                $scope.loginLoading='false';
                                console.log('getting DSNs');
                                console.log(data);
                                $scope.userDSNs=data;
                                Auth.setDSNs(data);
                                if(data.length=='1'){
                                    $scope.goo(data[0]);
                                }
                                Auth.setIsAuthenticated(login);
                            })
                            .error(function(error){
                                $scope.error='true';
                                $scope.loginLoading='false';
                                $scope.message='Error getting DSN';
                                console.log('DSN error');
                                console.log(error);
                            });
                    }
                    else
                    {
                        $scope.error='true';
                        $scope.message='Incorrect Username or Password. Please try again';
                        $scope.loginLoading='false';
                        console.log($scope.message);
                    }
                })
                .error(function(error){
                    $scope.error='true';
                    $scope.loginLoading='false';
                    $scope.message='Remote Server Not Reachable';
                    Auth.setServer('');
                    console.log('auth error');
                    console.log(error);
                });
        };

        $scope.goo=function(dsn){

            Claim.reset();
            Code.reset();
            Data.reset();
            Event.reset();
            Task.reset();

            $scope.dsnLoading='true';
            $scope.selectedDSN=dsn;
            Auth.setCurrentDSN(dsn);
            console.log('setting up home');
            console.log('selected DSN  : '+$scope.selectedDSN.Key);
            $scope.user=Auth.getUser();
            console.log($scope.user);
            $scope.user.DsnName=$scope.selectedDSN.Key;
            $scope.user.UserName=Auth.getUsername();


            //requesting full name of adjuster
            var tempUser=Auth.getUser();
            tempUser.DsnId=dsn.Value;
            console.log(tempUser);
            Auth.requestName(tempUser)
                .success(function(fullname){
                    console.log(fullname);
                    //removing quotes
                    fullname=fullname.replace(/^"(.*)"$/, '$1');
                    Auth.setFullName(fullname);
                    $rootScope.fullName=Auth.getFullName();
                })
                .error(function(){
                    console.log('full name failed');
                    Auth.setFullName($scope.user.UserName);
                });

            Auth.getUserSessionID($scope.user)
                .success(function(data){
                    Auth.setSessionID(data);
                    console.log('sessionDone going to /home');
                    console.log(Auth.getSession());
                    Data.setProgressBar();
                    console.log('progressBar '+Data.getProgressBarStatus());
                    Data.load();
                    Data.setWelcomeStatus(false);
                    Data.setHomeStatus(true);
                    $scope.dsnLoading='false';
                    $location.path('/home');
                })
                .error(function(error){
                    $scope.dsnLoading='false';
                    $scope.dsnError='true';
                    $scope.message='Error Getting Session';
                    console.log('Error Getting Session');
                    console.log(error);
                    $scope.loginDiv=true;
                    $scope.homeDiv=false;
                    Data.setLoginStatus(true);
                    Data.setHomeStatus(false);
                });
        };

    }]);
