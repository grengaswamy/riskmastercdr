/**
 * Created by nsharma202 on 12/18/2014.
 */
app.controller('PeekUsersCtrl',['$scope','Peek','$location',function($scope,Peek,$location){
  'use strict';

  $scope.loading=true;
  $scope.error=false;
  $scope.errorMessage='';
  $scope.users=[];

  Peek.requestUsers()
    .success(function(data){
      console.log(data);
      $scope.loading=false;
      if(data==="error"){
        $scope.error=true;
      }
      else{
        $scope.users=data;
      }
    })
    .error(function(data,status){
      if(status===404){
        $scope.errorMessage='You are running an older version of RMA backend. Pls update to 15.1 to get this functionality';
      }
      else {
        $scope.errorMessage='Please try again'
      }
      console.log(data);
      $scope.loading=false;
      $scope.error=true;
    });

  $scope.fetchDiaries=function(user){
    console.log(user);
    $location.path('/peekDiaries/'+user);
  };

  $scope.left=function(){
    $location.path('/tasks');
  };
}]);
