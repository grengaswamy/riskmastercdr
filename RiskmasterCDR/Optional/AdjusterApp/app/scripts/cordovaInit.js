'use strict';
var CordovaInit = function () {

  var onDeviceReady = function () {
    receivedEvent('deviceready');
    document.addEventListener('backbutton', onBackKeyDown, true);
    document.addEventListener("offline", youAreOffline, false);
    window.onorientationchange = readDeviceOrientation;
  };

  var receivedEvent = function () {
    console.log('Start event received, bootstrapping application setup.');
    angular.bootstrap($('body'), ['yoRiskyApp']);
  };

  this.bindEvents = function () {
    document.addEventListener('deviceready', onDeviceReady, false);
  };

  //If cordova is present, wait for it to initialize, otherwise just try to
  //bootstrap the application.
  if (window.cordova !== undefined) {
    console.log('Cordova found, waiting for device.');
    this.bindEvents();
  } else {
    console.log('Cordova not found, booting application');
    receivedEvent('manual');
  }

  //event listeners
  var onBackKeyDown = function () {
    //doing nothing
  };
  var youAreOffline = function () {
   // alert("network status : Offline");
  };

  var readDeviceOrientation = function () {

    if (Math.abs(window.orientation) === 90) {
      // Landscape
      var height = $(window).height();
      $('.view').css('height', height);
    } else {
      // Portrait
      var height = $(window).height();
      $('.view').css('height', height);
    }
  };
};

$(function () {
  console.log('Bootstrapping!');
  new CordovaInit();
});
