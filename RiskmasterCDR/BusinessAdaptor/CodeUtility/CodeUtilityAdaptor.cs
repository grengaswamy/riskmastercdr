using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using System.IO;

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: CodeUtilityAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 22-Feb-2005
	///* $Author	: Nikhil Kr. Garg
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	///* Amendment  -->
	///  1.Date		: 7 Feb 06 
	///    Desc		: Replacing RMO_ACCESS use, and changing params of permission
	///				  violation exception so it can be correctly called. 
	///    Author	: Sumit
	/// <summary>	
	///	This class is used to call the application layer component for Code Utility which 
	///	Implements all the functionality pertaining to the Table Maintenance Module. 
	///	Returns data to be shown on the Table Maintenance Module in the form of XML string. 
	///	Also implements the functionality to save/edit new codes and tables.
	/// </summary>
	public class CodeUtilityAdaptor: BusinessAdaptorBase
	{	private const int RMB_CODES = 16000;
//		private const int RMO_ACCESS = 0;
//		private const int RMO_VIEW = 1;
//		private const int RMO_UPDATE = 2;
//		private const int RMO_CREATE = 3;
//		private const int RMO_DELETE = 4;
		private const int RMO_IMPORT = 5;
		#region Constructor
		/// <summary>
		/// Constructor for the class.
		/// </summary>
		public CodeUtilityAdaptor()
		{}
		#endregion

		#region Public Methods

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.CodeUtility.UtilityDriver.GetXMLData() method.
		///		Returns output data in the form of xml string.		
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<Document>
		///			<CodeUtility>
		///				<FormName>Form Name</FormName>
		///				<ID>Table id/ code id used either way</ID>
		///				<XML>Input xml</XML>
		///				<TypeCode>Code Type</TypeCode>
		///				<Alpha>Alphabet</Alpha>
		///			</CodeUtility>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results</param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		public bool GetXMLData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{


			Riskmaster.Application.CodeUtility.UtilityDriver objCodeUtility=null; //Application layer component
			string sFormName=string.Empty;		//Form Name passed in the input XML document
			int iID=0;							//TableId/CodeId passed in the input XML document
			string sXML=string.Empty;			//Input Xml passed in the input XML document
			string sTypeCode=string.Empty;		//Code Type passed in the input XML document
			char cAlpha='\x0000';				//Alphabet passed in the input XML document
			XmlElement objElement=null;			//used for parsing the input xml  
            int iCodeID = 0;
            int iLangCode = 0;
			try
			{
				//check existence of Form Name which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//FormName");
				if (objElement==null)
				{
                    p_objErrOut.Add("CodeUtilityAdaptorParameterMissing", Globalization.GetString("CodeUtilityAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				sFormName=objElement.InnerText;

				//check existence of TableId/CodeId which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ID");
				if (objElement==null)
				{
                    p_objErrOut.Add("CodeUtilityAdaptorParameterMissing", Globalization.GetString("CodeUtilityAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				if (!(string.Equals(objElement.InnerText.Trim(),string.Empty)))
					iID=Conversion.ConvertStrToInteger(objElement.InnerText);

				//check existence of Input Xml which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//XML");
				if (objElement==null)
				{
                    p_objErrOut.Add("CodeUtilityAdaptorParameterMissing", Globalization.GetString("CodeUtilityAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				sXML=objElement.InnerXml;

				//check existence of Code Type which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//TypeCode");
				if (objElement==null)
				{
                    p_objErrOut.Add("CodeUtilityAdaptorParameterMissing", Globalization.GetString("CodeUtilityAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				if (!(string.Equals(objElement.InnerText.Trim(),string.Empty)))
					sTypeCode=objElement.InnerText;

				//check existence of Alphabet which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//Alpha");
				if (objElement==null)
				{
                    p_objErrOut.Add("CodeUtilityAdaptorParameterMissing", Globalization.GetString("CodeUtilityAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				if (string.Equals(objElement.InnerText.Trim(),string.Empty))
					cAlpha=new char();
				else
					cAlpha=Convert.ToChar(objElement.InnerText);
				
                // Ash - cloud, client overload to support multi-tenant environment
				//objCodeUtility = new Riskmaster.Application.CodeUtility.UtilityDriver(connectionString);
                objCodeUtility = new Riskmaster.Application.CodeUtility.UtilityDriver(connectionString, base.ClientId);
				objCodeUtility.DBID = userLogin.DatabaseId.ToString();
                //abisht MITS 10932
                objCodeUtility.USERID = userLogin.UserId;
				// Nitesh 12/28/2005 Check security and fail if sufficient privilege doesn't exist
				if (sFormName=="frmeditcode") 
					if (!userLogin.IsAllowedEx(RMB_CODES, RMPermissions.RMO_VIEW ))
						throw new PermissionViolationException(RMPermissions.RMO_VIEW, RMB_CODES);
				if (sFormName=="frmaddcode" )
					if (!userLogin.IsAllowedEx(RMB_CODES, RMPermissions.RMO_CREATE))
						throw new PermissionViolationException(RMPermissions.RMO_CREATE, RMB_CODES);
                //Aman ML Change
                if (sFormName.Equals("frmaddnonbasecodedesc") || sFormName.Equals("frmeditnonbasecodedesc"))
                {
                    //check existence of Code ID which is required
                    objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//CodeID");
                    if (objElement == null)
                    {
                        p_objErrOut.Add("CodeUtilityAdaptorParameterMissing", Globalization.GetString("CodeUtilityAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                        return false;
                    }
                    if (!(string.Equals(objElement.InnerText.Trim(), string.Empty)))
                        iCodeID = Conversion.ConvertStrToInteger(objElement.InnerText);
                    objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//LangCode");
                    if (objElement == null)
                    {
                        p_objErrOut.Add("CodeUtilityAdaptorParameterMissing", Globalization.GetString("CodeUtilityAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                        return false;
                    }
                    if (!(string.Equals(objElement.InnerText.Trim(), string.Empty)))
                        iLangCode = Conversion.ConvertStrToInteger(objElement.InnerText);
                    objCodeUtility.LanguageCode = iLangCode;
                    objCodeUtility.CodeID = iCodeID;                  
                }
                p_objXmlOut = objCodeUtility.GetXMLData(sFormName, iID, sXML, sTypeCode, cAlpha);
                //Aman ML Change
				return true;
			}
			catch(XmlException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(DataModelException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("CodeUtilityAdaptor.GetXMLData.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objCodeUtility = null;
				objElement=null;
			}
		}        
		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.CodeUtility.UtilityDriver.SaveTable() method.
		///		Saves Glossary Table to the database
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<Document>
		///			<CodeUtility>
		///				<XmlDoc>Xml Input String</XmlDoc>
		///				<Mode>Edit / Add</Mode>
		///				<InputData>Input Xml String</InputData>
		///			</CodeUtility>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be
		///		<form name="" title="" sid="" topbuttons="" actionname="" bcolor="">
		///			<body1 req_func="" func="" />
		///			<group name="" title="">
		///				<error value="0" desc="" />
		///				<control lock="" name="" type="" title="" value="" isrequ="" />
		///			</group>
		///			<button linkto="" param="" type="" title="" name="" disable="" />
		///		</form>
		/// </param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		public bool SaveTable(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.CodeUtility.UtilityDriver objCodeUtility=null; //Application layer component
			string sXmlDoc=string.Empty;		//Xml Input String passed in the input XML document
			string sMode=string.Empty;			//Edit/Add Flag passed in the input XML document
			string sInputData=string.Empty;		//Input Xml String passed in the input XML document
			XmlElement objElement=null;			//used for parsing the input xml

			try 
			{
				//check existence of Xml Input String  which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//XmlDoc");
				if (objElement==null)
				{
                    p_objErrOut.Add("CodeUtilityAdaptorParameterMissing", Globalization.GetString("CodeUtilityAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				sXmlDoc=objElement.InnerXml;

				//check existence of Edit/Add Flag which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//Mode");
				if (objElement==null)
				{
                    p_objErrOut.Add("CodeUtilityAdaptorParameterMissing", Globalization.GetString("CodeUtilityAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				sMode=objElement.InnerText;

				//check existence of Input Xml String which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//InputData");
				if (objElement==null)
				{
                    p_objErrOut.Add("CodeUtilityAdaptorParameterMissing", Globalization.GetString("CodeUtilityAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				sInputData=objElement.InnerXml;

                // Ash - cloud, client overload to support multi-tenant environment
                //objCodeUtility = new Riskmaster.Application.CodeUtility.UtilityDriver(connectionString);
                objCodeUtility = new Riskmaster.Application.CodeUtility.UtilityDriver(connectionString, base.ClientId);
				objCodeUtility.DBID = userLogin.DatabaseId.ToString();
				//save table data
                //abisht MITS 10932
                objCodeUtility.USERID = userLogin.UserId;
				p_objXmlOut=objCodeUtility.SaveTable(sXmlDoc,sMode,sInputData, userLogin.DatabaseId);
				
				return true;

			}
			catch(XmlException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(DataModelException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("CodeUtilityAdaptor.SaveTable.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objCodeUtility = null;
				objElement=null;
			}
		}


		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.CodeUtility.UtilityDriver.SaveCode() method.
		///		Saves the code
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<Document>
		///			<CodeUtility>
		///				<XmlDoc>Xml Input String</XmlDoc>
		///				<Mode>Edit / Add</Mode>
		///				<InputData>Input Xml String</InputData>
		///			</CodeUtility>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be
		///		<Code>
		///			<IndStdCode /><LineOfBusCode /><CodeId /><TableId /><ShortCode /><CodeDesc />
		///			<RelatedCode /><LanguageCode /><TriggerDt /><StartDt /><EndDt /><OrgId />
		///		</Code>
		/// </param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool SaveCode(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			//Nitesh 12/28/2005 Check security and fail if sufficient privilege doesn't exist
			if (!userLogin.IsAllowedEx(RMB_CODES, RMPermissions.RMO_UPDATE))
				throw new PermissionViolationException(RMPermissions.RMO_UPDATE, RMB_CODES);

			Riskmaster.Application.CodeUtility.UtilityDriver objCodeUtility=null; //Application layer component
			string sMode=string.Empty;			//Edit/Add Flag passed in the input XML document
			string sInputData=string.Empty;		//Input Xml String passed in the input XML document
			XmlElement objElement=null;			//used for parsing the input xml
            int iLangCode = 0;  //Aman ML Change
			try 
			{
				//check existence of Edit/Add Flag which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//Mode");
				if (objElement==null)
				{
                    p_objErrOut.Add("CodeUtilityAdaptorParameterMissing", Globalization.GetString("CodeUtilityAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				sMode=objElement.InnerText;

				//check existence of Input Xml String which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//InputData");
				if (objElement==null)
				{
                    p_objErrOut.Add("CodeUtilityAdaptorParameterMissing", Globalization.GetString("CodeUtilityAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				sInputData=objElement.InnerXml;

                // Ash - cloud, client overload to support multi-tenant environment
                //objCodeUtility = new Riskmaster.Application.CodeUtility.UtilityDriver(connectionString);
                objCodeUtility = new Riskmaster.Application.CodeUtility.UtilityDriver(connectionString, base.ClientId);
				objCodeUtility.DBID = userLogin.DatabaseId.ToString();
                objCodeUtility.USERID = userLogin.UserId;  //Aman ML Change
				//save code data

                //zalam mits:-7295 04/04/2008 Start (Checks permisson for Delete)
                if (sMode == "delete")
                {

                    if (!userLogin.IsAllowedEx(RMB_CODES, RMPermissions.RMO_DELETE))
                        throw new PermissionViolationException(RMPermissions.RMO_DELETE, RMB_CODES);

                    objCodeUtility.SaveCode(sMode, sInputData, m_userLogin.LoginName);
                }
                    //Aman ML Change
                else if (sMode == "deleteNonBase")
                {
                    if (!userLogin.IsAllowedEx(RMB_CODES, RMPermissions.RMO_DELETE))
                        throw new PermissionViolationException(RMPermissions.RMO_DELETE, RMB_CODES);
                   
                    objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//LangCode");
                    if (objElement == null)
                    {
                        p_objErrOut.Add("CodeUtilityAdaptorParameterMissing", Globalization.GetString("CodeUtilityAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                        return false;
                    }
                    if (!(string.Equals(objElement.InnerText.Trim(), string.Empty)))
                        iLangCode = Conversion.ConvertStrToInteger(objElement.InnerText);
                    objCodeUtility.LanguageCode = iLangCode;
                    objCodeUtility.SaveCode(sMode, sInputData, m_userLogin.LoginName);
                }
                //Aman ML Change
                else
                {
                    objCodeUtility.SaveCode(sMode, sInputData, m_userLogin.LoginName);
                }
                //zalam mits:-7295 04/04/2008 End
				
				return true;

			}
			catch(XmlException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(DataModelException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("CodeUtilityAdaptor.SaveCode.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objCodeUtility = null;
				objElement=null;
			}
		}


		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.CodeUtility.UtilityDriver.ImportDataFromTextFile() method.
		///		Check for the file format and upload if OK
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<Document>
		///			<CodeUtility>
		///				<Xml>String Xml input</Xml>
		///				<FileName>File name to be imported</FileName>
		///				<TypeCode>Code Type</TypeCode>
		///				<Action>Action to be taken</Action>
		///				<Edit>Add/Edit mode</Edit>
		///			</CodeUtility>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be
		///		<import>
		///			<msg level="" desc="" id="" isedit="" filename="">
		///				<info id="" name="" value="" />
		///			</msg>
		///		</import>
		/// </param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool ImportDataFromTextFile(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			//Nitesh 12/28/2005 Check security and fail if sufficient privilege doesn't exist
			if (!userLogin.IsAllowedEx(RMB_CODES, RMO_IMPORT))
				throw new PermissionViolationException(RMPermissions.RMO_IMPORT, RMB_CODES);

			Random objRandom=new Random();
			int iRandom=objRandom.Next();
			Riskmaster.Application.CodeUtility.UtilityDriver objCodeUtility=null; //Application layer component
			string sXml=string.Empty;		//String Xml input passed in the input XML document
			string sFileContent=string.Empty;
			string sFilePath = string.Empty;
			string sFileName = string.Empty;
			//File name to be imported passed in the input XML document
			int iTypeCode=0;
			int iTableId=0;//Code Type passed in the input XML document
			int iAction=0;					//Action to be taken passed in the input XML document
			bool bEdit=false;				//Add/Edit mode passed in the input XML document
			XmlElement objElement=null;		//used for parsing the input xml
			bool bFileTransfer = false;
			try
			{
				//check existence of String Xml input which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//Xml");
				if (objElement==null)
				{
                    p_objErrOut.Add("CodeUtilityAdaptorParameterMissing", Globalization.GetString("CodeUtilityAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				sXml=objElement.InnerXml;

				//check existence of File Name which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//FileName");
				if (objElement==null)
				{
                    p_objErrOut.Add("CodeUtilityAdaptorParameterMissing", Globalization.GetString("CodeUtilityAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				sFileContent=objElement.InnerText;

                sFilePath = RMConfigurator.CombineFilePath(RMConfigurator.TempPath, "CodeUtility");
				DirectoryInfo sDir = new DirectoryInfo(sFilePath);
				if(!sDir.Exists)
					sDir.Create();
                sFileName = String.Format("{0}\\Codes_{1}.txt", sFilePath, iRandom);
                bFileTransfer = Utilities.FileTranfer(sFileContent, sFileName, base.ClientId);
				if(!bFileTransfer)
					return false;

				//check existence of Code Type which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//TypeCode");
				if (objElement==null)
				{
                    p_objErrOut.Add("CodeUtilityAdaptorParameterMissing", Globalization.GetString("CodeUtilityAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				iTypeCode=Conversion.ConvertStrToInteger(objElement.InnerText);
//Code commented by Nitesh:02Jan2006 Starts
//				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//TableId");
//				if (objElement==null)
//				{
//					p_objErrOut.Add("CodeUtilityAdaptorParameterMissing",Globalization.GetString("CodeUtilityAdaptorParameterMissing"),BusinessAdaptorErrorType.Error);
//					return false;
//				}
//
//				iTableId=Conversion.ConvertStrToInteger(objElement.InnerText);
//Code commented by Nitesh:02Jan2006 Ends
				//check existence of Action which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//Action");
				if (objElement==null)
				{
                    p_objErrOut.Add("CodeUtilityAdaptorParameterMissing", Globalization.GetString("CodeUtilityAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				iAction=Conversion.ConvertStrToInteger(objElement.InnerText);

				//check existence of Add/Edit mode which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//Edit");
				if (objElement==null)
				{
                    p_objErrOut.Add("CodeUtilityAdaptorParameterMissing", Globalization.GetString("CodeUtilityAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				bEdit=Conversion.ConvertStrToBool(objElement.InnerText);

                // Ash - cloud, client overload to support multi-tenant environment
                //objCodeUtility = new Riskmaster.Application.CodeUtility.UtilityDriver(connectionString);
                objCodeUtility = new Riskmaster.Application.CodeUtility.UtilityDriver(connectionString, base.ClientId);
				objCodeUtility.DBID = userLogin.DatabaseId.ToString();
				//check and import data
				p_objXmlOut=objCodeUtility.ImportDataFromTextFile(sXml,sFileName,iTypeCode,iAction,bEdit,iTableId);
				FileInfo objFile = new FileInfo(sFileName);
				objFile.Delete();
				
				return true;
			}
			catch(XmlException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(DataModelException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("CodeUtilityAdaptor.ImportDataFromTextFile.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objCodeUtility = null;
				objElement=null;
			}
		}


		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.CodeUtility.UtilityDriver.GetCodesXML() method.
		///		Gets the xml representation of Codes data
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<Document>
		///			<CodeUtility>
		///				<Table>Table Id</Table>
		///				<XML>Input Xml String</XML>
		///				<TypeCode>Code Type</TypeCode>
		///				<Alpha>Alphabet</Alpha>
		///			</CodeUtility>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be
		///		<form name="" title="" sid="" topbuttons="" supp="" bcolor="">
		///			<body1 req_func="" func="" />
		///			<group name="" title="">
		///				<control name="" islink="" isfunc="" col1="" col2="" col3="" col4="" col5="" col6="" type="" linkpage="" title="">
		///					<option id="" code="" Description="" Parent="" Organization="" />
		///				</control>
		///			</group>
		///			<button linkto="" param="" title="" type="" name="" />
		///		</form>
		/// </param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool GetCodesXML(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{	

			Riskmaster.Application.CodeUtility.UtilityDriver objCodeUtility=null; //Application layer component
			int iTable=0;						//Table Id passed in the input XML document
			string sXML=string.Empty;			//Input Xml String passed in the input XML document
			string sTypeCode=string.Empty;		//Code Type passed in the input XML document
			char cAlpha='\x0000';				//Alphabet passed in the input XML document
			XmlElement objElement=null;			//used for parsing the input xml

			try
			{
				//check existence of Table Id which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//Table");
				if (objElement==null)
				{
                    p_objErrOut.Add("CodeUtilityAdaptorParameterMissing", Globalization.GetString("CodeUtilityAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				if (!(String.Equals(objElement.InnerText.Trim(),string.Empty)))
					iTable=Conversion.ConvertStrToInteger(objElement.InnerText);

				//check existence of Input Xml String which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//XML");
				if (objElement==null)
				{
                    p_objErrOut.Add("CodeUtilityAdaptorParameterMissing", Globalization.GetString("CodeUtilityAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				sXML=objElement.InnerXml;

				//check existence of Code Type which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//TypeCode");
				if (objElement==null)
				{
					p_objErrOut.Add("CodeUtilityAdaptorParameterMissing",Globalization.GetString("CodeUtilityAdaptorParameterMissing",base.ClientId),BusinessAdaptorErrorType.Error);
					return false;
				}
				if (!(string.Equals(objElement.InnerText.Trim(),string.Empty)))
					sTypeCode=objElement.InnerText;

				//check existence of Alphabet which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//Alpha");
				if (objElement==null)
				{
					p_objErrOut.Add("CodeUtilityAdaptorParameterMissing",Globalization.GetString("CodeUtilityAdaptorParameterMissing",base.ClientId),BusinessAdaptorErrorType.Error);
					return false;
				}
				if (string.Equals(objElement.InnerText.Trim(),string.Empty))
					cAlpha=new char();
				else
					cAlpha=Convert.ToChar(objElement.InnerText);

                // Ash - cloud, client overload to support multi-tenant environment
                //objCodeUtility = new Riskmaster.Application.CodeUtility.UtilityDriver(connectionString);
                objCodeUtility = new Riskmaster.Application.CodeUtility.UtilityDriver(connectionString, base.ClientId);
				objCodeUtility.DBID = userLogin.DatabaseId.ToString();
                //abisht MITS 10932
                objCodeUtility.USERID = userLogin.UserId;
				//get xml representation of codes
				p_objXmlOut=objCodeUtility.GetCodesXML(iTable,sXML,sTypeCode,cAlpha);
				
				return true;
			}
			catch(XmlException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(DataModelException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("CodeUtilityAdaptor.GetCodesXML.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objCodeUtility = null;
				objElement=null;
			}
		}
        public bool GetNonBaseCodesXML(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            Riskmaster.Application.CodeUtility.UtilityDriver objCodeUtility = null; //Application layer component
            int iTable = 0;						//Table Id passed in the input XML document
            string sXML = string.Empty;			//Input Xml String passed in the input XML document
            string sTypeCode = string.Empty;		//Code Type passed in the input XML document
            char cAlpha = '\x0000';				//Alphabet passed in the input XML document
            XmlElement objElement = null;			//used for parsing the input xml
            int iCodeID = 0;
            try
            {
                //check existence of Table Id which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//Table");
                if (objElement == null)
                {
                    p_objErrOut.Add("CodeUtilityAdaptorParameterMissing", Globalization.GetString("CodeUtilityAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                if (!(String.Equals(objElement.InnerText.Trim(), string.Empty)))
                    iTable = Conversion.ConvertStrToInteger(objElement.InnerText);

                //check existence of Input Xml String which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//XML");
                if (objElement == null)
                {
                    p_objErrOut.Add("CodeUtilityAdaptorParameterMissing", Globalization.GetString("CodeUtilityAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                sXML = objElement.InnerXml;

                //check existence of Code Type which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//TypeCode");
                if (objElement == null)
                {
                    p_objErrOut.Add("CodeUtilityAdaptorParameterMissing", Globalization.GetString("CodeUtilityAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                if (!(string.Equals(objElement.InnerText.Trim(), string.Empty)))
                    sTypeCode = objElement.InnerText;

                //check existence of Alphabet which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//Alpha");
                if (objElement == null)
                {
                    p_objErrOut.Add("CodeUtilityAdaptorParameterMissing", Globalization.GetString("CodeUtilityAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                if (string.Equals(objElement.InnerText.Trim(), string.Empty))
                    cAlpha = new char();
                else
                    cAlpha = Convert.ToChar(objElement.InnerText);

                //Aman ML Change
                //check existence of Code ID which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//CodeID");
                if (objElement == null)
                {
                    p_objErrOut.Add("CodeUtilityAdaptorParameterMissing", Globalization.GetString("CodeUtilityAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                if (!(string.Equals(objElement.InnerText.Trim(), string.Empty)))
                  iCodeID = Conversion.ConvertStrToInteger(objElement.InnerText);
                //Aman ML Change

                // Ash - cloud, client overload to support multi-tenant environment
                //objCodeUtility = new Riskmaster.Application.CodeUtility.UtilityDriver(connectionString);
                objCodeUtility = new Riskmaster.Application.CodeUtility.UtilityDriver(connectionString, base.ClientId);
                objCodeUtility.DBID = userLogin.DatabaseId.ToString();
                //abisht MITS 10932
                objCodeUtility.USERID = userLogin.UserId;
                objCodeUtility.CodeID = iCodeID;  //Aman ML Change
                //get xml representation of codes
                p_objXmlOut = objCodeUtility.GetNonBaseCodesXML(iCodeID, sXML);

                return true;
            }
            catch (XmlException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (DataModelException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("CodeUtilityAdaptor.GetCodesXML.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objCodeUtility = null;
                objElement = null;
            }
        }
		//Changed by Gagan for MITS 8599 : Start

		/// <summary>
		/// Print codes Table
		/// </summary>
		/// <param name="p_objXmlIn">Request Xml</param>
		/// <param name="p_objXmlOut">Response Xml</param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>
		/// <returns>True/False for success or failure of the function</returns>
		public bool PrintCodesXML(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.CodeUtility.UtilityDriver objCodeUtility=null; //Application layer component
            //int iTable=0;						//Table Id passed in the input XML document			
            //string sTypeCode=string.Empty;		//Code Type passed in the input XML document
            //char cAlpha='\x0000';				//Alphabet passed in the input XML document
			XmlElement objElement=null;
			MemoryStream objMemory=null;			
			XmlElement objTempElement = null;

            try
			{
                //pmittal5 MITS 12685 08/07/08  - Input XML changed to the CodesXML of the table, obtained while opening the codes list. 
                
                //objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//TableId");
                //if (objElement==null)
                //{
                //    p_objErrOut.Add("CodeUtilityAdaptorParameterMissing",Globalization.GetString("CodeUtilityAdaptorParameterMissing"),BusinessAdaptorErrorType.Error);
                //    return false;
                //}
                //if (!(String.Equals(objElement.InnerText.Trim(),string.Empty)))
                //    iTable=Conversion.ConvertStrToInteger(objElement.InnerText);				

				//check existence of Code Type which is required
                //objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//TypeCode");
                //if (objElement==null)
                //{
                //    p_objErrOut.Add("CodeUtilityAdaptorParameterMissing",Globalization.GetString("CodeUtilityAdaptorParameterMissing"),BusinessAdaptorErrorType.Error);
                //    return false;
                //}
                //if (!(string.Equals(objElement.InnerText.Trim(),string.Empty)))
                //    sTypeCode=objElement.InnerText;

				//check existence of Alphabet which is required
                //objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//Alpha");
                //if (objElement==null)
                //{
                //    p_objErrOut.Add("CodeUtilityAdaptorParameterMissing",Globalization.GetString("CodeUtilityAdaptorParameterMissing"),BusinessAdaptorErrorType.Error);
                //    return false;
                //}
                //if (string.Equals(objElement.InnerText.Trim(),string.Empty))
                //    cAlpha=new char();
                //else
                //    cAlpha=Convert.ToChar(objElement.InnerText);

                // Ash - cloud, client overload to support multi-tenant environment
                //objCodeUtility = new Riskmaster.Application.CodeUtility.UtilityDriver(connectionString);
                objCodeUtility = new Riskmaster.Application.CodeUtility.UtilityDriver(connectionString, base.ClientId);
			    //objCodeUtility.CreateCodeListReport(iTable, sTypeCode,cAlpha, out objMemory);				
                objCodeUtility.CreateCodeListReport(p_objXmlIn, out objMemory);
				
                //End - pmittal5
				
				objTempElement=p_objXmlOut.CreateElement("PrintCodeList");
				p_objXmlOut.AppendChild(objTempElement);
				objTempElement=p_objXmlOut.CreateElement("File");				
				objTempElement.InnerText=Convert.ToBase64String(objMemory.ToArray());
				p_objXmlOut.FirstChild.AppendChild(objTempElement);		
                
				return true;
			}
			catch(XmlException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(DataModelException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("CodeUtilityAdaptor.PrintCodesXML.Error",base.ClientId),BusinessAdaptorErrorType.Error);				
				return false;
			}
			finally
			{
				 objCodeUtility=null;
                objElement = null;
                objTempElement = null;
                if(objMemory != null)
				     objMemory.Dispose();
			}		
		}

		//Changed by Gagan for MITS 8599 : End



		#endregion

	}
}
