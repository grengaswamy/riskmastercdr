﻿using System;
using System.Xml;
using System.Text;
using System.Collections;

using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.BRS;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: BRSAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 22-Feb-2005
	///* $Author	: Navneet Sota
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	A class representing the Billing Review System adaptor. It exposes various methods for setting
	/// and modifying the Billing information. 
	/// </summary>
	public class BRSAdaptor: BusinessAdaptorBase
	{
		#region Variable Declarations	

		/// <summary>
		/// Represents the Module variable for BRS Manager Class.
		/// </summary>
		private Manager m_objManager = null;

		# endregion

		#region Default Constructor
		/// Name		: BRSAdapter
		/// Author		: Navneet Sota
		/// Date Created: 2/22/2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>
		/// This is the default constructor.
		/// </summary>
		public BRSAdaptor()
		{}		
		#endregion

		#region Public Methods
        /// <summary>
        /// Retrieve template BRS Xml from the BRSSplitFile folder specified in the Riskmaster.config file.
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool FetchBRSSplitXml(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            XmlDocument objtempStandardInstance = new XmlDocument();

            try
            {
                p_objErrOut.Clear();
                p_objXmlOut = new XmlDocument();
                p_objXmlOut.Load(RMConfigurator.CombineFilePath(RMConfigurator.AppFilesPath, @"BRS\instance.xml"));
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("BRSAdaptor.FetchBRSSplitXml.Error", base.ClientId),//rkaur27
                    BusinessAdaptorErrorType.Error);
                p_objXmlOut = p_objXmlIn;
                return true;
            }

            return true;
        }


        /// <summary>
        /// Entry point for BRS functions
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
		public bool BRSEntrypoint(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			BillItem objBill=null;
            bool bIsBRSNavigated = false;

			try
			{
                int iStep = Conversion.ConvertObjToInt(p_objXmlIn.SelectSingleNode("//step").InnerText, base.ClientId);
				if(iStep==-1)
				{
					iStep = 0;
					p_objXmlIn.SelectSingleNode("//step").InnerText = "0";

				}
				p_objErrOut.Clear();
				/*
				 * Start->Following lines for navigating in/out of BRS Wizard
				 * */

                bIsBRSNavigated=Navigate(ref p_objXmlIn,ref p_objErrOut);
				/*
				 * End->Following lines for navigating in/out of BRS Wizard
				 * */

 
				if(!bIsBRSNavigated)
				{
                    InitializeBRS();
					
					/*
					 * Assign the error collection.Will also contain the alerts/warnings.
					 * 
					 * */
					m_objManager.Common.Errors=p_objErrOut;
					objBill=new BillItem(m_objManager.Common,base.ClientId);
			
				    switch(iStep)
				    {
					    case (int)Riskmaster.Application.BRS.Common.eSteps.FeeSchedule :
					    {
						    m_objManager.FillBRSDOM(ref p_objXmlIn);
						    break;
					    }
					    case (int)Riskmaster.Application.BRS.Common.eSteps.LoadBRSXML :
					    {
                            //abisht Checking for .mdb file in the Folder.
                            //If the file is notpresent then giving file not present error
                            //also not going to the next step.
                            bool bFileExist = m_objManager.CheckMDBFile(ref p_objXmlIn);
                            if (bFileExist == false)
                                ((XmlElement)p_objXmlIn.SelectSingleNode("//step")).InnerText = "1";

						    m_objManager.FillBRSItem(ref p_objXmlIn,ref objBill);
						    m_objManager.FillBRSDOM(ref p_objXmlIn,ref objBill);

                            if (bFileExist == true)
                            {
                                ((XmlElement)p_objXmlIn.SelectSingleNode("//selectedtab")).InnerText = "Calculate";
                            }

						    break;
					    }
					    case (int)Riskmaster.Application.BRS.Common.eSteps.Calculate :
					    {
						    m_objManager.WebReviewBill(ref p_objXmlIn);
						    break;
					    }
				    }
				}
				p_objXmlOut=p_objXmlIn;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				p_objXmlOut=p_objXmlIn;
				return true;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("BRSAdaptor.BRSEntrypoint.Error", base.ClientId),
					BusinessAdaptorErrorType.Error);
				p_objXmlOut=p_objXmlIn;
				return true;
			}
			finally
			{
				if(m_objManager!=null)
					m_objManager.Dispose();
                if (objBill != null)
                    objBill.Dispose();
			}
             return true;
		}

		private bool Navigate(ref XmlDocument p_objIncomingBRSDoc,ref BusinessAdaptorErrors p_objErrs)
		{
			BillItem objBill=null;

            Hashtable objHashtable = null;
			bool bBRSNavigated=true;
            const int FLHOSP = 20; 
            int iFeeSched = 0;
            string sBillItem = "";

			try
			{
                objHashtable = new Hashtable();
				string sCommand=p_objIncomingBRSDoc.SelectSingleNode("//cmd").InnerText.Trim().ToLower();
				switch(sCommand)
				{
					case "complete":
					{
						InitializeBRS();
						m_objManager.Common.Errors=p_objErrs;
						objBill=new BillItem(m_objManager.Common,base.ClientId);
						//get the CalcType  and apply if else

                        iFeeSched = Conversion.ConvertObjToInt(p_objIncomingBRSDoc.SelectSingleNode("//feesched").Attributes["codeid"].Value, base.ClientId);
                        int iCalcType  = Conversion.ConvertStrToInteger(m_objManager.Common.GetSingleString("CALC_TYPE", "FEE_TABLES", "TABLE_ID=" + iFeeSched));
                        if (iCalcType == FLHOSP)
                        {
                            //put all the items into the hashtable
                            XmlNodeList oItemList = p_objIncomingBRSDoc.SelectNodes("/FundsBRSSplits/option");
                            int idx = 1;
                            foreach (XmlNode oItem in oItemList)
                            {
                                objHashtable.Add(idx, oItem.OuterXml);
                                idx++;
                            }

                            int iFLType = m_objManager.GetFLType(objHashtable);

                            XmlDocument oItemDoc = new XmlDocument();
                            StringBuilder sbXML = new StringBuilder();
                            for (int iCount = 1; iCount <= objHashtable.Count; iCount++)
                            {
                                sBillItem = objHashtable[iCount].ToString();
                                oItemDoc.LoadXml(sBillItem);
                                oItemDoc.SelectSingleNode("//step").InnerText = "3";
                                m_objManager.WebReviewBill(ref oItemDoc, iFLType, iCount, objHashtable);

                                sBillItem = oItemDoc.OuterXml;
                                sbXML.Append(sBillItem);
                            }

                            //Replace xml with the modified in the hashtable
                            XmlNode oFundsBRSSplits = p_objIncomingBRSDoc.SelectSingleNode("/FundsBRSSplits");
                            oFundsBRSSplits.InnerXml = sbXML.ToString();

                            bBRSNavigated = true;

                        }
                        else
                        {
                             bBRSNavigated = false;
                        }
						break;
					}
					default:
					{
						//
						// TO DO-> throw error back to client.
						//
                        bBRSNavigated = false;
						break;
					}
				}
			}
			catch(Exception p_objErr)
			{
				throw new RMAppException(p_objErr.Message, p_objErr);
			}
			finally
			{
				if(m_objManager!=null)
				m_objManager.Dispose();
                objHashtable = null;
                if (objBill != null)
                    objBill.Dispose();
			}
			return bBRSNavigated;
		}
		

		#region public GetBillingCodes()
		/// Name		: GetBillingCodes
		/// Author		: Tanuj Narula
		/// Date Created: 02/22/2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>
		///  This function is a wrapper over Riskmaster.Application.BRS.Manager.FetchKeepOnNextList() function 
		///  This method is basically used for fetching the KeepOnNextList constant 
		///  value from database.
		/// </summary>
		/// <param name="p_objXmlIn">Input xml document for this function.
		/// Following is the structure for input xml-:
		///		<Document>
		///			<FetchKeepOnNextList/>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Xml document containing the output xml.
		///	Output structure is as follows-:
		///		<FetchKeepOnNextList>
		///			<KeepOnNext> KeepOnNext Value </KeepOnNext>
		///		</FetchKeepOnNextList>
		///	</param>
		/// <param name="p_objErrOut">This will contain the errors occured during the processing of this function.</param>
		/// <returns>'True' if function call succeeded or 'false' if it fails.</returns>
		public bool GetBillingCodes(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			//Element used for parsing the input xml
			Riskmaster.Application.BRS.Common objCommon=new Riskmaster.Application.BRS.Common(base.ClientId);

			try
			{   
				objCommon.FillBillingCodeDom(ref p_objXmlIn,base.userLogin.objRiskmasterDatabase.ConnectionString);
				p_objXmlOut=p_objXmlIn;
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("BRSAdaptor.GetBillingCodes.Error", base.ClientId),
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{				
			}
		}//end GetBillingCodes
		#endregion	
		#endregion		
		#region Private Methods
		/// Name		: InitializeBRS
		/// Author		: Navneet Sota
		/// Date Created: 02/22/2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>
		/// This function will instantiate the application layer's BRS.Manager class.
		/// </summary>
		/// <returns>Instance of application layer's BRS.Manager class.</returns>
		private void InitializeBRS()
		{
			try
			{
				m_objManager = new Manager(userLogin, base.ClientId);
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
		}

        private void UpdateBillItem(ref XmlDocument p_objSessionBillItem, ref XmlDocument p_objCurrentInstance)
        {
            BRSRequestMapper objReqMapper = null;
            try
            {
                objReqMapper = new BRSRequestMapper(ref p_objCurrentInstance, ref p_objSessionBillItem,base.ClientId);
                objReqMapper.MapRequestInstanceToStandardBrsInstance();

            }
            catch (Exception p_objErr)
            {
                throw p_objErr;
            }
        }


		#endregion
		#region Destructor
		~BRSAdaptor()
		{
			m_objManager = null;
		}
		# endregion

	}//end class
}//end Namespace
