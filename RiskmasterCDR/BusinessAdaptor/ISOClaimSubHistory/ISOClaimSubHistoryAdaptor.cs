using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.Application.ISOClaimSubHistory;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using System.IO;

namespace Riskmaster.BusinessAdaptor
{

    /**************************************************************
     * $File		: ISOClaimSubHistoryAdaptor.cs
     * $Revision	: 1.0.0.0
     * $Date		: 15/02/2010
     * $Author		: Rahul Aggarwal
    **************************************************************/

    /// <summary>
    ///	This class is used to call the application layer component for ISO Claim Submission History which 
    ///	contains methods to get ISO Claim Submission History and save ISO Claim Submission Settings.
	/// </summary>
	public class ISOClaimSubHistoryAdaptor: BusinessAdaptorBase
	{
        public ISOClaimSubHistoryAdaptor()
		{
		}

		#region Public Methods

        public bool GetISOClaimSubHistory(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
            ISOClaimSubHistory objISOClaimSubHis = null;
			try
			{
                objISOClaimSubHis = new ISOClaimSubHistory(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password,base.ClientId);//dvatsa-cloud
                p_objXmlOut = objISOClaimSubHis.GetISOClaimSubHistory(p_objXmlIn);
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("ISOClaimSubHistoryAdaptor.GetISOClaimSubHistory.Error",base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
                objISOClaimSubHis = null;
			}
			return true;
		}

        public bool SaveISOClaimSubSettings(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            ISOClaimSubHistory objISOClaimSubHis = null;
            try
            {
                objISOClaimSubHis = new ISOClaimSubHistory(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password,base.ClientId);//dvatsa-cloud
                p_objXmlOut = objISOClaimSubHis.SaveISOClaimSubSettings(p_objXmlIn);
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ISOClaimSubHistoryAdaptor.SaveISOClaimSubSettings.Error",base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-cloud
                return false;
            }
            finally
            {
                objISOClaimSubHis = null;
            }
            return true;
        }

		#endregion
	}
}
