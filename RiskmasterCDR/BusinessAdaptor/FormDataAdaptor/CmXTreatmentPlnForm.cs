using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for CmXTreatmentPlnForm.
	/// </summary>
	public class CmXTreatmentPlnForm : DataEntryFormBase
	{
		const string CLASS_NAME = "CmXTreatmentPln";
		private CmXTreatmentPln CmXTreatmentPln{get{return objData as CmXTreatmentPln;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		const string FILTER_KEY_NAME = "CmtpRowId";
		private ClaimAdjuster objCurrentAdjuster = null;
        private string sLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").Split('|')[0].ToString();
		private Claim objClaim = null;
		private int iClaimId = 0;
        //aaggarwal29 ;MITS 26215 start
        private CmXTreatmentPln objTreatmentPln { get { return objData as CmXTreatmentPln; } }
        //aaggarwal29; MITS 26215 end
		public override void InitNew()
		{
			base.InitNew(); 
			this.CmXTreatmentPln.CasemgtRowId = base.GetSysExDataNodeInt("/SysExData/CasemgtRowId");
		}

		public CmXTreatmentPlnForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
            sLangCode = Convert.ToString(fda.userLogin.objUser.NlsCode);
		}

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
			base.OnUpdateForm ();		

			int iSavedVisits=0;
			string sCurrentAdjuster = string.Empty;

			iSavedVisits=CmXTreatmentPln.NoReqVisits - CmXTreatmentPln.NoAppVisits;
			if (iSavedVisits<0) iSavedVisits=0;

			base.ResetSysExData("SavedVisits",iSavedVisits.ToString());

			Double dblDaysFromReqToApp=0;
			if (CmXTreatmentPln.DateRequested !=string.Empty && CmXTreatmentPln.ApprovedDate!=string.Empty && CmXTreatmentPln.ApprovedDate.CompareTo(CmXTreatmentPln.DateRequested)>0 )
			{
				DateTime dDateRequested=Conversion.ToDate(CmXTreatmentPln.DateRequested);
				DateTime dApprovedDate=Conversion.ToDate(CmXTreatmentPln.ApprovedDate);
				TimeSpan span=dApprovedDate-dDateRequested;
				dblDaysFromReqToApp=span.TotalDays;
			}
			base.ResetSysExData("DaysFromReqToApp",dblDaysFromReqToApp.ToString());

			Double dblDaysforReview=0;
			if (CmXTreatmentPln.DateReferral !=string.Empty && CmXTreatmentPln.DateReceived!=string.Empty && CmXTreatmentPln.DateReceived.CompareTo(CmXTreatmentPln.DateReferral)>0 )
			{
				DateTime dDateReferral=Conversion.ToDate(CmXTreatmentPln.DateReferral);
				DateTime dDateReceived=Conversion.ToDate(CmXTreatmentPln.DateReceived);
				TimeSpan span=dDateReceived-dDateReferral;
				dblDaysforReview=span.TotalDays;
			}
			base.ResetSysExData("DaysforReview",dblDaysforReview.ToString());
			
			if (CmXTreatmentPln.ProviderEid!=0)
				base.ResetSysExData("ProviderPhone",GetProviderPhone(CmXTreatmentPln.ProviderEid));
			else
				base.ResetSysExData("ProviderPhone","");

			
			iClaimId=base.GetSysExDataNodeInt("/SysExData/ClaimId",false);
			
			//TR Id 1669. Rahul 05/16/2006. Approver Eid must have default value of Current Aduster Eid.
			XmlNode objOld=null;
			XmlElement objNew=null;
			XmlCDataSection objCData=null;
			XmlDocument objXML = base.SysEx;

			objOld = objXML.SelectSingleNode("//ApprovedByEid");
			objNew = objXML.CreateElement("ApprovedByEid");
			
			if(CmXTreatmentPln.ApprovedByEid == 0)
			{
				objClaim = (Claim)objData.Context.Factory.GetDataModelObject("Claim", false);	
				objClaim.MoveTo(iClaimId);
				foreach(ClaimAdjuster objAdjuster in objClaim.AdjusterList)
				{
					if(objAdjuster.CurrentAdjFlag == true)
					{
						objCurrentAdjuster = objAdjuster;
						break;
					}
				}
				if(objCurrentAdjuster != null)
				{
					CmXTreatmentPln.ApprovedByEid = objCurrentAdjuster.AdjusterEid;
					sCurrentAdjuster = objCache.GetEntityLastFirstName(objCurrentAdjuster.AdjusterEid);
				}
				else
					CmXTreatmentPln.ApprovedByEid = 0;
				objCData = objXML.CreateCDataSection(sCurrentAdjuster);
				objNew.AppendChild(objCData);
				objNew.SetAttribute("codeid",CmXTreatmentPln.ApprovedByEid.ToString());
				
			}
			if(objOld !=null)
				objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);
			//Changes end of TRid 1669
			//for Displaying Back button on Entity Maintenance Screen
			base.ResetSysExData("DisplayBackButton","true");

            //aaggarwal29 : Treatment Plan Permissions not working properly; MITS 26215 start
            int iCasemgtRowId = CmXTreatmentPln.CasemgtRowId;
            CaseManagement oCaseManagement;
            Claim objClaimTemp;
            using (oCaseManagement = (CaseManagement)objTreatmentPln.Context.Factory.GetDataModelObject("CaseManagement", false))
            {
                oCaseManagement.MoveTo(iCasemgtRowId);
                objClaimTemp = (Claim)objTreatmentPln.Context.Factory.GetDataModelObject("Claim", false);
                objClaimTemp.MoveTo(oCaseManagement.ClaimId);
            }
            switch (objClaimTemp.LineOfBusCode)
            {
                case 243:
                    m_SecurityId = RMO_WC_TREATMENT_PLAN;
                    break;
                case 844:
                    m_SecurityId = RMO_DI_TREATMENT_PLAN;
                    break;
            }
            objClaimTemp.Dispose();
            //Updating Security Id in XML file
            XmlElement objSysSid = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysSid");
            objSysSid.InnerText = m_SecurityId.ToString();
            if (CmXTreatmentPln.CmtpRowId == 0)
            {
                if (!CmXTreatmentPln.Context.RMUser.IsAllowedEx(m_SecurityId, RMO_CREATE))
                {
                    LogSecurityError(RMO_CREATE);
                    return;
                }
            }
            else
            {
                if (!CmXTreatmentPln.Context.RMUser.IsAllowedEx(m_SecurityId, RMO_VIEW))
                {
                    LogSecurityError(RMO_VIEW);
                    return;
                }
            }
        //aaggarwal29 : MITS 26215 end
		}

		public override void OnUpdateObject()
		{
			//TR Id 1669. Rahul 05/16/2006. Approver Eid must have default value of Current Aduster Eid.
			objClaim = (Claim)objData.Context.Factory.GetDataModelObject("Claim", false);
			iClaimId=base.GetSysExDataNodeInt("/SysExData/ClaimId",false);
			objClaim.MoveTo(iClaimId);
			objCurrentAdjuster = null;
			if(CmXTreatmentPln.ApprovedByEid == 0)
			{
				foreach(ClaimAdjuster objAdjuster in objClaim.AdjusterList)
				{
					if(objAdjuster.CurrentAdjFlag == true)
					{
						objCurrentAdjuster = objAdjuster;
						break;
					}
				}
				if(objCurrentAdjuster != null)
					CmXTreatmentPln.ApprovedByEid = objCurrentAdjuster.AdjusterEid;
			}
			//Changes end of TRid 1669
		}

		//validate data according to the Business Rules
		public override void OnValidate(ref bool Cancel)
		{
			bool bError = false;
			
			//validation for authorization dates
			if (CmXTreatmentPln.DateAuthStart!=string.Empty && CmXTreatmentPln.DateAuthEnd !=string.Empty && 
				CmXTreatmentPln.DateAuthStart.CompareTo(CmXTreatmentPln.DateAuthEnd)>0)
			{
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId,sLangCode),
                    String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId, sLangCode), Globalization.GetString("Field.DateAuthEnd", base.ClientId, sLangCode), Globalization.GetString("Field.DateAuthStart", base.ClientId, sLangCode)),
					BusinessAdaptorErrorType.Error);

				bError = true;
			}

			//validation for Referred & Review Dates
			if (CmXTreatmentPln.DateReferral!=string.Empty && CmXTreatmentPln.DateReceived !=string.Empty && 
				CmXTreatmentPln.DateReferral.CompareTo(CmXTreatmentPln.DateReceived)>0)
			{
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                    String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId, sLangCode), Globalization.GetString("Field.DateReceived", base.ClientId, sLangCode), Globalization.GetString("Field.DateReferral", base.ClientId, sLangCode)),
					BusinessAdaptorErrorType.Error);

				bError = true;
			}

			//validation for Request & Approve Dates
			if (CmXTreatmentPln.DateRequested!=string.Empty && CmXTreatmentPln.ApprovedDate !=string.Empty && 
				CmXTreatmentPln.DateRequested.CompareTo(CmXTreatmentPln.ApprovedDate)>0)
			{
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                    String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId, sLangCode), Globalization.GetString("Field.ApprovedDate", base.ClientId, sLangCode), Globalization.GetString("Field.DateRequested", base.ClientId, sLangCode)),
					BusinessAdaptorErrorType.Error);

				bError = true;
			}

			// Return true if there were validation errors
			Cancel = bError;
		}

		private string GetProviderPhone(int iEntityId)
		{
			string sSQL=string.Empty;
			string sPhoneNo=string.Empty;

			sSQL = "SELECT PHONE1 FROM ENTITY WHERE ENTITY_ID = " + iEntityId.ToString();
			
			sPhoneNo = CmXTreatmentPln.Context.DbConnLookup.ExecuteString(sSQL);

			return sPhoneNo;
		}
	}
}
