﻿using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using Riskmaster.Settings;
using Riskmaster.Application.FundManagement;
using Riskmaster.Application.EnhancePolicy;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Text;
using System.Collections.Generic;//Add by kuladeep MITS:24736//skhare7 RMSC merge

namespace Riskmaster.BusinessAdaptor
{
    public class PolicyEnhForm : DataEntryFormBase
    {                    
        
        #region Global Varaibles and Properties

        
        protected Hashtable objExposuresUniqueIds = new Hashtable();
        protected Hashtable objCoveragesUniqueIds = new Hashtable();
        protected Hashtable objExposureUarSessionIds = new Hashtable();//Sumit
        //Sumit - Start(05/05/2010) - MITS# 20483
        protected Hashtable objScheduleSessionIds = new Hashtable();
        protected Hashtable objScheduleSessionIDs = new Hashtable();
        protected Hashtable objExposureSchedule = new Hashtable();
        protected Hashtable objUarSchedule = new Hashtable();
        //Sumit - End
        private EnhancePolicyManager m_objEnhancePolicyManager = null;
        private FundManager m_objFundManager = null;

        public EnhancePolicyManager EnhancePolicyManager
        {
            get
            {
                if (m_objEnhancePolicyManager == null)
                    m_objEnhancePolicyManager = new EnhancePolicyManager(PolicyEnh);
                return (m_objEnhancePolicyManager);
            }
        }

        
        protected EnhancePolicyUI structEnhancePolicyUI; 
        
        
        private FundManager FundManagerObject()
        {
            if (m_objFundManager == null)
            {
                //Retrieve the security credential information to pass to the FundManager object
                string sDSNName = base.m_fda.userLogin.objRiskmasterDatabase.DataSourceName;
                string sUserName = base.m_fda.userLogin.LoginName;
                string sPassword = base.m_fda.userLogin.Password;
                int iUserId = base.m_fda.userLogin.UserId;
                int iGroupId = base.m_fda.userLogin.GroupId;

                //Create and return the instance of the FundManager object
                m_objFundManager = new FundManager(sDSNName, sUserName, sPassword, iUserId, iGroupId, base.ClientId);
            }
            return m_objFundManager;
        }

        const string CLASS_NAME = "PolicyEnh";
        protected PolicyEnh PolicyEnh { get { return objData as PolicyEnh; } }
        public PolicyEnhForm(FormDataAdaptor fda) : base(fda)
        {
            base.m_ClassName = CLASS_NAME;
        }

        private SortedList objPolicyXTermEnhSorted = null;
        protected SortedList PolicyXTermEnhSorted
        {
            get
            {
                if (objPolicyXTermEnhSorted == null)
                {
                    string sKeyValue = string.Empty;
                    objPolicyXTermEnhSorted = new SortedList();
                    foreach (PolicyXTermEnh objPolicyXTermEnh in PolicyEnh.PolicyXTermEnhList)
                    {
                        sKeyValue = objPolicyXTermEnh.DttmRcdAdded;
                        objPolicyXTermEnhSorted.Add(sKeyValue, objPolicyXTermEnh);
                    }
                }
                return (objPolicyXTermEnhSorted);
            }
        }
        
        private UserLogin UserLogin { get { return PolicyEnh.Context.RMUser; } }
        #endregion 

        private int m_iTransNumber = 0;
        private bool m_bDataChanged = true; //pmahli EPA 5/10/2007 
        //umesh
        public override void InitNew()
        {
            base.InitNew();
            if (SysEx.DocumentElement.SelectSingleNode("PreSelectedTransactionId") == null)
                CreateSysExData("PreSelectedTransactionId");   
            //Sumit-MITS#18251 -09/29/2009 -Set hidden field for Policy comments
            if (SysEx.DocumentElement.SelectSingleNode("PolicyComments") == null)
                CreateSysExData("PolicyComments");
            //Sumit-MITS#18251 -09/29/2009 -Set hidden field for Policy comments
            if (SysEx.DocumentElement.SelectSingleNode("PolicyCommentsFlag") == null)
                CreateSysExData("PolicyCommentsFlag"); 
        }
        //End Umesh

        #region OnUpdateObject

        public override void OnUpdateObject()
        {           
            base.OnUpdateObject();
            int iTransNumber = 0;
            string sPostBackAction = string.Empty;
            m_bDataChanged = true;//pmahli EPA 5/10/2007 

            iTransNumber = base.GetSysExDataNodeInt("/SysExData//SelectedTransactionId", true);
            sPostBackAction = base.GetSysExDataNodeText( "/SysExData//" + EnhancePolicyManager.POST_BACK_ACTION );

            this.UpdateExpsoureList();
            this.UpdateCoveragesList();

            //Add by kuladeep for Audit Issue MITS:24736 Start
            this.UpdateInsuredList();
            //Add by kuladeep for Audit Issue MITS:24736 End //skhare7 RMSC merge
            // Update the Policy object from SysEx Data.
            this.UpdateObjectForPolicyQuoteDetails();

            //Start:Sumit(09/09/2010)-MITS#21872
            //Sumit(11/10/2010)-Reverting - Will be fixed later
            if (EnhancePolicyManager.UseBillingSystem)
            {
                this.OnUpdatePolicyBillingObject(iTransNumber);

            }
            //End:Sumit

            // Start Naresh (1/4/2007) MITS 9563 Amend Policy, Reload new business data and then endorsement data
            // Update the Policy.DiscountList with the edited discount factor amount.
            EnhancePolicyManager.PremiumManager.UpdateDiscounts(iTransNumber, SysEx);
            // End Naresh (1/4/2007)MITS 9563 Amend Policy, Reload new business data and then endorsement data

            // Perform any optional action 
            switch (sPostBackAction)
            {
                case EnhancePolicyManager.ACTION_ACCEPT_TRANSACTION :
                    base.m_DataChanged = true; 
                    Save();
                    this.OnAcceptTransaction( iTransNumber );
                    Save();
                    break;
                case EnhancePolicyManager.ACTION_DELETE_TRANSACTION:
                    base.m_DataChanged = true; 
                    Save();
                    this.OnDeleteTransaction(iTransNumber);
                    iTransNumber = 0;
                    Save();
                    break;
                case EnhancePolicyManager.ACTION_AMEND_POLICY :
                    string sTransationDate = base.GetSysExDataNodeText( "/SysExData//" + EnhancePolicyManager.SYSEX_AMEND_POLICY_TRANSACTION_DATE );                    
                    base.m_DataChanged = true;
                    Save();
                    this.OnAmendPolicy( sTransationDate );
                    m_iTransNumber = -1;
                    UpdateForm();
                    base.m_DataChanged = true;
                    Save();
                    iTransNumber = this.GetLatestTransactionNumberId();
                    break;
                case EnhancePolicyManager.ACTION_AUDIT_POLICY :
                    int iTermNumber = base.GetSysExDataNodeInt( "/SysExData//" + EnhancePolicyManager.SYSEX_AUDIT_POLICY_TERM_NUMBER );
                    base.m_DataChanged = true; 
                    Save();
                    this.OnAuditPolicy(iTermNumber);
                    m_iTransNumber = -1;
                    UpdateForm();
                    EnhancePolicyManager.PremiumManager.UpdateDiscountAndTiers(base.SysEx);
                    base.m_DataChanged = true;
                    Save();
                    iTransNumber = this.GetLatestTransactionNumberId();
                    break;
                case EnhancePolicyManager.ACTION_CANCEL_POLICY :
                    string sCancelType = base.GetSysExDataNodeText( "/SysExData//" + EnhancePolicyManager.SYSEX_CANCEL_POLICY_TYPE);
                    string sCancelDate = base.GetSysExDataNodeText("/SysExData//" +  EnhancePolicyManager.SYSEX_CANCEL_POLICY_DATE);
                    string sCancelReason = base.GetSysExDataNodeText("/SysExData//" +  EnhancePolicyManager.SYSEX_CANCEL_POLICY_REASON);
                    base.m_DataChanged = true; 
                    Save();
                    this.OnCancelPolicy(sCancelType, sCancelDate, sCancelReason);
                    m_iTransNumber = -1;
                    UpdateForm();
                    EnhancePolicyManager.PremiumManager.UpdateDiscountAndTiers(base.SysEx);
                    base.m_DataChanged = true;
                    Save();
                    iTransNumber = this.GetLatestTransactionNumberId();
                    break;
                case EnhancePolicyManager.ACTION_CONVERT_TO_POLICY :
                    //pmahli MITS 9793
                    // added save function as it was not saving the discount and discount tier information 
                    //if exposure is edited before doing convert to policy.
                    Save();
                    this.OnConvertToPolicy();                                        
                    iTransNumber = this.GetLatestTransactionNumberId();
                    break ;
                case EnhancePolicyManager.ACTION_RENEW_POLICY:
                    this.OnRenewPolicy();
                    m_iTransNumber = -1;
                    UpdateForm();
                    EnhancePolicyManager.PremiumManager.AddDiscountAndTiers(base.SysEx);
                    base.m_DataChanged = true;
                    Save();
                    iTransNumber = this.GetLatestTransactionNumberId();
                    break;
                case EnhancePolicyManager.ACTION_REINSTATE_POLICY:
                    string sReinsDate = base.GetSysExDataNodeText(EnhancePolicyManager.SYSEX_REINSTATE_POLICY_DATE);
                    bool bReinsWithLapseFlag;
                    if (sReinsDate != "")
                        bReinsWithLapseFlag = true;
                    else
                        bReinsWithLapseFlag = false;
                    this.OnReinstatePolicy(sReinsDate, bReinsWithLapseFlag);
                    m_iTransNumber = -1;
                    UpdateForm();
                    EnhancePolicyManager.PremiumManager.UpdateDiscountAndTiers(base.SysEx);
                    base.m_DataChanged = true;
                    Save();
                    iTransNumber = this.GetLatestTransactionNumberId();
                    break;

                // Start Naresh (1/4/2007) MITS 9563 Amend Policy, Reload new business data and then endorsement data
                case EnhancePolicyManager.ACTION_TRANSACTION_CHANGE:
                    base.m_DataChanged = true;
                    // Start Naresh MITS 9270 Discount Refresh Problem
                    int iTransId = 0;
                    string sTransactionStatus = string.Empty;
                    string sTransactionType = string.Empty;
                    foreach (PolicyXTransEnh objPolicyXTransEnh in this.PolicyEnh.PolicyXTransEnhList)
                    {
                        if (objPolicyXTransEnh.TransactionId > iTransId)
                        {
                            iTransId = objPolicyXTransEnh.TransactionId;
                            sTransactionStatus = EnhancePolicyManager.LocalCache.GetShortCode(objPolicyXTransEnh.TransactionStatus);
                            sTransactionType = EnhancePolicyManager.LocalCache.GetShortCode(objPolicyXTransEnh.TransactionType);
                        }
                    }
                    //pmahli MITS 9793 
                    //UpdateDiscount called when Stransaction type = Audit and Transaction status is Provisional
                    if ((sTransactionType == "RN" && sTransactionStatus == "PR") || (sTransactionType == "AU" && sTransactionStatus == "PR"))
                    {
                        EnhancePolicyManager.PremiumManager.UpdateDiscounts(iTransId, SysEx);
                    }
                    // End Naresh MITS 9270 Discount Refresh Problem
                    Save();
                    break;
                case EnhancePolicyManager.ACTION_EXPOSURE_EDIT:
                    // Start Naresh MITS 9270 Discount Refresh Problem
                    iTransId = 0;
                    sTransactionStatus = string.Empty;
                    sTransactionType = string.Empty;
                    // End Naresh MITS 9270 Discount Refresh Problem
                    foreach (PolicyXTransEnh objPolicyXTransEnh in this.PolicyEnh.PolicyXTransEnhList)
                    {
                        if (objPolicyXTransEnh.TransactionId > iTransId)
                        {
                            iTransId = objPolicyXTransEnh.TransactionId;
                            sTransactionStatus = EnhancePolicyManager.LocalCache.GetShortCode(objPolicyXTransEnh.TransactionStatus);
                            sTransactionType = EnhancePolicyManager.LocalCache.GetShortCode(objPolicyXTransEnh.TransactionType);
                        }
                    }
                    //pmahli MITS 9870 Saving Exposure Edit data in Renew 
                    //pmahli MITS 9793 Saving Exposure Edit data in Audit
                    if ((sTransactionType == "EN" && sTransactionStatus == "PR") || (sTransactionType == "RN" && sTransactionStatus == "PR") || (sTransactionType == "AU" && sTransactionStatus == "PR"))
                    {
                        base.m_DataChanged = true;
                        Save();
                    }
                    break;
                // End Naresh (1/4/2007) MITS 9563 Amend Policy, Reload new business data and then endorsement data

                // Start Naresh (6/4/2007) MITS 9511 Populate the Insured details in the Policy Billing Tab
                case "INSURED_DETAILS":
                    int iInsuredId = base.GetSysExDataNodeInt(EnhancePolicyManager.SYSEX_INSURED_ID);
                    string sControlType = base.GetSysExDataNodeText(EnhancePolicyManager.SYSEX_CONTROL_TYPE);
                    OnInsuredSelect(iInsuredId,sControlType);
                    break;
                // End Naresh (6/4/2007) MITS 9511 Populate the Insured details in the Policy Billing Tab

            }
            // Reset the Transaction, If it has been deleted or in case of Policy Amend/Audit/ConvertToQuote.
            m_iTransNumber = iTransNumber;
        }

        // Start Naresh (6/4/2007) MITS 9511 Populate the Insured details in the Policy Billing Tab
        private void OnInsuredSelect(int p_iInsuredId,string p_sControlType)
        {
            int iTransNumber = base.GetSysExDataNodeInt("/SysExData//SelectedTransactionId", true);
            XmlDocument objXmlDoc = null;
            objXmlDoc = EnhancePolicyManager.InsuredSelect(p_iInsuredId);

            bool blnSuccess;
            foreach (PolicyXBillEnh objPolicyXBillEnh in this.PolicyEnh.PolicyXBillEnhList)
            {
                if (objPolicyXBillEnh.TransactionId == iTransNumber)
                {
                    if (p_sControlType == "InsuredList")
                    {
                        objPolicyXBillEnh.BillToEid = Conversion.ConvertStrToInteger(((XmlElement)objXmlDoc.SelectSingleNode("//EntityId")).InnerText);
                        objPolicyXBillEnh.InsuredEntity.LastName = ((XmlElement)objXmlDoc.SelectSingleNode("//LastName")).InnerText;
                        objPolicyXBillEnh.InsuredEntity.FirstName = ((XmlElement)objXmlDoc.SelectSingleNode("//FirstName")).InnerText;
                        objPolicyXBillEnh.InsuredEntity.Addr1 = ((XmlElement)objXmlDoc.SelectSingleNode("//Addr1")).InnerText;
                        objPolicyXBillEnh.InsuredEntity.Addr2 = ((XmlElement)objXmlDoc.SelectSingleNode("//Addr2")).InnerText;
                        objPolicyXBillEnh.InsuredEntity.Addr3 = ((XmlElement)objXmlDoc.SelectSingleNode("//Addr3")).InnerText;// JIRA 6420 pkandhari
                        objPolicyXBillEnh.InsuredEntity.Addr4 = ((XmlElement)objXmlDoc.SelectSingleNode("//Addr4")).InnerText;// JIRA 6420 pkandhari
                        objPolicyXBillEnh.InsuredEntity.City = ((XmlElement)objXmlDoc.SelectSingleNode("//City")).InnerText;
                        objPolicyXBillEnh.InsuredEntity.ZipCode = ((XmlElement)objXmlDoc.SelectSingleNode("//ZipCode")).InnerText;
                        objPolicyXBillEnh.InsuredEntity.StateId = Conversion.CastToType<Int32>((objXmlDoc.SelectSingleNode("//StateId/@codeid")).Value,out blnSuccess);
                        objPolicyXBillEnh.InsuredEntity.CountryCode = Conversion.CastToType<Int32>((objXmlDoc.SelectSingleNode("//CountryCode/@codeid")).Value, out blnSuccess);
                    }
                    // Start Naresh (6/5/2007) MITS 9512 Populate the Insured Details in New Billing Info
                    else if (p_sControlType == "NewInsured")
                    {
                        objPolicyXBillEnh.NewBillToEid = Conversion.ConvertStrToInteger(((XmlElement)objXmlDoc.SelectSingleNode("//EntityId")).InnerText);
                        objPolicyXBillEnh.NewInsuredEntity.LastName = ((XmlElement)objXmlDoc.SelectSingleNode("//LastName")).InnerText;
                        objPolicyXBillEnh.NewInsuredEntity.FirstName = ((XmlElement)objXmlDoc.SelectSingleNode("//FirstName")).InnerText;
                        objPolicyXBillEnh.NewInsuredEntity.Addr1 = ((XmlElement)objXmlDoc.SelectSingleNode("//Addr1")).InnerText;
                        objPolicyXBillEnh.NewInsuredEntity.Addr2 = ((XmlElement)objXmlDoc.SelectSingleNode("//Addr2")).InnerText;
                        objPolicyXBillEnh.NewInsuredEntity.Addr3 = ((XmlElement)objXmlDoc.SelectSingleNode("//Addr3")).InnerText;// JIRA 6420 pkandhari
                        objPolicyXBillEnh.NewInsuredEntity.Addr4 = ((XmlElement)objXmlDoc.SelectSingleNode("//Addr4")).InnerText;// JIRA 6420 pkandhari
                        objPolicyXBillEnh.NewInsuredEntity.City = ((XmlElement)objXmlDoc.SelectSingleNode("//City")).InnerText;
                        objPolicyXBillEnh.NewInsuredEntity.ZipCode = ((XmlElement)objXmlDoc.SelectSingleNode("//ZipCode")).InnerText;
                        objPolicyXBillEnh.NewInsuredEntity.StateId = Conversion.CastToType<Int32>((objXmlDoc.SelectSingleNode("//StateId/@codeid")).Value, out blnSuccess);
                        objPolicyXBillEnh.NewInsuredEntity.CountryCode = Conversion.CastToType<Int32>((objXmlDoc.SelectSingleNode("//CountryCode/@codeid")).Value, out blnSuccess);
                    }
                    // End Naresh (6/5/2007) MITS 9512 Populate the Insured Details in New Billing Info
                    
                }
                

            }
        }
        // End Naresh (6/4/2007) MITS 9511 Populate the Insured details in the Policy Billing Tab

        private void OnUpdatePolicyBillingObject(int p_iTransNumber)
        {
            XmlDocument objSysExDataXmlDoc = base.SysEx;

            
            XmlNode objBillToEid = null;
            XmlNode objNewBillToEid = null;
            try
            {
                objBillToEid = objSysExDataXmlDoc.SelectSingleNode("/SysExData//PayPlan_EntityId");
                objNewBillToEid = objSysExDataXmlDoc.SelectSingleNode("/SysExData//NewLastName_Cid");
            }
            catch { };

            foreach(PolicyXBillEnh obj in this.PolicyEnh.PolicyXBillEnhList)
            {
                if (obj.TransactionId == p_iTransNumber)
                {
                    obj.BillToEid = Conversion.ConvertStrToInteger(objBillToEid.InnerText);
                    obj.NewBillToEid = Conversion.ConvertStrToInteger(objNewBillToEid.InnerText);
                    obj.DataChanged = true;
                }
            }
        }

        virtual protected void UpdateExpsoureList()
        {            
            //Added rjhamb for Updating Session Object on Policy State and Date Change
            string sAction = string.Empty;
            string sTransType = string.Empty;
            string sOrgHier = string.Empty;
            PolicyXExpEnh objTempExpsoure = null;
            XmlNode objNode = null;
            SessionManager objSessionManager = null;
            //Added rjhamb for Updating Session Object on Policy State and Date Change
            string sNewAddedExposureSessionId = base.GetSysExDataNodeText("NewAddedExposureSessionId");
            int iDeletedRowExposureId = base.GetSysExDataNodeInt("DeletedRowExposureId");
            XmlNode objExposureCollectionNode = base.SysEx.DocumentElement.SelectSingleNode("ExposureList");
            PolicyXExpEnh objUpdateExpsoure = (PolicyXExpEnh)EnhancePolicyManager.DataModelFactory.GetDataModelObject("PolicyXExpEnh", false);
            int iTransNumber = base.GetSysExDataNodeInt("/SysExData//SelectedTransactionId", true);
            objExposuresUniqueIds.Clear();

            //Parijat:Mits 9887
            ArrayList arrExposure = new ArrayList();
            // For Edit Rows.
            foreach (XmlNode objOptionNode in objExposureCollectionNode.SelectNodes("option"))
            {                                            
                string sSessionId = objOptionNode.SelectSingleNode("SessionId").InnerText ;
                string[] sSessionID = sSessionId.Split(new char[] { '~' });
                int iExposureId = Conversion.ConvertStrToInteger(objOptionNode.SelectSingleNode("ExposureId").InnerText);

                if (iDeletedRowExposureId == iExposureId)
                {
                    PolicyEnh.PolicyXExposureEnhList.Remove(iExposureId);
                    continue;
                }
                //Parijat:Mits 9887 
                //Just maintaining the status which has to be updated for the grid 
                //through the xml
                arrExposure.Add(iExposureId.ToString());

                //if( sSessionId != "" )
                if (!string.IsNullOrEmpty(sSessionID[0]))
                {
                    // Replace the object from the session object.                                         
                    //string sSessionRawData = (string)base.m_fda.getCachedObject(sSessionId);
                    string sSessionRawData = (string)base.m_fda.getCachedObject(sSessionID[0]);
                    XmlDocument objPropertyStore = new XmlDocument();
                    objPropertyStore.LoadXml(sSessionRawData);

                    //Parijat: Mits 9887
                    //checking if the previous Exposure which was added is missing at the object 
                    //level then adding it once again else updating it from the previous session.  
                    // npadhy MITS 17067 Accessing the Objects using Indices was creating problems so Accessing as per this.
                    if (iExposureId > 0)
                    {
                        PolicyEnh.PolicyXExposureEnhList[iExposureId].PopulateObject(objPropertyStore);

                        PolicyEnh.PolicyXExposureEnhList[iExposureId].Supplementals.DataChanged = true;
                        // Overwrite the object ExposureId, this might be the old one, while object added in collection
                        // Datamodel generates it new unique Id. 
                        PolicyEnh.PolicyXExposureEnhList[iExposureId].ExposureId = iExposureId;
                    }
                    else
                    {
                        PolicyXExpEnh objUpdateExpsoure1 = (PolicyXExpEnh)EnhancePolicyManager.DataModelFactory.GetDataModelObject("PolicyXExpEnh", false);
                        objUpdateExpsoure1.PopulateObject(objPropertyStore);
                        objUpdateExpsoure1.Supplementals.DataChanged = true;
                        // Overwrite the object ExposureId, this might be the old one, while object added in collection
                        // Datamodel generates it new unique Id. 
                        objUpdateExpsoure1.ExposureId = iExposureId;
                        PolicyEnh.PolicyXExposureEnhList.Add(objUpdateExpsoure1, true);
                        //Added rjhamb for Updating Session Object on Policy State and Date Change
                        if (base.GetSysExDataNodeText("IsQuote") == "True")
                        {
                            sAction = base.GetSysExDataNodeText("/SysExData//" + EnhancePolicyManager.POST_BACK_ACTION);
                            if (sAction == "POLICY_DATE_CHANGE" || sAction == "POLICY_STATE_CHANGE" )
                            {
                                objTempExpsoure = (PolicyXExpEnh)EnhancePolicyManager.DataModelFactory.GetDataModelObject("PolicyXExpEnh", false);
                                objSessionManager = base.m_fda.GetSessionObject();
                                objUpdateExpsoure1.EffectiveDate = base.GetSysExDataNodeText("EffectiveDate");
                                objUpdateExpsoure1.ExpirationDate = base.GetSysExDataNodeText("ExpirationDate");
                                sTransType = EnhancePolicyManager.CCacheFunctions.GetShortCode(objUpdateExpsoure1.TransactionType);
                                sOrgHier = PolicyEnh.PolicyXInsuredEnh.ToString();
                                sOrgHier = sOrgHier.Replace(" ", ",");
                                objTempExpsoure = objUpdateExpsoure1;
                                EnhancePolicyManager.Rate(ref objTempExpsoure, PolicyEnh.State, EnhancePolicyManager.RoundFlag, objTempExpsoure.PrAnnPremAmt, objTempExpsoure.FullAnnPremAmt, sTransType, DateTime.MinValue, PolicyEnh.PolicyType, sOrgHier);
                                objNode = objPropertyStore.SelectSingleNode("PolicyXExpEnh/ExposureRate");
                                if (objNode != null)
                                {
                                    objNode.InnerText = objTempExpsoure.ExposureRate.ToString();
                                }
                                objNode = objPropertyStore.SelectSingleNode("PolicyXExpEnh/ExposureBaseRate");
                                if (objNode != null)
                                {
                                    objNode.InnerText = objTempExpsoure.ExposureBaseRate.ToString();
                                }
                                objNode = objPropertyStore.SelectSingleNode("PolicyXExpEnh/PremAdjAmt");
                                if (objNode != null)
                                {
                                    objNode.InnerText = objTempExpsoure.PremAdjAmt.ToString();
                                }
                                objNode = objPropertyStore.SelectSingleNode("PolicyXExpEnh/PrAnnPremAmt");
                                if (objNode != null)
                                {
                                    objNode.InnerText = objTempExpsoure.PrAnnPremAmt.ToString();
                                }
                                objNode = objPropertyStore.SelectSingleNode("PolicyXExpEnh/FullAnnPremAmt");
                                if (objNode != null)
                                {
                                    objNode.InnerText = objTempExpsoure.FullAnnPremAmt.ToString();
                                }
                                // Added by Amitosh for mits 20820 (2/7/2011) 
                                objPropertyStore.SelectSingleNode("PolicyXExpEnh/EffectiveDate").InnerText = base.GetSysExDataNodeText("EffectiveDate");
                                objPropertyStore.SelectSingleNode("PolicyXExpEnh/ExpirationDate").InnerText = base.GetSysExDataNodeText("ExpirationDate");
                                // end Amitosh
                                objSessionManager.SetBinaryItem(sSessionID[0], Utilities.BinarySerialize(objPropertyStore.OuterXml), base.ClientId);

                            }
                        }
                        //Added rjhamb for Updating Session Object on Policy State and Date Change
                    }

                   
                    //PolicyEnh.PolicyXExposureEnhList[iExposureId].Supplementals.DataChanged = true ;
                    // Overwrite the object ExposureId, this might be the old one, while object added in collection
                    // Datamodel generates it new unique Id. 
                    //PolicyEnh.PolicyXExposureEnhList[iExposureId].ExposureId = iExposureId;

                    // Add new session key.
                    objExposuresUniqueIds.Add(iExposureId, sSessionId);
                    
                }                
            }
            //Parijat:Mits 9887
            //Just updating the object status with ,the grid data .
            //Using the arrExposure which was being used to maintain the required info.
            //Also a check is placed,in order to avoid the code from running in case the event is "Transaction Change" after the amend , 
            //since this particular case requires the page to be loaded from the beginning and hence no object updation is required.
            string sPostBackAction = base.GetSysExDataNodeText("/SysExData//" + EnhancePolicyManager.POST_BACK_ACTION);
            //Commented rjhamb for Updating Session Object on Policy State and Date Change
            ////Sumit - MITS#20820
            //string sTransactionType = string.Empty;
            //DateTime reinstDate = DateTime.MinValue;
            //string sOrgH = string.Empty;
            //PolicyXExpEnh objExpsoureNew = (PolicyXExpEnh)EnhancePolicyManager.DataModelFactory.GetDataModelObject("PolicyXExpEnh", false);
            ////End: Sumit
            //Commented rjhamb for Updating Session Object on Policy State and Date Change
            if (sPostBackAction != "TRANSACTION_CHANGE")
            {
                foreach (PolicyXExpEnh item in PolicyEnh.PolicyXExposureEnhList)
                {
                    //Commented rjhamb for Updating Session Object on Policy State and Date Change
                    ////Start (07/06/2010) MITS# 20820:Sumit - Updated dates as user may change them at later stage.
                    //if (base.GetSysExDataNodeText("IsQuote") == "True")
                    //{
                    //    item.EffectiveDate=base.GetSysExDataNodeText("EffectiveDate");
                    //    item.ExpirationDate = base.GetSysExDataNodeText("ExpirationDate");

                    //    if (sPostBackAction == "POLICY_DATE_CHANGE" || sPostBackAction == "POLICY_STATE_CHANGE")
                    //    {
                    //        sTransactionType = EnhancePolicyManager.CCacheFunctions.GetShortCode(item.TransactionType);
                    //        sOrgH = PolicyEnh.PolicyXInsuredEnh.ToString();
                    //        sOrgH = sOrgH.Replace(" ", ",");
                    //        objExpsoureNew = item;
                    //        EnhancePolicyManager.Rate(ref objExpsoureNew, PolicyEnh.State, EnhancePolicyManager.RoundFlag, objExpsoureNew.PrAnnPremAmt, objExpsoureNew.FullAnnPremAmt, sTransactionType, reinstDate, PolicyEnh.PolicyType, sOrgH);
                    //        item.ExposureRate = objExpsoureNew.ExposureRate;
                    //        item.ExposureBaseRate = objExpsoureNew.ExposureBaseRate;
                    //        item.PremAdjAmt = objExpsoureNew.PremAdjAmt;
                    //        item.PrAnnPremAmt = objExpsoureNew.PrAnnPremAmt;
                    //        item.FullAnnPremAmt = objExpsoureNew.FullAnnPremAmt;
                    //    }
                    //}
                    ////End :Sumit
                    //Commented rjhamb for Updating Session Object on Policy State and Date Change
                    if (item.TransactionId == iTransNumber || item.TransactionId == 0)
                    {
                        if (!arrExposure.Contains(item.ExposureId.ToString()))
                        {
                            PolicyEnh.PolicyXExposureEnhList.Remove(Convert.ToInt32(item.ExposureId));
                        }
                    }
                }
            }
            else  // Parijat:Updating objects in case there is a transaction_change,so that the deleted items  
            {     // can be saved effectively for the previously selected transaction in case of transaction_change

                string strPrevTransId = base.GetSysExDataNodeText("/SysExData/PreSelectedTransactionId");
                int iPrevTransId = Conversion.ConvertStrToInteger(strPrevTransId);
                foreach (PolicyXExpEnh item in PolicyEnh.PolicyXExposureEnhList)
                {
                    if (item.TransactionId == iPrevTransId )//|| item.TransactionId == 0)
                    {
                        if (!arrExposure.Contains(item.ExposureId.ToString()))
                        {
                            PolicyEnh.PolicyXExposureEnhList.Remove(Convert.ToInt32(item.ExposureId));
                        }
                    }
                }
            }
            // For New Rows
            if (sNewAddedExposureSessionId != "")
            {
                string sSessionRawData = (string)base.m_fda.getCachedObject(sNewAddedExposureSessionId);
                XmlDocument objPropertyStore = new XmlDocument();
                objPropertyStore.LoadXml(sSessionRawData);
                objUpdateExpsoure.PopulateObject(objPropertyStore);

                PolicyEnh.PolicyXExposureEnhList.Add(objUpdateExpsoure);                
                int iExposureId = objUpdateExpsoure.ExposureId;                                

                // Add new session key.
                objExposuresUniqueIds.Add(iExposureId, sNewAddedExposureSessionId);

            }

            double dblMaualPremium = 0 ;
           // int iTransNumber = base.GetSysExDataNodeInt("/SysExData//SelectedTransactionId", true);
            
            // Edit Manual Premium in SysEx which would get updated while updating Rating Record. 
            foreach (PolicyXExpEnh objPolicyXExpEnh in PolicyEnh.PolicyXExposureEnhList)
            {
                if (objPolicyXExpEnh.TransactionId == iTransNumber )
                    dblMaualPremium += objPolicyXExpEnh.PrAnnPremAmt;                
            }

            XmlElement objManualPremiumNode = (XmlElement)SysEx.SelectSingleNode("/SysExData//PremiumAmounts/ManualPremium");
            if (objManualPremiumNode != null)
                objManualPremiumNode.InnerText = dblMaualPremium.ToString();
            else
            {
                // The Manual Premium node does not exist in SysEx so we create a node of the same name
                XmlNode objPremiumNode = SysEx.SelectSingleNode("/SysExData//PremiumAmounts");
                if (objPremiumNode != null)
                {
                    XmlNode objManualPremium = objPremiumNode.OwnerDocument.CreateElement("ManualPremium" );
                    objManualPremium.InnerText = dblMaualPremium.ToString();
                    objPremiumNode.AppendChild(objManualPremium);

                }

            }
        }

        private void UpdateCoveragesList()
        {
            string sNewAddedCoveragesSessionId = base.GetSysExDataNodeText("NewAddedCoveragesSessionId");
            int iDeletedRowCoverageId = base.GetSysExDataNodeInt("DeletedRowCoveragesId");
            XmlNode objCoveragesCollectionNode = base.SysEx.DocumentElement.SelectSingleNode("Coverages");
            PolicyXCvgEnh objUpdateCoverages = (PolicyXCvgEnh)EnhancePolicyManager.DataModelFactory.GetDataModelObject("PolicyXCvgEnh", false);
            //Parijat:Mits 9887
            int iTransNumber = base.GetSysExDataNodeInt("/SysExData//SelectedTransactionId", true);
            objCoveragesUniqueIds.Clear();
            // For Edit Rows.
            //Parijat:MITS 9887
            ArrayList arrCoverage = new ArrayList();
            foreach (XmlNode objOptionNode in objCoveragesCollectionNode.SelectNodes("option"))
            {
                string sSessionId = objOptionNode.SelectSingleNode("SessionId").InnerText;
                int iCoveragesId = Conversion.ConvertStrToInteger(objOptionNode.SelectSingleNode("CoveragesId").InnerText);

                if (iDeletedRowCoverageId == iCoveragesId)
                {
                    PolicyEnh.PolicyXCvgEnhList.Remove(iCoveragesId);
                    continue;
                }
                //Parijat:Mits 9887 
                //Just maintaining the status which has to be updated, for the grid, through the xml
                arrCoverage.Add(iCoveragesId.ToString());

                if (sSessionId != "")
                {
                    // Replace the object from the session object. 

                    //int iPolicyCvgRowID = Conversion.ConvertStrToInteger(objOptionNode.SelectSingleNode("
                    string sSessionRawData = (string)base.m_fda.getCachedObject(sSessionId);
                    XmlDocument objPropertyStore = new XmlDocument();
                    objPropertyStore.LoadXml(sSessionRawData);
                    //Parijat: Mits 9887
                    //checking if the previous Coverage which was added is missing at the object 
                    //level then adding it once again else updating it from the previous session.
                    // npadhy MITS 17067 Accessing the Objects using Indices was creating problems so Accessing as per this.
                    if (iCoveragesId > 0)
                    {
                        PolicyEnh.PolicyXCvgEnhList[iCoveragesId].PopulateObject(objPropertyStore);
                        (PolicyEnh.PolicyXCvgEnhList[iCoveragesId] as PolicyXCvgEnh).Supplementals.DataChanged = true;
                        // Overwrite the object ExposureId, this might be the old one, while object added in collection
                        // Datamodel generates it new unique Id. 
                        (PolicyEnh.PolicyXCvgEnhList[iCoveragesId]as PolicyXCvgEnh).PolcvgRowId = iCoveragesId;
                    }
                    else
                    {
                        PolicyXCvgEnh objUpdateCoverages1 = (PolicyXCvgEnh)EnhancePolicyManager.DataModelFactory.GetDataModelObject("PolicyXCvgEnh", false);
                        objUpdateCoverages1.PopulateObject(objPropertyStore);
                        objUpdateCoverages1.Supplementals.DataChanged = true;
                        // Overwrite the object ExposureId, this might be the old one, while object added in collection
                        // Datamodel generates it new unique Id. 
                        objUpdateCoverages1.PolcvgRowId = iCoveragesId;
                        PolicyEnh.PolicyXCvgEnhList.Add(objUpdateCoverages1, true);
                    }

                    //PolicyEnh.PolicyXCvgEnhList[iCoveragesId].Supplementals.DataChanged = true;
                    // Overwrite the object ExposureId, this might be the old one, while object added in collection
                    // Datamodel generates it new unique Id. 
                    //PolicyEnh.PolicyXCvgEnhList[iCoveragesId].PolcvgRowId = iCoveragesId;
                    // Add new session key.
                    objCoveragesUniqueIds.Add(iCoveragesId, sSessionId);
                }
            }
            //Parijat:Mits 9887
            //Just updating the object status with ,the grid data .
            //Using the arrExposure which was being used to maintain the required info.
            //Also a check is placed,in order to avoid the code from running in case the event is "Transaction Change" after the amend , 
            //since this particular case requires the page to be loaded from the beginning and hence no object updation is required.
            string sPostBackAction = base.GetSysExDataNodeText("/SysExData//" + EnhancePolicyManager.POST_BACK_ACTION);
            //Sumit - MITS#20820
            bool bIsSuccess = false;
            string sOrgH = string.Empty;
            string sLOB = string.Empty;
            XmlDocument objReturnDoc = null;
            XmlDocument objReturnXML = null;
            XmlElement objRoot = null;
            //End: Sumit

            if (sPostBackAction != "TRANSACTION_CHANGE")
            {
                foreach (PolicyXCvgEnh item in PolicyEnh.PolicyXCvgEnhList)
                {
                    //Start (07/06/2010) MITS# 20820:Sumit - Updated dates as user may change them at later stage.
                    if (base.GetSysExDataNodeText("IsQuote") == "True")
                    {
                        item.EffectiveDate = base.GetSysExDataNodeText("EffectiveDate");
                        item.ExpirationDate = base.GetSysExDataNodeText("ExpirationDate");

                        if (sPostBackAction == "POLICY_DATE_CHANGE" || sPostBackAction == "POLICY_STATE_CHANGE")
                        {
                            objReturnDoc=null;
                            objRoot=null;
                            objReturnXML=null;
                            //Start - averma62 - MISTS 29121
                            foreach (PolicyXInsuredEnh objPolInsuredEnh in PolicyEnh.PolicyXInsuredEnhList)
                            {
                                if (string.IsNullOrEmpty(sOrgH))
                                    sOrgH = objPolInsuredEnh.InsuredEid.ToString();
                                else
                                    sOrgH = sOrgH + "," + objPolInsuredEnh.InsuredEid.ToString();                                
                            }
                            //  sOrgH = PolicyEnh.PolicyXInsuredEnh.ToString();
                            //sOrgH = sOrgH.Replace(" ", ",");
                            //End - averma62 - MISTS 29121
                            sLOB = EnhancePolicyManager.CCacheFunctions.GetShortCode(PolicyEnh.PolicyType);
                            StartDocument(ref objReturnDoc, ref objRoot, "Coverages");
                            CreateAndSetElement(objRoot, "LOB", sLOB);
                            sOrgH = EnhancePolicyManager.OrgHierarchy(sOrgH);
                            CreateAndSetElement(objRoot, "Org", sOrgH);
                            CreateAndSetElement(objRoot, "EffDate", item.EffectiveDate);
                            CreateAndSetElement(objRoot, "ExpDate", item.ExpirationDate);
                            CreateAndSetElement(objRoot, "State", PolicyEnh.State.ToString());
                            CreateAndSetElement(objRoot, "CoverageTypeId", item.CoverageTypeCode.ToString());
                            objReturnXML = EnhancePolicyManager.CoverageManager.CalculateDeductible(base.Adaptor.GetSessionObject(), objReturnDoc); 
                            
                            if (objReturnXML.SelectSingleNode("//Range") != null)
                            {
                                item.Deductible = 0;
                                item.Modifier = 0;
                            }
                            else
                            {
                                if (objReturnXML.SelectSingleNode("//Amount") != null)
                                {
                                    item.Deductible = Conversion.CastToType<double>(objReturnXML.SelectSingleNode("//Amount").InnerText, out bIsSuccess);
                                    item.Modifier = item.Deductible;
                                }
                            }
                        }
                    }
                    //End :Sumit

                    if (item.TransactionId == iTransNumber || item.TransactionId == 0)
                    {
                        if (!arrCoverage.Contains(item.PolcvgRowId.ToString()))
                        {
                            PolicyEnh.PolicyXCvgEnhList.Remove(Convert.ToInt32(item.PolcvgRowId));
                        }
                    }
                }
            }
            else  // Parijat:Updating objects in case there is a transaction_change,so that the deleted items  
            {     // can be saved effectively for the previously selected transaction in case of transaction_change

                string strPrevTransId = base.GetSysExDataNodeText("/SysExData/PreSelectedTransactionId");
                int iPrevTransId = Conversion.ConvertStrToInteger(strPrevTransId);
                foreach (PolicyXCvgEnh item in PolicyEnh.PolicyXCvgEnhList)
                {
                    if (item.TransactionId == iPrevTransId )//|| item.TransactionId == 0)
                    {
                        if (!arrCoverage.Contains(item.PolcvgRowId.ToString()))
                        {
                            PolicyEnh.PolicyXCvgEnhList.Remove(Convert.ToInt32(item.PolcvgRowId));
                        }
                    }
                }
            }
            // For New Rows
            if (sNewAddedCoveragesSessionId != "")
            {
                string sSessionRawData = (string)base.m_fda.getCachedObject(sNewAddedCoveragesSessionId);
                XmlDocument objPropertyStore = new XmlDocument();
                objPropertyStore.LoadXml(sSessionRawData);
                objUpdateCoverages.PopulateObject(objPropertyStore);

                PolicyEnh.PolicyXCvgEnhList.Add(objUpdateCoverages);
                int iCoveragesId = objUpdateCoverages.PolcvgRowId;

                // Add new session key.
                objCoveragesUniqueIds.Add(iCoveragesId, sNewAddedCoveragesSessionId);

            }
            
        }

        private void OnAcceptTransaction(  int p_iTransactionId )
        {
            if (!EnhancePolicyManager.TransactionManager.TransactionAlreadyAccepted(p_iTransactionId))
            {
                if (!EnhancePolicyManager.TransactionManager.TransactionAlreadyDeleted(p_iTransactionId))
                {
                    EnhancePolicyManager.TransactionManager.AcceptTransaction(p_iTransactionId);
                    base.m_DataChanged = true;
                    m_bDataChanged = false; //pmahli EPA 5/10/2007 
                }
                else
                    Errors.Add("Transaction", "Transaction has been deleted by another user. Please re-retrieve the policy.", BusinessAdaptorErrorType.PopupMessage);
            }
            else
                Errors.Add("Transaction", "Transaction has been accepted by another user. Please re-retrieve the policy.", BusinessAdaptorErrorType.PopupMessage);
               
        }

        private void OnDeleteTransaction(int p_iTransactionId)
        {
            if (!EnhancePolicyManager.TransactionManager.TransactionAlreadyAccepted(p_iTransactionId))
            {
                if (!EnhancePolicyManager.TransactionManager.TransactionAlreadyDeleted(p_iTransactionId))
                {
                    EnhancePolicyManager.TransactionManager.DeleteTransaction(p_iTransactionId);
                    base.m_DataChanged = true;
                    m_bDataChanged = false; //pmahli EPA 5/10/2007 
                }
                else
                    Errors.Add("Transaction", "Transaction has been deleted by another user. Please re-retrieve the policy.", BusinessAdaptorErrorType.PopupMessage);
            }
            else
                Errors.Add("Transaction", "Transaction has been accepted by another user. Please re-retrieve the policy.", BusinessAdaptorErrorType.PopupMessage);
        }

        private void OnAmendPolicy(string p_sTransationDate)
        {
            if (!EnhancePolicyManager.PolicyAlreadyUpdated())
            {
                //pmahli MITS 9841 Policy Management Supplementals -Start
                if (base.m_fda.HasParam("SysPropertyStore"))
                    EnhancePolicyManager.PropertyStore.LoadXml(base.m_fda.SafeParam("SysPropertyStore").InnerXml);

                EnhancePolicyManager.AmendPolicy( p_sTransationDate);
                base.m_DataChanged = true;
                m_bDataChanged = false; //pmahli EPA 5/10/2007 
            }
            else
                Errors.Add("AmendPolicy", "Policy has been updated by another user. Please re-retrieve the policy.", BusinessAdaptorErrorType.PopupMessage);
        }

        private void OnAuditPolicy(int p_iTermNumber)
        {
            if (!EnhancePolicyManager.PolicyAlreadyUpdated())
            {
                //pmahli MITS 9841 Policy Management Supplementals -Start
                if (base.m_fda.HasParam("SysPropertyStore"))
                    EnhancePolicyManager.PropertyStore.LoadXml(base.m_fda.SafeParam("SysPropertyStore").InnerXml);

                EnhancePolicyManager.AuditPolicy(p_iTermNumber);
                base.m_DataChanged = true;
                m_bDataChanged = false; //pmahli EPA 5/10/2007 
            }
            else
                Errors.Add("AuditPolicy", "Policy has been updated by another user. Please re-retrieve the policy.", BusinessAdaptorErrorType.PopupMessage);
        }

        private void OnCancelPolicy(string p_sCancelType, string p_sCancelDate, string p_sCancelReason)
        {
            if (!EnhancePolicyManager.PolicyAlreadyUpdated())
            {
                //pmahli MITS 9841 Policy Management Supplementals -Start
                if (base.m_fda.HasParam("SysPropertyStore"))
                    EnhancePolicyManager.PropertyStore.LoadXml(base.m_fda.SafeParam("SysPropertyStore").InnerXml);

                EnhancePolicyManager.CancelPolicy(p_sCancelType, p_sCancelDate, p_sCancelReason);
                base.m_DataChanged = true;
                m_bDataChanged = false;//pmahli EPA 5/10/2007 
            }
            else
                Errors.Add("CancelPolicy", "Policy has been updated by another user. Please re-retrieve the policy.", BusinessAdaptorErrorType.PopupMessage);
            
        }

        private void OnRenewPolicy()
        {
            if (!EnhancePolicyManager.PolicyAlreadyUpdated())
            {
                //pmahli MITS 9841 Policy Management Supplementals -Start
                if (base.m_fda.HasParam("SysPropertyStore"))
                    EnhancePolicyManager.PropertyStore.LoadXml(base.m_fda.SafeParam("SysPropertyStore").InnerXml);

                EnhancePolicyManager.RenewPolicy();
                m_bDataChanged = false;        //pmahli EPA 5/10/2007         
            }
            else
            {
                Errors.Add("CancelPolicy", "Policy has been updated by another user. Please re-retrieve the policy.", BusinessAdaptorErrorType.PopupMessage);
            }
        }

        private void OnReinstatePolicy(string p_ReinsDate, bool p_bReinsWithLapseFlag)
        {
            if (!EnhancePolicyManager.PolicyAlreadyUpdated())
            {
                //pmahli MITS 9841 Policy Management Supplementals -Start
                if (base.m_fda.HasParam("SysPropertyStore"))
                    EnhancePolicyManager.PropertyStore.LoadXml(base.m_fda.SafeParam("SysPropertyStore").InnerXml);

                EnhancePolicyManager.ReinstatePolicy(p_bReinsWithLapseFlag, p_ReinsDate);
                base.m_DataChanged = true;
                m_bDataChanged = false; //pmahli EPA 5/10/2007 
            }
            else
                Errors.Add("ReinstatePolicy", "Policy has been updated by another user. Please re-retrieve the policy.", BusinessAdaptorErrorType.PopupMessage);


        }

        private void OnConvertToPolicy()
        {
            DbReader objReader = null ;
            DbConnection objConn = null;
            
            string sSQL = string.Empty;
            int iQuoteId = 0;

            if (!EnhancePolicyManager.QuoteAlreadyConverted())
            {
                // Change the Quote Status. 
                PolicyEnh.PolicyStatusCode = EnhancePolicyManager.CCacheFunctions.GetCodeIDWithShort("CV", "POLICY_STATUS");
                
                foreach( PolicyXCvgEnh objPolicyXCvgEnh in PolicyEnh.PolicyXCvgEnhList )
                    objPolicyXCvgEnh.Status = EnhancePolicyManager.CCacheFunctions.GetCodeIDWithShort("OK", "COVEXP_STATUS");

                foreach( PolicyXExpEnh objPolicyXExpEnh in PolicyEnh.PolicyXExposureEnhList )
                    objPolicyXExpEnh.Status = EnhancePolicyManager.CCacheFunctions.GetCodeIDWithShort("OK", "COVEXP_STATUS");

                // Save the Quote. 
                base.m_DataChanged = true;
                Save();
                iQuoteId = PolicyEnh.PolicyId;

                //pmahli MITS 9841 Policy Management Supplementals -Start
                if (base.m_fda.HasParam("SysPropertyStore"))
                    EnhancePolicyManager.PropertyStore.LoadXml(base.m_fda.SafeParam("SysPropertyStore").InnerXml);
               //pmahli MITS 9841-End
                // Convert to Policy.
                objData = (DataObject)EnhancePolicyManager.ConvertToPolicy();
                base.m_DataChanged = true;

                // Policy Object has change, reload the EnahncePolicyManager object.
                m_objEnhancePolicyManager = new EnhancePolicyManager(PolicyEnh);

                // Save Policy. 
                base.m_DataChanged = true;   
                Save();

                // Copy all the forms over from the quote
                sSQL = " SELECT * FROM POLICY_X_FORMS_ENH WHERE POLICY_ID=" + iQuoteId;
                objReader = DbFactory.GetDbReader( EnhancePolicyManager.ConnectionString, sSQL);
                objConn = DbFactory.GetDbConnection(EnhancePolicyManager.ConnectionString);
                objConn.Open();

                while (objReader.Read())
                {
                    sSQL = " INSERT INTO POLICY_X_FORMS_ENH(POLICY_ID,FORM_ID) VALUES (" + PolicyEnh.PolicyId + "," + Conversion.ConvertObjToInt(objReader.GetValue("FORM_ID"), base.ClientId) + ")";
                    objConn.ExecuteNonQuery(sSQL);
                }
                objReader.Close();

                if (objConn != null)
                    objConn.Close();

                // Move to Policy. 
                MoveTo(PolicyEnh.PolicyId);
                m_bDataChanged = false; //pmahli EPA 5/10/2007 

            }
            else
            {
                Errors.Add("ConvertToPolicy", "Cannot convert quote. Quote has been converted by another user.", BusinessAdaptorErrorType.PopupMessage);            
            }            
        }

        private int GetLatestTransactionNumberId()
        {
            // TODO : implement Latest Transation Id function. 
            return 0;
        }

        private void UpdateObjectForPolicyQuoteDetails()
        {
            PolicyXTermEnh objPolicyXTermEnhNew = null;
            PolicyXRtngEnh objPolicyXRtngEnhNew = null;
            bool bIsQuote = Conversion.ConvertStrToBool(base.GetSysExDataNodeText("/SysExData//IsQuoteDup", true).ToLower());
            int iPolicyStatus = base.GetSysExDataNodeInt("/SysExData//PolicyStatus/@codeid", true);
            int iTransNumber = base.GetSysExDataNodeInt("/SysExData//SelectedTransactionId", true);
            string sPostBackAction = base.GetSysExDataNodeText("/SysExData//" + EnhancePolicyManager.POST_BACK_ACTION);

            // Set PolicyStatus and Indicator
            if (bIsQuote)
            {
                PolicyEnh.PolicyStatusCode = iPolicyStatus;
                PolicyEnh.PolicyIndicator = EnhancePolicyManager.CCacheFunctions.GetCodeIDWithShort( "Q" , "POLICY_INDICATOR" );
            }
            else
            {
                if (iTransNumber == 0)
                {
                    PolicyEnh.PolicyStatusCode = iPolicyStatus;
                    PolicyEnh.PolicyIndicator = EnhancePolicyManager.CCacheFunctions.GetCodeIDWithShort("P", "POLICY_INDICATOR");
                }
            }
            string sIndicator = EnhancePolicyManager.CCacheFunctions.GetShortCode(PolicyEnh.PolicyIndicator);

            // Set Country
            PolicyEnh.Country = EnhancePolicyManager.CCacheFunctions.GetCodeIDWithShort("USA", "COUNTRY_CODE");

            // Set Non renew Flag
            if (PolicyEnh.NonRenewReason != 0)
            {
                if (EnhancePolicyManager.CCacheFunctions.GetShortCode(PolicyEnh.NonRenewReason) != "")
                    PolicyEnh.NonRenewFlag = true;
                else
                    PolicyEnh.NonRenewFlag = false;
            }

            #region Update Dates
            // Set Effective Date and Expiry date with other Term Data. 
            if (PolicyEnh.PolicyXTermEnhList.Count == 0)
            {
                if (PolicyEnh.PolicyId == 0 && sIndicator == "Q")
                {
                    // We are just saving a new quote , Create a new term record and save the reference to the term
                    objPolicyXTermEnhNew = PolicyEnh.PolicyXTermEnhList.AddNew();
                    this.UpdateObjectTerm(objPolicyXTermEnhNew);
                }
            }
            else
            {
                objPolicyXTermEnhNew = null ;
                foreach (PolicyXTermEnh objPolicyXTermEnh in PolicyEnh.PolicyXTermEnhList)
                {
                    if (PolicyEnh.PolicyXTermEnhList.Count == 1 || objPolicyXTermEnh.TransactionId == iTransNumber)
                    {
                        objPolicyXTermEnhNew = objPolicyXTermEnh;
                        break;
                    }
                }

                if (objPolicyXTermEnhNew != null)
                {
                    if( sPostBackAction != "TRANSACTION_CHANGE" )
                        this.UpdateObjectTerm(objPolicyXTermEnhNew);
                    if (sIndicator == "Q")
                    {
                        foreach (PolicyXExpEnh objPolicyXExpEnh in PolicyEnh.PolicyXExposureEnhList)
                        {
                            if (objPolicyXExpEnh.EffectiveDate != objPolicyXTermEnhNew.EffectiveDate || objPolicyXExpEnh.ExpirationDate != objPolicyXTermEnhNew.ExpirationDate)
                            {
                                objPolicyXExpEnh.ExpirationDate = objPolicyXTermEnhNew.ExpirationDate;
                                objPolicyXExpEnh.EffectiveDate = objPolicyXTermEnhNew.EffectiveDate;
                                //TODO EnhancePolicyManager.Common.Rate( ref objPolicyXExpEnh, PolicyEnh.State, EnhancePolicyManager.Common.RoundFlag , 0 , 0, sIndicator );
                            }
                        }
                        foreach (PolicyXCvgEnh objPolicyXCvgEnh in PolicyEnh.PolicyXCvgEnhList)
                        {
                            objPolicyXCvgEnh.ExpirationDate = objPolicyXTermEnhNew.ExpirationDate;
                            objPolicyXCvgEnh.EffectiveDate = objPolicyXTermEnhNew.EffectiveDate;                            
                        }
                    }
                }
            }
            #endregion 

            #region Update Rating Record
            // Start Naresh (1/4/2007) MITS 9563 Amend Policy, Reload new business data and then endorsement data
            //if (sPostBackAction != "TRANSACTION_CHANGE")
            //{
                // We are just saving a new quote, so create a new record
                if (PolicyEnh.PolicyXRatingEnhList.Count == 0)
                {
                    objPolicyXRtngEnhNew = PolicyEnh.PolicyXRatingEnhList.AddNew();
                    this.UpdateRate(objPolicyXRtngEnhNew);
                }
                else
                {
                    if (sPostBackAction != "TRANSACTION_CHANGE")
                    {
                        foreach (PolicyXRtngEnh objPolicyXRtngEnh in PolicyEnh.PolicyXRatingEnhList)
                            if (objPolicyXRtngEnh.TransactionId == iTransNumber)
                                this.UpdateRate(objPolicyXRtngEnh);
                    }
                    else
                    {
                        // Start Naresh MITS 9791 Transaction Change was not Supposed to be called when the Trans is Cancel
                        string sTransactionType = "";
                        int iTransId = 0;
                        foreach (PolicyXTransEnh objPolicyXTransEnh in PolicyEnh.PolicyXTransEnhList)
                            if (objPolicyXTransEnh.TransactionId > iTransId)
                            {
                                iTransId = objPolicyXTransEnh.TransactionId;
                                sTransactionType = EnhancePolicyManager.LocalCache.GetShortCode(objPolicyXTransEnh.TransactionType);
                            }
                        //pmahli MITS 9870 update rate function avoided in case of Renew transaction change
                        //Update rate function avoided in case of Amend Transaction Change
                        //pmahli MITS 9793 Update rate function avoided in case of Audit Transaction Change
                        //pmahli MITS 9861 Update rate function avoided in case of Reinstate with/without lapse
                        if (sTransactionType != "CPR" && sTransactionType != "CF" && sTransactionType != "RN" && sTransactionType != "EN" && sTransactionType != "AU" && sTransactionType != "RNL" && sTransactionType !="RL")
                        {
                        foreach (PolicyXRtngEnh objPolicyXRtngEnh in PolicyEnh.PolicyXRatingEnhList)
                            if (objPolicyXRtngEnh.TransactionId > iTransId)
                                iTransId = objPolicyXRtngEnh.TransactionId;
                        foreach (PolicyXRtngEnh objPolicyXRtngEnh in PolicyEnh.PolicyXRatingEnhList)
                            if (objPolicyXRtngEnh.TransactionId == iTransNumber && iTransId == iTransNumber)
                                this.UpdateRate(objPolicyXRtngEnh);
                    }
                        // End Naresh MITS 9791 Transaction Change was not Supposed to be called when the Trans is Cancel
                    }
                }
            //}
                // End Naresh (1/4/2007) MITS 9563 Amend Policy, Reload new business data and then endorsement data
            #endregion 

            #region Update AccountId and SubAccRowId
            //mona:marker
            if ( PolicyEnh.Context.InternalSettings.SysSettings.UseFundsSubAcc )
            {
                // DropDown represents SubAccRowId 
            //    PolicyEnh.SubAccRowId = PolicyEnh.BankAccId;
                if (PolicyEnh.SubAccRowId != 0)
                {
                    BankAccSub objBankAccSub = (BankAccSub)EnhancePolicyManager.DataModelFactory.GetDataModelObject("BankAccSub", false);
                    objBankAccSub.MoveTo(PolicyEnh.SubAccRowId);
                    PolicyEnh.BankAccId = objBankAccSub.AccountId;
                }
            }
            #endregion 

        }

        private void UpdateObjectTerm( PolicyXTermEnh p_objPolicyXTermEnh )
        {
            p_objPolicyXTermEnh.EffectiveDate = Conversion.GetDate(base.GetSysExDataNodeText("/SysExData//EffectiveDate", true));
            p_objPolicyXTermEnh.ExpirationDate = Conversion.GetDate(base.GetSysExDataNodeText("/SysExData//ExpirationDate", true));
            p_objPolicyXTermEnh.CancelDate = Conversion.GetDate(base.GetSysExDataNodeText("/SysExData//CancelDate", true));
            p_objPolicyXTermEnh.CancelReason = base.GetSysExDataNodeInt("/SysExData//CancelReason/@codeid", true);
            p_objPolicyXTermEnh.PolicyNumber = base.GetSysExDataNodeText( "/SysExData//PolicyNumber" ,true);
            //npadhy RMSC retrofit Starts
            p_objPolicyXTermEnh.BrokerCommission = Conversion.ConvertStrToDouble(base.GetSysExDataNodeText("/SysExData//BrokerCommission", true));   
            //npadhy RMSC retrofit Ends
        }

        private void UpdateRate(PolicyXRtngEnh p_objPolicyXRtngEnh)
        {
            string sAuditedPremium;
            string sManualPremium;
            string sExpModFactor;
            string sExpModAmount;
            string sModifiedPremium;
            string sTotalBilledPrem;
            string sTotalEstPremium;
            string sExpenseConstant;
            string sTransactionPrem;
            //npadhy RMSC retrofit Starts
            string sTaxAmount = string.Empty;
            string sTranTax = string.Empty;
            string sTaxRate = string.Empty;
            string sTaxType = string.Empty;  
            //npadhy RMSC retrofit Ends

            sAuditedPremium = base.GetSysExDataNodeText(EnhancePolicyManager.SYSEX_DOC_ROOT + EnhancePolicyManager.CALC_PREM_ROOT + "/" + EnhancePolicyManager.CALC_PREM_AUDITED_PREMIUM_ID);
            sManualPremium = base.GetSysExDataNodeText(EnhancePolicyManager.SYSEX_DOC_ROOT + EnhancePolicyManager.CALC_PREM_ROOT + "/" + EnhancePolicyManager.CALC_PREM_MANUAL_PREMIUM_ID);
            sExpModFactor = base.GetSysExDataNodeText(EnhancePolicyManager.SYSEX_DOC_ROOT + EnhancePolicyManager.CALC_PREM_ROOT + "/" + EnhancePolicyManager.CALC_PREM_EXP_MOD_FACTOR_ID);
            sExpModAmount = base.GetSysExDataNodeText(EnhancePolicyManager.SYSEX_DOC_ROOT + EnhancePolicyManager.CALC_PREM_ROOT + "/" + EnhancePolicyManager.CALC_PREM_EXP_MOD_FACTOR_AMOUNT_ID);
            sModifiedPremium = base.GetSysExDataNodeText(EnhancePolicyManager.SYSEX_DOC_ROOT + EnhancePolicyManager.CALC_PREM_ROOT + "/" + EnhancePolicyManager.CALC_PREM_MODIFIED_PREMIUM_ID);
            sTotalBilledPrem = base.GetSysExDataNodeText(EnhancePolicyManager.SYSEX_DOC_ROOT + EnhancePolicyManager.CALC_PREM_ROOT + "/" + EnhancePolicyManager.CALC_PREM_TOTAL_BILLED_PREMIUM_ID);
            sTotalEstPremium = base.GetSysExDataNodeText(EnhancePolicyManager.SYSEX_DOC_ROOT + EnhancePolicyManager.CALC_PREM_ROOT + "/" + EnhancePolicyManager.CALC_PREM_ESTIMATED_PREMIUM_ID);
            sExpenseConstant = base.GetSysExDataNodeText(EnhancePolicyManager.SYSEX_DOC_ROOT + EnhancePolicyManager.CALC_PREM_ROOT + "/" + EnhancePolicyManager.CALC_PREM_EXPENSE_CONST_ID);
            sTransactionPrem = base.GetSysExDataNodeText(EnhancePolicyManager.SYSEX_DOC_ROOT + EnhancePolicyManager.CALC_PREM_ROOT + "/" + EnhancePolicyManager.CALC_PREM_TRANSACTION_PREMIUM_ID);
            //npadhy RMSC retrofit Starts
            sTaxAmount = base.GetSysExDataNodeText(EnhancePolicyManager.SYSEX_DOC_ROOT + EnhancePolicyManager.CALC_PREM_ROOT + "/" + EnhancePolicyManager.CALC_PREM_TAX_ID);
            sTranTax = base.GetSysExDataNodeText(EnhancePolicyManager.SYSEX_DOC_ROOT + EnhancePolicyManager.CALC_PREM_ROOT + "/" + EnhancePolicyManager.CALC_PREM_TRANS_TAX_ID);
            sTaxRate = base.GetSysExDataNodeText(EnhancePolicyManager.SYSEX_DOC_ROOT + EnhancePolicyManager.CALC_PREM_ROOT + "/" + EnhancePolicyManager.CALC_PREM_TAX_RATE_ID);
            sTaxType = base.GetSysExDataNodeText(EnhancePolicyManager.SYSEX_DOC_ROOT + EnhancePolicyManager.CALC_PREM_ROOT + "/" + EnhancePolicyManager.CALC_PREM_HID_TAX_TYPE_ID);
            //npadhy RMSC retrofit Ends

            if (sAuditedPremium != "")
                p_objPolicyXRtngEnh.AuditedPremium = Conversion.ConvertStrToDouble(sAuditedPremium);
            else
                p_objPolicyXRtngEnh.AuditedPremium = 0;

            if (sManualPremium != "")
                p_objPolicyXRtngEnh.ManualPremium = Conversion.ConvertStrToDouble(sManualPremium);
            else
                p_objPolicyXRtngEnh.ManualPremium = 0;
            if (sExpModFactor != "")
                p_objPolicyXRtngEnh.ExpModFactor = Conversion.ConvertStrToDouble(sExpModFactor);
            else
                p_objPolicyXRtngEnh.ExpModFactor = 0;
            if (sExpModAmount != "")
                p_objPolicyXRtngEnh.ExpModAmount = Conversion.ConvertStrToDouble(sExpModAmount);
            else
                p_objPolicyXRtngEnh.ExpModAmount = 0;
            if (sModifiedPremium != "")
                p_objPolicyXRtngEnh.ModifiedPremium = Conversion.ConvertStrToDouble(sModifiedPremium);
            else
                p_objPolicyXRtngEnh.ModifiedPremium = 0;
            if (sTotalBilledPrem != "")
                p_objPolicyXRtngEnh.TotalBilledPrem = Conversion.ConvertStrToDouble(sTotalBilledPrem);
            else
                p_objPolicyXRtngEnh.TotalBilledPrem = 0;
            if (sTotalEstPremium != "")
                p_objPolicyXRtngEnh.TotalEstPremium = Conversion.ConvertStrToDouble(sTotalEstPremium);
            else
                p_objPolicyXRtngEnh.TotalEstPremium = 0;
            if (sExpenseConstant != "")
                p_objPolicyXRtngEnh.ExpenseConstant = Conversion.ConvertStrToDouble(sExpenseConstant);
            else
                p_objPolicyXRtngEnh.ExpenseConstant = 0;
            if (sTransactionPrem != "")
                p_objPolicyXRtngEnh.TransactionPrem = Conversion.ConvertStrToDouble(sTransactionPrem);
            else
                p_objPolicyXRtngEnh.TransactionPrem = 0;
            //npadhy RMSC retrofit Starts
            if (sTaxAmount != "")
                p_objPolicyXRtngEnh.TaxAmount = Conversion.ConvertStrToDouble(sTaxAmount);
            else
                p_objPolicyXRtngEnh.TaxAmount = 0;
            if (sTranTax != "")
                p_objPolicyXRtngEnh.TranTax = Conversion.ConvertStrToDouble(sTranTax);
            else
                p_objPolicyXRtngEnh.TranTax = 0;
            if (sTaxRate != "")
                p_objPolicyXRtngEnh.TaxRate = Conversion.ConvertStrToDouble(sTaxRate);
            else
                p_objPolicyXRtngEnh.TaxRate = 0;
            if (sTaxType != "")
                p_objPolicyXRtngEnh.TaxType  = Conversion.ConvertStrToInteger(sTaxType);
            else
                p_objPolicyXRtngEnh.TaxType = 0;

            //npadhy RMSC retrofit Ends
            p_objPolicyXRtngEnh.DataChanged = true;
        }

        private void UpdateTransaction(PolicyXTransEnh p_objPolicyXTransEnh)
        {
            p_objPolicyXTransEnh.TotalBilledPrem = Conversion.ConvertStrToDouble( base.GetSysExDataNodeText("/SysExData//TotalBilledPremium", true) );
            p_objPolicyXTransEnh.TotalEstPremium = Conversion.ConvertStrToDouble( base.GetSysExDataNodeText("/SysExData//EstimatedPremium", true));
            p_objPolicyXTransEnh.TransactionPrem = Conversion.ConvertStrToDouble(base.GetSysExDataNodeText("/SysExData//TransactionPremium", true));
            p_objPolicyXTransEnh.WaivePremiumInd = Conversion.ConvertStrToBool(base.GetSysExDataNodeText("/SysExData//WaiveTransactionPremium", true));
            p_objPolicyXTransEnh.ChgBilledPremium = Conversion.ConvertStrToDouble(base.GetSysExDataNodeText("/SysExData//changeInBilledPremium", true));            
            //npadhy RMSC retrofit Starts
            p_objPolicyXTransEnh.TaxAmount = Conversion.ConvertStrToDouble(base.GetSysExDataNodeText("/SysExData//Tax", true));
            p_objPolicyXTransEnh.TranTax = Conversion.ConvertStrToDouble(base.GetSysExDataNodeText("/SysExData//Transaction_Tax", true));
            p_objPolicyXTransEnh.TaxRate = Conversion.ConvertStrToDouble(base.GetSysExDataNodeText("/SysExData//TaxRate", true));
            p_objPolicyXTransEnh.TaxType = Conversion.ConvertStrToInteger(base.GetSysExDataNodeText("/SysExData//Tax_Type", true));            
            //npadhy RMSC retrofit Ends
            if (base.SysEx.SelectSingleNode("//PolicyCommentsFlag") != null)
            {
                int iPolicyCommentsFlag = Conversion.ConvertStrToInteger(base.GetSysExDataNodeText("/SysExData//PolicyCommentsFlag", true));
                if(iPolicyCommentsFlag == -1)
                    p_objPolicyXTransEnh.Comments = base.GetSysExDataNodeText("/SysExData//PolicyComments", true);
            }

        }

        #endregion 

        #region OnUpdateForm

        public override void OnUpdateForm()
        {
            bool bBuildFlag = false;
            bool bCalcFlag = false;
            bool bEditFlag = false;
            string sTransactionType = string.Empty;
            string sPostBackAction = string.Empty; //pmahli MITS 9793 Post Back Action captured 

            //Call the base implementation of OnUpdateForm from the FormBase class
            base.OnUpdateForm();

            int iTransNumber = m_iTransNumber;

            // Set the Build , Re-Calculate Preimum Flag and Last Transaction Number .
            // pmahli MITS 9793 sPostBackAction passed in SetPremCalculateScreenFlag as parameter 
            sPostBackAction = base.GetSysExDataNodeText("/SysExData//" + EnhancePolicyManager.POST_BACK_ACTION);
            EnhancePolicyManager.PremiumManager.SetPremCalculateScreenFlag( ref bBuildFlag, ref bCalcFlag, ref bEditFlag, ref iTransNumber, ref sTransactionType, sPostBackAction);
            // Added by Charanpreet : MITS 11398  Start
            if (base.SysEx.SelectSingleNode("//ClaimId") != null)
            {
                int p_iClaimId = Conversion.ConvertStrToInteger(base.GetSysExDataNodeText("/SysExData//ClaimId", true));
                if (p_iClaimId != 0)
                {
                    using (Claim objClaim = (Claim)PolicyEnh.Context.Factory.GetDataModelObject("Claim", false))
                    {
                        objClaim.MoveTo(p_iClaimId);

                        if (PolicyEnh.TriggerClaimFlag == true)
                        {
                            foreach (PolicyXTransEnh objPolicyXTransEnh in PolicyEnh.PolicyXTransEnhList)
                                if (objPolicyXTransEnh.EffectiveDate.CompareTo(objClaim.DateOfClaim) <= 0 && objPolicyXTransEnh.ExpirationDate.CompareTo(objClaim.DateOfClaim) >= 0)
                                    iTransNumber = objPolicyXTransEnh.TransactionId;
                        }
                        else
                        {
                            using (Event objEvent = (Event)PolicyEnh.Context.Factory.GetDataModelObject("Event", false))
                            {
                                objEvent.MoveTo(objClaim.EventId);
                                foreach (PolicyXTransEnh objPolicyXTransEnh in PolicyEnh.PolicyXTransEnhList)
                                    if (objPolicyXTransEnh.EffectiveDate.CompareTo(objEvent.DateOfEvent) <= 0 && objPolicyXTransEnh.ExpirationDate.CompareTo(objEvent.DateOfEvent) >= 0)
                                        iTransNumber = objPolicyXTransEnh.TransactionId;
                            }
                        }                        
                    }
                }                
            }
            // Added by Charanpreet : MITS 11398  End
            // Start Naresh MITS 9565 Added the Screen Title [Policy Number]
            //Apply Record Details to Title
            this.ApplyFormTitle();
            // End Naresh MITS 9565 Added the Screen Title [Policy Number]

            this.UpdatePolicyQuoteDetails(ref iTransNumber);
            
            EnhancePolicyManager.PremiumManager.CreatePremiumCalculationUI(iTransNumber, bBuildFlag, bEditFlag, bCalcFlag, sTransactionType, structEnhancePolicyUI, SysViewSection, SysEx);

            foreach (PolicyXRtngEnh objPolicyXRtngEnh in PolicyEnh.PolicyXRatingEnhList)
                if (objPolicyXRtngEnh.TransactionId == iTransNumber)
                    this.UpdateRate(objPolicyXRtngEnh);

            foreach (PolicyXTransEnh objPolicyXTransEnh in PolicyEnh.PolicyXTransEnhList)
                if (objPolicyXTransEnh.TransactionId == iTransNumber)
                    this.UpdateTransaction(objPolicyXTransEnh);

            EnhancePolicyManager.TransactionManager.RenderTransactionList( base.SysEx , iTransNumber);

            this.RenderMcoListXml();
            //Add by kuladeep for Audit Issue MITS:24736 Start
            this.OnRenderInsuredList();
            //Add by kuladeep for Audit Issue MITS:24736 End//skahre7 RMSC merge

            if (structEnhancePolicyUI.ExposureUI.ExposureEnabled)  //Added by csingh7 MITS 20427
                EnhancePolicyManager.ExposureManager.RenderExposureList(base.SysEx, iTransNumber, objExposuresUniqueIds, objExposureUarSessionIds, base.Adaptor.GetSessionObject(), objScheduleSessionIDs);//Sumit

            if (structEnhancePolicyUI.CoverageUI.CoverageEnabled)  //Added by csingh7 MITS 20427          
                EnhancePolicyManager.CoverageManager.RenderCoverageList(SysEx, iTransNumber, objCoveragesUniqueIds);

            this.UpdateDiscountTierObject(iTransNumber);

            // This code should be moved to a common place. Bank Account 
            // List is being fetched at more than one place in the product.
            this.AppendBankAccountList();

            // Added by csingh7 MITS 20427 Start
            if(!structEnhancePolicyUI.CoverageUI.CoverageEnabled)
                base.AddKillNode(EnhancePolicyManager.CTL_POLICY_COVERAGE_TAB);

            if (!structEnhancePolicyUI.ExposureUI.ExposureEnabled)
                base.AddKillNode(EnhancePolicyManager.CTL_POLICY_EXPOSURE_TAB);
            // Added by csingh7 MITS 20427 End

            base.ResetSysExData(EnhancePolicyManager.POST_BACK_ACTION, "");
            base.ResetSysExData(EnhancePolicyManager.SYSEX_AMEND_POLICY_TRANSACTION_DATE, "");
            base.ResetSysExData(EnhancePolicyManager.SYSEX_AUDIT_POLICY_TERM_NUMBER, "");
            base.ResetSysExData(EnhancePolicyManager.SYSEX_CANCEL_POLICY_DATE, "");
            base.ResetSysExData(EnhancePolicyManager.SYSEX_CANCEL_POLICY_REASON, "");
            base.ResetSysExData(EnhancePolicyManager.SYSEX_CANCEL_POLICY_TYPE, "");
            base.ResetSysExData(EnhancePolicyManager.SYSEX_REINSTATE_POLICY_LAPSE_FLAG, "");
            base.ResetSysExData(EnhancePolicyManager.SYSEX_REINSTATE_POLICY_DATE, "");
            base.ResetSysExData(EnhancePolicyManager.DATA_CHANGED, m_bDataChanged.ToString());//pmahli EPA 5/10/2007 
            base.ResetSysExData("UseBillingFlag", EnhancePolicyManager.UseBillingSystem.ToString());
            // Start rrachev JIRA RMA-4208 
            //base.ResetSysExData("PrimaryPolicy", ""); 
            // End rrachev JIRA RMA-4208

            // Start Naresh MITS 9901 Mail Merge :- Claima Made and Event Made Policy Issue
            base.ResetSysExData("TriggerEventFlag", "");
            base.ResetSysExData("TriggerClaimFlag", "");
            // End Naresh MITS 9901 Mail Merge :- Claima Made and Event Made Policy Issue
            // Start Naresh (6/4/2007) MITS 9511 Populate the Insured details in the Policy Billing Tab
            base.ResetSysExData(EnhancePolicyManager.SYSEX_INSURED_ID, "");
            base.ResetSysExData(EnhancePolicyManager.SYSEX_CONTROL_TYPE, "");
            // End Naresh (6/4/2007) MITS 9511 Populate the Insured details in the Policy Billing Tab
            //umesh      
            string sPreSelectedTransactionId = "";
            sPreSelectedTransactionId = SysEx.SelectSingleNode("//SelectedTransactionId").InnerText;
            base.ResetSysExData("PreSelectedTransactionId", sPreSelectedTransactionId);
            //End umesh
            base.ResetSysExData("PolicyComments", "");//Sumit-MITS#18251 -09/29/2009 -Set hidden field for Policy comments
            base.ResetSysExData("PolicyCommentsFlag", "");//Sumit-MITS#18251 -09/29/2009 -Set hidden field for Policy comments

            //Ashish Ahuja Mits 32489 , 32152 Start
            //Start - averma62 - MISTS 29121 - one more codintion to check null or empty value.
            //if (!PolicyEnh.IsNew && !string.IsNullOrEmpty(structEnhancePolicyUI.PolicyUIData.ExpirationDate) && ((Convert.ToDateTime(Conversion.GetDBDateFormat(structEnhancePolicyUI.PolicyUIData.ExpirationDate, "d")) < System.DateTime.Today) || (PolicyEnh.PolicyStatusCode == EnhancePolicyManager.LocalCache.GetCodeId("C", "POLICY_STATUS")) || (PolicyEnh.PolicyStatusCode == EnhancePolicyManager.LocalCache.GetCodeId("CPR", "POLICY_STATUS")) || (PolicyEnh.PolicyStatusCode == EnhancePolicyManager.LocalCache.GetCodeId("CF", "POLICY_STATUS"))))
            //{
            //    base.AddReadWriteNode("btnAuditPolicy");
            //}
            //else
            //{
            //    base.AddReadOnlyNode("btnAuditPolicy");
            //}
            //End averma62 - MISTS 29121
            if (!PolicyEnh.IsNew && !string.IsNullOrEmpty(structEnhancePolicyUI.PolicyUIData.ExpirationDate) && ((Convert.ToDateTime(Conversion.GetDBDateFormat(structEnhancePolicyUI.PolicyUIData.ExpirationDate, "d")) < System.DateTime.Today) || (structEnhancePolicyUI.PolicyUIData.PolicyStatus == EnhancePolicyManager.LocalCache.GetCodeId("C", "POLICY_STATUS")) || (structEnhancePolicyUI.PolicyUIData.PolicyStatus == EnhancePolicyManager.LocalCache.GetCodeId("CPR", "POLICY_STATUS")) || (structEnhancePolicyUI.PolicyUIData.PolicyStatus == EnhancePolicyManager.LocalCache.GetCodeId("CF", "POLICY_STATUS"))))
            {
                if (base.ReadOnlyNodes.Contains("btnAuditPolicyk"))
                    base.ReadOnlyNodes.Remove("btnAuditPolicyk");

                base.AddReadWriteNode("btnAuditPolicy");
            }
            else
            {
                if (base.ReadWriteNodes.Contains("btnAuditPolicyk"))
                    base.ReadWriteNodes.Remove("btnAuditPolicyk");

                base.AddReadOnlyNode("btnAuditPolicy");
            }

            //Ashish Ahuja Mits 32489 , 32152 End
        }

        private void UpdateDiscountTierObject(int p_iTransNumber)
        {
            string sPostBackAction = string.Empty;
            sPostBackAction = base.GetSysExDataNodeText("/SysExData//" + EnhancePolicyManager.POST_BACK_ACTION);

            // npadhy Start MITS 19030 When the Policy is Reinsated we do not Populate the DiscountTierAmount in the Discount Tier Object.
            // So we need to Update the object from SYSEx
            //pmahli MITS 9793
            //added spostback action "Convert to policy" because so as to incorporate case for quote.
            //added spostback action "CALCULATION PREMIUM CHANGED" so as to incorporate case of exp mod factor
            switch (sPostBackAction)
            {
                case EnhancePolicyManager.ACTION_EXPOSURE_EDIT:
                case EnhancePolicyManager.ACTION_CONVERT_TO_POLICY:
                case EnhancePolicyManager.ACTION_CALCULATION_PREMIUM_CHANGED:
                case EnhancePolicyManager.ACTION_ACCEPT_TRANSACTION:
                    // npadhy End MITS 19030 When the Policy is Reinsated we do not Populate the DiscountTierAmount in the Discount Tier Object.
                    // So we need to Update the object from SYSEx
                    int iTransId = 0;
                    string sTransactionStatus = string.Empty;
                    string sTransactionType = string.Empty;
                    //pmahli MITS 9793 
                    //Since no record of quote will be found in PolicyXTransEnhList 
                    //so for quote UpdateDiscountsTiersObject is called with a different condition only for "Q"
                    string sPolicyIndicatorSC = string.Empty;
                    sPolicyIndicatorSC = EnhancePolicyManager.LocalCache.GetShortCode(PolicyEnh.PolicyIndicator);
                    //Quote
                    if (sPolicyIndicatorSC == "Q")
                    {
                        EnhancePolicyManager.PremiumManager.UpdateDiscountsTiersObject(p_iTransNumber, this.SysEx);
                    }
                    else
                    {
                        foreach (PolicyXTransEnh objPolicyXTransEnh in this.PolicyEnh.PolicyXTransEnhList)
                        {
                            if (objPolicyXTransEnh.TransactionId > iTransId)
                            {
                                iTransId = objPolicyXTransEnh.TransactionId;
                                sTransactionStatus = EnhancePolicyManager.LocalCache.GetShortCode(objPolicyXTransEnh.TransactionStatus);
                                sTransactionType = EnhancePolicyManager.LocalCache.GetShortCode(objPolicyXTransEnh.TransactionType);
                            }
                        }

                        // npadhy Start MITS 19030 When the Policy is Reinsated we do not Populate the DiscountTierAmount in the Discount Tier Object.
                        // So we need to Update the object from SYSEx
                        //pmahli MITS 9793 
                        //cases for Renew and Audit also incorporated so that Discount and Discount Tier info is updated for these cases also
                        if (sTransactionStatus == "PR" && (sTransactionType == "EN" ||
                                                            sTransactionType == "RN" ||
                                                            sTransactionType == "AU" ||
                                                            sTransactionType == "RL" ||
                                                            sTransactionType == "RNL" ||
                                                            sTransactionType == "CPR"))
                        // npadhy End MITS 19030 When the Policy is Reinsated we do not Populate the DiscountTierAmount in the Discount Tier Object.
                        // So we need to Update the object from SYSEx
                        {
                            EnhancePolicyManager.PremiumManager.UpdateDiscountsTiersObject(p_iTransNumber, this.SysEx);
                            foreach (PolicyXDcntEnh objPolicyXDcntEnh in this.PolicyEnh.PolicyXDiscountEnhList)
                            {
                                objPolicyXDcntEnh.Save();
                            }
                            foreach (PolicyXDtierEnh objPolicyXDtierEnh in this.PolicyEnh.PolicyXDiscountTierEnhList)
                            {
                                objPolicyXDtierEnh.Save();
                            }
                        }
                    }
                break;
            }
        }
        // Start Naresh MITS 9565 Added the Screen Title [Policy Number]
        private void ApplyFormTitle()
        {
            string sCaption;
            if (PolicyEnh.IsNew)
                sCaption = "";
            else
            {
                sCaption = " [ " + PolicyEnh.PolicyName + " ]";
            }
            //Naresh: Adding SubTitle to modified controls list
            ArrayList singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singleRow, "id", "formsubtitle");
            base.AddElementToList(ref singleRow, "Text", sCaption);
            base.m_ModifiedControls.Add(singleRow);
        }
        // End Naresh MITS 9565 Added the Screen Title [Policy Number]

        #region MCO List
        private void RenderMcoListXml()
        {
            XmlElement objMcoListElement = null;
            XmlElement objListHeadXmlElement = null;
            XmlElement objOptionXmlElement = null;

            try
            {
                objMcoListElement = (XmlElement)base.SysEx.SelectSingleNode("/SysExData/McoList");
                if (objMcoListElement != null)
                    objMcoListElement.ParentNode.RemoveChild(objMcoListElement);

                this.CreateElement(base.SysEx.DocumentElement, "McoList", ref objMcoListElement);

                // Create HEADER nodes for Grid.
                this.CreateElement(objMcoListElement, "listhead", ref objListHeadXmlElement);
                this.CreateAndSetElement(objListHeadXmlElement, "McoName", "MCO Name");
                this.CreateAndSetElement(objListHeadXmlElement, "BeginDate", "Begin Date");
                this.CreateAndSetElement(objListHeadXmlElement, "EndDate", "End Date");

                foreach (PolicyXMcoEnh objPolicyXMcoEnh in PolicyEnh.PolicyXMcoEnhList)
                {
                    int iIndex = 0;
                    this.CreateElement(objMcoListElement, "option", ref objOptionXmlElement);
                    objOptionXmlElement.SetAttribute("ref", EnhancePolicyManager.INSTANCE_SYSEXDATA_PATH + objOptionXmlElement.ParentNode.LocalName + "/option[" + (iIndex++).ToString() + "]");

                    string sMCODesc = EnhancePolicyManager.LocalCache.GetEntityLastFirstName(objPolicyXMcoEnh.McoEid);
                    string sMCOAbbreviation = EnhancePolicyManager.LocalCache.GetEntityAbbreviation(objPolicyXMcoEnh.McoEid);

                    this.CreateAndSetElement(objOptionXmlElement, "McoName", sMCOAbbreviation + " " + sMCODesc);
                    this.CreateAndSetElement(objOptionXmlElement, "BeginDate", Conversion.GetDBDateFormat(objPolicyXMcoEnh.McoBeginDate, "d"));
                    this.CreateAndSetElement(objOptionXmlElement, "EndDate", Conversion.GetDBDateFormat(objPolicyXMcoEnh.McoEndDate, "d"));
                    this.CreateAndSetElement(objOptionXmlElement, "PolXMcoEnhRowId", objPolicyXMcoEnh.PolXMcoEnhRowId.ToString());
                }
                base.ResetSysExData("SelectedMcoId", "");
            }
            finally
            {
                objMcoListElement = null;
                objListHeadXmlElement = null;
                objOptionXmlElement = null;
            }
        }
        #endregion       

        #region Update Policy/Quote Detials

        private void UpdatePolicyQuoteDetails(ref int p_iTransNumber)
        {
            this.SetPolicyQuoteDetails(ref p_iTransNumber);

            this.SetPolicyQuoteReadOnlyNodes();

            this.SetPolicyQuoteKillNodes();

            this.SetPolicyQuoteEnableNodes();

            this.SetPolicyQuoteVisibleNodes();

            this.SetPolicyQuoteAttributes(p_iTransNumber);

            this.SetPolicyQuoteDataToSysEx();

        }

        private void SetPolicyQuoteVisibleNodes()
        {
            // Show the Controls which were hidden using AddKillNodes
            if (structEnhancePolicyUI.PolicyUI.AuditButtonVisbile)
                base.AddDisplayNode(EnhancePolicyManager.CTL_AUDIT_POLICY);
            if (structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible)
                base.AddDisplayNode(EnhancePolicyManager.CTL_EARNED_PREMIUM);
            if (EnhancePolicyManager.UseBillingSystem)
            {
                base.AddDisplayNode(EnhancePolicyManager.CTL_POLICY_BILLING_TAB);
            }
        }

        private void SetPolicyQuoteEnableNodes()
        {
            // Enabling Controls which are disabled by Add Read Only
            if (structEnhancePolicyUI.PolicyUI.PolicyNameEnabled)
                base.AddReadWriteNode(EnhancePolicyManager.CTL_POLICY_QUOTE_NAME);
            if (structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled)
                base.AddReadWriteNode(EnhancePolicyManager.CTL_POLICY_QUOTE_NUMBER);
            if (structEnhancePolicyUI.PolicyUI.PolicyStatusEnabled)
                base.AddReadWriteNode(EnhancePolicyManager.CTL_POLICY_QUOTE_STATUS);
            if (structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled)
                base.AddReadWriteNode(EnhancePolicyManager.CTL_POLICY_QUOTE_TYPE);
            if (structEnhancePolicyUI.PolicyUI.PolicyStateEnabled)
                base.AddReadWriteNode(EnhancePolicyManager.CTL_POLICY_QUOTE_STATE);
            if (structEnhancePolicyUI.PolicyUI.IssueDateEnabled)
                base.AddReadWriteNode(EnhancePolicyManager.CTL_ISSUE_DATE);
            if (structEnhancePolicyUI.PolicyUI.ReviewDateEnabled)
                base.AddReadWriteNode(EnhancePolicyManager.CTL_REVIEW_DATE);
            if (structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled)
                base.AddReadWriteNode(EnhancePolicyManager.CTL_EFFECTIVE_DATE);
            if (structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled)
                base.AddReadWriteNode(EnhancePolicyManager.CTL_EXPIRATION_DATE);
            if (structEnhancePolicyUI.PolicyUI.CancelReasonEnabled)
                base.AddReadWriteNode(EnhancePolicyManager.CTL_CANCEL_REASON);
            if (structEnhancePolicyUI.PolicyUI.CancelDateEnabled)
                base.AddReadWriteNode(EnhancePolicyManager.CTL_CANCEL_DATE);
            if (structEnhancePolicyUI.PolicyUI.RetroDateEnabled)
                base.AddReadWriteNode(EnhancePolicyManager.CTL_RETRO_DATE);
            if (structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled)
                base.AddReadWriteNode(EnhancePolicyManager.CTL_NON_RENEW_REASON);

            if (structEnhancePolicyUI.PolicyUI.InsurerEnabled)
            {
                base.AddReadWriteNode(EnhancePolicyManager.CTL_INSURER);
                base.AddReadWriteNode(EnhancePolicyManager.CTL_INSURER_CONTACT);
                base.AddReadWriteNode(EnhancePolicyManager.CTL_INSURER_ADDRESS1);
                base.AddReadWriteNode(EnhancePolicyManager.CTL_INSURER_ADDRESS2);
                base.AddReadWriteNode(EnhancePolicyManager.CTL_INSURER_ADDRESS3);
                base.AddReadWriteNode(EnhancePolicyManager.CTL_INSURER_ADDRESS4);
                base.AddReadWriteNode(EnhancePolicyManager.CTL_INSURER_CITY);
                base.AddReadWriteNode(EnhancePolicyManager.CTL_INSURER_STATE);
                base.AddReadWriteNode(EnhancePolicyManager.CTL_INSURER_ZIP);
                base.AddReadWriteNode(EnhancePolicyManager.CTL_INSURER_COUNTRY);
                base.AddReadWriteNode(EnhancePolicyManager.CTL_INSURER_OFFICE_PHONE);
                base.AddReadWriteNode(EnhancePolicyManager.CTL_INSURER_ALT_PHONE);
                base.AddReadWriteNode(EnhancePolicyManager.CTL_INSURER_FAX);
            }

            if (structEnhancePolicyUI.PolicyUI.BrokerEnabled)
            {
                base.AddReadWriteNode(EnhancePolicyManager.CTL_BROKER_LAST_NAME);
                base.AddReadWriteNode(EnhancePolicyManager.CTL_BROKER_FIRST_NAME);
                base.AddReadWriteNode(EnhancePolicyManager.CTL_BROKER_ADDRESS1);
                base.AddReadWriteNode(EnhancePolicyManager.CTL_BROKER_ADDRESS2);
                base.AddReadWriteNode(EnhancePolicyManager.CTL_BROKER_ADDRESS3);
                base.AddReadWriteNode(EnhancePolicyManager.CTL_BROKER_ADDRESS4);
                base.AddReadWriteNode(EnhancePolicyManager.CTL_BROKER_CITY);
                base.AddReadWriteNode(EnhancePolicyManager.CTL_BROKER_STATE);
                base.AddReadWriteNode(EnhancePolicyManager.CTL_BROKER_ZIP);
                base.AddReadWriteNode(EnhancePolicyManager.CTL_BROKER_COUNTRY);
                base.AddReadWriteNode(EnhancePolicyManager.CTL_BROKER_OFFICE_PHONE);
                base.AddReadWriteNode(EnhancePolicyManager.CTL_BROKER_ALT_PHONE);
                base.AddReadWriteNode(EnhancePolicyManager.CTL_BROKER_FAX);
                base.AddReadWriteNode(EnhancePolicyManager.CTL_BROKER_EMAIL_TYPE);
                base.AddReadWriteNode(EnhancePolicyManager.CTL_BROKER_EMAIL_ADD);
                base.AddReadWriteNode(EnhancePolicyManager.CTL_BROKER_TAX_ID);
                base.AddReadWriteNode(EnhancePolicyManager.CTL_BROKER_BROKER_FIRM);
            }


        }

        private void SetPolicyQuoteDetails(ref int p_iTransNumber)
        {
            
            PolicyXTermEnh objPolicyXTermEnhProper = null;
            PolicyXTransEnh objPolicyXTransEnhProper = null;

            string sPolicyIndicatorSC = string.Empty;
            bool bFound = false;
            string sPolicyStatus = "";
            string sPolicyType = "";

            if( PolicyEnh.PolicyIndicator != 0 )
            sPolicyIndicatorSC = EnhancePolicyManager.LocalCache.GetShortCode(PolicyEnh.PolicyIndicator);

            if (sPolicyIndicatorSC == "Q" || sPolicyIndicatorSC == "")
                this.SetLabelAsQuotes();
            else
                this.SetLabelAsPolicy();
            
            structEnhancePolicyUI.PolicyUIData.PolicyType = PolicyEnh.PolicyType;// EnhancePolicyManager.CCacheFunctions.GetCodeIDWithShort("AL", "POLICY_LOB");

            switch (sPolicyIndicatorSC)
            {
                // New record.
                case "":
                    structEnhancePolicyUI.PolicyUIData.PolicyStatus = EnhancePolicyManager.CCacheFunctions.GetCodeIDWithShort("Q", "POLICY_STATUS");
                    break;
                // Quote
                case "Q":
                    // Get the Last term.
                    if (base.m_fda.SafeFormVariableParamText("SysCmd") != "6")
                    {
                        objPolicyXTermEnhProper = (PolicyXTermEnh)PolicyXTermEnhSorted.GetByIndex(PolicyXTermEnhSorted.Count - 1);
                        structEnhancePolicyUI.PolicyUIData.PolicyStatus = PolicyEnh.PolicyStatusCode;
                        structEnhancePolicyUI.PolicyUIData.PolicyNumber = objPolicyXTermEnhProper.PolicyNumber;
                        structEnhancePolicyUI.PolicyUIData.ExpirationDate = objPolicyXTermEnhProper.ExpirationDate;
                        structEnhancePolicyUI.PolicyUIData.EffectiveDate = objPolicyXTermEnhProper.EffectiveDate;
                        structEnhancePolicyUI.PolicyUIData.CancelDate = objPolicyXTermEnhProper.CancelDate;
                        structEnhancePolicyUI.PolicyUIData.CancelReason = objPolicyXTermEnhProper.CancelReason;
                        //npadhy RMSC retrofit Starts
                        structEnhancePolicyUI.PolicyUIData.BrokerCommission = objPolicyXTermEnhProper.BrokerCommission;      
                        //npadhy RMSC retrofit Ends
                    }
                    break;
                // Policy
                default:
                    if (base.m_fda.SafeFormVariableParamText("SysCmd") != "6")
                    {
                        if (p_iTransNumber == 0)
                            p_iTransNumber = EnhancePolicyManager.GetLatestTran(PolicyEnh, true).TransactionId;

                        // loop through terms to find right one
                        foreach (PolicyXTermEnh objPolicyXTermEnh in PolicyEnh.PolicyXTermEnhList)
                        {
                            if (objPolicyXTermEnh.TransactionId == p_iTransNumber)
                            {
                                objPolicyXTermEnhProper = objPolicyXTermEnh;
                                bFound = true;
                                break;
                            }
                        }

                        if (!bFound)
                        {
                            foreach (PolicyXTransEnh objPolicyXTransEnh in PolicyEnh.PolicyXTransEnhList)
                            {
                                if (objPolicyXTransEnh.TransactionId == p_iTransNumber)
                                {
                                    objPolicyXTransEnhProper = objPolicyXTransEnh;
                                    break;
                                }
                            }
                            foreach (PolicyXTermEnh objPolicyXTermEnh in PolicyEnh.PolicyXTermEnhList)
                            {
                                if (objPolicyXTermEnh.TermNumber == objPolicyXTransEnhProper.TermNumber && objPolicyXTermEnh.SequenceAlpha == objPolicyXTransEnhProper.TermSeqAlpha)
                                {
                                    objPolicyXTermEnhProper = objPolicyXTermEnh;
                                    break;
                                }
                            }
                        }

                        structEnhancePolicyUI.PolicyUIData.PolicyNumber = objPolicyXTermEnhProper.PolicyNumber;
                        structEnhancePolicyUI.PolicyUIData.ExpirationDate = objPolicyXTermEnhProper.ExpirationDate;
                        structEnhancePolicyUI.PolicyUIData.EffectiveDate = objPolicyXTermEnhProper.EffectiveDate;
                        structEnhancePolicyUI.PolicyUIData.CancelDate = objPolicyXTermEnhProper.CancelDate;
                        structEnhancePolicyUI.PolicyUIData.CancelReason = objPolicyXTermEnhProper.CancelReason;
                        //npadhy RMSC retrofit Starts
                        structEnhancePolicyUI.PolicyUIData.BrokerCommission = objPolicyXTermEnhProper.BrokerCommission;     
                        //npadhy RMSC retrofit Ends

                        if (sPolicyIndicatorSC == "P")
                        {
                            foreach (PolicyXTransEnh objPolicyXTransEnh in PolicyEnh.PolicyXTransEnhList)
                            {
                                if (objPolicyXTransEnh.TransactionId == p_iTransNumber)
                                {
                                    structEnhancePolicyUI.PolicyUIData.PolicyStatus = objPolicyXTransEnh.PolicyStatus;
                                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumSelected = objPolicyXTransEnh.WaivePremiumInd;
                                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumComments = objPolicyXTransEnh.WaivePremComment;
                                    break;
                                }
                            }
                        }
                    }
                        break;                   
            }

            if (sPolicyIndicatorSC == "")
            {
                sPolicyStatus = "Q";
                //sPolicyType = "";
                EnhancePolicyManager.LocalCache.GetShortCode(PolicyEnh.PolicyType); //"AL";
            }
            else
            {
                if (base.m_fda.SafeFormVariableParamText("SysCmd") != "6")
                {
                    if (sPolicyIndicatorSC == "Q")
                    {
                        sPolicyStatus = EnhancePolicyManager.LocalCache.GetShortCode(PolicyEnh.PolicyStatusCode);
                        //sPolicyType = "";
                        EnhancePolicyManager.LocalCache.GetShortCode(PolicyEnh.PolicyType); //"AL";
                    }
                    else
                    {
                        foreach (PolicyXTransEnh objPolicyXTransEnh in PolicyEnh.PolicyXTransEnhList)
                        {
                            if (objPolicyXTransEnh.TransactionId == p_iTransNumber)
                            {
                                sPolicyStatus = EnhancePolicyManager.LocalCache.GetShortCode(objPolicyXTransEnh.PolicyStatus);
                                sPolicyType = EnhancePolicyManager.LocalCache.GetShortCode(objPolicyXTransEnh.TransactionType);
                                break;
                            }

                        }
                    }
                }
            }

            this.SetEntityFieldsDetails(sPolicyStatus, sPolicyType, p_iTransNumber);

        }

        private void SetLabelAsPolicy()
        {
            structEnhancePolicyUI.PolicyUI.PolicyTabCaption = "Policy";
            structEnhancePolicyUI.PolicyUI.NameCaption = "-Policy Name";
            structEnhancePolicyUI.PolicyUI.NumberCaption = "Policy Number";
            structEnhancePolicyUI.PolicyUI.StatusCaption = "-Policy Status";
            structEnhancePolicyUI.PolicyUI.TypeCaption = "Policy Type";
            structEnhancePolicyUI.PolicyUI.StateCaption = "Policy State";
            structEnhancePolicyUI.PolicyUI.PrintCaption = "Print Policy";

            structEnhancePolicyUI.PolicyUI.IsQuote = false;
        }

        private void SetLabelAsQuotes()
        {
            structEnhancePolicyUI.PolicyUI.PolicyTabCaption = "Quote";
            structEnhancePolicyUI.PolicyUI.NameCaption = "-Quote Name";
            structEnhancePolicyUI.PolicyUI.NumberCaption = "Quote Number";
            structEnhancePolicyUI.PolicyUI.StatusCaption = "-Quote Status";
            structEnhancePolicyUI.PolicyUI.TypeCaption = "Quote Type";
            structEnhancePolicyUI.PolicyUI.StateCaption = "Quote State";
            structEnhancePolicyUI.PolicyUI.PrintCaption = "Print Quote";

            structEnhancePolicyUI.PolicyUI.IsQuote = true;

            structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;
            structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
        }

        protected virtual void SetEntityFieldsDetails(string p_sPolicyStatus, string p_sPolicyType, int p_iTransNumber)
        {
            DbReader objReader = null;
            DbReader objReader2 = null;
            string sSQL = string.Empty;
            bool bDisableAll = false;

            int iGreatestTrans = 0;
            string sOtherType = string.Empty;
            string sOtherStatus = string.Empty;
            string sTransactionStatus = string.Empty;

            bool bEditBroker = false;
            bool bEditInsurer = false;

            // Check authority for insure and broker.
            if (EntityAccessCheck())
            {
                if (PolicyEnh.InsurerEntity.EntityId == 0)
                {
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_PEOPMAINT, RMO_CREATE))
                        bEditInsurer = true;
                }
                else
                {
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_PEOPMAINT, RMO_UPDATE))
                        bEditInsurer = true;
                }
            }
            if (EntityAccessCheck())
            {
                if (PolicyEnh.BrokerEntity.EntityId == 0)
                {
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_PEOPMAINT, RMO_CREATE))
                        bEditBroker = true;
                }
                else
                {
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_PEOPMAINT, RMO_UPDATE))
                        bEditBroker = true;
                }
            }

            structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = false;
            structEnhancePolicyUI.CoverageUI.DeleteCoverageEnabled = false;
            structEnhancePolicyUI.ExposureUI.AddExposureEnabled = false;
            structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled = false;
            structEnhancePolicyUI.PolicyUI.PolicyStatusEnabled = false;

            // Need to find the most recent txn that is not an audit.
            // If that is not tran num we have then we need to disable all fields.

            if (!(p_iTransNumber == 0) && !(PolicyEnh.PolicyXTransEnhList.Count == 0) && !(PolicyEnh.PolicyId == 0))
            {
                sSQL = " SELECT * FROM POLICY_X_TRANS_ENH WHERE POLICY_ID=" + PolicyEnh.PolicyId
                    + " ORDER BY TRANSACTION_ID DESC ";
                objReader = PolicyEnh.Context.DbConnLookup.ExecuteReader(sSQL);

                while (objReader.Read())
                {
                    iGreatestTrans = Conversion.ConvertObjToInt(objReader.GetValue("TRANSACTION_ID"), base.ClientId);
                    sOtherType = EnhancePolicyManager.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objReader.GetValue("TRANSACTION_TYPE"), base.ClientId));
                    sOtherStatus = EnhancePolicyManager.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objReader.GetValue("TRANSACTION_STATUS"), base.ClientId));

                    if (sOtherType == "AU")
                    {
                        if (p_iTransNumber != iGreatestTrans)
                        {
                            if (sOtherStatus == "PR")
                            {
                                structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                                bDisableAll = true;
                                break;
                            }
                        }
                        else
                        {
                            p_sPolicyStatus = "AU";
                            break;
                        }
                    }
                    else
                    {
                        if (p_iTransNumber != iGreatestTrans)
                        {
                            if (sOtherStatus != "PR")
                                structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = true;
                            else
                                structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                            bDisableAll = true;
                            break;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                objReader.Close();
            }

            if (bDisableAll)
            {
                #region Disabled All
                structEnhancePolicyUI.PolicyUI.PolicyNameEnabled = false;
                structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled = false;
                structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled = false;
                structEnhancePolicyUI.PolicyUI.PolicyStateEnabled = false;

                structEnhancePolicyUI.PolicyUI.IssueDateEnabled = true;
                structEnhancePolicyUI.PolicyUI.ReviewDateEnabled = true;
                structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled = false;
                structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled = false;
                structEnhancePolicyUI.PolicyUI.RetroDateEnabled = true;

                structEnhancePolicyUI.PolicyUI.CancelReasonEnabled = false;
                structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = false;
                // Shruti
                structEnhancePolicyUI.PolicyUI.MCOEnabled = false;
                structEnhancePolicyUI.PolicyUI.ConvertQuoteEnabled = false;
                structEnhancePolicyUI.PolicyUI.ReinstatePolicyEnabled = false;
                structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = false;
                structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumEnabled = false;
                structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible = false;
                structEnhancePolicyUI.PolicyUI.AcceptTransacationEnabled = false;
                structEnhancePolicyUI.PolicyUI.DeleteTransactionEnabled = false;
                structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = false;
                structEnhancePolicyUI.CoverageUI.DeleteCoverageEnabled = false;
                structEnhancePolicyUI.CoverageUI.EditCoverageEnabled = true;
                structEnhancePolicyUI.ExposureUI.AddExposureEnabled = false;
                structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled = false;
                structEnhancePolicyUI.ExposureUI.EditExposureEnabled = true;
                structEnhancePolicyUI.McoUI.AddMcoEnabled = true;
                structEnhancePolicyUI.McoUI.DeleteMcoEnabled = true;
                structEnhancePolicyUI.McoUI.EditMcoEnabled = true;

                structEnhancePolicyUI.PolicyUI.BrokerEnabled = bEditBroker;
                structEnhancePolicyUI.PolicyUI.InsurerEnabled = bEditInsurer;

                structEnhancePolicyUI.PolicyUI.PrimaryPolicyEnabled = false;
                structEnhancePolicyUI.PolicyUI.CoverageEnabled = false;

                structEnhancePolicyUI.PolicyUI.BankAccountEnabled = true;
                //MGaba2:MITS 11802
                //structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = true;
                structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_PRINT);
                if (p_sPolicyType == "EN")
                {
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = true;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = true;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = true;
                }
                else
                {
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = false;
                }
                structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;

                #endregion
                return;
            }

            foreach (PolicyXTransEnh objPolicyXTransEnh in PolicyEnh.PolicyXTransEnhList)
            {
                if (objPolicyXTransEnh.TransactionId == p_iTransNumber)
                {
                    sTransactionStatus = EnhancePolicyManager.LocalCache.GetShortCode(objPolicyXTransEnh.TransactionStatus);
                    break;
                }
            }

            switch (p_sPolicyStatus)
            {
                // New 
                case "":
                    #region New
                    structEnhancePolicyUI.PolicyUI.PolicyNameEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyStateEnabled = true;

                    structEnhancePolicyUI.PolicyUI.IssueDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ReviewDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.RetroDateEnabled = true;

                    structEnhancePolicyUI.PolicyUI.CancelReasonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;
                    // Shruti
                    structEnhancePolicyUI.PolicyUI.MCOEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ConvertQuoteEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReinstatePolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.BrokerEnabled = bEditBroker;
                    structEnhancePolicyUI.PolicyUI.InsurerEnabled = bEditInsurer;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible = false;
                    structEnhancePolicyUI.PolicyUI.PrimaryPolicyEnabled = true;
                    structEnhancePolicyUI.PolicyUI.CoverageEnabled = true;

                    //Commented by csingh7 : MITS 20427 Start
                    //if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_COV_ENH, RMO_ACCESS) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_COV_ENH, RMO_VIEW) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_COV_ENH, RMO_CREATE))
                    //    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = true;
                    //else
                    //    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = false;
                    //Commented by csingh7 : MITS 20427 End
                    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_COV_ENH, RMO_CREATE);   //Added by csingh7 MITS 20427
                    structEnhancePolicyUI.CoverageUI.DeleteCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_COV_ENH, RMO_DELETE);
                    structEnhancePolicyUI.CoverageUI.EditCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_COV_ENH, RMO_UPDATE);

                    //Commented by csingh7 : MITS 20427 Start
                    //if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_ACCESS) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_VIEW) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_CREATE))
                    //    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = true;
                    //else
                    //    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = false;
                    //Commented by csingh7 : MITS 20427 End
                    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_CREATE);   //Added by csingh7 : MITS 20427
                    structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_DELETE);
                    structEnhancePolicyUI.ExposureUI.EditExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_UPDATE);

                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = false;
                    structEnhancePolicyUI.McoUI.AddMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.DeleteMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.EditMcoEnabled = true;
                    structEnhancePolicyUI.PolicyUI.BankAccountEnabled = true;
                    //MGaba2:MITS 11802
                    //structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_PRINT);
                    if( EnhancePolicyManager.CCacheFunctions.GetShortCode( PolicyEnh.PolicyType ) == "GL" )
                        structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;
                    else
                        structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = true;
                    break;
                    #endregion
                // Audit
                case "AU":
                    #region  Audit
                    structEnhancePolicyUI.PolicyUI.PolicyNameEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyStateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.IssueDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReviewDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RetroDateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.CancelReasonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = false;

                    structEnhancePolicyUI.PolicyUI.ReinstatePolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = true;
                    // Shruti
                    structEnhancePolicyUI.PolicyUI.MCOEnabled = true;
                    if (sTransactionStatus == "PR")
                    {
                        structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                        structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = false;

                        //Commented by csingh7 : MITS 20427 Start
                        //if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_COV_ENH, RMO_ACCESS) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_COV_ENH, RMO_VIEW) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_COV_ENH, RMO_CREATE))
                        //    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = true;
                        //else
                        //    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = false;
                        //Commented by csingh7 : MITS 20427 End
                        structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_COV_ENH, RMO_CREATE);   //Added by csingh7 : MITS 20427
                        structEnhancePolicyUI.CoverageUI.DeleteCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_COV_ENH, RMO_DELETE);
                        structEnhancePolicyUI.CoverageUI.EditCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_COV_ENH, RMO_UPDATE);

                        //Commented by csingh7 : MITS 20427 Start
                        //if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_ACCESS) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_VIEW) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_CREATE))
                        //    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = true;
                        //else
                        //    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = false;
                        //Commented by csingh7 : MITS 20427 End
                        structEnhancePolicyUI.ExposureUI.AddExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_CREATE);   //Added by csingh7 : MITS 20427
                        structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_DELETE);
                        structEnhancePolicyUI.ExposureUI.EditExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_UPDATE);

                        structEnhancePolicyUI.PolicyUI.AcceptTransacationEnabled = true;
                        structEnhancePolicyUI.PolicyUI.DeleteTransactionEnabled = true;
                        structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                        structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = true;
                        structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = true;
                        structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = true;
                        structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = true;
                        structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                        if (EnhancePolicyManager.CCacheFunctions.GetShortCode(PolicyEnh.PolicyType) == "GL")
                            structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;
                        else
                            structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = true;
                    }
                    else
                    {
                        structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = false;
                        structEnhancePolicyUI.CoverageUI.DeleteCoverageEnabled = false;
                        structEnhancePolicyUI.ExposureUI.AddExposureEnabled = false;
                        structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled = false;
                        structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                        structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = false;
                        structEnhancePolicyUI.PolicyUI.AcceptTransacationEnabled = false;
                        structEnhancePolicyUI.PolicyUI.DeleteTransactionEnabled = false;
                        structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                        structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                        structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = true;
                        structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = true;
                        structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = true;
                        if (EnhancePolicyManager.LocalCache.GetShortCode(PolicyEnh.PolicyType) == "WC")
                            structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_AUDIT);
                        else
                            structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                        
                        structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;                        
                    }

                    structEnhancePolicyUI.PolicyUI.BrokerEnabled = bEditBroker;
                    structEnhancePolicyUI.PolicyUI.InsurerEnabled = bEditInsurer;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible = false;
                    structEnhancePolicyUI.PolicyUI.PrimaryPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CoverageEnabled = false;
                    structEnhancePolicyUI.McoUI.AddMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.DeleteMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.EditMcoEnabled = true;
                    structEnhancePolicyUI.PolicyUI.BankAccountEnabled = true;
                    //MGaba2:MITS 11802
                   // structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = true;
                     structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled =UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_PRINT);

                    break;
                    #endregion
                // Quote
                case "Q":
                    #region Quote
                    structEnhancePolicyUI.PolicyUI.PolicyNameEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyStateEnabled = true;

                    structEnhancePolicyUI.PolicyUI.IssueDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ReviewDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.RetroDateEnabled = true;

                    structEnhancePolicyUI.PolicyUI.CancelReasonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = false;

                    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;
                    // Shruti
                    structEnhancePolicyUI.PolicyUI.MCOEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ConvertQuoteEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_CONVERT);
                    structEnhancePolicyUI.PolicyUI.ReinstatePolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = false;

                    structEnhancePolicyUI.PolicyUI.BrokerEnabled = bEditBroker;
                    structEnhancePolicyUI.PolicyUI.InsurerEnabled = bEditInsurer;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible = false;
                    structEnhancePolicyUI.PolicyUI.PrimaryPolicyEnabled = true;
                    structEnhancePolicyUI.PolicyUI.CoverageEnabled = true;

                    //Commented by csingh7 : MITS 20427 Start
                    //if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_COV_ENH, RMO_ACCESS) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_COV_ENH, RMO_VIEW) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_COV_ENH, RMO_CREATE))
                    //    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = true;
                    //else
                    //    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = false;
                    //Commented by csingh7 : MITS 20427 End
                    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_COV_ENH, RMO_CREATE);   //Added by csingh7 : MITS 20427
                    structEnhancePolicyUI.CoverageUI.DeleteCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_COV_ENH, RMO_DELETE);
                    structEnhancePolicyUI.CoverageUI.EditCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_COV_ENH, RMO_UPDATE);

                    //Commented by csingh7 : MITS 20427 Start
                    //if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_ACCESS) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_VIEW) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_CREATE))
                    //    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = true;
                    //else
                    //    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = false;
                    //Commented by csingh7 : MITS 20427 End
                    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_CREATE);   //Added by csingh7 : MITS 20427
                    structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_DELETE);
                    structEnhancePolicyUI.ExposureUI.EditExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_UPDATE);

                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = false;
                    structEnhancePolicyUI.McoUI.AddMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.DeleteMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.EditMcoEnabled = true;
                    structEnhancePolicyUI.PolicyUI.BankAccountEnabled = true;
                    //MGaba2:MITS 11802
                    //structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_PRINT);
                    if (EnhancePolicyManager.CCacheFunctions.GetShortCode(PolicyEnh.PolicyType) == "GL")
                        structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;
                    else
                        structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = true;
                    break;
                    #endregion
                // Converted Quote
                case "CV":
                    #region Converted Quote
                    structEnhancePolicyUI.PolicyUI.PolicyNameEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyStateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.IssueDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReviewDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RetroDateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.CancelReasonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = false;

                    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;

                    // Shruti
                    structEnhancePolicyUI.PolicyUI.MCOEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ConvertQuoteEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReinstatePolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AcceptTransacationEnabled = false;
                    structEnhancePolicyUI.PolicyUI.DeleteTransactionEnabled = false;

                    structEnhancePolicyUI.PolicyUI.BrokerEnabled = false;
                    structEnhancePolicyUI.PolicyUI.InsurerEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible = false;
                    structEnhancePolicyUI.PolicyUI.PrimaryPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CoverageEnabled = false;

                    structEnhancePolicyUI.CoverageUI.EditCoverageEnabled = true;
                    structEnhancePolicyUI.ExposureUI.EditExposureEnabled = true;

                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = false;
                    structEnhancePolicyUI.McoUI.AddMcoEnabled = false;
                    structEnhancePolicyUI.McoUI.DeleteMcoEnabled = false;
                    structEnhancePolicyUI.McoUI.EditMcoEnabled = false;
                    //Divya -03/23/2007 Bank Account should be enabled for coverted quote
                    structEnhancePolicyUI.PolicyUI.BankAccountEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;
                    
                    break;
                    #endregion
                // In Effect
                case "I":
                    #region In Effect
                    structEnhancePolicyUI.PolicyUI.PolicyNameEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyStateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.IssueDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ReviewDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RetroDateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.CancelReasonEnabled = false;
                    if (p_sPolicyType == "AU")
                        structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = false;
                    else
                        structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = true;

                    if (EnhancePolicyManager.LocalCache.GetShortCode(PolicyEnh.PolicyType) == "WC")
                        structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = true;
                    else
                        structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;
                    // Shruti
                    structEnhancePolicyUI.PolicyUI.MCOEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ConvertQuoteEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReinstatePolicyEnabled = false;

                    if (sTransactionStatus == "PR")
                    {
                        structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                        structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = false;

                        //Commented by csingh7 : MITS 20427 Start
                        //if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_COV_ENH, RMO_ACCESS) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_COV_ENH, RMO_VIEW) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_COV_ENH, RMO_CREATE))
                        //    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = true;
                        //else
                        //    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = false;
                        //Commented by csingh7 : MITS 20427 End
                        structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_COV_ENH, RMO_CREATE);   //Added by csingh7 : MITS 20427
                        structEnhancePolicyUI.CoverageUI.DeleteCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_COV_ENH, RMO_DELETE);
                        structEnhancePolicyUI.CoverageUI.EditCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_COV_ENH, RMO_UPDATE);

                        //Commented by csingh7 : MITS 20427 Start
                        //if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_ACCESS) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_VIEW) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_CREATE))
                        //    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = true;
                        //else
                        //    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = false;
                        //Commented by csingh7 : MITS 20427 End
                        structEnhancePolicyUI.ExposureUI.AddExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_CREATE);   //Added by csingh7 : MITS 20427
                        structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_DELETE);
                        structEnhancePolicyUI.ExposureUI.EditExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_UPDATE);

                        structEnhancePolicyUI.PolicyUI.AcceptTransacationEnabled = true;
                        structEnhancePolicyUI.PolicyUI.DeleteTransactionEnabled = true;
                        structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                        structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = true;
                        structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = true;
                        structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = true;
                        structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = true;
                        structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    }
                    else
                    {
                        structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = false;
                        structEnhancePolicyUI.CoverageUI.DeleteCoverageEnabled = false;
                        structEnhancePolicyUI.ExposureUI.AddExposureEnabled = false;
                        structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled = false;
                        //pmahli MITS 9773 Coverage and Exposure Edit button enabled in case of acccepted transaction
                        structEnhancePolicyUI.CoverageUI.EditCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_COV_ENH, RMO_UPDATE);
                        structEnhancePolicyUI.ExposureUI.EditExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_UPDATE);
                        if (p_sPolicyType != "AU")
                        {
                            structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_AMEND);
                            //MGaba2:MITS 15382
                            //structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_AMEND);
                            structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_CANCEL);
                        }
                        else
                        {
                            structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                            structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = false;
                        }

                        structEnhancePolicyUI.PolicyUI.AcceptTransacationEnabled = false;
                        structEnhancePolicyUI.PolicyUI.DeleteTransactionEnabled = false;

                        if (p_sPolicyType != "AU")
                        {
                            if (PolicyEnh.NonRenewFlag)
                                structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                            else
                                structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_RENEW);
                        }
                        else
                        {
                            sSQL = " SELECT TERM_NUMBER FROM POLICY_X_TRANS_ENH WHERE TRANSACTION_ID = " + p_iTransNumber;
                            objReader = PolicyEnh.Context.DbConnLookup.ExecuteReader(sSQL);
                            if (objReader.Read())
                            {
                                sSQL = " SELECT * FROM POLICY_X_TERM_ENH WHERE POLICY_ID=" + PolicyEnh.PolicyId
                                    + " AND TERM_NUMBER=" + (Conversion.ConvertObjToInt(objReader.GetValue("TERM_NUMBER"), base.ClientId) + 1);

                                objReader2 = PolicyEnh.Context.DbConnLookup.ExecuteReader(sSQL);
                                if (objReader2.Read())
                                    structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                                else
                                    structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_RENEW);

                                objReader2.Close();
                            }
                            objReader.Close();
                        }
                        if (p_sPolicyType == "EN" || p_sPolicyType == "AU")
                        {
                            structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                            structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = true;
                            structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = true;
                            structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = true;
                        }
                        else
                        {
                            structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                            structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = false;
                            structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = false;
                            structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = false;
                        }
                        if (EnhancePolicyManager.LocalCache.GetShortCode(PolicyEnh.PolicyType) == "WC")
                            structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_AUDIT);
                        else
                            structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    }
                    structEnhancePolicyUI.PolicyUI.BrokerEnabled = bEditBroker;
                    structEnhancePolicyUI.PolicyUI.InsurerEnabled = bEditInsurer;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumEnabled = true;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible = true;
                    structEnhancePolicyUI.PolicyUI.PrimaryPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CoverageEnabled = false;

                    structEnhancePolicyUI.McoUI.AddMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.DeleteMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.EditMcoEnabled = true;
                    //MGaba2:MITS 11802
                    //structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_PRINT);
                    structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;
                    //Mukul(07/09/2007) Added MITS 9973
                    structEnhancePolicyUI.PolicyUI.BankAccountEnabled = true;
                    break;
                    #endregion
                // Revoked
                case "R":
                    #region Revoked
                    structEnhancePolicyUI.PolicyUI.PolicyNameEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyStateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.IssueDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ReviewDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RetroDateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.CancelReasonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = true;

                    if (EnhancePolicyManager.LocalCache.GetShortCode(PolicyEnh.PolicyType) == "WC")
                    {
                        structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_AUDIT);
                        structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = true;
                    }
                    else
                    {
                        structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                        structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;
                    }
                    // Shruti
                    structEnhancePolicyUI.PolicyUI.MCOEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ConvertQuoteEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReinstatePolicyEnabled = false;

                    if (EnhancePolicyManager.LocalCache.GetShortCode( EnhancePolicyManager.GetLatestTran(PolicyEnh,true).TransactionStatus) == "PR")
                    {
                        structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                        structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = false;
                    }
                    else
                    {//MGaba2:MITS 15382:Start
                       // structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_RENEW);
                       // structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_AMEND);
                        structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_AMEND);
                        structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_CANCEL);
                        //MGaba2:MITS 15382:End                        
                    }
                    structEnhancePolicyUI.PolicyUI.BrokerEnabled = false;
                    structEnhancePolicyUI.PolicyUI.InsurerEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumEnabled = true;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible = true;
                    structEnhancePolicyUI.PolicyUI.PrimaryPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CoverageEnabled = false;

                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = false;
                    structEnhancePolicyUI.McoUI.AddMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.DeleteMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.EditMcoEnabled = true;
                    structEnhancePolicyUI.PolicyUI.BankAccountEnabled = true;
                    //MGaba2:MITS 11802
                    //structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_PRINT);
                    break;
                    #endregion
                case "E":
                    #region Expired
                    structEnhancePolicyUI.PolicyUI.PolicyNameEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyStateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.IssueDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReviewDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RetroDateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.CancelReasonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = false;

                    if (EnhancePolicyManager.LocalCache.GetShortCode(PolicyEnh.PolicyType) == "WC")
                    {
                        structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_AUDIT);
                        structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = true;
                    }
                    else
                    {
                        structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                        structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;
                    }
                    // Shruti
                    structEnhancePolicyUI.PolicyUI.MCOEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ConvertQuoteEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReinstatePolicyEnabled = false;

                    structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;

                    structEnhancePolicyUI.PolicyUI.BrokerEnabled = false;
                    structEnhancePolicyUI.PolicyUI.InsurerEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumEnabled = true;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible = true;
                    structEnhancePolicyUI.PolicyUI.PrimaryPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CoverageEnabled = false;

                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = false;
                    structEnhancePolicyUI.McoUI.AddMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.DeleteMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.EditMcoEnabled = true;
                    structEnhancePolicyUI.PolicyUI.BankAccountEnabled = true;
                    //MGaba2:MITS 11802
                    //structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_PRINT);
                    break;
                    #endregion
                case "PR":
                    #region Provisionally Renewed
                    structEnhancePolicyUI.PolicyUI.PolicyNameEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyStateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.IssueDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ReviewDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RetroDateEnabled = true;

                    structEnhancePolicyUI.PolicyUI.CancelReasonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = false;

                    if (EnhancePolicyManager.LocalCache.GetShortCode(PolicyEnh.PolicyType) == "WC")
                    {
                        structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                        structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = true;
                    }
                    else
                    {
                        structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                        structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;
                    }
                    // Shruti
                    structEnhancePolicyUI.PolicyUI.MCOEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ConvertQuoteEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReinstatePolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = false;

                    structEnhancePolicyUI.PolicyUI.BrokerEnabled = bEditBroker;
                    structEnhancePolicyUI.PolicyUI.InsurerEnabled = bEditInsurer;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumEnabled = true;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible = true;
                    structEnhancePolicyUI.PolicyUI.PrimaryPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CoverageEnabled = false;

                    //Commented by csingh7 : MITS 20427 Start
                    //if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_COV_ENH, RMO_ACCESS) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_COV_ENH, RMO_VIEW) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_COV_ENH, RMO_CREATE))
                    //    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = true;
                    //else
                    //    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = false;
                    //Commented by csingh7 : MITS 20427 End
                    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_COV_ENH, RMO_CREATE);   //Added by csingh7 : MITS 20427
                    structEnhancePolicyUI.CoverageUI.DeleteCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_COV_ENH, RMO_DELETE);
                    structEnhancePolicyUI.CoverageUI.EditCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_COV_ENH, RMO_UPDATE);

                    //Commented by csingh7 : MITS 20427 Start
                    //if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_ACCESS) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_VIEW) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_CREATE))
                    //    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = true;
                    //else
                    //    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = false;
                    //Commented by csingh7 : MITS 20427 End
                    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_CREATE);   //Added by csingh7 : MITS 20427
                    structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_DELETE);
                    structEnhancePolicyUI.ExposureUI.EditExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_UPDATE);

                    structEnhancePolicyUI.PolicyUI.AcceptTransacationEnabled = true;
                    structEnhancePolicyUI.PolicyUI.DeleteTransactionEnabled = true;

                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = false;
                    structEnhancePolicyUI.McoUI.AddMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.DeleteMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.EditMcoEnabled = true;
                    structEnhancePolicyUI.PolicyUI.BankAccountEnabled = true;
                    //MGaba2:MITS 11802
                    //structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_PRINT);
                    if (EnhancePolicyManager.CCacheFunctions.GetShortCode(PolicyEnh.PolicyType) == "GL")
                        structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;
                    else
                        structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = true;
                    break;
                    #endregion
                // Cancelled
                case "C":
                case "CPR":
                case "CF":
                    #region Cancelled
                    structEnhancePolicyUI.PolicyUI.PolicyNameEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyStateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.IssueDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReviewDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RetroDateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.CancelReasonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = false;

                    if (EnhancePolicyManager.LocalCache.GetShortCode(PolicyEnh.PolicyType) == "WC")
                    {
                        structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_AUDIT);
                        structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = true;
                    }
                    else
                    {
                        structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                        structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;
                    }
                    // Shruti
                    structEnhancePolicyUI.PolicyUI.MCOEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ConvertQuoteEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReinstatePolicyEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_REINSTATE);
                    structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = false;

                    structEnhancePolicyUI.PolicyUI.BrokerEnabled = false;
                    structEnhancePolicyUI.PolicyUI.InsurerEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumEnabled = true;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible = true;
                    structEnhancePolicyUI.PolicyUI.PrimaryPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CoverageEnabled = false;

                    structEnhancePolicyUI.PolicyUI.AcceptTransacationEnabled = false;
                    structEnhancePolicyUI.PolicyUI.DeleteTransactionEnabled = false;

                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = false;
                    structEnhancePolicyUI.McoUI.AddMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.DeleteMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.EditMcoEnabled = true;
                    structEnhancePolicyUI.PolicyUI.BankAccountEnabled = true;
                    //MGaba2:MITS 11802
                    //structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_PRINT);
                    structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;
                    //pmahli MITS 9773
                    structEnhancePolicyUI.CoverageUI.EditCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_COV_ENH, RMO_UPDATE);
                    structEnhancePolicyUI.ExposureUI.EditExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_UPDATE);
                                        
                    break;
                    #endregion
                // Provisional Cancel
                case "PCF":
                case "PCP":
                    #region Provisional Cancel
                    structEnhancePolicyUI.PolicyUI.PolicyNameEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyStateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.IssueDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ReviewDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RetroDateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.CancelReasonEnabled = true;
                    structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = false;

                    if (EnhancePolicyManager.LocalCache.GetShortCode(PolicyEnh.PolicyType) == "WC")
                    {
                        structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                        structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = true;
                    }
                    else
                    {
                        structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                        structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;
                    }
                    // Shruti
                    structEnhancePolicyUI.PolicyUI.MCOEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ConvertQuoteEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReinstatePolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = false;

                    structEnhancePolicyUI.PolicyUI.BrokerEnabled = bEditBroker;
                    structEnhancePolicyUI.PolicyUI.InsurerEnabled = bEditInsurer;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumEnabled = true;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible = true;
                    structEnhancePolicyUI.PolicyUI.PrimaryPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CoverageEnabled = false;

                    structEnhancePolicyUI.PolicyUI.AcceptTransacationEnabled = true;
                    structEnhancePolicyUI.PolicyUI.DeleteTransactionEnabled = true;

                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = false;
                    structEnhancePolicyUI.McoUI.AddMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.DeleteMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.EditMcoEnabled = true;
                    structEnhancePolicyUI.PolicyUI.BankAccountEnabled = true;
                    //MGaba2:MITS 11802
                    //structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_PRINT);
                    structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;
                    //pmahli MITS 9773
                    structEnhancePolicyUI.CoverageUI.EditCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_COV_ENH, RMO_UPDATE);
                    structEnhancePolicyUI.ExposureUI.EditExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_UPDATE);
                    
                    break;
                    #endregion
                // Provisional Reinstate
                case "PRN":
                case "PRL":
                    #region Provisional Reinstate
                    structEnhancePolicyUI.PolicyUI.PolicyNameEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyStateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.IssueDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ReviewDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RetroDateEnabled = true;

                    structEnhancePolicyUI.PolicyUI.CancelReasonEnabled = true;
                    structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = false;

                    if (EnhancePolicyManager.LocalCache.GetShortCode(PolicyEnh.PolicyType) == "WC")
                    {
                        structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                        structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = true;
                    }
                    else
                    {
                        structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                        structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;
                    }
                    // Shruti
                    structEnhancePolicyUI.PolicyUI.MCOEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ConvertQuoteEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReinstatePolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = false;

                    structEnhancePolicyUI.PolicyUI.BrokerEnabled = bEditBroker;
                    structEnhancePolicyUI.PolicyUI.InsurerEnabled = bEditInsurer;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumEnabled = true;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible = true;
                    structEnhancePolicyUI.PolicyUI.PrimaryPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CoverageEnabled = false;

                    structEnhancePolicyUI.PolicyUI.AcceptTransacationEnabled = true;
                    structEnhancePolicyUI.PolicyUI.DeleteTransactionEnabled = true;

                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = false;
                    structEnhancePolicyUI.McoUI.AddMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.DeleteMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.EditMcoEnabled = true;
                    structEnhancePolicyUI.PolicyUI.BankAccountEnabled = true;
                    //MGaba2:MITS 11802
                    //structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_PRINT);
                    structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;
                    //pmahli MITS 9773 
                    structEnhancePolicyUI.CoverageUI.EditCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_COV_ENH, RMO_UPDATE);
                    structEnhancePolicyUI.ExposureUI.EditExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_UPDATE);
                    break;
                    #endregion
            }

            // Set Defaults which need to be READONLY in each case. 
            structEnhancePolicyUI.PolicyUI.CancelDateEnabled = false;

        }

        protected bool EntityAccessCheck()
        {
            if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_PEOPMAINT, RMO_ACCESS))
            {
                if (!UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_PEOPMAINT, RMO_CREATE) && !(UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_PEOPMAINT, RMO_VIEW)))
                    return false;
                else
                    return true;
            }
            else
            {
                return false;
            }
        }

        private void SetPolicyQuoteReadOnlyNodes()
        {
            if (!structEnhancePolicyUI.PolicyUI.PolicyNameEnabled)
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_POLICY_QUOTE_NAME);
            if (!structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled)
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_POLICY_QUOTE_NUMBER);
            if (!structEnhancePolicyUI.PolicyUI.PolicyStatusEnabled)
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_POLICY_QUOTE_STATUS);
            if (!structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled)
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_POLICY_QUOTE_TYPE);
            if (!structEnhancePolicyUI.PolicyUI.PolicyStateEnabled)
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_POLICY_QUOTE_STATE);
            if (!structEnhancePolicyUI.PolicyUI.IssueDateEnabled)
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_ISSUE_DATE);
            if (!structEnhancePolicyUI.PolicyUI.ReviewDateEnabled)
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_REVIEW_DATE);
            if (!structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled)
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_EFFECTIVE_DATE);
            if (!structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled)
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_EXPIRATION_DATE);
            if (!structEnhancePolicyUI.PolicyUI.CancelReasonEnabled)
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_CANCEL_REASON);
            if (!structEnhancePolicyUI.PolicyUI.CancelDateEnabled)
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_CANCEL_DATE);
            if (!structEnhancePolicyUI.PolicyUI.RetroDateEnabled)
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_RETRO_DATE);
            if (!structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled)
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_NON_RENEW_REASON);            
            
            if (!structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled)
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_POLICY_QUOTE_TYPE);

            if (!structEnhancePolicyUI.PolicyUI.InsurerEnabled)
            {
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_INSURER);
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_INSURER_CONTACT);
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_INSURER_ADDRESS1);
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_INSURER_ADDRESS2);
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_INSURER_ADDRESS3);
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_INSURER_ADDRESS4);
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_INSURER_CITY);
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_INSURER_STATE);
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_INSURER_ZIP);
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_INSURER_COUNTRY);
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_INSURER_OFFICE_PHONE);
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_INSURER_ALT_PHONE);
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_INSURER_FAX);
            }

            if (!structEnhancePolicyUI.PolicyUI.BrokerEnabled)
            {
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_BROKER_LAST_NAME);
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_BROKER_FIRST_NAME);
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_BROKER_ADDRESS1);
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_BROKER_ADDRESS2);
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_BROKER_ADDRESS3);
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_BROKER_ADDRESS4);
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_BROKER_CITY);
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_BROKER_STATE);
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_BROKER_ZIP);
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_BROKER_COUNTRY);
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_BROKER_OFFICE_PHONE);
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_BROKER_ALT_PHONE);
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_BROKER_FAX);
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_BROKER_EMAIL_TYPE);
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_BROKER_EMAIL_ADD);
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_BROKER_TAX_ID);
                base.AddReadOnlyNode(EnhancePolicyManager.CTL_BROKER_BROKER_FIRM);
            }
        }

        private void SetPolicyQuoteKillNodes()
        {
            if (!structEnhancePolicyUI.PolicyUI.AuditButtonVisbile)
                base.AddKillNode(EnhancePolicyManager.CTL_AUDIT_POLICY);
            if( !structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible )
            	base.AddKillNode( EnhancePolicyManager.CTL_EARNED_PREMIUM );
            if (!EnhancePolicyManager.UseBillingSystem)
            {
                base.AddKillNode(EnhancePolicyManager.CTL_POLICY_BILLING_TAB);
            }
        }

        private void SetPolicyQuoteAttributes(int p_iTransNumber)
        {
            XmlDocument objSysEx = base.SysEx;
            XmlNode objOld = null;
            XmlElement objNew = null;

            objOld = objSysEx.SelectSingleNode("//ControlAppendAttributeList");
            objNew = objSysEx.CreateElement("ControlAppendAttributeList");

            this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_POLICY_QUOTE_TAB, "title", structEnhancePolicyUI.PolicyUI.PolicyTabCaption);
            this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_POLICY_QUOTE_NAME, "title", structEnhancePolicyUI.PolicyUI.NameCaption);
            this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_POLICY_QUOTE_NUMBER, "title", structEnhancePolicyUI.PolicyUI.NumberCaption);
            this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_POLICY_QUOTE_STATUS, "title", structEnhancePolicyUI.PolicyUI.StatusCaption);
            this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_POLICY_QUOTE_TYPE, "title", structEnhancePolicyUI.PolicyUI.TypeCaption);
            this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_POLICY_QUOTE_STATE, "title", structEnhancePolicyUI.PolicyUI.StateCaption);
            this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_PRINT_POLICY, "title", structEnhancePolicyUI.PolicyUI.PrintCaption);

            // Shruti
            if (!structEnhancePolicyUI.PolicyUI.MCOEnabled)
                this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_MCO, "disabled", "true");
            else
                this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_MCO, "disabled", "false");

            if( !structEnhancePolicyUI.PolicyUI.ConvertQuoteEnabled )
                this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_CONVERT_TO_POLICY, "disabled", "true" );
            else
                this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_CONVERT_TO_POLICY, "disabled", "false");

            if (!structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled)
                this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_AMEND_POLICY, "disabled", "true");
            else
                this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_AMEND_POLICY, "disabled", "false");

            if (!structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled)
                this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_RENEW_POLICY, "disabled", "true");
            else
                this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_RENEW_POLICY, "disabled", "false");

            if (!structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled)
                this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_CANCEL_POLICY, "disabled", "true");
            else
                this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_CANCEL_POLICY, "disabled", "false");

            if (!structEnhancePolicyUI.PolicyUI.ReinstatePolicyEnabled)
                this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_REINSTATE_POLICY, "disabled", "true");
            else
                this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_REINSTATE_POLICY, "disabled", "false");
            if (!structEnhancePolicyUI.PolicyUI.AuditButtonEnabled )
                this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_AUDIT_POLICY, "disabled", "true");
            else
                this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_AUDIT_POLICY, "disabled", "false");
            if (!structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled )
                this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_PRINT_POLICY, "disabled", "true");
            else
                this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_PRINT_POLICY, "disabled", "false");
            if (!structEnhancePolicyUI.PolicyUI.AcceptTransacationEnabled)
                this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_ACCEPT_TRANSACTION, "disabled", "true");
            else
                this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_ACCEPT_TRANSACTION, "disabled", "false");
            if (!structEnhancePolicyUI.PolicyUI.DeleteTransactionEnabled)
                this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_DELETE_TRANSACTION, "disabled", "true");
            else
                this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_DELETE_TRANSACTION, "disabled", "false");
            if (!structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumEnabled)
                this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_EARNED_PREMIUM, "disabled", "true");
            else
                this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_EARNED_PREMIUM, "disabled", "false");

            if (EnhancePolicyManager.UseBillingSystem)
            {
            // Populate the Policy Billing Tab from Policy Billing Object
            this.OnRenderBillingList(p_iTransNumber, ref objNew); 
            }

            // Change the Ref of Bank Account to Point to Sub Bank Account when UseSubBankAccount Setting is true
            // ijha Mits 24391: the bank account name is changed to sub bank account when the use sub bank account is checked in utililties.
            ArrayList singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singleRow, "id", "lbl_bankaccount");

            if (PolicyEnh.Context.InternalSettings.SysSettings.UseFundsSubAcc)
            {
                this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_BANK_ACCOUNT, "ref", "Instance/PolicyEnh/SubAccRowId");
                base.AddElementToList(ref singleRow, "Text", "Sub Bank Account");
                base.m_ModifiedControls.Add(singleRow);
            
            }
            else
            {
                this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_BANK_ACCOUNT, "ref", "Instance/PolicyEnh/BankAccId");
                base.AddElementToList(ref singleRow, "Text", "Bank Account");            
            }
            // ijha end 
            if (objOld != null)
                objSysEx.DocumentElement.ReplaceChild(objNew, objOld);
            else
                objSysEx.DocumentElement.AppendChild(objNew);

        }

        private void SetPolicyQuoteDataToSysEx()
        {
                this.AppendRuntimeData("PolicyStatus", structEnhancePolicyUI.PolicyUIData.PolicyStatus.ToString(), EnhancePolicyManager.ELEMENT_TYPE_CODE);
                this.AppendRuntimeData("PolicyNumber", structEnhancePolicyUI.PolicyUIData.PolicyNumber);
                
                this.AppendRuntimeData("PolicyType", structEnhancePolicyUI.PolicyUIData.PolicyType.ToString(), EnhancePolicyManager.ELEMENT_TYPE_CODE);
                
                // rrachev JIRA RMA-4208
                string sPrimaryPolicy = "False";
                if (PolicyEnh.PrimaryPolicyFlg) {sPrimaryPolicy = "True";}
                this.AppendRuntimeData("PrimaryPolicy", sPrimaryPolicy);

                this.AppendRuntimeData("ExpirationDate", Conversion.GetDBDateFormat(structEnhancePolicyUI.PolicyUIData.ExpirationDate, "d"));
                this.AppendRuntimeData("EffectiveDate", Conversion.GetDBDateFormat(structEnhancePolicyUI.PolicyUIData.EffectiveDate, "d"));
                this.AppendRuntimeData("CancelDate", Conversion.GetDBDateFormat(structEnhancePolicyUI.PolicyUIData.CancelDate, "d"));
                this.AppendRuntimeData("CancelReason", structEnhancePolicyUI.PolicyUIData.CancelReason.ToString(), EnhancePolicyManager.ELEMENT_TYPE_CODE);
                //npadhy RMSC retrofit Starts
                this.AppendRuntimeData("BrokerCommission", Conversion.ConvertObjToStr(structEnhancePolicyUI.PolicyUIData.BrokerCommission));        
                //npadhy RMSC retrofit Ends
                if (PolicyEnh.DttmRcdLastUpd != "")
                    this.AppendRuntimeData("LastUpdated", Conversion.ToDate(PolicyEnh.DttmRcdLastUpd).ToString());
                else
                    this.AppendRuntimeData("LastUpdated", "");
                this.AppendRuntimeData("WaiveTransactionPremium", structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumSelected.ToString());
                this.AppendRuntimeData("WaiveTransactionPremiumComments", structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumComments);
                this.AppendRuntimeData("DupPolicyStateId", PolicyEnh.State.ToString());


                // Following fields are Of type RADIO , CHECKBOX or LIST type.
                // Need to be make readonly through JAVASCRIPT.
                this.AppendRuntimeData("IsQuote", structEnhancePolicyUI.PolicyUI.IsQuote.ToString());
                this.AppendRuntimeData("IsQuoteDup", structEnhancePolicyUI.PolicyUI.IsQuote.ToString());
                this.AppendRuntimeData("PrimaryPolicyEnabled", structEnhancePolicyUI.PolicyUI.PrimaryPolicyEnabled.ToString().ToLower());
                this.AppendRuntimeData("CoverageEnabled", structEnhancePolicyUI.PolicyUI.CoverageEnabled.ToString().ToLower());
                this.AppendRuntimeData("BankAccountEnabled", structEnhancePolicyUI.PolicyUI.BankAccountEnabled.ToString().ToLower());
                this.AppendRuntimeData("InsurerInsuredEnabled", structEnhancePolicyUI.PolicyUI.InsurerEnabled.ToString().ToLower());
                this.AppendRuntimeData("WaiveTransactionPremiumEnabled", structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled.ToString().ToLower());

                this.AppendRuntimeData("ExposureAddEnabled", structEnhancePolicyUI.ExposureUI.AddExposureEnabled.ToString().ToLower());
                this.AppendRuntimeData("ExposureEditEnabled", structEnhancePolicyUI.ExposureUI.EditExposureEnabled.ToString().ToLower());
                this.AppendRuntimeData("ExposureDeleteEnabled", structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled.ToString().ToLower());

                this.AppendRuntimeData("CoverageAddEnabled", structEnhancePolicyUI.CoverageUI.AddCoverageEnabled.ToString().ToLower());
                this.AppendRuntimeData("CoverageEditEnabled", structEnhancePolicyUI.CoverageUI.EditCoverageEnabled.ToString().ToLower());
                this.AppendRuntimeData("CoverageDeleteEnabled", structEnhancePolicyUI.CoverageUI.DeleteCoverageEnabled.ToString().ToLower());            
            // npadhy MITS 17516 Get the Insured to be Billed from SYSEx. If there is nothing in SysEx, Check for DB, and Display it on Screen
            // Get Insured from DB. 
            DbReader objReader = null;
            int iBillingInsured = 0;

            iBillingInsured = base.GetSysExDataNodeInt("/SysExData//" + "BillingInsured/@codeid", false);

            if (iBillingInsured == 0)
            {
                string sSql = "SELECT INSURED_EID FROM POLICY_X_INSRD_ENH WHERE BILLING_INSURED <> 0 AND POLICY_ID = " + PolicyEnh.PolicyId;
                objReader = PolicyEnh.Context.DbConnLookup.ExecuteReader(sSql);
                if (objReader.Read())
                    iBillingInsured = Conversion.ConvertObjToInt(objReader.GetValue("INSURED_EID"), base.ClientId);
                objReader.Close();

                // Check if the Insured is removed from the Listbox or if the Remove Insured from Billing is clicked
                // Do not populate the Insured back in the Insured for Billing Textbox

                string sPostBackAction = base.GetSysExDataNodeText("/SysExData//" + EnhancePolicyManager.POST_BACK_ACTION);
                int iInsuredId = base.GetSysExDataNodeInt(EnhancePolicyManager.SYSEX_INSURED_ID);

                if (sPostBackAction == "INSURED_DETAILS" && iInsuredId == 0)
                    iBillingInsured = 0;
            }

            
            this.AppendRuntimeData("BillingInsured", iBillingInsured.ToString(), EnhancePolicyManager.ELEMENT_TYPE_ORG);
        }

        private void OnRenderBillingList(int p_iTransNumber,ref XmlElement objNew)
        {
            PolicyXBillEnh objPolicyXBillEnhNew = null;
            bool bBillItemExist = false ;
            
            ///////////////////////////////////////////////////////////////////////////////
            // VAIBHAV KASUHIK, START : For cases where Billing turned on, and we are moving to a record which has 
            // been saved while billing was OFF.
            ///////////////////////////////////////////////////////////////////////////////

            // Check if there is any billing record present, against this Transaction ID. 
            foreach (PolicyXBillEnh objPolicyXBillEnh in PolicyEnh.PolicyXBillEnhList)
            {
                if (objPolicyXBillEnh.TransactionId == p_iTransNumber)
                {
                    bBillItemExist = true;
                    break;
                }
            }

            if (!bBillItemExist)
            {
                objPolicyXBillEnhNew = PolicyEnh.PolicyXBillEnhList.AddNew();
                p_iTransNumber = 0;
            }

            ///////////////////////////////////////////////////////////////////////////////
            // VAIBHAV KASUHIK, END : For cases where Billing turned on, and we are moving to a record which has 
            // been saved while billing was OFF.
            ///////////////////////////////////////////////////////////////////////////////

            if (PolicyEnh.PolicyXBillEnhList.Count == 0)
            {
                objPolicyXBillEnhNew = PolicyEnh.PolicyXBillEnhList.AddNew();
            }

            XmlDocument objSysEx = base.SysEx;
            //pmahli MITS 9231 Added Instance in sBasePath as recordsummary.xsl doesnot recognize '//'
            string sBasePath = "Instance//PolicyEnh/PolicyXBillEnhList/PolicyXBillEnh/TransactionId[.=\"" + p_iTransNumber + "\"]/../";
            this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_POLICY_BILLING_ID, "ref", sBasePath + "PolicyBillingId");
            this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_BILL_TO_EID, "ref", sBasePath + "BillToEid");
            this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_NEW_BILL_TO_EID, "ref", sBasePath + "NewBillToEid");
            this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_BILL_X_INS_ENTITYID, "ref", sBasePath + "InsuredEntity/EntityId");
            this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_BILL_X_NEW_INS_ENTITYID, "ref", sBasePath + "NewInsuredEntity/EntityId"); 
            this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_DONOTBILL, "ref", sBasePath + "DoNotBill"); 
            this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_PAY_PLAN_ROWID, "ref", sBasePath + "PayPlanRowId");
            this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_BILLING_RULE_ROWID, "ref", sBasePath + "BillingRuleRowId");
            this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_BILL_TO_TYPE, "ref", sBasePath + "BillToType");
            this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_BILL_TO_OVERRIDE, "ref", sBasePath + "BillToOverride");
            this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_IND_LASTNAME_PAYPLAN, "ref", sBasePath + "InsuredEntity/LastName");
            this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_NEW_LASTNAME, "ref", sBasePath + "NewInsuredEntity/LastName");
            this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_IND_FIRSTNAME, "ref", sBasePath + "InsuredEntity/FirstName");
            this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_NEW_FIRSTNAME, "ref", sBasePath + "NewInsuredEntity/FirstName");
            this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_IND_ADDR1, "ref", sBasePath + "InsuredEntity/Addr1");
            this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_NEW_ADDR1, "ref", sBasePath + "NewInsuredEntity/Addr1");
            this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_IND_ADDR2, "ref", sBasePath + "InsuredEntity/Addr2");
            this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_NEW_ADDR2, "ref", sBasePath + "NewInsuredEntity/Addr2");
            this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_IND_ADDR3, "ref", sBasePath + "InsuredEntity/Addr3");
            this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_NEW_ADDR3, "ref", sBasePath + "NewInsuredEntity/Addr3");
            this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_IND_ADDR4, "ref", sBasePath + "InsuredEntity/Addr4");
            this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_NEW_ADDR4, "ref", sBasePath + "NewInsuredEntity/Addr4");
            this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_IND_CITY, "ref", sBasePath + "InsuredEntity/City");
            this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_NEW_CITY, "ref", sBasePath + "NewInsuredEntity/City");
            this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_IND_STATE, "ref", sBasePath + "InsuredEntity/StateId");
            this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_NEW_STATE, "ref", sBasePath + "NewInsuredEntity/StateId");
            this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_IND_ZIPCODE, "ref", sBasePath + "InsuredEntity/ZipCode");
            this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_NEW_ZIPCODE, "ref", sBasePath + "NewInsuredEntity/ZipCode");
            this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_IND_COUNTRY, "ref", sBasePath + "InsuredEntity/CountryCode");
            this.AppendRuntimeAttribute(objNew, EnhancePolicyManager.CTL_NEW_COUNTRY, "ref", sBasePath + "NewInsuredEntity/CountryCode");

            //Start:Sumit(09/09/2010)-MITS#21872
            //Sumit(11/10/2010)-Reverting - Will be fixed later
            //Mona
            this.AppendRuntimeAttribute(objNew, "newlastname_cid", "ref", "Instance/UI/FormVariables/SysExData/NewLastName_Cid");
            //this.AppendRuntimeAttribute(objNew, "indentityid", "ref", sBasePath + "BillToEid");
            //this.AppendRuntimeAttribute(objNew, "newlastname_entityid", "ref", sBasePath + "NewBillToEid");
            //End:Sumit
            int iPayPlanId = 0;
            int iPayPlanCode = 0;
            string sPayPlanShortCode = "";
            string sPayPlanDesc = "";
            int iBillingRuleId = 0;
            int iBillingRuleCode = 0;
            string sBillingRuleShortCode = "";
            string sBillingRuleDesc = "";
            DbReader objReader = null;
            string sSQL = "";
            bool bAppendBlankData = true;
            foreach (PolicyXBillEnh objPolicyBilling in this.PolicyEnh.PolicyXBillEnhList)
            {
                if (objPolicyBilling.TransactionId == p_iTransNumber)
                {
                    iPayPlanId = Conversion.ConvertObjToInt(objPolicyBilling.PayPlanRowId, base.ClientId);
                    sSQL = "SELECT PAY_PLAN_CODE FROM SYS_BILL_PAY_PLAN WHERE PAY_PLAN_ROWID = " + iPayPlanId.ToString();
                    objReader = PolicyEnh.Context.DbConnLookup.ExecuteReader(sSQL);
                    if (objReader.Read())
                        iPayPlanCode = Conversion.ConvertObjToInt(objReader.GetValue("PAY_PLAN_CODE"), base.ClientId);
                    objReader.Close();
                    EnhancePolicyManager.LocalCache.GetCodeInfo(iPayPlanCode, ref sPayPlanShortCode, ref sPayPlanDesc, base.Adaptor.userLogin.objUser.NlsCode); //tmalhotra3 ML Change
                    if (sPayPlanShortCode != "" && sPayPlanDesc != "")
                        base.ResetSysExData("PayPlanCode", sPayPlanShortCode + " - " + sPayPlanDesc);
                    else
                        base.ResetSysExData("PayPlanCode", " ");
                    iBillingRuleId = Conversion.ConvertObjToInt(objPolicyBilling.BillingRuleRowId, base.ClientId);
                    sSQL = "SELECT BILLING_RULE_CODE FROM SYS_BILL_BLNG_RULE WHERE BILLING_RULE_ROWID = " + iBillingRuleId.ToString();
                    objReader = PolicyEnh.Context.DbConnLookup.ExecuteReader(sSQL);
                    if (objReader.Read())
                        iBillingRuleCode = Conversion.ConvertObjToInt(objReader.GetValue("BILLING_RULE_CODE"), base.ClientId);
                    objReader.Close();
                    EnhancePolicyManager.LocalCache.GetCodeInfo(iBillingRuleCode, ref sBillingRuleShortCode, ref sBillingRuleDesc);
                    if (sBillingRuleShortCode != "" && sBillingRuleDesc != "")
                        base.ResetSysExData("BillingRuleCode", sBillingRuleShortCode + " - " + sBillingRuleDesc);
                    else
                        base.ResetSysExData("BillingRuleCode", "");

                    //Start:Sumit(09/09/2010)-MITS#21872
                    //Sumit(11/10/2010)-Reverting - Will be fixed later
                    base.ResetSysExData("PayPlan_EntityId", objPolicyBilling.BillToEid.ToString());
                    base.ResetSysExData("NewLastName_Cid", objPolicyBilling.NewBillToEid.ToString());
                    //End:Sumit
                    bAppendBlankData = false;
                    break;
                }
            }
            if (bAppendBlankData)
            {
                base.ResetSysExData("PayPlanCode", " ");
                base.ResetSysExData("BillingRuleCode", "");
                //Start:Sumit(09/09/2010)-MITS#21872
                //Sumit(11/10/2010)-Reverting - Will be fixed later
                base.ResetSysExData("PayPlan_EntityId", " ");
                base.ResetSysExData("NewLastName_Cid", "");
                //End:Sumit
            }             
        }
        

        #endregion        

        #region Appened Data for BankAccount Combobox List
        private void AppendBankAccountList()
        {
            //Variable declarations
            ArrayList arrAcctList = new ArrayList();
            XmlDocument objXML = base.SysEx;
            XmlNode objOld = null;
            XmlElement objNew = null;
            XmlCDataSection objCData = null;

            //Retrieve the complete Bank Account List and store it in the ArrayList
            using (FundManager oFundManager = FundManagerObject())
            {
                arrAcctList = oFundManager.GetAccounts(this.objData.Context.InternalSettings.SysSettings.UseFundsSubAcc);
            }
            m_objFundManager = null;

            //Create the necessary SysExData to be used in ref binding
            objOld = objXML.SelectSingleNode("//AccountList");
            objNew = objXML.CreateElement("AccountList");

            XmlElement xmlOption = null;
            XmlAttribute xmlOptionAttrib = null;

            // Blank value for the combo box
            xmlOption = objXML.CreateElement("option");
            xmlOptionAttrib = objXML.CreateAttribute("value");
            xmlOption.Attributes.Append(xmlOptionAttrib);
            objNew.AppendChild(xmlOption);
            xmlOptionAttrib = null;
            xmlOption = null;

            //Loop through and create all the option values for the combobox control
            foreach (FundManager.AccountDetail item in arrAcctList)
            {
                xmlOption = objXML.CreateElement("option");
                objCData = objXML.CreateCDataSection(item.AccountName);
                xmlOption.AppendChild(objCData);
                xmlOptionAttrib = objXML.CreateAttribute("value");

                if (this.objData.Context.InternalSettings.SysSettings.UseFundsSubAcc)
                    xmlOptionAttrib.Value = item.SubRowId.ToString();
                else
                    xmlOptionAttrib.Value = item.AccountId.ToString();

                xmlOption.Attributes.Append(xmlOptionAttrib);
                objNew.AppendChild(xmlOption);
            }//end foreach

            //Add the XML to the SysExData 
            if (objOld != null)
                objXML.DocumentElement.ReplaceChild(objNew, objOld);
            else
                objXML.DocumentElement.AppendChild(objNew);


            //Clean up
            arrAcctList = null;
            objOld = null;
            objNew = null;
            objCData = null;
            objXML = null;
        }
        #endregion

        #endregion 
        
        #region OnValidate

        public override void OnValidate(ref bool Cancel)
        {
            bool bError = false;

            if (Conversion.ConvertStrToInteger(structEnhancePolicyUI.PolicyUIData.EffectiveDate) > Conversion.ConvertStrToInteger(structEnhancePolicyUI.PolicyUIData.ExpirationDate))
            {
                Errors.Add("ValidationError", "Effective Date must be prior to expiration date.", BusinessAdaptorErrorType.Error);
                bError = true;
            }

            //Change by kuladeep for Audit Issue MITS:24736//skhare7 RMSC merge
            //Charanpreet for Mits 11323 : Start
            //if (PolicyEnh.PolicyXInsuredEnh.Count != 0)
            if (PolicyEnh.PolicyXInsuredEnhList.Count != 0)
            {
                bool bValerror = false;
                bValerror = OrgHierarchyValidate(ref bValerror);
                if (bValerror)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        "Selected Insured(s) Not Within Effective Date Range",
                        BusinessAdaptorErrorType.Error);
                    bError = true;
                }
            }
            //Charanpreet for Mits 11323 : End
            
            // Return true if there were validation errors
            Cancel = bError;

            // npadhy MITS 15379, MITS 15487, MITS 16231 In Case of error we need to have the OnUpdateObject and Onupdateform
            // So that the XMLOut is proper. This is required so that if the user corrects the error and then presses the save 
            // button, he has all the data on the page.
            if (Cancel)
            {
                OnUpdateObject();
                OnUpdateForm();
            }
        }

        #endregion OnValidate

        #region OnSave
        // MITS9685 : Umesh
        public override void BeforeSave(ref bool Cancel)
        {
            base.BeforeSave(ref Cancel);
            string sPolicyName = PolicyEnh.PolicyName;
            int iPolicyId = PolicyEnh.PolicyId;
            if (EnhancePolicyManager.LocalCache.GetShortCode(this.PolicyEnh.PolicyStatusCode) == "Q")
            {
                if (sPolicyName.Trim() != string.Empty)
                {
                    //MGaba2:MITS 15615:Issue with making policy with apostrophe
                    //string sSQL = "SELECT POLICY_ID FROM POLICY_ENH WHERE POLICY_NAME='" + sPolicyName.Trim() + "' AND POLICY_ID<>" + iPolicyId.ToString();
                    string sSQL = "SELECT POLICY_ID FROM POLICY_ENH WHERE POLICY_NAME='" + sPolicyName.Trim().Replace("'", "''") + "' AND POLICY_ID<>" + iPolicyId.ToString();                     
                    DbReader objReader = PolicyEnh.Context.DbConn.ExecuteReader(sSQL);
                    if (objReader.Read())
                    {
                        Errors.Add(Globalization.GetString("SaveError", base.ClientId),
                            String.Format(Globalization.GetString("Save.DuplicatePolicyNameWarning", base.ClientId)),
                            BusinessAdaptorErrorType.Error);
                        Cancel = true;
                        if (objReader != null)
                            objReader.Close();

                        // npadhy MITS 15379, MITS 15487, MITS 16231 In Case of error we need to have the OnUpdateObject and Onupdateform
                        // So that the XMLOut is proper. This is required so that if the user corrects the error and then presses the save 
                        // button, he has all the data on the page.
                        OnUpdateObject();
                        OnUpdateForm();
                        return;
                    }
                    if (objReader != null)
                        objReader.Close();
                }
            }
            //End  MITS9685 

            //06/29/07 Geeta Starts : Modified for MITS 9886                               
            if (EnhancePolicyManager.LocalCache.GetShortCode(PolicyEnh.PolicyIndicator) == "P")
            {
                int iPrevTransId = 0;
                int iSelectedTransId = 0;                
                string sSQL = string.Empty;
                string sWaiveComments = string.Empty; ;
                string sPostBackAction = base.GetSysExDataNodeText("/SysExData//" + EnhancePolicyManager.POST_BACK_ACTION);                                

                iPrevTransId = base.GetSysExDataNodeInt("/SysExData//" + "PreSelectedTransactionId", false);                
                iSelectedTransId = base.GetSysExDataNodeInt("/SysExData//" + "SelectedTransactionId", false);

                if (sPostBackAction == EnhancePolicyManager.ACTION_TRANSACTION_CHANGE)
                {
                    sSQL = "SELECT TRANSACTION_ID,WAIVE_PREM_COMMENT FROM POLICY_X_TRANS_ENH WHERE POLICY_ID=" + PolicyEnh.PolicyId + "AND TRANSACTION_ID =" + iSelectedTransId ;
                    DbReader objReader = PolicyEnh.Context.DbConnLookup.ExecuteReader(sSQL);

                    if(objReader.Read())
                    {
                        sWaiveComments = Conversion.ConvertObjToStr(objReader.GetValue("WAIVE_PREM_COMMENT"));
                    }

                    objReader.Close();
                                    
                    foreach (PolicyXTransEnh objPolicyXTransEnh in this.PolicyEnh.PolicyXTransEnhList)
                    {
                        if (objPolicyXTransEnh.TransactionId == iPrevTransId)
                        {
                            objPolicyXTransEnh.WaivePremComment = base.GetSysExDataNodeText("/SysExData//" + EnhancePolicyManager.CALC_PREM_COMMENTS_ID, false);
                        }
                        else if (objPolicyXTransEnh.TransactionId == iSelectedTransId)
                        {
                            objPolicyXTransEnh.WaivePremComment = sWaiveComments;
                        }
                    }
                }
                else
                {
                    foreach (PolicyXTransEnh objPolicyXTransEnh in this.PolicyEnh.PolicyXTransEnhList)
                    {
                        if (objPolicyXTransEnh.TransactionId == iSelectedTransId)
                        {
                            objPolicyXTransEnh.WaivePremComment = base.GetSysExDataNodeText("/SysExData//" + EnhancePolicyManager.CALC_PREM_COMMENTS_ID, false);
                        }
                    }
                }
            }
            //06/29/07 Geeta Ends : Modified for MITS 9886                 
        } 
        //End  MITS9685 
        public override void AfterSave()
        {
            base.AfterSave();
            // If Object is saved and we would try to load the data from the seesion XML, than Populate object
            // would throw an error and says object has already been updated.
            objExposuresUniqueIds.Clear();
            objCoveragesUniqueIds.Clear();
            objExposureUarSessionIds.Clear();

            // npadhy MITS 17516 First Update all the Data to 0 and then Get the Billing Insured and Update it in DB
            StringBuilder sSQL = null;
            int iSelectedTransId = 0; 
            sSQL = new StringBuilder();
            int iBillingInsured = 0;
            iSelectedTransId = base.GetSysExDataNodeInt("/SysExData//" + "SelectedTransactionId", false);
            // Get the Code Id for Quote and Renew
            int iQuoteCodeId = EnhancePolicyManager.LocalCache.GetCodeId("Q","POLICY_STATUS");
            int iRenewCodeId = EnhancePolicyManager.LocalCache.GetCodeId("RN", "POLICY_TXN_TYPE");
            int iPolicyCodeId = EnhancePolicyManager.LocalCache.GetCodeId("I", "POLICY_STATUS");
            int iConvertedQuoteCodeId = EnhancePolicyManager.LocalCache.GetCodeId("CV", "POLICY_STATUS");

            bool bIsRenew = false;
            foreach (PolicyXTransEnh objPolicyXTransEnh in this.PolicyEnh.PolicyXTransEnhList)
            {
                if (objPolicyXTransEnh.TransactionId == iSelectedTransId && objPolicyXTransEnh.TransactionType == iRenewCodeId)
                {
                    bIsRenew = true;
                }
            }

            // Do this when the Object getting saved is for Quote
            if (PolicyEnh.PolicyStatusCode == 0 ||
                PolicyEnh.PolicyStatusCode == iQuoteCodeId || bIsRenew 
                || PolicyEnh.PolicyStatusCode == iConvertedQuoteCodeId)
            {
                
                iBillingInsured = base.GetSysExDataNodeInt("/SysExData//" + "BillingInsured/@codeid", false);
                sSQL.Append("UPDATE POLICY_X_INSRD_ENH SET BILLING_INSURED = 0 WHERE POLICY_ID = " + this.PolicyEnh.PolicyId  );
                PolicyEnh.Context.DbConnLookup.ExecuteNonQuery(sSQL.ToString());
                sSQL.Remove(0, sSQL.Length);
                sSQL.Append("UPDATE POLICY_X_INSRD_ENH SET BILLING_INSURED = -1 WHERE POLICY_ID = " + this.PolicyEnh.PolicyId + " AND INSURED_EID = " + iBillingInsured);
                PolicyEnh.Context.DbConnLookup.ExecuteNonQuery(sSQL.ToString());
            }
            //Comment by kuladeep for Audit Issue MITS:24736 handle Isured list by datamodel
            //else if (PolicyEnh.PolicyStatusCode == iPolicyCodeId && !bIsRenew)
            //{
            //    // Get the Name of Quote from Policy Object and get the Insured for Billing from DB, And Save it in Policy
            //    sSQL.AppendFormat("UPDATE POLICY_X_INSRD_ENH SET BILLING_INSURED = 0 WHERE POLICY_ID =  {0} ", this.PolicyEnh.PolicyId );
            //    PolicyEnh.Context.DbConnLookup.ExecuteNonQuery(sSQL.ToString());
            //    sSQL.Remove(0, sSQL.Length);
            //    sSQL.Append("UPDATE POLICY_X_INSRD_ENH SET BILLING_INSURED = -1 WHERE INSURED_EID = ");
            //    sSQL.Append("(SELECT INSURED_EID FROM POLICY_X_INSRD_ENH PIE INNER JOIN POLICY_ENH PE ON PIE.POLICY_ID = PE.POLICY_ID ");
            //    //MGaba2:MITS 15615:Issue with making policy with apostrophe
            //    //sSQL.AppendFormat("AND PE.POLICY_NAME = '{0}' AND PIE.BILLING_INSURED = -1 AND PE.POLICY_STATUS_CODE = {1}) ", this.PolicyEnh.PolicyName, iConvertedQuoteCodeId);
            //    sSQL.AppendFormat("AND PE.POLICY_NAME = '{0}' AND PIE.BILLING_INSURED = -1 AND PE.POLICY_STATUS_CODE = {1}) ", this.PolicyEnh.PolicyName.Trim().Replace("'", "''"), iConvertedQuoteCodeId);
            //    sSQL.AppendFormat("AND POLICY_ID = {0}", this.PolicyEnh.PolicyId);
            //    PolicyEnh.Context.DbConnLookup.ExecuteNonQuery(sSQL.ToString());
            //}
        }
        #endregion 
        //Start:Add by kuladeep for Audit issue MITS:24736
        //Add UpdateInsuredList function for Insured, save/update insured list object 
        virtual protected void UpdateInsuredList()
        {
            string sPostBackAction = base.GetSysExDataNodeText("/SysExData//" + EnhancePolicyManager.POST_BACK_ACTION);
            int iBillingInsured = base.GetSysExDataNodeInt("/SysExData//" + "BillingInsured/@codeid", false);

            if (sPostBackAction != "TRANSACTION_CHANGE")
            {
                string sInsuredEids = string.Empty;
                if (base.SysEx.DocumentElement.SelectSingleNode("InsuredsList/@codeid") != null) // Added the Null Check: Devender
                {
                    sInsuredEids = base.SysEx.DocumentElement.SelectSingleNode("InsuredsList/@codeid").InnerText;
                }
                sInsuredEids = sInsuredEids.Trim();
                string[] arrInsuredEids = sInsuredEids.Split(new char[] { ' ' });
                bool bInSuccess = false;
                List<string> lstInsuredEid = new List<string>();
                List<string> lstNewInsuredEid = new List<string>();
                lstNewInsuredEid.AddRange(arrInsuredEids);

                foreach (PolicyXInsuredEnh objPolicyXInsuredEnh in this.PolicyEnh.PolicyXInsuredEnhList)
                {
                    lstInsuredEid.Add(objPolicyXInsuredEnh.InsuredEid.ToString());
                    if (!lstNewInsuredEid.Contains(objPolicyXInsuredEnh.InsuredEid.ToString()))
                    {
                        PolicyEnh.PolicyXInsuredEnhList.Remove(objPolicyXInsuredEnh.InsrdRowId);
                    }
                }
                foreach (string sEID in arrInsuredEids)
                {
                    if (!string.IsNullOrEmpty(sEID))
                    {
                        if (!lstInsuredEid.Contains(sEID))
                        {
                            PolicyXInsuredEnh objPolicyXInsrdEnhNew = PolicyEnh.PolicyXInsuredEnhList.AddNew();
                            objPolicyXInsrdEnhNew.PolicyId = PolicyEnh.PolicyId;
                            objPolicyXInsrdEnhNew.InsuredEid = Conversion.CastToType<int>(sEID, out bInSuccess);
                            if (iBillingInsured == objPolicyXInsrdEnhNew.InsuredEid)
                            {
                                objPolicyXInsrdEnhNew.BillingInsrd = -1;
                            }
                            else
                            {
                                objPolicyXInsrdEnhNew.BillingInsrd = 0;
                            }

                        }
                    }
                }
            }
        }
        //Add OnRenderInsuredList function for Render Insured Listbox in Insurer/Insured tab    
        private void OnRenderInsuredList()
        {
            //Variable declarations
            XmlDocument objXML = base.SysEx;
            XmlNode objOld = null;
            XmlElement objNew = null;
            XmlCDataSection objCData = null;

            string sAbbreviation = string.Empty;
            string sOrghLastName = string.Empty;
            string sCData = string.Empty;
            //Create the necessary SysExData to be used in ref binding
            objOld = objXML.SelectSingleNode("//InsuredsList");
            objNew = objXML.CreateElement("InsuredsList");
            objNew.SetAttribute("tablename", "ANY");
            objNew.SetAttribute("codeid", "");

            XmlElement xmlItem = null;
            XmlAttribute xmlItemAttrib = null;
            XmlAttribute xmlNewAttrib = null;
            if (this.PolicyEnh.PolicyXInsuredEnhList.Count == 0)
            {
                xmlItem = objXML.CreateElement("Item");
                sCData = "None Selected";
                objCData = objXML.CreateCDataSection(sCData);
                xmlItem.AppendChild(objCData);
                xmlItemAttrib = objXML.CreateAttribute("value");
                xmlItemAttrib.Value = "0";
                xmlItem.Attributes.Append(xmlItemAttrib);
                objNew.AppendChild(xmlItem);
                xmlNewAttrib = objNew.GetAttributeNode("codeid");
                xmlNewAttrib.Value = "";
                xmlItem = null;
                xmlItemAttrib = null;
            }
            else
            {
                //Loop through and create all the option values for the combobox control
                foreach (PolicyXInsuredEnh objPolicyXInsrdEnh in this.PolicyEnh.PolicyXInsuredEnhList)
                {
                        xmlItem = objXML.CreateElement("Item");
                        EnhancePolicyManager.LocalCache.GetOrgInfo(objPolicyXInsrdEnh.InsuredEid, ref sAbbreviation, ref sOrghLastName);
                        sCData = sAbbreviation + " " + sOrghLastName;
                        sCData = sCData.Trim();
                        objCData = objXML.CreateCDataSection(sCData);
                        xmlItem.AppendChild(objCData);
                        xmlItemAttrib = objXML.CreateAttribute("value");
                        xmlItemAttrib.Value = Convert.ToString(objPolicyXInsrdEnh.InsuredEid);
                        xmlItem.Attributes.Append(xmlItemAttrib);
                        xmlNewAttrib = objNew.GetAttributeNode("codeid");
                        xmlNewAttrib.Value = xmlNewAttrib.Value + " " + Convert.ToString(objPolicyXInsrdEnh.InsuredEid);
                        objNew.AppendChild(xmlItem);
                }//end foreach
            }
            //Add the XML to the SysExData 
            if (objOld != null)
                objXML.DocumentElement.ReplaceChild(objNew, objOld);
            else
                objXML.DocumentElement.AppendChild(objNew);


            //Clean up
            objOld = null;
            objNew = null;
            objCData = null;
            objXML = null;
        }
        //End:Add by kuladeep for Audit issue MITS:24736// skhar7 RMSc merge

        #region Common XML function.

        private void AppendRuntimeAttribute(XmlElement p_objParentNode, string p_sControlName, string p_sAttributeName, string p_sValue)
        {
            XmlElement objControlNode = null;
            XmlElement objAttributeNode = null;

            objControlNode = p_objParentNode.OwnerDocument.CreateElement(p_sControlName);
            objAttributeNode = p_objParentNode.OwnerDocument.CreateElement(p_sAttributeName);
            objAttributeNode.SetAttribute("value", p_sValue);
            objControlNode.AppendChild(objAttributeNode);
            p_objParentNode.AppendChild(objControlNode);
        }

        private void AppendRuntimeData(string p_objElementName, string p_sValue)
        {
            this.AppendRuntimeData(p_objElementName, p_sValue, "");
        }

        private void AppendRuntimeData(string p_objElementName, string p_sValue, string p_sElementType)
        {
            XmlDocument objSysEx = base.SysEx;
            XmlNode objOld = null;
            XmlElement objNew = null;

            // npadhy MITS 17516 Handled logic for Entity
            string sAbbreviation = string.Empty;
            string sName = string.Empty;

            objOld = objSysEx.SelectSingleNode("//" + p_objElementName);
            objNew = objSysEx.CreateElement(p_objElementName);

            if (p_sElementType == EnhancePolicyManager.ELEMENT_TYPE_CODE)
            {
                int iCodeId = Conversion.ConvertStrToInteger(p_sValue);
                objNew.SetAttribute("codeid", p_sValue);
                if (iCodeId != 0)
                    objNew.InnerText = EnhancePolicyManager.LocalCache.GetShortCode(iCodeId) + " " + EnhancePolicyManager.LocalCache.GetCodeDesc(iCodeId, base.Adaptor.userLogin.objUser.NlsCode);
                else
                    objNew.InnerText = "";
            }
            else if (p_sElementType == EnhancePolicyManager.ELEMENT_TYPE_ORG)
            {
                int iCodeId = Conversion.ConvertStrToInteger(p_sValue);
                objNew.SetAttribute("codeid", p_sValue);
                if (iCodeId != 0)
                {
                    EnhancePolicyManager.CCacheFunctions.GetOrgInfo(iCodeId, ref sAbbreviation, ref sName);
                    objNew.InnerText = sAbbreviation + " - " + sName;
                }
                else
                    objNew.InnerText = "";
            }
            else
            {
                objNew.InnerText = p_sValue;
            }

            if (objOld != null)
                objSysEx.DocumentElement.ReplaceChild(objNew, objOld);
            else
                objSysEx.DocumentElement.AppendChild(objNew);

        }

        protected void StartDocument(ref XmlDocument objXmlDocument, ref XmlElement p_objRootNode, string p_sRootNodeName)
        {
            objXmlDocument = new XmlDocument();
            p_objRootNode = objXmlDocument.CreateElement(p_sRootNodeName);
            objXmlDocument.AppendChild(p_objRootNode);
        }

        private void CreateElement(XmlElement p_objParentNode, string p_sNodeName)
        {
            XmlElement objChildNode = null;
            objChildNode = p_objParentNode.OwnerDocument.CreateElement(p_sNodeName);
            p_objParentNode.AppendChild(objChildNode);
            objChildNode = null;
        }

        
        protected void CreateElement(XmlElement p_objParentNode, string p_sNodeName, ref XmlElement p_objChildNode)
        {
            p_objChildNode = p_objParentNode.OwnerDocument.CreateElement(p_sNodeName);
            p_objParentNode.AppendChild(p_objChildNode);
        }
        
        protected void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText)
        {
            XmlElement objChildNode = null;
            this.CreateAndSetElement(p_objParentNode, p_sNodeName, p_sText, ref objChildNode);
        }
        
        protected void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText, ref XmlElement p_objChildNode)
        {
            CreateElement(p_objParentNode, p_sNodeName, ref p_objChildNode);
            p_objChildNode.InnerText = p_sText;
        }

        #endregion

        #region Org Hierarchy Validate On Filter Date
        // Charanpreet for MITS 11323 : Start
        private bool OrgHierarchyValidate(ref bool bCancel)
        {
            string sSql = "";            
            string sOrg_Trig = "";
            string sSys_dt = "";
            string sEff_start_dt = "";
            string sEff_end_dt = "";
            string m_EffectiveDate = Conversion.GetDate(base.GetSysExDataNodeText("/SysExData//EffectiveDate", true));

            //Add & Change by kuladeep for Audit issue MITS:24736 Start
            //string p_sInsuredEid = Conversion.ConvertObjToStr(PolicyEnh.PolicyXInsuredEnh);   
            //string sInsParentEid = GetParentEid(p_sInsuredEid);
            string p_sInsuredEid = string.Empty;
            foreach (PolicyXInsuredEnh objInsrd in PolicyEnh.PolicyXInsuredEnhList)
            {
                p_sInsuredEid = " " + objInsrd.InsuredEid + p_sInsuredEid;
            }
            if (p_sInsuredEid.Length > 1)
            {
                p_sInsuredEid = p_sInsuredEid.Substring(1);
            }
            string sInsParentEid = GetParentEid(p_sInsuredEid);
            //Add & Change by kuladeep for Audit issue MITS:24736 End //skhare7 RMSC emrge

            sSql = "SELECT TRIGGER_DATE_FIELD,EFF_START_DATE,EFF_END_DATE FROM ENTITY WHERE TRIGGER_DATE_FIELD IS NOT NULL AND ENTITY_ID IN (" + sInsParentEid + ")";
                using (DbReader objReader = DbFactory.GetDbReader(EnhancePolicyManager.ConnectionString, sSql))
                {
                    while (objReader.Read())
                    {
                        sOrg_Trig = objReader.GetValue(0).ToString();
                        if (sOrg_Trig != "")
                        {
                            sEff_start_dt = objReader.GetValue(1).ToString();
                            sEff_end_dt = objReader.GetValue(2).ToString();
                            if (!(sEff_start_dt == "" && sEff_end_dt == ""))
                            {
                                switch (sOrg_Trig)
                                {
                                    case "POLICY.EFFECTIVE_DATE,SYSTEM_DATE":
                                        if (sEff_start_dt != "")
                                            if (m_EffectiveDate.CompareTo(sEff_start_dt) < 0)
                                                return (true);
                                        if (sEff_end_dt != "")
                                            if (m_EffectiveDate.CompareTo(sEff_end_dt) > 0)
                                                return (true);
                                        break;
                                    case "SYSTEM_DATE":
                                        sSys_dt = Conversion.GetDate(DateTime.Now.ToShortDateString());
                                        if (sEff_start_dt != "")
                                            if (sSys_dt.CompareTo(sEff_start_dt) < 0)
                                                return (true);
                                        if (sEff_end_dt != "")
                                            if (sSys_dt.CompareTo(sEff_end_dt) > 0)
                                                return (true);
                                        break;
                                }
                            }
                        }
                    }
                }
            //}
            return (false);
        }
        #endregion

        #region Get ParentEids
        /// Name		: GetParentEid
        /// Author		: Prashant J Singh         
        /// Date Created: 05/18/09	
        /// MITS        : 11323
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Fetches the Table Id of the Org Hierarchy
        /// </summary>
        /// <param name="sInsEid">List of Insured</param>
        /// <returns>Entity Ids of parents of insureds</returns>

        private string GetParentEid(string sInsEid)
        {
            StringBuilder sParentEid = new StringBuilder();
            string sTempIns = string.Empty;
            string sParent = string.Empty;
            string sRet = string.Empty;

            string sSQL = string.Empty;	//sql string
            DbReader objReader = null;	//Data reader object

            string[] sInsList = sInsEid.Split(' ');


            try
            {
                foreach (string sInsId in sInsList)
                {
                    sTempIns = sInsId;

                    if (sTempIns != "0" && !String.IsNullOrEmpty(sTempIns))
                    {
                        sParentEid.Append(sTempIns);
                        sParentEid.Append(" ,");
                    }

                    while (sTempIns != "0" && !String.IsNullOrEmpty(sTempIns))
                    {
                        sSQL.Remove(0, sSQL.Length);
                        sSQL = "SELECT PARENT_EID FROM ENTITY WHERE ENTITY_ID =" + sTempIns;

                        //Fetch the Details for this Department
                        objReader = DbFactory.GetDbReader(EnhancePolicyManager.ConnectionString, sSQL);

                        if (objReader.Read())
                        {
                            sParent = Conversion.ConvertObjToStr(objReader.GetValue("PARENT_EID"));
                            if (sParent != "0" && !String.IsNullOrEmpty(sParent))
                            {
                                sParentEid.Append(sParent);
                                sTempIns = sParent;
                                sParentEid.Append(" ,");
                            }
                            else if (sParent == "0")
                                sTempIns = sParent;
                        }

                        objReader.Close();
                    }
                }
                sRet = sParentEid.ToString().TrimEnd(',');
            }
            catch (DataModelException p_objExp)
            {
                throw p_objExp;
            }
            catch (RMAppException p_objExp)
            {
                throw p_objExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("FormDataAdaptor.PolicyEnh.GetParentEid.Error", base.ClientId), p_objExp);
            }
            finally
            {
                if (objReader != null) objReader.Dispose();
            }

            return sRet;
        }
        #endregion
    }
}
