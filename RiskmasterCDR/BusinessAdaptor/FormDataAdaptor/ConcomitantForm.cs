using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

//TODO: Auto-Generated Form - migrate and verify behaviors from Legacy RMNet form.

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for Concomitant Screen.
	/// </summary>
	public class ConcomitantForm : DataEntryFormBase
	{
		const string CLASS_NAME = "EvXConcomProd";
		private EvXConcomProd EvXConcomProd{get{return objData as EvXConcomProd;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		const string FILTER_KEY_NAME = "EventId";
		public override void InitNew()
		{
			base.InitNew();

			//Filter by this EventId if Present.
			int eventId = base.GetSysExDataNodeInt(FILTER_KEY_NAME);
			if(eventId !=0)
			{
				(objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + eventId;
				// Mihika 11/30/2005 Defect no.405 Assigning the value of eventid to parentid
				this.m_ParentId = eventId;
                //Start by Shivendu for MITS 9677
                this.EvXConcomProd.EventId =  eventId;
                //Start by Shivendu for MITS 9677
			}			
		}

		public ConcomitantForm(FormDataAdaptor fda):base(fda){base.m_ClassName = CLASS_NAME;}
		
		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
			base.OnUpdateForm ();
		}

		// Mihika - Defect No.405 - 11/30/2005
		// throwing the proper parent key back on in case the MoveFirst\Last request
		// didn't find any records.
		public override void AfterAction(enumFormActionType eActionType)
		{
			base.AfterAction(eActionType);
			switch(eActionType)
			{
				case enumFormActionType.MoveFirst:
				case enumFormActionType.MoveLast:
				case enumFormActionType.MoveTo:
					if(EvXConcomProd.EventId==0) //No record found - treat as new.
						EvXConcomProd.EventId = this.m_ParentId;
					break;
			}
		}
	}
}