﻿using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Security.RMApp;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using Riskmaster.Settings;
using System.Collections.Generic;


namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for EventScreen.
	/// </summary>
	public class EventForm : DataEntryFormBase
	{
		const string CLASS_NAME = "Event";
		private const int RMO_EVENT_OSHA = 12500;
		private const int RMO_EVENT_MEDWATCH = 12050;
		private Event objEvent{get{return objData as Event;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		private bool bCloseDiaries = false;
		string m_EventLimitFilter="";
        int iEventId = 0;   // csingh7 : R6 Claim Comment Enhancement
        UserLoginLimits objLimits = null;

        //MITS 11874 Abhishek
        string sUserCloseDiary = "false";

 //PSO SMS Security offset--neha goel--11/10/2011
        private const int RMO_EVENT_PSODATA = 1220400185;
        private string sLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").Split('|')[0].ToString();
        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="fda"></param>
		public EventForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
            objLimits = base.GetUserLoginLimits();
            sLangCode= Convert.ToString(fda.userLogin.objUser.NlsCode);
		}

		//Bug no. 001395 Parag Start
		public override void Init()
		{
			base.Init();
					
			//Filter Innaccessible Event types based on UserLimit Security.
			if(objEvent.Context.InternalSettings.SysSettings.EVTypeAccFlag)//If UserLimits Enabled...
			{
				if(objLimits != null)
				{
					string[] arr = objLimits.EventAccessTypeList();
					string sTypeFilter = " EVENT_TYPE_CODE NOT IN (";
					for(int i = 0; i<arr.Length;i++)
					{
						if(i>0)
							sTypeFilter += ",";
						sTypeFilter += arr[i];
					}
					if(arr.Length==0)
						sTypeFilter = "";
					else
						sTypeFilter +=")";

					m_EventLimitFilter = sTypeFilter;

					if(m_EventLimitFilter !="")
						(objEvent as DataModel.INavigation).Filter += m_EventLimitFilter;
				}
			}
			
		}

        // This is invoked just after the memory object is being populated from Serialized Instance Xml.
		// Makes it an appropriate place to re-populate any additional "non-child" Xml object data.
        public override void OnUpdateObject()
        {
            XmlDocument objXML = base.SysEx;

            //For new claim, InitCClaim custom script could set value for Comments.
            //If LegacyComments is true, we need to bring value back
            objEvent.UpdateObjectComments(objXML, objEvent.EventId);
        }

		//Bug no. 001395 Parag End
		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
			
			base.OnUpdateForm ();

			XmlDocument objXML = base.SysEx;
            //XmlAttribute tempAttr = null;
            //For new claim, InitCClaim custom script could set value for Comments.
            //If LegacyComments is true, we need to bring value back
            objEvent.GetCommentsXml(ref objXML, objEvent.EventId);

            //MITS 11779:Asif code added for making time fields mandatory
            if (this.CurrentAction == enumFormActionType.AddNew)
            {
                if (objEvent.Context.InternalSettings.SysSettings.DefaultTimeFlag == -1)
                {
                    XmlNode xnode = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysNotReqNew");
                    if (xnode != null)
                    {
                        xnode.InnerText = xnode.InnerText + "|timerequired";
                        xnode = null;
                    }

                }
            }
            ////MITS 11779 END
			
			//*********************
			//Get Derived Fields
			//*********************

			int captionLevel = 1006;
			string sCaption = string.Empty;
            string sTmp = string.Empty;

			if(this.CurrentAction == enumFormActionType.AddNew)
				//We're adding a new event 
				base.ResetSysExData("NewEvent","1");
			//Event Category
			int eventCatId = objEvent.Context.DbConnLookup.ExecuteInt(
					//@"SELECT SYS_EVENT_CAT.DISPLAY_ID FROM SYS_EVENT_CAT, CODES
					//Changed By: Nikhil Garg	Dated: 13/Oct/2005	Bug No: 97
//					@"SELECT SYS_EVENT_CAT.EVENT_CAT_CODE FROM SYS_EVENT_CAT, CODES
//					WHERE SYS_EVENT_CAT.EVENT_CAT_CODE = CODES.RELATED_CODE_ID 
//					AND CODES.CODE_ID = " + objEvent.EventIndCode);

				// Mihika 18-Jan-2006 Defect no. 1574
				// Only those categories were shown for which an entry was made in SYS_EVENT_CAT table
				@"SELECT C2.CODE_ID FROM CODES C1, CODES C2
					WHERE C1.RELATED_CODE_ID = C2.CODE_ID
					AND C1.CODE_ID = " + objEvent.EventIndCode);

			int eventDisplayId=objEvent.Context.DbConnLookup.ExecuteInt(
						@"SELECT SYS_EVENT_CAT.DISPLAY_ID FROM SYS_EVENT_CAT, CODES
						WHERE SYS_EVENT_CAT.EVENT_CAT_CODE = CODES.RELATED_CODE_ID 
						AND CODES.CODE_ID = " + objEvent.EventIndCode);
//				objCache.GetRelatedCodeId(objEvent.EventIndCode);
			string catShortCode = string.Empty;
            string catCodeDesc = string.Empty;
			
			objCache.GetCodeInfo(eventCatId,ref catShortCode,ref catCodeDesc);

			XmlNode objNew = objXML.CreateElement("EventCat");
			XmlNode objOld = objXML.SelectSingleNode("//EventCat");

			XmlAttribute objAtt = objXML.CreateAttribute("tablename");
			objAtt.Value = objCache.GetTableName(objCache.GetCodeTableId(objCache.GetRelatedCodeId(objEvent.EventIndCode)));
			objNew.Attributes.Append(objAtt);
			
			objAtt = objXML.CreateAttribute("codeid");
			objAtt.Value = eventCatId.ToString();
			objNew.Attributes.Append(objAtt);
			
            objAtt = null;
			XmlCDataSection objCData = objXML.CreateCDataSection(String.Format("{0} {1}",catShortCode,catCodeDesc));
			objNew.AppendChild(objCData);
            objCData = null;
			if(objOld !=null)
				objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);
			string sNewEvent=base.GetSysExDataNodeText("/SysExData/NewEvent");

			//Check UserLimit Security.
			if(objEvent.Context.InternalSettings.SysSettings.EVTypeAccFlag)//If UserLimits Enabled...
			{
				if(objLimits != null)
					if ((!objEvent.IsNew && !objLimits.EventTypeAccessAllowed(objEvent.EventTypeCode) && sNewEvent!="1"))
					{
						base.ResetSysExData("NewEvent","0");
						Errors.Add(Globalization.GetString("LimitError",base.ClientId,sLangCode),
                            String.Format(Globalization.GetString("Access.Event.UserLimitFailed", base.ClientId, sLangCode), objCache.GetCodeDesc(objEvent.EventTypeCode), base.Adaptor.userLogin.objUser.NlsCode),
							BusinessAdaptorErrorType.SystemError); //Aman ML Change
						return;
					}
			}

            objNew = null;
            objOld = null;

			//Hide Medwatch\FallInfo buttons appropriately.
			switch(eventDisplayId)
			{
				case (int)Constants.EVENT_CATEGORY_DISPLAY_ID.Medication:
				case (int)Constants.EVENT_CATEGORY_DISPLAY_ID.Equipment: //Medication or Equipment
					base.AddKillNode("btnFallInfo");
                    base.AddDisplayNode("btnMedWatch");
					break;
				case (int)Constants.EVENT_CATEGORY_DISPLAY_ID.Fall:
					base.AddKillNode("btnMedWatch");
                    base.AddDisplayNode("btnFallInfo");
					break;
				default:
					base.AddKillNode("btnFallInfo");
					base.AddKillNode("btnMedWatch");
					break;
			}
			
			//Hide Sentinel button appropriately.
			bool isSentinelEvent = (0 != objEvent.Context.DbConnLookup.ExecuteInt("SELECT EVENT_IND_CODE FROM SYS_SENT_EVENT WHERE EVENT_IND_CODE=" + objEvent.EventIndCode));
			if(!isSentinelEvent)
				base.AddKillNode("btnSentinel");
            else
                base.AddDisplayNode("btnSentinel");


			//Handle User Specific Caption Level
			if(objEvent.EventId !=0)
			{
				if(objEvent.Context.InternalSettings.SysSettings.EvCaptionLevel!=0)
					captionLevel = objEvent.Context.InternalSettings.SysSettings.EvCaptionLevel;
				else
                    // Charanpreet for MITS 12519 : Start
                    //captionLevel = 1006;
                    captionLevel = 0;
                // Charanpreet for MITS 12519 : End
			
				if(captionLevel <1005 || captionLevel >1012)
					captionLevel = 0;
				
				if (captionLevel==0)
				{
					sCaption="";
				}
				else if(captionLevel !=1012)
					sCaption = objEvent.Context.InternalSettings.CacheFunctions.GetOrgParent(objEvent.DeptEid,captionLevel,0,ref captionLevel);//captionLevel discarded
				else
				{
					objEvent.Context.InternalSettings.CacheFunctions.GetOrgInfo(objEvent.DeptEid,ref sCaption,ref sTmp);
					sCaption += " "+sTmp; 
				}
				sCaption = sCaption.Substring(0, Math.Min(sCaption.Length, 25));
			
				if(objEvent.EventId!=0 && objEvent.PiList.GetPrimaryPi()!=null)
					sCaption = " [" + objEvent.EventNumber + " * " + sCaption + " * " +	
						objEvent.PiList.GetPrimaryPi().PiEntity.GetLastFirstName() + "]";
				else
					sCaption = " [" + objEvent.EventNumber + " * " + sCaption + " * - ]";
			}
			//Pass this subtitle value to view (ref'ed from @valuepath).
			base.ResetSysExData("SubTitle",sCaption);
            ArrayList singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singleRow, "id", "formsubtitle");
            base.AddElementToList(ref singleRow, "Text", sCaption);
            base.m_ModifiedControls.Add(singleRow);

			//Handle Active Quality Management Level Selection
			//Defect No: 000579. Nikhil Garg	Dated: 16-Mar-2006
			//This link should always be available
			//base.AddKillNode("initialreview");
			base.AddKillNode("physicianadvisorreview");
			base.AddKillNode("committeedepartmentreview");
			base.AddKillNode("qualitymanagerreview");
			base.AddKillNode("initialreviewlight");
			base.AddKillNode("physicianadvisorreviewlight");
			base.AddKillNode("committeedepartmentreviewlight");
			base.AddKillNode("qualitymanagerreviewlight");

            //MITS 11874 Abhishek - start
            //R7 PRF IMP: passing Dsn
            base.ResetSysExData("AllCodes", m_fda.Factory.Context.LocalCache.GetAllParentCodesForClosedEvents(241, m_fda.Factory.Context.RMDatabase.DataSourceId));

            //check whether open diaries exist for this event or not
            base.ResetSysExData("CloseDiary", "false");
            //skhare7 perf 
            string sSQL = "SELECT COUNT(1) FROM WPA_DIARY_ENTRY WHERE STATUS_OPEN<>0 AND ATTACH_RECORDID = " + objEvent.EventId.ToString() + " AND ATTACH_TABLE = 'EVENT'";
            int iNumDiary = objEvent.Context.DbConn.ExecuteInt(sSQL);
            if ( iNumDiary > 0 )
                base.ResetSysExData("ContainsOpenDiaries", "true");
            else
                base.ResetSysExData("ContainsOpenDiaries", "false");

            //MITS 11874 Abhishek - end

			//Raman Bhatia .. As per email from Mike, Making changes in Progress Notes Design
			//Kill Comments or Enhanced Notes node depending on SYS_PARMS_LOB settings
	
			/*
			try
			{
				int iUseClaimProgressNotes = objEvent.Context.DbConnLookup.ExecuteInt(
					@"SELECT TOP 1 USE_CLAIM_PROGRESS_NOTES FROM SYS_PARMS_LOB");

				if(iUseClaimProgressNotes == -1)
					base.AddKillNode("comments");
				else
					base.AddKillNode("enhancednotes");
			}
			catch
			{
				base.AddKillNode("enhancednotes");
			}
			*/

			//Defect #1464 - Able to see only Initial Review Link on Event. 
			/* Previuosly the reviews were shown one by one, now all reviews
			 * are shown. Following the order of reviews i.e. first initial review then 
			 * physician adviser review and like that, review which is currently in order 
			 * and not done is shown in blue, others are in gray
			 */

			//Defect No: 000579. Nikhil Garg	Dated: 16-Mar-2006
			//Changed the logic for displaying active/inactive links for quality management reviews
			if(objEvent.EventQM.IrDetermCode !=0)
			{
				base.KillNodes.Remove("initialreviewk");
				base.KillNodes.Remove("physicianadvisorreviewk");
                base.AddDisplayNode("initialreview");
                base.AddDisplayNode("physicianadvisorreview");
			}
			else
			{
				if (objEvent.EventQM.PaDetermCode == 0 && objEvent.EventQM.CdDetermCode == 0 
					&& objEvent.EventQM.QmDetermCode == 0)
				{
					base.KillNodes.Remove("physicianadvisorreviewlightk");
					base.KillNodes.Remove("committeedepartmentreviewlightk");
					base.KillNodes.Remove("qualitymanagerreviewlightk");
                    base.AddDisplayNode("physicianadvisorreviewlight");
                    base.AddDisplayNode("committeedepartmentreviewlight");
                    base.AddDisplayNode("qualitymanagerreviewlight");
				}
			}

			if(objEvent.EventQM.PaDetermCode !=0)
			{                
				base.KillNodes.Remove("initialreviewk");
				base.KillNodes.Remove("physicianadvisorreviewk");
				base.KillNodes.Remove("committeedepartmentreviewk");
                base.AddDisplayNode("initialreview");
                base.AddDisplayNode("physicianadvisorreview");
                base.AddDisplayNode("committeedepartmentreview");
			}
			else
			{
				if(objEvent.EventQM.QmDetermCode == 0 && objEvent.EventQM.CdDetermCode == 0)
				{
					base.KillNodes.Remove("committeedepartmentreviewlight");
					base.KillNodes.Remove("qualitymanagerreviewlight");
                    base.AddDisplayNode("committeedepartmentreviewlight");
                    base.AddDisplayNode("qualitymanagerreviewlight");
				}
			}

			if(objEvent.EventQM.CdDetermCode !=0)
			{
				base.KillNodes.Remove("initialreviewk");
				base.KillNodes.Remove("physicianadvisorreviewk");
				base.KillNodes.Remove("committeedepartmentreviewk");
				base.KillNodes.Remove("qualitymanagerreviewk");
                base.AddDisplayNode("initialreview");
                base.AddDisplayNode("physicianadvisorreview");
                base.AddDisplayNode("committeedepartmentreview");
                base.AddDisplayNode("qualitymanagerreview");
			}
			else
			{
                if (objEvent.EventQM.QmDetermCode == 0)
                {
                    base.KillNodes.Remove("qualitymanagerreviewlightk");
                    base.AddDisplayNode("qualitymanagerreviewlight");
                }
			}

			if(objEvent.EventQM.QmDetermCode !=0)
			{
				base.KillNodes.Remove("initialreviewk");
				base.KillNodes.Remove("physicianadvisorreviewk");
				base.KillNodes.Remove("committeedepartmentreviewk");
				base.KillNodes.Remove("qualitymanagerreviewk");
                base.AddDisplayNode("initialreview");
                base.AddDisplayNode("physicianadvisorreview");
                base.AddDisplayNode("committeedepartmentreview");
                base.AddDisplayNode("qualitymanagerreview");
			}

            // ABhateja, 05.07.2007 -START-
            // MITS 9980, Commented the code for location and event description 
            // freeze flags as it is being checked in the editor.cs file.
            //Gather all controls who need to be read-only based on business rules.
            //if(objEvent.Context.InternalSettings.SysSettings.FreezeEventDesc!=0)
            //    base.AddReadOnlyNode("eventdescription");
			
			if(		(objEvent.EventId ==0 && !objEvent.Context.RMUser.IsAllowedEx(this.SecurityId,RMO_ALLOW_ENTRY_EVENT_NUM))
				||	(objEvent.EventId !=0 && !objEvent.Context.RMUser.IsAllowedEx(this.SecurityId,RMO_ALLOW_EDIT_EVENT_NUM)))
				base.AddReadOnlyNode("eventnumber");
			
            //JIRA 1342 ajohari2: Start
            bool bIsAnyClaimHavePolicy = false;
            if (objEvent.EventId != 0 && objEvent.ClaimList.Count > 0 )
            {
                foreach (Claim objClaimTemp in objEvent.ClaimList)
                {
                    if (objClaimTemp.ClaimPolicyList.Count > 0)
                    {
                        bIsAnyClaimHavePolicy = true;
                        break;
                    }
                }
            }
            
            CreateSysExData("IsAnyClaimHavePolicy",Convert.ToString(bIsAnyClaimHavePolicy));
            //JIRA 1342 ajohari2: End
            //if(objEvent.Context.InternalSettings.SysSettings.FreezeLocDesc!=0)
            //    base.AddReadOnlyNode("locationareadesc");
            // ABhateja, 05.07.2007 -END-

				
			if(!objEvent.Context.RMUser.IsAllowedEx(this.SecurityId,FormBase.RMO_SHOW_QUALITY_MANAGEMENT_TAB))			
				base.AddKillNode("qualitymanagement");
			if(!objEvent.Context.RMUser.IsAllowedEx(this.SecurityId,FormBase.RMO_LAUNCH_CLAIM_FROM_EVENT_SCREEN))
					base.AddKillNode("btnClaim");

			//TR #2299,2303 Pankaj 02/28/06 OSHA and medwatch security
			if(base.ModuleSecurityEnabled)//Is Security Enabled
			{
				if(!objEvent.Context.RMUser.IsAllowedEx(RMO_EVENT_OSHA))
					base.AddKillNode("btnOSHA");

				if(!objEvent.Context.RMUser.IsAllowedEx(RMO_EVENT_MEDWATCH))
					base.AddKillNode("btnMedWatch");
			}
			if (objEvent.EventQM.EventId==0)
				objEvent.EventQM.EventId=objEvent.EventId;
			if (objEvent.EventMedwatch.EventId==0)
				objEvent.EventMedwatch.EventId=objEvent.EventId;

			//Check for People Maintenance permission
			PeoplePermissionChecks4Other("event", m_SecurityId+RMO_UPDATE);

            //tkr mits 8764.  auto-populate event location fields
            //MGaba2:MITS 15642:creating the node so that it doesnt come in missing refs:start
            CreateSysExData("EventOnPremiseChecked","false");
            //Reload the department data if this node is true
            if (base.GetSysExDataNodeText("/SysExData/EventOnPremiseChecked") == "true")
            {//MGaba2:MITS 15642:End
                PopulateEventDetailLocation(objEvent);
                base.ResetSysExData("EventOnPremiseChecked", "false");//MGaba2:MITS 15642
            }

			//shruti, MITS 8507 starts
            //using(DbConnection objCon = DbFactory.GetDbConnection(objEvent.Context.DbConn.ConnectionString))
            //{
            //    objCon.Open();
            //    int iDbData = Conversion.ConvertStrToInteger(objCon.ExecuteString("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='QUALITY_MGT_FLAG'"));
			//R7 PRF IMP: Using sysSettings
            int iDbData = objEvent.Context.InternalSettings.SysSettings.QualityMgtFlag;
            if(iDbData == 0)
					base.AddKillNode("qualitymanagement");
            //    objCon.Close();
            //}	
			//shruti, MITS 8507 ends

            //tkr mits 10267.  need entire org hierarchy to hide/show group assoc supp fields
            bool isOhSet = false;
            if (objEvent.DeptEid > 0)
            {
                using (DbReader rdr = objEvent.Context.DbConnLookup.ExecuteReader("SELECT * FROM ORG_HIERARCHY WHERE DEPARTMENT_EID = " + objEvent.DeptEid.ToString()))
                {
                    while (rdr.Read())
                    {
                        isOhSet = true;
                        base.ResetSysExData("OH_FACILITY_EID", rdr.GetInt("FACILITY_EID").ToString());
                        base.ResetSysExData("OH_LOCATION_EID", rdr.GetInt("LOCATION_EID").ToString());
                        base.ResetSysExData("OH_DIVISION_EID", rdr.GetInt("DIVISION_EID").ToString());
                        base.ResetSysExData("OH_REGION_EID", rdr.GetInt("REGION_EID").ToString());
                        base.ResetSysExData("OH_OPERATION_EID", rdr.GetInt("OPERATION_EID").ToString());
                        base.ResetSysExData("OH_COMPANY_EID", rdr.GetInt("COMPANY_EID").ToString());
                        base.ResetSysExData("OH_CLIENT_EID", rdr.GetInt("CLIENT_EID").ToString());
                    }
                }
            }
            if (!isOhSet)
            {
                base.ResetSysExData("OH_FACILITY_EID", "0");
                base.ResetSysExData("OH_LOCATION_EID", "0");
                base.ResetSysExData("OH_DIVISION_EID", "0");
                base.ResetSysExData("OH_REGION_EID", "0");
                base.ResetSysExData("OH_OPERATION_EID", "0");
                base.ResetSysExData("OH_COMPANY_EID", "0");
                base.ResetSysExData("OH_CLIENT_EID", "0");
            }
            CreateSysExData("UseLegacyComments", Riskmaster.Common.Conversion.ConvertObjToStr(objEvent.Context.InternalSettings.SysSettings.UseLegacyComments));
            //pmittal5- Confidential Record
            base.CreateSysExData("hdnselectedpermsusers");
            if (!(Adaptor.userLogin.objRiskmasterDatabase.OrgSecFlag && Adaptor.userLogin.objRiskmasterDatabase.ConfFlag))
                base.AddKillNode("confrecflag");
 //spahariya MITS 25646 start- 08/05/2011 hide/dispaly PSO data button on event screen based on utility setting for PSO reporting system
            //skhare7 performance change code used in setting
            if ((objEvent.Context.InternalSettings.SysSettings.UsePSOReportingSys))
            {
                if (!objEvent.Context.RMUser.IsAllowedEx(RMO_EVENT_PSODATA))
                {
                    base.AddKillNode("psodata");
                }


                // PSHEKHAWAT 09/27/2012 : Adding to fetch Event types excluded from PSO
                sSQL = "Select EVENT_TYPE_CODE from SYS_EVENT_Type_PSO";
                string sEvtTypeValues = string.Empty;
                DbReader objReader = DbFactory.GetDbReader(objEvent.Context.DbConn.ConnectionString, sSQL.ToString());
                while (objReader.Read())
                {
                    sEvtTypeValues = sEvtTypeValues + " " + Conversion.ConvertObjToStr(objReader.GetValue("EVENT_TYPE_CODE"));
                }

            base.ResetSysExData("PSO_EVENT_TYPES", sEvtTypeValues.Trim());
            // PSHEKHAWAT : Addition Ends
            //spahariya MITS 25646 - end

              
                //Anu Tennyson : Added this code to set the value of the already saved pso data : Starts
                SetPSORowIDs();
                //Anu Tennyson : Ends
            }
            else
            {
                base.AddKillNode("psodata");
            }
            base.ResetSysExData("hdnselectedpermsusers", "0");

            if (objEvent.ConfRecFlag && (!base.Adaptor.userLogin.IsOrgSecAdmin))
            {
                    base.AddReadOnlyNode("confrecflag");
            }
            //End - pmittal5
            //BOB Enhancement 
            base.ResetSysExData("isAutoPopulateDpt", objEvent.Context.InternalSettings.SysSettings.AutoPopulateDpt.ToString());
            //Added by Amitosh for R8 enhancement 
            if (objEvent.Context.InternalSettings.SysSettings.MultiCovgPerClm != -1)
            {
                base.AddKillNode("catastrophenumber");
           
            }
            //End Amitosh
		}

        //pmittal5 - Confidential Record
        private void ConfRecUsers(Event objEvent)
        {
            String sUserID = String.Empty;
            String sSql = String.Empty;
            String[] ArrUser;
            int iEventID = 0;
            int iUserID = 0;
            int iRec = 0;
            int iCount = 0;
            sUserID = GetSysExDataNodeText("/SysExData/hdnselectedpermsusers");
            sUserID = sUserID.TrimStart(',');
            sUserID = sUserID.Trim();
            ArrUser = sUserID.Split(',');

            iEventID = objEvent.EventId;

            try
            {
                if (((sUserID != "0") && (sUserID != "")) || ((!objEvent.ConfRecFlag) && (objEvent.EventId != 0)))
                {
                    sSql = "UPDATE CONF_EVENT_PERM SET DELETED_FLAG=-1 WHERE EVENT_ID=" + iEventID;
                    iRec = objEvent.Context.DbConnLookup.ExecuteNonQuery(sSql);

                    for (int i = 0; i < ArrUser.Length; i++)
                    {
                        if ((ArrUser[i] != "") && (ArrUser[i] != "0"))
                        {
                            sSql = "SELECT USER_ID FROM CONF_EVENT_PERM WHERE EVENT_ID=" + iEventID + " AND USER_ID=" + ArrUser[i];
                            iUserID = Conversion.ConvertObjToInt(objEvent.Context.DbConnLookup.ExecuteScalar(sSql), base.ClientId);
                            if (iUserID == 0)
                            {

                                sSql = "INSERT INTO CONF_EVENT_PERM (EVENT_ID,USER_ID,DELETED_FLAG,DTTM_ADDED,ADDED_BY_USER) VALUES (" + iEventID + "," + ArrUser[i] + ",0,'" + objEvent.DttmRcdAdded + "','" + objEvent.AddedByUser + "')";
                                iRec = objEvent.Context.DbConnLookup.ExecuteNonQuery(sSql);
                            }
                            else
                            {
                                sSql = "UPDATE CONF_EVENT_PERM SET DELETED_FLAG=0,DTTM_RCD_LAST_UPD='" + objEvent.DttmRcdLastUpd + "',UPDATED_BY_USER='" + objEvent.UpdatedByUser + "' WHERE EVENT_ID=" + iEventID + " AND USER_ID=" + ArrUser[i];
                                iRec = objEvent.Context.DbConnLookup.ExecuteNonQuery(sSql);
                            }
                        }
                    }
                    //Update Event,Claim and Person_Involved tables when Conf_Event_Perm is updated
                    sSql = "SELECT COUNT(*) FROM CONF_EVENT_PERM WHERE DELETED_FLAG = 0 AND EVENT_ID = " + iEventID;
                    iCount = Conversion.ConvertObjToInt(objEvent.Context.DbConnLookup.ExecuteScalar(sSql), base.ClientId);
                    if (iCount > 0)
                    {
                        sSql = "UPDATE EVENT SET CONF_EVENT_ID = " + iEventID + " WHERE EVENT_ID = " + iEventID;
                        iRec = objEvent.Context.DbConnLookup.ExecuteNonQuery(sSql);
                        sSql = "UPDATE CLAIM SET CONF_FLAG = -1 WHERE EVENT_ID = " + iEventID;
                        iRec = objEvent.Context.DbConnLookup.ExecuteNonQuery(sSql);
                        sSql = "UPDATE PERSON_INVOLVED SET CONF_FLAG = -1 WHERE EVENT_ID = " + iEventID;
                        iRec = objEvent.Context.DbConnLookup.ExecuteNonQuery(sSql);
                    }
                    else
                    {
                        sSql = "UPDATE EVENT SET CONF_EVENT_ID = 0 WHERE EVENT_ID = " + iEventID;
                        iRec = objEvent.Context.DbConnLookup.ExecuteNonQuery(sSql);
                        sSql = "UPDATE CLAIM SET CONF_FLAG = 0 WHERE EVENT_ID = " + iEventID;
                        iRec = objEvent.Context.DbConnLookup.ExecuteNonQuery(sSql);
                        sSql = "UPDATE PERSON_INVOLVED SET CONF_FLAG = 0 WHERE EVENT_ID = " + iEventID;
                        iRec = objEvent.Context.DbConnLookup.ExecuteNonQuery(sSql);
                    }
                }
            }

            catch (Exception e)
            {
                Errors.Add(e, Globalization.GetString("FormDataAdaptor.ScreenBase.ConfRecUsers.Exception", base.ClientId, sLangCode), Common.BusinessAdaptorErrorType.Error);
                m_CurrentAction = enumFormActionType.None;
            }
		}

		public override void BeforeSave(ref bool Cancel)
		{
			base.BeforeSave (ref Cancel);
			
			//Check UserLimit Security.
			if(objEvent.Context.InternalSettings.SysSettings.EVTypeAccFlag)//If UserLimits Enabled...
			{
				if(objLimits != null)
					if((!objLimits.EventTypeCreateAllowed(objEvent.EventTypeCode)))
					{
                        Errors.Add(Globalization.GetString("SaveError", base.ClientId, sLangCode),
                            String.Format(Globalization.GetString("Save.UserLimitFailed", base.ClientId, sLangCode), objCache.GetCodeDesc(objEvent.EventTypeCode), base.Adaptor.userLogin.objUser.NlsCode),
							BusinessAdaptorErrorType.Error); //Aman ML Change
						Cancel = true;
						return;
					}
			}
			// Legacy version includes a check of the Reporter Entity 
			// needed to decide whether to populate the reporter entity child.
			// Was based on non-empty Last name.
			// BSB: Verify that this is not really required anymore.  This is handled
			// inside of the Datamodel population/serialization logic.

			//Check Permission to change department.
			if(m_fda.userLogin.objRiskmasterDatabase.Status)//Is Security Enabled
			{
				if(objEvent.EventId!=0 && objEvent.DeptEid != objEvent.Context.DbConnLookup.ExecuteInt("SELECT DEPT_EID FROM EVENT WHERE EVENT_ID=" + objEvent.EventId))
					if(!m_fda.userLogin.IsAllowedEx(this.SecurityId,FormBase.RMO_EVENT_CHANGE_DEPARTMENT))
					{
                        Errors.Add(Globalization.GetString("SaveError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Save.DeptChangeFailed", base.ClientId, sLangCode)),
						BusinessAdaptorErrorType.Error);
						Cancel = true;
						return;
					}
			}
			
			//Default the Time of Event & Time Reported
			if(objEvent.TimeOfEvent=="" && objEvent.Context.InternalSettings.SysSettings.DefaultTimeFlag !=-1)
				objEvent.TimeOfEvent= "12:00 AM";
			if(objEvent.TimeReported=="" && objEvent.Context.InternalSettings.SysSettings.DefaultTimeFlag !=-1)
				objEvent.TimeReported= "12:00 AM";
			// Decide if we should close open event diaries after the save...
			// Based on changing status of the event to closed.
			bCloseDiaries = (objEvent.EventId!=0 
							&& objEvent.EventStatusCode != objEvent.Context.DbConnLookup.ExecuteInt("SELECT EVENT_STATUS_CODE FROM EVENT WHERE EVENT_ID=" + objEvent.EventId)
							&& objCache.GetRelatedCodeId(objEvent.EventStatusCode)==8);
            //Abhishek MITS 11874
            sUserCloseDiary = base.SysEx.SelectSingleNode("//CloseDiary").InnerText;
            //pmittal5 - Update Conf_event_perm table before event is saved.
            //When Event is saved, Conf_flag is updated in Event table, while Conf_Event_id not.
            //So a user whose views are defined and is permitted to view this event, is not able to select/update such event.
            if (Adaptor.userLogin.objRiskmasterDatabase.OrgSecFlag && Adaptor.userLogin.objRiskmasterDatabase.ConfFlag)
                ConfRecUsers(objEvent);
            //End
        }
		public override void AfterSave()
		{
			base.AfterSave ();
            //MITS 11874 Abhishek added condition sUserCloseDiary == "true"
			//Check and close Diaries
            if (bCloseDiaries && sUserCloseDiary == "true")
				objEvent.Context.DbConnLookup.ExecuteNonQuery(
					String.Format(@"UPDATE WPA_DIARY_ENTRY SET STATUS_OPEN = 0 
					WHERE ATTACH_RECORDID = {0} AND ATTACH_TABLE = 'EVENT'",objEvent.EventId));
			bCloseDiaries =false;
		}

        public override void BeforeDelete(ref bool Cancel)  // Added by csingh7 R6 Claim Comment Enhancement
        {
            iEventId = objEvent.EventId;
        }
        public override void AfterDelete()  // Added by csingh7 R6 Claim Comment Enhancement
        {
            base.AfterDelete();
            if (iEventId != 0)
            {
                objEvent.Context.DbConnLookup.ExecuteNonQuery(
                   String.Format(@"DELETE FROM COMMENTS_TEXT 
                WHERE ATTACH_RECORDID = {0} AND ATTACH_TABLE = 'EVENT'", iEventId));
            }
        }

        public override Hashtable GetTrimHashTable()
        {
            Hashtable ht = new Hashtable();
            ht.Add("Comments", "Comments");
            ht.Add("HTMLComments", "HTMLComments");
            ht.Add("PiList", "PiList");
            ht.Add("ProgressNoteList", "ProgressNoteList");
            ht.Add("EventOsha", "EventOsha");
            ht.Add("EventFallIndicator", "EventFallIndicator");
            ht.Add("EventMedwatch", "EventMedwatch");
            ht.Add("EventSentinel", "EventSentinel");
            ht.Add("ClaimList", "ClaimList");
            return ht;
        }

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnValidate(ref bool Cancel)
		{
			bool bError = false;

			// Perform data validation
            // Time Zone Enhancement for R7: yatharth
            string sToday = Conversion.ToDbDate(System.DateTime.Now);
            string sTime = Conversion.GetTime(System.DateTime.Now.ToShortTimeString()); // Current server Time
            string sOrgLevelToday = Conversion.ToDbDate(System.DateTime.Now); //Current Org Level Date
            string sOrgLevelTime = Conversion.GetTime(System.DateTime.Now.ToShortTimeString()); //Current Org Level Time
            int iFacilityID = 0;
            //int iDateChange = 0; //0-> No Change in date, 1-> Subtract 1 day from date, 2-> Add 1 Day to date
            DateTime dtOrgLevelCurrentDateTime = System.DateTime.Now;//For storing Current Date time of Org Level
            string sOrgLevelCurrentDate = string.Empty; //Current Date in Org Level
            string sOrgLevelCurrentTime = string.Empty; //Current Time in Org Level
            DateTime dtTemp = System.DateTime.Now.ToUniversalTime(); //DateTime in GMT
            string sSQL = string.Empty;
            int iTimeZoneTracking = 0; //-1 if Time Zone is enabled 
            //int iDayLightSavings = 0; //-1 if Day Light Savings is enabled
            //bool bDayLightSavings = false; //True for Day Light Savings and vice-versa
            string sTimeZone = string.Empty;//For storing TimeZone of Facility
            int OrgTimeZoneLevel = 0;

            //pmittal5 Mits 18751 09/20/10 - If existing Event Number is used
            int iEventId = 0;
            object oEvent = null;
            bool bEventFound = false;
             //Parag R7 : We will check for Duplicate EventNumber only in case when Generate AutoEventNumber is OFF
            if (!objEvent.EventNumber.Equals(""))
            {
                Dictionary<string, string> dictParams = new Dictionary<string, string>();
                if (objEvent.EventId == 0)
                {

                    //sSQL = String.Format("SELECT EVENT_ID FROM EVENT WHERE EVENT_NUMBER = '{0}'", objEvent.EventNumber);
                    sSQL = String.Format("SELECT EVENT_ID FROM EVENT WHERE EVENT_NUMBER = {0}", "~EVENTNUMBER~");
                    dictParams.Add("EVENTNUMBER", objEvent.EventNumber);
                }
                else
                {
                    //sSQL = String.Format("SELECT EVENT_ID FROM EVENT WHERE EVENT_NUMBER = '{0}' and EVENT_ID <> {1} ", objEvent.EventNumber, objEvent.EventId);
                    sSQL = String.Format("SELECT EVENT_ID FROM EVENT WHERE EVENT_NUMBER = {0} and EVENT_ID <> {1} ", "~EVENTNUMBER~", "~EVENTID~");
                    dictParams.Add("EVENTNUMBER", objEvent.EventNumber);
                    dictParams.Add("EVENTID", objEvent.EventId.ToString());
                }

                //oEvent = objEvent.Context.DbConn.ExecuteScalar(sSQL);//Deb MITS 25141
                oEvent = DbFactory.ExecuteScalar(objEvent.Context.DbConn.ConnectionString, sSQL, dictParams);
                if (oEvent != null)
                {
                    iEventId = Conversion.CastToType<int>(oEvent.ToString(), out bEventFound);
                    if (iEventId != 0)
                    {
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                                    String.Format(Globalization.GetString("Validation.EventAlreadyExists", base.ClientId, sLangCode), objEvent.EventNumber.ToString()),
                                    BusinessAdaptorErrorType.Error);
                        bError = true;
                    }
                }
            }
            //End - pmittal5
            //BOB Enhancement  :  Ukusvaha
            if (objEvent.DeptEid == 0)
            {
                if (objEvent.Context.InternalSettings.SysSettings.AutoPopulateDpt == -1)
                {

                    objEvent.DeptEid = objEvent.Context.InternalSettings.SysSettings.AutoFillDpteid;
                }
            }
            //End BOB Enhancement 
            //sSQL = @"SELECT ORG_TIMEZONE_LEVEL FROM SYS_PARMS";
            //OrgTimeZoneLevel = Conversion.ConvertStrToInteger(objEvent.Context.DbConn.ExecuteScalar(sSQL).ToString());

            OrgTimeZoneLevel = objEvent.Context.InternalSettings.SysSettings.OrgTimeZoneLevel;
              //Parag R7 : We will check for Time Validation only in case when Time Validation is ON
            if (OrgTimeZoneLevel != 0)
            {
                sSQL = @"SELECT ";

                switch (OrgTimeZoneLevel)
                {
                    case 1005:
                        sSQL = sSQL + "CLIENT_EID";
                        break;
                    case 1006:
                        sSQL = sSQL + "COMPANY_EID";
                        break;
                    case 1007:
                        sSQL = sSQL + "OPERATION_EID";
                        break;
                    case 1008:
                        sSQL = sSQL + "REGION_EID";
                        break;
                    case 1009:
                        sSQL = sSQL + "DIVISION_EID";
                        break;
                    case 1010:
                        sSQL = sSQL + "LOCATION_EID";
                        break;
                    case 1011:
                        sSQL = sSQL + "FACILITY_EID";
                        break;
                    default:
                        sSQL = sSQL + "DEPARTMENT_EID";
                        break;
                }

                sSQL = sSQL + " FROM ORG_HIERARCHY WHERE DEPARTMENT_EID = " + objEvent.DeptEid;

            
            //sSQL = sSQL + " FROM ORG_HIERARCHY WHERE DEPARTMENT_EID = " + objEvent.DeptEid;
                //END
                iFacilityID = Conversion.ConvertStrToInteger(objEvent.Context.DbConn.ExecuteScalar(sSQL).ToString());

                //To get Time Zone of the facility is enabled or not
                sSQL = @"SELECT TIME_ZONE_TRACKING FROM ENTITY WHERE ENTITY_ID = " + iFacilityID.ToString();
                iTimeZoneTracking = Conversion.ConvertObjToInt(objEvent.Context.DbConn.ExecuteScalar(sSQL), base.ClientId);

                if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
                {

                    sSQL = @"SELECT SHORT_CODE FROM CODES WHERE CODE_ID=
                        (SELECT TIME_ZONE_CODE FROM ENTITY WHERE ENTITY_ID=" + iFacilityID.ToString() + ")";
                    sTimeZone = objEvent.Context.DbConn.ExecuteScalar(sSQL).ToString();

                    //To see Day light saving is enabled or not for Org Level
                //sSQL = @"SELECT DAY_LIGHT_SAVINGS FROM ENTITY WHERE ENTITY_ID = " + iFacilityID.ToString();
                //iDayLightSavings = Conversion.ConvertObjToInt(objEvent.Context.DbConn.ExecuteScalar(sSQL), base.ClientId);

                //If Daylight savings are enabled
                //if (iDayLightSavings != 0)
                //{
                //    bDayLightSavings = true;
                //}
                ////If Daylight savings are disabled
                //else
                //{
                //    bDayLightSavings = false;
                //}

                //Current Time in Org Level
                //sOrgLevelTime = Conversion.ConvertGMTToOrgHierarchyTime(sTimeZone, Conversion.ToDbTime(System.DateTime.Now.ToUniversalTime()), bDayLightSavings, ref iDateChange);
                dtOrgLevelCurrentDateTime = Conversion.ConvertGMTToOrgHierarchyTime(sTimeZone, dtTemp);
                //if (iDateChange != 0)
                //{
                //    if (iDateChange == 1)
                //    {
                //        dtTemp = dtTemp.AddDays(-1);
                //    }
                //    else
                //    {
                //        dtTemp = dtTemp.AddDays(1);
                //    }
                //}
                    //Current Date in Org Level
                sOrgLevelToday = Conversion.ToDbDate(dtOrgLevelCurrentDateTime);
                //Date in Org Level in mm/dd/yy format for displaying validation error message
                //sOrgLevelCurrentDate = dtTemp.ToShortDateString();
                //Time in Org Level for displaying validation error message
                sOrgLevelCurrentTime = Conversion.ToDbTime(dtOrgLevelCurrentDateTime);
                //dtOrgLevelCurrentDateTime = Convert.ToDateTime(sOrgLevelCurrentDate + " " + sOrgLevelCurrentTime);
                }
            }
            //Validation for Time Of Event :START
            if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
            {
                if (objEvent.DateOfEvent.CompareTo(sOrgLevelToday) == 0 && objEvent.TimeOfEvent.CompareTo(sOrgLevelCurrentTime) > 0)//rsushilaggar MITS 24067 Date 03/11/2011
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                    String.Format(Globalization.GetString("Field.TimeOfEvent", base.ClientId, sLangCode), dtOrgLevelCurrentDateTime.ToLongTimeString()),
                    BusinessAdaptorErrorType.Error);
                    bError = true;
                }
            }
            else
            {
                if (objEvent.DateOfEvent.CompareTo(sToday) == 0 &&  objEvent.TimeOfEvent.CompareTo(sTime) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                    String.Format(Globalization.GetString("Field.TimeOfEvent", base.ClientId, sLangCode), System.DateTime.Now.ToLongTimeString()),
                    BusinessAdaptorErrorType.Error);
                    bError = true;
                }
            }
            //Validation for Time of Event :END

            //Validation in case of TimeZone
            if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
            {

                if (objEvent.DateOfEvent.CompareTo(sOrgLevelToday) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                    String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId, sLangCode), Globalization.GetString("Field.EventDate", base.ClientId, sLangCode), dtOrgLevelCurrentDateTime.ToShortDateString()),
                    BusinessAdaptorErrorType.Error);
                    bError = true;
                }
            }
            //Validation without TimeZone
            else
            {
                if (objEvent.DateOfEvent.CompareTo(sToday) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                    String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId, sLangCode), Globalization.GetString("Field.EventDate", base.ClientId, sLangCode), System.DateTime.Now.ToShortDateString()),
                    BusinessAdaptorErrorType.Error);

                    bError = true;
                }
            }
            //Abhishek ::MITS 11186 Added Check for injury from and to date
            if (objEvent.InjuryFromDate != "" && objEvent.InjuryToDate != "")
            {
                if (objEvent.InjuryFromDate.CompareTo(objEvent.InjuryToDate) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                  String.Format(Globalization.GetString("Validation.MustBeLessThanInjuryToDate", base.ClientId, sLangCode), Globalization.GetString("Field.InjuryFromDate", base.ClientId, sLangCode), Conversion.ToDate(objEvent.InjuryToDate).ToShortDateString()),
                  BusinessAdaptorErrorType.Error);
                  bError = true;
                }
            }

            //Validation in case of TimeZone
            if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
            {

                if (objEvent.DateReported.CompareTo(sOrgLevelToday) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId, sLangCode), Globalization.GetString("Field.DateReported", base.ClientId, sLangCode), dtOrgLevelCurrentDateTime.ToShortDateString()),
                        BusinessAdaptorErrorType.Error);

                    bError = true;
                }
            }
            //Validation without TimeZone
            else
            {

                if (objEvent.DateReported.CompareTo(sToday) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId, sLangCode), Globalization.GetString("Field.DateReported", base.ClientId, sLangCode), System.DateTime.Now.ToShortDateString()),
                        BusinessAdaptorErrorType.Error);

                    bError = true;
                }
            }

            //Validation in case of TimeZone
            if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
            {
                if (objEvent.DateReported.CompareTo(sOrgLevelToday) == 0 && objEvent.TimeReported.CompareTo(sOrgLevelCurrentTime) > 0)//rsushilaggar MITS 24073 Date 03/11/2011
                {
                    // npadhy Start MITS 22714 The Format of Message was coming wrong
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                      String.Format(Globalization.GetString("Validation.MustBeLessThan", base.ClientId, sLangCode), Globalization.GetString("Field.EventTimeReported", base.ClientId, sLangCode), dtOrgLevelCurrentDateTime.ToLongTimeString()),
                      BusinessAdaptorErrorType.Error);
                    // npadhy End MITS 22714 The Format of Message was coming wrong
                    bError = true;
                }
            }
            //Validation without TimeZone
            else
            {
                if (objEvent.DateReported.CompareTo(sToday) == 0 && objEvent.TimeReported.CompareTo(sTime) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeLessThan", base.ClientId, sLangCode), Globalization.GetString("Field.EventTimeReported", base.ClientId, sLangCode), System.DateTime.Now.ToLongTimeString()),
                        BusinessAdaptorErrorType.Error);

                    bError = true;
                }
            }
			if (objEvent.DateReported.CompareTo(objEvent.DateOfEvent) < 0)
			{
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                    String.Format(Globalization.GetString("Validation.MustBeGreaterThanEventDate", base.ClientId, sLangCode), Globalization.GetString("Field.DateReported", base.ClientId, sLangCode), Conversion.ToDate(objEvent.DateOfEvent).ToShortDateString()),
					BusinessAdaptorErrorType.Error);

				bError = true;
			}

			if (objEvent.DateOfEvent.CompareTo(objEvent.DateReported) == 0 && objEvent.TimeReported.CompareTo(objEvent.TimeOfEvent) < 0)
			{
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                    String.Format(Globalization.GetString("Validation.MustBeGreaterThanEventTime", base.ClientId, sLangCode), Globalization.GetString("Field.EventTimeReported", base.ClientId, sLangCode), Conversion.ToDate(objEvent.DateOfEvent + objEvent.TimeOfEvent).ToLongTimeString()),
					BusinessAdaptorErrorType.Error);

				bError = true;
			}
			
			if (objEvent.DateCarrierNotif.CompareTo(objEvent.DateOfEvent) < 0 && objEvent.DateCarrierNotif != "")
			{
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                    String.Format(Globalization.GetString("Validation.MustBeGreaterThanEventDate", base.ClientId, sLangCode), Globalization.GetString("Field.DateCarrierNotif", base.ClientId, sLangCode), Conversion.ToDate(objEvent.DateOfEvent).ToShortDateString()),
					BusinessAdaptorErrorType.Error);

				bError = true;
			}

            //Validation in case of TimeZone
            if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
            {
                if (objEvent.DateCarrierNotif.CompareTo(sOrgLevelToday) > 0 && objEvent.DateCarrierNotif != "")
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                       String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId, sLangCode), Globalization.GetString("Field.DateCarrierNotif", base.ClientId, sLangCode), dtOrgLevelCurrentDateTime.ToLongTimeString()),
                       BusinessAdaptorErrorType.Error);

                    bError = true;
                }
            }
            else
            {
                if (objEvent.DateCarrierNotif.CompareTo(sToday) > 0 && objEvent.DateCarrierNotif != "")
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId, sLangCode), Globalization.GetString("Field.DateCarrierNotif", base.ClientId, sLangCode), System.DateTime.Now.ToShortDateString()),
                        BusinessAdaptorErrorType.Error);

                    bError = true;
                }
            }
			if (objEvent.DateCarrierNotif.CompareTo(objEvent.DateReported) < 0 && objEvent.DateCarrierNotif != "")
			{
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                    String.Format(Globalization.GetString("Validation.MustBeGreaterThanReportedDate", base.ClientId, sLangCode), Globalization.GetString("Field.DateCarrierNotif", base.ClientId, sLangCode), Conversion.ToDate(objEvent.DateReported).ToShortDateString()),
					BusinessAdaptorErrorType.Error);

				bError = true;
			}

			if (objEvent.DatePhysAdvised.CompareTo(objEvent.DateOfEvent) < 0 && objEvent.DatePhysAdvised != "")
			{
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                    String.Format(Globalization.GetString("Validation.MustBeGreaterThanEventDate", base.ClientId, sLangCode), Globalization.GetString("Field.DatePhysAdvised", base.ClientId, sLangCode), Conversion.ToDate(objEvent.DateOfEvent).ToShortDateString()),
					BusinessAdaptorErrorType.Error);

				bError = true;
			}
			
			if (objEvent.DateToFollowUp.CompareTo(objEvent.DateOfEvent) < 0 && objEvent.DateToFollowUp != "")
			{
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                    String.Format(Globalization.GetString("Validation.MustBeGreaterThanEventDate", base.ClientId, sLangCode), Globalization.GetString("Field.DateToFollowUp", base.ClientId, sLangCode), Conversion.ToDate(objEvent.DateOfEvent).ToShortDateString()),
					BusinessAdaptorErrorType.Error);

				bError = true;
			}
			
			if (objEvent.DateToFollowUp.CompareTo(objEvent.DateReported) < 0 && objEvent.DateToFollowUp != "")
			{
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                    String.Format(Globalization.GetString("Validation.MustBeGreaterThanReportedDate", base.ClientId, sLangCode), Globalization.GetString("Field.DateToFollowUp", base.ClientId, sLangCode), Conversion.ToDate(objEvent.DateReported).ToShortDateString()),
					BusinessAdaptorErrorType.Error);

				bError = true;
			}

            //Charanpreet for MITS 12174 : Start
            if (objEvent.DeptEid > 0)
            {
                bool bValerror = false;
                bValerror = OrgHierarchyValidate();
                if (bValerror)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        "Selected Department Not Within Effective Date Range",
                        BusinessAdaptorErrorType.Error);
                    bError = true;
                }
            }
            //Charanpreet for Mits 12174 : End
		
			// Return true if there were validation errors
			Cancel = bError;
		}


        #region Org Hierarchy Validate On Filter Date
        // Charanpreet for MITS 12174 : Start
        private bool OrgHierarchyValidate()
        {
            //StringBuilder sbSQL = new StringBuilder();
            string sSql = string.Empty;
            string sOrg_Trig = "";
            string sSys_dt = "";
            string sEff_start_dt = "";
            string sEff_end_dt = "";

            //R7 Prf Imp: Data can be fetched in one query
            sSql = "SELECT TRIGGER_DATE_FIELD,EFF_START_DATE,EFF_END_DATE FROM "
                +"ENTITY E, ORG_HIERARCHY OH WHERE (E.ENTITY_ID = OH.CLIENT_EID OR E.ENTITY_ID = OH.COMPANY_EID "
                +"OR E.ENTITY_ID = OH.REGION_EID OR E.ENTITY_ID = OH.OPERATION_EID "
                +"OR E.ENTITY_ID = OH.DIVISION_EID OR E.ENTITY_ID = OH.LOCATION_EID "
                +"OR E.ENTITY_ID = OH.FACILITY_EID OR E.ENTITY_ID = OH.DEPARTMENT_EID) "
                +"AND OH.DEPARTMENT_EID=" 
                + objEvent.DeptEid;
            using (DbReader objReader = DbFactory.GetDbReader(objEvent.Context.DbConn.ConnectionString, sSql))
            {
                while (objReader.Read())
                {
                    sOrg_Trig = objReader.GetValue("TRIGGER_DATE_FIELD").ToString();
                    //  if (sOrg_Trig != "" ) //MITS  32157
                    if (!string.IsNullOrEmpty(sOrg_Trig))
                    {
                        sEff_start_dt = objReader.GetValue("EFF_START_DATE").ToString();
                        sEff_end_dt = objReader.GetValue("EFF_END_DATE").ToString();
                        // if (!(sEff_start_dt == "" && sEff_end_dt == "")) //MITS  32157
                        if (!(string.IsNullOrEmpty( sEff_start_dt) && string.IsNullOrEmpty(sEff_end_dt )))
                        {
                            switch (sOrg_Trig)
                            {
                                case "EVENT.DATE_OF_EVENT":
                                    //if (sEff_start_dt != "") //MITS  32157
                                    //    if (objEvent.DateOfEvent.CompareTo(sEff_start_dt) < 0)
                                    //        return (true);
                                    //if (sEff_end_dt != "")
                                    //    if (objEvent.DateOfEvent.CompareTo(sEff_end_dt) > 0)
                                    //        return (true);
                                    if (!string.IsNullOrEmpty(sEff_start_dt) && objEvent.DateOfEvent.CompareTo(sEff_start_dt) < 0)                                       
                                            return (true);
                                    if (!string.IsNullOrEmpty(sEff_end_dt) && objEvent.DateOfEvent.CompareTo(sEff_end_dt) > 0)                                     
                                            return (true);
                                    break;
                                case "SYSTEM_DATE":
                                    sSys_dt = Conversion.GetDate(DateTime.Now.ToShortDateString());
                                    //MITS  32157
                                    //if (sEff_start_dt != "")
                                    //    if (sSys_dt.CompareTo(sEff_start_dt) < 0)
                                    //        return (true);
                                    //if (sEff_end_dt != "")
                                    //    if (sSys_dt.CompareTo(sEff_end_dt) > 0)
                                    //        return (true);
                                    if (!string.IsNullOrEmpty(sEff_start_dt) && sSys_dt.CompareTo(sEff_start_dt) < 0)                                       
                                            return (true);
                                    if (!string.IsNullOrEmpty(sEff_end_dt) && sSys_dt.CompareTo(sEff_end_dt) > 0 )                                        
                                            return (true);
                                    break;
                            }
                        }                            
                    }
                }
            }

            return (false);
        }
        // Charanpreet for MITS 12174 : End
        #endregion

        //Anu Tennyson : Added to set the PSO row ids
        private void SetPSORowIDs()
        {
            string sSQL = string.Empty;
            int iEventPsoRowId = 0;
            int iPatPsoRowId = 0;

            try
            {
                sSQL = "SELECT EVT_PSO_ROW_ID FROM EVENT_PSO WHERE EVENT_ID = " + objEvent.EventId;
                iEventPsoRowId = objEvent.Context.DbConnLookup.ExecuteInt(sSQL);
                base.ResetSysExData("EvtPsoRowId", Convert.ToString(iEventPsoRowId));


                sSQL = "SELECT PAT_PSO_ROW_ID FROM PATIENT_PSO WHERE EVENT_ID = " + objEvent.EventId;
                iPatPsoRowId = objEvent.Context.DbConnLookup.ExecuteInt(sSQL);
                base.ResetSysExData("PatPsoRowId", Convert.ToString(iPatPsoRowId));
            }
            catch (Exception e)
            {
                Errors.Add(e, Globalization.GetString("FormDataAdaptor.SetPSORowIDs.Exception", base.ClientId, sLangCode), Common.BusinessAdaptorErrorType.Error);
            }

        }
        //Ends
	}
}
