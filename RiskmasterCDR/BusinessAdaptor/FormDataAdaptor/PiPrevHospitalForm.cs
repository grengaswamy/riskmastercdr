using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

//TODO: Auto-Generated Form - migrate and verify behaviors from Legacy RMNet form.

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for PiPrevHospital Screen.
	/// </summary>
	public class PiPrevHospitalForm : DataEntryFormBase
	{
		const string CLASS_NAME = "PhysicianPrevHospital";
		private PhysicianPrevHospital PhysicianPrevHospital{get{return objData as PhysicianPrevHospital;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		const string FILTER_KEY_NAME = "PhysEid";
		public override void InitNew()
		{
			base.InitNew(); 

			this.m_ParentId = base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME,false);
			(objData as INavigation).Filter = 
				objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + this.m_ParentId.ToString();		
			this.PhysicianPrevHospital.PhysEid = this.m_ParentId;
		}

		public PiPrevHospitalForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

        // npadhy MITS 14200 Validation for Privilege Start Date and Privilege End Date as Per RMWorld
        public override void OnValidate(ref bool Cancel)
        {
            bool bError = false;

            if (!string.IsNullOrEmpty(PhysicianPrevHospital.IntDate) && !(string.IsNullOrEmpty(PhysicianPrevHospital.EndDate)))
            {
                if (PhysicianPrevHospital.IntDate.CompareTo(PhysicianPrevHospital.EndDate) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                            String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId), Globalization.GetString("Field.EndDate", base.ClientId), Conversion.ToDate(PhysicianPrevHospital.IntDate).ToShortDateString() + "(" + Globalization.GetString("Field.StartDate", base.ClientId) + ")"),
                            BusinessAdaptorErrorType.Error);

                    bError = true;
                }
            }

            // Return true if there were validation errors
            Cancel = bError;
        }

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
			base.OnUpdateForm ();
		}

	/*	public override void BeforeSave(ref bool Cancel)
		{
			XmlDocument objSysExDataXmlDoc = base.SysEx;
			XmlNode objPhysEidNode = null;
			try{objPhysEidNode = objSysExDataXmlDoc.SelectSingleNode("/SysExData/PhysEid");}
			catch{};
			
			if(objPhysEidNode !=null)
				if(this.PhysicianPrevHospital.PhysEid==0)
					this.PhysicianPrevHospital.PhysEid = Conversion.ConvertStrToInteger(objPhysEidNode.InnerText);

			objPhysEidNode = null;
			objSysExDataXmlDoc = null;

			base.BeforeSave (ref Cancel);		

		}*/
	}
}