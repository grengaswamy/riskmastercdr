﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using Riskmaster.Application.FundManagement;
using Riskmaster.Settings;
using System.Xml;
using Riskmaster.Application.Reserves;
using Riskmaster.Application.VSSInterface;

namespace Riskmaster.BusinessAdaptor
{
    /// <summary>
    /// Summary description for Reserve current supplementals
    /// </summary>
    public class ReserveCurrentForm : DataEntryFormBase
    {
        private ReserveCurrent objReserveCurrent { get { return objData as ReserveCurrent; } }
        const string CLASS_NAME = "ReserveCurrent";
        bool IsDeductibleReserveApplicable = false;
        int  iDisabilityCatCode = 0, iLossTypeCode = 0;
        bool bIsNewRecord = false;

        public ReserveCurrentForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

        public override void InitNew()
        {

            base.InitNew();
            objReserveCurrent.ClaimId = base.GetSysExDataNodeInt("ClaimId");

        }   
        public override void OnUpdateObject()
        {
            string p_ReservecurrencyType = string.Empty;
            double p_dAmount = 0;
            base.OnUpdateObject();
            objReserveCurrent.SaveHistory = true;
            int iClaimCurrCode = 0;
            int iLOB = 0;
            GetClaimData(objReserveCurrent.ClaimId, out iLOB, out iClaimCurrCode);
            //MITS:34082 MultiCurrency START
            p_dAmount = objReserveCurrent.ClaimCurrReserveAmt;//Conversion.ConvertStrToDouble(base.GetSysExDataNodeText("Amount"));
            int iReserveCurrCode = 0;
            if (objReserveCurrent.Context.InternalSettings.SysSettings.UseMultiCurrency != 0 && base.SysEx.GetElementsByTagName("ReserveCurrencyType") != null)
                iReserveCurrCode = Conversion.ConvertStrToInteger(((System.Xml.XmlElement)(base.SysEx.GetElementsByTagName("ReserveCurrencyType")[0])).Attributes["codeid"].Value);
            
            else
                iReserveCurrCode = iClaimCurrCode;
            objReserveCurrent.iReserveCurrCode = iReserveCurrCode;
            //MITS:34082 MultiCurrency END
            //if (iClaimCurrCode > 0)
            //{
            //    double dExhRateClaimToBase = CommonFunctions.ExchangeRateSrc2Dest(iClaimCurrCode, objReserveCurrent.Context.InternalSettings.SysSettings.BaseCurrencyType, base.Adaptor.connectionString, base.ClientId);
            //    //MITS:34082 MultiCurrency START
            //    double dExcRateReserveToClaim = CommonFunctions.ExchangeRateSrc2Dest(iReserveCurrCode, iClaimCurrCode, base.Adaptor.connectionString, base.ClientId);
            //    double dExcRateReserveToBase = CommonFunctions.ExchangeRateSrc2Dest(iReserveCurrCode, objReserveCurrent.Context.InternalSettings.SysSettings.BaseCurrencyType, base.Adaptor.connectionString, base.ClientId);
            //    double dExcRateBaseToReserve = CommonFunctions.ExchangeRateSrc2Dest(objReserveCurrent.Context.InternalSettings.SysSettings.BaseCurrencyType, iReserveCurrCode, base.Adaptor.connectionString, base.ClientId);
            //    objReserveCurrent.dReserveToClaimCurRate = dExcRateReserveToClaim;
            //    objReserveCurrent.dReserveToBaseCurRate = dExcRateReserveToBase;
            //    objReserveCurrent.dReserveCurReserveAmount = p_dAmount;
            //    objReserveCurrent.ReserveAmount = p_dAmount * dExcRateReserveToBase;
            //    objReserveCurrent.dBaseToReserveCurrRate = dExcRateBaseToReserve;
            //    //MITS:34082 MultiCurrency END
            //    objReserveCurrent.ClaimToBaseCurrRate = dExhRateClaimToBase;
            //}
            //else
            //{
               // objReserveCurrent.ReserveAmount = p_dAmount;
            //}

            if (iLOB == 243)
            {
                objReserveCurrent.iLossTypecode = base.GetSysExDataNodeInt("DisabilityLossType");
            }
            else
            {
                objReserveCurrent.iLossTypecode = base.GetSysExDataNodeInt("LossTypeCode");
            }
            objReserveCurrent.iDisabilityCode = base.GetSysExDataNodeInt("DisablityCat");
            objReserveCurrent.iCoverageId = objReserveCurrent.CoverageId;
            objReserveCurrent.DateEntered = Conversion.ToDbDate(DateTime.Now);
            objReserveCurrent.EnteredByUser = objReserveCurrent.Context.RMUser.LoginName;
            objReserveCurrent.iUserId = objReserveCurrent.Context.RMUser.UserId;
            objReserveCurrent.iGroupId = objReserveCurrent.Context.RMUser.GroupId;
            objReserveCurrent.IsFirstFinalReserve = 0;
            objReserveCurrent.sUpdateType = "Reserves";

        }

        public override void OnValidate(ref bool Cancel)
        {
            bool bValidate = false;
            Cancel = false;
            // Perform data validation
            bValidate = ValidateReserve(objReserveCurrent.ClaimId, objReserveCurrent.ReserveTypeCode, ref IsDeductibleReserveApplicable);
            //Errors.Add( = p_objErrOut;
            if (!bValidate)
            {
                Errors.Add(new RMAppException(Globalization.GetString("ReserveCurrentForm.AddDeductibleRecReserve.Error", base.ClientId)), BusinessAdaptorErrorType.Error);
                Cancel = true;
                return;
            }
            int iRcRowId = CommonFunctions.GetRCRowId(base.Adaptor.connectionString, objReserveCurrent.ClaimId, objReserveCurrent.ClaimantEid, 0, objReserveCurrent.ReserveTypeCode, objReserveCurrent.CoverageId, objReserveCurrent.iLossTypecode, true, base.ClientId);
            if (iRcRowId > 0 && objReserveCurrent.IsNew)
            {
                Errors.Add(new RMAppException(Globalization.GetString("ReserveCurrentForm.OnValidate.DuplicateReserveError", base.ClientId)), BusinessAdaptorErrorType.Error);
                Cancel = true;
            }

        }

        private  string GetMessageTemplateFinancialKey(int iClaimID)
        {
            StringBuilder sXml = new StringBuilder("<BOB>");
            sXml = sXml.Append("<ClaimID>");
            sXml = sXml.Append(iClaimID.ToString());
            sXml = sXml.Append("</ClaimID>");
            sXml = sXml.Append("<ReserveType>");
            sXml = sXml.Append(objReserveCurrent.ReserveCategory.ToString());
            sXml = sXml.Append("</ReserveType>");
            sXml = sXml.Append("</BOB>");

            return sXml.ToString();

        }

        public override void OnUpdateForm()
        {
            int iLOB, iClaimId = 0, iCurrencyCode = 0;
            string sCWSRequest = String.Empty, response = string.Empty, sResCurrCode = string.Empty, sReserveCurrencyType = string.Empty, sShortCode = string.Empty, sParentShortCode = string.Empty;
            bool bFlag = false;
            XmlNodeList datanode = null;
            XmlDocument objXmlOut = new XmlDocument();
            DataModelFactory oDmf = null ;
            ReserveFunds objReserveFunds = new ReserveFunds(base.Adaptor.connectionString, this.Adaptor.userLogin, base.Adaptor.ClientId);
            base.OnUpdateForm();
            base.ResetSysExData("PolicyId", base.GetSysExDataNodeText("PolicyId"));
            base.ResetSysExData("UnitName", base.GetSysExDataNodeText("UnitName"));
            base.ResetSysExData("ClaimCurrencyCode", base.GetSysExDataNodeText("ClaimCurrencyCode"));
            if (objReserveCurrent.ClaimId == 0)
            {
                iClaimId = base.GetSysExDataNodeInt("ClaimId");
            }
            else
            {
                iClaimId = objReserveCurrent.ClaimId;
            }
           
            sCWSRequest = GetMessageTemplateFinancialKey(iClaimId);
            objReserveFunds.LanguageCode = base.Adaptor.userLogin.objUser.NlsCode;

            objXmlOut = objReserveFunds.GetLookupXml(iClaimId, objReserveCurrent.ReserveCategory);
            
            sParentShortCode = objReserveCurrent.Context.LocalCache.GetRelatedShortCode(objReserveCurrent.ResStatusCode);
            base.ResetSysExData("IsHold", (sParentShortCode == "H") ? "-1" : "0"); //On Hold reserve

            base.ResetSysExData("DDLData", objXmlOut.InnerText.Replace("&gt;", ">").Replace("&lt;", "<"));

            objXmlOut = objReserveFunds.GetReserveStatusReasons(sParentShortCode, objReserveCurrent.ResStatusCode);

            int iCancelledParent = objReserveCurrent.Context.LocalCache.GetCodeId("CN", "RESERVE_STATUS_PARENT");
            int iClosedParent = objReserveCurrent.Context.LocalCache.GetCodeId("C", "RESERVE_STATUS_PARENT");
            int iReserveCodeTable = objReserveCurrent.Context.LocalCache.GetTableId("RESERVE_STATUS");

            object sCancelledCode = DbFactory.ExecuteScalar(objReserveCurrent.Context.DbConn.ConnectionString, "SELECT SHORT_CODE FROM CODES WHERE DELETED_FLAG =0 AND RELATED_CODE_ID= " + iCancelledParent + " AND TABLE_ID = " + iReserveCodeTable);
            base.ResetSysExData("CancelledShortCode", Conversion.ConvertObjToStr(sCancelledCode));
            object sClosedCode = DbFactory.ExecuteScalar(objReserveCurrent.Context.DbConn.ConnectionString, "SELECT SHORT_CODE FROM CODES WHERE DELETED_FLAG =0 AND RELATED_CODE_ID= " + iClosedParent + " AND TABLE_ID = " + iReserveCodeTable);
            base.ResetSysExData("ClosedShortCode", Conversion.ConvertObjToStr(sClosedCode));
            base.ResetSysExData("PaidAmount", objReserveCurrent.PaidTotal.ToString());
            base.ResetSysExData("DelValue", "0");


            base.ResetSysExData("ReserveStatusReason", objXmlOut.InnerXml);
            GetClaimData(iClaimId, out iLOB, out iCurrencyCode);

            switch (iLOB)
            {
                case 241:
                    m_SecurityId = RMO_GC_RESERVE;
                    base.AddKillNode("cmbdisablitycat");
                    base.AddKillNode("disabilitytype");
                    break;
                case 243:
                    m_SecurityId = RMO_WC_RESERVE;
                    base.AddKillNode("cmbcoveragelosstype");
                    break;
            }

            if (base.objData.Context.InternalSettings.SysSettings.UseMultiCurrency == 0)
            {
                base.AddKillNode("reservecurrencytypetext");
            }

            GetLossTypeCode(objReserveCurrent.CoverageLossId, out iDisabilityCatCode, out iLossTypeCode);

            if (!objReserveCurrent.IsNew)
            {
                if (base.SysEx.GetElementsByTagName("ReserveCurrencyType").Count != 0)
                {
                    sReserveCurrencyType = base.SysEx.GetElementsByTagName("ReserveCurrencyType")[0].Value;
                    sResCurrCode = base.SysEx.GetElementsByTagName("ReserveCurrencyType")[0].Attributes["codeid"].Value;
                }
                else 
                {
                    sResCurrCode = base.GetSysExDataNodeText("ClaimCurrencyCode");
                }

                base.AddReadOnlyNode("ctReserveSubType");
                base.ResetSysExData("LossTypeCode", iLossTypeCode.ToString());
                base.ResetSysExData("DisablityCat", iDisabilityCatCode.ToString());
                base.ResetSysExData("DisabilityLossType", iLossTypeCode.ToString());
                base.ResetSysExData("DisabilityLossType1", objReserveCurrent.Context.LocalCache.GetShortCode(iLossTypeCode));
                //base.ResetSysExData("Amount", objReserveCurrent.ClaimCurrReserveAmt.ToString());
                //base.ResetSysExData("ReserveBalance", objReserveCurrent.ClaimCurrBalanceAmt.ToString());
                base.ResetSysExData("ClaimId", objReserveCurrent.ClaimId.ToString());
                base.ResetSysExData("ClaimantEID", objReserveCurrent.ClaimantEid.ToString());
                base.ResetSysExData("LOB", iLOB.ToString());
                base.ResetSysExData("ReserveCurrencyType", CommonFunctions.sGetCurrencyShortCode(Conversion.ConvertStrToInteger(sResCurrCode), objReserveCurrent.Context.DbConn.ConnectionString));
                //base.ResetSysExData("ClaimCurrencyCode", iCurrencyCode.ToString());
                datanode = base.SysEx.GetElementsByTagName("ReserveCurrencyType");
                if (datanode != null && datanode.Count > 0)
                {
                    datanode[0].Attributes.Append(base.SysEx.CreateAttribute("codeid"));
                    datanode[0].Attributes["codeid"].Value = sResCurrCode;
                }
                datanode = base.SysEx.GetElementsByTagName("DisabilityLossType1");
                if (datanode != null && datanode.Count > 0)
                {
                    datanode[0].Attributes.Append(base.SysEx.CreateAttribute("codeid"));
                    datanode[0].Attributes["codeid"].Value = iLossTypeCode.ToString();
                }
                base.AddKillNode("addmorereserve");
            }
            else
            {
                
                if (base.SysEx.GetElementsByTagName("ReserveCurrencyType").Count != 0)
                {
                    sResCurrCode = base.SysEx.GetElementsByTagName("ReserveCurrencyType")[0].Attributes["codeid"].Value;
                }
                else
                {
                    sResCurrCode = base.GetSysExDataNodeText("ClaimCurrencyCode");
                }

                base.ResetSysExData("LossTypeCode", Conversion.ConvertObjToStr(objReserveCurrent.iLossTypecode));
                base.ResetSysExData("DisablityCat", Conversion.ConvertObjToStr(objReserveCurrent.iDisabilityCode));
                base.ResetSysExData("DisabilityLossType", Conversion.ConvertObjToStr(objReserveCurrent.iLossTypecode));
                base.ResetSysExData("DisabilityLossType1", objReserveCurrent.Context.LocalCache.GetShortCode(objReserveCurrent.iLossTypecode));
                //base.ResetSysExData("Amount", Conversion.ConvertObjToStr(objReserveCurrent.dReserveCurReserveAmount));
                base.ResetSysExData("ClaimId", Conversion.ConvertObjToStr(objReserveCurrent.ClaimId));
                base.ResetSysExData("LOB", iLOB.ToString());
                base.ResetSysExData("ClaimantEID", base.GetSysExDataNodeText("ClaimantEID"));
                base.ResetSysExData("LOBQueryString", string.Empty);
                //base.ResetSysExData("ReserveBalance", string.Empty);
                base.ResetSysExData("ReserveCurrencyType", CommonFunctions.sGetCurrencyShortCode(Conversion.ConvertStrToInteger(sResCurrCode), objReserveCurrent.Context.DbConn.ConnectionString));
                datanode = base.SysEx.GetElementsByTagName("ReserveCurrencyType");
                if (datanode != null)
                {
                    datanode[0].Attributes.Append(base.SysEx.CreateAttribute("codeid"));
                    datanode[0].Attributes["codeid"].Value = sResCurrCode;
                }
                datanode = base.SysEx.GetElementsByTagName("DisabilityLossType1");
                if (datanode != null)
                {
                    datanode[0].Attributes.Append(base.SysEx.CreateAttribute("codeid"));
                    datanode[0].Attributes["codeid"].Value = string.Empty;
                }
                //base.ResetSysExData("ClaimCurrencyCode", iCurrencyCode.ToString());
                base.AddKillNode("txtReserveBalance");
            }

            sShortCode = GetShortCode(objReserveCurrent.ResStatusCode);
            if (objReserveCurrent.IsNew || (!string.IsNullOrEmpty(sShortCode) && sShortCode != "H"))
            {
                base.AddKillNode("approvereserve");
                base.AddKillNode("rejectreserve");
                base.AddKillNode("txtAppRejReqCom");
            }
            if (!string.IsNullOrEmpty(sShortCode) && sShortCode == "H")
            {
                base.AddKillNode("Save");
                base.AddReadOnlyNode("txtReserveBalance");
                base.AddReadOnlyNode("txtAmount");
            }
            
            #region VSS Adjuster Export
            string sVssFlag = string.Empty;
            string sSQL = string.Empty;
            //sSQL = "SELECT VSS_CLAIM_IND FROM CLAIM WHERE CLAIM_ID = " + iClaimID;
            //use utility check intead of claim check.
            sSQL = "SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='ENABLE_VSS'";
            sVssFlag = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(objReserveCurrent.Context.DbConn.ConnectionString, sSQL));

            if (sVssFlag.Equals("-1"))
            {
                VssExportAsynCall objVss = new VssExportAsynCall(objReserveCurrent.Context.RMUser.objRiskmasterDatabase.DataSourceName, objReserveCurrent.Context.RMUser.LoginName, objReserveCurrent.Context.RMUser.Password, base.ClientId);
                objVss.AsynchVssReserveExport(objReserveCurrent.ClaimId, objReserveCurrent.ClaimantEid, objReserveCurrent.RcRowId, 0, 0, "", "", "", "", "Reserve");
            }
            #endregion

            base.ResetSysExData("NotDetDedTypeCode", objReserveCurrent.Context.LocalCache.GetCodeId("ND", "DEDUCTIBLE_TYPE").ToString());
                
        }

        #region Utility Functions

        private void GetClaimData(int iClaimId, out int iLob, out int iCurrencyCode)
        {

            using (DbReader objReader = DbFactory.GetDbReader(objReserveCurrent.Context.DbConn.ConnectionString, "SELECT LINE_OF_BUS_CODE, CLAIM_CURR_CODE  FROM CLAIM WHERE CLAIM_ID=" + iClaimId))
            {
                if (objReader.Read())
                {
                    iLob = Conversion.ConvertObjToInt(objReader.GetValue("LINE_OF_BUS_CODE"), objReserveCurrent.Context.ClientId);
                    iCurrencyCode = Conversion.ConvertObjToInt(objReader.GetValue("CLAIM_CURR_CODE"), objReserveCurrent.Context.ClientId);
                }
                else
                {
                    iLob = 0;
                    iCurrencyCode = 0;
                }
            }
        }
        private void GetLossTypeCode(int p_iCoverageLossId, out int iDisabilityCatCode, out int iLossTypeCode)
        {

            using (DbReader objReader = DbFactory.GetDbReader(objReserveCurrent.Context.DbConn.ConnectionString, "SELECT LOSS_CODE, DISABILITY_CAT FROM COVERAGE_X_LOSS WHERE CVG_LOSS_ROW_ID = " + p_iCoverageLossId))
            {
                if (objReader.Read())
                {
                    iLossTypeCode = Conversion.ConvertObjToInt(objReader.GetValue("LOSS_CODE"), objReserveCurrent.Context.ClientId);
                    iDisabilityCatCode = Conversion.ConvertObjToInt(objReader.GetValue("DISABILITY_CAT"), objReserveCurrent.Context.ClientId);
                }
                else
                {
                    iDisabilityCatCode = 0;
                    iLossTypeCode = 0;
                }
            }
        }
        private string GetShortCode(int iCodeId)
        {
            string sShortCode = string.Empty;

            using (DbReader objReader = DbFactory.GetDbReader(objReserveCurrent.Context.DbConn.ConnectionString, "SELECT SHORT_CODE FROM CODES WHERE CODE_ID = " + iCodeId))
            {
                if (objReader.Read())
                {
                    sShortCode = Conversion.ConvertObjToStr(objReader.GetValue("SHORT_CODE"));//, objReserveCurrent.Context.ClientId
                }
            }

            return sShortCode;
        }

        #endregion

        private bool ValidateReserve(int iClaimID, int iReserveTypeCode, ref bool IsDeductibleReserveApplicable)
        {
            DataModelFactory objDatamodel;

            objDatamodel = null;

            try
            {
                int iDedReserverTypeCode;
                int iTocheckDedSettings;

                int iSettingCarrierClaim;
                int iLineofBusCode;
                iDedReserverTypeCode = 0;
                iTocheckDedSettings = 0;

                iSettingCarrierClaim = 0;
                iLineofBusCode = 0;
                IsDeductibleReserveApplicable = false;
                objDatamodel = new DataModelFactory(base.Adaptor.userLogin, base.ClientId);

                iLineofBusCode = objDatamodel.Context.DbConnLookup.ExecuteInt("SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID = " + iClaimID);

                iDedReserverTypeCode = objDatamodel.Context.InternalSettings.ColLobSettings[iLineofBusCode].DedRecReserveType;
                iTocheckDedSettings = objDatamodel.Context.InternalSettings.ColLobSettings[iLineofBusCode].ApplyDedToPaymentsFlag;

                iSettingCarrierClaim = objDatamodel.Context.InternalSettings.SysSettings.MultiCovgPerClm;

                if (objDatamodel.Context.LocalCache.GetRelatedCodeId(iReserveTypeCode) != objDatamodel.Context.LocalCache.GetCodeId("R", "MASTER_RESERVE") && iTocheckDedSettings == -1 && iSettingCarrierClaim == -1)
                {
                    IsDeductibleReserveApplicable = true;
                }

                if (iReserveTypeCode == iDedReserverTypeCode && iTocheckDedSettings == -1 && iSettingCarrierClaim == -1)
                {
                    return false;
                }
                return true;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            finally
            {
                if (objDatamodel != null)
                {
                    objDatamodel.Dispose();
                    objDatamodel = null;
                }

            }
        }

        public override void BeforeSave(ref bool Cancel)
        {
            base.BeforeSave(ref Cancel);
            bIsNewRecord = objReserveCurrent.IsNew;

        }
        public override void AfterSave()
        {
            DataModelFactory oDmf = null ;
            string sReserveCurrencyType = string.Empty;
            ReserveFunds objReserveFunds = new ReserveFunds(base.Adaptor.connectionString, this.Adaptor.userLogin, base.Adaptor.ClientId);
            System.Collections.Hashtable objErrors = new System.Collections.Hashtable();

            base.AfterSave();
            GetLossTypeCode(objReserveCurrent.CoverageLossId, out iDisabilityCatCode, out iLossTypeCode);
            sReserveCurrencyType = CommonFunctions.sGetCurrencyShortCode(objReserveCurrent.iReserveCurrCode, base.Adaptor.connectionString);
            //Adding/Editing a corresponding deductible reserve when a reserve is added/edited
            if (IsDeductibleReserveApplicable)
            {
                if (bIsNewRecord)
                {
                    oDmf = new DataModelFactory(objReserveCurrent.Context.RMUser, base.ClientId);
                    if (oDmf.Context.LocalCache.GetRelatedCodeId(objReserveCurrent.ResStatusCode) == oDmf.Context.LocalCache.GetCodeId("O", "RESERVE_STATUS_PARENT"))
                    {

                        objReserveFunds.AddRecoveryReserve(objReserveCurrent.ClaimId, objReserveCurrent.ClaimantEid, base.GetSysExDataNodeInt("PolicyId"), objReserveCurrent.CoverageId, objReserveCurrent.ClaimCurrReserveAmt, objReserveCurrent.iLossTypecode, iDisabilityCatCode, objReserveCurrent.ReserveTypeCode, objReserveCurrent.AssignAdjusterEid, sReserveCurrencyType, objReserveCurrent.ResStatusCode);
                    }
                }
                else if (objReserveCurrent.RcRowId > 0)
                {
                    oDmf = new DataModelFactory(objReserveCurrent.Context.RMUser, base.ClientId);
                    if (oDmf.Context.LocalCache.GetRelatedCodeId(objReserveCurrent.ResStatusCode) == oDmf.Context.LocalCache.GetCodeId("O", "RESERVE_STATUS_PARENT"))
                    {
                        //Start:added by Nitin goel
                        objReserveFunds.EditRecoveryReserve(ref objErrors, objReserveCurrent.ClaimId, objReserveCurrent.ClaimantEid, base.GetSysExDataNodeInt("PolicyId"), objReserveCurrent.CoverageId, objReserveCurrent.ReserveTypeCode, objReserveCurrent.AssignAdjusterEid, objReserveCurrent.ClaimCurrReserveAmt, objReserveCurrent.ResStatusCode, objReserveCurrent.Reason, objReserveCurrent.CoverageLossId, sReserveCurrencyType);
                        // end: added by Nitin goel
                        foreach (System.Collections.DictionaryEntry objWarnings in objErrors)
                        {
                            Errors.Add((Errors.Count + 1).ToString(), objWarnings.Value.ToString(), Common.BusinessAdaptorErrorType.Warning);
                        }
                    }
                }
            }

        }
    }
}
