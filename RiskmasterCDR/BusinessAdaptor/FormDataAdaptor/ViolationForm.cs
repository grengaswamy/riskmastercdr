using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for Violation Screen.
	/// </summary>
	public class ViolationForm : DataEntryFormBase
	{
		const string CLASS_NAME = "EmpXViolation";
		const string FILTER_KEY_NAME = "EmployeeEid";

		private EmpXViolation EmpXViolation{get{return objData as EmpXViolation;}}

		private LocalCache objCache{get{return objData.Context.LocalCache;}}
	    
		/// <summary>
		/// Class constructor which initializes Class Name information
		/// </summary>
		/// <param name="fda"></param>
		public ViolationForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}
		
		/// <summary>
		/// Initializes the class and sets the parent ID for the class
		/// that can be used for parent-child navigation
		/// </summary>
		public override void InitNew()
		{
			base.InitNew();

			//set filter for EmployeeEid
			EmpXViolation.EmployeeEid=base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME,false);
			if (EmpXViolation.EmployeeEid>0)
				(objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + EmpXViolation.EmployeeEid;
			
		}//end method InitNew()

//		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
//		public override void OnUpdateForm()
//		{
//			base.OnUpdateForm ();
//			if(EmpXViolation.EmployeeEid==0)
//			{
//				AfterAddNew();
//			}
//
//		}
//		public override void AfterAddNew()
//		{
//			base.AfterAddNew ();
//
//			XmlDocument objXML = base.SysEx;
//			XmlNode objEmployeeEid=null;
//			try{objEmployeeEid = objXML.GetElementsByTagName(FILTER_KEY_NAME)[0];}
//			catch{};
//			
//			//Filter by this staffeid if Present.
//			if(objEmployeeEid !=null)
//			{
//				EmpXViolation.EmployeeEid=Conversion.ConvertStrToInteger(objEmployeeEid.InnerText);
//			}
//		}
//		public override void BeforeSave(ref bool Cancel)
//		{
//			XmlDocument objSysExDataXmlDoc = base.SysEx;
//			XmlNode objEmployeeEidNode = null;
//			try{objEmployeeEidNode = objSysExDataXmlDoc.SelectSingleNode("/SysExData/EmployeeEid");}
//			catch{};
//			
//			if(objEmployeeEidNode !=null)
//				if(this.EmpXViolation.EmployeeEid==0)
//					this.EmpXViolation.EmployeeEid = Conversion.ConvertStrToInteger(objEmployeeEidNode.InnerText);
//
//			objEmployeeEidNode = null;
//			objSysExDataXmlDoc = null;
//
//			base.BeforeSave (ref Cancel);		
//
//		}
	}
}