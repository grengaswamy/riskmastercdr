﻿
using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
    /// <summary>
    /// Summary description for Policy Insurer ReInsurer Screen.
    /// </summary>
    public class PolicyReInsurerForm : DataEntryFormBase
    {
        const string CLASS_NAME = "PolicyXReInsurer";
        private PolicyXReInsurer objPolicyXReInsurer { get { return objData as PolicyXReInsurer; } }
        private LocalCache objCache { get { return objData.Context.LocalCache; } }
        const string FILTER_KEY_NAME = "InRowID";

        /// <summary>
        /// Class constructor to initialize Class Name information
        /// </summary>
        /// <param name="fda"></param>
        public PolicyReInsurerForm(FormDataAdaptor fda)
            : base(fda)
        {
            base.m_ClassName = CLASS_NAME;
        }

        /// <summary>
        /// Initializes the class and sets the parent ID for the class
        /// that can be used for parent-child navigation
        /// </summary>
        public override void InitNew()
        {
            base.InitNew();

            //If we were sent here from a parent(event) then we'll have an eventid in the SysEx
            //originally from ScreenFlow\SysExData.
            XmlNode objInsurerRowID = null;

            try
            {
                objInsurerRowID = base.SysEx.GetElementsByTagName(FILTER_KEY_NAME)[0];
            }//end try
            catch
            {
            }//end catch
            if (objInsurerRowID != null)
                this.m_ParentId = Conversion.ConvertObjToInt(objInsurerRowID.InnerText, base.ClientId);

            objPolicyXReInsurer.InRowID = this.m_ParentId;

            //Filter by this PhysEid if Present.
            if (this.m_ParentId != 0)
            {
                (objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + this.m_ParentId;
            }

        }//end method InitNew()

        

        //Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
        public override void OnUpdateForm()
        {            
            string sName = "";
            //Call the base implementation of OnUpdateForm from the FormBase class
            base.OnUpdateForm();
            sName = this.objPolicyXReInsurer.ReInsurerEntity.LastName.ToString();
            //zmohammad MITS 36450  : Only one reinsurer to be allowed in case of Multiple Reinsurer off
            if (!this.objPolicyXReInsurer.Context.InternalSettings.SysSettings.MultipleReInsurer)
            {
                base.AddKillNode("new");
            }
            if (sName != "")
            {//	this.SysView.SelectSingleNode("//form/@title").InnerText = this.SysView.SelectSingleNode("//form/@title").InnerText + "[ "+ sName+" ]";
                ArrayList singleRow = new ArrayList();
                base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
                base.AddElementToList(ref singleRow, "id", "formsubtitle");
                base.AddElementToList(ref singleRow, "Text", "[ " + sName + " ]");
                base.m_ModifiedControls.Add(singleRow);
            }        
        }


      
    }
}
