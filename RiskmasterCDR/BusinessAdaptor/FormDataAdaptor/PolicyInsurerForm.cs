﻿
using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for PolicyInsurer Screen.
	/// </summary>
	public class PolicyInsurerForm : DataEntryFormBase
	{
		const string CLASS_NAME = "PolicyXInsurer";
		private PolicyXInsurer objPolicyXInsurer{get{return objData as PolicyXInsurer;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		const string FILTER_KEY_NAME = "PolicyId";

		/// <summary>
		/// Class constructor to initialize Class Name information
		/// </summary>
		/// <param name="fda"></param>
		public PolicyInsurerForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

		/// <summary>
		/// Initializes the class and sets the parent ID for the class
		/// that can be used for parent-child navigation
		/// </summary>
		public override void InitNew()
		{
			base.InitNew();

			//If we were sent here from a parent(event) then we'll have an eventid in the SysEx
			//originally from ScreenFlow\SysExData.
			XmlNode objPolicyId=null;

			try
			{
				objPolicyId = base.SysEx.GetElementsByTagName(FILTER_KEY_NAME)[0];
			}//end try
			catch
			{
			}//end catch
			if(objPolicyId !=null)
				this.m_ParentId = Conversion.ConvertObjToInt(objPolicyId.InnerText, base.ClientId);

			objPolicyXInsurer.PolicyId = this.m_ParentId;

			//Filter by this PhysEid if Present.
			if(this.m_ParentId != 0)
			{
				(objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + this.m_ParentId;
			}
			
		}//end method InitNew()

		public override void OnValidate(ref bool Cancel)
		{
			bool bError = false;
			string sSQL = string.Empty;
			double iResPercent = objPolicyXInsurer.ResPercentage;

			sSQL = "SELECT SUM(RES_PERCENTAGE) FROM POLICY_X_INSURER WHERE POLICY_ID = " + objPolicyXInsurer.PolicyId + " AND IN_ROW_ID <> " + objPolicyXInsurer.InRowID;
			using(DbReader objReader = objPolicyXInsurer.Context.DbConnLookup.ExecuteReader(sSQL))
			{
				objReader.Read();
				iResPercent = iResPercent + objReader.GetDouble(0);
			}

			if (iResPercent > 100)
			{
				Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                    Globalization.GetString("Validation.PercentShouldBeLessThan100", base.ClientId),
					BusinessAdaptorErrorType.Error);

				bError = true;
			}

			Cancel = bError;
		}

        public override void BeforeDelete(ref bool Cancel)
        {
            base.BeforeDelete(ref Cancel);
            bool bError = false;

            // A Primary Insurer can not be deleted
            if (objPolicyXInsurer.PrimaryInsurer)
            {
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                    Globalization.GetString("BeforeDelete.CantDelPrimInsurer", base.ClientId),
                    BusinessAdaptorErrorType.Error); 
                bError = true;
            }
            Cancel = bError;
        }

        //navdeep added
        
        //Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
        public override void OnUpdateForm()
        {
            //Changes made here to add Policy name to form -Ratheen(#2113)
            string sName = "";
            //Call the base implementation of OnUpdateForm from the FormBase class
            base.OnUpdateForm();
            sName = this.objPolicyXInsurer.InsurerEntity.LastName.ToString();           
            if (sName != "")
            {//	this.SysView.SelectSingleNode("//form/@title").InnerText = this.SysView.SelectSingleNode("//form/@title").InnerText + "[ "+ sName+" ]";
                ArrayList singleRow = new ArrayList();
                base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
                base.AddElementToList(ref singleRow, "id", "formsubtitle");
                base.AddElementToList(ref singleRow, "Text", "[ " + sName + " ]");
                base.m_ModifiedControls.Add(singleRow);
            }

            // If the utility setting for 'Multiple Re Insurer' is unchecked, remove the 'Re Insurer' button
            //zmohammad MITs 36450 : Should not be disabled, atleast one should be there.
            //if (!objData.Context.InternalSettings.SysSettings.MultipleReInsurer )
            //    base.AddKillNode("btnReinsurer");
            //akaur9 MITS 25163- Policy Interface Implementation --start
            if (objPolicyXInsurer.PolicyId > 0)
            {
                Policy objPolicy = (Policy)objData.Context.Factory.GetDataModelObject("Policy", false);
                objPolicy.MoveTo(objPolicyXInsurer.PolicyId);
                if (objPolicy.PolicySystemId > 0)
                {
                    base.AddReadOnlyNode("readOnlyPage");
                    base.AddKillNode("new");
                    base.AddKillNode("save");
                    base.AddKillNode("delete");
                }
            }
            //akaur9 MITS 25163- Policy Interface Implementation --End
        }
        //navdeep added

        public override void BeforeSave(ref bool Cancel)
        {
            base.BeforeSave(ref Cancel);
            string sSQL = string.Empty;
            bool bPrimIns = false;

            // A Primary Insurer can not be unmarked
            if (!objPolicyXInsurer.IsNew)
            {
                sSQL = "SELECT PRIMARY_INSURER FROM POLICY_X_INSURER WHERE IN_ROW_ID = " + objPolicyXInsurer.InRowID;
                using (DbReader objReader = objPolicyXInsurer.Context.DbConnLookup.ExecuteReader(sSQL))
                {
                    objReader.Read();
                    bPrimIns = objReader.GetBoolean("PRIMARY_INSURER");
                }
                if (bPrimIns)
                    objPolicyXInsurer.PrimaryInsurer = true;
            }
        }
	}
}
