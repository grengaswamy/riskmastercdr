﻿/**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            *
 **********************************************************************************************
 * 02/11/2014 | 34276  | anavinkumars   | Enhancement to Entity related functionality in RMA 
 * 02/10/2014 | 34276  | abhadouria   | changes req for Entity Address Type
 * 02/27/2015 | RMA-6865        | nshah28   | Swiss Re - Ability to set Effective Date_or_Expiration Date on Address
 **********************************************************************************************/
using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using System.Collections.Generic;
using System.Text;


//TODO: Auto-Generated Form - migrate and verify behaviors from Legacy RMNet form.

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for Employee Screen.
	/// </summary>
	public class EmployeeForm : DataEntryFormBase
	{
		const string CLASS_NAME = "Employee";
		private Employee Employee{get{return objData as Employee;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
        //Added Rakhi for R7:Add Emp Data Elements
        const string INSTANCE_SYSEXDATA_PATH = "/Instance/UI/FormVariables/SysExData"; 
        string sOfficePhone = string.Empty;
        string sHomePhone = string.Empty;
        int iHomeType = 0;
        int iOfficeType = 0;
        //Added Rakhi for R7:Add Emp Data Elements

		public EmployeeForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
			//this.objData = fda.Factory.GetDataModelObject(CLASS_NAME,false) as DataObject;
		}
        public override void InitNew()
        {
            base.InitNew();

            if (SysEx.DocumentElement.SelectSingleNode("IsPostBack") == null)
                CreateSysExData("IsPostBack");
            if (SysEx.DocumentElement.SelectSingleNode("IsEmpFlag") == null)
                CreateSysExData("IsEmpFlag");
            if (this.Employee.Context.InternalSettings.SysSettings.UseEntityRole)
            {
                //this.Employee.EmployeeEid = ;
                (objData as INavigation).Filter = objData.PropertyNameToField("EmployeeEid") + "=" + base.GetSysExDataNodeInt("/SysExData/employeeeid");
                // = true;
            }
        }

        private bool IsDuplicateSSN()
        {
            //MGaba2: MITS 9052
            XmlNode eltDupe = null;
            string sSQL = "";


            //User already saw dup popup and is choosing to proceed.           
            if (base.SysEx.SelectSingleNode("//DupSSN").InnerText.Trim() == "AfterIgnore")
                return false;
            DbConnection objDbConnection = null;

            if (Employee.EmployeeEntity.EntityId.ToString() != "0")
            {
                sSQL = "SELECT TAX_ID FROM ENTITY WHERE ENTITY_ID = " + Employee.EmployeeEntity.EntityId.ToString();
                objDbConnection = DbFactory.GetDbConnection(Employee.Context.DbConn.ConnectionString);
                if (objDbConnection != null)
                    objDbConnection.Open();
                string sTaxId = Conversion.ConvertObjToStr(objDbConnection.ExecuteScalar(sSQL));
                if (sTaxId == Employee.EmployeeEntity.TaxId)
                    return false;
                objDbConnection.Close();
            }

            //sSQL = "SELECT TAX_ID FROM ENTITY WHERE TAX_ID = " + Utilities.FormatSqlFieldValue(Employee.EmployeeEntity.TaxId.Trim()) + " AND ENTITY_ID <> " + Employee.EmployeeEntity.EntityId.ToString();
            sSQL = "SELECT TAX_ID FROM ENTITY WHERE TAX_ID = " + Utilities.FormatSqlFieldValue(Employee.EmployeeEntity.TaxId.Trim()) + " AND ENTITY_ID <> " + Employee.EmployeeEntity.EntityId.ToString() + " AND DELETED_FLAG = 0 ";
            objDbConnection = DbFactory.GetDbConnection(Employee.Context.DbConn.ConnectionString);
            if (objDbConnection != null)
                objDbConnection.Open();
            Object objDuplicate = objDbConnection.ExecuteScalar(sSQL);
            objDbConnection.Close();
            if (objDuplicate != null)
            {
                base.ResetSysExData("DupSSN", "");
                eltDupe = this.SysEx.SelectSingleNode("//DupSSN");
                eltDupe.InnerText = "BeforeIgnore";               
                return true;
            }

            return false;

        }



        //Shruti for 10779
        public override void BeforeSave(ref bool Cancel)
        {
            //Raman: MITS 15981
            //Employee Maintainance should be seperate from WC and should not check for any WC permission..This has been discussed with Paul and hence 10779 changes will be reverted

            //if (base.CurrentAction == enumFormActionType.Save && !(this.m_objData as Employee).IsNew && 
            //    !Employee.Context.RMUser.IsAllowedEx(RMO_WC_EMPLOYEE, RMO_UPDATE))
            //{
            //    Cancel = true;
            //    LogSecurityError(RMO_UPDATE);
            //    return;
            //}
            //else if (base.CurrentAction == enumFormActionType.Save && (this.m_objData as Employee).IsNew && 
            //    !Employee.Context.RMUser.IsAllowedEx(RMO_WC_EMPLOYEE, RMO_CREATE))
            //{
            //    Cancel = true;
            //    LogSecurityError(RMO_CREATE);
            //    return;
            //}
            //MITS 9052:Start
            if (IsDuplicateSSN())//MGaba2:MITS 9052
            {
                Cancel = true;
                return;
            }
            //MITS 9052:End
            //START rsushilaggar Entity Payment Approval(make the people entity approval status as pending approval) MITS 20606 05/05/2010
            if (Employee.Context.InternalSettings.SysSettings.UseEntityApproval)
            {
                if (Employee.EmployeeEntity.EntityApprovalStatusCode == 0)
                {
                    Employee.EmployeeEntity.EntityApprovalStatusCode = Employee.Context.InternalSettings.CacheFunctions.GetCodeIDWithShort("P", "ENTITY_APPRV_REJ");
                }
            }
            //End rsushilaggar
        }
        public override void AfterSave()
        {
            //MGaba2:MITS 9052
            base.ResetSysExData("DupSSN", "");           
        }

        //Shruti for 10779 ends

        private void ApplyFormTitle()
        {
            //string sCaption= Entity.Context.LocalCache.GetUserTableName(Entity.EntityTableId) + 

            string sSubTitleText = string.Empty;
            ArrayList singleRow = null;
            //avipinsrivas start : Worked for JIRA - 7767
            if (Employee.Context.InternalSettings.SysSettings.UseEntityRole)
            {
                int iEntityID = 0;

                if (base.SysEx.SelectSingleNode("//employeeeid") != null && !string.IsNullOrEmpty(base.SysEx.SelectSingleNode("//employeeeid").InnerText))
                {
                    bool bSuccess;
                    iEntityID = Conversion.CastToType<int>(base.SysEx.SelectSingleNode("//employeeeid").InnerText, out bSuccess);
                    if (iEntityID > 0)
                        sSubTitleText += string.Concat(" [ ", Employee.Context.LocalCache.GetEntityLastFirstName(iEntityID), " ]");
                }
            }
            //avipinsrivas end
            if (string.IsNullOrEmpty(sSubTitleText) && Employee != null && Employee.EmployeeEntity != null)
                sSubTitleText += " [ " + Employee.EmployeeEntity.Default + " ]";

            //Pass this title value to view (ref'ed from @valuepath).
            base.ResetSysExData("SubTitle", sSubTitleText);

            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singleRow, "id", "formsubtitle");
            base.AddElementToList(ref singleRow, "Text", sSubTitleText);
            base.m_ModifiedControls.Add(singleRow);
        }

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
			XmlDocument objXML = null;
			XmlElement objWeeksPerMonth = null;
            XmlDocument objXmlDoc = null;
			base.OnUpdateForm ();
            //avipinsrivas start : Worked for JIRA - 13515 (From Epic 7767 And Story 13196)
            if (Employee.Context.InternalSettings.SysSettings.UseEntityRole && Employee.EmployeeEid <= 0)
                base.ResetSysExData("existingrecord", "false");
            else
                base.ResetSysExData("existingrecord", "true");
            if (Employee.Context.InternalSettings.SysSettings.UseEntityRole)
            {
                if (base.SysEx.SelectSingleNode("//employeeeid") != null && !string.IsNullOrEmpty(base.SysEx.SelectSingleNode("//employeeeid").InnerText))
                {
                    bool bSuccess;
                    Employee.EmployeeEid = Conversion.CastToType<int>(base.SysEx.SelectSingleNode("//employeeeid").InnerText, out bSuccess);
                }
                else if (Employee.EmployeeEid > 0)
                    base.ResetSysExData("employeeeid", Employee.EmployeeEid.ToString());
            }
            //avipinsrivas end
            ApplyFormTitle();
			//Handle Locked down Toolbar buttons.
			if(!Employee.Context.RMUser.IsAllowedEx(FormBase.RMO_SEARCH,FormBase.RMO_EMPLOYEE_SEARCH))
				base.AddKillNode("search");
			if(!Employee.Context.RMUser.IsAllowedEx(FormBase.RMO_LOOKUP,FormBase.RMO_EMPLOYEE_SEARCH))
				base.AddKillNode("lookup");

			//ApplyFormTitle();


            //mbahl3 mits 30224
            string sPresentdate = String.Empty;
            if (Employee.EmployeeEntity.EntityId <= 0)
            {
                if (Employee.EmployeeEntity.EffectiveDate == String.Empty)
                {
                    sPresentdate = DateTime.Now.ToShortDateString();
                    Employee.EmployeeEntity.EffectiveDate = sPresentdate;
                }
            }

            //mbahl3 mits 30224

			CheckAutoGenerateEmpNo();

            // Shruti 16-Mar-2007 Mits 9052
            //MITS 9052:Start          
            if (base.SysEx.SelectSingleNode("//DupSSN") == null) //MGaba2:MITS 9052      
                base.ResetSysExData("DupSSN", "");
            //MITS 9052:End

			if (Employee.EmployeeEntity.BirthDate!=string.Empty)
				base.ResetSysExData("EmployeeAge",Utilities.CalculateAgeInYears(Employee.EmployeeEntity.BirthDate));
			else
				base.ResetSysExData("EmployeeAge","");

			// Defect no. 1468 week days should get checked automatically, according to the utilities settings
			if(this.CurrentAction == enumFormActionType.AddNew)
			{
				Employee.WorkFriFlag=Employee.Context.InternalSettings.SysSettings.WorkFri;
				Employee.WorkMonFlag=Employee.Context.InternalSettings.SysSettings.WorkMon;
				Employee.WorkSatFlag=Employee.Context.InternalSettings.SysSettings.WorkSat;
				Employee.WorkSunFlag=Employee.Context.InternalSettings.SysSettings.WorkSun;
				Employee.WorkThuFlag=Employee.Context.InternalSettings.SysSettings.WorkThu;
				Employee.WorkTueFlag=Employee.Context.InternalSettings.SysSettings.WorkTue;
				Employee.WorkWedFlag=Employee.Context.InternalSettings.SysSettings.WorkWed;
			}

			if(!Employee.Context.RMUser.IsAllowedEx(m_SecurityId,FormBase.RMO_ENABLE_SALARY))
			{
				base.AddKillNode("paytypecode");
				base.AddKillNode("payamount");
				base.AddKillNode("hourlyrate");
				base.AddKillNode("weeklyhours");
				base.AddKillNode("weeklyrate");
				base.AddKillNode("monthlyrate");
			}

            //MITS 20237 Add support for View SSN permission
            if (Employee.EmployeeEid > 0 && !Employee.Context.RMUser.IsAllowedEx(RMPermissions.RMO_EMPLOYEE, RMPermissions.RMO_EMPLOYEE_VIEW_SSN))
            {
                base.AddKillNode("taxid");
            }
            //Start rsushilaggar Entity Payment Approval (only execute this block if the Use Entity approval check box is "off") MITS 20606 05/05/2010
            if (!Employee.Context.InternalSettings.SysSettings.UseEntityApproval)
            {
                base.AddKillNode("entityapprovalstatuscode");
                base.AddKillNode("rejectreasontext");
            }
            //End rsushilaggar

			//Raman Bhatia - Acrosoft Phase 1 changes.. Document Management Not available for Employee if Acrosoft is enabled
			bool bUseAcrosoftInterface = Employee.Context.InternalSettings.SysSettings.UseAcrosoftInterface;
            bool bUsePaperVision = Employee.Context.InternalSettings.SysSettings.UsePaperVisionInterface;//Mona:PaperVisionMerge: Animesh Inserted MITS 16697
            //asingh263 mits 30117  starts
            RMConfigurationManager.GetAcrosoftSettings();
            bool bUseRMADocumentManagement = !object.ReferenceEquals(AcrosoftSection.UseRMADocumentManagementForEntity, null) && AcrosoftSection.UseRMADocumentManagementForEntity.ToLower().Equals("true"); 
            if ((bUseAcrosoftInterface && !bUseRMADocumentManagement) || bUsePaperVision || Employee.Context.InternalSettings.SysSettings.UseMediaViewInterface)
            //asingh263 mits 30117 ends
            {
                base.AddKillNode("attachdocuments");//Mona:PaperVisionMerge:Animesh Inserted MITS 16697
                base.AddKillNode("attach");

            }
           
			//Raman Bhatia - LTD Phase 3 changes : Weeks Per Month should be passed via a node in SYSEX
			string sSQL =	"SELECT NO_WEEKS_PER_MONTH FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE = 844";

			DbReader objReader = null;
            using (objReader = objData.Context.DbConnLookup.ExecuteReader(sSQL))
            {

                objXML = base.SysEx;
                objWeeksPerMonth = objXML.CreateElement("weekspermonth");
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        objWeeksPerMonth.InnerText = Conversion.ConvertObjToStr(objReader.GetValue(0));
                    }

                }
            }
			objXML.DocumentElement.AppendChild(objWeeksPerMonth);
            base.ResetSysExData("IsPostBack", "0");
            //if(objReader!=null)
            //    objReader.Close();

            //Added Rakhi for R7:Add Emp Data Elements
            XmlNode objPhoneTypeList = base.SysEx.SelectSingleNode("//PhoneTypeList");
            XmlNode objPhoneNumbers = base.SysEx.SelectSingleNode("//PhoneNumbers");

            objXmlDoc = new XmlDocument();
            objXmlDoc = CommonForm.PopulatePhoneTypes(Employee.Context.DbConn.ConnectionString, ref iOfficeType, ref iHomeType,base.Adaptor.userLogin.objUser.NlsCode); //Aman ML Change
           
            if (objPhoneTypeList != null)
                base.SysEx.DocumentElement.RemoveChild(objPhoneTypeList);
            base.SysEx.DocumentElement.AppendChild(base.SysEx.ImportNode(objXmlDoc.SelectSingleNode("PhoneTypeList"), true));

            if (objPhoneNumbers == null)
            {
                string sPhoneNumbers = PopulatePhoneNumbers();
                base.ResetSysExData("PhoneNumbers", sPhoneNumbers);
            }
            else
            {
                objPhoneTypeList = base.SysEx.SelectSingleNode("//PhoneTypeList");
                if (objPhoneTypeList != null)
                    CommonForm.SetPhoneNumbers(objPhoneNumbers, objPhoneTypeList);
                
            }
            if (base.SysEx.SelectSingleNode("//PhoneType1") == null)
            {
                base.ResetSysExData("PhoneType1", iOfficeType.ToString());
            }
            if (base.SysEx.SelectSingleNode("//PhoneNumber1") == null)
            {
                base.ResetSysExData("PhoneNumber1", sOfficePhone);
            }
            if (base.SysEx.SelectSingleNode("//PhoneType2") == null)
            {
                base.ResetSysExData("PhoneType2", iHomeType.ToString());
            }
            if (base.SysEx.SelectSingleNode("//PhoneNumber2") == null)
            {
                base.ResetSysExData("PhoneNumber2", sHomePhone);
            }
            //Added Rakhi for R7:Add Emp Data Elements
            //Added Rakhi for R7:Add Employee Data Elements
            bool bMulAddresses = this.objData.Context.InternalSettings.SysSettings.UseMultipleAddresses && !this.objData.Context.InternalSettings.SysSettings.UseEntityRole;        //avipinsrivas start : Worked for JIRA - 7767;
            if (bMulAddresses)
            {
                base.AddDisplayNode("addressesinfo");
                SetPrimaryAddressReadOnly();
                bool bIsFirstRecord = false;
                XmlDocument objEntityXAddressInfoXmlDoc = GetEntityXAddressInfo();
                XmlNode objOldEntityXAddressInfoNode = SysEx.DocumentElement.SelectSingleNode(objEntityXAddressInfoXmlDoc.DocumentElement.LocalName);
                if (objOldEntityXAddressInfoNode != null)
                {
                    SysEx.DocumentElement.RemoveChild(objOldEntityXAddressInfoNode);
                }
                XmlNode objEntityXAddressInfoNode =
                    SysEx.ImportNode(objEntityXAddressInfoXmlDoc.DocumentElement, true);
                SysEx.DocumentElement.AppendChild(objEntityXAddressInfoNode);

                if (SysEx.DocumentElement.SelectSingleNode("IsPostBack") == null)
                    CreateSysExData("IsPostBack");
                if (SysEx.DocumentElement.SelectSingleNode("EntityXOperatingAsGrid_RowDeletedFlag") != null)
                {
                    if (SysEx.DocumentElement.SelectSingleNode("EntityXOperatingAsGrid_RowDeletedFlag").InnerText == "true")
                    {
                        if (base.m_fda.SafeFormVariableParamText("SysCmd") == "7")
                        {
                            base.ResetSysExData("IsPostBack", "1");
                        }
                    }
                }
                if (this.Employee.EmployeeEntity.EntityXAddressesList.Count == 0)
                    bIsFirstRecord = true;

                if (base.SysEx.SelectSingleNode("//PrimaryFlag") == null)
                    base.ResetSysExData("PrimaryFlag", "false");

                if (base.SysEx.SelectSingleNode("//PrimaryRow") == null)
                    base.ResetSysExData("PrimaryRow", "");

                if (base.SysEx.SelectSingleNode("//PrimaryAddressChanged") == null)
                    base.ResetSysExData("PrimaryAddressChanged", "false");

                base.ResetSysExData("EntityXAddressesInfoSelectedId", "");
                base.ResetSysExData("EntityXAddressesInfoGrid_RowDeletedFlag", "false");
                base.ResetSysExData("EntityXAddressesInfoGrid_RowAddedFlag", "false");
                base.ResetSysExData("UseMultipleAddresses", bMulAddresses.ToString());
                base.ResetSysExData("IsFirstRecord", bIsFirstRecord.ToString());
            }
            else
            {
                base.AddKillNode("addressesinfo");
                base.AddKillNode("EntityXAddressesInfoGrid");//Mits 22246:GridTitle and Buttons getting visible in topdown Layout
            }

            //MITS:34276 Starts-- Entity ID Type tab display
            XmlDocument objEntityIDTypeAsXmlDoc = GetEntityIdTypeInfoData();

            XmlNode objOldEntityIdTypeAsNode = SysEx.DocumentElement.SelectSingleNode(objEntityIDTypeAsXmlDoc.DocumentElement.LocalName);
            if (objOldEntityIdTypeAsNode != null)
            {
                SysEx.DocumentElement.RemoveChild(objOldEntityIdTypeAsNode);
            }
            XmlNode objEntityIdTypeAsNode = SysEx.ImportNode(objEntityIDTypeAsXmlDoc.DocumentElement, true);
            SysEx.DocumentElement.AppendChild(objEntityIdTypeAsNode);

            base.ResetSysExData("EntityXEntityIDTypeSelectedId", "");
            base.ResetSysExData("EntityXEntityIDTypeGrid_RowDeletedFlag", "false");
            base.ResetSysExData("EntityXEntityIDTypeGrid_RowAddedFlag", "false");
            //MITS:34276 Ends

            //if (base.SysEx.SelectSingleNode("//PopupGridRowsDeleted") == null || base.m_fda.SafeFormVariableParamText("SysCmd") == "5")
            //    base.ResetSysExData("PopupGridRowsDeleted", "");
            XmlNode objRecordSummary = base.SysEx.SelectSingleNode("//IsRecordSummary");
            if (objRecordSummary != null && objRecordSummary.InnerText == "true")
            {
                CommonForm.DisplayPhoneNumbersRecordSummary(base.SysEx, base.SysView);
            }           

            //MITS:34276 Starts-- Entity ID Type View Permission
            if (!Employee.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_IDTYPE_VIEW))
            {
                base.AddKillNode("TABSentityidtypeinfo");
                base.AddKillNode("TBSPentityidtypeinfo");
            }

            //MITS:34276 ends
            //Added Rakhi for R7:Add Employee Data Elements 
            //Added by Manika for R8 enhancement of EFT and Withholding
            if (!Employee.Context.RMUser.IsAllowedEx(FormBase.RMO_EMPLOYEEMAINT_EFT))
            {
                base.AddKillNode("BankingInfo");
            }
            if (!Employee.Context.RMUser.IsAllowedEx(FormBase.RMO_EMPMAINT_WITHHOLDING))
            {
                base.AddKillNode("Withholding");
            }
            //End Manika
            ERSetting();            //avipinsrivas start : Worked for JIRA - 7767
		}

        //avipinsrivas start : Worked for JIRA - 7767
        private void ERSetting()
        {
            if (Employee.Context.InternalSettings.SysSettings.UseEntityRole)
            {
                //Toolbars
                base.AddKillNode("lookup");
                base.AddKillNode("attach");
                base.AddKillNode("diary");
                base.AddKillNode("comments");
                //base.AddKillNode("mailmerge");
                //base.AddKillNode("recordsummary");            //avipinsrivas Start : Worked for 13947 (Issue of 13196 - Epic 7767)
                base.AddKillNode("withholding");
                base.AddKillNode("BankingInfo");
                //End Toolbars

                //Controls
                //First Tab Start
                base.AddKillNode("abbreviation");
                base.AddKillNode("lastname");
                base.AddKillNode("alsoknownas");
                base.AddKillNode("firstname");
                base.AddKillNode("taxid");
                base.AddKillNode("middlename");
                base.AddKillNode("birthdate");
                base.AddKillNode("age");
                base.AddKillNode("entityage");
                base.AddKillNode("addr1");
                base.AddKillNode("addr2");
                base.AddKillNode("addr3");
                base.AddKillNode("faxnumber");
                base.AddKillNode("addr4");
                base.AddKillNode("zipcode");
                base.AddKillNode("city");
                base.AddKillNode("title");
                base.AddKillNode("stateid");
                base.AddKillNode("sexcode");
                base.AddKillNode("countrycode");
                base.AddKillNode("county");
                base.AddKillNode("rejectreasontext");
                base.AddKillNode("freezepayments");
                base.AddKillNode("entityapprovalstatuscode");
                base.AddKillNode("effectivedate");
                base.AddKillNode("expirationdate");
                //avipinsrivas Start : Worked for 13953 (Issue of 13196 - Epic 7767)
                base.AddKillNode("phone1");
                base.AddKillNode("phone2");
                //avipinsrivas end
                //First Tab End

                //Third Tab (Employee Info) Start
                base.AddKillNode("emailtypecode");
                base.AddKillNode("emailaddress");
                base.AddKillNode("costcentercode");
                //Third Tab (Employee Info) End
                //End Controls

                //Tabs
                base.AddKillNode("addressesinfo");
                base.AddKillNode("entityidtypeinfo");
                //End Tabs
            }
            else
                base.AddKillNode("BackToParent");
        }
        //avipinsrivas End
		internal override bool ValidateRequiredViewFields(XmlDocument propertyStore)
		{
			CheckAutoGenerateEmpNo();

			return base.ValidateRequiredViewFields (propertyStore);
		}

		private void CheckAutoGenerateEmpNo()
		{
			//Bug No: 636,649,672. For Autogenerating Employee Number
			//Nikhil Garg		Dated: 20/Dec/2005
			XmlElement objNotReqNew = null;
			string sNotReqNew=string.Empty;
			if (objData.Context.InternalSettings.SysSettings.AutoNumWCEmp)
			{
                objNotReqNew = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysNotReqNew");
                sNotReqNew = objNotReqNew.InnerText;
				if (sNotReqNew.IndexOf("employeenumber")==-1)
					sNotReqNew=sNotReqNew + "employeenumber|";
                objNotReqNew.InnerText = sNotReqNew;
			}
		}

        public override void OnValidate(ref bool Cancel)
        {
            base.OnValidate(ref Cancel);
            bool bError = false;

            // Perform data validation
            string sToday = Conversion.ToDbDate(System.DateTime.Now);
            string sBirthDate = Employee.EmployeeEntity.BirthDate;

            //******************************** Validate Data ************************************
            if (sBirthDate != string.Empty && sBirthDate.CompareTo(sToday) > 0)
            {
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                    String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.BirthDate", base.ClientId), System.DateTime.Now.ToShortDateString()),
                    BusinessAdaptorErrorType.Error);

                bError = true;
            }

            //avipinsrivas start : Worked for JIRA - 13508 (From Epic 7767 and Story 13)
            if (Employee.IsNew && Employee.Context.InternalSettings.SysSettings.UseEntityRole && !string.IsNullOrEmpty(Employee.EmployeeNumber.Trim()))
            {
                bool bEmpExist = false;
                StringBuilder sbQuery = null;
                Dictionary<string, string> objDictParams = null;
                object objCount = null;
                bool bSuccess;
                try
                {
                    sbQuery = new StringBuilder();
                    objDictParams = new Dictionary<string, string>();

                    sbQuery.Append("SELECT EMPLOYEE_EID FROM EMPLOYEE WHERE EMPLOYEE_NUMBER = {0}");
                    sbQuery.Replace("{0}", "~EMPLOYEENUMBER~");

                    objDictParams.Add("EMPLOYEENUMBER", Employee.EmployeeNumber);

                    objCount = DbFactory.ExecuteScalar(Employee.EmployeeEntity.Context.RMDatabase.ConnectionString, sbQuery.ToString(), objDictParams);
                    if (objCount != null && objCount != System.DBNull.Value && Conversion.CastToType<Int32>(objCount.ToString(), out bSuccess) > 0)
                        bEmpExist = true;
                }
                finally
                {
                    sbQuery = null;
                    objDictParams = null;
                    objCount = null;
                }
                if (bEmpExist)
                {
                    Errors.Add(Globalization.GetString("ExistingEmployeeError", base.ClientId),
                                           "This Employee Number is associated with some other entity; Please enter another one",
                                           BusinessAdaptorErrorType.Error);
                    bError = true;
                }
            }
            //avipinsrivas end

            if (Employee.DateHired != string.Empty)
            {
                if (Employee.DateHired.CompareTo(sToday) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.DateHired", base.ClientId), System.DateTime.Now.ToShortDateString()),
                        BusinessAdaptorErrorType.Error);
                    bError = true;
                }
                if (Employee.DateHired.CompareTo(sBirthDate) < 0)
                {
                    // Start MITS 31947 pkumar236 05/06/2013
                    string DateofBirth = Globalization.GetString("Field.BirthDate", base.ClientId);
                    string[] arrTmp = { "|^|" };
                    string[] sArr = DateofBirth.Split(arrTmp, StringSplitOptions.None);
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId), Globalization.GetString("Field.DateHired", base.ClientId), sArr[2]),
                        BusinessAdaptorErrorType.Error);
                    // End MITS 31947 pkumar236 05/06/2013
                    bError = true;
                }
            }
            if (Employee.TermDate != string.Empty)
            {
                if (Employee.TermDate.CompareTo(sToday) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.TermDate", base.ClientId), System.DateTime.Now.ToShortDateString()),
                        BusinessAdaptorErrorType.Error);
                    bError = true;
                }
                if (Employee.TermDate.CompareTo(Employee.DateHired) < 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId), Globalization.GetString("Field.TermDate", base.ClientId), Globalization.GetString("Field.DateHired", base.ClientId)),
                        BusinessAdaptorErrorType.Error);
                    bError = true;
                }
                if (Employee.TermDate.CompareTo(sBirthDate) < 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId), Globalization.GetString("Field.TermDate", base.ClientId), Globalization.GetString("Field.BirthDate", base.ClientId)),
                        BusinessAdaptorErrorType.Error);
                    bError = true;
                }
            }
            if (Employee.DateOfDeath != string.Empty)
            {
                if (Employee.DateOfDeath.CompareTo(sToday) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.DateOfDeath", base.ClientId), System.DateTime.Now.ToShortDateString()),
                        BusinessAdaptorErrorType.Error);
                    bError = true;
                }
                if (Employee.DateOfDeath.CompareTo(sBirthDate) < 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId), Globalization.GetString("Field.DateOfDeath", base.ClientId), Globalization.GetString("Field.BirthDate", base.ClientId)),
                        BusinessAdaptorErrorType.Error);
                    bError = true;
                }
                if (Employee.DateOfDeath.CompareTo(Employee.DateHired) < 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId), Globalization.GetString("Field.DateOfDeath", base.ClientId), Globalization.GetString("Field.DateHired", base.ClientId)),
                        BusinessAdaptorErrorType.Error);
                    bError = true;
                }
            }
            if (Employee.DateDriverslicexp != string.Empty && Employee.DateDriverslicexp.CompareTo(sBirthDate) < 0)
            {
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                    String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId), Globalization.GetString("Field.DateDriverslicexp", base.ClientId), Globalization.GetString("Field.BirthDate", base.ClientId)),
                    BusinessAdaptorErrorType.Error);
                bError = true;
            }
            if (Employee.LastVerifiedDate != string.Empty && Employee.LastVerifiedDate.CompareTo(sToday) > 0)
            {
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                    String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.DateDriverslicexp", base.ClientId)),
                    BusinessAdaptorErrorType.Error);
                bError = true;
            }

            if (Employee.LastVerifiedDate != string.Empty && Employee.LastVerifiedDate.CompareTo(sBirthDate) < 0)
            {
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                    String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId), Globalization.GetString("Field.DateDriverslicexp", base.ClientId), Globalization.GetString("Field.BirthDate", base.ClientId)),
                    BusinessAdaptorErrorType.Error);
                bError = true;
            }
            if (Employee.WorkPermitDate != string.Empty && Employee.WorkPermitDate.CompareTo(sBirthDate) < 0)
            {
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                    String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId), Globalization.GetString("Field.WorkPermitDate", base.ClientId), Globalization.GetString("Field.BirthDate", base.ClientId)),
                    BusinessAdaptorErrorType.Error);
                bError = true;
            }
            bool bMulAddresses = this.objData.Context.InternalSettings.SysSettings.UseMultipleAddresses && !this.objData.Context.InternalSettings.SysSettings.UseEntityRole;            //avipinsrivas start : Worked for JIRA - 7767
            if (bMulAddresses)
            {
                XmlNode PrimaryAddress = base.SysEx.DocumentElement.SelectSingleNode("PrimaryFlag");
                if (PrimaryAddress != null && Employee.EmployeeEntity.EntityXAddressesList.Count != 0 && PrimaryAddress.InnerText.ToLower() == "false")
                {
                    Errors.Add("PrimaryAddressError", "No Address Marked as Primary for this Entity", BusinessAdaptorErrorType.Error);
                    bError = true;
                }
            }


            //mbahl3 mits 30224
            if (!string.IsNullOrEmpty(Employee.EmployeeEntity.EffectiveDate) && !string.IsNullOrEmpty(Employee.EmployeeEntity.ExpirationDate))
            {
                if (Employee.EmployeeEntity.ExpirationDate.CompareTo(Employee.EmployeeEntity.EffectiveDate) < 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError,base.ClientId", base.ClientId),
                        Globalization.GetString("Validation.ExpirationDateMustBeGreaterThanEffectiveDate", base.ClientId),
                        BusinessAdaptorErrorType.Error);

                    bError = true;
                    //Cancel = true;
                    //return;
                }
            }

            //MITS 34276 Starts:Check for Unique Entity ID Number and Entity ID Type Combination
            List<EntityXEntityIDType> lstEntityXIDType = new List<EntityXEntityIDType>();
            bool bDupEntityIDType = false;
            foreach (EntityXEntityIDType objEntityIDType in Employee.EmployeeEntity.EntityXEntityIDTypeList)
            {
                if (!bDupEntityIDType && Riskmaster.Common.CommonFunctions.IsEntityIDNumberExists(Employee.EmployeeEntity.Context.RMDatabase.ConnectionString, objEntityIDType.IdNumRowId, objEntityIDType.Table, objEntityIDType.IdNum, objEntityIDType.IdType))
                {
                    Errors.Add("EntityIDNumberError", "Entity ID Number with Entity ID Type already exists",
                        BusinessAdaptorErrorType.Error);
                    bError = true;
                }
                if (!bDupEntityIDType && lstEntityXIDType.Exists(x => x.IdType == objEntityIDType.IdType && x.IdNum == objEntityIDType.IdNum))
                {
                    Errors.Add("EntityIDNumberError", "Entity ID Number with Entity ID Type already exists",
                         BusinessAdaptorErrorType.Error);
                    bError = true;
                    bDupEntityIDType = true;
                }
                else
                    lstEntityXIDType.Add(objEntityIDType);

            }
            //MITS 34276 Ends:Check for Unique Entity ID Number and Entity ID Type Combination
            Cancel = bError;
        }

            //mbahl3 mits 30224
		

		/// <summary>
		/// Sets comments data to the object. This is overridden because Employee object
		/// doesn't have Comments/HTMLComments fields. The EmployeeEntity object does.
		/// </summary>
		/// <param name="sComments"></param>
		/// <param name="sHTMLComments"></param>
		protected override void SetComments(string sComments, string sHTMLComments)
		{
			Employee.EmployeeEntity.Comments = sComments;
			Employee.EmployeeEntity.HTMLComments = sHTMLComments;
		}

        //Change for MITS 12151
        override public bool MoveTo(params int[] paramValue)
        {
            // BSB "Claim Record Filtering Fix"- don't move if target recordid is 0.
            // This was clearing the LOB on claim which was set =  part of a record filter.                      
            XmlDocument propertyStore = new XmlDocument();
            string sPropertyStore = "";
            string sSQL = "";
            string sEmpNumber = "";
            int iEmpId = 0;
            bool bCancel = false;
            string sIsPostBack = "";
            int flag = 0;

            sIsPostBack = base.GetSysExDataNodeText("IsPostBack");

            try
            {
                m_CurrentAction = enumFormActionType.MoveTo;
                if (!Adaptor.userLogin.IsAllowedEx(m_SecurityId, RMO_VIEW))
                {
                    m_CurrentAction = enumFormActionType.None;
                    LogSecurityError(RMO_VIEW);
                    return false;
                }
                if (sIsPostBack == "0" && paramValue[0] == 0)
                {
                    return false;
                }

                else if (sIsPostBack == "1")
                {
                    if (base.m_fda.HasParam("SysPropertyStore"))
                    {
                        sPropertyStore = base.m_fda.SafeParam("SysPropertyStore").InnerXml;
                        propertyStore.LoadXml(sPropertyStore);
                        sEmpNumber = propertyStore.SelectSingleNode("//EmployeeNumber").InnerText;
                    }
                    if (sEmpNumber != "")
                    {
                        sSQL = "SELECT EMPLOYEE_EID FROM EMPLOYEE WHERE EMPLOYEE_NUMBER = " + "'" + sEmpNumber + "'";
                        using (DbReader objDbReader = Employee.Context.DbConnLookup.ExecuteReader(sSQL))
                        {
                            if (objDbReader.Read())
                            {
                                iEmpId = Conversion.ConvertObjToInt(objDbReader.GetValue(0), base.ClientId);
                                flag = 1;
                            }
                        }

                        if (flag == 0)
                        { 
                            XmlElement objSysSkipBindToControl = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysSkipBindToControl");
                            objSysSkipBindToControl.InnerText = "true";
                            base.ResetSysExData("IsEmpNumChanged", "true"); //pmittal5 Mits 17607 11/14/09 - In case Employee Number field is changed for existing employee, set this control to "true"
                            return false;
                        }

                        if (iEmpId != 0 && iEmpId != Employee.EmployeeEid)
                        {
                            paramValue[0] = iEmpId;
                        }
                        else
                        {
                            Employee.PopulateObject(propertyStore);
                            base.OnUpdateObject();
                            UpdateForm();
                            base.ResetSysExData("IsEmpFlag", "1");
                            return true;
                        }
                    }

                }

                BeforeMoveTo(ref bCancel, paramValue);
                if (!bCancel)
                    BeforeAction(m_CurrentAction, ref bCancel);

                if (bCancel)
                {
                    this.Cancelled = true;
                    m_CurrentAction = enumFormActionType.None;
                    return false;
                }

                if (paramValue.Length != 1)
                    Errors.Add("InvalidArgs", Globalization.GetString("FormDataAdaptor.ScreenBase.MoveTo.InvalidArgs", base.ClientId), Common.BusinessAdaptorErrorType.Error);

                ClearViewNodeChanges();
                pINav.MoveTo(paramValue[0]);

                AfterMoveTo();
                AfterAction(m_CurrentAction);

                UpdateForm();
                m_DataChanged = false;
                base.ResetSysExData("IsPostBack", "0");

            }
            catch (Exception e)
            {
                Errors.Add(e, Globalization.GetString("FormDataAdaptor.ScreenBase.MoveTo.Exception", base.ClientId), Common.BusinessAdaptorErrorType.Error);
                m_CurrentAction = enumFormActionType.None;
                return false;
            }
            m_CurrentAction = enumFormActionType.None;
            return true;

        }
        public override void OnUpdateObject()
        {
            base.OnUpdateObject();
            //Added Rakhi for R7:Add Emp Data Elements
            #region "Updating Address Info Object"
             bool bMulAddresses = this.objData.Context.InternalSettings.SysSettings.UseMultipleAddresses;
             //MITS:34276 Starts-- changed for Entity ID Type
             XmlAttribute objTypeAttributeNode = null;
             bool bIsNew = false;
             //MITS:34276 Ends
            //avipinsrivas start : Worked for JIRA - 7767
             if (!this.objData.Context.InternalSettings.SysSettings.UseEntityRole)
             {
                 if (bMulAddresses)
                 {
                     SortedList objEntityXAddressesInfoList = new SortedList();
                     SortedList objAddressesInfoList = new SortedList();
                     //XmlDocument objAddressXPhoneInfoDoc = null;
                     bool bIsSuccess = false;
                     //MITS:34276 starts-- commented and shifted up 
                     //XmlAttribute objTypeAttributeNode = null;
                     //bool bIsNew = false;
                     //MITS:34276 ends
                     int iAddressIdKey = -1;
                     int iRowIdKey = -1;//AA//RMA-8753 nshah28(Added by ashish)
                     XmlDocument objEntityXAddressXmlDoc = null;
                     XmlElement objEntityXAddressRootElement = null;

                     int iEntityXAddressesInfoSelectedId =
                         base.GetSysExDataNodeInt("EntityXAddressesInfoSelectedId", true);

                     string sEntityXAddressesInfoRowAddedFlag =
                         base.GetSysExDataNodeText("EntityXAddressesInfoGrid_RowAddedFlag", true);
                     bool bEntityXAddressesInfoRowAddedFlag =
                         sEntityXAddressesInfoRowAddedFlag.ToUpper().CompareTo("TRUE") == 0 ? true : false;

                     string sEntityXAddressesInfoRowDeletedFlag =
                         base.GetSysExDataNodeText("EntityXAddressesInfoGrid_RowDeletedFlag", true);
                     bool bEntityXAddressesInfoRowDeletedFlag =
                         sEntityXAddressesInfoRowDeletedFlag.ToUpper().CompareTo("TRUE") == 0 ? true : false;


                     XmlNode objEntityXAddressesInfoNode = base.SysEx.DocumentElement.SelectSingleNode("EntityXAddressesInfo");
                     if (objEntityXAddressesInfoNode != null)
                     {
                         // Loop through data for all rows of the grid
                         foreach (XmlNode objOptionNode in objEntityXAddressesInfoNode.SelectNodes("option"))
                         {
                             objTypeAttributeNode = objOptionNode.Attributes["type"];
                             bIsNew = false;
                             if (objTypeAttributeNode != null && objTypeAttributeNode.InnerText.Equals("new") == true)
                             {
                                 // This is the 'extra' hidden <option> for capturing a new grid row data
                                 bIsNew = true;
                             }
                             if ((bIsNew == false) || (bIsNew == true && bEntityXAddressesInfoRowAddedFlag == true))
                             {
                                 objEntityXAddressXmlDoc = new XmlDocument();
                                 objEntityXAddressRootElement = objEntityXAddressXmlDoc.CreateElement("EntityXAddresses");
                                 objEntityXAddressXmlDoc.AppendChild(objEntityXAddressRootElement);

                                 #region AddressXPhone Info Grid
                                 //XmlNode objAddressNode = objOptionNode.SelectSingleNode("AddressXPhoneInfo");
                                 //if (objOptionNode != null && !(bIsNew == false && bEntityXAddressesInfoRowDeletedFlag == true && iAddressIdKey == iEntityXAddressesInfoSelectedId))
                                 //{

                                 //    XmlElement objAddressXPhoneRootElement = null;
                                 //    int iPhoneId = 0;
                                 //    bool bResult = false;
                                 //    string sKey = string.Empty;
                                 //    foreach (XmlNode objPhoneNode in objAddressNode.SelectNodes("option"))
                                 //    {
                                 //        objAddressXPhoneInfoDoc = new XmlDocument();
                                 //        objAddressXPhoneRootElement = objAddressXPhoneInfoDoc.CreateElement("AddressXPhoneInfo");
                                 //        objAddressXPhoneInfoDoc.AppendChild(objAddressXPhoneRootElement);
                                 //        objPhoneNode.SelectSingleNode("ContactId").InnerText = objOptionNode.SelectSingleNode("ContactId").InnerText;
                                 //        objAddressXPhoneRootElement.InnerXml = objPhoneNode.InnerXml;
                                 //        bResult = Int32.TryParse(objAddressXPhoneRootElement.SelectSingleNode("PhoneId").InnerText, out iPhoneId);
                                 //        if (iPhoneId != 0)
                                 //        {
                                 //            sKey = objPhoneNode.SelectSingleNode("ContactId").InnerText + "_" + objAddressXPhoneRootElement.SelectSingleNode("PhoneId").InnerText;
                                 //            objAddressesInfoList.Add(sKey, objAddressXPhoneInfoDoc);
                                 //        }
                                 //    }
                                 //}
                                 //objOptionNode.RemoveChild(objAddressNode);
                                 #endregion //AddressXPhone Info Grid

                                 objEntityXAddressRootElement.InnerXml = objOptionNode.InnerXml;

                                 //AA //RMA-8753 nshah28 start
                                 string strSearchString = string.Empty;
                                 string sStateCode = string.Empty;
                                 string sStateDesc = string.Empty;
                                 string sCountry = string.Empty;

                                 AddressForm objAddressForm = new AddressForm(m_fda);
                                 strSearchString = objAddressForm.CreateSearchString(objEntityXAddressRootElement, objData.Context.LocalCache);
                                 //RMA-8753 nshah28(Added by ashish)
                                 XmlElement objRootElement = objEntityXAddressXmlDoc.CreateElement("SearchString");
                                 objRootElement.InnerText = strSearchString; //change by nshah28
                                 objEntityXAddressRootElement.AppendChild(objRootElement);
                                 //RMA-8753 nshah28(Added by ashish)
                                 iRowIdKey = Conversion.CastToType<int>(objEntityXAddressRootElement.SelectSingleNode("RowId").InnerText, out bIsSuccess);

                                 //  iAddressIdKey =
                                 //    Conversion.CastToType<int>(objEntityXAddressRootElement.SelectSingleNode("AddressId").InnerText, out bIsSuccess);

                                 if ((bIsNew == true && bEntityXAddressesInfoRowAddedFlag == true) ||
                                     (iAddressIdKey < 0 && objEntityXAddressRootElement.SelectSingleNode("EntityId").InnerText == ""))
                                     objEntityXAddressRootElement.SelectSingleNode("EntityId").InnerText = "0";
                                 else
                                     objEntityXAddressRootElement.SelectSingleNode("EntityId").InnerText = Employee.EmployeeEid.ToString(); //RMA-8753(Adding else condition)
                                 //Marking Changed Address as Primary Address
                                 XmlNode objPrimaryAddressChanged = base.SysEx.SelectSingleNode("//PrimaryAddressChanged");
                                 XmlNode objNewPrimaryAddress = objEntityXAddressRootElement.SelectSingleNode("NewPrimaryRecord"); //For identification of New Primary Address
                                 if (objPrimaryAddressChanged != null && objPrimaryAddressChanged.InnerText.ToLower() == "true")
                                 {
                                     if (objNewPrimaryAddress != null)
                                     {
                                         XmlNode objPrimaryAddress = objEntityXAddressRootElement.SelectSingleNode("PrimaryAddress");
                                         if (objPrimaryAddress != null)
                                         {
                                             if (objNewPrimaryAddress.InnerText.ToLower() == "true")
                                             {
                                                 objPrimaryAddress.InnerText = "True";
                                             }

                                             else
                                             {
                                                 objPrimaryAddress.InnerText = "False";
                                             }
                                         }
                                     }
                                 }
                                 if (objNewPrimaryAddress != null)
                                 {
                                     objEntityXAddressRootElement.RemoveChild(objNewPrimaryAddress); //Since it is not a Datamodel Property,Removing it before the processing.
                                 }
                                 //Marking Changed Address as Primary Address
                                 // if (bIsNew == false && bEntityXAddressesInfoRowDeletedFlag == true && iAddressIdKey == iEntityXAddressesInfoSelectedId)
                                 if (bIsNew == false && bEntityXAddressesInfoRowDeletedFlag == true && iRowIdKey == iEntityXAddressesInfoSelectedId) //RMA-8753 nshah28
                                 {
                                     // Aditya - This particular Tpp has been deleted. 
                                     continue;
                                 }
                                 // objEntityXAddressesInfoList.Add(iAddressIdKey, objEntityXAddressXmlDoc);
                                 objEntityXAddressesInfoList.Add(iRowIdKey, objEntityXAddressXmlDoc); //RMA-8753 nshah28
                             }
                         }
                     }

                     /* foreach (EntityXAddresses objEntityXAddressesInfo in Employee.EmployeeEntity.EntityXAddressesList)
                      {
                          if (base.m_fda.SafeFormVariableParamText("SysCmd") == "7" &&
                              (objEntityXAddressesInfo.AddressId < 0 || (bEntityXAddressesInfoRowDeletedFlag == true && objEntityXAddressesInfo.AddressId == iEntityXAddressesInfoSelectedId)))
                          {
                              Employee.EmployeeEntity.EntityXAddressesList.Remove(objEntityXAddressesInfo.AddressId);
                          }
                          else if (objEntityXAddressesInfo.AddressId < 0)
                              Employee.EmployeeEntity.EntityXAddressesList.Remove(objEntityXAddressesInfo.AddressId);
                      } */
                     foreach (EntityXAddresses objEntityXAddressesInfo in Employee.EmployeeEntity.EntityXAddressesList)
                     {
                         if (base.m_fda.SafeFormVariableParamText("SysCmd") == "7" &&
                             (objEntityXAddressesInfo.RowId < 0 || (bEntityXAddressesInfoRowDeletedFlag == true && objEntityXAddressesInfo.RowId == iEntityXAddressesInfoSelectedId)))
                         {
                             Employee.EmployeeEntity.EntityXAddressesList.Remove(objEntityXAddressesInfo.RowId);
                         }
                         else if (objEntityXAddressesInfo.RowId < 0)
                             Employee.EmployeeEntity.EntityXAddressesList.Remove(objEntityXAddressesInfo.RowId);
                     }
                     EntityXAddresses objTmpEntityXAddressesInfo = null;
                     XmlDocument objTmpEntityXAddressXMLDoc = null;
                     XmlDocument objTmpAddressXMLDoc = null; //RMA-8753 nshah28
                     //AddressXPhoneInfo objTmpAddressXPhoneInfo = null;
                     //XmlDocument objTmpAddressXPhoneXMLDoc = null;
                     //string sOrgAddressId = string.Empty;
                     //Parijat:Mits 9610
                     ArrayList arrAddressIds = new ArrayList();
                     ArrayList arrRowIds = new ArrayList(); //RMA-8753 nshah28
                     //
                     for (int iListIndex = 0; iListIndex < objEntityXAddressesInfoList.Count; iListIndex++)
                     {
                         objTmpEntityXAddressXMLDoc = (XmlDocument)objEntityXAddressesInfoList.GetByIndex(iListIndex);
                         //sOrgAddressId = objTmpEntityXAddressXMLDoc.DocumentElement.SelectSingleNode("AddressId").InnerText.Trim();
                         if (objTmpEntityXAddressXMLDoc.DocumentElement.SelectSingleNode("RowId").InnerText.Trim() == "" || Conversion.CastToType<int>(objTmpEntityXAddressXMLDoc.DocumentElement.SelectSingleNode("RowId").InnerText, out bIsSuccess) < 0)
                         {
                             objTmpEntityXAddressXMLDoc.DocumentElement.SelectSingleNode("RowId").InnerText = ((-1 * iListIndex) - 1).ToString();
                             objTmpEntityXAddressesInfo = Employee.EmployeeEntity.EntityXAddressesList.AddNew();
                         }
                         else
                             objTmpEntityXAddressesInfo = Employee.EmployeeEntity.EntityXAddressesList[Conversion.CastToType<int>(objTmpEntityXAddressXMLDoc.DocumentElement.SelectSingleNode("RowId").InnerText, out bIsSuccess)];
                         //RMA-8753 nshah28(Replacing "AddressId" by "RowId" in above condition)
                         //Parijat:Mits 9610
                         arrAddressIds.Add(objTmpEntityXAddressXMLDoc.DocumentElement.SelectSingleNode("AddressId").InnerText);
                         arrRowIds.Add(objTmpEntityXAddressXMLDoc.DocumentElement.SelectSingleNode("RowId").InnerText); //RMA-8753 nshah28
                         //objTmpEntityXAddressesInfo.PopulateObject(objTmpEntityXAddressXMLDoc);
                         //RMA 8753 Ashish Ahuja For Address Master Enhancement Start
                         if (objTmpEntityXAddressXMLDoc.SelectSingleNode("//RowId").InnerText != String.Empty)
                             objTmpEntityXAddressesInfo.RowId = Convert.ToInt32(objTmpEntityXAddressXMLDoc.SelectSingleNode("//RowId").InnerText);
                         if (objTmpEntityXAddressXMLDoc.SelectSingleNode("//AddressType").Attributes["codeid"].Value != String.Empty)
                             objTmpEntityXAddressesInfo.AddressType = Convert.ToInt32(objTmpEntityXAddressXMLDoc.SelectSingleNode("//AddressType").Attributes["codeid"].Value);
                         if (objTmpEntityXAddressXMLDoc.SelectSingleNode("//PrimaryAddress").InnerText != String.Empty)
                             objTmpEntityXAddressesInfo.PrimaryAddress = Convert.ToBoolean(objTmpEntityXAddressXMLDoc.SelectSingleNode("//PrimaryAddress").InnerText) ? -1 : 0;
                         //objTmpEntityXAddressesInfo.PrimaryAddress = Convert.ToInt32(objTmpEntityXAddressXMLDoc.SelectSingleNode("//PrimaryAddress").InnerText);
                         objTmpEntityXAddressesInfo.Address.Addr1 = objTmpEntityXAddressXMLDoc.SelectSingleNode("//Addr1").InnerText;
                         objTmpEntityXAddressesInfo.Address.Addr2 = objTmpEntityXAddressXMLDoc.SelectSingleNode("//Addr2").InnerText;
                         objTmpEntityXAddressesInfo.Address.Addr3 = objTmpEntityXAddressXMLDoc.SelectSingleNode("//Addr3").InnerText;
                         objTmpEntityXAddressesInfo.Address.Addr4 = objTmpEntityXAddressXMLDoc.SelectSingleNode("//Addr4").InnerText;
                         objTmpEntityXAddressesInfo.Address.City = objTmpEntityXAddressXMLDoc.SelectSingleNode("//City").InnerText;
                         if (objTmpEntityXAddressXMLDoc.SelectSingleNode("//Country").Attributes["codeid"].Value != String.Empty)
                             objTmpEntityXAddressesInfo.Address.Country = Convert.ToInt32(objTmpEntityXAddressXMLDoc.SelectSingleNode("//Country").Attributes["codeid"].Value);
                         if (objTmpEntityXAddressXMLDoc.SelectSingleNode("//State").Attributes["codeid"].Value != String.Empty)
                             objTmpEntityXAddressesInfo.Address.State = Convert.ToInt32(objTmpEntityXAddressXMLDoc.SelectSingleNode("//State").Attributes["codeid"].Value);
                         objTmpEntityXAddressesInfo.Email = objTmpEntityXAddressXMLDoc.SelectSingleNode("//Email").InnerText;
                         objTmpEntityXAddressesInfo.Fax = objTmpEntityXAddressXMLDoc.SelectSingleNode("//Fax").InnerText; ;
                         objTmpEntityXAddressesInfo.Address.County = objTmpEntityXAddressXMLDoc.SelectSingleNode("//County").InnerText;
                         objTmpEntityXAddressesInfo.Address.ZipCode = objTmpEntityXAddressXMLDoc.SelectSingleNode("//ZipCode").InnerText;
                         objTmpEntityXAddressesInfo.EffectiveDate = objTmpEntityXAddressXMLDoc.SelectSingleNode("//EffectiveDate").InnerText;
                         objTmpEntityXAddressesInfo.ExpirationDate = objTmpEntityXAddressXMLDoc.SelectSingleNode("//ExpirationDate").InnerText;
                         objTmpEntityXAddressesInfo.Address.SearchString = objTmpEntityXAddressXMLDoc.SelectSingleNode("//SearchString").InnerText;
                         int iAddressId = CommonFunctions.CheckAddressDuplication(objTmpEntityXAddressXMLDoc.SelectSingleNode("//SearchString").InnerText, Employee.Context.RMDatabase.ConnectionString, base.ClientId);
                         objTmpEntityXAddressesInfo.AddressId = iAddressId;
                         objTmpEntityXAddressesInfo.Address.AddressId = iAddressId;
                         //RMA 8753 Ashish Ahuja For Address Master Enhancement End

                         #region Phone Info Details
                         //int iPhoneIndex = 0;
                         //for (int iCount = 0; iCount < objAddressesInfoList.Count; iCount++)
                         //{
                         //    objTmpAddressXPhoneXMLDoc = (XmlDocument)objAddressesInfoList.GetByIndex(iCount);
                         //    if (sOrgContactId == objTmpAddressXPhoneXMLDoc.DocumentElement.SelectSingleNode("ContactId").InnerText.Trim())
                         //    {
                         //        if (objTmpAddressXPhoneXMLDoc.DocumentElement.SelectSingleNode("PhoneId").InnerText.Trim() == "" || Conversion.ConvertStrToInteger(objTmpAddressXPhoneXMLDoc.DocumentElement.SelectSingleNode("PhoneId").InnerText) < 0)
                         //        {
                         //            objTmpAddressXPhoneXMLDoc.DocumentElement.SelectSingleNode("PhoneId").InnerText = ((-1 * iPhoneIndex) - 1).ToString();
                         //            objTmpAddressXPhoneInfo = objTmpEntityXAddressesInfo.AddressXPhoneInfoList.AddNew();
                         //            iPhoneIndex++;
                         //        }
                         //        else
                         //            objTmpAddressXPhoneInfo = objTmpEntityXAddressesInfo.AddressXPhoneInfoList[Conversion.ConvertStrToInteger(objTmpAddressXPhoneXMLDoc.DocumentElement.SelectSingleNode("PhoneId").InnerText)];
                         //        objTmpAddressXPhoneInfo.PopulateObject(objTmpAddressXPhoneXMLDoc);
                         //        objTmpAddressXPhoneInfo.ContactId = objTmpEntityXAddressesInfo.ContactId;
                         //    }
                         //}
                         #endregion //Phone Info Details

                         #region Added code to remove the saved phone numbers
                         //if (base.m_fda.SafeFormVariableParamText("SysCmd") == "5")
                         //{
                         //    XmlNode objPhoneNumbersDeleted = base.SysEx.SelectSingleNode("//PopupGridRowsDeleted");
                         //    if (objPhoneNumbersDeleted != null && !String.IsNullOrEmpty(objPhoneNumbersDeleted.InnerText))
                         //    {
                         //        string[] strSplit = objPhoneNumbersDeleted.InnerText.Split(new Char[] { '|' });
                         //        foreach (AddressXPhoneInfo objAddressXPhoneInfo in objTmpEntityXAddressesInfo.AddressXPhoneInfoList)
                         //        {
                         //            foreach (string sPhoneNumber in strSplit)
                         //            {
                         //                int iPhoneId = Conversion.ConvertStrToInteger(sPhoneNumber);
                         //                if (iPhoneId == objAddressXPhoneInfo.PhoneId)
                         //                {
                         //                    objTmpEntityXAddressesInfo.AddressXPhoneInfoList.Remove(iPhoneId);
                         //                    break;
                         //                }

                         //            }
                         //        }
                         //    }
                         //}
                         #endregion //Added code to remove the saved phone numbers
                     }
                     //Parijat:Mits 9610
                     /* foreach (EntityXAddresses objEntityXAddressesInfo in Employee.EmployeeEntity.EntityXAddressesList)
                      {
                          if (!arrAddressIds.Contains(objEntityXAddressesInfo.AddressId.ToString()))
                          {
                              Employee.EmployeeEntity.EntityXAddressesList.Remove(objEntityXAddressesInfo.AddressId);
                          }
                      } */
                     //RMA-8753 start
                     foreach (EntityXAddresses objEntityXAddressesInfo in Employee.EmployeeEntity.EntityXAddressesList)
                     {
                         if (!arrRowIds.Contains(objEntityXAddressesInfo.RowId.ToString()))
                         {
                             Employee.EmployeeEntity.EntityXAddressesList.Remove(objEntityXAddressesInfo.RowId);
                         }
                     } //8753-End
                 }
                 else
                 {
                     if (base.m_fda.SafeFormVariableParamText("SysCmd") == "5")
                     {
                         string sAddr1 = Employee.EmployeeEntity.Addr1;
                         string sAddr2 = Employee.EmployeeEntity.Addr2;
                         string sAddr3 = Employee.EmployeeEntity.Addr3;// JIRA 6420 pkandhari
                         string sAddr4 = Employee.EmployeeEntity.Addr4;// JIRA 6420 pkandhari
                         string sCity = Employee.EmployeeEntity.City;
                         int iCountryCode = Employee.EmployeeEntity.CountryCode;
                         int iStateId = Employee.EmployeeEntity.StateId;
                         string sEmailAddress = Employee.EmployeeEntity.EmailAddress;
                         string sFaxNumber = Employee.EmployeeEntity.FaxNumber;
                         string sCounty = Employee.EmployeeEntity.County;
                         string sZipCode = Employee.EmployeeEntity.ZipCode;
                         //RMA-8753 nshah28(Added by ashish)
                         string sSearchString = string.Empty;
                         //RMA-8753 nshah28(Added by ashish) END
                         if (Employee.EmployeeEntity.EntityXAddressesList.Count > 0)
                         {
                             foreach (EntityXAddresses objEntityXAddressesInfo in Employee.EmployeeEntity.EntityXAddressesList)
                             {
                                 if (objEntityXAddressesInfo.PrimaryAddress == -1)
                                 {
                                     //RMA-8753 nshah28(Added by ashish) START
                                     //objEntityXAddressesInfo.Addr1 = sAddr1;
                                     //objEntityXAddressesInfo.Addr2 = sAddr2;
                                     //objEntityXAddressesInfo.Addr3 = sAddr3;// JIRA 6420 pkandhari
                                     //objEntityXAddressesInfo.Addr4 = sAddr4;// JIRA 6420 pkandhari
                                     //objEntityXAddressesInfo.City = sCity;
                                     //objEntityXAddressesInfo.Country = iCountryCode;
                                     //objEntityXAddressesInfo.State = iStateId;
                                     objEntityXAddressesInfo.Address.Addr1 = sAddr1;
                                     objEntityXAddressesInfo.Address.Addr2 = sAddr2;
                                     objEntityXAddressesInfo.Address.Addr3 = sAddr3;// JIRA 6420 pkandhari
                                     objEntityXAddressesInfo.Address.Addr4 = sAddr4;// JIRA 6420 pkandhari
                                     objEntityXAddressesInfo.Address.City = sCity;
                                     objEntityXAddressesInfo.Address.Country = iCountryCode;
                                     objEntityXAddressesInfo.Address.State = iStateId;
                                     objEntityXAddressesInfo.Email = sEmailAddress;
                                     objEntityXAddressesInfo.Fax = sFaxNumber;
                                     //objEntityXAddressesInfo.County = sCounty;
                                     //objEntityXAddressesInfo.ZipCode = sZipCode;
                                     objEntityXAddressesInfo.Address.County = sCounty;
                                     objEntityXAddressesInfo.Address.ZipCode = sZipCode;
                                     AddressForm objAddressForm = new AddressForm(m_fda);
                                     sSearchString = objAddressForm.SearchStringForAddress(objEntityXAddressesInfo.Address, objData.Context.LocalCache);
                                     objEntityXAddressesInfo.Address.SearchString = sSearchString;
                                     objEntityXAddressesInfo.AddressId = CommonFunctions.CheckAddressDuplication(sSearchString, Employee.Context.RMDatabase.ConnectionString, base.ClientId);
                                     objEntityXAddressesInfo.Address.AddressId = objEntityXAddressesInfo.AddressId;
                                     //RMA-8753 nshah28(Added by ashish) END
                                     break;
                                 }

                             }
                         }
                         else
                         {
                             if (
                                    sAddr1 != string.Empty || sAddr2 != string.Empty || sAddr3 != string.Empty || sAddr4 != string.Empty ||
                                    sCity != string.Empty || iCountryCode != 0 || iStateId != 0 || sEmailAddress != string.Empty ||
                                    sFaxNumber != string.Empty || sCounty != string.Empty || sZipCode != string.Empty
                                )
                             {
                                 EntityXAddresses objEntityXAddressesInfo = Employee.EmployeeEntity.EntityXAddressesList.AddNew();
                                 //RMA-8753 nshah28(Added by ashish) START
                                 //objEntityXAddressesInfo.Addr1 = sAddr1;
                                 //objEntityXAddressesInfo.Addr2 = sAddr2;
                                 //objEntityXAddressesInfo.Addr3 = sAddr3;// JIRA 6420 pkandhari
                                 //objEntityXAddressesInfo.Addr4 = sAddr4;// JIRA 6420 pkandhari
                                 //objEntityXAddressesInfo.City = sCity;
                                 //objEntityXAddressesInfo.Country = iCountryCode;
                                 //objEntityXAddressesInfo.State = iStateId;
                                 objEntityXAddressesInfo.Address.Addr1 = sAddr1;
                                 objEntityXAddressesInfo.Address.Addr2 = sAddr2;
                                 objEntityXAddressesInfo.Address.Addr3 = sAddr3;// JIRA 6420 pkandhari
                                 objEntityXAddressesInfo.Address.Addr4 = sAddr4;// JIRA 6420 pkandhari
                                 objEntityXAddressesInfo.Address.City = sCity;
                                 objEntityXAddressesInfo.Address.Country = iCountryCode;
                                 objEntityXAddressesInfo.Address.State = iStateId;
                                 objEntityXAddressesInfo.Email = sEmailAddress;
                                 objEntityXAddressesInfo.Fax = sFaxNumber;
                                 //objEntityXAddressesInfo.County = sCounty;
                                 //objEntityXAddressesInfo.ZipCode = sZipCode;
                                 objEntityXAddressesInfo.Address.County = sCounty;
                                 objEntityXAddressesInfo.Address.ZipCode = sZipCode;
                                 //RMA-8753 nshah28(Added by ashish) END
                                 objEntityXAddressesInfo.EntityId = Employee.EmployeeEntity.EntityId;
                                 objEntityXAddressesInfo.PrimaryAddress = -1;
                                 //objEntityXAddressesInfo.AddressId = -1;
                                 AddressForm objAddressForm = new AddressForm(m_fda);
                                 sSearchString = objAddressForm.SearchStringForAddress(objEntityXAddressesInfo.Address, objData.Context.LocalCache);
                                 objEntityXAddressesInfo.Address.SearchString = sSearchString;
                                 objEntityXAddressesInfo.AddressId = CommonFunctions.CheckAddressDuplication(sSearchString, Employee.Context.RMDatabase.ConnectionString, base.ClientId);
                                 objEntityXAddressesInfo.Address.AddressId = objEntityXAddressesInfo.AddressId;
                             }
                         }
                     }
                 }
             }
            //avipinsrivas end
            #endregion

             #region Update Phone Numbers
             if (base.m_fda.SafeFormVariableParamText("SysCmd") == "5")
             {
                 Employee.EmployeeEntity.FormName = "EmployeeForm"; //Mits 22497

                 XmlNode objPhoneNumber = base.SysEx.SelectSingleNode("//PhoneNumbers");
                 if (objPhoneNumber != null)
                 {
                     string sPhoneData = objPhoneNumber.InnerText;
                     string[] sPhoneNumbers = sPhoneData.Split(new string[] { CommonForm.OUTER_PHONE_DELIMITER }, StringSplitOptions.None);
                     foreach (string sPhoneNumber in sPhoneNumbers)
                     {
                         string[] sNumber = sPhoneNumber.Split(new string[] { CommonForm.INNER_PHONE_DELIMITER }, StringSplitOptions.None);
                         if (sNumber.Length > 2)
                         {
                             string sPhoneId = sNumber[0];
                             string sPhoneType = sNumber[1];
                             string sPhoneNo = sNumber[2];
                             bool bPhoneCodeExists = false;
                             string sPhoneDesc = string.Empty;
                             string sPhoneShortCode = string.Empty; //Aman ML Change
                             if (Employee.EmployeeEntity.AddressXPhoneInfoList.Count > 0)
                             {
                                 foreach (AddressXPhoneInfo objAddressXPhoneInfo in Employee.EmployeeEntity.AddressXPhoneInfoList)
                                 {
                                     if (objAddressXPhoneInfo.PhoneCode == Convert.ToInt32(sPhoneType))
                                     {
                                         if (sPhoneNo.Trim() != string.Empty)
                                             objAddressXPhoneInfo.PhoneNo = sPhoneNo;
                                         else
                                             Employee.EmployeeEntity.AddressXPhoneInfoList.Remove(objAddressXPhoneInfo.PhoneId);

                                         bPhoneCodeExists = true;
                                         break;
                                     }

                                 }
                                 if (!bPhoneCodeExists && sPhoneNo.Trim() != string.Empty)
                                 {
                                     AddressXPhoneInfo objAddressXPhoneInfo = Employee.EmployeeEntity.AddressXPhoneInfoList.AddNew();
                                     objAddressXPhoneInfo.PhoneId = Convert.ToInt32(sPhoneId);
                                     objAddressXPhoneInfo.PhoneCode = Convert.ToInt32(sPhoneType);
                                     objAddressXPhoneInfo.PhoneNo = sPhoneNo;
                                 }
                             }
                             else
                             {
                                 if (sPhoneNo.Trim() != string.Empty)
                                 {
                                     AddressXPhoneInfo objAddressXPhoneInfo = Employee.EmployeeEntity.AddressXPhoneInfoList.AddNew();
                                     objAddressXPhoneInfo.PhoneId = Convert.ToInt32(sPhoneId);
                                     objAddressXPhoneInfo.PhoneCode = Convert.ToInt32(sPhoneType);
                                     objAddressXPhoneInfo.PhoneNo = sPhoneNo;
                                 }
                             }
                             //Added to save Office and Alt phone numbers in Entity Table as well
                             //sPhoneDesc = this.objCache.GetCodeDesc(Convert.ToInt32(sPhoneType)).Trim().ToLower();
                             //{
                             //    if (sPhoneDesc == "office")
                             //        Employee.EmployeeEntity.Phone1 = sPhoneNo;
                             //    else if (sPhoneDesc == "home")
                             //        Employee.EmployeeEntity.Phone2 = sPhoneNo;
                             //}
                             //Added to save Office and Alt phone numbers in Entity Table as well
                             //Aman ML Change
                             sPhoneShortCode = this.objCache.GetShortCode(Convert.ToInt32(sPhoneType)).Trim().ToLower();
                             {
                                 if (sPhoneShortCode == "o")
                                     Employee.EmployeeEntity.Phone1 = sPhoneNo;
                                 else if (sPhoneShortCode == "h")
                                     Employee.EmployeeEntity.Phone2 = sPhoneNo;
                             }
                             //Aman ML Change
                         }
                     }



                 }
             }

             #endregion

            //Added Rakhi for R7:Add Emp Data Elements

             //MITS:34276-- Entity ID Type tab display start
             #region "Updating Entity Id Type"
             SortedList objEntityIdTypeAsList = new SortedList();

             XmlNode objEntityIdTypeAsNode = base.SysEx.DocumentElement.SelectSingleNode("EntityXEntityIDType");
             objTypeAttributeNode = null;

             bIsNew = false;
             int iEntityIdTypeKey = -1;

             XmlDocument objEntityXIDTypeXmlDoc = null;
             XmlElement objEntityXIDTypeRootElement = null;

             int iEntityIdTypeSelectedId = base.GetSysExDataNodeInt("EntityXEntityIDTypeSelectedId", true);

             string sEntityIdTypeRowAddedFlag = base.GetSysExDataNodeText("EntityXEntityIDTypeGrid_RowAddedFlag", true);
             bool bEntityIdTypeRowAddedFlag = sEntityIdTypeRowAddedFlag.ToUpper().CompareTo("TRUE") == 0 ? true : false;

             string sEntityIdTypeRowDeletedFlag = base.GetSysExDataNodeText("EntityXEntityIDTypeGrid_RowDeletedFlag", true);
             bool bEntityIdTypeRowDeletedFlag = sEntityIdTypeRowDeletedFlag.ToUpper().CompareTo("TRUE") == 0 ? true : false;

             if (objEntityIdTypeAsNode != null)
             {
                 // Loop through data for all rows of the grid
                 foreach (XmlNode objOptionNode in objEntityIdTypeAsNode.SelectNodes("option"))
                 {
                     objTypeAttributeNode = objOptionNode.Attributes["type"];
                     bIsNew = false;
                     if (objTypeAttributeNode != null && objTypeAttributeNode.InnerText.Equals("new") == true)
                     {
                         // This is the 'extra' hidden <option> for capturing a new grid row data
                         bIsNew = true;
                     }
                     if ((bIsNew == false) || (bIsNew == true && bEntityIdTypeRowAddedFlag == true))
                     {
                         objEntityXIDTypeXmlDoc = new XmlDocument();
                         objEntityXIDTypeRootElement = objEntityXIDTypeXmlDoc.CreateElement("EntityXEntityIDType");
                         objEntityXIDTypeXmlDoc.AppendChild(objEntityXIDTypeRootElement);
                         objEntityXIDTypeRootElement.InnerXml = objOptionNode.InnerXml;

                         iEntityIdTypeKey = Conversion.ConvertStrToInteger(objEntityXIDTypeRootElement.SelectSingleNode("IdNumRowId").InnerText);

                         if ((bIsNew == true && bEntityIdTypeRowAddedFlag == true) || (iEntityIdTypeKey < 0 && objEntityXIDTypeRootElement.SelectSingleNode("EntityId").InnerText == ""))
                             objEntityXIDTypeRootElement.SelectSingleNode("EntityId").InnerText = "0";

                         if (bIsNew == false && bEntityIdTypeRowDeletedFlag == true && iEntityIdTypeKey == iEntityIdTypeSelectedId)
                         {
                             continue;
                         }

                         objEntityIdTypeAsList.Add(iEntityIdTypeKey, objEntityXIDTypeXmlDoc);
                     }
                 }
             }

             foreach (EntityXEntityIDType objEntityXEntityIDTypeAs in Employee.EmployeeEntity.EntityXEntityIDTypeList)
             {
                 if (base.m_fda.SafeFormVariableParamText("SysCmd") == "7" && (objEntityXEntityIDTypeAs.IdNumRowId < 0 || (bEntityIdTypeRowDeletedFlag == true && objEntityXEntityIDTypeAs.IdNumRowId == iEntityIdTypeSelectedId)))
                 {
                     Employee.EmployeeEntity.EntityXEntityIDTypeList.Remove(objEntityXEntityIDTypeAs.IdNumRowId);
                 }
                 else if (objEntityXEntityIDTypeAs.IdNumRowId < 0)
                     Employee.EmployeeEntity.EntityXEntityIDTypeList.Remove(objEntityXEntityIDTypeAs.IdNumRowId);
             }


             EntityXEntityIDType objTmpEntityXEntityIDTypeAs = null;
             XmlDocument objTmpEntityXEntityIdTypeDoc = null;

             ArrayList arrEntityIdTypes = new ArrayList();

             for (int iListIndex = 0; iListIndex < objEntityIdTypeAsList.Count; iListIndex++)
             {
                 objTmpEntityXEntityIdTypeDoc = (XmlDocument)objEntityIdTypeAsList.GetByIndex(iListIndex);

                 if (objTmpEntityXEntityIdTypeDoc.DocumentElement.SelectSingleNode("IdNumRowId").InnerText.Trim() == "" || Conversion.ConvertStrToInteger(objTmpEntityXEntityIdTypeDoc.DocumentElement.SelectSingleNode("IdNumRowId").InnerText) < 0)
                 {
                     objTmpEntityXEntityIdTypeDoc.DocumentElement.SelectSingleNode("IdNumRowId").InnerText = ((-1 * iListIndex) - 1).ToString();
                     objTmpEntityXEntityIDTypeAs = Employee.EmployeeEntity.EntityXEntityIDTypeList.AddNew();
                     objTmpEntityXEntityIDTypeAs.PopulateObject(objTmpEntityXEntityIdTypeDoc);
                 }
                 else
                 {
                     objTmpEntityXEntityIDTypeAs = Employee.EmployeeEntity.EntityXEntityIDTypeList[Conversion.ConvertStrToInteger(objTmpEntityXEntityIdTypeDoc.DocumentElement.SelectSingleNode("IdNumRowId").InnerText)];

                     objTmpEntityXEntityIDTypeAs.PopulateObject(objTmpEntityXEntityIdTypeDoc);
                 }

                 arrEntityIdTypes.Add(objTmpEntityXEntityIdTypeDoc.DocumentElement.SelectSingleNode("IdNumRowId").InnerText);
             }


             foreach (EntityXEntityIDType objEntityXEntityIDType in Employee.EmployeeEntity.EntityXEntityIDTypeList)
             {
                 if (!arrEntityIdTypes.Contains(objEntityXEntityIDType.IdNumRowId.ToString()))
                 {
                     Employee.EmployeeEntity.EntityXEntityIDTypeList.Remove(objEntityXEntityIDType.IdNumRowId);
                 }
             }

             #endregion
            //MITS:34276-- Entity ID Type tab display end
        }
        private XmlDocument GetEntityXAddressInfo()
        {
            XmlElement objOptionXmlElement = null;
            XmlElement objXmlElement = null;
            XmlElement objListHeadXmlElement = null;
            XmlAttribute objXmlAttribute = null;
            int iEntityXAddressInfoCount = 0;
            string sStateCode = string.Empty;
            string sStateDesc = string.Empty;
            string sCountryCode = string.Empty;
            string sCountryDesc = string.Empty;
            string sPrimaryAddressRef = string.Empty;
            string p_ShortCode = String.Empty; //MITS:34276 Entity Address Type
            string p_sDesc = String.Empty; //MITS:34276 Entity Address Type

            XmlDocument objEntityXAddressInfoXmlDoc = new XmlDocument();
            XmlElement objRootElement = objEntityXAddressInfoXmlDoc.CreateElement("EntityXAddressesInfo");
            objEntityXAddressInfoXmlDoc.AppendChild(objRootElement);

            objListHeadXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("listhead");

            //MITS:34276 Entity Address Type START
            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("AddressType");
            objXmlElement.InnerText = "Address Type";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;
            //MITS:34276 Entity Address Type END

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr1");
            objXmlElement.InnerText = "Address1";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr2");
            objXmlElement.InnerText = "Address2";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr3");// JIRA 6420 pkandhari
            objXmlElement.InnerText = "Address3";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr4");// JIRA 6420 pkandhari
            objXmlElement.InnerText = "Address4";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("City");
            objXmlElement.InnerText = "City";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("State");
            objXmlElement.InnerText = "State";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Country");
            objXmlElement.InnerText = "Country";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("County");
            objXmlElement.InnerText = "County";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("ZipCode");
            objXmlElement.InnerText = "Zip Code";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Email");
            objXmlElement.InnerText = "E-Mail";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Fax");
            objXmlElement.InnerText = "Fax No.";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("PrimaryAddress");
            objXmlElement.InnerText = "Primary Address";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            //JIRA:6865 START:
            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("EffectiveDate");
            objXmlElement.InnerText = "Effective Date";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("ExpirationDate");
            objXmlElement.InnerText = "Expiration Date";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;
            //JIRA:6865 END:

            objRootElement.AppendChild(objListHeadXmlElement);
            objListHeadXmlElement = null;


            foreach (EntityXAddresses objEntityXAddressInfo in this.Employee.EmployeeEntity.EntityXAddressesList)
            {

                iEntityXAddressInfoCount++;

                objOptionXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("option");

                objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("ref");
                objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH
                    + "/" + objEntityXAddressInfoXmlDoc.DocumentElement.LocalName
                    + "/option[" + iEntityXAddressInfoCount.ToString() + "]";
                objOptionXmlElement.Attributes.Append(objXmlAttribute);
                sPrimaryAddressRef = objXmlAttribute.InnerText;
                objXmlAttribute = null;

                //MITS:34276 Entity Address Type START
                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("AddressType");
                this.objCache.GetCodeInfo(objEntityXAddressInfo.AddressType, ref p_ShortCode, ref p_sDesc);
                objXmlElement.InnerText = p_ShortCode + " " + p_sDesc;
                objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("codeid");
                objXmlAttribute.InnerText = objEntityXAddressInfo.AddressType.ToString();
                objXmlElement.Attributes.Append(objXmlAttribute);
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlAttribute = null;
                objXmlElement = null;
                //MITS:34276 Entity Address Type END

				//RMA-8753 nshah28(Added by ashish) START
                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr1");
                objXmlElement.InnerText = objEntityXAddressInfo.Address.Addr1;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr2");
                objXmlElement.InnerText = objEntityXAddressInfo.Address.Addr2;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr3");// JIRA 6420 pkandhari
                objXmlElement.InnerText = objEntityXAddressInfo.Address.Addr3;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr4");// JIRA 6420 pkandhari
                objXmlElement.InnerText = objEntityXAddressInfo.Address.Addr4;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("City");
                objXmlElement.InnerText = objEntityXAddressInfo.Address.City;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("State");
                this.objCache.GetStateInfo(objEntityXAddressInfo.Address.State, ref sStateCode, ref sStateDesc);
                objXmlElement.InnerText = sStateCode + " " + sStateDesc;
                objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("codeid");
                objXmlAttribute.InnerText = objEntityXAddressInfo.Address.State.ToString();
                objXmlElement.Attributes.Append(objXmlAttribute);
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlAttribute = null;
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Country");
                this.objCache.GetCodeInfo(objEntityXAddressInfo.Address.Country, ref sCountryCode, ref sCountryDesc, base.Adaptor.userLogin.objUser.NlsCode); //Aman ML Change
                objXmlElement.InnerText = sCountryCode + " " + sCountryDesc;
                objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("codeid");
                objXmlAttribute.InnerText = objEntityXAddressInfo.Address.Country.ToString();
                objXmlElement.Attributes.Append(objXmlAttribute);
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlAttribute = null;
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("County");
                objXmlElement.InnerText = objEntityXAddressInfo.Address.County;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;


                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("ZipCode");
                objXmlElement.InnerText = objEntityXAddressInfo.Address.ZipCode;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;
				//RMA-8753 nshah28(Added by ashish) END

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Email");
                objXmlElement.InnerText = objEntityXAddressInfo.Email;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Fax");
                objXmlElement.InnerText = objEntityXAddressInfo.Fax;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("PrimaryAddress");
                int iPrimaryAddress = Convert.ToInt32(objEntityXAddressInfo.PrimaryAddress);
                if (iPrimaryAddress == -1)
                    objXmlElement.InnerText = "True";
                else
                    objXmlElement.InnerText = "False";
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                //JIRA:6865 START:
                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("EffectiveDate");
                objXmlElement.InnerText = Conversion.GetDBDateFormat(objEntityXAddressInfo.EffectiveDate, "MM/dd/yyyy");
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("ExpirationDate");
                objXmlElement.InnerText = Conversion.GetDBDateFormat(objEntityXAddressInfo.ExpirationDate, "MM/dd/yyyy");
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;
                //JIRA:6865 END:

                /************** Hidden Columns on the Grid ******************/
                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("NewPrimaryRecord");//for identication of changed Primary Address
                objXmlElement.InnerText = "False";
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("AddressId");
                objXmlElement.InnerText = objEntityXAddressInfo.AddressId.ToString();
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;
                //RMA-8753 nshah28 start
                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("RowId");
                objXmlElement.InnerText = objEntityXAddressInfo.RowId.ToString();
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;
                //RMA-8753 nshah28 end

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("EntityId");
                objXmlElement.InnerText = objEntityXAddressInfo.EntityId.ToString();
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;


                //XmlDocument objAddressXPhoneInfoXmlDoc = GetAddressXPhoneInfo(objEntityXAddressInfo);
                //objOptionXmlElement.AppendChild(objEntityXAddressInfoXmlDoc.ImportNode(objAddressXPhoneInfoXmlDoc.SelectSingleNode("AddressXPhoneInfo"), true));

                objRootElement.AppendChild(objOptionXmlElement);
                objOptionXmlElement = null;


            }

            iEntityXAddressInfoCount++;

            objOptionXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("option");

            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("ref");
            objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH
                + "/" + objEntityXAddressInfoXmlDoc.DocumentElement.LocalName
                + "/option[" + iEntityXAddressInfoCount.ToString() + "]";
            objOptionXmlElement.Attributes.Append(objXmlAttribute);
            objXmlAttribute = null;

            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("type");
            objXmlAttribute.InnerText = "new";
            objOptionXmlElement.Attributes.Append(objXmlAttribute);
            objXmlAttribute = null;

            //MITS:34276 Entity Address Type START
            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("AddressType");
            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("codeid");
            objXmlAttribute.InnerText = "-1";
            objXmlElement.Attributes.Append(objXmlAttribute);
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlAttribute = null;
            objXmlElement = null;
            //MITS:34276 Entity Address Type END

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr1");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr2");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr3");// JIRA 6420 pkandhari
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr4");// JIRA 6420 pkandhari
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("City");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("State");
            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("codeid");
            objXmlAttribute.InnerText = "-1";
            objXmlElement.Attributes.Append(objXmlAttribute);
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlAttribute = null;
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Country");
            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("codeid");
            objXmlAttribute.InnerText = "-1";
            objXmlElement.Attributes.Append(objXmlAttribute);
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlAttribute = null;
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("County");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("ZipCode");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Email");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Fax");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("PrimaryAddress");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            //JIRA:6865 START:
            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("EffectiveDate");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("ExpirationDate");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;
            //JIRA:6865 END:

            /************** Hidden Columns on the Grid ******************/
            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("NewPrimaryRecord"); //for identication of changed Primary Address
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("AddressId");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            //RMA-8753 nshah28 start
            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("RowId");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;
            //RMA-8753 nshah28 end
            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("EntityId");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            #region PhoneInfo

            //            XmlElement objInnerXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("AddressXPhoneInfo");
//            objOptionXmlElement.AppendChild(objInnerXmlElement);

//            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("listhead");
//            string sNode = @"<PhoneCode>Phone Code</PhoneCode>
//                             <PhoneNo>Phone Number</PhoneNo>";
//            objXmlElement.InnerXml = sNode;
//            objInnerXmlElement.AppendChild(objXmlElement);

//            XmlElement objInnerOptionXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("option");

//            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("ref");
//            objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH
//                + "/AddressXPhoneInfo" + "/option[" + iEntityXAddressInfoCount.ToString() + "]";
//            objInnerOptionXmlElement.Attributes.Append(objXmlAttribute);
//            objXmlAttribute = null;

//            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("type");
//            objXmlAttribute.InnerText = "new";
//            objInnerOptionXmlElement.Attributes.Append(objXmlAttribute);
//            objXmlAttribute = null;

//            objInnerXmlElement.AppendChild(objInnerOptionXmlElement);

//            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("PhoneCode");
//            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("codeid");
//            objXmlAttribute.InnerText = "-1";
//            objXmlElement.Attributes.Append(objXmlAttribute);
//            objInnerOptionXmlElement.AppendChild(objXmlElement);
//            objXmlAttribute = null;
//            objXmlElement = null;

//            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("PhoneNo");
//            objInnerOptionXmlElement.AppendChild(objXmlElement);
//            objXmlElement = null;

//            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("ContactId");
//            objInnerOptionXmlElement.AppendChild(objXmlElement);
//            objXmlElement = null;

//            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("PhoneId");
//            objInnerOptionXmlElement.AppendChild(objXmlElement);
            //            objXmlElement = null;

            #endregion //PhoneInfo

            objRootElement.AppendChild(objOptionXmlElement);
            objOptionXmlElement = null;

            objRootElement = null;
            return objEntityXAddressInfoXmlDoc;
        }
        private void SetPrimaryAddressReadOnly()
        {

            base.AddReadOnlyNode("addr1");
            base.AddReadOnlyNode("addr2");
            base.AddReadOnlyNode("addr3");// JIRA 6420 pkandhari
            base.AddReadOnlyNode("addr4");// JIRA 6420 pkandhari
            base.AddReadOnlyNode("city");
            base.AddReadOnlyNode("countrycode");
            base.AddReadOnlyNode("county");
            base.AddReadOnlyNode("stateid");
            base.AddReadOnlyNode("emailaddress");
            base.AddReadOnlyNode("faxnumber");
            base.AddReadOnlyNode("zipcode");

        }
        private string PopulatePhoneNumbers()
        {

            XmlDocument SysExDoc = new XmlDocument();
            string sNumber = string.Empty;
            foreach (AddressXPhoneInfo objPhoneInfo in Employee.EmployeeEntity.AddressXPhoneInfoList)
            {
                //Added to set Default types to office and home
                if (objPhoneInfo.PhoneCode == iOfficeType)
                {
                    sOfficePhone = objPhoneInfo.PhoneNo;

                }
                else if (objPhoneInfo.PhoneCode == iHomeType)
                {
                    sHomePhone = objPhoneInfo.PhoneNo;
                }
                //Added to set Default types to office and home
                if (String.IsNullOrEmpty(sNumber))
                {
                    sNumber = objPhoneInfo.PhoneId + CommonForm.INNER_PHONE_DELIMITER + objPhoneInfo.PhoneCode + CommonForm.INNER_PHONE_DELIMITER + objPhoneInfo.PhoneNo;
                }
                else
                {
                    sNumber += CommonForm.OUTER_PHONE_DELIMITER + objPhoneInfo.PhoneId + CommonForm.INNER_PHONE_DELIMITER + objPhoneInfo.PhoneCode + CommonForm.INNER_PHONE_DELIMITER + objPhoneInfo.PhoneNo;
                }
                XmlNode objNode = base.SysEx.SelectSingleNode("//PhoneTypeList");
                if (objNode != null)
                {
                    foreach (XmlNode node in objNode.ChildNodes)
                    {
                        if (objPhoneInfo.PhoneCode.ToString() == node.Attributes["value"].Value)
                        {
                            node.InnerText += " " + CommonForm.MARK_PHONECODE;
                            break;
                        }
                    }
                }

            }

            return sNumber;
        }
        #region GetAddressXPhoneInfo
        //private XmlDocument GetAddressXPhoneInfo(EntityXAddresses objEntityXAddresses)
        //{
        //    XmlDocument objAddressXPhoneInfoXmlDoc = new XmlDocument();

        //    XmlElement objRootElement = objAddressXPhoneInfoXmlDoc.CreateElement("AddressXPhoneInfo");
        //    objAddressXPhoneInfoXmlDoc.AppendChild(objRootElement);

        //    XmlElement objOptionXmlElement = null;
        //    XmlElement objXmlElement = null;
        //    XmlElement objListHeadXmlElement = null;
        //    XmlAttribute objXmlAttribute = null;
        //    int iAddressXPhoneInfoCount = 0;
        //    string sPhoneCode = "";
        //    string sPhoneDesc = "";



        //    objListHeadXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("listhead");


        //    objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneCode");
        //    objXmlElement.InnerText = "Phone Code";
        //    objListHeadXmlElement.AppendChild(objXmlElement);
        //    objXmlElement = null;

        //    objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneNo");
        //    objXmlElement.InnerText = "Phone Number";
        //    objListHeadXmlElement.AppendChild(objXmlElement);
        //    objXmlElement = null;

        //    objRootElement.AppendChild(objListHeadXmlElement);
        //    objListHeadXmlElement = null;


        //    foreach (AddressXPhoneInfo objAddressXPhoneInfo in objEntityXAddresses.AddressXPhoneInfoList)
        //    {

        //        iAddressXPhoneInfoCount++;

        //        objOptionXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("option");

        //        objXmlAttribute = objAddressXPhoneInfoXmlDoc.CreateAttribute("ref");
        //        objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH
        //            + "/" + objAddressXPhoneInfoXmlDoc.DocumentElement.LocalName
        //            + "/option[" + iAddressXPhoneInfoCount.ToString() + "]";
        //        objOptionXmlElement.Attributes.Append(objXmlAttribute);
        //        objXmlAttribute = null;



        //        objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneCode");
        //        this.objCache.GetCodeInfo(objAddressXPhoneInfo.PhoneCode, ref sPhoneCode, ref sPhoneDesc);
        //        objXmlElement.InnerText = sPhoneCode + " " + sPhoneDesc;
        //        objXmlAttribute = objAddressXPhoneInfoXmlDoc.CreateAttribute("codeid");
        //        objXmlAttribute.InnerText = objAddressXPhoneInfo.PhoneCode.ToString();
        //        objXmlElement.Attributes.Append(objXmlAttribute);
        //        objOptionXmlElement.AppendChild(objXmlElement);
        //        objXmlAttribute = null;
        //        objXmlElement = null;


        //        objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneNo");
        //        objXmlElement.InnerText = objAddressXPhoneInfo.PhoneNo;
        //        objOptionXmlElement.AppendChild(objXmlElement);
        //        objXmlElement = null;


        //        /************** Hidden Columns on the Grid ******************/
        //        objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("ContactId");
        //        objXmlElement.InnerText = objAddressXPhoneInfo.ContactId.ToString();
        //        objOptionXmlElement.AppendChild(objXmlElement);
        //        objXmlElement = null;

        //        objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneId");
        //        objXmlElement.InnerText = objAddressXPhoneInfo.PhoneId.ToString();
        //        objOptionXmlElement.AppendChild(objXmlElement);
        //        objXmlElement = null;

        //        objRootElement.AppendChild(objOptionXmlElement);
        //        objOptionXmlElement = null;

        //    }

        //    iAddressXPhoneInfoCount++;

        //    objOptionXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("option");

        //    objXmlAttribute = objAddressXPhoneInfoXmlDoc.CreateAttribute("ref");
        //    objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH
        //        + "/" + objAddressXPhoneInfoXmlDoc.DocumentElement.LocalName
        //        + "/option[" + iAddressXPhoneInfoCount.ToString() + "]";
        //    objOptionXmlElement.Attributes.Append(objXmlAttribute);
        //    objXmlAttribute = null;

        //    objXmlAttribute = objAddressXPhoneInfoXmlDoc.CreateAttribute("type");
        //    objXmlAttribute.InnerText = "new";
        //    objOptionXmlElement.Attributes.Append(objXmlAttribute);
        //    objXmlAttribute = null;


        //    objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneCode");
        //    objXmlAttribute = objAddressXPhoneInfoXmlDoc.CreateAttribute("codeid");
        //    objXmlAttribute.InnerText = "-1";
        //    objXmlElement.Attributes.Append(objXmlAttribute);
        //    objOptionXmlElement.AppendChild(objXmlElement);
        //    objXmlAttribute = null;
        //    objXmlElement = null;


        //    objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneNo");
        //    objOptionXmlElement.AppendChild(objXmlElement);
        //    objXmlElement = null;

        //    /************** Hidden Columns on the Grid ******************/
        //    objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("ContactId");
        //    objOptionXmlElement.AppendChild(objXmlElement);
        //    objXmlElement = null;

        //    objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneId");
        //    objOptionXmlElement.AppendChild(objXmlElement);
        //    objXmlElement = null;

        //    objRootElement.AppendChild(objOptionXmlElement);
        //    objOptionXmlElement = null;

        //    objRootElement = null;
        //    return objAddressXPhoneInfoXmlDoc;
        //}
        #endregion //GetAddressXPhoneInfo

        //MITS:34276 Starts-- Entity ID Type tab display
        private XmlDocument GetEntityIdTypeInfoData()
        {
            XmlDocument objEntityIDTypeAsXmlDoc = new XmlDocument();

            XmlElement objRootElement = objEntityIDTypeAsXmlDoc.CreateElement("EntityXEntityIDType");
            //MITS #34276 : Starts Entity ID Type Permission starts
            XmlAttribute objXmlPermissionAttribute = null;
            if (!Employee.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_IDTYPE_VIEW))
            {

                objXmlPermissionAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsViewable");
                objXmlPermissionAttribute.InnerText = "false";
                objRootElement.Attributes.Append(objXmlPermissionAttribute);
                objXmlPermissionAttribute = null;
                objEntityIDTypeAsXmlDoc.AppendChild(objRootElement);
            }
            else
            {
                if (!Employee.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_IDTYPE_NEW))
                {
                    objXmlPermissionAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsNew");
                    objXmlPermissionAttribute.InnerText = "false";
                    objRootElement.Attributes.Append(objXmlPermissionAttribute);
                    objXmlPermissionAttribute = null;
                }
                objEntityIDTypeAsXmlDoc.AppendChild(objRootElement);

                if (!Employee.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_IDTYPE_EDIT))
                {
                    objXmlPermissionAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsUpdate");
                    objXmlPermissionAttribute.InnerText = "false";
                    objRootElement.Attributes.Append(objXmlPermissionAttribute);
                    objXmlPermissionAttribute = null;
                }
                objEntityIDTypeAsXmlDoc.AppendChild(objRootElement);

                if (!Employee.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_IDTYPE_DELETE))
                {
                    objXmlPermissionAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsDelete");
                    objXmlPermissionAttribute.InnerText = "false";
                    objRootElement.Attributes.Append(objXmlPermissionAttribute);
                    objXmlPermissionAttribute = null;
                }
                objEntityIDTypeAsXmlDoc.AppendChild(objRootElement);

                if (!Employee.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_VENDORTYPE_NEW))
                {
                    objXmlPermissionAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsVendorNew");
                    objXmlPermissionAttribute.InnerText = "false";
                    objRootElement.Attributes.Append(objXmlPermissionAttribute);
                    objXmlPermissionAttribute = null;
                }
                objEntityIDTypeAsXmlDoc.AppendChild(objRootElement);
            }
            //MITS #34276 : Starts  Entity ID Type Permission Ends 

            XmlElement objOptionXmlElement = null;
            XmlElement objXmlElement = null;
            XmlElement objListHeadXmlElement = null;
            XmlAttribute objXmlAttribute = null;
            int iEntityIDTypeAsCount = 0;
            string sIDCode = string.Empty;
            string sIDDesc = string.Empty;

            objListHeadXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("listhead");

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdType");
            objXmlElement.InnerText = "Entity ID Type";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdNum");
            objXmlElement.InnerText = "Entity ID Number";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EffStartDate");
            objXmlElement.InnerText = "Effective Date";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EffEndDate");
            objXmlElement.InnerText = "Expiration Date";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objRootElement.AppendChild(objListHeadXmlElement);
            objListHeadXmlElement = null;

            foreach (EntityXEntityIDType objEntityXEntityIDType in this.Employee.EmployeeEntity.EntityXEntityIDTypeList)
            {
                iEntityIDTypeAsCount++;

                objOptionXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("option");

                objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("ref");
                objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH + "/" + objEntityIDTypeAsXmlDoc.DocumentElement.LocalName + "/option[" + iEntityIDTypeAsCount.ToString() + "]";
                objOptionXmlElement.Attributes.Append(objXmlAttribute);
                objXmlAttribute = null;

                //MITS #34276 : Starts  ID Type Vendor Permission Starts 
                this.objCache.GetCodeInfo(objEntityXEntityIDType.IdType, ref sIDCode, ref sIDDesc);                
                if (sIDCode.Trim().ToUpper() == "VENDOR")
                {
                    if (!Employee.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_VENDORTYPE_VIEW))
                    {
                        objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsVendorView");
                        objXmlAttribute.InnerText = "false";
                        objOptionXmlElement.Attributes.Append(objXmlAttribute);
                        objXmlAttribute = null;
                    }                   
                    if (!Employee.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_VENDORTYPE_UPDATE))
                    {
                        objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsVendorUpdate");
                        objXmlAttribute.InnerText = "false";
                        objOptionXmlElement.Attributes.Append(objXmlAttribute);
                        objXmlAttribute = null;
                    }
                    if (!Employee.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_VENDORTYPE_DELETE))
                    {
                        objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsVendorDelete");
                        objXmlAttribute.InnerText = "false";
                        objOptionXmlElement.Attributes.Append(objXmlAttribute);
                        objXmlAttribute = null;
                    }
                }
                //MITS #34276 : Starts ID Type Vendor Permission ends

                objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdType");
                this.objCache.GetCodeInfo(objEntityXEntityIDType.IdType, ref sIDCode, ref sIDDesc);
                objXmlElement.InnerText = sIDCode + " " + sIDDesc;
                objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("codeid");
                objXmlAttribute.InnerText = objEntityXEntityIDType.IdType.ToString();
                objXmlElement.Attributes.Append(objXmlAttribute);
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlAttribute = null;
                objXmlElement = null;

                objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdNum");
                objXmlElement.InnerText = objEntityXEntityIDType.IdNum.ToString();
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EffStartDate");
                objXmlElement.InnerText = Conversion.GetDBDateFormat(objEntityXEntityIDType.EffStartDate, "MM/dd/yyyy");
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EffEndDate");
                objXmlElement.InnerText = Conversion.GetDBDateFormat(objEntityXEntityIDType.EffEndDate, "MM/dd/yyyy");
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                /************** Hidden Columns on the Grid ******************/
                objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdNumRowId");
                objXmlElement.InnerText = objEntityXEntityIDType.IdNumRowId.ToString();
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EntityId");
                objXmlElement.InnerText = objEntityXEntityIDType.EntityId.ToString();
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;


                objRootElement.AppendChild(objOptionXmlElement);
                objOptionXmlElement = null;

            }

            iEntityIDTypeAsCount++;

            objOptionXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("option");

            objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("ref");
            objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH
                + "/" + objEntityIDTypeAsXmlDoc.DocumentElement.LocalName
                + "/option[" + iEntityIDTypeAsCount.ToString() + "]";
            objOptionXmlElement.Attributes.Append(objXmlAttribute);
            objXmlAttribute = null;

            objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("type");
            objXmlAttribute.InnerText = "new";
            objOptionXmlElement.Attributes.Append(objXmlAttribute);
            objXmlAttribute = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdType");
            objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("codeid");
            objXmlAttribute.InnerText = "-1";
            objXmlElement.Attributes.Append(objXmlAttribute);
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlAttribute = null;
            objXmlElement = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdNum");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EffStartDate");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EffEndDate");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;


            /************** Hidden Columns on the Grid ******************/
            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdNumRowId");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EntityId");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objRootElement.AppendChild(objOptionXmlElement);
            objOptionXmlElement = null;

            objRootElement = null;
            return objEntityIDTypeAsXmlDoc;

        }
        //MITS:34276 Ends
    }
}
