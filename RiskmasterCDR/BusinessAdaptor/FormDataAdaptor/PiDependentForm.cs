using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;


namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for PiDependent Screen.
	/// </summary>
	public class PiDependentForm : DataEntryFormBase
	{
		const string CLASS_NAME = "PiXDependent";
		private PiXDependent PiXDependent{get{return objData as PiXDependent;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		const string FILTER_KEY_NAME = "PiRowId";
		public override void InitNew()
		{
			base.InitNew(); 
			
			this.m_ParentId = base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME,false);
			(objData as INavigation).Filter = 
				objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + this.m_ParentId.ToString();		
			this.PiXDependent.PiRowId = this.m_ParentId;			
		}

		public PiDependentForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}
		
		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
			base.OnUpdateForm ();

			// Subtitle node should already be present in SysExData
			// as this screen always opens from PiEmployee
            //Added for Mits 19315:The person involved age is being calculated off the current date, and not the date of event.  
            Event objEvent = (Event)this.PiXDependent.Context.Factory.GetDataModelObject("Event", false);
            string sEventDateFromDB = string.Empty;
            string sLastName=string.Empty;
            if (objEvent != null)
            {
                int iEventId = 0;
                XmlNode objEventId = base.SysEx.SelectSingleNode("//EventId");
                if (objEventId != null)
                {
                    iEventId = Convert.ToInt32(objEventId.InnerText);
                }
                //Added for Mits 20577-On creating and deleting  a new Dependent, throws  out  error 
                else //for New case(toolbar button) where we don't get any info in the SysEx node other than the pirowid
                {
                    XmlNode objRowId = base.SysEx.SelectSingleNode("//PiRowId");
                    if (objRowId != null)
                    {
                        string sRowId = objRowId.InnerText;
                        if(!String.IsNullOrEmpty(sRowId))
                        {
                            string sSql = @"SELECT PI.EVENT_ID,ET.LAST_NAME FROM PERSON_INVOLVED PI,ENTITY ET
                                            WHERE PI.PI_EID=ET.ENTITY_ID
                                            AND PI.PI_ROW_ID=" + sRowId ;
                            using (DbReader objReader = DbFactory.GetDbReader(PiXDependent.Context.DbConn.ConnectionString, sSql))
                            {
                                if (objReader.Read())
                                {
                                    iEventId = objReader.GetInt32("EVENT_ID");
                                    sLastName = objReader.GetString("LAST_NAME");
                                }
                            }
                        }
                    }
                }
                //Added for Mits 20577-On creating and deleting  a new Dependent, throws  out  error 
                if (iEventId != 0)
                {
                    objEvent.MoveTo(iEventId);
                    sEventDateFromDB = objEvent.DateOfEvent;
                    if (!String.IsNullOrEmpty(sEventDateFromDB))
                    {
                        if (base.SysEx.SelectSingleNode("DateOfEvent") == null)
                        {
                            string sUIEventDate = string.Empty;
                            sUIEventDate = Conversion.ToDate(sEventDateFromDB).ToString("MM/dd/yyyy");
                            base.CreateSysExData("DateOfEvent", sUIEventDate);
                        }
                    }
                }
            }
            //Added for Mits 19315:The person involved age is being calculated off the current date, and not the date of event.
			if (PiXDependent.DependentEntity.BirthDate!=string.Empty)
                base.ResetSysExData("DependentAge", Utilities.CalculateAgeInYears(PiXDependent.DependentEntity.BirthDate, sEventDateFromDB)); //Added sEventDateFromDB for MITS 19315
			else
				base.ResetSysExData("DependentAge","");

			if(objData.IsNew)
			{
                //Rakhi-Modified as Last Name doesn't come as filled when the New button in the toolbar is selected.GetSysExDataNodeText returns "" when the node doesn't exists.
                //and in the New case(toolbar button) we don't get any info in the SysEx node other than the pirowid
                
                //if(base.GetSysExDataNodeText("/SysExData/LastName")!=null) 
                XmlNode objLastName = base.SysEx.SelectSingleNode("/SysExData/LastName"); 
                if (objLastName!= null)
				{
					//PiXDependent.DependentEntity.LastName =  base.GetSysExDataNodeText("/SysExData/LastName");   
                    PiXDependent.DependentEntity.LastName = objLastName.InnerText;   
				}
                else
                {
                    PiXDependent.DependentEntity.LastName = sLastName;
                }
			}
            //smishra25: MITS 24395, MITS 13748 implementation was missing
            //nadim for 13748 hide /unhide ssn field
            if (!PiXDependent.Context.RMUser.IsAllowedEx(m_SecurityId, FormBase.RMO_VIEW_SSN))
            {
                base.AddKillNode("depntaxid");
            }
            //nadim for 13748 hide /unhide ssn field

			//Check for People Maintenance permission
			PeoplePermissionChecks4Other("pidependent", m_SecurityId+RMO_UPDATE);
		}

        //Added by pmittal5 Mits 13632 03/23/09 
        public override void BeforeApplyLookupData(ref bool Cancel, ArrayList arrToSaveFields)
        {
            arrToSaveFields.Add("EntityTableId");
        }

		public override void OnValidate(ref bool Cancel)
		{
			bool bError = false;

			// Perform data validation
			string sToday = Conversion.ToDbDate(System.DateTime.Now);
			string sTime = Conversion.GetTime(System.DateTime.Now.ToShortTimeString());
			
			if(PiXDependent.DependentEntity.BirthDate!="")
			{
				if(PiXDependent.DependentEntity.BirthDate.CompareTo(sToday)>0)
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.BirthDate", base.ClientId), System.DateTime.Now.ToShortDateString()),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}
			}
			
			// Return true if there were validation errors
			Cancel = bError;
		}

		/*public override void BeforeSave(ref bool Cancel)
		{
			XmlDocument objSysExDataXmlDoc = base.SysEx;
			XmlNode objPiRowIdNode = null;
			try{objPiRowIdNode = objSysExDataXmlDoc.SelectSingleNode("/SysExData/PiRowId");}
			catch{};
			
			if(objPiRowIdNode !=null)
				if(this.PiXDependent.PiRowId==0)
					this.PiXDependent.PiRowId = Conversion.ConvertStrToInteger(objPiRowIdNode.InnerText);

			objPiRowIdNode = null;
			objSysExDataXmlDoc = null;

			base.BeforeSave (ref Cancel);		

		}*/
	}
}