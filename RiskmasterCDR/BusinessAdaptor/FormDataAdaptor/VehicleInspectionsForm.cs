using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// VehicleInspections Screen.
	/// </summary>
	public class VehicleInspectionsForm : DataEntryFormBase
	{
		const string CLASS_NAME = "VehicleXInspct";
		const string FILTER_KEY_NAME = "UnitId";

		private VehicleXInspct VehicleInspct{get{return objData as VehicleXInspct;}}

		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		
		/// <summary>
		/// Class constructor which initializes Class Name information
		/// </summary>
		/// <param name="fda"></param>
		public VehicleInspectionsForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

		/// <summary>
		/// Initializes the class and sets the parent ID for the class
		/// that can be used for parent-child navigation
		/// </summary>
		public override void InitNew()
		{
			base.InitNew();

			//set filter for UnitId
			VehicleInspct.UnitId=base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME,false);
			if (VehicleInspct.UnitId>0)
				(objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + VehicleInspct.UnitId;

		}//end method InitNew()

//		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
//		public override void OnUpdateForm()
//		{
//			base.OnUpdateForm ();
//
//			if(VehicleInspct.UnitId==0)
//			{
//				AfterAddNew();
//			}
//		}
//
//		public override void BeforeSave(ref bool Cancel)
//		{
//			//If we were sent here from a parent(event) then we'll have an eventid in the SysEx
//			//originally from ScreenFlow\SysExData.
//			XmlDocument objXML = base.SysEx;
//			XmlNode objUnitId=null;
//			try{objUnitId = objXML.GetElementsByTagName("UnitId")[0];}
//			catch{};
//			
//			//Filter by this EventId if Present.
//			if(objUnitId !=null)
//				if(this.VehicleInspct.UnitId ==0)
//					this.VehicleInspct.UnitId = Conversion.ConvertStrToInteger(objUnitId.InnerText);
//
//			  
//			base.BeforeSave (ref Cancel);
//
//		}
//
//		public override void AfterAddNew()
//		{
//			base.AfterAddNew ();
//
//			XmlDocument objXML = base.SysEx;
//			XmlNode objUnitId=null;
//			try{objUnitId = objXML.GetElementsByTagName(FILTER_KEY_NAME)[0];}
//			catch{};
//			
//			//Filter by this staffeid if Present.
//			if(objUnitId !=null)
//			{
//				VehicleInspct.UnitId=Conversion.ConvertStrToInteger(objUnitId.InnerText);
//			}
//		}
        //Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
        //akaur9 MITS 25163- Policy Interface Implementation --start
        public override void OnUpdateForm()
        {
            base.OnUpdateForm();            
            int iSystemPolicyId = base.GetSysExDataNodeInt("//SystemPolicyId");

            if (iSystemPolicyId > 0)
            {
                base.AddReadOnlyNode("readOnlyPage");
                base.AddKillNode("new");
                base.AddKillNode("save");
                base.AddKillNode("delete");
            }         
        }
        //akaur9 MITS 25163- Policy Interface Implementation --End
	}
}