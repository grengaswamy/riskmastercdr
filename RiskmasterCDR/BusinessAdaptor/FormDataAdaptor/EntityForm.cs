using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

//TODO: Auto-Generated Form - migrate and verify behaviors from Legacy RMNet form.

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for Entity Screen.
	/// </summary>
	public class EntityForm : DataEntryFormBase
	{
		const string CLASS_NAME = "Entity";
		private Entity Entity{get{return objData as Entity;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}

		public EntityForm(FormDataAdaptor fda):base(fda)
		{
			this.objData = fda.Factory.GetDataModelObject(CLASS_NAME,false) as DataObject;
		}
		
		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
			base.OnUpdateForm ();
		}
	}
}