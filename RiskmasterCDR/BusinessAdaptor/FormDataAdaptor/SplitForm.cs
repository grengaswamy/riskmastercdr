using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Split Screen.
	/// </summary>
	public class SplitForm : DataEntryFormBase
	{
		const string CLASS_NAME = "FundsTransSplit";
		const string FILTER_KEY_NAME = "TransId";

		private FundsTransSplit FundsTransSplit{get{return objData as FundsTransSplit;}}

		private LocalCache objCache{get{return objData.Context.LocalCache;}}
	
		public SplitForm(FormDataAdaptor fda):base(fda)
		{
			this.m_ClassName = CLASS_NAME;
		}

		public override void InitNew()
		{
			base.InitNew();

			//set filter for TransId
			FundsTransSplit.TransId=base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME,false);
			if (FundsTransSplit.TransId>0)
				(objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + FundsTransSplit.TransId;
		}
	}
}