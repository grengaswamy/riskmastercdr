﻿using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Litigation Screen.
	/// </summary>
	public class LitigationForm : DataEntryFormBase
	{
		const string CLASS_NAME = "ClaimXLitigation";
		const string FILTER_KEY_NAME = "ClaimId";

		private ClaimXLitigation objClaimXLitigation{get{return objData as ClaimXLitigation;}}

		private LocalCache objCache{get{return objData.Context.LocalCache;}}

        private string m_Caption = string.Empty;
            //nnorouzi; MITS 19478
        public override string GetCaption()
        {
            return base.GetCaption();
        }
	
		public LitigationForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

		public override void InitNew()
		{
			base.InitNew();

			//set filter for ClaimId
			objClaimXLitigation.ClaimId=base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME,false);
			if (objClaimXLitigation.ClaimId>0)
				(objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + objClaimXLitigation.ClaimId;

		}

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
            ArrayList singleRow = null;
            int iUseClaimProgressNotes = 0;
			base.OnUpdateForm ();

            //MGaba2:MITS 16498:Start
            //Module Security Permission was not working as security id was different each time defendant 
            //is opened from different claim.

            Claim objClaim = (Claim)objClaimXLitigation.Context.Factory.GetDataModelObject("Claim", false);
            objClaim.MoveTo(objClaimXLitigation.ClaimId);

            switch (objClaim.LineOfBusCode)
            {
                case 241:
                    m_SecurityId = RMO_GC_LITIGATION;
                    break;
                case 242:
                    m_SecurityId = RMO_VA_LITIGATION;
                    break;
                case 243:
                    m_SecurityId = RMO_WC_LITIGATION;
                    break;
                case 844:
                    m_SecurityId = RMO_DI_LITIGATION;
                    break;
                //Start-Mridul Bansal. 01/04/10. MITS#19314. Support for Property Claims.
                case 845:
                    m_SecurityId = RMO_PC_LITIGATION;
                    break;
                //End-Mridul Bansal. 01/04/10. MITS#19314. Support for Property Claims.
            }

            //Setting Security Id for further permissions like attachment
            XmlElement objSysSid = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysSid");
            objSysSid.InnerText = m_SecurityId.ToString();

            //Create new Permission wasnt working
            if (objClaimXLitigation.LitigationRowId == 0)
            {
                if (!objClaimXLitigation.Context.RMUser.IsAllowedEx(m_SecurityId, RMO_CREATE))
                {
                    m_CurrentAction = enumFormActionType.None;
                    LogSecurityError(RMO_CREATE);
                    return;
                }
            }
            else
            {//View Permission wasnt working
                if (!objClaimXLitigation.Context.RMUser.IsAllowedEx(m_SecurityId, RMO_VIEW))
                {
                    m_CurrentAction = enumFormActionType.None;
                    LogSecurityError(RMO_VIEW);
                    return;
                }
            }

            //MGaba2:MITS 16498:End

            //Start by Shivendu for MITS 9467
            if (SysEx.DocumentElement.SelectSingleNode("LINEOFBUSCODE") == null)
            {
                CreateSysExData("LINEOFBUSCODE");
                base.ResetSysExData("LINEOFBUSCODE", (objClaimXLitigation.Parent as Claim).LineOfBusCode.ToString());
            }
            //End by Shivendu for MITS 9467


            //mgaba2:mits 29116
            iUseClaimProgressNotes = objClaim.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode].ClmProgressNotesFlag;
            if (iUseClaimProgressNotes == 0)
            {
                base.AddKillNode("enhancednotes");
            }

			
			// atavaragiri :MITS 28458:setting the department id to be reflected in the query string //
			 base.ResetSysExData("ev_depteid_cid", (objClaim.Parent as Event).DeptEid.ToString());
			// end:MITS 28458 //

            //nnorouzi; MITS 19478			
            m_Caption = this.GetCaption();

            //Pass this subtitle value to view (ref'ed from @valuepath).
            base.ResetSysExData("SubTitle", m_Caption);
            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singleRow, "id", "formsubtitle");
            base.AddElementToList(ref singleRow, "Text", m_Caption);
            base.m_ModifiedControls.Add(singleRow);


            //Added by Amitosh for Mits 23441 01/20/2011
			//Check for People Maintenance permission
			//PeoplePermissionChecks4Other("litigation", m_SecurityId+RMO_UPDATE);

            if (!Adaptor.userLogin.IsAllowedEx(RMO_PEOPLE + RMO_UPDATE))
            {
                base.AddReadOnlyFieldMarkNode("litatt");
             }
           //end Amitosh

			//JAP - LSS changes - lock down form as read-only if LSS interface created the record
			if (objClaimXLitigation.AddedByUser == "LSSINF")
			{
                //gagnihotri MITS 16453 05/12/2009
                //XmlElement xmlFormElement = (XmlElement)base.SysView.SelectSingleNode("//form");
                //if (xmlFormElement != null) xmlFormElement.SetAttribute("readonly", "1");
                base.ResetSysExData("FormReadOnly", "Disable");
            }
             else
                base.ResetSysExData("FormReadOnly", "Enable");


            //MITS 17107
            int iAttFlag = 0;
            iAttFlag = Conversion.ConvertObjToInt(objClaimXLitigation.Context.DbConn.ExecuteInt(
                "SELECT ATTACHMENTS_FLAG FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = 'CLAIM_X_LITIGATION'"), base.ClientId);

            if (SysEx.DocumentElement.SelectSingleNode("AttachDocFlag") == null) CreateSysExData("AttachDocFlag");                
            
            if (iAttFlag == 1) base.ResetSysExData("AttachDocFlag", "true");
            else base.ResetSysExData("AttachDocFlag", "false");
            //nsachdeva2 - MITS: 27077 - 6/5/2012
            CreateSysExData("UseLegacyComments", Riskmaster.Common.Conversion.ConvertObjToStr(objClaimXLitigation.Context.InternalSettings.SysSettings.UseLegacyComments));
            //End MITS: 27077
		}

		//validate data according to the Business Rules
		public override void OnValidate(ref bool Cancel)
		{
			bool bError = false;
			
			// Perform data validation
			string sToday = Conversion.ToDbDate(System.DateTime.Now);
			Claim objClaim = (Claim) this.objClaimXLitigation.Context.Factory.GetDataModelObject("Claim",false);    
			objClaim.MoveTo(objClaimXLitigation.ClaimId);
			if (objClaimXLitigation.SuitDate!="" && objClaimXLitigation.SuitDate.CompareTo(sToday)>0)
			{
				Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                    String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.SuitDate", base.ClientId), System.DateTime.Now.ToShortDateString()),
					BusinessAdaptorErrorType.Error);

				bError = true;
			}
			
			//Suit date could be earlier than claim date.
			/*if(objClaimXLitigation.SuitDate!="" && objClaimXLitigation.SuitDate.CompareTo(objClaim.DateOfClaim)<0)
			{
				Errors.Add(Globalization.GetString("ValidationError"),
					String.Format(Globalization.GetString("Validation.MustBeLessThanClaimDate"), Globalization.GetString("Field.SuitDate"),Conversion.ToDate(objClaim.DateOfClaim).ToShortDateString()),
					BusinessAdaptorErrorType.Error);

				bError = true;
			}*/

			if (objClaimXLitigation.CourtDate!="" && objClaimXLitigation.CourtDate.CompareTo(objClaimXLitigation.SuitDate)<0)
			{
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                    String.Format(Globalization.GetString("Validation.MustBeGreaterThanSuitDate", base.ClientId), Globalization.GetString("Field.CourtDate", base.ClientId), Conversion.ToDate(objClaimXLitigation.SuitDate).ToShortDateString()),
					BusinessAdaptorErrorType.Error);

				bError = true;
			}

			// Return true if there were validation errors
			Cancel = bError;
		}

		public override void BeforeApplyLookupData(ref bool Cancel, ArrayList arrToSaveFields)
		{
			arrToSaveFields.Add("EntityTableId");
		}
	}
}
