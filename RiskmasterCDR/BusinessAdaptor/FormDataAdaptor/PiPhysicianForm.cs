﻿
using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using Riskmaster.Settings;
using System.Text;

//TODO: Auto-Generated Form - migrate and verify behaviors from Legacy RMNet form.

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for PiPhysician Screen.
	/// </summary>
	public class PiPhysicianForm : DataEntryFormBase
	{
		const string CLASS_NAME = "PiPhysician";
		private PiPhysician PiPhysician{get{return objData as PiPhysician;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		const string FILTER_KEY_NAME = "EventId";
		private int m_iEventId = 0;
		private string sConnectionString = null;  // Ishan Mobile Apps

		public override void InitNew()
		{
			base.InitNew(); 

			this.m_iEventId = base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME,false);
			string sFilter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + this.m_iEventId.ToString();
			
			//Nikhil Garg	03/02/2006 
			//Moved from problematic Code below (Not to save before user presses Save button)
			int iPiEid = base.GetSysExDataNodeInt("/SysExData/PiEid",false);
			sFilter += " AND PI_EID=" + iPiEid;

			(objData as INavigation).Filter = sFilter;
			
			this.PiPhysician.EventId = this.m_iEventId;
			this.PiPhysician.PiEid = iPiEid;
            //rsushilaggar JIRA 7767
            if (PiPhysician.Context.InternalSettings.SysSettings.UseEntityRole)
            {
                if (PiPhysician.PiEntity.EntityId <= 0 && PiPhysician.PiEntity.NameType <= 0)
                    PiPhysician.PiEntity.NameType = objCache.GetCodeId("IND", "ENTITY_NAME_TYPE");
            }
		}

//		public override void AfterAddNew()
//		{
//			base.AfterAddNew ();
//
//			int iPiEid = base.GetSysExDataNodeInt("/SysExData/PiEid",false);
// 
//			if(iPiEid>0)
//			{
//				string sSQL = "SELECT PI_ROW_ID FROM PERSON_INVOLVED WHERE EVENT_ID=" + this.m_iEventId + " AND PI_EID=" + iPiEid;
//				int iPiRowId = 0;
//				iPiRowId = this.PiPhysician.Context.DbConnLookup.ExecuteInt(sSQL);
//				if(iPiRowId>0)
//				{
//					// PI Record already exists
//					this.PiPhysician.MoveTo(iPiRowId); 
//				}
//				else
//				{
//					// TODO - Confirm whether this would do the trick. 
//					// PiPatient, PiPhysician and PiMedStaff are different from rest of the bunch.
//					this.PiPhysician.PiEid = iPiEid;
//					this.PiPhysician.Save();  
//				}
//			}
//		}

		public PiPhysicianForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
			sConnectionString = fda.connectionString;  // Ishan Mobile Apps
		}

        //Charanpreet for 12409
        public override void BeforeSave(ref bool Cancel)
        {
            int iPiEid = base.GetSysExDataNodeInt("/SysExData/PiEid", false);

            if (iPiEid > 0 && PiPhysician.IsNew)
            {
                //avipinsrivas Start : Worked for Jira-340
                //string sSQL = "SELECT PI_ROW_ID FROM PERSON_INVOLVED WHERE EVENT_ID=" + this.m_iEventId + " AND PI_EID=" + iPiEid;
                //int iPiRowId = 0;
                //iPiRowId = this.PiPhysician.Context.DbConnLookup.ExecuteInt(sSQL);
                //if (iPiRowId > 0)
                //{
                //    // PI Record already exists	
                //    Cancel = true;
                //    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                //        "Selected Physician already exists as Person Involved.",
                //        BusinessAdaptorErrorType.Error);
                //    return;
                //}
                int iPiErRowID = 0;
                if (this.PiPhysician.Context.InternalSettings.SysSettings.UseEntityRole)
                {
                    int iEntityTableID = this.PiPhysician.Context.LocalCache.GetTableId(Globalization.PersonInvolvedLookupsGlossaryTableNames.PHYSICIANS.ToString());
                    iPiErRowID = this.PiPhysician.PiEntity.IsEntityRoleExists(this.PiPhysician.PiEntity.EntityXRoleList, iEntityTableID);
                }
                if(base.CheckPIEntity(this.m_iEventId, iPiEid, iPiErRowID, Globalization.PersonInvolvedGlossaryTableNames.PHYSICIAN.ToString(),this.PiPhysician.Context.DbConn.ConnectionString, this.PiPhysician.Context.InternalSettings.SysSettings.UseEntityRole))
                {
                    Cancel = true;
                    return;
                }
                //avipinsrivas End
            }
        }
        //Charanpreet for 12409 ends

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
        {
            ArrayList singleRow = null;
			base.OnUpdateForm ();

			XmlDocument objSysExDataXmlDoc = base.SysEx;

			string sSubTitleText = string.Empty; 
			
			// Add EventNumber node, if already not present, to avoid Orbeon 'ref' error 			
			XmlNode objEventNumberNode = objSysExDataXmlDoc.SelectSingleNode("/SysExData/EventNumber");

			if(objEventNumberNode==null)
			{
				base.CreateSysExData("EventNumber"); 
			}
			else
			{
				if(objEventNumberNode.InnerText.Length>0)
					sSubTitleText = " [" + objEventNumberNode.InnerText + "]"; 
				objEventNumberNode = null;
			}
			

			// Add ClaimNumber node, if already not present, to avoid Orbeon 'ref' error
			XmlNode objClaimNumberNode = objSysExDataXmlDoc.SelectSingleNode("/SysExData/ClaimNumber");

			if(objClaimNumberNode==null)
			{
				base.CreateSysExData("ClaimNumber"); 
			}			
			else
			{
				if(objClaimNumberNode.InnerText.Length>0)
					sSubTitleText = " [" + objClaimNumberNode.InnerText + "]"; 
				objClaimNumberNode = null;
			}
			
			// TODO - To find a better way to ascertain which is our starting page (Event or Claim)
			// As of now, first event number and then claim number is checked for subtitle text.
			base.ResetSysExData("SubTitle",sSubTitleText);

            //mona

            //objSysExDataXmlDoc = null;
            //Added for Mits 19315:The person involved age is being calculated off the current date, and not the date of event.  
            Event objEvent = (Event)this.PiPhysician.Context.Factory.GetDataModelObject("Event", false);
            string sEventDateFromDB = string.Empty;
            if (objEvent != null)
            {
                int iEventId = PiPhysician.EventId;
                if (iEventId != 0)
                {
                    objEvent.MoveTo(iEventId);
                    sEventDateFromDB = objEvent.DateOfEvent;
                    if (!String.IsNullOrEmpty(sEventDateFromDB))
                    {
                        if (base.SysEx.SelectSingleNode("DateOfEvent") == null)
                        {
                            string sUIEventDate = string.Empty;
                            sUIEventDate = Conversion.ToDate(sEventDateFromDB).ToString("MM/dd/yyyy");
                            base.CreateSysExData("DateOfEvent", sUIEventDate);
                        }
                    }

                    singleRow = new ArrayList();
                    sSubTitleText = string.Format(" [{0} * {1} {2}]", objEvent.EventNumber, PiPhysician.PiEntity.FirstName, PiPhysician.PiEntity.LastName);
                    base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
                    base.AddElementToList(ref singleRow, "id", "formsubtitle");
                    base.AddElementToList(ref singleRow, "Text", sSubTitleText);
                    base.m_ModifiedControls.Add(singleRow);

                
                }
            }
            //Added for Mits 19315:The person involved age is being calculated off the current date, and not the date of event.
			if (PiPhysician.Physician.PhysEntity.BirthDate!=string.Empty)
                base.ResetSysExData("EntityAge", Utilities.CalculateAgeInYears(PiPhysician.Physician.PhysEntity.BirthDate, sEventDateFromDB)); //Added sEventDateFromDB for MITS 19315
			else
				base.ResetSysExData("EntityAge","");
            // akaushik5 Changed for MITS 37242 Starts
            //if(!SysSettings.IsBRSInstalled(base.ClientId))

            if (!this.PiPhysician.Context.InternalSettings.SysSettings.IsBRSInstalled())
                // akaushik5 Changed for MITS 37242 Ends
				base.AddKillNode("btnProviderContracts");
			
			base.ResetSysExData("ContractCount",PiPhysician.Physician.PhysEntity.Context.DbConnLookup.ExecuteInt("SELECT COUNT(*) FROM PROVIDER_CONTRACTS WHERE PROVIDER_EID=" + PiPhysician.Physician.PhysEntity.EntityId).ToString());

			if (this.PiPhysician.PiRowId > 0 || this.PiPhysician.PiEid <= 0)
				base.AddKillNode("javascript_SetDirtyFlag");

            //smishra25: MITS 24395, MITS 13748 implementation was missing
            //nadim for 13748,added to hide/unhide SSN field
            if (!PiPhysician.Context.RMUser.IsAllowedEx(m_SecurityId, FormBase.RMO_PI_PHYSICIAN_VIEW_SSN))
            {
                base.AddKillNode("taxid");

            }
          
            //nadim for 13748

            // Add DiaryMessage node, if already not present, to avoid Orbeon 'ref' error
            XmlNode objDiaryMessageNode = objSysExDataXmlDoc.SelectSingleNode("/SysExData/DiaryMessage");

            if (objDiaryMessageNode == null)
            {
                base.CreateSysExData("DiaryMessage");
            }
			//Shruti Choudhary updated code for mits 10384
            //if (this.SysFormStack.SelectSingleNode("//ScreenFlowStack/ScreenFlow[2]/SysFormName") != null)
            //{
            //    ((XmlElement)base.SysView.SelectSingleNode("//internal[@name='SysFormPForm']")).SetAttribute("value",
            //       this.SysFormStack.SelectSingleNode("//ScreenFlowStack/ScreenFlow[2]/SysFormName").InnerText);
            //}

            objSysExDataXmlDoc = null;
			//Check for People Maintenance permission
			PeoplePermissionChecks4PI("piphysician", m_SecurityId+RMO_UPDATE);
            //PJS MITS # 16563 - Adding Lookupdata attributes for policynumberlookup
            singleRow = new ArrayList();

            //Start-07/22/2010: Sumit - Disable Insurance Policy Field if Enhanced Policy is active for any LOB.
            if (PiPhysician.Physician.Context.InternalSettings.ColLobSettings[this.objCache.GetCodeId("PC", "LINE_OF_BUSINESS")].UseEnhPolFlag == 0 &&
                PiPhysician.Physician.Context.InternalSettings.ColLobSettings[this.objCache.GetCodeId("GC", "LINE_OF_BUSINESS")].UseEnhPolFlag == 0 &&
                PiPhysician.Physician.Context.InternalSettings.ColLobSettings[this.objCache.GetCodeId("WC", "LINE_OF_BUSINESS")].UseEnhPolFlag == 0 &&
                PiPhysician.Physician.Context.InternalSettings.ColLobSettings[this.objCache.GetCodeId("VA", "LINE_OF_BUSINESS")].UseEnhPolFlag == 0)
            {
                base.CreateSysExData("IsAnyPolicyDeActive", "0");
            }
            else
            {
                base.CreateSysExData("IsAnyPolicyDeActive", "-1");
                base.AddReadOnlyNode("primarypolicyid");
            }
            //End: Sumit

            //Start-07/22/2010: Sumit - No need to check for Enhanced Policy as Insurance Policy field remain disabled when it is active for any LOB.
            // Naresh Code Changed for Saving the Enhanced Policy Ids attached with the Claim
            //if (PiPhysician.Physician.Context.InternalSettings.SysSettings.UseEnhPolFlag == -1)
            //{
            //    base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.PolicyNumberLookup.ToString());
            //    base.AddElementToList(ref singleRow, "id", "primarypolicyid");
            //    base.AddElementToList(ref singleRow, "idref", "/Instance/PiPhysician/Physician/PrimaryPolicyIdEnh");
            //    base.AddElementToList(ref singleRow, "ref", "/Instance/PiPhysician/Physician/PrimaryPolicyIdEnh/@defaultdetail");
            //    base.AddElementToList(ref singleRow, "type", "enhpolicylookup");
            //    string sParameters = "return lookupData('primarypolicyid','policyenh',-1,'primarypolicyid',20)";
            //    base.AddElementToList(ref singleRow, "onclientclickparameters", sParameters);


            //}
            //else
            //{
                base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.PolicyNumberLookup.ToString());
                base.AddElementToList(ref singleRow, "id", "primarypolicyid");
                base.AddElementToList(ref singleRow, "idref", "/Instance/PiPhysician/Physician/PrimaryPolicyId");
                base.AddElementToList(ref singleRow, "ref", "/Instance/PiPhysician/Physician/PrimaryPolicyId/@defaultdetail");
                base.AddElementToList(ref singleRow, "type", "policynumberlookup");
                string sParameters = "return lookupData('primarypolicyid','policy',-1,'primarypolicyid',9)";
                base.AddElementToList(ref singleRow, "onclientclickparameters", sParameters);
            //}
            //End: Sumit
            base.m_ModifiedControls.Add(singleRow);
            if (PiPhysician.PolicyUnitRowId !=0)
            {
                DisplayUnitNo();
              
            }
            else
            {
                base.AddKillNode("Unitno");

            }
            //rsushilaggar JIRA 7767
            if (this.PiPhysician.Context.InternalSettings.SysSettings.UseEntityRole)
            {
                if (this.PiPhysician != null && this.PiPhysician.PiEntity != null && this.PiPhysician.PiEntity.NameType > 0)
                    this.ResetSysExData("EntityType", objCache.GetCodeDesc(this.PiPhysician.PiEntity.NameType));
                else
                    this.ResetSysExData("EntityType", objCache.GetCodeDesc(objCache.GetCodeId("IND", "ENTITY_NAME_TYPE")));
            }
            else
            {
                this.AddKillNode("entitytype");
            }
            //end rushilaggar
		}

		public override void OnValidate(ref bool Cancel)
		{
			bool bError = false;

			// Perform data validation
			string sToday = Conversion.ToDbDate(System.DateTime.Now);
			string sTime = Conversion.GetTime(System.DateTime.Now.ToShortTimeString());

			if(PiPhysician.Physician.PhysEntity.BirthDate!="")
			{
				if(PiPhysician.Physician.PhysEntity.BirthDate.CompareTo(sToday)>0)
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.BirthDate", base.ClientId), System.DateTime.Now.ToShortDateString()),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}
			}
			
			// Return true if there were validation errors
			Cancel = bError;
		}
        //Added Rakhi for R7:Add Emp Data Elements
        public override void OnUpdateObject()
        {
            base.OnUpdateObject();
			//ijha:Mobile Adjuster
			XmlElement objCaller = null;


			objCaller = (XmlElement)base.FormVariables.SelectSingleNode("//caller");
			if (objCaller != null)
			{
                if (objCaller.InnerText == "MobileAdjuster" || objCaller.InnerText == "MobilityAdjuster")
                {
					string sSQL = " SELECT EVENT_ID FROM CLAIM WHERE CLAIM_NUMBER = '" + base.GetSysExDataNodeText("//ClaimNumber") + "'";
					using (DbReader objReader = DbFactory.ExecuteReader(sConnectionString, sSQL))
					{
						if (objReader.Read())
						{
							PiPhysician.EventId = Conversion.ConvertObjToInt(objReader.GetValue("EVENT_ID"), base.ClientId);
							this.m_iEventId = PiPhysician.EventId;
							//base.FormVariables.SelectSingleNode("//SysExData//EventId").InnerText = objReader.GetValue("EVENT_ID").ToString();
							if (base.FormVariables.SelectSingleNode("//SysExData//EventId") != null)
							{
								base.FormVariables.SelectSingleNode("//SysExData//EventId").InnerText = objReader.GetValue("EVENT_ID").ToString();

							}

						}
					}
				}
			}
		   // ijha end
            if (base.m_fda.SafeFormVariableParamText("SysCmd") == "5")
            {
                PiPhysician.PiEntity.FormName = "PiPhysicianForm"; //Mits 22497

                #region "Updating Address Info Object"

                string sAddr1 = PiPhysician.PiEntity.Addr1;
                string sAddr2 = PiPhysician.PiEntity.Addr2;
                string sAddr3 = PiPhysician.PiEntity.Addr3;// JIRA 6420 pkandhari
                string sAddr4 = PiPhysician.PiEntity.Addr4;// JIRA 6420 pkandhari
                string sCity = PiPhysician.PiEntity.City;
                int iCountryCode = PiPhysician.PiEntity.CountryCode;
                int iStateId = PiPhysician.PiEntity.StateId;
                string sEmailAddress = PiPhysician.PiEntity.EmailAddress;
                string sFaxNumber = PiPhysician.PiEntity.FaxNumber;
                string sCounty = PiPhysician.PiEntity.County;
                string sZipCode = PiPhysician.PiEntity.ZipCode;
                //RMA-8753 nshah28(Added by ashish)
                string sSearchString = string.Empty;
                //RMA-8753 nshah28(Added by ashish) END

                if (
                        sAddr1 != string.Empty || sAddr2 != string.Empty || sAddr3 != string.Empty || sAddr4 != string.Empty ||
                        sCity != string.Empty || iCountryCode != 0 || iStateId != 0 || sEmailAddress != string.Empty ||
                        sFaxNumber != string.Empty || sCounty != string.Empty || sZipCode != string.Empty
                    )
                {
                    if (PiPhysician.PiEntity.EntityXAddressesList.Count == 0)
                    {
                       

                        EntityXAddresses objEntityXAddressesInfo = PiPhysician.PiEntity.EntityXAddressesList.AddNew();
						//RMA-8753 nshah28(Added by ashish) START
                        //objEntityXAddressesInfo.Addr1 = sAddr1;
                        //objEntityXAddressesInfo.Addr2 = sAddr2;
                        //objEntityXAddressesInfo.Addr3 = sAddr3;// JIRA 6420 pkandhari
                        //objEntityXAddressesInfo.Addr4 = sAddr4;// JIRA 6420 pkandhari
                        //objEntityXAddressesInfo.City = sCity;
                        //objEntityXAddressesInfo.Country = iCountryCode; 
                        //objEntityXAddressesInfo.State = iStateId;
                        objEntityXAddressesInfo.Address.Addr1 = sAddr1;
                        objEntityXAddressesInfo.Address.Addr2 = sAddr2;
                        objEntityXAddressesInfo.Address.Addr3 = sAddr3;
                        objEntityXAddressesInfo.Address.Addr4 = sAddr4;
                        objEntityXAddressesInfo.Address.City = sCity;
                        objEntityXAddressesInfo.Address.Country = iCountryCode;
                        objEntityXAddressesInfo.Address.State = iStateId;
                        objEntityXAddressesInfo.Email = sEmailAddress;
                        objEntityXAddressesInfo.Fax = sFaxNumber;
                        //objEntityXAddressesInfo.County = sCounty;
                        //objEntityXAddressesInfo.ZipCode = sZipCode;
                        objEntityXAddressesInfo.Address.County = sCounty;
                        objEntityXAddressesInfo.Address.ZipCode = sZipCode;
                        objEntityXAddressesInfo.EntityId = PiPhysician.PiEntity.EntityId;
                        objEntityXAddressesInfo.PrimaryAddress = -1;
                        //objEntityXAddressesInfo.AddressId = -1;
                        AddressForm objAddressForm = new AddressForm(m_fda);
                        sSearchString = objAddressForm.SearchStringForAddress(objEntityXAddressesInfo.Address, objData.Context.LocalCache);
                        objEntityXAddressesInfo.Address.SearchString = sSearchString;
                        objEntityXAddressesInfo.AddressId = CommonFunctions.CheckAddressDuplication(sSearchString, PiPhysician.Context.RMDatabase.ConnectionString, base.ClientId);
                        objEntityXAddressesInfo.Address.AddressId = objEntityXAddressesInfo.AddressId;
                        //RMA-8753 nshah28(Added by ashish) END
                        
                    }
                    else
                    {
                        foreach (EntityXAddresses objEntityXAddressesInfo in PiPhysician.PiEntity.EntityXAddressesList)
                        {
                            if (objEntityXAddressesInfo.PrimaryAddress == -1)
                            {
								//RMA-8753 nshah28(Added by ashish) START
                                //objEntityXAddressesInfo.Addr1 = sAddr1;
                                //objEntityXAddressesInfo.Addr2 = sAddr2;
                                //objEntityXAddressesInfo.Addr3 = sAddr3;// JIRA 6420 pkandhari
                                //objEntityXAddressesInfo.Addr4 = sAddr4;// JIRA 6420 pkandhari
                                //objEntityXAddressesInfo.City = sCity;
                                //objEntityXAddressesInfo.Country = iCountryCode;
                                //objEntityXAddressesInfo.State = iStateId;
                                objEntityXAddressesInfo.Address.Addr1 = sAddr1;
                                objEntityXAddressesInfo.Address.Addr2 = sAddr2;
                                objEntityXAddressesInfo.Address.Addr3 = sAddr3;
                                objEntityXAddressesInfo.Address.Addr4 = sAddr4;
                                objEntityXAddressesInfo.Address.City = sCity;
                                objEntityXAddressesInfo.Address.Country = iCountryCode;
                                objEntityXAddressesInfo.Address.State = iStateId;
                                objEntityXAddressesInfo.Email = sEmailAddress;
                                objEntityXAddressesInfo.Fax = sFaxNumber;
                                //objEntityXAddressesInfo.County = sCounty;
                                //objEntityXAddressesInfo.ZipCode = sZipCode;
                                objEntityXAddressesInfo.Address.County = sCounty;
                                objEntityXAddressesInfo.Address.ZipCode = sZipCode;
                                AddressForm objAddressForm = new AddressForm(m_fda);
                                sSearchString = objAddressForm.SearchStringForAddress(objEntityXAddressesInfo.Address, objData.Context.LocalCache);
                                objEntityXAddressesInfo.Address.SearchString = sSearchString;
                                objEntityXAddressesInfo.AddressId = CommonFunctions.CheckAddressDuplication(sSearchString, PiPhysician.Context.RMDatabase.ConnectionString, base.ClientId);
                                objEntityXAddressesInfo.Address.AddressId = objEntityXAddressesInfo.AddressId;
                                //RMA-8753 nshah28(Added by ashish) END
                                break;
                            }
                        }
                    }
                }
                else
                {
                    if (PiPhysician.PiEntity.EntityXAddressesList.Count > 0)
                    {
                        foreach (EntityXAddresses objEntityXAddressesInfo in PiPhysician.PiEntity.EntityXAddressesList)
                        {
                            if (objEntityXAddressesInfo.PrimaryAddress == -1)
                            {
                                //PiPhysician.PiEntity.EntityXAddressesList.Remove(objEntityXAddressesInfo.AddressId);
                                PiPhysician.PiEntity.EntityXAddressesList.Remove(objEntityXAddressesInfo.RowId); //RMA-8753 nshah28(Added by ashish)
                                break;
                            }

                        }
                    }

                }
            #endregion

                #region Added for updating Phone Numbers

                string sOfficePhone =  PiPhysician.PiEntity.Phone1.Trim();
                string sHomePhone =  PiPhysician.PiEntity.Phone2.Trim();
                int iHomeCode = objCache.GetCodeId("h", "PHONES_CODES");
                int iOfficeCode = objCache.GetCodeId("o", "PHONES_CODES");
                bool bOfficePhoneEntered = false;
                AddressXPhoneInfo objAddressesInfo = null;
                bool bOfficeCodeExists = false;
                bool bHomePhoneEntered = false;
                bool bHomeCodeExists = false;

                if (sOfficePhone != string.Empty)
                    bOfficePhoneEntered = true;
                if (sHomePhone != string.Empty)
                    bHomePhoneEntered = true;

                if ( PiPhysician.PiEntity.AddressXPhoneInfoList.Count == 0)
                {
                    if (bOfficePhoneEntered)
                    {
                        objAddressesInfo =  PiPhysician.PiEntity.AddressXPhoneInfoList.AddNew();
                        objAddressesInfo.PhoneCode = iOfficeCode;
                        objAddressesInfo.PhoneNo = sOfficePhone;
                    }
                    if (bHomePhoneEntered)
                    {
                        objAddressesInfo =  PiPhysician.PiEntity.AddressXPhoneInfoList.AddNew();
                        objAddressesInfo.PhoneCode = iHomeCode;
                        objAddressesInfo.PhoneNo = sHomePhone;
                    }
                }
                else
                {
                    foreach (AddressXPhoneInfo objXAddressesInfo in  PiPhysician.PiEntity.AddressXPhoneInfoList)
                    {
                        if (objXAddressesInfo.PhoneCode == iOfficeCode)
                        {
                            if (bOfficePhoneEntered)
                            {
                                objXAddressesInfo.PhoneNo = sOfficePhone;
                                bOfficeCodeExists = true;
                            }
                            else
                            {
                                 PiPhysician.PiEntity.AddressXPhoneInfoList.Remove(objXAddressesInfo.PhoneId);
                            }
                            continue;
                        }
                        else if (objXAddressesInfo.PhoneCode == iHomeCode)
                        {
                            if (bHomePhoneEntered)
                            {
                                objXAddressesInfo.PhoneNo = sHomePhone;
                                bHomeCodeExists = true;
                            }
                            else
                            {
                                 PiPhysician.PiEntity.AddressXPhoneInfoList.Remove(objXAddressesInfo.PhoneId);
                            }
                        }
                    }
                    if (bOfficePhoneEntered && !bOfficeCodeExists)
                    {
                        objAddressesInfo =  PiPhysician.PiEntity.AddressXPhoneInfoList.AddNew();
                        objAddressesInfo.PhoneCode = iOfficeCode;
                        objAddressesInfo.PhoneNo = sOfficePhone;
                    }
                    if (bHomePhoneEntered && !bHomeCodeExists)
                    {
                        objAddressesInfo =  PiPhysician.PiEntity.AddressXPhoneInfoList.AddNew();
                        objAddressesInfo.PhoneCode = iHomeCode;
                        objAddressesInfo.PhoneNo = sHomePhone;
                    }
                }

                #endregion
            }
           
        }
        //Added Rakhi for R7:Add Emp Data Elements
        private void DisplayUnitNo()
        {

            string sStaUnitNo = string.Empty;
            string sRiskLoc = string.Empty;
            string sSubLoc = string.Empty;

            StringBuilder sbSql = new StringBuilder();
            // RMA-10039: Ash, fixed error but in general this is incorrect handling for POINT, Integral, Staging
            sbSql.Append("SELECT POINT_UNIT_DATA.STAT_UNIT_NUMBER, POINT_UNIT_DATA.UNIT_RISK_LOC, POINT_UNIT_DATA.UNIT_RISK_SUB_LOC from POLICY_X_UNIT,POINT_UNIT_DATA,PERSON_INVOLVED ");
            sbSql.Append(" WHERE POLICY_X_UNIT.POLICY_UNIT_ROW_ID=PERSON_INVOLVED.POLICY_UNIT_ROW_ID AND POLICY_X_UNIT.UNIT_ID=POINT_UNIT_DATA.UNIT_ID ");
            sbSql.Append(" AND POINT_UNIT_DATA.UNIT_TYPE=POLICY_X_UNIT.UNIT_TYPE AND PERSON_INVOLVED.PI_ROW_ID=" + PiPhysician.PiRowId);

            DbReader objReader = null;
            objReader = DbFactory.GetDbReader(base.m_fda.connectionString, sbSql.ToString());
            if (objReader != null)
            {
                if (objReader.Read())
                {
                    sStaUnitNo = objReader.GetValue("STAT_UNIT_NUMBER").ToString();
                    sRiskLoc = objReader.GetValue("UNIT_RISK_LOC").ToString();
                    if (sRiskLoc == string.Empty)
                        sRiskLoc = "-";

                    sSubLoc = objReader.GetValue("UNIT_RISK_SUB_LOC").ToString();
                    if (sSubLoc == string.Empty)
                        sSubLoc =  "-";

                    base.ResetSysExData("UnitNo", sStaUnitNo + "/" + sRiskLoc + "/" + sSubLoc);


                }

            }


        }
	}
}
