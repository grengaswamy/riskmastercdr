using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;


namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for EventScreen.
	/// </summary>
	public class EventDatedTextForm : DataEntryFormBase
	{
		const string CLASS_NAME = "EventXDatedText";

		private EventXDatedText objDatedText{get{return objData as EventXDatedText;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}

		public override void InitNew()
		{
			base.InitNew();
			objDatedText.EventId = base.GetSysExDataNodeInt("EventId");
			if(objDatedText.EventId >0)
				(objData as INavigation).Filter = "EVENT_ID=" + objDatedText.EventId;

		}

		public EventDatedTextForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}
		public override void OnUpdateForm()
		{
			base.OnUpdateForm ();
			string sCaption = "";
			XmlNode elt = null;
			try{elt = base.SysEx.GetElementsByTagName("EventNumber")[0];}catch{};
			
			if(elt!=null)
				sCaption= "  (" + elt.InnerText +")" ;
			if(objDatedText.IsNew==true)
			{
				objDatedText.TimeEntered=DateTime.Now.TimeOfDay.ToString();
				objDatedText.DateEntered=DateTime.Today.ToShortDateString();
				if(objDatedText.Context.RMUser.objUser.LastName != "")
					objDatedText.EnteredByUser= objDatedText.Context.RMUser.objUser.FirstName + ", " + objDatedText.Context.RMUser.objUser.LastName;
				else
					objDatedText.EnteredByUser= objDatedText.Context.RMUser.objUser.FirstName;
			}
			//Pass this subtitle value to view (ref'ed from @valuepath).
			base.ResetSysExData("SubTitle",sCaption);
            //deb : MITS 25213
            if (objDatedText.Context.InternalSettings.SysSettings.UseAcrosoftInterface)
            {
                string sEventnumber = objDatedText.Context.DbConnLookup.ExecuteString("SELECT EVENT_NUMBER FROM EVENT WHERE EVENT_ID=" + objDatedText.EventId);
                base.ResetSysExData("EventNumber", sEventnumber);
            }
            //deb : MITS 25213
		}

	}
}
