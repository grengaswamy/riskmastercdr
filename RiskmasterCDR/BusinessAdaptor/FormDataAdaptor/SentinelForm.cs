using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Event Sentinel.
	/// </summary>
	public class SentinelForm : DataEntryFormBase
	{
		const string CLASS_NAME = "EventSentinel";
		const string FILTER_KEY_NAME = "EventId";

		private EventSentinel EventSentinel{get{return objData as EventSentinel;}}

		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		
		public SentinelForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

		public override void InitNew()
		{
			base.InitNew();

			//set filter for EventId
			EventSentinel.EventId=base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME,false);
			if (EventSentinel.EventId>0)
				(objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + EventSentinel.EventId;
			
			// Mihika - 11/23/2005 Defect No. 585
			this.m_ParentId=EventSentinel.EventId;
		}

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
			
			base.OnUpdateForm ();

			XmlDocument objSysExDataXmlDoc = base.SysEx;

			string sSubTitleText = string.Empty; 
			
			// Add EventNumber node, if already not present, to avoid Orbeon 'ref' error 			
			XmlNode objEventNumberNode = objSysExDataXmlDoc.SelectSingleNode("/SysExData/EventNumber");
			int iReportLevel= EventSentinel.ReportLevel;

			if(objEventNumberNode==null)
			{
				base.CreateSysExData("EventNumber"); 
			}
			else
			{
				if(objEventNumberNode.InnerText.Length>0)
					sSubTitleText = " [" + objEventNumberNode.InnerText + "]"; 
				objEventNumberNode = null;
			}

			// Mihika 12/02/2005 Defect no.585
			// In case no sentinel info is present, the EventDescription of the parent has to be provided as the SentEventDesc
			Event objEvent = (Event)EventSentinel.Context.Factory.GetDataModelObject("Event", false);
			objEvent.MoveTo(this.m_ParentId);
			if (EventSentinel.EventId == 0)
				EventSentinel.SentEventDesc = objEvent.EventDescription;
			objEvent.Dispose();

			// TODO - Confirm if we would like to use SubTitle set by the Event Screen
			base.ResetSysExData("SubTitle",sSubTitleText); 
			base.ResetSysExData("ReportLevelValue",iReportLevel.ToString());
			objSysExDataXmlDoc = null;
			//*********************
			//Get Derived Fields
			//*********************
		}


		// Added by Mihika - 11/23/2005 Defect No. 585
		// BSB InitNew on 1:1 case causes navigation to be aborted if we slap in the key
		// of the parent because the value is shared as the key of the current record.
		// This makes navigation silently stop becuase the assumption is that you are already
		// looking at the record data you've requested.  As a work-around for only the 
		// 1:1 Cases let's zero this back out before the navigation call.
		public override void BeforeAction(enumFormActionType eActionType, ref bool Cancel)
		{
			base.BeforeAction (eActionType, ref Cancel);
			switch(eActionType)
			{
				case enumFormActionType.MoveFirst:
				case enumFormActionType.MoveLast:
				case enumFormActionType.MoveTo:
					EventSentinel.EventId = 0;
					break;
			}
		}	

		// Added by Mihika - 11/23/2005 Defect No. 585
		//BSB 1:1 Hack Let's throw the proper parent key back on in case the MoveFirst\Last request
		// didn't find any records (we want to be sure the eventId stays set when creating a new
		// record.
		public override void AfterAction(enumFormActionType eActionType)
		{
			base.AfterAction(eActionType);
			switch(eActionType)
			{
				case enumFormActionType.MoveFirst:
				case enumFormActionType.MoveLast:
				case enumFormActionType.MoveTo:
					if(EventSentinel.EventId==0) //No record found - treat as new.
						EventSentinel.EventId = this.m_ParentId;
					break;
			}
		}


//		// BSB Hack due to unique 1:1 addition situation where this child is being created on the fly.
//		public override void BeforeSave(ref bool Cancel)
//		{
//			//If we were sent here from a parent(event) then we'll have an eventid in the SysEx
//			//originally from ScreenFlow\SysExData.
//			XmlDocument objXML = base.SysEx;
//			XmlNode objEventId=null;
//			try{objEventId = objXML.GetElementsByTagName("EventId")[0];}
//			catch{};
//			
//			//Filter by this EventId if Present.
//			if(objEventId !=null)
//				if(this.EventSentinel.EventId ==0)
//					this.EventSentinel.EventId = Conversion.ConvertStrToInteger(objEventId.InnerText);
//
//			base.BeforeSave (ref Cancel);
//
//		}
	}
}