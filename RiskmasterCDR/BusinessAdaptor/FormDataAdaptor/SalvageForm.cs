﻿      using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;


namespace Riskmaster.BusinessAdaptor
{
  public  class SalvageForm : DataEntryFormBase
    {


		const string CLASS_NAME = "Salvage";
        const string FILTER_KEY_NAME = "ParentId";
      
        const string PARENT_NAME1 = "unit";
        const string PARENT_NAME2 = "propertyloss";

		private Salvage objSalvage{get{return objData as Salvage;}}

		private LocalCache objCache{get{return objData.Context.LocalCache;}}

        public SalvageForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

        private string m_Caption = string.Empty;
        
        public override string GetCaption()
        {
            return base.GetCaption();
        }

		public override void InitNew()
		{
            string sParentSysName = string.Empty;
			base.InitNew();
     
            objSalvage.ParentId = base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME, false);
            if (objSalvage.ParentId > 0)
                (objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + objSalvage.ParentId;

            sParentSysName = base.GetFormVarNodeText("SysFormPForm");
        
             if (sParentSysName == PARENT_NAME1)
            {
                objSalvage.ParentName = typeof(UnitXClaim).Name;
            }
            else if (sParentSysName == PARENT_NAME2)
            {
                objSalvage.ParentName = typeof(ClaimXPropertyLoss).Name;
            }
       
		}
       
        public SortedList SalvageHistSorted(Salvage  p_objSalvage, bool p_bReloadSortedCollection)
        { 
           SortedList objSalvageHistSorted = null;
           if (objSalvageHistSorted == null || p_bReloadSortedCollection)
            {
                string sKeyValue = string.Empty;
                int iKey = 0;
                objSalvageHistSorted = new SortedList();
                foreach (SalvageHistory objSalvageHist in p_objSalvage.SalvageHistoryList)
                {
                    if (objSalvageHist.SalvageHistRowId > 0)
                    {
                        sKeyValue = objSalvageHist.SalvageHistRowId.ToString();
                        iKey = Conversion.ConvertStrToInteger(sKeyValue);
                        objSalvageHistSorted.Add(iKey, objSalvageHist);
                    }
                }
            }
            return (objSalvageHistSorted);
        }

        public void RenderSalvageHistoryList(XmlDocument objSysEx)
        {
            XmlElement objSalvageHistListElement = null;
            XmlElement objListHeadXmlElement = null;
            XmlElement objOptionXmlElement = null;
           string sAddr1=null;
            string sAddr2= null;
            string sAddr3 = null;// JIRA 6420 pkandhari
            string sAddr4 = null;// JIRA 6420 pkandhari
            string sCity= null;
            string sCounty= null;
            string sCountry= null;
            string sZipCode= null;
            string sState= null;
            try
            {
                objSalvageHistListElement = (XmlElement)objSysEx.SelectSingleNode("/SysExData/SalvageLocationList");
                if (objSalvageHistListElement != null)
                    objSalvageHistListElement.ParentNode.RemoveChild(objSalvageHistListElement);

                CreateElement(objSysEx.DocumentElement, "SalvageLocationList", ref objSalvageHistListElement);

           
                CreateElement(objSalvageHistListElement, "listhead", ref objListHeadXmlElement);
              
                CreateAndSetElement(objListHeadXmlElement, "InPossessionOf", "In Possession Of");
                CreateAndSetElement(objListHeadXmlElement, "MoveDate", "Move Date");

               CreateAndSetElement(objListHeadXmlElement, "DailyFees", "Daily Fees");
              CreateAndSetElement(objListHeadXmlElement, "SalvageAddress", "Salvage Address");
          
                    for (int iIndex = 0; iIndex < SalvageHistSorted(objSalvage, true).Count; iIndex++)
                    {
                        SalvageHistory objSalvageHistory = (SalvageHistory)SalvageHistSorted(objSalvage, false).GetByIndex(iIndex);

                        CreateElement(objSalvageHistListElement, "option", ref objOptionXmlElement);
                        objOptionXmlElement.SetAttribute("ref", "/Instance/UI/FormVariables/SysExData/" + objOptionXmlElement.ParentNode.LocalName + "/option[" + iIndex.ToString() + "]");

                       
                        CreateAndSetElement(objOptionXmlElement, "InPossessionOf",objCache.GetEntityLastName(objSalvageHistory.InPosEid));
                        CreateAndSetElement(objOptionXmlElement, "MoveDate", Conversion.GetDBDateFormat(objSalvageHistory.MoveDate, "d"));
                       CreateAndSetElement(objOptionXmlElement, "DailyFees", objSalvageHistory.DailyFees.ToString());
                        if(objSalvageHistory.SalvageAddress >0)
                            objCache.GetAddressInFo(objSalvageHistory.SalvageAddress, ref sAddr1, ref sAddr2, ref sAddr3, ref sAddr4, ref sCity, ref sCounty, ref sState, ref sCountry, ref sZipCode);
                                              
                        CreateAndSetElement(objOptionXmlElement, "SalvageAddress", sAddr1 +" " + sAddr2+" " + sAddr3 +" " + sAddr4 +" " +sCity + " " +sCounty + " " +sState+ " " +sCountry+" "+ " " +sZipCode);
                   
                    }
            }
            finally
            {
                objSalvageHistListElement = null;
                objListHeadXmlElement = null;
                objOptionXmlElement = null;
            }
        }
	
		public override void OnUpdateForm()
		{
            int claimId = 0;
            DataObject objParent = null;
            Claim objClaim = (Claim)objSalvage.Context.Factory.GetDataModelObject("Claim", false);
			
            base.OnUpdateForm();

             if (objSalvage.ParentName == typeof(UnitXClaim).Name)
            {
                objParent = (UnitXClaim)objSalvage.Context.Factory.GetDataModelObject("UnitXClaim", false);
                objParent.MoveTo(objSalvage.ParentId);
                claimId = ((UnitXClaim)objParent).ClaimId;
                
            }
            else if (objSalvage.ParentName == typeof(ClaimXPropertyLoss).Name)
            {
                objParent = (ClaimXPropertyLoss)objSalvage.Context.Factory.GetDataModelObject("ClaimXPropertyLoss", false);
                objParent.MoveTo(objSalvage.ParentId);
                claimId = ((ClaimXPropertyLoss)objParent).ClaimId;
              
            }

             objClaim.MoveTo(claimId);
             //added by Manika for R8 enhancement of Salvage :MITS 26295
             if (objParent is UnitXClaim)
             {
                 switch (objClaim.LineOfBusCode)
                 {
                     case 241:
                         m_SecurityId = RMO_GC_VEHICLELOSS_SALVAGE;
                         break;
                 }
             }
             //End MITS 26295
            if (objParent is ClaimXPropertyLoss)
            {
                switch (objClaim.LineOfBusCode)
                {
                    case 241:
                        m_SecurityId = RMO_GC_PROPERTYLOSS_SALVAGE;
                        break;
                                  }
            }


            XmlElement objSysSid = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysSid");
            objSysSid.InnerText = m_SecurityId.ToString();

            if (objSalvage.SalvageRowId == 0)
            {
                if (!objSalvage.Context.RMUser.IsAllowedEx(m_SecurityId, RMO_CREATE))
                {
                    m_CurrentAction = enumFormActionType.None;
                    LogSecurityError(RMO_CREATE);
                    return;
                }
            }
            else
            {
                if (!objSalvage.Context.RMUser.IsAllowedEx(m_SecurityId, RMO_VIEW))
                {
                    m_CurrentAction = enumFormActionType.None;
                    LogSecurityError(RMO_VIEW);
                    return;
                }
               
            }

            // akaushik5 Added for MITS 31973 Starts
            if (claimId != default(int))
            {
                base.ResetSysExData("ClaimId", objClaim.ClaimId.ToString());
            }
            else 
            {
                base.ResetSysExData("ClaimId", string.Empty);
            }

            int iUseClaimProgressNotes = 0;
            iUseClaimProgressNotes = objClaim.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode].ClmProgressNotesFlag;
            if (iUseClaimProgressNotes == default(int))
            {
                base.AddKillNode("enhancednotes");
            }
            // akaushik5 Added for MITS 31973 Ends

            m_Caption = this.GetCaption();
            base.ResetSysExData("SubTitle", m_Caption);
            ArrayList singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singleRow, "id", "formsubtitle");
            base.AddElementToList(ref singleRow, "Text", m_Caption);
            base.m_ModifiedControls.Add(singleRow);

           RenderSalvageHistoryList(base.SysEx);

           //Added By agupta298 - PMC GAP08 - NMVTIS Reporting - Start - RMA-4694
           if (!objSalvage.Context.InternalSettings.SysSettings.UseNMVTISReqFields)
           {
               base.AddKillNode("mileage");
               base.AddKillNode("titlenumber");
               base.AddKillNode("statebrand");
               base.AddKillNode("istheftrecovered");
               base.AddKillNode("salvagedate");
               base.AddKillNode("salvagereason");
               base.AddKillNode("salvagesubrogation");
           }
            //Added By agupta298 - PMC GAP08 - NMVTIS Reporting - End - RMA-4694
	
		}
       internal void CreateAndSetElement( XmlElement p_objParentNode , string p_sNodeName , string p_sText )
        {
            try
            {
				XmlElement objChildNode = null ;
				this.CreateAndSetElement( p_objParentNode , p_sNodeName , p_sText , ref objChildNode );
            }
			catch( RMAppException p_objEx )
            {
				throw p_objEx ;
            }
			catch( Exception p_objEx )
            {
                throw new RMAppException(Globalization.GetString("TODO", base.ClientId), p_objEx);
            }
        }
     
		internal void CreateAndSetElement( XmlElement p_objParentNode , string p_sNodeName , string p_sText , ref XmlElement p_objChildNode )
        {
            try
            {
				CreateElement( p_objParentNode , p_sNodeName , ref p_objChildNode );
				p_objChildNode.InnerText = p_sText ;			
            }
			catch( RMAppException p_objEx )
            {
				throw p_objEx ;
            }
			catch( Exception p_objEx )
            {
                throw new RMAppException(Globalization.GetString("TODO", base.ClientId), p_objEx);
            }
        }

        internal void CreateElement(XmlElement p_objParentNode, string p_sNodeName, ref XmlElement p_objChildNode)
        {
            try
            {
                p_objChildNode = p_objParentNode.OwnerDocument.CreateElement(p_sNodeName);
                p_objParentNode.AppendChild(p_objChildNode);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TODO", base.ClientId), p_objEx);
            }

        }


        public override void OnValidate(ref bool Cancel)
        {
            bool bError = false;
            string sToday = Conversion.ToDbDate(System.DateTime.Now);
            
            //Added by agupta298 PMC GAP08 - End - RMA-4694
            UnitXClaim objUnitXClaim = null;
            int iVehicleYear = 0;
            //Added by agupta298 PMC GAP08 - End - RMA-4694

            if (objSalvage.MoveDate.CompareTo(sToday) > 0)
            {
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                   Globalization.GetString("Validation.MustBeLessThanToday", base.ClientId), BusinessAdaptorErrorType.Error);

                bError = true;
            }

            //Added By agupta298 - PMC GAP08 - NMVTIS Reporting - Start - RMA-4694
            if (objSalvage.Context.InternalSettings.SysSettings.UseNMVTISReqFields)
            {
                if (objSalvage.ParentName == typeof(UnitXClaim).Name)
                {
                    objUnitXClaim = (UnitXClaim)objSalvage.Context.Factory.GetDataModelObject("UnitXClaim", false);
                    objUnitXClaim.MoveTo(objSalvage.ParentId);
                    iVehicleYear = objUnitXClaim.Vehicle.VehicleYear;
                }

                if (!string.IsNullOrWhiteSpace(objSalvage.SalvageDate) && iVehicleYear == 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId), CommonFunctions.FilterBusinessMessage(Globalization.GetString("SalvageNMVTISVehicleYear", base.ClientId)),
                        BusinessAdaptorErrorType.Error);
                    bError = true;
                }

                if (!string.IsNullOrWhiteSpace(objSalvage.SalvageDate) && objSalvage.SalvageDate.CompareTo(sToday) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                       Globalization.GetString("Validation.SalvageDateMustBeLessThanToday", base.ClientId), BusinessAdaptorErrorType.Error);
                    bError = true;
                }
            }

            //Commented : As this validation we are currently handling through JavaScript
            //if (!string.IsNullOrWhiteSpace(objSalvage.SalvageDate) && objSalvage.BuyerEid == 0)
            //{
            //    Errors.Add(Globalization.GetString("ValidationError"), CommonFunctions.FilterBusinessMessage(Globalization.GetString("SalvageNMVTISBuyerName")),
            //        BusinessAdaptorErrorType.Error);
            //    bError = true;
            //}

            //Added By agupta298 - PMC GAP08 - NMVTIS Reporting - End - RMA-4694

            Cancel = bError;
          
        }
	}
}