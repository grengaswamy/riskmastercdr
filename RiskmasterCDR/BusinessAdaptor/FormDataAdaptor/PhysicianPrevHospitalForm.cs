﻿
using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

//TODO: Auto-Generated Form - migrate and verify behaviors from Legacy RMNet form.

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for PhysicianPrevHospital Screen.
	/// </summary>
	public class PhysicianPrevHospitalForm : DataEntryFormBase
	{
		const string CLASS_NAME = "PhysicianPrevHospital";
		private PhysicianPrevHospital PhysicianPrevHospital{get{return objData as PhysicianPrevHospital;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		const string FILTER_KEY_NAME = "PhysEid";

		/// <summary>
		/// Class constructor which initializes Class Name information
		/// </summary>
		/// <param name="fda"></param>
		public PhysicianPrevHospitalForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

		/// <summary>
		/// Initializes the class and sets the parent ID for the class
		/// that can be used for parent-child navigation
		/// </summary>
		public override void InitNew()
		{
			base.InitNew();
			
			//If we were sent here from a parent(event) then we'll have an eventid in the SysEx
			//originally from ScreenFlow\SysExData.
			XmlNode objPhysEid=null;

			try
			{
				objPhysEid = base.SysEx.GetElementsByTagName(FILTER_KEY_NAME)[0];
			}//end try
			catch
			{
			}; //end catch
			if(objPhysEid !=null)
				this.m_ParentId = Conversion.ConvertObjToInt(objPhysEid.InnerText, base.ClientId);
			
			PhysicianPrevHospital.PhysEid = this.m_ParentId;

			//Filter by this PhysEid if Present.
			if(this.m_ParentId != 0)
			{
				(objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + this.m_ParentId;
			}
			
		}//end method InitNew()

        // npadhy MITS 14200 Validation for Privilege Start Date and Privilege End Date as Per RMWorld
        public override void OnValidate(ref bool Cancel)
        {
            bool bError = false;

            if (!string.IsNullOrEmpty(PhysicianPrevHospital.IntDate) && !(string.IsNullOrEmpty(PhysicianPrevHospital.EndDate)))
            {
                if (PhysicianPrevHospital.IntDate.CompareTo(PhysicianPrevHospital.EndDate) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                            String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId), Globalization.GetString("Field.EndDate", base.ClientId), Conversion.ToDate(PhysicianPrevHospital.IntDate).ToShortDateString() + "(" + Globalization.GetString("Field.StartDate", base.ClientId) + ")"),
                            BusinessAdaptorErrorType.Error);

                    bError = true;
                }
            }

            // Return true if there were validation errors
            Cancel = bError;
        }
		
		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
			base.OnUpdateForm ();
		}
	}
}
