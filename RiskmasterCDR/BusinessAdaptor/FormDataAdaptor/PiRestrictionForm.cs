﻿
using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using Riskmaster.Settings;

//TODO: Auto-Generated Form - migrate and verify behaviors from Legacy RMNet form.

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for PiRestriction Screen.
	/// </summary>
	public class PiRestrictionForm : DataEntryFormBase
	{
		const string CLASS_NAME = "PiXRestrict";
        private string sPageId = "10075";
		private PiXRestrict PiXRestrict{get{return objData as PiXRestrict;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		const string FILTER_KEY_NAME = "PiRowId";
        private string sLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").Split('|')[0].ToString();
		string[] sDaysOfWeek=new string[7];
		public struct Holidays
		{
			internal string sDate;
			internal string sDesc;
			internal long[] lOrgID;
		}
		public Holidays[] SYS_HOLIDAYS;

		public override void InitNew()
		{
			base.InitNew(); 

			this.m_ParentId = base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME,false);
			(objData as INavigation).Filter = 
				objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + this.m_ParentId.ToString();		
			this.PiXRestrict.PiRowId = this.m_ParentId;
		}

		public PiRestrictionForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
            sLangCode = Convert.ToString(fda.userLogin.objUser.NlsCode);
		}
		

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		//Changes by: Nikhil Garg		Dated: 23-Dec-2005
		//Claim should not be involved for getting Employee. We have to pick it from the Parent of PiRestriction.
		//Also changes for calculating the TotalRestrictedDays only if there is a postback i.e. Date has been changed.
		public override void OnUpdateForm()
		{
			base.OnUpdateForm ();

            XmlDocument objDOM=new XmlDocument();
			string sSysCmd = base.m_fda.SafeFormVariableParamText("SysCmd");
			
			// Subtitle node should already be present in SysExData
			// as this screen always opens from PiEmployee, so we would reuse its value.
			Event objEvent = (Event) this.PiXRestrict.Context.Factory.GetDataModelObject("Event",false);    
			DateTime dtFirstRestrictedDate = new DateTime(0);
			DateTime dtLastRestrictedDate = new DateTime(0);
			DateTime dtEnd= new DateTime(0);
			int iTotalRestrictedDays = 0;
//			int iOSHAFlag = 0;
			int iTotalDays = 0;
			string sStart = string.Empty;
			string sStartOSHACount = string.Empty;
			string sEventDateFromDB = string.Empty;
			string sEventDate = string.Empty;
			string bHavePreEventDates="False";
			string sOSHA = string.Empty;
			string sWorkFlags = string.Empty; 
			int iEventId = 0;
//			int iClaimID = 0;

			iEventId = base.GetSysExDataNodeInt("/SysExData/EventId",true);
//			iClaimID = base.GetSysExDataNodeInt("/SysExData/ClaimId",true);
			objEvent.MoveTo(iEventId);   			
			sEventDateFromDB = objEvent.DateOfEvent;			
			sEventDate = Conversion.ToDate(sEventDateFromDB).ToString("MM/dd/yyyy");
//			Claim objClaim=objEvent.ClaimList[iClaimID];
//			PiEmployee objEmployee=objClaim.PrimaryPiEmployee as PiEmployee;
//			PiXRestrictList objResList = objClaim.PrimaryPiEmployee.PiXRestrictList;

			if (PiXRestrict.Parent==null)
				PiXRestrict.LoadParent();
			PiEmployee objEmployee=PiXRestrict.Parent as PiEmployee;
			PiXRestrictList objResList=objEmployee.PiXRestrictList;

            //srajindersin:mits 24977:The second First Restriction Day entry was showing durationspan of first First Restriction Day entry : 2/8/2012
            //foreach(PiXRestrict objRestrict in objResList)
            //{
            //    if (objRestrict.PiRestrictRowId != PiXRestrict.PiRestrictRowId  )
            //        {
            //        sStart = objRestrict.DateFirstRestrct;
            //        sStartOSHACount = sStart;
            //        if (objRestrict.DateLastRestrct.Trim() == "")
            //            dtEnd = System.DateTime.Today;
            //        else
            //            dtEnd = Conversion.ToDate(objRestrict.DateLastRestrct);
            //    //  ijha :MITS:24977 : restricted first day compared with event date + 1            
            //    //  if(Conversion.ToDate(sStartOSHACount)<=Conversion.ToDate(sEventDateFromDB).AddDays(1))
            //        if(Conversion.ToDate(sStartOSHACount)<=Conversion.ToDate(sEventDateFromDB))
            //        {
            //            bHavePreEventDates="True";
            //        //  sStartOSHACount=Conversion.ToDate(sEventDateFromDB).AddDays(1).ToShortDateString();
            //            sStartOSHACount=Conversion.ToDate(sEventDateFromDB).ToShortDateString();
            //        }
            //        else
            //            sStartOSHACount=Conversion.ToDate(sStartOSHACount).ToShortDateString();

            //        if(Convert.ToDateTime(sStartOSHACount)<=dtEnd)
            //        {
            //            TimeSpan objTimeSpan=dtEnd.Subtract(Convert.ToDateTime(sStartOSHACount));
            //            iTotalDays = iTotalDays + objTimeSpan.Days + 1;
            //        }
            //    }
            //}
            //END srajindersin:mits 24977:The second First Restriction Day entry was showing durationspan of first First Restriction Day entry : 2/8/2012
            
			//Add for current record
			if (PiXRestrict.DateFirstRestrct.Trim()!="")
			{
				sStart = PiXRestrict.DateFirstRestrct;
				sStartOSHACount = sStart;
				if(PiXRestrict.DateLastRestrct.Trim()=="")
					dtEnd=System.DateTime.Today;
				else
					dtEnd=Conversion.ToDate(PiXRestrict.DateLastRestrct);
              
             //  ijha :MITS:24977 : restricted first day compared with event date + 1
            //   if (Conversion.ToDate(sStartOSHACount) <= Conversion.ToDate(sEventDateFromDB).AddDays(1))
                //if (Conversion.ToDate(sStartOSHACount) <= Conversion.ToDate(sEventDateFromDB)) atavaragiri mits:24977
                    if (Conversion.ToDate(sStartOSHACount) < Conversion.ToDate(sEventDateFromDB))
                {
                    bHavePreEventDates = "True";
                //  sStartOSHACount = Conversion.ToDate(sEventDateFromDB).AddDays(1).ToShortDateString();
                    sStartOSHACount = Conversion.ToDate(sEventDateFromDB).ToShortDateString();
                }
                else
                    sStartOSHACount = Conversion.ToDate(sStartOSHACount).ToShortDateString();

				if(Convert.ToDateTime(sStartOSHACount)<=dtEnd)
				{
					TimeSpan objTimeSpan=dtEnd.Subtract(Convert.ToDateTime(sStartOSHACount));
					iTotalDays = iTotalDays + objTimeSpan.Days + 1;
				}
			}

			if(sSysCmd=="7")
			{
				dtFirstRestrictedDate = Conversion.ToDate(PiXRestrict.DateFirstRestrct);
				dtLastRestrictedDate = Conversion.ToDate(PiXRestrict.DateLastRestrct);
			
				if (objEmployee!=null)
					sWorkFlags = objEmployee.WorkSunFlag + ","
						+	objEmployee.WorkMonFlag + "," 
						+	objEmployee.WorkTueFlag + "," 
						+	objEmployee.WorkWedFlag + "," 
						+	objEmployee.WorkThuFlag + "," 
						+	objEmployee.WorkFriFlag + "," 
						+	objEmployee.WorkSatFlag;
				sDaysOfWeek=sWorkFlags.Split(",".ToCharArray());

				if(PiXRestrict.DateLastRestrct.Trim()=="")
					iTotalRestrictedDays = GetWorkDay(dtFirstRestrictedDate,System.DateTime.Today,sDaysOfWeek);
				else
					iTotalRestrictedDays = GetWorkDay(dtFirstRestrictedDate,dtLastRestrictedDate,sDaysOfWeek);
                PiXRestrict.Duration = iTotalRestrictedDays;
			}
			else
				iTotalRestrictedDays = PiXRestrict.Duration;
            //ijha:mits 24978:The second First Restriction Day entry was showing durationspan of first First Restriction Day entry 
            if (string.IsNullOrEmpty(this.PiXRestrict.DateFirstRestrct))
            {
                iTotalDays = 0;
            }

            // If the event happened after Osha 300 rules came into effect
            if (Convert.ToDateTime(sEventDate) >= Convert.ToDateTime("01/01/2002"))
            {
                if (bHavePreEventDates == "True")
                {
                    sOSHA = CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("sOSHAValue", 0, sPageId, base.ClientId), sLangCode) + iTotalDays.ToString() + CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("sOSHAtotresdays2", 0, sPageId, base.ClientId), sLangCode);
                    
                }

                else
                {
                    sOSHA = CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("sOSHAValue", 0, sPageId, base.ClientId), sLangCode) + iTotalDays.ToString();
                   
                }
            }
            else
            {
                sOSHA = CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("sOSHAtotresdays3", 0, sPageId, base.ClientId), sLangCode);
                
                
            }
			
			base.ResetSysExData("TotalDays",sOSHA);
			base.ResetSysExData("TotalRestrictedDays",iTotalRestrictedDays.ToString()); 
			
			objEvent = null;
		}

		public override void OnUpdateObject()
		{
			base.OnUpdateObject();
            //XmlDocument objXML=base.SysEx;
            //int iTotalRestrictedDays=0;
            //DateTime dtFirstRestrictedDate = new DateTime(0);
            //string sWorkFlags=string.Empty;
            //DateTime dtEnd= new DateTime(0);

            //iTotalRestrictedDays=Conversion.ConvertStrToInteger(objXML.SelectSingleNode("//TotalRestrictedDays").InnerText);
            //if (iTotalRestrictedDays <= 0)
            //{
            //    dtFirstRestrictedDate = Conversion.ToDate(PiXRestrict.DateFirstRestrct);
            //    if(PiXRestrict.DateLastRestrct.Trim()=="")
            //        dtEnd=System.DateTime.Today;
            //    else
            //        dtEnd=Conversion.ToDate(PiXRestrict.DateLastRestrct);
            //    if (PiXRestrict.Parent==null)
            //        PiXRestrict.LoadParent();
            //    PiEmployee objEmployee=PiXRestrict.Parent as PiEmployee;

            //    if (objEmployee!=null)
            //        sWorkFlags = objEmployee.WorkSunFlag + ","
            //            +	objEmployee.WorkMonFlag + "," 
            //            +	objEmployee.WorkTueFlag + "," 
            //            +	objEmployee.WorkWedFlag + "," 
            //            +	objEmployee.WorkThuFlag + "," 
            //            +	objEmployee.WorkFriFlag + "," 
            //            +	objEmployee.WorkSatFlag;
            //    sDaysOfWeek=sWorkFlags.Split(",".ToCharArray());
            //    iTotalRestrictedDays = GetWorkDay(dtFirstRestrictedDate,dtEnd,sDaysOfWeek);
            //}
			
            //PiXRestrict.Duration=iTotalRestrictedDays;
		}
		#region Count No. of Restricted Days
		private void subInitHolidayInfo()
		{
			string sDate1=string.Empty;
			string sDate2=string.Empty;
			int i=0;
			int j=0;
			int count=1;

			DbReader objReader=null;
			DbReader objCount=null;
            using (objCount = DbFactory.GetDbReader(PiXRestrict.Context.DbConn.ConnectionString, "SELECT COUNT(*) COUNTI FROM SYS_HOLIDAYS"))
            {
                if (objCount != null)
                {
                    while (objCount.Read())
                    {
                        count = Conversion.ConvertObjToInt(objCount.GetValue("COUNTI"), base.ClientId);
                        SYS_HOLIDAYS = new Holidays[count];
                    }
                }
            }
            using (objReader = DbFactory.GetDbReader(PiXRestrict.Context.DbConn.ConnectionString, "SELECT HOL_DATE,ORG_EID FROM SYS_HOLIDAYS ORDER BY HOL_DATE,ORG_EID"))
            {
                if (objReader != null)
                {
                    while (objReader.Read())
                    {

                        sDate1 = objReader.GetString("HOL_DATE");
                        if (sDate1 != sDate2)
                        {
                            if (sDate2 != "")
                                i = i + 1;
                            SYS_HOLIDAYS[i].sDate = sDate1;
                            j = 0;
                            SYS_HOLIDAYS[i].lOrgID = new long[count];
                            SYS_HOLIDAYS[i].lOrgID[j] = Conversion.ConvertObjToInt64(objReader.GetValue("ORG_EID"), base.ClientId);
                            sDate2 = sDate1;
                        }
                        else
                        {
                            j = j + 1;
                            SYS_HOLIDAYS[i].lOrgID[j] = Conversion.ConvertObjToInt64(objReader.GetValue("ORG_EID"), base.ClientId);
                        }
                    }
                }
            }
		}
		private int GetWorkDay(DateTime dDate1, DateTime dDate2,string[] sDaysOfWeek)
		{
			int iDays=0;
			int iDayOfWeekStart=0;
			int iDayOfWeekEnd=0;
			int iNewDays=0;
			int iReturnEid=0;
			bool bCount;
			string sParent=string.Empty;
			TimeSpan objDays;
            //Mgaba2:MITS 15484:Exclude Holidays were not coming into effect
            //Getting department id from database rather from parent screen:start
            //int iDeptAssignedEid =
            //    base.GetSysExDataNodeInt("/SysExData/DeptAssignedEid", true); 
            DbReader objDbReader = null;
            int iDeptAssignedEid = 0;
            string sSQL = "SELECT DEPT_ASSIGNED_EID FROM PERSON_INVOLVED WHERE PI_ROW_ID=" + PiXRestrict.PiRowId.ToString();
            objDbReader = this.PiXRestrict.Context.DbConnLookup.ExecuteReader(sSQL);
            while (objDbReader!=null && objDbReader.Read())
            {
               iDeptAssignedEid = objDbReader.GetInt("DEPT_ASSIGNED_EID");
            }
            objDbReader.Close();
            //Mgaba2:MITS 15484:End

			subInitHolidayInfo();
			objDays=dDate2.Subtract(dDate1);
			iDays=objDays.Days+1;
			iDayOfWeekStart = (int)dDate1.DayOfWeek ;
			iDayOfWeekEnd=(int)dDate2.DayOfWeek ;
			for(int i=1; i<=iDays; i++)
			{
				if(sDaysOfWeek[(int) dDate1.AddDays(i-1).DayOfWeek]=="True")
				{
					if(PiXRestrict.Context.InternalSettings.SysSettings.ExclHolidays==true && iDeptAssignedEid!=0)
					{
						bCount=false;
						for(int j=0; j<=SYS_HOLIDAYS.Length-1; j++)
						{
							if(dDate1.AddDays(i-1).ToShortDateString()== Conversion.ToDate(SYS_HOLIDAYS[j].sDate).ToShortDateString())
							{
								for(int k=0; k<= SYS_HOLIDAYS[j].lOrgID.Length-1; k++)
								{
									iReturnEid=0;
									sParent=PiXRestrict.Context.InternalSettings.CacheFunctions.GetOrgParent(
											iDeptAssignedEid,PiXRestrict.Context.LocalCache.GetOrgTableId(Conversion.ConvertObjToInt(SYS_HOLIDAYS[j].lOrgID[k], base.ClientId)),0,ref iReturnEid);
									if(SYS_HOLIDAYS[j].lOrgID[k]== iReturnEid)
										bCount=true;
								}
							}
						}
						if(bCount==false)
							iNewDays=iNewDays+1;
					}
					else
					{
						iNewDays=iNewDays+1;
					}
				}
			}
			return iNewDays;
		}

		
		#endregion
//		private int GetTotalRestrictedDays(int iPiRowId)
//		{
//			int iTotalDays = 0;
//			string sSQL = string.Empty;
//
//			DbReader objDbReader = null;
//			DateTime dtFirstRestrictedDate;
//			DateTime dtLastRestrictedDate;
//			TimeSpan span;
//			string sLastRestrictedDate = string.Empty; 
//
//			try
//			{
//				sSQL = "SELECT PI_RESTRICT_ROW_ID, DATE_FIRST_RESTRCT, DATE_LAST_RESTRCT,"
//					+ "	DURATION, PERCENT_DISABLED, POSITION_CODE "
//					+ " FROM PI_X_RESTRICT WHERE PI_ROW_ID = " + iPiRowId.ToString()
//					+ " ORDER BY DATE_FIRST_RESTRCT";
//
//				objDbReader = this.PiXRestrict.Context.DbConnLookup.ExecuteReader(sSQL);     
//
//				while(objDbReader.Read())
//				{
//					dtFirstRestrictedDate = Conversion.ToDate(objDbReader.GetString("DATE_FIRST_RESTRCT")); 
// 					sLastRestrictedDate = objDbReader.GetString("DATE_LAST_RESTRCT");
//					
//					if(sLastRestrictedDate.Length>0)
//						dtLastRestrictedDate = Conversion.ToDate(sLastRestrictedDate);
//					else
//						dtLastRestrictedDate = DateTime.Now; 
//					
//					span = dtLastRestrictedDate - dtFirstRestrictedDate;
//					iTotalDays += span.Days + 1;
//				}
//				
//			}
//			catch(RMAppException p_objRMAppException)
//			{
//				throw p_objRMAppException; 
//			}
//			catch(Exception p_objException)
//			{
//				throw new FDMException(Globalization.GetString("FormDataAdaptor.PiRestrictionForm.GetTotalRestrictedDays.Exception"),p_objException);
//			}
//			finally
//			{
//				if(objDbReader!=null)
//				{
//					objDbReader.Close();
//					objDbReader.Dispose();
//					objDbReader = null;
//				}
//			}
//
//			return iTotalDays; 
//			
//		}

        /// <summary>
        /// MITS 19904 : retrofit for restrictions : RAMAN BHATIA 07/26/2010
        /// The logic to validate DateFirstRestricted and DateLastRestricted is that all date restricted periods can not overlap. 
        /// 1. DateFirstRestricted <= Today
        /// 2. DateLastRestricted >= DateFirstRestricted
        /// 3. Any DateFirstRestricted should be later than previous Restriction DateLastRestricted
        /// 4. Any DateLastRestricted should be earlier than next Restriction DateFirstRestricted
        /// 5. If there is one open end restriction period x (DateLastRestricted is blank), no new dateLoss record with
        ///     DateFirstRestricted > DateFirstRestricted(x) can be created
        /// 6. If Date Last Restricted is blank and open end, set to 99991231
        /// </summary>
        /// <param name="Cancel"></param>
        public override void OnValidate(ref bool Cancel)
        {
            string sDateOpenEnd = "99991231";
            string sDateOpenEndMessage = "(blank)";

            string sDateFirstRestrct = PiXRestrict.DateFirstRestrct;
            string sDateLastRestrct = PiXRestrict.DateLastRestrct;
            string sDateLastRestrctMessage = sDateLastRestrct;
            string sLangCode = base.LanguageCode;
            if (string.IsNullOrEmpty(sDateLastRestrct))
            {
                sDateLastRestrct = sDateOpenEnd;
                sDateLastRestrctMessage = sDateOpenEndMessage;
            }
            else
            {
                sDateLastRestrctMessage = Conversion.ToDate(sDateLastRestrct).ToShortDateString();
            }

            string sToday = Conversion.ToDbDate(System.DateTime.Now);

            //Start rsushilaggar MITS 23694
            ////First restricted date can not be future date
            //if (sDateFirstRestrct.CompareTo(sToday) > 0)
            //{
            //    Errors.Add(Globalization.GetString("ValidationError"),
            //        String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate"), Globalization.GetString("Field.DateLastWorked"), System.DateTime.Now.ToShortDateString()),
            //        BusinessAdaptorErrorType.Error);

            //    Cancel = true;
            //    return;
            //}

            //first restricted date should not be later than last restricted date
            if (sDateFirstRestrct.CompareTo(sDateLastRestrct) > 0)
            {
                //rsharma220 MITS 31580
                Errors.Add(CommonFunctions.FilterBusinessMessageRetainErrorCode(Globalization.GetString("ValidationError", base.ClientId, sLangCode), sLangCode),
                    String.Format(CommonFunctions.FilterBusinessMessageRetainErrorCode(Globalization.GetString("Validation.MustBeLessThanLastRestricted", base.ClientId, sLangCode), sLangCode), CommonFunctions.FilterBusinessMessageRetainErrorCode(Globalization.GetString("Field.DateFirstRestricted", base.ClientId, sLangCode), sLangCode)),
                    BusinessAdaptorErrorType.Error);

                Cancel = true;
                return;
            }

            if (PiXRestrict.Parent == null)
                PiXRestrict.LoadParent();

            PiEmployee objEmployee = PiXRestrict.Parent as PiEmployee;
            PiXRestrictList objPiRestrictList = objEmployee.PiXRestrictList;

            //Add all the restriction entries to the SortedList and set open end restriction date to 
            //a special value
            SortedList<string, string> slRestrctDays = new SortedList<string, string>();
            foreach (PiXRestrict wl in objPiRestrictList)
            {
                if (PiXRestrict.PiRestrictRowId != wl.PiRestrictRowId)
                {
                    if (string.IsNullOrEmpty(wl.DateLastRestrct))
                        slRestrctDays.Add(wl.DateFirstRestrct, sDateOpenEnd);
                    else
                        slRestrctDays.Add(wl.DateFirstRestrct, wl.DateLastRestrct);
                }
            }

            //No two restriction period could have same last work date
            if (slRestrctDays.ContainsKey(sDateFirstRestrct))
            {
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                    String.Format(Globalization.GetString("Validation.DateFirstRestrictedMustNotBeDuplicated", base.ClientId, sLangCode), Conversion.ToDate(sDateFirstRestrct).ToShortDateString()),
                    BusinessAdaptorErrorType.Error);
                Cancel = true;
                return;
            }

            //No two WorkLoss period could have same last restricted date
            if (slRestrctDays.ContainsValue(sDateLastRestrct))
            {
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                    String.Format(Globalization.GetString("Validation.DateLastRestrictedMustNotBeDeplicated", base.ClientId, sLangCode), sDateLastRestrctMessage),
                    BusinessAdaptorErrorType.Error);

                Cancel = true;
                return;
            }

            slRestrctDays.Add(sDateFirstRestrct, sDateLastRestrct);
            int iCount = slRestrctDays.Count;

            //If this is the only entry, just return
            if (iCount <= 1)
                return;

            int iKeyIndex = slRestrctDays.IndexOfKey(sDateFirstRestrct);

            //this last work date should be later than previous workloss's return to work date
            if (iKeyIndex > 0)
            {
                if (sDateFirstRestrct.CompareTo(slRestrctDays.Values[iKeyIndex - 1]) <= 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        string.Format(Globalization.GetString("Validation.FirstRestricteddDateError", base.ClientId, sLangCode), Conversion.ToDate(sDateFirstRestrct).ToShortDateString()),
                        BusinessAdaptorErrorType.Error);

                    Cancel = true;
                    return;
                }
            }

            //this last restricted date should be earlier than next workloss's first restricted date
            if (iKeyIndex < iCount - 1)
            {
                if (sDateLastRestrct.CompareTo(slRestrctDays.Keys[iKeyIndex + 1]) >= 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        string.Format(Globalization.GetString("Validation.LastRestrictedDateError", base.ClientId, sLangCode), sDateLastRestrctMessage),
                        BusinessAdaptorErrorType.Error);

                    Cancel = true;
                    return;
                }
            }
        }

        //public override void OnValidate(ref bool Cancel)
        //{
        //    bool bError = false;

        //    // Perform data validation
        //    if(PiXRestrict.DateLastRestrct.Length>0)
        //    {
        //        if(PiXRestrict.DateFirstRestrct.CompareTo(PiXRestrict.DateLastRestrct)>0)	
        //        {
        //            Errors.Add(Globalization.GetString("ValidationError"),
        //                        String.Format(Globalization.GetString("Validation.MustBeLessThan"), 
        //                                        Globalization.GetString("Field.DateFirstRestrct"), 
        //                                        Globalization.GetString("Field.DateLastRestrct")),
        //                        BusinessAdaptorErrorType.Error);

        //            bError = true;

        //        }
        //    }

        //    if (PiXRestrict.Parent==null)
        //        PiXRestrict.LoadParent();
        //    int iRestrictId = 0;
        //    PiEmployee objEmployee=PiXRestrict.Parent as PiEmployee;
        //    PiXRestrictList objPiRestrictList =	objEmployee.PiXRestrictList;			
        //    if(objEmployee.PiXRestrictList.Count>0)
        //    {
        //        foreach(PiXRestrict objRestrict in objPiRestrictList)
        //        {
        //            iRestrictId = objRestrict.PiRestrictRowId;
        //            //If and else if conditions added by Shivendu for MITS 14230
        //            if (PiXRestrict.PiRestrictRowId == 0)
        //                break;
        //            else if (iRestrictId < PiXRestrict.PiRestrictRowId)
        //                break;
        //        }
        //       if (iRestrictId != PiXRestrict.PiRestrictRowId)
        //        {
        //            DateTime dtFirstRestrict = Conversion.ToDate(PiXRestrict.DateFirstRestrct);
        //            DateTime dtLastRestrict = Conversion.ToDate(objPiRestrictList[iRestrictId].DateLastRestrct);
        //            if(dtFirstRestrict.CompareTo(dtLastRestrict)<=0)
        //            {
        //                Errors.Add(Globalization.GetString("ValidationError"),
        //                    String.Format(Globalization.GetString("Validation.MustBeGreaterThanLastRstcDate"), 
        //                    Globalization.GetString("Field.DateFirstRestrct"), 
        //                    Conversion.ToDate(objPiRestrictList[iRestrictId].DateLastRestrct).ToShortDateString()),
        //                    BusinessAdaptorErrorType.Error);
        //                bError = true;
        //            }
        //        }
        //    }
        //    // Return true if there were validation errors
        //    Cancel = bError;
        //}

	/*	public override void BeforeSave(ref bool Cancel)
		{
			XmlDocument objSysExDataXmlDoc = base.SysEx;
			XmlNode objPiRowIdNode = null;
			try{objPiRowIdNode = objSysExDataXmlDoc.SelectSingleNode("/SysExData/PiRowId");}
			catch{};
			
			if(objPiRowIdNode !=null)
				if(this.PiXRestrict.PiRowId==0)
					this.PiXRestrict.PiRowId = Conversion.ConvertStrToInteger(objPiRowIdNode.InnerText);

			objPiRowIdNode = null;
			objSysExDataXmlDoc = null;

			base.BeforeSave (ref Cancel);		

		}*/
	}
}
