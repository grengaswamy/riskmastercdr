using System;
using Riskmaster.DataModel;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Db; 
namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for AdminTrackingList.
	/// </summary>
	public class AdminTrackingListForm: ListFormBase
	{
		const string CLASS_NAME = "ADMTableList";

		public string SerializeTableList()
		{
			//BSB Hack to fake a row id for the current screen.
			base.ResetSysExData("RowId","0");
			ADMTableList objList = base.m_fda.Factory.GetDataModelObject(CLASS_NAME,false) as ADMTableList;
			return ApplyPowerViews(objList.SerializeTableList());
		}

		public AdminTrackingListForm(FormDataAdaptor fda):base(fda)
		{
//			this.objDataList = fda.Factory.GetDataModelObject(CLASS_NAME,false) as DataCollection;
		}

		//Pankaj 24-Dec-05 TR#479
		//This will remove the Adm Tables from the list for which the powerview setup 
		//has not been done. This is the case only when the system is currently under a power view.
		//For non-pv it will show all the adm tables
		private string ApplyPowerViews(string p_sTableList)
		{
			int iViewId=0;
			DbReader objRdr=null;
			const string XHTML_NAMESPACE="http://www.w3.org/1999/xhtml";
			XmlDocument objAdmLst=new XmlDocument(); 
			XmlNamespaceManager nsNamespace = null;

			objAdmLst.LoadXml(p_sTableList); 
			nsNamespace = new XmlNamespaceManager(objAdmLst.NameTable);
			nsNamespace.AddNamespace("xhtml", XHTML_NAMESPACE);
			//Check if this is a powerview. Powerview for admlist being stored in xhtml body
			XmlNode objBody = this.SysView.SelectSingleNode("//xhtml:body",nsNamespace);
			if(objBody!=null)
				iViewId = Conversion.ConvertStrToInteger(((XmlElement)objBody).GetAttribute("viewid"));
            if(iViewId==0)
				return p_sTableList;

			foreach(XmlNode objNod in objAdmLst.SelectNodes("//SystemTableName"))
			{
                using (objRdr = base.m_fda.Factory.Context.DbConn.ExecuteReader(
                    "SELECT FORM_NAME FROM NET_VIEW_FORMS WHERE VIEW_ID = " + iViewId + " AND FORM_NAME= 'admintracking|" + objNod.InnerText + ".xml'"))
                {

                    if (!objRdr.Read())
                        //remove node for which pv setup is not done
                        objNod.ParentNode.ParentNode.RemoveChild(objNod.ParentNode);
                }
                    //objRdr.Dispose();
			}
			return objAdmLst.OuterXml; 
		}
	}
}