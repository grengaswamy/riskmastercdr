using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;


namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for EventScreen.
	/// </summary>
	public class EventInterventionForm : DataEntryFormBase
	{
		const string CLASS_NAME = "EventXIntervention";

		private EventXIntervention objEventXInter{get{return objData as EventXIntervention;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}

		public override void InitNew()
		{
			base.InitNew();
			objEventXInter.EventId = base.GetSysExDataNodeInt("EventId");
			if(objEventXInter.EventId >0)
				(objData as INavigation).Filter = "EVENT_ID=" + objEventXInter.EventId;

		}
		public EventInterventionForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}
		public override void OnUpdateForm()
		{
			base.OnUpdateForm ();
			string sCaption = "";
			XmlNode elt = null;
			try{elt = base.SysEx.GetElementsByTagName("EventNumber")[0];}
			catch{};
			
			if(elt!=null)
				sCaption= "  (" + elt.InnerText +")" ;
			if(objEventXInter.IsNew==true)
			{
			}
			//Pass this subtitle value to view (ref'ed from @valuepath).
			base.ResetSysExData("SubTitle",sCaption);
		}

	}
}
