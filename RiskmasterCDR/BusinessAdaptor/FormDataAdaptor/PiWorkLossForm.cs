﻿
using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

//TODO: Auto-Generated Form - migrate and verify behaviors from Legacy RMNet form.

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for PiWorkLoss Screen.
	/// </summary>
	public class PiWorkLossForm : DataEntryFormBase
	{
		const string CLASS_NAME = "PiXWorkLoss";
		private PiXWorkLoss PiXWorkLoss{get{return objData as PiXWorkLoss;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		const string FILTER_KEY_NAME = "PiRowId";
		private string m_sEventDate = string.Empty;
		private int m_iStateId = 0;
        private string sLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").Split('|')[0].ToString();
		public struct Holidays
		{
			internal string sDate;
			internal string sDesc;
			internal long[] lOrgID;
		}
		public Holidays[] SYS_HOLIDAYS; 

		public override void InitNew()
		{
			base.InitNew(); 
            //parijat : Mits 9937
            if (base.m_fda.SafeFormVariableParamText("SysCmd") != "0" || base.m_fda.SafeParamText("SysFormId") == "0")
            {
                this.m_ParentId = base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME, false);
                (objData as INavigation).Filter =
                    objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + this.m_ParentId.ToString();
                this.PiXWorkLoss.PiRowId = this.m_ParentId;
            }
		}

		public override void Init()
		{
			base.Init();
			//parijat : Mits 9937
            if (base.m_fda.SafeFormVariableParamText("SysCmd") != "0" || base.m_fda.SafeParamText("SysFormId") == "0")
            {
                int iEventId = base.GetSysExDataNodeInt("/SysExData/EventId", true);
                Event objEvent = (Event)this.PiXWorkLoss.Context.Factory.GetDataModelObject("Event", false);
                objEvent.MoveTo(iEventId);
                this.m_sEventDate = objEvent.DateOfEvent;
                this.m_iStateId = objEvent.StateId;
                objEvent = null;
            }
            //Bijender has changed for Mits 14124
            else
            {
                //Bijender has written for Mits 16037
                if (base.GetSysExDataNodeText("/SysExData/PiRowId", true) == "")
                {
                    this.PiXWorkLoss.PiRowId = GetPiRowID();
                }
                else
                {
                    this.PiXWorkLoss.PiRowId = Convert.ToInt32(base.GetSysExDataNodeText("/SysExData/PiRowId", true));
                }
                //this.PiXWorkLoss.PiRowId = Convert.ToInt32(base.GetSysExDataNodeText("/SysExData/PiRowId", true));
                //Bijender Ends Mits 16037

                PersonInvolved objPersonInv = (PersonInvolved)this.PiXWorkLoss.Context.Factory.GetDataModelObject("PersonInvolved", false);
                objPersonInv.MoveTo(this.PiXWorkLoss.PiRowId);
                int iEvent = objPersonInv.EventId;
                Event oEvent = (Event)this.PiXWorkLoss.Context.Factory.GetDataModelObject("Event", false);
                oEvent.MoveTo(iEvent);
                this.m_sEventDate = oEvent.DateOfEvent;
            }
            //End Mits 14124
		}

        //Bijender has written for Mits 16037
        public int GetPiRowID()
        {
            string sSQL= string.Empty;
            int iPiRowId = 0;
            DbReader objDbReader = null;            

            string sRecordID= base.m_fda.SafeParam("SysFormId").InnerText;
            try
            {
            sSQL = "select pi_row_id from  pi_x_work_loss where pi_wl_row_id=" + sRecordID.ToString() ;
					

				objDbReader = this.PiXWorkLoss.Context.DbConnLookup.ExecuteReader(sSQL);     

				while(objDbReader.Read())
				{
                    iPiRowId = objDbReader.GetInt32("pi_row_id"); 				
					
				}
				
			}
			catch(RMAppException p_objRMAppException)
			{
				throw p_objRMAppException;
			}
			catch(Exception p_objException)
			{
                throw new FDMException(Globalization.GetString("FormDataAdaptor.PiWorkLossForm.GetTotalWorkLossInfo.Exception", base.ClientId, sLangCode), p_objException);
			}
			finally
			{
				if(objDbReader!=null)
				{
					objDbReader.Close();
					objDbReader.Dispose();
					objDbReader = null;
				}
			}

            return iPiRowId;
        }
        //Bijender End Mits 16037

		public PiWorkLossForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
            sLangCode = Convert.ToString(fda.userLogin.objUser.NlsCode);
		}
		
		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
			base.OnUpdateForm ();

			// Subtitle node should already be present in SysExData
			// as this screen always opens from PiEmployee, so we would reuse its value.
            //rsushilaggar MITS 37986
            Event objEvent = (Event)this.PiXWorkLoss.Context.Factory.GetDataModelObject("Event", false);
            PersonInvolved objPI = (PersonInvolved)this.PiXWorkLoss.Context.Factory.GetDataModelObject("PersonInvolved", false);   
            int iTotalDuration = 0;
			int iStateDuration = 0;
			int iOshaDuration = 0;
            string sEventDate = string.Empty;
            int iEventId = 0;
            
            //iEventId = base.GetSysExDataNodeInt("/SysExData/PIRowId", true);
            
            
            //Bijender has written for Mits 16037
            //string sPiRowId = base.GetSysExDataNodeText("/SysExData/PiRowId",true);
            string sPiRowId = string.Empty;
            if (base.GetSysExDataNodeText("/SysExData/PiRowId", true) == "")
            {
                 sPiRowId = GetPiRowID().ToString();
            }
            else
            {
                 sPiRowId = base.GetSysExDataNodeText("/SysExData/PiRowId", true);
            }
            //Bijender Ends Mits 16037

            //rsushilaggar MITS 37986
            objPI.MoveTo(Convert.ToInt32(sPiRowId));
            objEvent.MoveTo(objPI.EventId);
            sEventDate = Conversion.ToDate(objEvent.DateOfEvent).ToString("MM/dd/yyyy");
			
			GetTotalWorkLossInfo(Conversion.ConvertStrToInteger(sPiRowId),
									sEventDate,
									out iTotalDuration,
									out iStateDuration,
									out iOshaDuration);
		

			// If the event happened after Osha 300 rules came into effect
			if(Convert.ToDateTime(sEventDate)<Convert.ToDateTime("01/01/2002"))
			{
				iOshaDuration = 0;
			}

			base.ResetSysExData("TotalDuration",iTotalDuration.ToString()); 
			base.ResetSysExData("StateDuration",iStateDuration.ToString());   
			base.ResetSysExData("OshaDuration",iOshaDuration.ToString());   

			// Reset the value of 'CalculateDurationsFlag' each time
			base.ResetSysExData("CalculateDurationsFlag","0");
            ArrayList singleRow = null;
            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singleRow, "id", "totwloss");
            base.AddElementToList(ref singleRow, "Width", "70%");
            base.m_ModifiedControls.Add(singleRow);

            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singleRow, "id", "totstateduration");
            base.AddElementToList(ref singleRow, "Width", "70%");
            base.m_ModifiedControls.Add(singleRow);
            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singleRow, "id", "totoshaduration");
            base.AddElementToList(ref singleRow, "Width", "70%");
            base.m_ModifiedControls.Add(singleRow);
		}

		public override void OnUpdateObject()
		{
			base.OnUpdateObject ();
			
			int iCalculateDurationsFlag = base.GetSysExDataNodeInt("/SysExData/CalculateDurationsFlag",true);
 
			if(iCalculateDurationsFlag==-1)
			{
				//string sWorkFlags = base.GetSysExDataNodeText("/SysExData/WorkFlags",true); 
                string sWorkFlags = string.Empty;
                if (PiXWorkLoss.Parent == null)
                    PiXWorkLoss.LoadParent();
                PiEmployee objEmployee = PiXWorkLoss.Parent as PiEmployee;
                if (objEmployee != null)
                {
                    sWorkFlags = objEmployee.WorkSunFlag + ","
                        + objEmployee.WorkMonFlag + ","
                        + objEmployee.WorkTueFlag + ","
                        + objEmployee.WorkWedFlag + ","
                        + objEmployee.WorkThuFlag + ","
                        + objEmployee.WorkFriFlag + ","
                        + objEmployee.WorkSatFlag;
            }
				int iDeptAssignedEid = 
					base.GetSysExDataNodeInt("/SysExData/DeptAssignedEid/@codeid",true);
			
				int iStateId = 	this.m_iStateId ;  
		
				int iDuration = 0;
				int iStateDuration = 0;

				CalculateWorkLoss(	this.PiXWorkLoss.DateLastWorked,
									this.PiXWorkLoss.DateReturned,
									sWorkFlags,
									iDeptAssignedEid,
									PiXWorkLoss.Context.InternalSettings.SysSettings.ExclHolidays,    
									iStateId,
									out iDuration,
									out iStateDuration);

				this.PiXWorkLoss.Duration = iDuration;
				this.PiXWorkLoss.StateDuration = iStateDuration;
			}			
		}

		private int CalculateWorkLoss(	string sDateLastWorked,
										string sDateReturned,
										string sWorkFlags,
										int iDeptAssignedEid,
										bool bExcludeHolidaysFlag,
										int iStateId,
										out int iDuration,
										out int iStateDuration)
		{
			int iReturnValue = 0;
			iDuration = 0;
			iStateDuration = 0;

			string sSeparator = ",";
			string[] arrWorkingDays;
			string sSQL = string.Empty; 
			//Hashtable htHolidays = new Hashtable(); 

			DbReader objDbReader = null;
			int iStateDurationCalcOption = 0;

			try
			{
				arrWorkingDays = sWorkFlags.Split(sSeparator.ToCharArray());
//				GetDayCount(sDateLastWorked,
//							sDateReturned,
//							arrWorkingDays,
//							iDeptAssignedEid,
//							bExcludeHolidaysFlag,
//							out iDuration);
				GetWorkLossDayCount(Conversion.ToDate(sDateLastWorked),Conversion.ToDate(sDateReturned),arrWorkingDays,out iDuration);

				iStateDurationCalcOption = 
					this.PiXWorkLoss.Context.DbConnLookup.ExecuteInt("SELECT DUR_CALC_OPTION FROM STATES WHERE STATE_ROW_ID=" + iStateId); 

				switch(iStateDurationCalcOption)
				{
					case 1:
						// Count all days
//						for(int index=0; index<7; index++)
//						{
//							arrWorkingDays[index] = "True" ;
//						}
//
//						GetDayCount(sDateLastWorked,
//									sDateReturned,
//									arrWorkingDays,
//									iDeptAssignedEid,
//									bExcludeHolidaysFlag,
//									htHolidays,
//									out iStateDuration);
						TimeSpan objTime;
						objTime=Conversion.ToDate(sDateReturned).Subtract(Conversion.ToDate(sDateLastWorked));
						iStateDuration = objTime.Days;
						break;					
					case 2:
						// Count WeekDays only
						for(int index=1; index<6; index++)
						{
							arrWorkingDays[index] = "True" ;
						}
						arrWorkingDays[0]="False";
						arrWorkingDays[6]="False";
//						GetDayCount(sDateLastWorked,
//									sDateReturned,
//									arrWorkingDays,
//									iDeptAssignedEid,
//									bExcludeHolidaysFlag,
//									out iStateDuration);
						GetWorkLossDayCount(Conversion.ToDate(sDateLastWorked),Conversion.ToDate(sDateReturned),arrWorkingDays,out iStateDuration);

						break;
                    case 3: //Mona:Mits: 23626 :Added case for 6 day week for Illinois
                        // Count WeekDays only
                        for (int index = 1; index <= 6; index++)
                        {
                            arrWorkingDays[index] = "True";
                        }
                        arrWorkingDays[0] = "False";                        
                        
                        GetWorkLossDayCount(Conversion.ToDate(sDateLastWorked), Conversion.ToDate(sDateReturned), arrWorkingDays, out iStateDuration);

                        break;
					default:
						iStateDuration = iDuration;
						break;
	
				}

			}
			catch(RMAppException p_objRMAppException)
			{
				throw p_objRMAppException;
			}
			catch(Exception p_objException)
			{
                throw new FDMException(Globalization.GetString("FormDataAdaptor.PiWorkLossForm.CalculateWorkLoss.Exception", base.ClientId, sLangCode), p_objException);
			}
			finally
			{
				if(objDbReader!=null)
				{
					objDbReader.Close();
					objDbReader.Dispose();
					objDbReader = null;
				}

				//htHolidays = null;
				arrWorkingDays = null;
			} 

			return iReturnValue;

		}
		private int GetWorkLossDayCount(DateTime sLastWorkDate, DateTime sReturnedToWorkDate, string[] sDaysOfWeek,out int iDayCount)
		{
			DateTime objLastWorkedDate=System.DateTime.Today;
			DateTime objReturnedToWorkDate=System.DateTime.Today;
			TimeSpan objDays;
			int iDayOfWeekStart=0;
			int iDayOfWeekEnd=0;
			int iNewDays=0;
			int iDays=0;
			bool bCount;
			int iReturnEid=0;
			string sParent=string.Empty;
            //Mgaba2:MITS 15484:Exclude Holidays were not coming into effect
            //Getting department id from database rather from parent screen:start
            //int iDeptAssignedEid = 
            //    base.GetSysExDataNodeInt("/SysExData/DeptAssignedEid",true); 
            DbReader objDbReader = null;
            int iDeptAssignedEid = 0;
            string sSQL = "SELECT DEPT_ASSIGNED_EID FROM PERSON_INVOLVED WHERE PI_ROW_ID=" + PiXWorkLoss.PiRowId.ToString();
            objDbReader = this.PiXWorkLoss.Context.DbConnLookup.ExecuteReader(sSQL);
            while (objDbReader != null && objDbReader.Read())
            {
                iDeptAssignedEid = objDbReader.GetInt("DEPT_ASSIGNED_EID");
            }
            objDbReader.Close();
            //Mgaba2:MITS 15484:End
			objLastWorkedDate=sLastWorkDate.AddDays(1);
			objReturnedToWorkDate=sReturnedToWorkDate;
			objDays=sReturnedToWorkDate.Subtract(objLastWorkedDate);
			iDays=objDays.Days;
			iDayOfWeekStart = (int)objLastWorkedDate.DayOfWeek ;
			iDayOfWeekEnd=(int)objReturnedToWorkDate.DayOfWeek ;
			subInitHolidayInfo();
			for(int i=1; i<=iDays; i++)
			{
				if(sDaysOfWeek[(int) objLastWorkedDate.AddDays(i-1).DayOfWeek]=="True")
				{
					if(PiXWorkLoss.Context.InternalSettings.SysSettings.ExclHolidays==true && iDeptAssignedEid!=0)
					{
						bCount=false;
						for(int j=0; j<=SYS_HOLIDAYS.Length-1; j++)
						{
							if(objLastWorkedDate.AddDays(i-1).ToShortDateString()== Conversion.ToDate(SYS_HOLIDAYS[j].sDate).ToShortDateString())
							{
								for(int k=0; k<= SYS_HOLIDAYS[j].lOrgID.Length-1; k++)
								{
									iReturnEid=0;
									sParent=PiXWorkLoss.Context.InternalSettings.CacheFunctions.GetOrgParent(
										iDeptAssignedEid,PiXWorkLoss.Context.LocalCache.GetOrgTableId(Conversion.ConvertObjToInt(SYS_HOLIDAYS[j].lOrgID[k], base.ClientId)),0,ref iReturnEid);
									if(SYS_HOLIDAYS[j].lOrgID[k]== iReturnEid)
										bCount=true;
								}
							}
						}
						if(bCount==false)
							iNewDays=iNewDays+1;
					}
					else
					{
						iNewDays=iNewDays+1;
					}
				}
			}
			iDayCount=iNewDays;
			return iDayCount;
		}
//		private int GetDayCount(string sDateFrom,
//								string sDateTo,
//								string[] arrWorkingDays,
//								int iDeptEid,
//								bool bExcludeHolidaysFlag,
//								out int iDayCount)
//		{
//			int iReturnValue = 0;
//			iDayCount = 0;
//			bool bCount=false;
//
//			DateTime dtDateFrom = Conversion.ToDate(sDateFrom);
//			DateTime dtDateTo = Conversion.ToDate(sDateTo); 
//
//			dtDateTo = dtDateTo.AddDays(-1);  
//			subInitHolidayInfo();
//			while(dtDateFrom<dtDateTo)
//			{
//				dtDateFrom = dtDateFrom.AddDays(1);
//
//				if(arrWorkingDays[(int) dtDateFrom.DayOfWeek].ToString().CompareTo("True")==0)
//				{
//					if(bExcludeHolidaysFlag && iDeptEid>0)
//					{
//						bCount=false;
//						for(int j=0; j<=SYS_HOLIDAYS.GetUpperBound(SYS_HOLIDAYS.Length-1); j++)
//						{
//							if(dtDateFrom.AddDays(i-1).ToShortDateString()== SYS_HOLIDAYS[j].sDate)
//							{
//								for(int k=0; k<= SYS_HOLIDAYS.GetUpperBound(SYS_HOLIDAYS.Length-1); k++)
//								{
//									iReturnEid=0;
//									sParent=PiXRestrict.Context.InternalSettings.CacheFunctions.GetOrgParent(0,Conversion.ConvertObjToInt(SYS_HOLIDAYS[j].lOrgID[k], base.ClientId),0,ref iReturnEid);
//									if(SYS_HOLIDAYS[j].lOrgID[k]== iReturnEid)
//										bCount=true;
//								}
//							}
//						}
//						if(bCount==false)
//							iNewDays=iNewDays+1;
////						if(!htHolidays.ContainsKey(Conversion.ToDbDate(dtDateFrom)))
////						{
////							iDayCount++;
////						}
//					}
//					else
//					{
//						iDayCount++;
//					}
//				}
//			}			
//
//			return iReturnValue;
//		}


		private int GetTotalWorkLossInfo(	int iPiRowId,
											string sEventDate,
											out int iTotalDuration,
											out int iStateDuration,
											out int iOshaDuration)
		{
			int iReturnValue = 0;
			iTotalDuration = 0;
			iStateDuration = 0;
			iOshaDuration = 0;

			string sSQL = string.Empty;

			DbReader objDbReader = null;
			DateTime dtStartDate;
			DateTime dtEndDate;
			DateTime dtEventDate;
			TimeSpan span;
			string sEndDate = string.Empty; 

			try
			{
				dtEventDate = DateTime.Parse(sEventDate);

				sSQL = "SELECT PI_WL_ROW_ID, DATE_LAST_WORKED, DATE_RETURNED, DURATION, STATE_DURATION "
					+ " FROM PI_X_WORK_LOSS "
					+ " WHERE PI_ROW_ID = " + iPiRowId.ToString()  
					+ " ORDER BY DATE_LAST_WORKED, DATE_RETURNED";

				objDbReader = this.PiXWorkLoss.Context.DbConnLookup.ExecuteReader(sSQL);     

				while(objDbReader.Read())
				{
					dtStartDate = Conversion.ToDate(objDbReader.GetString("DATE_LAST_WORKED")); 
					sEndDate = objDbReader.GetString("DATE_RETURNED");
					
					if(sEndDate.Length>0)
						dtEndDate = Conversion.ToDate(sEndDate);
					else
						dtEndDate = DateTime.Now; 
					if(DateTime.Compare(dtStartDate,dtEventDate)<0)
					{
						dtStartDate = dtEventDate;
					}
					if(DateTime.Compare(dtStartDate,dtEndDate)<0)
					{
						span = dtEndDate - dtStartDate;
						int iDays=span.Days-1;
						if(iDays>0)
							iOshaDuration += iDays;
					}
					iTotalDuration += objDbReader.GetInt("DURATION"); 
					iStateDuration += objDbReader.GetInt("STATE_DURATION"); 
				}
				
			}
			catch(RMAppException p_objRMAppException)
			{
				throw p_objRMAppException;
			}
			catch(Exception p_objException)
			{
                throw new FDMException(Globalization.GetString("FormDataAdaptor.PiWorkLossForm.GetTotalWorkLossInfo.Exception", base.ClientId, sLangCode), p_objException);
			}
			finally
			{
				if(objDbReader!=null)
				{
					objDbReader.Close();
					objDbReader.Dispose();
					objDbReader = null;
				}
			}

			return iReturnValue;
		}


        /// <summary>
        /// MITS 19904
        /// The logic to validate DateLastWorked and DateReturned is that all WorkLoss periods can not overlap. 
        /// 1. DateLastWorked <= Today
        /// 2. DateReturned >= DateLastWorked
        /// 3. Any DateLastWorked should be later than previous WorkLoss DateReturned
        /// 4. Any DateReturned should be earlier than next WorkLoss DateLastWorked
        /// 5. If there is one open end dataLoss period x (DateReturned is blank), no new dateLoss record with
        ///     DateLastWorked > DateLastWorked(x) can be created
        /// 6. If Return to work is blank and open end, set to 99991231
        /// </summary>
        /// <param name="Cancel"></param>
		public override void OnValidate(ref bool Cancel)
		{
            string sDateOpenEnd = "99991231";
            string sDateOpenEndMessage = "(blank)";

            string sDateLastWorked = PiXWorkLoss.DateLastWorked;
            string sDateReturnToWork = PiXWorkLoss.DateReturned;
            string sDateReturnToWorkMessage = sDateReturnToWork;
            string sDateofDisability = PiXWorkLoss.DateofDisability;
            string sDateReleasedtoReturnToWork = PiXWorkLoss.DateReleasedtoReturnToWork;
            string sLangCode = base.LanguageCode;
            if (string.IsNullOrEmpty(sDateReturnToWork))
            {
                sDateReturnToWork = sDateOpenEnd;
                sDateReturnToWorkMessage = sDateOpenEndMessage;
            }
            else
            {
                sDateReturnToWorkMessage = Conversion.ToDate(sDateReturnToWork).ToShortDateString();
            }

			string sToday = Conversion.ToDbDate(System.DateTime.Now);

            //Start rsushilaggar MITS 23694
            //Last work date can not be future date
            //if(sDateLastWorked.CompareTo(sToday)>0)
            //{
            //    Errors.Add(Globalization.GetString("ValidationError"),
            //        String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate"), Globalization.GetString("Field.DateLastWorked"), System.DateTime.Now.ToShortDateString()),
            //        BusinessAdaptorErrorType.Error);

            //    Cancel = true;
            //    return;
            //}
            //End rsushilaggar
            //gdass2 MITS 24474 start
            //rsharma220 JIRA 3837 start
            if (!string.IsNullOrEmpty(sDateofDisability))
            {
                if (sDateLastWorked.CompareTo(sDateofDisability) > 0)
                {
                    //rsharma220 MITS 31580
                    Errors.Add(CommonFunctions.FilterBusinessMessageRetainErrorCode(Globalization.GetString("ValidationError", base.ClientId, sLangCode), sLangCode),
                         String.Format(CommonFunctions.FilterBusinessMessageRetainErrorCode(Globalization.GetString("Validation.MustBeLessThanDisability", base.ClientId, sLangCode), sLangCode), CommonFunctions.FilterBusinessMessageRetainErrorCode(Globalization.GetString("Field.DateLastWorked", base.ClientId, sLangCode), sLangCode)),
                         BusinessAdaptorErrorType.Error);

                    Cancel = true;
                    return;
                }
                if (!string.IsNullOrEmpty(sDateReturnToWork))
                {
                    if (sDateReturnToWork.CompareTo(sDateofDisability) <= 0)
                    {
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId,sLangCode),
                             String.Format(Globalization.GetString("Validation.MustBeGreater", base.ClientId, sLangCode), Globalization.GetString("Field.DateReturned", base.ClientId, sLangCode), Globalization.GetString("Field.DateofDisability", base.ClientId, sLangCode)),
                             BusinessAdaptorErrorType.Error);

                        Cancel = true;
                        return;
                    }
                }
            }
            if (!string.IsNullOrEmpty(sDateReleasedtoReturnToWork))
            {
                if (sDateReleasedtoReturnToWork.CompareTo(sDateLastWorked) < 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId,sLangCode),
                         String.Format(Globalization.GetString("Validation.MustBeGreater", base.ClientId, sLangCode), Globalization.GetString("Field.DateReleasedtoReturnToWork", base.ClientId, sLangCode), Globalization.GetString("Field.DateLastWorked", base.ClientId, sLangCode)),
                         BusinessAdaptorErrorType.Error);

                    Cancel = true;
                    return;
                }
                if (!string.IsNullOrEmpty(sDateofDisability))
                {
                    if (sDateReleasedtoReturnToWork.CompareTo(sDateofDisability) < 0)
                    {
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId,sLangCode),
                             String.Format(Globalization.GetString("Validation.MustBeGreater", base.ClientId, sLangCode), Globalization.GetString("Field.DateReleasedtoReturnToWork", base.ClientId, sLangCode), Globalization.GetString("Field.DateofDisability", base.ClientId, sLangCode)),
                             BusinessAdaptorErrorType.Error);

                        Cancel = true;
                        return;
                    }
                }
                if (!string.IsNullOrEmpty(sDateReturnToWork))
                {
                    if (sDateReleasedtoReturnToWork.CompareTo(sDateReturnToWork) > 0)
                    {
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                             String.Format(Globalization.GetString("Validation.MustBeLessThan", base.ClientId, sLangCode), Globalization.GetString("Field.DateReleasedtoReturnToWork", base.ClientId, sLangCode), Globalization.GetString("Field.DateReturned", base.ClientId, sLangCode)),
                             BusinessAdaptorErrorType.Error);

                        Cancel = true;
                        return;
                    }
                }
            }//gdass2 MITS 24474 end
            //Last work date should not be later than return to work date
			if(sDateLastWorked.CompareTo(sDateReturnToWork)>0)
			{
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId,sLangCode),
                    String.Format(Globalization.GetString("Validation.MustBeLessThan", base.ClientId, sLangCode), Globalization.GetString("Field.DateLastWorked", base.ClientId, sLangCode), Globalization.GetString("Field.DateReturned", base.ClientId, sLangCode)),
                    BusinessAdaptorErrorType.Error);

				Cancel = true;
                return;
			}
            //rsharma220 JIRA 3837 End
			if (PiXWorkLoss.Parent == null)
				PiXWorkLoss.LoadParent();

			PiEmployee objEmployee=PiXWorkLoss.Parent as PiEmployee;
			PiXWorkLossList objPiWorkLossList =	objEmployee.PiXWorkLossList;

            //Add all the WorkLoss entry to the SortedList and set open end Return to work date to 
            //a special value
            SortedList<string, string> slWorkLoss = new SortedList<string, string>();
            foreach (PiXWorkLoss wl in objPiWorkLossList)
            {
                if (PiXWorkLoss.PiWlRowId != wl.PiWlRowId)
                {
                    if (string.IsNullOrEmpty(wl.DateReturned))
                        slWorkLoss.Add(wl.DateLastWorked, sDateOpenEnd);
                    else
                        slWorkLoss.Add(wl.DateLastWorked, wl.DateReturned);
                }
            }

            //No two WorkLoss period could have same last work date
            if (slWorkLoss.ContainsKey(sDateLastWorked))
            {
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                    String.Format(Globalization.GetString("Validation.DateLastWorkMustNotBeDuplicated", base.ClientId, sLangCode), Conversion.ToDate(sDateLastWorked).ToShortDateString()),
                    BusinessAdaptorErrorType.Error);
                Cancel = true;
                return;
            }

            //No two WorkLoss period could have same return to work date
            if (slWorkLoss.ContainsValue(sDateReturnToWork))
            {
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                    String.Format(Globalization.GetString("Validation.DateReturnedMustNotBeDeplicated", base.ClientId, sLangCode), sDateReturnToWorkMessage),
                    BusinessAdaptorErrorType.Error);

                Cancel = true;
                return;
            }

            slWorkLoss.Add(sDateLastWorked, sDateReturnToWork);
            int iCount = slWorkLoss.Count;

            //If this is the only entry, just return
            if (iCount <= 1)
                return;

            int iKeyIndex = slWorkLoss.IndexOfKey(sDateLastWorked);

            //this last work date should be later than previous workloss's return to work date
            if (iKeyIndex > 0)
            {
                // If Condition Changed by Amitosh for MITS 22539 (2/14/2011)
                //if (sDateLastWorked.CompareTo(slWorkLoss.Values[iKeyIndex - 1]) <= 0)
                if (sDateLastWorked.CompareTo(slWorkLoss.Values[iKeyIndex - 1]) < 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        string.Format(Globalization.GetString("Validation.LastWorkdDateError", base.ClientId, sLangCode), Conversion.ToDate(sDateLastWorked).ToShortDateString()),
                        BusinessAdaptorErrorType.Error);

                    Cancel = true;
                    return;
                }
            }

            //this return to work date should be earlier than next workloss's last work date
            if (iKeyIndex < iCount - 1)
            {
                if (sDateReturnToWork.CompareTo(slWorkLoss.Keys[iKeyIndex + 1]) >= 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        string.Format(Globalization.GetString("Validation.ReturnToWorkDateError", base.ClientId, sLangCode), sDateReturnToWorkMessage),
                        BusinessAdaptorErrorType.Error);

                    Cancel = true;
                    return;
                }
            }
		}

		#region Fill Up Holidays
		private void subInitHolidayInfo()
		{
			string sDate1=string.Empty;
			string sDate2=string.Empty;
			int i=0;
			int j=0;
			int count=1;

			DbReader objReader=null;
			DbReader objCount=null;
            try
            {
                objCount = DbFactory.GetDbReader(PiXWorkLoss.Context.DbConn.ConnectionString, "SELECT COUNT(*) COUNTI FROM SYS_HOLIDAYS");
                objReader = DbFactory.GetDbReader(PiXWorkLoss.Context.DbConn.ConnectionString, "SELECT HOL_DATE,ORG_EID FROM SYS_HOLIDAYS ORDER BY HOL_DATE,ORG_EID");
                if (objCount != null)
                {
                    while (objCount.Read())
                    {
                        count = Conversion.ConvertObjToInt(objCount.GetValue("COUNTI"), base.ClientId);
                        SYS_HOLIDAYS = new Holidays[count];
                    }
                }
                if (objReader != null)
                {
                    while (objReader.Read())
                    {

                        sDate1 = objReader.GetString("HOL_DATE");
                        if (sDate1 != sDate2)
                        {
                            if (sDate2 != "")
                                i = i + 1;
                            SYS_HOLIDAYS[i].sDate = sDate1;
                            j = 0;
                            SYS_HOLIDAYS[i].lOrgID = new long[count];
                            SYS_HOLIDAYS[i].lOrgID[j] = Conversion.ConvertObjToInt64(objReader.GetValue("ORG_EID"), base.ClientId);
                            sDate2 = sDate1;
                        }
                        else
                        {
                            j = j + 1;
                            SYS_HOLIDAYS[i].lOrgID[j] = Conversion.ConvertObjToInt64(objReader.GetValue("ORG_EID"), base.ClientId);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (objReader != null)
                    objReader.Dispose();
                if (objCount != null)
                    objCount.Dispose();
            }
		}
		#endregion
	/*	public override void BeforeSave(ref bool Cancel)
		{
			XmlDocument objSysExDataXmlDoc = base.SysEx;
			XmlNode objPiRowIdNode = null;
			try{objPiRowIdNode = objSysExDataXmlDoc.SelectSingleNode("/SysExData/PiRowId");}
			catch{};
			
			if(objPiRowIdNode !=null)
				if(this.PiXWorkLoss.PiRowId==0)
					this.PiXWorkLoss.PiRowId = Conversion.ConvertStrToInteger(objPiRowIdNode.InnerText);

			objPiRowIdNode = null;
			objSysExDataXmlDoc = null;

			base.BeforeSave (ref Cancel);		

		}*/
	}
}
