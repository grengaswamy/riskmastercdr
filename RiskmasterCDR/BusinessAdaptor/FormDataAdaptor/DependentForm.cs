using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Dependent Screen.
	/// </summary>
	public class DependentForm : DataEntryFormBase
	{
		const string CLASS_NAME = "EmpXDependent";
		const string FILTER_KEY_NAME = "EmployeeEid";

		private EmpXDependent EmpXDependent{get{return objData as EmpXDependent;}}

		private LocalCache objCache{get{return objData.Context.LocalCache;}}
	    
		/// <summary>
		/// Class constructor to initialize Class Name information
		/// </summary>
		/// <param name="fda"></param>
		public DependentForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

		/// <summary>
		/// Initializes the class and sets the parent ID for the class
		/// that can be used for parent-child navigation
		/// </summary>
		public override void InitNew()
		{
			base.InitNew();

			//set filter for EmployeeEid
			EmpXDependent.EmployeeEid=base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME,false);
			if (EmpXDependent.EmployeeEid>0)
				(objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + EmpXDependent.EmployeeEid;
		}

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
			base.OnUpdateForm ();

			if (EmpXDependent.DependentEntity.BirthDate!=string.Empty)
				base.ResetSysExData("DependentAge",Utilities.CalculateAgeInYears(EmpXDependent.DependentEntity.BirthDate));
			else
				base.ResetSysExData("DependentAge","");
			if(objData.IsNew)
			{
				if(base.GetSysExDataNodeText("/SysExData/LastName")!=null)
				{
					EmpXDependent.DependentEntity.LastName =  base.GetSysExDataNodeText("/SysExData/LastName");
				}
			}
			//Check for People Maintenance permission
			PeoplePermissionChecks4Other("dependent", m_SecurityId+RMO_UPDATE);
		}
		
//		public override void AfterAddNew()
//		{
//			base.AfterAddNew ();
//
//			XmlDocument objXML = base.SysEx;
//			XmlNode objEmployeeEid=null;
//			try{objEmployeeEid = objXML.GetElementsByTagName(FILTER_KEY_NAME)[0];}
//			catch{};
//			
//			if(objEmployeeEid !=null)
//			{
//				EmpXDependent.EmployeeEid=Conversion.ConvertStrToInteger(objEmployeeEid.InnerText);
//			}
//		}
//		public override void BeforeSave(ref bool Cancel)
//		{
//			XmlDocument objSysExDataXmlDoc = base.SysEx;
//			XmlNode objEmployeeEidNode = null;
//			try{objEmployeeEidNode = objSysExDataXmlDoc.SelectSingleNode("/SysExData/EmployeeEid");}
//			catch{};
//			
//			if(objEmployeeEidNode !=null)
//				if(this.EmpXDependent.EmployeeEid==0)
//					this.EmpXDependent.EmployeeEid = Conversion.ConvertStrToInteger(objEmployeeEidNode.InnerText);
//
//			objEmployeeEidNode = null;
//			objSysExDataXmlDoc = null;
//
//			base.BeforeSave (ref Cancel);		
//
//		}

        //Added by pmittal5 Mits 13632 03/23/09 
        public override void BeforeApplyLookupData(ref bool Cancel, ArrayList arrToSaveFields)
        {
            arrToSaveFields.Add("EntityTableId");
        }

		public override void OnValidate(ref bool Cancel)
		{
			bool bError = false;
			
			// Perform data validation
			string sToday = Conversion.ToDbDate(System.DateTime.Now);

			if (EmpXDependent.DependentEntity.BirthDate!="" && EmpXDependent.DependentEntity.BirthDate.CompareTo(sToday)>0)
			{
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                    String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.BirthDate", base.ClientId), System.DateTime.Now.ToShortDateString()),
					BusinessAdaptorErrorType.Error);

				bError = true;
			}
			// Return true if there were validation errors
			Cancel = bError;
		}
	}
}