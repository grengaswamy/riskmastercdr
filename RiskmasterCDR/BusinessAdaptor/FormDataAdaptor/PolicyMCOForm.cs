﻿
using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

//TODO: Auto-Generated Form - migrate and verify behaviors from Legacy RMNet form.

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for PolicyMCO Screen.
	/// </summary>
	public class PolicyMCOForm : DataEntryFormBase
	{
		const string CLASS_NAME = "PolicyXMco";
		private PolicyXMco PolicyXMco{get{return objData as PolicyXMco;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		const string FILTER_KEY_NAME = "PolicyId";

		/// <summary>
		/// Class constructor to initialize Class Name information
		/// </summary>
		/// <param name="fda"></param>
		public PolicyMCOForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

		/// <summary>
		/// Initializes the class and sets the parent ID for the class
		/// that can be used for parent-child navigation
		/// </summary>
		public override void InitNew()
		{
			base.InitNew();

			//If we were sent here from a parent(event) then we'll have an eventid in the SysEx
			//originally from ScreenFlow\SysExData.
			XmlNode objPolicyId=null;

			try
			{
				objPolicyId = base.SysEx.GetElementsByTagName(FILTER_KEY_NAME)[0];
			}//end try
			catch
			{
			}; //end catch
			if(objPolicyId !=null)
				this.m_ParentId = Conversion.ConvertObjToInt(objPolicyId.InnerText, base.ClientId);

			PolicyXMco.PolicyId = this.m_ParentId;

			//Filter by this PhysEid if Present.
			if(this.m_ParentId != 0)
			{
				(objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + this.m_ParentId;
			}
			
		}//end method InitNew()

				
		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
			base.OnUpdateForm ();
			
			XmlDocument objXML = base.SysEx;
			XmlElement objOrigValues = objXML.CreateElement( "OrigValues" );
			XmlNode objOrigValuesOld = objXML.SelectSingleNode("//OrigValues");
            objOrigValues.InnerText = PolicyXMco.McoEid + "|" + PolicyXMco.McoBeginDate + "|" + PolicyXMco.McoEndDate ;

			if(objOrigValuesOld !=null)
				objXML.DocumentElement.ReplaceChild(objOrigValues,objOrigValuesOld);
			else
				objXML.DocumentElement.AppendChild( objOrigValues );			

            //akaur9 MITS 25163- Policy Interface Implementation --start
            if (PolicyXMco.PolicyId > 0)
            {
                Policy objPolicy = (Policy)objData.Context.Factory.GetDataModelObject("Policy", false);
                objPolicy.MoveTo(PolicyXMco.PolicyId);
                if (objPolicy.PolicySystemId > 0)
                {
                    base.AddReadOnlyNode("readOnlyPage");
                    base.AddKillNode("new");
                    base.AddKillNode("save");
                    base.AddKillNode("delete");
                }
            }
            //akaur9 MITS 25163- Policy Interface Implementation --End
		}
	}
}
