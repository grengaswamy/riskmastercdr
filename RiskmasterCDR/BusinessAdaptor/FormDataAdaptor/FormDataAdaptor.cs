﻿
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

using Riskmaster.Application;
using Riskmaster.Application.AcrosoftWebserviceWrapper;
using Riskmaster.BusinessAdaptor;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;
using Riskmaster.Application.MediaViewWrapper;

namespace Riskmaster.BusinessAdaptor
{
    /// <summary>
    /// Summary description for Class1.
    /// </summary>
    public class FormDataAdaptor : BusinessAdaptorBase
    {
        private class FormMappingCacheRecord
        {
            public string BaseFormName;	// Name key. This is the form name we're customizing.
            public string FormName;		// The new customized formname.
            public string TypeName;		// Class implementing the custom form.
            public string Assembly;
        }
        private const string AUTHKEY = "6378b87457a5ecac8674e9bac12e7cd9";
        //MITS 11524 - Abhishek start
        private const int ORG_ACCESS_PERMISSION = 17000;
        private const int ORG_VIEW_PERMISSION = 17001;
        private const int ORG_UPDATE_PERMISSION = 17002;
        private const int ORG_CREATE_PERMISSION = 17003;
        private const int ORG_DELETE_PERMISSION = 17004;
        private const int ORG_ATTACH_PERMISSION = 17006;
        private bool m_bIsOrgHieryField = false;
        //End MITS 11524
        //Abhishek -- Safeway Retrofit Entity Specification -- 5/28/2008 : start
        //Entity Security 
        private const int ENT_VENDOR_PERMISSION = 13000;
        private const int ENT_VENDOR_VIEW_PERMISSION = 13001;
        private const int ENT_VENDOR_UPDATE_PERMISSION = 13002;
        private const int ENT_VENDOR_CREATE_PERMISSION = 13003;
        private const int ENT_VENDOR_DELETE_PERMISSION = 13004;
        private bool m_bIsVendorField = false;
        private bool m_bIsNewEntity = false;
        //Abhishek -- Safeway Retrofit Entity Specification -- 5/28/2008 : end
        private string sLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").Split('|')[0].ToString();
        
        private XmlDocument m_req = null;
        private XmlDocument m_resp = null;
        internal BusinessAdaptorErrors m_err = null;
        internal DataModelFactory m_dmf = null;
        internal FormViewManager m_viewMgr = null;

        #region BSB Standard  XML Parameter Parsing Routines

        static XmlNamespaceManager _nsmgr = null;
        static XmlNameTable _nt = null;
        static NameValueCollection _settings = null;
        static Hashtable _mappings = new Hashtable();

        static FormDataAdaptor()
        {
            _nt = new NameTable();
            _nsmgr = new XmlNamespaceManager(_nt);
            _nsmgr.AddNamespace("m", AppConstants.APP_NAMESPACE);
            _nsmgr.AddNamespace("soap", @"http://schemas.xmlsoap.org/soap/envelope/");
            _nsmgr.AddNamespace("xforms", @"http://www.w3.org/2002/xforms");
            _nsmgr.AddNamespace("xxforms", @"http://orbeon.org/oxf/xml/xforms");

            //BSB Add Support for mapping forms to custom form implementation code.
            RMExtensibilityCollection oColl = RMConfigurationManager.GetRMExtensibilityForms();
            if (oColl.Count > 0)
            {
                // Cache the <adaptor> tags into a static hashtable
                FormMappingCacheRecord MapItem;
                foreach (RMExtensibilityElement xmlMapping in oColl)
                {
                    MapItem = new FormMappingCacheRecord();
                    MapItem.BaseFormName = xmlMapping.BaseFormName;
                    MapItem.FormName = xmlMapping.FormName;
                    MapItem.TypeName = xmlMapping.TypeName;
                    MapItem.Assembly = xmlMapping.Assembly;

                    _mappings.Add(MapItem.BaseFormName, MapItem);
                }//foreach
                
            }
        }
        //BSB 01.17.2005 Added routines for Easy Retrieval of SysFormVariables from new aggregate param.
        internal bool HasParam(string paramName) { return (SafeParam(paramName) != null); }
        internal bool HasFormVariableParam(string paramName) { return (SafeFormVariableParam(paramName) != null); }
        internal XmlElement SafeParam(string paramName)
        {
            return (SafeXPath(String.Format("ParamList/Param[@name='{0}']", paramName)) as XmlElement);
        }
        internal XmlElement SafeFormVariableParam(string paramName)
        {
            return (SafeXPath(String.Format("ParamList/Param[@name='SysFormVariables']/FormVariables/{0}", paramName)) as XmlElement);
        }
        public string SafeFormVariableParamText(string paramName)
        {
            try { return (SafeFormVariableParam(paramName) as XmlElement).InnerText; }
            catch { return ""; }
        }
        internal string SafeParamText(string paramName)
        {
            try { return (SafeParam(paramName) as XmlElement).InnerText; }
            catch { return ""; }
        }

        internal XmlNode SafeXPath(string xPath)
        {
            try
            {
                return m_req.SelectSingleNode(xPath);
            }
            catch
            {
                return null;
            }
        }
        public bool SafeSaveParamText(string paramName, string paramValue)
        {
            try { (SafeParam(paramName) as XmlElement).InnerText = paramValue; return true; }
            catch { return false; }
        }
        public bool SafeSaveFormVariableParamText(string paramName, string paramValue)
        {
            try { (SafeFormVariableParam(paramName) as XmlElement).InnerText = paramValue; return true; }
            catch { return false; }
        }
        internal bool SafeSaveParamXml(string paramName, string paramValue)
        {
            try
            {
                (SafeParam(paramName) as XmlElement).InnerXml = paramValue;
                return true;
            }
            catch (Exception e)
            {
                LogInvalidXmlException(paramName, paramValue, e);
                return false;
            }
        }

        /// <summary>
        /// Try to find which element in the XML causes problem. For bad XML, the assumption
        /// is that the error message contain position of bad ASCII character.
        /// </summary>
        /// <param name="paramName">parameter name</param>
        /// <param name="sBadXml">Xml string which causes problem</param>
        /// <param name="ex">Exception generated when assign XML to InnerXml</param>
        private void LogInvalidXmlException(string paramName, string sBadXml, Exception ex)
        {
            //Assume the error message is in the format of: "is an invalid character. Line 1, position 13972."
            string sErrorMessage = ex.Message;
            string sPosition = string.Empty;
            int iPosition = -1;
            int iBeginofEndTag = -1;
            int iEndofEndTag = -1;
            string sTagName = string.Empty;

            Regex rx = new Regex(@"position\s+(?<position>\d+)");
            if (rx.IsMatch(sErrorMessage))
            {
                sPosition = rx.Match(sErrorMessage).Result("${position}");
                if (int.TryParse(sPosition, out iPosition))
                {
                    iBeginofEndTag = sBadXml.IndexOf("]]></", iPosition);
                    if (iBeginofEndTag > iPosition)
                    {
                        iEndofEndTag = sBadXml.IndexOf(">", iBeginofEndTag + 5);
                        if (iEndofEndTag > iBeginofEndTag)
                        {
                            sTagName = sBadXml.Substring(iBeginofEndTag + 5, iEndofEndTag - iBeginofEndTag - 5);
                        }
                    }
                }
            }
            Log.Write(String.Format(Globalization.GetString("FormDataAdaptor.InvalidXml", base.ClientId), paramName, ex.Message, sTagName) + " Bad XML: " + sBadXml, "CommonWebServiceLog", base.ClientId);
 			m_err.Add(Globalization.GetString("FormDataAdaptor.InvalidXmlError", base.ClientId, sLangCode), String.Format(Globalization.GetString("FormDataAdaptor.InvalidXml", base.ClientId, sLangCode), paramName, ex.Message, sTagName), BusinessAdaptorErrorType.Error);
        }

        internal bool SafeSaveFormVariableParamXml(string paramName, string paramValue)
        {
            try
            {
                (SafeFormVariableParam(paramName) as XmlElement).InnerXml = paramValue;
                return true;
            }
            catch (Exception e)
            {
                LogInvalidXmlException(paramName, paramValue, e);
                return false;
            }
        }

        static internal string CData(string s) { return String.Format("<![CDATA[{0}]]>", s); }
        #endregion

        public FormDataAdaptor()
        {
            m_resp = new XmlDocument(_nt);
            m_err = new BusinessAdaptorErrors(base.ClientId);
        }

        internal FormViewManager ViewManager
        {
            get
            {
                if (m_viewMgr == null)
                    m_viewMgr = new FormViewManager(base.userLogin, m_err,base.ClientId);
                return m_viewMgr;

            }
        }
        internal DataModelFactory Factory
        {
            get
            {
                if (m_dmf == null)
                    m_dmf = new DataModelFactory(base.userLogin,base.ClientId);
                return m_dmf;
            }
        }
        /// <summary>
        /// This method moves to the "first" database record meeting the criteria specified in the request xml.
        /// The formname type MUST be specified in the ClassName tag in the request xml.
        /// Serialization specifics can be specified in a "SerializationConfig" tag in the request xml.
        /// Additional Filtering or mode selection can be specified via additional param tags in the request.
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public bool NavigateRecordSummary(XmlDocument xmlIn, ref XmlDocument xmlResponse, ref BusinessAdaptorErrors errOut)
        {
            try
            {

                m_req = xmlIn;
                errOut = m_err;

                FormBase objFormBase = null;

                string sValue = "";

                //Extract Parameters from Message
                string sFormName; // = SafeFormVariableParamText("SysFormName");

                string sSysFormEx = "";
                if (HasFormVariableParam("SysExData"))
                    sSysFormEx = SafeFormVariableParam("SysExData").OuterXml;


                //rupal:start, r8 multicurrency
                //add the node BaseCurrencyType in SysEX so that it will be available for all FDM pages
                sSysFormEx = AppendBaseCurrencyNodeToSysEx(sSysFormEx);
                //rupal:end

                //Get the FormVariables
                string sFormVariables = string.Empty;
                if (HasParam("SysFormVariables"))
                    sFormVariables = SafeParam("SysFormVariables").InnerXml;
                //This is more of an "output" param...
                string sSysViewSection = "";
                if (HasFormVariableParam("SysViewSection"))
                    sSysViewSection = SafeFormVariableParam("SysViewSection").OuterXml;
                string sSysFlowStack = "";

                if (HasParam("ScreenFlowStack"))
                    sSysFlowStack = SafeParam("ScreenFlowStack").OuterXml;

                // BSB Store off the "posted" view and dynamic sections.
                // we will need them for validation and in case of any error response.
                string sSysPostedViewSection = sSysViewSection;

                //This is the current legacy view.
                string sSysPostedView = "";
                if (HasFormVariableParam("SysView"))
                    sSysPostedView = SafeFormVariableParam("SysView").OuterXml;

                //This is the fresh view from the database.
                string sSysView = "";

                sSysView = FetchViewWithRawParamsForRecordSummary();
                if (this.m_err.Count > 0 || sSysView == "")
                    return false;


                //BSB 01.17.2006 Modified to generate view on the fly instead of recieving as 
                // part of the Request Message.  Part of potential perf improvement to consolidate 
                // a web service call.
                sFormName = FetchFormNameWithRawParams();
                if (sFormName.IndexOf('|') >= 0)
                    sFormName = sFormName.Split('|')[0];


                //BSB Default\Initialize Param Style Values (from View Definition)
                /*
                 * The value should be get from the UI
				if(		this.SafeFormVariableParamText("SysClassName")!="" && 				
					this.SafeParamText("SysScreenAction")=="") //Is Postback to same screen. 
					this.InitPostBackFormVariables(ref sSysView); //Just book-keep a bit.
				else	
					this.InitFormVariables(sSysView); //Pickup default values from view definition.
                */

                //BSB Pick up Param Style Values that may have just been defaulted\initialized
                string sSysSid = SafeFormVariableParamText("SysSid");
                string sSysLookup = String.Format(
                    @"<SysLookup>{0}{1}{2}{3}</SysLookup>",
                    this.SafeFormVariableParam("SysLookupClass").OuterXml,
                    this.SafeFormVariableParam("SysLookupResultConfig").OuterXml,
                    this.SafeFormVariableParam("SysLookupRecordId").OuterXml,
                    this.SafeFormVariableParam("SysLookupAttachNodePath").OuterXml);

                //Perform Requested Process

                //Pick a Form Implementation
                objFormBase = GetFormImplementation(sFormName);
                //sai start -- 32157
              //  if (sSysFormEx != "")
                if (!string.IsNullOrEmpty( sSysFormEx) )
                    objFormBase.SysEx.LoadXml(sSysFormEx);

                if (!string.IsNullOrEmpty(sFormVariables))
                    objFormBase.FormVariables.LoadXml(sFormVariables);
              //  if (sSysViewSection != "")
                if (!string.IsNullOrEmpty(sSysViewSection))
                    objFormBase.SysViewSection.LoadXml(sSysViewSection);

               // if (sSysView != "")
                if(!string.IsNullOrEmpty( sSysView))
                    objFormBase.SysView.LoadXml(sSysView);

               // if (sSysPostedView != "")
                if (!string.IsNullOrEmpty(sSysPostedView))
                    objFormBase.SysPostedView.LoadXml(sSysPostedView);

               // if (sSysPostedViewSection != "")
                    if (!string.IsNullOrEmpty(sSysPostedViewSection))
                    objFormBase.SysPostedViewSection.LoadXml(sSysPostedViewSection);

                //if (sSysLookup != "")
                    if (!string.IsNullOrEmpty(sSysLookup))
                    objFormBase.SysLookup.LoadXml(sSysLookup);

               // if (sSysFlowStack != "")
                    if (!string.IsNullOrEmpty(sSysFlowStack))
                    objFormBase.SysFormStack.LoadXml(sSysFlowStack);
                    //sai End
                //Abhishek MITS 11403 - Start
                m_bIsOrgHieryField = IsOrgHierarchyType(xmlIn);
                //MITS 11403 End 
                //Abhishek -- Safeway Retrofit Entity Specification -- 5/28/2008 : start
                m_bIsVendorField = IsVendorType(xmlIn);
                if (m_bIsVendorField)
                {
                    m_bIsNewEntity = IsNewEntity(xmlIn);

                    if (!(base.userLogin.IsAllowedEx(ENT_VENDOR_PERMISSION)))
                        throw new Exception(Globalization.GetString("Permission.NoAccess", base.ClientId, sLangCode));
                }
                //Abhishek -- Safeway Retrofit Entity Specification -- 5/28/2008 : end
				


                // BSB 08.25.2005 Change this function name to Init() as discussed with CSCI.
                // Used only as a secondary Constructor within derived forms and 
                // called after the preceeding Xml properties have been fully populated.
                // Note: The default class implementation of this is just to wrap a call InitNew().

                // objFormBase.CreateRecordFilter();//Params are "implicit" and avaiable via SysEx XML property if supplied.
                objFormBase.Init();

                objFormBase.SecurityId = Conversion.ConvertStrToInteger(sSysSid);//Int32.Parse(sSysSid);

                //BSB Fix to support Post-back w/o destroying section information for Lookup Requests.
                //Moved this call here to make it explicit and conditional.
                if (SafeFormVariableParamText("SysCmd") != "8" && SafeFormVariableParamText("SysCmd") != "5" && SafeFormVariableParamText("SysCmd") != "9") //If Not A Lookup Or Save Request
                    objFormBase.ResetSysSections();

                AdminTrackingForm objAdmForm = objFormBase as AdminTrackingForm;
                if (objAdmForm != null)
                    sValue = NavAdm(objAdmForm);

                DataEntryFormBase objDEForm = objFormBase as DataEntryFormBase;
                if (objDEForm != null && objAdmForm == null)
                    sValue = NavDataEntryRecordSummary(objDEForm);

                ListFormBase objListForm = objFormBase as ListFormBase;
                if (objListForm != null)
                {
                    sValue = NavList(objListForm);
                }
                //objFormBase.ApplyViewNodeChanges();

                //Raman Bhatia: We need to take care of the memo field customization requirements.. refer to MITS 10376
                objFormBase.StoreMemoCustomizations();

                //Raman Bhatia: Put All the modified nodes in SysEx.. for R5 OnUpdateForm implementation
                objFormBase.StoreModifiedControls();


                //Put the killed nodes in the SysExData element
                StringBuilder sbNodes = new StringBuilder();
                foreach (DictionaryEntry oNode in objFormBase.KillNodes)
                {
                    if (sbNodes.Length > 0)
                        sbNodes.Append("|");
                    sbNodes.Append(oNode.Value);
                }

                //Put the readonly nodes in the SysExData element
                StringBuilder sbReadOnlyNodes = new StringBuilder();
                foreach (DictionaryEntry oNode in objFormBase.ReadOnlyNodes)
                {
                    if (sbReadOnlyNodes.Length > 0)
                        sbReadOnlyNodes.Append("|");
                    sbReadOnlyNodes.Append(oNode.Value);
                }

                //Put the readwrite nodes in the SysExData element
                StringBuilder sbReadWriteNodes = new StringBuilder();
                foreach (DictionaryEntry oNode in objFormBase.ReadWriteNodes)
                {
                    if (sbReadWriteNodes.Length > 0)
                        sbReadWriteNodes.Append("|");
                    sbReadWriteNodes.Append(oNode.Value);
                }

                //Put the display nodes in the SysExData element
                StringBuilder sbDisplayNodes = new StringBuilder();
                foreach (DictionaryEntry oNode in objFormBase.DisplayNodes)
                {
                    if (sbDisplayNodes.Length > 0)
                        sbDisplayNodes.Append("|");
                    sbDisplayNodes.Append(oNode.Value);
                }

                //Added by Amitosh for R8 enhancement of LiabilityLoss
                //Put the KillFieldMark  nodes in the SysExData element
                StringBuilder sbKillFieldMarkNodes = new StringBuilder();
                foreach (DictionaryEntry oNode in objFormBase.KillFieldMarkNodes)
                {
                    if (sbKillFieldMarkNodes.Length > 0)
                        sbKillFieldMarkNodes.Append("|");
                    sbKillFieldMarkNodes.Append(oNode.Value);
                }
                //End Amitosh

                objFormBase.ClearSysExternalParam();
                SafeSaveParamXml("SysPropertyStore", sValue);
                SafeSaveParamXml("SysFormVariables", objFormBase.FormVariables.OuterXml);
                this.SafeSaveFormVariableParamXml("SysExData", objFormBase.SysEx.DocumentElement.InnerXml);
                this.SafeSaveFormVariableParamXml("SysViewSection", objFormBase.SysViewSection.DocumentElement.InnerXml);
                this.SafeSaveFormVariableParamXml("SysView", objFormBase.SysView.InnerXml);
                this.SafeSaveFormVariableParamText("SysKilledNodes", sbNodes.ToString());
                this.SafeSaveFormVariableParamText("SysReadOnlyNodes", sbReadOnlyNodes.ToString());
                this.SafeSaveFormVariableParamText("SysReadWriteNodes", sbReadWriteNodes.ToString());
                this.SafeSaveFormVariableParamText("SysDisplayNodes", sbDisplayNodes.ToString());
                //Added by amitosh for R8 enhancement of LiablityLoss
                this.SafeSaveFormVariableParamText("SysKillFieldMarkNode", sbKillFieldMarkNodes.ToString());
                m_resp = m_req;
                xmlResponse = m_resp;
            }
            //			catch(Exception e)
            //			{
            //				m_err.Add(e,BusinessAdaptorErrorType.SystemError);
            //				throw new RMAppException("Test Exception Caught:",e);
            //			}
            finally
            {
                this.Factory.UnInitialize();
            }
            //return (this.m_err.Count ==0); 		
            return IsNavigationSuccessfull();
        }
        /// <summary>
        /// This method moves to the "first" database record meeting the criteria specified in the request xml.
        /// The formname type MUST be specified in the ClassName tag in the request xml.
        /// Serialization specifics can be specified in a "SerializationConfig" tag in the request xml.
        /// Additional Filtering or mode selection can be specified via additional param tags in the request.
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public bool Navigate(XmlDocument xmlIn, ref XmlDocument xmlResponse, ref BusinessAdaptorErrors errOut)
        {
            try
            {

                m_req = xmlIn;
                errOut = m_err;

                FormBase objFormBase = null;

                string sValue = "";
                string sSysFormId = string.Empty;
                //Extract Parameters from Message
                string sFormName; // = SafeFormVariableParamText("SysFormName");

                string sSysFormEx = "";
                if (HasFormVariableParam("SysExData"))
                    sSysFormEx = SafeFormVariableParam("SysExData").OuterXml;

                //rupal:start, r8 multicurrency
                //add the node BaseCurrencyType in SysEX so that it will be available for all FDM pages
                sSysFormEx = AppendBaseCurrencyNodeToSysEx(sSysFormEx);
               //rupal:end

                //Get the FormVariables
                string sFormVariables = string.Empty;
                if (HasParam("SysFormVariables"))
                    sFormVariables = SafeParam("SysFormVariables").InnerXml;
                //This is more of an "output" param...
                string sSysViewSection = "";
                if (HasFormVariableParam("SysViewSection"))
                    sSysViewSection = SafeFormVariableParam("SysViewSection").OuterXml;
                string sSysFlowStack = "";

                if (HasParam("ScreenFlowStack"))
                    sSysFlowStack = SafeParam("ScreenFlowStack").OuterXml;

                // BSB Store off the "posted" view and dynamic sections.
                // we will need them for validation and in case of any error response.
                string sSysPostedViewSection = sSysViewSection;

                //This is the current legacy view.
                string sSysPostedView = "";
                if (HasFormVariableParam("SysView"))
                    sSysPostedView = SafeFormVariableParam("SysView").OuterXml;

                //This is the fresh view from the database.
                string sSysView = "";
                /*
                 * No need to retrieve the view from the database.
				sSysView = FetchViewWithRawParams();
				if(this.m_err.Count>0 || sSysView=="")
					return false;
                */

                //BSB 01.17.2006 Modified to generate view on the fly instead of recieving as 
                // part of the Request Message.  Part of potential perf improvement to consolidate 
                // a web service call.
                sFormName = FetchFormNameWithRawParams();
                if (sFormName.IndexOf('|') >= 0)
                    sFormName = sFormName.Split('|')[0];
                
                //rsolanki2: mcm updates
                sSysFormId = xmlIn.SelectSingleNode(@"//ParamList/Param[@name='SysFormId']").InnerText.Trim();

                if (!string.IsNullOrEmpty(sSysFormId) && sSysFormId != "-1")
                {
                    AcrosoftFolderReIndex(xmlIn, sFormName);
                    MediaviewFolderReIndex(xmlIn, sFormName);
                }



                //BSB Default\Initialize Param Style Values (from View Definition)
                /*
                 * The value should be get from the UI
				if(		this.SafeFormVariableParamText("SysClassName")!="" && 				
					this.SafeParamText("SysScreenAction")=="") //Is Postback to same screen. 
					this.InitPostBackFormVariables(ref sSysView); //Just book-keep a bit.
				else	
					this.InitFormVariables(sSysView); //Pickup default values from view definition.
                */

                //BSB Pick up Param Style Values that may have just been defaulted\initialized
                string sSysSid = SafeFormVariableParamText("SysSid");
                string sSysLookup = String.Format(
                    @"<SysLookup>{0}{1}{2}{3}</SysLookup>",
                    this.SafeFormVariableParam("SysLookupClass").OuterXml,
                    this.SafeFormVariableParam("SysLookupResultConfig").OuterXml,
                    this.SafeFormVariableParam("SysLookupRecordId").OuterXml,
                    this.SafeFormVariableParam("SysLookupAttachNodePath").OuterXml);

                //Perform Requested Process

                //Pick a Form Implementation
                objFormBase = GetFormImplementation(sFormName);

                sLangCode = Convert.ToString(objFormBase.Adaptor.LangCode);
                //   if (sSysFormEx != "") //Mits 32157 
                if (!string.IsNullOrEmpty( sSysFormEx) )
                    objFormBase.SysEx.LoadXml(sSysFormEx);

                if (!string.IsNullOrEmpty(sFormVariables))
                    objFormBase.FormVariables.LoadXml(sFormVariables);
                // if (sSysViewSection != "") //Mits 32157
                if (!string.IsNullOrEmpty(sSysViewSection) )
                    objFormBase.SysViewSection.LoadXml(sSysViewSection);

                // if (sSysView != "")       //Mits 32157
                    if (!string.IsNullOrEmpty(sSysView))
                    objFormBase.SysView.LoadXml(sSysView);

                //  if (sSysPostedView != "")   //Mits 32157
                    if (!string.IsNullOrEmpty(sSysPostedView))
                    objFormBase.SysPostedView.LoadXml(sSysPostedView);

                // if (sSysPostedViewSection != "") //Mits 32157
                    if (!string.IsNullOrEmpty(sSysPostedViewSection))
                    objFormBase.SysPostedViewSection.LoadXml(sSysPostedViewSection);

                // if (sSysLookup != "") //Mits 32157
                    if (!string.IsNullOrEmpty(sSysLookup))
                    objFormBase.SysLookup.LoadXml(sSysLookup);

                //if (sSysFlowStack != "") //Mits 32157
                    if (!string.IsNullOrEmpty(sSysFlowStack))
                    objFormBase.SysFormStack.LoadXml(sSysFlowStack);

                //Abhishek MITS 11403 - Start
                m_bIsOrgHieryField = IsOrgHierarchyType(xmlIn);
                //MITS 11403 End 

                //Abhishek -- Safeway Retrofit Entity Specification -- 5/28/2008 : start
                m_bIsVendorField = IsVendorType(xmlIn);
                if (m_bIsVendorField)
                {
                    m_bIsNewEntity = IsNewEntity(xmlIn);

                    if (!(base.userLogin.IsAllowedEx(ENT_VENDOR_PERMISSION)))
                        throw new Exception(Globalization.GetString("Permission.NoAccess", base.ClientId, sLangCode));
                }
                //Abhishek -- Safeway Retrofit Entity Specification -- 5/28/2008 : end
				
                // BSB 08.25.2005 Change this function name to Init() as discussed with CSCI.
                // Used only as a secondary Constructor within derived forms and 
                // called after the preceeding Xml properties have been fully populated.
                // Note: The default class implementation of this is just to wrap a call InitNew().

                // objFormBase.CreateRecordFilter();//Params are "implicit" and avaiable via SysEx XML property if supplied.
                objFormBase.Init();

                objFormBase.SecurityId = Conversion.ConvertStrToInteger(sSysSid);//Int32.Parse(sSysSid);

                //BSB Fix to support Post-back w/o destroying section information for Lookup Requests.
                //Moved this call here to make it explicit and conditional.
                if (SafeFormVariableParamText("SysCmd") != "8" && SafeFormVariableParamText("SysCmd") != "5" && SafeFormVariableParamText("SysCmd") != "9") //If Not A Lookup Or Save Request
                    objFormBase.ResetSysSections();

                AdminTrackingForm objAdmForm = objFormBase as AdminTrackingForm;
                if (objAdmForm != null)
                {
                    objAdmForm.ClientId = base.ClientId;
                    sValue = NavAdm(objAdmForm);
                }
                DataEntryFormBase objDEForm = objFormBase as DataEntryFormBase;
                if (objDEForm != null && objAdmForm == null)
                {
                    objDEForm.ClientId = base.ClientId;
                    sValue = NavDataEntry(objDEForm);
                }

                ListFormBase objListForm = objFormBase as ListFormBase;
                if (objListForm != null)
                {
                    sValue = NavList(objListForm);
                }
                //objFormBase.ApplyViewNodeChanges();

                //Raman Bhatia: Need to add hidden controls for Zapatec Grid Implementation
                if (objAdmForm == null)
                {
                    UpdateZapatecGridData(objDEForm);
                }
                else
                {
                    UpdateZapatecGridDataAdmin(objAdmForm);
                }

                //Raman Bhatia: We need to take care of the memo field customization requirements.. refer to MITS 10376
                objFormBase.StoreMemoCustomizations();

                //Raman Bhatia: Put All the modified nodes in SysEx.. for R5 OnUpdateForm implementation
                objFormBase.StoreModifiedControls();


                //Put the killed nodes in the SysExData element
                StringBuilder sbNodes = new StringBuilder();
                foreach (DictionaryEntry oNode in objFormBase.KillNodes)
                {
                    if (sbNodes.Length > 0)
                        sbNodes.Append("|");
                    sbNodes.Append(oNode.Value);
                }

                //Put the readwrite nodes in the SysExData element
                StringBuilder sbReadWriteNodes = new StringBuilder();
                foreach (DictionaryEntry oNode in objFormBase.ReadWriteNodes)
                {
                    if (sbReadWriteNodes.Length > 0)
                        sbReadWriteNodes.Append("|");
                    sbReadWriteNodes.Append(oNode.Value);
                }

                //deb : MITS 20003
                //Put the readonly nodes in the SysExData element
                //StringBuilder sbReadOnlyNodes = new StringBuilder();
                //foreach (DictionaryEntry oNode in objFormBase.ReadOnlyNodes)
                //{
                //    if (sbReadOnlyNodes.Length > 0)
                //        sbReadOnlyNodes.Append("|");
                //    sbReadOnlyNodes.Append(oNode.Value);
                //}

                //Put the display nodes in the SysExData element
                StringBuilder sbDisplayNodes = new StringBuilder();
                foreach (DictionaryEntry oNode in objFormBase.DisplayNodes)
                {
                    if (sbDisplayNodes.Length > 0)
                        sbDisplayNodes.Append("|");
                    sbDisplayNodes.Append(oNode.Value);
                }

                //Amitosh mits 23441 01/20/2011
                //Put the ReadOnlyFieldMark  nodes in the SysExData element
                StringBuilder sbReadOnlyFieldMarkNodes = new StringBuilder();
                foreach (DictionaryEntry oNode in objFormBase.ReadOnlyFieldMarkNode)
                {
                    if (sbReadOnlyFieldMarkNodes.Length > 0)
                        sbReadOnlyFieldMarkNodes.Append("|");
                    sbReadOnlyFieldMarkNodes.Append(oNode.Value);
                }
                //End Amitosh

                //Added by Amitosh for R8 enhancement of LiabilityLoss
                //Put the KillFieldMark  nodes in the SysExData element
                StringBuilder sbKillFieldMarkNodes = new StringBuilder();
                foreach (DictionaryEntry oNode in objFormBase.KillFieldMarkNodes)
                {
                    if (sbKillFieldMarkNodes.Length > 0)
                        sbKillFieldMarkNodes.Append("|");
                    sbKillFieldMarkNodes.Append(oNode.Value);
                }
                //End Amitosh

                StringBuilder sbSysRequiredFields = new StringBuilder();
                foreach (DictionaryEntry oNode in objFormBase.SysRequiredFields)
                {
                    if (sbSysRequiredFields.Length > 0)
                        sbSysRequiredFields.Append("|");
                    sbSysRequiredFields.Append(oNode.Value);
                }

                objFormBase.ClearSysExternalParam();
                SafeSaveParamXml("SysPropertyStore", sValue);
                SafeSaveParamXml("SysFormVariables", objFormBase.FormVariables.OuterXml);
                this.SafeSaveFormVariableParamXml("SysExData", objFormBase.SysEx.DocumentElement.InnerXml);
                this.SafeSaveFormVariableParamXml("SysViewSection", objFormBase.SysViewSection.DocumentElement.InnerXml);
                this.SafeSaveFormVariableParamXml("SysView", objFormBase.SysView.InnerXml);
                this.SafeSaveFormVariableParamText("SysKilledNodes", sbNodes.ToString());
                //this.SafeSaveFormVariableParamText("SysReadOnlyNodes", sbReadOnlyNodes.ToString());
                //smishra54: R8 Withholding Enhancement, MITS 26019
                sSysFormId = xmlIn.SelectSingleNode(@"//ParamList/Param[@name='SysFormId']").InnerText.Trim();

                // npadhy - Instead of checking sSysFormId = "" as new record we will use a different logic.
                // As in some case the SysFormId can be < 0
                bool bIsNew = sSysFormId == string.Empty || Convert.ToInt32(sSysFormId) <= 0 ? true : false;
                //smishra54:end
                //Deb: MITS 32257
                bool IsExistingEntityEditable = true;
                string sPieid = string.Empty;
                XmlNode objTempNode = objFormBase.SysEx.SelectSingleNode("//PiEid");
                if (objTempNode != null)
                {
                    sPieid = objTempNode.InnerText.ToString();
                }
                else
                {
                    PiOtherForm objPiother = objFormBase as PiOtherForm;
                    if (objPiother != null)
                    {
                        sPieid = Conversion.ConvertObjToStr((objPiother.objData as PiOther).PiEid);
                    }
                }
                if (!string.IsNullOrEmpty(sPieid) && sPieid != "0" && !this.userLogin.IsAllowedEx(objFormBase.SecurityId, FormBase.RMO_ALLOW_UPDATE_EXISTING_ENTITY))
                {
                    //for now only org. hierarchy 
                    int iCount = Conversion.ConvertObjToInt(objDEForm.objData.Context.DbConnLookup.ExecuteScalar("SELECT COUNT(1) FROM ENTITY WHERE ENTITY_ID=" + sPieid + " AND ENTITY_TABLE_ID BETWEEN 1005 AND 1012"), base.ClientId);
                    if (iCount > 0) IsExistingEntityEditable = false;
                }
                objTempNode = null;
                //Deb: MITS 32257
                //deb : MITS 20003
                switch (bIsNew)
                {
                    case true:
                        if ((!this.userLogin.IsAllowedEx(objFormBase.SecurityId, FormBase.RMO_CREATE) && !this.userLogin.IsAllowedSuppEx(objFormBase.SecurityId + FormBase.RMO_SUPP, FormBase.RMO_CREATE)) || IsExistingEntityEditable == false)//Deb: MITS 32257
                        {
                            this.SafeSaveFormVariableParamText("SysReadOnlyNodes", "readOnlyPage");
                        }
                        else
                        {
                            AddReadOnlyNodes(objFormBase);
                            if (!this.userLogin.IsAllowedEx(objFormBase.SecurityId, FormBase.RMO_CREATE) || IsExistingEntityEditable == false)//Deb: MITS 32257
                            {
                                this.SafeSaveFormVariableParamText("SysReadOnlyNodes", "readOnlyPageExceptSupp");
                            }
                            else if (!this.userLogin.IsAllowedSuppEx(objFormBase.SecurityId + FormBase.RMO_SUPP, FormBase.RMO_CREATE))
                            {
                                if (string.IsNullOrEmpty((SafeFormVariableParam("SysReadOnlyNodes") as XmlElement).InnerText))
                                    this.SafeSaveFormVariableParamText("SysReadOnlyNodes", "readOnlyPageForSupp");
                                else
                                    this.SafeSaveFormVariableParamText("SysReadOnlyNodes", (SafeFormVariableParam("SysReadOnlyNodes") as XmlElement).InnerText + "|readOnlyPageForSupp");
                            }
                            //else
                            //Put the readonly nodes in the SysExData element
                        }
                        break;
                    default:
                        if (((!this.userLogin.IsAllowedEx(objFormBase.SecurityId, FormBase.RMO_UPDATE) && !this.userLogin.IsAllowedSuppEx(objFormBase.SecurityId + FormBase.RMO_SUPP, FormBase.RMO_UPDATE)) || IsExistingEntityEditable == false) && int.Parse(sSysFormId) > 0)//Deb sSysFormId > 0 bcoz it may be -1//Deb: MITS 32257
                        {
                            this.SafeSaveFormVariableParamText("SysReadOnlyNodes", "readOnlyPage");
                        }
                        else
                        {
                            AddReadOnlyNodes(objFormBase);
                            if (!this.userLogin.IsAllowedEx(objFormBase.SecurityId, FormBase.RMO_UPDATE))
                                this.SafeSaveFormVariableParamText("SysReadOnlyNodes", "readOnlyPageExceptSupp");
                            else if (!this.userLogin.IsAllowedSuppEx(objFormBase.SecurityId + FormBase.RMO_SUPP, FormBase.RMO_UPDATE))
                            {
                                if (string.IsNullOrEmpty((SafeFormVariableParam("SysReadOnlyNodes") as XmlElement).InnerText))
                                    this.SafeSaveFormVariableParamText("SysReadOnlyNodes", "readOnlyPageForSupp");
                                else
                                    this.SafeSaveFormVariableParamText("SysReadOnlyNodes", (SafeFormVariableParam("SysReadOnlyNodes") as XmlElement).InnerText + "|readOnlyPageForSupp");
                            }
                            //    //Put the readonly nodes in the SysExData element

                        }                           
                        break;
                }
                //deb : MITS 20003
                //Deb :MITS 32257
                //Amitosh mits 23441 01/20/2011
                this.SafeSaveFormVariableParamText("SysReadOnlyFieldMarkNode", sbReadOnlyFieldMarkNodes.ToString());
                //Added by amitosh for R8 enhancement of LiablityLoss
                this.SafeSaveFormVariableParamText("SysKillFieldMarkNode", sbKillFieldMarkNodes.ToString());
                this.SafeSaveFormVariableParamText("UpdateSysRequired", sbSysRequiredFields.ToString());
                this.SafeSaveFormVariableParamText("SysReadWriteNodes", sbReadWriteNodes.ToString());
                this.SafeSaveFormVariableParamText("SysDisplayNodes", sbDisplayNodes.ToString());
                m_resp = m_req;
                xmlResponse = m_resp;
            }
            //Abhishek Safeway Retrofit Entity Specification
            //Atul -- Safeway Entity Specification -- 5/28/2008 : start
            catch (Exception e)
            {
                m_err.Add(Globalization.GetString("PermissionFailure", base.ClientId, sLangCode),
                    string.Format(Globalization.GetString("Permission.FullPermissionMsg", base.ClientId, sLangCode), e.Message.ToString()),
                    BusinessAdaptorErrorType.Error);

                //We need to log inner exception here. Otherwise lost important error info
                Exception oInnerExceptoin = e;
                while (oInnerExceptoin != null)
                {
                    Log.Write(oInnerExceptoin.Message + ". Stack Trace: " + oInnerExceptoin.StackTrace.ToString(), base.ClientId);
                    oInnerExceptoin = oInnerExceptoin.InnerException;
                }

                //m_err.Add(e, BusinessAdaptorErrorType.SystemError);
                //    throw new RMAppException("Test Exception Caught:",e);
            }
            //Atul -- Safeway Entity Specification -- 5/28/2008 : end
			
            //			catch(Exception e)
            //			{
            //				m_err.Add(e,BusinessAdaptorErrorType.SystemError);
            //				throw new RMAppException("Test Exception Caught:",e);
            //			}
            finally
            {
                this.Factory.UnInitialize();
            }
            //return (this.m_err.Count ==0); 		
            return IsNavigationSuccessfull();
        }

        //rupal:start, r8 multicurrency
        //add the node BaseCurrencyType in SysEX so that it will be available for all FDM pages
        private string AppendBaseCurrencyNodeToSysEx(string sSysFormEx)
        {
            LocalCache objCache=null;
            try
            {
                objCache = new LocalCache(connectionString,base.ClientId);
                string sShortCode = string.Empty;
                string sDesc = string.Empty;
                XmlDocument oxmlDocSysEx = new XmlDocument();
                oxmlDocSysEx.LoadXml(sSysFormEx);
                //XmlElement oElement
                XmlNode oSysExNode = oxmlDocSysEx.SelectSingleNode("SysExData");
                if (oSysExNode.SelectSingleNode("BaseCurrencyType") != null)
                    oSysExNode.RemoveChild(oSysExNode.SelectSingleNode("BaseCurrencyType"));
                XmlElement oElement = oxmlDocSysEx.CreateElement("BaseCurrencyType");
                objCache.GetCodeInfo(CommonFunctions.GetBaseCurrencyCode(connectionString), ref sShortCode, ref sDesc);
                oElement.InnerText = sDesc.Split('|')[1];
                oSysExNode.AppendChild(oElement);
                sSysFormEx = oxmlDocSysEx.OuterXml;                                
            }
            catch(Exception ex)
            {
                LogInvalidXmlException("SysEx", sSysFormEx, ex);
            }
            finally
            {
                if (objCache != null)
                    objCache.Dispose();
                objCache=null;
            }
            return sSysFormEx;
        }
        //rupal:end
        

        //deb :MITS 20003
        private void AddReadOnlyNodes(FormBase objFormBase)
        {
            StringBuilder sbReadOnlyNodes = new StringBuilder();
            foreach (DictionaryEntry oNode in objFormBase.ReadOnlyNodes)
            {
                if (sbReadOnlyNodes.Length > 0)
                    sbReadOnlyNodes.Append("|");
                sbReadOnlyNodes.Append(oNode.Value);
            }
            this.SafeSaveFormVariableParamText("SysReadOnlyNodes", sbReadOnlyNodes.ToString());
        }
        //deb
        private void AcrosoftFolderReIndex(XmlDocument xmlIn, string sFormName)
        {
            //rsolanki2 : april 15 2010 : start acrosoft update 
            //  ParamList\Param[@name='SysPropertyStore']\FormVariables\SysFormIdName

            //string F

            // Rsolanki2: if Acrosoft is enabled, and if claim number or event number gets changed , 
            // we need to make sure to change int accordingly in MCM as well

            //todo: add reference to System.Configuration in the Proj file
            //todo: check this for other fdm screens apart from event/claim
            if (SafeFormVariableParamText("SysCmd") == "5" ) //Save operation 
            {
                Boolean bSuccess = false;
                string sTempAcrosoftUserId = base.userLogin.LoginName;
                string sTempAcrosoftPassword = base.userLogin.Password;

                Riskmaster.Settings.SysSettings objSettings = null;
                objSettings = new Riskmaster.Settings.SysSettings(connectionString, base.ClientId); //Ash - cloud
                if (objSettings.UseAcrosoftInterface != false)
                {
                    if (Conversion.CastToType<Boolean>(AcrosoftSection.UseCommonAcrosoftUser, out bSuccess))
                    {
                        if (AcrosoftSection.AcroUserMapSection.UserMap[base.loginName] != null
                            && !string.IsNullOrEmpty(AcrosoftSection.AcroUserMapSection.UserMap[base.loginName].RmxUser))
                        {
                            sTempAcrosoftUserId = AcrosoftSection.AcroUserMapSection.UserMap[base.loginName].AcrosoftUserId;
                            sTempAcrosoftPassword = AcrosoftSection.AcroUserMapSection.UserMap[base.loginName].AcrosoftPassword;
                        }
                        else
                        {
                            sTempAcrosoftUserId = AcrosoftSection.AcrosoftUsername;
                            sTempAcrosoftPassword = AcrosoftSection.AcrosoftUserPassword;
                        }
                    }

                    DbConnection objConn = null;
                    DbCommand objCommand = null;
                    //DbParameter objParam = null;
                    Riskmaster.Db.DbReader objReader = null;
                    string sSql = string.Empty, sOldRecNumber = string.Empty;

                    Acrosoft obj = new Acrosoft(base.ClientId);
                    string OutString = string.Empty;
                    string sAcroSession = string.Empty;
                    string xNdxReindexFldXml = string.Empty;
                    string sAppExcpXml = string.Empty;
                    string sNewRecordnumber = string.Empty;
                    string sEventNo = string.Empty;
                    //Boolean bIsClaim = false;

                    objConn = DbFactory.GetDbConnection(base.connectionString);
                    objConn.Open();
                    objCommand = objConn.CreateCommand();
                    //for event folders reindexing
                    if (sFormName.Equals("event", StringComparison.OrdinalIgnoreCase))
                    {
                        sSql = "SELECT EVENT_NUMBER FROM EVENT WHERE EVENT_ID = "
                            + xmlIn.SelectSingleNode(@"//ParamList/Param[@name='SysFormId']").InnerText.Trim();
                        //bIsClaim = false;
                        objReader = DbFactory.GetDbReader(base.connectionString, sSql);
                        if (objReader.Read())
                        {
                            //sXml = objReader.GetValue("PREF_XML").ToString();
                            sOldRecNumber = objReader.GetValue("EVENT_NUMBER").ToString();
                        }
                        objReader.Close();

                        sNewRecordnumber = xmlIn.SelectSingleNode(@"//ParamList/Param[@name='SysPropertyStore']/Instance/Event/EventNumber")
                            .InnerText.Trim();
                        if (!sOldRecNumber.Equals(sNewRecordnumber))
                        {
                            // i.e. event number was got changed 

                           
                            obj.Authenticate(sTempAcrosoftUserId, sTempAcrosoftPassword, out sAcroSession, out sAppExcpXml);

                            //todo: check for Authenticated AS session 

                            xNdxReindexFldXml = string.Concat(
                                "<NDXReindexFldData><IndexName>"
                                    , "Riskmaster"
                                , "</IndexName><FolderId>"
                                    , "RMXEVNT|"
                                    , sOldRecNumber
                                    ,"|0"
                                , "</FolderId><OldFolderKey><EventNbr>"
                                    , sOldRecNumber
                                , "</EventNbr><ClaimNbr>"
                                    , "0"
                                , "</ClaimNbr></OldFolderKey>"
                                , "<NewFolderKey><EventNbr>"
                                    , sNewRecordnumber
                                , "</EventNbr><ClaimNbr>"
                                    , "0"
                                , "</ClaimNbr></NewFolderKey></NDXReindexFldData>"
                                );

                            obj.ReIndexFolder(sAcroSession, xNdxReindexFldXml, out OutString);
                        }
                    }
                    //Changed by Gagan for MITS 22264 : start
                    //for claim folders reindexing
                    //else if (sFormName.StartsWith("claim", StringComparison.OrdinalIgnoreCase))                        
                    else if (      sFormName.Equals("claimgc", StringComparison.OrdinalIgnoreCase)
                                || sFormName.Equals("claimva", StringComparison.OrdinalIgnoreCase)
                                || sFormName.Equals("claimdi", StringComparison.OrdinalIgnoreCase)
                                || sFormName.Equals("claimpc", StringComparison.OrdinalIgnoreCase)
                                || sFormName.Equals("claimwc", StringComparison.OrdinalIgnoreCase)
                        )
                    //Changed by Gagan for MITS 22264 : end
                    {
                        sSql = "SELECT CLAIM_NUMBER FROM CLAIM WHERE CLAIM_ID = "
                            + xmlIn.SelectSingleNode(@"//ParamList/Param[@name='SysFormId']").InnerText.Trim();
                        //bIsClaim = true;

                        objReader = DbFactory.GetDbReader(base.connectionString, sSql);
                        if (objReader.Read())
                        {
                            //sXml = objReader.GetValue("PREF_XML").ToString();
                            sOldRecNumber = objReader.GetValue("CLAIM_NUMBER").ToString();
                        }
                        objReader.Close();
                        sEventNo = xmlIn.SelectSingleNode(@"//ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/Parent/Instance/Event/EventNumber").InnerText.Trim();
                        sNewRecordnumber = xmlIn.SelectSingleNode(@"//ParamList/Param[@name='SysPropertyStore']/Instance/Claim/ClaimNumber").InnerText.Trim();
                        if (!sOldRecNumber.Equals(sNewRecordnumber))
                        {
                            // i.e. claim number was got changed 
                           
                            obj.Authenticate(sTempAcrosoftUserId, sTempAcrosoftPassword, out sAcroSession, out sAppExcpXml);

                            //todo: check for Authenticated AS session 
                            xNdxReindexFldXml = string.Concat(
                                                           "<NDXReindexFldData><IndexName>"
                                                               , "Riskmaster"
                                                           , "</IndexName><FolderId>"
                                                               , "RMXEVNT|"
                                                               , sEventNo
                                                               , "|"
                                                               , sOldRecNumber
                                                           , "</FolderId><OldFolderKey><EventNbr>"
                                                               , sEventNo
                                                           , "</EventNbr><ClaimNbr>"
                                                               , sOldRecNumber
                                                           , "</ClaimNbr></OldFolderKey>"
                                                           , "<NewFolderKey><EventNbr>"
                                                               , sEventNo
                                                           , "</EventNbr><ClaimNbr>"
                                                               , sNewRecordnumber
                                                           , "</ClaimNbr></NewFolderKey></NDXReindexFldData>"
                                                           );
                            obj.ReIndexFolder(sAcroSession, xNdxReindexFldXml, out OutString);
                        }
                    }  //for policy folders reindexing
                        //added by skhare7
                    else if (sFormName.Equals("policy", StringComparison.OrdinalIgnoreCase))
                    {
                        sSql = "SELECT POLICY_NAME FROM POLICY WHERE POLICY_ID = "
                            + xmlIn.SelectSingleNode(@"//ParamList/Param[@name='SysFormId']").InnerText.Trim();
                        //bIsClaim = true;

                        objReader = DbFactory.GetDbReader(base.connectionString, sSql);
                        if (objReader.Read())
                        {
                            //sXml = objReader.GetValue("PREF_XML").ToString();
                            sOldRecNumber = objReader.GetValue("POLICY_NAME").ToString();
                        }
                        objReader.Close();
                       // sEventNo = xmlIn.SelectSingleNode(@"//ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/Parent/Instance/Event/EventNumber").InnerText.Trim();
                        sNewRecordnumber 
                            = xmlIn.SelectSingleNode(@"//ParamList/Param[@name='SysPropertyStore']/Instance/Policy/PolicyName").InnerText.Trim();
                        if (!sOldRecNumber.Equals(sNewRecordnumber))
                        {
                            // i.e. event number was got changed 

                            //sAcroSession =;
                            //obj.Authenticate("asadmin", "password", out sAcroSession, out sAppExcpXml);
                            obj.Authenticate(sTempAcrosoftUserId, sTempAcrosoftPassword, out sAcroSession, out sAppExcpXml);

                            //todo: check for Authenticated AS session 
                            xNdxReindexFldXml = string.Concat(
                                                           "<NDXReindexFldData><IndexName>"
                                                               , "RiskmasterPolicy"
                                                           , "</IndexName><FolderId>"
                                                               , "RMXPOL|"
                                                                , sOldRecNumber  
                                                               , "</FolderId><OldFolderKey><PolicyNbr>"                                                           
                                                               , sOldRecNumber
                                                           , "</PolicyNbr>"
                                                           , "</OldFolderKey>"
                                                           , "<NewFolderKey><PolicyNbr>"
                                                               , sNewRecordnumber
                                                           , "</PolicyNbr></NewFolderKey></NDXReindexFldData>"
                                                           );
                            obj.ReIndexFolder(sAcroSession, xNdxReindexFldXml, out OutString);
                        }
                    }
                    else if ((sFormName.Equals("policyenhgl", StringComparison.OrdinalIgnoreCase))
                        || (sFormName.Equals("policyenhpc", StringComparison.OrdinalIgnoreCase)) 
                        || (sFormName.Equals("policyenhal", StringComparison.OrdinalIgnoreCase))
                        || (sFormName.Equals("policyenhwc", StringComparison.OrdinalIgnoreCase)))
                    {
                        sSql = "SELECT POLICY_NAME FROM POLICY_ENH WHERE POLICY_ID = "
                            + xmlIn.SelectSingleNode(@"//ParamList/Param[@name='SysFormId']").InnerText.Trim();
                        //bIsClaim = true;

                        objReader = DbFactory.GetDbReader(base.connectionString, sSql);
                        if (objReader.Read())
                        {
                            //sXml = objReader.GetValue("PREF_XML").ToString();
                            sOldRecNumber = objReader.GetValue("POLICY_NAME").ToString();
                        }
                        objReader.Close();
                        // sEventNo = xmlIn.SelectSingleNode(@"//ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/Parent/Instance/Event/EventNumber").InnerText.Trim();
                        sNewRecordnumber
                            = xmlIn.SelectSingleNode(@"//ParamList/Param[@name='SysPropertyStore']/Instance/PolicyEnh/PolicyName").InnerText.Trim();
                        if (!sOldRecNumber.Equals(sNewRecordnumber))
                        {
                            // i.e. event number was got changed 

                            //sAcroSession =;
                            //obj.Authenticate("asadmin", "password", out sAcroSession, out sAppExcpXml);
                            obj.Authenticate(sTempAcrosoftUserId, sTempAcrosoftPassword, out sAcroSession, out sAppExcpXml);

                            //todo: check for Authenticated AS session 
                            xNdxReindexFldXml = string.Concat(
                                                           "<NDXReindexFldData><IndexName>"
                                                               , "RiskmasterPolicy"
                                                           , "</IndexName><FolderId>"
                                                               , "RMXPOL|"
                                                                , sOldRecNumber
                                                               , "</FolderId><OldFolderKey><PolicyNbr>"
                                                               , sOldRecNumber
                                                           , "</PolicyNbr>"
                                                           , "</OldFolderKey>"
                                                           , "<NewFolderKey><PolicyNbr>"
                                                               , sNewRecordnumber
                                                           , "</PolicyNbr></NewFolderKey></NDXReindexFldData>"
                                                           );
                            obj.ReIndexFolder(sAcroSession, xNdxReindexFldXml, out OutString);
                        }
                    }
                    // todo: check for additional scenarios where its not a claim or event 



                    // <NDXReindexFldData> 
                    //    <IndexName></IndexName> 
                    //    <FolderId></FolderId> 
                    //    <OldFolderKey> 
                    //        <EventNbr></EventNbr> 
                    //        <ClaimNbr></ClaimNbr> 
                    //    </OldFolderKey>  
                    //    <NewFolderKey> 
                    //        <EventNbr></EventNbr> 
                    //        <ClaimNbr></ClaimNbr> 
                    //    </NewFolderKey>  
                    // </NDXReindexFldData> 




                    if (objReader != null)
                    {
                        if (!objReader.IsClosed)
                        {
                            objReader.Close();
                        }
                        objReader.Dispose();
                    }

                    if (objConn != null)
                    {
                        if (objConn.State == System.Data.ConnectionState.Open)
                        {
                            objConn.Close();
                        }
                        objConn.Dispose();
                    }
                } // if for acrosft enable check
            }

            //rsolanki2 : april 15 2010 : end acrosoft update 
        }

         private void MediaviewFolderReIndex(XmlDocument xmlIn, string sFormName)
        
        {
            //rbhatia4 12 Sep 2011: Mediaview Updates
            
            // if Mediaview is enabled, and if claim number or event number gets changed , 
            // we need to make sure to change int accordingly in Mediaview as well

            
            if (SafeFormVariableParamText("SysCmd") == "5" ) //Save operation 
            {
                Boolean bSuccess = false;
                
                Riskmaster.Settings.SysSettings objSettings = null;
                objSettings = new Riskmaster.Settings.SysSettings(connectionString, base.ClientId); //Ash - cloud
                if (objSettings.UseMediaViewInterface)
                {
                
                    DbConnection objConn = null;
                    DbCommand objCommand = null;
                    //DbParameter objParam = null;
                    Riskmaster.Db.DbReader objReader = null;
                    string sSql = string.Empty, sOldRecNumber = string.Empty;

                    MediaView obj = new MediaView();
                    string OutString = string.Empty;
                    string sNewRecordnumber = string.Empty;
                    string sEventNo = string.Empty;
                    //Boolean bIsClaim = false;

                    objConn = DbFactory.GetDbConnection(base.connectionString);
                    objConn.Open();
                    objCommand = objConn.CreateCommand();
                    //for event folders reindexing
                    if (sFormName.Equals("event", StringComparison.OrdinalIgnoreCase))
                    {
                        sSql = "SELECT EVENT_NUMBER FROM EVENT WHERE EVENT_ID = "
                            + xmlIn.SelectSingleNode(@"//ParamList/Param[@name='SysFormId']").InnerText.Trim();
                        //bIsClaim = false;
                        objReader = DbFactory.GetDbReader(base.connectionString, sSql);
                        if (objReader.Read())
                        {
                            //sXml = objReader.GetValue("PREF_XML").ToString();
                            sOldRecNumber = objReader.GetValue("EVENT_NUMBER").ToString();
                        }
                        objReader.Close();

                        sNewRecordnumber = xmlIn.SelectSingleNode(@"//ParamList/Param[@name='SysPropertyStore']/Instance/Event/EventNumber")
                            .InnerText.Trim();
                        if (!sOldRecNumber.Equals(sNewRecordnumber))
                        {
                            // i.e. event number was got changed 
                            obj.MMUpdate(sOldRecNumber , "Event" , sNewRecordnumber);
                           
                            
                        }
                    }
                    
                    //else if (sFormName.StartsWith("claim", StringComparison.OrdinalIgnoreCase))                        
                    else if (      sFormName.Equals("claimgc", StringComparison.OrdinalIgnoreCase)
                                || sFormName.Equals("claimva", StringComparison.OrdinalIgnoreCase)
                                || sFormName.Equals("claimdi", StringComparison.OrdinalIgnoreCase)
                                || sFormName.Equals("claimpc", StringComparison.OrdinalIgnoreCase)
                                || sFormName.Equals("claimwc", StringComparison.OrdinalIgnoreCase)
                        )
                    //Changed by Gagan for MITS 22264 : end
                    {
                        sSql = "SELECT CLAIM_NUMBER FROM CLAIM WHERE CLAIM_ID = "
                            + xmlIn.SelectSingleNode(@"//ParamList/Param[@name='SysFormId']").InnerText.Trim();
                        //bIsClaim = true;

                        objReader = DbFactory.GetDbReader(base.connectionString, sSql);
                        if (objReader.Read())
                        {
                            //sXml = objReader.GetValue("PREF_XML").ToString();
                            sOldRecNumber = objReader.GetValue("CLAIM_NUMBER").ToString();
                        }
                        objReader.Close();
                        sEventNo = xmlIn.SelectSingleNode(@"//ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/Parent/Instance/Event/EventNumber").InnerText.Trim();
                        sNewRecordnumber = xmlIn.SelectSingleNode(@"//ParamList/Param[@name='SysPropertyStore']/Instance/Claim/ClaimNumber").InnerText.Trim();
                        if (!sOldRecNumber.Equals(sNewRecordnumber))
                        {
                            // i.e. claim number was got changed 
                           obj.MMUpdate(sOldRecNumber , "Claim" , sNewRecordnumber);
                            
                        }
                    }  //for policy folders reindexing
                        //added by skhare7
                    else if (sFormName.Equals("policy", StringComparison.OrdinalIgnoreCase))
                    {
                        sSql = "SELECT POLICY_NAME FROM POLICY WHERE POLICY_ID = "
                            + xmlIn.SelectSingleNode(@"//ParamList/Param[@name='SysFormId']").InnerText.Trim();
                        //bIsClaim = true;

                        objReader = DbFactory.GetDbReader(base.connectionString, sSql);
                        if (objReader.Read())
                        {
                            //sXml = objReader.GetValue("PREF_XML").ToString();
                            sOldRecNumber = objReader.GetValue("POLICY_NAME").ToString();
                        }
                        objReader.Close();
                        sNewRecordnumber 
                            = xmlIn.SelectSingleNode(@"//ParamList/Param[@name='SysPropertyStore']/Instance/Policy/PolicyName").InnerText.Trim();
                        if (!sOldRecNumber.Equals(sNewRecordnumber))
                        {
                            obj.MMUpdate(sOldRecNumber , "Policy" , sNewRecordnumber);
                            // i.e. policy number was got changed 

                           
                        }
                    }
                    else if ((sFormName.Equals("policyenhgl", StringComparison.OrdinalIgnoreCase))
                        || (sFormName.Equals("policyenhpc", StringComparison.OrdinalIgnoreCase)) 
                        || (sFormName.Equals("policyenhal", StringComparison.OrdinalIgnoreCase))
                        || (sFormName.Equals("policyenhwc", StringComparison.OrdinalIgnoreCase)))
                    {
                        sSql = "SELECT POLICY_NAME FROM POLICY_ENH WHERE POLICY_ID = "
                            + xmlIn.SelectSingleNode(@"//ParamList/Param[@name='SysFormId']").InnerText.Trim();
                        //bIsClaim = true;

                        objReader = DbFactory.GetDbReader(base.connectionString, sSql);
                        if (objReader.Read())
                        {
                            //sXml = objReader.GetValue("PREF_XML").ToString();
                            sOldRecNumber = objReader.GetValue("POLICY_NAME").ToString();
                        }
                        objReader.Close();
                        // sEventNo = xmlIn.SelectSingleNode(@"//ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/Parent/Instance/Event/EventNumber").InnerText.Trim();
                        sNewRecordnumber
                            = xmlIn.SelectSingleNode(@"//ParamList/Param[@name='SysPropertyStore']/Instance/PolicyEnh/PolicyName").InnerText.Trim();
                        if (!sOldRecNumber.Equals(sNewRecordnumber))
                        {
                            // i.e. event number was got changed 

                            obj.MMUpdate(sOldRecNumber, "Policy", sNewRecordnumber);
                        }
                    }
                  
                    if (objReader != null)
                    {
                        if (!objReader.IsClosed)
                        {
                            objReader.Close();
                        }
                        objReader.Dispose();
                    }

                    if (objConn != null)
                    {
                        if (objConn.State == System.Data.ConnectionState.Open)
                        {
                            objConn.Close();
                        }
                        objConn.Dispose();
                    }
                } 
            }

            //rbhatia4  12 Sep 2011 : end media view update 
        }

        //Abhishek - MITS 11524 check for org hierarchy permission -Start
        /// <summary>
        /// Function check for orgnisation hierarchy permissions, restricts user to 
        /// edit, add company,dept client.. depending upon permission
        /// </summary>
        /// <param name="xmlIn"></param>
        private bool IsOrgHierarchyType(XmlDocument xmlIn)
        {

            //objForm.GetEntityTableId
            XmlElement objTempNode = null;
            int intEntityTableId = 0;
            
            objTempNode = (XmlElement)xmlIn.SelectSingleNode("//SysFormName");
            //abansal23 09/08/2009 MITS 17408: Users are able to edit and delete entities from the Org Hierarchy when they only have view permissions Starts
            if (objTempNode.InnerText == "entitymaint" || objTempNode.InnerText== "orghierarchymaint") 
            {
                objTempNode = (XmlElement)m_req.SelectSingleNode("//SysFormId");
                if (objTempNode.InnerText == "")
                {
                    objTempNode = (XmlElement)m_req.SelectSingleNode("//Param[@name='SysFormId']");
                }
                if (objTempNode != null)
                {
                    using (LocalCache objCache = new LocalCache(this.connectionString,base.ClientId))
                    {
                        intEntityTableId = objCache.GetOrgTableId(Conversion.ConvertStrToInteger(objTempNode.InnerText));
                    }
                }
                //abansal23 09/08/2009 MITS 17408: Users are able to edit and delete entities from the Org Hierarchy when they only have view permissions Ends
                //Check for org hierarchy table
                if (intEntityTableId >= 1005 && intEntityTableId <= 1012)
                {
                    return true;
                }
            }
            return false;

        }
        //Abhishek - MITS 11524 check for org hierarchy permission -End

        //Abhishek -- Safeway Retrofit Entity Specification -- start 
        //Check for Vendor Entity Type
        private bool IsVendorType(XmlDocument xmlIn)
        {
            LocalCache objCache = null;
            Riskmaster.Settings.SysSettings objSettings = null;         //avipinsrivas Start : Worked for Jira-340
            try
            {
                XmlElement objTempNode = null;
                int intEntityTableId = 0;
                string sSQL = "";
                objCache = new LocalCache(this.connectionString,base.ClientId);                
                objSettings = new Riskmaster.Settings.SysSettings(connectionString, base.ClientId);     //avipinsrivas Start : Worked for Jira-340
                objTempNode = (XmlElement)xmlIn.SelectSingleNode("//SysFormName");
                if (objTempNode.InnerText == "entitymaint")
                {
                    objTempNode = (XmlElement)m_req.SelectSingleNode("//EntityType");
                    if (objTempNode != null) //Required for Entity Maintaince Form Request
                    {
                        //Check for Vendor Entity Type
                        if (Conversion.ConvertStrToInteger(objTempNode.InnerText) == objCache.GetTableId("VENDOR"))
                        {
                            return true;
                        }
                    }
                    else //Required for Entity Search Request
                    {
                        objTempNode = (XmlElement)xmlIn.SelectSingleNode("//SysFormId");
                        if (objTempNode != null)
                        {
                            int iEntityId;
                            if (objTempNode.InnerText == "")
                            {
                                iEntityId = Conversion.ConvertObjToInt(SafeParamText("SysFormId"), base.ClientId);
                            }
                            else
                            {
                                iEntityId = Conversion.ConvertObjToInt(objTempNode.InnerText, base.ClientId);
                            }
                            //avipinsrivas Start : Worked for Jira-340
                            bool bVendorEntityType = false;
                            //sSQL = "SELECT ENTITY_TABLE_ID FROM ENTITY WHERE ENTITY_ID=" + iEntityId;
                            if (objSettings.UseEntityRole)
                                sSQL = "SELECT EXR.ENTITY_TABLE_ID ";
                            else
                                sSQL = "SELECT E.ENTITY_TABLE_ID ";
                            sSQL = string.Concat(sSQL, " FROM ENTITY E");
                            if (objSettings.UseEntityRole)
                                sSQL = string.Concat(sSQL, " INNER JOIN ENTITY_X_ROLES EXR ON EXR.ENTITY_ID = E.ENTITY_ID ");
                            sSQL = string.Concat(sSQL, " WHERE E.ENTITY_ID =" + iEntityId);     //Rijul : Worked for 7475 (Issue of 4634 - Epic 340)
                            //avipinsrivas End
                            using (DbReader objReader = DbFactory.GetDbReader(this.connectionString, sSQL))
                            {
                                while (objReader.Read())
                                {
                                    intEntityTableId = Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("ENTITY_TABLE_ID"), base.ClientId);
                                    //avipinsrivas Start : Worked for Jira-340
                                    if (int.Equals(intEntityTableId, objCache.GetTableId("VENDOR")))
                                    {
                                        bVendorEntityType = true;
                                        break;
                                    }
                                    //avipinsrivas End
                                }
                                objReader.Close();
                            }
                            //Check for Vendor Entity Type
                            //avipinsrivas Start : Worked for Jira-340
                            //if (intEntityTableId == objCache.GetTableId("VENDOR"))
                            if(bVendorEntityType)
                            //avipinsrivas End
                            {
                                return true;
                            }
                        }
                    }
                }

                return false;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            finally
            {
                if (objCache != null)
                    objCache.Dispose();
            }
        }

        // To check if the request is to update or save new entity
        private bool IsNewEntity(XmlDocument xmlIn)
        {
            XmlElement objTempNode = null;
            LocalCache objCache = new LocalCache(this.connectionString,base.ClientId);
            objTempNode = (XmlElement)xmlIn.SelectSingleNode("//SysFormId");
            if (objTempNode.InnerText == "")
            {
                objTempNode = (XmlElement)xmlIn.SelectSingleNode("//Param[@name='SysFormId']");
            }
            if (objTempNode.InnerText == "0" || objTempNode.InnerText == "")
            {
                return true;
            }
            return false;
        }
        //Abhishek -- Safeway Retrofit Entity Specification -- : end
       


        // BSB 09.05.2007 Make Warning Logic work for scripting.
        private bool IsNavigationSuccessfull()
        {

            if (this.m_err.Count == 0)
                return true;

            // Otherwise Filter out Warnings\Message\PopupMessage which do 
            // ride in the m_err collection but do 
            // not really mean the operation has been a complete failure.
            int trueErrorCount = 0;
            foreach (BusinessAdaptorError objErr in this.m_err)
            {
                switch (objErr.ErrorType)
                {
                    case BusinessAdaptorErrorType.SystemError:
                    case BusinessAdaptorErrorType.Error:
                        return false;
                    case BusinessAdaptorErrorType.Warning:
                    case BusinessAdaptorErrorType.Message:
                    case BusinessAdaptorErrorType.PopupMessage:
                        break;
                    default:
                        break;
                };
            }
            return true;
        }
        private FormBase GetFormImplementation(string sFormName)
        {
            Type t = null;
            object[] constructorParams = { this };
            if (_mappings.ContainsKey(sFormName)) //Override with Custom Form (Matches on BaseFormName attribute)
            {
                FormMappingCacheRecord mapping = (_mappings[sFormName] as FormMappingCacheRecord);

                // Load the assembly
                Assembly a = Assembly.LoadFrom(AppDomain.CurrentDomain.BaseDirectory + "\\bin\\" + mapping.Assembly);   // TO DO: Hardwired to go to bin subdirectory right now. Probably needs to change.
                if (a == null)
                {
                    // Throw back system error if cannot load assembly.
                    m_err.Add("Riskmaster.FormDataAdaptor.ErrorLoadingAssembly",
                        Globalization.GetString("Riskmaster.FormDataAdaptor.ErrorLoadingAssembly", base.ClientId, sLangCode),
                        BusinessAdaptorErrorType.SystemError);
                    return null;
                }

                // Get the type of the class
                t = a.GetType(mapping.TypeName, true, true);
                if (t == null)
                {
                    // Throw back system error if cannot load assembly.
                    m_err.Add("Riskmaster.FormDataAdaptor.ErrorLoadingAssembly",
                        Globalization.GetString("Riskmaster.FormDataAdaptor.ErrorLoadingAssembly", base.ClientId, sLangCode),
                        BusinessAdaptorErrorType.SystemError);
                    return null;
                }

            }
            else
            {
                t = Type.GetType("Riskmaster.BusinessAdaptor." + sFormName + "Form", true, true);
            }
            return (FormBase)System.Activator.CreateInstance(t, constructorParams);

        }
        #region BSB Routines for handling FetchView logic.
        //Note: "WithRawParams" indicates that the function consumes parameters directly from the XML Message.
        string FetchFormNameWithRawParams()
        {
            //Extract Parameters from Message
            string sSysSid = SafeFormVariableParamText("SysSid");
            int prevSid = Conversion.ConvertStrToInteger(sSysSid);
            string ret = ViewManager.FormNameToSpecialized(SafeFormVariableParamText("SysFormName"), prevSid);
            //Mukul 3/15/2007 MITS 9019
            switch (ret.ToLower())
            {
                case "claim":
                    ret = ViewManager.GetClaimFormNameFromRecord(Conversion.ConvertStrToInteger(SafeParamText("SysFormId")));
                    break;
                case "entitymaint":
                    //avipinsrivas start : Worked for JIRA - 14586
                    string sNavigatedFormName = SafeFormVariableParamText("SysExData/NavigatedFormName");
                    if (string.IsNullOrEmpty(sNavigatedFormName))
                        ret = ViewManager.GetEntityFormNameFromRecord(Conversion.ConvertStrToInteger(SafeParamText("SysFormId")));
                    else
                        ret = sNavigatedFormName;
                    //avipinsrivas end
                    break;

            }
            //if(ret.ToLower()=="claim")
            //	ret = ViewManager.GetClaimFormNameFromRecord(Conversion.ConvertStrToInteger(SafeParamText("SysFormId")));	
            return ret;
        }
        string FetchViewWithRawParams()
        {
            //Extract Parameters from Message
            string sSysSid = SafeFormVariableParamText("SysSid");
            int prevSid = Conversion.ConvertStrToInteger(sSysSid);

            string sSysViewId = SafeParamText("SysViewId");
            int ViewId = Conversion.ConvertStrToInteger(sSysViewId);

            string sSysRecordId = SafeFormVariableParamText("SysFormId");
            int RecordId = Conversion.ConvertStrToInteger(sSysRecordId);

            string sFormName = FetchFormNameWithRawParams();
            // BSB 02.13.2007 Determine if Extensibility Form Mapping is in use, 
            // If in use AND no power-view defined then swap it in.
            // Note that PV case is handled inside of PV Editor which will create 
            // views based on the appropriate extended definition.
            if (_mappings.ContainsKey(sFormName) && ViewId == Constants.Views.BASE_VIEWID)
                ViewId = Constants.Views.EXTENDED_SCREEN_VIEWID;

            //Execute Logic
            return ViewManager.FetchView(prevSid, ViewId, RecordId, sFormName);

        }
        #endregion
        #region BSB Routines for Applying View Default Values into Instance\FormVariables section.
        //(Replaces old xforms-model XPL logic)

        //BSB Invoked to clear certain Instance\FormVariables
        private void InitPostBackFormVariables(ref string sView)
        {
            //1.) Clear SysEx
            this.SafeSaveFormVariableParamXml("SysEx", "");

            //2.) Only if view could have been corrupted do we allow a Fresh Legacy View to be used.  
            //BSB Explanation:  Deals with a "bug" in orbeon where these attributes are stripped even from the instance 
            // document (not just the xform host document) during each client server round trip.
            // BSB Potential Complications:  This essentially means that lookup or other 
            //if(sView.IndexOf("xxforms:")<=0) //Note dependence on namespace prefix here since we're text processing.
            //	sView = SafeFormVariableParam("SysView").InnerXml;

            return;
        }
        //BSB Invoked to initialize certain Instance\FormVariables from the view.
        private void InitFormVariables(string sView)
        {
            XmlDocument objDom = new XmlDocument(_nt);
            objDom.LoadXml(sView);

            XmlElement objFormVariables = (this.SafeParam("SysFormVariables").FirstChild as XmlElement);
            string sRef = "";
            foreach (XmlElement objVariable in objFormVariables.ChildNodes)
            {
                switch (objVariable.Name)
                {
                    // FormVariables that should never be defaulted\cleared.
                    case "SysCmd":
                    case "SysFormId":
                    case "SysFormPId":
                    case "SysFormPForm":
                    case "SysViewType":
                    case "SysSysExData":
                        break;
                    default:
                        sRef = "Instance/UI/FormVariables/" + objVariable.Name;
                        XmlElement objSrc = objDom.SelectSingleNode(String.Format("//xforms:input[@ref='{0}'] | //internal[@ref='{0}']", sRef), _nsmgr) as XmlElement;

                        //This is a hierarchy of superceding value sources.
                        objVariable.InnerText = "";
                        objVariable.InnerXml = "";
                        if (objSrc == null)
                            continue;
                        if (objSrc.HasAttribute("rmxforms:value"))
                            objVariable.InnerText = objSrc.GetAttribute("rmxforms:value");
                        if (objSrc.HasAttribute("value"))
                            objVariable.InnerText = objSrc.GetAttribute("value");
                        if (objSrc.InnerXml != "")
                            objVariable.InnerXml = objSrc.InnerXml;
                        break;
                }
            }
            this.SafeSaveParamText("SysScreenAction", "");
            //this.SafeSaveFormVariableParamXml("SysView",sView);
        }

        #endregion

        #region In Progress BSB
        //For now pretty straight-forward since list screens are just read-only.
        private string NavList(ListFormBase objForm)
        {

            //BSB Hack - Special case since ADMTableList isn't a "full blown"
            // DataCollection.  Just get the available tables and get out.
            if (objForm is AdminTrackingListForm)
                return (objForm as AdminTrackingListForm).SerializeTableList();


            string sValue = "";
            string sSysCmd = SafeFormVariableParamText("SysCmd");
            XmlDocument propertyStore;

            string sPropertyStore = "";
            if (HasParam("SysPropertyStore"))
                sPropertyStore = SafeParam("SysPropertyStore").InnerXml;

            string sSerializationConfig = "";
            //kchoudhary2, 15th Dec 2006, MITS 7822
            //			if(HasParam("SysSerializationConfig"))
            //				sSerializationConfig = SafeFormVariableParam("SysSerializationConfig").InnerXml;

            if (this.HasFormVariableParam("SysSerializationConfig"))
                sSerializationConfig = SafeFormVariableParam("SysSerializationConfig").InnerXml;



            switch (sSysCmd)
            {
                case "0": // Default - Just fall through and Show Current Contents
                case "1":
                    if (!base.userLogin.IsAllowedEx(objForm.SecurityId, FormBase.RMO_ACCESS))
                        objForm.LogSecurityError(FormBase.RMO_ACCESS);
                    break;

                case "6": //Delete\Remove
                    propertyStore = new XmlDocument();
                    propertyStore.LoadXml(sPropertyStore);
                    objForm.Remove(propertyStore);
                    //objForm.objDataList.PopulateObject(propertyStore); //Update the in memory instance 
                    //Note that deleted items arrive tagged and are
                    //removed automatically.
                    break;
                case "7": //Post-back (Example: Used to show the "choose item type" pi sceen.
                    //Adding an Item to the database is the responsibility of a child screen 
                    //to save it's own contents. In general the List screen is "read/remove" only.
                    //In special cases, an "add" operation may require an intermediate screen in order 
                    // to choose the type of child item to be added.  In this case we can use a new 
                    // SysCmd value for "PostBack" to generate a "new screen" from the same form class.
                    //propertyStore = new XmlDocument();
                    //propertyStore.LoadXml(sPropertyStore);
                    //objForm.objDataList.PopulateObject(propertyStore); //Update the in memory instance 
                    objForm.Refresh();
                    break;
            }

            if (sSerializationConfig != "")
            {
                XmlDocument dom = new XmlDocument();
                sSerializationConfig = sSerializationConfig.Replace("&lt;", "<").Replace("&gt;", ">");
                dom.LoadXml(sSerializationConfig);
                if (objForm.objDataList != null)
                    sValue = objForm.objDataList.SerializeObject(dom);
            }
            else
                if (objForm.objDataList != null)
                    sValue = objForm.objDataList.SerializeObject();

            return sValue;
        }

        //Basically stubbed until NavList and list screens are completed.
        // Then we can navigate from a list to a specific ADM screen.
        //TODO Test...
        private string NavAdm(AdminTrackingForm objForm)
        {
            string sValue = "";
            string sSysCmd = SafeFormVariableParamText("SysCmd");
            string sSysFormId = SafeParamText("SysFormId");
            if (sSysFormId == "") sSysFormId = "0";

            string sSerializationConfig = "";
            if (HasFormVariableParam("SysSerializationConfig"))
                sSerializationConfig = SafeFormVariableParam("SysSerializationConfig").InnerXml;

            string sPropertyStore = "";
            if (HasParam("SysPropertyStore"))
                sPropertyStore = SafeParam("SysPropertyStore").InnerXml;

            switch (sSysCmd)
            {
                case "": //New
                    //Already done at this point by default... (TODO: enhance for "new with specific LOB already applied")
                    // BSB Note: Allow a place to ensure that all Derived fields (or their placeholders)
                    // are made available in the instance data returned (SysExData) 
                    // Orbeon get's nasty if any "ref'ed" node isn't present.
                    objForm.AddNew(); //zmohammad MITs 33695
                    //objForm.UpdateForm();  
                    break;
                case "0": //Move To
                    objForm.MoveTo(Convert.ToInt32(sSysFormId));
                    break;
                case "1": //Move First
                    objForm.MoveFirst();
                    break;
                case "2": //Move Previous
                    objForm.objAdm.KeyValue = Convert.ToInt32(sSysFormId);
                    objForm.MovePrevious();
                    //Test for "on first record already"
                    if (objForm.objAdm.KeyValue == Convert.ToInt32(sSysFormId))
                    {
                        objForm.objAdm.KeyValue = 0;
                        objForm.MoveTo(Convert.ToInt32(sSysFormId));
                    }
                    break;
                case "3": //Move Next
                    objForm.objAdm.KeyValue = Convert.ToInt32(sSysFormId);
                    objForm.MoveNext();
                    //Test for "on first record already"
                    if (objForm.objAdm.KeyValue == Convert.ToInt32(sSysFormId))
                    {
                        objForm.objAdm.KeyValue = 0;
                        objForm.MoveTo(Convert.ToInt32(sSysFormId));
                    }
                    break;
                case "4": //Move Last
                    objForm.MoveLast();
                    break;
                //MITS 13151:Asif Start
                case "9": //Confirm Save (User action was interrupted by save confirmation question)
                //MITS 13151:Asif End
                case "5": //Save
                    XmlDocument propertyStore = new XmlDocument();
                    propertyStore.LoadXml(sPropertyStore);
                    //TODO objForm.ValidateRequiredViewFields(propertyStore);
                    objForm.ResetSysSections();
                    objForm.objAdm.PopulateObject(propertyStore); //Update the in memory instance.
                    objForm.UpdateObject();
                    objForm.Save();
                    objForm.Refresh(); //zmohammad MITs 33695
                    //BSB Handle Failed Save during Interrupted User Action
                    // Clear out user Action in case of save failure.
                    if (!this.IsNavigationSuccessfull() || objForm.Cancelled)
                    {
                        //Restore the original view since we're not going to run "updateform."
                        //objForm.RestorePostedViewData();
                        CancelScriptAction(objForm);
                    }
                    break;
                case "6": //Delete
                    objForm.MoveTo(Convert.ToInt32(sSysFormId));
                    objForm.Delete();
                    break;
                case "7":	//Post-back (Example: When we change Jurisdiction value from ClaimInfo tab 
                    //then we need to reload the Juridiction tab fields
                    propertyStore = new XmlDocument();
                    propertyStore.LoadXml(sPropertyStore);
                    objForm.objAdm.PopulateObject(propertyStore); //Update the in memory instance.
                    objForm.UpdateObject();
                    objForm.UpdateForm();
                    break;
                case "8":	//Data Lookup. (Example: Entity Lookup)
                    return objForm.ApplyLookupData();
            }

            //if (IsNavigationSuccessfull())
            //{
            if (sSerializationConfig != "")
            {
                XmlDocument dom = new XmlDocument();
                //For R5 
                sSerializationConfig = sSerializationConfig.Replace("&lt;", "<").Replace("&gt;", ">");
                dom.LoadXml(sSerializationConfig);
                sValue = objForm.objAdm.SerializeObject(dom);
            }
            else
            {
                //objForm.objAdm.Load();//Commented for MITS 18475, no need to call load() again
                sValue = objForm.objAdm.SerializeObject();
            }

            
            //}
            return sValue;
        }
        #endregion

        private string NavDataEntry(DataEntryFormBase objForm)
        {
            string sValue = "";
            string sSysCmd = SafeFormVariableParamText("SysCmd");
            string sSysFormId = SafeParamText("SysFormId");
            bool bConfirmSave = false;
            // if (sSysFormId == "") sSysFormId = "0"; Mits 32157
            if (String.IsNullOrEmpty( sSysFormId)) sSysFormId = "0"; 

            string sSerializationConfig = "";
            if (this.HasFormVariableParam("SysSerializationConfig"))
                sSerializationConfig = SafeFormVariableParam("SysSerializationConfig").InnerXml;

            string sPropertyStore = "";
            if (HasParam("SysPropertyStore"))
                sPropertyStore = SafeParam("SysPropertyStore").InnerXml;

            switch (sSysCmd)
            {
                case "":
                    //MITS 11524 - Abhishek start
                    if (m_bIsOrgHieryField)
                    {
                        if (!(base.userLogin.IsAllowedEx(ORG_CREATE_PERMISSION)))
                            throw new PermissionViolationException(RMPermissions.RMO_CREATE, ORG_CREATE_PERMISSION);

                    }
                    //MITS 11524 - Abhishek end
                    //New
                    //Already done at this point by default... (TODO: enhance for "new with specific LOB already applied")
                    // BSB Note: Allow a place to ensure that all Derived fields (or their placeholders)
                    // are made available in the instance data returned (SysExData) 
                    // Orbeon get's nasty if any "ref'ed" node isn't present.
                    //Abhishek -- Safeway Retrofit Entity Specification -- 5/28/2008 : start
                    if (m_bIsVendorField)
                    {
                        if (!(base.userLogin.IsAllowedEx(ENT_VENDOR_CREATE_PERMISSION)))
                            throw new Exception(Globalization.GetString("Permission.NoCreate", base.ClientId, sLangCode));
                    }
                    //Abhishek -- Safeway Retrofit Entity Specification -- 5/28/2008 : end
					
                    objForm.AddNew();
                    //objForm.UpdateForm();  
                    break;
                case "0": //Move To
                    //MITS 11524 - Abhishek start
                    if (m_bIsOrgHieryField)
                    {
                        if (!(base.userLogin.IsAllowedEx(ORG_VIEW_PERMISSION)))
                            throw new PermissionViolationException(RMPermissions.RMO_ACCESS, ORG_VIEW_PERMISSION);

                    }
                    //MITS 11524 - Abhishek end
                    //Abhishek -- Safeway Retrofit Entity Specification -- 5/28/2008 : start
                    if (m_bIsVendorField)
                    {
                        if (!(base.userLogin.IsAllowedEx(ENT_VENDOR_VIEW_PERMISSION)))
                            throw new Exception(Globalization.GetString("Permission.NoView", base.ClientId, sLangCode));
                    }
                    //Abhishek -- Safeway Retrofit Entity Specification -- 5/28/2008 : end
					
                    objForm.MoveTo(Convert.ToInt32(sSysFormId));
                    break;
                case "1": //Move First
                    //Atul -- Safeway Entity Specification -- 5/28/2008 : start
                    if (m_bIsVendorField)
                    {
                        if (!(base.userLogin.IsAllowedEx(ENT_VENDOR_VIEW_PERMISSION)))
                            throw new Exception(Globalization.GetString("Permission.NoView", base.ClientId, sLangCode));
                    }
                    //Atul -- Safeway Entity Specification -- 5/28/2008 : end
					
                    objForm.MoveFirst();
                    break;
                case "2": //Move Previous
                    //Atul -- Safeway Entity Specification -- 5/28/2008 : start
                    if (m_bIsVendorField)
                    {
                        if (!(base.userLogin.IsAllowedEx(ENT_VENDOR_VIEW_PERMISSION)))
                            throw new Exception(Globalization.GetString("Permission.NoView", base.ClientId, sLangCode));
                    }
                    //Atul -- Safeway Entity Specification -- 5/28/2008 : end
				
                    objForm.objData.KeyFieldValue = Convert.ToInt32(sSysFormId);
                    objForm.MovePrevious();
                    //Test for "on first record already"
                    if (objForm.objData.KeyFieldValue == Convert.ToInt32(sSysFormId))
                    {
                        if (objForm.objData.KeyFieldValue <= 0)
                        {
                            objForm.InitNew();
                            objForm.MoveFirst();
                        }
                        else
                        {
                            objForm.InitNew();
                            objForm.MoveTo(Convert.ToInt32(sSysFormId));
                        }
                    }
                    break;
                case "3": //Move Next
                    //Atul -- Safeway Entity Specification -- 5/28/2008 : start
                    if (m_bIsVendorField)
                    {
                        if (!(base.userLogin.IsAllowedEx(ENT_VENDOR_VIEW_PERMISSION)))
                            throw new Exception(Globalization.GetString("Permission.NoView", base.ClientId, sLangCode));
                    }
                    //Atul -- Safeway Entity Specification -- 5/28/2008 : end
				
                    objForm.objData.KeyFieldValue = Convert.ToInt32(sSysFormId);
                    objForm.MoveNext();
                    //Test for "on first record already"
                    if (objForm.objData.KeyFieldValue == Convert.ToInt32(sSysFormId)
                        || objForm.objData.KeyFieldValue <= 0)
                    {
                        objForm.InitNew();
                        objForm.MoveTo(Convert.ToInt32(sSysFormId));
                    }
                    break;
                case "4": //Move Last
                    //Atul -- Safeway Entity Specification -- 5/28/2008 : start
                    if (m_bIsVendorField)
                    {
                        if (!(base.userLogin.IsAllowedEx(ENT_VENDOR_VIEW_PERMISSION)))
                            throw new Exception(Globalization.GetString("Permission.NoView", base.ClientId, sLangCode));
                    }
                    //Atul -- Safeway Entity Specification -- 5/28/2008 : end
				
                    objForm.MoveLast();
                    break;
                case "9": //Confirm Save (User action was interrupted by save confirmation question)
                case "5": //Save
                    //MITS 11524 - Abhishek start
                    if (m_bIsOrgHieryField)
                    {
                        if (!(base.userLogin.IsAllowedEx(ORG_UPDATE_PERMISSION)))
                            throw new PermissionViolationException(RMPermissions.RMO_UPDATE, ORG_UPDATE_PERMISSION);

                    }
                    //MITS 11524 - Abhishek end
                    //Atul -- Safeway Entity Specification -- 5/28/2008 : start
                    if (m_bIsVendorField)
                    {
                        if (m_bIsNewEntity) //Create Permission Check for New Entity
                        {
                            if (!(base.userLogin.IsAllowedEx(ENT_VENDOR_CREATE_PERMISSION)))
                                throw new Exception(Globalization.GetString("Permission.NoCreate", base.ClientId, sLangCode));
                        }
                        else //Update Permission Check for Existing Entity
                        {
                            if (!(base.userLogin.IsAllowedEx(ENT_VENDOR_UPDATE_PERMISSION)))
                                throw new Exception(Globalization.GetString("Permission.NoUpdate", base.ClientId, sLangCode));
                        }
                    }
                    //Atul -- Safeway Entity Specification -- 5/28/2008 : end

                    //  bConfirmSave = (sSysCmd == "9"); //Mits  32157
                    bConfirmSave = (sSysCmd.Equals("9"));
                    //Prepare for Validation
                    XmlDocument propertyStore = new XmlDocument();
                    propertyStore.LoadXml(sPropertyStore);
                    XmlElement objElt = propertyStore.CreateElement(objForm.SysEx.DocumentElement.LocalName);
                    objElt.InnerXml = objForm.SysEx.DocumentElement.InnerXml;

                    //-- ABhateja, 08.14.2006, MITS 7627 -START-
                    //-- Append the "Currently Invisible" fields to the SysExData section
                    //-- of propertyStore.
                    string sSysInv = "";
                    if (HasFormVariableParam("SysInvisible"))
                    {
                        sSysInv = SafeFormVariableParam("SysInvisible").InnerXml;
                        //start 32157
                        //if (sSysInv != "")
                        if (!string.IsNullOrEmpty(sSysInv))
                        {
                            XmlElement objInv = propertyStore.CreateElement("SysInvisible");
                            objInv.InnerXml = sSysInv;

                            objElt.InsertBefore(objInv, objElt.FirstChild);
                        }
                    }
                    //-- ABhateja, 08.14.2006, MITS 7627 -END-

                    propertyStore.DocumentElement.InsertBefore(objElt, propertyStore.DocumentElement.FirstChild);
                    bool bIsValid = (objForm.ValidateRequiredViewFields(propertyStore));
                    //Clean Up from Validation
                    propertyStore.DocumentElement.RemoveChild(propertyStore.DocumentElement.FirstChild);
                    objForm.ResetSysSections();

                    // BSB 01.31.2006 Hack to quietly enforce Supplemental Edit Permissions
                    // Just Trim off incoming Supp data values before population.
                    //bool bIsSuppEditAllowed = base.userLogin.IsAllowedEx(objForm.SecurityId + FormBase.RMO_SUPP, FormBase.RMO_UPDATE);
                    //Updated by geeta sharma for mits 10729
                    //bool bIsSuppEditAllowed = base.userLogin.IsAllowedSuppEx(objForm.SecurityId + FormBase.RMO_SUPP, FormBase.RMO_UPDATE);
                    //objForm.IsSuppUpdatePermission = bIsSuppEditAllowed;//Added for Mits 22873
                    //XmlElement suppData = (XmlElement)propertyStore.SelectSingleNode("/Instance/*/Supplementals");
                    //if (suppData != null && !bIsSuppEditAllowed)
                    //{
                    //    suppData.ParentNode.RemoveChild(suppData);
                    //    //m_err.Add("Warning",Globalization.GetString("Permission.NoSuppUpdate"),BusinessAdaptorErrorType.Warning);

                    //} //deb

                    // BSB 02.23.2006 Hack to quietly enforce Jurisdictional Edit Permissions
                    // Just Trim off incoming Juris data values before population.
                    bool bIsJurisEditAllowed = base.userLogin.IsAllowedEx(objForm.SecurityId + FormBase.JUR_OFFSET, FormBase.RMO_UPDATE);
                    XmlElement jurisData = (XmlElement)propertyStore.SelectSingleNode("/Instance/*/Jurisdictionals");
                    if (jurisData != null && !bIsJurisEditAllowed)
                    {
                        jurisData.ParentNode.RemoveChild(jurisData);
                        //m_err.Add("Warning",Globalization.GetString("Permission.NoJurisUpdate"),BusinessAdaptorErrorType.Warning);
                    }

                    if (propertyStore.FirstChild.ChildNodes.Count > 0)
                    objForm.objData.PopulateObject(propertyStore); //Update the in memory instance.
                    objForm.UpdateObject();
                    if (bIsValid)
                        objForm.Save();

                    //BSB Handle Failed Save during Interrupted User Action
                    // Clear out user Action in case of save failure.
                    //if(this.m_err.Count>0 || !bIsValid || objForm.Cancelled)
                    if (!this.IsNavigationSuccessfull() || !bIsValid || objForm.Cancelled)
                    {
                        //Restore the original view since we're not going to run "updateform."
                        //Mjain8 19th sept commented out as we dont need it anymore otherwise it will override the actual error
                        //objForm.RestorePostedViewData();
                        //smishra25:Adding the switch statement.
                        if (!objForm.b_UpdateFormCalledOnValidationFailure)
                        {
                            switch (objForm.m_ClassName.ToUpper().Trim())
                            {
                                case "FUNDS":
                                case "POLICYENH":
                                // npadhy Start MITS 20329 Grid does render properly if some error comes. 
                                // Adding the handling for all the Grid screens
                                case "ENTITY":
                                case "PATIENT":
                                case "EMPLOYEE":
                                case "LEAVE":
                                case "PHYSICIAN":
                                case "MEDICALSTAFF":
                                case "ORGHIERARCHY":
                                case "FUNDSAUTOBATCH"://Add by kuladeep for mits:31109
                                // npadhy End MITS 20329 Grid does render properly if some error comes. 
                                // Adding the handling for all the Grid screens
                                    objForm.UpdateForm();//Calling update form for MITS 18937.
                                    break;
                                default:
                                    break;

                            }
                        }
                        
                        if (bConfirmSave)
                            CancelScriptAction(objForm);
                    }
                    break;
                case "6": //Delete
                    //MITS 11524 - Abhishek start
                    if (m_bIsOrgHieryField)
                    {
                        if (!(base.userLogin.IsAllowedEx(ORG_DELETE_PERMISSION)))
                            throw new PermissionViolationException(RMPermissions.RMO_DELETE, ORG_DELETE_PERMISSION);

                    }
                    //MITS 11524 - Abhishek end
                    //Atul -- Safeway Entity Specification -- 5/28/2008 : start
                    if (m_bIsVendorField)
                    {
                        if (!(base.userLogin.IsAllowedEx(ENT_VENDOR_DELETE_PERMISSION)))
                            throw new Exception(Globalization.GetString("Permission.NoDelete", base.ClientId, sLangCode));
                    }
                    //Atul -- Safeway Entity Specification -- 5/28/2008 : end
					
                    objForm.MoveTo(Convert.ToInt32(sSysFormId));
                    objForm.Delete();
                    break;
                case "7":	//Post-back (Example: When we change Jurisdiction value from ClaimInfo tab 
                    //then we need to reload the Juridiction tab fields
                    if (m_bIsVendorField)
                    {
                        if (!(base.userLogin.IsAllowedEx(ENT_VENDOR_CREATE_PERMISSION)))
                            throw new Exception(Globalization.GetString("Permission.NoCreate", base.ClientId, sLangCode));
                    }
                    propertyStore = new XmlDocument();
                    propertyStore.LoadXml(sPropertyStore);
                    if (propertyStore.FirstChild.ChildNodes.Count > 0)
                    objForm.objData.PopulateObject(propertyStore); //Update the in memory instance.
                    objForm.UpdateObject();
                    objForm.UpdateForm();
                    // BSB 11.22.2005 
                    // Force negative "new placeholder" key values back to zero for further
                    // consumption by script.
                    if (objForm.objData.IsNew)
                        objForm.objData.KeyFieldValue = 0;

                    break;
                case "8":	//Data Lookup. (Example: Entity Lookup)
                    return objForm.ApplyLookupData();
            }

            // Clear out any cached Comments that are lying around if we revisit a record
            //  if (sSysCmd == "0" || sSysCmd == "1" || sSysCmd == "2" || sSysCmd == "3" || sSysCmd == "4") // Mits 32157
            if (sSysCmd.Equals( "0") || sSysCmd .Equals("1") || sSysCmd.Equals( "2") || sSysCmd.Equals( "3") || sSysCmd.Equals("4"))
            {
                string sFormName = objForm.GetType().Name;
                sFormName = sFormName.Substring(0, sFormName.Length - 4).ToLower(); // take off Form on end
                string sKey = sFormName.ToLower() + "|" + objForm.objData.KeyFieldValue.ToString();

                if (this.cachedObjectExists(sKey + "|Comments"))
                    this.removeCachedObject(sKey + "|Comments");
                if (this.cachedObjectExists(sKey + "|HTMLComments"))
                    this.removeCachedObject(sKey + "|HTMLComments");
            }

            if (sSerializationConfig != "")
            {
                XmlDocument dom = new XmlDocument();
                //For R5 test
                //Added by bhsarma33 - Pan testing
                while (sSerializationConfig.Contains("&amp;"))
                    sSerializationConfig = sSerializationConfig.Replace("&amp;", "&");
                //End additions by bhsarma33 - Pan testing
                sSerializationConfig = sSerializationConfig.Replace("&lt;", "<").Replace("&gt;", ">");
                dom.LoadXml(sSerializationConfig);
                sValue = objForm.objData.SerializeObject(dom, objForm.GetTrimHashTable());
                //sValue = objForm.objData.SerializeObject(objForm.GetTrimHashTable());
                //sValue = objForm.objData.SerializeObject(dom);

            }
            else
            {
                //sValue = objForm.objData.SerializeObject();
                sValue = objForm.objData.SerializeObject(objForm.GetTrimHashTable());
            }

            return sValue;
        }

        private bool DefaultResponse()
        {
            m_resp = m_req;
            return true;
        }
        void CancelScriptAction(DataEntryFormBase objForm)
        {
            SafeSaveFormVariableParamText("SysCmdQueue", "");
        }
        private string NavDataEntryRecordSummary(DataEntryFormBase objForm)
        {
            string sValue = "";
            string sSysCmd = SafeFormVariableParamText("SysCmd");
            string sSysFormId = SafeParamText("SysFormId");
            bool bConfirmSave = false;
            if (sSysFormId == "") sSysFormId = "0";

            string sSerializationConfig = "";
            if (objForm.SysView.SelectSingleNode("//internal[@name='SysSerializationConfig']") != null)
                sSerializationConfig = objForm.SysView.SelectSingleNode("//internal[@name='SysSerializationConfig']").InnerXml;
            string sPropertyStore = "";
            if (HasParam("SysPropertyStore"))
                sPropertyStore = SafeParam("SysPropertyStore").InnerXml;

            switch (sSysCmd)
            {
                case "":
                    //MITS 11524 - Abhishek start
                    if (m_bIsOrgHieryField)
                    {
                        if (!(base.userLogin.IsAllowedEx(ORG_CREATE_PERMISSION)))
                            throw new PermissionViolationException(RMPermissions.RMO_CREATE, ORG_CREATE_PERMISSION);

                    }
                    //MITS 11524 - Abhishek end
                    //New
                    //Already done at this point by default... (TODO: enhance for "new with specific LOB already applied")
                    // BSB Note: Allow a place to ensure that all Derived fields (or their placeholders)
                    // are made available in the instance data returned (SysExData) 
                    // Orbeon get's nasty if any "ref'ed" node isn't present.
                    objForm.AddNew();
                    //objForm.UpdateForm();  
                    break;
                case "0": //Move To
                    //MITS 11524 - Abhishek start
                    if (m_bIsOrgHieryField)
                    {
                        if (!(base.userLogin.IsAllowedEx(ORG_VIEW_PERMISSION)))
                            throw new PermissionViolationException(RMPermissions.RMO_ACCESS, ORG_VIEW_PERMISSION);

                    }
                    //MITS 11524 - Abhishek end
                    objForm.MoveTo(Convert.ToInt32(sSysFormId));
                    break;
                case "1": //Move First
                    objForm.MoveFirst();
                    break;
                case "2": //Move Previous
                    objForm.objData.KeyFieldValue = Convert.ToInt32(sSysFormId);
                    objForm.MovePrevious();
                    //Test for "on first record already"
                    if (objForm.objData.KeyFieldValue == Convert.ToInt32(sSysFormId))
                    {
                        if (objForm.objData.KeyFieldValue <= 0)
                        {
                            objForm.InitNew();
                            objForm.MoveFirst();
                        }
                        else
                        {
                            objForm.InitNew();
                            objForm.MoveTo(Convert.ToInt32(sSysFormId));
                        }
                    }
                    break;
                case "3": //Move Next
                    objForm.objData.KeyFieldValue = Convert.ToInt32(sSysFormId);
                    objForm.MoveNext();
                    //Test for "on first record already"
                    if (objForm.objData.KeyFieldValue == Convert.ToInt32(sSysFormId)
                        || objForm.objData.KeyFieldValue <= 0)
                    {
                        objForm.InitNew();
                        objForm.MoveTo(Convert.ToInt32(sSysFormId));
                    }
                    break;
                case "4": //Move Last
                    objForm.MoveLast();
                    break;
                case "9": //Confirm Save (User action was interrupted by save confirmation question)
                case "5": //Save
                    //MITS 11524 - Abhishek start
                    if (m_bIsOrgHieryField)
                    {
                        if (!(base.userLogin.IsAllowedEx(ORG_UPDATE_PERMISSION)))
                            throw new PermissionViolationException(RMPermissions.RMO_UPDATE, ORG_UPDATE_PERMISSION);

                    }
                    //MITS 11524 - Abhishek end
                    //  bConfirmSave = (sSysCmd == "9"); //Mits  32157
                    bConfirmSave = (sSysCmd.Equals( "9"));

                    //Prepare for Validation
                    XmlDocument propertyStore = new XmlDocument();
                    propertyStore.LoadXml(sPropertyStore);
                    XmlElement objElt = propertyStore.CreateElement(objForm.SysEx.DocumentElement.LocalName);
                    objElt.InnerXml = objForm.SysEx.DocumentElement.InnerXml;

                    //-- ABhateja, 08.14.2006, MITS 7627 -START-
                    //-- Append the "Currently Invisible" fields to the SysExData section
                    //-- of propertyStore.
                    string sSysInv = "";
                    if (HasFormVariableParam("SysInvisible"))
                    {
                        sSysInv = SafeFormVariableParam("SysInvisible").InnerXml;
                        //start 32157
                        //if (sSysInv != "")
                        if (!string.IsNullOrEmpty(sSysInv))
                        {
                            XmlElement objInv = propertyStore.CreateElement("SysInvisible");
                            objInv.InnerXml = sSysInv;

                            objElt.InsertBefore(objInv, objElt.FirstChild);
                        }
                    }
                    //-- ABhateja, 08.14.2006, MITS 7627 -END-

                    propertyStore.DocumentElement.InsertBefore(objElt, propertyStore.DocumentElement.FirstChild);
                    bool bIsValid = (objForm.ValidateRequiredViewFields(propertyStore));
                    //Clean Up from Validation
                    propertyStore.DocumentElement.RemoveChild(propertyStore.DocumentElement.FirstChild);
                    objForm.ResetSysSections();

                    // BSB 01.31.2006 Hack to quietly enforce Supplemental Edit Permissions
                    // Just Trim off incoming Supp data values before population.
                    //bool bIsSuppEditAllowed = base.userLogin.IsAllowedEx(objForm.SecurityId + FormBase.RMO_SUPP, FormBase.RMO_UPDATE);
                    //Updated by geeta sharma for mits 10729
                    bool bIsSuppEditAllowed = base.userLogin.IsAllowedSuppEx(objForm.SecurityId + FormBase.RMO_SUPP, FormBase.RMO_UPDATE);
                    XmlElement suppData = (XmlElement)propertyStore.SelectSingleNode("/Instance/*/Supplementals");
                    if (suppData != null && !bIsSuppEditAllowed)
                    {
                        suppData.ParentNode.RemoveChild(suppData);
                        //m_err.Add("Warning",Globalization.GetString("Permission.NoSuppUpdate"),BusinessAdaptorErrorType.Warning);

                    }

                    // BSB 02.23.2006 Hack to quietly enforce Jurisdictional Edit Permissions
                    // Just Trim off incoming Juris data values before population.
                    bool bIsJurisEditAllowed = base.userLogin.IsAllowedEx(objForm.SecurityId + FormBase.JUR_OFFSET, FormBase.RMO_UPDATE);
                    XmlElement jurisData = (XmlElement)propertyStore.SelectSingleNode("/Instance/*/Jurisdictionals");
                    if (jurisData != null && !bIsJurisEditAllowed)
                    {
                        jurisData.ParentNode.RemoveChild(jurisData);
                        //m_err.Add("Warning",Globalization.GetString("Permission.NoJurisUpdate"),BusinessAdaptorErrorType.Warning);
                    }
                    objForm.objData.PopulateObject(propertyStore); //Update the in memory instance.
                    objForm.UpdateObject();
                    if (bIsValid)
                        objForm.Save();

                    //BSB Handle Failed Save during Interrupted User Action
                    // Clear out user Action in case of save failure.
                    //if(this.m_err.Count>0 || !bIsValid || objForm.Cancelled)
                    if (!this.IsNavigationSuccessfull() || !bIsValid || objForm.Cancelled)
                    {
                        //Restore the original view since we're not going to run "updateform."
                        //Mjain8 19th sept commented out as we dont need it anymore otherwise it will override the actual error
                        //objForm.RestorePostedViewData();
                        if (bConfirmSave)
                            CancelScriptAction(objForm);
                    }
                    break;
                case "6": //Delete
                    //MITS 11524 - Abhishek start
                    if (m_bIsOrgHieryField)
                    {
                        if (!(base.userLogin.IsAllowedEx(ORG_DELETE_PERMISSION)))
                            throw new PermissionViolationException(RMPermissions.RMO_DELETE, ORG_DELETE_PERMISSION);

                    }
                    //MITS 11524 - Abhishek end
                    objForm.MoveTo(Convert.ToInt32(sSysFormId));
                    objForm.Delete();
                    break;
                case "7":	//Post-back (Example: When we change Jurisdiction value from ClaimInfo tab 
                    //then we need to reload the Juridiction tab fields
                    propertyStore = new XmlDocument();
                    propertyStore.LoadXml(sPropertyStore);
                    objForm.objData.PopulateObject(propertyStore); //Update the in memory instance.
                    objForm.UpdateObject();
                    objForm.UpdateForm();
                    // BSB 11.22.2005 
                    // Force negative "new placeholder" key values back to zero for further
                    // consumption by script.
                    if (objForm.objData.IsNew)
                        objForm.objData.KeyFieldValue = 0;

                    break;
                case "8":	//Data Lookup. (Example: Entity Lookup)
                    return objForm.ApplyLookupData();
            }

            // Clear out any cached Comments that are lying around if we revisit a record
            if (sSysCmd == "0" || sSysCmd == "1" || sSysCmd == "2" || sSysCmd == "3" || sSysCmd == "4")
            {
                string sFormName = objForm.GetType().Name;
                sFormName = sFormName.Substring(0, sFormName.Length - 4).ToLower(); // take off Form on end
                string sKey = sFormName.ToLower() + "|" + objForm.objData.KeyFieldValue.ToString();

                if (this.cachedObjectExists(sKey + "|Comments"))
                    this.removeCachedObject(sKey + "|Comments");
                if (this.cachedObjectExists(sKey + "|HTMLComments"))
                    this.removeCachedObject(sKey + "|HTMLComments");
            }

            if (sSerializationConfig != "")
            {
                XmlDocument dom = new XmlDocument();
                //For R5 test
                sSerializationConfig = sSerializationConfig.Replace("&lt;", "<").Replace("&gt;", ">");
                dom.LoadXml(sSerializationConfig);
                sValue = objForm.objData.SerializeObject(dom);
            }
            else
                sValue = objForm.objData.SerializeObject();

            return sValue;
        }
        string FetchViewWithRawParamsForRecordSummary()
        {
            //Extract Parameters from Message
            string sSysSid = SafeFormVariableParamText("SysSid");
            int prevSid = Conversion.ConvertStrToInteger(sSysSid);

            string sSysViewId = SafeParamText("SysViewId");
            int ViewId = Conversion.ConvertStrToInteger(sSysViewId);

            string sSysRecordId = SafeFormVariableParamText("SysFormId");
            int RecordId = Conversion.ConvertStrToInteger(sSysRecordId);

            string sFormName = FetchFormNameWithRawParams();
            // BSB 02.13.2007 Determine if Extensibility Form Mapping is in use, 
            // If in use AND no power-view defined then swap it in.
            // Note that PV case is handled inside of PV Editor which will create 
            // views based on the appropriate extended definition.
            if (_mappings.ContainsKey(sFormName) && ViewId == Constants.Views.BASE_VIEWID)
                ViewId = Constants.Views.EXTENDED_SCREEN_VIEWID;

            //Execute Logic
            return ViewManager.FetchViewForRecordSummary(prevSid, ViewId, RecordId, sFormName);

        }
        //Added by Abhishek
        void UpdateZapatecGridDataAdmin(AdminTrackingForm objForm)
        {
            string sMinRows = "";
            string sMaxRows = "";
            ArrayList singleRow = null;
            //Supplementals objSupps = null;
            string sGridXML =string.Empty;
            string sId = string.Empty;

            try
            {
                if (objForm != null)
                {


                    XmlDocument oXMLDoc = (((Riskmaster.DataModel.ADMTable)(objForm.m_objData))).ViewXml; ;
                    XmlNodeList oNodeList = oXMLDoc.SelectNodes("//control[@type='ZapatecGrid']");
                    int iCounter = 0;
                    foreach (XmlNode oNode in oNodeList)
                    {
                        //rsolanki2: moving the declarations out of the loop
                        sGridXML = oNode.Attributes["gridXML"].Value;
                        sId = oNode.Attributes["name"].Value;

                        singleRow = new ArrayList();
                        objForm.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Hidden.ToString());
                        objForm.AddElementToList(ref singleRow, "id", sId + "_zapatecgridxml");
                        objForm.AddElementToList(ref singleRow, "NewControl", "true");
                        objForm.AddElementToList(ref singleRow, "Text", sGridXML);
                        objForm.m_ModifiedControls.Add(singleRow);

                        if (iCounter == 0)
                        {
                            sMinRows = oNode.Attributes["minRows"].Value;
                            sMaxRows = oNode.Attributes["maxRows"].Value;
                            iCounter = iCounter + 1;

                            singleRow = new ArrayList();
                            objForm.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Hidden.ToString());
                            objForm.AddElementToList(ref singleRow, "id", "zapatecminrows");
                            objForm.AddElementToList(ref singleRow, "NewControl", "true");
                            objForm.AddElementToList(ref singleRow, "Text", sMinRows);
                            objForm.m_ModifiedControls.Add(singleRow);

                            singleRow = new ArrayList();
                            objForm.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Hidden.ToString());
                            objForm.AddElementToList(ref singleRow, "id", "zapatecmaxrows");
                            objForm.AddElementToList(ref singleRow, "NewControl", "true");
                            objForm.AddElementToList(ref singleRow, "Text", sMaxRows);
                            objForm.m_ModifiedControls.Add(singleRow);

                        }
                    }


                }

            }
            catch (Exception e)
            {
            }


        }


        void UpdateZapatecGridData(DataEntryFormBase objForm)
        {
            string sMinRows = "";
            string sMaxRows = "";
            ArrayList singleRow = null;
            Supplementals objSupps = null;

            try
            {
                if (objForm != null)
                {
                    if(objForm.objData != null)
                    {
                        if (objForm.objData.GetType().GetProperty("Supplementals") != null)
                        {
                    
                            objSupps = (objForm.objData.GetProperty("Supplementals") as Supplementals);
                            XmlDocument oXMLDoc = objSupps.ViewXml;
                            XmlNodeList oNodeList = oXMLDoc.SelectNodes("//control[@type='ZapatecGrid']");
                            int iCounter = 0;
                            foreach (XmlNode oNode in oNodeList)
                            {
                                string sGridXML = oNode.Attributes["gridXML"].Value;
                                string sId = oNode.Attributes["name"].Value;

                                singleRow = new ArrayList();
                                objForm.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Hidden.ToString());
                                objForm.AddElementToList(ref singleRow, "id", sId + "_zapatecgridxml");
                                objForm.AddElementToList(ref singleRow, "NewControl", "true");
                                objForm.AddElementToList(ref singleRow, "Text", sGridXML);
                                objForm.m_ModifiedControls.Add(singleRow);

                                if (iCounter == 0)
                                {
                                    sMinRows = oNode.Attributes["minRows"].Value;
                                    sMaxRows = oNode.Attributes["maxRows"].Value;
                                    iCounter = iCounter + 1;

                                    singleRow = new ArrayList();
                                    objForm.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Hidden.ToString());
                                    objForm.AddElementToList(ref singleRow, "id", "zapatecminrows");
                                    objForm.AddElementToList(ref singleRow, "NewControl", "true");
                                    objForm.AddElementToList(ref singleRow, "Text", sMinRows);
                                    objForm.m_ModifiedControls.Add(singleRow);

                                    singleRow = new ArrayList();
                                    objForm.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Hidden.ToString());
                                    objForm.AddElementToList(ref singleRow, "id", "zapatecmaxrows");
                                    objForm.AddElementToList(ref singleRow, "NewControl", "true");
                                    objForm.AddElementToList(ref singleRow, "Text", sMaxRows);
                                    objForm.m_ModifiedControls.Add(singleRow);

                               }
                            }
                        }
                    }
                }

            }
            catch (Exception e)
            {
            }

            
        }

    }
}
