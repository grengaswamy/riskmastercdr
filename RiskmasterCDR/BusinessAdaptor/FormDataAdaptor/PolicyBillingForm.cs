﻿
using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using Riskmaster.Application.EnhancePolicy.Billing ;
using System.Globalization;
using System.Threading;


namespace Riskmaster.BusinessAdaptor
{
    internal struct ControlAttribute
    {
        private string m_ControlName;
        private string m_AttributeName;
        private string m_AttributeValue;
        public string ControlName { get { return m_ControlName; } set { m_ControlName = value; } }
        public string AttributeName { get { return m_AttributeName; } set { m_AttributeName = value; } }
        public string AttributeValue { get { return m_AttributeValue; } set { m_AttributeValue = value; } }
    }
	public class PolicyBillingForm : DataEntryFormBase
	{

        /**************************************************************
         * $File		: PolicyBillingForm.cs
         * $Revision	: 1.0.0.0
         * $Date		: 
         * $Author	    : Nitesh Deedwania
         * $Comment	    :  
         * $Source	    :  	
        **************************************************************/
        
        #region Constant Members

        const string CLASS_NAME = "PolicyEnh";
        const string INSTANCE_SYSEXDATA_PATH = "/Instance/UI/FormVariables/SysExData";
        const string ELEMENT_TYPE_CODE = "Code";

		private const string POLICY_ID = "PolicyId" ;
		private const string POLICY_NAME = "PolicyName" ;
		private const string POLICY_STATUS = "PolicyStatus" ;
		private const string POLICY_NUMBER = "PolicyNumber" ;
		private const string POLICY_TERM_NUMBER = "PolicyTermNumber" ;
        private const string SELTED_BIL_ITEM_STATUS = "SelBillingItemStatus";
        private const string DATE_RANGE_FROM = "DateRangeFrom";
        private const string DATE_RANGE_TO ="DateRangeTo";

        private const string SELECTED_BILL_ITEMS = "SelectedBillItems";
        private const string ROOT_NODE_NAME = "Billing";
        private const string ACCOUNT_HISTORY = "AccountHistory";
        private const string BILL_ITEMS_GROUP_NODE_NAME = "BillItems";
        private const string BILL_ITEM_NODE_NAME = "BillItem";
        private const string BILL_ITEM_DATE_ENTERED = "DateEntered";
        private const string BILL_ITEM_TYPE = "BillItemType";
        //npadhy RMSC retrofit Starts
        private const string BILL_RCPT_TYPE = "RcptType";
        //npadhy RMSC retrofit Starts
        private const string BILL_ITEM_STATUS = "BillItemStatus";
        private const string BILL_ITEM_AMOUNT = "Amount";
        private const string BILL_ITEM_DUE_DATE = "DueDate";
        private const string BILL_ITEM_DATE_RECONCILED = "DateReconciled";
        private const string BILL_ITEM_FINAL_BATCH_NUM = "FinalBatchNum";
        private const string BILL_ITEM_FINAL_BATCH_TYPE = "FinalBatchType";
        private const string BILL_ITEM_ENTRY_BATCH_NUM = "EntryBatchNum";
        private const string BILL_ITEM_ENTRY_BATCH_TYPE = "EntryBatchType";
        private const string BILL_ITEM_INVOICE_ID = "InvoiceId";
        private const string BILL_ITEM_ROW_ID = "BillItemRowId";
        private const string BILL_ITEM_TYPE_STATUS = "BillItemTypeStatus";
        private const string CALCULATE_BALANCE = "CalculateBalance";
        private const string POLICY_BALANCE = "PolicyBalance";
        private const string TOTAL_BILLED_PREMIUM = "TotalBilledPremium";
        private const string PAY_PLAN = "PayPlan";
        private const string PAY_PLAN_NODE_NAME = "PayPlanInfo";
        private const string INSTALL_ITEMS_GROUP_NODE_NAME = "InstallItems";
        private const string INSTALL_ITEM_NODE_NAME = "InstallItem";
        private const string INSTALL_ITEM_GENERATED_FLAG = "GeneratedFlag";
        private const string INSTALL_ITEM_ROW_ID = "InstallItemRowId";
        private const string INSTALL_ITEM_NUMBER = "InstallNumber";
        private const string INSTALL_ITEM_AMOUNT = "Amount";
        private const string INSTALL_ITEM_INSTALL_DATE = "InstallDate";
        private const string INSTALL_ITEM_INSTALL_DUE_DATE = "InstallDueDate";
         //npadhy RMSC retrofit Starts
        private const string INSTALL_ITEM_TAX_AMOUNT = "TaxAmount";
        private const string INSTALL_ITEM_TOTAL_AMOUNT = "TotalAmount";
        private const string TOTAL_INSTLMT_AMOUNT = "TotalInstlmtAmount";
        private const string TOTAL_TAX_AMOUNT = "TotalTaxAmount";
        //npadhy RMSC retrofit Ends
        private const string POLICT_TERM_NO_LIST = "PolicyTermNoList";
        private const string ALL_TERMS = "ALL";

        const string POST_BACK_ACTION = "PostBackAction";
        const string ACTION_SELECT_FOR_RECONCILIATION = "SELECT_FOR_RECONCILIATION";
        const string ACTION_UNSELECT_FOR_RECONCILIATION = "UNSELECT_FOR_RECONCILIATION";
        const string ACTION_RECONCILIATION = "RECONCILIATION";
        const string ACTION_DELETE = "DELETE";

        private const int RMB_POLBILL_ENH_RECONCILIATION = 1200500192;
        private const int RMB_POLBILL_ENH_RECONCILIATION_SELECT = 1200500193;
        private const int RMB_POLBILL_ENH_RECONCILIATION_UNSELECT = 1200500194;
        private const int RMB_POLBILL_ENH_RECONCILIATION_RECONCILE = 1200500195;
        private const int RMB_POLBILL_ENH_PRINT_DISBURSEMENTS = 1200500196;

        private const int RMB_POLBILL_ENH_RECEIPTS = 1200500189;
        private const int RMB_POLBILL_ENH_ADJUSTMENTS = 1200500190;
        private const int RMB_POLBILL_ENH_DISBURSEMENTS = 1200500191;
        

        #endregion
        #region Variable Members
        private string m_sConnectionString;
        private string m_PolicyLimitFilter;
		private PolicyEnh PolicyEnh{get{return objData as PolicyEnh;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
        private XmlDocument m_objDocument = null;
        #endregion 
		/// <summary>
		/// Class constructor to initialize Class Name
		/// </summary>
		/// <param name="fda"></param>
		public PolicyBillingForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
            m_sConnectionString = fda.connectionString;
		}

        public override void Init()
        {
            base.Init();
            m_PolicyLimitFilter = " POLICY_INDICATOR = " + Convert.ToString(this.objCache.GetCodeId("P", "POLICY_INDICATOR"));
            if (m_PolicyLimitFilter != "")
                (PolicyEnh as DataModel.INavigation).Filter += m_PolicyLimitFilter;
        }
        public override void OnUpdateObject()
        {
            base.OnUpdateObject();
            string sPostBackAction = string.Empty;
            string sSelectedBillingItemRowID = string.Empty;
            string[] arrlstSelectBillingItem = { "" };
            int iReconcileType = 0;
            //Changed for MITS 9775 by Gagan : Start 
            bool bCancelPayPlan = false;
            //Changed for MITS 9775 by Gagan : End
            sPostBackAction = base.GetSysExDataNodeText( POST_BACK_ACTION );
            sSelectedBillingItemRowID = base.GetSysExDataNodeText(SELECTED_BILL_ITEMS);
            arrlstSelectBillingItem = sSelectedBillingItemRowID.Split(' ');

            switch (sPostBackAction)
            {
                case ACTION_SELECT_FOR_RECONCILIATION:
                    SelectForReconcilation(arrlstSelectBillingItem);
                    break;
                case ACTION_UNSELECT_FOR_RECONCILIATION:
                    UnSelectForReconcilation(arrlstSelectBillingItem);
                    break;
                case ACTION_RECONCILIATION:
                    iReconcileType= Conversion.ConvertObjToInt( base.GetSysExDataNodeText("ReconcileType"), base.ClientId);
                    //Changed for MITS 9775 by Gagan : Start 
                    bCancelPayPlan = Conversion.ConvertObjToBool(base.GetSysExDataNodeText("CancelPayPlan"), base.ClientId);                    
                    OnReconcilation(arrlstSelectBillingItem, iReconcileType, bCancelPayPlan);
                    //Changed for MITS 9775 by Gagan : End
                    break;
                case ACTION_DELETE:
                    DeleteBillingItem(arrlstSelectBillingItem);
                    break;
            
            }
        }
        public override void OnUpdateForm()
		{
			//Call the base implementation of OnUpdateForm from the FormBase class
			base.OnUpdateForm();

			int iPolicyId=PolicyEnh.PolicyId;
            int iPolicyStatus = 0;
            int iTermNumber = 0;
            int iPayeeEid = 0;

			string sPolicyName=string.Empty;
            string sSelBillingItemStatusCode = string.Empty;
            string sDateFrom = string.Empty;
            string sDateTo = string.Empty;
            string sTermNumber = string.Empty;
            string sPolicyNumber = string.Empty;

            XmlDocument objSysEx = base.SysEx;
            XmlNode objPolicyID; 
            XmlNode objOld;
            XmlNode objNew;
            ArrayList arrlstControlAttribute = new ArrayList();

            //Fetch Policy Details based of PolicyId
            this.GetPolicyDetails(iPolicyId, ref sPolicyName, ref iTermNumber, ref iPolicyStatus, ref sPolicyNumber, ref iPayeeEid);
            AppendRuntimeData(POLICY_NUMBER, sPolicyNumber);


            objPolicyID = objSysEx.SelectSingleNode("//" + POLICY_ID);
//Changes made for R5 : Term number reference was incorrect
            objOld = objSysEx.SelectSingleNode("/SysExData/" + POLICY_TERM_NUMBER);
//            objOld = objSysEx.SelectSingleNode("//" + POLICY_TERM_NUMBER);

            //Set Or Reset Term Number Combo on Navigating
            if (objPolicyID != null && objOld != null)
            {
                if (Conversion.ConvertStrToInteger(objPolicyID.InnerXml) != iPolicyId)
                {
                    sTermNumber = iTermNumber.ToString();
                }
                else
                {
                    sTermNumber = objOld.InnerXml;
                }
            }
            else
            {
                if(iTermNumber==0)
                    sTermNumber = "";
                else
                    sTermNumber = iTermNumber.ToString();
            }

            AppendRuntimeData(POLICY_TERM_NUMBER, sTermNumber);

            objOld = objSysEx.SelectSingleNode("//" + SELTED_BIL_ITEM_STATUS);
            if (objOld != null)
                sSelBillingItemStatusCode = objOld.InnerXml;
            else
                sSelBillingItemStatusCode = Convert.ToString(objCache.GetCodeId("U", "FILTER_TYPES"));
            objOld = null;

            AppendRuntimeData(SELTED_BIL_ITEM_STATUS, sSelBillingItemStatusCode);
            AppendBillingItemStatusList();


            objOld = objSysEx.SelectSingleNode("//" + DATE_RANGE_FROM);
            if (objOld != null)
                sDateFrom = objOld.InnerXml;
            objOld = null;
            AppendRuntimeData(DATE_RANGE_FROM, sDateFrom);

            objOld = objSysEx.SelectSingleNode("//" + DATE_RANGE_TO);
            if (objOld != null)
                sDateTo = objOld.InnerXml;
            objOld = null;
            AppendRuntimeData(DATE_RANGE_TO, sDateTo);


            objOld = objSysEx.SelectSingleNode("//" + ROOT_NODE_NAME);
            objNew = objSysEx.CreateElement(ROOT_NODE_NAME);

            XmlDocument objPolicyBillingXmlDoc = GetPolicyBillingXml(iPolicyId, objCache.GetShortCode(Conversion.ConvertStrToInteger(sSelBillingItemStatusCode)), sTermNumber, sDateFrom, sDateTo);
            XmlNode objPolicyBillingXmlNode = objPolicyBillingXmlDoc.SelectSingleNode("//" + ROOT_NODE_NAME);

            objNew.InnerXml = objPolicyBillingXmlNode.InnerXml;

            if (objOld != null)
                objSysEx.DocumentElement.ReplaceChild(objNew, objOld);
            else
                objSysEx.DocumentElement.AppendChild(objNew);

            objOld = null;
            objNew = null;

            EnableDisableButtons(ref arrlstControlAttribute);
            AddAttributeToControls(arrlstControlAttribute);

            //Changed for MITS 9775 by Gagan : Start
            base.ResetSysExData("CancelPayPlan", "");
            //Changed for MITS 9775 by Gagan : End
            base.ResetSysExData("ReconcileType", "");
            base.ResetSysExData(POST_BACK_ACTION, "");

           

            
        }

        # region Delete Billing Item
        private void DeleteBillingItem(string[] p_arrlstSelectBillingItem)
        {

            BillingManager objBillingManager = null;
            try
            {
                objBillingManager = new BillingManager(m_fda.Factory,base.ClientId);
                objBillingManager.DeleteBillingItem(p_arrlstSelectBillingItem);
            }
            catch (Exception p_objEx)
            {
                Errors.Add(p_objEx, Common.BusinessAdaptorErrorType.Error);
            }
            finally
            {
                if (objBillingManager != null)
                {
                    objBillingManager.Dispose();
                    objBillingManager = null;
                }
            }
        }
        #endregion

        #region Select Reconcilation
        private void SelectForReconcilation(string[] p_arrlstBillingItemIDs)
        {
            BillXBillItem objBillXBillItem = null;
            ArrayList arrlstBillItems = null;
            int iBillItemRowId = 0;

            try
            {
                arrlstBillItems = new ArrayList();
                foreach (string sBillingRowID in p_arrlstBillingItemIDs)
                {
                    iBillItemRowId = Conversion.ConvertObjToInt(sBillingRowID, base.ClientId);
                    if (iBillItemRowId == 0)
                        continue;
                    objBillXBillItem = (BillXBillItem)m_fda.Factory.GetDataModelObject("BillXBillItem", false);
                    objBillXBillItem.MoveTo(iBillItemRowId);
                    arrlstBillItems.Add(objBillXBillItem);
                }

                this.SelectForReconcilationSave(arrlstBillItems);
            }
            catch (Exception p_objEx)
            {
                Errors.Add(p_objEx, Common.BusinessAdaptorErrorType.Error);
            }
            finally
            {
                if (objBillXBillItem != null)
                {
                    objBillXBillItem.Dispose();
                    objBillXBillItem = null;
                }
                arrlstBillItems = null;
            }
        }
        private void SelectForReconcilationSave(ArrayList p_arrlstBillItems)
        {
            BillingMaster objBillingMaster = null;
            BillingManager objBillingManager = null;
            BillXBillItem objBillXBillItem = null;
            BillXBatch objBillXBatch = null;
            int iPolicyId = 0;

            string sSQL = string.Empty;
            int iIndex = 0;

            try
            {
                if (p_arrlstBillItems != null)
                {
                    objBillingMaster = new BillingMaster(m_fda.Factory,base.ClientId);
                    objBillingManager = new BillingManager(m_fda.Factory,base.ClientId);

                    for (iIndex = 0; iIndex < p_arrlstBillItems.Count; iIndex++)
                    {
                        objBillXBillItem = (BillXBillItem)p_arrlstBillItems[iIndex];

                        // If Batch Has not started then create New Batch.
                        if (objBillXBatch == null)
                        {
                            objBillXBatch = (BillXBatch)m_fda.Factory.GetDataModelObject("BillXBatch", false);

                            sSQL = " SELECT * FROM BILL_X_BATCH WHERE POLICY_ID=" + objBillXBillItem.PolicyId + " AND STATUS="
                                + objCache.GetCodeId("O", "BATCH_STATUS") + " AND BATCH_TYPE=" + objCache.GetCodeId("R", "BATCH_TYPES");

                            if (!objBillingManager.FillBatch(objBillXBatch, sSQL))
                            {
                                objBillXBatch.BatchType = objCache.GetCodeId("R", "BATCH_TYPES");
                                objBillXBatch.PolicyId = objBillXBillItem.PolicyId;
                                objBillXBatch.DateOpened = Conversion.GetDate(DateTime.Now.ToString("d"));
                                objBillXBatch.Status = objCache.GetCodeId("O", "BATCH_STATUS");
                                objBillXBatch.BatchRowid = Utilities.GetNextUID(m_sConnectionString, "BILL_X_BATCH", base.ClientId);
                            }
                        }
                        objBillXBatch.BatchCount++;
                        objBillXBatch.BatchAmount += objBillXBillItem.Amount;
                        objBillXBillItem.FinalBatchNum = objBillXBatch.BatchRowid;
                        objBillXBillItem.FinalBatchType = objBillXBatch.BatchType;
                        objBillXBillItem.Status = objCache.GetCodeId("SR", "BILLING_ITEM_STAT");

                        objBillingMaster.BillingItems.Add(objBillXBillItem.BillingItemRowid, objBillXBillItem);
                    }
                    objBillingMaster.Batches.Add(objBillXBatch.BatchRowid, objBillXBatch);
                    iPolicyId = objBillXBatch.PolicyId;

                    // Save the Billing Master.
                    objBillingMaster.Save();

                }
            }
            catch (Exception p_objEx)
            {
                Errors.Add(p_objEx, Common.BusinessAdaptorErrorType.Error);
            }
            finally
            {

                if (objBillingMaster != null)
                {
                    objBillingMaster.Dispose();
                    objBillingMaster = null;
                }
                if (objBillingManager != null)
                {
                    objBillingManager.Dispose();
                    objBillingManager = null;
                }
                if (objBillXBillItem != null)
                {
                    objBillXBillItem.Dispose();
                    objBillXBillItem = null;
                }
                if (objBillXBatch != null)
                {
                    objBillXBatch.Dispose();
                    objBillXBatch = null;
                }
            }
        }
        #endregion

        #region Unselect Reconcilation
        private void UnSelectForReconcilation(string[] p_arrlstBillingItemIDs)
        {
            BillXBillItem objBillXBillItem = null;
            ArrayList arrlstBillItems = null;

            int iBillItemRowId = 0;
            try
            {
                arrlstBillItems = new ArrayList();

                foreach (string sBillingRowID in p_arrlstBillingItemIDs)
                {
                    iBillItemRowId = Conversion.ConvertObjToInt(sBillingRowID, base.ClientId);
                    if (iBillItemRowId == 0)
                        continue;
                    objBillXBillItem = (BillXBillItem)m_fda.Factory.GetDataModelObject("BillXBillItem", false);
                    objBillXBillItem.MoveTo(iBillItemRowId);
                    arrlstBillItems.Add(objBillXBillItem);
                }

                this.UnSelectForReconcilationSave(arrlstBillItems);
            }
            catch (Exception p_objEx)
            {
                Errors.Add(p_objEx, Common.BusinessAdaptorErrorType.Error);
            }
            finally
            {
                if (objBillXBillItem != null)
                {
                    objBillXBillItem.Dispose();
                    objBillXBillItem = null;
                }
                arrlstBillItems = null;
            }
        }
        private void UnSelectForReconcilationSave(ArrayList p_arrlstBillItems)
        {
            BillingMaster objBillingMaster = null;
            BillingManager objBillingManager = null;
            BillXBillItem objBillXBillItem = null;
            BillXBatch objBillXBatch = null;
            int iPolicyId = 0;

            string sSQL = string.Empty;
            int iIndex = 0;

            try
            {

                if (p_arrlstBillItems != null)
                {
                    objBillingMaster = new BillingMaster(m_fda.Factory,base.ClientId);
                    objBillingManager = new BillingManager(m_fda.Factory,base.ClientId);

                    for (iIndex = 0; iIndex < p_arrlstBillItems.Count; iIndex++)
                    {
                        objBillXBillItem = (BillXBillItem)p_arrlstBillItems[iIndex];

                        // Select the Batch.
                        if (objBillXBatch == null)
                        {
                            objBillXBatch = (BillXBatch)m_fda.Factory.GetDataModelObject("BillXBatch", false);

                            sSQL = "SELECT * FROM BILL_X_BATCH WHERE BATCH_ROWID=" + objBillXBillItem.FinalBatchNum;
                            objBillingManager.FillBatch(objBillXBatch, sSQL);
                        }
                        objBillXBatch.BatchCount--;
                        objBillXBatch.BatchAmount -= objBillXBillItem.Amount;
                        objBillXBillItem.FinalBatchNum = 0;
                        objBillXBillItem.FinalBatchType = 0;
                        objBillXBillItem.Status = objCache.GetCodeId("OK", "BILLING_ITEM_STAT");

                        objBillingMaster.BillingItems.Add(objBillXBillItem.BillingItemRowid, objBillXBillItem);
                    }
                    objBillingMaster.Batches.Add(objBillXBatch.BatchRowid, objBillXBatch);
                    if (objBillXBatch.BatchCount == 0)
                        objBillingMaster.DeleteBatch(objBillXBatch.BatchRowid);

                    iPolicyId = objBillXBatch.PolicyId;

                    // Save the Billing Master.
                    objBillingMaster.Save();

                    // Get the Updated XML for the Billing Screen.
                    // this.GetPolicyBillingXml(iPolicyId, p_sStatus, p_sTermNumber, p_sFromDate, p_sToDate);
                }
            }
            catch (Exception p_objEx)
            {
                Errors.Add(p_objEx, Common.BusinessAdaptorErrorType.Error);
            }
            finally
            {
                if (objBillingMaster != null)
                {
                    objBillingMaster.Dispose();
                    objBillingMaster = null;
                }
                if (objBillingManager != null)
                {
                    objBillingManager.Dispose();
                    objBillingManager = null;
                }
                if (objBillXBillItem != null)
                {
                    objBillXBillItem.Dispose();
                    objBillXBillItem = null;
                }
                if (objBillXBatch != null)
                {
                    objBillXBatch.Dispose();
                    objBillXBatch = null;
                }
            }
        }
        #endregion

        #region Do Reconcilation
        public void OnReconcilation(string[] p_arrlstBillingItemIDs, int iReconcileType, bool bCancelPayPlan)
        {
            BillXBillItem objBillXBillItem = null;
            ArrayList arrlstBillItems = null;
            ArrayList arrlstBillItemsRowId = null;
            string sSQL = string.Empty;
            BillingManager objBillingManager = null;

            int iBillItemRowId = 0;
            int iBatchNumber = 0;
            int iPolicyId = 0;
            try
            {
                objBillingManager = new BillingManager(m_fda.Factory,base.ClientId);
                arrlstBillItems = new ArrayList();
                //arrlstBillItems = m_arrBillingItems;
                foreach (string sBillingRowID in p_arrlstBillingItemIDs)
                {
                    iBillItemRowId = Conversion.ConvertObjToInt(sBillingRowID, base.ClientId);
                    objBillXBillItem = (BillXBillItem)m_fda.Factory.GetDataModelObject("BillXBillItem", false);
                    objBillXBillItem.MoveTo(iBillItemRowId);
                    iBatchNumber = objBillXBillItem.FinalBatchNum;
                    iPolicyId = objBillXBillItem.PolicyId;
                }

                sSQL = "Select * from BILL_X_BILL_ITEM where POLICY_ID = " + iPolicyId;
                objBillingManager.FillBillingItemLoop(arrlstBillItems, sSQL, true);
                //Changed for MITS 9775 by Gagan : Start
                this.OnReconcilationSave(arrlstBillItems, iReconcileType, iBatchNumber, bCancelPayPlan);
                //Changed for MITS 9775 by Gagan : End
            }
            catch (Exception p_objEx)
            {
                Errors.Add(p_objEx, Common.BusinessAdaptorErrorType.Error);
            }
            finally
            {
                if (objBillXBillItem != null)
                {
                    objBillXBillItem.Dispose();
                    objBillXBillItem = null;
                }
                arrlstBillItemsRowId = null;
                arrlstBillItems = null;
                if (objBillingManager != null)
                {
                    objBillingManager.Dispose();
                    objBillingManager = null;
                }
            }
            //            return (m_objDocument);
        }
        private void OnReconcilationSave(ArrayList p_arrlstBillItems, int p_iReconcileType, int iBatchNumber, bool bCancelPayPlan)
        {
            BillingManager objBillingManager = null;
            BillingMaster objBillingMaster = null;
            BillXBillItem objBillXBillItem = null;
            BillXBatch objBillXBatch = null;
            DiaryManager objDiaryManager = null;
            ArrayList arrlstAccountItems = null;
            ArrayList arrlstInstallItems = null;

            //int iBatchNumber = 0;
            int iPolicyId = 0;
            int iTermNumber = 0;
            string sSQL = string.Empty;
            try
            {
                objBillingManager = new BillingManager(m_fda.Factory,base.ClientId);
                objBillingMaster = new BillingMaster(m_fda.Factory, base.ClientId);
                objDiaryManager = new DiaryManager(m_fda.Factory, base.ClientId);

                arrlstAccountItems = new ArrayList();
                arrlstInstallItems = new ArrayList();

                // Get the Batch Number.
                objBillXBillItem = (BillXBillItem)p_arrlstBillItems[0];
                //iBatchNumber = objBillXBillItem.FinalBatchNum;
                iPolicyId = objBillXBillItem.PolicyId;
                iTermNumber = objBillXBillItem.TermNumber;

                // Get the Batch.
                sSQL = " SELECT * FROM BILL_X_BATCH WHERE BATCH_ROWID=" + iBatchNumber;
                objBillXBatch = (BillXBatch)m_fda.Factory.GetDataModelObject("BillXBatch", false);
                objBillXBatch.MoveTo(iBatchNumber);
                objBillingMaster.Batches.Add(objBillXBatch.BatchRowid, objBillXBatch);

                // Fill bill-x-account records.
                sSQL = "SELECT * from BILL_X_ACCOUNT WHERE BILL_X_ACCOUNT.POLICY_ID = " + iPolicyId;
                objBillingManager.FillBillAccount(arrlstAccountItems, sSQL);


                // Fill bill-x-instlmnt records
                sSQL = "SELECT * from BILL_X_INSTLMNT WHERE BILL_X_INSTLMNT.POLICY_ID = " + iPolicyId;
                objBillingManager.FillBillInstall(arrlstInstallItems, sSQL);

                //Changed for MITS 9775 by Gagan : Start
                //if (objBillingManager.CheckPaidInFull(p_arrlstBillItems, arrlstAccountItems, arrlstInstallItems, objBillingMaster, iBatchNumber))
                objBillingManager.CheckPaidInFull(p_arrlstBillItems, arrlstAccountItems, arrlstInstallItems, objBillingMaster, iBatchNumber);
                if(bCancelPayPlan)
                {
                //Changed for MITS 9775 by Gagan : End
                    objBillingManager.CancelPayPlanPaid(p_arrlstBillItems, arrlstInstallItems, objBillingMaster, iBatchNumber);

                    // Get Latest List of Billing Items.
                    p_arrlstBillItems.Clear();
                    sSQL = "Select * from BILL_X_BILL_ITEM where POLICY_ID = " + iPolicyId;
                    objBillingManager.FillBillingItemLoop(p_arrlstBillItems, sSQL, true);
                }

                objBillingManager.Reconcile(p_arrlstBillItems, p_iReconcileType, objBillingMaster);
                objBillingMaster.Save();

                // Get the Updated XML for the Billing Screen.
                //this.GetPolicyBillingXml(iPolicyId, p_sStatus, p_sTermNumber, p_sFromDate, p_sToDate);

                objDiaryManager.ScheduleCreditDiaries(iPolicyId, iTermNumber);
            }
            catch (Exception p_objEx)
            {
                Errors.Add(p_objEx, Common.BusinessAdaptorErrorType.Error);
            }
            finally
            {
                if (objBillXBillItem != null)
                {
                    objBillXBillItem.Dispose();
                    objBillXBillItem = null;
                }
                if (objBillingManager != null)
                {
                    objBillingManager.Dispose();
                    objBillingManager = null;
                }
                if (objBillingMaster != null)
                {
                    objBillingMaster.Dispose();
                    objBillingMaster = null;
                }
                if (objBillXBatch != null)
                {
                    objBillXBatch.Dispose();
                    objBillXBatch = null;
                }
                if (objDiaryManager != null)
                {
                    objDiaryManager.Dispose();
                    objDiaryManager = null;
                }
                arrlstAccountItems = null;
                arrlstInstallItems = null;
            }
        }
        #endregion 

        #region Add Attributes To Controls
        private void EnableDisableButtons(ref ArrayList p_arrlstControlAttribute)
        {
            XmlDocument objSysEx = base.SysEx;
            XmlNode objXmlNode;
            string sTermNumber=string.Empty;

            objXmlNode = objSysEx.SelectSingleNode("//" + POLICY_TERM_NUMBER);
            if (objXmlNode != null)
                sTermNumber = objXmlNode.InnerXml;

            if (PolicyEnh.PolicyId == 0)
            {
                p_arrlstControlAttribute.Add(GetControlAttributeObj("BillingItemsGrid","hidebuttons","New|Edit|Delete"));
                p_arrlstControlAttribute.Add(GetControlAttributeObj("btnAddReceipt", "disabled", "true"));
                p_arrlstControlAttribute.Add(GetControlAttributeObj("btnAddAdjustment", "disabled", "true"));
                p_arrlstControlAttribute.Add(GetControlAttributeObj("btnAddDisbursment", "disabled", "true"));
                p_arrlstControlAttribute.Add(GetControlAttributeObj("btnSelectForReconciliation", "disabled", "true"));
                p_arrlstControlAttribute.Add(GetControlAttributeObj("btnUnselectForReconciliation", "disabled", "true"));
                p_arrlstControlAttribute.Add(GetControlAttributeObj("btnReconciliation", "disabled", "true"));
                
            }
            else
                if(sTermNumber==""||sTermNumber=="ALL" )
                {
                    p_arrlstControlAttribute.Add(GetControlAttributeObj("BillingItemsGrid", "hidebuttons", "New"));
                    p_arrlstControlAttribute.Add(GetControlAttributeObj("btnAddReceipt", "disabled", "true"));
                    p_arrlstControlAttribute.Add(GetControlAttributeObj("btnAddAdjustment", "disabled", "true"));
                    p_arrlstControlAttribute.Add(GetControlAttributeObj("btnAddDisbursment", "disabled", "true"));
                    p_arrlstControlAttribute.Add(GetControlAttributeObj("btnSelectForReconciliation", "disabled", "true"));
                    p_arrlstControlAttribute.Add(GetControlAttributeObj("btnUnselectForReconciliation", "disabled", "true"));
                    p_arrlstControlAttribute.Add(GetControlAttributeObj("btnReconciliation", "disabled", "true"));

                }
            //Added by Gagan for Enhanced Policy Billing : Start
            else
            {                   
                    p_arrlstControlAttribute.Add(GetControlAttributeObj("BillingItemsGrid","hidebuttons","")); // Added by csingh7
                    p_arrlstControlAttribute.Add(GetControlAttributeObj("btnAddReceipt", "disabled", "false"));
                    p_arrlstControlAttribute.Add(GetControlAttributeObj("btnAddAdjustment", "disabled", "false"));
                    p_arrlstControlAttribute.Add(GetControlAttributeObj("btnAddDisbursment", "disabled", "false"));
                    p_arrlstControlAttribute.Add(GetControlAttributeObj("btnSelectForReconciliation", "disabled", "false"));
                    p_arrlstControlAttribute.Add(GetControlAttributeObj("btnUnselectForReconciliation", "disabled", "false"));
                    p_arrlstControlAttribute.Add(GetControlAttributeObj("btnReconciliation", "disabled", "false"));

                if (!this.objData.Context.RMUser.IsAllowedEx(RMB_POLBILL_ENH_RECEIPTS))
                    p_arrlstControlAttribute.Add(GetControlAttributeObj("btnAddReceipt", "disabled", "true"));
                if (!this.objData.Context.RMUser.IsAllowedEx(RMB_POLBILL_ENH_ADJUSTMENTS))
                    p_arrlstControlAttribute.Add(GetControlAttributeObj("btnAddAdjustment", "disabled", "true"));
                if (!this.objData.Context.RMUser.IsAllowedEx(RMB_POLBILL_ENH_DISBURSEMENTS))
                    p_arrlstControlAttribute.Add(GetControlAttributeObj("btnAddDisbursment", "disabled", "true"));
                if (!this.objData.Context.RMUser.IsAllowedEx(RMB_POLBILL_ENH_RECONCILIATION_SELECT))
                    p_arrlstControlAttribute.Add(GetControlAttributeObj("btnSelectForReconciliation", "disabled", "true"));
                if (!this.objData.Context.RMUser.IsAllowedEx(RMB_POLBILL_ENH_RECONCILIATION_UNSELECT))
                    p_arrlstControlAttribute.Add(GetControlAttributeObj("btnUnselectForReconciliation", "disabled", "true"));
                if (!this.objData.Context.RMUser.IsAllowedEx(RMB_POLBILL_ENH_RECONCILIATION_RECONCILE))
                    p_arrlstControlAttribute.Add(GetControlAttributeObj("btnReconciliation", "disabled", "true"));                
                if (!this.objData.Context.RMUser.IsAllowedEx(RMB_POLBILL_ENH_PRINT_DISBURSEMENTS))
                    p_arrlstControlAttribute.Add(GetControlAttributeObj("btnPrintDisbursment", "disabled", "true"));
            }
            //Added by Gagan for Enhanced Policy Billing : End
            
            p_arrlstControlAttribute.Add(GetControlAttributeObj("btnPrintDisbursment", "disabled", "true"));

        }
        private ControlAttribute GetControlAttributeObj(string p_ControlName, string p_AttributeName, string p_AttributeValue)
        {
            ControlAttribute objControlAttribute = new ControlAttribute();
            objControlAttribute.ControlName = p_ControlName;
            objControlAttribute.AttributeName = p_AttributeName;
            objControlAttribute.AttributeValue = p_AttributeValue;
            return objControlAttribute;
        }
        private void AddAttributeToControls(ArrayList p_arrlstControlAttribute)
        {
            XmlDocument objSysEx = base.SysEx;

            XmlNode objOld;
            XmlNode objNew;
            XmlElement objElem = null;
            XmlElement objChild = null;

            objOld = objSysEx.SelectSingleNode("//ControlAppendAttributeList");
            objNew = objSysEx.CreateElement("ControlAppendAttributeList");

            foreach (ControlAttribute objControlAttribute in p_arrlstControlAttribute)
            {
                objElem = objSysEx.CreateElement(objControlAttribute.ControlName);
                objChild = objSysEx.CreateElement(objControlAttribute.AttributeName);
                objChild.SetAttribute("value", objControlAttribute.AttributeValue);
                objElem.AppendChild(objChild);
                objNew.AppendChild(objElem);
            }
            if (objOld != null)
                objSysEx.DocumentElement.ReplaceChild(objNew, objOld);
            else
                objSysEx.DocumentElement.AppendChild(objNew);
        }
        #endregion

        #region Policy Details
        private void GetPolicyDetails( int p_iPolicyId, ref string p_sPolicyName , ref int p_iTermNumber , ref int p_iPolicyStatus , ref string p_sPolicyNumber , ref int p_iPayeeEid )
		{
		
			int iMaxTermNumber = 0 ;
			int iMaxSequenceNumber = 0 ;			

			try
			{
				p_sPolicyName = Function.GetPolicyName( p_iPolicyId , m_sConnectionString,base.ClientId );
				if( p_sPolicyName != "" )
				{
					iMaxTermNumber = Function.GetMaxTermNumber( p_iPolicyId , m_sConnectionString,base.ClientId );
                    iMaxSequenceNumber = Function.GetMaxSequenceNumber(p_iPolicyId, iMaxTermNumber, m_sConnectionString, base.ClientId);
                    p_iPolicyStatus = Function.GetPolicyStatus(p_iPolicyId, iMaxTermNumber, iMaxSequenceNumber, m_sConnectionString, base.ClientId);
                    p_sPolicyNumber = Function.GetPolicyNumber(p_iPolicyId, iMaxTermNumber, iMaxSequenceNumber, ref p_iTermNumber, m_sConnectionString, base.ClientId);					
				}
                
                p_iPayeeEid = Function.GetBillToPayeeEid(p_iPolicyId, iMaxTermNumber, m_sConnectionString, base.ClientId);

			}
            catch (Exception p_objEx)
            {
                Errors.Add(p_objEx, Common.BusinessAdaptorErrorType.Error);
            }

        }
        #endregion

        # region AppendBillingItemStatusList
        private void AppendBillingItemStatusList()
        {
            //Variable declarations
            string sSQL;
            int iCodeID;
            string sCodeDesc;
            DbReader objReader = null;

            XmlDocument objXML = base.SysEx;
            XmlNode objOld = null;
            XmlElement objNew = null;
            XmlCDataSection objCData = null;
            objOld = objXML.SelectSingleNode("//BillingItemStatusList");
            objNew = objXML.CreateElement("BillingItemStatusList");

            XmlElement xmlOption = null;
            XmlAttribute xmlOptionAttrib = null;

            sSQL = "SELECT C.CODE_ID,CT.CODE_DESC FROM CODES C,CODES_TEXT CT WHERE C.CODE_ID =CT.CODE_ID AND C.TABLE_ID = " + objCache.GetTableId("FILTER_TYPES");
            using (objReader = DbFactory.GetDbReader(PolicyEnh.Context.RMDatabase.ConnectionString, sSQL))
            {

                while (objReader.Read())
                {
                    iCodeID = Conversion.ConvertObjToInt(objReader.GetValue("CODE_ID"), base.ClientId);
                    sCodeDesc = Conversion.ConvertObjToStr(objReader.GetValue("CODE_DESC"));
                    xmlOption = objXML.CreateElement("option");
                    objCData = objXML.CreateCDataSection(sCodeDesc);
                    xmlOption.AppendChild(objCData);
                    xmlOptionAttrib = objXML.CreateAttribute("value");
                    xmlOptionAttrib.Value = Convert.ToString(iCodeID);
                    xmlOption.Attributes.Append(xmlOptionAttrib);
                    objNew.AppendChild(xmlOption);
                    xmlOptionAttrib = null;
                    xmlOption = null;
                }
            }
            if (objOld != null)
                objXML.DocumentElement.ReplaceChild(objNew, objOld);
            else
                objXML.DocumentElement.AppendChild(objNew);

            objOld = null;
            objNew = null;
            objCData = null;
            objXML = null;
        }
		#endregion

        # region Local XML functions
		private void AppendRuntimeData( string p_objElementName , string p_sValue )
		{
			this.AppendRuntimeData( p_objElementName , p_sValue , "" );
		}
		private void AppendRuntimeData( string p_objElementName , string p_sValue , string p_sElementType )
		{
			XmlDocument objSysEx = base.SysEx;
			XmlNode objOld = null;
			XmlElement objNew = null;

			//objOld = objSysEx.SelectSingleNode("//" + p_objElementName);
            objOld = objSysEx.SelectSingleNode("/" + p_objElementName);
			objNew = objSysEx.CreateElement( p_objElementName );

			if( p_sElementType == ELEMENT_TYPE_CODE )
			{
				int iCodeId = Conversion.ConvertStrToInteger( p_sValue );
				objNew.SetAttribute( "codeid" , p_sValue );
				if( iCodeId != 0 )
                    objNew.InnerText = objCache.GetShortCode(iCodeId) + " " + objCache.GetCodeDesc(iCodeId, base.Adaptor.userLogin.objUser.NlsCode); //tmalhotra3 ML Change
				else
					objNew.InnerText = "" ;
			}
			else
			{
				objNew.InnerText = p_sValue ;
			}

			if( objOld != null )
				objSysEx.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objSysEx.DocumentElement.AppendChild(objNew);

		}
		#endregion

        #region Get Policy Billing Xml
        public XmlDocument GetPolicyBillingXml(int p_iPolicyId, string p_sStatus, string p_sTermNumber, string p_sFromDate, string p_sToDate)
        {
            BillingManager objBillingManager = null;
            XmlElement objRootNode = null;
            ArrayList arrlstBillingItems = null;
            ArrayList arrlstAccountItems = null;
            ArrayList arrlstInstallItems = null;

            string sSQL = string.Empty;
            string sPolicyName = string.Empty;
            string sPolicyNumber = string.Empty;
            int iTermNumber = 0;
            int iPolicyStatus = 0;
            int iPayeeEid = 0;

            try
            {
                objBillingManager = new BillingManager( m_fda.Factory,base.ClientId);
                arrlstBillingItems = new ArrayList();
                arrlstAccountItems = new ArrayList();
                arrlstInstallItems = new ArrayList();

                Function.StartDocument(ref m_objDocument, ref objRootNode, ROOT_NODE_NAME, base.ClientId);
                this.GetPolicyDetails(p_iPolicyId, ref sPolicyName, ref iTermNumber, ref iPolicyStatus, ref sPolicyNumber, ref iPayeeEid, objRootNode);

                if (p_sTermNumber == "")
                    p_sTermNumber = iTermNumber.ToString();
                if (p_iPolicyId != 0)
                {
                    sSQL = "Select * from BILL_X_BILL_ITEM where POLICY_ID = " + p_iPolicyId;
                    objBillingManager.FillBillingItemLoop(arrlstBillingItems, sSQL, true);

                    sSQL = "SELECT * from BILL_X_ACCOUNT WHERE BILL_X_ACCOUNT.POLICY_ID = " + p_iPolicyId;
                    objBillingManager.FillBillAccount(arrlstAccountItems, sSQL);

                    sSQL = "SELECT * from BILL_X_INSTLMNT WHERE BILL_X_INSTLMNT.POLICY_ID = " + p_iPolicyId + " ORDER BY INSTALLMENT_NUM ASC";
                    objBillingManager.FillBillInstall(arrlstInstallItems, sSQL);
                }
                //Term Number XML
                this.GetTermNoXml(p_iPolicyId, iTermNumber,arrlstAccountItems, ref objRootNode);

                // Billing Items Xml
                this.GetBillingItemsXml(arrlstBillingItems, p_sStatus, p_sTermNumber, p_sFromDate, p_sToDate, objRootNode);
                

                // Pay Plan Xml.
                this.GetPayPlanXml(arrlstAccountItems, arrlstInstallItems, p_sTermNumber, objRootNode);

            }

            catch (Exception p_objEx)
            {
                Errors.Add(p_objEx, Common.BusinessAdaptorErrorType.Error);
            }
            finally
            {
                objRootNode = null;
                if (objBillingManager != null)
                {
                    objBillingManager.Dispose();
                    objBillingManager = null;
                }
                arrlstBillingItems = null;
                arrlstAccountItems = null;
                arrlstInstallItems = null;
            }
            return (m_objDocument);
        }
        private void GetTermNoXml(int p_iPolicyId,int p_iTermNumber,ArrayList p_arrlstAccountItems, ref XmlElement p_objParentNode)
        {

            XmlElement objTermNoListNode=null;
            XmlElement objOption=null;
            XmlAttribute xmlOptionAttrib;
            XmlCDataSection objCData;
            bool bCurrentTermnumber=false;

            Function.CreateElement(p_objParentNode, POLICT_TERM_NO_LIST, ref objTermNoListNode, base.ClientId);

            if (p_iPolicyId != 0)
            {
                Function.CreateElement(objTermNoListNode, "option", ref objOption, base.ClientId);
                objCData = p_objParentNode.OwnerDocument.CreateCDataSection(ALL_TERMS);
                objOption.AppendChild(objCData);
                xmlOptionAttrib = p_objParentNode.OwnerDocument.CreateAttribute("value");
                xmlOptionAttrib.Value = ALL_TERMS;
                objOption.Attributes.Append(xmlOptionAttrib);
                objTermNoListNode.AppendChild(objOption);
                xmlOptionAttrib = null;
                objOption = null;

                foreach (BillXAccount objBillXAccount in p_arrlstAccountItems)
                {
                    Function.CreateElement(objTermNoListNode, "option", ref objOption, base.ClientId);
                    if (p_iTermNumber ==objBillXAccount.TermNumber)
                        bCurrentTermnumber=true;
                    objCData = p_objParentNode.OwnerDocument.CreateCDataSection(objBillXAccount.TermNumber.ToString());
                    objOption.AppendChild(objCData);
                    xmlOptionAttrib = p_objParentNode.OwnerDocument.CreateAttribute("value");
                    xmlOptionAttrib.Value = objBillXAccount.TermNumber.ToString();
                    objOption.Attributes.Append(xmlOptionAttrib);
                    objTermNoListNode.AppendChild(objOption);
                    xmlOptionAttrib = null;
                    objOption = null;
                }
                if (!bCurrentTermnumber)
                {
                    Function.CreateElement(objTermNoListNode, "option", ref objOption, base.ClientId);
                    objCData = p_objParentNode.OwnerDocument.CreateCDataSection(p_iTermNumber.ToString());
                    objOption.AppendChild(objCData);
                    xmlOptionAttrib = p_objParentNode.OwnerDocument.CreateAttribute("value");
                    xmlOptionAttrib.Value = p_iTermNumber.ToString();
                    objOption.Attributes.Append(xmlOptionAttrib);
                    objTermNoListNode.AppendChild(objOption);
                    xmlOptionAttrib = null;
                    objOption = null;
                }
            }
            else
            {
                Function.CreateElement(objTermNoListNode, "option", ref objOption, base.ClientId);
                objCData = p_objParentNode.OwnerDocument.CreateCDataSection("");
                objOption.AppendChild(objCData);
                xmlOptionAttrib = p_objParentNode.OwnerDocument.CreateAttribute("value");
                xmlOptionAttrib.Value = "";
                objOption.Attributes.Append(xmlOptionAttrib);
                objTermNoListNode.AppendChild(objOption);
                xmlOptionAttrib = null;
                objOption = null;
            }
        }
        private void GetPayPlanXml(ArrayList p_arrlstAccountItems, ArrayList p_arrlstInstallItems, string p_sTermNumber, XmlElement p_objParentNode)
        {
            BillXAccount objBillXAccount = null;
            BillXInstlmnt objBillXInstlmnt = null;
            XmlElement objPayPlanNode = null;
            XmlElement objPayPlan= null;
            XmlElement objInstallItemsNode = null;
            DbReader objReader = null;
            XmlCDataSection objCData = null;
            int iTermNumber = 0;
            int iIndex = 0;
            int iPayPlanCode = 0;
            double dblTotalBilledPremium = 0.0;
            //npadhy RMSC retrofit Starts
            double dblTotalInstlmntAmt = 0.0;
            double dblTotalTaxAmt = 0.0; 
            //npadhy RMSC retrofit Ends
            string sSQL = string.Empty;
            int iOptionCount = 0;
            try
            {
                if (p_sTermNumber != "ALL")
                {
                    iTermNumber = Conversion.ConvertStrToInteger(p_sTermNumber);
                }

                Function.CreateElement(p_objParentNode, PAY_PLAN_NODE_NAME, ref objPayPlanNode, base.ClientId);

                // Total Billed Information
                if (p_sTermNumber != "ALL")
                {
                    for (iIndex = 0; iIndex < p_arrlstAccountItems.Count; iIndex++)
                    {
                        objBillXAccount = (BillXAccount)p_arrlstAccountItems[iIndex];
                        if (objBillXAccount.TermNumber == iTermNumber)
                        {
                            dblTotalBilledPremium = objBillXAccount.AmountDue;

                            sSQL = "SELECT PAY_PLAN_CODE FROM SYS_BILL_PAY_PLAN where PAY_PLAN_ROWID = " + objBillXAccount.PayPlanRowid;
                            objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                            if (objReader.Read())
                            {
                                iPayPlanCode = Conversion.ConvertObjToInt(objReader.GetValue("PAY_PLAN_CODE"), base.ClientId);
                            }
                            objReader.Close();
                        }
                    }
                }
                Function.CreateAndSetElement(objPayPlanNode, POLICY_TERM_NUMBER, p_sTermNumber, base.ClientId);
                //npadhy RMSC retrofit Starts

                //Function.CreateAndSetElement(objPayPlanNode, TOTAL_BILLED_PREMIUM, string.Format("{0:C}", dblTotalBilledPremium));
                //npadhy RMSC retrofit Ends
                Function.CreateNodeWithCodeValues(objPayPlanNode, PAY_PLAN, iPayPlanCode, m_sConnectionString, ref objPayPlan, base.ClientId);
                objCData = objPayPlan.OwnerDocument.CreateCDataSection(objCache.GetCodeDesc(iPayPlanCode, base.Adaptor.userLogin.objUser.NlsCode)); //tmalhotra3 ML Change
                objPayPlan.AppendChild(objCData);
                // Pay plan information
                if (objInstallItemsNode == null)
                    Function.CreateElement(objPayPlanNode, INSTALL_ITEMS_GROUP_NODE_NAME, ref objInstallItemsNode, base.ClientId);
                CreateInstallListHead(objInstallItemsNode);
                if (p_sTermNumber != "ALL")
                {
                    for (iIndex = 0; iIndex < p_arrlstInstallItems.Count; iIndex++)
                    {
                        objBillXInstlmnt = (BillXInstlmnt)p_arrlstInstallItems[iIndex];


                        if (objBillXInstlmnt.TermNumber == iTermNumber)
                        {
                            //npadhy RMSC retrofit Starts
                             //this.CreateInstallItemXml(objInstallItemsNode, objBillXInstlmnt, ++iOptionCount);
                            this.CreateInstallItemXml(objInstallItemsNode, objBillXInstlmnt, ++iOptionCount,ref dblTotalBilledPremium, ref dblTotalTaxAmt, ref dblTotalInstlmntAmt );
                            //npadhy RMSC retrofit Ends
                        }
                    }
                }
                //npadhy RMSC retrofit Starts
                //Function.CreateAndSetElement(objPayPlanNode, TOTAL_INSTLMT_AMOUNT, string.Format("{0:C}", dblTotalInstlmntAmt));
                //Function.CreateAndSetElement(objPayPlanNode, TOTAL_TAX_AMOUNT, string.Format("{0:C}", dblTotalTaxAmt));
                //Function.CreateAndSetElement(objPayPlanNode, TOTAL_BILLED_PREMIUM, string.Format("{0:C}", dblTotalBilledPremium));
                //Manish MultiCurrency
                Function.CreateAndSetElement(objPayPlanNode, TOTAL_INSTLMT_AMOUNT, Convert.ToString(dblTotalInstlmntAmt), base.ClientId);
                Function.CreateAndSetElement(objPayPlanNode, TOTAL_TAX_AMOUNT, Convert.ToString(dblTotalTaxAmt), base.ClientId);
                Function.CreateAndSetElement(objPayPlanNode, TOTAL_BILLED_PREMIUM, Convert.ToString(dblTotalBilledPremium), base.ClientId);
                //npadhy RMSC retrofit Ends
            }
            catch (Exception p_objEx)
            {
                Errors.Add(p_objEx, Common.BusinessAdaptorErrorType.Error);
            }
            finally
            {
                if (objBillXInstlmnt != null)
                {
                    objBillXInstlmnt.Dispose();
                    objBillXInstlmnt = null;
                }
                if (objBillXAccount != null)
                {
                    objBillXAccount.Dispose();
                    objBillXAccount = null;
                }
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                    objReader = null;
                }
                objInstallItemsNode = null;
                objPayPlanNode = null;
                objPayPlan = null;
                objCData = null;
            }
        }
        private void CreateInstallListHead(XmlElement p_objParentNode)
        {
            XmlElement objListHead = null;
            Function.CreateElement(p_objParentNode, "listhead", ref objListHead, base.ClientId);
            Function.CreateAndSetElement(objListHead, INSTALL_ITEM_GENERATED_FLAG, "Generated", base.ClientId);
            Function.CreateAndSetElement(objListHead, INSTALL_ITEM_NUMBER, "Install Number", base.ClientId);
            Function.CreateAndSetElement(objListHead, INSTALL_ITEM_AMOUNT, "Install Amount", base.ClientId);
            //npadhy RMSC retrofit Starts
            Function.CreateAndSetElement(objListHead, INSTALL_ITEM_TAX_AMOUNT, "Tax Amount", base.ClientId);
            Function.CreateAndSetElement(objListHead, INSTALL_ITEM_TOTAL_AMOUNT, "Total Installment Amount", base.ClientId);
            //npadhy RMSC retrofit Ends
            Function.CreateAndSetElement(objListHead, INSTALL_ITEM_INSTALL_DATE, "Install Date", base.ClientId);
            Function.CreateAndSetElement(objListHead, INSTALL_ITEM_INSTALL_DUE_DATE, "Due Date", base.ClientId);
            Function.CreateAndSetElement(objListHead, INSTALL_ITEM_ROW_ID, "Install Row Id", base.ClientId);

        }
		//npadhy RMSC retrofit Starts
        //private void CreateInstallItemXml(XmlElement p_objParentNode, BillXInstlmnt p_objBillXInstlmnt,int p_iOptionCount)
        private void CreateInstallItemXml(XmlElement p_objParentNode, BillXInstlmnt p_objBillXInstlmnt,int p_iOptionCount,ref double dblTotalBilledPremium,ref double dblTotalTaxAmount,ref double dblTotalInstallmentAmount)
        {
            XmlElement objInstallItemNode = null;
            try
            {
                //npadhy RMSC retrofit Starts
                dblTotalInstallmentAmount = dblTotalInstallmentAmount + p_objBillXInstlmnt.Amount;
                dblTotalTaxAmount += p_objBillXInstlmnt.TaxAmount;
                dblTotalBilledPremium = dblTotalInstallmentAmount + dblTotalTaxAmount;   
                //npadhy RMSC retrofit Ends
                Function.CreateElement(p_objParentNode, "option", ref objInstallItemNode,base.ClientId);
                objInstallItemNode.SetAttribute("ref", INSTANCE_SYSEXDATA_PATH + "/" + ROOT_NODE_NAME + "/" + PAY_PLAN_NODE_NAME + "/" + p_objParentNode.Name.ToString() + "/option[" + (p_iOptionCount).ToString() + "]");                
                Function.CreateAndSetElement(objInstallItemNode, INSTALL_ITEM_GENERATED_FLAG, p_objBillXInstlmnt.GeneratedFlag? "Yes" : "No",base.ClientId);
                Function.CreateAndSetElement(objInstallItemNode, INSTALL_ITEM_NUMBER, p_objBillXInstlmnt.InstallmentNum.ToString(),base.ClientId);
                Function.CreateAndSetElement(objInstallItemNode, INSTALL_ITEM_AMOUNT, string.Format("{0:C}", p_objBillXInstlmnt.Amount),base.ClientId);
                //npadhy RMSC retrofit Starts
                Function.CreateAndSetElement(objInstallItemNode, INSTALL_ITEM_TAX_AMOUNT, string.Format("{0:C}", p_objBillXInstlmnt.TaxAmount),base.ClientId);
                Function.CreateAndSetElement(objInstallItemNode, INSTALL_ITEM_TOTAL_AMOUNT, string.Format("{0:C}", (p_objBillXInstlmnt.Amount + p_objBillXInstlmnt.TaxAmount)),base.ClientId);
                //npadhy RMSC retrofit Ends
                Function.CreateAndSetElement(objInstallItemNode, INSTALL_ITEM_INSTALL_DATE, Conversion.GetDBDateFormat(p_objBillXInstlmnt.InstallmentDate, "d"),base.ClientId);
                Function.CreateAndSetElement(objInstallItemNode, INSTALL_ITEM_INSTALL_DUE_DATE, Conversion.GetDBDateFormat(p_objBillXInstlmnt.InstallDueDate, "d"),base.ClientId);
                Function.CreateAndSetElement(objInstallItemNode, INSTALL_ITEM_ROW_ID, p_objBillXInstlmnt.InstallmentRowid.ToString(),base.ClientId);
            }
            catch (Exception p_objEx)
            {
                Errors.Add(p_objEx, Common.BusinessAdaptorErrorType.Error);
            }
            finally
            {
                objInstallItemNode = null;
            }
        }
		//npadhy RMSC retrofit Ends
        private void GetBillingItemsXml(ArrayList p_arrlstBillingItems, string p_sStatus, string p_sTermNumber, string p_sFromDate, string p_sToDate, XmlElement p_objParentNode)
        {
            BillXBillItem objBillXBillItem = null;
            XmlElement objBillItemsNode = null;
            XmlElement objAccountHistory = null;

            XmlAttribute objXmlAttribute = null;           
            XmlElement objBillItemNode = null;
            XmlElement objBillItemTypeNode = null;

            string sSQL = string.Empty;
            string sStatus = string.Empty;
            int iMaxDate = 99991231;
            int iMinDate = 00000000;
            int iFromDate = 0;
            int iToDate = 0;
            int iTermNumber = 0;
            int iIndex = 0;
            bool bDisplayAllTerm = true;
            double dblCalculateBalance = 0.0;
            int optionCount = 0;

          

            //Manish for multi currency
            string culture = CommonFunctions.GetCulture(0, CommonFunctions.NavFormType.None, m_sConnectionString);
            Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);

            try
            {
                p_sFromDate = Conversion.GetDate(p_sFromDate);
                p_sToDate = Conversion.GetDate(p_sToDate);

                if (p_sFromDate == "")
                    iFromDate = iMinDate;
                else
                    iFromDate = Conversion.ConvertStrToInteger(p_sFromDate);

                if (p_sToDate == "")
                    iToDate = iMaxDate;
                else
                    iToDate = Conversion.ConvertStrToInteger(p_sToDate);

                if (p_sTermNumber != "ALL")
                {
                    iTermNumber = Conversion.ConvertStrToInteger(p_sTermNumber);
                    bDisplayAllTerm = false;
                }


                Function.CreateElement(p_objParentNode, ACCOUNT_HISTORY, ref objAccountHistory, base.ClientId);
                Function.CreateElement(objAccountHistory, SELECTED_BILL_ITEMS, base.ClientId);
                Function.CreateElement(objAccountHistory, BILL_ITEMS_GROUP_NODE_NAME, ref objBillItemsNode, base.ClientId);

                
                this.CreateBillItemListHead(objBillItemsNode);
                #region Create Bill Item XML
                switch (p_sStatus)
                {
                    case "":
                    case "U":
                        for (iIndex = 0; iIndex < p_arrlstBillingItems.Count; iIndex++)
                        {
                            objBillXBillItem = (BillXBillItem)p_arrlstBillingItems[iIndex];
                            if (objBillXBillItem.DateReconciled.Trim() == "")
                            {
                                if (objBillXBillItem.DateEntered.Trim() != "")
                                {
                                    if (Conversion.ConvertStrToInteger(objBillXBillItem.DateEntered) >= iFromDate && Conversion.ConvertStrToInteger(objBillXBillItem.DateEntered) <= iToDate)
                                    {
                                        if (bDisplayAllTerm || objBillXBillItem.TermNumber == iTermNumber)
                                        {
                                            dblCalculateBalance += objBillXBillItem.Amount;
                                            this.CreateBillItemXml(objBillItemsNode, objBillXBillItem, optionCount);
                                            optionCount++;
                                        }
                                    }
                                }
                                else
                                {
                                    if (bDisplayAllTerm || objBillXBillItem.TermNumber == iTermNumber)
                                    {
                                        dblCalculateBalance += objBillXBillItem.Amount;
                                        this.CreateBillItemXml(objBillItemsNode, objBillXBillItem, optionCount);
                                        optionCount++;
                                    }
                                }
                            }
                        }
                        break;
                    case "R":
                        for (iIndex = 0; iIndex < p_arrlstBillingItems.Count; iIndex++)
                        {
                            objBillXBillItem = (BillXBillItem)p_arrlstBillingItems[iIndex];
                            if (objBillXBillItem.DateReconciled.Trim() != "")
                            {
                                if (objBillXBillItem.DateEntered.Trim() != "")
                                {
                                    if (Conversion.ConvertStrToInteger(objBillXBillItem.DateEntered) >= iFromDate && Conversion.ConvertStrToInteger(objBillXBillItem.DateEntered) <= iToDate)
                                    {
                                        if (bDisplayAllTerm || objBillXBillItem.TermNumber == iTermNumber)
                                        {
                                            dblCalculateBalance += objBillXBillItem.Amount;
                                            this.CreateBillItemXml(objBillItemsNode, objBillXBillItem, optionCount);
                                            optionCount++;
                                        }
                                    }
                                }
                                else
                                {
                                    if (bDisplayAllTerm || objBillXBillItem.TermNumber == iTermNumber)
                                    {
                                        dblCalculateBalance += objBillXBillItem.Amount;
                                        this.CreateBillItemXml(objBillItemsNode, objBillXBillItem, optionCount);
                                        optionCount++;
                                    }
                                }
                            }
                        }
                        break;
                    case "A":
                        for (iIndex = 0; iIndex < p_arrlstBillingItems.Count; iIndex++)
                        {
                            objBillXBillItem = (BillXBillItem)p_arrlstBillingItems[iIndex];
                            if (objBillXBillItem.DateEntered.Trim() != "")
                            {
                                if (Conversion.ConvertStrToInteger(objBillXBillItem.DateEntered) >= iFromDate && Conversion.ConvertStrToInteger(objBillXBillItem.DateEntered) <= iToDate)
                                {
                                    if (bDisplayAllTerm || objBillXBillItem.TermNumber == iTermNumber)
                                    {
                                        dblCalculateBalance += objBillXBillItem.Amount;
                                        this.CreateBillItemXml(objBillItemsNode, objBillXBillItem, optionCount);
                                        optionCount++;
                                    }
                                }
                            }
                            else
                            {
                                if (bDisplayAllTerm || objBillXBillItem.TermNumber == iTermNumber)
                                {
                                    dblCalculateBalance += objBillXBillItem.Amount;
                                    this.CreateBillItemXml(objBillItemsNode, objBillXBillItem, optionCount);
                                    optionCount++;
                                }
                            }

                        }
                        break;
                }
                #endregion                

                Function.CreateElement(objBillItemsNode, "option", ref objBillItemNode, base.ClientId);
                objBillItemNode.SetAttribute("ref", INSTANCE_SYSEXDATA_PATH + "/" + ROOT_NODE_NAME + "/" + ACCOUNT_HISTORY + "/" + p_objParentNode.Name.ToString() + "/option[" + (optionCount+1).ToString() + "]");

                //objOptionXmlElement.SetAttribute("ref", Constants.INSTANCE_SYSEXDATA_PATH + objOptionXmlElement.ParentNode.LocalName + "/option[" + (p_arrlstBillingItems.Count+1).ToString() + "]");

                objXmlAttribute = objBillItemsNode.OwnerDocument.CreateAttribute("type");
                objXmlAttribute.InnerText = "new";
                objBillItemNode.Attributes.Append(objXmlAttribute);
                objXmlAttribute = null;

                Function.CreateElement(objBillItemNode, "BillItemType", ref objBillItemTypeNode, base.ClientId);
                objBillItemTypeNode.SetAttribute("codeid", "0");

                Function.CreateElement(objBillItemNode, "BillItemStatus", ref objBillItemTypeNode, base.ClientId);
                objBillItemTypeNode.SetAttribute("codeid", "0");

                Function.CreateElement(objBillItemNode, "FinalBatchType", ref objBillItemTypeNode, base.ClientId);
                objBillItemTypeNode.SetAttribute("codeid", "0");

                Function.CreateElement(objBillItemNode, "EntryBatchType", ref objBillItemTypeNode, base.ClientId);
                objBillItemTypeNode.SetAttribute("codeid", "0");                             
                
                //Function.CreateAndSetElement(objAccountHistory, CALCULATE_BALANCE, string.Format("{0:C}", dblCalculateBalance));
                Function.CreateAndSetElement(objAccountHistory, CALCULATE_BALANCE, Convert.ToString(dblCalculateBalance), base.ClientId);
                //Function.CreateAndSetElement(objAccountHistory, POLICY_BALANCE, string.Format("{0:C}", this.GetPolicyBalance(p_arrlstBillingItems)));
                Function.CreateAndSetElement(objAccountHistory, POLICY_BALANCE, Convert.ToString(this.GetPolicyBalance(p_arrlstBillingItems)), base.ClientId);

              

            }
            catch (Exception p_objEx)
            {
                Errors.Add(p_objEx, Common.BusinessAdaptorErrorType.Error);
            }
            finally
            {
                if (objBillXBillItem != null)
                {
                    objBillXBillItem.Dispose();
                    objBillXBillItem = null;
                }
                objBillItemsNode = null;
                objAccountHistory = null;
            }
        }
        private void CreateBillItemListHead(XmlElement p_objParentNode)
        {
            XmlElement objListHead = null;

            Function.CreateElement(p_objParentNode, "listhead", ref objListHead, base.ClientId);
            Function.CreateAndSetElement(objListHead, BILL_ITEM_DATE_ENTERED, "Date Entered", base.ClientId);
            Function.CreateAndSetElement(objListHead, BILL_ITEM_TYPE, "Billing Item Type", base.ClientId);
            //npadhy RMSC retrofit Starts

            Function.CreateAndSetElement(objListHead, BILL_RCPT_TYPE, "Receipt Type", base.ClientId);
            //npadhy RMSC retrofit Ends
            Function.CreateAndSetElement(objListHead, BILL_ITEM_STATUS, "Status", base.ClientId);
            Function.CreateAndSetElement(objListHead, BILL_ITEM_AMOUNT, "Amount", base.ClientId);
            Function.CreateAndSetElement(objListHead, BILL_ITEM_DUE_DATE, "Due Date", base.ClientId);
            Function.CreateAndSetElement(objListHead, BILL_ITEM_DATE_RECONCILED, "Date Reconciled", base.ClientId);
            Function.CreateAndSetElement(objListHead, BILL_ITEM_FINAL_BATCH_NUM, "Final batch Number", base.ClientId);
            Function.CreateAndSetElement(objListHead, BILL_ITEM_FINAL_BATCH_TYPE, "Final Batch Type", base.ClientId);
            Function.CreateAndSetElement(objListHead, BILL_ITEM_ENTRY_BATCH_NUM, "Entry Batch Number", base.ClientId);
            Function.CreateAndSetElement(objListHead, BILL_ITEM_ENTRY_BATCH_TYPE, "Entry Batch Type", base.ClientId);
            Function.CreateAndSetElement(objListHead, BILL_ITEM_INVOICE_ID, "Invoice Number", base.ClientId);
            Function.CreateAndSetElement(objListHead, BILL_ITEM_ROW_ID, "Bill Item Row ID", base.ClientId);
        }
        private void CreateBillItemXml(XmlElement p_objParentNode, BillXBillItem p_objBillXBillItem,int p_optionCount)
        {
            XmlElement objBillItemNode = null;
            XmlElement objBillItem = null;
            try
            {
                Function.CreateElement(p_objParentNode, "option", ref objBillItemNode, base.ClientId);
                objBillItemNode.SetAttribute("ref", INSTANCE_SYSEXDATA_PATH + "/" + ROOT_NODE_NAME + "/" + ACCOUNT_HISTORY +"/"+p_objParentNode.Name.ToString()  + "/option[" + (p_optionCount + 1).ToString() + "]");

                Function.CreateAndSetElement(objBillItemNode, BILL_ITEM_DATE_ENTERED, Conversion.GetDBDateFormat(p_objBillXBillItem.DateEntered, "d"), base.ClientId);
                Function.CreateAndSetElement(objBillItemNode, BILL_ITEM_TYPE, objCache.GetCodeDesc(p_objBillXBillItem.BillingItemType), ref objBillItem, base.ClientId); 
                objBillItem.SetAttribute("codeid", p_objBillXBillItem.BillingItemType.ToString());
                
                //npadhy RMSC retrofit Starts
                if (p_objBillXBillItem.BillXReceipt != null)
                {
                    Function.CreateAndSetElement(objBillItemNode, BILL_RCPT_TYPE, objCache.GetCodeDesc(p_objBillXBillItem.BillXReceipt.RcptType), ref objBillItem, base.ClientId); 
                    objBillItem.SetAttribute("codeid", p_objBillXBillItem.BillXReceipt.RcptType.ToString());
                }
                else
                {
                    Function.CreateAndSetElement(objBillItemNode, BILL_RCPT_TYPE, "", ref objBillItem, base.ClientId);
                    objBillItem.SetAttribute("codeid", "0");
                }
                //npadhy RMSC retrofit Ends

                Function.CreateAndSetElement(objBillItemNode, BILL_ITEM_STATUS, objCache.GetShortCode(p_objBillXBillItem.Status), ref objBillItem, base.ClientId);
                objBillItem.SetAttribute("codeid", p_objBillXBillItem.Status.ToString());

                Function.CreateAndSetElement(objBillItemNode, BILL_ITEM_AMOUNT, string.Format("{0:C}", p_objBillXBillItem.Amount), base.ClientId);
                Function.CreateAndSetElement(objBillItemNode, BILL_ITEM_DUE_DATE, Conversion.GetDBDateFormat(p_objBillXBillItem.DueDate, "d"), base.ClientId);
                Function.CreateAndSetElement(objBillItemNode, BILL_ITEM_DATE_RECONCILED, Conversion.GetDBDateFormat(p_objBillXBillItem.DateReconciled, "d"), base.ClientId);
                Function.CreateAndSetElement(objBillItemNode, BILL_ITEM_FINAL_BATCH_NUM, p_objBillXBillItem.FinalBatchNum.ToString(), base.ClientId);
                Function.CreateAndSetElement(objBillItemNode, BILL_ITEM_FINAL_BATCH_TYPE, objCache.GetCodeDesc(p_objBillXBillItem.FinalBatchType), ref objBillItem, base.ClientId); 
                objBillItem.SetAttribute("codeid", p_objBillXBillItem.FinalBatchType.ToString());

                Function.CreateAndSetElement(objBillItemNode, BILL_ITEM_ENTRY_BATCH_NUM, p_objBillXBillItem.EntryBatchNum.ToString(), base.ClientId);
                Function.CreateAndSetElement(objBillItemNode, BILL_ITEM_ENTRY_BATCH_TYPE, objCache.GetCodeDesc(p_objBillXBillItem.EntryBatchType), ref objBillItem, base.ClientId);
                objBillItem.SetAttribute("codeid", p_objBillXBillItem.EntryBatchType.ToString());

                Function.CreateAndSetElement(objBillItemNode, BILL_ITEM_INVOICE_ID, p_objBillXBillItem.InvoiceId.ToString(), base.ClientId);
                Function.CreateAndSetElement(objBillItemNode, BILL_ITEM_ROW_ID, p_objBillXBillItem.BillingItemRowid.ToString(), base.ClientId);

                Function.CreateAndSetElement(objBillItemNode, BILL_ITEM_TYPE_STATUS, objCache.GetCodeDesc(p_objBillXBillItem.BillingItemType, base.Adaptor.userLogin.objUser.NlsCode) + "_" + objCache.GetShortCode(p_objBillXBillItem.Status), ref objBillItem, base.ClientId);
            }
            catch (Exception p_objEx)
            {
                Errors.Add(p_objEx, Common.BusinessAdaptorErrorType.Error);
            }
            finally
            {
                objBillItemNode = null;
                objBillItem = null;
            }
        }
        private void GetPolicyDetails(int p_iPolicyId, ref string p_sPolicyName, ref int p_iTermNumber, ref int p_iPolicyStatus, ref string p_sPolicyNumber, ref int p_iPayeeEid, XmlElement p_objParentNode)
        {
            XmlElement objPolicyStatus = null;
            XmlCDataSection objCData;
            int iMaxTermNumber = 0;
            int iMaxSequenceNumber = 0;

            try
            {
                p_sPolicyName = Function.GetPolicyName(p_iPolicyId, m_sConnectionString,base.ClientId);
                if (p_sPolicyName != "")
                {
                    iMaxTermNumber = Function.GetMaxTermNumber(p_iPolicyId, m_sConnectionString,base.ClientId);
                    iMaxSequenceNumber = Function.GetMaxSequenceNumber(p_iPolicyId, iMaxTermNumber, m_sConnectionString,base.ClientId);
                    p_iPolicyStatus = Function.GetPolicyStatus(p_iPolicyId, iMaxTermNumber, iMaxSequenceNumber, m_sConnectionString,base.ClientId);
                    p_sPolicyNumber = Function.GetPolicyNumber(p_iPolicyId, iMaxTermNumber, iMaxSequenceNumber, ref p_iTermNumber, m_sConnectionString,base.ClientId);
                }

                p_iPayeeEid = Function.GetBillToPayeeEid(p_iPolicyId, iMaxTermNumber, m_sConnectionString,base.ClientId);


                Function.CreateAndSetElement(p_objParentNode, POLICY_NAME, p_sPolicyName, base.ClientId);
                Function.CreateAndSetElement(p_objParentNode, POLICY_NUMBER, p_sPolicyNumber, base.ClientId);
                Function.CreateAndSetElement(p_objParentNode, POLICY_ID, p_iPolicyId.ToString(), base.ClientId);

                Function.CreateElement(p_objParentNode, POLICY_STATUS, ref objPolicyStatus, base.ClientId);
                objPolicyStatus.SetAttribute("CodeId", p_iPolicyStatus.ToString());
                objPolicyStatus.SetAttribute("ShortCode", objCache.GetShortCode(p_iPolicyStatus));
                objPolicyStatus.SetAttribute("CodeDesc", objCache.GetCodeDesc(p_iPolicyStatus, base.Adaptor.userLogin.objUser.NlsCode));
                objCData = objPolicyStatus.OwnerDocument.CreateCDataSection(objCache.GetCodeDesc(p_iPolicyStatus, base.Adaptor.userLogin.objUser.NlsCode));
                objPolicyStatus.AppendChild(objCData);

                //Function.CreateAndSetElement(p_objParentNode, POLICY_TERM_NUMBER, p_iTermNumber.ToString());
            }
            catch (Exception p_objEx)
            {
                Errors.Add(p_objEx, Common.BusinessAdaptorErrorType.Error);
            }
            finally
            {
                objPolicyStatus = null;
                objCData = null;
            }
        }
        private double GetPolicyBalance(ArrayList p_arrlstBillingItems)
        {
            BillXBillItem objBillXBillItem = null;
            int iIndex = 0;
            double dblPolicyBalance = 0.0;

            try
            {
                for (iIndex = 0; iIndex < p_arrlstBillingItems.Count; iIndex++)
                {
                    objBillXBillItem = (BillXBillItem)p_arrlstBillingItems[iIndex];
                    dblPolicyBalance += objBillXBillItem.Amount;
                }
            }
            catch (Exception p_objEx)
            {
                Errors.Add(p_objEx, Common.BusinessAdaptorErrorType.Error);
            }
            finally
            {
                if (objBillXBillItem != null)
                {
                    objBillXBillItem.Dispose();
                    objBillXBillItem = null;
                }
            }
            return (dblPolicyBalance);
        }
        #endregion
	}
}
