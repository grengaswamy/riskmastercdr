﻿using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using System.Text;
using Riskmaster.Application.Diaries;

namespace Riskmaster.BusinessAdaptor
{
    /// <summary>
    /// Summary description for WpaDiaryEntry Screen.
    /// Created by agupta298 
    /// </summary>
    public class WpaDiaryEntryForm : DataEntryFormBase
    {
        const string CLASS_NAME = "WpaDiaryEntry";
        private WpaDiaryEntry WpaDiaryEntry { get { return objData as WpaDiaryEntry; } }
        private LocalCache objCache { get { return objData.Context.LocalCache; } }
        const string INSTANCE_SYSEXDATA_PATH = "/Instance/UI/FormVariables/SysExData";
        private string m_connectionString = null;
        private Hashtable m_DiariesConfigs = null;
        private int m_iClientId = 0;
        public const string DISABLE_MAIL = "MailIntegration";

        public override void InitNew()
        {
            base.InitNew();
        }

        public WpaDiaryEntryForm(FormDataAdaptor fda)
            : base(fda)
        {
            base.m_ClassName = CLASS_NAME;
            m_connectionString = fda.connectionString;
            m_iClientId = base.ClientId;
            m_DiariesConfigs = RMConfigurationManager.GetSingleTagSectionSettings("Diaries", m_connectionString, m_iClientId);//rkaur27
        }

        public override void AfterSave()
        {
        }

        public override void OnUpdateForm()
        {
            //XmlDocument propertyStore = new XmlDocument();
            //string sPropertyStore = string.Empty;

            WPA objWPA = null;
            string sAttachRecordInfo = string.Empty;
            string sCodeDescRollFlag = string.Empty;
            string sCodeDescRouteFlag = string.Empty;
            int iLangCode = 0;
            iLangCode = base.Adaptor.userLogin.objUser.NlsCode;

            base.OnUpdateForm();

            objWPA = new WPA(m_connectionString, base.ClientId);
            
            //if (base.m_fda.HasParam("SysPropertyStore"))
            //    sPropertyStore = base.m_fda.SafeParam("SysPropertyStore").InnerXml;
            //propertyStore.LoadXml(sPropertyStore);

            if (m_DiariesConfigs[DISABLE_MAIL].ToString() == "true")
            {
                base.AddReadOnlyNode("AutoConfirmNotify");
            }

            base.CreateSysExData("EmailNotifyFlag");
            base.CreateSysExData("ActivityList");
            base.CreateSysExData("IssueDateText");
            base.CreateSysExData("IssueOnComp");
            base.CreateSysExData("DaysAfterComp");
            base.CreateSysExData("AttachRecordInfo");
            base.CreateSysExData("AttachTable");
            base.CreateSysExData("AssigningUser");
            base.CreateSysExData("StatusOpen");
            base.CreateSysExData("UserNameList");
            base.CreateSysExData("GroupIdList");

            base.ResetSysExData("LoginUser", base.m_fda.userLogin.LoginName);
            base.ResetSysExData("LoginID", Convert.ToString(base.m_fda.userLogin.UserId));

            PopulateWpaTaskList();
            PopulateAssignedUser(WpaDiaryEntry.EntryId);

            if (this.CurrentAction == enumFormActionType.AddNew)//TODO:
            {
                base.ResetSysExData("DefaultAssignedTo", objWPA.GetDefaultAssignedTo());
                base.ResetSysExData("CurrentDate", String.Format("{0:MM/dd/yyyy}", DateTime.Now));
                base.ResetSysExData("CurrentTime", String.Format("{0:hh:mm tt}", DateTime.Now));
            }

            if (WpaDiaryEntry.EntryId > 0)
            {
                base.AddKillNode("AssignedUser");
                base.AddKillNode("IssueOnComp");
                base.AddKillNode("IssueEvery");
                base.AddReadOnlyNode("AttachRecordid");
                PopulateActivityList(WpaDiaryEntry.EntryId);
                sAttachRecordInfo = objWPA.GetAttachRecordInfoDetails(WpaDiaryEntry.AttachTable, WpaDiaryEntry.AttachRecordid);
                base.ResetSysExData("AttachRecordInfo", sAttachRecordInfo);
            }
            else
            {
                //For Scripting in case of Attached Record in creatediary only
                if (string.Compare(base.GetSysExDataNodeText("/SysExData/CalledBy"), "creatediary") == 0)
                {
                    if (WpaDiaryEntry.AttachTable.ToLower() == "claim")
                    {
                        sAttachRecordInfo = objWPA.GetAttachRecordInfoDetails(WpaDiaryEntry.AttachTable, WpaDiaryEntry.AttachRecordid);
                        sAttachRecordInfo = sAttachRecordInfo.Replace("Claim: ", string.Empty);
                    }
                    base.ResetSysExData("AttachRecordInfo", sAttachRecordInfo);
                }
                else
                {
                    //As Attachdiary page work with particular/specific Record from which it is getting created, so it can not be assigned from script. 
                    WpaDiaryEntry.AttachRecordid = 0;
                    base.ResetSysExData("AttachTable",string.Empty);
                    base.ResetSysExData("AttachRecordInfo",string.Empty);
                }           
            }
            base.ResetSysExData("AttachTable", WpaDiaryEntry.AttachTable.ToUpper());
            if (string.Compare(base.GetSysExDataNodeText("/SysExData/CalledBy"), "creatediary") == 0)
            {                
                base.ResetSysExData("CalledBy", "creatediary");
            }
            else if (string.Compare(base.GetSysExDataNodeText("/SysExData/CalledBy"), "attachdiary") == 0)
            {
                base.ResetSysExData("CalledBy", "attachdiary");
            }
            else if (string.Compare(base.GetSysExDataNodeText("/SysExData/CalledBy"), "diarydetails") == 0)
            {
                base.ResetSysExData("CalledBy", "diarydetails");
                base.ResetSysExData("CompleteDateTime", Conversion.GetDBDateFormat(WpaDiaryEntry.CompleteDate, "d") + " " + Conversion.GetDBTimeFormat(Conversion.ConvertObjToStr(WpaDiaryEntry.CompleteTime), "t"));

                base.ResetSysExData("CreateDate", Conversion.ToDate(WpaDiaryEntry.CreateDate).ToString());
                base.ResetSysExData("TeStartTime", Conversion.GetDBTimeFormat(WpaDiaryEntry.TeStartTime, "t"));
                base.ResetSysExData("TeEndTime", Conversion.GetDBTimeFormat(WpaDiaryEntry.TeEndTime, "t"));
                base.ResetSysExData("ResponseDate", Conversion.GetDBDateFormat(WpaDiaryEntry.ResponseDate, "d"));


                if (Conversion.ConvertObjToStr(WpaDiaryEntry.NonRouteFlag) == Boolean.TrueString)
                    sCodeDescRouteFlag = objCache.GetCodeDesc(objCache.GetCodeId("Y", "YES_NO"), iLangCode);
                else if (Conversion.ConvertObjToStr(WpaDiaryEntry.NonRouteFlag) == Boolean.FalseString)
                    sCodeDescRouteFlag = objCache.GetCodeDesc(objCache.GetCodeId("N", "YES_NO"), iLangCode);
                base.ResetSysExData("NonRouteFlag", sCodeDescRouteFlag);

                if (Conversion.ConvertObjToStr(WpaDiaryEntry.NonRollFlag) == Boolean.TrueString)
                    sCodeDescRollFlag = objCache.GetCodeDesc(objCache.GetCodeId("Y", "YES_NO"), iLangCode);
                else if (Conversion.ConvertObjToStr(WpaDiaryEntry.NonRollFlag) == Boolean.FalseString)
                    sCodeDescRollFlag = objCache.GetCodeDesc(objCache.GetCodeId("N", "YES_NO"), iLangCode);
                base.ResetSysExData("NonRollFlag", sCodeDescRollFlag);

                if(WpaDiaryEntry.StatusOpen)
                {
                    base.AddKillNode("CompletedOn");
                    base.AddKillNode("CompletionResponse");
                    base.AddKillNode("CompletedByUser");
                    base.AddKillNode("AssigningUser");
                    base.AddKillNode("DiaryStatusMsg");
                    base.AddKillNode("closediary");
                }
                else
                {
                    base.AddKillNode("StatusMsg");
                    base.AddKillNode("editdiary");
                    base.AddKillNode("completediary");
                    base.AddKillNode("routediary");
                    base.AddKillNode("rolldiary");
                    base.AddKillNode("canceldiary");
                    base.AddKillNode("printdiary");
                }

                if(!WpaDiaryEntry.TeTracked)
                {
                    base.AddKillNode("TeMessage");
                    base.AddKillNode("TEStartTime");
                    base.AddKillNode("TEEndTime");
                    base.AddKillNode("TeTotalHours");
                    base.AddKillNode("TeExpenses");
                }
            }
            else
            {
                base.ResetSysExData("CalledBy", "");
            }
        }

        public override void OnValidate(ref bool Cancel)
        {
            bool bError = false;
            //Validation Logic - Start

            if (WpaDiaryEntry.EntryId == 0)
            {
                if (!Adaptor.userLogin.IsAllowedEx(m_SecurityId, RMO_CREATE))
                {
                    //m_CurrentAction = enumFormActionType.None;
                    Errors.Add(Globalization.GetString("PermissionFailure", base.ClientId),
                    string.Format(Globalization.GetString("Permission.FullPermissionMsg", base.ClientId), Globalization.GetString("Permission.NoCreate", base.ClientId)),
                    BusinessAdaptorErrorType.Error);
                    bError = true;
                    return;
                }
            }
            else if (!Adaptor.userLogin.IsAllowedEx(m_SecurityId, RMO_UPDATE))
            {
                //m_CurrentAction = enumFormActionType.None;
                Errors.Add(Globalization.GetString("PermissionFailure", base.ClientId),
                string.Format(Globalization.GetString("Permission.FullPermissionMsg", base.ClientId), Globalization.GetString("Permission.NoUpdate", base.ClientId)),
                BusinessAdaptorErrorType.Error);
                bError = true;
                return;
            }

            //Validation Logic - End
            // Return true if there were validation errors
            Cancel = bError;
        }

        public override void OnUpdateObject()
        {
            XmlDocument objXML = base.SysEx;
            XmlElement xAssignedUserList = null;
            string[] sAssignedUsersArray = null;
            string[] sAssignedGroupArray = null;
            XmlElement xmlOption = null;
            XmlAttribute xmlOptionAttrib = null;
            XmlAttribute xmlCodeIdAttrib = null;
            Claim objClaim = null;
            Event objEvent = null;
            WPA objWPA = null;
            string sClaimNumber = string.Empty;
            string sClaimantName = string.Empty;
            string sDepartmentName = string.Empty;
            string sOrgLevel = string.Empty;
            string sEventNumber = string.Empty;
            string[] arrActivity = null;
            objWPA = new WPA(m_connectionString, base.ClientId);

            base.OnUpdateObject();

            if (objXML.SelectSingleNode("//ActivityList") != null)
            {
                string sActivityList = objXML.SelectSingleNode("//ActivityList").InnerXml;
                if (!string.IsNullOrWhiteSpace(sActivityList))
                {
                    arrActivity = sActivityList.Split(new char[] { '|' });
                    WpaDiaryEntry.ActivityList = arrActivity;
                }

                if (arrActivity != null)
                {
                    XmlElement xActivityList = (XmlElement)base.SysEx.SelectSingleNode("//ActivityList");

                    if (xActivityList != null)
                        xActivityList.RemoveAll();

                    for (int iActivityCount = 0; iActivityCount < arrActivity.GetLength(0); iActivityCount += 2)
                    {
                        xmlOption = base.SysEx.CreateElement("option");
                        xmlOption.InnerText = arrActivity[iActivityCount + 1];
                        xmlOptionAttrib = base.SysEx.CreateAttribute("value");
                        xmlOptionAttrib.Value = arrActivity[iActivityCount];
                        xmlOption.Attributes.Append(xmlOptionAttrib);
                        xActivityList.AppendChild(xmlOption);
                    }
                }
            }

            if (objXML.SelectSingleNode("//IssueDateText") != null && !string.IsNullOrWhiteSpace(objXML.SelectSingleNode("//IssueDateText").InnerText))
            {
                WpaDiaryEntry.IssueTillDays = Conversion.ConvertStrToInteger(objXML.SelectSingleNode("//IssueDateText").InnerText);
                WpaDiaryEntry.IssuePeriod = 2;
            }

            if (objXML.SelectSingleNode("//CalledBy") != null && !string.IsNullOrWhiteSpace(objXML.SelectSingleNode("//CalledBy").InnerText))
            {
                WpaDiaryEntry.CalledBy = objXML.SelectSingleNode("//CalledBy").InnerXml;
            }

            //if (objXML.SelectSingleNode("//AttachRecordId") != null && !string.IsNullOrWhiteSpace(objXML.SelectSingleNode("//AttachRecordId").InnerText))
            //{
            //    WpaDiaryEntry.AttachRecordid = Conversion.ConvertStrToInteger(objXML.SelectSingleNode("//AttachRecordId").InnerText);
            //    if (WpaDiaryEntry.AttachRecordid > 0)
            //        WpaDiaryEntry.IsAttached = true;
            //}

            if (WpaDiaryEntry.AttachRecordid > 0)
                WpaDiaryEntry.IsAttached = true;

            if (objXML.SelectSingleNode("//EmailNotifyFlag") != null && !string.IsNullOrWhiteSpace(objXML.SelectSingleNode("//EmailNotifyFlag").InnerText))
            {
                WpaDiaryEntry.EmailNotifyFlag = objXML.SelectSingleNode("//EmailNotifyFlag").InnerText;
            }

            if (objXML.SelectSingleNode("//AttachTable") != null && !string.IsNullOrWhiteSpace(objXML.SelectSingleNode("//AttachTable").InnerText))
            {
                WpaDiaryEntry.AttachTable = objXML.SelectSingleNode("//AttachTable").InnerText;
            }

            if (WpaDiaryEntry.AttachTable.ToUpper() == "CLAIM")
            {
                objClaim = (Claim)m_fda.Factory.GetDataModelObject("Claim", false);
                objClaim.MoveTo(WpaDiaryEntry.AttachRecordid);
                sClaimNumber = objClaim.ClaimNumber;
                sClaimantName = objWPA.GetClaimantsName(WpaDiaryEntry.AttachRecordid);
                sDepartmentName = objWPA.GetDeptName(0, WpaDiaryEntry.AttachRecordid, ref sOrgLevel);
                WpaDiaryEntry.Regarding = String.Format("{0} * {1} * {2}", sClaimNumber, sClaimantName, sDepartmentName);
            }
            else if (WpaDiaryEntry.AttachTable.ToUpper() == "EVENT")
            {
                objEvent = (Event)m_fda.Factory.GetDataModelObject("Event", false);
                objEvent.MoveTo(WpaDiaryEntry.AttachRecordid);
                sEventNumber = objEvent.EventNumber;
                sDepartmentName = objWPA.GetDeptName(WpaDiaryEntry.AttachRecordid, 0, ref sOrgLevel);
                WpaDiaryEntry.Regarding = String.Format("{0} * {1}", sEventNumber, sDepartmentName);
            }
            WpaDiaryEntry.AttParentCode = objWPA.GetParentRecord(WpaDiaryEntry.AttachTable.ToUpper(), WpaDiaryEntry.AttachRecordid);
            WpaDiaryEntry.DiaryVoid = false;
            WpaDiaryEntry.DiaryDeleted = false;

            if (WpaDiaryEntry.AutoConfirm)
                WpaDiaryEntry.NotifyFlag = true;

            if (objXML.SelectSingleNode("//AssigningUser") != null)
            {
                if (objXML.SelectSingleNode("//AssigningUser").InnerText == "currentuser" && !(WpaDiaryEntry.EntryId > 0))
                    WpaDiaryEntry.AssigningUser = base.m_fda.userLogin.LoginName;
                else if (!(WpaDiaryEntry.EntryId > 0))
                    WpaDiaryEntry.AssigningUser = objXML.SelectSingleNode("//AssigningUser").InnerText;
            }

            if (objXML.SelectSingleNode("//StatusOpen") != null && !string.IsNullOrWhiteSpace(objXML.SelectSingleNode("//StatusOpen").InnerXml))
            {
                WpaDiaryEntry.StatusOpen = Conversion.ConvertStrToBool(objXML.SelectSingleNode("//StatusOpen").InnerXml.Trim());
                if (!Conversion.ConvertStrToBool(objXML.SelectSingleNode("//StatusOpen").InnerXml.Trim()))
                {
                    WpaDiaryEntry.CompletedByUser = base.m_fda.userLogin.LoginName;
                }
            }

            xAssignedUserList = (XmlElement)base.SysEx.CreateElement("AssignedUserList");

            if (objXML.SelectSingleNode("//UserNameList") != null && !string.IsNullOrWhiteSpace(objXML.SelectSingleNode("//UserNameList").InnerText))
            {
                WpaDiaryEntry.AssignedUser = objXML.SelectSingleNode("//UserNameList").InnerText;
                #region Code for Ressigning Assingeduser in case of Validation
                sAssignedUsersArray = WpaDiaryEntry.AssignedUser.Split(new char[] { ',' });
                for (int iCount = 0; iCount < sAssignedUsersArray.GetLength(0); iCount++)
                {
                    if (!string.IsNullOrWhiteSpace(sAssignedUsersArray[iCount]))
                    {
                        xmlOption = base.SysEx.CreateElement("option");
                        xmlOption.InnerText = sAssignedUsersArray[iCount];
                        xmlOptionAttrib = base.SysEx.CreateAttribute("value");
                        xmlOptionAttrib.Value = Convert.ToString(DbFactory.ExecuteScalar(SecurityDatabase.GetSecurityDsn(base.ClientId), "SELECT USER_ID FROM USER_DETAILS_TABLE WHERE LOGIN_NAME = '" + sAssignedUsersArray[iCount] + "'"));
                        xmlOption.Attributes.Append(xmlOptionAttrib);
                        xAssignedUserList.AppendChild(xmlOption);
                    }
                } 
                #endregion
            }

            if (objXML.SelectSingleNode("//GroupIdList") != null && !string.IsNullOrWhiteSpace(objXML.SelectSingleNode("//GroupIdList").InnerText))
            {
                WpaDiaryEntry.AssignedGroup = objXML.SelectSingleNode("//GroupIdList").InnerText;
                #region Code for Ressigning Assingeduser in case of Validation
                sAssignedGroupArray = WpaDiaryEntry.AssignedGroup.Split(new char[] { ',' });
                for (int iCount = 0; iCount < sAssignedGroupArray.GetLength(0); iCount++)
                {
                    if (!string.IsNullOrWhiteSpace(sAssignedGroupArray[iCount]))
                    {
                        xmlOption = base.SysEx.CreateElement("option");
                        xmlOption.InnerText = Convert.ToString(DbFactory.ExecuteScalar(m_connectionString, "SELECT GROUP_NAME FROM USER_GROUPS WHERE GROUP_ID = '" + sAssignedGroupArray[iCount] + "'"));
                        xmlOptionAttrib = base.SysEx.CreateAttribute("value");
                        xmlOptionAttrib.Value = sAssignedGroupArray[iCount];
                        xmlOption.Attributes.Append(xmlOptionAttrib);
                        xAssignedUserList.AppendChild(xmlOption);
                    }
                }
                #endregion
            }

            XmlNode objOrig = base.SysEx.SelectSingleNode("/*/AssignedUserList");
            if (objOrig != null)
                base.SysEx.DocumentElement.RemoveChild(objOrig);
            base.SysEx.DocumentElement.AppendChild(xAssignedUserList as XmlNode);
        }

        private void PopulateWpaTaskList()
        {
            XmlElement taskList = base.SysEx.CreateElement("WpaTaskList");
            bool bIsRowExist = false;
            string SQL = string.Empty;
            int iLangCode = 0;
            string sTableName = string.Empty;
            iLangCode = base.Adaptor.userLogin.objUser.NlsCode;
            sTableName = "WPA_TASKS";
            if (DbFactory.IsOracleDatabase(m_connectionString))
            {
                SQL = "SELECT CODES.CODE_ID, CODES.SHORT_CODE || ' - ' || CODES_TEXT.CODE_DESC AS CodeDescription FROM CODES INNER JOIN CODES_TEXT ON CODES.CODE_ID = CODES_TEXT.CODE_ID INNER JOIN GLOSSARY g ON CODES.TABLE_ID = g.TABLE_ID WHERE g.SYSTEM_TABLE_NAME = 'WPA_TASKS' AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND CODES_TEXT.LANGUAGE_CODE = 1033";
            }
            else
            {
                SQL = "SELECT CODES.CODE_ID, CODES.SHORT_CODE + ' - ' + CODES_TEXT.CODE_DESC AS CodeDescription FROM CODES INNER JOIN CODES_TEXT ON CODES.CODE_ID = CODES_TEXT.CODE_ID INNER JOIN GLOSSARY g ON CODES.TABLE_ID = g.TABLE_ID WHERE g.SYSTEM_TABLE_NAME = 'WPA_TASKS' AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL)  AND CODES_TEXT.LANGUAGE_CODE = 1033";
            }
            StringBuilder sbSQL = new StringBuilder();
            sbSQL = CommonFunctions.PrepareMultilingualQuery(SQL.ToString(), sTableName, iLangCode);
            sbSQL.Append(" ORDER BY CodeDescription");
            using (DbReader rdr = DbFactory.GetDbReader(m_connectionString, sbSQL.ToString()))
            {
                while (rdr.Read())
                {
                    XmlElement xmlOption = base.SysEx.CreateElement("option");
                    xmlOption.InnerText = Conversion.ConvertObjToStr(rdr.GetValue(1));
                    XmlAttribute xmlOptionAttrib = base.SysEx.CreateAttribute("value");
                    xmlOptionAttrib.Value = Conversion.ConvertObjToStr(rdr.GetValue(0));
                    xmlOption.Attributes.Append(xmlOptionAttrib);
                    taskList.AppendChild(xmlOption);
                    bIsRowExist = true;
                }
            }

            base.CreateSysExData("UseWpaTask");
            base.CreateSysExData("ComboEntryName");

            if (!bIsRowExist)
            {
                base.AddKillNode("EntryNameDcboBox");
                base.ResetSysExData("UseWpaTask", "0");
            }
            else
            {
                base.SysEx.DocumentElement.AppendChild(taskList as XmlNode);
                base.ResetSysExData("UseWpaTask", "-1");
            }

        }

        private void PopulateAssignedUser(int p_EntryId)
        {
            WPA objWPA = null;
            XmlElement xAssignedUserList = null;
            XmlElement xmlOption = null;
            XmlAttribute xmlOptionAttrib = null;
            XmlAttribute xmlCodeIdAttrib = null;
            string sUserId = string.Empty;
            string sAssignedUserNames = string.Empty;
            string sAssignedUserGroupIds = string.Empty;
            string sCodeIds = string.Empty;

            xAssignedUserList = (XmlElement)base.SysEx.CreateElement("AssignedUserList");
            objWPA = new WPA(m_connectionString, base.ClientId);

            if (p_EntryId == 0)
            {
                if (objWPA.GetDefaultAssignedTo().ToLower() == "true")
                {
                    xmlOption = base.SysEx.CreateElement("option");
                    xmlOption.InnerText = base.m_fda.userLogin.LoginName;
                    xmlOptionAttrib = base.SysEx.CreateAttribute("value");
                    xmlOptionAttrib.Value = Convert.ToString(base.m_fda.userLogin.UserId);
                    xmlOption.Attributes.Append(xmlOptionAttrib);
                    xAssignedUserList.AppendChild(xmlOption);
                    
                    sAssignedUserNames = sAssignedUserNames + base.m_fda.userLogin.LoginName + ",";
                    sCodeIds = sCodeIds + Convert.ToString(base.m_fda.userLogin.UserId) + " ";
                }

                if (!string.IsNullOrWhiteSpace(WpaDiaryEntry.AssignedUser))
                {
                    sUserId = Convert.ToString(DbFactory.ExecuteScalar(SecurityDatabase.GetSecurityDsn(base.ClientId), "SELECT USER_ID FROM USER_DETAILS_TABLE WHERE LOGIN_NAME = '" + WpaDiaryEntry.AssignedUser + "'"));
                    xmlOption = base.SysEx.CreateElement("option");
                    xmlOption.InnerText = WpaDiaryEntry.AssignedUser;
                    xmlOptionAttrib = base.SysEx.CreateAttribute("value");
                    xmlOptionAttrib.Value = sUserId;
                    xmlOption.Attributes.Append(xmlOptionAttrib);
                    xAssignedUserList.AppendChild(xmlOption);

                    sAssignedUserNames = sAssignedUserNames + WpaDiaryEntry.AssignedUser;
                    sCodeIds = sCodeIds + sUserId + " ";
                }

                if (!string.IsNullOrWhiteSpace(WpaDiaryEntry.AssignedGroup) && WpaDiaryEntry.AssignedGroup != "NA")
                {
                    xmlOption = base.SysEx.CreateElement("option");
                    xmlOption.InnerText = Convert.ToString(DbFactory.ExecuteScalar(m_connectionString, "SELECT GROUP_NAME FROM USER_GROUPS WHERE GROUP_ID = '" + WpaDiaryEntry.AssignedGroup + "'"));
                    xmlOptionAttrib = base.SysEx.CreateAttribute("value");
                    xmlOptionAttrib.Value = WpaDiaryEntry.AssignedGroup;
                    xmlOption.Attributes.Append(xmlOptionAttrib);
                    xAssignedUserList.AppendChild(xmlOption);
                    sAssignedUserGroupIds = WpaDiaryEntry.AssignedGroup;
                    sCodeIds = sCodeIds + WpaDiaryEntry.AssignedGroup;
                }

                //to add codeid in attribute of AssignedUserList
                xmlCodeIdAttrib = base.SysEx.CreateAttribute("codeid");
                xmlCodeIdAttrib.Value = sCodeIds.Trim();
                xAssignedUserList.Attributes.Append(xmlCodeIdAttrib);

                base.ResetSysExData("UserNameList", sAssignedUserNames.TrimEnd(','));
                base.ResetSysExData("GroupIdList", sAssignedUserGroupIds);
            }
            else
            {
                xmlCodeIdAttrib = base.SysEx.CreateAttribute("codeid");
                xmlCodeIdAttrib.Value = string.Empty;
                xAssignedUserList.Attributes.Append(xmlCodeIdAttrib);

                if (!string.IsNullOrWhiteSpace(WpaDiaryEntry.AssignedUser))
                {
                    base.ResetSysExData("UserNameList", WpaDiaryEntry.AssignedUser);
                }
                if (!string.IsNullOrWhiteSpace(WpaDiaryEntry.AssignedGroup) && WpaDiaryEntry.AssignedGroup != "NA")
                {
                    base.ResetSysExData("GroupIdList", WpaDiaryEntry.AssignedGroup);
                }
            }


            XmlNode objOrig = base.SysEx.SelectSingleNode("/*/AssignedUserList");
            if (objOrig != null)
                base.SysEx.DocumentElement.RemoveChild(objOrig);
            base.SysEx.DocumentElement.AppendChild(xAssignedUserList as XmlNode);
        }

        private void PopulateActivityList(int p_EntryId)
        {
            string sSQL = string.Empty;
            string sActs = string.Empty;

            XmlElement xActivityList = (XmlElement)base.SysEx.SelectSingleNode("//ActivityList");
            XmlElement xActivityListOption = base.SysEx.CreateElement("ActivityListOption");

            if (xActivityList != null)
                xActivityList.RemoveAll(); //Just to remove CDATA Tag

            sSQL = "SELECT * FROM WPA_DIARY_ACT WHERE PARENT_ENTRY_ID=" + p_EntryId;
            using (DbReader objRdr = DbFactory.GetDbReader(m_connectionString, sSQL))
            {
                //Add activities for the given diary
                while (objRdr.Read())
                {
                    if (xActivityList != null)
                    {
                        XmlElement xmlOption = base.SysEx.CreateElement("option");
                        xmlOption.InnerText = Conversion.ConvertObjToStr(objRdr.GetValue("ACT_TEXT"));
                        XmlAttribute xmlOptionAttrib = base.SysEx.CreateAttribute("value");
                        xmlOptionAttrib.Value = Conversion.ConvertObjToStr(objRdr.GetValue("ACT_CODE"));
                        xmlOption.Attributes.Append(xmlOptionAttrib);
                        xActivityList.AppendChild(xmlOption);
                    }

                    //Activity data is needed in a string format too..raman bhatia
                    if (sActs != "")
                    {
                        sActs = (sActs + "|");
                    }
                    sActs = sActs + objRdr.GetValue("ACT_CODE") + "|" + objRdr.GetValue("ACT_TEXT");
                }
            }
            XmlElement xActs = base.SysEx.CreateElement("sActs");
            xActs.InnerText = sActs;
            xActivityListOption.AppendChild(xActs);
            base.SysEx.DocumentElement.AppendChild(xActivityListOption as XmlNode);
        }
    }
}
