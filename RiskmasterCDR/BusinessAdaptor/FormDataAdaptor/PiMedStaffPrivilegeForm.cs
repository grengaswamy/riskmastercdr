using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;


namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for PiMedStaffPrivilege Screen.
	/// </summary>
	public class PiMedStaffPrivilegeForm : DataEntryFormBase
	{
		const string CLASS_NAME = "MedicalStaffPrivilege";
		private MedicalStaffPrivilege MedicalStaffPrivilege{get{return objData as MedicalStaffPrivilege;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		const string FILTER_KEY_NAME = "StaffEid";
		public override void InitNew()
		{
			base.InitNew(); 

			this.m_ParentId = base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME,false);
			(objData as INavigation).Filter = 
				objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + this.m_ParentId.ToString();		
			this.MedicalStaffPrivilege.StaffEid = this.m_ParentId;	
		}

		public PiMedStaffPrivilegeForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}
        
        private void ApplyFormTitle()
        {
            XmlDocument objSysExDataXmlDoc = base.SysEx;
            string sSubTitleText = string.Empty;
            XmlNode objSubTitle = objSysExDataXmlDoc.SelectSingleNode("/SysExData/DupSubTitle");
            if (objSubTitle != null)
            {
                sSubTitleText=objSubTitle.InnerText;
            }
            ArrayList singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singleRow, "id", "formsubtitle");
            base.AddElementToList(ref singleRow, "Text", sSubTitleText);
            base.m_ModifiedControls.Add(singleRow);
        }

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
			base.OnUpdateForm ();
            ApplyFormTitle();
			// Subtitle node should already be present in SysExData
			// as this screen always opens from PiMedStaff
		}

	/*	public override void BeforeSave(ref bool Cancel)
		{
			XmlDocument objSysExDataXmlDoc = base.SysEx;
			XmlNode objStaffEidNode = null;
			try{objStaffEidNode = objSysExDataXmlDoc.SelectSingleNode("/SysExData/StaffEid");}
			catch{};
			
			if(objStaffEidNode !=null)
				if(this.MedicalStaffPrivilege.StaffEid==0)
					this.MedicalStaffPrivilege.StaffEid = Conversion.ConvertStrToInteger(objStaffEidNode.InnerText);

			objStaffEidNode = null;
			objSysExDataXmlDoc = null;

			base.BeforeSave (ref Cancel);		

		}*/
	}
}