using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using System.Text;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
    /// Author  :   Mridul Bansal
    /// Date    :   10/19/09
	/// Comment :   Property Maintenance
    /// MITS    :   18230
	/// </summary>
	public class PropertyUnitForm : DataEntryFormBase
	{
		const string CLASS_NAME = "PropertyUnit";
        const string FILTER_KEY_NAME = "PropertyId";

		private PropertyUnit PropertyUnit{get{return objData as PropertyUnit;}}

		private LocalCache objCache{get{return objData.Context.LocalCache;}}
	
		public PropertyUnitForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

		public override void InitNew()
		{
			base.InitNew();

			//set filter for UnitId            
            PropertyUnit.PropertyId = base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME);
            if (PropertyUnit.PropertyId > 0)
                (objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + PropertyUnit.PropertyId;
		}

        private void ApplyFormTitle()
        {
            string sSubTitleText = string.Empty;
            ArrayList singleRow = null;
            if (PropertyUnit != null)
            {
                sSubTitleText += " [ " + PropertyUnit.Pin + " ]";
            }

            //Pass this title value to view (ref'ed from @valuepath).
            base.ResetSysExData("SubTitle", sSubTitleText);

            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singleRow, "id", "formsubtitle");
            base.AddElementToList(ref singleRow, "Text", sSubTitleText);
            base.m_ModifiedControls.Add(singleRow);
        }

        /// <summary>
        /// Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
        /// </summary>
		public override void OnUpdateForm()
		{
			base.OnUpdateForm ();
            ApplyFormTitle();
            if (!PropertyUnit.Context.RMUser.IsAllowedEx(FormBase.RMO_SEARCH, FormBase.RMO_PROPERTY_SEARCH))
				base.AddKillNode("search");
            if (!PropertyUnit.Context.RMUser.IsAllowedEx(FormBase.RMO_LOOKUP, FormBase.RMO_PROPERTY_SEARCH))
				base.AddKillNode("lookup");

			CheckAutoGeneratePin();

            //Raman Bhatia - Acrosoft Phase 1 changes.. Document Management Not available for Employee if Acrosoft is enabled
            bool bUseAcrosoftInterface = PropertyUnit.Context.InternalSettings.SysSettings.UseAcrosoftInterface;
            bool bUsePaperVision = PropertyUnit.Context.InternalSettings.SysSettings.UsePaperVisionInterface; //Mona:PaperVisionMerge : Animesh inserted MITS 16697
            //Mona:PaperVisionMerge: Adding bUsePaperVision in the condition
            //rbhatia4:R8: Use Media View Setting : July 11 2011
            if (bUseAcrosoftInterface || bUsePaperVision || PropertyUnit.Context.InternalSettings.SysSettings.UseMediaViewInterface)
            {
                base.AddKillNode("attachdocuments");//Mona:PaperVisionMerge:Animesh Inserted MITS 16697
                base.AddKillNode("attach");
            }
            //akaur9 MITS 25163- Policy Interface Implementation --start

            int iSystemPolicyId = base.GetSysExDataNodeInt("//SystemPolicyId");

            if (iSystemPolicyId > 0)
            {
                base.AddReadOnlyNode("readOnlyPage");
                base.AddKillNode("save");
                base.AddKillNode("delete");
                //skhare7 Point Policy interface Added statunitno/riskloc/subloc
                if (PropertyUnit.PropertyId > 0)
                {
                    DisplayUnitNo();

                }
            }
            else
            {
                base.AddKillNode("Unitno");
            }
            //akaur9 MITS 25163- Policy Interface Implementation --End
		}

		internal override bool ValidateRequiredViewFields(XmlDocument propertyStore)
		{
			CheckAutoGeneratePin();
			return base.ValidateRequiredViewFields (propertyStore);
		}

        /// <summary>
        /// For Autogenerating PIN for new record in case it is not provided
        /// </summary>
        private void CheckAutoGeneratePin()
        {
            XmlElement objNotReqNew = null;
            string sNotReqNew = string.Empty;
            if (objData.Context.InternalSettings.SysSettings.AutoNumPin)
            {
                objNotReqNew = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysNotReqNew");
                sNotReqNew = objNotReqNew.InnerText;

                if (sNotReqNew.IndexOf("pin") == -1)
                    sNotReqNew = sNotReqNew + "pin|";
                objNotReqNew.InnerText = sNotReqNew;
            }
        }
        //Added by Yatharth
        //validate data according to the Business Rules
        public override void OnValidate(ref bool Cancel)
        {
            bool bError = false;
            // Perform data validation
            string sToday = Conversion.ToDbDate(System.DateTime.Now);
            
            //MITS 22408: Yatharth
            //Perform validation for entry of any future date in Appraised Date Field
            if (PropertyUnit.AppraisedDate != string.Empty)
            {
                if (PropertyUnit.AppraisedDate.CompareTo(sToday) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                        String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate",base.ClientId), Globalization.GetString("Field.AppraisedDate",base.ClientId), System.DateTime.Now.ToShortDateString()),
                        BusinessAdaptorErrorType.Error);
                    bError = true;
                }
            }

            // Return true if there were validation errors
            Cancel = bError;
        }

        private void DisplayUnitNo()
        {

            string sStaUnitNo = string.Empty;
            string sRiskLoc = string.Empty;
            string sSubLoc = string.Empty;
            string sUnitNo = string.Empty; //RMA-10039
            string sSystemType = objCache.GetShortCode(base.GetSysExDataNodeInt("//PolicySystemType"));//vkumar258-JIRA-4425
            StringBuilder sbSql = new StringBuilder();
            if (CommonFunctions.GetPolicySystemTypeIndicator(sSystemType) == Constants.POLICY_SYSTEM_TYPE.POINT)//vkumar258-JIRA-4425
            {
                sbSql.Append("SELECT STAT_UNIT_NUMBER,UNIT_RISK_LOC,UNIT_RISK_SUB_LOC FROM  POINT_UNIT_DATA WHERE ");
            }
            else if (CommonFunctions.GetPolicySystemTypeIndicator(sSystemType) == Constants.POLICY_SYSTEM_TYPE.INTEGRAL)// vkumar258--4425 JIRA-added for Integral
            {
                sbSql.Append("SELECT UNIT_NUMBER FROM  INTEGRAL_UNIT_DATA WHERE ");
            }
            else
            {// RMA-10039, Generic API processing -  15.2 release included Staging and cyberlife policies / units
                // The current approach of recording common data in separate POINT_UNIT_DATA and INTEGRAL_UNIT_DATA is obsolete 
                // and will be corrected when API consumes POINT and INTEGRAL policies
                sbSql.Append("SELECT UNIT_NUMBER,STAT_UNIT_NUMBER,UNIT_RISK_LOC,UNIT_RISK_SUB_LOC FROM POLICY_X_UNIT WHERE ");
            }
            sbSql.Append("  UNIT_TYPE='P' AND UNIT_ID=" + PropertyUnit.PropertyId);
            DbReader objReader = null;
            objReader = DbFactory.GetDbReader(base.m_fda.connectionString, sbSql.ToString());
            if (objReader != null)
            {
                if (objReader.Read())
                {
                    if (CommonFunctions.GetPolicySystemTypeIndicator(sSystemType) == Constants.POLICY_SYSTEM_TYPE.POINT)// vkumar258--4425 JIRA-added for Integral
                    {
                        sStaUnitNo = objReader.GetValue("STAT_UNIT_NUMBER").ToString();
                        sRiskLoc = objReader.GetValue("UNIT_RISK_LOC").ToString();
                        if (sRiskLoc == string.Empty)
                            sRiskLoc = "-";

                        sSubLoc = objReader.GetValue("UNIT_RISK_SUB_LOC").ToString();
                        if (sSubLoc == string.Empty)
                            sSubLoc = "-";

                        base.ResetSysExData("UnitNo", sStaUnitNo + "/" + sRiskLoc + "/" + sSubLoc);
                    }
                    else if (CommonFunctions.GetPolicySystemTypeIndicator(sSystemType) == Constants.POLICY_SYSTEM_TYPE.INTEGRAL)// vkumar258--4425 JIRA-added for Integral
                    {
                        sStaUnitNo = objReader.GetValue("UNIT_NUMBER").ToString();
                        base.ResetSysExData("UnitNo", sStaUnitNo);
                    }
                    else
                    {   // RMA-10039, Generic API processing -  15.2 release included Staging and cyberlife policies / units
                        // The current approach of recording common data in separate POINT_UNIT_DATA and INTEGRAL_UNIT_DATA is obsolete 
                        // and will be corrected when API consumes POINT and INTEGRAL policies

                        sUnitNo = objReader.GetValue("UNIT_NUMBER").ToString();
                        sRiskLoc = objReader.GetValue("UNIT_RISK_LOC").ToString();
                        if (!(sRiskLoc.Equals(string.Empty)))
                            sUnitNo = sUnitNo + "/" + sRiskLoc;

                        sSubLoc = objReader.GetValue("UNIT_RISK_SUB_LOC").ToString();
                        if (!(sSubLoc.Equals(string.Empty)))
                            sUnitNo = sUnitNo + "/" + sSubLoc;

                        base.ResetSysExData("UnitNo", sUnitNo);

                    }
                }

            }


        }
	}
}