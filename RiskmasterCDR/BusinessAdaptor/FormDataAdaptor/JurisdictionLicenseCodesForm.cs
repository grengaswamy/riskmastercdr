using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for Jurisdiction License or Codes Screen.
	/// </summary>
	public class JurisdictionLicenseCodesForm : DataEntryFormBase
	{
		const string CLASS_NAME = "JurisLicenseOrCode";

		private JurisLicenseOrCode objLicenseCodes{get{return objData as JurisLicenseOrCode;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}

		public override void InitNew()
		{
			base.InitNew();
			objLicenseCodes.EntityId = base.GetSysExDataNodeInt("/SysExData/EntityId",true);
			if(objLicenseCodes.EntityId >0)
				(objData as INavigation).Filter = "ENTITY_ID=" + objLicenseCodes.EntityId;

		}

		public JurisdictionLicenseCodesForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}
	}
}
