using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for CmXVocrehabForm.
	/// </summary>
	public class CmXVocrehabForm : DataEntryFormBase
	{
		const string CLASS_NAME = "CmXVocrehab";
		private CmXVocrehab CmXVocrehab{get{return objData as CmXVocrehab;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		const string FILTER_KEY_NAME = "CASEMGT_ROW_ID";

		public override void InitNew()
		{
			base.InitNew(); 
			this.CmXVocrehab.CasemgtRowId = base.GetSysExDataNodeInt("/SysExData/CasemgtRowId");

			(objData as INavigation).Filter = FILTER_KEY_NAME + "=" + this.CmXVocrehab.CasemgtRowId;
		}

		public CmXVocrehabForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
        {
            int iUseClaimProgressNotes = 0;
            base.OnUpdateForm ();
            int iClaimID = 0;


            // PSHEKHAWAT 06-June-2012: MITS 23383, VIEW,CREATE,UPDATE,DELETE permissions not working
            string strClaimIDQuery = "SELECT CLAIM_ID from CASE_MANAGEMENT where CASEMGT_ROW_ID = " + CmXVocrehab.CasemgtRowId;
            iClaimID = CmXVocrehab.Context.DbConn.ExecuteInt(strClaimIDQuery);

            Claim objClaim = (Claim)CmXVocrehab.Context.Factory.GetDataModelObject("Claim", false);
            objClaim.MoveTo(iClaimID);

            switch (objClaim.LineOfBusCode)
            {
                // for WC claims
                case 243:
                    m_SecurityId = RMO_WC_VOCREHAB;
                    break;
                // for Non occupational claims
                case 844:
                    m_SecurityId = RMO_DI_VOCREHAB;
                    break;
            }
            // Check Create new Permissions, Row ID is 0 when new claim is created
            if (CmXVocrehab.CmvrRowId == 0)
            {
                if (!CmXVocrehab.Context.RMUser.IsAllowedEx(m_SecurityId, RMO_CREATE))
                {
                    m_CurrentAction = enumFormActionType.None;
                    LogSecurityError(RMO_CREATE);
                    return;
                }
            }
            // check View permissions
            else
            {
                if (!CmXVocrehab.Context.RMUser.IsAllowedEx(m_SecurityId, RMO_VIEW))
                {
                    m_CurrentAction = enumFormActionType.None;
                    LogSecurityError(RMO_VIEW);
                    return;
                }
            }
            // PSHEKHAWAT : MITS 23383 END
            //mgaba2:mits 29116
            if (iClaimID > 0)
            {
                iUseClaimProgressNotes = objClaim.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode].ClmProgressNotesFlag;

                if (iUseClaimProgressNotes == 0)
                {
                    base.AddKillNode("enhancednotes");
                }
            }
			//for Displaying Back button on Entity Maintenance Screen
			base.ResetSysExData("DisplayBackButton","true");
		}

	}

}
