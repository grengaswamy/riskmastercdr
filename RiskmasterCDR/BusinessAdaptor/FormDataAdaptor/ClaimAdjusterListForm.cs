using System;
using Riskmaster.DataModel;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for ClaimAdjusterList.
	/// </summary>
	public class ClaimAdjusterListForm: ListFormBase
	{
		const string CLASS_NAME = "ClaimAdjusterList";

		public override void Init()
		{
			//If we were sent here from a parent(claim) then we'll have an eventid in the SysEx
			//originally from ScreenFlow\SysExData.
			XmlDocument objXML = base.SysEx;
			XmlNode objClaimId=null;
			try{objClaimId = objXML.SelectSingleNode("/SysExData/ClaimId");}
			catch{};
			
			//Filter by this ClaimId if Present.
			if(objClaimId !=null)
			{
				objDataList.BulkLoadFlag = true;
				objDataList.SQLFilter = "CLAIM_ID=" + objClaimId.InnerText;
			}
			else
				throw new Riskmaster.ExceptionTypes.CollectionException(String.Format(Globalization.GetString("ListForm.MissingExpectedFilterKey",base.ClientId),"ClaimId"));
			OnUpdateForm();
		}


		public ClaimAdjusterListForm(FormDataAdaptor fda):base(fda)
		{
			this.objDataList = fda.Factory.GetDataModelObject(CLASS_NAME,false) as DataCollection;
		}
		private LocalCache objCache{get{return objDataList.Context.LocalCache;}}

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm() //Only called from base classes on Refresh()caused by PostBack.
		{
			
			base.OnUpdateForm ();

			XmlDocument objXML = base.SysEx;
			XmlNode objNew;
			XmlNode objOld ;
			XmlNode objAdjRowId;
			XmlNode objtxttype;

			objAdjRowId=objXML.CreateElement("AdjRowId");
			objOld = objXML.SelectSingleNode("//SysExData/AdjRowId");
			if(objOld==null)
			objXML.DocumentElement.AppendChild(objAdjRowId);

			objtxttype=objXML.CreateElement("TextTypeCode");
			objtxttype.Attributes.Append(objXML.CreateAttribute("codeid"));
			objtxttype.Attributes["codeid"].Value="";
			objOld = objXML.SelectSingleNode("//SysExData/TextTypeCode");
			if(objOld==null)
			objXML.DocumentElement.AppendChild(objtxttype);

			
			objNew = objXML.CreateElement("Screen");
			objNew.Attributes.Append(objXML.CreateAttribute("type"));
			objNew.Attributes["type"].Value="";

			objOld = objXML.SelectSingleNode("//Screen");
			if(objOld !=null)
				;
				//Let it Ride from the Client... 
				//Will be acted on in the Form XSL
				//No Need to clear since next page request should hit a new
				//form altogether anyhow.
				//objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);


			//			//Clear any existing tablename nodes.
			//			foreach(XmlNode nd in objXML.SelectNodes("//EntityTableName"))
			//				nd.ParentNode.RemoveChild(nd);
			//
			//			//*********************
			//			// Parallel Array to the PIList to be serialized...
			//			//*********************
			//			foreach(PersonInvolved objPI in objDataList)
			//			{
			//				//Entity Table Name - Used as fall-back for Blank Entity Name.
			//				string sTableName = objCache.GetUserTableName(objPI.PiEntity.EntityTableId);
			//				objNew = objXML.CreateElement("EntityTableName");
			//				objNew.AppendChild(objXML.CreateCDataSection(sTableName));
			//				objXML.DocumentElement.AppendChild(objNew);
			//			}
		}
	}
}
