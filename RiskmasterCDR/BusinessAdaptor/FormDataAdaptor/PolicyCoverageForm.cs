﻿/**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            *
 **********************************************************************************************
 * 05/22/2014 |  33573  | dagrawal   | SRe Gap34B - Claims Made Handling
 * 06/02/2014 |  34278   | abhadouria  | SRe Gap 20 - Add  Reinsurance indicator
 **********************************************************************************************/
using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

//TODO: Auto-Generated Form - migrate and verify behaviors from Legacy RMNet form.

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for PolicyCoverage Screen.
	/// </summary>
	public class PolicyCoverageForm : DataEntryFormBase
	{
		const string CLASS_NAME = "PolicyXCvgType";
		private PolicyXCvgType PolicyXCvgType{get{return objData as PolicyXCvgType;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		const string FILTER_KEY_NAME = "PolicyId";

		/// <summary>
		/// Class constructor to initialize Class Name information
		/// </summary>
		/// <param name="fda"></param>
		public PolicyCoverageForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

		/// <summary>
		/// Initializes the class and sets the parent ID for the class
		/// that can be used for parent-child navigation
		/// </summary>
		public override void InitNew()
		{
			base.InitNew();

			//If we were sent here from a parent(event) then we'll have an eventid in the SysEx
			//originally from ScreenFlow\SysExData.
            //XmlNode objPolicyId=null;
            
            //try
            //{
            //    objPolicyId = base.SysEx.GetElementsByTagName(FILTER_KEY_NAME)[0];
            //}//end try
            //catch
            //{
            //}//end catch
            //if(objPolicyId !=null)
            //    this.m_ParentId = Conversion.ConvertObjToInt(objPolicyId.InnerText, base.ClientId);

            //PolicyXCvgType.PolicyId = this.m_ParentId;

            ////Filter by this PhysEid if Present.
            //if(this.m_ParentId != 0)
            //{
            //    (objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + this.m_ParentId;
            //}
            XmlNode objId = null;
            if (objData.Context.InternalSettings.SysSettings.MultiCovgPerClm == -1)
            {
                objId = base.SysEx.GetElementsByTagName("PolicyUnitRowId")[0];
                if (objId != null)
                {
                    try
                    {
                        this.m_ParentId = Conversion.ConvertObjToInt(objId.InnerText.Split("_".ToCharArray()[0])[2], base.ClientId);
                    }
                    catch (Exception e)
                    {
                        this.m_ParentId = Conversion.ConvertObjToInt(objId.InnerText, base.ClientId);
                    }
                }

            }
            else
            {
                objId = base.SysEx.GetElementsByTagName(FILTER_KEY_NAME)[0];
                if (objId != null)
                    this.m_ParentId = Conversion.ConvertObjToInt(objId.InnerText, base.ClientId);
            }
            if (objData.Context.InternalSettings.SysSettings.MultiCovgPerClm == -1)
            {
                PolicyXCvgType.PolicyUnitRowId = this.m_ParentId;
            }
            else
            {
                PolicyXCvgType.PolicyId = this.m_ParentId;
            }
            if (this.m_ParentId != 0)
            {
                if (objData.Context.InternalSettings.SysSettings.MultiCovgPerClm == -1)
                {
                    (objData as INavigation).Filter = objData.PropertyNameToField("PolicyUnitRowId") + "=" + this.m_ParentId;
                }
                else
                {
                    (objData as INavigation).Filter = objData.PropertyNameToField("PolicyId") + "=" + this.m_ParentId;
                }
                
            }
			
		}//end method InitNew()

	
		//Handle Extended Screen Elements not directly applicable to database fields\datamodel pro  perties.
		public override void OnUpdateForm()
        {
            //ArrayList singleRow = null;
            //base.OnUpdateForm ();
            //string sPolicyLOB = base.SysEx.SelectSingleNode("/SysExData/hdnPolicyLOB").InnerText.Trim();
            ArrayList singleRow = null;
            base.OnUpdateForm();
            DbReader objReader = null;
            int iPolicyLOB = 0;
            string sSQL = string.Empty;
            string sBasePolicyLOB = string.Empty;
            
            //PJS MITS # 16563 - Adding Lookupdata attributes for policynumberlookup
            if (PolicyXCvgType.Context.InternalSettings.SysSettings.UseEnhPolFlag != -1)
            {
                singleRow = new ArrayList();
                base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.PolicyNumberLookup.ToString());
                base.AddElementToList(ref singleRow, "id", "nextpolicyid");
                base.AddElementToList(ref singleRow, "idref", "/Instance/PolicyXCvgType/NextPolicyId");
                base.AddElementToList(ref singleRow, "ref", "/Instance/PolicyXCvgType/NextPolicyId/@defaultdetail");
                base.AddElementToList(ref singleRow, "type", "policynumberlookup");
                string sParameters = "return lookupData('NextPolicyId','policy',-1,'NextPolicyId',9)";
                base.AddElementToList(ref singleRow, "onclientclickparameters", sParameters);
            }
            base.m_ModifiedControls.Add(singleRow);

            if (objData.Context.InternalSettings.SysSettings.MultiCovgPerClm == -1)
            {
                XmlNode xnode = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysFormPIdName");
                if (xnode != null)
                {
                    xnode.InnerText = "PolicyUnitRowId";
                }
                //need to remove this and currect the lookup functionality for coverage lookup
                //in case the units are being used in policies.//skhare7 changed for policy interface
               // base.AddKillNode("lookup");

                sSQL = "SELECT POLICY_LOB_CODE FROM POLICY, POLICY_X_UNIT WHERE POLICY_UNIT_ROW_ID=" + PolicyXCvgType.PolicyUnitRowId + " AND POLICY.POLICY_ID = POLICY_X_UNIT.POLICY_ID";
                using (objReader = PolicyXCvgType.Context.DbConnLookup.ExecuteReader(sSQL))
                {

                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            iPolicyLOB = Conversion.ConvertObjToInt(objReader.GetValue("POLICY_LOB_CODE").ToString(), base.ClientId);
                        }
                    }
                }
                //string[] sCollection = sPolicyLOB.Split(' ');
                sBasePolicyLOB = objCache.GetRelatedShortCode(iPolicyLOB);
                if (sBasePolicyLOB != "PL")
                {
                    base.AddKillNode("limitcovA");
                    base.AddKillNode("limitcovB");
                    base.AddKillNode("limitcovC");
                    base.AddKillNode("limitcovD");
                    base.AddKillNode("limitcovE");
                    base.AddKillNode("limitcovF");
                }
                if (sBasePolicyLOB != "WL")
                {
                    base.AddKillNode("WcDedAmt");
                    base.AddKillNode("WcDedAggr");
                }
                //MITS 34278 START
                base.AddDisplayNode("Reinsurance");
                //MITS 34278 END
                //Ashish Ahuja: Claims Made Jira 1342
                base.AddDisplayNode("ClaimsMadeIndicator");
            }
            else
            {
                XmlNode xnode = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysFormPIdName");
                if (xnode != null)
                {
                    xnode.InnerText = "policyid";
                }
                //MITS 34278 START
                base.AddKillNode("Reinsurance");
                //MITS 34278 END 
                //Ashish Ahuja: Claims Made Jira 1342
                base.AddDisplayNode("ClaimsMadeIndicator");
            }
           // base.ResetSysExData("PolicyUnitRowId", this.m_ParentId.ToString());//MGaba2:MITS 15642

            //akaur9 MITS 25163- Policy Interface Implementation --start
           
            int iSystemPolicyId = base.GetSysExDataNodeInt("//hdnPolicySystemId");
            if (iSystemPolicyId > 0)
            {
                base.AddReadOnlyNode("readOnlyPage");
                base.AddKillNode("new");
                base.AddKillNode("save");
                base.AddKillNode("delete");
                //MITS:33573 starts
                base.AddDisplayNode("RetroDate");
                base.AddDisplayNode("TailDate");
                //MITS:33573 ends
            
                //Start:commented by Nitin goel, MITS 37653,06/14/2014, Class code is needed for GC policies also.
                //// add coverage class code field only for external WL policy                
                //if (iPolicyLOB > 0 && sBasePolicyLOB.Trim() != "WL")
                //{
                //    base.AddKillNode("CvgClassCode");
                //}
                //end: added by Nitin goel
            }
            else
            {
                base.AddKillNode("btnAdditionalDetailsCoverage");   //Policy Inetrface Implementation aaggarwal29: Additional detail btn should be visible for external policies only 
                base.AddKillNode("perpersonlimit");
                base.AddKillNode("policysystemdata");
                //MITS:33573 starts
                base.AddKillNode("RetroDate");
                base.AddKillNode("TailDate");
                //MITS:33573 ends
				
                //tanwar2 - mits 30910 - start
                base.AddReadOnlyNode("deductibletype");
                //tanwar2 - mits 30910 - end
            }
            //akaur9 MITS 25163- Policy Interface Implementation --End

        

		}
	}
}
