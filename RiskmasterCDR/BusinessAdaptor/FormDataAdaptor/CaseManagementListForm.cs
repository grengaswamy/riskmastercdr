﻿using System;
using Riskmaster.DataModel;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;


namespace Riskmaster.BusinessAdaptor
{
    class CaseManagementListForm:ListFormBase
    {
        const string CLASS_NAME = "CaseManagementList";

		public override void Init()
		{
			//If we were sent here from a parent(event) then we'll have an eventid in the SysEx
			//originally from ScreenFlow\SysExData.
			XmlDocument objXML = base.SysEx;
			XmlNode objPiRowId=null;
			try{objPiRowId = objXML.SelectSingleNode("/SysExData/PiRowId");}
			catch{};

            //Filter by this PI_ROW_ID if Present.
			if(objPiRowId !=null)
			{
				objDataList.BulkLoadFlag = true;
				objDataList.SQLFilter = "PI_ROW_ID =" + objPiRowId.InnerText;
			}
			else
				throw new Riskmaster.ExceptionTypes.CollectionException(String.Format(Globalization.GetString("ListForm.MissingExpectedFilterKey",base.ClientId),"PiRowId"));
			OnUpdateForm();
		}


        public CaseManagementListForm(FormDataAdaptor fda): base(fda)
		{
			this.objDataList = fda.Factory.GetDataModelObject(CLASS_NAME,false) as DataCollection;
		}
		private LocalCache objCache{get{return objDataList.Context.LocalCache;}}

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm() //Only called from base classes on Refresh()caused by PostBack.
		{
			
			base.OnUpdateForm ();
			base.CreateSysExData("PiEid","");


			XmlDocument objXML = base.SysEx;
			XmlNode objNew;
			XmlNode objOld ;
			
			objNew = objXML.CreateElement("Screen");
			objNew.Attributes.Append(objXML.CreateAttribute("type"));
			objNew.Attributes["type"].Value="";

			objOld = objXML.SelectSingleNode("//Screen");
			if(objOld !=null)
				;
				//Let it Ride from the Client... 
				//Will be acted on in the Form XSL
				//No Need to clear since next page request should hit a new
				//form altogether anyhow.
				//objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);


			//Clear any existing tablename nodes.
			foreach(XmlNode nd in objXML.SelectNodes("//EntityTableName"))
				nd.ParentNode.RemoveChild(nd);
		}
    }
}
