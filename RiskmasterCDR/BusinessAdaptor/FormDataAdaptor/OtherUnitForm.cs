﻿using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;


namespace Riskmaster.BusinessAdaptor
{
    class OtherUnitForm : DataEntryFormBase
    {
        const string CLASS_NAME = "OtherUnit";
        const string FILTER_KEY_NAME = "OtherUnitId";

        private OtherUnit OtherUnit { get { return objData as OtherUnit; } }

        private LocalCache objCache { get { return objData.Context.LocalCache; } }

        public OtherUnitForm(FormDataAdaptor fda)
            : base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

        public override void InitNew()
        {
            base.InitNew();

            //set filter for UnitId            
            OtherUnit.OtherUnitId = base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME);
            if (OtherUnit.OtherUnitId > 0)
                (objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + OtherUnit.OtherUnitId;
        }

        private void ApplyFormTitle()
        {
            string sSubTitleText = string.Empty;
            ArrayList singleRow = null;
            if (OtherUnit != null)
            {
                sSubTitleText += " [ " + OtherUnit.OtherUnitId + " ]";
            }

            //Pass this title value to view (ref'ed from @valuepath).
            base.ResetSysExData("SubTitle", sSubTitleText);

            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singleRow, "id", "formsubtitle");
            base.AddElementToList(ref singleRow, "Text", sSubTitleText);
            base.m_ModifiedControls.Add(singleRow);
        }

        public override void OnUpdateForm()
        {
            base.OnUpdateForm();
            ApplyFormTitle();
            if (!OtherUnit.Context.RMUser.IsAllowedEx(FormBase.RMO_SEARCH, FormBase.RMO_PROPERTY_SEARCH))
                base.AddKillNode("search");
            if (!OtherUnit.Context.RMUser.IsAllowedEx(FormBase.RMO_LOOKUP, FormBase.RMO_PROPERTY_SEARCH))
                base.AddKillNode("lookup");

            
            bool bUseAcrosoftInterface = OtherUnit.Context.InternalSettings.SysSettings.UseAcrosoftInterface;
            bool bUsePaperVision = OtherUnit.Context.InternalSettings.SysSettings.UsePaperVisionInterface; //Mona:PaperVisionMerge : Animesh inserted MITS 16697

            if (bUseAcrosoftInterface || bUsePaperVision || OtherUnit.Context.InternalSettings.SysSettings.UseMediaViewInterface)
            {
                base.AddKillNode("attachdocuments");//Mona:PaperVisionMerge:Animesh Inserted MITS 16697
                base.AddKillNode("attach");
            }
            //akaur9 MITS 25163- Policy Interface Implementation --start

            int iSystemPolicyId = base.GetSysExDataNodeInt("//SystemPolicyId");

            if (iSystemPolicyId > 0)
            {
                base.AddReadOnlyNode("readOnlyPage");
                base.AddKillNode("save");
                base.AddKillNode("delete");
            }
            //akaur9 MITS 25163- Policy Interface Implementation --End
        }       

       
    }
}
