using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using Riskmaster.Settings;
using Riskmaster.Application.LeaveLibrary;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
    /// Summary description for Leave Screen
	/// </summary>
	public class LeaveForm : DataEntryFormBase
	{
		const string CLASS_NAME = "Leave";	
		const string FILTER_KEY_NAME  = "ClaimId";
		private Leave Leave{get{return objData as Leave;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		const string INSTANCE_SYSEXDATA_PATH = "/Instance/UI/FormVariables/SysExData";
        string sUpdateCmd = string.Empty;
        string sAvailHour = string.Empty;
	
		public LeaveForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

		public override void InitNew()
		{
            base.InitNew();

            //Set Filter For ClaimId        
            
            Leave.ClaimId = base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME, false);
                if (Leave.ClaimId > 0)
                    (objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + Leave.ClaimId;            

            if (SysEx.DocumentElement.SelectSingleNode("IsUpdate") == null)
                CreateSysExData("IsUpdate");           
         
			if (SysEx.DocumentElement.SelectSingleNode("UseLeavesFlag") == null)
				CreateSysExData("UseLeavesFlag");
            if (SysEx.DocumentElement.SelectSingleNode("ChildAge") == null)
                CreateSysExData("ChildAge");
            //Geeta 03/12/07 : Modified for saving leave detail grid records after deletion
            if (SysEx.DocumentElement.SelectSingleNode("LeaveGridRowDeleteFlag") == null)
                CreateSysExData("LeaveGridRowDeleteFlag");
        }

        private void ApplyFormTitle()
        {
            string sCaption = string.Empty;
            sCaption = " [" + base.GetSysExDataNodeText("LeavePlanCode") + "]";
            ArrayList singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singleRow, "id", "formsubtitle");
            base.AddElementToList(ref singleRow, "Text", sCaption);
            base.m_ModifiedControls.Add(singleRow);
        }

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
                       
            string sLeavePlanName = string.Empty;
            string sLeavePlanCode = string.Empty;	
	        string sEligibilityCode = string.Empty;
            string sEligibilityCodeDes = string.Empty;
            //string sName = string.Empty;
            string sTitle = string.Empty;
            string sSysCmd =string.Empty;
            string sDateEligibility = string.Empty;   //Umesh
            string sChildDOB = string.Empty;//Umesh
            string sAge = string.Empty;//Umesh
			int iUseLeavesFlag = 0;

            base.OnUpdateForm();

            if (base.SysEx.SelectSingleNode("/SysExData/PiRowId") == null)
            {
                Claim objClaim = (Claim)Leave.Context.Factory.GetDataModelObject("Claim", false);               
                objClaim.MoveTo(Leave.ClaimId);

                if (objClaim.EventId != 0 && objClaim.PrimaryClaimant != null)
                {                
                    string sSQL = "SELECT PI_ROW_ID FROM PERSON_INVOLVED WHERE EVENT_ID =" + objClaim.EventId;

                    using (DbReader objReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, sSQL))
                    {
                        if (objReader.Read())
                        {
                            base.ResetSysExData("PiRowId", Conversion.ConvertObjToStr(objReader.GetValue("PI_ROW_ID")));
                        }
                    }
                }
                else
                {
                    base.ResetSysExData("PiRowId", "0");
                }
                objClaim.Dispose();

            }            
            
            sSysCmd = base.m_fda.SafeFormVariableParamText("SysCmd");

            DataModel.LeavePlan parentLeavePlan = this.Leave.Parent as DataModel.LeavePlan;

            if (Leave.LeaveRowId > 0 && !(Leave.IsNew))
            {
                sLeavePlanName = parentLeavePlan.LeavePlanName;
                sLeavePlanCode = parentLeavePlan.LeavePlanCode;
                iUseLeavesFlag = Conversion.ConvertBoolToInt(parentLeavePlan.UseLeavesFlag, base.ClientId);
                if (sSysCmd != "7")//abansal23: MITS 15896,15898 on 04/29/2009
                base.ResetSysExData("DateEligibility", this.Leave.DateEligibility);
            }
            else
            {
                if (SysEx.DocumentElement.SelectSingleNode("DateOfClaim") != null)
				{
                    base.ResetSysExData("DateEligibility", SysEx.DocumentElement.SelectSingleNode("DateOfClaim").InnerText);                
                }
            }
            //Geeta 01/29/07 FMLA Ends
            //abansal23 : MITS 15896,15898 on 04/29/2009 Starts
            if ((Leave.LeaveRowId > 0) && (sUpdateCmd == "1" || sSysCmd  == "7"))
            {
                
                base.ResetSysExData("LeavePlanName", sLeavePlanName);
                base.ResetSysExData("LeavePlanCode", sLeavePlanCode);
            }
            else 
            {
                base.ResetSysExData("LeavePlanName", parentLeavePlan.LeavePlanName);
                base.ResetSysExData("LeavePlanCode", parentLeavePlan.LeavePlanCode);
            }
            //abansal23 : MITS 15896,15898 on 04/29/2009 Ends
			base.ResetSysExData("UseLeavesFlag",Conversion.ConvertObjToStr(iUseLeavesFlag));					
            
            //Get the data for the Leave Detail Grid 
			XmlDocument objLeaveDetailXmlDoc = GetLeaveDetailData();
			
			XmlNode objOldLeaveDetailNode = SysEx.DocumentElement.SelectSingleNode(objLeaveDetailXmlDoc.DocumentElement.LocalName);
			if(objOldLeaveDetailNode!=null)
			{
				SysEx.DocumentElement.RemoveChild(objOldLeaveDetailNode);  
			}
			XmlNode objLeaveDetailNode = SysEx.ImportNode(objLeaveDetailXmlDoc.DocumentElement ,true);
			SysEx.DocumentElement.AppendChild(objLeaveDetailNode);  	

			if(SysEx.DocumentElement.SelectSingleNode("IsPostBack") == null)
				CreateSysExData("IsPostBack");

            //Geeta 03/12/07 : Modified for saving leave detail grid records after deletion
            if (SysEx.DocumentElement.SelectSingleNode("LeaveDetailGrid_RowDeletedFlag") != null)
            {                
                string sLeaveDetailRowDeletedFlag =
                    base.GetSysExDataNodeText("LeaveDetailGrid_RowDeletedFlag", true);
                
                if (sLeaveDetailRowDeletedFlag == "true")
                {
                    base.ResetSysExData("LeaveGridRowDeleteFlag", "-1");
                }
                else
                {
                    base.ResetSysExData("LeaveGridRowDeleteFlag", "0");
                }
            }

			base.ResetSysExData("LeaveDetailSelectedId",""); 
			base.ResetSysExData("LeaveDetailGrid_RowDeletedFlag","false"); 
			base.ResetSysExData("LeaveDetailGrid_RowAddedFlag", "false");            

            //Get the Title for the Leave Form
            //mona-start
           
            //            sTitle = this.SysView.SelectSingleNode("//form/@title").InnerText;            

  //          if (sName != "" && sTitle == "Leave Management")
//                this.SysView.SelectSingleNode("//form/@title").InnerText = this.SysView.SelectSingleNode("//form/@title").InnerText + " [ " + sName + " ]";                                                              
            //called this function to set the title 
            ApplyFormTitle();
            //mona-end
            //abansal23 : MITS 15896,15898 on 04/29/2009 Starts
            if (sSysCmd != "7" && sSysCmd != "5")
            {
                base.ResetSysExData("DateTFStart", this.Leave.DateTFStart);
                base.ResetSysExData("DateTFEnd", this.Leave.DateTFEnd);
                //03/22/07 : Modified for Mits number 9009
                base.ResetSysExData("HoursAvail", this.Leave.HoursAvail.ToString("#0.00"));
                base.ResetSysExData("HoursUsed", this.Leave.HoursUsed.ToString("#0.00"));
                base.ResetSysExData("MaxDurHours", this.Leave.MaxDurHours.ToString("#0.00"));
                //CreateSysExData("DateTFStart");
                //CreateSysExData("DateTFEnd");
                //CreateSysExData("HoursAvail");
                //CreateSysExData("HoursUsed");
                //CreateSysExData("MaxDurHours");

                this.objCache.GetCodeInfo(this.Leave.EligStatusCode, ref sEligibilityCode, ref sEligibilityCodeDes); //no need to put the language code here as LEAVE_ELIG_STATUS is the system table

                //    //Geeta 03/12/07 : Modified for displaying description for Eliginility Status field
                base.ResetSysExData("EligStatusCode", sEligibilityCodeDes);
                //   CreateSysExData("EligStatusCode");
            }
            //abansal23 : MITS 15896,15898 on 04/29/2009 Ends
            //To get the age of child
            sDateEligibility = this.Leave.DateEligibility;
            sChildDOB = this.Leave.ChildDOB;
            if (sDateEligibility != "")
                sAge = CalculateAge(sChildDOB, sDateEligibility);
            else
                sAge = CalculateAge(sChildDOB, Conversion.ToDbDate(DateTime.Today));
            base.ResetSysExData("ChildAge", sAge);
		}
			

		private XmlDocument GetLeaveDetailData()
		{
			XmlDocument objLeaveDetailXmlDoc = new XmlDocument();
 
			XmlElement objRootElement = objLeaveDetailXmlDoc.CreateElement("LeaveDetail"); 
			objLeaveDetailXmlDoc.AppendChild(objRootElement);
			
			XmlElement objOptionXmlElement = null;
			XmlElement objXmlElement = null;
			XmlElement objListHeadXmlElement = null;
			XmlAttribute objXmlAttribute = null;
			int iLeaveDetailCount = 0;
			string sLeaveCode = string.Empty;
			string sLeaveDesc = string.Empty;			 

			objListHeadXmlElement = objLeaveDetailXmlDoc.CreateElement("listhead"); 
					
			objXmlElement = objLeaveDetailXmlDoc.CreateElement("DateLeaveStart");
			objXmlElement.InnerText = "Start Date";
			objListHeadXmlElement.AppendChild(objXmlElement); 					
			objXmlElement = null;

			objXmlElement = objLeaveDetailXmlDoc.CreateElement("DateLeaveEnd");
			objXmlElement.InnerText = "End Date";
			objListHeadXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objXmlElement = objLeaveDetailXmlDoc.CreateElement("LeaveTypeCode");
			objXmlElement.InnerText = "Leave Type";
			objListHeadXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objXmlElement = objLeaveDetailXmlDoc.CreateElement("HoursLeave");
			objXmlElement.InnerText = "Hours";
			objListHeadXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objXmlElement = null;
			objRootElement.AppendChild(objListHeadXmlElement);
			objListHeadXmlElement = null;

			foreach(LeaveDetail objLeaveDetail in this.Leave.LeaveDetailList)
			{
				
				iLeaveDetailCount++;				

				objOptionXmlElement = objLeaveDetailXmlDoc.CreateElement("option"); 
					
				objXmlAttribute = objLeaveDetailXmlDoc.CreateAttribute("ref");  
				objXmlAttribute.InnerText =		INSTANCE_SYSEXDATA_PATH 
					+ "/" + objLeaveDetailXmlDoc.DocumentElement.LocalName  
					+ "/option[" + iLeaveDetailCount.ToString() + "]" ;
				objOptionXmlElement.Attributes.Append(objXmlAttribute);
				objXmlAttribute = null;

				objXmlElement = objLeaveDetailXmlDoc.CreateElement("DateLeaveStart");
                objXmlElement.InnerText = Conversion.GetDBDateFormat(objLeaveDetail.DateLeaveStart, "MM/dd/yyyy");													 

				objOptionXmlElement.AppendChild(objXmlElement); 					
				objXmlElement = null;

				objXmlElement = objLeaveDetailXmlDoc.CreateElement("DateLeaveEnd");
                objXmlElement.InnerText = Conversion.GetDBDateFormat(objLeaveDetail.DateLeaveEnd, "MM/dd/yyyy");				
				
                objOptionXmlElement.AppendChild(objXmlElement); 
				objXmlElement = null;				
				
				objXmlElement = objLeaveDetailXmlDoc.CreateElement("LeaveTypeCode");
                this.objCache.GetCodeInfo(objLeaveDetail.LeaveTypeCode, ref sLeaveCode, ref sLeaveDesc,base.Adaptor.userLogin.objUser.NlsCode);  //Aman ML Change
                objXmlElement.InnerText = sLeaveDesc;
				objXmlAttribute = objLeaveDetailXmlDoc.CreateAttribute("codeid");  
				objXmlAttribute.InnerText =	objLeaveDetail.LeaveTypeCode.ToString();	
				objXmlElement.Attributes.Append(objXmlAttribute);					
				objOptionXmlElement.AppendChild(objXmlElement); 
				objXmlAttribute = null;
				objXmlElement = null;

				objXmlElement = objLeaveDetailXmlDoc.CreateElement("HoursLeave");
				objXmlElement.InnerText = objLeaveDetail.HoursLeave.ToString();
				objOptionXmlElement.AppendChild(objXmlElement); 
				objXmlElement = null;				

				/************** Hidden Columns on the Grid ******************/

				objXmlElement = objLeaveDetailXmlDoc.CreateElement("LDRowId");
				objXmlElement.InnerText = objLeaveDetail.LDRowId.ToString();
				objOptionXmlElement.AppendChild(objXmlElement); 
				objXmlElement = null;
				
				objXmlElement = objLeaveDetailXmlDoc.CreateElement("LeaveRowId");
				objXmlElement.InnerText = objLeaveDetail.LeaveRowId.ToString();
				objOptionXmlElement.AppendChild(objXmlElement); 
				objXmlElement = null;

				objRootElement.AppendChild(objOptionXmlElement);
				objOptionXmlElement = null;
			}
			
			iLeaveDetailCount++;

			objOptionXmlElement = objLeaveDetailXmlDoc.CreateElement("option"); 
					
			objXmlAttribute = objLeaveDetailXmlDoc.CreateAttribute("ref");  			            
            objXmlAttribute.InnerText =		INSTANCE_SYSEXDATA_PATH 
				+ "/" + objLeaveDetailXmlDoc.DocumentElement.LocalName  
				+ "/option[" + iLeaveDetailCount.ToString() + "]" ;
			objOptionXmlElement.Attributes.Append(objXmlAttribute);
			objXmlAttribute = null;

			objXmlAttribute = objLeaveDetailXmlDoc.CreateAttribute("type"); 
			objXmlAttribute.InnerText = "new";
			objOptionXmlElement.Attributes.Append(objXmlAttribute);
			objXmlAttribute = null;

			objXmlElement = objLeaveDetailXmlDoc.CreateElement("DateLeaveStart");
			objOptionXmlElement.AppendChild(objXmlElement); 					
			objXmlElement = null;

			objXmlElement = objLeaveDetailXmlDoc.CreateElement("DateLeaveEnd");
			objOptionXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objXmlElement = objLeaveDetailXmlDoc.CreateElement("LeaveTypeCode");
			objXmlAttribute = objLeaveDetailXmlDoc.CreateAttribute("codeid");  
			objXmlAttribute.InnerText =	"-1";
			objXmlElement.Attributes.Append(objXmlAttribute);					
			objOptionXmlElement.AppendChild(objXmlElement); 
			objXmlAttribute = null;
			objXmlElement = null;

			objXmlElement = objLeaveDetailXmlDoc.CreateElement("HoursLeave");
			objOptionXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;			

			/************** Hidden Columns on the Grid ******************/

			objXmlElement = objLeaveDetailXmlDoc.CreateElement("LDRowId");
			objOptionXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objXmlElement = objLeaveDetailXmlDoc.CreateElement("LeaveRowId");
			objOptionXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objRootElement.AppendChild(objOptionXmlElement);
			objOptionXmlElement = null;

			objRootElement = null;			
			return objLeaveDetailXmlDoc;	
		}

        private string CalculateAge(string sDate1, string sDate2)
        {   TimeSpan ts;
            int iAge = 0;
            if (sDate1 != "" && sDate2 != "")
            {
                DateTime Date1 = Convert.ToDateTime(Conversion.GetDBDateFormat(sDate1, "yyyy/MM/dd"));
                DateTime Date2 = Convert.ToDateTime(Conversion.GetDBDateFormat(sDate2, "yyyy/MM/dd"));
                ts = Date2 - Date1;
                iAge = Convert.ToInt32(Math.Floor((ts.Days)/365.25));
                if (iAge > 32000) 
                    iAge = 0;
                if (iAge < 0)
                    iAge = 0;
               
            }
           
            return iAge.ToString();

        }
		//This method will be invoked both on Post-Back and on Save.
		public override void OnUpdateObject()
		{
			bool bIsNew = false; 
			XmlAttribute objTypeAttributeNode = null;
			base.OnUpdateObject ();

			#region "Updating Leave Detail Object"
			SortedList objLeaveDetailList = new SortedList(); 

			XmlNode objLeaveDetailNode = base.SysEx.DocumentElement.SelectSingleNode("LeaveDetail");  
			objTypeAttributeNode = null;

			bIsNew = false;
			int iLDRowIdKey = -1;

			XmlDocument objLeaveDetailXmlDoc = null;
			XmlElement objLeaveDetailRootElement = null;
			
			int iLeaveDetailSelectedId = 
				base.GetSysExDataNodeInt("LeaveDetailSelectedId",true); 

			string sLeaveDetailRowAddedFlag =  
				base.GetSysExDataNodeText("LeaveDetailGrid_RowAddedFlag",true); 
			bool bLeaveDetailRowAddedFlag = 
				sLeaveDetailRowAddedFlag.ToUpper().CompareTo("TRUE")==0?true:false;  

			string sLeaveDetailRowDeletedFlag =  
				base.GetSysExDataNodeText("LeaveDetailGrid_RowDeletedFlag",true); 
			bool bLeaveDetailRowDeletedFlag = 
				sLeaveDetailRowDeletedFlag.ToUpper().CompareTo("TRUE")==0?true:false;  

			if(objLeaveDetailNode!=null)
			{
				// Loop through data for all rows of the grid
				foreach(XmlNode objOptionNode in objLeaveDetailNode.SelectNodes("option"))
				{
					objTypeAttributeNode = objOptionNode.Attributes["type"];
					bIsNew = false;
					if(objTypeAttributeNode!=null && objTypeAttributeNode.InnerText.Equals("new")==true)
					{
						// This is the 'extra' hidden <option> for capturing a new grid row data
						bIsNew = true;
					}					
					if((bIsNew==false)||(bIsNew==true&&bLeaveDetailRowAddedFlag==true))
					{
						objLeaveDetailXmlDoc = new XmlDocument();
						objLeaveDetailRootElement = objLeaveDetailXmlDoc.CreateElement("LeaveDetail");
						objLeaveDetailXmlDoc.AppendChild(objLeaveDetailRootElement);
						objLeaveDetailRootElement.InnerXml = objOptionNode.InnerXml;

						iLDRowIdKey = 
							Conversion.ConvertStrToInteger(objLeaveDetailRootElement.SelectSingleNode("LDRowId").InnerText); 

                        if ((bIsNew == true && bLeaveDetailRowAddedFlag == true) ||
                            (iLDRowIdKey < 0 && objLeaveDetailRootElement.SelectSingleNode("LeaveRowId").InnerText == ""))
							objLeaveDetailRootElement.SelectSingleNode("LeaveRowId").InnerText = "0";

						if(bIsNew==false&&bLeaveDetailRowDeletedFlag==true&&iLDRowIdKey==iLeaveDetailSelectedId)
						{							
							continue;
						}
						
                        XmlElement objElement = (XmlElement)objLeaveDetailXmlDoc.SelectSingleNode("//DateLeaveStart");
                        if (!string.IsNullOrEmpty(objElement.InnerText))
                        {
                            objElement.InnerText = Convert.ToDateTime(objElement.InnerText).ToString("yyyyMMdd");
                        }

                        objElement = (XmlElement)objLeaveDetailXmlDoc.SelectSingleNode("//DateLeaveEnd");
                        if (!string.IsNullOrEmpty(objElement.InnerText))
                        {
                            objElement.InnerText = Convert.ToDateTime(objElement.InnerText).ToString("yyyyMMdd");
                        }
						
						objLeaveDetailList.Add(iLDRowIdKey,objLeaveDetailXmlDoc);   
					}
				}
			}

			foreach(LeaveDetail objLeaveDetail in Leave.LeaveDetailList)
			{
				if(base.m_fda.SafeFormVariableParamText("SysCmd")=="7" && 
					(objLeaveDetail.LDRowId <0 || (bLeaveDetailRowDeletedFlag==true&&objLeaveDetail.LDRowId==iLeaveDetailSelectedId)))
                {
					Leave.LeaveDetailList.Remove(objLeaveDetail.LDRowId);
                    //Geeta 03/12/07 : Modified for saving leave detail grid records after deletion
                    this.Leave.LeaveDetailList.Save();                    
                }

				else if (objLeaveDetail.LDRowId<0)
					Leave.LeaveDetailList.Remove(objLeaveDetail.LDRowId);				 
			}

			LeaveDetail objTmpLeaveDetail = null;
			XmlDocument objTmpLeaveDetailXMLDoc = null;

			for(int iListIndex=0; iListIndex<objLeaveDetailList.Count; iListIndex++)
			{
				objTmpLeaveDetailXMLDoc = (XmlDocument) objLeaveDetailList.GetByIndex(iListIndex);

				if(objTmpLeaveDetailXMLDoc.DocumentElement.SelectSingleNode("LDRowId").InnerText.Trim() == "" || Conversion.ConvertStrToInteger(objTmpLeaveDetailXMLDoc.DocumentElement.SelectSingleNode("LDRowId").InnerText)< 0 )
				{
					objTmpLeaveDetailXMLDoc.DocumentElement.SelectSingleNode("LDRowId").InnerText = ((-1 * iListIndex)-1).ToString(); 
					objTmpLeaveDetail = Leave.LeaveDetailList.AddNew();
				}
				else
					objTmpLeaveDetail=Leave.LeaveDetailList[Conversion.ConvertStrToInteger(objTmpLeaveDetailXMLDoc.DocumentElement.SelectSingleNode("LDRowId").InnerText)];
				objTmpLeaveDetail.PopulateObject(objTmpLeaveDetailXMLDoc); 
			}

			#endregion         

            #region "Updating Leave Eligibility Data"

            string sSysCmd = base.m_fda.SafeFormVariableParamText("SysCmd");
			string sPropertyStore = string.Empty;
			string sEligStatus = string.Empty;
			string sEligStatusDesc = string.Empty;
			string sTemp = string.Empty;
			string sEligMessage = string.Empty;

			int iLPRowId = 0;
			int iPiRowId = 0;
			int iPiEId = 0;
			double dHoursWorked = 0.0;
			bool bReturn = false;
			sUpdateCmd = base.GetSysExDataNodeText("IsUpdate");
			string sUseLeavesFlag = base.GetSysExDataNodeText("UseLeavesFlag");
			string sDateEligibility = Conversion.GetDate(base.GetSysExDataNodeText("DateEligibility"));
            UpdateLeaveHistory objUpdtLeaveHist = new UpdateLeaveHistory(base.m_fda.connectionString, base.ClientId);
            Generic objGeneric = new Generic(base.m_fda.connectionString, base.ClientId);
            LeaveElig objLeaveElig = new LeaveElig(base.m_fda.connectionString, base.ClientId);

			if(base.m_fda.HasParam("SysPropertyStore"))
				sPropertyStore = base.m_fda.SafeParam("SysPropertyStore").InnerXml; 
			
			XmlDocument propertyStore = new XmlDocument();
			propertyStore.LoadXml(sPropertyStore);
			
			XmlElement objLPRowId = propertyStore.SelectSingleNode("//LPRowId") as XmlElement;
			if(objLPRowId != null)
				iLPRowId = Conversion.ConvertStrToInteger(objLPRowId.InnerText);
			
			XmlElement objHoursWorked = propertyStore.SelectSingleNode("//HoursWorked") as XmlElement;
			if(objLPRowId != null)
				dHoursWorked = Conversion.ConvertStrToDouble(objHoursWorked.InnerText);

			iPiRowId = Conversion.ConvertStrToInteger(base.GetSysExDataNodeText("PiRowId"));

			iPiEId = objGeneric.GetPiEmpID(iPiRowId);

			if (sSysCmd == "7" && sUpdateCmd == "1")
            {
				if(sUseLeavesFlag == "1")
					bReturn = objUpdtLeaveHist.UpdateLeaveHist(sDateEligibility,iLPRowId,iPiEId,dHoursWorked);
                //MITS 22294
                bReturn = objLeaveElig.LoadData(iPiEId, iLPRowId, sDateEligibility, iPiRowId);
				if(bReturn)
				{
					base.ResetSysExData("DateTFStart", objLeaveElig.DateTFStart);                
					base.ResetSysExData("DateTFEnd", objLeaveElig.DateTFEnd);          
                    //03/22/07 : Modified for Mits number 9009
                    base.ResetSysExData("HoursAvail", objLeaveElig.HoursAvail.ToString("#0.00"));
                    base.ResetSysExData("HoursUsed", objLeaveElig.HoursUsed.ToString("#0.00"));
                    base.ResetSysExData("MaxDurHours", objLeaveElig.MaxDurHours.ToString("#0.00"));
                    objCache.GetCodeInfo(objLeaveElig.EligStatusCode, ref sEligStatus, ref sEligStatusDesc);  //no need to put the language code here as LEAVE_ELIG_STATUS is the system table
                    //Geeta 03/12/07 : Modified for displaying description for Eliginility Status field
					sTemp = sEligStatusDesc;
					base.ResetSysExData("EligStatusCode", sTemp);                
					
					//set eligibility status code and message if applicable
					if(objLeaveElig.EligDateFlag != 0 && objLeaveElig.HoursWorkedFlag == 0)
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                            Globalization.GetString("Validation.IneligibleHoursWorked", base.ClientId),
							BusinessAdaptorErrorType.Error);
					else if(objLeaveElig.EligDateFlag == 0 && objLeaveElig.HoursWorkedFlag != 0)
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                            Globalization.GetString("Validation.IneligibleEligDate", base.ClientId),
							BusinessAdaptorErrorType.Error);
					else if(objLeaveElig.EligDateFlag == 0 && objLeaveElig.HoursWorkedFlag == 0)
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                            Globalization.GetString("Validation.IneligibleBoth", base.ClientId),
							BusinessAdaptorErrorType.Error);

					XmlNode objEligCode = null;
					XmlDocument objSysExData = base.SysEx;
					objEligCode = objSysExData.SelectSingleNode("/SysExData/EligStatusCode"); 
					base.SafeSetAttribute(objEligCode,"codeid",Conversion.ConvertObjToStr(objLeaveElig.EligStatusCode));
				}
				else
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        Globalization.GetString("Validation.NoLeaveHistRecordFound", base.ClientId),
						BusinessAdaptorErrorType.Error);
			}     
            
            #endregion

            if (Leave.LeaveRowId!=0)
				return;

		}

        public override void BeforeSave(ref bool Cancel)
        {
            XmlDocument objSysExDataXmlDoc = base.SysEx;
			XmlNode objDateTFStart = null;
			XmlNode objDateTFEnd = null;
			XmlNode objHoursAvail = null;
			XmlNode objHoursUsed = null;
			XmlNode objMaxDurHours = null;
			XmlNode objEligStatus = null;
            XmlNode objDateEligibilityNode = null;
			string sUpdateCmd = base.GetSysExDataNodeText("IsUpdate");
            try 
			{ 
				objDateTFStart = objSysExDataXmlDoc.SelectSingleNode("/SysExData/DateTFStart"); 
				objDateTFEnd = objSysExDataXmlDoc.SelectSingleNode("/SysExData/DateTFEnd"); 
				objHoursAvail = objSysExDataXmlDoc.SelectSingleNode("/SysExData/HoursAvail"); 
				objHoursUsed = objSysExDataXmlDoc.SelectSingleNode("/SysExData/HoursUsed"); 
				objMaxDurHours = objSysExDataXmlDoc.SelectSingleNode("/SysExData/MaxDurHours"); 
				objEligStatus = objSysExDataXmlDoc.SelectSingleNode("/SysExData/EligStatusCode"); 
				objDateEligibilityNode = objSysExDataXmlDoc.SelectSingleNode("/SysExData/DateEligibility");
			}
            catch { };

            if (objDateEligibilityNode != null)
                this.Leave.DateEligibility = objDateEligibilityNode.InnerText;

				if (sUpdateCmd == "1")
				{
					string sTemp = objEligStatus.InnerText;
					this.Leave.DateTFStart = objDateTFStart.InnerText;
					this.Leave.DateTFEnd = objDateTFEnd.InnerText;
					this.Leave.HoursAvail = Conversion.ConvertStrToDouble(objHoursAvail.InnerText);
					this.Leave.HoursUsed = Conversion.ConvertStrToDouble(objHoursUsed.InnerText);
					this.Leave.MaxDurHours = Conversion.ConvertStrToDouble(objMaxDurHours.InnerText);					
					if(objEligStatus.Attributes.GetNamedItem("codeid") != null)
						this.Leave.EligStatusCode = Conversion.ConvertStrToInteger(objEligStatus.Attributes.GetNamedItem("codeid").Value);
					else
					{
						if(sTemp != "")
							this.Leave.EligStatusCode = Convert.ToInt32(objCache.GetCodeId(sTemp.Substring(0,1),"LEAVE_ELIG_STATUS"));					
                        else
                            this.Leave.EligStatusCode = 0;
					}

				}    
            sUpdateCmd = "";
			objDateTFStart = null;
			objDateTFEnd = null;
			objHoursAvail = null;
			objHoursUsed = null;
			objMaxDurHours = null;
			objMaxDurHours = null;
			objEligStatus = null;
            objDateEligibilityNode = null;
            objSysExDataXmlDoc = null;
            base.ResetSysExData("IsUpdate", ""); //MITS 9956

            base.BeforeSave(ref Cancel);

        }
 
	}
}
