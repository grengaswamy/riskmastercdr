using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

//TODO: Auto-Generated Form - migrate and verify behaviors from Legacy RMNet form.

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for BRS Screen.
	/// </summary>
	public class BRSForm : DataEntryFormBase
	{
		const string CLASS_NAME = "Funds";
		private Funds Funds{get{return objData as Funds;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
	const string FILTER_KEY_NAME = "ClaimId";
		public override void Init()
		{
			//If we were sent here from a parent(event) then we'll have an eventid in the SysEx
			//originally from ScreenFlow\SysExData.
			XmlDocument objXML = base.SysEx;
			XmlNode objClaimId=null;
			try{objClaimId = objXML.GetElementsByTagName(FILTER_KEY_NAME)[0];}
			catch{};
			
			//Filter by this ClaimId if Present.
			if(objClaimId !=null)
			{
				(objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + objClaimId.InnerText;
			}
		}

		public BRSForm(FormDataAdaptor fda):base(fda)
		{
			this.objData = fda.Factory.GetDataModelObject(CLASS_NAME,false) as DataObject;
		}
		
		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
			base.OnUpdateForm ();
		}
	}
}