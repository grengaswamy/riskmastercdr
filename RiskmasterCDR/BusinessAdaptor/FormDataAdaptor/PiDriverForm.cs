﻿
using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using System.Text;

namespace Riskmaster.BusinessAdaptor
{
    /// <summary>
    /// Summary description for PiDriver Screen.
    /// Author: Mini Bahl
    /// </summary>
    public class PiDriverForm : DataEntryFormBase
    {
        const string CLASS_NAME = "PiDriver";
        private PiDriver PiDriver { get { return objData as PiDriver; } }
        private LocalCache objCache { get { return objData.Context.LocalCache; } }
        const string FILTER_KEY_NAME = "EventId";
        const string INSTANCE_SYSEXDATA_PATH = "/Instance/UI/FormVariables/SysExData"; 
        private int m_iEventId = 0;
        private string sConnectionString = null;
        private bool bIsNew=false;
        private bool bIsDriverExists = false;

        public override void InitNew()
        {
            base.InitNew();

            this.m_iEventId = base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME, false);
            string sFilter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + this.m_iEventId.ToString();
            bIsDriverExists = false;

            int iPiEid = base.GetSysExDataNodeInt("/SysExData/PiEid", false);
            int iDriverRowID = base.GetSysExDataNodeInt("/SysExData/DriverRowId", false);
            sFilter += " AND PI_EID=" + iPiEid;

            (objData as INavigation).Filter = sFilter;
            this.PiDriver.EventId = this.m_iEventId;

            if (iDriverRowID != 0)
            {
                iPiEid = iDriverRowID;
            }
            //avipinsrivas Start : Worked for Jira-340
            //this.PiDriver.DriverRowId = iPiEid;           //avipinsrivas Start : Worked for 7500 (Issue of 4634 - Epic 340)
            if (this.PiDriver.Context.InternalSettings.SysSettings.UseEntityRole && (base.GetSysExDataNodeText("SearchType") == "entity") && (iDriverRowID == 0))
            {
                string sSql = "SELECT DRIVER_ROW_ID FROM DRIVER WHERE DRIVER_EID = " + iPiEid;

                this.PiDriver.DriverRowId = Riskmaster.Common.Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(sConnectionString, sSql), base.ClientId);

                this.PiDriver.PiEid = iPiEid;

                if (this.PiDriver.DriverRowId > 0)
                {
                    bIsDriverExists = true;
                }
            }
            else
            {
                this.PiDriver.DriverRowId = iPiEid;             //avipinsrivas Start : Worked for 7500 (Issue of 4634 - Epic 340)
                //this.PiDriver.PiEid = iPiEid;
                string sSql = "SELECT DRIVER_EID FROM DRIVER WHERE DRIVER_ROW_ID = " + iPiEid;
                DbReader objReader = DbFactory.GetDbReader(sConnectionString, sSql);
                if (objReader.Read())
                {
                    this.PiDriver.PiEid = Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("DRIVER_EID"), base.ClientId);
                }
                objReader.Close();

                if (this.PiDriver.PiEid > 0)
                {
                    bIsDriverExists = true;
                }
            }
           //avipinsrivas End
            //rsushilaggar JIRA 7767
            if (PiDriver.Context.InternalSettings.SysSettings.UseEntityRole)
            {
                if (PiDriver.PiEntity.EntityId <= 0 && PiDriver.PiEntity.NameType <= 0)
                    PiDriver.PiEntity.NameType = objCache.GetCodeId("IND", "ENTITY_NAME_TYPE");
            }
        }
        public PiDriverForm(FormDataAdaptor fda)
            : base(fda)
        {
            base.m_ClassName = CLASS_NAME;
            sConnectionString = fda.connectionString;
        }
        public override void BeforeSave(ref bool Cancel)
        {
            int iPiEid = base.GetSysExDataNodeInt("/SysExData/PiEid", false);
            if (iPiEid > 0 && (this.m_objData as PiDriver).IsNew)
            //if (iPiEid > 0 && PiDriver.IsNew)
            {
                //avipinsrivas Start : Worked for Jira-340
                //string sSQL = "SELECT PI_ROW_ID FROM PERSON_INVOLVED WHERE EVENT_ID=" + this.m_iEventId + " AND PI_EID=" + iPiEid;
                //int iPiRowId = 0;
                //iPiRowId = this.PiDriver.Context.DbConnLookup.ExecuteInt(sSQL);
                //if (iPiRowId > 0)
                //{
                //    // PI Record already exists	
                //    Cancel = true;
                //    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                //       Globalization.GetString("Validation.DriverExistsAsPI", base.ClientId),
                //        BusinessAdaptorErrorType.Error); 
                //    return;
                //}
                int iPiErRowID = 0;
                if (this.PiDriver.Context.InternalSettings.SysSettings.UseEntityRole)
                {
                    int iEntityTableID = this.PiDriver.Context.LocalCache.GetTableId(Globalization.EntityGlossaryTableNames.DRIVERS.ToString());
                    iPiErRowID = this.PiDriver.PiEntity.IsEntityRoleExists(this.PiDriver.PiEntity.EntityXRoleList, iEntityTableID);
                }
                if (base.CheckPIEntity(this.m_iEventId, iPiEid, iPiErRowID, Globalization.PersonInvolvedGlossaryTableNames.DRIVER.ToString(), this.PiDriver.Context.DbConn.ConnectionString, this.PiDriver.Context.InternalSettings.SysSettings.UseEntityRole))
                {
                    Cancel = true;
                    return;
                }
                //avipinsrivas End
            }
       
        }

        //public override void AfterSave()
        //{
        //    XmlNode objOrig = base.SysEx.SelectSingleNode("/*/PiType");
        //    Driver objDriver = null;
        //    string sSQL = "SELECT DISTINCT DRIVER_ROW_ID FROM DRIVER WHERE DRIVER_EID=" + PiDriver.PiEid;
        //    int iRd = this.PiDriver.Context.DbConnLookup.ExecuteInt(sSQL);
        //    if (bIsNew && iRd <= 0)
        //    {
        //        objDriver = (Driver)PiDriver.Context.Factory.GetDataModelObject("Driver", true);
        //        objDriver.DriverEId = PiDriver.PiEid;
        //        objDriver.DriversRowId = PiDriver.Context.GetNextUID("Driver");
        //        objDriver.DriverTypeCode = Conversion.ConvertStrToInteger(objOrig.InnerText);
        //        objDriver.Save();
        //    }
        //    //else
        //    //{
        //    //    objDriver = (Driver)PiDriver.Context.Factory.GetDataModelObject("Driver", false);
        //    //    object ojj = PiDriver.Context.DbConn.ExecuteScalar("SELECT DISTINCT DRIVER_ROW_ID FROM DRIVER WHERE DRIVER_EID=" + PiDriver.PiEid);
        //    //    objDriver.MoveTo(Conversion.ConvertObjToInt(ojj, base.ClientId));
        //    //    objDriver.DriverEId = PiDriver.PiEid;
        //    //    objDriver.Save();
        //    //}
               
        //    base.ResetSysExData("DupSSN", "");
        //}

        public override void OnUpdateForm()
        {
            string sSubTitleText = string.Empty; //Aman MITS 30248
             base.OnUpdateForm();
             PopulateEntityTypes();
             //avipinsrivas Start : Worked for Jira-340
             //if (PiDriver.PiEid != 0)
           //  if (CommonFunctions.IsIDExist(sConnectionString, PiDriver.PiEid, "DRIVER", "DRIVER_EID"))
            if(bIsDriverExists || this.PiDriver.DriverRowId > 0)
             {
                 //avipinsrivas End
                 this.AddKillNode("Drivertyped");
                 this.AddDisplayNode("drivertypeid");
                 this.AddDisplayNode("Drivertextname");
             }
             else
             {
                 this.AddKillNode("drivertypeid");
                 this.AddKillNode("Drivertextname");
                 this.AddDisplayNode("Drivertyped");
             }
             // hide/unhide SSN field
             if (!PiDriver.Context.RMUser.IsAllowedEx(m_SecurityId, FormBase.RMO_PI_DRIVER_VIEW_SSN))
             {
                 base.AddKillNode("taxid");

             }
             Event objEvent = (Event)this.PiDriver.Context.Factory.GetDataModelObject("Event", false);
             string sEventDateFromDB = string.Empty;
             if (objEvent != null)
             {
                 int iEventId = PiDriver.EventId;
                 if (iEventId != 0)
                 {
                     objEvent.MoveTo(iEventId);
                     sEventDateFromDB = objEvent.DateOfEvent;
                     if (!String.IsNullOrEmpty(sEventDateFromDB))
                     {
                         if (base.SysEx.SelectSingleNode("DateOfEvent") == null)
                         {
                             string sUIEventDate = string.Empty;
                             sUIEventDate = Conversion.ToDate(sEventDateFromDB).ToString("MM/dd/yyyy");
                             base.CreateSysExData("DateOfEvent", sUIEventDate);
                         }
                     }
                 }
             }
             //added by neha goel MITS# 36916: PMC gap 7 CLUE fields- RMA - 5499
             if (!PiDriver.Context.InternalSettings.SysSettings.UseCLUEReportingFields)
             {
                base.AddKillNode("VehicleOperRel");
                base.AddKillNode("ClueSubjIdentifier");
                base.AddKillNode("ClueSubjRemovalIdentifier");
             
             }
             else
             {
                base.AddDisplayNode("VehicleOperRel");  
                base.AddDisplayNode("ClueSubjIdentifier");
                base.AddDisplayNode("ClueSubjRemovalIdentifier");
             }
             //end by neha goel MITS#36916 PMC CLUE gap 7 - RMA - 5499
             //Aman Driver MITS 30248       Start      
             sSubTitleText = string.Format(" [{0} * {1} {2}]", objEvent.EventNumber, PiDriver.PiEntity.FirstName, PiDriver.PiEntity.LastName);
             ArrayList singleRow = new ArrayList();
             base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
             base.AddElementToList(ref singleRow, "id", "formsubtitle");
             base.AddElementToList(ref singleRow, "Text", sSubTitleText);
             base.m_ModifiedControls.Add(singleRow);
             //Aman Driver MITS 30248    End
             if (PiDriver.Driver.DriverEntity.BirthDate != string.Empty)
                 base.ResetSysExData("EntityAge", Utilities.CalculateAgeInYears(PiDriver.Driver.DriverEntity.BirthDate, sEventDateFromDB)); //Added sEventDateFromDB for MITS 19315
             else
                 base.ResetSysExData("EntityAge", "");
             PeoplePermissionChecks4PI("pidriver", m_SecurityId + RMO_UPDATE);
            //rsushilaggar JIRA 7767
             if (this.PiDriver.Context.InternalSettings.SysSettings.UseEntityRole)
             {
                 if (this.PiDriver != null && this.PiDriver.PiEntity != null && this.PiDriver.PiEntity.NameType > 0)
                     this.ResetSysExData("EntityType", objCache.GetCodeDesc(this.PiDriver.PiEntity.NameType));
                 else
                     this.ResetSysExData("EntityType", objCache.GetCodeDesc(objCache.GetCodeId("IND", "ENTITY_NAME_TYPE")));
             }
             else
             {
                 this.AddKillNode("entitytype");
             }
            //end rushilaggar
        }
        
        public override void OnUpdateObject()
        {
            if (PiDriver.IsNew)
                bIsNew = true;
            base.OnUpdateObject();

            //avipinsrivas Start : Worked for Jira-340
            if (int.Equals(PiDriver.Driver.DriverTypeCode, 0) && base.GetSysExDataNodeInt("/SysExData/PiType", false) > 0)
                PiDriver.Driver.DriverTypeCode = base.GetSysExDataNodeInt("/SysExData/PiType", false);
            //avipinsrivas End

            if (PiDriver.PiEid != 0) //Aman Driver Changes
            {
                PiDriver.PiTypeCode = PiDriver.Context.LocalCache.GetCodeId("D", "PERSON_INV_TYPE");
                return;
            }
            XmlNode objOrig = base.SysEx.SelectSingleNode("/*/PiType");
            if (PiDriver.PiEid == 0 && objOrig != null)
            {
               PiDriver.PiTypeCode = PiDriver.Context.LocalCache.GetCodeId("D","PERSON_INV_TYPE");
               PiDriver.Driver.DriverTypeCode =  Conversion.ConvertStrToInteger(objOrig.InnerText);
            }
        }
        private void PopulateEntityTypes()
        {
           
            bool bBlankValue = true;
            string strTableName = base.GetSysExDataNodeText("/SysExData/PageTypeName", false);
             strTableName = "DRIVER_TYPE";
            
            string sSQL = @"SELECT CODES.CODE_ID,CODES.SHORT_CODE,CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT  WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID " +
      " AND DELETED_FLAG = 0 AND CODES.TABLE_ID =(SELECT G.TABLE_ID FROM GLOSSARY_TEXT GT, GLOSSARY G " +
      " WHERE GT.TABLE_ID=G.TABLE_ID AND UPPER(SYSTEM_TABLE_NAME) = '" + strTableName + "') ORDER BY CODES.SHORT_CODE";
            
            XmlElement xmlRoot = base.SysEx.CreateElement("EntityTypeList");

            using (DbReader rdr = PiDriver.Context.DbConnLookup.ExecuteReader(sSQL))
            {

                
                while (rdr.Read())
                {
                    
                    if (bBlankValue)
                    {
                        XmlElement xmlOptionBlank = base.SysEx.CreateElement("option");
                        xmlOptionBlank.InnerText = "";
                        XmlAttribute xmlOptionBlankAttrib = base.SysEx.CreateAttribute("value");
                        xmlOptionBlankAttrib.Value = "";
                        xmlOptionBlank.Attributes.Append(xmlOptionBlankAttrib);
                        xmlRoot.AppendChild(xmlOptionBlank);
                        bBlankValue = false;
                        XmlElement xmlOption = base.SysEx.CreateElement("option");
                        xmlOption.InnerText = rdr.GetString("CODE_DESC");
                        XmlAttribute xmlOptionAttrib = base.SysEx.CreateAttribute("value");
                        xmlOptionAttrib.Value = rdr.GetInt("CODE_ID").ToString();
                        xmlOption.Attributes.Append(xmlOptionAttrib);
                        xmlRoot.AppendChild(xmlOption);
                    }
                    else
                    {
                        XmlElement xmlOption = base.SysEx.CreateElement("option");
                        xmlOption.InnerText = rdr.GetString("CODE_DESC");
                        XmlAttribute xmlOptionAttrib = base.SysEx.CreateAttribute("value");
                        xmlOptionAttrib.Value = rdr.GetInt("CODE_ID").ToString();
                        xmlOption.Attributes.Append(xmlOptionAttrib);
                        xmlRoot.AppendChild(xmlOption);
                    }
                    
                }
                rdr.Close();
            }



            XmlNode objOrig = base.SysEx.SelectSingleNode("/*/EntityTypeList");
            if (objOrig != null)
            base.SysEx.DocumentElement.RemoveChild(objOrig);
            base.SysEx.DocumentElement.AppendChild(xmlRoot as XmlNode);
            base.ResetSysExData("PiType", PiDriver.Context.LocalCache.GetUserTableName(PiDriver.PiEntity.EntityTableId));
            sSQL = "SELECT DISTINCT DRIVER_TYPE FROM DRIVER WHERE DRIVER_EID="+PiDriver.PiEid;
            int iTypeCode = Conversion.ConvertObjToInt(PiDriver.Context.DbConn.ExecuteScalar(sSQL), base.ClientId);
            base.ResetSysExData("PiTypeCode", PiDriver.Context.LocalCache.GetCodeDesc(iTypeCode));
        }

        private void DisplayUnitNo()
        {

            string sStaUnitNo = string.Empty;
            string sRiskLoc = string.Empty;
            string sSubLoc = string.Empty;

            StringBuilder sbSql = new StringBuilder();
            // RMA-10039: Ash, fixed error but in general this is incorrect handling for POINT, Integral, Staging
            sbSql.Append("SELECT POINT_UNIT_DATA.STAT_UNIT_NUMBER, POINT_UNIT_DATA.UNIT_RISK_LOC, POINT_UNIT_DATA.UNIT_RISK_SUB_LOC from POLICY_X_UNIT,POINT_UNIT_DATA,PERSON_INVOLVED ");
            sbSql.Append(" WHERE POLICY_X_UNIT.POLICY_UNIT_ROW_ID=PERSON_INVOLVED.POLICY_UNIT_ROW_ID AND POLICY_X_UNIT.UNIT_ID=POINT_UNIT_DATA.UNIT_ID ");
            sbSql.Append(" AND POINT_UNIT_DATA.UNIT_TYPE=POLICY_X_UNIT.UNIT_TYPE AND PERSON_INVOLVED.PI_ROW_ID=" + PiDriver.PiRowId);

            DbReader objReader = null;
            objReader = DbFactory.GetDbReader(base.m_fda.connectionString, sbSql.ToString());
            if (objReader != null)
            {
                if (objReader.Read())
                {
                    sStaUnitNo = objReader.GetValue("STAT_UNIT_NUMBER").ToString();
                    sRiskLoc = objReader.GetValue("UNIT_RISK_LOC").ToString();
                    if (sRiskLoc == string.Empty)
                        sRiskLoc =  "-";

                    sSubLoc = objReader.GetValue("UNIT_RISK_SUB_LOC").ToString();
                    if (sSubLoc == string.Empty)
                        sSubLoc = "-";

                    base.ResetSysExData("UnitNo", sStaUnitNo + "/" + sRiskLoc + "/" + sSubLoc);


                }

            }


        }
    }
}
