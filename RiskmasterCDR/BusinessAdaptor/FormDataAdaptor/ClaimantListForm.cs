using System;
using Riskmaster.DataModel;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for ClaimantList.
	/// </summary>
	public class ClaimantListForm: ListFormBase
	{
		const string CLASS_NAME = "ClaimantList";

		public override void Init()
		{
			//If we were sent here from a parent(claim) then we'll have an eventid in the SysEx
			//originally from ScreenFlow\SysExData.
			XmlDocument objXML = base.SysEx;
			XmlNode objClaimId=null;
			try{objClaimId = objXML.SelectSingleNode("/SysExData/ClaimId");}
			catch{};
			
			//Filter by this EventId if Present.
			if(objClaimId !=null)
			{
				objDataList.BulkLoadFlag = true;
				objDataList.SQLFilter = "CLAIM_ID=" + objClaimId.InnerText;

                //Raman: Trying to obtain claimnumber from database
                string sClaimNumber = objDataList.Context.DbConn.ExecuteString("select claim_number from claim where claim_id = " + objClaimId.InnerText);

                XmlNode objNew;
                XmlNode objOld;


                objNew = objXML.CreateElement("ClaimNumber");
                objNew.InnerText = sClaimNumber;
                
                objOld = objXML.SelectSingleNode("//ClaimNumber");
                if (objOld == null)
                {
                    objXML.DocumentElement.AppendChild(objNew);
                }
			}
			else
				throw new Riskmaster.ExceptionTypes.CollectionException(String.Format(Globalization.GetString("ListForm.MissingExpectedFilterKey",base.ClientId),"EventId"));
			OnUpdateForm();
		}


		public ClaimantListForm(FormDataAdaptor fda):base(fda)
		{
			this.objDataList = fda.Factory.GetDataModelObject(CLASS_NAME,false) as DataCollection;
		}
		private LocalCache objCache{get{return objDataList.Context.LocalCache;}}

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm() //Only called from base classes on Refresh()caused by PostBack.
		{
			
			base.OnUpdateForm ();

			XmlDocument objXML = base.SysEx;
			XmlNode objNew;
			XmlNode objOld ;
			
			
			objNew = objXML.CreateElement("Screen");
			objNew.Attributes.Append(objXML.CreateAttribute("type"));
			objNew.Attributes["type"].Value="";

			objOld = objXML.SelectSingleNode("//Screen");
			if(objOld !=null)
				;
				//Let it Ride from the Client... 
				//Will be acted on in the Form XSL
				//No Need to clear since next page request should hit a new
				//form altogether anyhow.
				//objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);

            //Added Rakhi for Mits 13083-START:Able to hide the Add and Delete  button for Claimant
            CheckSecuritySettings(ref objXML);
            //Added Rakhi for Mits 13083-END:Able to hide the Add and Delete  button for Claimant

			//			//Clear any existing tablename nodes.
			//			foreach(XmlNode nd in objXML.SelectNodes("//EntityTableName"))
			//				nd.ParentNode.RemoveChild(nd);
			//
			//			//*********************
			//			// Parallel Array to the PIList to be serialized...
			//			//*********************
			//			foreach(PersonInvolved objPI in objDataList)
			//			{
			//				//Entity Table Name - Used as fall-back for Blank Entity Name.
			//				string sTableName = objCache.GetUserTableName(objPI.PiEntity.EntityTableId);
			//				objNew = objXML.CreateElement("EntityTableName");
			//				objNew.AppendChild(objXML.CreateCDataSection(sTableName));
			//				objXML.DocumentElement.AppendChild(objNew);
			//			}
		}
        #region Added Rakhi for Mits 13083-START:Able to hide the Add and Delete  button for Claimant
        private void CheckSecuritySettings(ref XmlDocument objXML)
        {
            //bool bIsView = false;
            bool bIsAddNew = false;
            bool bIsDelete = false;
            string sSysSid = Adaptor.SafeFormVariableParamText("SysSid");
            int SecurityId = Conversion.ConvertStrToInteger(sSysSid);
            XmlNode objSec = null;
            //bIsView = Adaptor.userLogin.IsAllowedEx(SecurityId, FormBase.RMO_VIEW);
            bIsAddNew = Adaptor.userLogin.IsAllowedEx(SecurityId, RMO_CREATE);
            bIsDelete = Adaptor.userLogin.IsAllowedEx(SecurityId, FormBase.RMO_DELETE);
            objSec = objXML.SelectSingleNode("//Add");
            if (objSec == null)
            {
                if (bIsAddNew)
                {
                    objSec = objXML.CreateElement("Add");
                    objSec.Attributes.Append(objXML.CreateAttribute("type"));
                    objSec.Attributes["type"].Value = "true";
                    objXML.DocumentElement.AppendChild(objSec);
                }
                else
                {
                    objSec = objXML.CreateElement("Add");
                    objSec.Attributes.Append(objXML.CreateAttribute("type"));
                    objSec.Attributes["type"].Value = "false";
                    objXML.DocumentElement.AppendChild(objSec);

                }
            }
            objSec = objXML.SelectSingleNode("//Delete");
            if (objSec == null)
            {
                if (bIsDelete)
                {
                    objSec = objXML.CreateElement("Delete");
                    objSec.Attributes.Append(objXML.CreateAttribute("type"));
                    objSec.Attributes["type"].Value = "true";
                    objXML.DocumentElement.AppendChild(objSec);
                }
                else
                {
                    objSec = objXML.CreateElement("Delete");
                    objSec.Attributes.Append(objXML.CreateAttribute("type"));
                    objSec.Attributes["type"].Value = "false";
                    objXML.DocumentElement.AppendChild(objSec);
                }
            }
        }
        #endregion Added Rakhi for Mits 13083-END:Able to hide the Add and Delete  button for Claimant
	}
}
