using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;


namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for CaseMgrNotesForm.
	/// </summary>
	public class CaseMgrNotesForm : DataEntryFormBase
	{
		const string CLASS_NAME = "CaseMgrNotes";
		private CaseMgrNotes CaseMgrNotes{get{return objData as CaseMgrNotes;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		const string FILTER_KEY_NAME = "CASEMGR_ROW_ID";

		public override void InitNew()
		{
			base.InitNew(); 
			this.CaseMgrNotes.CasemgrRowId = base.GetSysExDataNodeInt("/SysExData/CmcmhRowId"); 

			(objData as INavigation).Filter = FILTER_KEY_NAME + "=" + this.CaseMgrNotes.CasemgrRowId;
		}

		public CaseMgrNotesForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
			base.OnUpdateForm ();	
	
			if (this.CaseMgrNotes.IsNew && this.CaseMgrNotes.CmgrNotesRowId==0)
			{
				this.CaseMgrNotes.EnteredByUser=this.CaseMgrNotes.Context.RMUser.LoginName;
				this.CaseMgrNotes.DateEntered=Conversion.ToDbDate(System.DateTime.Now);
				this.CaseMgrNotes.TimeEntered=System.DateTime.Now.ToShortTimeString();
			}
           
            //aaggarwal29 : Case Management Permissions not working properly; MITS 28485 start

            int iCaseMgrRowID = CaseMgrNotes.CasemgrRowId;
            CaseManagement oCaseManagement;
            CmXCmgrHist oCmXCmgrHist;
            Claim oClaim;
            using (oCmXCmgrHist = (CmXCmgrHist)CaseMgrNotes.Context.Factory.GetDataModelObject("CmXCmgrHist", false))
            {
                oCmXCmgrHist.MoveTo(iCaseMgrRowID);
                oCaseManagement = (CaseManagement)CaseMgrNotes.Context.Factory.GetDataModelObject("CaseManagement", false);
                oCaseManagement.MoveTo(oCmXCmgrHist.CasemgtRowId);
                oClaim = (Claim)CaseMgrNotes.Context.Factory.GetDataModelObject("Claim", false);
                oClaim.MoveTo(oCaseManagement.ClaimId);
            }

            switch (oClaim.LineOfBusCode)
            {
                case 243:
                    m_SecurityId = RMO_WC_MANAGER_NOTES;
                    break;
                case 844:
                    m_SecurityId = RMO_DI_MANAGER_NOTES;
                    break;
            }
            oClaim.Dispose();

            //Updating Security Id in XML file
            XmlElement objSysSid = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysSid");
            objSysSid.InnerText = m_SecurityId.ToString();
            if (CaseMgrNotes.CmgrNotesRowId == 0)
            {
                if (!CaseMgrNotes.Context.RMUser.IsAllowedEx(m_SecurityId, RMO_CREATE))
                {
                    LogSecurityError(RMO_CREATE);
                    return;
                }
            }
            else
            {
                if (!CaseMgrNotes.Context.RMUser.IsAllowedEx(m_SecurityId, RMO_VIEW))
                {
                    LogSecurityError(RMO_VIEW);
                    return;
                }
            }
            //aaggarwal29 : MITS 28485 end
		}

	}

}