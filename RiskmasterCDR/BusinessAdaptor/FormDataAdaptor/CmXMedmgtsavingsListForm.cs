using System;
using Riskmaster.DataModel;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;


namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for PersonInvolvedList.
	/// </summary>
	public class CmXMedmgtsavingsListForm: ListFormBase
	{
		const string CLASS_NAME = "CmXMedmgtsavingsList";

		public override void Init()
		{
			//If we were sent here from a parent(event) then we'll have an eventid in the SysEx
			//originally from ScreenFlow\SysExData.
			XmlDocument objXML = base.SysEx;
			XmlNode objCasemgtRowId=null;
			try{objCasemgtRowId = objXML.SelectSingleNode("/SysExData/CasemgtRowId");}
			catch{};
			
			//Filter by this CasemgtRowId if Present.
			if(objCasemgtRowId !=null)
			{
				objDataList.BulkLoadFlag = true;
				objDataList.SQLFilter = "CASEMGT_ROW_ID=" + objCasemgtRowId.InnerText;
			}
			else
                throw new Riskmaster.ExceptionTypes.CollectionException(String.Format(Globalization.GetString("ListForm.MissingExpectedFilterKey", base.ClientId), "ClaimId"));
			OnUpdateForm();
		}


		public CmXMedmgtsavingsListForm(FormDataAdaptor fda):base(fda)
		{
			this.objDataList = fda.Factory.GetDataModelObject(CLASS_NAME,false) as DataCollection;
		}
		private LocalCache objCache{get{return objDataList.Context.LocalCache;}}

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm() //Only called from base classes on Refresh()caused by PostBack.
		{
			
			base.OnUpdateForm ();
			base.CreateSysExData("PiEid","");


			XmlDocument objXML = base.SysEx;
			XmlNode objNew;
			XmlNode objOld ;
			
			objNew = objXML.CreateElement("Screen");
			objNew.Attributes.Append(objXML.CreateAttribute("type"));
			objNew.Attributes["type"].Value="";

			objOld = objXML.SelectSingleNode("//Screen");
			if(objOld !=null)
				;
				//Let it Ride from the Client... 
				//Will be acted on in the Form XSL
				//No Need to clear since next page request should hit a new
				//form altogether anyhow.
				//objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);


			//Clear any existing tablename nodes.
			foreach(XmlNode nd in objXML.SelectNodes("//EntityTableName"))
				nd.ParentNode.RemoveChild(nd);
		}

        // akaushik5 Added for MITS 37554 Starts
        /// <summary>
        /// Called when [security identifier changed].
        /// </summary>
        public override void OnSecurityIdChanged()
        {
            base.OnSecurityIdChanged();
            int claimId = default(int);
            int casemgtRowId = default(int);
            bool success = default(bool);
            XmlDocument objXML = base.SysEx;

            XmlNode objCasemgtRowIdNode = objXML.SelectSingleNode("/SysExData/CasemgtRowId");
            if (!object.ReferenceEquals(objCasemgtRowIdNode, null))
            {
                casemgtRowId = Conversion.CastToType<int>(objCasemgtRowIdNode.InnerText, out success);
            }

            if (!casemgtRowId.Equals(default(int)))
            {
                using (CaseManagement objCaseManagement = this.objDataList.Context.Factory.GetDataModelObject("CaseManagement", false) as CaseManagement)
                {
                    objCaseManagement.MoveTo(casemgtRowId);
                    claimId = objCaseManagement.ClaimId;

                    if (!claimId.Equals(default(int)))
                    {
                        using (Claim objClaim = this.objDataList.Context.Factory.GetDataModelObject("Claim", false) as Claim)
                        {
                            objClaim.MoveTo(claimId);
                            switch (objClaim.LineOfBusCode)
                            {
                                // WC Claim
                                case 243:
                                    base.m_SecurityId = FormBase.RMO_WC_MEDMGTSAVINGS;
                                    break;
                                // DI Claim
                                case 844:
                                    base.m_SecurityId = FormBase.RMO_DI_MEDMGTSAVINGS;
                                    break;
                            }
                        }
                    }
                }
            }
        }
        // akaushik5 Added for MITS 37554 Ends
    }
}
