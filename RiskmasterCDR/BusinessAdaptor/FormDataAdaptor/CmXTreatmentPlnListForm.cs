using System;
using Riskmaster.DataModel;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for CmXTreatmentPlnListForm.
	/// </summary>
	public class CmXTreatmentPlnListForm: ListFormBase
	{
		const string CLASS_NAME = "CmXTreatmentPlnList";
        public override void Init()
		{
			//If we were sent here from a parent(event) then we'll have an eventid in the SysEx
			//originally from ScreenFlow\SysExData.
			XmlDocument objXML = base.SysEx;
			XmlNode objCasemgtRowId=null;
			try{objCasemgtRowId = objXML.SelectSingleNode("/SysExData/CasemgtRowId");}
			catch{};
			
			//Filter by this CasemgtRowId if Present.
			if(objCasemgtRowId !=null)
			{
				objDataList.BulkLoadFlag = true;
				objDataList.SQLFilter = "CASEMGT_ROW_ID=" + objCasemgtRowId.InnerText;
			}
			else
                throw new Riskmaster.ExceptionTypes.CollectionException(String.Format(Globalization.GetString("ListForm.MissingExpectedFilterKey", base.ClientId), "ClaimId"));
			OnUpdateForm();
		}


		public CmXTreatmentPlnListForm(FormDataAdaptor fda):base(fda)
		{
			this.objDataList = fda.Factory.GetDataModelObject(CLASS_NAME,false) as DataCollection;
		}
		private LocalCache objCache{get{return objDataList.Context.LocalCache;}}

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm() //Only called from base classes on Refresh()caused by PostBack.
		{
			base.OnUpdateForm ();
			base.CreateSysExData("PiEid","");


			XmlDocument objXML = base.SysEx;
			XmlNode objNew;
			XmlNode objOld ;
			
			objNew = objXML.CreateElement("Screen");
			objNew.Attributes.Append(objXML.CreateAttribute("type"));
			objNew.Attributes["type"].Value="";

			objOld = objXML.SelectSingleNode("//Screen");
			if(objOld !=null)
				;
				//Let it Ride from the Client... 
				//Will be acted on in the Form XSL
				//No Need to clear since next page request should hit a new
				//form altogether anyhow.
				//objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);

            //aaggarwal29 : Treatment Plan Permissions not working properly; MITS 26215 start
            XmlNode objCasemgtRowId = null;
            int iCasemgtRowId = 0;
            CaseManagement oCaseManagement;
            Claim objClaimTemp;
            try { objCasemgtRowId = objXML.SelectSingleNode("/SysExData/CasemgtRowId"); }
            catch { };

            if (objCasemgtRowId != null)
            {
                Int32.TryParse(objCasemgtRowId.InnerText, out iCasemgtRowId);
                using(oCaseManagement = (CaseManagement)base.m_fda.Factory.GetDataModelObject("CaseManagement", false))
                {
                    oCaseManagement.MoveTo(iCasemgtRowId);
                    objClaimTemp = (Claim)base.m_fda.Factory.GetDataModelObject("Claim", false);
                    objClaimTemp.MoveTo(oCaseManagement.ClaimId);
                }
                switch (objClaimTemp.LineOfBusCode)
                {
                    case 243:
                        m_SecurityId = RMO_WC_TREATMENT_PLAN;
                        break;
                    case 844:
                        m_SecurityId = RMO_DI_TREATMENT_PLAN;
                        break;
                }
                objClaimTemp.Dispose();
                //Updating Security Id in XML file
               XmlElement objSysSid = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysSid");
               objSysSid.InnerText = m_SecurityId.ToString();
            }
           
            //aaggarwal29 : MITS 26215 end

			//Clear any existing tablename nodes.
			foreach(XmlNode nd in objXML.SelectNodes("//EntityTableName"))
				nd.ParentNode.RemoveChild(nd);
		}
	}
}
