using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for CmXAccommodationForm.
	/// </summary>
	public class CmXAccommodationForm : DataEntryFormBase
	{
		const string CLASS_NAME = "CmXAccommodation";
		private CmXAccommodation CmXAccommodation{get{return objData as CmXAccommodation;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		const string FILTER_KEY_NAME = "CmAccommRowId";

		public override void InitNew()
		{
			base.InitNew(); 
			this.CmXAccommodation.CmRowId = base.GetSysExDataNodeInt("/SysExData/CasemgtRowId");  
		}

		public CmXAccommodationForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
            int iClaimId = 0;
            int iUseClaimProgressNotes = 0;
			base.OnUpdateForm ();		

            // PSHEKHAWAT 06-June-2012: MITS 23383, VIEW,CREATE,UPDATE,DELETE permissions not working
            string strClaimIDQuery = "SELECT CLAIM_ID from CASE_MANAGEMENT where CASEMGT_ROW_ID = " + CmXAccommodation.CmRowId; 
            iClaimId = CmXAccommodation.Context.DbConn.ExecuteInt(strClaimIDQuery);

            Claim objClaim = (Claim)CmXAccommodation.Context.Factory.GetDataModelObject("Claim", false);
            objClaim.MoveTo(iClaimId);

            switch (objClaim.LineOfBusCode)
            {
                // for WC claims
                case 243:
                    m_SecurityId = RMO_WC_ACCOMMODATION;
                    break;
                // for Non occupational claims
                case 844:
                    m_SecurityId = RMO_DI_ACCOMMODATION;
                    break;
            }
            // Check Create new Permissions, Row ID is 0 when new claim is created
            if (CmXAccommodation.CmAccommRowId == 0)
            {
                if(!CmXAccommodation.Context.RMUser.IsAllowedEx(m_SecurityId,RMO_CREATE))
                {
                    m_CurrentAction = enumFormActionType.None;
                    LogSecurityError(RMO_CREATE);
                    return;
                }
            }
            // check View permissions
            else
            {
                if (!CmXAccommodation.Context.RMUser.IsAllowedEx(m_SecurityId,RMO_VIEW))
                {
                    m_CurrentAction = enumFormActionType.None;
                    LogSecurityError(RMO_VIEW);
                    return;
                }
            }
            // PSHEKHAWAT : MITS 23383 END
            //mgaba2:mits 29116
            if (iClaimId > 0)
            {
                iUseClaimProgressNotes = objClaim.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode].ClmProgressNotesFlag;

                if (iUseClaimProgressNotes == 0)
                {
                    base.AddKillNode("enhancednotes");
                }
            }
		}
	}
}
