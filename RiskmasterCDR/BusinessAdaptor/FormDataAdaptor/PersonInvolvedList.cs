using System;
using Riskmaster.DataModel;
namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for PersonInvolvedList.
	/// </summary>
	public class PersonInvolvedListForm: ListFormBase
	{
		const string CLASS_NAME = "PersonInvolvedList";

		public PersonInvolvedList(FormDataAdaptor fda):base(fda)
		{
			this.objDataList = fda.Factory.GetDataModelObject(CLASS_NAME,false) as DataCollection;
		}

	}
}
