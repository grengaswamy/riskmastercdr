﻿
using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for Policy Screen.
	/// </summary>
	public class BankAccountSubForm : DataEntryFormBase
	{
		const string CLASS_NAME = "BankAccSub";
		private BankAccSub BankAccSub{get{return objData as BankAccSub;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		const string FILTER_KEY_NAME = "AccountId";

		public BankAccountSubForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}
		
		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
			//Call the base implementation of OnUpdateForm from the FormBase class
			base.OnUpdateForm ();		
					
			//Create the StockList necessary to populate the DropDownList on the FDM XML form
			if( !BankAccSub.Context.InternalSettings.SysSettings.AssignCheckStockToSubAcc )
				this.AddKillNode( "checkstock" );
			else
				AppendStockList();

            if (BankAccSub.Context.InternalSettings.SysSettings.MultiCovgPerClm == -1)
            {
                base.AddKillNode("OWNER_LOB5");
                base.AddKillNode("OWNER_LOB3");
            }
            else
            {
                base.AddKillNode("policyLOBCode");
                //mona: Making Policy Lob on Bank Account as multicode and non mandatory
                base.AddKillNode("AllPolLOB");
                base.AddKillNode("SpecificPolLOB");
                base.AddKillNode("labelPolicyLob");
                //mona: Making Policy Lob on Bank Account as multicode and non mandatory
            }

		}
				
		/// <summary>
		/// Adds the necessary Stock List information to SysExData
		/// </summary>
		/// <remarks>The Stock List information is created in a manner
		/// that can be used to populate DropDownLists.  The XPath to the SysExData
		/// will be used for binding to the itemset ref tag in the FDM XML Document
		/// </remarks>
		private void AppendStockList()
		{
			DbReader objReader = null ;
			string sSQL = string.Empty ;
			XmlDocument objXML = base.SysEx;
			XmlNode objOld=null;
			XmlElement objNew=null;
			XmlCDataSection objCData=null;
			bool bFound = false;
			bool bFirst= true;
			//Create the necessary SysExData to be used in ref binding
			objOld = objXML.SelectSingleNode("//StockList");
			objNew = objXML.CreateElement("StockList");

			sSQL = "SELECT STOCK_NAME, STOCK_ID FROM CHECK_STOCK WHERE ACCOUNT_ID = " + BankAccSub.AccountId + " ORDER BY STOCK_NAME" ;
            using (objReader = DbFactory.GetDbReader(BankAccSub.Context.DbConn.ConnectionString, sSQL))
            {

                //Loop through and create all the option values for the combobox control
                while (objReader.Read())
                {
                    //Defect 002164 Fixed by Neelima
                    if (bFirst)
                    {
                        XmlElement xmlOption1 = objXML.CreateElement("option");
                        objCData = objXML.CreateCDataSection("");
                        xmlOption1.AppendChild(objCData);
                        XmlAttribute xmlOptionAttrib1 = objXML.CreateAttribute("value");
                        xmlOptionAttrib1.Value = "";
                        xmlOption1.Attributes.Append(xmlOptionAttrib1);
                        objNew.AppendChild(xmlOption1);
                    }
                    bFirst = false;

                    XmlElement xmlOption = objXML.CreateElement("option");
                    //Create a CData section to handle any possible strange characters
                    //in the Account Name that may cause problems with the output XML
                    objCData = objXML.CreateCDataSection(objReader.GetString("STOCK_NAME"));
                    xmlOption.AppendChild(objCData);
                    XmlAttribute xmlOptionAttrib = objXML.CreateAttribute("value");
                    xmlOptionAttrib.Value = Conversion.ConvertObjToInt(objReader.GetValue("STOCK_ID"), base.ClientId).ToString();
                    xmlOption.Attributes.Append(xmlOptionAttrib);
                    objNew.AppendChild(xmlOption);
                    bFound = true;
                }//end foreach
            }

			if(!bFound)
			{
				//TR# 2061 In case of nothing in stock list it was giving error. so send a blank option.
				XmlElement xmlOption = objXML.CreateElement("option");
				objCData = objXML.CreateCDataSection("");
				xmlOption.AppendChild(objCData);
				XmlAttribute xmlOptionAttrib = objXML.CreateAttribute("value");
				xmlOptionAttrib.Value = "";
				xmlOption.Attributes.Append(xmlOptionAttrib);
				objNew.AppendChild(xmlOption);
			}

			//Add the XML to the SysExData 
			if(objOld !=null)
				objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);

			//Clean up
            //if( objReader != null )
            //{
            //    objReader.Close();
            //    objReader = null ;
            //}
			objOld = null;
			objNew = null;
			objCData = null;
		}//end method AppendBankAccountList()


		public override void InitNew()
		{
			base.InitNew();

			//If we were sent here from a parent(event) then we'll have an eventid in the SysEx
			//originally from ScreenFlow\SysExData.
			XmlNode objAccountId=null;

			try
			{
				objAccountId = base.SysEx.GetElementsByTagName(FILTER_KEY_NAME)[0];
			}//end try
			catch
			{
            }; //end catch
            //Changed by Gagan for MITS 15355 : Start
            if (objAccountId != null)
            {
                this.m_ParentId = Conversion.ConvertObjToInt(objAccountId.InnerText, base.ClientId);
                BankAccSub.AccountId = this.m_ParentId;
            }
            else
            {
                string sPropertyStore = "";
                if (base.m_fda.HasParam("SysPropertyStore"))
                {
                    sPropertyStore = base.m_fda.SafeParam("SysPropertyStore").InnerXml;
                    XmlDocument propertyStore = new XmlDocument();
                    propertyStore.LoadXml(sPropertyStore);
                    // npadhy MITS 15374 The Fix of 15355 Introduced this Bug. Fixing it.
                    if (propertyStore.SelectSingleNode("//AccountId") != null && propertyStore.SelectSingleNode("//AccountId").InnerText != "")
                    {
                        this.m_ParentId = Conversion.ConvertObjToInt(propertyStore.SelectSingleNode("//AccountId").InnerText, base.ClientId);
                        BankAccSub.AccountId = this.m_ParentId;
                    }
                }
            }
            //Changed by Gagan for MITS 15355 : End

			

			//Filter by this PhysEid if Present.
			if(this.m_ParentId != 0)
			{
				(objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + this.m_ParentId;
			}
			
		}


		public override void OnValidate(ref bool Cancel)
		{
			
		}
	}
}
