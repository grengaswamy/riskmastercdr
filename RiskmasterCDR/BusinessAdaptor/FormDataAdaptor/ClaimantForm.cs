﻿using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using System.Text;
using Riskmaster.Settings;


namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for EventScreen.
	/// </summary>
	public class ClaimantForm : DataEntryFormBase
	{
		const string CLASS_NAME = "Claimant";
		const string FILTER_KEY_NAME = "ClaimId";
        //skhare7 MITS 18069
        private int m_MMSEA_SID = 0;
        //skhare7 MITS 18069 End
		private Claimant objClaimant{get{return objData as Claimant;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}

        private string m_Caption = string.Empty;
            //nnorouzi; MITS 19478
        public override string GetCaption()
        {
            return base.GetCaption();
        }
		
		public override void InitNew()
		{
			base.InitNew();
			//If we were sent here from a parent(event) then we'll have an eventid in the SysEx
			//originally from ScreenFlow\SysExData.
			XmlNode objClaimId=null;
			try{objClaimId = base.SysEx.GetElementsByTagName(FILTER_KEY_NAME)[0];}
			catch{};
			
			if(objClaimId !=null)
				this.m_ParentId = Conversion.ConvertObjToInt(objClaimId.InnerText, base.ClientId);
			objClaimant.ClaimId = this.m_ParentId;
			//Filter by this Parent EventId if Present.
			if(m_ParentId >0)
				(objData as INavigation).Filter = "CLAIM_ID=" + this.m_ParentId;
		}

        //added:yukti, MITS 35772
        public override void OnUpdateObject()
        {
            base.OnUpdateObject();
            CreateUnits();
        }

        private void CreateUnits()
        {
            string[] array = null;
            XmlDocument objXML = base.SysEx;

                if (objXML.SelectSingleNode("//ClaimantUnitList/@codeid") != null)
                {
                    string sClaimUnitList = objXML.SelectSingleNode("//ClaimantUnitList").Attributes["codeid"].Value;
                    array = sClaimUnitList.Split("~".ToCharArray()[0]);
                }
                if (array != null && array[0] != "")
                {
                    for (int count = 0; count <= array.Length - 1; count++)
                    {
                        bool bAdd = true;
                        string[] arrTemp = null;
                        arrTemp = array[count].Split("|".ToCharArray()[0]);
                        foreach (ClaimantXUnit OClaimantXUnit in objClaimant.ClaimantXUnitList)
                        {
                             if ((arrTemp[0] + "|" + arrTemp[1]) == (OClaimantXUnit.UnitId.ToString() + "|" + OClaimantXUnit.UnitType))                         
                            {
                                bAdd = false;
                                break;
                            }
                        }
                        if (bAdd)
                        {
                            ClaimantXUnit objClaimantUnit = objClaimant.ClaimantXUnitList.AddNew();
                            objClaimantUnit.UnitId = Conversion.ConvertStrToInteger(arrTemp[0]);
                            objClaimantUnit.UnitType = arrTemp[1];
                        
                        
                        }
                    }
                }
                
                    foreach (ClaimantXUnit oClaimantXUnit in objClaimant.ClaimantXUnitList)
                    {

                        bool bDelete = true;
                        if (array != null && array[0] != "")
                        {
                            string[] arrTemp = null;
                            for (int count = 0; count <= array.Length - 1;count++)
                            {
                                arrTemp = array[count].Split("|".ToCharArray()[0]);
                                if ((arrTemp[0] + "|" + arrTemp[1]) == (oClaimantXUnit.UnitId.ToString() + "|" + oClaimantXUnit.UnitType))                                
                                {
                                    bDelete = false;
                                    break;
                                }
                            }
                        }
                    
                        if (bDelete)
                        {
                            objClaimant.ClaimantXUnitList.Remove(oClaimantXUnit.ClaimantUnitRowId);                         
                        }
                    }
                }
        //ended:yukti, MITS 35772
        
		public ClaimantForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}
		
		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
            string sDiaryMessage = "";
            ArrayList singleRow = null;
            int iUseClaimProgressNotes = 0;
			base.OnUpdateForm ();


            //MGaba2:MITS 16498:Start
            //Module Security Permission was not working as security id was different each time defendant 
            //is opened from different claim.

            Claim objClaim = (Claim)objClaimant.Context.Factory.GetDataModelObject("Claim", false);
            objClaim.MoveTo(objClaimant.ClaimId);

            switch (objClaim.LineOfBusCode)
            {
                case 241:
               
                    m_SecurityId = RMO_GC_CLAIMANT;
                    //skhare7 MITS 18069
                    m_MMSEA_SID = RMPermissions.RMO_GC_MMSEA;
                    //skhare7 MITS 18069 End
                    break;
                case 242:
                    m_SecurityId = RMO_VA_CLAIMANT;
                    //skhare7 MITS 18069
                    m_MMSEA_SID = RMPermissions.RMO_VA_MMSEA;
                    //skhare7 MITS 18069 end
                    break;                
                //Start-Mridul Bansal. 01/04/10. MITS#18230. Support for Property Claims.
                case 845:
                    m_SecurityId = RMO_PC_CLAIMANT;
                    break;
                //End-Mridul Bansal. 01/04/10. MITS#18230. Support for Property Claims.
            }

            //Setting Security Id for further permissions like attachment
            XmlElement objSysSid = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysSid");
            objSysSid.InnerText = m_SecurityId.ToString();

            //added:YUkti, MITS 35772
            if (objClaim.UnitList.Count <= 0 && objClaim.PropertyLossList.Count <= 0 && objClaim.SiteLossList.Count <= 0 && objClaim.OtherUnitLossList.Count <= 0)
            {
                bool ifUnitsAttached = false;
                base.ResetSysExData("SysAttachedUnits","false");

            }
            else
            {
                base.ResetSysExData("SysAttachedUnits", "");
            }
            //Ended:YUkti, MITS 35772
            //Create new Permission wasnt working
            if (objClaimant.ClaimantRowId == 0)
            {
                if (!objClaimant.Context.RMUser.IsAllowedEx(m_SecurityId, RMO_CREATE))
                {
                    m_CurrentAction = enumFormActionType.None;
                    LogSecurityError(RMO_CREATE);
                    return;
                }
            }
            else
            {//View Permission wasnt working
                if (!objClaimant.Context.RMUser.IsAllowedEx(m_SecurityId, RMO_VIEW))
                {
                    m_CurrentAction = enumFormActionType.None;
                    LogSecurityError(RMO_VIEW);
                    return;
                }
            }
            //MGaba2:MITS 16498:End

            //vsharma203- RMA 12108. Setting the value of hidden field RestrictAddNew.  
            if (!objClaimant.Context.RMUser.IsAllowedEx(m_SecurityId, RMO_Restrict_Add_New))
            {
                base.ResetSysExData("RestrictAddNew", "false");
            }
            else
            {
                base.ResetSysExData("RestrictAddNew", "true");
            }
            //vsharma203- RMA 12108 ends
            //     //skhare7 MITS 18069 
            if (!objClaimant.Context.RMUser.IsAllowedEx(m_MMSEA_SID, RMO_VIEW))

            {
                 base.AddKillNode("btnMMSEAData");
 
            
            }
            else
            {
            
            base.AddDisplayNode("btnMMSEAData");
            }
            //skhare7 MITS 18069 end



            //mgaba2:mits 29116
            iUseClaimProgressNotes = objClaim.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode].ClmProgressNotesFlag;
            if (iUseClaimProgressNotes == 0)
            {
                base.AddKillNode("enhancednotes");
            }



            //nnorouzi; MITS 19478
            m_Caption = this.GetCaption();


			//Pass this subtitle value to view (ref'ed from @valuepath).
            base.ResetSysExData("SubTitle", m_Caption);
            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singleRow, "id", "formsubtitle");
            base.AddElementToList(ref singleRow, "Text", m_Caption);
            base.m_ModifiedControls.Add(singleRow);
			// ** END   Form up a caption and put in SysEx **
            //Start: Nitin Goel. Remove MMSEA Button for Property Claim. 1/11/10, MITS :18230
            if (objClaim.LineOfBusCode == 845)
            {
                base.AddKillNode("btnMMSEAData");
            }
            //End: Nitin Goel. Remove MMSEA Button for Property Claim. 1/11/10, MITS :18230

			// Mihika 12/12/2005. Defect no. 718
			// Killing the 'Financials' node in case 'Claim Level' Reserve Tracking is on
			//Claim objClaim = (Claim)objClaimant.Context.Factory.GetDataModelObject("Claim", false);
			//objClaim.MoveTo(this.m_ParentId);
			if (objClaim.LineOfBusCode == objClaimant.Context.LocalCache.GetCodeId("VA", "LINE_OF_BUSINESS") || 
				objClaim.LineOfBusCode == objClaimant.Context.LocalCache.GetCodeId("GC", "LINE_OF_BUSINESS"))
			{
				//LOB settings
				Riskmaster.Settings.ColLobSettings objLOB = null;
				Riskmaster.Settings.LobSettings objLOBSettings = null;

                objLOB = new Riskmaster.Settings.ColLobSettings(objClaimant.Context.DbConn.ConnectionString, base.ClientId);
				objLOBSettings =  objLOB[objClaim.LineOfBusCode];
				int iReserveTracking = objLOBSettings.ReserveTracking;

				if (iReserveTracking == 0)
					base.AddKillNode("btnFinancials");

                if (objData.Context.InternalSettings.SysSettings.P2Login == "" ||
                    objData.Context.InternalSettings.SysSettings.P2Pass == "" ||
                    objData.Context.InternalSettings.SysSettings.P2Url == "")
                {
                 //   ((XmlElement)base.SysView.SelectSingleNode("//control[@name='clmntlastname']")).SetAttribute("withlink", "");
                    singleRow = new ArrayList();
                    base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.PiEntityLookup.ToString());
                    base.AddElementToList(ref singleRow, "id", "clmntlastname");
                    base.AddElementToList(ref singleRow, "withlink", "");
                    base.m_ModifiedControls.Add(singleRow);
                }
				objLOB = null;
				objLOBSettings = null;
			}
            if (objClaim.EventId != 0 && objClaim.PrimaryClaimant != null)
                sDiaryMessage = objClaim.PrimaryClaimant.ClaimantEntity.GetLastFirstName();
            else
                sDiaryMessage = "";
            base.ResetSysExData("DiaryMessage", sDiaryMessage);
            //Shruti for 10384
            if (this.SysFormStack.SelectSingleNode("//ScreenFlowStack/ScreenFlow[2]/SysFormName") != null)
            {
                //((XmlElement)base.SysView.SelectSingleNode("//internal[@name='SysFormPForm']")).SetAttribute("value",
                 //   this.SysFormStack.SelectSingleNode("//ScreenFlowStack/ScreenFlow[2]/SysFormName").InnerText);
                //todo
                string sSysFormname=this.SysFormStack.SelectSingleNode("//ScreenFlowStack/ScreenFlow[2]/SysFormName").InnerText;
                singleRow = new ArrayList();
                base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.TextBox.ToString());
                base.AddElementToList(ref singleRow, "id", "SysFormPForm");
                base.AddElementToList(ref singleRow, "Text", sSysFormname);
                base.m_ModifiedControls.Add(singleRow);
            }

            //smishra25: MITS 24395, MITS 13748 implementation was missing
            //nadim for 13748 Hide/unhide SSn field
            if (objClaim.LineOfBusCode == 241 || objClaim.LineOfBusCode == 242 || objClaim.LineOfBusCode == 845)
            {
                if (!objClaim.Context.RMUser.IsAllowedEx(m_SecurityId, FormBase.RMO_MAINT_VIEW_SSN))
                {
                    base.AddKillNode("clmnttaxid");

                }
            }
            //if (objClaim.LineOfBusCode == 242)
            //{
            //    if (!objClaim.Context.RMUser.IsAllowedEx(m_SecurityId, FormBase.RMO_MAINT_VIEW_SSN))
            //    {
            //        base.AddKillNode("clmnttaxid");

            //    }


            //}
            //nadim for 13748 Hide/unhide SSn field


            //Shruti for 11708
            base.ResetSysExData("dateofclaim", objClaim.DateOfClaim);
            base.ResetSysExData("ev_depteid_cid", (objClaim.Parent as Event).DeptEid.ToString());
            base.ResetSysExData("ev_dateofevent", (objClaim.Parent as Event).DateOfEvent);
            //Shruti for 11708

            objClaim.Dispose();
			// End - Defect no. 718

			//Check for People Maintenance permission
			PeoplePermissionChecks4Other("claimant", m_SecurityId+RMO_UPDATE);

//			//Update the Instance Document with the necessary SysExData
//			base.CreateSysExData("ClaimNumber");
//			if(objClaimant.KeyFieldValue!=0)//Fetch Appropriate ClaimNumber and stuff into SysEx
//			{
//				objClaimant.LoadParent();
//				base.ResetSysExData("ClaimNumber",(objClaimant.Parent as Claim).ClaimNumber);	
//			}
//			//Otherwise leave it alone.
            //Start - averma62
            base.ResetSysExData("ClaimantChanged", "false");
            base.ResetSysExData("ClaimantAttorneyChanged", "false");
            //End - averma62

            //rkaur27 : RMA - 4039 Start    
            string sEventDateFromDB = string.Empty;
            sEventDateFromDB = (objClaim.Parent as Event).DateOfEvent;

            if (!String.IsNullOrEmpty(sEventDateFromDB))
            {
                string sUIEventDate = string.Empty;
                sUIEventDate = Conversion.ToDate(sEventDateFromDB).ToString("MM/dd/yyyy");
                base.ResetSysExData("ev_dateofevent", sUIEventDate);
            }

            if (objClaimant.ClaimantEntity.BirthDate != string.Empty)
                base.ResetSysExData("clmntage", Utilities.CalculateAgeInYears(objClaimant.ClaimantEntity.BirthDate, sEventDateFromDB));
            else
                base.ResetSysExData("clmntage", "");
            //rkaur27 : RMA - 4039 End

            //JIRA RMA-9685 ajohari2  : Start
            int ClmStatus = 0;
            //JIRA RMA-11122 ajohari2  : Start
            //StringBuilder sbSQL = new StringBuilder();

            //sbSQL.AppendFormat("SELECT CODE_ID FROM CODES WHERE TABLE_ID = " + objClaimant.Context.LocalCache.GetTableId("CLAIMANT_STATUS") + " AND RELATED_CODE_ID= " + objClaimant.Context.LocalCache.GetCodeId("O", "CLAIMANT_STATUS_PARENT") + "  AND DELETED_FLAG=0");
            //using (DbReader objReader = DbFactory.GetDbReader(objClaimant.Context.DbConn.ConnectionString, sbSQL.ToString()))
            //{
            //    while (objReader.Read())
            //    {
            //        if (objReader.GetValue("CODE_ID") != DBNull.Value)
            //            ClmStatus = objReader.GetInt32("CODE_ID");
            //    }
            //}
            ClmStatus = CommonFunctions.GetChildIDFromParent(objClaimant.Context.LocalCache.GetTableId("CLAIMANT_STATUS"), objClaimant.Context.LocalCache.GetCodeId("O", "CLAIMANT_STATUS_PARENT"), objClaimant.Context.DbConn.ConnectionString, base.ClientId);
            //JIRA RMA-11122 ajohari2  : End

            if (!ClmStatus.Equals(0))
            {
                if (m_CurrentAction != enumFormActionType.Save && m_CurrentAction != enumFormActionType.Delete && m_CurrentAction != enumFormActionType.Refresh && m_CurrentAction != enumFormActionType.MoveTo)
                    objClaimant.ClaimantStatusCode = ClmStatus;
            }

            //JIRA RMA-9685 ajohari2  : End

            XmlDocument objXML = base.SysEx;
            RenderUnitList(ref objXML);
            //RMA-7414 start
            if (objClaimant.Context.InternalSettings.SysSettings.MultiCovgPerClm != 0)
            {
                int iReservesCount = objClaimant.Context.DbConnLookup.ExecuteInt(string.Format("SELECT COUNT (*) FROM RESERVE_CURRENT WHERE CLAIM_ID={0} AND CLAIMANT_EID={1}",
                    this.objClaimant.ClaimId, objClaimant.ClaimantEid));
                if (iReservesCount > 0)
                {
                    // changes for RMA- 10180, MITS 38172 start
                    //base.AddReadOnlyNode("clmntlastname");
                    base.AddReadOnlyNode("clmntlastnamebtn");
                    base.AddReadOnlyNode("clmntlastnamepibtn");
                    // changes for RMA- 10180, MITS 38172 end
                }
            }
            //RMA-7414 end
		}

        //added:yukti,DT:05/15/2014, MITS 35772
        internal void CreateElement(XmlElement p_objParentNode, string p_sNodeName, ref XmlElement p_objChildNode)
        {
            try
            {
                p_objChildNode = p_objParentNode.OwnerDocument.CreateElement(p_sNodeName);
                p_objParentNode.AppendChild(p_objChildNode);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ClaimantForm.RenderUnitList.Err",base.ClientId), p_objEx);
            }

        }
        internal void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText, ref XmlElement p_objChildNode)
        {
            try
            {
                CreateElement(p_objParentNode, p_sNodeName, ref p_objChildNode);
                p_objChildNode.InnerText = p_sText;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ClaimantForm.RenderUnitList.Err",base.ClientId), p_objEx);
            }
        }
        internal void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText, string sValue)
        {
            try
            {
                XmlElement objChildNode = null;
                this.CreateAndSetElement(p_objParentNode, p_sNodeName, p_sText, ref objChildNode);
                objChildNode.SetAttribute("value", sValue);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ClaimantForm.RenderUnitList.Err",base.ClientId), p_objEx);
            }
        }

        private void RenderUnitList(ref XmlDocument objXML)
        {
            XmlElement objClaimantUnitElement = null;
            string sCodeId = string.Empty;
            Vehicle objVehicle = (Vehicle)m_fda.Factory.GetDataModelObject("Vehicle", false);
            PropertyUnit objProperty = (PropertyUnit)m_fda.Factory.GetDataModelObject("PropertyUnit", false);
            SiteUnit objSiteUnit = (SiteUnit)m_fda.Factory.GetDataModelObject("SiteUnit", false);
            OtherUnit objOtherUnit = (OtherUnit)m_fda.Factory.GetDataModelObject("OtherUnit", false);
            try
            {
                objClaimantUnitElement = (XmlElement)objXML.SelectSingleNode("/SysExData/ClaimantUnitList");
                if (objClaimantUnitElement != null)
                    objClaimantUnitElement.ParentNode.RemoveChild(objClaimantUnitElement);

                CreateElement(objXML.DocumentElement, "ClaimantUnitList", ref objClaimantUnitElement);

                foreach (ClaimantXUnit objClaimantXUnit in objClaimant.ClaimantXUnitList)
                {
                    string sUnitDesc = string.Empty;

                    if (objClaimantXUnit.ClaimantRowId != 0)
                    {
                        if (objClaimantXUnit.UnitType == "P")
                        {
                            objProperty.MoveTo(objClaimantXUnit.UnitId);
                            CreateAndSetElement(objClaimantUnitElement, "Item", "PIN: " + objProperty.Pin,objProperty.PropertyId + "|P|" + objClaimantXUnit.ClaimantUnitRowId.ToString());
                            sUnitDesc =  objProperty.Pin;
                        }   
                        else if (objClaimantXUnit.UnitType == "V")
                        {
                            objVehicle.MoveTo(objClaimantXUnit.UnitId);
                            CreateAndSetElement(objClaimantUnitElement, "Item", "VIN: " + objVehicle.Vin,objVehicle.UnitId + "|V|" + objClaimantXUnit.ClaimantUnitRowId.ToString());
                             sUnitDesc= objVehicle.Vin;
                        }
                        else if (objClaimantXUnit.UnitType == "S")
                        {
                            objSiteUnit.MoveTo(objClaimantXUnit.UnitId);
                            CreateAndSetElement(objClaimantUnitElement, "Item", "SITE:" + objSiteUnit.SiteNumber, objSiteUnit.SiteId+ "|S|" + objClaimantXUnit.ClaimantUnitRowId.ToString());
                            sUnitDesc= objSiteUnit.SiteNumber;
                        }   
                        else if (objClaimantXUnit.UnitType == "SU")
                        {
                            objOtherUnit.MoveTo(objClaimantXUnit.UnitId);
                            Entity objEntity = (Entity)m_fda.Factory.GetDataModelObject("Entity", false);
                            objEntity.MoveTo(objOtherUnit.EntityId);                            
                            CreateAndSetElement(objClaimantUnitElement, "Item", "STAT:" + objEntity.LastName + " " + objEntity.FirstName, objOtherUnit.OtherUnitId + "|SU|" +  objClaimantXUnit.ClaimantUnitRowId.ToString());
                            sUnitDesc= objEntity.LastName;
                        }
                    }


                    if (string.IsNullOrEmpty(sCodeId) && objClaimantXUnit.ClaimantRowId != 0)
                    {                        
                        sCodeId = objClaimantXUnit.UnitId + "|" + objClaimantXUnit.UnitType + "|" +sUnitDesc;
                    }
                    else if (objClaimantXUnit.ClaimantRowId != 0)
                    {                        
                        sCodeId = string.Concat(sCodeId + "~" + objClaimantXUnit.UnitId + "|" + objClaimantXUnit.UnitType + "|" +sUnitDesc);
                    }

                }
                objClaimantUnitElement.SetAttribute("codeid", sCodeId);
                CreateElement(objXML.DocumentElement, "ClaimUnitListAll", ref objClaimantUnitElement);

            }
            finally
            {
                if (objClaimantUnitElement != null)
                {
                    objClaimantUnitElement = null;
                }
                if (objVehicle != null)
                {
                    objVehicle.Dispose();
                    objVehicle = null;
                }
                if (objProperty != null)
                {
                    objProperty.Dispose();
                    objProperty = null;
                }
                if (objSiteUnit != null)
                {
                    objSiteUnit.Dispose();
                    objSiteUnit = null;
                }
                if (objOtherUnit != null)
                {
                    objOtherUnit.Dispose();
                    objOtherUnit = null;
                }

            }
        }
        //Ended:yukti,DT:05/15/2014, MITS 35772

		public override void BeforeApplyLookupData(ref bool Cancel, ArrayList arrToSaveFields)
		{
			arrToSaveFields.Add("EntityTableId");
		}

		public override void OnValidate(ref bool Cancel)
		{
			bool bError = false;

            //Start - averma62 MITS 31676
            string sClaimantChanged = base.GetSysExDataNodeText("ClaimantChanged");
            string sAttorneyChanged = base.GetSysExDataNodeText("ClaimantAttorneyChanged");


            //rsharma220 MITS 32879 Start
            //if ((!Adaptor.userLogin.IsAllowedEx(RMO_ENTITYMAINTENANCE + RMO_UPDATE)) && !string.IsNullOrEmpty(sClaimantChanged) && string.Equals(sClaimantChanged, "true", StringComparison.InvariantCultureIgnoreCase) && objClaimant.ClaimantEntity.EntityId > 0)
            //{
            //    Errors.Add(Globalization.GetString("ValidationError"),
            //        Globalization.GetString("Permission.ClaimantForm.NoClaimantUpdate"), BusinessAdaptorErrorType.Error);

            //    bError = true;
            //}
            if ((!Adaptor.userLogin.IsAllowedEx(RMO_ENTITYMAINTENANCE + RMO_UPDATE)) && !string.IsNullOrEmpty(sClaimantChanged) && string.Equals(sAttorneyChanged, "true", StringComparison.InvariantCultureIgnoreCase) && objClaimant.AttorneyEntity.EntityId > 0)
            {
                Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                    Globalization.GetString("Permission.ClaimantForm.NoAttorneyUpdate",base.ClientId), BusinessAdaptorErrorType.Error);

                bError = true;
            }
            if ((!Adaptor.userLogin.IsAllowedEx(RMO_ENTITYMAINTENANCE + RMO_CREATE)) && objClaimant.AttorneyEntity.IsNew && !string.IsNullOrEmpty(objClaimant.AttorneyEntity.LastName) && objClaimant.AttorneyEntity.EntityId == 0)
            {
                Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                    Globalization.GetString("Permission.ClaimantForm.NoAttorneyCreate",base.ClientId), BusinessAdaptorErrorType.Error);

                bError = true;
            }
            //if ((!Adaptor.userLogin.IsAllowedEx(RMO_ENTITYMAINTENANCE + RMO_CREATE)) && !string.IsNullOrEmpty(objClaimant.ClaimantEntity.LastName) && objClaimant.ClaimantEntity.IsNew && objClaimant.ClaimantEntity.EntityId == 0)
            //{
            //    Errors.Add(Globalization.GetString("ValidationError"),
            //        Globalization.GetString("Permission.ClaimantForm.NoClaimantCreate"), BusinessAdaptorErrorType.Error);

            //    bError = true;
            //}
            //rsharma220 MITS 32879 End
            //End - averma62 MITS 31676

            // Charanpreet for MITS 12409 : Start
            Claim objClaim = (Claim)objClaimant.Context.Factory.GetDataModelObject("Claim", false);
            objClaim.MoveTo(this.m_ParentId);
            // Charanpreet for MITS 12409 : End

			// Perform data validation
			string sToday = Conversion.ToDbDate(System.DateTime.Now);

			if (objClaimant.ClaimantEntity.BirthDate != "")
				if (objClaimant.ClaimantEntity.BirthDate.CompareTo(sToday) > 0)
				{
					Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
						String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate",base.ClientId), Globalization.GetString("Field.BirthDate",base.ClientId), System.DateTime.Now.ToShortDateString()),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}

            // Charanpreet for MITS 12409 : Start :R5
            //MGaba2:MITS 14139:When an existing entity is selected as claimant,its id is received in entity id rather claimanteid
            //int iClaimantEid = objClaimant.ClaimantEid;
            // MGaba2 for MITS 15283: Start
            // int iClaimantEid = objClaimant.ClaimantEntity.EntityId;
            int iCurrentClaimantEid = objClaimant.ClaimantEntity.EntityId;
            int iClaimantEid = objClaimant.ClaimantEid;
            //In case we are selecting an already attached claimant while editing a claimant
            if ((iClaimantEid == 0 && iCurrentClaimantEid != 0) || (iClaimantEid != 0 && iClaimantEid != iCurrentClaimantEid))
            //if (iClaimantEid != 0 && objClaimant.IsNew)
            {
                foreach (Claimant objClaimant1 in objClaim.ClaimantList)
                {

                    //if (objClaimant1.ClaimantEid == iClaimantEid && objClaimant1.ClaimId == objClaimant.ClaimId)
                    if (objClaimant1.ClaimantEid == iCurrentClaimantEid && objClaimant1.ClaimId == objClaimant.ClaimId)
                    { // MGaba2 for MITS 15283: End
                        // Claimant Record already exists	
                        Cancel = true;
                        Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                            "Selected Entity already exists as Claimant for this Claim.",
                            BusinessAdaptorErrorType.Error);
                        return;
                    }
                }
            }
            // Charanpreet for MITS 12409 : End


			// Return true if there were validation errors
			Cancel = bError;
		}

        private void CreateSubTypeEntity(int iEntityId, string entityTableName,int iEventId )
        {
            string sSQL = string.Empty;
            bool blnSuccess = false;
            Entity objEntity = null;
            Employee objEmployee = null;
            PiEmployee objPiEmployee = null;
            Patient objPatient = null;
            PiPatient objPiPatient = null;
            MedicalStaff objMedicalStaff = null;
            Physician objPhysician = null;

            try
            {
                

                switch (entityTableName.ToUpper())
                {
                    case "EMPLOYEES":
                        string sEmpNumber = string.Empty;
                        sSQL = "SELECT EMPLOYEE_EID, EMPLOYEE_NUMBER FROM EMPLOYEE WHERE EMPLOYEE_EID = " + iEntityId;
                        using (DbReader objReader = DbFactory.GetDbReader(objClaimant.Context.DbConn.ConnectionString, sSQL))
                        {
                            if (objReader.Read())
                            {
                                sEmpNumber = Conversion.ConvertObjToStr(objReader.GetValue(1));
                            }
                        }
                        objPiEmployee = (PiEmployee)objClaimant.Context.Factory.GetDataModelObject("PiEmployee", false);

                                objPiEmployee.EventId = iEventId;
                                objPiEmployee.PiEid = iEntityId;
                                objPiEmployee.EmployeeNumber = sEmpNumber;
                            objPiEmployee.PiTypeCode = objCache.GetCodeId("E", "PERSON_INV_TYPE");
                            objPiEmployee.PolicyUnitRowId = 0;

                            objPiEmployee.Save();
                            objPiEmployee.Dispose();
                        
                        break;
                    case "PATIENTS":
                        int iPatientId = 0;
                        sSQL = "SELECT PATIENT_ID FROM PATIENT WHERE PATIENT_EID = " + iEntityId;
                        using (DbReader objReader = DbFactory.GetDbReader(objClaimant.Context.DbConn.ConnectionString, sSQL))
                        {
                            if (objReader.Read())
                            {
                                iPatientId = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);
                            }

                        }
                        objPiPatient = (PiPatient)objClaimant.Context.Factory.GetDataModelObject("PiPatient", false);
                                objPiPatient.EventId = iEventId;
                                objPiPatient.PiEid = iEntityId;
                            objPiPatient.PatientId = iPatientId;
                            objPiPatient.PiTypeCode = objCache.GetCodeId("P", "PERSON_INV_TYPE");
                            objPiPatient.PolicyUnitRowId = 0;
                            objPiPatient.Save();
                            objPiPatient.Dispose();
                        
                        break;
                    case "MEDICAL_STAFF":
                        CreatePersonInvolvedEntities(iEntityId, "MED", iEventId);
                        break;
                    case "PHYSICIANS":
                        CreatePersonInvolvedEntities(iEntityId, "PHYS", iEventId);
                        break;
                    case "WITNESS":
                        CreatePersonInvolvedEntities(iEntityId, "W", iEventId);
                        break;
                    case "DRIVERS":
                        CreatePersonInvolvedEntities(iEntityId, "D", iEventId);
                        break;
                    default:
                        CreatePersonInvolvedEntities(iEntityId, "O", iEventId);
                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (objEntity != null)
                    objEntity.Dispose();
                if (objEmployee != null)
                    objEmployee.Dispose();
                if (objPiEmployee != null)
                    objPiEmployee.Dispose();
                if (objPatient != null)
                    objPatient.Dispose();
                if (objPiPatient != null)
                    objPiPatient.Dispose();
                if (objMedicalStaff != null)
                    objMedicalStaff.Dispose();
                if (objPhysician != null)
                    objPhysician.Dispose();
            }
        }

        private void CreatePersonInvolvedEntities(int iEntityId, string sPiType, int iEventId)
        {
            PersonInvolved objPersonInvloved;
            string sSQL = string.Empty;
            string entityTableName = string.Empty;
            int iPiRowId = 0, iDriverRowId = 0;

         
            objPersonInvloved = (PersonInvolved)objClaimant.Context.Factory.GetDataModelObject("PersonInvolved", false);
         
            //for driver only
            if (sPiType.Trim().ToUpper() == "D" && iPiRowId == 0)
            {
                sSQL = "SELECT DRIVER_ROW_ID FROM DRIVER WHERE DRIVER_EID = " + iEntityId;
                using (DbReader objReader = DbFactory.GetDbReader(objClaimant.Context.DbConn.ConnectionString, sSQL))
                {
                    if (objReader.Read())
                    {
                        iDriverRowId = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);
                        objPersonInvloved.DriverRowId = iDriverRowId;
                    }
                }
            }

            objPersonInvloved.EventId = iEventId;
            objPersonInvloved.PiEid = iEntityId;
            objPersonInvloved.PiTypeCode = objCache.GetCodeId(sPiType, "PERSON_INV_TYPE");
            objPersonInvloved.PolicyUnitRowId = 0;
            objPersonInvloved.Save();
            objPersonInvloved.Dispose();
        }


        //Neha 07/25/2011 Added the vent to save person involved details in 
        public override void AfterSave()
        {
            base.AfterSave();

            //// MITS 29411 PSHEKHAWAT 9/4/2012- Commented to revert functionality of auto attaching a claimant to PI in case of VA, GC and PC  
            //remove comment by hlv MITS 29356 11/13/12
            Claimant objClaimant = (Riskmaster.DataModel.Claimant)this.m_objData;
            string sSql = null;
            bool bIsNotPresent = true;
            //tanwar2 - mits 30910 - start
            string sClmXPolDedIds = string.Empty;
            int iClmXPolDedId = 0;
            //tanwar2 -mi ts 30910 - end
            Claim objClaim = (Claim)objClaimant.Context.Factory.GetDataModelObject("Claim", false);
            objClaim.MoveTo(this.m_ParentId);
            sSql = "SELECT PI_ROW_ID FROM PERSON_INVOLVED WHERE PI_EID = " + objClaimant.ClaimantEid.ToString() + " AND  EVENT_ID =" + objClaim.EventId.ToString();
            using (DbReader objReaderPI = objClaimant.Context.DbConn.ExecuteReader(sSql))
            {
                if (objReaderPI.Read())
                    bIsNotPresent = false;
            }

            //hlv MITS 29356 11/13/12 begin
            bool bIsSetting = true;
            sSql = "SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='ADD_CLAIMANT_PI'";
            using (DbReader objReaderPISetting = objClaimant.Context.DbConn.ExecuteReader(sSql))
            {
                if (objReaderPISetting.Read())
                    bIsSetting = (objReaderPISetting.GetString(0).Equals("-1"));
            }
            //hlv MITS 29356 11/13/12 end

            //if (bIsNotPresent) //claimant is added/updated  and is not already prsent in person involved.
            if(bIsNotPresent && bIsSetting) //hlv MITS 29356 11/13/12
            {

                string sTableName = null;
                bool bValue = true;

                sTableName = objClaimant.Context.LocalCache.GetTableName(objClaimant.ClaimantEntity.EntityTableId);

                switch (sTableName)
                {
                    case "EMPLOYEES":
                        sTableName = "EMPLOYEES";
                        break;

                    case "MED":
                        sTableName = "MEDICAL_STAFF";
                        break;
                    //case "DEPENDENTS":
                    //case "OTHER_PEOPLE":
                    //case "EXPERT_WITNESS":
                    //case "ANESTHETISTS":
                    //case "REHAB_PROVIDER":
                    //case "REHAB_JOB_PROVIDER":
                    //case "BROKER":
                    //case "DRIVER_INSURED":
                    //case "DRIVER_OTHER":
                    //case "INJURED":
                    //case "CASE_MANAGER":
                    //case "PASSENGER":
                    //case "CONTRACTOR":
                    //    sTableName = "O";
                    //    break;
                    case "PATIENTS":
                        sTableName = "PATIENTS";
                        break;
                    case "PHYSICIAN":
                        sTableName = "PHYSICIANS";
                        break;
                    case "WITNESS":
                        sTableName = "WITNESS";
                        break;
                    case "DRIVERS":
                        sTableName = "DRIVERS";
                        break;
                    default:
                        sTableName = "O";
                       // bValue = false;
                        break;
                }
                if (bValue)
                {

                    //if employee
                    //if (string.Compare(sTableName, "E") == 0)
                    //{
                    //    Employee objEmployee = (Employee)objClaimant.Context.Factory.GetDataModelObject("Employee", false);
                    //    objEmployee.MoveTo(objClaimant.ClaimantEid);
                    //    PiEmployee objPiEmployee = (PiEmployee)objClaimant.Context.Factory.GetDataModelObject("PiEmployee", false);
                    //    objPiEmployee.PiEid = objClaimant.ClaimantEid;
                    //    objPiEmployee.EventId = objClaim.EventId;
                    //    objPiEmployee.PiTypeCode = objCache.GetCodeId(sTableName, "PERSON_INV_TYPE");
                    //    objPiEmployee.EmployeeNumber = objEmployee.EmployeeNumber;
                    //    objPiEmployee.Save();
                    //}
                    //else // for all other types;
                    //{

                    //    PersonInvolved objPersonInvolved = (PersonInvolved)objClaimant.Context.Factory.GetDataModelObject("PersonInvolved", false);
                    //    objPersonInvolved.PiEid = objClaimant.ClaimantEid;
                    //    objPersonInvolved.EventId = objClaim.EventId;
                    //    objPersonInvolved.PiTypeCode = objCache.GetCodeId(sTableName, "PERSON_INV_TYPE");
                    //    objPersonInvolved.Save();

                    //}
                    CreateSubTypeEntity(objClaimant.ClaimantEid, sTableName, objClaim.EventId);
                }

            }
            // MITS 29411 - END
			            //tanwar2 - mits 30910 - start
            using (DbReader oDbReader = DbFactory.GetDbReader(objClaimant.Context.DbConnLookup.ConnectionString, string.Format("SELECT CLM_X_POL_DED_ID FROM CLAIM_X_POL_DED WHERE CLAIMANT_EID=0 AND CLAIM_ID={0}", objClaim.ClaimId)))
            {
                while (oDbReader.Read())
                {
                    iClmXPolDedId = oDbReader.GetInt32("CLM_X_POL_DED_ID");
                    if (string.IsNullOrEmpty(sClmXPolDedIds))
                    {

                        sClmXPolDedIds = iClmXPolDedId.ToString();
                    }
                    else
                    {
                        sClmXPolDedIds = string.Concat(sClmXPolDedIds , "," , iClmXPolDedId.ToString());
                    }
                }
            }
            if (!string.IsNullOrEmpty(sClmXPolDedIds))
            {
                objClaimant.Context.DbConnLookup.ExecuteNonQuery(string.Format("UPDATE CLAIM_X_POL_DED SET CLAIMANT_EID={0} WHERE CLM_X_POL_DED_ID in({1})", objClaimant.ClaimantEid, sClmXPolDedIds));
                objClaimant.Context.DbConnLookup.ExecuteNonQuery(string.Format("UPDATE CLAIM_X_POL_DED_HIST SET CLAIMANT_EID={0} WHERE CLM_X_POL_DED_ID in({1})", objClaimant.ClaimantEid, sClmXPolDedIds));
            }
            
            //tanwar2 - mits 30910 - end
        }

        //Neha end

        public override void BeforeDelete(ref bool Cancel)
        {
            //tanwar2 - mits 30910 - start

            // rrachev JIRA RMA-7414 Begin
            //int iClaimId;
            //int iClaimantEid;
            //int iCount;

            //iCount = 0;
            //iClaimId = this.m_ParentId;
            //iClaimantEid = objClaimant.ClaimantEid;

            //iCount = objClaimant.Context.DbConnLookup.ExecuteInt(string.Format("SELECT COUNT (*) FROM CLAIM_X_POL_DED WHERE CLAIM_ID={0} AND CLAIMANT_EID={1}", 
            //    iClaimId, iClaimantEid));

            //if (iCount > 0)
            //{
            //    Errors.Add(new Exception(Globalization.GetString("Claimant.AssociatedDedecutible.Error", base.ClientId)), BusinessAdaptorErrorType.Error);
            //    Cancel = true;
            //}

            if (objClaimant.Context.InternalSettings.SysSettings.MultiCovgPerClm != 0)
            {
                int iReservesCount = objClaimant.Context.DbConnLookup.ExecuteInt(string.Format("SELECT COUNT (*) FROM RESERVE_CURRENT WHERE CLAIM_ID={0} AND CLAIMANT_EID={1}",
                    this.m_ParentId, objClaimant.ClaimantEid));
                if (iReservesCount > 0)
                {
                    Errors.Add(new Exception(Globalization.GetString("Claimant.AssociatedReserve.Error", base.ClientId)), BusinessAdaptorErrorType.Error);
                    Cancel = true;
                }
            }
            // rrachev JIRA RMA-7414 End

            base.BeforeDelete(ref Cancel);
            //tanwar2 - mits 30910 - end
        }
	}
}
