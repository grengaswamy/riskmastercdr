﻿using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

//TODO: Auto-Generated Form - migrate and verify behaviors from Legacy RMNet form.

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for BankAccount Screen.
	/// </summary>
	public class BankAccountForm : DataEntryFormBase
	{
		const string CLASS_NAME = "Account";
		private const int RMB_FUNDS_BNKACCT = 9800;
		private const int RMB_FUNDS_DEPOSIT = 10650;
		private const int RMB_FUNDS_BNKACCT_CHKSTOCK = 9950;
		private const int SUPP_OFFSET = 50;	

		//Defect 002257 Fixed by Neelima, added security for Bank Acccount -> balance
		private const int RMO_FUNDS_BNKACCT_BALANCE = 10300;

		private Account Account{get{return objData as Account;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}

		/// <summary>
		/// Class constructor to initialize Class Name
		/// </summary>
		/// <param name="fda"></param>
		public BankAccountForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;

			/*if (objData == null)
				InitNew();*/

			//this.objData = fda.Factory.GetDataModelObject(CLASS_NAME,false) as DataObject;
		}//end class constructor

		/// <summary>
		/// Initializes the DataModel class and sets the parent ID for the class
		/// that can be used for parent-child navigation
		/// </summary>
        public override void InitNew()
        {
            base.InitNew();

            Account.AccountId = this.m_ParentId;
            //Changed for mits 11165
            if (SysEx.DocumentElement.SelectSingleNode("MasterAccFlag") == null)
                CreateSysExData("MasterAccFlag");


        }//end method InitNew()
        



		/// <summary>
		/// Overrides the base class OnUpdateForm method to handle a Bank Account List in the 
		/// instance data for ref binding.  
		/// </summary>
		/// <remarks>This instance data is specifically held in the SysExData section
		/// of the Instance document.
		/// </remarks>
		public override void OnUpdateForm()
		{
			string strSQL;
			string sNode = "FundsDepositList";
			string sCaption="";
			XmlDocument objXML = base.SysEx;
			XmlNode objOld=null;
			XmlElement objNew=null;
			Riskmaster.Db.DbConnection objConn = null;
            //Changed for mits 11165
            XmlDocument propertyStore = new XmlDocument();
            string sPropertyStore = "";
            string sSQL = "";
            int iMasterAccFlag = 0;
            //int iNextCheckNumber = 0;
            long lNextCheckNumber = 0;    //pmittal5 Mits 14500 02/24/09 - Check Number is of 64 bits length
            string sMasterBankAccId = "0";
            bool bParent = false;


			if(base.ModuleSecurityEnabled)//Is Security Enabled
			{
				if(!m_fda.userLogin.IsAllowedEx(RMB_FUNDS_BNKACCT_CHKSTOCK,RMO_ACCESS))
				{
					base.AddKillNode("btnCheckStocks");
				}
				if(!m_fda.userLogin.IsAllowedEx(RMB_FUNDS_BNKACCT+SUPP_OFFSET,RMO_ACCESS))
				{
					base.AddKillNode("btnAccountBalance");
				}
				//Defect 002257 Fixed by Neelima, added security for Bank Acccount -> balance				
				if(!m_fda.userLogin.IsAllowedEx(RMO_FUNDS_BNKACCT_BALANCE,RMO_ACCESS))
				{
					base.AddKillNode("btnBalance");
				}	
			}
			
			//Call the base implementation of OnUpdateForm from the FormBase class
			base.OnUpdateForm();

			//Defect 002062 Fixed by Neelima
			sCaption = " [" + Account.AccountNumber + " * " + Account.AccountName + " ]";			
			if (Account.AccountNumber.Trim() != "")
				base.ResetSysExData("SubTitle",sCaption);
			

			// Mihika Defect no. 1119
			if (Account.AccountId <= 0)
				Account.NextCheckNumber = 1;

			//Build the SQL to be executed against the database
			strSQL = "SELECT COUNT(*)  CNT ";
			strSQL += "FROM FUNDS_DEPOSIT ";
			strSQL += "WHERE BANK_ACC_ID=";
			strSQL += Account.AccountId;

			//Create the Connection object
			objConn = DbFactory.GetDbConnection(base.Adaptor.connectionString);

			//Open the Connection object
			objConn.Open();

			//Execute the SQL against the database
			string strCount = objConn.ExecuteScalar(strSQL).ToString();

			//Close the database Connection object
			objConn.Close();

			//Create the necessary SysExData to be used in ref binding
			objOld = objXML.SelectSingleNode("//" + sNode);
			//Create the necessary SysExData to be used in ref binding
			objNew = objXML.CreateElement(sNode);

			//Create the XML attribute used for binding
			XmlAttribute xmlCountAttrib = objXML.CreateAttribute("count");
			xmlCountAttrib.Value = strCount;

			//Add the Count Attribute to the AccountList element
			objNew.Attributes.Append(xmlCountAttrib);

			//Add the XML to the SysExData 
			if(objOld !=null)
				objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);

			//Clean up
			objConn = null;
			objOld = null;
			objNew = null;

			// Add the SubAccount button visible code.

			objOld = objXML.SelectSingleNode("//ControlAppendAttributeList");
			objNew = objXML.CreateElement("ControlAppendAttributeList");
			XmlElement objElem = null;
			XmlElement objChild = null;
		
			objElem = objXML.CreateElement("btnSubAccounts");
			objChild = objXML.CreateElement("visible");
			
			//Defect 002083 Fixed by Neelima
			string sUseFundsSubAcc ="";
			sUseFundsSubAcc=Account.Context.InternalSettings.SysSettings.UseFundsSubAcc.ToString().ToLower() ;
				
			objChild.SetAttribute( "value", sUseFundsSubAcc);
			
			objElem.AppendChild(objChild);;
			objNew.AppendChild(objElem);

			if(objOld !=null)
				objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);
			
			objElem = null ;
			objChild = null ;
			objOld = null ;
			objNew = null ;

			//Defect 002083 Fixed by Neelima
			if (sUseFundsSubAcc != "true")
				base.AddKillNode("btnAccBankReports");
	
			//Check for People Maintenance permission
			PeoplePermissionChecks4Other("bankaccount", m_SecurityId+RMO_UPDATE);



            //Geeta 07/28/08 : Mits 11165
            if (Account.Context.InternalSettings.SysSettings.UseMastBankAcct && !(Account.Context.InternalSettings.SysSettings.UseFundsSubAcc))
            {    
               // base.AddDisplayNode("masterbankacc");
                if (this.Account.AccountId != 0)
                {
                    sSQL = sSQL = "SELECT COUNT(*) FROM ACCOUNT WHERE MAST_BANK_ACCT_ID = " + this.Account.AccountId;

                    using (DbReader objReader = Account.Context.DbConnLookup.ExecuteReader(sSQL))

                        if (objReader.Read())
                        {
                            if (Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId) > 0)
                            {
                                bParent = true;
                            }
                        }
                    //Changed by Gagan for MITS 15941 : Start
                    if (!bParent)
                        base.AddDisplayNode("masterbankacc");
                    else
                    {
                        base.AddKillNode("masterbankacc");
                    //    return;
                    }
                    //Changed by Gagan for MITS 15941 : End
                }
                else
                {
                    base.AddKillNode("masterbankacc");
                    //Changed by Gagan for MITS 15941 : Start
                    base.AddReadWriteNode("nextchecknumber");
                    //Changed by Gagan for MITS 15941 : End
                    return;
                }
                
                if (!bParent)
                {                    
                    iMasterAccFlag = base.GetSysExDataNodeInt("MasterAccFlag");

                    if (base.m_fda.HasParam("SysPropertyStore"))
                    {
                        sPropertyStore = base.m_fda.SafeParam("SysPropertyStore").InnerXml;
                        propertyStore.LoadXml(sPropertyStore);
                    }

                    sMasterBankAccId = this.Account.MastBankAcctId.ToString();

                    //Changed by Gagan for MITS 15984 : Start
                    if (Account.MastBankAcctId != 0)
                        iMasterAccFlag = 1;

                    //if (base.GetSysExDataNodeInt("MasterAccFlag") == 1)
                    if(iMasterAccFlag == 1)
                    {
                        if (sMasterBankAccId != "0")
                        {
                            sSQL = "SELECT NEXT_CHECK_NUMBER FROM ACCOUNT WHERE ACCOUNT_ID= " + sMasterBankAccId;//propertyStore.SelectSingleNode("//MastBankAcctId").InnerText;

                            using (DbReader objReader = Account.Context.DbConnLookup.ExecuteReader(sSQL))
                            {
                                if (objReader.Read())
                                {
                                    //pmittal5 Mits 14500 02/24/09 - Check Number is of 64 bits length
                                    //iNextCheckNumber = Conversion.ConvertObjToInt((objReader.GetValue(0)), base.ClientId);
                                    lNextCheckNumber = Conversion.ConvertObjToInt64((objReader.GetValue(0)), base.ClientId);
                                }
                            }
                        }
                    }

                    //Changed by Gagan for MITS 15984 : End

                    //if (base.SysView.SelectSingleNode("//control[@name='nextchecknumber']") != null)
                    //{
                        if ((base.GetSysExDataNodeInt("MasterAccFlag") == 1 && sMasterBankAccId != "0"))
                        {
                       //     ((XmlElement)base.SysView.SelectSingleNode("//control[@name='nextchecknumber']")).SetAttribute("type", "readonly");
                            base.AddReadOnlyNode("nextchecknumber");

                        }
                        if (Account.AccountId > 0)
                        {
                            if (sMasterBankAccId != "0")
                            {
                         //       ((XmlElement)base.SysView.SelectSingleNode("//control[@name='nextchecknumber']")).SetAttribute("type", "readonly");
                                base.AddReadOnlyNode("nextchecknumber");
                            }
                        }
                    //}
                    AppendBankAccountList();
                    base.AddDisplayNode("masterbankacc");
                }
                else
                {
                    base.AddKillNode("masterbankacc");
                    //Changed by Gagan for MITS 15941 : Start
                    base.AddReadWriteNode("nextchecknumber");
                    //Changed by Gagan for MITS 15941 : End
                }
            }

            else
            {
                base.AddKillNode("masterbankacc");
            }

            if (!Account.Context.InternalSettings.SysSettings.UseMastBankAcct)
            {
                if (Account.AccountId <= 0)
                {
                    Account.NextCheckNumber = 1;
                }
            }
            else if (Account.Context.InternalSettings.SysSettings.UseMastBankAcct)
            {
                if (Account.AccountId <= 0)
                {
                    if (iMasterAccFlag == 0)
                    {
                        Account.NextCheckNumber = 1;
                    }
                    else
                    {
                        if (sMasterBankAccId != "0")
                        {
                            Account.NextCheckNumber = lNextCheckNumber;
                        }
                        else
                        {
                            Account.NextCheckNumber = 1;
                        }
                    }
                }
                else if (iMasterAccFlag == 1)
                {
                    if (sMasterBankAccId != "0")
                    {
                        Account.NextCheckNumber = lNextCheckNumber;
                    }
                    else
                    {
                        sSQL = "SELECT NEXT_CHECK_NUMBER FROM ACCOUNT WHERE ACCOUNT_ID= " + propertyStore.SelectSingleNode("//AccountId").InnerText;

                        using (DbReader objReader = Account.Context.DbConnLookup.ExecuteReader(sSQL))
                        {
                            if (objReader.Read())
                            {
                                //pmittal5 Mits 14500 02/24/09 - Check Number is of 64 bits length
                                //iNextCheckNumber = Conversion.ConvertObjToInt((objReader.GetValue(0)), base.ClientId);
                                lNextCheckNumber = Conversion.ConvertObjToInt64((objReader.GetValue(0)), base.ClientId);
                            }
                        }
                        Account.NextCheckNumber = lNextCheckNumber;
                    }
                }
            }


            if (Account.Context.InternalSettings.SysSettings.MultiCovgPerClm == -1)
            {
                base.AddKillNode("OWNER_LOB5");
                base.AddKillNode("OWNER_LOB3");
                base.AddKillNode("OWNER_LOB6");
            }
            else
            {
                base.AddKillNode("policyLOBCode");
                //mona: Making Policy Lob on Bank Account as multicode and non mandatory
                    base.AddKillNode("AllPolLOB");
                    base.AddKillNode("SpecificPolLOB");
                    base.AddKillNode("labelPolicyLob");
                //mona: Making Policy Lob on Bank Account as multicode and non mandatory
                    
            }


		}//end method OnUpdateForm()

		public override void BeforeApplyLookupData(ref bool Cancel, ArrayList arrToSaveFields)
		{
			arrToSaveFields.Add("EntityTableId");
		}



        /// <summary>
        /// Geeta 07/28/08 : Mits 11165 
        /// Adds the necessary Bank Account List information to SysExData
        /// </summary>
        /// <remarks>The Bank Account List information is created in a manner
        /// that can be used to populate DropDownLists.  The XPath to the SysExData
        /// will be used for binding to the itemset ref tag in the FDM XML Document
        /// </remarks>
        private void AppendBankAccountList()
        {
            //Variable declarations
            ArrayList arrAcctList = new ArrayList();
            XmlDocument objXML = base.SysEx;
            XmlNode objOld = null;
            XmlElement objNew = null;
            XmlElement objElem = null;
            XmlCDataSection objCData = null;
            int iAccountId = 0;
            string sMasterAccId = "0";
            string sAccName = "";
            int iAccListCount = 0;

            //Create the necessary SysExData to be used in ref binding
            objOld = objXML.SelectSingleNode("//MastBankAcctList");
            objNew = objXML.CreateElement("MastBankAcctList");
            // Start Naresh MITS 10009 Blank Value Added in Combo
            XmlElement xmlOption = null;
            XmlAttribute xmlOptionAttrib = null;

            // Blank value for the combo box
            xmlOption = objXML.CreateElement("option");
            xmlOptionAttrib = objXML.CreateAttribute("value");
            xmlOption.Attributes.Append(xmlOptionAttrib);
            objNew.AppendChild(xmlOption);
            xmlOptionAttrib = null;
            xmlOption = null;

            //Loop through and create all the option values for the combobox control

            string sSQL = string.Empty;

            sSQL = "SELECT ACCOUNT_ID, ACCOUNT_NAME, MAST_BANK_ACCT_ID FROM ACCOUNT " +
                   "ORDER BY ACCOUNT_NAME";

            using (DbReader objReader = Account.Context.DbConnLookup.ExecuteReader(sSQL))
            {
                while (objReader.Read())
                {
                    iAccountId = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);
                    sAccName = Conversion.ConvertObjToStr(objReader.GetValue(1));
                    sMasterAccId = Conversion.ConvertObjToStr(objReader.GetValue(2));

                    if (Account.AccountId != iAccountId && sMasterAccId == "0")
                    {
                        xmlOption = objXML.CreateElement("option");
                        //Create a CData section to handle any possible strange characters
                        //in the Account Name that may cause problems with the output XML
                        objCData = objXML.CreateCDataSection(sAccName);
                        xmlOption.AppendChild(objCData);
                        xmlOptionAttrib = objXML.CreateAttribute("value");
                        xmlOptionAttrib.Value = Convert.ToString(iAccountId);
                        xmlOption.Attributes.Append(xmlOptionAttrib);
                        objNew.AppendChild(xmlOption);
                        iAccListCount = iAccListCount + 1;
                    }
                }
            }

            if (iAccListCount == 0)
            {
                objElem = objXML.CreateElement("option");
                objElem.SetAttribute("value", "0");
                objElem.InnerText = string.Empty;
                objNew.AppendChild(objElem);
            }
            //Add the XML to the SysExData 
            if (objOld != null)
                objXML.DocumentElement.ReplaceChild(objNew, objOld);
            else
                objXML.DocumentElement.AppendChild(objNew);

            //Clean up
            base.ResetSysExData("MasterAccFlag", "0");
            arrAcctList = null;
            objOld = null;
            objNew = null;
            objCData = null;

        }//end method AppendBankAccountList()


        /// <summary>
        /// pmittal5 MITS:12771 09/11/08
        /// Overrides the base class AfterApplyLookupData method to reset other Account related fields when a bank is selected.
        /// </summary>
        public override void AfterApplyLookupData(XmlDocument objPropertyStore)
        {
            XmlDocument objTargetDoc = new XmlDocument();
            XmlDocument objConfigDom = new XmlDocument();
            XmlNode objSource = null;
            XmlNode objTarget = null;


            string sTempXml = objPropertyStore.SelectSingleNode("Instance").InnerXml;

            objPropertyStore.LoadXml(sTempXml);

            base.AfterApplyLookupData(objPropertyStore);

            if (base.SysLookup.SelectSingleNode("/SysLookup/SysLookupAttachNodePath").InnerText.IndexOf("BankEntity") > 0)
            {
                this.Account.Refresh();  //Emptying the Account object
                //Changed for MITS 15171 : Start
                if (Account.AccountId <= 0)
                    Account.NextCheckNumber = 1;
                //Changed for MITS 15171 : End
                objConfigDom.LoadXml("<Account><BankEntity/><Supplementals/></Account>");
                objTargetDoc.LoadXml(this.Account.SerializeObject(objConfigDom));

                //Bank Entity data copied from PropertyStore to the dummy document
                objSource = (XmlNode)objTargetDoc.SelectSingleNode("//BankEntity"); 
                objTarget = objTargetDoc.CreateElement(objSource.Name);
                objTarget.InnerXml = objPropertyStore.SelectSingleNode("//BankEntity").InnerXml;
                objSource.ParentNode.ReplaceChild(objTarget, objSource);
                objSource = null;
                objTarget = null;

                //Replacing the PropertyStore with dummy document
                objSource = (XmlNode)objPropertyStore.SelectSingleNode("//Account");
                objTarget = objPropertyStore.CreateElement(objSource.Name);
                objTarget.InnerXml = objTargetDoc.SelectSingleNode("//Account").InnerXml;
                objSource.ParentNode.ReplaceChild(objTarget, objSource);

                objSource = null;
                objTarget = null;
            }
         }//End AfterApplyLookupData()
        //sgoel6 MITS 15275 04/18/2009
        public override void OnValidate(ref bool Cancel)
        {
            bool bError = false;

            // Perform data validation
            string sToday = Conversion.ToDbDate(System.DateTime.Now);
            string sTime = Conversion.GetTime(System.DateTime.Now.ToShortTimeString());
            string sLangCode = base.LanguageCode;//Deb ML Changes
            if (Account.TriggerFromDate != "" && Account.TriggerToDate != "")
            {
                if (Account.TriggerFromDate.CompareTo(Account.TriggerToDate) > 0)
                {
                    //Deb ML Changes
                    Errors.Add(CommonFunctions.FilterBusinessMessageRetainErrorCode(Globalization.GetString("ValidationError",base.ClientId), sLangCode),
                    String.Format(CommonFunctions.FilterBusinessMessageRetainErrorCode(Globalization.GetString("Validation.MustBeLessThanToDate", base.ClientId), sLangCode), CommonFunctions.FilterBusinessMessageRetainErrorCode(Globalization.GetString("Field.EffectiveStartDate", base.ClientId), sLangCode), Conversion.ToDate(Account.TriggerToDate, sLangCode, base.ClientId)),
                    BusinessAdaptorErrorType.Error);
                    //Deb ML Changes
                    bError = true;
                }

                // Return true if there were validation errors
                Cancel = bError;
            }
        }
    }
}
