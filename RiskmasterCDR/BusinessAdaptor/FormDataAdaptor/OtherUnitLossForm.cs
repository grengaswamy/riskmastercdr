﻿using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
    class OtherUnitLossForm : DataEntryFormBase
    {
        const string CLASS_NAME = "ClaimXOtherUnit";
        const string FILTER_KEY_NAME = "ClaimId";
        private ClaimXOtherUnit objClaimXOtherUnit { get { return objData as ClaimXOtherUnit; } }
        private LocalCache objCache { get { return objData.Context.LocalCache; } }
        private string m_Caption = string.Empty;

        public OtherUnitLossForm(FormDataAdaptor fda)
            : base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

        public override string GetCaption()
        {
            return base.GetCaption();
        }

        public override void InitNew()
        {
            base.InitNew();


            objClaimXOtherUnit.ClaimId = base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME, false);
            if (objClaimXOtherUnit.ClaimId > 0)
                (objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + objClaimXOtherUnit.ClaimId;


        }

        public override void OnUpdateForm()
        {
            ArrayList singleRow = null;          

            base.OnUpdateForm();
            Claim objClaim = (Claim)objClaimXOtherUnit.Context.Factory.GetDataModelObject("Claim", false);
            objClaim.MoveTo(objClaimXOtherUnit.ClaimId);

            switch (objClaim.LineOfBusCode)
            {
                case 241:
                    m_SecurityId = RMO_GC_OTHERUNITLOSS;
                    break;
                case 243:
                    m_SecurityId = RMO_WC_OTHERUNITLOSS;
                    break;
            }

            if (SysEx.DocumentElement.SelectSingleNode("LINEOFBUSCODE") == null)
            {
                CreateSysExData("LINEOFBUSCODE");
                base.ResetSysExData("LINEOFBUSCODE", (objClaimXOtherUnit.Parent as Claim).LineOfBusCode.ToString());
            }

            m_Caption = this.GetCaption();

            base.ResetSysExData("SubTitle", m_Caption);
            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singleRow, "id", "formsubtitle");
            base.AddElementToList(ref singleRow, "Text", m_Caption);
            base.m_ModifiedControls.Add(singleRow);

            XmlElement objSysSid = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysSid");
            objSysSid.InnerText = m_SecurityId.ToString();


            //Create new Permission wasnt working
            if (objClaimXOtherUnit.RowId == 0)
            {

                if (!objClaimXOtherUnit.Context.RMUser.IsAllowedEx(m_SecurityId, RMO_CREATE))
                {
                    m_CurrentAction = enumFormActionType.None;
                    LogSecurityError(RMO_CREATE);
                    return;
                }
            }
            else
            {

                if (!objClaimXOtherUnit.Context.RMUser.IsAllowedEx(m_SecurityId, RMO_VIEW))
                {
                    m_CurrentAction = enumFormActionType.None;
                    LogSecurityError(RMO_VIEW);
                    return;
                }

            }

            if (!objClaimXOtherUnit.IsNew)
            {
                base.AddReadOnlyNode("liabilitytype");
            }

            //Pass this subtitle value to view (ref'ed from @valuepath).
            base.ResetSysExData("SubTitle", m_Caption);

            if (objClaimXOtherUnit.AddedByUser == "LSSINF")
            {                
                base.ResetSysExData("FormReadOnly", "Disable");
            }
            else
                base.ResetSysExData("FormReadOnly", "Enable");
        }

        public override void OnUpdateObject()
        {
            base.OnUpdateObject();

        }

        public override void OnValidate(ref bool Cancel)
        {
            bool bError = false;           
            Cancel = bError;
        }
    }
}
