using System;
using Riskmaster.DataModel;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for AdjustDatedTextList.
	/// </summary>
	public class AdjustDatedTextListForm: ListFormBase
	{
		const string CLASS_NAME = "AdjustDatedTextList";

		public override void Init()
		{
			XmlDocument objXML = base.SysEx;
			XmlNode objAdjRowId=null;
			XmlNode objTextTypeCodeId= null;
			try{objAdjRowId = objXML.SelectSingleNode("/SysExData/AdjRowId");
				//objTextTypeCodeId = objXML.SelectSingleNode("/SysExData/TextTypeCode/@codeid");
                objTextTypeCodeId = objXML.SelectSingleNode("/SysExData/CodeId");
            }
			catch{};
			
			//Creating a filter using ADJ_ROW_ID and TEXT_TYPE_CODE.
			if(objAdjRowId !=null)
			{
				objDataList.BulkLoadFlag = true;

				if(objTextTypeCodeId==null||objTextTypeCodeId.InnerText=="" ||objTextTypeCodeId.InnerText=="0" )
				{
					objDataList.SQLFilter = "ADJ_ROW_ID=" + objAdjRowId.InnerText;
						
				}
				else
					objDataList.SQLFilter = "ADJ_ROW_ID=" + objAdjRowId.InnerText + " And TEXT_TYPE_CODE=" + objTextTypeCodeId.InnerText ;

			}
			else
				throw new Riskmaster.ExceptionTypes.CollectionException(String.Format(Globalization.GetString("ListForm.MissingExpectedFilterKey",base.ClientId),"AdjRowId"));
			OnUpdateForm();
		}


		public AdjustDatedTextListForm(FormDataAdaptor fda):base(fda)
		{
			this.objDataList = fda.Factory.GetDataModelObject(CLASS_NAME,false) as DataCollection;
		}
		private LocalCache objCache{get{return objDataList.Context.LocalCache;}}

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm() //Only called from base classes on Refresh()caused by PostBack.
		{
			
			base.OnUpdateForm ();

			XmlDocument objXML = base.SysEx;
			XmlNode objNew;
			XmlNode objOld ;
			
			
			objNew = objXML.CreateElement("Screen");
			objNew.Attributes.Append(objXML.CreateAttribute("type"));
			objNew.Attributes["type"].Value="";

			objOld = objXML.SelectSingleNode("//Screen");
			if(objOld !=null)
				;
				//Let it Ride from the Client... 
				//Will be acted on in the Form XSL
				//No Need to clear since next page request should hit a new
				//form altogether anyhow.
				//objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);


			//			//Clear any existing tablename nodes.
			//			foreach(XmlNode nd in objXML.SelectNodes("//EntityTableName"))
			//				nd.ParentNode.RemoveChild(nd);
			//
			//			//*********************
			//			// Parallel Array to the PIList to be serialized...
			//			//*********************
			//			foreach(PersonInvolved objPI in objDataList)
			//			{
			//				//Entity Table Name - Used as fall-back for Blank Entity Name.
			//				string sTableName = objCache.GetUserTableName(objPI.PiEntity.EntityTableId);
			//				objNew = objXML.CreateElement("EntityTableName");
			//				objNew.AppendChild(objXML.CreateCDataSection(sTableName));
			//				objXML.DocumentElement.AppendChild(objNew);
			//			}
		}
	}
}
