﻿using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

using System.Xml.Linq; //RMA-8753 nshah28
using System.Xml.XPath; //RMA-8753 nshah28
namespace Riskmaster.BusinessAdaptor
{
    /// <summary>
    /// Summary description for Address master Screen.
    /// //Created by nshah28 RMA-8753
    /// </summary>
    public class AddressForm : DataEntryFormBase
    {
        const string CLASS_NAME = "Address";
        private Address Address { get { return objData as Address; } }
        private LocalCache objCache { get { return objData.Context.LocalCache; } }
        const string FILTER_KEY_NAME = "AddressId";
        const string INSTANCE_SYSEXDATA_PATH = "/Instance/UI/FormVariables/SysExData"; 
         private int m_iEventId = 0;
        private string sConnectionString = null;


        public override void InitNew()
        {
            
            base.InitNew();

            this.m_iEventId = base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME, false);
            string sFilter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + this.m_iEventId.ToString();

          
            int iAddressId = base.GetSysExDataNodeInt("/SysExData/PiEid", false);
            sFilter += " AND ADDRESS_ID=" + iAddressId;

            (objData as INavigation).Filter = sFilter;

            this.Address.AddressId = iAddressId;
        }
        public AddressForm(FormDataAdaptor fda): base(fda)
        {
            base.m_ClassName = CLASS_NAME;
            sConnectionString = fda.connectionString;
        }

        public override void BeforeSave(ref bool Cancel)
        {
            if (IsDupeAddress())
            {
                Cancel = true;
                return;
            }
        }

        public override void AfterSave()
        {
            base.ResetSysExData("dupeoverride", "");
            base.ResetSysExData("HdnIsDupAddr", "");
            base.ResetSysExData("HdnDupAddrId", "");
        }
        public override void OnUpdateForm()
        {         
            base.OnUpdateForm();
           // PopulateEntityTypes();
            if (Address.AddressId != 0)
            {
               // this.AddKillNode("entitytableid");
            }
            else
            {
                //this.AddKillNode("drivertypeid");
            }
          
            //Need to add this field here, so it will not count as missing ref.(Hidden fields taken on FDM page, if not set like below in "onupdateform" and "aftersave" it will count as missing ref)
            if (base.SysEx.SelectSingleNode("//dupeoverride") == null) 
                base.ResetSysExData("dupeoverride", "");
            if (base.SysEx.SelectSingleNode("//HdnIsDupAddr") == null)
                base.ResetSysExData("HdnIsDupAddr", "");
            if (base.SysEx.SelectSingleNode("//HdnDupAddrId") == null)
                base.ResetSysExData("HdnDupAddrId", "");
        }
        
        public override void OnValidate(ref bool Cancel)
        {
            bool bError = false;
        }
        public override void OnUpdateObject()
        {
            base.OnUpdateObject();
            
         //   Address.SearchString = CreateSearchString();
            Address.SearchString = SearchStringForAddress(Address, this.objCache);

            if (Address.AddressId != 0)
                return;
            
           // XmlNode objOrig = base.SysEx.SelectSingleNode("/*/AddressType");
          /*  if (Address.AddressId == 0 && objOrig != null)
            {

                // Address.AddressTypeCode = Conversion.ConvertStrToInteger(objOrig.InnerText); //uncomment this
            } */
             
        }

        /*
        public string CreateSearchString()
        {
            string strSearchString = string.Empty;
            string sStateCode = string.Empty;
            string sStateDesc = string.Empty;
            this.objCache.GetStateInfo(Address.State, ref sStateCode, ref sStateDesc);
            if (Address != null)
            {
                strSearchString = Address.Addr1.Replace(" ", string.Empty).ToLower() + Address.Addr2.Replace(" ", string.Empty).ToLower() + Address.Addr3.Replace(" ", string.Empty).ToLower() + Address.Addr4.Replace(" ", string.Empty).ToLower();
                strSearchString += Address.City.Replace(" ", string.Empty).ToLower() + sStateDesc.Replace(" ", string.Empty).ToLower() + Address.County.Replace(" ", string.Empty).ToLower() + this.objCache.GetCodeDesc(Address.Country).Replace(" ", string.Empty).ToLower() + Address.ZipCode.ToString().Replace(" ", string.Empty).ToLower() + Address.AddressSeqNum.ToString().Replace(" ", string.Empty).ToLower();
            }
            return strSearchString;
        } */
        /// <summary>
        /// Function check for duplicate entry
        /// </summary>
        /// <returns></returns>
        private bool IsDupeAddress()
        {
            if (this.SysEx.SelectSingleNode("//dupeoverride") != null && (this.SysEx.SelectSingleNode("//dupeoverride").InnerText.Trim() == "IsSaveDup" || this.SysEx.SelectSingleNode("//dupeoverride").InnerText.Trim() == "Cancel"))  
                return false;

            string sSql = string.Empty;
            XmlNode nodeDup = null;
            XmlNode nodeAddress = null;
            int sAddressId=0;
           /* sSql = string.Format("SELECT ADDRESS_ID FROM ADDRESS WHERE SEARCH_STRING = '{0}' ", Address.SearchString);
            DbConnection objDbConnection = null;
            objDbConnection = DbFactory.GetDbConnection(Address.Context.DbConn.ConnectionString);

            if (objDbConnection != null && objDbConnection.State != System.Data.ConnectionState.Open)
            {
                objDbConnection.Open();
            }
            sAddressId = objDbConnection.ExecuteInt(sSql); */
            sAddressId = CommonFunctions.CheckAddressDuplication(Address.SearchString, Address.Context.DbConn.ConnectionString, Address.Context.ClientId);
            if (sAddressId<=0)
                return false;
            else
            {
                base.ResetSysExData("HdnIsDupAddr", "");
                nodeDup = this.SysEx.SelectSingleNode("//HdnIsDupAddr");
                nodeDup.InnerText = "1";

                base.ResetSysExData("HdnDupAddrId", "");
                nodeAddress = this.SysEx.SelectSingleNode("//HdnDupAddrId");
                nodeAddress.InnerText = Convert.ToString(sAddressId);
                return true;
            }
        }

        private void PopulateEntityTypes()
        {
           
        }
        /// <summary>
        /// This used to Create SearchString 
        /// </summary>
        /// <param name="objAddress">Address object</param>
        /// <param name="oCache"></param>
        /// <returns></returns>
        public string SearchStringForAddress(Address objAddress, LocalCache oCache)
        {
            string strSearchString = string.Empty;
            string sStateCode = string.Empty;
            string sStateDesc = string.Empty;
            
            if (objAddress != null)
            {
                oCache.GetStateInfo(objAddress.State, ref sStateCode, ref sStateDesc);
                strSearchString = objAddress.Addr1 + objAddress.Addr2 + objAddress.Addr3 + objAddress.Addr4;
               // strSearchString += objAddress.City + sStateDesc + objAddress.County + oCache.GetCodeDesc(objAddress.Country) + objAddress.ZipCode.ToString() + objAddress.AddressSeqNum.ToString();
                strSearchString += objAddress.City + sStateDesc + oCache.GetCodeDesc(objAddress.Country) + objAddress.ZipCode.ToString() + objAddress.AddressSeqNum.ToString(); //Remove "County"

                strSearchString = strSearchString.Replace(" ", string.Empty).ToLower();
            }

            return strSearchString;
        }

        //RMA-8753 starts
        /// <summary>
        /// Create Search String when passing Xelements
        /// </summary>
        /// <param name="oElements"></param>
        /// <param name="oCache"></param>
        /// <returns></returns>
        public string CreateSearchString(XmlElement oElements, LocalCache oCache)
        {
            string strSearchString = string.Empty;
            string sStateCode = string.Empty;
            string sStateDesc = string.Empty;
            string sAddr1 = string.Empty;
            string sAddr2 = string.Empty;
            string sAddr3 = string.Empty;
            string sAddr4 = string.Empty;
            string sCity = string.Empty;
            string sState = string.Empty;
            string sCountry = string.Empty;
            string sCounty = string.Empty;
            string sZipCode = string.Empty;
            string sAddressSeqNum = "0"; //set it with 0 because this is int, in search string need to add "0" if sAddressSeqNum=0

           
            if (oElements.SelectSingleNode("//Addr1") != null && !string.IsNullOrEmpty(oElements.SelectSingleNode("//Addr1").InnerText))
            {
                sAddr1 = oElements.SelectSingleNode("//Addr1").InnerText;
            }
            if (oElements.SelectSingleNode("//Addr2") != null && !string.IsNullOrEmpty(oElements.SelectSingleNode("//Addr2").InnerText))
            {
                sAddr2 = oElements.SelectSingleNode("//Addr2").InnerText;
            }
            if (oElements.SelectSingleNode("//Addr3") != null && !string.IsNullOrEmpty(oElements.SelectSingleNode("//Addr3").InnerText))
            {
                sAddr3 = oElements.SelectSingleNode("//Addr3").InnerText;
            }
            if (oElements.SelectSingleNode("//Addr4") != null && !string.IsNullOrEmpty(oElements.SelectSingleNode("//Addr4").InnerText))
            {
                sAddr4 = oElements.SelectSingleNode("//Addr4").InnerText;
            }
            if (oElements.SelectSingleNode("//City") != null && !string.IsNullOrEmpty(oElements.SelectSingleNode("//City").InnerText))
            {
                sCity = oElements.SelectSingleNode("//City").InnerText;
            }
            if (oElements.SelectSingleNode("//State") != null && oElements.SelectSingleNode("//State").Attributes["codeid"] != null && !string.IsNullOrEmpty(oElements.SelectSingleNode("//State").Attributes["codeid"].Value))
            {
                oCache.GetStateInfo(Convert.ToInt32(oElements.SelectSingleNode("//State").Attributes["codeid"].Value), ref sStateCode, ref sStateDesc);
                sState = sStateDesc;
            }
            if (oElements.SelectSingleNode("//Country") != null && oElements.SelectSingleNode("//Country").Attributes["codeid"] != null && !string.IsNullOrEmpty(oElements.SelectSingleNode("//Country").Attributes["codeid"].Value))
            {
                sCountry = oCache.GetCodeDesc(Convert.ToInt32(oElements.SelectSingleNode("//Country").Attributes["codeid"].Value));
            }
            if (oElements.SelectSingleNode("//County") != null && !string.IsNullOrEmpty(oElements.SelectSingleNode("//County").InnerText))
            {
                sCounty = oElements.SelectSingleNode("//County").InnerText;
            }
            if (oElements.SelectSingleNode("//ZipCode") != null && !string.IsNullOrEmpty(oElements.SelectSingleNode("//ZipCode").InnerText))
            {
                sZipCode = oElements.SelectSingleNode("//ZipCode").InnerText;
            }
            if (oElements.SelectSingleNode("//AddressSeqNum") != null && !string.IsNullOrEmpty(oElements.SelectSingleNode("//AddressSeqNum").InnerText))
            {
                sAddressSeqNum = oElements.SelectSingleNode("//AddressSeqNum").InnerText;
            }

            //strSearchString = sAddr1 + sAddr2 + sAddr3 + sAddr4 + sCity + sState + sCounty + sCountry + sZipCode + sAddressSeqNum;
            strSearchString = sAddr1 + sAddr2 + sAddr3 + sAddr4 + sCity + sState + sCountry + sZipCode + sAddressSeqNum; //Remove "County"
            strSearchString = strSearchString.Replace(" ", "").ToLower();
            return strSearchString;

        } //RMA-8753 End
    }
}
