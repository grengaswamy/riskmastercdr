﻿using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
    class PropertyLossForm: DataEntryFormBase
    {

        const string CLASS_NAME = "ClaimXPropertyLoss";
        const string FILTER_KEY_NAME = "ClaimId";
        private ClaimXPropertyLoss objClaimXPropertyLoss { get { return objData as ClaimXPropertyLoss; } }
        private LocalCache objCache { get { return objData.Context.LocalCache; } }
        private string m_Caption = string.Empty;

        public PropertyLossForm(FormDataAdaptor fda): base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

        public override string GetCaption()
        {
            return base.GetCaption();
        }

        public override void InitNew()
        {
            base.InitNew();


            objClaimXPropertyLoss.ClaimId = base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME, false);
            if (objClaimXPropertyLoss.ClaimId > 0)
                (objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + objClaimXPropertyLoss.ClaimId;


        }
        public override void OnUpdateForm()
        {
            ArrayList singleRow = null;
            int iUseClaimProgressNotes = 0;
            base.OnUpdateForm();
            Claim objClaim = (Claim)objClaimXPropertyLoss.Context.Factory.GetDataModelObject("Claim", false);
            objClaim.MoveTo(objClaimXPropertyLoss.ClaimId);

            switch (objClaim.LineOfBusCode)
            {
                case 241:
                    m_SecurityId = RMO_GC_PROPERTYLOSS;
                    break;
                     }

            if (SysEx.DocumentElement.SelectSingleNode("LINEOFBUSCODE") == null)
            {
                CreateSysExData("LINEOFBUSCODE");
                base.ResetSysExData("LINEOFBUSCODE", (objClaimXPropertyLoss.Parent as Claim).LineOfBusCode.ToString());
            }

            //mgaba2:mits 29116
            iUseClaimProgressNotes = objClaim.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode].ClmProgressNotesFlag;

            if (iUseClaimProgressNotes == 0)
            {
                base.AddKillNode("enhancednotes");
            }

            m_Caption = this.GetCaption();

            base.ResetSysExData("SubTitle", m_Caption);
            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singleRow, "id", "formsubtitle");
            base.AddElementToList(ref singleRow, "Text", m_Caption);
            base.m_ModifiedControls.Add(singleRow);

            XmlElement objSysSid = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysSid");
            objSysSid.InnerText = m_SecurityId.ToString();

            //Create new Permission wasnt working
            if (objClaimXPropertyLoss.RowId == 0)
            {
                if (!objClaimXPropertyLoss.Context.RMUser.IsAllowedEx(m_SecurityId, RMO_CREATE))
                {
                    m_CurrentAction = enumFormActionType.None;
                    LogSecurityError(RMO_CREATE);
                    return;
                }
            }
            else
            {//View Permission wasnt working
                if (!objClaimXPropertyLoss.Context.RMUser.IsAllowedEx(m_SecurityId, RMO_VIEW))
                {
                    m_CurrentAction = enumFormActionType.None;
                    LogSecurityError(RMO_VIEW);
                    return;
                }
            }


            //Pass this subtitle value to view (ref'ed from @valuepath).
            base.ResetSysExData("SubTitle", m_Caption);

            if (objClaimXPropertyLoss.AddedByUser == "LSSINF")
            {
                //gagnihotri MITS 16453 05/12/2009
                //XmlElement xmlFormElement = (XmlElement)base.SysView.SelectSingleNode("//form");
                //if (xmlFormElement != null) xmlFormElement.SetAttribute("readonly", "1");
                base.ResetSysExData("FormReadOnly", "Disable");
            }
            else
                base.ResetSysExData("FormReadOnly", "Enable");


        }
        public override void OnUpdateObject()
        {
            base.OnUpdateObject();

        }

        public override void OnValidate(ref bool Cancel)
        {
            bool bError = false;

            if (objClaimXPropertyLoss.ExpenseTo.CompareTo(objClaimXPropertyLoss.ExpenseFrom) < 0)
            {
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                   Globalization.GetString("Validation.MustBeGreaterThanExpenseFrom", base.ClientId), BusinessAdaptorErrorType.Error);

                bError = true;
            }
            Cancel = bError;
        }

      
    }
}
