/**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            *
 **********************************************************************************************
 * 02/10/2014 | 34276  | abhadouria   | changes req for Entity Address Type
 * 02/11/2014 | 34276  | anavinkumars   | Enhancement to Entity related functionality in RMA 
 * 02/27/2015 | RMA-6865        | nshah28      | Swiss Re - Ability to set Effective Date_or_Expiration Date on Address
 **********************************************************************************************/
using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using System.Collections.Generic;
using Riskmaster.Settings;
using System.Text;  //RMA-8753 nshah28 

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Staff Screen.
	/// </summary>
	public class StaffForm : DataEntryFormBase
	{
		const string CLASS_NAME = "MedicalStaff";

		private MedicalStaff MedicalStaff{get{return objData as MedicalStaff;}}

		private LocalCache objCache{get{return objData.Context.LocalCache;}}
        //Added Rakhi for R7:Add Emp Data Elements
        const string INSTANCE_SYSEXDATA_PATH = "/Instance/UI/FormVariables/SysExData"; 
        string sOfficePhone = string.Empty;
        string sHomePhone = string.Empty;
        int iHomeType = 0;
        int iOfficeType = 0;
        //Added Rakhi for R7:Add Emp Data Elements

		public StaffForm(FormDataAdaptor fda):base(fda)
		{
			this.m_ClassName = CLASS_NAME;
		}

        // Anjaneya : MITS 9678
        private void ApplyFormTitle()
        {
            //string sCaption= Entity.Context.LocalCache.GetUserTableName(Entity.EntityTableId) + 

            string sSubTitleText = string.Empty;
            ArrayList singleRow = null;

            //avipinsrivas start : Worked for JIRA - 7767
            if (MedicalStaff.Context.InternalSettings.SysSettings.UseEntityRole)
            {
                int iEntityID = 0;

                if (base.SysEx.SelectSingleNode("//staffeid") != null && !string.IsNullOrEmpty(base.SysEx.SelectSingleNode("//staffeid").InnerText))
                {
                    bool bSuccess;
                    iEntityID = Conversion.CastToType<int>(base.SysEx.SelectSingleNode("//staffeid").InnerText, out bSuccess);
                    if (iEntityID > 0)
                        sSubTitleText += string.Concat(" [ ", MedicalStaff.Context.LocalCache.GetEntityLastFirstName(iEntityID), " ]");
                }
            }


            if (string.IsNullOrEmpty(sSubTitleText) && MedicalStaff != null)
                    sSubTitleText += " [ " + MedicalStaff.Default + " ]";

            //avipinsrivas end
            //Pass this title value to view (ref'ed from @valuepath).
            //Pass this title value to view (ref'ed from @valuepath).
            base.ResetSysExData("SubTitle", sSubTitleText);

            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singleRow, "id", "formsubtitle");
            base.AddElementToList(ref singleRow, "Text", sSubTitleText);
            base.m_ModifiedControls.Add(singleRow);
 
        }
        // Anjaneya : MITS 9678 ends

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
            ArrayList singleRow = null;
            XmlDocument objXmlDoc = null;
			base.OnUpdateForm ();
            //avipinsrivas start : Worked for JIRA - 7767
            if (MedicalStaff.Context.InternalSettings.SysSettings.UseEntityRole && MedicalStaff.StaffEid <= 0)
                base.ResetSysExData("existingrecord", "false");
            else
                base.ResetSysExData("existingrecord", "true");
            if (MedicalStaff.Context.InternalSettings.SysSettings.UseEntityRole)
            {
                if (base.SysEx.SelectSingleNode("//staffeid") != null && !string.IsNullOrEmpty(base.SysEx.SelectSingleNode("//staffeid").InnerText))
                {
                    bool bSuccess;
                    MedicalStaff.StaffEid = Conversion.CastToType<int>(base.SysEx.SelectSingleNode("//staffeid").InnerText, out bSuccess);
                }
                else if (MedicalStaff.StaffEid > 0)
                    base.ResetSysExData("staffeid", MedicalStaff.StaffEid.ToString());
            }
            //avipinsrivas end
            //smishra25: MITS 24395, MITS 13748 implementation was missing
            //nadim for 13748,added to hide/unhide SSN field
            if (!MedicalStaff.Context.RMUser.IsAllowedEx(FormBase.RMO_MED_STAFF, FormBase.RMO_MAINT_VIEW_SSN))
            {
                base.AddKillNode("taxid");

            }
            //nadim for 13748

            //mbahl3 mits 30224


            string sPresentdate = String.Empty;
            if (MedicalStaff.StaffEntity.EntityId <= 0)
            {
                if (MedicalStaff.StaffEntity.EffectiveDate == String.Empty)
                {
                    sPresentdate = DateTime.Now.ToShortDateString();
                    MedicalStaff.StaffEntity.EffectiveDate = sPresentdate;
                }
            }
            //mbahl3 mits 30224


            //02/26/2010 MITS: kuladeep code for checking any policy is de-active or not START
            //Start-07/22/2010: Sumit - Removed Hardcoded value and commented this object
            //ColLobSettings objLobSettings = null;
            //objLobSettings = new ColLobSettings(MedicalStaff.Context.DbConn.ConnectionString);

            //04/15/2010 Change by Kuladeep MITS:20333 
            //if (objLobSettings[241].UseEnhPolFlag == 0 || objLobSettings[242].UseEnhPolFlag == 0 || objLobSettings[243].UseEnhPolFlag == 0 || objLobSettings[845].UseEnhPolFlag == 0)
            //if (objLobSettings[241].UseEnhPolFlag == 0 && objLobSettings[242].UseEnhPolFlag == 0 && objLobSettings[243].UseEnhPolFlag == 0 && objLobSettings[845].UseEnhPolFlag == 0)
            if (MedicalStaff.Context.InternalSettings.ColLobSettings[this.objCache.GetCodeId("PC", "LINE_OF_BUSINESS")].UseEnhPolFlag == 0 &&
                MedicalStaff.Context.InternalSettings.ColLobSettings[this.objCache.GetCodeId("GC", "LINE_OF_BUSINESS")].UseEnhPolFlag == 0 &&
                MedicalStaff.Context.InternalSettings.ColLobSettings[this.objCache.GetCodeId("WC", "LINE_OF_BUSINESS")].UseEnhPolFlag == 0 &&
                MedicalStaff.Context.InternalSettings.ColLobSettings[this.objCache.GetCodeId("VA", "LINE_OF_BUSINESS")].UseEnhPolFlag == 0)
            //End: Sumit
            {
                base.CreateSysExData("IsAnyPolicyDeActive", "0");
            }
            else
            {
                base.CreateSysExData("IsAnyPolicyDeActive", "-1");
                base.AddReadOnlyNode("primarypolicyid");
            }
            //02/26/2010 MITS: kuladeep code for checking any policy is de-active or not END

            //Added Rakhi for R7:Add Emp Data Elements
            XmlNode objPhoneTypeList = base.SysEx.SelectSingleNode("//PhoneTypeList");
            XmlNode objPhoneNumbers = base.SysEx.SelectSingleNode("//PhoneNumbers");

            objXmlDoc = new XmlDocument();
            objXmlDoc = CommonForm.PopulatePhoneTypes(MedicalStaff.Context.DbConn.ConnectionString, ref iOfficeType, ref iHomeType, base.Adaptor.userLogin.objUser.NlsCode); //Aman ML Change
            if (objPhoneTypeList != null)
                base.SysEx.DocumentElement.RemoveChild(objPhoneTypeList);
            base.SysEx.DocumentElement.AppendChild(base.SysEx.ImportNode(objXmlDoc.SelectSingleNode("PhoneTypeList"), true));

            if (objPhoneNumbers == null)
            {
                string sPhoneNumbers = PopulatePhoneNumbers();
                base.ResetSysExData("PhoneNumbers", sPhoneNumbers);
            }
            else
            {
                objPhoneTypeList = base.SysEx.SelectSingleNode("//PhoneTypeList");
                if (objPhoneTypeList != null)
                    CommonForm.SetPhoneNumbers(objPhoneNumbers, objPhoneTypeList);

            }
            if (base.SysEx.SelectSingleNode("//PhoneType1") == null)
            {
                base.ResetSysExData("PhoneType1", iOfficeType.ToString());
            }
            if (base.SysEx.SelectSingleNode("//PhoneNumber1") == null)
            {
                base.ResetSysExData("PhoneNumber1", sOfficePhone);
            }
            if (base.SysEx.SelectSingleNode("//PhoneType2") == null)
            {
                base.ResetSysExData("PhoneType2", iHomeType.ToString());
            }
            if (base.SysEx.SelectSingleNode("//PhoneNumber2") == null)
            {
                base.ResetSysExData("PhoneNumber2", sHomePhone);
            }
            //Added Rakhi for R7:Add Emp Data Elements
            //Added Rakhi for R7:Add Employee Data Elements
            bool bMulAddresses = this.objData.Context.InternalSettings.SysSettings.UseMultipleAddresses && !this.objData.Context.InternalSettings.SysSettings.UseEntityRole;        //avipinsrivas start : Worked for JIRA - 7767
            if (bMulAddresses)
            {
                base.AddDisplayNode("addressesinfo");
                SetPrimaryAddressReadOnly();
                bool bIsFirstRecord = false;
                XmlDocument objEntityXAddressInfoXmlDoc = GetEntityXAddressInfo();
                XmlNode objOldEntityXAddressInfoNode = SysEx.DocumentElement.SelectSingleNode(objEntityXAddressInfoXmlDoc.DocumentElement.LocalName);
                if (objOldEntityXAddressInfoNode != null)
                {
                    SysEx.DocumentElement.RemoveChild(objOldEntityXAddressInfoNode);
                }
                XmlNode objEntityXAddressInfoNode =
                    SysEx.ImportNode(objEntityXAddressInfoXmlDoc.DocumentElement, true);
                SysEx.DocumentElement.AppendChild(objEntityXAddressInfoNode);

                if (SysEx.DocumentElement.SelectSingleNode("IsPostBack") == null)
                    CreateSysExData("IsPostBack");
                if (SysEx.DocumentElement.SelectSingleNode("EntityXOperatingAsGrid_RowDeletedFlag") != null)
                {
                    if (SysEx.DocumentElement.SelectSingleNode("EntityXOperatingAsGrid_RowDeletedFlag").InnerText == "true")
                    {
                        if (base.m_fda.SafeFormVariableParamText("SysCmd") == "7")
                        {
                            base.ResetSysExData("IsPostBack", "1");
                        }
                    }
                }
                if (this.MedicalStaff.StaffEntity.EntityXAddressesList.Count == 0)
                    bIsFirstRecord = true;

                if (base.SysEx.SelectSingleNode("//PrimaryFlag") == null)
                    base.ResetSysExData("PrimaryFlag", "false");

                if (base.SysEx.SelectSingleNode("//PrimaryRow") == null)
                    base.ResetSysExData("PrimaryRow", "");

                if (base.SysEx.SelectSingleNode("//PrimaryAddressChanged") == null)
                    base.ResetSysExData("PrimaryAddressChanged", "false");

                base.ResetSysExData("EntityXAddressesInfoSelectedId", "");
                base.ResetSysExData("EntityXAddressesInfoGrid_RowDeletedFlag", "false");
                base.ResetSysExData("EntityXAddressesInfoGrid_RowAddedFlag", "false");
                base.ResetSysExData("UseMultipleAddresses", bMulAddresses.ToString());
                base.ResetSysExData("IsFirstRecord", bIsFirstRecord.ToString());
            }
            else
            {
                base.AddKillNode("addressesinfo");
                base.AddKillNode("EntityXAddressesInfoGrid");//Mits 22246:GridTitle and Buttons getting visible in topdown Layout
            }
            //if (base.SysEx.SelectSingleNode("//PopupGridRowsDeleted") == null || base.m_fda.SafeFormVariableParamText("SysCmd") == "5")
            //    base.ResetSysExData("PopupGridRowsDeleted", "");

            XmlNode objRecordSummary = base.SysEx.SelectSingleNode("//IsRecordSummary");
            if (objRecordSummary != null && objRecordSummary.InnerText == "true")
            {
                CommonForm.DisplayPhoneNumbersRecordSummary(base.SysEx, base.SysView);
            }
            //Added Rakhi for R7:Add Employee Data Elements
            ApplyFormTitle();
			if(!MedicalStaff.Context.RMUser.IsAllowedEx(FormBase.RMO_SEARCH,FormBase.RMO_MEDICAL_STAFF_SEARCH))
				base.AddKillNode("search");
			if(!MedicalStaff.Context.RMUser.IsAllowedEx(FormBase.RMO_LOOKUP,FormBase.RMO_MEDICAL_STAFF_SEARCH))
				base.AddKillNode("lookup");

            //PJS MITS # 16563 - Adding Lookupdata attributes for policynumberlookup
            singleRow = new ArrayList();
            //Start-07/22/2010: Sumit - No need to check for Enhanced Policy as Insurance Policy field remain disabled when it is active for any LOB.
             // Naresh Code Changed for Saving the Enhanced Policy Ids attached with the Claim
            //if (MedicalStaff.Context.InternalSettings.SysSettings.UseEnhPolFlag == -1)
            //{
            //    base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.PolicyNumberLookup.ToString());
            //    base.AddElementToList(ref singleRow, "id", "primarypolicyid");
            //    base.AddElementToList(ref singleRow, "idref", "/Instance/MedicalStaff/PrimaryPolicyId");
            //    base.AddElementToList(ref singleRow, "ref", "/Instance/MedicalStaff/PrimaryPolicyId/@defaultdetail");
            //    base.AddElementToList(ref singleRow, "type", "enhpolicylookup");
            //    //02/26/2010 MITS: Kuladeep code changed for Policy Tracking rather than  Policy Search START
            //    //string sParameters= "return lookupData('primarypolicyid','policyenh',-1,'primarypolicyid',20)";
            //    string sParameters = "return lookupData('primarypolicyid','policy',-1,'primarypolicyid',9)";
            //    //02/26/2010 MITS: Kuladeep code changed for Policy Tracking rather than  Policy Search END
            //    base.AddElementToList(ref singleRow, "onclientclickparameters", sParameters);
            //}
            //else
            //{
                base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.PolicyNumberLookup.ToString());
                base.AddElementToList(ref singleRow, "id", "primarypolicyid");
                base.AddElementToList(ref singleRow, "idref", "Instance/MedicalStaff/PrimaryPolicyId");
                base.AddElementToList(ref singleRow, "ref", "/Instance/MedicalStaff/PrimaryPolicyId/@defaultdetail");
                base.AddElementToList(ref singleRow, "type", "policynumberlookup");
                string sParameters = "return lookupData('primarypolicyid','policy',-1,'primarypolicyid',9)";
                base.AddElementToList(ref singleRow, "onclientclickparameters", sParameters);
            //}
            //End: Sumit
            //Start rsushilaggar Entity Payment Approval (only execute this block if the Use Entity approval check box is "off") MITS 20606 05/05/2010
            if (!MedicalStaff.Context.InternalSettings.SysSettings.UseEntityApproval)
            {
                base.AddKillNode("entityapprovalstatuscode");
                base.AddKillNode("rejectreasontext");
            }
            base.m_ModifiedControls.Add(singleRow);
          
            //Deb:added for mediaview
            if (MedicalStaff.Context.InternalSettings.SysSettings.UseMediaViewInterface)
            {
                base.AddKillNode("attachdocuments");
                base.AddKillNode("attach");
            }
            //Deb:added for mediaview

            //MITS:34276 Starts-- Entity ID Type View Permission 
            if (!MedicalStaff.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_IDTYPE_VIEW))
            {
                base.AddKillNode("TABSentityidtypeinfo");
                base.AddKillNode("TBSPentityidtypeinfo");
            }
            //MITS:34276 Ends 

            //Added by Manika for R8 enhancement of EFT and Withholding
            if (!MedicalStaff.Context.RMUser.IsAllowedEx(FormBase.RMO_STAFFMAINT_EFT))
            {
                base.AddKillNode("BankingInfo");
            }
            if (!MedicalStaff.Context.RMUser.IsAllowedEx(FormBase.RMO_STAFFMAINT_WITHHOLDING))
            {
                base.AddKillNode("Withholding");
            }
            //End Manika
	        //MITS:34276 Starts Entity ID Type Tab Starts
            XmlDocument objEntityIDTypeAsXmlDoc = GetEntityIdTypeInfoData();

            XmlNode objOldEntityIdTypeAsNode = SysEx.DocumentElement.SelectSingleNode(objEntityIDTypeAsXmlDoc.DocumentElement.LocalName);
            if (objOldEntityIdTypeAsNode != null)
            {
                SysEx.DocumentElement.RemoveChild(objOldEntityIdTypeAsNode);
            }
            XmlNode objEntityIdTypeAsNode = SysEx.ImportNode(objEntityIDTypeAsXmlDoc.DocumentElement, true);
            SysEx.DocumentElement.AppendChild(objEntityIdTypeAsNode);

            base.ResetSysExData("EntityXEntityIDTypeSelectedId", "");
            base.ResetSysExData("EntityXEntityIDTypeGrid_RowDeletedFlag", "false");
            base.ResetSysExData("EntityXEntityIDTypeGrid_RowAddedFlag", "false");
            //MITS:34276 End

            ERSetting();            //avipinsrivas start : Worked for JIRA - 7767
		}

        //avipinsrivas start : Worked for JIRA - 7767
        private void ERSetting()
        {
            if (MedicalStaff.Context.InternalSettings.SysSettings.UseEntityRole)
            {
                //Toolbars
                base.AddKillNode("lookup");
                base.AddKillNode("attach");
                base.AddKillNode("diary");
                //base.AddKillNode("mailmerge");
                //base.AddKillNode("recordsummary");            //avipinsrivas Start : Worked for 13947 (Issue of 13196 - Epic 7767)
                base.AddKillNode("withholding");
                base.AddKillNode("BankingInfo");
                //End Toolbars

                //Controls
                //First Tab Start
                base.AddKillNode("taxid");
                base.AddKillNode("lastname");
                base.AddKillNode("freezepayments");
                base.AddKillNode("firstname");
                base.AddKillNode("middlename");
                base.AddKillNode("addr1");
                base.AddKillNode("addr2");
                base.AddKillNode("addr3");
                base.AddKillNode("addr4");
                base.AddKillNode("city");
                base.AddKillNode("faxnumber");
                base.AddKillNode("stateid");
                base.AddKillNode("zipcode");
                base.AddKillNode("county");
                base.AddKillNode("countrycode");
                base.AddKillNode("entityapprovalstatuscode");
                base.AddKillNode("rejectreasontext");
                base.AddKillNode("effectivedate");
                base.AddKillNode("expirationdate");
                //avipinsrivas Start : Worked for 13953 (Issue of 13196 - Epic 7767)
                base.AddKillNode("phone1");
                base.AddKillNode("phone2");
                //avipinsrivas end
                //First Tab End

                //Second Tab (Staff Info) Start
                base.AddKillNode("abbreviation");
                base.AddKillNode("alsoknownas");
                base.AddKillNode("sexcode");
                base.AddKillNode("labelLicense");
                base.AddKillNode("npinumber");
                base.AddKillNode("birthdate");
                //Second Tab (Staff Info) End                
                //End Controls

                //Tabs
                base.AddKillNode("addressesinfo");
                base.AddKillNode("entityidtypeinfo");
                //End Tabs
            }
            else
                base.AddKillNode("BackToParent");
        }
        //avipinsrivas End

		public override void OnValidate(ref bool Cancel)
		{
			bool bError = false;
			
			// Perform data validation
			string sToday = Conversion.ToDbDate(System.DateTime.Now);

			if (MedicalStaff.StaffEntity.BirthDate!="" && MedicalStaff.StaffEntity.BirthDate.CompareTo(sToday)>0)
			{
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                    String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.BirthDate", base.ClientId), System.DateTime.Now.ToShortDateString()),
					BusinessAdaptorErrorType.Error);

				bError = true;
			}
            bool bMulAddresses = this.objData.Context.InternalSettings.SysSettings.UseMultipleAddresses && !this.objData.Context.InternalSettings.SysSettings.UseEntityRole;        //avipinsrivas start : Worked for JIRA - 7767
             if (bMulAddresses)
             {
                 XmlNode PrimaryAddress = base.SysEx.DocumentElement.SelectSingleNode("PrimaryFlag");
                 if (PrimaryAddress != null && MedicalStaff.StaffEntity.EntityXAddressesList.Count != 0 && PrimaryAddress.InnerText.ToLower() == "false")
                 {
                     Errors.Add("PrimaryAddressError", "No Address Marked as Primary for this Entity", BusinessAdaptorErrorType.Error);
                     bError = true;
                  }
             }
			// Return true if there were validation errors
			
            //mbahl3 mits 30224
             if (!string.IsNullOrEmpty(MedicalStaff.StaffEntity.EffectiveDate) && !string.IsNullOrEmpty(MedicalStaff.StaffEntity.ExpirationDate))
             {
                 if (MedicalStaff.StaffEntity.ExpirationDate.CompareTo(MedicalStaff.StaffEntity.EffectiveDate) < 0)
                 {
                     Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                         Globalization.GetString("Validation.ExpirationDateMustBeGreaterThanEffectiveDate", base.ClientId),
                         BusinessAdaptorErrorType.Error);

                     bError = true;
                     //Cancel = true;
                     //return;
                 }
                 
             }
             
            //mbahl3 mits 30224

             //MITS 34276 Starts:Check for Unique Entity ID Number and Entity ID Type Combination
             List<EntityXEntityIDType> lstEntityXIDType = new List<EntityXEntityIDType>();
             bool bDupEntityIDType = false;
             foreach (EntityXEntityIDType objEntityIDType in MedicalStaff.StaffEntity.EntityXEntityIDTypeList)
             {
                 if (!bDupEntityIDType && Riskmaster.Common.CommonFunctions.IsEntityIDNumberExists(MedicalStaff.StaffEntity.Context.RMDatabase.ConnectionString, objEntityIDType.IdNumRowId, objEntityIDType.Table, objEntityIDType.IdNum, objEntityIDType.IdType))
                 {
                     Errors.Add("EntityIDNumberError", "Entity ID Number with Entity ID Type already exists",
                         BusinessAdaptorErrorType.Error);
                     bError = true;
                 }
                 if (!bDupEntityIDType && lstEntityXIDType.Exists(x => x.IdType == objEntityIDType.IdType && x.IdNum == objEntityIDType.IdNum))
                 {
                     Errors.Add("EntityIDNumberError", "Entity ID Number with Entity ID Type already exists",
                          BusinessAdaptorErrorType.Error);
                     Cancel = true;
                     bDupEntityIDType = true;
                 }
                 else
                     lstEntityXIDType.Add(objEntityIDType);
             }
             //MITS 34276 Ends:Check for Unique Entity ID Number and Entity ID Type Combination
             Cancel = bError;
            //mbahl3 mits 30224
		}
        public override void OnUpdateObject()
        {
            base.OnUpdateObject();
            //Added Rakhi for R7:Add Emp Data Elements
            #region "Updating Address Info Object"

             bool bMulAddresses = this.objData.Context.InternalSettings.SysSettings.UseMultipleAddresses;
             //MITS:34276 Starts-- changes for Entity ID Type
             XmlAttribute objTypeAttributeNode = null;
             bool bIsNew = false;
             //MITS:34276 Ends
            //avipinsrivas start : Worked for JIRA - 7767
             if (!this.objData.Context.InternalSettings.SysSettings.UseEntityRole)
             {
                 if (bMulAddresses)
                 {
                     SortedList objEntityXAddressesInfoList = new SortedList();
                     SortedList objAddressesInfoList = new SortedList();
                     //XmlDocument objAddressXPhoneInfoDoc = null;
                     bool bIsSuccess = false;
                     //MITS:34276 starts-- commented and shifted up 
                     //XmlAttribute objTypeAttributeNode = null;
                     //bool bIsNew=false;
                     //MITS:34276 ends 
                     int iAddressIdKey = -1;
                     int iRowIdKey = -1;//AA//RMA-8753 nshah28(Added by ashish)
                     XmlDocument objEntityXAddressXmlDoc = null;
                     XmlElement objEntityXAddressRootElement = null;


                     int iEntityXAddressesInfoSelectedId =
                         base.GetSysExDataNodeInt("EntityXAddressesInfoSelectedId", true);

                     string sEntityXAddressesInfoRowAddedFlag =
                         base.GetSysExDataNodeText("EntityXAddressesInfoGrid_RowAddedFlag", true);
                     bool bEntityXAddressesInfoRowAddedFlag =
                         sEntityXAddressesInfoRowAddedFlag.ToUpper().CompareTo("TRUE") == 0 ? true : false;

                     string sEntityXAddressesInfoRowDeletedFlag =
                         base.GetSysExDataNodeText("EntityXAddressesInfoGrid_RowDeletedFlag", true);
                     bool bEntityXAddressesInfoRowDeletedFlag =
                         sEntityXAddressesInfoRowDeletedFlag.ToUpper().CompareTo("TRUE") == 0 ? true : false;

                     XmlNode objEntityXAddressesInfoNode = base.SysEx.DocumentElement.SelectSingleNode("EntityXAddressesInfo");

                     if (objEntityXAddressesInfoNode != null)
                     {
                         // Loop through data for all rows of the grid
                         foreach (XmlNode objOptionNode in objEntityXAddressesInfoNode.SelectNodes("option"))
                         {
                             objTypeAttributeNode = objOptionNode.Attributes["type"];
                             bIsNew = false;
                             if (objTypeAttributeNode != null && objTypeAttributeNode.InnerText.Equals("new") == true)
                             {
                                 // This is the 'extra' hidden <option> for capturing a new grid row data
                                 bIsNew = true;
                             }
                             if ((bIsNew == false) || (bIsNew == true && bEntityXAddressesInfoRowAddedFlag == true))
                             {
                                 objEntityXAddressXmlDoc = new XmlDocument();
                                 objEntityXAddressRootElement = objEntityXAddressXmlDoc.CreateElement("EntityXAddresses");
                                 objEntityXAddressXmlDoc.AppendChild(objEntityXAddressRootElement);
                                 #region AddressXPhone Info Grid
                                 //XmlNode objAddressNode = objOptionNode.SelectSingleNode("AddressXPhoneInfo");
                                 //if (objOptionNode != null && !(bIsNew == false && bEntityXAddressesInfoRowDeletedFlag == true && iAddressIdKey == iEntityXAddressesInfoSelectedId))
                                 //{

                                 //    XmlElement objAddressXPhoneRootElement = null;
                                 //    int iPhoneId = 0;
                                 //    bool bResult = false;
                                 //    string sKey = string.Empty;
                                 //    foreach (XmlNode objPhoneNode in objAddressNode.SelectNodes("option"))
                                 //    {
                                 //        objAddressXPhoneInfoDoc = new XmlDocument();
                                 //        objAddressXPhoneRootElement = objAddressXPhoneInfoDoc.CreateElement("AddressXPhoneInfo");
                                 //        objAddressXPhoneInfoDoc.AppendChild(objAddressXPhoneRootElement);
                                 //        objPhoneNode.SelectSingleNode("ContactId").InnerText = objOptionNode.SelectSingleNode("ContactId").InnerText;
                                 //        objAddressXPhoneRootElement.InnerXml = objPhoneNode.InnerXml;
                                 //        bResult = Int32.TryParse(objAddressXPhoneRootElement.SelectSingleNode("PhoneId").InnerText, out iPhoneId);
                                 //        if (iPhoneId != 0)
                                 //        {
                                 //            sKey = objPhoneNode.SelectSingleNode("ContactId").InnerText + "_" + objAddressXPhoneRootElement.SelectSingleNode("PhoneId").InnerText;
                                 //            objAddressesInfoList.Add(sKey, objAddressXPhoneInfoDoc);
                                 //        }
                                 //    }
                                 //}
                                 //objOptionNode.RemoveChild(objAddressNode);
                                 #endregion //AddressXPhone Info Grid

                                 objEntityXAddressRootElement.InnerXml = objOptionNode.InnerXml;

                                 //AA //RMA-8753 nshah28 start

                                 string strSearchString = string.Empty;
                                 string sStateCode = string.Empty;
                                 string sStateDesc = string.Empty;
                                 string sCountry = string.Empty;

                                 AddressForm objAddressForm = new AddressForm(m_fda);

                                 strSearchString = objAddressForm.CreateSearchString(objEntityXAddressRootElement, objData.Context.LocalCache);
                                 //RMA-8753 nshah28(Added by ashish)

                                 XmlElement objRootElement = objEntityXAddressXmlDoc.CreateElement("SearchString");
                                 objRootElement.InnerText = strSearchString; //change by nshah28
                                 objEntityXAddressRootElement.AppendChild(objRootElement);
                                 //RMA-8753 nshah28(Added by ashish) END

                                 //iAddressIdKey =
                                 //    Conversion.CastToType<int>(objEntityXAddressRootElement.SelectSingleNode("AddressId").InnerText, out bIsSuccess);

                                 iRowIdKey =
                                    Conversion.CastToType<int>(objEntityXAddressRootElement.SelectSingleNode("RowId").InnerText, out bIsSuccess);

                                 if ((bIsNew == true && bEntityXAddressesInfoRowAddedFlag == true) ||
                                     (iAddressIdKey < 0 && objEntityXAddressRootElement.SelectSingleNode("EntityId").InnerText == ""))
                                     objEntityXAddressRootElement.SelectSingleNode("EntityId").InnerText = "0";
                                 else
                                     objEntityXAddressRootElement.SelectSingleNode("EntityId").InnerText = MedicalStaff.StaffEntity.EntityId.ToString();
                                 //RMA-8753 nshah28 END

                                 //Marking Changed Address as Primary Address
                                 XmlNode objPrimaryAddressChanged = base.SysEx.SelectSingleNode("//PrimaryAddressChanged");
                                 XmlNode objNewPrimaryAddress = objEntityXAddressRootElement.SelectSingleNode("NewPrimaryRecord"); //For identification of New Primary Address
                                 if (objPrimaryAddressChanged != null && objPrimaryAddressChanged.InnerText.ToLower() == "true")
                                 {
                                     if (objNewPrimaryAddress != null)
                                     {
                                         XmlNode objPrimaryAddress = objEntityXAddressRootElement.SelectSingleNode("PrimaryAddress");
                                         if (objPrimaryAddress != null)
                                         {
                                             if (objNewPrimaryAddress.InnerText.ToLower() == "true")
                                             {
                                                 objPrimaryAddress.InnerText = "True";
                                             }

                                             else
                                             {
                                                 objPrimaryAddress.InnerText = "False";
                                             }
                                         }
                                     }
                                 }
                                 if (objNewPrimaryAddress != null)
                                 {
                                     objEntityXAddressRootElement.RemoveChild(objNewPrimaryAddress); //Since it is not a Datamodel Property,Removing it before the processing.
                                 }
                                 //Marking Changed Address as Primary Address

                                 if (bIsNew == false && bEntityXAddressesInfoRowDeletedFlag == true && iRowIdKey == iEntityXAddressesInfoSelectedId) //RMA-8753 nshah28
                                 {
                                     // Aditya - This particular Tpp has been deleted. 
                                     continue;
                                 }
                                 objEntityXAddressesInfoList.Add(iRowIdKey, objEntityXAddressXmlDoc);//AA//RMA-8753 nshah28(Added by ashish)
                             }
                         }
                     }
                     //AA//RMA-8753 nshah28
                     //foreach (EntityXAddresses objEntityXAddressesInfo in MedicalStaff.StaffEntity.EntityXAddressesList)
                     //{
                     //    if (base.m_fda.SafeFormVariableParamText("SysCmd") == "7" &&
                     //        (objEntityXAddressesInfo.AddressId < 0 || (bEntityXAddressesInfoRowDeletedFlag == true && objEntityXAddressesInfo.AddressId == iEntityXAddressesInfoSelectedId)))
                     //    {
                     //        MedicalStaff.StaffEntity.EntityXAddressesList.Remove(objEntityXAddressesInfo.AddressId);
                     //    }
                     //    else if (objEntityXAddressesInfo.AddressId < 0)
                     //        MedicalStaff.StaffEntity.EntityXAddressesList.Remove(objEntityXAddressesInfo.AddressId);
                     //}

                     foreach (EntityXAddresses objEntityXAddressesInfo in MedicalStaff.StaffEntity.EntityXAddressesList)
                     {
                         if (base.m_fda.SafeFormVariableParamText("SysCmd") == "7" &&
                             (objEntityXAddressesInfo.RowId < 0 || (bEntityXAddressesInfoRowDeletedFlag == true && objEntityXAddressesInfo.RowId == iEntityXAddressesInfoSelectedId)))
                         {
                             MedicalStaff.StaffEntity.EntityXAddressesList.Remove(objEntityXAddressesInfo.RowId);
                         }
                         else if (objEntityXAddressesInfo.RowId < 0)
                             MedicalStaff.StaffEntity.EntityXAddressesList.Remove(objEntityXAddressesInfo.RowId);
                     }
                     //AA//RMA-8753 nshah28 END
                     EntityXAddresses objTmpEntityXAddressesInfo = null;
                     XmlDocument objTmpEntityXAddressXMLDoc = null;
                     //AddressXPhoneInfo objTmpAddressXPhoneInfo = null;
                     //XmlDocument objTmpAddressXPhoneXMLDoc = null;
                     //string sOrgAddressId = string.Empty;
                     //Parijat:Mits 9610
                     ArrayList arrAddressIds = new ArrayList();
                     ArrayList arrRowIds = new ArrayList(); //RMA-8753 nshah28 
                     //
                     for (int iListIndex = 0; iListIndex < objEntityXAddressesInfoList.Count; iListIndex++)
                     {
                         objTmpEntityXAddressXMLDoc = (XmlDocument)objEntityXAddressesInfoList.GetByIndex(iListIndex);
                         //sOrgAddressId = objTmpEntityXAddressXMLDoc.DocumentElement.SelectSingleNode("AddressId").InnerText.Trim();
                         //AA//RMA-8753 nshah28 STARt
                         if (objTmpEntityXAddressXMLDoc.DocumentElement.SelectSingleNode("RowId").InnerText.Trim() == "" || Conversion.CastToType<int>(objTmpEntityXAddressXMLDoc.DocumentElement.SelectSingleNode("RowId").InnerText, out bIsSuccess) < 0)
                         {
                             objTmpEntityXAddressXMLDoc.DocumentElement.SelectSingleNode("RowId").InnerText = ((-1 * iListIndex) - 1).ToString();
                             objTmpEntityXAddressesInfo = MedicalStaff.StaffEntity.EntityXAddressesList.AddNew();
                         }
                         else
                             objTmpEntityXAddressesInfo = MedicalStaff.StaffEntity.EntityXAddressesList[Conversion.CastToType<int>(objTmpEntityXAddressXMLDoc.DocumentElement.SelectSingleNode("RowId").InnerText, out bIsSuccess)];
                         //Parijat:Mits 9610
                         arrAddressIds.Add(objTmpEntityXAddressXMLDoc.DocumentElement.SelectSingleNode("AddressId").InnerText);
                         //objTmpEntityXAddressesInfo.PopulateObject(objTmpEntityXAddressXMLDoc);
                         arrRowIds.Add(objTmpEntityXAddressXMLDoc.DocumentElement.SelectSingleNode("RowId").InnerText);//RMA-8753 nshah28 
                         //AA//RMA-8753 nshah28 END
                         //RMA 8753 Ashish Ahuja For Address Master Enhancement Start
                         if (objTmpEntityXAddressXMLDoc.SelectSingleNode("//RowId").InnerText != String.Empty)
                             objTmpEntityXAddressesInfo.RowId = Convert.ToInt32(objTmpEntityXAddressXMLDoc.SelectSingleNode("//RowId").InnerText);
                         if (objTmpEntityXAddressXMLDoc.SelectSingleNode("//AddressType").Attributes["codeid"].Value != String.Empty)
                             objTmpEntityXAddressesInfo.AddressType = Convert.ToInt32(objTmpEntityXAddressXMLDoc.SelectSingleNode("//AddressType").Attributes["codeid"].Value);
                         if (objTmpEntityXAddressXMLDoc.SelectSingleNode("//PrimaryAddress").InnerText != String.Empty)
                             objTmpEntityXAddressesInfo.PrimaryAddress = Convert.ToBoolean(objTmpEntityXAddressXMLDoc.SelectSingleNode("//PrimaryAddress").InnerText) ? -1 : 0;
                         //objTmpEntityXAddressesInfo.PrimaryAddress = Convert.ToInt32(objTmpEntityXAddressXMLDoc.SelectSingleNode("//PrimaryAddress").InnerText);
                         objTmpEntityXAddressesInfo.Address.Addr1 = objTmpEntityXAddressXMLDoc.SelectSingleNode("//Addr1").InnerText;
                         objTmpEntityXAddressesInfo.Address.Addr2 = objTmpEntityXAddressXMLDoc.SelectSingleNode("//Addr2").InnerText;
                         objTmpEntityXAddressesInfo.Address.Addr3 = objTmpEntityXAddressXMLDoc.SelectSingleNode("//Addr3").InnerText;
                         objTmpEntityXAddressesInfo.Address.Addr4 = objTmpEntityXAddressXMLDoc.SelectSingleNode("//Addr4").InnerText;
                         objTmpEntityXAddressesInfo.Address.City = objTmpEntityXAddressXMLDoc.SelectSingleNode("//City").InnerText;
                         if (objTmpEntityXAddressXMLDoc.SelectSingleNode("//Country").Attributes["codeid"].Value != String.Empty)
                             objTmpEntityXAddressesInfo.Address.Country = Convert.ToInt32(objTmpEntityXAddressXMLDoc.SelectSingleNode("//Country").Attributes["codeid"].Value);
                         if (objTmpEntityXAddressXMLDoc.SelectSingleNode("//State").Attributes["codeid"].Value != String.Empty)
                             objTmpEntityXAddressesInfo.Address.State = Convert.ToInt32(objTmpEntityXAddressXMLDoc.SelectSingleNode("//State").Attributes["codeid"].Value);
                         objTmpEntityXAddressesInfo.Email = objTmpEntityXAddressXMLDoc.SelectSingleNode("//Email").InnerText;
                         objTmpEntityXAddressesInfo.Fax = objTmpEntityXAddressXMLDoc.SelectSingleNode("//Fax").InnerText; ;
                         objTmpEntityXAddressesInfo.Address.County = objTmpEntityXAddressXMLDoc.SelectSingleNode("//County").InnerText;
                         objTmpEntityXAddressesInfo.Address.ZipCode = objTmpEntityXAddressXMLDoc.SelectSingleNode("//ZipCode").InnerText;
                         objTmpEntityXAddressesInfo.EffectiveDate = objTmpEntityXAddressXMLDoc.SelectSingleNode("//EffectiveDate").InnerText;
                         objTmpEntityXAddressesInfo.ExpirationDate = objTmpEntityXAddressXMLDoc.SelectSingleNode("//ExpirationDate").InnerText;

                         objTmpEntityXAddressesInfo.Address.SearchString = objTmpEntityXAddressXMLDoc.SelectSingleNode("//SearchString").InnerText;
                         int iAddressId = CommonFunctions.CheckAddressDuplication(objTmpEntityXAddressXMLDoc.SelectSingleNode("//SearchString").InnerText, MedicalStaff.Context.RMDatabase.ConnectionString, base.ClientId);
                         objTmpEntityXAddressesInfo.AddressId = iAddressId;
                         objTmpEntityXAddressesInfo.Address.AddressId = iAddressId;
                         //RMA 8753 Ashish Ahuja For Address Master Enhancement End

                         #region Phone Info Details
                         //int iPhoneIndex = 0;
                         //for (int iCount = 0; iCount < objAddressesInfoList.Count; iCount++)
                         //{
                         //    objTmpAddressXPhoneXMLDoc = (XmlDocument)objAddressesInfoList.GetByIndex(iCount);
                         //    if (sOrgContactId == objTmpAddressXPhoneXMLDoc.DocumentElement.SelectSingleNode("ContactId").InnerText.Trim())
                         //    {
                         //        if (objTmpAddressXPhoneXMLDoc.DocumentElement.SelectSingleNode("PhoneId").InnerText.Trim() == "" || Conversion.ConvertStrToInteger(objTmpAddressXPhoneXMLDoc.DocumentElement.SelectSingleNode("PhoneId").InnerText) < 0)
                         //        {
                         //            objTmpAddressXPhoneXMLDoc.DocumentElement.SelectSingleNode("PhoneId").InnerText = ((-1 * iPhoneIndex) - 1).ToString();
                         //            objTmpAddressXPhoneInfo = objTmpEntityXAddressesInfo.AddressXPhoneInfoList.AddNew();
                         //            iPhoneIndex++;
                         //        }
                         //        else
                         //            objTmpAddressXPhoneInfo = objTmpEntityXAddressesInfo.AddressXPhoneInfoList[Conversion.ConvertStrToInteger(objTmpAddressXPhoneXMLDoc.DocumentElement.SelectSingleNode("PhoneId").InnerText)];
                         //        objTmpAddressXPhoneInfo.PopulateObject(objTmpAddressXPhoneXMLDoc);
                         //        objTmpAddressXPhoneInfo.ContactId = objTmpEntityXAddressesInfo.ContactId;
                         //    }
                         //}
                         #endregion //Phone Info Details

                         #region Added code to remove the saved phone numbers
                         //if (base.m_fda.SafeFormVariableParamText("SysCmd") == "5")
                         //{
                         //    XmlNode objPhoneNumbersDeleted = base.SysEx.SelectSingleNode("//PopupGridRowsDeleted");
                         //    if (objPhoneNumbersDeleted != null && !String.IsNullOrEmpty(objPhoneNumbersDeleted.InnerText))
                         //    {
                         //        string[] strSplit = objPhoneNumbersDeleted.InnerText.Split(new Char[] { '|' });
                         //        foreach (AddressXPhoneInfo objAddressXPhoneInfo in objTmpEntityXAddressesInfo.AddressXPhoneInfoList)
                         //        {
                         //            foreach (string sPhoneNumber in strSplit)
                         //            {
                         //                int iPhoneId = Conversion.ConvertStrToInteger(sPhoneNumber);
                         //                if (iPhoneId == objAddressXPhoneInfo.PhoneId)
                         //                {
                         //                    objTmpEntityXAddressesInfo.AddressXPhoneInfoList.Remove(iPhoneId);
                         //                    break;
                         //                }

                         //            }
                         //        }
                         //    }
                         //}
                         #endregion //Added code to remove the saved phone numbers
                     }
                     //Parijat:Mits 9610
                     foreach (EntityXAddresses objEntityXAddressesInfo in MedicalStaff.StaffEntity.EntityXAddressesList)
                     {
                         if (!arrRowIds.Contains(objEntityXAddressesInfo.RowId.ToString()))//RMA-8753 nshah28
                         {
                             MedicalStaff.StaffEntity.EntityXAddressesList.Remove(objEntityXAddressesInfo.RowId);//RMA-8753 nshah28
                         }
                     }
                 }
                 else
                 {
                     if (base.m_fda.SafeFormVariableParamText("SysCmd") == "5")
                     {
                         string sAddr1 = MedicalStaff.StaffEntity.Addr1;
                         string sAddr2 = MedicalStaff.StaffEntity.Addr2;
                         string sAddr3 = MedicalStaff.StaffEntity.Addr3;// JIRA 6420 pkandhari
                         string sAddr4 = MedicalStaff.StaffEntity.Addr4;// JIRA 6420 pkandhari
                         string sCity = MedicalStaff.StaffEntity.City;
                         int iCountryCode = MedicalStaff.StaffEntity.CountryCode;
                         int iStateId = MedicalStaff.StaffEntity.StateId;
                         string sEmailAddress = MedicalStaff.StaffEntity.EmailAddress;
                         string sFaxNumber = MedicalStaff.StaffEntity.FaxNumber;
                         string sCounty = MedicalStaff.StaffEntity.County;
                         string sZipCode = MedicalStaff.StaffEntity.ZipCode;
                         //RMA-8753 nshah28(Added by ashish)
                         string sSearchString = string.Empty;
                         //RMA-8753 nshah28(Added by ashish) END
                         if (MedicalStaff.StaffEntity.EntityXAddressesList.Count > 0)
                         {
                             foreach (EntityXAddresses objEntityXAddressesInfo in MedicalStaff.StaffEntity.EntityXAddressesList)
                             {
                                 if (objEntityXAddressesInfo.PrimaryAddress == -1)
                                 {
                                     //RMA-8753 nshah28(Added by ashish) START
                                     //objEntityXAddressesInfo.Addr1 = sAddr1;
                                     //objEntityXAddressesInfo.Addr2 = sAddr2;
                                     //objEntityXAddressesInfo.Addr3 = sAddr3;// JIRA 6420 pkandhari
                                     //objEntityXAddressesInfo.Addr4 = sAddr4;// JIRA 6420 pkandhari
                                     //objEntityXAddressesInfo.City = sCity;
                                     //objEntityXAddressesInfo.Country = iCountryCode;
                                     //objEntityXAddressesInfo.State = iStateId;
                                     objEntityXAddressesInfo.Address.Addr1 = sAddr1;
                                     objEntityXAddressesInfo.Address.Addr2 = sAddr2;
                                     objEntityXAddressesInfo.Address.Addr3 = sAddr3;
                                     objEntityXAddressesInfo.Address.Addr4 = sAddr4;
                                     objEntityXAddressesInfo.Address.City = sCity;
                                     objEntityXAddressesInfo.Address.Country = iCountryCode;
                                     objEntityXAddressesInfo.Address.State = iStateId;
                                     objEntityXAddressesInfo.Email = sEmailAddress;
                                     objEntityXAddressesInfo.Fax = sFaxNumber;
                                     //objEntityXAddressesInfo.County = sCounty;
                                     //objEntityXAddressesInfo.ZipCode = sZipCode;
                                     objEntityXAddressesInfo.Address.County = sCounty;
                                     objEntityXAddressesInfo.Address.ZipCode = sZipCode;
                                     AddressForm objAddressForm = new AddressForm(m_fda);
                                     sSearchString = objAddressForm.SearchStringForAddress(objEntityXAddressesInfo.Address, objData.Context.LocalCache);
                                     objEntityXAddressesInfo.Address.SearchString = sSearchString;
                                     objEntityXAddressesInfo.AddressId = CommonFunctions.CheckAddressDuplication(sSearchString, MedicalStaff.Context.RMDatabase.ConnectionString, base.ClientId);
                                     objEntityXAddressesInfo.Address.AddressId = objEntityXAddressesInfo.AddressId;
                                     //RMA-8753 nshah28(Added by ashish) END
                                     break;
                                 }

                             }
                         }
                         else
                         {
                             //RMA-8753 nshah28(Added by ashish) START
                             if (
                                    sAddr1 != string.Empty || sAddr2 != string.Empty || sAddr3 != string.Empty || sAddr4 != string.Empty ||
                                    sCity != string.Empty || iCountryCode != 0 || iStateId != 0 || sEmailAddress != string.Empty ||
                                    sFaxNumber != string.Empty || sCounty != string.Empty || sZipCode != string.Empty
                                )
                             {
                                 EntityXAddresses objEntityXAddressesInfo = MedicalStaff.StaffEntity.EntityXAddressesList.AddNew();
                                 //objEntityXAddressesInfo.Addr1 = sAddr1;
                                 //objEntityXAddressesInfo.Addr2 = sAddr2;
                                 //objEntityXAddressesInfo.Addr3 = sAddr3;// JIRA 6420 pkandhari
                                 //objEntityXAddressesInfo.Addr4 = sAddr4;// JIRA 6420 pkandhari
                                 //objEntityXAddressesInfo.City = sCity;
                                 //objEntityXAddressesInfo.Country = iCountryCode;
                                 //objEntityXAddressesInfo.State = iStateId;
                                 objEntityXAddressesInfo.Address.Addr1 = sAddr1;
                                 objEntityXAddressesInfo.Address.Addr2 = sAddr2;
                                 objEntityXAddressesInfo.Address.Addr3 = sAddr3;
                                 objEntityXAddressesInfo.Address.Addr4 = sAddr4;
                                 objEntityXAddressesInfo.Address.City = sCity;
                                 objEntityXAddressesInfo.Address.Country = iCountryCode;
                                 objEntityXAddressesInfo.Address.State = iStateId;
                                 AddressForm objAddressForm = new AddressForm(m_fda);
                                 sSearchString = objAddressForm.SearchStringForAddress(objEntityXAddressesInfo.Address, objData.Context.LocalCache);
                                 objEntityXAddressesInfo.Address.SearchString = sSearchString;
                                 objEntityXAddressesInfo.AddressId = CommonFunctions.CheckAddressDuplication(sSearchString, MedicalStaff.Context.RMDatabase.ConnectionString, base.ClientId);
                                 objEntityXAddressesInfo.Address.AddressId = objEntityXAddressesInfo.AddressId;
                                 objEntityXAddressesInfo.Email = sEmailAddress;
                                 objEntityXAddressesInfo.Fax = sFaxNumber;
                                 //objEntityXAddressesInfo.County = sCounty;
                                 //objEntityXAddressesInfo.ZipCode = sZipCode;
                                 objEntityXAddressesInfo.Address.County = sCounty;
                                 objEntityXAddressesInfo.Address.ZipCode = sZipCode;
                                 objEntityXAddressesInfo.EntityId = MedicalStaff.StaffEntity.EntityId;
                                 objEntityXAddressesInfo.PrimaryAddress = -1;
                                 //objEntityXAddressesInfo.AddressId = -1;
                                 //RMA-8753 nshah28(Added by ashish) END
                             }
                         }
                     }
                 }
             }
            //avipinsrivas end
            #endregion

             #region Update Phone Numbers
             if (base.m_fda.SafeFormVariableParamText("SysCmd") == "5")
             {
                 MedicalStaff.StaffEntity.FormName = "StaffForm"; //Mits 22497
                 XmlNode objPhoneNumber = base.SysEx.SelectSingleNode("//PhoneNumbers");
                 if (objPhoneNumber != null)
                 {
                     string sPhoneData = objPhoneNumber.InnerText;
                     string[] sPhoneNumbers = sPhoneData.Split(new string[] { CommonForm.OUTER_PHONE_DELIMITER }, StringSplitOptions.None);
                     foreach (string sPhoneNumber in sPhoneNumbers)
                     {
                         string[] sNumber = sPhoneNumber.Split(new string[] { CommonForm.INNER_PHONE_DELIMITER }, StringSplitOptions.None);
                         if (sNumber.Length > 2)
                         {
                             string sPhoneId = sNumber[0];
                             string sPhoneType = sNumber[1];
                             string sPhoneNo = sNumber[2];
                             bool bPhoneCodeExists = false;
                             string sPhoneDesc = string.Empty;

                             if (MedicalStaff.StaffEntity.AddressXPhoneInfoList.Count > 0)
                             {
                                 foreach (AddressXPhoneInfo objAddressXPhoneInfo in MedicalStaff.StaffEntity.AddressXPhoneInfoList)
                                 {
                                     if (objAddressXPhoneInfo.PhoneCode == Convert.ToInt32(sPhoneType))
                                     {
                                         if (sPhoneNo.Trim() != string.Empty)
                                             objAddressXPhoneInfo.PhoneNo = sPhoneNo;
                                         else
                                             MedicalStaff.StaffEntity.AddressXPhoneInfoList.Remove(objAddressXPhoneInfo.PhoneId);

                                         bPhoneCodeExists = true;
                                         break;
                                     }

                                 }
                                 if (!bPhoneCodeExists && sPhoneNo.Trim() != string.Empty)
                                 {
                                     AddressXPhoneInfo objAddressXPhoneInfo = MedicalStaff.StaffEntity.AddressXPhoneInfoList.AddNew();
                                     objAddressXPhoneInfo.PhoneId = Convert.ToInt32(sPhoneId);
                                     objAddressXPhoneInfo.PhoneCode = Convert.ToInt32(sPhoneType);
                                     objAddressXPhoneInfo.PhoneNo = sPhoneNo;
                                 }
                             }
                             else
                             {
                                 if (sPhoneNo.Trim() != string.Empty)
                                 {
                                     AddressXPhoneInfo objAddressXPhoneInfo = MedicalStaff.StaffEntity.AddressXPhoneInfoList.AddNew();
                                     objAddressXPhoneInfo.PhoneId = Convert.ToInt32(sPhoneId);
                                     objAddressXPhoneInfo.PhoneCode = Convert.ToInt32(sPhoneType);
                                     objAddressXPhoneInfo.PhoneNo = sPhoneNo;
                                 }
                             }
                             //Added to save Office and Alt phone numbers in Entity Table as well
                             sPhoneDesc = this.objCache.GetCodeDesc(Convert.ToInt32(sPhoneType)).Trim().ToLower();
                             {
                                 if (sPhoneDesc == "office")
                                     MedicalStaff.StaffEntity.Phone1 = sPhoneNo;
                                 else if (sPhoneDesc == "home")
                                     MedicalStaff.StaffEntity.Phone2 = sPhoneNo;
                             }
                             //Added to save Office and Alt phone numbers in Entity Table as well
                         }
                     }



                 }
             }

             #endregion 
			//Added Rakhi for R7:Add Emp Data Elements
	        // MITS :34276 Starts Entity ID Type Update
             #region "Updating Entity Id Type"
             SortedList objEntityIdTypeAsList = new SortedList();

             XmlNode objEntityIdTypeAsNode = base.SysEx.DocumentElement.SelectSingleNode("EntityXEntityIDType");
             objTypeAttributeNode = null;

             bIsNew = false;
             int iEntityIdTypeKey = -1;

             XmlDocument objEntityXIDTypeXmlDoc = null;
             XmlElement objEntityXIDTypeRootElement = null;

             int iEntityIdTypeSelectedId = base.GetSysExDataNodeInt("EntityXEntityIDTypeSelectedId", true);

             string sEntityIdTypeRowAddedFlag = base.GetSysExDataNodeText("EntityXEntityIDTypeGrid_RowAddedFlag", true);
             bool bEntityIdTypeRowAddedFlag = sEntityIdTypeRowAddedFlag.ToUpper().CompareTo("TRUE") == 0 ? true : false;

             string sEntityIdTypeRowDeletedFlag = base.GetSysExDataNodeText("EntityXEntityIDTypeGrid_RowDeletedFlag", true);
             bool bEntityIdTypeRowDeletedFlag = sEntityIdTypeRowDeletedFlag.ToUpper().CompareTo("TRUE") == 0 ? true : false;

             if (objEntityIdTypeAsNode != null)
             {
                 // Loop through data for all rows of the grid
                 foreach (XmlNode objOptionNode in objEntityIdTypeAsNode.SelectNodes("option"))
                 {
                     objTypeAttributeNode = objOptionNode.Attributes["type"];
                     bIsNew = false;
                     if (objTypeAttributeNode != null && objTypeAttributeNode.InnerText.Equals("new") == true)
                     {
                         // This is the 'extra' hidden <option> for capturing a new grid row data
                         bIsNew = true;
                     }
                     if ((bIsNew == false) || (bIsNew == true && bEntityIdTypeRowAddedFlag == true))
                     {
                         objEntityXIDTypeXmlDoc = new XmlDocument();
                         objEntityXIDTypeRootElement = objEntityXIDTypeXmlDoc.CreateElement("EntityXEntityIDType");
                         objEntityXIDTypeXmlDoc.AppendChild(objEntityXIDTypeRootElement);
                         objEntityXIDTypeRootElement.InnerXml = objOptionNode.InnerXml;

                         iEntityIdTypeKey = Conversion.ConvertStrToInteger(objEntityXIDTypeRootElement.SelectSingleNode("IdNumRowId").InnerText);

                         if ((bIsNew == true && bEntityIdTypeRowAddedFlag == true) || (iEntityIdTypeKey < 0 && objEntityXIDTypeRootElement.SelectSingleNode("EntityId").InnerText == ""))
                             objEntityXIDTypeRootElement.SelectSingleNode("EntityId").InnerText = "0";

                         if (bIsNew == false && bEntityIdTypeRowDeletedFlag == true && iEntityIdTypeKey == iEntityIdTypeSelectedId)
                         {
                             continue;
                         }

                         objEntityIdTypeAsList.Add(iEntityIdTypeKey, objEntityXIDTypeXmlDoc);
                     }
                 }
             }

             foreach (EntityXEntityIDType objEntityXEntityIDTypeAs in MedicalStaff.StaffEntity.EntityXEntityIDTypeList)
             {
                 if (base.m_fda.SafeFormVariableParamText("SysCmd") == "7" && (objEntityXEntityIDTypeAs.IdNumRowId < 0 || (bEntityIdTypeRowDeletedFlag == true && objEntityXEntityIDTypeAs.IdNumRowId == iEntityIdTypeSelectedId)))
                 {
                     MedicalStaff.StaffEntity.EntityXEntityIDTypeList.Remove(objEntityXEntityIDTypeAs.IdNumRowId);
                 }
                 else if (objEntityXEntityIDTypeAs.IdNumRowId < 0)
                     MedicalStaff.StaffEntity.EntityXEntityIDTypeList.Remove(objEntityXEntityIDTypeAs.IdNumRowId);
             }


             EntityXEntityIDType objTmpEntityXEntityIDTypeAs = null;
             XmlDocument objTmpEntityXEntityIdTypeDoc = null;

             ArrayList arrEntityIdTypes = new ArrayList();

             for (int iListIndex = 0; iListIndex < objEntityIdTypeAsList.Count; iListIndex++)
             {
                 objTmpEntityXEntityIdTypeDoc = (XmlDocument)objEntityIdTypeAsList.GetByIndex(iListIndex);

                 if (objTmpEntityXEntityIdTypeDoc.DocumentElement.SelectSingleNode("IdNumRowId").InnerText.Trim() == "" || Conversion.ConvertStrToInteger(objTmpEntityXEntityIdTypeDoc.DocumentElement.SelectSingleNode("IdNumRowId").InnerText) < 0)
                 {
                     objTmpEntityXEntityIdTypeDoc.DocumentElement.SelectSingleNode("IdNumRowId").InnerText = ((-1 * iListIndex) - 1).ToString();
                     objTmpEntityXEntityIDTypeAs = MedicalStaff.StaffEntity.EntityXEntityIDTypeList.AddNew();
                     objTmpEntityXEntityIDTypeAs.PopulateObject(objTmpEntityXEntityIdTypeDoc);
                 }
                 else
                 {
                     objTmpEntityXEntityIDTypeAs = MedicalStaff.StaffEntity.EntityXEntityIDTypeList[Conversion.ConvertStrToInteger(objTmpEntityXEntityIdTypeDoc.DocumentElement.SelectSingleNode("IdNumRowId").InnerText)];

                     objTmpEntityXEntityIDTypeAs.PopulateObject(objTmpEntityXEntityIdTypeDoc);
                 }

                 arrEntityIdTypes.Add(objTmpEntityXEntityIdTypeDoc.DocumentElement.SelectSingleNode("IdNumRowId").InnerText);
             }


             foreach (EntityXEntityIDType objEntityXEntityIDType in MedicalStaff.StaffEntity.EntityXEntityIDTypeList)
             {
                 if (!arrEntityIdTypes.Contains(objEntityXEntityIDType.IdNumRowId.ToString()))
                 {
                     MedicalStaff.StaffEntity.EntityXEntityIDTypeList.Remove(objEntityXEntityIDType.IdNumRowId);
                 }
             }

             #endregion
            //MITS:34276 Ends -- Entity ID Type Update
            
        }
        public override void AfterSave()
        {
            base.AfterSave();
            //avipinsrivas Start : Worked for 7569 (Issue of 4634 - Epic 340) 
            if (MedicalStaff.Context.InternalSettings.SysSettings.UseEntityRole)
            {
                if (MedicalStaff.StaffEntity.EntityId > 0)
                {
                    int intTableID = MedicalStaff.Context.LocalCache.GetTableId(Globalization.PersonInvolvedLookupsGlossaryTableNames.MEDICAL_STAFF.ToString());
                    MedicalStaff.StaffEntity.UpdateEntityRoles((MedicalStaff.StaffEntity as IDataModel), intTableID);
                }
            }
            //avipinsrivas End
        }

        private XmlDocument GetEntityXAddressInfo()
        {
            XmlElement objOptionXmlElement = null;
            XmlElement objXmlElement = null;
            XmlElement objListHeadXmlElement = null;
            XmlAttribute objXmlAttribute = null;
            int iEntityXAddressInfoCount = 0;
            string sStateCode = string.Empty;
            string sStateDesc = string.Empty;
            string sCountryCode = string.Empty;
            string sCountryDesc = string.Empty;
            string sPrimaryAddressRef = string.Empty;
            string p_ShortCode = String.Empty; //MITS:34276 Entity Address Type 
            string p_sDesc = String.Empty;  //MITS:34276 Entity Address Type 

            XmlDocument objEntityXAddressInfoXmlDoc = new XmlDocument();
            XmlElement objRootElement = objEntityXAddressInfoXmlDoc.CreateElement("EntityXAddressesInfo");
            objEntityXAddressInfoXmlDoc.AppendChild(objRootElement);

            objListHeadXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("listhead");

            //MITS:34276 Entity Address Type START
            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("AddressType");
            objXmlElement.InnerText = "Address Type";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;
            //MITS:34276 Entity Address Type END

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr1");
            objXmlElement.InnerText = "Address1";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr2");
            objXmlElement.InnerText = "Address2";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr3");// JIRA 6420 pkandhari
            objXmlElement.InnerText = "Address3";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr4");// JIRA 6420 pkandhari
            objXmlElement.InnerText = "Address4";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("City");
            objXmlElement.InnerText = "City";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("State");
            objXmlElement.InnerText = "State";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Country");
            objXmlElement.InnerText = "Country";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("County");
            objXmlElement.InnerText = "County";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("ZipCode");
            objXmlElement.InnerText = "Zip Code";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Email");
            objXmlElement.InnerText = "E-Mail";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Fax");
            objXmlElement.InnerText = "Fax No.";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("PrimaryAddress");
            objXmlElement.InnerText = "Primary Address";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            //JIRA:6865 START:
            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("EffectiveDate");
            objXmlElement.InnerText = "Effective Date";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("ExpirationDate");
            objXmlElement.InnerText = "Expiration Date";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;
            //JIRA:6865 END:

            objRootElement.AppendChild(objListHeadXmlElement);
            objListHeadXmlElement = null;


            foreach (EntityXAddresses objEntityXAddressInfo in this.MedicalStaff.StaffEntity.EntityXAddressesList)
            {

                iEntityXAddressInfoCount++;

                objOptionXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("option");

                objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("ref");
                objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH
                    + "/" + objEntityXAddressInfoXmlDoc.DocumentElement.LocalName
                    + "/option[" + iEntityXAddressInfoCount.ToString() + "]";
                objOptionXmlElement.Attributes.Append(objXmlAttribute);
                sPrimaryAddressRef = objXmlAttribute.InnerText;
                objXmlAttribute = null;

                //MITS:34276 Entity Address Type START
                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("AddressType");
                this.objCache.GetCodeInfo(objEntityXAddressInfo.AddressType, ref p_ShortCode, ref p_sDesc);
                objXmlElement.InnerText = p_ShortCode + " " + p_sDesc;
                objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("codeid");
                objXmlAttribute.InnerText = objEntityXAddressInfo.AddressType.ToString();
                objXmlElement.Attributes.Append(objXmlAttribute);
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlAttribute = null;
                objXmlElement = null;
                //MITS:34276 Entity Address Type END

				//RMA-8753 nshah28(Added by ashish) START
                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr1");
                objXmlElement.InnerText = objEntityXAddressInfo.Address.Addr1;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr2");
                objXmlElement.InnerText = objEntityXAddressInfo.Address.Addr2;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr3");// JIRA 6420 pkandhari
                objXmlElement.InnerText = objEntityXAddressInfo.Address.Addr3;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr4");// JIRA 6420 pkandhari
                objXmlElement.InnerText = objEntityXAddressInfo.Address.Addr4;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("City");
                objXmlElement.InnerText = objEntityXAddressInfo.Address.City;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("State");
                this.objCache.GetStateInfo(objEntityXAddressInfo.Address.State, ref sStateCode, ref sStateDesc);
                objXmlElement.InnerText = sStateCode + " " + sStateDesc;
                objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("codeid");
                objXmlAttribute.InnerText = objEntityXAddressInfo.Address.State.ToString();
                objXmlElement.Attributes.Append(objXmlAttribute);
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlAttribute = null;
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Country");
                this.objCache.GetCodeInfo(objEntityXAddressInfo.Address.Country, ref sCountryCode, ref sCountryDesc, base.Adaptor.userLogin.objUser.NlsCode);
                objXmlElement.InnerText = sCountryCode + " " + sCountryDesc;
                objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("codeid");
                objXmlAttribute.InnerText = objEntityXAddressInfo.Address.Country.ToString();
                objXmlElement.Attributes.Append(objXmlAttribute);
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlAttribute = null;
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("County");
                objXmlElement.InnerText = objEntityXAddressInfo.Address.County;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;


                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("ZipCode");
                objXmlElement.InnerText = objEntityXAddressInfo.Address.ZipCode;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;
				//RMA-8753 nshah28(Added by ashish) END

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Email");
                objXmlElement.InnerText = objEntityXAddressInfo.Email;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Fax");
                objXmlElement.InnerText = objEntityXAddressInfo.Fax;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("PrimaryAddress");
                //int iPrimaryAddress = objEntityXAddressInfo.PrimaryAddress;
                int iPrimaryAddress = Convert.ToInt32(objEntityXAddressInfo.PrimaryAddress); //RMA-8753 nshah28
                if (iPrimaryAddress == -1)
                    objXmlElement.InnerText = "True";
                else
                    objXmlElement.InnerText = "False";
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                //JIRA:6865 START:
                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("EffectiveDate");
                objXmlElement.InnerText = Conversion.GetDBDateFormat(objEntityXAddressInfo.EffectiveDate, "MM/dd/yyyy");
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("ExpirationDate");
                objXmlElement.InnerText = Conversion.GetDBDateFormat(objEntityXAddressInfo.ExpirationDate, "MM/dd/yyyy");
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                //JIRA:6865 END:

                /************** Hidden Columns on the Grid ******************/
                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("NewPrimaryRecord");//for identication of changed Primary Address
                objXmlElement.InnerText = "False";
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("AddressId");
                objXmlElement.InnerText = objEntityXAddressInfo.AddressId.ToString();
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;
                //AA//AA //RMA-8753 nshah28
                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("RowId");
                objXmlElement.InnerText = objEntityXAddressInfo.RowId.ToString();
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;
                //AA //RMA-8753 nshah28 END

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("EntityId");
                objXmlElement.InnerText = objEntityXAddressInfo.EntityId.ToString();
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;


                //XmlDocument objAddressXPhoneInfoXmlDoc = GetAddressXPhoneInfo(objEntityXAddressInfo);
                //objOptionXmlElement.AppendChild(objEntityXAddressInfoXmlDoc.ImportNode(objAddressXPhoneInfoXmlDoc.SelectSingleNode("AddressXPhoneInfo"), true));

                objRootElement.AppendChild(objOptionXmlElement);
                objOptionXmlElement = null;


            }

            iEntityXAddressInfoCount++;

            objOptionXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("option");

            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("ref");
            objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH
                + "/" + objEntityXAddressInfoXmlDoc.DocumentElement.LocalName
                + "/option[" + iEntityXAddressInfoCount.ToString() + "]";
            objOptionXmlElement.Attributes.Append(objXmlAttribute);
            objXmlAttribute = null;

            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("type");
            objXmlAttribute.InnerText = "new";
            objOptionXmlElement.Attributes.Append(objXmlAttribute);
            objXmlAttribute = null;

            //MITS:34276 Entity Address Type START
            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("AddressType");
            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("codeid");
            objXmlAttribute.InnerText = "-1";
            objXmlElement.Attributes.Append(objXmlAttribute);
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlAttribute = null;
            objXmlElement = null;
            //MITS:34276 Entity Address Type END

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr1");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr2");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr3");// JIRA 6420 pkandhari
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr4");// JIRA 6420 pkandhari
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("City");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("State");
            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("codeid");
            objXmlAttribute.InnerText = "-1";
            objXmlElement.Attributes.Append(objXmlAttribute);
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlAttribute = null;
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Country");
            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("codeid");
            objXmlAttribute.InnerText = "-1";
            objXmlElement.Attributes.Append(objXmlAttribute);
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlAttribute = null;
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("County");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("ZipCode");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Email");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Fax");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("PrimaryAddress");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            //JIRA:6865 START:
            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("EffectiveDate");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("ExpirationDate");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;
            //JIRA:6865 END:

            /************** Hidden Columns on the Grid ******************/
            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("NewPrimaryRecord"); //for identication of changed Primary Address
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("AddressId");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            //AA //RMA-8753 nshah28
            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("RowId");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;
            //AA //RMA-8753 nshah28 END
            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("EntityId");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            #region PhoneInfo

            //            XmlElement objInnerXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("AddressXPhoneInfo");
//            objOptionXmlElement.AppendChild(objInnerXmlElement);

//            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("listhead");
//            string sNode = @"<PhoneCode>Phone Code</PhoneCode>
//                             <PhoneNo>Phone Number</PhoneNo>";
//            objXmlElement.InnerXml = sNode;
//            objInnerXmlElement.AppendChild(objXmlElement);

//            XmlElement objInnerOptionXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("option");

//            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("ref");
//            objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH
//                + "/AddressXPhoneInfo" + "/option[" + iEntityXAddressInfoCount.ToString() + "]";
//            objInnerOptionXmlElement.Attributes.Append(objXmlAttribute);
//            objXmlAttribute = null;

//            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("type");
//            objXmlAttribute.InnerText = "new";
//            objInnerOptionXmlElement.Attributes.Append(objXmlAttribute);
//            objXmlAttribute = null;

//            objInnerXmlElement.AppendChild(objInnerOptionXmlElement);

//            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("PhoneCode");
//            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("codeid");
//            objXmlAttribute.InnerText = "-1";
//            objXmlElement.Attributes.Append(objXmlAttribute);
//            objInnerOptionXmlElement.AppendChild(objXmlElement);
//            objXmlAttribute = null;
//            objXmlElement = null;

//            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("PhoneNo");
//            objInnerOptionXmlElement.AppendChild(objXmlElement);
//            objXmlElement = null;

//            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("ContactId");
//            objInnerOptionXmlElement.AppendChild(objXmlElement);
//            objXmlElement = null;

//            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("PhoneId");
//            objInnerOptionXmlElement.AppendChild(objXmlElement);
            //            objXmlElement = null;

            #endregion //PhoneInfo

            objRootElement.AppendChild(objOptionXmlElement);
            objOptionXmlElement = null;

            objRootElement = null;
            return objEntityXAddressInfoXmlDoc;
        }
        private void SetPrimaryAddressReadOnly()
        {

            base.AddReadOnlyNode("addr1");
            base.AddReadOnlyNode("addr2");
            base.AddReadOnlyNode("addr3");// JIRA 6420 pkandhari
            base.AddReadOnlyNode("addr4");// JIRA 6420 pkandhari
            base.AddReadOnlyNode("city");
            base.AddReadOnlyNode("countrycode");
            base.AddReadOnlyNode("county");
            base.AddReadOnlyNode("stateid");
            base.AddReadOnlyNode("emailaddress");
            base.AddReadOnlyNode("faxnumber");
            base.AddReadOnlyNode("zipcode");

        }
        private string PopulatePhoneNumbers()
        {

            XmlDocument SysExDoc = new XmlDocument();
            string sNumber = string.Empty;
            foreach (AddressXPhoneInfo objPhoneInfo in MedicalStaff.StaffEntity.AddressXPhoneInfoList)
            {
                //Added to set Default types to office and home
                if (objPhoneInfo.PhoneCode == iOfficeType)
                {
                    sOfficePhone = objPhoneInfo.PhoneNo;

                }
                else if (objPhoneInfo.PhoneCode == iHomeType)
                {
                    sHomePhone = objPhoneInfo.PhoneNo;
                }
                //Added to set Default types to office and home
                if (String.IsNullOrEmpty(sNumber))
                {
                    sNumber = objPhoneInfo.PhoneId + CommonForm.INNER_PHONE_DELIMITER + objPhoneInfo.PhoneCode + CommonForm.INNER_PHONE_DELIMITER + objPhoneInfo.PhoneNo;
                }
                else
                {
                    sNumber += CommonForm.OUTER_PHONE_DELIMITER + objPhoneInfo.PhoneId + CommonForm.INNER_PHONE_DELIMITER + objPhoneInfo.PhoneCode + CommonForm.INNER_PHONE_DELIMITER + objPhoneInfo.PhoneNo;
                }
                XmlNode objNode = base.SysEx.SelectSingleNode("//PhoneTypeList");
                if (objNode != null)
                {
                    foreach (XmlNode node in objNode.ChildNodes)
                    {
                        if (objPhoneInfo.PhoneCode.ToString() == node.Attributes["value"].Value)
                        {
                            node.InnerText += " " + CommonForm.MARK_PHONECODE;
                            break;
                        }
                    }
                }

            }

            return sNumber;
        }
        #region GetAddressXPhoneInfo

        /// <summary>
        /// Override the Init function
        /// </summary>
        public override void InitNew()
        {
            base.InitNew();

            if (this.MedicalStaff.Context.InternalSettings.SysSettings.UseEntityRole)
            {
                //this.MedicalStaff.StaffEid = base.GetSysExDataNodeInt("/SysExData/staffeid");
                (objData as INavigation).Filter = objData.PropertyNameToField("StaffEid") + "=" + base.GetSysExDataNodeInt("/SysExData/staffeid");
            }
        }
        /// <summary>
        /// override the BeforeSave function to set entity approval status "Pending" if it is 0 or "".
        /// </summary>
        /// <param name="Cancel"></param>
        public override void BeforeSave(ref bool Cancel)
        {
            //rsushilaggar Entity Payment Approval(make the people entity approval status as pending approval) MITS 20606 05/05/2010
            if (MedicalStaff.Context.InternalSettings.SysSettings.UseEntityApproval)
            {
                if (MedicalStaff.StaffEntity.EntityApprovalStatusCode == 0)
                {
                    MedicalStaff.StaffEntity.EntityApprovalStatusCode = MedicalStaff.Context.InternalSettings.CacheFunctions.GetCodeIDWithShort("P", "ENTITY_APPRV_REJ");
                }
            }
            //End rsushilaggar
        }

        //private XmlDocument GetAddressXPhoneInfo(EntityXAddresses objEntityXAddresses)
        //{
        //    XmlDocument objAddressXPhoneInfoXmlDoc = new XmlDocument();

        //    XmlElement objRootElement = objAddressXPhoneInfoXmlDoc.CreateElement("AddressXPhoneInfo");
        //    objAddressXPhoneInfoXmlDoc.AppendChild(objRootElement);

        //    XmlElement objOptionXmlElement = null;
        //    XmlElement objXmlElement = null;
        //    XmlElement objListHeadXmlElement = null;
        //    XmlAttribute objXmlAttribute = null;
        //    int iAddressXPhoneInfoCount = 0;
        //    string sPhoneCode = "";
        //    string sPhoneDesc = "";



        //    objListHeadXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("listhead");


        //    objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneCode");
        //    objXmlElement.InnerText = "Phone Code";
        //    objListHeadXmlElement.AppendChild(objXmlElement);
        //    objXmlElement = null;

        //    objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneNo");
        //    objXmlElement.InnerText = "Phone Number";
        //    objListHeadXmlElement.AppendChild(objXmlElement);
        //    objXmlElement = null;

        //    objRootElement.AppendChild(objListHeadXmlElement);
        //    objListHeadXmlElement = null;


        //    foreach (AddressXPhoneInfo objAddressXPhoneInfo in objEntityXAddresses.AddressXPhoneInfoList)
        //    {

        //        iAddressXPhoneInfoCount++;

        //        objOptionXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("option");

        //        objXmlAttribute = objAddressXPhoneInfoXmlDoc.CreateAttribute("ref");
        //        objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH
        //            + "/" + objAddressXPhoneInfoXmlDoc.DocumentElement.LocalName
        //            + "/option[" + iAddressXPhoneInfoCount.ToString() + "]";
        //        objOptionXmlElement.Attributes.Append(objXmlAttribute);
        //        objXmlAttribute = null;



        //        objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneCode");
        //        this.objCache.GetCodeInfo(objAddressXPhoneInfo.PhoneCode, ref sPhoneCode, ref sPhoneDesc);
        //        objXmlElement.InnerText = sPhoneCode + " " + sPhoneDesc;
        //        objXmlAttribute = objAddressXPhoneInfoXmlDoc.CreateAttribute("codeid");
        //        objXmlAttribute.InnerText = objAddressXPhoneInfo.PhoneCode.ToString();
        //        objXmlElement.Attributes.Append(objXmlAttribute);
        //        objOptionXmlElement.AppendChild(objXmlElement);
        //        objXmlAttribute = null;
        //        objXmlElement = null;


        //        objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneNo");
        //        objXmlElement.InnerText = objAddressXPhoneInfo.PhoneNo;
        //        objOptionXmlElement.AppendChild(objXmlElement);
        //        objXmlElement = null;


        //        /************** Hidden Columns on the Grid ******************/
        //        objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("ContactId");
        //        objXmlElement.InnerText = objAddressXPhoneInfo.ContactId.ToString();
        //        objOptionXmlElement.AppendChild(objXmlElement);
        //        objXmlElement = null;

        //        objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneId");
        //        objXmlElement.InnerText = objAddressXPhoneInfo.PhoneId.ToString();
        //        objOptionXmlElement.AppendChild(objXmlElement);
        //        objXmlElement = null;

        //        objRootElement.AppendChild(objOptionXmlElement);
        //        objOptionXmlElement = null;

        //    }

        //    iAddressXPhoneInfoCount++;

        //    objOptionXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("option");

        //    objXmlAttribute = objAddressXPhoneInfoXmlDoc.CreateAttribute("ref");
        //    objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH
        //        + "/" + objAddressXPhoneInfoXmlDoc.DocumentElement.LocalName
        //        + "/option[" + iAddressXPhoneInfoCount.ToString() + "]";
        //    objOptionXmlElement.Attributes.Append(objXmlAttribute);
        //    objXmlAttribute = null;

        //    objXmlAttribute = objAddressXPhoneInfoXmlDoc.CreateAttribute("type");
        //    objXmlAttribute.InnerText = "new";
        //    objOptionXmlElement.Attributes.Append(objXmlAttribute);
        //    objXmlAttribute = null;


        //    objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneCode");
        //    objXmlAttribute = objAddressXPhoneInfoXmlDoc.CreateAttribute("codeid");
        //    objXmlAttribute.InnerText = "-1";
        //    objXmlElement.Attributes.Append(objXmlAttribute);
        //    objOptionXmlElement.AppendChild(objXmlElement);
        //    objXmlAttribute = null;
        //    objXmlElement = null;


        //    objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneNo");
        //    objOptionXmlElement.AppendChild(objXmlElement);
        //    objXmlElement = null;

        //    /************** Hidden Columns on the Grid ******************/
        //    objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("ContactId");
        //    objOptionXmlElement.AppendChild(objXmlElement);
        //    objXmlElement = null;

        //    objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneId");
        //    objOptionXmlElement.AppendChild(objXmlElement);
        //    objXmlElement = null;

        //    objRootElement.AppendChild(objOptionXmlElement);
        //    objOptionXmlElement = null;

        //    objRootElement = null;
        //    return objAddressXPhoneInfoXmlDoc;
        //}
        #endregion //GetAddressXPhoneInfo

        //MITS:34276 Start : Entity ID Type display
        private XmlDocument GetEntityIdTypeInfoData()
        {
            XmlDocument objEntityIDTypeAsXmlDoc = new XmlDocument();

            XmlElement objRootElement = objEntityIDTypeAsXmlDoc.CreateElement("EntityXEntityIDType");
            //MITS #34276 Entity ID Type Permission starts
            XmlAttribute objXmlPermissionAttribute = null;
            if (!MedicalStaff.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_IDTYPE_VIEW))
            {

                objXmlPermissionAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsViewable");
                objXmlPermissionAttribute.InnerText = "false";
                objRootElement.Attributes.Append(objXmlPermissionAttribute);
                objXmlPermissionAttribute = null;
                objEntityIDTypeAsXmlDoc.AppendChild(objRootElement);
            }
            else
            {
                if (!MedicalStaff.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_IDTYPE_NEW))
                {
                    objXmlPermissionAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsNew");
                    objXmlPermissionAttribute.InnerText = "false";
                    objRootElement.Attributes.Append(objXmlPermissionAttribute);
                    objXmlPermissionAttribute = null;
                }
                objEntityIDTypeAsXmlDoc.AppendChild(objRootElement);

                if (!MedicalStaff.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_IDTYPE_EDIT))
                {
                    objXmlPermissionAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsUpdate");
                    objXmlPermissionAttribute.InnerText = "false";
                    objRootElement.Attributes.Append(objXmlPermissionAttribute);
                    objXmlPermissionAttribute = null;
                }
                objEntityIDTypeAsXmlDoc.AppendChild(objRootElement);

                if (!MedicalStaff.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_IDTYPE_DELETE))
                {
                    objXmlPermissionAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsDelete");
                    objXmlPermissionAttribute.InnerText = "false";
                    objRootElement.Attributes.Append(objXmlPermissionAttribute);
                    objXmlPermissionAttribute = null;
                }
                objEntityIDTypeAsXmlDoc.AppendChild(objRootElement);
                if (!MedicalStaff.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_VENDORTYPE_NEW))
                {
                    objXmlPermissionAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsVendorNew");
                    objXmlPermissionAttribute.InnerText = "false";
                    objRootElement.Attributes.Append(objXmlPermissionAttribute);
                    objXmlPermissionAttribute = null;
                }
                objEntityIDTypeAsXmlDoc.AppendChild(objRootElement);
            }
            //MITS #34276  Entity ID Type Permission Ends 

            XmlElement objOptionXmlElement = null;
            XmlElement objXmlElement = null;
            XmlElement objListHeadXmlElement = null;
            XmlAttribute objXmlAttribute = null;
            int iEntityIDTypeAsCount = 0;
            string sIDCode = string.Empty;
            string sIDDesc = string.Empty;

            objListHeadXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("listhead");

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdType");
            objXmlElement.InnerText = "Entity ID Type";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdNum");
            objXmlElement.InnerText = "Entity ID Number";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EffStartDate");
            objXmlElement.InnerText = "Effective Date";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EffEndDate");
            objXmlElement.InnerText = "Expiration Date";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objRootElement.AppendChild(objListHeadXmlElement);
            objListHeadXmlElement = null;

            foreach (EntityXEntityIDType objEntityXEntityIDType in this.MedicalStaff.StaffEntity.EntityXEntityIDTypeList)
            {
                iEntityIDTypeAsCount++;

                objOptionXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("option");

                objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("ref");
                objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH + "/" + objEntityIDTypeAsXmlDoc.DocumentElement.LocalName + "/option[" + iEntityIDTypeAsCount.ToString() + "]";
                objOptionXmlElement.Attributes.Append(objXmlAttribute);
                objXmlAttribute = null;

                //achouhan3 05-may-2013    MITS #34276  ID Type Vendor Permission Starts 
                this.objCache.GetCodeInfo(objEntityXEntityIDType.IdType, ref sIDCode, ref sIDDesc);
                if (sIDCode.Trim().ToUpper() == "VENDOR")
                {
                    if (!MedicalStaff.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_VENDORTYPE_VIEW))
                    {
                        objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsVendorView");
                        objXmlAttribute.InnerText = "false";
                        objOptionXmlElement.Attributes.Append(objXmlAttribute);
                        objXmlAttribute = null;
                    }                   
                    if (!MedicalStaff.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_VENDORTYPE_UPDATE))
                    {
                        objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsVendorUpdate");
                        objXmlAttribute.InnerText = "false";
                        objOptionXmlElement.Attributes.Append(objXmlAttribute);
                        objXmlAttribute = null;
                    }
                    if (!MedicalStaff.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_VENDORTYPE_DELETE))
                    {
                        objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsVendorDelete");
                        objXmlAttribute.InnerText = "false";
                        objOptionXmlElement.Attributes.Append(objXmlAttribute);
                        objXmlAttribute = null;
                    }
                }
                //achouhan3 05-may-2013    MITS #34276  ID Type Vendor Permission ends

                objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdType");
                this.objCache.GetCodeInfo(objEntityXEntityIDType.IdType, ref sIDCode, ref sIDDesc);
                objXmlElement.InnerText = sIDCode + " " + sIDDesc;
                objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("codeid");
                objXmlAttribute.InnerText = objEntityXEntityIDType.IdType.ToString();
                objXmlElement.Attributes.Append(objXmlAttribute);
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlAttribute = null;
                objXmlElement = null;

                objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdNum");
                objXmlElement.InnerText = objEntityXEntityIDType.IdNum.ToString();
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EffStartDate");
                objXmlElement.InnerText = Conversion.GetDBDateFormat(objEntityXEntityIDType.EffStartDate, "MM/dd/yyyy");
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EffEndDate");
                objXmlElement.InnerText = Conversion.GetDBDateFormat(objEntityXEntityIDType.EffEndDate, "MM/dd/yyyy");
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                /************** Hidden Columns on the Grid ******************/
                objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdNumRowId");
                objXmlElement.InnerText = objEntityXEntityIDType.IdNumRowId.ToString();
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EntityId");
                objXmlElement.InnerText = objEntityXEntityIDType.EntityId.ToString();
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objRootElement.AppendChild(objOptionXmlElement);
                objOptionXmlElement = null;

            }

            iEntityIDTypeAsCount++;

            objOptionXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("option");

            objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("ref");
            objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH
                + "/" + objEntityIDTypeAsXmlDoc.DocumentElement.LocalName
                + "/option[" + iEntityIDTypeAsCount.ToString() + "]";
            objOptionXmlElement.Attributes.Append(objXmlAttribute);
            objXmlAttribute = null;

            objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("type");
            objXmlAttribute.InnerText = "new";
            objOptionXmlElement.Attributes.Append(objXmlAttribute);
            objXmlAttribute = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdType");
            objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("codeid");
            objXmlAttribute.InnerText = "-1";
            objXmlElement.Attributes.Append(objXmlAttribute);
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlAttribute = null;
            objXmlElement = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdNum");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EffStartDate");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EffEndDate");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;


            /************** Hidden Columns on the Grid ******************/
            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdNumRowId");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EntityId");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objRootElement.AppendChild(objOptionXmlElement);
            objOptionXmlElement = null;

            objRootElement = null;
            return objEntityIDTypeAsXmlDoc;

        }
        //MITS:34276 Ends

          
    }
}