using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	///	 Entity Exposure Screen.
	///  Author: Nikhil Garg	Dated: 06/Oct/2005
	/// </summary>
	public class EntityExposureForm : DataEntryFormBase
	{
		const string CLASS_NAME = "EntityXExposure";
		const string FILTER_KEY_NAME = "EntityId";

		private EntityXExposure EntityXExposure{get{return objData as EntityXExposure;}}

		private LocalCache objCache{get{return objData.Context.LocalCache;}}
	    
		/// <summary>
		/// Class constructor to initialize Class Name information
		/// </summary>
		/// <param name="fda"></param>
		public EntityExposureForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

		/// <summary>
		/// Initializes the class and sets the parent ID for the class
		/// that can be used for parent-child navigation
		/// </summary>
		public override void InitNew()
		{
			base.InitNew();

			//set filter for EntityId
			EntityXExposure.EntityId=base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME,false);
            if (EntityXExposure.EntityId == 0)
            {
                //Shruti for 11658
                if (base.SysEx.SelectSingleNode("//SysExternalParam") != null)
                {
                    EntityXExposure.EntityId = base.GetSysExDataNodeInt("//SysExternalParam/EntityId");
                }
                //Shruti for 11658 ends
            }
				(objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + EntityXExposure.EntityId;
		}

		public override void OnUpdateForm()
		{
			base.OnUpdateForm ();
            ApplyFormTitle();
            //Shruti for 11658
            XmlDocument objXML = new XmlDocument();
            objXML = base.SysEx;

            Entity objEntity = (Entity)this.EntityXExposure.Context.Factory.GetDataModelObject("Entity", false);
            
            XmlNode objSysExternalParam = base.SysEx.SelectSingleNode("//SysExternalParam");
            if (objSysExternalParam != null)
            {
                objEntity.MoveTo(base.GetSysExDataNodeInt("//SysExternalParam/EntityId"));
            
                this.EntityXExposure.Parent = objEntity;

                XmlNode objOld = objXML.SelectSingleNode("//Parent");
                XmlElement objNew = objXML.CreateElement("Parent");
                objNew.InnerXml = objEntity.SerializeObject();

                if (objOld != null)
                    objXML.DocumentElement.ReplaceChild(objNew, objOld);
                else
                    objXML.DocumentElement.AppendChild(objNew);
                base.ResetSysExData("EntityId", objEntity.EntityId.ToString());
                base.ResetSysExData("EntityTableId", objEntity.EntityTableId.ToString());                
            }
            else if (base.GetSysExDataNodeInt("EntityId") != 0)
            {
                objEntity.MoveTo(base.GetSysExDataNodeInt("EntityId"));
                base.ResetSysExData("EntityTableId", objEntity.EntityTableId.ToString());
                
            }
            
            //Shruti for 11658 ends
			if (EntityXExposure.ExposureRowId!=0)
				if (EntityXExposure.UserGeneratedFlag)
					base.ResetSysExData("ExposureType","User Entered");
				else
					base.ResetSysExData("ExposureType","Auto Roll Up");
			else
				base.ResetSysExData("ExposureType","");
		}

		public override void OnUpdateObject()
		{
			base.OnUpdateObject ();
            //MGaba2: MITS 28221: Exposure Roll Up Deletes Data
            //Commenting the check
			//forcibly set the true on save
			//if (EntityXExposure.ExposureRowId!=0)
				EntityXExposure.UserGeneratedFlag=true;
		}

		public override void AfterAddNew()
		{
			base.AfterAddNew();

			EntityXExposure.StartDate=GetLastExpoDate(EntityXExposure.EntityId);
		}

		private void ApplyFormTitle()
		{
			//MGaba2:MITS 16352:Last name no more exists in last name
            //string sCaption=base.GetSysExDataNodeText("/SysExData/LastName");

            Entity objTempEntity=(Entity)this.EntityXExposure.Context.Factory.GetDataModelObject("Entity", false);
            objTempEntity.MoveTo(EntityXExposure.EntityId);
            string sCaption = objTempEntity.LastName;
			if (sCaption!=string.Empty)
				sCaption=" [" + sCaption + "]";
			//Pass this subtitle value to view (ref'ed from @valuepath).
			base.ResetSysExData("SubTitle",sCaption);

            //Raman: Adding SubTitle to modified controls list
            ArrayList singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singleRow, "id", "formsubtitle");
            base.AddElementToList(ref singleRow, "Text", sCaption);
            base.m_ModifiedControls.Add(singleRow);
		}

		private string GetLastExpoDate(int p_iEntityID)
		{
			DbReader objReader=null;
			string sSQL=string.Empty;
			string sExpDate=string.Empty;

			if (p_iEntityID != 0)
			{
				sSQL = "SELECT MAX(END_DATE) FROM ENTITY_EXPOSURE WHERE ENTITY_ID = " + p_iEntityID;

                using (objReader = objData.Context.DbConnLookup.ExecuteReader(sSQL))
                {

                    if (objReader.Read())
                    {
                        sExpDate = Conversion.ConvertObjToStr(objReader.GetValue(0));
                        if (sExpDate.Trim() != "")
                            sExpDate = Conversion.ToDbDate(Conversion.ToDate(sExpDate).AddDays(1));
                    }
                }
                //objReader.Close();
                //objReader.Dispose();
			}
			return sExpDate;        
		}
        
        //rsharma220 MITS 31078 Start
        public override void OnValidate(ref bool Cancel)
        {
            bool bError = false;
            if (EntityXExposure.EndDate.CompareTo(EntityXExposure.StartDate) < 0)
            {
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                String.Format(Globalization.GetString("Validation.MustBeGreaterThanStartDt", base.ClientId), Conversion.ToDate(EntityXExposure.StartDate).ToShortDateString()), 
                BusinessAdaptorErrorType.Error);
                bError = true;
            }
            Cancel = bError;
        }
        //rsharma220 MITS 31078 End
	}
}