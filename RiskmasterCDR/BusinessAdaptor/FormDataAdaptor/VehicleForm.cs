﻿
using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using System.Text;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Vehicle Screen.
	/// </summary>
	public class VehicleForm : DataEntryFormBase
	{
		const string CLASS_NAME = "Vehicle";
		const string FILTER_KEY_NAME = "UnitId";

        //asatishchand mits:25241 
        private string m_sConfirmation = "";
        private Vehicle m_objFunds = null;
		private Vehicle Vehicle{get{return objData as Vehicle;}}

		private LocalCache objCache{get{return objData.Context.LocalCache;}}
	
		public VehicleForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

		public override void InitNew()
		{
			base.InitNew();

			//set filter for UnitId
			Vehicle.UnitId=base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME);
			if (Vehicle.UnitId>0)
				(objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + Vehicle.UnitId;
		}

        private void ApplyFormTitle()
        {
            //string sCaption= Entity.Context.LocalCache.GetUserTableName(Entity.EntityTableId) + 

            string sSubTitleText = string.Empty;
            ArrayList singleRow = null;
            if (Vehicle != null)
            {
                sSubTitleText += " [ " + Vehicle.VehicleMake + " " + Vehicle.VehicleModel + " ]";
            }

            //Pass this title value to view (ref'ed from @valuepath).
            base.ResetSysExData("SubTitle", sSubTitleText);

            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singleRow, "id", "formsubtitle");
            base.AddElementToList(ref singleRow, "Text", sSubTitleText);
            base.m_ModifiedControls.Add(singleRow);
        }

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
			base.OnUpdateForm ();
            ApplyFormTitle();
			if(!Vehicle.Context.RMUser.IsAllowedEx(FormBase.RMO_SEARCH,FormBase.RMO_VEHICLE_SEARCH))
				base.AddKillNode("search");
			if(!Vehicle.Context.RMUser.IsAllowedEx(FormBase.RMO_LOOKUP,FormBase.RMO_VEHICLE_SEARCH))
				base.AddKillNode("lookup");

			CheckAutoGenerateVin();

			//Raman Bhatia - Acrosoft Phase 1 changes.. Document Management Not available for Employee if Acrosoft is enabled
			bool bUseAcrosoftInterface = Vehicle.Context.InternalSettings.SysSettings.UseAcrosoftInterface;
            bool bUsePaperVision = Vehicle.Context.InternalSettings.SysSettings.UsePaperVisionInterface; //Mona:PaperVisionMerge : Animesh inserted MITS 16697
            //Mona:PaperVisionMerge: Adding bUsePaperVision in the condition
            //rbhatia4:R8: Use Media View Setting : July 11 2011
            if (bUseAcrosoftInterface || bUsePaperVision || Vehicle.Context.InternalSettings.SysSettings.UseMediaViewInterface)
            {
                base.AddKillNode("attachdocuments");//Mona:PaperVisionMerge:Animesh Inserted MITS 16697
                base.AddKillNode("attach");
            }
            //akaur9 MITS 25163- Policy Interface Implementation --start

            int iSystemPolicyId = base.GetSysExDataNodeInt("//SystemPolicyId");

            if (iSystemPolicyId > 0)
            {
                base.AddReadOnlyNode("readOnlyPage");
                base.AddKillNode("save");
                base.AddKillNode("delete");

                //skhare7 Point Policy interface Added statunitno/riskloc/subloc
                if (Vehicle.UnitId>0)
                {

                    DisplayUnitNo();
                }
            }
            else
            {
                base.AddKillNode("Unitno");
            }
            //akaur9 MITS 25163- Policy Interface Implementation --End
            //asatishchand mits 25241 starts
            base.ResetSysExData("htmlDuplicatevehicleId", "");
			//asatishchand mits 25241 ends
		}

		internal override bool ValidateRequiredViewFields(XmlDocument propertyStore)
		{
			CheckAutoGenerateVin();

			return base.ValidateRequiredViewFields (propertyStore);
		}

        //asatishchand:mits-25241 starts
        private string GenerateTransSplitHTML()
        {
              
            DbReader objReader = null;
            string sHTML = string.Empty;
            string VehicleID =Vehicle.Vin.ToString();
            string VehicleMake = string.Empty;
            string VehicleModel = string.Empty;
            string VehicleYear = string.Empty;
            string VehicleType = string.Empty;
            string VehicleDepartment = string.Empty;
            string VehicleLicense = string.Empty;
            string VehicleLicenseState = string.Empty;
            string sSQL = string.Empty;
       
            try
            {
                sSQL = "select (VIN) as VehicleID,"
                    + "(VEHICLE_MAKE) as VehicleMake,"
                    + "(VEHICLE_MODEL) as VehicleModel,"
                    + "(VEHICLE_YEAR) as VehicleYear,"
                    + "(UNIT_TYPE_CODE) as VehicleType,"
                    + "(HOME_DEPT_EID) as VehicleDepartment,"
                    + "(LICENSE_NUMBER) as VehicleLicense,"
                    + "(STATE_ROW_ID) as VehicleLicenseState"
                    + " from VEHICLE where VIN ='" + Vehicle.Vin + "'";

                objReader = DbFactory.GetDbReader(m_fda.connectionString, sSQL);

                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        sHTML += "*" + VehicleID +"|";

                        VehicleMake = objReader.GetString("VehicleMake");
                        sHTML += VehicleMake + "|";

                        VehicleModel = objReader.GetString("VehicleModel");

                        sHTML += VehicleModel + "|";

                        VehicleYear = Conversion.ConvertObjToStr(objReader.GetValue("VehicleYear"));

                        sHTML += VehicleYear + "|";

                        VehicleType = Conversion.ConvertObjToStr(objReader.GetValue("VehicleType"));

                        sHTML += VehicleType + "|";

                        VehicleDepartment = Conversion.ConvertObjToStr(objReader.GetValue("VehicleDepartment"));

                        sHTML += VehicleDepartment + "|";

                        VehicleLicense = Conversion.ConvertObjToStr(objReader.GetValue("VehicleLicense"));

                        sHTML += VehicleLicense + "|";

                        VehicleLicenseState = Conversion.ConvertObjToStr(objReader.GetValue("VehicleLicenseState"));

                        sHTML += VehicleLicenseState + "|";


                    }
                }
                objReader.Close();
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundsForm.GenerateTransSplitHTML.ErrorGenHTML",base.ClientId), p_objEx);//sharishkumar Jira 804
            }
            finally
            {
             
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return (sHTML);
           
        }
       
        public override void OnValidate(ref bool Cancel)
        {

            
            m_objFunds = Vehicle;
            ///Mbahl3 Jira RMA-5629
            if ((Vehicle.UnitId==0)&& CheckForDupVin() && base.GetSysExDataNodeText("htmlDuplicatevehicleId") != "Ignore")
            {
                m_sConfirmation = GenerateTransSplitHTML();
                base.ResetSysExData("htmlDuplicatevehicleId", m_sConfirmation);
                Cancel = true;
                
             }
            else
            {
               
                Cancel = false;
            }
        }
       
        private bool CheckForDupVin()
        {
            //DbReader objReader = null;
            //bool bError = false;
            try
            {
                //StringBuilder sbSql = new StringBuilder();
                //sbSql.Append("SELECT count(*)  FROM  vehicle where vin = '" + Vehicle.Vin +"'");
                int VinCount = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_fda.connectionString,
                    "SELECT count(1)  FROM  vehicle where vin = '" + Vehicle.Vin + "'"), base.ClientId);
                if (VinCount > 0)
                {
                    return true;
                }
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckForDupVin.Data.ErrorDupPay", base.ClientId), p_objEx);//sharishkumar Jira 804
            }
            return false;
        }
		
        //asatishchand:mits-25241 Ends
		private void CheckAutoGenerateVin()
		{
			//Bug No: 636,649,672. For Autogenerating Vin
			//Nikhil Garg		Dated: 20/Dec/2005
			XmlElement objNotReqNew = null;
			string sNotReqNew=string.Empty;
			if (objData.Context.InternalSettings.SysSettings.AutoNumVin)
			{
                //objNotReqNew=(XmlElement)base.SysView.SelectSingleNode("//internal[@name='SysNotReqNew']");
				//sNotReqNew=objNotReqNew.Attributes["value"].Value;
                objNotReqNew = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysNotReqNew");
                sNotReqNew = objNotReqNew.InnerText;

				if (sNotReqNew.IndexOf("vin")==-1)
					sNotReqNew=sNotReqNew + "vin|";
				//objNotReqNew.Attributes["value"].Value=sNotReqNew;
                objNotReqNew.InnerText = sNotReqNew; 

			}
		}
        private void DisplayUnitNo()
        {

            string sStaUnitNo = string.Empty;
            string sRiskLoc = string.Empty;
            string sSubLoc = string.Empty;
            string sUnitNo = string.Empty; //RMA-10039
            string sSystemType = objCache.GetShortCode(base.GetSysExDataNodeInt("//PolicySystemType"));

            StringBuilder sbSql = new StringBuilder();
            if (CommonFunctions.GetPolicySystemTypeIndicator(sSystemType) == Constants.POLICY_SYSTEM_TYPE.POINT)// vkumar258--5361 JIRA-added for Integral
            {
                sbSql.Append("SELECT STAT_UNIT_NUMBER,UNIT_RISK_LOC,UNIT_RISK_SUB_LOC FROM  POINT_UNIT_DATA WHERE ");
            }
            else if (CommonFunctions.GetPolicySystemTypeIndicator(sSystemType) == Constants.POLICY_SYSTEM_TYPE.INTEGRAL)// vkumar258--5361 JIRA-added for Integral
            {
                sbSql.Append("SELECT UNIT_NUMBER FROM  INTEGRAL_UNIT_DATA WHERE ");
            }
            else
            {// RMA-10039, Generic API processing -  15.2 release included Staging and cyberlife policies / units
             // The current approach of recording common data in separate POINT_UNIT_DATA and INTEGRAL_UNIT_DATA is obsolete 
             // and will be corrected when API consumes POINT and INTEGRAL policies
                sbSql.Append("SELECT UNIT_NUMBER,STAT_UNIT_NUMBER,UNIT_RISK_LOC,UNIT_RISK_SUB_LOC FROM POLICY_X_UNIT WHERE ");
            }

            sbSql.Append(" UNIT_TYPE='V' AND UNIT_ID=" + Vehicle.UnitId);
            DbReader objReader = null;
            objReader = DbFactory.GetDbReader(base.m_fda.connectionString, sbSql.ToString());
            if (objReader != null)
            {
                if (objReader.Read())
                {
                    if (CommonFunctions.GetPolicySystemTypeIndicator(sSystemType) == Constants.POLICY_SYSTEM_TYPE.POINT)
                {
                    sStaUnitNo = objReader.GetValue("STAT_UNIT_NUMBER").ToString();
                    sRiskLoc = objReader.GetValue("UNIT_RISK_LOC").ToString();
                    if (sRiskLoc == string.Empty)
                        sRiskLoc = "-";

                    sSubLoc = objReader.GetValue("UNIT_RISK_SUB_LOC").ToString();
                    if (sSubLoc == string.Empty)
                        sSubLoc = "-";

                    base.ResetSysExData("UnitNo", sStaUnitNo + "/" + sRiskLoc + "/" + sSubLoc);
                    }
                    else if (CommonFunctions.GetPolicySystemTypeIndicator(sSystemType) == Constants.POLICY_SYSTEM_TYPE.INTEGRAL)// vkumar258--5361 JIRA-added for Integral
                    {
                        sStaUnitNo = objReader.GetValue("UNIT_NUMBER").ToString();
                        base.ResetSysExData("UnitNo", sStaUnitNo);
                    }
                    else
                    {   // RMA-10039, Generic API processing -  15.2 release included Staging and cyberlife policies / units
                        // The current approach of recording common data in separate POINT_UNIT_DATA and INTEGRAL_UNIT_DATA is obsolete 
                        // and will be corrected when API consumes POINT and INTEGRAL policies

                        sUnitNo = objReader.GetValue("UNIT_NUMBER").ToString();
                        sRiskLoc = objReader.GetValue("UNIT_RISK_LOC").ToString();
                        if (!(sRiskLoc.Equals(string.Empty)))
                            sUnitNo = sUnitNo + "/" + sRiskLoc;

                        sSubLoc = objReader.GetValue("UNIT_RISK_SUB_LOC").ToString();
                       if (!(sSubLoc.Equals(string.Empty)))
                           sUnitNo = sUnitNo + "/" + sSubLoc;

                       base.ResetSysExData("UnitNo", sUnitNo);
                        
                    }

                }

            }


        }
	}
}
