using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using System.Text;


//TODO: Auto-Generated Form - migrate and verify behaviors from Legacy RMNet form.

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for ProviderContract Screen.
	/// </summary>
	public class ProviderContractForm : DataEntryFormBase
	{
		const string CLASS_NAME = "ProviderContracts";
		private ProviderContracts ProviderContracts{get{return objData as ProviderContracts;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		const string FILTER_KEY_NAME = "ProviderEid";
        private string sLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").Split('|')[0].ToString();
		public ProviderContractForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
            sLangCode = Convert.ToString(fda.userLogin.objUser.NlsCode);
		}

		public override void InitNew()
		{
			base.InitNew();
			ProviderContracts.ProviderEid=base.GetSysExDataNodeInt("/SysExData/EntityId" ,true);
			if (ProviderContracts.ProviderEid>0)
				(objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + ProviderContracts.ProviderEid;
		}
		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
			
			base.OnUpdateForm ();

			XmlDocument objSysExDataXML = base.SysEx;
			XmlElement objOrigValues = objSysExDataXML.CreateElement( "DFactor" );
			XmlNode objOrigValuesOld = objSysExDataXML.SelectSingleNode("//DFactor");
			objOrigValues.InnerText =  Convert.ToString(100-ProviderContracts.Discount);

			if(objOrigValuesOld !=null)
				objSysExDataXML.DocumentElement.ReplaceChild(objOrigValues,objOrigValuesOld);
			else
				objSysExDataXML.DocumentElement.AppendChild( objOrigValues );
	
			objOrigValues = objSysExDataXML.CreateElement( "DFactor2" );
			objOrigValuesOld = objSysExDataXML.SelectSingleNode("//DFactor2");
			objOrigValues.InnerText =  Convert.ToString(100-ProviderContracts.Discount2nd);
			
			if(objOrigValuesOld !=null)
				objSysExDataXML.DocumentElement.ReplaceChild(objOrigValues,objOrigValuesOld);
			else
				objSysExDataXML.DocumentElement.AppendChild( objOrigValues );

			objOrigValues = objSysExDataXML.CreateElement( "DFactorBill" );
			objOrigValuesOld = objSysExDataXML.SelectSingleNode("//DFactorBill");
			objOrigValues.InnerText =  Convert.ToString(100-ProviderContracts.DiscountBill);
			
			if(objOrigValuesOld !=null)
				objSysExDataXML.DocumentElement.ReplaceChild(objOrigValues,objOrigValuesOld);
			else
				objSysExDataXML.DocumentElement.AppendChild( objOrigValues );
			PopulateFeeScheduleCombo();
			PopulateFeeSchedule2Combo();
			PopulateStateList();
			int iFeeSchedId=ProviderContracts.FeeTableId;
			int iStateID=0;
			if(iFeeSchedId<0)
			{
				iStateID=(iFeeSchedId+100)*(-1);
				iFeeSchedId=0;
			}
			 
			base.ResetSysExData("FeeSchedId",iFeeSchedId.ToString());
			base.ResetSysExData("StateFeeId",iStateID.ToString());

		}
		public override void BeforeSave(ref bool Cancel)
		{
			//If we were sent here from a parent(event) then we'll have an eventid in the SysEx
			//originally from ScreenFlow\SysExData.
			//base.BeforeSave (ref Cancel);
			//XmlDocument objXML = base.SysEx;
            //MITS 9971  : Umesh/Pawan

			XmlNode objProviderEid=null;
			try 
                {//objProviderEid = objXML.GetElementsByTagName("EntityId")[0];

                objProviderEid = base.SysEx.SelectSingleNode("/SysExData/EntityId");
                }
			catch{};
			if(objProviderEid !=null)
				if(this.ProviderContracts.ProviderEid ==0)
					this.ProviderContracts.ProviderEid = Conversion.ConvertStrToInteger(objProviderEid.InnerText);
		}
		
		public override void AfterAddNew()
		{
			base.AfterAddNew ();
            //MITS 9971  : Umesh/Pawan
			//XmlDocument objXML = base.SysEx;
			XmlNode objProviderEid=null;
			try
                {//objProviderEid = objXML.GetElementsByTagName("EntityId")[0];
                objProviderEid = base.SysEx.SelectSingleNode("/SysExData/EntityId");
            
                }
			catch{};
			
			if(objProviderEid !=null)
			{
				ProviderContracts.ProviderEid=Conversion.ConvertStrToInteger(objProviderEid.InnerText);
			}
            ProviderContracts.DiscOnBillFlag = Conversion.ConvertObjToBool(objData.Context.InternalSettings.BrsSettings.DiscOnBillFlag, base.ClientId);
            ProviderContracts.DiscOnSchdFlag = Conversion.ConvertObjToBool(objData.Context.InternalSettings.BrsSettings.DiscOnSchdFlag, base.ClientId);
            ProviderContracts.Disc2ndSchdFlag = Conversion.ConvertObjToBool(objData.Context.InternalSettings.BrsSettings.Disc2ndSchdFlag, base.ClientId);
							  
		}
		private void  PopulateFeeScheduleCombo()
		{
			XmlDocument objSysExDataXML = base.SysEx;
			XmlElement objOrigValues   = objSysExDataXML.CreateElement( "FeeSchedList" );
			XmlNode objOrigValuesOld = objSysExDataXML.SelectSingleNode("//FeeSchedList");
			XmlCDataSection objCData=null;
			XmlElement xmlOption;
			XmlAttribute xmlOptionAttrib;
			
			xmlOption = objSysExDataXML.CreateElement("option");
			
			xmlOption.InnerText="";
			xmlOptionAttrib = objSysExDataXML.CreateAttribute("value");
			xmlOptionAttrib.Value ="0";
			xmlOption.Attributes.Append(xmlOptionAttrib);
			objOrigValues.AppendChild(xmlOption);
			foreach(DictionaryEntry entry in  ProviderContracts.FeeScheds)
			{
				xmlOption = objSysExDataXML.CreateElement("option");
				objCData = objSysExDataXML.CreateCDataSection(Conversion.ConvertObjToStr(entry.Value));
				xmlOption.AppendChild(objCData);
				xmlOptionAttrib = objSysExDataXML.CreateAttribute("value");
				xmlOptionAttrib.Value =Conversion.ConvertObjToStr(entry.Key);
				xmlOption.Attributes.Append(xmlOptionAttrib);
				objOrigValues.AppendChild(xmlOption);
			}
			if(objOrigValuesOld !=null)
				objSysExDataXML.DocumentElement.ReplaceChild(objOrigValues,objOrigValuesOld);
			else
				objSysExDataXML.DocumentElement.AppendChild( objOrigValues );
		}
		private void  PopulateFeeSchedule2Combo()
		{
			XmlDocument objSysExDataXML = base.SysEx;
			XmlElement objOrigValues= objSysExDataXML.CreateElement( "FeeSchedList2" );
			XmlNode objOrigValuesOld  = objSysExDataXML.SelectSingleNode("//FeeSchedList2");
			XmlCDataSection objCData=null;
			XmlElement xmlOption;
			XmlAttribute xmlOptionAttrib;
			
			xmlOption = objSysExDataXML.CreateElement("option");
			
			xmlOption.InnerText="";
			xmlOptionAttrib = objSysExDataXML.CreateAttribute("value");
			xmlOptionAttrib.Value ="0";
			xmlOption.Attributes.Append(xmlOptionAttrib);
			objOrigValues.AppendChild(xmlOption);
			foreach(DictionaryEntry entry in  ProviderContracts.FeeScheds2)
			{
				xmlOption = objSysExDataXML.CreateElement("option");
				objCData = objSysExDataXML.CreateCDataSection(Conversion.ConvertObjToStr(entry.Value));
				xmlOption.AppendChild(objCData);
				xmlOptionAttrib = objSysExDataXML.CreateAttribute("value");
				xmlOptionAttrib.Value = Conversion.ConvertObjToStr(entry.Key);
				xmlOption.Attributes.Append(xmlOptionAttrib);
				objOrigValues.AppendChild(xmlOption);
			}
			if(objOrigValuesOld !=null)
				objSysExDataXML.DocumentElement.ReplaceChild(objOrigValues,objOrigValuesOld);
			else
				objSysExDataXML.DocumentElement.AppendChild( objOrigValues );
		}
		private void  PopulateStateList()
		{
			XmlDocument objSysExDataXML = base.SysEx;
			XmlElement objOrigValues = objSysExDataXML.CreateElement( "StateList" );
			XmlNode objOrigValuesOld = objSysExDataXML.SelectSingleNode("//StateList");
			XmlCDataSection objCData=null;
			XmlAttribute xmlOptionAttrib;
			XmlElement xmlOption = objSysExDataXML.CreateElement("option");
			
			xmlOption.InnerText="";
			xmlOptionAttrib = objSysExDataXML.CreateAttribute("value");
			xmlOptionAttrib.Value ="0";
			xmlOption.Attributes.Append(xmlOptionAttrib);
			objOrigValues.AppendChild(xmlOption);
			string sSQL = "SELECT STATES.STATE_ROW_ID, STATES.STATE_ID, STATES.STATE_NAME" +
				" FROM STATES WHERE STATE_ROW_ID > 0 ORDER BY STATES.STATE_ID" ;

            using (DbReader objReader = ProviderContracts.Context.DbConn.ExecuteReader(sSQL))
            {
                while (objReader.Read())
                {
                    xmlOption = objSysExDataXML.CreateElement("option");
                    objCData = objSysExDataXML.CreateCDataSection(Conversion.ConvertObjToStr(objReader.GetString(1)) + " - " + Conversion.ConvertObjToStr(objReader.GetString(2)));
                    xmlOption.AppendChild(objCData);
                    xmlOptionAttrib = objSysExDataXML.CreateAttribute("value");
                    xmlOptionAttrib.Value = objReader.GetInt32(0).ToString();
                    xmlOption.Attributes.Append(xmlOptionAttrib);
                    objOrigValues.AppendChild(xmlOption);
                }
            }
			if(objOrigValuesOld !=null)
				objSysExDataXML.DocumentElement.ReplaceChild(objOrigValues,objOrigValuesOld);
			else
				objSysExDataXML.DocumentElement.AppendChild( objOrigValues );
            //objReader.Close();
		}
		
		public override void OnValidate(ref bool Cancel)
		{
			bool bError = false;
			
			// Perform data validation
			StringBuilder sbSql=new StringBuilder();
			sbSql.Append("SELECT PRVD_CONT_ROW_ID");
			sbSql.Append(" FROM PROVIDER_CONTRACTS");
			sbSql.Append(" WHERE ('" + ProviderContracts.StartDate + "' BETWEEN START_DATE AND END_DATE");
			sbSql.Append(" OR '" + ProviderContracts.EndDate + "' BETWEEN START_DATE AND END_DATE)"); 
			sbSql.Append(" AND PROVIDER_EID = " + ProviderContracts.ProviderEid);
			if(ProviderContracts.PrvdContRowId!=0)
			{
				sbSql.Append("AND PRVD_CONT_ROW_ID <> " + ProviderContracts.PrvdContRowId);
			}
            using (DbReader objReader = objData.Context.DbConn.ExecuteReader(sbSql.ToString()))
            {
                if (objReader.Read())
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        Globalization.GetString("Validation.ProviderContractOverLap", base.ClientId, sLangCode),
                        BusinessAdaptorErrorType.Error);

					//abisht BRS Issue.Changed the code.need to set the value of bError not Cancel variable.
					//Cancel = true;
                    bError = true;
                }
            }
            //objReader.Close();
			sbSql=null;


			int iFeeSchedId=0;
			int iStateID=0;
			//Only Apply Entity Type Change (if any requested) on a New Record.
			XmlNode objOrig = base.SysEx.SelectSingleNode("/*/FeeSchedId");
			if(objOrig !=null)
			{
				iFeeSchedId=Conversion.ConvertStrToInteger(objOrig.InnerText);

			}
			objOrig=null;
			objOrig=base.SysEx.SelectSingleNode("/*/StateFeeId");
			if(objOrig !=null)
			{
				iStateID=Conversion.ConvertStrToInteger(objOrig.InnerText);

			}
			if(ProviderContracts.DiscOnSchdFlag==false)
			{
                //abisht ProviderContracts.FeeTableId=0; changed from zero to -1 to make it compatible to RMWorld
				ProviderContracts.FeeTableId=-1;
				ProviderContracts.Discount=0;
			}
			else
			{
				if(iFeeSchedId==0 && iStateID!=0)
				{
					ProviderContracts.FeeTableId=(-1)* (iStateID+100);
				}
				else
				{
					if(iFeeSchedId!=0 && iStateID==0)
					{
						ProviderContracts.FeeTableId=iFeeSchedId;
					}
					else
					{
						if(iFeeSchedId!=0 && iStateID!=0)
						{
                            Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                                Globalization.GetString("Validation.NonZeroFeeSchduleAndState", base.ClientId, sLangCode),
								BusinessAdaptorErrorType.Error);
                            bError = true;
							//abisht BRS Issue.Changed the code.need to set the value of berror not Cancel variable.
							//Cancel = true;
						}
						else
						{
							if(iFeeSchedId==0 && iStateID==0)
							{
                                Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                                    Globalization.GetString("Validation.ZeroFeeSchduleAndState", base.ClientId, sLangCode),
									BusinessAdaptorErrorType.Error);
                                bError = true;
								//abisht BRS Issue.Changed the code.need to set the value of berror not Cancel variable.
								//Cancel = true;
							}
						}
					}
				}
			}


			if(ProviderContracts.Disc2ndSchdFlag==false)
			{
                //abisht ProviderContracts.FeeTableId2nd=0; changed from zero to -1 to make it compatible to RMWorld
				ProviderContracts.FeeTableId2nd=-1;
				ProviderContracts.Discount2nd=0;
			}
			else
			{
				if(ProviderContracts.FeeTableId2nd==0)
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        Globalization.GetString("Validation.MustSelectFeeSchedule", base.ClientId, sLangCode),
						BusinessAdaptorErrorType.Error);
                    bError = true;
					//abisht BRS Issue.Changed the code.need to set the value of berror not Cancel variable.
					//Cancel = true;		
				}
			}
	
			if(ProviderContracts.DiscOnBillFlag==false)
			{
				ProviderContracts.DiscountBill=0;
			}
			if (ProviderContracts.DiscOnSchdFlag==false && ProviderContracts.Disc2ndSchdFlag==false && ProviderContracts.DiscOnBillFlag==false
				&& ProviderContracts.FirstRate==0 && ProviderContracts.Amount==0)
			{
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                    Globalization.GetString("Validation.MustSelectCombination", base.ClientId, sLangCode),
					BusinessAdaptorErrorType.Error);
							
				bError = true;
			}
			if (ProviderContracts.Discount>100 || ProviderContracts.Discount2nd>100 ||ProviderContracts.DiscountBill>100)
			{
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                    Globalization.GetString("Validation.DiscountExceed", base.ClientId, sLangCode),
					BusinessAdaptorErrorType.Error);
		
				bError = true;
			}

			if (ProviderContracts.StartDate!=string.Empty && ProviderContracts.EndDate!=string.Empty)
			{
				if(ProviderContracts.StartDate.CompareTo(ProviderContracts.EndDate)>0)
				{
                    //Errors.Add(Globalization.GetString("ValidationError"),
                    //    String.Format(Globalization.GetString("Validation.MustBeLessThan"), Globalization.GetString("Field.StartDate"), Globalization.GetString("Field.EndDate")),
                    //    BusinessAdaptorErrorType.Error);
                    //Aman MITS 31298
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode), Globalization.GetString("StartDateMustlessthanEndDate", base.ClientId, sLangCode), BusinessAdaptorErrorType.Error);
					bError = true;
				}
			}
			if(ProviderContracts.SecondRate > 0)
        
			{   //if enter a second rate must enter a first rate
				if(ProviderContracts.FirstRate == 0 )
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustSelectWithPerDiemRate", base.ClientId, sLangCode), Globalization.GetString("Field.SecondRate", base.ClientId, sLangCode), Globalization.GetString("Field.FirstRate", base.ClientId, sLangCode)),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}
				if(ProviderContracts.RateChg == 0 )
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustSelectWithPerDiemRate", base.ClientId, sLangCode), Globalization.GetString("Field.SecondRate", base.ClientId, sLangCode), Globalization.GetString("Field.Day2ndRateStarts", base.ClientId, sLangCode)),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}
				if(ProviderContracts.RateChg <2 )
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.2ndRateStartsMustBeGreaterThanOne", base.ClientId, sLangCode)),
						BusinessAdaptorErrorType.Error);
			
					bError = true;
				}
			}
			if (ProviderContracts.RateChg>0 && ProviderContracts.SecondRate==0)
			{
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                    String.Format(Globalization.GetString("Validation.RateStarts", base.ClientId, sLangCode), Globalization.GetString("Field.Day2ndRateStarts", base.ClientId, sLangCode), Globalization.GetString("Field.SecondRate", base.ClientId, sLangCode)), BusinessAdaptorErrorType.Error);
		
				bError = true;
			}
					
			if(ProviderContracts.LastRate > 0)
        
			{   //if enter a second rate must enter a first rate
				if(ProviderContracts.SecondRate == 0 )
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustSelectWithPerDiemRate", base.ClientId, sLangCode), Globalization.GetString("Field.LastRate", base.ClientId, sLangCode), Globalization.GetString("Field.SecondRate", base.ClientId, sLangCode)),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}
				if(ProviderContracts.RateChg2nd == 0 )
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustSelectWithPerDiemRate", base.ClientId, sLangCode), Globalization.GetString("Field.LastRate", base.ClientId, sLangCode), Globalization.GetString("Field.DayLastRateStarts", base.ClientId, sLangCode)),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}
				if(ProviderContracts.RateChg2nd <ProviderContracts.RateChg )
				{
                    //Errors.Add(Globalization.GetString("ValidationError"),
                    //    String.Format(Globalization.GetString("Validation.MustBeGreater"),Globalization.GetString("Field.DayLastRateStarts"),Globalization.GetString("Field.Day2ndRateStarts")),
                    //    BusinessAdaptorErrorType.Error);
                    //Aman MITS 31298   
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode), Globalization.GetString("Validation.LastRateMustBeGreater", base.ClientId, sLangCode), BusinessAdaptorErrorType.Error);
                                   
					bError = true;
				}
			}
			if (ProviderContracts.RateChg2nd>0 && ProviderContracts.LastRate==0)
			{
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                    String.Format(Globalization.GetString("Validation.RateStarts", base.ClientId, sLangCode), Globalization.GetString("Field.DayLastRateStarts", base.ClientId, sLangCode), Globalization.GetString("Field.LastRate", base.ClientId, sLangCode))
					,BusinessAdaptorErrorType.Error);
		
				bError = true;
			}
			//abisht BRS Issue.Changed the If condition.The message should not be shown incase if there is any error in the code above.
			if(ProviderContracts.DiscOnSchdFlag==false && ProviderContracts.Disc2ndSchdFlag==false &&
				ProviderContracts.DiscOnBillFlag==false && ProviderContracts.FirstRate!=0 &&
				ProviderContracts.Amount==0 && bError != true)
			{
				/*TO DO->Alerts need to be worked upon*/
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                    String.Format(Globalization.GetString("Alert.PerDiemRatesApplyDiscount", base.ClientId, sLangCode)),
					BusinessAdaptorErrorType.Error);
				//abisht BRS Issue.No need to set the value of bError here as the contract is saved in this case.
				//bError = false;
			}
			//abisht BRS Issue.Changed the If condition.The message should not be shown incase if there is any error in the code above.
			if(ProviderContracts.DiscOnSchdFlag==false && ProviderContracts.Disc2ndSchdFlag==false &&
				ProviderContracts.DiscOnBillFlag!=false && ProviderContracts.FirstRate!=0 &&
				ProviderContracts.Amount==0 && ProviderContracts.DiscountBill==0 && bError != true)
			{
				/*TO DO->Alerts need to be worked upon*/
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                    String.Format(Globalization.GetString("Alert.PerDiemRatesDiscount", base.ClientId, sLangCode)),
					BusinessAdaptorErrorType.Error);
				//abisht BRS Issue.No need to set the value of bError here as the contract is saved in this case.
				//bError = false;
			}
						
			Cancel=bError;

		}
	}
	

}