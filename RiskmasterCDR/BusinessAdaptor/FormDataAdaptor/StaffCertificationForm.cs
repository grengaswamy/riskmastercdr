using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// StaffCertification Screen.
	/// </summary>
	public class StaffCertificationForm : DataEntryFormBase
	{
		const string CLASS_NAME = "MedicalStaffCert";
		const string FILTER_KEY_NAME = "StaffEid";

		private MedicalStaffCert MedicalStaffCert{get{return objData as MedicalStaffCert;}}

		private LocalCache objCache{get{return objData.Context.LocalCache;}}
	    
		/// <summary>
		/// Class constructor to initialize Class Name information
		/// </summary>
		/// <param name="fda"></param>
		public StaffCertificationForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

		/// <summary>
		/// Initializes the class and sets the parent ID for the class
		/// that can be used for parent-child navigation
		/// </summary>
		public override void InitNew()
		{
			base.InitNew();

			//set filter for StaffEid
			MedicalStaffCert.StaffEid=base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME,false);
			if (MedicalStaffCert.StaffEid>0)
				(objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + MedicalStaffCert.StaffEid;
		}

//		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
//		public override void OnUpdateForm()
//		{
//			base.OnUpdateForm ();
//			if(MedicalStaffCert.StaffEid==0)
//			{
//				AfterAddNew();
//			}
//		}
//
//		public override void AfterAddNew()
//		{
//			base.AfterAddNew ();
//
//			XmlDocument objXML = base.SysEx;
//			XmlNode objStaffEid=null;
//			try{objStaffEid = objXML.GetElementsByTagName(FILTER_KEY_NAME)[0];}
//			catch{};
//			
//			//Filter by this staffeid if Present.
//			if(objStaffEid !=null)
//			{
//				MedicalStaffCert.StaffEid=Conversion.ConvertStrToInteger(objStaffEid.InnerText);
//			}
//		}

		public override void OnValidate(ref bool Cancel)
		{
			bool bError = false;
			
			// Perform data validation
			if (MedicalStaffCert.IntDate!="" && MedicalStaffCert.EndDate!="")
			{
				if(MedicalStaffCert.IntDate.CompareTo(MedicalStaffCert.EndDate)>0)
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThanStartDate", base.ClientId), Globalization.GetString("Field.EndDate", base.ClientId), Conversion.ToDate(MedicalStaffCert.IntDate).ToShortDateString(), Globalization.GetString("Field.StartDate", base.ClientId)),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}
			}
			else 
			{
				if(MedicalStaffCert.IntDate=="" && MedicalStaffCert.EndDate!="")
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        string.Format(Globalization.GetString("Validation.MustBeValidDate", base.ClientId), Globalization.GetString("Field.StartDate", base.ClientId)),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}
				else
				{
					if(MedicalStaffCert.IntDate!="" && MedicalStaffCert.EndDate=="")
					{
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                            string.Format(Globalization.GetString("Validation.MustBeValidDate", base.ClientId), Globalization.GetString("Field.EndDate", base.ClientId)),
							BusinessAdaptorErrorType.Error);

						bError = true;
					}
				}
			}
			// Return true if there were validation errors
			Cancel = bError;
		}
	}
}