﻿using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
    class BankingInfoForm : DataEntryFormBase
    {


         const string CLASS_NAME = "BankingInfo";
         const string FILTER_KEY_NAME = "ParentId";

        private BankingInfo BankingInfo { get { return objData as BankingInfo; } }
        private LocalCache objCache { get { return objData.Context.LocalCache; } }

        public BankingInfoForm(FormDataAdaptor fda)
            : base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}




    public override void InitNew()
		{
            string sParentSysName = string.Empty;
			base.InitNew();
            BankingInfo.ParentId = base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME, false);
            if (BankingInfo.ParentId > 0)
            {
                (objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + BankingInfo.ParentId;
            } 
        string sParentFormName = string.Empty;
           sParentFormName=  base.GetSysExDataNodeText("/SysExData/ParentFormName", false);
           if (sParentFormName.Equals("funds") || sParentFormName.Equals("autoclaimchecks") || sParentFormName.Equals("thirdpartypayments"))
           {
               BankingInfo.BankingStatus = objCache.GetCodeId("A", "EFT_BANKING_STATUS");
           }
       
		}
       public override void OnUpdateObject()
        {
            base.OnUpdateObject();

        }


       public override void AfterApplyLookupData(XmlDocument objPropertyStore)
       {
           XmlDocument objTargetDoc = new XmlDocument();
           XmlDocument objConfigDom = new XmlDocument();
           XmlNode objSource = null;
           XmlNode objTarget = null;


           string sTempXml = objPropertyStore.SelectSingleNode("Instance").InnerXml;

           objPropertyStore.LoadXml(sTempXml);

           base.AfterApplyLookupData(objPropertyStore);

           if (base.SysLookup.SelectSingleNode("/SysLookup/SysLookupAttachNodePath").InnerText.IndexOf("BankEntity") > 0)
           {
              
               objConfigDom.LoadXml("<BankingInfo><BankEntity/></BankingInfo>");
               objTargetDoc.LoadXml(this.BankingInfo.SerializeObject(objConfigDom));

               //Bank Entity data copied from PropertyStore to the dummy document
               objSource = (XmlNode)objTargetDoc.SelectSingleNode("//BankEntity");
               objTarget = objTargetDoc.CreateElement(objSource.Name);
               objTarget.InnerXml = objPropertyStore.SelectSingleNode("//BankEntity").InnerXml;
               objSource.ParentNode.ReplaceChild(objTarget, objSource);
               objSource = null;
               objTarget = null;

               //Replacing the PropertyStore with dummy document
               objSource = (XmlNode)objPropertyStore.SelectSingleNode("//BankingInfo");
               objTarget = objPropertyStore.CreateElement(objSource.Name);
               objTarget.InnerXml = objTargetDoc.SelectSingleNode("//BankingInfo").InnerXml;
               objSource.ParentNode.ReplaceChild(objTarget, objSource);

               objSource = null;
               objTarget = null;
           }
       }

       public override void OnUpdateForm()
       {
           base.OnUpdateForm();
           if (base.GetSysExDataNodeText("/SysExData/ParentFormName", false).Equals("funds") || base.GetSysExDataNodeText("/SysExData/ParentFormName", false).Equals("autoclaimchecks") || base.GetSysExDataNodeText("/SysExData/ParentFormName", false).Equals("thirdpartypayments")) 
          {
              base.AddReadOnlyNode("bankingstatus");
          }

       }
       public override void OnValidate(ref bool Cancel)
       {
           bool bError = false;
            string sSQL= string.Empty;
            //nsachdeva2 - MITS:27917 - 03/22/2012
            //if (BankingInfo.IsNew && BankingInfo.BankingStatus == objCache.GetCodeId("A", "EFT_BANKING_STATUS"))
       //Added by bsharma33 MITS  # 31254
       if (this.BankingInfo.ExpirationDate.CompareTo(this.BankingInfo.EffectiveDate) < 0)
            {
                Errors.Add(Globalization.GetString("ValidationError",base.ClientId), Globalization.GetString("Validation.MustBeGreaterThanEffectiveDate",base.ClientId), BusinessAdaptorErrorType.Error);
                bError = true;
            }
       // End changes bsharma33 MITS  # 31254
         if (BankingInfo.BankingStatus == objCache.GetCodeId("A", "EFT_BANKING_STATUS"))
           {

                sSQL      = "SELECT BANK_NAME FROM  ENTITY_X_BANKING WHERE ENTITY_ID = ";
           sSQL = sSQL + BankingInfo.ParentId;
           sSQL = sSQL + "AND BANK_STATUS_CODE = " + objCache.GetCodeId("A", "EFT_BANKING_STATUS");
         

           try
           {
               using(  DbReader objReader =  DbFactory.GetDbReader(BankingInfo.Context.DbConn.ConnectionString,sSQL))
               {
                   //Added:Yukti,DT:02/10/2014,MITS 35256
                   //Changing this condition if first Bank Account status is set to Pending and then changed to Active from Details Window.
                   //if (objReader.Read() && objData.KeyFieldValue == 0)
                   if (objReader.Read())
                   //Ended:Yukti,Dt:02/10/2014, MITS 35256
                   {
                       Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                                         Globalization.GetString("Validation.AcceptedBankExists",base.ClientId), BusinessAdaptorErrorType.Error);

                       bError = true;
                   }
               }
               
               Cancel = bError;
           }
          
          
           
           
           catch (Exception e)
           {
               throw e;
           }

       }
      else
       {
           Cancel = bError;
       }

       }
}
}
