using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// MedwatchTest Screen.
	/// </summary>
	public class MedwatchTestForm : DataEntryFormBase
	{
		const string CLASS_NAME = "EventXMedwTest";
		const string FILTER_KEY_NAME = "EventId";

		private EventXMedwTest EventXMedwTest{get{return objData as EventXMedwTest;}}

		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		
		/// <summary>
		/// Class constructor which initializes Class Name information
		/// </summary>
		/// <param name="fda"></param>
		public MedwatchTestForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

		/// <summary>
		/// Initializes the class and sets the parent ID for the class
		/// that can be used for parent-child navigation
		/// </summary>
		public override void InitNew()
		{
			base.InitNew();

			//set filter for EventId
			EventXMedwTest.EventId=base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME,false);
			if (EventXMedwTest.EventId>0)
				(objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + EventXMedwTest.EventId;

		}//end method InitNew()

//		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
//		public override void OnUpdateForm()
//		{
//			base.OnUpdateForm ();
//		}
	}
}