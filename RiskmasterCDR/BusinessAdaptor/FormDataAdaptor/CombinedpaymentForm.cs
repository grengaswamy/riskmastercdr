﻿using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using Riskmaster.Settings;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using Riskmaster.Application.FundManagement;


namespace Riskmaster.BusinessAdaptor
{
    /// <summary>
    /// Manages data for combined Payment form
    /// </summary>
    public class CombinedpaymentForm : DataEntryFormBase
    {   
        #region security codes
        private const int RMO_ACCESS = 0;
        private const int RMO_VIEW = 1;
        private const int RMO_UPDATE = 2;
        private const int RMO_CREATE = 3;
       #endregion

        #region Constants
        const string CLASS_NAME = "CombinedPayment";
        const string INSTANCE_SYSEXDATA_PATH = "/Instance/UI/FormVariables/SysExData";
        #endregion

        #region Variables


        private static string sBaseCurrCulture = string.Empty;//skhare7 R8 Multicurrency



      
        private FundManager m_objFundManager = null;
        private string m_sOldSelectedPayeeTypeCode = string.Empty;
    
        private string m_sConnectionString = "";

        private bool m_bPayeePaymentsFrozen = false;
        
        private string sFromRsvListingPage = string.Empty;
      
        #endregion

        #region Properties
        private CombinedPayment CombinedPayment { get { return objData as CombinedPayment; } }
        private LocalCache objCache { get { return objData.Context.LocalCache; } }
        private DataModelFactory Factory { get { return objData.Context.Factory; } }
        //skhare7 R8 Combined Payment defects removed
       
        #endregion

        #region Constructor
        public CombinedpaymentForm(FormDataAdaptor fda)
            : base(fda)
        {
            base.m_ClassName = CLASS_NAME;
            //Changed by Gagan for MITS 11991 : Start
            m_sConnectionString = fda.connectionString;
            //Changed by Gagan for MITS 11991 : End
        }

        #endregion

        #region OnUpdateObject
       
        public override void InitNew()
        {
            base.InitNew();
            CombinedPayment.EntityId = base.GetSysExDataNodeInt("/SysExData/PayeeEid", false);
         
        }
        public override void OnUpdateObject()
        {

          

            base.OnUpdateObject();
            CombinedPayment CombPayment = CombinedPayment;
            int iTemp = 0;
            XmlDocument objFundsXmlDoc = new XmlDocument();

            XmlNode objCombPayNode = base.SysEx.SelectSingleNode("/SysExData/CombinedPayment");
            bool bFlag = false;
            // Update the AccountId in case SubAccount In Use.
          

           
            if (CombPayment.Context.InternalSettings.SysSettings.UseFundsSubAcc)
            {
                // DropDown represents SubBankAccountId 
                BankAccSub objBankAccSub = (BankAccSub)this.CombinedPayment.Context.Factory.GetDataModelObject("BankAccSub", false);
                objBankAccSub.MoveTo(Conversion.CastToType<Int32>((base.SysEx.SelectSingleNode("/SysExData/CombinedPayment/SubAccId").InnerText),out bFlag));


                XmlElement objNew = null;
                objNew = base.SysEx.CreateElement("AccountId");
                XmlNode objFundsXmlNode = base.SysEx.SelectSingleNode("/SysExData/CombinedPayment");
                objFundsXmlNode.AppendChild(objNew);
                CombPayment.SubAccId = objBankAccSub.SubRowId;
                CombPayment.AccountId = objBankAccSub.AccountId;
                objCombPayNode.AppendChild(objNew);

                base.SysEx.SelectSingleNode("/SysExData/CombinedPayment/AccountId").InnerText = objBankAccSub.AccountId.ToString();
            }
            else
            {

                objCombPayNode = base.SysEx.DocumentElement.SelectSingleNode("/SysExData/CombinedPayment");
                iTemp = Conversion.CastToType<Int32>((objCombPayNode.SelectSingleNode("AccountId").InnerText),out bFlag);
                CombinedPayment.AccountId = iTemp;
                CombinedPayment.SubAccId = 0;
            }

        
            if (CombPayment.NextPrintOverride != string.Empty)
            {
                CombPayment.NextSchPrintDate = CombPayment.NextPrintOverride;


            }
          
            
        }
        public void setAccSubAccIds()
        {
            string sAccID = "0";
            string sSubAccID = "0";
            XmlElement objRootNode = null;
            XmlElement objCombPayRootNode = null;

            objRootNode = (XmlElement)base.SysEx.SelectSingleNode("/SysExData");
            CreateElement(objRootNode, "CombinedPayment", ref objCombPayRootNode);


            using (DbReader rdr = CombinedPayment.Context.DbConnLookup.ExecuteReader("SELECT ACCOUNT_ID, SUB_ACCOUNT_ID FROM COMBINED_PAYMENTS WHERE COMBINED_PAY_ROW_ID = " + CombinedPayment.CombPayRowId))
            {
                if (rdr != null)
                {
                    if (rdr.Read())
                    {
                        sAccID = Conversion.ConvertObjToStr(rdr.GetValue("ACCOUNT_ID"));
                        sSubAccID = Conversion.ConvertObjToStr(rdr.GetValue("SUB_ACCOUNT_ID"));
                    }
                    rdr.Close();
                }
            }
            this.CreateAndSetElement(objCombPayRootNode, "AccountId", sAccID);
            this.CreateAndSetElement(objCombPayRootNode, "SubAccId", sSubAccID);

        }
     
        #endregion

      

       

        #region OnUpdateForm
        // Vaibhav 14 Feb 2006 :
        // Following function has been reconstructed and based on the SortedLists of payments.
        // Remove dependency on the Database sort.
        public override void OnUpdateForm()
        {

            


            m_bPayeePaymentsFrozen = CheckForPayeeFreezePayments();
            AppendBankAccountList();
            setAccSubAccIds();


            //Deb Multi Currency
            int iBaseCurrCode = CombinedPayment.Context.InternalSettings.SysSettings.BaseCurrencyType;
            if (CombinedPayment.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
            {
                if (CombinedPayment.IsNew)
                {
                    if (iBaseCurrCode > 0)
                    {
                        if (string.IsNullOrEmpty(sBaseCurrCulture))
                            sBaseCurrCulture = CombinedPayment.Context.DbConn.ExecuteString("SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + iBaseCurrCode);
                        base.ResetSysExData("BaseCurrencyType", sBaseCurrCulture.Split('|')[1]);

                        if (CombinedPayment.CombPmtCurrencyType <= 0)
                        {
                            CombinedPayment.CombPmtCurrencyType = iBaseCurrCode;
                        }
                    }
                    else
                    {
                        Errors.Add(Globalization.GetString("BaseCurrency.Error", base.ClientId), "Please set the Base Currency in the Utilities", BusinessAdaptorErrorType.Error);
                    }
                }
                else
                {
                    if (iBaseCurrCode > 0)
                    {
                        if (string.IsNullOrEmpty(sBaseCurrCulture))
                            sBaseCurrCulture = CombinedPayment.Context.DbConn.ExecuteString("SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + iBaseCurrCode);
                        base.ResetSysExData("BaseCurrencyType", sBaseCurrCulture.Split('|')[1]);
                        if (CombinedPayment.CombPmtCurrencyType <= 0)
                        {
                            CombinedPayment.CombPmtCurrencyType = iBaseCurrCode;
                        }
                       
                        
                    }
                    else
                    {
                        Errors.Add(Globalization.GetString("BaseCurrency.Error", base.ClientId), "Please set the Base Currency in the Utilities", BusinessAdaptorErrorType.Error);
                    }
                    
                }
            }
            else
            {
                if (iBaseCurrCode > 0)
                {
                    if (string.IsNullOrEmpty(sBaseCurrCulture))
                        sBaseCurrCulture = CombinedPayment.Context.DbConn.ExecuteString("SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + iBaseCurrCode);
                    base.ResetSysExData("BaseCurrencyType", sBaseCurrCulture.Split('|')[1]);
                    if (CombinedPayment.CombPmtCurrencyType <= 0)
                    {
                        CombinedPayment.CombPmtCurrencyType = iBaseCurrCode;
                    }
                    base.AddKillNode("currencytypetext");
                    base.AddKillNode("pmtcurrencytypetext");
                }
            }
           
           // base.OnUpdateForm();
          
//Added by Amitosh for R8 enhancement of Combined Payment
            if (!(CommonFunctions.IsScheduleDatesOn("SCH_DATE_COMBINED_PMT", CombinedPayment.Context.DbConn.ConnectionString, base.ClientId)))
            {
                base.AddKillNode("adjustprintdatesflag");
            }
          
        }

        /// <summary>
        /// Check to See if Payee has Frozen Payments.
        /// </summary>
        /// <returns></returns>
        public bool CheckForPayeeFreezePayments()
        {
            bool bPayeeFreezePayments = false;
            CombinedPayment CombPayment = CombinedPayment;
            if ((CombinedPayment.CombPayRowId > 0) && (CombinedPayment.EntityId > 0))
            {
                string sSQL = "SELECT FREEZE_PAYMENTS FROM ENTITY WHERE ENTITY_ID = " + CombinedPayment.EntityId;

                using (DbReader rdr = CombinedPayment.Context.DbConnLookup.ExecuteReader(sSQL))
                {
                    if (rdr.Read())
                    {
                        bPayeeFreezePayments = rdr.GetBoolean("FREEZE_PAYMENTS");
                    }
                    if (rdr != null)
                    {
                        rdr.Close();
                        rdr.Dispose();
                    }
                }
            }
            return bPayeeFreezePayments;
        }

        #region Appened Data for BankAccount Combobox List
        private void AppendBankAccountList()
        {
            //Variable declarations
            ArrayList arrAcctList = new ArrayList();
            XmlDocument objXML = base.SysEx;
            XmlNode objOld = null;
            XmlElement objNew = null;
            XmlCDataSection objCData = null;
            //Parijat: Mits 9378---since the fundManager was not being populated with the claims data.
          //  string claimNumber = base.GetSysExDataNodeText("//SysExternalParam/ClaimNumber");
        int claimId = base.GetSysExDataNodeInt("//SysExternalParam/ClaimId");
            string sDSNName = base.m_fda.userLogin.objRiskmasterDatabase.DataSourceName;
            string sUserName = base.m_fda.userLogin.LoginName;
            string sPassword = base.m_fda.userLogin.Password;
            int iUserId = base.m_fda.userLogin.UserId;
            int iGroupId = base.m_fda.userLogin.GroupId;
            //abisht MITS 10875
            //mcapps2 MITS 19769
            string sAccountName = string.Empty;
            int iThisAccountId = 0;
            int iThisSubAccountId = 0;
            bool bThisAccountIdFound = false;


            //if (claimId == 0)
            //{
            //    claimId = base.GetSysExDataNodeInt("//FundsAuto/ClaimId");
            //}

            iThisAccountId = base.GetSysExDataNodeInt("//CombinedPayment/AccountId");
            iThisSubAccountId = base.GetSysExDataNodeInt("//CombinedPayment/SubAccId");

            FundManager m_objFundMngr = new FundManager(sDSNName, sUserName, sPassword, iUserId, iGroupId, claimId, base.ClientId);
            //Retrieve the complete Bank Account List and store it in the ArrayList
            arrAcctList = m_objFundMngr.GetAccounts(this.objData.Context.InternalSettings.SysSettings.UseFundsSubAcc);
         //   m_objFundManager = null;

            //Create the necessary SysExData to be used in ref binding
            objOld = objXML.SelectSingleNode("//AccountList");
            objNew = objXML.CreateElement("AccountList");

            XmlElement xmlOption = null;
            XmlAttribute xmlOptionAttrib = null;

            if (this.objData.Context.InternalSettings.SysSettings.FundsAccountLink != true) //MITS 22227 - mcapps2 - 09/08/2010
            {
                // Blank value for the combo box
                xmlOption = objXML.CreateElement("option");
                xmlOptionAttrib = objXML.CreateAttribute("value");
                xmlOption.Attributes.Append(xmlOptionAttrib);
                objNew.AppendChild(xmlOption);
                xmlOptionAttrib = null;
                xmlOption = null;
            }
            //Loop through and create all the option values for the combobox control
            foreach (FundManager.AccountDetail item in arrAcctList)
            {
                xmlOption = objXML.CreateElement("option");
                objCData = objXML.CreateCDataSection(item.AccountName);
                xmlOption.AppendChild(objCData);
                xmlOptionAttrib = objXML.CreateAttribute("value");

                if (this.objData.Context.InternalSettings.SysSettings.UseFundsSubAcc)
                {
                    xmlOptionAttrib.Value = item.SubRowId.ToString();
                }
                else
                {
                    xmlOptionAttrib.Value = item.AccountId.ToString();
                }
                xmlOption.Attributes.Append(xmlOptionAttrib);
                objNew.AppendChild(xmlOption);
                if (this.objData.Context.InternalSettings.SysSettings.UseFundsSubAcc)
                {
                    if (iThisSubAccountId == item.SubRowId)
                    {
                        bThisAccountIdFound = true;
                    }
                }
                else
                {
                    if (iThisAccountId == item.AccountId)
                    {
                        bThisAccountIdFound = true;
                    }
                }
            }//end foreach
            if ((!bThisAccountIdFound) && (iThisAccountId != 0))
            {
                sAccountName = sGetAccountName(iThisAccountId, iThisSubAccountId, this.objData.Context.InternalSettings.SysSettings.UseFundsSubAcc);
                xmlOption = objXML.CreateElement("option");
                objCData = objXML.CreateCDataSection(sAccountName);
                xmlOption.AppendChild(objCData);
                xmlOptionAttrib = objXML.CreateAttribute("value");

                if (this.objData.Context.InternalSettings.SysSettings.UseFundsSubAcc)
                {
                    xmlOptionAttrib.Value = iThisSubAccountId.ToString();
                }
                else
                {
                    xmlOptionAttrib.Value = iThisAccountId.ToString();
                }
                xmlOption.Attributes.Append(xmlOptionAttrib);
                objNew.AppendChild(xmlOption);
            }
            //Add the XML to the SysExData 
            if (objOld != null)
                objXML.DocumentElement.ReplaceChild(objNew, objOld);
            else
                objXML.DocumentElement.AppendChild(objNew);


            objOld = objXML.SelectSingleNode("//ControlAppendAttributeList");
            objNew = objXML.CreateElement("ControlAppendAttributeList");
            XmlElement objElem = null;
            XmlElement objChild = null;

            objElem = objXML.CreateElement("bankaccount");
            objChild = objXML.CreateElement("ref");
            if (this.objData.Context.InternalSettings.SysSettings.UseFundsSubAcc)
                objChild.SetAttribute("value", "/Instance/UI/FormVariables/SysExData/CombinedPayment/SubAccId");
            else
                objChild.SetAttribute("value", "/Instance/UI/FormVariables/SysExData/CombinedPayment/AccountId");

         

            objElem.AppendChild(objChild); ;
            objNew.AppendChild(objElem);

            if (objOld != null)
                objXML.DocumentElement.ReplaceChild(objNew, objOld);
            else
                objXML.DocumentElement.AppendChild(objNew);
            ArrayList singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singleRow, "id", "lbl_bankaccount");
            if (this.objData.Context.InternalSettings.SysSettings.UseFundsSubAcc)
            {
                base.AddElementToList(ref singleRow, "Text", "Sub Bank Account");
            }
            else
            {
                base.AddElementToList(ref singleRow, "Text", "Bank Account");
            }
            base.m_ModifiedControls.Add(singleRow);

            //Clean up
            arrAcctList = null;
            objOld = null;
            objNew = null;
            objCData = null;
            objXML = null;
            //Parijat: Mits 9378
            m_objFundMngr.Dispose();
        }
        #endregion

        #endregion

        #region Common XML functions
        /*
		 * Vaibhav Kaushik 11 Feb 2006
		 * 
		 * Following functions are used to create the Xml document.
		 */
        private void StartDocument(ref XmlDocument objXmlDocument, ref XmlElement p_objRootNode, string p_sRootNodeName)
        {
            objXmlDocument = new XmlDocument();
            p_objRootNode = objXmlDocument.CreateElement(p_sRootNodeName);
            objXmlDocument.AppendChild(p_objRootNode);
        }

        private string sGetAccountName(int iAccountID, int iSubAccountId, bool bUseSubAccounts)
        {
            string sTemp = string.Empty;
            string sSQL = string.Empty;

            if (bUseSubAccounts == true)
            {
                sSQL = "SELECT SUB_ACC_NAME FROM BANK_ACC_SUB WHERE ACCOUNT_ID = " + iAccountID + " AND SUB_ROW_ID = " + iSubAccountId;
            }
            else
            {
                sSQL = "SELECT ACCOUNT_NAME FROM ACCOUNT WHERE ACCOUNT_ID = " + iAccountID;
            }
            using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
            {
                if (objReader != null && objReader.Read())
                {
                    if (bUseSubAccounts == true)
                    {
                        sTemp = objReader.GetString("SUB_ACC_NAME");
                    }
                    else
                    {
                        sTemp = objReader.GetString("ACCOUNT_NAME");
                    }
                }
            }
            return sTemp;
        }

        private void CreateElement(XmlElement p_objParentNode, string p_sNodeName)
        {
            XmlElement objChildNode = null;
            objChildNode = p_objParentNode.OwnerDocument.CreateElement(p_sNodeName);
            p_objParentNode.AppendChild(objChildNode);
            objChildNode = null;
        }


        private void CreateElement(XmlElement p_objParentNode, string p_sNodeName, ref XmlElement p_objChildNode)
        {
            p_objChildNode = p_objParentNode.OwnerDocument.CreateElement(p_sNodeName);
            p_objParentNode.AppendChild(p_objChildNode);
        }


        private void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText)
        {
            XmlElement objChildNode = null;
            this.CreateAndSetElement(p_objParentNode, p_sNodeName, p_sText, ref objChildNode);
        }


        private void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText, ref XmlElement p_objChildNode)
        {
            CreateElement(p_objParentNode, p_sNodeName, ref p_objChildNode);
            p_objChildNode.InnerText = p_sText;
        }

        private void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText, ref XmlElement p_objChildNode, string p_sAttValue, string p_AttName)
        {
            CreateElement(p_objParentNode, p_sNodeName, ref p_objChildNode);
            p_objChildNode.InnerText = p_sText;
            p_objChildNode.SetAttribute(p_AttName, p_sAttValue);
        }

        private void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText, string p_sAttValue, string p_AttName)
        {
            XmlElement objChildNode = null;
            this.CreateAndSetElement(p_objParentNode, p_sNodeName, p_sText, ref objChildNode, p_sAttValue, p_AttName);
        }
        #endregion

        public override void OnValidate(ref bool Cancel)
        {
            bool bError = false;

           if (CombinedPayment.IsNew)
           {
               string sSQL = " SELECT COMBINED_PAY_ROW_ID FROM COMBINED_PAYMENTS WHERE COMBINED_PAY_EID = " + CombinedPayment.EntityId;

                using (DbReader objreader = DbFactory.GetDbReader(CombinedPayment.Context.RMDatabase.ConnectionString, sSQL))
                {
                    if (objreader.Read())
                    {

                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                           Globalization.GetString("Validation.CombinedPayeeExists", base.ClientId), BusinessAdaptorErrorType.Error);
                        bError = true;

                    }
                }

            }


            Cancel = bError;

        }
    
    }
}
