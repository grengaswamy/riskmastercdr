﻿using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
    class LiabilityLossForm : DataEntryFormBase
    {
        const string CLASS_NAME = "ClaimXLiabilityLoss";
        const string FILTER_KEY_NAME = "ClaimId";
        private ClaimXLiabilityLoss objClaimXLiabilityLoss { get { return objData as ClaimXLiabilityLoss; } }
        private LocalCache objCache { get { return objData.Context.LocalCache; } }
        private string m_Caption = string.Empty;

        public LiabilityLossForm(FormDataAdaptor fda)
            : base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

        public override string GetCaption()
        {
            return base.GetCaption();
        }

        public override void InitNew()
        {
            base.InitNew();


            objClaimXLiabilityLoss.ClaimId = base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME, false);
            if (objClaimXLiabilityLoss.ClaimId > 0)
                (objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + objClaimXLiabilityLoss.ClaimId;


        }
        public override void OnUpdateForm()
        {
            ArrayList singleRow = null;
            string p_ShortCode = null;
            string p_sDesc = null;
            int iUseClaimProgressNotes = 0;
            base.OnUpdateForm();
            Claim objClaim = (Claim)objClaimXLiabilityLoss.Context.Factory.GetDataModelObject("Claim", false);
            objClaim.MoveTo(objClaimXLiabilityLoss.ClaimId);

            switch (objClaim.LineOfBusCode)
            {
                case 241:
                    m_SecurityId = RMO_GC_LIABILITYLOSS;
                    break;
                case 243:
                    m_SecurityId = RMO_WC_LIABILITYLOSS;
                    break;
             }

            if (SysEx.DocumentElement.SelectSingleNode("LINEOFBUSCODE") == null)
            {
                CreateSysExData("LINEOFBUSCODE");
                base.ResetSysExData("LINEOFBUSCODE", (objClaimXLiabilityLoss.Parent as Claim).LineOfBusCode.ToString());
            }

            //mgaba2:mits 28450
            iUseClaimProgressNotes = objClaim.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode].ClmProgressNotesFlag;
            if (iUseClaimProgressNotes == 0)
            {
                base.AddKillNode("enhancednotes");
            }

            m_Caption = this.GetCaption();

            base.ResetSysExData("SubTitle", m_Caption);
            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singleRow, "id", "formsubtitle");
            base.AddElementToList(ref singleRow, "Text", m_Caption);
            base.m_ModifiedControls.Add(singleRow);

            XmlElement objSysSid = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysSid");
            objSysSid.InnerText = m_SecurityId.ToString();

  
            //Create new Permission wasnt working
            if (objClaimXLiabilityLoss.RowId == 0)
            {
               
                if (!objClaimXLiabilityLoss.Context.RMUser.IsAllowedEx(m_SecurityId, RMO_CREATE))
                {
                    m_CurrentAction = enumFormActionType.None;
                    LogSecurityError(RMO_CREATE);
                    return;
                }
            }
            else
            {
               
                if (!objClaimXLiabilityLoss.Context.RMUser.IsAllowedEx(m_SecurityId, RMO_VIEW))
                {
                    m_CurrentAction = enumFormActionType.None;
                    LogSecurityError(RMO_VIEW);
                    return;
                }
               
            }

            if (!objClaimXLiabilityLoss.IsNew)
            {
                base.AddReadOnlyNode("liabilitytype");
            }

            if (objClaimXLiabilityLoss.LiabilityType == 0)
            {
                base.AddKillFieldMarkNode("el");
                base.AddKillFieldMarkNode("glpl");
                base.AddKillNode("lldamagetype");
            }
            else
            {
                objCache.GetCodeInfo(objClaimXLiabilityLoss.LiabilityType, ref p_ShortCode, ref p_sDesc,base.Adaptor.userLogin.objUser.NlsCode);  //Aman ML Change

                switch (p_ShortCode)
                {
                    case "DNO":
                    case "EL":
                    case "PRL":
                        {
                            base.AddKillFieldMarkNode("glpl");

                            base.AddDisplayNode("lldamagetype");
                            base.AddDisplayNode("elpracticearea");
                            base.AddDisplayNode("elempdate");
                            base.AddDisplayNode("elplaintiffclaimantype");
                            base.AddDisplayNode("elyrposition");
                            base.AddDisplayNode("elcoverageformtype");

                            base.AddDisplayNode("elemptitle");
                            base.AddDisplayNode("elbgcheck");

                            base.AddDisplayNode("elterminationdate");
                            base.AddDisplayNode("elbusinessscope");
                            base.AddDisplayNode("elempliability");
                        }
                        break;
                    case "GL":
                    case "PL":
                        {
                            base.AddKillFieldMarkNode("el");

                            base.AddDisplayNode("glplgeneralliability");

                            base.AddDisplayNode("lldamagetype");

                            base.AddDisplayNode("glplallegedharm");
                            base.AddDisplayNode("glplbrandname");
                            base.AddDisplayNode("glplgenericname");
                            base.AddDisplayNode("glplproductseen");

                            base.AddDisplayNode("glplproductother");


                            base.AddDisplayNode("glplinjurytype");
                            base.AddDisplayNode("glplpremisetype");
                            base.AddDisplayNode("glplpremiseother");
                            base.AddDisplayNode("glplproductliability");

                            base.AddDisplayNode("glplproducttype");
                            base.AddDisplayNode("glplproductind");

                            base.AddDisplayNode("glplincidentdate");
                            base.AddDisplayNode("glplmanufacturer");

                        }
                        break;

                }
            }
          
            base.ResetSysExData("SubTitle", m_Caption);

            if (objClaimXLiabilityLoss.AddedByUser == "LSSINF")
            {
              
                base.ResetSysExData("FormReadOnly", "Disable");
            }
            else
                base.ResetSysExData("FormReadOnly", "Enable");


        }
        public override void OnUpdateObject()
        {
            base.OnUpdateObject();

        }

      
      
    }
}

