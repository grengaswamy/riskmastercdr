using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;


namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for CmXCmgrHistForm.
	/// </summary>
	public class CmXCmgrHistForm : DataEntryFormBase
	{
		const string CLASS_NAME = "CmXCmgrHist";
		private CmXCmgrHist CmXCmgrHist{get{return objData as CmXCmgrHist;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		const string FILTER_KEY_NAME = "CmcmhRowId";

		public override void InitNew()
		{
			base.InitNew(); 
			this.CmXCmgrHist.CasemgtRowId = base.GetSysExDataNodeInt("/SysExData/CasemgtRowId"); 
		}

		public CmXCmgrHistForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

       
		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
            bool bSuccess;
            int iClaimId = 0;

			base.OnUpdateForm ();

            //MITS 28450:In case case manager is opened from screens other than claim .for ex:Injury
            iClaimId = Conversion.CastToType<Int32>(CmXCmgrHist.Context.DbConn.ExecuteString("SELECT CLAIM_ID FROM CASE_MANAGEMENT WHERE CASEMGT_ROW_ID = " + this.CmXCmgrHist.CasemgtRowId), out bSuccess);
            if (iClaimId != 0)
            {
                //MGaba2:MITS 29116
                Claim objClaim = (Claim)CmXCmgrHist.Context.Factory.GetDataModelObject("Claim", false);
                objClaim.MoveTo(iClaimId);
                int iUseClaimProgressNotes = CmXCmgrHist.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode].ClmProgressNotesFlag;
                if (iUseClaimProgressNotes == 0)
                {
                    base.AddKillNode("enhancednotes");
                }
            }

			int iCaseMgrDays=0;
			TimeSpan objDateDiff;
			if(CmXCmgrHist.DateClosed=="" || CmXCmgrHist.DateClosed==null)
			{
				if(CmXCmgrHist.RefDate=="" || CmXCmgrHist.RefDate==null)
					iCaseMgrDays=0;
				else
				{
					objDateDiff=System.DateTime.Today.Subtract(Conversion.ToDate(CmXCmgrHist.RefDate));
					iCaseMgrDays=objDateDiff.Days;
				}
			}
			else
			{
				if(CmXCmgrHist.RefDate=="" || CmXCmgrHist.RefDate==null)
					iCaseMgrDays=0;
				else
				{
					objDateDiff=Conversion.ToDate(CmXCmgrHist.DateClosed).Subtract(Conversion.ToDate(CmXCmgrHist.RefDate));
					iCaseMgrDays=objDateDiff.Days;
				}
			}
			if(iCaseMgrDays<0)
				base.ResetSysExData("CaseMgrDays","");
			else
				base.ResetSysExData("CaseMgrDays",iCaseMgrDays.ToString());
           
            //aaggarwal29 : Case Management Permissions not working properly; MITS 28485 start
            int iCasemgtRowId = this.CmXCmgrHist.CasemgtRowId;
            CaseManagement oCaseManagement;
            Claim oClaim;
            
            using (oCaseManagement = (CaseManagement)CmXCmgrHist.Context.Factory.GetDataModelObject("CaseManagement", false))
            {
                oCaseManagement.MoveTo(iCasemgtRowId);
                oClaim = (Claim)CmXCmgrHist.Context.Factory.GetDataModelObject("Claim", false);
                oClaim.MoveTo(oCaseManagement.ClaimId);
            }
            switch (oClaim.LineOfBusCode)
            {
                case 243:
                    m_SecurityId = RMO_WC_CASE_MANAGEMENT;
                    break;
                case 844:
                    m_SecurityId = RMO_DI_CASE_MANAGEMENT;
                    break;
            }
            oClaim.Dispose();

            //Updating Security Id in XML file
            XmlElement objSysSid = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysSid");
            objSysSid.InnerText = m_SecurityId.ToString();
            if (CmXCmgrHist.CmcmhRowId == 0)
            {
                if (!CmXCmgrHist.Context.RMUser.IsAllowedEx(m_SecurityId, RMO_CREATE))
                {
                    LogSecurityError(RMO_CREATE);
                    return;
                }
            }
            else
            {
                if (!CmXCmgrHist.Context.RMUser.IsAllowedEx(m_SecurityId, RMO_VIEW))
                {
                    LogSecurityError(RMO_VIEW);
                    return;
                }
            }
            //aaggarwal29 : MITS 28485 end
		}

	}

}
