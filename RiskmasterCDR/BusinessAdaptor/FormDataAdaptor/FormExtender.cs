using System;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// FormExtender is the limited programmatic surface we want to provide 
	/// for custom scripting during Screen Rendering.
	/// 
	/// It contains at a minimum the DM object if there is one 
	/// (not present for List screens)
	/// and the set of operations we want to allow the scripting layer to 
	/// perform against the screen before it is shown to the user.
	///		a.) Remove\Add a control.
	///		b.) Remove\Add a control attribute.
	///		c.) Disable\Enable a control.
	///		More TBD...
	/// </summary>
	///
	public class FormExtender
	{
		//Wrap an internal FormBase object
		private FormBase m_FormBase = null;
		private DataEntryFormBase m_pFormDataEntry = null;

		//Expose flag for checking if DM object is available...
		public bool HasDataObject
		{
			get
			{
				if(m_pFormDataEntry==null)
					return false;
                return (m_pFormDataEntry.m_objData as DataModel.DataObject)!=null;
			}
		}
		//Expose DM object if available.
		public Riskmaster.DataModel.DataObject DataObject
		{
			get
			{
				if(m_pFormDataEntry==null)
					return null;

				return m_pFormDataEntry.m_objData as DataModel.DataObject;
			}
		}
		

		//Instantiate our Wrapper
		internal FormExtender(FormBase formBase)
		{
			m_FormBase = formBase;
			m_pFormDataEntry = formBase as DataEntryFormBase;
		}

		/// Question: How do we Identify Controls?
		/// 
		///  TODO:
		///		a.) Remove\Add a control.
		///		b.) Remove\Add a control attribute.
		///		c.) Disable\Enable a control.
		///		
	}
}
