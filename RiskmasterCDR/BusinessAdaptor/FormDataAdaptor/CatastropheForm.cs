﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Db;

namespace Riskmaster.BusinessAdaptor
{
    /// <summary>
    /// Catastrophe Screen.
    /// </summary>
    public class CatastropheForm : DataEntryFormBase
    {
        const string CLASS_NAME = "Catastrophe";
        private Catastrophe objCatastrophe { get { return objData as Catastrophe; } }
        public CatastropheForm(FormDataAdaptor fda)
            : base(fda)
		{
            base.m_ClassName = CLASS_NAME;
        }

        public override void OnUpdateForm()
        {
            base.OnUpdateForm();

            //if ((objCatastrophe.CatastropheRowId>0)  && !Adaptor.userLogin.IsAllowedEx(m_SecurityId, FormBase.RMO_VIEW))
            //{               
            //    throw new RMAppException(Globalization.GetString("Permission.NoAccess"));                                       
            //}
        }
        public override void OnValidate(ref bool Cancel)
        {
            bool bError = false;
            if (!string.IsNullOrEmpty(objCatastrophe.LossStartDate) && !string.IsNullOrEmpty(objCatastrophe.LossEndDate))
            {
                if (objCatastrophe.LossEndDate.CompareTo(objCatastrophe.LossStartDate) < 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThanStartDate",base.ClientId), Globalization.GetString("Field.LossEndDate",base.ClientId), Conversion.ToDate(objCatastrophe.LossStartDate).ToShortDateString()),
                        BusinessAdaptorErrorType.Error);

                    bError = true;
                }
            }           
            if (!Adaptor.userLogin.IsAllowedEx(m_SecurityId, FormBase.RMO_CREATE) && (objCatastrophe.IsNew))
            {
                Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                    Globalization.GetString("Permission.NoCreate",base.ClientId), BusinessAdaptorErrorType.Error);
                bError = true;
                return;
            }
        
            if (!Adaptor.userLogin.IsAllowedEx(m_SecurityId, FormBase.RMO_UPDATE) && !(objCatastrophe.IsNew))
            {
                Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                    Globalization.GetString("Permission.NoUpdate",base.ClientId), BusinessAdaptorErrorType.Error);

                bError = true;
                return;
            }
            Cancel = bError;

        }

        //Amandeep MITS 28799 --start
        public override void BeforeSave(ref bool Cancel)
        {           
            if (objCatastrophe.CatNumber.Trim() != string.Empty)
            {
                string sSQL = string.Empty;
                int iCount = 0;
                Dictionary<string, string> dictParms = new Dictionary<string, string>();
                sSQL = string.Format("SELECT COUNT(*) FROM CATASTROPHE WHERE CAT_NUMBER = {0} AND CATASTROPHE_ROW_ID <> {1}", "~CATNUMBER~", "~CATASTROPHEROWID~");
                dictParms.Add("CATNUMBER", objCatastrophe.CatNumber);
                dictParms.Add("CATASTROPHEROWID", objCatastrophe.CatastropheRowId.ToString());
               
                iCount = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(objCatastrophe.Context.DbConn.ConnectionString, sSQL, dictParms), base.ClientId);
                if (iCount > 0)
                {
                    Errors.Add(Globalization.GetString("SaveError",base.ClientId),
                    String.Format(Globalization.GetString("Save.DuplicateCatastropheNumberWarning",base.ClientId)),
                        BusinessAdaptorErrorType.Error);
                    Cancel = true;                       
                    return;
                }
               
            }
        }
        //Amandeep MITS 28799 --end
    }
}
