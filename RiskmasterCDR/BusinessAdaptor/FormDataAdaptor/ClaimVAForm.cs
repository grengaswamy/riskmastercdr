﻿using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using Riskmaster.Settings;
using Riskmaster.Security.RMApp;
using System.Data;
using System.Text;
using Riskmaster.BusinessAdaptor.AutoFroiAcord;
using System.Collections.Generic;
using Riskmaster.Application.ImagRightWrapper;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for ClaimVAScreen.
	/// </summary>
	public class ClaimVAForm : DataEntryFormBase
	{
		const string CLASS_NAME = "Claim";

		private Claim objClaim{get{return objData as Claim;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		private int m_LOB;
		private LobSettings objLobSettings = null;
        private SysSettings objSysSettings = null;
		bool bCloseDiaries = false;
        //Changed by Gagan for MITS 11451 : Start
        bool bDeleteAutoChecks = false;
        //Changed by Gagan for MITS 11451 : End
		string sUserCloseDiary = "false";
		string m_ClaimLimitFilter = "";
        int iClaimId = 0;
        //Mridul 05/28/09 MITS 16745-Chubb ACK Letter Enhancement
        bool bTriggerLetter = false;
        UserLoginLimits objLimits = null;
        private ArrayList arrPoliciesDeleted = new ArrayList();
        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="fda"></param>
        public ClaimVAForm(FormDataAdaptor fda)
            : base(fda)
        {
            base.m_ClassName = CLASS_NAME;
            m_LOB = fda.Factory.Context.LocalCache.GetCodeId("VA", "LINE_OF_BUSINESS");
            base.m_PreInitHandler = new PreInitHandler(this.CustomPreInitHandler);
            objLimits = base.GetUserLoginLimits();
        }

		public override void InitNew()
		{
			base.InitNew();
			objClaim.LineOfBusCode = m_LOB;
			objClaim.NavType = Riskmaster.DataModel.ClaimNavType.ClaimNavLOB;
			if(m_ParentId >0)
				(objData as INavigation).Filter = "EVENT_ID=" + m_ParentId;
			if(m_ClaimLimitFilter !="")
            {
                if (m_ParentId > 0) //Geeta 09/19/07 : Modified for MITS 10377
                {
                    (objClaim as DataModel.INavigation).Filter += " AND " + m_ClaimLimitFilter;
                }
                else
                {
                    (objClaim as DataModel.INavigation).Filter += m_ClaimLimitFilter;
                }
            }
			
			objClaim.EventId = m_ParentId;
            objSysSettings = objClaim.Context.InternalSettings.SysSettings;
		}

        /// <summary>
        /// Author: Sumit Kumar
        /// Date: 09/23/2010
        /// MITS: 21919
        /// </summary>
        /// <returns></returns>
        public override bool AddNew()
        {
            try
            {
                bool bIsClaimVAEnabled = objClaim.Context.InternalSettings.SysSettings.UseVALOB;
                if (bIsClaimVAEnabled)
                {
                    base.AddNew();
                }
                else
                {
                    //base.LogSecurityError(FormBase.RMO_ACCESS); - May use this if decided in future.
                    Errors.Add(Globalization.GetString("PermissionFailure",base.ClientId), Globalization.GetString("Claim.AccessPermission.error",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 804
                    return false;
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Claim.GetUtilitySetting.error", base.ClientId), p_objException);//sharishkumar Jira 804
            }
            return true;
        }

        public override void BeforeDelete(ref bool Cancel)  // Added by csingh7 R6 Claim Comment Enhancement
        {
            iClaimId = objClaim.ClaimId;
        }
        //Mukul Added 2/2/07 MITS 8760
        public override void AfterDelete()
        {
            base.AfterDelete();
            if (iClaimId != 0)
            {
                objClaim.Context.DbConnLookup.ExecuteNonQuery(
                   String.Format(@"DELETE FROM COMMENTS_TEXT 
                WHERE ATTACH_RECORDID = {0} AND ATTACH_TABLE = 'CLAIM'", iClaimId));
            }
            base.ResetSysExData("EventId", "0");
            objClaim.EventId = 0;
        }

		
		//BSB 10.10.2006 If present, this handler will be called by DM during object construction 
		// just prior to invoking any client initialization scripts.
		public void CustomPreInitHandler(object objSrc,Riskmaster.DataModel.InitObjEventArgs e)
		{
			Claim objClaim = (objSrc as Claim);
			objClaim.LineOfBusCode=m_LOB;
			this.m_ParentId = base.GetSysExDataNodeInt("/SysExData/EventId");
			objClaim.EventId = m_ParentId;
			//Log.Write("CustomPreInitHandler Called");
			// Now DM will go off and run customer coded Init Scripts with the same information
			// that RMWorld used to provide.
		}
			// BSB 11.09.2005 Work Around for ClaimId jumping to -1 when this object creates a new parent and 
		// is added to it's claimlist.  We would like client side javascript to be able to assume
		// that only "ClaimId==0" means new record.
		public override void AfterAddNew()
		{
			base.AfterAddNew ();
			objClaim.ClaimId=0;
		}

		public override void Init()
		{
			m_LOB = m_fda.Factory.Context.LocalCache.GetCodeId("VA", "LINE_OF_BUSINESS");
			objLobSettings = m_fda.Factory.Context.InternalSettings.ColLobSettings[m_LOB];
			this.m_ParentId = base.GetSysExDataNodeInt("/SysExData/EventId");

			//Filter Innaccessible Claim types based on UserLimit Security.
			if(objLobSettings.ClmAccLmtFlag)//If UserLimits Enabled...
			{
				if(objLimits != null)
				{
					string[,] arr = objLimits.ClaimStatusTypeAccessList(m_LOB);

					if(arr.Length!=0)
					{
						m_ClaimLimitFilter = "";
						string sTypeFilter = "";
						string sTypeFilter1 = "";
						string sTypeFilter2 = "";

						for(int i=0; i<Convert.ToInt32(arr.Length/2); i++)
						{
							if(arr[i,0]!="0" && arr[i,1]!="0")
							{
								if(i>0 && sTypeFilter!="")
									sTypeFilter += " AND ";
								sTypeFilter += "NOT(CLAIM_TYPE_CODE =" + arr[i,0];
								sTypeFilter += " AND ";
								sTypeFilter += "CLAIM_STATUS_CODE =" + arr[i,1];
								sTypeFilter += ")";
							}
							else if(arr[i,0]!="0" || arr[i,1]!="0")
							{
								if(arr[i,0]!="0")
								{									
									if(i>0 && sTypeFilter1!="")
										sTypeFilter1 += ",";
									sTypeFilter1 += arr[i,0];
								}
								else
								{									
									if(i>0 && sTypeFilter2!="")
										sTypeFilter2 += ",";
									sTypeFilter2 += arr[i,1];
								}
							}
						}
						if(sTypeFilter!="")
							m_ClaimLimitFilter += sTypeFilter;

						if(sTypeFilter1!="")
						{	
							if(m_ClaimLimitFilter!="")
								m_ClaimLimitFilter += " AND " + "CLAIM_TYPE_CODE NOT IN (" + sTypeFilter1 + ")";
							else
								m_ClaimLimitFilter += "CLAIM_TYPE_CODE NOT IN (" + sTypeFilter1 + ")";
						}
						if(sTypeFilter2!="")
						{	
							if(m_ClaimLimitFilter!="")
								m_ClaimLimitFilter += " AND " + "CLAIM_STATUS_CODE NOT IN (" + sTypeFilter2 + ")";
							else
								m_ClaimLimitFilter += "CLAIM_STATUS_CODE NOT IN (" + sTypeFilter2 + ")";
						}
					}
				}
			}
			InitNew();

		}

		private void ApplyFormTitle()
		{
            string sDiaryMessage = "";
			string sCaption="";
			int captionLevel=0;
			string sTmp="";

			Event objEvent = (objClaim.Parent as Event);
			//Handle User Specific Caption Level
			if(!objClaim.IsNew || objClaim.PrimaryClaimant.ClaimantRowId!=0)
			{
				
				if(objLobSettings.CaptionLevel!=0)
					captionLevel = objLobSettings.CaptionLevel;
				else
					captionLevel = 1006;
			//nadim for 15014
				if(captionLevel <1005 || captionLevel >1012)
					captionLevel = 0;

				if(captionLevel !=1012)
					sCaption = objClaim.Context.InternalSettings.CacheFunctions.GetOrgParent(objEvent.DeptEid,captionLevel,0,ref captionLevel);//captionLevel discarded
				else
				{
					objClaim.Context.InternalSettings.CacheFunctions.GetOrgInfo(objEvent.DeptEid,ref sCaption,ref sTmp);
                    sCaption += " - " + sTmp;
				}
				sCaption = sCaption.Substring(0, Math.Min(sCaption.Length, 25));
			
				if(objClaim.EventId!=0 && objClaim.PrimaryClaimant!=null)
                {
                    sCaption = " [" + objClaim.ClaimNumber + " * " + sCaption + " * " +
                        objClaim.PrimaryClaimant.ClaimantEntity.GetLastFirstName() + "]";
                    sDiaryMessage = objClaim.PrimaryClaimant.ClaimantEntity.GetLastFirstName();
                }
                else
                {
                    sCaption = " [" + objClaim.ClaimNumber + " * " + sCaption + " * - ]";
                    sDiaryMessage = "";
                }
			}
			if(objClaim.IsNew)
				sCaption="";
			//Pass this subtitle value to view (ref'ed from @valuepath).
			base.ResetSysExData("SubTitle",sCaption);
            ArrayList singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singleRow, "id", "formsubtitle");
            base.AddElementToList(ref singleRow, "Text", sCaption);
            base.m_ModifiedControls.Add(singleRow);
            base.ResetSysExData("DiaryMessage", sDiaryMessage);
		}


		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
			base.OnUpdateForm ();

            XmlDocument objXmlCustom = null;                  // Umesh
            XmlDocument objXML = base.SysView;                  //Umesh
            // 03/01/2007 REM  to customize the page flow : Umesh
            base.CreateSysExData("CheckPolicyValidation", "");
            //For new claim, InitCClaim custom script could set value for Comments.
            //If LegacyComments is true, we need to bring value back
            XmlDocument objSysExData = base.SysEx;
            objClaim.GetCommentsXml(ref objSysExData, objClaim.ClaimId);

            objXmlCustom = new XmlDocument();
           // XmlElement objXmlElement = null;
            ArrayList singleRow = null;
            //Mridul 05/28/09 MITS 16745 - Pass value of Template for Claim Letter 
            base.CreateSysExData("ClaimLetterTmplId", "0");
            //using (DbReader objRdr = RMSessionManager.GetCustomizedContent("customize_custom"))//R5 PS2 Merge
            //{//R5 PS2 Merge
            //    if (objRdr.Read())//R5 PS2 Merge
            string strContent = RMSessionManager.GetCustomContent(base.ClientId);//R5 PS2 Merge
            if (!string.IsNullOrEmpty(strContent))//R5 PS2 Merge
            {
                    //objXmlCustom.LoadXml(objRdr.GetString("CONTENT"));//R5 PS2 Merge
                    objXmlCustom.LoadXml(strContent);//R5 PS2 Merge
                    // objRdr.Dispose();
                    //Added by Shivendu to do a null check
                    if (objXmlCustom.SelectSingleNode("//SpecialSettings/Show_AdjusterList") != null)
                    if (objXmlCustom.SelectSingleNode("//SpecialSettings/Show_AdjusterList").InnerText == "-1")
                    {
                        //Mona
                        //objXmlElement = objXML.SelectSingleNode("//button[@name='btnAdjuster']") as XmlElement;
                        //if (objXmlElement != null)
                        //    objXmlElement.Attributes["param"].Value = "SysFormPIdName=claimid&SysFormName=claimadjusterlist&SysCmd=1&SysViewType=controlsonly&SysFormIdName=adjrowid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber";
                        //objXmlElement = null;

                        singleRow = new ArrayList();
                        base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.FormButton.ToString());
                        base.AddElementToList(ref singleRow, "id", "btnAdjusters");
                        base.AddElementToList(ref singleRow, "param", "SysFormPIdName=claimid&SysFormName=claimadjusterlist&SysCmd=1&SysViewType=controlsonly&SysFormIdName=adjrowid&SysEx=ClaimId|ClaimNumber&SysExMapCtl=ClaimId|ClaimNumber");
                        base.m_ModifiedControls.Add(singleRow);
                    }
                //}
            }
            // 03/01/2007 REM End

            // Naresh Code Changed for Saving the Enhanced Policy Ids attached with the Claim
            //objXmlElement = (XmlElement)base.SysView.SelectSingleNode("//control[@name='primarypolicyid']");
            //if (objXmlElement != null)
            //{
                //Start:Nitin Goel,Check for specific LOB. 01/13/10, MITS#18229
                //if (objClaim.Context.InternalSettings.SysSettings.UseEnhPolFlag == -1)
                  if (objLobSettings.UseEnhPolFlag == -1)
                {
                    //objXmlElement.SetAttribute("idref", "/Instance/Claim/PrimaryPolicyIdEnh");
                    //// Start Naresh MITS 9996 Set the Ref for Policy Management
                    //objXmlElement.SetAttribute("ref", "/Instance/Claim/PrimaryPolicyIdEnh/@defaultdetail");
                    //objXmlElement.SetAttribute("param", "SysFormPIdName=claimid&SysFormName=policyenh&SysCmd=0&SysFormIdName=policyid&SysFormId=%primarypolicyid_cid%&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber");

                    singleRow = new ArrayList();
                    base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.PolicyLookup.ToString());
                    base.AddElementToList(ref singleRow, "id", "primarypolicyid");
                    base.AddElementToList(ref singleRow, "idref", "/Instance/Claim/PrimaryPolicyIdEnh");
                    base.AddElementToList(ref singleRow, "ref", "/Instance/Claim/PrimaryPolicyIdEnh/@defaultdetail");
                    //MGaba2:MITS 14169:Setting onclientclick attribute in .aspx.cs..So no need of this param attribute
                    // base.AddElementToList(ref singleRow, "param", "SysFormPIdName=claimid&SysFormName=policyenh&SysCmd=0&SysFormIdName=policyid&SysFormId=%primarypolicyid_cid%&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber");
                    base.m_ModifiedControls.Add(singleRow);
                    //MGaba2:MITS 14169: Need for opening Enhanced Policy/Policy Screen through MDI when Open is clicked
                    base.ResetSysExData("UseEnhPolFlag", "-1");
                    base.ResetSysExData("UseAdvancedClaim", "0");
                }
                else
                {
                    if (objSysSettings.MultiCovgPerClm == -1)
                    {
                        singleRow = new ArrayList();
                        base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.PolicyLookup.ToString());
                        base.AddElementToList(ref singleRow, "id", "primarypolicyid");
                        base.AddElementToList(ref singleRow, "idref", "/Instance/Claim/PrimaryPolicyId");
                        base.AddElementToList(ref singleRow, "ref", "/Instance/UI/FormVariables/SysExData/ClaimPolicyList");
                        base.m_ModifiedControls.Add(singleRow);
                        base.ResetSysExData("UseAdvancedClaim", "-1");
                    }
                    //objXmlElement.SetAttribute("idref", "/Instance/Claim/PrimaryPolicyId");
                    //// Start Naresh MITS 9996 Set the Ref for Policy Tracking
                    //objXmlElement.SetAttribute("ref", "/Instance/Claim/PrimaryPolicyId/@defaultdetail");
                    //objXmlElement.SetAttribute("param", "SysFormPIdName=claimid&SysFormName=policy&SysCmd=0&SysFormIdName=policyid&SysFormId=%primarypolicyid_cid%&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber");
                    else
                    {
                        singleRow = new ArrayList();
                        base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.PolicyLookup.ToString());
                        base.AddElementToList(ref singleRow, "id", "primarypolicyid");
                        base.AddElementToList(ref singleRow, "idref", "/Instance/Claim/PrimaryPolicyId");
                        base.AddElementToList(ref singleRow, "ref", "/Instance/Claim/PrimaryPolicyId/@defaultdetail");
                        //MGaba2:MITS 14169:Setting onclientclick attribute in .aspx.cs..So no need of this param attribute
                        // base.AddElementToList(ref singleRow, "param", "SysFormPIdName=claimid&SysFormName=policyenh&SysCmd=0&SysFormIdName=policyid&SysFormId=%primarypolicyid_cid%&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber");
                        base.m_ModifiedControls.Add(singleRow);
                        //MGaba2:MITS 14169: Need for opening Enhanced Policy/Policy Screen through MDI when Open is clicked
                        base.ResetSysExData("UseEnhPolFlag", "0");
                        base.ResetSysExData("UseAdvancedClaim", "0");
                    }
                }
            //}
                //MITS 11779 START code added for making time fields mandatory for General claim form.
                if (this.CurrentAction == enumFormActionType.AddNew)
                {
                    if (objClaim.Context.InternalSettings.SysSettings.DefaultTimeFlag == -1)
                    {
                        XmlNode xnode = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysNotReqNew");
                        if (xnode != null)
                        {
                            xnode.InnerText = xnode.InnerText + "|timerequired";
                        }

                    }
                }
                //MITS 11779 END

            objXML = null;
            objXML = base.SysEx;
			
			if(this.CurrentAction == enumFormActionType.AddNew)
			{
				//We're adding a new claim - check to see if we are supposed to put it 
				//under an existing event...
				XmlNode objEventNode = this.SysEx.SelectSingleNode("/SysExData/EventId");
				if(objEventNode!=null && objClaim.EventId==0)
				{
					objClaim.EventId = Conversion.ConvertStrToInteger(objEventNode.InnerText);
				}
				base.ResetSysExData("NewClaim","1");
			}

			//Umesh MITS_8093
			else
				base.ResetSysExData("NewClaim","0");
			//	Umesh MITS_8093

			base.ResetSysExData("AllCodes",m_fda.Factory.Context.LocalCache.GetAllParentCodesForClosedClaims(242,m_fda.Factory.Context.RMDatabase.DataSourceId));
			//add the Current Adjuster Name Node
            //R7 PRF IMP: No need to create a new object
			//ClaimAdjuster objAdj = objClaim.CurrentAdjuster;
            if (objClaim.CurrentAdjuster != null)
			{
                if (objClaim.CurrentAdjuster.CurrentAdjFlag)
                    base.ResetSysExData("CurrentAdjuster", objClaim.CurrentAdjuster.AdjusterEntity.Default);
				else
					base.ResetSysExData("CurrentAdjuster","");
			}
			else
				base.ResetSysExData("CurrentAdjuster","");
			
			Event objEvent=objClaim.Parent as Event;

			//*********************
			//Place Full Parent Event Information into SysExData for XML Binding
			//*********************
			XmlNode objOld = objXML.SelectSingleNode("//Parent");
			XmlElement objNew = objXML.CreateElement("Parent");
			objNew.InnerXml = objEvent.SerializeObject();

			if(objOld !=null)
				objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);

			string sNewClaim=base.GetSysExDataNodeText("/SysExData/NewClaim");

			//Check UserLimit Security.
			if(objClaim.Context.InternalSettings.ColLobSettings[m_LOB].ClmAccLmtFlag)//If UserLimits Enabled...
			{
				if(objLimits != null)
                    //Asharma326 MITS 30874 Add Parameter objClaim.Context.InternalSettings.ColLobSettings[m_LOB].ClmAccLmtUsrFlag
                    if ((!objClaim.IsNew && !objLimits.ClaimAccessAllowed(objClaim.LineOfBusCode, objClaim.ClaimTypeCode, objClaim.ClaimStatusCode, objClaim.Context.InternalSettings.ColLobSettings[m_LOB].ClmAccLmtUsrFlag) && sNewClaim != "1"))
					{
						//Umesh MITS_8093
						int iClaim_Id;
						int iDirection=(int)this.CurrentAction;
                        //gagnihotri MITS 18851
                        if (sNewClaim == "0" && iDirection != 9 && iDirection != 6)
						{ 
							switch(iDirection.ToString())
							{
									
								case "1" : 
									MoveNext();
									break;

								case "2" :
									MovePrevious();
									break;

								case "3" :
									iClaim_Id=objClaim.Context.DbConnLookup.ExecuteInt("SELECT MAX(CLAIM_ID) FROM CLAIM WHERE (LINE_OF_BUS_CODE=" + objClaim.LineOfBusCode + ")" );
									if(objClaim.ClaimId==iClaim_Id)
										MovePrevious();
									else
										MoveNext();
									break;

								case "4" :
									iClaim_Id=objClaim.Context.DbConnLookup.ExecuteInt("SELECT MIN(CLAIM_ID) FROM CLAIM WHERE (LINE_OF_BUS_CODE=" + objClaim.LineOfBusCode + ")" );
									if(objClaim.ClaimId==iClaim_Id)
										MoveNext();
									else
										MovePrevious();
									break;
								
								
							}
						}
						//Umesh MITS_8093

						else
						{
							base.ResetSysExData("NewClaim","0");
							Errors.Add(Globalization.GetString("LimitError",base.ClientId),
                                String.Format(Globalization.GetString("Access.Claim.UserLimitFailed",base.ClientId), objCache.GetCodeDesc(objClaim.ClaimTypeCode, base.Adaptor.userLogin.objUser.NlsCode), objCache.GetCodeDesc(objClaim.ClaimStatusCode, base.Adaptor.userLogin.objUser.NlsCode)),
                                BusinessAdaptorErrorType.SystemError); //Aman ML Change//sharishkumar Jira 804
							return;
						}
					}
			}

			//Handle Locked down Toolbar buttons.
			if(!objClaim.Context.RMUser.IsAllowedEx(FormBase.RMO_SEARCH,FormBase.RMO_CLAIM_SEARCH))
				base.AddKillNode("search");
			if(!objClaim.Context.RMUser.IsAllowedEx(FormBase.RMO_LOOKUP,FormBase.RMO_CLAIM_SEARCH))
				base.AddKillNode("lookup");

			//Raman Bhatia .. As per email from Mike, Making changes in Progress Notes Design
			//Kill Comments or Enhanced Notes node depending on SYS_PARMS_LOB settings

			try
			{
                //rsushilaggar: Get the flag from the LobSetings Objects
                //int iUseClaimProgressNotes = objClaim.Context.DbConnLookup.ExecuteInt(
                //    "SELECT USE_CLAIM_PROGRESS_NOTES FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE = " + objClaim.LineOfBusCode.ToString());
                int iUseClaimProgressNotes = objClaim.Context.InternalSettings.ColLobSettings[m_LOB].ClmProgressNotesFlag;
                if (iUseClaimProgressNotes == 0)
				{
					base.AddKillNode("enhancednotes");
				}

                //rsushilaggar: Get the flag from the LobSetings Objects
                //int iUseClaimComments = objClaim.Context.DbConnLookup.ExecuteInt(
                //    "SELECT USE_CLAIM_COMMENTS FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE = " + objClaim.LineOfBusCode.ToString());
                int iUseClaimComments = objClaim.Context.InternalSettings.ColLobSettings[m_LOB].ClmCommentsFlag;

				if(iUseClaimComments == 0)
				{
					base.AddKillNode("comments");
				}
                //rsushilaggar: ISO Claim Search : Start
                int iUseISOSubmission = objClaim.Context.InternalSettings.ColLobSettings[m_LOB].ClmISOSubmissionFlag;
                if (iUseISOSubmission == 0 || iUseISOSubmission.ToString() == null)
                {
                    base.AddKillNode("iso");
                }
                //rsushilaggar: ISO Claim Search : End	
                if (!objClaim.Context.InternalSettings.SysSettings.ClaimActivityLog)
                {
                    base.AddKillNode("claimActLog");
                }
                else
                {
                    base.AddDisplayNode("claimActLog");
                }				 
			}
			catch
			{				
			}
			//Nitesh MITS 7656 Starts
			//Event Number Locking Logic
//			if(objClaim.IsNew && (objClaim.Parent as Event).EventId ==0 && !objClaim.Context.RMUser.IsAllowedEx(this.SecurityId,RMO_ALLOW_ENTRY_EVENT_NUM))
//				base.AddReadOnlyNode("ev_eventnumber");
//			
//			if(objClaim.IsNew && (objClaim.Parent as Event).EventId !=0 && !objClaim.Context.RMUser.IsAllowedEx(this.SecurityId,RMO_ALLOW_EDIT_EVENT_NUM))
//				base.AddReadOnlyNode("ev_eventnumber");
//
//			if(!objClaim.IsNew && !objClaim.Context.RMUser.IsAllowedEx(this.SecurityId,RMO_ALLOW_EDIT_EVENT_NUM))
//				base.AddReadOnlyNode("ev_eventnumber");
			//Nitesh MITS 7656 Ends
			//Claim Number Locking Logic
			if(		(objClaim.IsNew && !objClaim.Context.RMUser.IsAllowedEx(this.SecurityId,RMO_CLAIM_NUMBER_ALLOW_ENTRY))
				||	(!objClaim.IsNew && !objClaim.Context.RMUser.IsAllowedEx(this.SecurityId,RMO_CLAIM_NUMBER_ALLOW_EDIT)))
				base.AddReadOnlyNode("claimnumber");
			
			objOld = objXML.SelectSingleNode("//ControlAppendAttributeList");
			objNew = objXML.CreateElement("ControlAppendAttributeList");
			XmlElement objElem=null;
			XmlElement objChild=null;

			objElem=objXML.CreateElement("ev_eventnumber");
			if (!objClaim.IsNew)
			{
                //Commented by pmittal5 Mits 16471 - linktobutton has been removed for Event Number 
				//objChild=objXML.CreateElement("linktobutton");
				//objChild.SetAttribute("value","1");
				//Nitesh MITS 7656 Starts
				//objElem.AppendChild(objChild);
				if (!objEvent.Context.RMUser.IsAllowedEx(this.SecurityId, RMO_ALLOW_EDIT_EVENT_NUM))
				{
					objChild=objXML.CreateElement("readonlyText");
					objChild.SetAttribute("value","true");
					objElem.AppendChild(objChild);
					//base.AddReadOnlyNode("eventnumber");
				}
				//Nitesh MITS 7656 ends
			}
			else
			{
				// MITS 9558  Need to remove. Button doesn't work correctly.  objChild=objXML.CreateElement("lookupbutton");
				// MITS 9558  Need to remove. Button doesn't work correctly.  objChild.SetAttribute("value","1");
				//Nitesh MITS 7656 Starts
				// MITS 9558  Need to remove. Button doesn't work correctly.  objElem.AppendChild(objChild);
				if(	(objEvent.EventId ==0 && !objEvent.Context.RMUser.IsAllowedEx(this.SecurityId, RMO_ALLOW_ENTRY_EVENT_NUM))
					||	(objEvent.EventId !=0 && !objEvent.Context.RMUser.IsAllowedEx(this.SecurityId, RMO_ALLOW_EDIT_EVENT_NUM)))
				{
					objChild=objXML.CreateElement("readonlyText");
					objChild.SetAttribute("value","true");
					objElem.AppendChild(objChild);
					//base.AddReadOnlyNode("eventnumber");
				}
			}

			//objElem.AppendChild(objChild);;
			//Nitesh MITS 7656 Ends
			objNew.AppendChild(objElem);

			if(objOld !=null)
				objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);

			//Show/Clear DateTimeStamp of Record Closing if record is/is not closed.
			if(8!=objClaim.Context.InternalSettings.CacheFunctions.GetRelatedCodeID(objClaim.ClaimStatusCode))
				objClaim.DttmClosed="";

            // ABhateja, 05.07.2007 -START-
            // MITS 9980, Commented the code for location and event description 
            // freeze flags as it is being checked in the editor.cs file.
            //Freeze LocationAreaDesc and EventDesc if Required by Settings.
            //if(objClaim.Context.InternalSettings.SysSettings.FreezeLocDesc!=0)
            //    base.AddReadOnlyNode("ev_locationareadesc");
            //if(objClaim.Context.InternalSettings.SysSettings.FreezeEventDesc!=0)
            //    base.AddReadOnlyNode("ev_eventdescription");
            // ABhateja, 05.07.2007 -END-

            //Apply Record Details to Title
			ApplyFormTitle();

			//add the folder node
            //R7 PRF IMP: No need to check the directory 
            if ((objClaim.Parent as Event).StateId != 0)
            {
                string sPath = objClaim.Context.DbConnLookup.ExecuteString(
                    "SELECT STATE_ID FROM STATES WHERE STATE_ROW_ID =" + (objClaim.Parent as Event).StateId);
                sPath = RMConfigurator.CombineFilePath(RMConfigurator.AppFilesPath, string.Format(@"pdf-forms\ClaimForms\{0}", sPath));
                //RMConfigurator objConfig= new RMConfigurator();//R5 PS2 Merge
                //sPath = objConfig.GetValue("PDFFormsPath") + "ClaimForms\\" + sPath;//R5 PS2 Merge
                if (!System.IO.Directory.Exists(sPath))
                {
                    base.ResetSysExData("folder", "");
                    base.AddKillNode("btnClaimForms");
                }
                else
                    base.ResetSysExData("folder", sPath);
            }
            else
            {
                base.ResetSysExData("folder", "");
                base.AddKillNode("btnClaimForms");
            }
	
			if(base.m_CurrentAction != enumFormActionType.Save)
				base.ResetSysExData("dupeoverride","");

			//create nodes in sysExData for Claim Status History Field
			base.CreateSysExData("StatusApprovedBy");
			base.CreateSysExData("StatusDateChg");
			base.CreateSysExData("StatusReason");

           //MGaba2:MITS 10241:Start
           base.CreateSysExData("systemcurdate",Conversion.GetUIDate(DateTime.Now.ToShortDateString(),LanguageCode,ClientId));//vkumar258 ML Changes
           base.CreateSysExData("BackdtClaimSetting", "1");
           if (!m_fda.userLogin.IsAllowedEx(this.SecurityId, FormBase.RMO_VA_BKDATE_CLAIM_STATUS_HISTORY))
           {
               base.ResetSysExData("BackdtClaimSetting", "0");
           }
           //MGaba2:MITS 10241:End
			//for Displaying Back button on Policy Maintenance Screen
			base.ResetSysExData("DisplayBackButton","true");
			//for Closing Diary or not -- Rahul 6th Feb 2006
			base.ResetSysExData("CloseDiary","false");

			//Nikhil Garg		17-Feb-2006
			//check whether open diaries exist for this claim or not
            if (HasOpenDiaries)
                base.ResetSysExData("ContainsOpenDiaries", "true");
            else
                base.ResetSysExData("ContainsOpenDiaries", "false");

            //Changed by Gagan for MITS 11451 : Start

            //Initializing Delete Auto-check flag
            base.ResetSysExData("DeleteAutoCheck", "false");
            base.ResetSysExData("ClaimStatusCode", objClaim.ClaimStatusCode.ToString());

            //check whether auto checks diaries exist for this claim or not
            string sSQL = "SELECT CLAIM_ID FROM FUNDS_AUTO WHERE CLAIM_ID = " + objClaim.ClaimId.ToString();
            using (DbReader objReaderAutoChecks = objClaim.Context.DbConn.ExecuteReader(sSQL))
            {
                if (objReaderAutoChecks.Read())
                    base.ResetSysExData("ContainsAutoChecks", "true");
                else
                    base.ResetSysExData("ContainsAutoChecks", "false");
            }
            //Changed by Gagan for MITS 11451 : End

            //Changed by Gagan for MITS 14770
            if (objClaim.Context.InternalSettings.SysSettings.DeleteAllClaimDiaries == 0)
            {
                base.ResetSysExData("DeleteAllClaimDiaries", "false");
            }
            else
            {
                base.ResetSysExData("DeleteAllClaimDiaries", "true");
            }
			//Nitesh(09/27/2006): LSS Merge Setup changes Starts
			
			try
			{
				//sSQL="SELECT RMX_LSS_ENABLE FROM SYS_PARMS" ;
				//int LSSEnable = objClaim.Context.DbConn.ExecuteInt(sSQL);
                //R7 PRF IMP: Syssettings has this flag
                bool bLSSEnable = objClaim.Context.InternalSettings.SysSettings.RMXLSSEnable;
                if (!bLSSEnable)
				{ 
					base.AddKillNode("lssclaimind");
				}
			}
			catch
			{
				base.AddKillNode("lssclaimind");
			}
            //Start: Added by Sumit Agarwal, 08/05/2014, MITS 33588
            if (objClaim.Context.InternalSettings.SysSettings.UseInsuredClaimDept != -1)
            {
                base.AddKillNode("clm_insdepteid");
            }
            base.ResetSysExData("InsuredClaimDeptFlag", objClaim.Context.InternalSettings.SysSettings.UseInsuredClaimDept.ToString());  //Added by Sumit Agarwal to set the value for InsuredClaimDeptFlag: 09/23/2014: MITS 33588
            //End: Added by Sumit Agarwal, 08/05/2014, MITS 33588

            //Start - VSS enable setting check
            //if (!objClaim.Context.InternalSettings.SysSettings.EnableVSS.Equals(-1))
            //{
            //    base.AddKillNode("vssclaimind");
            //}
            //else
            //{
            //    base.AddDisplayNode("vssclaimind");
            //}
            //End - VSS enable setting check
			
			//Nitesh(09/27/2006): LSS Merge Setup changes Ends
            //MGaba2:MITS 15642:creating the node so that it doesnt come in missing refs:start
            CreateSysExData("EventOnPremiseChecked", "false");        
            //tkr mits 10267.  need entire org hierarchy to hide/show group assoc supp fields
            bool isOhSet = false;
            StringBuilder strB = null;//apandey21 Curr Mull
            int iResCount = 0;
            if (objClaim.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
            {
                iResCount = objClaim.Context.DbConn.ExecuteInt("SELECT COUNT(1) FROM FUNDS_AUTO WHERE CLAIM_ID=" + objClaim.ClaimId);
            }
            if (objEvent.DeptEid > 0)
            {
                using (DbReader rdr = objClaim.Context.DbConnLookup.ExecuteReader("SELECT * FROM ORG_HIERARCHY WHERE DEPARTMENT_EID = " + objEvent.DeptEid.ToString()))
                {
                    while (rdr.Read())
                    {
                        isOhSet = true;
                        base.ResetSysExData("OH_FACILITY_EID", rdr.GetInt("FACILITY_EID").ToString());
                        base.ResetSysExData("OH_LOCATION_EID", rdr.GetInt("LOCATION_EID").ToString());
                        base.ResetSysExData("OH_DIVISION_EID", rdr.GetInt("DIVISION_EID").ToString());
                        base.ResetSysExData("OH_REGION_EID", rdr.GetInt("REGION_EID").ToString());
                        base.ResetSysExData("OH_OPERATION_EID", rdr.GetInt("OPERATION_EID").ToString());
                        base.ResetSysExData("OH_COMPANY_EID", rdr.GetInt("COMPANY_EID").ToString());
                        base.ResetSysExData("OH_CLIENT_EID", rdr.GetInt("CLIENT_EID").ToString());
                        if (objClaim.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)//Deb Multi Currency
                        {
                            strB = new StringBuilder(objEvent.DeptEid.ToString());
                            if (strB.ToString() != "")
                                strB.Append(",");
                            strB.Append(rdr.GetInt("FACILITY_EID").ToString() + ",");
                            strB.Append(rdr.GetInt("LOCATION_EID").ToString() + ",");
                            strB.Append(rdr.GetInt("DIVISION_EID").ToString() + ",");
                            strB.Append(rdr.GetInt("REGION_EID").ToString() + ",");
                            strB.Append(rdr.GetInt("OPERATION_EID").ToString() + ",");
                            strB.Append(rdr.GetInt("COMPANY_EID").ToString() + ",");
                            strB.Append(rdr.GetInt("CLIENT_EID").ToString());
                        }
                    }
                }
                if (objClaim.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
                {
                    string sCurrCode = string.Empty;
                    string[] arr = strB.ToString().Split(',');
                    for (int i = 0; i < arr.Length; i++)
                    {
                        int orgCurrCode = objClaim.Context.DbConnLookup.ExecuteInt("SELECT ORG_CURR_CODE FROM ENTITY WHERE ENTITY_ID =" + arr[i]);
                        if (orgCurrCode > 0)
                        {
                            sCurrCode = orgCurrCode.ToString();
                            break;
                        }
                    }
                    if (!string.IsNullOrEmpty(sCurrCode))
                    {
                        if (m_CurrentAction != enumFormActionType.Save && m_CurrentAction != enumFormActionType.Delete && m_CurrentAction != enumFormActionType.Refresh && m_CurrentAction != enumFormActionType.MoveTo && iResCount <= 0)
                            objClaim.CurrencyType = Convert.ToInt32(sCurrCode);//Deb Multi Currency
                    }
                }
            }
            if (!isOhSet)
            {
                base.ResetSysExData("OH_FACILITY_EID", "0");
                base.ResetSysExData("OH_LOCATION_EID", "0");
                base.ResetSysExData("OH_DIVISION_EID", "0");
                base.ResetSysExData("OH_REGION_EID", "0");
                base.ResetSysExData("OH_OPERATION_EID", "0");
                base.ResetSysExData("OH_COMPANY_EID", "0");
                base.ResetSysExData("OH_CLIENT_EID", "0");
            }
            CreateSysExData("UseLegacyComments", Riskmaster.Common.Conversion.ConvertObjToStr(objClaim.Context.InternalSettings.SysSettings.UseLegacyComments));
            //BOB Enhancement 
            base.ResetSysExData("isAutoPopulateDpt", objClaim.Context.InternalSettings.SysSettings.AutoPopulateDpt.ToString());
            if (objClaim.Context.InternalSettings.SysSettings.MultiCovgPerClm == -1)
             RenderPolicyList(ref objXML);
            //Deb Multi Currency
            int iBaseCurrCode = objClaim.Context.InternalSettings.SysSettings.BaseCurrencyType;
            if (objClaim.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
            {
                if (objClaim.IsNew)
                {
                    if (iBaseCurrCode > 0 && objClaim.CurrencyType <= 0)
                    {
                        string sCurrency = objClaim.Context.DbConn.ExecuteString("SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + iBaseCurrCode);
                        base.ResetSysExData("BaseCurrencyType", sCurrency.Split('|')[1]);
                        objClaim.CurrencyType = iBaseCurrCode;
                    }
                    else if (objClaim.CurrencyType <= 0)
                    {
                        Errors.Add(Globalization.GetString("BaseCurrency.Error", base.ClientId), "Please set the Base Currency in the Utilities", BusinessAdaptorErrorType.Error);//sharishkumar Jira 804
                    }
                }
                else
                {
                    if (iBaseCurrCode > 0)
                    {
                        string sCurrency = objClaim.Context.DbConn.ExecuteString("SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + iBaseCurrCode);
                        base.ResetSysExData("BaseCurrencyType", sCurrency.Split('|')[1]);
                        //rupal:start,multicurrency
                        //commented because for existing claim, if claim currency is not set then we will ask user to manually set
                        /*
                        if (objClaim.CurrencyType <= 0)
                        {
                            objClaim.CurrencyType = iBaseCurrCode;
                        }
                        */
                        //rupal:end 
                    }
                    else
                    {
                        Errors.Add(Globalization.GetString("BaseCurrency.Error", base.ClientId), "Please set the Base Currency in the Utilities", BusinessAdaptorErrorType.Error);//sharishkumar Jira 804
                    }
                    if (iResCount > 0 || objClaim.FundsList.Count > 0)
                    {
                        base.AddReadOnlyNode("currencytypetext");
                    }
                }
            }
            else
            {
                if (iBaseCurrCode > 0)
                {
                    string sCurrency = objClaim.Context.DbConn.ExecuteString("SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + iBaseCurrCode);
                    base.ResetSysExData("BaseCurrencyType", sCurrency.Split('|')[1]);
                    if (objClaim.CurrencyType <= 0)
                    {
                        objClaim.CurrencyType = iBaseCurrCode;
                    }
                    base.AddKillNode("currencytypetext");
                }
            }
            //Deb Multi Currency
            //Added by Amitosh for Policy interface
            if (objClaim.IsNew)
                base.AddReadOnlyNode("multipolicyid");
            //End Amitosh
            //nsachdeva2 - 7/9/2012 - PolicySave
            int iTransCount = 0;
            if (objClaim.Context.InternalSettings.SysSettings.UsePolicyInterface)
            {
                iTransCount = objClaim.Context.DbConn.ExecuteInt("SELECT COUNT(1) FROM FUNDS,FUNDS_AUTO WHERE FUNDS.CLAIM_ID = FUNDS_AUTO.CLAIM_ID AND FUNDS.CLAIM_ID =" + objClaim.ClaimId);
                if (iTransCount == 0)
                {
                    base.AddKillNode("multipolicyidbtndel");
                }
            }
            //Amandeep Catastrophe Enhancement MITS 28528--start
            if (!objClaim.IsNew)
                base.ResetSysExData("CatastropheNumber", GetCatastropheNumber());
            else
                base.ResetSysExData("CatastropheNumber", "");

            //PSHEKHAWAT : MITS-23383 : Hide/display control 'Email ACORD to' (ID=emailaddresses) based on the value of AutoFROIACORDFlag in utilities          
            if (objSysSettings.AutoFROIACORDFlag == false)
                base.AddKillNode("emailaddresses");
	        //tanwar2 - ImageRight - start           
                if (objClaim.Context.InternalSettings.SysSettings.UseImgRight == false)
                {
                    base.AddKillNode("openImageRight");
                    base.AddKillNode("generateFUP");
                }
                //NameValueCollection nvCol = RMConfigurationManager.GetNameValueSectionSettings("ImageRight");
                //if (nvCol != null && !string.IsNullOrEmpty(nvCol["IR_Drawer"]))
                //{
                //    base.ResetSysExData("irdrawer", nvCol["IR_Drawer"]);
                //}
                int iIRCodeId = 0;
                bool bSuccess = false;
                if (objClaim.Supplementals["IR_DRAWER_CODE"] != null)
                {
                    iIRCodeId = Conversion.CastToType<int>(objClaim.Supplementals["IR_DRAWER_CODE"].Value.ToString(), out bSuccess);
                    base.ResetSysExData("irdrawer", objClaim.Context.LocalCache.GetCodeDesc(iIRCodeId));
                }
                if (objClaim.Supplementals["IR_FILE_TYPE_CODE"] != null)
                {
                    iIRCodeId = Conversion.CastToType<int>(objClaim.Supplementals["IR_FILE_TYPE_CODE"].Value.ToString(), out bSuccess);
                    base.ResetSysExData("irfiletype", objClaim.Context.LocalCache.GetCodeDesc(iIRCodeId));
                }
                if (string.Compare(base.GetSysExDataNodeText("/SysExData/generateFUPFile"), "-1") == 0)
                {
                    base.ResetSysExData("generateFUPFile", "-1");
                }
                else
                {
                    base.ResetSysExData("generateFUPFile", "0");
                }          
            //tanwar2 - ImageRight - end
            //// asingh263 Added for MITS 31673 Starts
            if (objClaim.Context.InternalSettings.SysSettings.MultiCovgPerClm == 0)
            {
                using (Policy objPolicy = (Policy)m_fda.Factory.GetDataModelObject("Policy", false))
                {
                    if (objClaim.PrimaryPolicyId != default(int))
                    {
                        objPolicy.MoveTo(objClaim.PrimaryPolicyId);

                        if (!string.IsNullOrEmpty(objPolicy.EffectiveDate))
                        {
                            string primaryPolicyEffectiveDate = Conversion.ToDate(objPolicy.EffectiveDate).ToShortDateString();
                            base.ResetSysExData("PrimaryPolicyEffectiveDate", primaryPolicyEffectiveDate);
                        }
                        else
                        {
                            base.ResetSysExData("PrimaryPolicyEffectiveDate", string.Empty);
                        }

                        if (!string.IsNullOrEmpty(objPolicy.ExpirationDate))
                        {
                            string primaryPolicyExpirationDate = Conversion.ToDate(objPolicy.ExpirationDate).ToShortDateString();
                            base.ResetSysExData("PrimaryPolicyExpirationDate", primaryPolicyExpirationDate);
                        }
                        else
                        {
                            base.ResetSysExData("PrimaryPolicyExpirationDate", string.Empty);
                        }
                    }
                    else
                    {
                        base.ResetSysExData("PrimaryPolicyExpirationDate", string.Empty);
                        base.ResetSysExData("PrimaryPolicyEffectiveDate", string.Empty);
                    }
                    bool claimTriggerClaimFlag = objPolicy.TriggerClaimFlag;
                    base.ResetSysExData("ClaimTriggerClaimFlag", claimTriggerClaimFlag.ToString());
                }
            }
            //// asingh263 Added for MITS 31673 Ends
		}

        private string GetCatastropheNumber()
        {
            string sSQL = "SELECT CAT_NUMBER FROM CATASTROPHE WHERE CATASTROPHE_ROW_ID = " + objClaim.CatastropheRowId;
            string sReturnValue = string.Empty;
            try
            {
                //using (DbReader objRdr = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, sSQL))
                using (DbReader objRdr = objClaim.Context.DbConn.ExecuteReader(sSQL))
                {
                    if (objRdr.Read())
                        sReturnValue = Conversion.ConvertObjToStr(objRdr.GetValue(0));
                }
            }
            catch (Exception e)
            {
                sReturnValue = string.Empty;
            }
            return sReturnValue;
        }
        //Amandeep Catastrophe Enhancement MITS 28528--end

		public override void BeforeSave(ref bool Cancel)
		{
			Event objEvent = (objClaim.Parent as Event);
			base.BeforeSave (ref Cancel);
            int iNumPolicy = 0;//added by rkaur7 on 05/27/2009 - MITS 16668
            string sShowPolPopUp = string.Empty;//added by rkaur7 on 05/27/2009 - MITS 16668
			//Check UserLimit Security.
			if(objLobSettings.ClmAccLmtFlag)//If UserLimits Enabled...
			{
				if(objLimits != null)
                    //Asharma326 MITS 30874 Add objLobSettings.ClmAccLmtUsrFlag
                    if ((objClaim.IsNew && !objLimits.ClaimCreateAllowed(objClaim.LineOfBusCode, objClaim.ClaimTypeCode, objClaim.ClaimStatusCode, objLobSettings.ClmAccLmtUsrFlag)) ||
                        !objClaim.IsNew && !objLimits.ClaimCreateAllowed(objClaim.LineOfBusCode, objClaim.ClaimTypeCode, objClaim.ClaimStatusCode, objLobSettings.ClmAccLmtUsrFlag))
					{
						Errors.Add(Globalization.GetString("SaveError",base.ClientId),
                            String.Format(Globalization.GetString("Save.Claim.UserLimitFailed", base.ClientId), objCache.GetCodeDesc(objClaim.ClaimTypeCode), objCache.GetCodeDesc(objClaim.ClaimStatusCode)),//sharishkumar Jira 804
							BusinessAdaptorErrorType.Error);
						Cancel = true;
						return;
					}
			}
			

			//Check Permission to Event Number.
			if(base.ModuleSecurityEnabled)//Is Security Enabled
			{
				if(!objClaim.IsNew)
				{
					if(!m_fda.userLogin.IsAllowedEx(this.SecurityId,FormBase.RMO_ALLOW_EDIT_EVENT_NUM))
					{
						string sPrev = objEvent.Context.DbConnLookup.ExecuteString("SELECT EVENT_NUMBER FROM EVENT WHERE EVENT_ID=" + objEvent.EventId);
						objEvent.EventNumber = sPrev;
						objClaim.EventNumber = sPrev;
					}
                    //Start:Sumit (10/28/2010) - MITS# 22849 - Update Claim Event number when edited.
                    else
                    {
                        objClaim.EventNumber = objEvent.EventNumber;
                    }
                    //End:Sumit
				}
				else
				{
					if(objEvent.EventId==0)
					{
						if(!m_fda.userLogin.IsAllowedEx(this.SecurityId,FormBase.RMO_ALLOW_ENTRY_EVENT_NUM))
						{
							//string sPrev = objEvent.Context.DbConnLookup.ExecuteString("SELECT EVENT_NUMBER FROM EVENT WHERE EVENT_ID=" + objEvent.EventId);
							//objEvent.EventNumber = sPrev;
							//objClaim.EventNumber = sPrev;
							objEvent.EventNumber = "";
							objClaim.EventNumber = "";
						}
					}
					else //New Claim, existing Event.
					{
						if(!m_fda.userLogin.IsAllowedEx(this.SecurityId,FormBase.RMO_ALLOW_EDIT_EVENT_NUM))
						{
							string sPrev = objEvent.Context.DbConnLookup.ExecuteString("SELECT EVENT_NUMBER FROM EVENT WHERE EVENT_ID=" + objEvent.EventId);
							objEvent.EventNumber = sPrev;
							objClaim.EventNumber = sPrev;
						}
					}
				}

				//Check Permissions to Claim Number
				if(objClaim.IsNew && !m_fda.userLogin.IsAllowedEx(this.SecurityId,FormBase.RMO_CLAIM_NUMBER_ALLOW_ENTRY))
					// objClaim.ClaimNumber = objEvent.Context.DbConnLookup.ExecuteString("SELECT CLAIM_NUMBER FROM CLAIM WHERE CLAIM_ID=" + objClaim.ClaimId);
					objClaim.ClaimNumber = "";
				else if(!objClaim.IsNew && !m_fda.userLogin.IsAllowedEx(this.SecurityId,FormBase.RMO_CLAIM_NUMBER_ALLOW_EDIT))
					objClaim.ClaimNumber = objEvent.Context.DbConnLookup.ExecuteString("SELECT CLAIM_NUMBER FROM CLAIM WHERE CLAIM_ID=" + objClaim.ClaimId);

				//Check Permission to change Department. --This one is considered fatal.
				if(objEvent.EventId!=0 && objEvent.DeptEid != objEvent.Context.DbConnLookup.ExecuteInt("SELECT DEPT_EID FROM EVENT WHERE EVENT_ID=" + objEvent.EventId))
					if(!m_fda.userLogin.IsAllowedEx(this.SecurityId,FormBase.RMO_CLAIM_CHANGE_DEPARTMENT))
					{
						Errors.Add(Globalization.GetString("SaveError",base.ClientId),
							String.Format(Globalization.GetString("Save.DeptChangeFailed",base.ClientId)),
                            BusinessAdaptorErrorType.Error);//sharishkumar Jira 804
						Cancel = true;
						return;
					}
				
				//Check Permission on Claim Status Change
				if(!objClaim.IsNew)
				{
					int prevCode= objClaim.Context.DbConnLookup.ExecuteInt("SELECT CLAIM_STATUS_CODE FROM CLAIM WHERE CLAIM_ID=" + objClaim.ClaimId);
					int prevParent= objClaim.Context.LocalCache.GetRelatedCodeId(prevCode);
					int curParent = objClaim.Context.LocalCache.GetRelatedCodeId(objClaim.ClaimStatusCode);
					
					if(prevParent!=curParent && curParent==8)
						if(!m_fda.userLogin.IsAllowedEx(this.SecurityId,FormBase.RMO_CLAIM_ALLOW_CLOSE))
						{
							Errors.Add(Globalization.GetString("SaveError",base.ClientId),
								String.Format(Globalization.GetString("Save.ClaimClosePermissionFailed",base.ClientId)),
                                BusinessAdaptorErrorType.Error);//sharishkumar Jira 804
							Cancel = true;
							return;
						}

					if(prevParent!=curParent && curParent==9)
						if(!m_fda.userLogin.IsAllowedEx(this.SecurityId,FormBase.RMO_CLAIM_ALLOW_REOPEN))
						{
							Errors.Add(Globalization.GetString("SaveError",base.ClientId),
								String.Format(Globalization.GetString("Save.ClaimReOpenPermissionFailed",base.ClientId)),
                                BusinessAdaptorErrorType.Error);//sharishkumar Jira 804
							Cancel = true;
							return;
						}

					//Nikhil Garg		Dated: 27-Jan-06
					//Checking for Permission to update a closed claim
					if (prevParent==curParent && curParent==8)
						if(!m_fda.userLogin.IsAllowedEx(this.SecurityId,FormBase.RMO_CLAIM_UPDATE_CLOSED))
						{
							Errors.Add(Globalization.GetString("SaveError",base.ClientId),
								String.Format(Globalization.GetString("Save.ClaimCloseUpdatePermissionFailed",base.ClientId)),
                                BusinessAdaptorErrorType.Error);//sharishkumar Jira 804
							Cancel = true;
							return;
						}
				}

			}//End "If Module Security"
			
			//Default the Time of Event & Time Reported
			if(objEvent.TimeOfEvent=="" && objClaim.Context.InternalSettings.SysSettings.DefaultTimeFlag !=-1)
				objEvent.TimeOfEvent= "12:00 AM";
			if(objClaim.TimeOfClaim=="")
				objClaim.TimeOfClaim= "12:00 AM";
			if(objEvent.TimeReported=="" && objClaim.Context.InternalSettings.SysSettings.DefaultTimeFlag !=-1)
				objEvent.TimeReported= "12:00 AM";
			//Nikhil Garg		Dated: 23-Dec-05	Bug No:1069
			//As per the RMWorld behaviour, there is no need to initialize Time Reported
//			if(objEvent.TimeReported=="" && objEvent.DateReported!="")
//				objEvent.TimeReported= "12:00 AM";
			
			
			// Update Close Diaries Flag for later if necessary.
            int prevStatusCode = objClaim.Context.DbConnLookup.ExecuteInt("SELECT CLAIM_STATUS_CODE FROM CLAIM WHERE CLAIM_ID=" + objClaim.ClaimId);

            //Changed by Saurabh Arora for MITS 19061:Start
            /* bool bWasClosed = (objClaim.Context.LocalCache.GetRelatedCodeId(prevStatusCode) == 8 || prevStatusCode == 8);
            bool bIsClosed = (objClaim.Context.LocalCache.GetRelatedCodeId(objClaim.ClaimStatusCode) == 8 || objClaim.ClaimStatusCode == 8);
            if (!bWasClosed && bIsClosed)
                bCloseDiaries = true; */
            int bClosedCodeId = objClaim.Context.LocalCache.GetCodeId("C", "Status");
            bCloseDiaries = (objClaim.Context.LocalCache.GetRelatedCodeId(objClaim.ClaimStatusCode) == bClosedCodeId);
            
            //Changed by Saurabh Arora for MITS 19061:End

            //Mridul 05/28/09 MITS 16745-Chubb ACK Letter Enhancement
            if (prevStatusCode != objClaim.ClaimStatusCode)
            {
                //Commented By Navdeep, Change in table, Closed Status Mapped to SYS_UTIL_CLM_LTR
                //string sStatInList = Conversion.ConvertObjToStr(objClaim.Context.DbConn.ExecuteScalar("SELECT CLOSED_STATUS FROM SYS_PARMS_CLM_LTR WHERE CLOSED_STATUS LIKE '%," + objClaim.ClaimStatusCode + ",%'"));                
                string sStatInList = Conversion.ConvertObjToStr(objClaim.Context.DbConn.ExecuteScalar("SELECT COLUMN_DESC FROM SYS_UTIL_CLM_LTR WHERE ROW_ID = 3 AND CODE_EID IN (" + objClaim.ClaimStatusCode + ")"));

                //Check if Current status is among those in the list
                if (sStatInList != "")
                    bTriggerLetter = true;
            }

			//Duplicate Claim Check
			// Mihika 6-Jan-2006 Defect no. 1189
			if(objClaim.IsNew && IsDupeClaim())
			{
//				Errors.Add(Globalization.GetString("SaveError"),
//					String.Format(Globalization.GetString("Save.DuplicateClaimWarning")),
//					BusinessAdaptorErrorType.Error);
				Cancel = true;
				return;
			}
			base.ResetSysExData("dupeoverride","");

            bool bCheckPolicyValidation = false; ;
            if (base.SysEx.SelectSingleNode("//CheckPolicyValidation") != null && base.SysEx.SelectSingleNode("//CheckPolicyValidation").InnerText != "")
                bCheckPolicyValidation = Conversion.ConvertStrToBool(base.SysEx.SelectSingleNode("//CheckPolicyValidation").InnerText);

			// Mihika 16-Jan-2006 Defect no. 1232
			// Autoselect Policy if the corresponding Utilities Setting is checked
			int iPolicyId = 0;

            //Start:Nitin Goel,Check for specific LOB. 01/13/10, MITS#18229
            //if (objClaim.Context.InternalSettings.SysSettings.UseEnhPolFlag == -1)
            if (objLobSettings.UseEnhPolFlag == -1)
            //End:Nitin Goel,Check for specific LOB. 01/13/10: MITS#18229
            {
                if (!ValidatePolicy(objClaim.PrimaryPolicyIdEnh))
                    objClaim.PrimaryPolicyIdEnh = 0;
            }
            else
            {
                if (bCheckPolicyValidation && !ValidatePolicy(objClaim.PrimaryPolicyId))
                    objClaim.PrimaryPolicyId = 0;
            }

            base.ResetSysExData("CheckPolicyValidation", "false");
            //added by rkaur7 on 05/27/2009 - MITS 16668- Start
            //sysex nodes created to capture Number of policy satisfying Auto Policy Select Criterion 
            //and flag for whether to select policy pop up or not
            base.CreateSysExData("NumOfPolicy");
            base.CreateSysExData("ShowPolicyPopUp");
            //End rkaur7

            //Sumit kumar, 07/01/2010 - Disable autoselect functionality when Policy mgmt is selected for VA
            if (objClaim.Context.InternalSettings.SysSettings.AutoSelectPolicy && objClaim.Context.InternalSettings.ColLobSettings[m_LOB].UseEnhPolFlag != -1)
            {
                //AutoFillPolicy(ref iPolicyId);
                //added by rkaur7 on 05/27/2009 - MITS 16668 - passed iNumPolicy in the AutoFillPolicy function to capture the number of policy satisfying autofill criteria
                AutoFillPolicy(ref iPolicyId, ref iNumPolicy);
                // Start Naresh MITS 9996 Policy Attach with Claim for Policy Management
                //if (objClaim.Context.InternalSettings.SysSettings.UseEnhPolFlag == -1 && objClaim.PrimaryPolicyIdEnh == 0)
                //added by rkaur7 on 05/27/2009 - MITS 16668  - Added NumOfPolicy also in the if condition
                //if (objClaim.Context.InternalSettings.SysSettings.UseEnhPolFlag == -1 && objClaim.PrimaryPolicyIdEnh == 0 && iNumPolicy <= 1)
                if (objLobSettings.UseEnhPolFlag == -1 && objClaim.PrimaryPolicyIdEnh == 0 && iNumPolicy <= 1)
                //End:Nitin Goel,Check for specific LOB. 01/13/10: MITS#18229
                {
                    objClaim.PrimaryPolicyIdEnh = iPolicyId;
                }
                // End Naresh MITS 9996 Policy Attach with Claim for Policy Management
                // Start Naresh MITS 9996 Policy Attach with Claim for Policy Tracking
                //else if (objClaim.Context.InternalSettings.SysSettings.UseEnhPolFlag != -1 && objClaim.PrimaryPolicyId == 0)
                //added by rkaur7 on 05/27/2009 - MITS 16668  - Added NumOfPolicy also in the if condition
                //else if (objClaim.Context.InternalSettings.SysSettings.UseEnhPolFlag != -1 && objClaim.PrimaryPolicyId == 0 && iNumPolicy <= 1)
                else if (objLobSettings.UseEnhPolFlag != -1 && objClaim.PrimaryPolicyId == 0 && iNumPolicy <= 1)
                //End:Nitin Goel,Check for specific LOB. 01/13/10: MITS#18229
                {
                    objClaim.PrimaryPolicyId = iPolicyId;
                }
                // End Naresh MITS 9996 Policy Attach with Claim for Policy Tracking
            }
			sUserCloseDiary = base.SysEx.SelectSingleNode("//CloseDiary").InnerText;
            //added by rkaur7 on 05/27/2009 - MITS 16668  - Start
            if (base.GetSysExDataNodeText("//ShowPolicyPopUp").ToString() == "")
            {
                sShowPolPopUp = "True";
            }
            else
            {
                sShowPolPopUp = "";
            }
            base.SysEx.SelectSingleNode("//NumOfPolicy").InnerText = iNumPolicy.ToString();
            base.SysEx.SelectSingleNode("//ShowPolicyPopUp").InnerText = sShowPolPopUp;
            //added by rkaur7 on 05/27/2009 - MITS 16668 - End
            //Changed by Gagan for MITS 11451 : Start
            if (base.SysEx.SelectSingleNode("//DeleteAutoCheck").InnerText != "")
                bDeleteAutoChecks = Conversion.ConvertStrToBool(base.SysEx.SelectSingleNode("//DeleteAutoCheck").InnerText);
            //Changed by Gagan for MITS 11451 : End

			//check for duplicate claim number
			if (objClaim.ClaimNumber.Trim() != string.Empty)
			{
                //string sSQL="SELECT CLAIM_ID FROM CLAIM WHERE CLAIM_NUMBER='" + objClaim.ClaimNumber + "' AND CLAIM_ID<>" + objClaim.ClaimId.ToString();
                //using (DbReader objReader = objClaim.Context.DbConn.ExecuteReader(sSQL))
                //{
                //Deb:Commented above line used parameterized input for sql query for MITS 25141

                Dictionary<string, string> dictParams = new Dictionary<string, string>();
                string sSQL = string.Format("SELECT CLAIM_ID FROM CLAIM WHERE CLAIM_NUMBER={0} AND CLAIM_ID <> {1}", "~CLAIMNUMBER~", "~CLAIMID~");
                dictParams.Add("CLAIMNUMBER", objClaim.ClaimNumber);
                dictParams.Add("CLAIMID", objClaim.ClaimId.ToString());
                using (DbReader objReader = DbFactory.ExecuteReader(objClaim.Context.DbConn.ConnectionString, sSQL, dictParams))
                {
                    if (objReader.Read())
                    {
                        Errors.Add(Globalization.GetString("SaveError",base.ClientId),
                            String.Format(Globalization.GetString("Save.DuplicateClaimNumberWarning",base.ClientId)),
                            BusinessAdaptorErrorType.Error);//sharishkumar Jira 804
                        Cancel = true;
                        return;
                    }
                }
			}
		}
		// This is invoked just after the memory object is being populated from Serialized Instance Xml.
		// Makes it an appropriate place to re-populate any additional "non-child" Xml object data.
		public override void OnUpdateObject()
		{
            XmlDocument objXML = base.SysEx;
            string[] array = null;
            //For new claim, InitCClaim custom script could set value for Comments.
            //If LegacyComments is true, we need to bring value back
            objClaim.UpdateObjectComments(objXML, objClaim.ClaimId);

            //Raman 08/28/2008 : We need to copy SysEx from Event
            //Storing Current SysEx in an xmldocument
            Event objEvent = (objClaim.Parent as Event);
            XmlElement oCurSysExele = (XmlElement)base.SysEx.SelectSingleNode("//Parent");
            if (oCurSysExele != null)
            {
                string sCurrentSysEx = oCurSysExele.InnerXml;
                XmlDocument oCurrentSysEx = new XmlDocument();
                oCurrentSysEx.InnerXml = sCurrentSysEx;

                //Preparing SysEx from Event

                base.SysEx.SelectSingleNode("//Parent").InnerXml = objEvent.SerializeObject();
                base.CopyNodes(ref objXML, oCurrentSysEx, "Event");
            }
            
            base.OnUpdateObject ();
			
            // BSB 06.15.2006 If the claim and event are "out of sync" after the 
			// populateObject call made by default against the objClaim object with the 
			// XML from propertyStore, we may need to handle this manually.
			// This is a possibility on new claims as a result of Init Scripts loading the parent event
			// and then a UI lookup requesting a different (already existing) parent explicitly.
			// This situation is only applicable to the claim screens.  It happens when the default "screen" 
			// represented object is not the logical "root".  (Here it's logically event but implemented as claim)
			
			if(objClaim.EventId != objEvent.EventId)
			{
				objClaim.Parent=null;
				objEvent=(objClaim.Parent as Event);
			}
			objEvent.PopulateObject(Utilities.XPointerDoc("//Parent/Instance",base.SysEx),true);

			//update Claim Status History Fields from SysExData
			objClaim.StatusApprovedBy=objXML.SelectSingleNode("//StatusApprovedBy").InnerText;
			objClaim.StatusDateChg=objXML.SelectSingleNode("//StatusDateChg").InnerText;
			objClaim.StatusReason=objXML.SelectSingleNode("//StatusReason").InnerText;
            if (objSysSettings.MultiCovgPerClm == -1)
            {
                if (objXML.SelectSingleNode("//ClaimPolicyList/@codeid") != null)
                {
                    string sClaimPolicyList = objXML.SelectSingleNode("//ClaimPolicyList").Attributes["codeid"].Value;
                    array = sClaimPolicyList.Split(" ".ToCharArray()[0]);
                }
                if (array != null)
                {
                    for (int count = 0; count <= array.Length - 1; count++)
                    {
                        bool bAdd = true;
                        foreach (ClaimXPolicy OClaimXPolicy in objClaim.ClaimPolicyList)
                        {
                            if (Conversion.ConvertStrToInteger(array[count]) == OClaimXPolicy.PolicyId)
                            {
                                bAdd = false;
                                break;
                            }
                        }
                        if (bAdd)
                        {
                            ClaimXPolicy objClaimXPolicy = objClaim.ClaimPolicyList.AddNew();
                            objClaimXPolicy.PolicyId = Conversion.ConvertStrToInteger(array[count]);
                        }
                    }
                }
                foreach (ClaimXPolicy OClaimXPolicy in objClaim.ClaimPolicyList)
                {
                    bool bDelete = true;
                    for (int count = 0; count <= array.Length - 1; count++)
                    {
                        if (Conversion.ConvertStrToInteger(array[count]) == OClaimXPolicy.PolicyId)
                        {
                            bDelete = false;
                            break;
                        }
                    }
                    if (bDelete)
                    {
                        objClaim.ClaimPolicyList.Remove(OClaimXPolicy.RowId);
                        arrPoliciesDeleted.Add(OClaimXPolicy.PolicyId);
                    }
                }
            }
            //tkr mits 8764.  auto-populate event location fields
            //MGaba2:MITS 15642:creating the node so that it doesnt come in missing refs:start
            //CreateSysExData("EventOnPremiseChecked", "false");
            //Reload the department data if this node is true
            if (base.GetSysExDataNodeText("/SysExData/EventOnPremiseChecked") == "true")
            {//MGaba2:MITS 15642:End
                PopulateEventDetailLocation(objEvent);
                base.ResetSysExData("EventOnPremiseChecked", "false");//MGaba2:MITS 15642
            }

            //tanwar2 - ImageRight - start
            //Generate FUP if text of this node is -1
            if (string.Compare(base.GetSysExDataNodeText("/SysExData/generateFUPFile"), "-1") == 0)
            {
                FUPGenerator oFUPGenerator = new FUPGenerator(base.Adaptor.userLogin, base.ClientId);
                oFUPGenerator.GenerateFUP(objClaim.ClaimId, objClaim);
            }
            //tanwar2 - ImageRight - end
		}

        private void RenderPolicyList(ref XmlDocument objXML)
        {
            XmlElement objPolicyListElement = null;
            string sCodeId = string.Empty;
            Policy objPolicy = (Policy)m_fda.Factory.GetDataModelObject("Policy", false);
            try
            {
                objPolicyListElement = (XmlElement)objXML.SelectSingleNode("/SysExData/ClaimPolicyList");
                if (objPolicyListElement != null)
                    objPolicyListElement.ParentNode.RemoveChild(objPolicyListElement);

                CreateElement(objXML.DocumentElement, "ClaimPolicyList", ref objPolicyListElement);

                foreach (ClaimXPolicy objClaimXPolicy in objClaim.ClaimPolicyList)
                {
                    if (string.IsNullOrEmpty(sCodeId) && objClaimXPolicy.PolicyId != 0)
                    {
                        sCodeId = objClaimXPolicy.PolicyId.ToString();
                    }
                    else if (objClaimXPolicy.PolicyId != 0)
                    {
                        sCodeId = string.Concat(sCodeId + " " + objClaimXPolicy.PolicyId.ToString());
                    }
                    if (objClaimXPolicy.PolicyId != 0)
                    {
                        objPolicy.MoveTo(objClaimXPolicy.PolicyId);
                        CreateAndSetElement(objPolicyListElement, "Item", objPolicy.PolicyName, objPolicy.PolicyId);
                    }
                }
                //objPolicyListElement = (XmlElement)objXML.SelectSingleNode("/SysExData/ClaimPolicyList");
                objPolicyListElement.SetAttribute("codeid", sCodeId);

            }
            finally
            {
                objPolicyListElement = null;
            }
        }

        /// <summary>
        /// Create Element
        /// </summary>
        /// <param name="p_objParentNode">Parent Node</param>
        /// <param name="p_sNodeName">Child Node Name</param>
        /// <param name="p_objChildNode">Child Node</param>
        internal void CreateElement(XmlElement p_objParentNode, string p_sNodeName, ref XmlElement p_objChildNode)
        {
            try
            {
                p_objChildNode = p_objParentNode.OwnerDocument.CreateElement(p_sNodeName);
                p_objParentNode.AppendChild(p_objChildNode);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TODO", base.ClientId), p_objEx);//sharishkumar Jira 804
            }

        }
        /// <summary>
        /// Create Element and set inner text.
        /// </summary>
        /// <param name="p_objParentNode">Parent node</param>
        /// <param name="p_sNodeName">Node Name</param>
        /// <param name="p_sText">Text</param>
        /// <param name="p_iPolicyId">Attribute name</param>
        internal void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText, int p_iPolicyId)
        {
            try
            {
                XmlElement objChildNode = null;
                this.CreateAndSetElement(p_objParentNode, p_sNodeName, p_sText, ref objChildNode);
                objChildNode.SetAttribute("value", p_iPolicyId.ToString());
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TODO",base.ClientId), p_objEx);//sharishkumar Jira 804
            }
        }
        /// <summary>
        /// Create Element and set inner text.
        /// </summary>
        /// <param name="p_objParentNode">Parent node</param>
        /// <param name="p_sNodeName">Node Name</param>
        /// <param name="p_sText">Text</param>
        /// <param name="p_objChildNode">Child Node</param>
        internal void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText, ref XmlElement p_objChildNode)
        {
            try
            {
                CreateElement(p_objParentNode, p_sNodeName, ref p_objChildNode);
                p_objChildNode.InnerText = p_sText;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TODO", base.ClientId), p_objEx);//sharishkumar Jira 804
            }
        }

        private bool ValidatePolicy(int p_iPolicyId)
        {
            string sSQL = CreateSqlForSelectingPolicy(p_iPolicyId, true);
            using (DbReader objReader = objClaim.Context.DbConnLookup.ExecuteReader(sSQL))
            {
                if (objReader.Read())
                {
                    return true;

                }
                else { return false; }
            }


        }
		public override void AfterSave()
		{
			base.AfterSave ();

            //Added by Navdeep for Auto FROI ACORD - Chubb
            AutoFroiAcordAdaptor objAutoFroiAcord = null;
            //R7 Perf Imp
            //objAutoFroiAcord = new AutoFroiAcordAdaptor();
            string sReason = string.Empty;
            int iRowID = 0;
            //End Navdeep for Auto FROI ACORD - Chubb
            //Mridul. MITS 16745-Chubb Enhancement
            #region ClaimLetter
            int iLtrGenerated = 0;
            int iEmailAck = 0;
            Event objEvent = null;
            StringBuilder sbSql = null;
            StringBuilder sOrgEid = null;
            DbReader objReader = null;
            int iTempId = 0;
            //Check from LIST instead of below TODO
            base.ResetSysExData("ClaimLetterTmplId", "");
            if (objData.Context.InternalSettings.SysSettings.ClaimLetterFlag)
            {
                iTempId = Conversion.ConvertObjToInt(objClaim.Context.DbConn.ExecuteScalar("SELECT CLLTR_TMPL_ID FROM SYS_PARMS_CLM_LTR"), base.ClientId);
                if (bTriggerLetter && objClaim.CurrentAdjuster.AdjusterEid.ToString() != "" && iTempId != 0)
                {
                    //If generated before
                    iLtrGenerated = Conversion.ConvertObjToInt(objClaim.Context.DbConn.ExecuteScalar("SELECT LTR_FLAG FROM CLM_LTR_LOG WHERE LTR_FLAG = 1 AND CLAIM_ID = " + objClaim.ClaimId.ToString()), base.ClientId);
                    if (iLtrGenerated == 0)
                    {
                        //Commented BY Navdeep - Table changed to SYS_UTIL_CLM_LTR instead of SYS_PARMS_CLM_LTR for Claim Type IDS
                        //string sTypeNotInList = Conversion.ConvertObjToStr(objClaim.Context.DbConn.ExecuteScalar("SELECT DTTM_RCD_LAST_UPD FROM SYS_PARMS_CLM_LTR WHERE X_CLAIM_TYPE_CODE NOT LIKE '%," + objClaim.ClaimTypeCode + ",%'"));
                        string sTypeNotInList = Conversion.ConvertObjToStr(objClaim.Context.DbConn.ExecuteScalar("SELECT COLUMN_DESC FROM SYS_UTIL_CLM_LTR WHERE ROW_ID = 2 AND CODE_EID IN (" + objClaim.ClaimTypeCode +  ")"));
                        //Check if Claim Type is in Included List
                        //Commented by Code - Change due to above modified Query
                        //if (sTypeNotInList != "")
                        if (string.IsNullOrEmpty(sTypeNotInList))
                        {
                            //Commented BY Navdeep - Table changed to SYS_UTIL_CLM_LTR instead of SYS_PARMS_CLM_LTR for Adjuster EIDs
                            //string sAdjNotInList = Conversion.ConvertObjToStr(objClaim.Context.DbConn.ExecuteScalar("SELECT ADJUSTERS FROM SYS_PARMS_CLM_LTR WHERE ADJUSTERS LIKE '%," + objClaim.CurrentAdjuster.AdjusterEid + ",%'"));
                            string sAdjNotInList = Conversion.ConvertObjToStr(objClaim.Context.DbConn.ExecuteScalar("SELECT COLUMN_DESC FROM SYS_UTIL_CLM_LTR WHERE ROW_ID = 1 AND CODE_EID IN (" + objClaim.CurrentAdjuster.AdjusterEid + ")"));
                            //if (sAdjNotInList != "")
                            if (!string.IsNullOrEmpty(sAdjNotInList))
                            {
                                //Check if ACK should be generated
                                objEvent = (Event)m_fda.Factory.GetDataModelObject("Event", false);
                                objEvent.MoveTo(objClaim.EventId);
                                sbSql = new StringBuilder();
                                sOrgEid = new StringBuilder();
                                sbSql.Append("SELECT * FROM ORG_HIERARCHY WHERE DEPARTMENT_EID = " + objEvent.DeptEid);
                                using (objReader = objClaim.Context.DbConn.ExecuteReader(sbSql.ToString()))
                                {
                                    if (objReader.Read())
                                        sOrgEid.Append(objReader.GetInt("DEPARTMENT_EID") + "," + objReader.GetInt("FACILITY_EID") + "," + objReader.GetInt("LOCATION_EID") + "," + objReader.GetInt("DIVISION_EID") + "," + objReader.GetInt("REGION_EID") + "," + objReader.GetInt("OPERATION_EID") + "," + objReader.GetInt("COMPANY_EID") + "," + objReader.GetInt("CLIENT_EID"));
                                }
                                //Geeta : Mits 18718 for Acord Comma Seperated List Issue
                                iEmailAck = Conversion.ConvertObjToInt(objClaim.Context.DbConn.ExecuteScalar("SELECT COUNT(EMAIL_ACK) FROM ENT_X_CONTACTINFO EXC,CONTACT_LOB WHERE EXC.EMAIL_ACK = -1 AND EXC.ENTITY_ID IN (" + sOrgEid.ToString() + ") AND EXC.CONTACT_ID = CONTACT_LOB.CONTACT_ID AND CONTACT_LOB.LINE_OF_BUS_CODE = " + objClaim.LineOfBusCode ), base.ClientId);
                                //Send CLOSED Letter Signal
                                if (iEmailAck > 0)
                                    base.ResetSysExData("ClaimLetterTmplId", "CL");
                            }
                        }
                    }
                }
            }
            bTriggerLetter = false;
            #endregion ClaimLetter
            //navdeep - Auto FROI ACORD For Chubb - Triggering Criteria
            #region Auto FROI ACORD            
            if (objData.Context.InternalSettings.SysSettings.AutoFROIACORDFlag)
            {                
                if (objClaim.CurrentAdjuster != null)
                {
                    objAutoFroiAcord = new AutoFroiAcordAdaptor(base.ClientId);
                    if (objClaim.CurrentAdjuster.CurrentAdjFlag == true && objClaim.MailSent == "")
                    {
                        //Commented By Navdeep - Table now changed to ADJUSTER_FROI_ACORD
                        //iRowID = Riskmaster.Common.Conversion.ConvertObjToInt(objCon.ExecuteScalar("SELECT ROW_ID FROM EMAIL_FROI_ACORD WHERE ADJUSTERS LIKE '%," + objClaim.CurrentAdjuster.AdjusterEid + ",%'"), base.ClientId);
                        iRowID = Conversion.ConvertObjToInt(objClaim.Context.DbConn.ExecuteScalar("SELECT ROW_ID FROM ADJUSTER_FROI_ACORD WHERE ADJUSTER_EID = " + objClaim.CurrentAdjuster.AdjusterEid), base.ClientId);                        
                        if (iRowID > 0)
                        {
                            // Geeta 19341  : Split architecture issue  ACORD and FROI
                            XmlNode objHostNode = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/RequestHost");
                            if (objHostNode != null)
                            {
                                objAutoFroiAcord.RequestHost = objHostNode.InnerText;
                            }
                       
                            sReason = objAutoFroiAcord.generateFROIACORD(objClaim);
                            objAutoFroiAcord.FROIACORDERRLOG(objClaim, sReason);
                        }
                        else
                        {
                            objAutoFroiAcord.FROIACORDERRLOG(objClaim, "Current Adjuster was not found in Assigned Adjuster List");
                        }
                    }
                    else if (objClaim.CurrentAdjuster.CurrentAdjFlag == false && objClaim.MailSent == "")
                    {
                        objAutoFroiAcord.FROIACORDERRLOG(objClaim, "Triggering criteria not met.");
                    }
                }
            }
            //navdeep - End 
            #endregion Auto FROI ACORD
			//Check and close Diaries
			// sUserCloseDiary applied by Rahul 6th Feb 2006.
//            if(bCloseDiaries && sUserCloseDiary=="true")
//                objClaim.Context.DbConnLookup.ExecuteNonQuery(
//                    String.Format(@"UPDATE WPA_DIARY_ENTRY SET STATUS_OPEN = 0 
//					WHERE ATTACH_RECORDID = {0} AND ATTACH_TABLE = 'CLAIM'",objClaim.ClaimId));
//            bCloseDiaries =false;
            CloseAllDiaries();

            //Changed by Gagan for MITS 11451 : Start
            //Delete auto checks
            if (bDeleteAutoChecks)
                DeleteAutoChecks();
            //Changed by Gagan for MITS 11451 : End
		}
        public override Hashtable GetTrimHashTable()
        {
            Hashtable ht = new Hashtable();
            ht.Add("Comments", "Comments");
            ht.Add("HTMLComments", "HTMLComments");
            ht.Add("AdjusterList", "AdjusterList");
            ht.Add("LeavePlanList", "LeavePlanList");
            ht.Add("ClaimantList", "ClaimantList");
            ht.Add("LitigationList", "LitigationList");
            ht.Add("DefendantList", "DefendantList");
            ht.Add("UnitList", "UnitList");
            ht.Add("ClaimStatusHistList", "ClaimStatusHistList");
            ht.Add("TimeAndExpenseList", "TimeAndExpenseList");
            ht.Add("FundsList", "FundsList");
            ht.Add("ReserveCurrentList", "ReserveCurrentList");
            ht.Add("ProgressNoteList", "ProgressNoteList");
            return ht;
        }
		private bool IsDupeClaim()
		{
			Event objEvent = (objClaim.Parent as Event);
			string SQL="";
//			string sTmp="";
			XmlNode eltDupe =null;
//			XmlNode eltClaim =null;

			int iDupNumDays;
			int iDupPayCriteria;

			if(!objLobSettings.DuplicateFlag)
				return false;

			//User already saw dup popup and is choosing to proceed.
			eltDupe = base.SysEx.SelectSingleNode("//dupeoverride");
			//if(eltDupe.InnerText.Trim()!="")             csingh7 : Commented for MITS 19840
            if (eltDupe.InnerText.Trim() == "IsSaveDup") 	//csingh7 MITS 19840 : If user saves Duplicate Claim.
                return false;

			iDupNumDays = objLobSettings.DupNumDays;
			iDupPayCriteria = objLobSettings.DupPayCriteria;

			switch(iDupPayCriteria)
			{
				case 0: //No Additional Criteria
					SQL =	@"SELECT CLAIM.CLAIM_ID
						FROM CLAIM, EVENT 
						WHERE CLAIM.LINE_OF_BUS_CODE =" + objClaim.LineOfBusCode 
						+ " AND CLAIM.CLAIM_TYPE_CODE = " + objClaim.ClaimTypeCode 
						+ " AND CLAIM.EVENT_ID = EVENT.EVENT_ID "
						+ " AND EVENT.DEPT_EID = " + objEvent.DeptEid 
						+ " AND EVENT.DATE_OF_EVENT = '" + objEvent.DateOfEvent + "'"
						+ " AND CLAIM.DATE_OF_CLAIM = '" + objClaim.DateOfClaim + "'";
					break;
				case 1: //Department and Event Date within X Days
					SQL = @"SELECT CLAIM.CLAIM_ID
						FROM CLAIM, EVENT
						WHERE CLAIM.LINE_OF_BUS_CODE =" + objClaim.LineOfBusCode
						+ " AND CLAIM.EVENT_ID = EVENT.EVENT_ID "
						+ " AND EVENT.DEPT_EID = " + objEvent.DeptEid
						+ " AND EVENT.DATE_OF_EVENT >= '" + Conversion.ToDbDate(Conversion.ToDate(objEvent.DateOfEvent).AddDays(-iDupNumDays)) + "'"
						+ " AND EVENT.DATE_OF_EVENT <= '" + Conversion.ToDbDate(Conversion.ToDate(objEvent.DateOfEvent).AddDays(iDupNumDays)) + "'";
					break;
				case 2: //Operation Org Level and Event Date within X Days
					string sAbbreviation = string.Empty;
					string sName = string.Empty;
					int iParentId = 0;
					objCache.GetParentOrgInfo(objEvent.DeptEid, 1007, ref sAbbreviation, ref sName, ref iParentId);
					SQL = @"SELECT CLAIM.CLAIM_ID
						FROM CLAIM, EVENT, ORG_HIERARCHY
						WHERE CLAIM.LINE_OF_BUS_CODE = " + objClaim.LineOfBusCode
						+ " AND CLAIM.EVENT_ID = EVENT.EVENT_ID "
						+ " AND EVENT.DEPT_EID = ORG_HIERARCHY.DEPARTMENT_EID "
						+ " AND ORG_HIERARCHY.OPERATION_EID = " + iParentId
						+ " AND EVENT.DATE_OF_EVENT >= '" + Conversion.ToDbDate(Conversion.ToDate(objEvent.DateOfEvent).AddDays(-iDupNumDays)) + "'"
						+ " AND EVENT.DATE_OF_EVENT <= '" + Conversion.ToDbDate(Conversion.ToDate(objEvent.DateOfEvent).AddDays(iDupNumDays)) + "'";
					break;
				case 3: //Dept, Clm Type and Event Date within X Days
					SQL =	@"SELECT CLAIM.CLAIM_ID
						FROM CLAIM, EVENT 
						WHERE CLAIM.LINE_OF_BUS_CODE =" + objClaim.LineOfBusCode 
						+ " AND CLAIM.CLAIM_TYPE_CODE = " + objClaim.ClaimTypeCode 
						+ " AND CLAIM.EVENT_ID = EVENT.EVENT_ID "
						+ " AND EVENT.DEPT_EID = " + objEvent.DeptEid
						+ " AND EVENT.DATE_OF_EVENT >= '" + Conversion.ToDbDate(Conversion.ToDate(objEvent.DateOfEvent).AddDays(-iDupNumDays)) + "'"
						+ " AND EVENT.DATE_OF_EVENT <= '" + Conversion.ToDbDate(Conversion.ToDate(objEvent.DateOfEvent).AddDays(iDupNumDays)) + "'";
					break;
				default:
					SQL =	@"SELECT CLAIM.CLAIM_ID
						FROM CLAIM, EVENT 
						WHERE CLAIM.LINE_OF_BUS_CODE =" + objClaim.LineOfBusCode 
						+ " AND CLAIM.CLAIM_TYPE_CODE = " + objClaim.ClaimTypeCode 
						+ " AND CLAIM.EVENT_ID = EVENT.EVENT_ID "
						+ " AND EVENT.DEPT_EID = " + objEvent.DeptEid 
						+ " AND EVENT.DATE_OF_EVENT = '" + objEvent.DateOfEvent + "'"
						+ " AND CLAIM.DATE_OF_CLAIM = '" + objClaim.DateOfClaim + "'";
					break;
			}
			
//			SQL =	@"SELECT CLAIM.CLAIM_ID, CLAIM.CLAIM_NUMBER, CLAIM.CLAIM_STATUS_CODE, EVENT.EVENT_NUMBER, EVENT.EVENT_DESCRIPTION, EVENT.EVENT_ID
//						FROM CLAIM, EVENT 
//						WHERE CLAIM.LINE_OF_BUS_CODE =" + objClaim.LineOfBusCode 
//						+ " AND CLAIM.CLAIM_TYPE_CODE = " + objClaim.ClaimTypeCode 
//						+ " AND CLAIM.EVENT_ID = EVENT.EVENT_ID "
//						+ " AND EVENT.DEPT_EID = " + objEvent.DeptEid 
//						+ " AND EVENT.DATE_OF_EVENT = '" + objEvent.DateOfEvent + "'"
//						+ " AND CLAIM.DATE_OF_CLAIM = '" + objClaim.DateOfClaim + "'"
//						+ " AND CLAIM.CLAIM_ID !=" + objClaim.ClaimId;
			
			
			using(DbReader rdr = objClaim.Context.DbConnLookup.ExecuteReader(SQL))
			{
				if(!rdr.Read())
					return false;
				
				//Clear Dupe SysEx node.
				base.ResetSysExData("dupeoverride","");
				eltDupe = this.SysEx.SelectSingleNode("//dupeoverride");
				//Set Info from the Current Claim.
//				this.SafeSetAttribute(eltDupe, "formname","ClaimVA");
//				this.SafeSetAttribute(eltDupe, "claimtype",objClaim.Context.LocalCache.GetCodeDesc(objClaim.ClaimTypeCode));
//				this.SafeSetAttribute(eltDupe, "department",objClaim.Context.LocalCache.GetEntityLastFirstName(objEvent.DeptEid));
//				this.SafeSetAttribute(eltDupe, "eventdate",Conversion.ToDate(objEvent.DateOfEvent).ToShortDateString());
//				this.SafeSetAttribute(eltDupe, "claimdate",Conversion.ToDate(objClaim.DateOfClaim).ToShortDateString());
				
				string sClaimIds = string.Empty;
				//Get Info From Potential Duplicate Matched Claims.
				do
				{
					sClaimIds = sClaimIds + rdr.GetInt("CLAIM_ID").ToString() + ",";	
//					eltClaim = eltDupe.OwnerDocument.CreateElement("claim");
//					this.SafeSetAttribute(eltClaim, "claimid",rdr.GetInt("CLAIM_ID").ToString());
//					this.SafeSetAttribute(eltClaim, "claimnumber",rdr.GetString("CLAIM_NUMBER"));
//					this.SafeSetAttribute(eltClaim, "claimstatus",objClaim.Context.LocalCache.GetCodeDesc(rdr.GetInt("CLAIM_STATUS_CODE")));
//					this.SafeSetAttribute(eltClaim, "eventid",rdr.GetInt("EVENT_ID").ToString());
//					this.SafeSetAttribute(eltClaim, "eventnumber",rdr.GetString("EVENT_NUMBER"));
//					SQL = rdr.GetString("EVENT_DESCRIPTION").Replace('\n',' ').Replace('\r',' ').Replace("'","\'");
//					if(SQL.Length >47)
//						SQL = SQL.Substring(0,47)+"...";
//					this.SafeSetAttribute(eltClaim, "eventdescription",SQL);
//					eltDupe.AppendChild(eltClaim);
				}while(rdr.Read());
				sClaimIds = sClaimIds.Substring(0,sClaimIds.Length - 1);

				sClaimIds = sClaimIds 
					+ ";" + Conversion.ToDate(objEvent.DateOfEvent).ToShortDateString()
					+ ";" + objClaim.Context.LocalCache.GetEntityLastFirstName(objEvent.DeptEid);
				eltDupe.InnerText = sClaimIds;
			}

			//Fetch First Claimant's Name (if available) for each Claim.
			
//			foreach(XmlNode eltTemp in eltDupe.SelectNodes("./claim"))
//			{
//				SQL = 
//					"SELECT ENTITY.LAST_NAME,ENTITY.FIRST_NAME FROM ENTITY,CLAIMANT WHERE " +
//					" ENTITY.ENTITY_ID = CLAIMANT.CLAIMANT_EID AND CLAIMANT.CLAIM_ID = " + eltTemp.Attributes["claimid"].Value;
//				using(DbReader rdr = objClaim.Context.DbConnLookup.ExecuteReader(SQL))
//				{
//					if(!rdr.Read())
//						sTmp = "[none]";
//					else
//						sTmp = rdr.GetString("LAST_NAME").Trim() + " " + rdr.GetString("FIRST_NAME").Trim();
//					this.SafeSetAttribute(eltTemp, "claimant",sTmp.Trim());
//				}
//				
//			}

			return true;
			
		}
		//validate data according to the Business Rules
		public override void OnValidate(ref bool Cancel)
		{
			bool bError = false;
			Event objEvent=objClaim.Parent as Event;
//			PiEmployee objEmp= objClaim.PrimaryPiEmployee as PiEmployee;

            // Perform data validation
            //Time Zone Enhancement for R7
            string sToday = Conversion.ToDbDate(System.DateTime.Now);
            string sTime = Conversion.GetTime(System.DateTime.Now.ToShortTimeString()); // Current server Time
            string sOrgLevelToday = Conversion.ToDbDate(System.DateTime.Now); //By vaibhav : Current OrgLevel Date
            string sOrgLevelTime = string.Empty;
            int iOrgLevelID = 0;
            //int iDateChange = 0; //0-> No Change in date, 1-> Subtract 1 day from date, 2-> Add 1 Day to date
            DateTime dtOrgLevelCurrentDateTime = System.DateTime.Now;//For storing Current Date time of OrgLevel
            //string sOrgLevelCurrentDate = string.Empty; //Current Date in OrgLevel
            string sOrgLevelCurrentTime = string.Empty; //Current Time in OrgLevel
            DateTime sOrgLevelDateTime; //Date Time in Org Level
            DateTime dtTemp = System.DateTime.Now.ToUniversalTime(); //DateTime in GMT
            string sSQL = string.Empty;
            int iTimeZoneTracking = 0; //-1 if Time Zone is enabled 
            //int iDayLightSavings = 0; //-1 if Day Light Savings is enabled
            //bool bDayLightSavings = false; //True for Day Light Savings and vice-versa
            string sTimeZone = string.Empty;//For storing TimeZone of OrgLevel
            int OrgTimeZoneLevel = 0;

            //pmittal5 Mits 18751 09/20/10 - If existing Event Number is used
            int iEventId = 0;
            object oEvent = null;
            bool bEventFound = false;
            //Parag R7 : We will check for Duplicate EventNumber only in case when Generate AutoEventNumber is OFF
            if (!objEvent.EventNumber.Equals(""))
            {
                Dictionary<string, string> dictParams = new Dictionary<string, string>();
                if (objEvent.EventId == 0)
                {

                    //sSQL = String.Format("SELECT EVENT_ID FROM EVENT WHERE EVENT_NUMBER = '{0}'", objEvent.EventNumber);
                    sSQL = String.Format("SELECT EVENT_ID FROM EVENT WHERE EVENT_NUMBER = {0}", "~EVENTNUMBER~");
                    dictParams.Add("EVENTNUMBER", objEvent.EventNumber);
                }
                else
                {
                    //sSQL = String.Format("SELECT EVENT_ID FROM EVENT WHERE EVENT_NUMBER = '{0}' and EVENT_ID <> {1} ", objEvent.EventNumber, objEvent.EventId);
                    sSQL = String.Format("SELECT EVENT_ID FROM EVENT WHERE EVENT_NUMBER = {0} and EVENT_ID <> {1} ", "~EVENTNUMBER~", "~EVENTID~");
                    dictParams.Add("EVENTNUMBER", objEvent.EventNumber);
                    dictParams.Add("EVENTID", objEvent.EventId.ToString());
                }
                //oEvent = objClaim.Context.DbConn.ExecuteScalar(sSQL);//Deb MITS 25141
                oEvent = DbFactory.ExecuteScalar(objEvent.Context.DbConn.ConnectionString, sSQL, dictParams);
                
                if (oEvent != null)
                {
                    iEventId = Conversion.CastToType<int>(oEvent.ToString(), out bEventFound);
                    if (iEventId != 0)
                    {
                        Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                                    String.Format(Globalization.GetString("Validation.EventAlreadyExists",base.ClientId), objEvent.EventNumber.ToString()),
                                    BusinessAdaptorErrorType.Error);//sharishkumar Jira 804
                        bError = true;
                    }
                }
            }
            //End - pmittal5
            //sSQL = @"SELECT ORG_TIMEZONE_LEVEL FROM SYS_PARMS";
            //OrgTimeZoneLevel = Conversion.ConvertStrToInteger(objClaim.Context.DbConn.ExecuteScalar(sSQL).ToString());
            //BOB Enhancement  :  Ukusvaha
            if (objEvent.DeptEid == 0)
            {
                if (objEvent.Context.InternalSettings.SysSettings.AutoPopulateDpt == -1)
                {

                    objEvent.DeptEid = objEvent.Context.InternalSettings.SysSettings.AutoFillDpteid;
                }
            }
            //End BOB Enhancement 
            OrgTimeZoneLevel = objClaim.Context.InternalSettings.SysSettings.OrgTimeZoneLevel;
            //Parag R7 : We will check for Time Validation only in case when Time Validation is ON
            if (OrgTimeZoneLevel != 0)
            {
                sSQL = @"SELECT ";

            switch (OrgTimeZoneLevel)
            {
                case 1005:
                    sSQL = sSQL + "CLIENT_EID";
                    break;
                case 1006:
                    sSQL = sSQL + "COMPANY_EID";
                    break;
                case 1007:
                    sSQL = sSQL + "OPERATION_EID";
                    break;
                case 1008:
                    sSQL = sSQL + "REGION_EID";
                    break;
                case 1009:
                    sSQL = sSQL + "DIVISION_EID";
                    break;
                case 1010:
                    sSQL = sSQL + "LOCATION_EID";
                    break;
                case 1011:
                    sSQL = sSQL + "FACILITY_EID";
                    break;
                default:
                    sSQL = sSQL + "DEPARTMENT_EID";
                    break;
            }

            //BOB Enhancement  :  Ukusvaha
            if (objEvent.DeptEid == 0)
            {
                if (objEvent.Context.InternalSettings.SysSettings.AutoPopulateDpt == -1)
                {

                    objEvent.DeptEid = objEvent.Context.InternalSettings.SysSettings.AutoFillDpteid;
                }
            }
            //End BOB Enhancement 
            sSQL = sSQL + " FROM ORG_HIERARCHY WHERE DEPARTMENT_EID = " + objEvent.DeptEid;

                //END
                iOrgLevelID = Conversion.ConvertStrToInteger(objClaim.Context.DbConn.ExecuteScalar(sSQL).ToString());

                //To get Time Zone of the OrgLevel is enabled or not
                sSQL = @"SELECT TIME_ZONE_TRACKING FROM ENTITY WHERE ENTITY_ID = " + iOrgLevelID.ToString();
                iTimeZoneTracking = Conversion.ConvertObjToInt(objClaim.Context.DbConn.ExecuteScalar(sSQL), base.ClientId);

                if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
                {

                    sSQL = @"SELECT SHORT_CODE FROM CODES WHERE CODE_ID=
                        (SELECT TIME_ZONE_CODE FROM ENTITY WHERE ENTITY_ID=" + iOrgLevelID.ToString() + ")";
                    sTimeZone = objClaim.Context.DbConn.ExecuteScalar(sSQL).ToString();

                //To see Day light saving is enabled or not for OrgLevel
                //sSQL = @"SELECT DAY_LIGHT_SAVINGS FROM ENTITY WHERE ENTITY_ID = " + iOrgLevelID.ToString();
                //iDayLightSavings = Conversion.ConvertObjToInt(objClaim.Context.DbConn.ExecuteScalar(sSQL), base.ClientId);

                //If Daylight savings are enabled
                //if (iDayLightSavings != 0)
                //{
                //    bDayLightSavings = true;
                //}
                //If Daylight savings are disabled
                //else
                //{
                //    bDayLightSavings = false;
                //}

                //Current Time in OrgLevel
                //sOrgLevelTime = Conversion.ConvertGMTToOrgHierarchyTime(sTimeZone, Conversion.ToDbTime(System.DateTime.Now.ToUniversalTime()), bDayLightSavings, ref iDateChange);
                sOrgLevelDateTime = Conversion.ConvertGMTToOrgHierarchyTime(sTimeZone, dtTemp);
                //if (iDateChange != 0)
                //{
                //    if (iDateChange == 1)
                //    {
                //        dtTemp = dtTemp.AddDays(-1);
                //    }
                //    else
                //    {
                //        dtTemp = dtTemp.AddDays(1);
                //    }
                //}
                //Current Date in OrgLevel
                sOrgLevelToday = Conversion.ToDbDate(sOrgLevelDateTime);
                //Date in OrgLevel in mm/dd/yy format for displaying validation error message
                //sOrgLevelCurrentDate = sOrgLevelDateTime.ToShortDateString();
                sOrgLevelTime = Conversion.ToDbTime(sOrgLevelDateTime);
                //Time in OrgLevel for displaying validation error message
                //sOrgLevelCurrentTime = (Conversion.GetDBTimeFormat(sOrgLevelDateTime)).ToString();
                dtOrgLevelCurrentDateTime = sOrgLevelDateTime;
                }
            }

            //Advance claim Validation -- start
            if (objClaim.Context.InternalSettings.SysSettings.MultiCovgPerClm == -1)
            {
                int iRecordCount = 0;
                foreach (int iPolicyId in arrPoliciesDeleted)
                {
                    sSQL = "SELECT COUNT(*) FROM RESERVE_CURRENT, POLICY_X_CVG_TYPE WHERE RESERVE_CURRENT.POLCVG_ROW_ID = POLICY_X_CVG_TYPE.POLCVG_ROW_ID AND RESERVE_CURRENT.CLAIM_ID = " + objClaim.ClaimId + " AND POLICY_X_CVG_TYPE.POLICY_ID = " + iPolicyId;
                    iRecordCount = Conversion.ConvertObjToInt(objClaim.Context.DbConn.ExecuteScalar(sSQL), base.ClientId);
                    if (iRecordCount > 0)
                    {
                        Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                            String.Format(Globalization.GetString("Field.Policies",base.ClientId)),
                            BusinessAdaptorErrorType.Error);//sharishkumar Jira 804

                        bError = true;
                        break;
                    }
                }
            }

            //Validation in case of TimeZone
            if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
            {
                if (objEvent.DateOfEvent.CompareTo(sOrgLevelToday) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                    String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate",base.ClientId), Globalization.GetString("Field.EventDate",base.ClientId), dtOrgLevelCurrentDateTime.ToShortDateString()),
                    BusinessAdaptorErrorType.Error);//sharishkumar Jira 804
                    bError = true;
                }
            }
            //Validation without TimeZone
            else
            {
			if (objEvent.DateOfEvent.CompareTo(sToday) > 0)
			{
				Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
					String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate",base.ClientId), Globalization.GetString("Field.EventDate",base.ClientId), System.DateTime.Now.ToShortDateString()),
                    BusinessAdaptorErrorType.Error);//sharishkumar Jira 804
                    bError = true;
                }
            }

            
            //Validation in case of TimeZone
            if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
            {

                if (objClaim.DateOfClaim.CompareTo(sOrgLevelToday) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                        String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate",base.ClientId), Globalization.GetString("Field.ClaimDate",base.ClientId), dtOrgLevelCurrentDateTime.ToShortDateString()),
                        BusinessAdaptorErrorType.Error);//sharishkumar Jira 804

                    bError = true;
                }
            }
            else
            {
                //Validation without TimeZone
			if (objClaim.DateOfClaim.CompareTo(sToday) > 0)
			{
				Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
					String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate",base.ClientId), Globalization.GetString("Field.ClaimDate",base.ClientId), System.DateTime.Now.ToShortDateString()),
					BusinessAdaptorErrorType.Error);//sharishkumar Jira 804

				bError = true;
			}
            }


            //Validations of the Time Fields present on the claim screen. START

            //Validation for the Time of event reported on the claim screen START
            if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
            {
                if (objEvent.DateReported.CompareTo(sOrgLevelToday) == 0 && objEvent.TimeReported.CompareTo(sOrgLevelTime) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                      String.Format(Globalization.GetString("Field.TimeReported",base.ClientId), dtOrgLevelCurrentDateTime.ToLongTimeString()),
                      BusinessAdaptorErrorType.Error);//sharishkumar Jira 804

                    bError = true;
                }
            }
            else
            {
                if (objEvent.DateReported.CompareTo(sToday) == 0 && objEvent.TimeReported.CompareTo(sTime) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                      String.Format(Globalization.GetString("Field.TimeReported",base.ClientId), System.DateTime.Now.ToLongTimeString()),
                      BusinessAdaptorErrorType.Error);//sharishkumar Jira 804

                    bError = true;
                }
            }
            //Validation for the Time of event reported on the claim screen END

            //Validation for the Time of Claim on the claim screen START
            if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
            {
                if (objClaim.DateOfClaim.CompareTo(sOrgLevelToday) == 0 && objClaim.TimeOfClaim.CompareTo(sOrgLevelTime) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                      String.Format(Globalization.GetString("Field.TimeOfClaim",base.ClientId), dtOrgLevelCurrentDateTime.ToLongTimeString()),
                      BusinessAdaptorErrorType.Error);//sharishkumar Jira 804

                    bError = true;
                }
            }
            else
            {
                if (objClaim.DateOfClaim.CompareTo(sToday) == 0 && objClaim.TimeOfClaim.CompareTo(sTime) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                      String.Format(Globalization.GetString("Field.TimeOfClaim",base.ClientId), System.DateTime.Now.ToLongTimeString()),
                      BusinessAdaptorErrorType.Error);//sharishkumar Jira 804

                    bError = true;
                }
            }

            if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
            {
                if (objEvent.DateOfEvent.CompareTo(sOrgLevelToday) == 0 && objEvent.TimeOfEvent.CompareTo(sOrgLevelTime) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                      String.Format(Globalization.GetString("Field.TimeOfEvent",base.ClientId), dtOrgLevelCurrentDateTime.ToLongTimeString()),
                      BusinessAdaptorErrorType.Error);//sharishkumar Jira 804

                    bError = true;
                }
            }
            else
            {
                if (objEvent.DateOfEvent.CompareTo(sToday) == 0 && objEvent.TimeOfEvent.CompareTo(sTime) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                      String.Format(Globalization.GetString("Field.TimeOfEvent",base.ClientId), System.DateTime.Now.ToLongTimeString()),
                      BusinessAdaptorErrorType.Error);//sharishkumar Jira 804

                    bError = true;
                }
            }

            //Validations of the Time Fields present on the claim screen. END
			if (objClaim.DateOfClaim.CompareTo(objEvent.DateOfEvent) < 0)
			{
				Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
					String.Format(Globalization.GetString("Validation.MustBeGreaterThanEventDate",base.ClientId), Globalization.GetString("Field.ClaimDate",base.ClientId), Conversion.ToDate(objEvent.DateOfEvent).ToShortDateString()),
                    BusinessAdaptorErrorType.Error);//sharishkumar Jira 804

				bError = true;
			}

            if (objEvent.DateOfEvent.CompareTo(objClaim.DateOfClaim) == 0 && objClaim.TimeOfClaim.CompareTo(objEvent.TimeOfEvent) < 0)
            {
                Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                    String.Format(Globalization.GetString("Validation.MustBeGreaterThanEventTime",base.ClientId), Globalization.GetString("Field.ClaimTime",base.ClientId), Conversion.ToDate(objEvent.TimeOfEvent).ToLongTimeString()),
                    BusinessAdaptorErrorType.Error);//sharishkumar Jira 804

                bError = true;
            }

			if (objClaim.DttmClosed!=string.Empty)
			{
				//Validation in case of TimeZone
                if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
                {
                    //nsachdeva2 - MITS: 28069 - 4/4/2012
                    //if (objClaim.DttmClosed.Substring(0, 8).CompareTo(sOrgLevelToday) > 0 &&
                    if (Conversion.ToDbDate(Conversion.ConvertGMTToOrgHierarchyTime(sTimeZone, Conversion.ToDate(objClaim.DttmClosed))).CompareTo(sOrgLevelToday) > 0 &&
                        objData.Context.InternalSettings.CacheFunctions.GetShortCode(objData.Context.InternalSettings.CacheFunctions.GetRelatedCodeID(objClaim.ClaimStatusCode)) == "C")
                    {
                        Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                            String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate",base.ClientId), Globalization.GetString("Field.DttmClosed",base.ClientId), dtOrgLevelCurrentDateTime.ToShortDateString()),
                            BusinessAdaptorErrorType.Error);//sharishkumar Jira 804

                        bError = true;
                    }
                }
                //Validation without TimeZone
                else
                {
                    if (objClaim.DttmClosed.Substring(0, 8).CompareTo(sToday) > 0 &&
                        objData.Context.InternalSettings.CacheFunctions.GetShortCode(objData.Context.InternalSettings.CacheFunctions.GetRelatedCodeID(objClaim.ClaimStatusCode)) == "C")
                    {
                        Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                            String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate",base.ClientId), Globalization.GetString("Field.DttmClosed",base.ClientId), System.DateTime.Now.ToShortDateString()),
                            BusinessAdaptorErrorType.Error);//sharishkumar Jira 804

                        bError = true;
                    }
                }
				if(objClaim.DttmClosed.Substring(0,8).CompareTo(objClaim.DateOfClaim) < 0 &&
					objData.Context.InternalSettings.CacheFunctions.GetShortCode(objData.Context.InternalSettings.CacheFunctions.GetRelatedCodeID(objClaim.ClaimStatusCode))=="C")
				{
					Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
						String.Format(Globalization.GetString("Validation.MustBeGreaterThanClaimDate",base.ClientId), Globalization.GetString("Field.DttmClosed",base.ClientId), Conversion.ToDate(objClaim.DateOfClaim).ToShortDateString()),
                        BusinessAdaptorErrorType.Error);//sharishkumar Jira 804

					bError = true;
				}
			}

			if (objEvent.DateReported!=string.Empty) 
			{
                
                //Validation in case of TimeZone
                if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
                {

                    if (objEvent.DateReported.CompareTo(sOrgLevelToday) > 0)
                    {
                        Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                            String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate",base.ClientId), Globalization.GetString("Field.DateReported",base.ClientId), dtOrgLevelCurrentDateTime.ToShortDateString()),
                            BusinessAdaptorErrorType.Error);//sharishkumar Jira 804

                        bError = true;
                    }
                }
                //Validation without TimeZone
                else
                {
				if (objEvent.DateReported.CompareTo(sToday) > 0)
				{
					Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
						String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate",base.ClientId), Globalization.GetString("Field.DateReported",base.ClientId), System.DateTime.Now.ToShortDateString()),
						BusinessAdaptorErrorType.Error);//sharishkumar Jira 804

					bError = true;
                    }
				}

				if (objEvent.DateReported.CompareTo(objEvent.DateOfEvent) < 0)
				{
					Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
						String.Format(Globalization.GetString("Validation.MustBeGreaterThanEventDate",base.ClientId), Globalization.GetString("Field.DateReported",base.ClientId), Conversion.ToDate(objEvent.DateOfEvent).ToShortDateString()),
						BusinessAdaptorErrorType.Error);//sharishkumar Jira 804

					bError = true;
				}

				//Nikhil Garg		Dated: 23-Dec-05	Bug No:1069
				//Checking if the TimeReported is Empty before comparing it with EventTime
				if (objEvent.DateOfEvent.CompareTo(objEvent.DateReported) == 0 && objEvent.TimeReported!=string.Empty && objEvent.TimeReported.CompareTo(objEvent.TimeOfEvent) < 0)
				{
					Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThanEventTime",base.ClientId), Globalization.GetString("Field.EventTimeReported",base.ClientId), Conversion.ToDate(objEvent.DateOfEvent + objEvent.TimeOfEvent).ToLongTimeString()),
                        BusinessAdaptorErrorType.Error);//sharishkumar Jira 804

					bError = true;
				}
			}
			if(objClaim.DateFddotRpt!= String.Empty)
			{
                
                //Validation in case of TimeZone
                if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
                {
                    if (objClaim.DateFddotRpt.CompareTo(sOrgLevelToday) > 0)
                    {
                        Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                            String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate",base.ClientId), Globalization.GetString("Field.DateFddotRpt",base.ClientId), dtOrgLevelCurrentDateTime.ToShortDateString()),
                            BusinessAdaptorErrorType.Error);//sharishkumar Jira 804

                        bError = true;
                    }
                }
                //Validation without Timezone
                else
                {
				if (objClaim.DateFddotRpt.CompareTo(sToday) > 0)
				{
					Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
						String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate",base.ClientId), Globalization.GetString("Field.DateFddotRpt",base.ClientId), System.DateTime.Now.ToShortDateString()),
                        BusinessAdaptorErrorType.Error);//sharishkumar Jira 804

					bError = true;
                    }
				}
				if (objClaim.DateFddotRpt.CompareTo(objEvent.DateOfEvent) < 0)
				{
					Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
						String.Format(Globalization.GetString("Validation.MustBeGreaterThanEventDate",base.ClientId), Globalization.GetString("Field.DateFddotRpt",base.ClientId), Conversion.ToDate(objEvent.DateOfEvent).ToShortDateString()),
                        BusinessAdaptorErrorType.Error);//sharishkumar Jira 804

					bError = true;
				}
			}
			if(objClaim.DateStdotRpt!= String.Empty)
			{
                
                //Validation in case of TimeZone
                if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
                {
                    if (objClaim.DateStdotRpt.CompareTo(sOrgLevelToday) > 0)
                    {
                        Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                            String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate",base.ClientId), Globalization.GetString("Field.DateStdotRpt",base.ClientId), dtOrgLevelCurrentDateTime.ToShortDateString()),
                            BusinessAdaptorErrorType.Error);//sharishkumar Jira 804

                        bError = true;
                    }
                }
                //Validation without Timezone
                else
                {
				if (objClaim.DateStdotRpt.CompareTo(sToday) > 0)
				{
					Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
						String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate",base.ClientId), Globalization.GetString("Field.DateStdotRpt",base.ClientId), System.DateTime.Now.ToShortDateString()),
                        BusinessAdaptorErrorType.Error);//sharishkumar Jira 804

					bError = true;
                    }
				}
				if (objClaim.DateStdotRpt.CompareTo(objEvent.DateOfEvent) < 0)
				{
					Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
						String.Format(Globalization.GetString("Validation.MustBeGreaterThanEventDate",base.ClientId), Globalization.GetString("Field.DateStdotRpt",base.ClientId), Conversion.ToDate(objEvent.DateOfEvent).ToShortDateString()),
                        BusinessAdaptorErrorType.Error);//sharishkumar Jira 804

					bError = true;
				}
			}
            if (objClaim.DateRptdToRm != string.Empty)
            {
                
                //Validation in case of TimeZone
                if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
                {
                    if (objClaim.DateRptdToRm.CompareTo(sOrgLevelToday) > 0)
                    {
                        Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                            String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate",base.ClientId), Globalization.GetString("Field.DateReported",base.ClientId), dtOrgLevelCurrentDateTime.ToShortDateString()),
                            BusinessAdaptorErrorType.Error);//sharishkumar Jira 804

                        bError = true;
                    }
                }
                else
                {
                if (objClaim.DateRptdToRm.CompareTo(sToday) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                        String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate",base.ClientId), Globalization.GetString("Field.DateReported",base.ClientId), System.DateTime.Now.ToShortDateString()),
                        BusinessAdaptorErrorType.Error);//sharishkumar Jira 804

                    bError = true;
                }
                }
                //if (objClaim.DateRptdToRm.CompareTo(objEvent.DateOfEvent) < 0)
                //{
                //    Errors.Add(Globalization.GetString("ValidationError"),
                //        String.Format(Globalization.GetString("Validation.MustBeGreaterThanEventDate"), Globalization.GetString("Field.DateReported"), Conversion.ToDate(objEvent.DateOfEvent).ToShortDateString()),
                //        BusinessAdaptorErrorType.Error);

                //    bError = true;
                //}
                if (objClaim.DateRptdToRm.CompareTo(objClaim.DateOfClaim) < 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThanClaimDate",base.ClientId), Globalization.GetString("Field.DateReported",base.ClientId), Conversion.ToDate(objClaim.DateOfClaim).ToShortDateString()),
                        BusinessAdaptorErrorType.Error);//sharishkumar Jira 804

                    bError = true;
                }
            }
            //Charanpreet for MITS 12174 : Start
            if (objEvent.DeptEid > 0)
            {
                bool bValerror = false;
                bValerror = objClaim.OrgHierarchyValidate();
                if (bValerror)
                {
                    Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                        "Selected Department Not Within Effective Date Range",
                        BusinessAdaptorErrorType.Error);//sharishkumar Jira 804
                    bError = true;
                }
            }
            //Charanpreet for Mits 12174 : End

            // End MITS 25163- Policy Interface Implementation 

            // rrachev JIRA 5121 Begin
            if (bCloseDiaries)
            {
                string sContainsOpenDiariesOnFormOpen = GetSysExDataNodeText("ContainsOpenDiaries");
                if (sContainsOpenDiariesOnFormOpen == "false" && HasOpenDiaries)
                {
                    this.ResetSysExData("ContainsOpenDiaries", "true");
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId), Globalization.GetString("Validation.DiariesWereOpening", base.ClientId), BusinessAdaptorErrorType.Error);
                    bError = true;
                }
            }
            // rrachev JIRA 5121 End

			// Return true if there were validation errors
			Cancel = bError;
		}

		// Mihika 16-Jan-2006 Defect. 1232 AutoUpdatePolicy Functionality
		// This is the same query implementation as it in Riskmaster.Application.ClaimLookups.LookupClaimPolicy class
		// private void AutoFillPolicy(ref int p_iPolicyId)
        //added by rkaur7 on 05/27/2009 - MITS 16668 
        //NumPolicy added in the parameter and number of policy satisfying Auto Select Criteria captured in the variable
		private void AutoFillPolicy(ref int p_iPolicyId ,ref int p_iNumPolicy)
		{
            string sSQL = CreateSqlForSelectingPolicy(p_iPolicyId, false);
            //using (DbReader objReader = objClaim.Context.DbConnLookup.ExecuteReader(sSQL))//R5 PS2 Merge
            DataSet objDataSet = null;
            DataRow[] objRow = null;
            objDataSet = DbFactory.GetDataSet(objClaim.Context.DbConn.ConnectionString, sSQL,base.ClientId);//sharishkumar Jira 804
            objRow = objDataSet.Tables[0].Select();

            p_iNumPolicy = objDataSet.Tables[0].Rows.Count;
            if (p_iNumPolicy != 0)
            {
                p_iPolicyId = Conversion.ConvertObjToInt(objRow[0]["POLICY_ID"], base.ClientId);
                //if (objReader.Read())
                //{
                //    p_iPolicyId = objReader.GetInt("POLICY_ID");
                //
                //}
            }
            if (objDataSet != null)
                objDataSet = null;
            if (objRow != null)
                objRow = null;
        }

        private string CreateSqlForSelectingPolicy(int p_iPolicyId, bool p_bIncludePolicyID)
        {
			string sSQL = string.Empty;
			string sSQLSelect=string.Empty;
			string sSQLFrom=string.Empty;
			string sSQLWhere=string.Empty;
			string sSQLOrder=string.Empty;

			int iCN_POLICY_REVOKED=0;
			int iCN_POLICY_IN_EFFECT=0;
			int iCN_POLICY_EXPIRED=0;
			int iCN_POLICY_CANCELED=0;
			int iCoverageTypeCode=0;
            int iClaimTypeCode = 0;
			string sGetOrgForDept=string.Empty;
            int iJurisdictionId = 0; // Ishan for MITS 27322
            int iPolicyDepartment = 0; //Added by Sumit Agarwal, 08/05/2014, MITS 33588

			const string  SC_POLICY_REVOKED = "R";
			const string SC_POLICY_IN_EFFECT = "I";
			const string SC_POLICY_EXPIRED = "E";
			const string SC_POLICY_CANCELED = "C";

			Event objEvent = (objClaim.Parent as Event);
            iJurisdictionId = objClaim.FilingStateId; // Ishan for MITS 27322

			iCN_POLICY_REVOKED = objCache.GetCodeId(SC_POLICY_REVOKED,"POLICY_STATUS");
			iCN_POLICY_IN_EFFECT = objCache.GetCodeId(SC_POLICY_IN_EFFECT, "POLICY_STATUS");
			iCN_POLICY_EXPIRED = objCache.GetCodeId(SC_POLICY_EXPIRED, "POLICY_STATUS");
			iCN_POLICY_CANCELED = objCache.GetCodeId(SC_POLICY_CANCELED, "POLICY_STATUS");
            iClaimTypeCode = objClaim.ClaimTypeCode;
            iCoverageTypeCode = objCache.GetRelatedCodeId(iClaimTypeCode);

            //Start: Added by Sumit Agarwal, 08/05/2014, MITS 33588
            if (objClaim.Context.InternalSettings.SysSettings.UseInsuredClaimDept == -1)
            {
                iPolicyDepartment = objClaim.InsuredClaimDeptEid;
            }
            else
            {
                iPolicyDepartment = objEvent.DeptEid;
            }
            //Start: Change the following code by Sumit Agarwal: 10/08/2014: MITS 33588
            //sGetOrgForDept = GetOrgListForDept(iPolicyDepartment);
            if (iPolicyDepartment == 0)
            {
                sGetOrgForDept = "0";
            }
            else
            {
                sGetOrgForDept = GetOrgListForDept(iPolicyDepartment);
            }
            //End: Change the following code by Sumit Agarwal: 10/08/2014: MITS 33588
            //End: Added by Sumit Agarwal, 08/05/2014, MITS 33588
            //Mukul Added 5/29/07 MITS 9424
            //if (objClaim.Context.InternalSettings.SysSettings.UseEnhPolFlag == -1)
            if (objLobSettings.UseEnhPolFlag == -1)
            //End:Nitin Goel,Check for specific LOB. 01/13/10: MITS#18229
            {
                //Build select for looking up policies from enhanced policy system for this claim
                //Start: Change the following code by Sumit Agarwal to only select the Policy_ID as this is only required: 10/01/2014: MITS 33588
                //sSQLSelect = "SELECT POLICY_ENH.POLICY_ID,POLICY_X_TERM_ENH.POLICY_NUMBER,POLICY_X_CVG_ENH.NEXT_POLICY_ID,POLICY_ENH.POLICY_NAME,POLICY_X_TERM_ENH.EFFECTIVE_DATE,POLICY_X_TERM_ENH.EXPIRATION_DATE,POLICY_X_CVG_ENH.OCCURRENCE_LIMIT";
                //sSQLSelect = sSQLSelect + ",POLICY_X_CVG_ENH.POLICY_LIMIT,POLICY_X_CVG_ENH.TOTAL_PAYMENTS,ENTITY.LAST_NAME";
                //sSQLSelect = sSQLSelect + ",POLICY_X_TERM_ENH.CANCEL_DATE,POLICY_X_TERM_ENH.REINSTATEMENT_DATE,POLICY_X_TERM_ENH.SEQUENCE_ALPHA,POLICY_X_TERM_ENH.TERM_NUMBER,POLICY_ENH.TRIGGER_CLAIM_FLAG";
                sSQLSelect = "SELECT DISTINCT POLICY_ENH.POLICY_ID ";
                //End: Change the following code by Sumit Agarwal to only select the Policy_ID as this is only required: 10/01/2014: MITS 33588
                sSQLFrom = " FROM POLICY_ENH, POLICY_X_TERM_ENH, POLICY_X_CVG_ENH, POLICY_X_INSRD_ENH, ENTITY";
                sSQLWhere = " WHERE (POLICY_ENH.POLICY_ID = POLICY_X_INSRD_ENH.POLICY_ID";
                sSQLWhere = sSQLWhere + " AND POLICY_ENH.POLICY_ID = POLICY_X_CVG_ENH.POLICY_ID AND POLICY_ENH.INSURER_EID = ENTITY.ENTITY_ID";
                sSQLWhere = sSQLWhere + " AND POLICY_ENH.POLICY_ID = POLICY_X_TERM_ENH.POLICY_ID AND POLICY_ENH.POLICY_ID = POLICY_X_INSRD_ENH.POLICY_ID)";

                sSQLWhere = sSQLWhere + " AND (POLICY_X_CVG_ENH.COVERAGE_TYPE_CODE IN (" + iCoverageTypeCode + ", " + iClaimTypeCode + ")";
                sSQLWhere = sSQLWhere + " AND POLICY_X_INSRD_ENH.INSURED_EID IN (" + sGetOrgForDept + "))";
                sSQLWhere = sSQLWhere + " AND ((POLICY_X_TERM_ENH.EFFECTIVE_DATE <= '" + objEvent.DateOfEvent + "'";
                sSQLWhere = sSQLWhere + " AND POLICY_X_TERM_ENH.EXPIRATION_DATE >= '" + objEvent.DateOfEvent + "'";
                sSQLWhere = sSQLWhere + " AND POLICY_ENH.TRIGGER_CLAIM_FLAG = 0)";
                sSQLWhere = sSQLWhere + " OR (POLICY_X_TERM_ENH.EFFECTIVE_DATE <= '" + objClaim.DateOfClaim + "'";
                sSQLWhere = sSQLWhere + " AND POLICY_X_TERM_ENH.EXPIRATION_DATE >= '" + objClaim.DateOfClaim + "'";
                sSQLWhere = sSQLWhere + " AND POLICY_ENH.TRIGGER_CLAIM_FLAG <> 0))";
                sSQLWhere = sSQLWhere + " AND (POLICY_ENH.POLICY_INDICATOR = " + objCache.GetCodeId("P", "POLICY_INDICATOR") + ")";
                sSQLWhere = sSQLWhere + " AND PRIMARY_POLICY_FLG <> 0 ";
                if (p_bIncludePolicyID)
                    sSQLWhere = sSQLWhere + " AND POLICY_ENH.POLICY_ID =" + p_iPolicyId;
                //sSQLOrder = " ORDER BY POLICY_NUMBER ";       //Commented by Sumit Agarwal as we are only selecting the Policy_Id: 10/01/2014: MITS 33588
            }
            //else//R5 PS2 merge
            //{//R5 PS2 merge
            //R5 PS2 merge
            //    sSQLSelect = "SELECT POLICY.POLICY_ID,POLICY_NUMBER,POLICY_X_CVG_TYPE.NEXT_POLICY_ID,POLICY_NAME,EFFECTIVE_DATE,EXPIRATION_DATE,OCCURRENCE_LIMIT,POLICY_LIMIT,TOTAL_PAYMENTS,ENTITY.LAST_NAME";//R5 PS2 merge

            //    sSQLFrom = " FROM POLICY, POLICY_X_CVG_TYPE, POLICY_X_INSURED, ENTITY";//R5 PS2 merge
            //rkaur7- It works based on utility setting of Show Jursidiction and Event State radio button
            else if (objClaim.Context.InternalSettings.SysSettings.ShowJurisdiction != 0 && objClaim.Context.InternalSettings.SysSettings.StateEval == 0)
            {
                //Condition added by rkaur7 to check whether Location state field is empty or not - MITS 16668
                if (objEvent.StateId == 0)
                {
                    //Start: Change the following code by Sumit Agarwal to only select the Policy_ID as this is only required: 10/01/2014: MITS 33588
                    //sSQLSelect = "SELECT DISTINCT(POLICY.POLICY_ID),POLICY_NUMBER,POLICY_X_CVG_TYPE.NEXT_POLICY_ID,POLICY_NAME,EFFECTIVE_DATE,EXPIRATION_DATE,OCCURRENCE_LIMIT,POLICY_LIMIT,TOTAL_PAYMENTS,ENTITY.LAST_NAME";
                    sSQLSelect = "SELECT DISTINCT POLICY.POLICY_ID ";
                    //End: Change the following code by Sumit Agarwal to only select the Policy_ID as this is only required: 10/01/2014: MITS 33588

                    sSQLFrom = " FROM POLICY, POLICY_X_CVG_TYPE, POLICY_X_INSURED, ENTITY ";
                    sSQLWhere = " WHERE (POLICY.POLICY_ID = POLICY_X_INSURED.POLICY_ID";
                    sSQLWhere = sSQLWhere + " AND POLICY.POLICY_ID = POLICY_X_CVG_TYPE.POLICY_ID AND POLICY.INSURER_EID = ENTITY.ENTITY_ID)";

                    sSQLWhere = sSQLWhere + " AND (POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE = " + iCoverageTypeCode;
                    sSQLWhere = sSQLWhere + "      AND POLICY_X_INSURED.INSURED_EID IN (" + sGetOrgForDept + "))";

                    sSQLWhere = sSQLWhere + " AND ((POLICY.POLICY_STATUS_CODE IN (" + iCN_POLICY_IN_EFFECT + "," + iCN_POLICY_EXPIRED + "))";
                    sSQLWhere = sSQLWhere + "      OR (POLICY.POLICY_STATUS_CODE = " + iCN_POLICY_CANCELED + " ";
                    sSQLWhere = sSQLWhere + "          AND POLICY.CANCEL_DATE > '" + objClaim.DateOfClaim + "'";
                    sSQLWhere = sSQLWhere + "          AND POLICY.TRIGGER_CLAIM_FLAG <> 0)";
                    sSQLWhere = sSQLWhere + "      OR (POLICY.POLICY_STATUS_CODE = " + iCN_POLICY_CANCELED;
                    sSQLWhere = sSQLWhere + "          AND POLICY.CANCEL_DATE > '" + objEvent.DateOfEvent + "'";
                    sSQLWhere = sSQLWhere + "          AND POLICY.TRIGGER_CLAIM_FLAG = 0))";
                    sSQLWhere = sSQLWhere + " AND ((POLICY.EFFECTIVE_DATE <= '" + objEvent.DateOfEvent + "'";
                    sSQLWhere = sSQLWhere + "       AND POLICY.EXPIRATION_DATE >= '" + objEvent.DateOfEvent + "'";
                    sSQLWhere = sSQLWhere + "       AND POLICY.TRIGGER_CLAIM_FLAG = 0)";
                    sSQLWhere = sSQLWhere + "      OR (POLICY.EFFECTIVE_DATE <= '" + objClaim.DateOfClaim + "'";
                    sSQLWhere = sSQLWhere + "          AND POLICY.EXPIRATION_DATE >= '" + objClaim.DateOfClaim + "'";
                    sSQLWhere = sSQLWhere + "          AND POLICY.TRIGGER_CLAIM_FLAG <> 0))";
                    sSQLWhere = sSQLWhere + " AND PRIMARY_POLICY_FLG <> 0 ";

                    if (p_bIncludePolicyID)
                        sSQLWhere = sSQLWhere + " AND POLICY.POLICY_ID =" + p_iPolicyId;

                    //sSQLOrder = " ORDER BY POLICY_NUMBER ";       //Commented by Sumit Agarwal as we are only selecting the Policy_Id: 10/01/2014: MITS 33588
                }
                //end
                else
                {
                    //Start: Change the following code by Sumit Agarwal to only select the Policy_ID as this is only required: 10/01/2014: MITS 33588
                    //sSQLSelect = "SELECT DISTINCT(POLICY.POLICY_ID),POLICY_NUMBER,POLICY_X_CVG_TYPE.NEXT_POLICY_ID,POLICY_NAME,EFFECTIVE_DATE,EXPIRATION_DATE,OCCURRENCE_LIMIT,POLICY_LIMIT,TOTAL_PAYMENTS,ENTITY.LAST_NAME";
                    sSQLSelect = "SELECT DISTINCT POLICY.POLICY_ID ";
                    //End: Change the following code by Sumit Agarwal to only select the Policy_ID as this is only required: 10/01/2014: MITS 33588

                    //modified by rkaur7- added POLICY_X_STATE table to query on 05/27/2009 - MITS 16668
                    sSQLFrom = " FROM POLICY, POLICY_X_CVG_TYPE, POLICY_X_INSURED, ENTITY,POLICY_X_STATE";
                    //end rkaur7
                    sSQLWhere = " WHERE (POLICY.POLICY_ID = POLICY_X_INSURED.POLICY_ID";
                    sSQLWhere = sSQLWhere + " AND POLICY.POLICY_ID = POLICY_X_CVG_TYPE.POLICY_ID AND POLICY.INSURER_EID = ENTITY.ENTITY_ID)";

                    sSQLWhere = sSQLWhere + " AND (POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE = " + iCoverageTypeCode;
                    sSQLWhere = sSQLWhere + "      AND POLICY_X_INSURED.INSURED_EID IN (" + sGetOrgForDept + "))";

                    sSQLWhere = sSQLWhere + " AND ((POLICY.POLICY_STATUS_CODE IN (" + iCN_POLICY_IN_EFFECT + "," + iCN_POLICY_EXPIRED + "))";
                    sSQLWhere = sSQLWhere + "      OR (POLICY.POLICY_STATUS_CODE = " + iCN_POLICY_CANCELED + " ";
                    sSQLWhere = sSQLWhere + "          AND POLICY.CANCEL_DATE > '" + objClaim.DateOfClaim + "'";
                    sSQLWhere = sSQLWhere + "          AND POLICY.TRIGGER_CLAIM_FLAG <> 0)";
                    sSQLWhere = sSQLWhere + "      OR (POLICY.POLICY_STATUS_CODE = " + iCN_POLICY_CANCELED;
                    sSQLWhere = sSQLWhere + "          AND POLICY.CANCEL_DATE > '" + objEvent.DateOfEvent + "'";
                    sSQLWhere = sSQLWhere + "          AND POLICY.TRIGGER_CLAIM_FLAG = 0))";
                    sSQLWhere = sSQLWhere + " AND ((POLICY.EFFECTIVE_DATE <= '" + objEvent.DateOfEvent + "'";
                    sSQLWhere = sSQLWhere + "       AND POLICY.EXPIRATION_DATE >= '" + objEvent.DateOfEvent + "'";
                    sSQLWhere = sSQLWhere + "       AND POLICY.TRIGGER_CLAIM_FLAG = 0)";
                    sSQLWhere = sSQLWhere + "      OR (POLICY.EFFECTIVE_DATE <= '" + objClaim.DateOfClaim + "'";
                    sSQLWhere = sSQLWhere + "          AND POLICY.EXPIRATION_DATE >= '" + objClaim.DateOfClaim + "'";
                    sSQLWhere = sSQLWhere + "          AND POLICY.TRIGGER_CLAIM_FLAG <> 0))";
                    sSQLWhere = sSQLWhere + " AND PRIMARY_POLICY_FLG <> 0 ";
                    if (p_bIncludePolicyID)
                        sSQLWhere = sSQLWhere + " AND POLICY.POLICY_ID =" + p_iPolicyId;

                    //added by rkaur7 on 05/27/2009 MITS 16668
                    sSQLWhere = sSQLWhere + " AND POLICY.POLICY_ID = POLICY_X_STATE.POLICY_ID ";
                    sSQLWhere = sSQLWhere + " AND POLICY_X_STATE.STATE_ID =" + objEvent.StateId + "";
                    //code end rkaur7
                    //sSQLOrder = " ORDER BY POLICY_NUMBER ";       //Commented by Sumit Agarwal as we are only selecting the Policy_Id: 10/01/2014: MITS 33588
                }
            }
            // MITS 27322 : Ishan : Check the jurisdiction state for policy when utility button is checked.
            else if (objClaim.Context.InternalSettings.SysSettings.ShowJurisdiction != 0 && objClaim.Context.InternalSettings.SysSettings.StateEval == 1)
            {
                //Start: Change the following code by Nitin Goel to only select the Policy_ID as this is only required: 10/01/2014: MITS 33588
                //sSQLSelect = "SELECT DISTINCT(POLICY.POLICY_ID),POLICY_NUMBER,POLICY_X_CVG_TYPE.NEXT_POLICY_ID,POLICY_NAME,POLICY.EFFECTIVE_DATE,POLICY.EXPIRATION_DATE,OCCURRENCE_LIMIT,POLICY_LIMIT,TOTAL_PAYMENTS,ENTITY.LAST_NAME";
                sSQLSelect = "SELECT DISTINCT(POLICY.POLICY_ID) ";
                //end:Nitin Goel
                sSQLFrom = " FROM POLICY, POLICY_X_CVG_TYPE, POLICY_X_INSURED, ENTITY,POLICY_X_STATE"; //modified by rkaur7 on 06/04/2009-added POLICY_X_STATE table to query-  MITS 16668

                sSQLWhere = sSQLWhere + " WHERE (POLICY.POLICY_ID = POLICY_X_INSURED.POLICY_ID";
                sSQLWhere = sSQLWhere + " AND POLICY.POLICY_ID = POLICY_X_CVG_TYPE.POLICY_ID AND POLICY.INSURER_EID = ENTITY.ENTITY_ID)";

                sSQLWhere = sSQLWhere + " AND (POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE = " + iCoverageTypeCode;
                sSQLWhere = sSQLWhere + "      AND POLICY_X_INSURED.INSURED_EID IN (" + sGetOrgForDept + "))";

                sSQLWhere = sSQLWhere + " AND ((POLICY.POLICY_STATUS_CODE IN (" + iCN_POLICY_IN_EFFECT + "," + iCN_POLICY_EXPIRED + "))";
                sSQLWhere = sSQLWhere + "      OR (POLICY.POLICY_STATUS_CODE = " + iCN_POLICY_CANCELED + " ";
                sSQLWhere = sSQLWhere + "          AND POLICY.CANCEL_DATE > '" + objClaim.DateOfClaim + "'";
                sSQLWhere = sSQLWhere + "          AND POLICY.TRIGGER_CLAIM_FLAG <> 0)";
                sSQLWhere = sSQLWhere + "      OR (POLICY.POLICY_STATUS_CODE = " + iCN_POLICY_CANCELED;
                sSQLWhere = sSQLWhere + "          AND POLICY.CANCEL_DATE > '" + objEvent.DateOfEvent + "'";
                sSQLWhere = sSQLWhere + "          AND POLICY.TRIGGER_CLAIM_FLAG = 0))";
                sSQLWhere = sSQLWhere + " AND ((POLICY.EFFECTIVE_DATE <= '" + objEvent.DateOfEvent + "'";
                sSQLWhere = sSQLWhere + "       AND POLICY.EXPIRATION_DATE >= '" + objEvent.DateOfEvent + "'";
                sSQLWhere = sSQLWhere + "       AND POLICY.TRIGGER_CLAIM_FLAG = 0)";
                sSQLWhere = sSQLWhere + "      OR (POLICY.EFFECTIVE_DATE <= '" + objClaim.DateOfClaim + "'";
                sSQLWhere = sSQLWhere + "          AND POLICY.EXPIRATION_DATE >= '" + objClaim.DateOfClaim + "'";
                sSQLWhere = sSQLWhere + "          AND POLICY.TRIGGER_CLAIM_FLAG <> 0))";
                sSQLWhere = sSQLWhere + " AND PRIMARY_POLICY_FLG <> 0 ";
                //added by rkaur7 on 06/04/2009-  MITS 16668 - filter for jurisdiction also added in auto select policy

                sSQLWhere = sSQLWhere + " AND POLICY.POLICY_ID = POLICY_X_STATE.POLICY_ID ";
                if (objClaim.FilingStateId != 0)
                {
                    sSQLWhere = sSQLWhere + " AND POLICY_X_STATE.STATE_ID =" + objClaim.FilingStateId + " ";

                }
                

                if (p_bIncludePolicyID)
                    sSQLWhere = sSQLWhere + " AND POLICY.POLICY_ID =" + p_iPolicyId;

                //sSQLOrder = " ORDER BY POLICY_NUMBER ";////Commented by Nitin Goel as we are only selecting the Policy_Id: 10/01/2014: MITS 33588
            }
            //Ishan : end



            else
            {
                //Start: Change the following code by Sumit Agarwal to only select the Policy_ID as this is only required: 10/01/2014: MITS 33588
                //sSQLSelect = "SELECT DISTINCT(POLICY.POLICY_ID),POLICY_NUMBER,POLICY_X_CVG_TYPE.NEXT_POLICY_ID,POLICY_NAME,EFFECTIVE_DATE,EXPIRATION_DATE,OCCURRENCE_LIMIT,POLICY_LIMIT,TOTAL_PAYMENTS,ENTITY.LAST_NAME";
                sSQLSelect = "SELECT DISTINCT POLICY.POLICY_ID ";
                //End: Change the following code by Sumit Agarwal to only select the Policy_ID as this is only required: 10/01/2014: MITS 33588

                sSQLFrom = " FROM POLICY, POLICY_X_CVG_TYPE, POLICY_X_INSURED, ENTITY ";

                sSQLWhere = " WHERE (POLICY.POLICY_ID = POLICY_X_INSURED.POLICY_ID";
                sSQLWhere = sSQLWhere + " AND POLICY.POLICY_ID = POLICY_X_CVG_TYPE.POLICY_ID AND POLICY.INSURER_EID = ENTITY.ENTITY_ID)";

                sSQLWhere = sSQLWhere + " AND (POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE = " + iCoverageTypeCode;
                sSQLWhere = sSQLWhere + "      AND POLICY_X_INSURED.INSURED_EID IN (" + sGetOrgForDept + "))";

                sSQLWhere = sSQLWhere + " AND ((POLICY.POLICY_STATUS_CODE IN (" + iCN_POLICY_IN_EFFECT + "," + iCN_POLICY_EXPIRED + "))";
                sSQLWhere = sSQLWhere + "      OR (POLICY.POLICY_STATUS_CODE = " + iCN_POLICY_CANCELED + " ";
                sSQLWhere = sSQLWhere + "          AND POLICY.CANCEL_DATE > '" + objClaim.DateOfClaim + "'";
                sSQLWhere = sSQLWhere + "          AND POLICY.TRIGGER_CLAIM_FLAG <> 0)";
                sSQLWhere = sSQLWhere + "      OR (POLICY.POLICY_STATUS_CODE = " + iCN_POLICY_CANCELED;
                sSQLWhere = sSQLWhere + "          AND POLICY.CANCEL_DATE > '" + objEvent.DateOfEvent + "'";
                sSQLWhere = sSQLWhere + "          AND POLICY.TRIGGER_CLAIM_FLAG = 0))";
                sSQLWhere = sSQLWhere + " AND ((POLICY.EFFECTIVE_DATE <= '" + objEvent.DateOfEvent + "'";
                sSQLWhere = sSQLWhere + "       AND POLICY.EXPIRATION_DATE >= '" + objEvent.DateOfEvent + "'";
                sSQLWhere = sSQLWhere + "       AND POLICY.TRIGGER_CLAIM_FLAG = 0)";
                sSQLWhere = sSQLWhere + "      OR (POLICY.EFFECTIVE_DATE <= '" + objClaim.DateOfClaim + "'";
                sSQLWhere = sSQLWhere + "          AND POLICY.EXPIRATION_DATE >= '" + objClaim.DateOfClaim + "'";
                sSQLWhere = sSQLWhere + "          AND POLICY.TRIGGER_CLAIM_FLAG <> 0))";
                sSQLWhere = sSQLWhere + " AND PRIMARY_POLICY_FLG <> 0 ";
                if (p_bIncludePolicyID)
                    sSQLWhere = sSQLWhere + " AND POLICY.POLICY_ID =" + p_iPolicyId;

                //sSQLOrder = " ORDER BY POLICY_NUMBER ";       //Commented by Sumit Agarwal as we are only selecting the Policy_Id: 10/01/2014: MITS 33588
            }
			sSQL = sSQLSelect + sSQLFrom + sSQLWhere + sSQLOrder;

            return sSQL;
		}

		private string GetOrgListForDept(int p_iDeptEId)
		{

			string sSQL=string.Empty;
			string sGetOrgListForDept=string.Empty;

			sSQL="SELECT ENTITY.ENTITY_ID, E1.ENTITY_ID, E2.ENTITY_ID, E3.ENTITY_ID, E4.ENTITY_ID, E5.ENTITY_ID, E6.ENTITY_ID, E7.ENTITY_ID " +
				" FROM ENTITY, ENTITY E1, ENTITY E2, ENTITY E3, ENTITY E4, ENTITY E5, ENTITY E6, ENTITY E7 " +
				" WHERE ENTITY.ENTITY_ID=" + p_iDeptEId + " AND E1.ENTITY_ID=ENTITY.PARENT_EID AND "+
				" E2.ENTITY_ID=E1.PARENT_EID AND E3.ENTITY_ID=E2.PARENT_EID AND E4.ENTITY_ID=E3.PARENT_EID " +
				" AND E5.ENTITY_ID=E4.PARENT_EID AND E6.ENTITY_ID=E5.PARENT_EID AND E7.ENTITY_ID=E6.PARENT_EID";

            using (DbReader objReader = objClaim.Context.DbConnLookup.ExecuteReader(sSQL))
            {
                if (objReader.Read())
                    sGetOrgListForDept = p_iDeptEId.ToString() + "," +
                        Conversion.ConvertObjToStr(objReader.GetValue(0)) + "," +
                        Conversion.ConvertObjToStr(objReader.GetValue(1)) + "," +
                        Conversion.ConvertObjToStr(objReader.GetValue(2)) + "," +
                        Conversion.ConvertObjToStr(objReader.GetValue(3)) + "," +
                        Conversion.ConvertObjToStr(objReader.GetValue(4)) + "," +
                        Conversion.ConvertObjToStr(objReader.GetValue(5)) + "," +
                        Conversion.ConvertObjToStr(objReader.GetValue(6)) + "," +
                        Conversion.ConvertObjToStr(objReader.GetValue(7));
            }

			return sGetOrgListForDept;
										
		}

        //Changed by Gagan for MITS 11451 : Start

        /// <summary>
        /// Deletes all auto checks for a claim
        /// </summary>
        /// <param name="p_iClaimID"></param>
        private void DeleteAutoChecks()
        {
            string sSQL;

            sSQL = "DELETE FROM FUNDS_AUTO_SPLIT WHERE AUTO_TRANS_ID IN (SELECT AUTO_TRANS_ID FROM FUNDS_AUTO WHERE CLAIM_ID = " + objClaim.ClaimId.ToString() + ")";
            objClaim.Context.DbConnLookup.ExecuteNonQuery(sSQL);

            sSQL = "DELETE FROM FUNDS_AUTO_BATCH WHERE AUTO_BATCH_ID IN (SELECT AUTO_BATCH_ID FROM FUNDS_AUTO WHERE CLAIM_ID = " + objClaim.ClaimId.ToString() + ")";
            objClaim.Context.DbConnLookup.ExecuteNonQuery(sSQL);

            sSQL = "DELETE FROM FUNDS_AUTO WHERE CLAIM_ID = " + objClaim.ClaimId.ToString();
            objClaim.Context.DbConnLookup.ExecuteNonQuery(sSQL);
        }

        //Changed by Gagan for MITS 11451 : End


        private void CloseAllDiaries()
        {
            SysSettings objSysSettings = null;
            objSysSettings = new SysSettings(objClaim.Context.DbConn.ConnectionString, base.ClientId); //Ash - cloud//sharishkumar Jira 804
            if (bCloseDiaries && sUserCloseDiary == "true")
            {
                objClaim.Context.DbConnLookup.ExecuteNonQuery(
                    String.Format(@"UPDATE WPA_DIARY_ENTRY SET STATUS_OPEN = 0 
					WHERE ATTACH_RECORDID = {0} AND ATTACH_TABLE = 'CLAIM'", objClaim.ClaimId));

                // Deleting all diaries
                //Check and close Diaries
                // sUserCloseDiary applied by Rahul 6th Feb 2006.
                if (objSysSettings.DeleteAllClaimDiaries != 0)
                {
                    int iCount = 0;
                    Event objEvent = null;
                    int i = 0;

                    string sSQL = "SELECT TRANS_ID FROM FUNDS WHERE CLAIM_ID=" + objClaim.ClaimId;

                    using(DbReader objReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, sSQL))
                    {
                        while (objReader.Read())
                        {
                            //sClaimDetails[i,0]=objReader.GetValue("TRANS_ID").ToString();
                            //sClaimDetails[i,1]="FUNDS";
                            iCount++;
                        }
                    }
                    iCount = iCount + objClaim.AdjusterList.Count;
                    iCount = iCount + objClaim.ClaimantList.Count;
                    iCount = iCount + objClaim.DefendantList.Count;
                    iCount = iCount + objClaim.LitigationList.Count;
                    objEvent = (objClaim.Parent as Event);
                    iCount = iCount + objEvent.PiList.Count;

                    string[,] sClaimDetails = new string[iCount, 2];

                    foreach (ClaimAdjuster objAdjuster in objClaim.AdjusterList)
                    {
                        sClaimDetails[i, 0] = objAdjuster.AdjRowId.ToString();
                        sClaimDetails[i, 1] = "ADJUSTER";
                        i++;
                    }
                    foreach (Claimant objClaimant in objClaim.ClaimantList)
                    {
                        sClaimDetails[i, 0] = objClaimant.ClaimantEid.ToString();
                        sClaimDetails[i, 1] = "CLAIMANT";
                        i++;
                    }
                    foreach (Defendant objDefendant in objClaim.DefendantList)
                    {
                        sClaimDetails[i, 0] = objDefendant.AttorneyEid.ToString();
                        sClaimDetails[i, 1] = "DEFENDANT";
                        i++;
                    }
                    foreach (ClaimXLitigation objLitigation in objClaim.LitigationList)
                    {
                        sClaimDetails[i, 0] = objLitigation.JudgeEid.ToString();
                        sClaimDetails[i, 1] = "LITIGATION";
                        i++;
                    }
                    foreach (PersonInvolved objPersons in objEvent.PiList)
                    {
                        int piTypeCode = objPersons.PiTypeCode;
                        string sTable = string.Empty;
                        switch (objClaim.Context.LocalCache.GetShortCode(piTypeCode))
                        {
                            case "E":
                                sTable = "PiEmployee";
                                break;
                            case "MED":
                                sTable = "PiMedicalStaff";
                                break;
                            case "O":
                                sTable = "PiOther";
                                break;
                            case "P":
                                sTable = "PiPatient";
                                break;
                            case "PHYS":
                                sTable = "PiPhysician";
                                break;
                            case "W":
                                sTable = "PiWitness";
                                break;
                        }
                        sTable = sTable.ToUpper();
                        sClaimDetails[i, 0] = objPersons.PiRowId.ToString();
                        sClaimDetails[i, 1] = sTable;
                        i++;
                    }

                    using(DbReader objReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, sSQL))
                    {
                        while (objReader.Read())
                        {
                            sClaimDetails[i, 0] = objReader.GetValue("TRANS_ID").ToString();
                            sClaimDetails[i, 1] = "FUNDS";
                            i++;
                        }
                    }

                    for (int j = 0; j < iCount; j++)
                    {
                        objClaim.Context.DbConnLookup.ExecuteNonQuery(
                            String.Format(@"UPDATE WPA_DIARY_ENTRY SET STATUS_OPEN = 0 
					WHERE ATTACH_RECORDID = {0} AND ATTACH_TABLE = '{1}'", sClaimDetails[j, 0], sClaimDetails[j, 1]));
                    }

                }
            }
            bCloseDiaries = false;
        }

        private bool HasOpenDiaries
        {
            get
            {
                //Nikhil Garg		17-Feb-2006
                //check whether open diaries exist for this claim or not
                string sSQL = "SELECT COUNT(*) FROM WPA_DIARY_ENTRY WHERE STATUS_OPEN<>0 AND ATTACH_RECORDID = " + objClaim.ClaimId.ToString() + " AND ATTACH_TABLE = 'CLAIM'";
                return objClaim.Context.DbConn.ExecuteInt(sSQL) > 0;
            }
        }

	}
}
