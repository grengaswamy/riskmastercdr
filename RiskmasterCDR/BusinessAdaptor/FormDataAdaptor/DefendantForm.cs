using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	///	Defendant Screen.
	/// </summary>
	public class DefendantForm : DataEntryFormBase
	{
		const string CLASS_NAME = "Defendant";
		const string FILTER_KEY_NAME = "ClaimId";

		private Defendant objDefendant{get{return objData as Defendant;}}

		private LocalCache objCache{get{return objData.Context.LocalCache;}}

        private string m_Caption = string.Empty;
            //nnorouzi; MITS 19478
        public override string GetCaption()
        {
            return base.GetCaption();
        }

		public DefendantForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

		public override void InitNew()
		{
			base.InitNew();

			//set filter for ClaimId
			objDefendant.ClaimId=base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME,false);
			if (objDefendant.ClaimId>0)
				(objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + objDefendant.ClaimId;
		}

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
            int iUseClaimProgressNotes = 0;
			base.OnUpdateForm ();

            //MGaba2:MITS 16498:Start
            //Module Security Permission was not working as security id was different each time defendant 
            //is opened from different claim.

            Claim objClaim = (Claim)objDefendant.Context.Factory.GetDataModelObject("Claim", false);
            objClaim.MoveTo(objDefendant.ClaimId);

            switch (objClaim.LineOfBusCode)
            {
                case 241:
                    m_SecurityId = RMO_GC_DEFENDANT;
                    break;
                case 242:
                    m_SecurityId = RMO_VA_DEFENDANT;
                    break;
                case 243:
                    m_SecurityId = RMO_WC_DEFENDANT;
                    break;
                case 844:
                    m_SecurityId = RMO_DI_DEFENDANT;
                    break;
                //Start-Mridul Bansal. 01/04/10. MITS#18230. Support for Property Claims.
                case 845:
                    m_SecurityId = RMO_PC_DEFENDANT;
                    break;
                //End-Mridul Bansal. 01/04/10. MITS#18230. Support for Property Claims.
            }

            //Setting Security Id for further permissions like attachment
            XmlElement objSysSid = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysSid");
            objSysSid.InnerText = m_SecurityId.ToString();

            //Create new Permission wasnt working
            if (objDefendant.DefendantRowId == 0)
            {
                if (!objDefendant.Context.RMUser.IsAllowedEx(m_SecurityId, RMO_CREATE))
                {
                    m_CurrentAction = enumFormActionType.None;
                    LogSecurityError(RMO_CREATE);
                    return;
                }
            }
            else
            {//View Permission wasnt working
                if (!objDefendant.Context.RMUser.IsAllowedEx(m_SecurityId, RMO_VIEW))
                {
                    m_CurrentAction = enumFormActionType.None;                    
                    LogSecurityError(RMO_VIEW);
                    return;
                }
            }
            //MGaba2:MITS 16498:End
            
            //mgaba2:mits 29116
            iUseClaimProgressNotes = objClaim.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode].ClmProgressNotesFlag;
            if (iUseClaimProgressNotes == 0)
            {
                base.AddKillNode("enhancednotes");
            }

            //nnorouzi; MITS 19478
            m_Caption = this.GetCaption();

			//Pass this subtitle value to view (ref'ed from @valuepath).
			base.ResetSysExData("SubTitle", m_Caption);

            //Gagan: Adding SubTitle to modified controls list
            ArrayList singlerow = new ArrayList();
            base.AddElementToList(ref singlerow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singlerow, "id", "formsubtitle");
            base.AddElementToList(ref singlerow, "Text", m_Caption);
            base.m_ModifiedControls.Add(singlerow);
            


			//Nikhil Garg		Dated: 23-Dec-05		Bug No: 1073
			//Instead of just DefInsurerLastName Now serializing the full Insurer Entity in SysExData  
			//(this was required because of the Lookup changes made for Event Lookup by Brian)

			XmlNode objOld=null;
			XmlElement objNew=null;
			XmlDocument objXML = base.SysEx;

			objOld = objXML.SelectSingleNode("//DefInsurer");
			objNew = objXML.CreateElement("DefInsurer");
			
//			//add defendant insurer last name node (DefInsurerLastName)
//			objOld = base.SysEx.SelectSingleNode("//DefInsurerLastName");
//			objNew = base.SysEx.CreateElement("DefInsurerLastName");

			Entity objInsurer=m_fda.Factory.GetDataModelObject("Entity",false) as Entity;
			if (objDefendant.InsurerEid!=0)
				objInsurer.MoveTo(objDefendant.InsurerEid);
			objNew.InnerXml = (objInsurer as DataObject).SerializeObject();
//				XmlCDataSection objCData = base.SysEx.CreateCDataSection(objInsurer.LastName);
//				objNew.AppendChild(objCData);
//				objNew.SetAttribute("codeid",objDefendant.InsurerEid.ToString());
	
			if(objOld !=null)
				objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);
            if (objData.Context.InternalSettings.SysSettings.P2Login == "" ||
                objData.Context.InternalSettings.SysSettings.P2Pass == "" ||
                objData.Context.InternalSettings.SysSettings.P2Url == "")
            {
                //((XmlElement)base.SysView.SelectSingleNode("//control[@name='deflastname']")).SetAttribute("withlink","");
                //Add deflastname to modified control list
                ArrayList singleRow = new ArrayList();
                base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.PiEntityLookup.ToString());
                base.AddElementToList(ref singleRow, "id", "deflastname");
                base.AddElementToList(ref singleRow, "withlink", "");
                base.m_ModifiedControls.Add(singleRow);

            }


            string sDiaryMessage = string.Empty;
            //Claim objClaim = (Claim)objDefendant.Context.Factory.GetDataModelObject("Claim", false);
            //objClaim.MoveTo(objDefendant.ClaimId);
            if (objClaim.EventId != 0 && objClaim.PrimaryClaimant != null)
                sDiaryMessage = objClaim.PrimaryClaimant.ClaimantEntity.GetLastFirstName();
            else
                sDiaryMessage = "";
            base.ResetSysExData("DiaryMessage", sDiaryMessage);

            objClaim.Dispose();

//Check for People Maintenance permission
            //PeoplePermissionChecks4Other("defendant", m_SecurityId+RMO_UPDATE);
            base.ResetSysExData("DefendantChanged", "false");
            base.ResetSysExData("AttorneyChanged", "false");                                             
		}

		public override void BeforeApplyLookupData(ref bool Cancel, ArrayList arrToSaveFields)
		{
			arrToSaveFields.Add("EntityTableId");
		}

        public override void OnValidate(ref bool Cancel)
        {
            bool bError = false;
            string sDefendantChanged = base.GetSysExDataNodeText("DefendantChanged");
            string sAttorneyChanged = base.GetSysExDataNodeText("AttorneyChanged");
            
            //string[] sETypes = sEntityType.Split('|');
            //foreach (string word in sETypes)
            //{
            //if (string.IsNullOrEmpty sDefendantChanged)
            //    bDefendant = true;
            //else if (word.Equals("attorneyentity"))
            //    bAttorney = true;
            //}


            //if ((!Adaptor.userLogin.IsAllowedEx(RMO_ENTITYMAINTENANCE + RMO_UPDATE)) && !string.IsNullOrEmpty(sDefendantChanged)  && string.Equals(sDefendantChanged,"true",StringComparison.InvariantCultureIgnoreCase)  && objDefendant.DefendantEntity.EntityId > 0)//Bharani - MITS : 33192
            if ((!Adaptor.userLogin.IsAllowedEx(m_SecurityId + RMO_UPDATE)) && !string.IsNullOrEmpty(sDefendantChanged) && string.Equals(sDefendantChanged, "true", StringComparison.InvariantCultureIgnoreCase) && objDefendant.DefendantEntity.EntityId > 0)//Bharani - MITS : 33192
            {
                Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                    Globalization.GetString("Permission.DefendantForm.NoDefendantUpdate",base.ClientId), BusinessAdaptorErrorType.Error);

                bError = true;
            }
            //rsharma220 MITS 31451 Start
            if ((!Adaptor.userLogin.IsAllowedEx(RMO_PEOPLE + RMO_UPDATE)) && !string.IsNullOrEmpty(sDefendantChanged)  && string.Equals(sAttorneyChanged,"true",StringComparison.InvariantCultureIgnoreCase) && objDefendant.AttorneyEntity.EntityId > 0)
            {
                Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                    Globalization.GetString("Permission.DefendantForm.NoAttorneyUpdate",base.ClientId), BusinessAdaptorErrorType.Error);

                bError = true;
            }
            if ((!Adaptor.userLogin.IsAllowedEx(RMO_PEOPLE + RMO_CREATE)) && objDefendant.AttorneyEntity.IsNew && !string.IsNullOrEmpty(objDefendant.AttorneyEntity.LastName)  && objDefendant.AttorneyEntity.EntityId == 0)
            {
                Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                    Globalization.GetString("Permission.DefendantForm.NoAttorneyCreate",base.ClientId), BusinessAdaptorErrorType.Error);

                bError = true;
            }
            //rsharma220 MITS 31451 End
            //if ((!Adaptor.userLogin.IsAllowedEx(RMO_ENTITYMAINTENANCE + RMO_CREATE)) && !string.IsNullOrEmpty(objDefendant.DefendantEntity.LastName) && objDefendant.DefendantEntity.IsNew && objDefendant.DefendantEntity.EntityId == 0)  //Bharani - MITS : 33192 - Change 1/2
            if ((!Adaptor.userLogin.IsAllowedEx(m_SecurityId + RMO_CREATE)) && !string.IsNullOrEmpty(objDefendant.DefendantEntity.LastName) && objDefendant.DefendantEntity.IsNew && objDefendant.DefendantEntity.EntityId == 0)    //Bharani - MITS : 33192 - Change 2/2
            {
                Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                    Globalization.GetString("Permission.DefendantForm.NoDefendantCreate",base.ClientId), BusinessAdaptorErrorType.Error);

                bError = true;
            }
            Cancel = bError;
        }
      

//		public override void BeforeSave(ref bool Cancel)
//		{
//			base.BeforeSave (ref Cancel);
//
//			XmlDocument objXML = base.SysEx;
//			XmlElement objElem=null;
//
//			objElem=(XmlElement)(objXML.GetElementsByTagName("DefInsurerLastName")[0]);
//			if (objElem.InnerText.Trim() != string.Empty)
//				objDefendant.InsurerEid=Conversion.ConvertStrToInteger(objElem.Attributes["codeid"].Value);
//			else
//				objDefendant.InsurerEid=0;
//		}
	}
}