﻿using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Text;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using System.Collections.Generic;

namespace Riskmaster.BusinessAdaptor
{
    class SiteUnitForm : DataEntryFormBase
    {
        const string CLASS_NAME = "SiteUnit";
        const string FILTER_KEY_NAME = "SiteId";

      
        private SiteUnit SiteUnit { get { return objData as SiteUnit; } }

        private LocalCache objCache { get { return objData.Context.LocalCache; } }

        public SiteUnitForm(FormDataAdaptor fda)
            : base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

        public override void InitNew()
        {
            base.InitNew();

            //set filter for UnitId            
            SiteUnit.SiteId = base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME);
            if (SiteUnit.SiteId > 0)
                (objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + SiteUnit.SiteId;
        }

        private void ApplyFormTitle()
        {
            string sSubTitleText = string.Empty;
            ArrayList singleRow = null;
            if (SiteUnit != null)
            {
                sSubTitleText += " [ " + SiteUnit.SiteNumber + " ]";
            }

            //Pass this title value to view (ref'ed from @valuepath).
            base.ResetSysExData("SubTitle", sSubTitleText);

            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singleRow, "id", "formsubtitle");
            base.AddElementToList(ref singleRow, "Text", sSubTitleText);
            base.m_ModifiedControls.Add(singleRow);
        }

        public override void OnUpdateForm()
        {
            
            base.OnUpdateForm();
            ApplyFormTitle();
            if (!SiteUnit.Context.RMUser.IsAllowedEx(FormBase.RMO_SEARCH, FormBase.RMO_PROPERTY_SEARCH))
                base.AddKillNode("search");
            if (!SiteUnit.Context.RMUser.IsAllowedEx(FormBase.RMO_LOOKUP, FormBase.RMO_PROPERTY_SEARCH))
                base.AddKillNode("lookup");

            AutoGenerateSiteNumber();

            //Raman Bhatia - Acrosoft Phase 1 changes.. Document Management Not available for Employee if Acrosoft is enabled
            bool bUseAcrosoftInterface = SiteUnit.Context.InternalSettings.SysSettings.UseAcrosoftInterface;
            bool bUsePaperVision = SiteUnit.Context.InternalSettings.SysSettings.UsePaperVisionInterface; //Mona:PaperVisionMerge : Animesh inserted MITS 16697
            //Mona:PaperVisionMerge: Adding bUsePaperVision in the condition
            //rbhatia4:R8: Use Media View Setting : July 11 2011
            if (bUseAcrosoftInterface || bUsePaperVision || SiteUnit.Context.InternalSettings.SysSettings.UseMediaViewInterface)
            {
                base.AddKillNode("attachdocuments");//Mona:PaperVisionMerge:Animesh Inserted MITS 16697
                base.AddKillNode("attach");
            }
            //akaur9 MITS 25163- Policy Interface Implementation --start

            int iSystemPolicyId = base.GetSysExDataNodeInt("//SystemPolicyId");

            if (iSystemPolicyId > 0)
            {
                base.AddReadOnlyNode("readOnlyPage");
                base.AddKillNode("save");
                base.AddKillNode("delete");
                //skhare7 Point Policy interface Added statunitno/riskloc/subloc
                if (SiteUnit.SiteId > 0)
                {

                    DisplayUnitNo();
                }
            }
            else
            {
                base.AddKillNode("Unitno");
            }
            //akaur9 MITS 25163- Policy Interface Implementation --End
        }

        internal override bool ValidateRequiredViewFields(XmlDocument propertyStore)
        {
            AutoGenerateSiteNumber();
            return base.ValidateRequiredViewFields(propertyStore);
        }

        /// <summary>
        /// For Autogenerating PIN for new record in case it is not provided
        /// </summary>
        private void AutoGenerateSiteNumber()
        {
            XmlElement objNotReqNew = null;
            string sNotReqNew = string.Empty;

            objNotReqNew = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysNotReqNew");
            sNotReqNew = objNotReqNew.InnerText;

            if (sNotReqNew.IndexOf("sitenumber") == -1)
                sNotReqNew = sNotReqNew + "sitenumber|";
            objNotReqNew.InnerText = sNotReqNew;

        }
        private void DisplayUnitNo()
        {

            string sStaUnitNo = string.Empty;
            string sRiskLoc = string.Empty;
            string sSubLoc = string.Empty;

            StringBuilder sbSql = new StringBuilder();
            sbSql.Append("SELECT STAT_UNIT_NUMBER,UNIT_RISK_LOC,UNIT_RISK_SUB_LOC FROM  POINT_UNIT_DATA WHERE ");
            sbSql.Append("  UNIT_TYPE='S' AND UNIT_ID=" + SiteUnit.SiteId);
            DbReader objReader = null;
            objReader = DbFactory.GetDbReader(base.m_fda.connectionString, sbSql.ToString());
            if (objReader != null)
            {
                if (objReader.Read())
                {
                    sStaUnitNo = objReader.GetValue("STAT_UNIT_NUMBER").ToString();
                    sRiskLoc = objReader.GetValue("UNIT_RISK_LOC").ToString();
                    if (sRiskLoc == string.Empty)
                        sRiskLoc =  "-";

                    sSubLoc = objReader.GetValue("UNIT_RISK_SUB_LOC").ToString();
                    if (sSubLoc == string.Empty)
                        sSubLoc =  "-";

                    base.ResetSysExData("UnitNo", sStaUnitNo + "/" + sRiskLoc + "/" + sSubLoc);


                }

            }
        
        
        }

    }
}
