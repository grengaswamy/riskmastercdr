//Author: BSB 06.21.2005
using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// FormStackItem is a wrapper for the meta-data of a single screen captured in a stack entry.
	/// </summary>
	/// 
	internal class FormStackItem
	{
		public int SecurityId;
		public string FormName;
		public string FormIdName;
		public int FormId;
		public XmlDocument objSysEx;
	}
	/// <summary> TODO
	/// FormStack roughly represents the form context blocks from RMWorld.
	/// It organizes "meta-information" (SysExData) of all "open" screens into a stack form.
	/// Relevant lineage details are also included.
	/// </summary>
	/// 
	internal class FormStack
	{
		Stack m_Stack = new Stack();

		public FormStack()
		{
		}
		public string SerializeObject()
		{
			string ret="";
			return ret;
		}
		#region Generic Stack Routines
		public void Push(FormStackItem item)
		{
			if(item == null)
				return;
			m_Stack.Push(item);
		}
		public FormStackItem Pop()
		{
			return m_Stack.Pop() as FormStackItem;
		}
		public FormStackItem Peek(){return m_Stack.Peek() as FormStackItem;}
		public FormStackItem Peek(int depth)
		{
			object[] arr = new object[]{};
			m_Stack.CopyTo(arr,0);
			return arr.GetValue(depth) as FormStackItem;
		}
		#endregion


	}
}
