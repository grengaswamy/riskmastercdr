using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// StaffPrivilege Screen.
	/// </summary>
	public class StaffPrivilegeForm : DataEntryFormBase
	{
		const string CLASS_NAME = "MedicalStaffPrivilege";
		const string FILTER_KEY_NAME = "StaffEid";

		private MedicalStaffPrivilege MedicalStaffPrivilege{get{return objData as MedicalStaffPrivilege;}}

		private LocalCache objCache{get{return objData.Context.LocalCache;}}
	    
		/// <summary>
		/// Class constructor to initialize Class Name information
		/// </summary>
		/// <param name="fda"></param>
		public StaffPrivilegeForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

		/// <summary>
		/// Initializes the class and sets the parent ID for the class
		/// that can be used for parent-child navigation
		/// </summary>
		public override void InitNew()
		{
			base.InitNew();

			//set filter for StaffEid
			MedicalStaffPrivilege.StaffEid=base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME,false);
			if (MedicalStaffPrivilege.StaffEid>0)
				(objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + MedicalStaffPrivilege.StaffEid;
		}

//		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
//		public override void OnUpdateForm()
//		{
//			base.OnUpdateForm ();
//			if(MedicalStaffPrivilege.StaffEid==0)
//			{
//				AfterAddNew();
//			}
//		}
//		public override void AfterAddNew()
//		{
//			base.AfterAddNew ();
//			XmlDocument objXML = base.SysEx;
//			XmlNode objStaffEid=null;
//			try{objStaffEid = objXML.GetElementsByTagName(FILTER_KEY_NAME)[0];}
//			catch{};
//			
//			//Filter by this staffeid if Present.
//			if(objStaffEid !=null)
//			{
//				MedicalStaffPrivilege.StaffEid=Conversion.ConvertStrToInteger(objStaffEid.InnerText);
//			}
//		}

		public override void OnValidate(ref bool Cancel)
		{
			bool bError = false;
			
			// Perform data validation
			if (MedicalStaffPrivilege.IntDate!="" && MedicalStaffPrivilege.EndDate!="")
			{
				if(MedicalStaffPrivilege.IntDate.CompareTo(MedicalStaffPrivilege.EndDate)>0)
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThanStartDate", base.ClientId), Globalization.GetString("Field.EndDate", base.ClientId), Conversion.ToDate(MedicalStaffPrivilege.IntDate).ToShortDateString(), Globalization.GetString("Field.StartDate", base.ClientId)),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}
			}
			else 
			{
				if(MedicalStaffPrivilege.IntDate=="" && MedicalStaffPrivilege.EndDate!="")
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        string.Format(Globalization.GetString("Validation.MustBeValidDate", base.ClientId), Globalization.GetString("Field.StartDate", base.ClientId)),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}
				else
				{
					if(MedicalStaffPrivilege.IntDate!="" && MedicalStaffPrivilege.EndDate=="")
					{
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                            string.Format(Globalization.GetString("Validation.MustBeValidDate", base.ClientId), Globalization.GetString("Field.EndDate", base.ClientId)),
							BusinessAdaptorErrorType.Error);

						bError = true;
					}
				}
			}
			// Return true if there were validation errors
			Cancel = bError;
		}
	}
}