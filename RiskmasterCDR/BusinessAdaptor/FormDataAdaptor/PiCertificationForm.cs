using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

//TODO: Auto-Generated Form - migrate and verify behaviors from Legacy RMNet form.

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for PiCertification Screen.
	/// </summary>
	public class PiCertificationForm : DataEntryFormBase
	{
		const string CLASS_NAME = "PhysicianCertification";
		private PhysicianCertification PhysicianCertification{get{return objData as PhysicianCertification;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		const string FILTER_KEY_NAME = "PhysEid";
        private string sLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").Split('|')[0].ToString();

		public override void InitNew()
		{
			base.InitNew(); 

			this.m_ParentId = base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME,false);
			(objData as INavigation).Filter = 
				objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + this.m_ParentId.ToString();		
			this.PhysicianCertification.PhysEid = this.m_ParentId;

		}

		public PiCertificationForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
            sLangCode = Convert.ToString(fda.userLogin.objUser.NlsCode);
		}

        // npadhy MITS 14200 Validation for Privilege Start Date and Privilege End Date as Per RMWorld
        public override void OnValidate(ref bool Cancel)
        {
            bool bError = false;
           // string sLangCode = base.LanguageCode;
            if (!string.IsNullOrEmpty(PhysicianCertification.IntDate) && !(string.IsNullOrEmpty(PhysicianCertification.EndDate)))
            {
                if (PhysicianCertification.IntDate.CompareTo(PhysicianCertification.EndDate) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                            String.Format(CommonFunctions.FilterBusinessMessageRetainErrorCode(Globalization.GetString("Validation.MustBeGreaterThanStartDate1", base.ClientId, sLangCode), sLangCode) + "(" + Conversion.ToDate(PhysicianCertification.IntDate, sLangCode, base.ClientId) + ")", CommonFunctions.FilterBusinessMessageRetainErrorCode(Globalization.GetString("Field.EndDate", base.ClientId, sLangCode), sLangCode)),
                            BusinessAdaptorErrorType.Error); //tmalhotra3:MITS 31125

                    bError = true;
                }
            }

            // Return true if there were validation errors
            Cancel = bError;
        }

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
			base.OnUpdateForm ();
		}

	/*	public override void BeforeSave(ref bool Cancel)
		{
			XmlDocument objSysExDataXmlDoc = base.SysEx;
			XmlNode objPhysEidNode = null;
			try{objPhysEidNode = objSysExDataXmlDoc.SelectSingleNode("/SysExData/PhysEid");}
			catch{};
			
			if(objPhysEidNode !=null)
				if(this.PhysicianCertification.PhysEid==0)
					this.PhysicianCertification.PhysEid = Conversion.ConvertStrToInteger(objPhysEidNode.InnerText);

			objPhysEidNode = null;
			objSysExDataXmlDoc = null;

			base.BeforeSave (ref Cancel);		

		}*/
	}
}