﻿using System;
using System.Xml;
using System.IO;
using System.Collections;
using System.Data;
using System.Text;
using System.Collections.Generic;
using System.Globalization;
using Riskmaster.DataModel;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.Settings;
using Riskmaster.Security;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.BusinessAdaptor;

namespace Riskmaster.BusinessAdaptor
{
    /// <summary>
    /// VSS Assignment Screen.
    /// </summary>
    public class ClaimXVssAssignmentForm : DataEntryFormBase
    {
        const string CLASS_NAME = "ClaimXVssAssignment";
        const string FILTER_KEY_NAME = "VssAssignmentRowId";

        private ClaimXVssAssignment ClaimXVssAssignment { get { return objData as ClaimXVssAssignment; } }

        private LocalCache objCache{get{return objData.Context.LocalCache;}}

        /// <summary>
		/// Class constructor to initialize Class Name information
		/// </summary>
		/// <param name="fda"></param>
        /// 
        public ClaimXVssAssignmentForm(FormDataAdaptor fda)
            : base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

        /// <summary>
        /// Initializes the class and sets the parent ID for the class
        /// that can be used for parent-child navigation
        /// </summary>
        public override void InitNew()
        {
            base.InitNew();

            //set filter for row id
            ClaimXVssAssignment.VssAssignmentRowId = base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME, false);
            if (ClaimXVssAssignment.VssAssignmentRowId > 0)
                (objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + ClaimXVssAssignment.VssAssignmentRowId;
        }

        public override void OnUpdateForm()
        {
            base.OnUpdateForm();
            AppendBankAccountList();
            setAccSubAccIds();
            
        }

        public struct AccountDetail
        {
            /// <summary>
            /// Private variable for Account Name.
            /// </summary>
            private string sAccountName;
            /// <summary>
            /// Private variable for SubRowId.
            /// </summary>
            private int iSubRowId;
            /// <summary>
            /// Private variable for AccountId.
            /// </summary>
            private int iAccountId;
            /// <summary>
            /// Read/Write property for AccountName.
            /// </summary>
            public string AccountName
            {
                get
                {
                    return (sAccountName);
                }
                set
                {
                    sAccountName = value;
                }
            }
            /// <summary>
            /// Read/Write property for SubRowId.
            /// </summary>
            public int SubRowId
            {
                get
                {
                    return (iSubRowId);
                }
                set
                {
                    iSubRowId = value;
                }
            }
            /// <summary>
            /// Read/Write property for AccountId.
            /// </summary>
            public int AccountId
            {
                get
                {
                    return (iAccountId);
                }
                set
                {
                    iAccountId = value;
                }
            }           
        }
        private bool m_bFundsAccountLink = false;

        /// <summary>
        /// Private variable to store connection string
        /// </summary>
        private string m_sConnectionString = string.Empty;

        /// <summary>
        /// Private variable for OverrideFundsAccountLink Flag
        /// </summary>
        private bool m_bOverrideFundsAccountLink = false;

        /// <summary>
        /// Get the Accounts List.
        /// </summary>
        /// <param name="p_bSubAccount">Sub Account Flag</param>
        /// <returns>Account Array List</returns>
        public ArrayList GetAccounts(bool p_bSubAccount)
        {
            return (GetAccounts(p_bSubAccount, 0));
        }
        public void setAccSubAccIds()
        {
            string sAccID = "0";
            string sSubAccID = "0";
            XmlElement objRootNode = null;
            XmlElement objVssAssignmentRootNode = null;

            objRootNode = (XmlElement)base.SysEx.SelectSingleNode("/SysExData");
            CreateElement(objRootNode, "ClaimXVssAssignment", ref objVssAssignmentRootNode);


            using (DbReader rdr = ClaimXVssAssignment.Context.DbConnLookup.ExecuteReader("SELECT ACCOUNT_ID,SUB_ACCOUNT_ID FROM CLAIM_X_VSSASSIGNMENT WHERE ASSIGNMENT_ID  = " + ClaimXVssAssignment.VssAssignmentRowId))
            {
                if (rdr != null)
                {
                    if (rdr.Read())
                    {
                        sAccID = Conversion.ConvertObjToStr(rdr.GetValue("ACCOUNT_ID"));
                        sSubAccID = Conversion.ConvertObjToStr(rdr.GetValue("SUB_ACCOUNT_ID"));
                    }
                    rdr.Close();
                }
            }
            this.CreateAndSetElement(objVssAssignmentRootNode, "AccountId", sAccID);
            this.CreateAndSetElement(objVssAssignmentRootNode, "SubAccountId", sSubAccID);

        }
        private void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText)
        {
            XmlElement objChildNode = null;
            this.CreateAndSetElement(p_objParentNode, p_sNodeName, p_sText, ref objChildNode);
        }
        private void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText, ref XmlElement p_objChildNode)
        {
            CreateElement(p_objParentNode, p_sNodeName, ref p_objChildNode);
            p_objChildNode.InnerText = p_sText;
        }
        private void CreateElement(XmlElement p_objParentNode, string p_sNodeName, ref XmlElement p_objChildNode)
        {
            p_objChildNode = p_objParentNode.OwnerDocument.CreateElement(p_sNodeName);
            p_objParentNode.AppendChild(p_objChildNode);
        }
        public ArrayList GetAccounts(bool p_bSubAccount, int p_iParentAccount)
        {
            SysSettings objSysSetting = null;
            ArrayList arrlstAccountDetail = null;
            AccountDetail structAccountDetail;
            object objAccountId = null;
            string sSQL = string.Empty;
            bool bUseFundsSubAccounts = false;
            bool bFundsAccountLink = false;
            int iOrderCondition = 0;
            bool bExit = false;
            string sEffTriger = string.Empty;
            string sTriggerFromDate = string.Empty;
            string sTriggerToDate = string.Empty;
            string sOrderBy = string.Empty;
            LocalCache objLocalCache = null;
            m_sConnectionString = m_fda.connectionString;
            int iPolLOBCode = 0;
            try
            {
                arrlstAccountDetail = new ArrayList();
                objSysSetting = new SysSettings(m_sConnectionString, ClientId); //Ash - cloud
                objLocalCache = new LocalCache(m_sConnectionString,ClientId);
                bUseFundsSubAccounts = objSysSetting.UseFundsSubAcc;

                if (m_bOverrideFundsAccountLink)
                    bFundsAccountLink = m_bFundsAccountLink;
                else
                    bFundsAccountLink = objSysSetting.FundsAccountLink;

                #region Set the Order By.

                if (p_bSubAccount && bUseFundsSubAccounts)
                    sOrderBy = " ORDER BY SUB_ACC_NAME";
                else
                    sOrderBy = " ORDER BY ACCOUNT_NAME";

                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, "SELECT ORDER_BANK_ACC FROM SYS_PARMS"))
                {
                    if (objReader != null && objReader.Read())
                    {
                        iOrderCondition = Conversion.ConvertObjToInt(objReader.GetValue("ORDER_BANK_ACC"), base.ClientId);
                        if (p_bSubAccount && bUseFundsSubAccounts)
                        {
                            switch (iOrderCondition)
                            {
                                case 0:
                                    sOrderBy = string.Empty;
                                    break;
                                case 1:
                                    sOrderBy = " ORDER BY BANK_ACC_SUB.PRIORITY  DESC, SUB_ACC_NAME ASC";
                                    break;
                                case 2:
                                    sOrderBy = " ORDER BY SUB_ACC_NAME";
                                    break;
                                case 3:
                                    sOrderBy = " ORDER BY SUB_ACC_NUMBER, SUB_ACC_NAME";
                                    break;
                            }
                        }
                        else
                        {
                            switch (iOrderCondition)
                            {
                                case 0:
                                    sOrderBy = string.Empty;
                                    break;
                                case 1:
                                    sOrderBy = " ORDER BY PRIORITY DESC, ACCOUNT_NAME ASC";
                                    break;
                                case 2:
                                    sOrderBy = " ORDER BY ACCOUNT_NAME";
                                    break;
                                case 3:
                                    sOrderBy = " ORDER BY ACCOUNT_NUMBER, ACCOUNT_NAME";
                                    break;
                            }
                        }
                    }
                }

                #endregion
                    if (p_bSubAccount && bUseFundsSubAccounts)
                    {
                        sSQL = "SELECT SUB_ACC_NAME,SUB_ROW_ID,BANK_ACC_SUB.ACCOUNT_ID"
                            + " FROM BANK_ACC_SUB,ACCOUNT"
                            + " WHERE BANK_ACC_SUB.SUB_ROW_ID NOT IN (SELECT SUB_ACC_ID FROM ACC_SERVICE_CODE WHERE SUB_ACC_ID IS NOT NULL) AND BANK_ACC_SUB.ACCOUNT_ID =ACCOUNT.ACCOUNT_ID";
                    }
                    else
                    {
                        sSQL = "SELECT ACCOUNT_NAME,ACCOUNT_ID"
                            + " FROM ACCOUNT"
                            + " WHERE ACCOUNT.ACCOUNT_ID NOT IN (SELECT ACCOUNT_ID FROM ACC_SERVICE_CODE)";
                    }
                  sSQL = sSQL + sOrderBy;

                if (!bExit)
                {
                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                    {
                        if (objReader != null)
                        {
                            while (objReader.Read())
                            {
                                    structAccountDetail = new AccountDetail();
                                    structAccountDetail.AccountName = objReader.GetString(0);
                                    if (p_bSubAccount && bUseFundsSubAccounts)
                                    {
                                        structAccountDetail.SubRowId = objReader.GetInt(1);
                                        objAccountId = objReader.GetInt(2);
                                    }
                                    else
                                    {
                                        structAccountDetail.SubRowId = 0;
                                        objAccountId = objReader.GetInt(1);
                                    }

                                    try
                                    {
                                        structAccountDetail.AccountId = Conversion.ConvertObjToInt(objAccountId, base.ClientId);
                                    }
                                    catch
                                    {
                                        structAccountDetail.AccountId = 0;
                                    }
                                    arrlstAccountDetail.Add(structAccountDetail);
                            }
                        }
                    }
                }
            }

            catch (RMAppException p_objRmAEx)
            {
                throw p_objRmAEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundForm.GetAccounts.ErrorAccount",base.ClientId), p_objEx);
            }
            finally
            {
                objSysSetting = null;
                if (objLocalCache != null)
                    objLocalCache.Dispose();
            }
            return (arrlstAccountDetail);
        }

        private string sGetAccountName(int iAccountID, int iSubAccountId, bool bUseSubAccounts)
        {
            string sTemp = string.Empty;
            string sSQL = string.Empty;

            if (bUseSubAccounts == true)
            {
                sSQL = "SELECT SUB_ACC_NAME FROM BANK_ACC_SUB WHERE ACCOUNT_ID = " + iAccountID + " AND SUB_ROW_ID = " + iSubAccountId;
            }
            else
            {
                sSQL = "SELECT ACCOUNT_NAME FROM ACCOUNT WHERE ACCOUNT_ID = " + iAccountID;
            }
            using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
            {
                if (objReader != null && objReader.Read())
                {
                    if (bUseSubAccounts == true)
                    {
                        sTemp = objReader.GetString("SUB_ACC_NAME");
                    }
                    else
                    {
                        sTemp = objReader.GetString("ACCOUNT_NAME");
                    }
                }
            }
            return sTemp;
        }

        private void AppendBankAccountList()
        {
            //Variable declarations
            ArrayList arrAcctList = new ArrayList();
            XmlDocument objXML = base.SysEx;
            XmlNode objOld = null;
            XmlElement objNew = null;
            XmlCDataSection objCData = null;
            string sAccountName = string.Empty;
            int iUserId = base.m_fda.userLogin.UserId;
            int iGroupId = base.m_fda.userLogin.GroupId;
            int iThisAccountId = 0;
            int iThisSubAccountId = 0;
            bool bThisAccountIdFound = false;
            //Retrieve the complete Bank Account List and store it in the ArrayList
            arrAcctList = GetAccounts(this.objData.Context.InternalSettings.SysSettings.UseFundsSubAcc);;
            //Create the necessary SysExData to be used in ref binding
            objOld = objXML.SelectSingleNode("//AccountList");
            objNew = objXML.CreateElement("AccountList");

            XmlElement xmlOption = null;
            XmlAttribute xmlOptionAttrib = null;
            xmlOptionAttrib = null;
            xmlOption = null;

            foreach (AccountDetail item in arrAcctList)
            {
                xmlOption = objXML.CreateElement("option");
                objCData = objXML.CreateCDataSection(item.AccountName);
                xmlOption.AppendChild(objCData);
                xmlOptionAttrib = objXML.CreateAttribute("value");

                if (this.objData.Context.InternalSettings.SysSettings.UseFundsSubAcc)
                {
                    xmlOptionAttrib.Value = item.SubRowId.ToString();
                }
                else
                {
                    xmlOptionAttrib.Value = item.AccountId.ToString();
                }
                xmlOption.Attributes.Append(xmlOptionAttrib);
                objNew.AppendChild(xmlOption);
                if (this.objData.Context.InternalSettings.SysSettings.UseFundsSubAcc)
                {
                    if (iThisSubAccountId == item.SubRowId)
                    {
                        bThisAccountIdFound = true;
                    }
                }
                else
                {
                    if (iThisAccountId == item.AccountId)
                    {
                        bThisAccountIdFound = true;
                    }
                }

            }//end foreach
            if ((!bThisAccountIdFound) && (iThisAccountId != 0))
            {
                sAccountName = sGetAccountName(iThisAccountId, iThisSubAccountId, this.objData.Context.InternalSettings.SysSettings.UseFundsSubAcc);
                xmlOption = objXML.CreateElement("option");
                objCData = objXML.CreateCDataSection(sAccountName);
                xmlOption.AppendChild(objCData);
                xmlOptionAttrib = objXML.CreateAttribute("value");

                if (this.objData.Context.InternalSettings.SysSettings.UseFundsSubAcc)
                {
                    xmlOptionAttrib.Value = iThisSubAccountId.ToString();
                }
                else
                {
                    xmlOptionAttrib.Value = iThisAccountId.ToString();
                }
                xmlOption.Attributes.Append(xmlOptionAttrib);
                objNew.AppendChild(xmlOption);
            }
            //Add the XML to the SysExData 
            if (objOld != null)
                objXML.DocumentElement.ReplaceChild(objNew, objOld);
            else
                objXML.DocumentElement.AppendChild(objNew);

            objOld = objXML.SelectSingleNode("//ControlAppendAttributeList");
            objNew = objXML.CreateElement("ControlAppendAttributeList");
            
            XmlElement objElem = null;
            XmlElement objChild = null;

            objElem = objXML.CreateElement("bankaccount");
            objChild = objXML.CreateElement("ref");
            if (this.objData.Context.InternalSettings.SysSettings.UseFundsSubAcc)
                objChild.SetAttribute("value", "/Instance/UI/FormVariables/SysExData/ClaimXVssAssignment/SubAccountId");
            else
                objChild.SetAttribute("value", "/Instance/UI/FormVariables/SysExData/ClaimXVssAssignment/AccountId");

             objElem.AppendChild(objChild); ;
             objNew.AppendChild(objElem);

             if (objOld != null)
                 objXML.DocumentElement.ReplaceChild(objNew, objOld);
             else
                objXML.DocumentElement.AppendChild(objNew);

                ArrayList singleRow = new ArrayList();
                base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
                base.AddElementToList(ref singleRow, "id", "lbl_bankaccount");
                if (this.objData.Context.InternalSettings.SysSettings.UseFundsSubAcc)
                {
                    base.AddElementToList(ref singleRow, "Text", "Sub Bank Account");
                }
                else
                {
                    base.AddElementToList(ref singleRow, "Text", "Bank Account");
                }
                base.m_ModifiedControls.Add(singleRow);

            //Clean up
            arrAcctList = null;
            objOld = null;
            objNew = null;
            objCData = null;
            objXML = null;
        }

        public override void OnUpdateObject()
        {
            int iTemp = 0;
            XmlNode objClaimXVssNode = null;
            base.OnUpdateObject();

            ClaimXVssAssignment objClaimXVss = ClaimXVssAssignment;

            // Update the AccountId in case SubAccount In Use.
            if (objClaimXVss.Context.InternalSettings.SysSettings.UseFundsSubAcc)
            {
                // DropDown represents SubBankAccountId 
                BankAccSub objBankAccSub = (BankAccSub)objClaimXVss.Context.Factory.GetDataModelObject("BankAccSub", false);
                objBankAccSub.MoveTo(Conversion.ConvertStrToInteger(base.SysEx.SelectSingleNode("/SysExData/ClaimXVssAssignment/SubAccountId").InnerText));

                XmlElement objNew = null;
                objNew = base.SysEx.CreateElement("AccountId");
                XmlNode objClaimXVssXmlNode = base.SysEx.SelectSingleNode("/SysExData/ClaimXVssAssignment");
                objClaimXVssXmlNode.AppendChild(objNew);
                objClaimXVss.SubAccountId = objBankAccSub.SubRowId;
                objClaimXVss.AccountId = objBankAccSub.AccountId;

                base.SysEx.SelectSingleNode("/SysExData/ClaimXVssAssignment/AccountId").InnerText = objBankAccSub.AccountId.ToString();
            }
            else
            {
                objClaimXVssNode = base.SysEx.DocumentElement.SelectSingleNode("/SysExData/ClaimXVssAssignment");
                iTemp = Conversion.ConvertObjToInt(objClaimXVssNode.SelectSingleNode("AccountId").InnerText, base.ClientId);
                objClaimXVss.AccountId = iTemp;
                objClaimXVss.SubAccountId = 0;
            }
            
        }
    }
}
