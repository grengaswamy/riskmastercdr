﻿using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;


namespace Riskmaster.BusinessAdaptor
{
    /// <summary>
    /// Summary description for PiPrivilege Screen.
    /// npradeepshar 07/05/2011 R8 Enhancement for Injury
    /// </summary>
    class PiInjuryForm: DataEntryFormBase
    {
        const string CLASS_NAME = "PiInjury";
		private PiInjury PiInjury{get{return objData as PiInjury;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
        const string FILTER_KEY_NAME = "EventId";
		private int m_iEventId = 0;
        private string sLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").Split('|')[0].ToString();

        
		public override void InitNew()
		{
			base.InitNew(); 

			this.m_iEventId = base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME,false);
			string sFilter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + this.m_iEventId.ToString();
			int iPiEid = base.GetSysExDataNodeInt("/SysExData/PiEid",false);
			sFilter += " AND PI_EID=" + iPiEid;

			(objData as INavigation).Filter = sFilter;
			
			this.PiInjury.EventId = this.m_iEventId;
			this.PiInjury.PiEid = iPiEid;

	}



		public PiInjuryForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
            sLangCode = Convert.ToString(fda.userLogin.objUser.NlsCode);
		}

        
        public override void BeforeSave(ref bool Cancel)
        {
                        
            int iPiEid = base.GetSysExDataNodeInt("/SysExData/PiEid", false);
                 
        }
        
        public override void AfterSave()
        {
            
            base.ResetSysExData("DupSSN", "");            
        }

        
       
		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
			base.OnUpdateForm ();
			XmlDocument objXML = null;
			XmlElement objWeeksPerMonth = null;       



			if (this.SecurityId!=RMB_WC_PI && this.SecurityId!=RMB_DI_PI)
			{
				if(!PiInjury.Context.RMUser.IsAllowedEx(this.SecurityId,RMO_PI_EMPLOYEE))
				{
					LogSecurityError(RMO_ACCESS);
					return;
				}
			}

            //Add Row Id in sysexdata to pass in button
            base.CreateSysExData("PiRowId", PiInjury.PiRowId.ToString()); 

			XmlDocument objSysExDataXmlDoc = base.SysEx;

			string sSubTitleText = string.Empty; 
			
			// Add EventNumber node, if already not present, to avoid Orbeon 'ref' error 			
			XmlNode objEventNumberNode = objSysExDataXmlDoc.SelectSingleNode("/SysExData/EventNumber");

			if(objEventNumberNode==null)
			{
				base.CreateSysExData("EventNumber"); 
			}
			else
			{
				if(objEventNumberNode.InnerText.Length>0)
					sSubTitleText = " [" + objEventNumberNode.InnerText + "]"; 
				objEventNumberNode = null;
			}
			

			// Add ClaimNumber node, if already not present, to avoid Orbeon 'ref' error
			XmlNode objClaimNumberNode = objSysExDataXmlDoc.SelectSingleNode("/SysExData/ClaimNumber");

			if(objClaimNumberNode==null)
			{
				base.CreateSysExData("ClaimNumber"); 
			}			
			else
			{
				if(objClaimNumberNode.InnerText.Length>0)
					sSubTitleText = " [" + objClaimNumberNode.InnerText + "]"; 
				objClaimNumberNode = null;
			}

			// TODO - To find a better way to ascertain which is our starting page (Event or Claim)
			// As of now, first event number and then claim number is checked for subtitle text.
			base.ResetSysExData("SubTitle",sSubTitleText);

           
            if (this.PiInjury != null && Convert.ToString(this.PiInjury.PiEid) != "")
            {
                if (this.PiInjury.PiEid > 0)
                {
                    this.CurrentAction = enumFormActionType.ApplyLookupData;
                }
            }
            
            if (base.SysEx.SelectSingleNode("//DupSSN") == null) //MGaba2:MITS 9052      
                base.ResetSysExData("DupSSN", "");            
                     Event objEvent = (Event)this.PiInjury.Context.Factory.GetDataModelObject("Event", false);
            string sEventDateFromDB = string.Empty;
            if (objEvent != null)
            {
                int iEventId = PiInjury.EventId;
                if (iEventId != 0)
                {
                    objEvent.MoveTo(iEventId);
                    sEventDateFromDB = objEvent.DateOfEvent;
                    if (!String.IsNullOrEmpty(sEventDateFromDB))
                    {
                        if (base.SysEx.SelectSingleNode("DateOfEvent") == null)
                        {
                            string sUIEventDate = string.Empty;
                            sUIEventDate = Conversion.ToDate(sEventDateFromDB).ToString("MM/dd/yyyy");
                            base.CreateSysExData("DateOfEvent", sUIEventDate);
                        }
                    }
                    //MGaba2:MITS 22114:Comments screen showing title as "PiInjury Comments undefined" :Start
                    sSubTitleText = string.Format(" [{0} * {1} {2}]", objEvent.EventNumber, PiInjury.PiEntity.FirstName, PiInjury.PiEntity.LastName);
                    ArrayList singleRow = new ArrayList();
                    base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
                    base.AddElementToList(ref singleRow, "id", "formsubtitle");
                    base.AddElementToList(ref singleRow, "Text", sSubTitleText);
                    base.m_ModifiedControls.Add(singleRow);
                    //MGaba2:MITS 22114:End
                }
            }
            //Added for Mits 19315:The person involved age is being calculated off the current date, and not the date of event.  
			

			
			
			if(!PiInjury.Context.RMUser.IsAllowedEx(m_SecurityId,FormBase.RMO_VIEW_DIAGNOSIS_INFO))
				base.AddKillNode("diagnosislist");
			
            //smishra25: MITS 24395, MITS 13748 implementation was missing
            //nadim for 13748 hide/unhide ssn field
            if (!PiInjury.Context.RMUser.IsAllowedEx(m_SecurityId, FormBase.RMO_PI_EMP_VIEW_SSN))
                base.AddKillNode("taxid");
            
            
			if(!PiInjury.Context.RMUser.IsAllowedEx(m_SecurityId,FormBase.RMO_ENABLE_SALARY))
			{
				base.AddKillNode("paytypecode");
				base.AddKillNode("payamount");
				base.AddKillNode("hourlyrate");
				base.AddKillNode("weeklyhours");
				base.AddKillNode("weeklyrate");
				base.AddKillNode("monthlyrate");
			}

			if (this.PiInjury.PiRowId > 0 || this.PiInjury.PiEid <= 0)
				base.AddKillNode("javascript_SetDirtyFlag");

            if (PiInjury.PiEid > 0 && !PiInjury.Context.RMUser.IsAllowedEx(RMPermissions.RMO_EMPLOYEE, RMPermissions.RMO_EMPLOYEE_VIEW_SSN))
            {
                base.AddKillNode("taxid");
            }

			//Check for People Maintenance permission
			PeoplePermissionChecks4PI("PiInjury", m_SecurityId+RMO_UPDATE);

			//Raman Bhatia - LTD Phase 3 changes : Weeks Per Month should be passed via a node in SYSEX
			string sSQL =	"SELECT NO_WEEKS_PER_MONTH FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE = 844";

			DbReader objReader = null;
            using (objReader = objData.Context.DbConnLookup.ExecuteReader(sSQL))
            {

                objXML = base.SysEx;
                objWeeksPerMonth = objXML.CreateElement("weekspermonth");
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        objWeeksPerMonth.InnerText = Conversion.ConvertObjToStr(objReader.GetValue(0));
                    }

                }
            }
			objXML.DocumentElement.AppendChild(objWeeksPerMonth);
            //if(objReader!=null)
            //    objReader.Close();
            // Add DiaryMessage node, if already not present, to avoid Orbeon 'ref' error
            XmlNode objDiaryMessageNode = objSysExDataXmlDoc.SelectSingleNode("/SysExData/DiaryMessage");

            if (objDiaryMessageNode == null)
            {
                base.CreateSysExData("DiaryMessage");
            }
			//Shruti Choudhary updated code for mits 10384
            //if (this.SysFormStack.SelectSingleNode("//ScreenFlowStack/ScreenFlow[2]/SysFormName") != null)
            //{
            //    ((XmlElement)base.SysView.SelectSingleNode("//internal[@name='SysFormPForm']")).SetAttribute("value",
            //        this.SysFormStack.SelectSingleNode("//ScreenFlowStack/ScreenFlow[2]/SysFormName").InnerText);
            //}

            objSysExDataXmlDoc = null;

            //MITS 28903: Raman
            //Removing Employment Info tab in case parent is piemployee
            if (PiInjury.Context.LocalCache.GetShortCode(PiInjury.PiTypeCode) == "E")
            {
                base.AddKillNode("employmentinfo");
            }
		}

        internal override bool ValidateRequiredViewFields(XmlDocument propertyStore)
        {
            return base.ValidateRequiredViewFields(propertyStore);
        }
        public override void OnValidate(ref bool Cancel)
        {
            bool bError = false;
            string sSQL = string.Empty;
            DbConnection objDbConnection = null;

            Event objEvent = (Event)this.PiInjury.Context.Factory.GetDataModelObject("Event", false);
            objEvent.MoveTo(this.m_iEventId);
            string sEventDateFromDB = objEvent.DateOfEvent;

            // Ideally this date formatting should be done by presentation server
            // depending upon user locale. We are formatting it for validation error message display. 
            string sEventDate = Conversion.ToDate(sEventDateFromDB).ToString("MM/dd/yyyy");

            string sToday = Conversion.ToDbDate(System.DateTime.Now);
            string sTime = Conversion.GetTime(System.DateTime.Now.ToShortTimeString());
            //rsushilaggar MITS 18991 Date 10/05/2010 
            string sHiredDateText = Globalization.GetString("Field.DateHired", base.ClientId, sLangCode) + ": " + Conversion.ToDate(PiInjury.DateHired).ToString("MM/dd/yyyy");
            string sBirthDateText = Globalization.GetString("Field.BirthDate", base.ClientId, sLangCode) + ": " + Conversion.ToDate(PiInjury.PiEntity.BirthDate).ToString("MM/dd/yyyy");



            if (PiInjury.DateHired != "")
            {
                if (PiInjury.DateHired.CompareTo(sToday) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId, sLangCode), Globalization.GetString("Field.DateHired", base.ClientId, sLangCode), System.DateTime.Now.ToShortDateString()),
                        BusinessAdaptorErrorType.Error);

                    bError = true;
                }
                if (PiInjury.DateHired.CompareTo(PiInjury.PiEntity.BirthDate) < 0)
                {
                    //rsushilaggar MITS 18991 Date 10/05/2010
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId, sLangCode), Globalization.GetString("Field.DateHired", base.ClientId, sLangCode), sBirthDateText),
                        BusinessAdaptorErrorType.Error);

                    bError = true;
                }
                if (PiInjury.DateHired.CompareTo(sEventDateFromDB) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeLessThanEventDate", base.ClientId, sLangCode), Globalization.GetString("Field.DateHired", base.ClientId, sLangCode), sEventDate),
                        BusinessAdaptorErrorType.Error);

                    bError = true;
                }
            }

            if (PiInjury.TermDate != "")
            {
                if (PiInjury.TermDate.CompareTo(sToday) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId, sLangCode), Globalization.GetString("Field.TermDate", base.ClientId, sLangCode), System.DateTime.Now.ToShortDateString()),
                        BusinessAdaptorErrorType.Error);

                    bError = true;
                }
                if (PiInjury.TermDate.CompareTo(PiInjury.PiEntity.BirthDate) < 0)
                {
                    //rsushilaggar MITS 18991 Date 10/05/2010
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId, sLangCode), Globalization.GetString("Field.TermDate", base.ClientId, sLangCode), sBirthDateText),
                        BusinessAdaptorErrorType.Error);

                    bError = true;
                }
                if (PiInjury.TermDate.CompareTo(PiInjury.DateHired) < 0)
                {
                    //rsushilaggar MITS 18991 Date 10/05/2010
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId, sLangCode), Globalization.GetString("Field.TermDate", base.ClientId, sLangCode), sHiredDateText),
                        BusinessAdaptorErrorType.Error);

                    bError = true;
                }
                if (PiInjury.TermDate.CompareTo(sEventDateFromDB) < 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThanEventDate", base.ClientId, sLangCode), Globalization.GetString("Field.TermDate", base.ClientId, sLangCode), sEventDate),
                        BusinessAdaptorErrorType.Error);

                    bError = true;
                }

            }

            if (PiInjury.DateOfDeath != "")
            {
                if (PiInjury.DateOfDeath.CompareTo(sToday) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId, sLangCode), Globalization.GetString("Field.DateOfDeath", base.ClientId, sLangCode), System.DateTime.Now.ToShortDateString()),
                        BusinessAdaptorErrorType.Error);

                    bError = true;
                }
                if (PiInjury.DateOfDeath.CompareTo(PiInjury.PiEntity.BirthDate) < 0)
                {
                    //rsushilaggar MITS 18991 Date 10/05/2010
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId, sLangCode), Globalization.GetString("Field.DateOfDeath", base.ClientId, sLangCode), sBirthDateText),
                        BusinessAdaptorErrorType.Error);

                    bError = true;
                }
                if (PiInjury.DateOfDeath.CompareTo(PiInjury.DateHired) < 0)
                {
                    //rsushilaggar MITS 18991 Date 10/05/2010
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId, sLangCode), Globalization.GetString("Field.DateOfDeath", base.ClientId, sLangCode), sHiredDateText),
                        BusinessAdaptorErrorType.Error);

                    bError = true;
                }

            }

            

            //Neha begin

            if (PiInjury.FileClosingdate != "")
            {
                if (PiInjury.FileClosingdate.CompareTo(sToday) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId, sLangCode), Globalization.GetString("Field.FileClosingDate", base.ClientId, sLangCode), System.DateTime.Now.ToShortDateString()),
                        BusinessAdaptorErrorType.Error);

                    bError = true;
                }
                if (PiInjury.FileClosingdate.CompareTo(PiInjury.PiEntity.BirthDate) < 0)
                {
                    //rsushilaggar MITS 18991 Date 10/05/2010
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId, sLangCode), Globalization.GetString("Field.FileClosingDate", base.ClientId, sLangCode), sBirthDateText),
                        BusinessAdaptorErrorType.Error);

                    bError = true;
                }
                if (PiInjury.FileClosingdate.CompareTo(sEventDateFromDB) < 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThanEventDate", base.ClientId, sLangCode), Globalization.GetString("Field.FileClosingDate", base.ClientId, sLangCode), sEventDate),
                        BusinessAdaptorErrorType.Error);

                    bError = true;
                }
            }
            try
            {

                /*
                if (PiInjury != null && PiInjury.PiEid == 0)  // Added  MITS 9052
                {


                    sSQL = "SELECT EMPLOYEE_NUMBER FROM EMPLOYEE WHERE EMPLOYEE_NUMBER = " + Utilities.FormatSqlFieldValue(PiInjury.EmployeeNumber.Trim());
                    objDbConnection = DbFactory.GetDbConnection(PiInjury.Context.DbConn.ConnectionString);
                    if (objDbConnection != null)
                        objDbConnection.Open();
                    Object objDuplicate = objDbConnection.ExecuteScalar(sSQL);
                    if (objDuplicate != null)
                    {
                        Errors.Add(Globalization.GetString("ValidationError"),
                            String.Format(Globalization.GetString("Validation.Duplicate"), Globalization.GetString("Field.EmployeeNumber"), PiInjury.EmployeeNumber),
                            BusinessAdaptorErrorType.Error);
                        bError = true;
                    }

                    objDbConnection.Close();
                }
                 */
             }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (objDbConnection != null)
                    objDbConnection.Dispose();
            }
            objEvent = null;

            // Return true if there were validation errors
            Cancel = bError;
        }
        public override void BeforeDelete(ref bool Cancel)
        {
            
           

        }
        
            

          
           

           
            

              
                
          
           
        
    }
}
