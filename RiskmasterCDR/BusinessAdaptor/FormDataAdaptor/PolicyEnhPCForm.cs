﻿
using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using Riskmaster.Settings;
using Riskmaster.Application.FundManagement;
using Riskmaster.Application.EnhancePolicy;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Text;

namespace Riskmaster.BusinessAdaptor
{
    /// <summary>
    /// Author  : Mridul Bansal
    /// Date    : 11/23/09.
    /// Replica of PolicyEnfForm.cs. Written afresh as now it is tied to a particular LOB.
    /// </summary>
    public class PolicyEnhPCForm : PolicyEnhForm
    {                    
        
        #region Global Varaibles and Properties
        const string CLASS_NAME = "PolicyEnh";
        public PolicyEnhPCForm(FormDataAdaptor fda) : base(fda)
        {
            base.m_ClassName = CLASS_NAME;
        }
        private UserLogin UserLogin { get { return PolicyEnh.Context.RMUser; } }
        #endregion 

        //umesh
        public override void InitNew()
        {
            base.InitNew();
            PolicyEnh.PolicyType = EnhancePolicyManager.LocalCache.GetCodeId("PC", "POLICY_LOB");
           
            //Sumit - Start(03/16/2010) - MITS# 18229 - Variable to store Deleted UAR Ids of selected Exposure.
            if (SysEx.DocumentElement.SelectSingleNode("ExposureListGrid_DeletedUarRowIds") == null)
                CreateSysExData("ExposureListGrid_DeletedUarRowIds");
            //Sumit - End

        }
        //End Umesh

        protected override void UpdateExpsoureList()
        {
            //Added rjhamb for Updating Session Object on Policy State and Date Change
            string sAction = string.Empty;
            string sTransType = string.Empty;
            string sOrgHier = string.Empty;
            PolicyXExpEnh objTempExpsoure = null;
            XmlNode objNode = null;
            SessionManager objSessionManager = null;
            //Added rjhamb for Updating Session Object on Policy State and Date Change
            string sNewAddedExposureSessionId = base.GetSysExDataNodeText("NewAddedExposureSessionId");
            int iDeletedRowExposureId = base.GetSysExDataNodeInt("DeletedRowExposureId");
            XmlNode objExposureCollectionNode = base.SysEx.DocumentElement.SelectSingleNode("ExposureList");
            PolicyXExpEnh objUpdateExpsoure = (PolicyXExpEnh)EnhancePolicyManager.DataModelFactory.GetDataModelObject("PolicyXExpEnh", false);
            int iTransNumber = base.GetSysExDataNodeInt("/SysExData//SelectedTransactionId", true);
            objExposuresUniqueIds.Clear();
            //Sumit - Start(05/13/2010) - MITS# 20483- Split Value for deleted Schedule Ids.
            bool m_bSuccess = false;
            String[] arrScheduleFinal = null;
            objExposureSchedule.Clear();
            objUarSchedule.Clear();
            string sSchdIdsPostDelete = base.GetSysExDataNodeText("ExposureListGrid_ScheduleRowIdsPostDelete");

            if (!String.IsNullOrEmpty(sSchdIdsPostDelete))
            {
                String[] arrSchdIdsPostDelete = sSchdIdsPostDelete.Split('$');
                for (int iRecCount = 0; iRecCount < arrSchdIdsPostDelete.Length; iRecCount++)
                {
                    String[] arrExpUar = arrSchdIdsPostDelete[iRecCount].Split('?');
                    if (arrExpUar.Length > 1)
                    {
                        objExposureSchedule.Add(Conversion.CastToType<int>(arrExpUar[0], out m_bSuccess), arrExpUar[1]);
                    }
                }
            }
            //Sumit - End

            //Sumit - Start(05/05/2010) - MITS# 20483- Variable to store Schedule Session Ids.
            String[] arrScheduleTemp = null;
            objScheduleSessionIds.Clear();
            objScheduleSessionIDs.Clear();
            string sUarScheduleSessionIds = base.GetSysExDataNodeText("ExposureUarSchedule");
            string sDeletedUarRowIds = base.GetSysExDataNodeText("ExposureListGrid_DeletedUarRowIds");
            if (!String.IsNullOrEmpty(sUarScheduleSessionIds))
            {
                String[] arrUarScheduleSessionIds = sUarScheduleSessionIds.Split('%');
                for (int iSchd = 0; iSchd < arrUarScheduleSessionIds.Length; iSchd++)
                {
                    String[] arrScheduleSessionIds = arrUarScheduleSessionIds[iSchd].Split('^');
                    objScheduleSessionIds.Add(Conversion.CastToType<int>(arrScheduleSessionIds[0], out m_bSuccess), arrScheduleSessionIds[1]);
                }
            }
            //Sumit - End

            //Sumit - Start(03/16/2010) - MITS# 18229 - Clear UAR Session IDs
            objExposureUarSessionIds.Clear();
            //Sumit - End

            //Sumit - Start(03/16/2010) - MITS# 18229
            XmlDocument objReturnDoc = null;
            XmlElement objRoot = null;
            string sUarSessionIds = base.GetSysExDataNodeText("ExposureListGrid_UarSessionIds");
            String[] arrUar = null;
            String[] arrSched = null;
            string[] arrUarDtl = null;
            int iPolicyId = 0;
            string[] arrUAR = null;
            bool bIsSuccess = false;
            int iUARId = 0;
            string[] arrScheduleListGridScheduleRowIds = null;
            string[] arrUARSchdSession = null;
            ArrayList arrDeletedUarRowIds = new ArrayList();
            ArrayList arrDeletedScheduleIds = new ArrayList();

            string[] arrDeletedUarRowIDs = sDeletedUarRowIds.Split('|');

            for (int i = 0; i < arrDeletedUarRowIDs.Length; i++)
                arrDeletedUarRowIds.Add(arrDeletedUarRowIDs[i]);
            //Sumit - End
            //Parijat:Mits 9887
            ArrayList arrExposure = new ArrayList();
            // For Edit Rows.
            foreach (XmlNode objOptionNode in objExposureCollectionNode.SelectNodes("option"))
            {
                string sSessionId = objOptionNode.SelectSingleNode("SessionId").InnerText;
                string[] arrSessionID = sSessionId.Split(new char[] { '~' });
                string sExpUarSessionIDs = string.Empty;
                string sExpUarScheduleSessionIDs = string.Empty;
                int iExposureId = Conversion.ConvertStrToInteger(objOptionNode.SelectSingleNode("ExposureId").InnerText);

                //Sumit - Start(03/16/2010) - MITS# 18229 - Store UAR Ids.
                if (objOptionNode.SelectSingleNode("UarSessionIds") != null)
                {
                    sExpUarSessionIDs = objOptionNode.SelectSingleNode("UarSessionIds").InnerText;
                }
                //Sumit-End

                //Sumit - Start(05/04/2010) - MITS# 20483 - Schedule Session Ids
                if (objOptionNode.SelectSingleNode("ScheduleSessionIds") != null)
                {
                    sExpUarScheduleSessionIDs = objOptionNode.SelectSingleNode("ScheduleSessionIds").InnerText;
                }
                if (!String.IsNullOrEmpty(sExpUarScheduleSessionIDs))
                    arrScheduleTemp = sExpUarScheduleSessionIDs.Split('|');
                if (!string.IsNullOrEmpty(sSessionId))
                {
                    if (arrSessionID.Length > 1)
                    {
                        arrUARSchdSession = arrSessionID[1].Split('$');
                    }
                    else
                    {
                        arrUARSchdSession = sUarSessionIds.Split('$');
                    }
                }
                if (iDeletedRowExposureId == iExposureId)
                {
                    PolicyEnh.PolicyXExposureEnhList.Remove(iExposureId);
                    continue;
                }
                //Parijat:Mits 9887 
                //Just maintaining the status which has to be updated for the grid 
                //through the xml
                arrExposure.Add(iExposureId.ToString());

                if (!string.IsNullOrEmpty(arrSessionID[0]))
                {
                    // Replace the object from the session object.                                         
                    string sSessionRawData = (string)base.m_fda.getCachedObject(arrSessionID[0]);
                    XmlDocument objPropertyStore = new XmlDocument();
                    objPropertyStore.LoadXml(sSessionRawData);

                    //Parijat: Mits 9887
                    //checking if the previous Exposure which was added is missing at the object 
                    //level then adding it once again else updating it from the previous session.  
                    // npadhy MITS 17067 Accessing the Objects using Indices was creating problems so Accessing as per this.
                    if (iExposureId > 0)
                    {
                        PolicyEnh.PolicyXExposureEnhList[iExposureId].Supplementals.DataChanged = true;
                        // Overwrite the object ExposureId, this might be the old one, while object added in collection
                        // Datamodel generates it new unique Id. 
                        PolicyEnh.PolicyXExposureEnhList[iExposureId].ExposureId = iExposureId;
                        PolicyEnh.PolicyXExposureEnhList[iExposureId].PopulateObject(objPropertyStore);
                        //Sumit - Start(03/16/2010) - MITS# 18229 - Update object for UAR Ids as well.
                        if (objPropertyStore.SelectSingleNode("/PolicyXExpEnh/PolicyId") != null)
                        {
                            iPolicyId = Conversion.CastToType<int>(objPropertyStore.SelectSingleNode("/PolicyXExpEnh/PolicyId").InnerText, out bIsSuccess);
                        }
                        if (!String.IsNullOrEmpty(sUarSessionIds) && String.IsNullOrEmpty(sNewAddedExposureSessionId) && arrUARSchdSession == null)
                        {
                            arrUARSchdSession = sUarSessionIds.Split('$');
                        }
                        if (arrUARSchdSession != null)
                        {
                            for (int iUarSchdCount = 0; iUarSchdCount < arrUARSchdSession.Length; iUarSchdCount++)
                            {
                                if (!String.IsNullOrEmpty(arrUARSchdSession[iUarSchdCount]))
                                {
                                        arrUAR = arrUARSchdSession[iUarSchdCount].Split('^');
                                    arrUar = arrUAR[0].Split('|');

                                    for (int iUar = 0; iUar < arrUar.Length; iUar++)
                                    {
                                        if (!String.IsNullOrEmpty(arrUar[iUar]))
                                        {
                                            string sSessionRawUarData = (string)base.m_fda.getCachedObject(arrUar[iUar]);
                                            XmlDocument objPropStore = new XmlDocument();
                                            objPropStore.LoadXml(sSessionRawUarData);
                                            PolicyXUar objUpdateUAROth = (PolicyXUar)EnhancePolicyManager.DataModelFactory.GetDataModelObject("PolicyXUar", false);

                                            StartDocument(ref objReturnDoc, ref objRoot, "PolicyXUar");
                                            if (objPropStore.SelectSingleNode("/PolicyXUar/UarRowID") != null)
                                            {
                                                CreateAndSetElement(objRoot, "UarRowId", objPropStore.SelectSingleNode("/PolicyXUar/UarRowID").InnerText.ToString());
                                            }
                                            else
                                            {
                                                CreateAndSetElement(objRoot, "UarRowId", objUpdateUAROth.UarRowId.ToString());
                                            }
                                            if (iPolicyId > 0)
                                            {
                                                CreateAndSetElement(objRoot, "PolicyId", Convert.ToString(iPolicyId));
                                            }
                                            else
                                            {
                                                CreateAndSetElement(objRoot, "PolicyId", objUpdateExpsoure.PolicyId.ToString());
                                            }
                                            if (iExposureId > 0)
                                            {
                                                CreateAndSetElement(objRoot, "ExposureId", Convert.ToString(iExposureId));
                                            }
                                            else
                                            {
                                                CreateAndSetElement(objRoot, "ExposureId", objUpdateExpsoure.ExposureId.ToString());
                                            }
                                            CreateAndSetElement(objRoot, "UarId", objPropStore.SelectSingleNode("/PolicyXUar/UarId").InnerText.ToString());
                                            CreateAndSetElement(objRoot, "PolicyStatusCode", EnhancePolicyManager.CCacheFunctions.GetCodeIDWithShort("Q", "POLICY_STATUS").ToString());
                                            if (objPropStore.SelectSingleNode("/PolicyXUar/ScheduleListGridScheduleRowIds") != null)
                                            {
                                                string sScheduleListGridScheduleRowIds = objPropStore.SelectSingleNode("/PolicyXUar/ScheduleListGridScheduleRowIds").InnerText.ToString();
                                                arrScheduleListGridScheduleRowIds = sScheduleListGridScheduleRowIds.Split('|');
                                            }
                                            if (objPropStore.SelectSingleNode("/PolicyXUar/UarRowID") != null)
                                            {
                                                iUARId = Conversion.CastToType<int>(objPropStore.SelectSingleNode("/PolicyXUar/UarRowID").InnerText.ToString(), out bIsSuccess);
                                                if (iUARId > 0)
                                                {
                                                    PolicyEnh.PolicyXExposureEnhList[iExposureId].PolicyXUarList[iUARId].UarRowId = iUARId;
                                                    PolicyEnh.PolicyXExposureEnhList[iExposureId].PolicyXUarList[iUARId].PopulateObject(objReturnDoc);
                                                }
                                                else
                                                {
                                                    objUpdateUAROth.PopulateObject(objReturnDoc);
                                                    PolicyEnh.PolicyXExposureEnhList[iExposureId].PolicyXUarList.Add(objUpdateUAROth);
                                                    iUARId = objUpdateUAROth.UarRowId;
                                                }
                                            }
                                            else
                                            {
                                                objUpdateUAROth.PopulateObject(objReturnDoc);
                                                PolicyEnh.PolicyXExposureEnhList[iExposureId].PolicyXUarList.Add(objUpdateUAROth);
                                                iUARId = objUpdateUAROth.UarRowId;
                                            }
                                            if (arrUAR.Length > 1)
                                            {
                                                arrSched = arrUAR[1].Split('|');
                                            }
                                            if (arrSched != null)
                                            {
                                            for (int iSchedule = 0; iSchedule < arrSched.Length; iSchedule++)
                                            {
                                                if (!String.IsNullOrEmpty(arrSched[iSchedule]))
                                                {
                                                    string sSessionRawScheduleData = (string)base.m_fda.getCachedObject(arrSched[iSchedule]);
                                                    int iScheduleListGridScheduleRowIds = 0;
                                                    XmlDocument objSchdStore = new XmlDocument();
                                                    objSchdStore.LoadXml(sSessionRawScheduleData);
                                                    if (arrScheduleListGridScheduleRowIds != null)
                                                    {
                                                        iScheduleListGridScheduleRowIds = Conversion.CastToType<int>(arrScheduleListGridScheduleRowIds[iSchedule], out bIsSuccess);
                                                    }
                                                    PolicyXPschedEnh objUpdateSchdOth = (PolicyXPschedEnh)EnhancePolicyManager.DataModelFactory.GetDataModelObject("PolicyXPschedEnh", false);

                                                    StartDocument(ref objReturnDoc, ref objRoot, "PolicyXPschedEnh");
                                                    if (iScheduleListGridScheduleRowIds > 0)
                                                    {
                                                        CreateAndSetElement(objRoot, "PschedRowID", Convert.ToString(iScheduleListGridScheduleRowIds));
                                                    }
                                                    else
                                                    {
                                                        CreateAndSetElement(objRoot, "PschedRowID", objUpdateSchdOth.PschedRowID.ToString());
                                                    }
                                                    if (iPolicyId > 0)
                                                    {
                                                        CreateAndSetElement(objRoot, "PolicyId", Convert.ToString(iPolicyId));
                                                    }
                                                    else
                                                    {
                                                        CreateAndSetElement(objRoot, "PolicyId", objUpdateUAROth.PolicyId.ToString());
                                                    }
                                                    CreateAndSetElement(objRoot, "UarId", iUARId.ToString());
                                                    CreateAndSetElement(objRoot, "Name", objSchdStore.SelectSingleNode("/Schedule/Name").InnerText.ToString());
                                                    CreateAndSetElement(objRoot, "Amount", objSchdStore.SelectSingleNode("/Schedule/Amount").InnerText.ToString());

                                                    if (iScheduleListGridScheduleRowIds > 0)
                                                    {
                                                        PolicyEnh.PolicyXExposureEnhList[iExposureId].PolicyXUarList[iUARId].PolicyXPschedEnhList[iScheduleListGridScheduleRowIds].PschedRowID = iScheduleListGridScheduleRowIds;
                                                        PolicyEnh.PolicyXExposureEnhList[iExposureId].PolicyXUarList[iUARId].PolicyXPschedEnhList[iScheduleListGridScheduleRowIds].PopulateObject(objReturnDoc);
                                                    }
                                                    else
                                                    {
                                                        objUpdateSchdOth.PopulateObject(objReturnDoc);
                                                        PolicyEnh.PolicyXExposureEnhList[iExposureId].PolicyXUarList[iUARId].PolicyXPschedEnhList.Add(objUpdateSchdOth);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                }
                                //Sumit-End
                            }
                        }
                    }
                    else
                    {
                        PolicyXExpEnh objUpdateExpsoure1 = (PolicyXExpEnh)EnhancePolicyManager.DataModelFactory.GetDataModelObject("PolicyXExpEnh", false);
                        objUpdateExpsoure1.PopulateObject(objPropertyStore);
                        objUpdateExpsoure1.Supplementals.DataChanged = true;
                        // Overwrite the object ExposureId, this might be the old one, while object added in collection
                        // Datamodel generates it new unique Id. 
                        objUpdateExpsoure1.ExposureId = iExposureId;
                        PolicyEnh.PolicyXExposureEnhList.Add(objUpdateExpsoure1, true);
                        if (arrUARSchdSession != null)
                        {
                            for (int iUarSchdCount = 0; iUarSchdCount < arrUARSchdSession.Length; iUarSchdCount++)
                            {
                                if (!String.IsNullOrEmpty(arrUARSchdSession[iUarSchdCount]))
                                {
                                        arrUAR = arrUARSchdSession[iUarSchdCount].Split('^');
                                    arrUar = arrUAR[0].Split('|');

                                    for (int iUar = 0; iUar < arrUar.Length; iUar++)
                                    {
                                        if (!String.IsNullOrEmpty(arrUar[iUar]))
                                        {
                                            string sSessionRawUarData = (string)base.m_fda.getCachedObject(arrUar[iUar]);
                                            XmlDocument objPropStore = new XmlDocument();
                                            objPropStore.LoadXml(sSessionRawUarData);
                                            PolicyXUar objUpdateUAROth = (PolicyXUar)EnhancePolicyManager.DataModelFactory.GetDataModelObject("PolicyXUar", false);

                                            StartDocument(ref objReturnDoc, ref objRoot, "PolicyXUar");
                                            CreateAndSetElement(objRoot, "UarRowId", objUpdateUAROth.UarRowId.ToString());
                                            CreateAndSetElement(objRoot, "PolicyId", objUpdateExpsoure.PolicyId.ToString());
                                            CreateAndSetElement(objRoot, "ExposureId", objUpdateExpsoure.ExposureId.ToString());
                                            CreateAndSetElement(objRoot, "UarId", objPropStore.SelectSingleNode("/PolicyXUar/UarId").InnerText.ToString());
                                            CreateAndSetElement(objRoot, "PolicyStatusCode", EnhancePolicyManager.CCacheFunctions.GetCodeIDWithShort("Q", "POLICY_STATUS").ToString());
                                            objUpdateUAROth.PopulateObject(objReturnDoc);
                                            PolicyEnh.PolicyXExposureEnhList[iExposureId].PolicyXUarList.Add(objUpdateUAROth);
                                            int iUarRowID = objUpdateUAROth.UarRowId;
                                            if (arrUAR.Length > 1)
                                            {
                                                arrSched = arrUAR[1].Split('|');
                                            }
                                            if (arrSched != null)
                                            {
                                                for (int iSchedule = 0; iSchedule < arrSched.Length; iSchedule++)
                                                {
                                                    if (!String.IsNullOrEmpty(arrSched[iSchedule]))
                                                    {
                                                        string sSessionRawScheduleData = (string)base.m_fda.getCachedObject(arrSched[iSchedule]);
                                                        XmlDocument objSchdStore = new XmlDocument();
                                                        objSchdStore.LoadXml(sSessionRawScheduleData);
                                                        PolicyXPschedEnh objUpdateSchdOth = (PolicyXPschedEnh)EnhancePolicyManager.DataModelFactory.GetDataModelObject("PolicyXPschedEnh", false);

                                                        StartDocument(ref objReturnDoc, ref objRoot, "PolicyXPschedEnh");
                                                        CreateAndSetElement(objRoot, "PschedRowID", objUpdateSchdOth.PschedRowID.ToString());
                                                        CreateAndSetElement(objRoot, "PolicyId", objUpdateUAROth.PolicyId.ToString());
                                                        CreateAndSetElement(objRoot, "UarId", iUarRowID.ToString());
                                                        if (objSchdStore.SelectSingleNode("/Schedule/Name") != null)
                                                        {
                                                            CreateAndSetElement(objRoot, "Name", objSchdStore.SelectSingleNode("/Schedule/Name").InnerText.ToString());
                                                        }
                                                        if (objSchdStore.SelectSingleNode("/Schedule/Amount") != null)
                                                        {
                                                            CreateAndSetElement(objRoot, "Amount", objSchdStore.SelectSingleNode("/Schedule/Amount").InnerText.ToString());
                                                        }

                                                        objUpdateSchdOth.PopulateObject(objReturnDoc);
                                                        PolicyEnh.PolicyXExposureEnhList[iExposureId].PolicyXUarList[iUarRowID].PolicyXPschedEnhList.Add(objUpdateSchdOth);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }// End of if by Amitosh for Mits 20820 (2/2/2011)
                            }// End of foreach by Amitosh for Mits 20820 (2/2/2011)
                        }// End of if by Amitosh for Mits 20820 (2/2/2011)
                                    //Sumit-End
                                    //Added rjhamb for Updating Session Object on Policy State and Date Change
                                    if (base.GetSysExDataNodeText("IsQuote") == "True")
                                    {
                                        sAction = base.GetSysExDataNodeText("/SysExData//" + EnhancePolicyManager.POST_BACK_ACTION);
                                        if (sAction == "POLICY_DATE_CHANGE" || sAction == "POLICY_STATE_CHANGE" )
                                        {
                                            objTempExpsoure = (PolicyXExpEnh)EnhancePolicyManager.DataModelFactory.GetDataModelObject("PolicyXExpEnh", false);
                                            objSessionManager = base.m_fda.GetSessionObject();
                                            objUpdateExpsoure1.EffectiveDate = base.GetSysExDataNodeText("EffectiveDate");
                                            objUpdateExpsoure1.ExpirationDate = base.GetSysExDataNodeText("ExpirationDate");
                                            sTransType = EnhancePolicyManager.CCacheFunctions.GetShortCode(objUpdateExpsoure1.TransactionType);
                                            sOrgHier = PolicyEnh.PolicyXInsuredEnh.ToString();
                                            sOrgHier = sOrgHier.Replace(" ", ",");
                                            objTempExpsoure = objUpdateExpsoure1;
                                            EnhancePolicyManager.Rate(ref objTempExpsoure, PolicyEnh.State, EnhancePolicyManager.RoundFlag, objTempExpsoure.PrAnnPremAmt, objTempExpsoure.FullAnnPremAmt, sTransType, DateTime.MinValue, PolicyEnh.PolicyType, sOrgHier);
                                            objNode = objPropertyStore.SelectSingleNode("PolicyXExpEnh/ExposureRate");
                                            if (objNode != null)
                                            {
                                                objNode.InnerText = objTempExpsoure.ExposureRate.ToString();
                                            }
                                            objNode = objPropertyStore.SelectSingleNode("PolicyXExpEnh/ExposureBaseRate");
                                            if (objNode != null)
                                            {
                                                objNode.InnerText = objTempExpsoure.ExposureBaseRate.ToString();
                                            }
                                            objNode = objPropertyStore.SelectSingleNode("PolicyXExpEnh/PremAdjAmt");
                                            if (objNode != null)
                                            {
                                                objNode.InnerText = objTempExpsoure.PremAdjAmt.ToString();
                                            }
                                            objNode = objPropertyStore.SelectSingleNode("PolicyXExpEnh/PrAnnPremAmt");
                                            if (objNode != null)
                                            {
                                                objNode.InnerText = objTempExpsoure.PrAnnPremAmt.ToString();
                                            }
                                            objNode = objPropertyStore.SelectSingleNode("PolicyXExpEnh/FullAnnPremAmt");
                                            if (objNode != null)
                                            {
                                                objNode.InnerText = objTempExpsoure.FullAnnPremAmt.ToString();
                                            }

                                            // Added by Amitosh for mits 20820 (2/7/2011) 
                                            objPropertyStore.SelectSingleNode("PolicyXExpEnh/EffectiveDate").InnerText = base.GetSysExDataNodeText("EffectiveDate");
                                            objPropertyStore.SelectSingleNode("PolicyXExpEnh/ExpirationDate").InnerText = base.GetSysExDataNodeText("ExpirationDate");
                                            // end Amitosh
                                            objSessionManager.SetBinaryItem(arrSessionID[0], Utilities.BinarySerialize(objPropertyStore.OuterXml), base.ClientId);

                                        }
                                    }
                                    //Added rjhamb for Updating Session Object on Policy State and Date Change
                                }
                            //} Amitosh Mits 20820 (2/2/2011) 
                    //} Amitosh Mits 20820 (2/2/2011) 

                    //} Amitosh Mits 20820 (2/2/2011) 

                    //PolicyEnh.PolicyXExposureEnhList[iExposureId].Supplementals.DataChanged = true ;
                    // Overwrite the object ExposureId, this might be the old one, while object added in collection
                    // Datamodel generates it new unique Id. 
                    //PolicyEnh.PolicyXExposureEnhList[iExposureId].ExposureId = iExposureId;

                    // Add new session key.
                    if (arrSessionID.Length == 1)
                    {
                        if (sSessionId.Contains("~"))
                        {
                            sSessionId.TrimEnd('~');
                        }

                        objExposuresUniqueIds.Add(iExposureId, sSessionId);
                        objScheduleSessionIDs.Add(iExposureId, sExpUarScheduleSessionIDs);
                        objExposureUarSessionIds.Add(iExposureId, sUarSessionIds);
                    }
                    else
                    {
                        objExposuresUniqueIds.Add(iExposureId, arrSessionID[0]);
                        objScheduleSessionIDs.Add(iExposureId, sExpUarScheduleSessionIDs);
                        objExposureUarSessionIds.Add(iExposureId, arrSessionID[1]);
                    }
                    //Sumit-End

                }
            }
            //Parijat:Mits 9887
            //Just updating the object status with ,the grid data .
            //Using the arrExposure which was being used to maintain the required info.
            //Also a check is placed,in order to avoid the code from running in case the event is "Transaction Change" after the amend , 
            //since this particular case requires the page to be loaded from the beginning and hence no object updation is required.
            string sPostBackAction = base.GetSysExDataNodeText("/SysExData//" + EnhancePolicyManager.POST_BACK_ACTION);
            //Commented rjhamb for Updating Session Object on Policy State and Date Change
            ////Sumit - MITS#20820
            //string sTransactionType = string.Empty;
            //DateTime reinstDate = DateTime.MinValue;
            //string sOrgH = string.Empty;
            //PolicyXExpEnh objExpsoureNew = (PolicyXExpEnh)EnhancePolicyManager.DataModelFactory.GetDataModelObject("PolicyXExpEnh", false);
            ////End: Sumit
            //Commented rjhamb for Updating Session Object on Policy State and Date Change
            if (sPostBackAction != "TRANSACTION_CHANGE")
            {
                foreach (PolicyXExpEnh item in PolicyEnh.PolicyXExposureEnhList)
                {
                    //Commented rjhamb for Updating Session Object on Policy State and Date Change
                    ////Start (07/06/2010) MITS# 20820:Sumit - Updated dates as user may change them at later stage.
                    //if (base.GetSysExDataNodeText("IsQuote") == "True")
                    //{
                    //    item.EffectiveDate = base.GetSysExDataNodeText("EffectiveDate");
                    //    item.ExpirationDate = base.GetSysExDataNodeText("ExpirationDate");

                    //    if (sPostBackAction == "POLICY_DATE_CHANGE" || sPostBackAction == "POLICY_STATE_CHANGE")
                    //    {
                    //        sTransactionType = EnhancePolicyManager.CCacheFunctions.GetShortCode(item.TransactionType);
                    //        sOrgH = PolicyEnh.PolicyXInsuredEnh.ToString();
                    //        sOrgH = sOrgH.Replace(" ", ",");
                    //        objExpsoureNew = item;
                    //        EnhancePolicyManager.Rate(ref objExpsoureNew, PolicyEnh.State, EnhancePolicyManager.RoundFlag, objExpsoureNew.PrAnnPremAmt, objExpsoureNew.FullAnnPremAmt, sTransactionType, reinstDate, PolicyEnh.PolicyType, sOrgH);
                    //        item.ExposureRate = objExpsoureNew.ExposureRate;
                    //        item.ExposureBaseRate = objExpsoureNew.ExposureBaseRate;
                    //        item.PremAdjAmt = objExpsoureNew.PremAdjAmt;
                    //        item.PrAnnPremAmt = objExpsoureNew.PrAnnPremAmt;
                    //        item.FullAnnPremAmt = objExpsoureNew.FullAnnPremAmt;
                    //    }
                    //}
                    ////End :Sumit
                    //Commented rjhamb for Updating Session Object on Policy State and Date Change
                    
                    if (item.TransactionId == iTransNumber || item.TransactionId == 0)
                    {
                        if (!arrExposure.Contains(item.ExposureId.ToString()))
                        {
                            PolicyEnh.PolicyXExposureEnhList.Remove(Convert.ToInt32(item.ExposureId));
                        }
                    }
                }
            }
            else  // Parijat:Updating objects in case there is a transaction_change,so that the deleted items  
            {     // can be saved effectively for the previously selected transaction in case of transaction_change

                string strPrevTransId = base.GetSysExDataNodeText("/SysExData/PreSelectedTransactionId");
                int iPrevTransId = Conversion.ConvertStrToInteger(strPrevTransId);
                foreach (PolicyXExpEnh item in PolicyEnh.PolicyXExposureEnhList)
                {
                    if (item.TransactionId == iPrevTransId)//|| item.TransactionId == 0)
                    {
                        if (!arrExposure.Contains(item.ExposureId.ToString()))
                        {
                            PolicyEnh.PolicyXExposureEnhList.Remove(Convert.ToInt32(item.ExposureId));
                        }
                    }
                }
            }

            ////Sumit - Start(03/16/2010) - MITS# 18229 - Remove Deleted UAR Ids from the Exposure object.
            foreach (PolicyXExpEnh itemExp in PolicyEnh.PolicyXExposureEnhList)
            {
                foreach (PolicyXUar itemUar in itemExp.PolicyXUarList)
                {
                    if (arrDeletedUarRowIds.Contains(itemUar.UarRowId.ToString()))
                    {
                        if (itemExp.ExposureId > 0)
                        {
                            PolicyEnh.PolicyXExposureEnhList[itemExp.ExposureId].PolicyXUarList.Remove(itemUar.UarRowId);
                        }
                    }
                }
            }
            ////Sumit - End
            //Sumit - Start(05/13/2010) - MITS# 20583 - Remove Deleted Schedule Ids.
            foreach (PolicyXExpEnh itemExp in PolicyEnh.PolicyXExposureEnhList)
            {
                if (objExposureSchedule.ContainsKey(itemExp.ExposureId))
                {
                    string sKeyData = (String)objExposureSchedule[itemExp.ExposureId];
                    if (!String.IsNullOrEmpty(sKeyData))
                    {
                        String[] arrSchdPostDelete = sKeyData.Split('~');
                        for (int iRecCount = 0; iRecCount < arrSchdPostDelete.Length; iRecCount++)
                        {
                            String[] arrUarSchedule = arrSchdPostDelete[iRecCount].Split('^');
                            if (arrUarSchedule.Length > 1)
                            {
                                objUarSchedule.Add(Conversion.CastToType<int>(arrUarSchedule[0], out m_bSuccess), arrUarSchedule[1]);
                            }
                        }
                    }
                }
                foreach (PolicyXUar itemUar in itemExp.PolicyXUarList)
                {
                    if (objUarSchedule.ContainsKey(itemUar.UarRowId))
                    {
                        string sKeyScheduleData = (String)objUarSchedule[itemUar.UarRowId];
                        if (!String.IsNullOrEmpty(sKeyScheduleData))
                        {
                            arrDeletedScheduleIds.Clear();
                            arrScheduleFinal = sKeyScheduleData.Split('|');
                            for (int iCount = 0; iCount < arrScheduleFinal.Length; iCount++)
                            {
                                arrDeletedScheduleIds.Add(arrScheduleFinal[iCount]);
                            }

                            foreach (PolicyXPschedEnh itemSchd in itemUar.PolicyXPschedEnhList)
                            {
                                if (arrDeletedScheduleIds.Contains(itemSchd.PschedRowID.ToString()))
                                {
                                    PolicyEnh.PolicyXExposureEnhList[itemExp.ExposureId].PolicyXUarList[itemUar.UarRowId].PolicyXPschedEnhList.Remove(itemSchd.PschedRowID);
                                    objUarSchedule.Remove(itemUar.UarRowId);
                                    if (objExposureSchedule.ContainsKey(itemExp.ExposureId))
                                    {
                                        if (itemExp.ExposureId > 0)
                                        {
                                            objExposureSchedule.Remove(itemExp.ExposureId);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (arrDeletedUarRowIds.Contains(itemUar.UarRowId.ToString()))
                    {
                        if (itemExp.ExposureId > 0)
                        {
                            PolicyEnh.PolicyXExposureEnhList[itemExp.ExposureId].PolicyXUarList.Remove(itemUar.UarRowId);
                        }
                    }
                }
            }
            //Sumit - End
            // For New Rows
            if (!String.IsNullOrEmpty(sNewAddedExposureSessionId))
            {
                string sSessionRawData = (string)base.m_fda.getCachedObject(sNewAddedExposureSessionId);
                XmlDocument objPropertyStore = new XmlDocument();
                objPropertyStore.LoadXml(sSessionRawData);
                objUpdateExpsoure.PopulateObject(objPropertyStore);

                PolicyEnh.PolicyXExposureEnhList.Add(objUpdateExpsoure);
                int iExposureId = objUpdateExpsoure.ExposureId;

                // Add new session key.
                objExposuresUniqueIds.Add(iExposureId, sNewAddedExposureSessionId);

                //Sumit - Start(03/16/2010) - MITS# 18229 - Update Exposure Children.
                objExposureUarSessionIds.Add(iExposureId, sUarSessionIds);
                string[] arrUarSchd = sUarSessionIds.Split('$');
                for (int iCount = 0; iCount < arrUarSchd.Length; iCount++)
                {
                    if (!String.IsNullOrEmpty(arrUarSchd[iCount]))
                    {
                        arrUarDtl = arrUarSchd[iCount].Split('^');
                    }
                    if (arrUarDtl != null)
                    {
                        arrUar = arrUarDtl[0].Split('|');

                        for (int iUar = 0; iUar < arrUar.Length; iUar++)
                        {
                            if (!String.IsNullOrEmpty(arrUar[iUar]))
                            {
                                string sSessionRawUarData = (string)base.m_fda.getCachedObject(arrUar[iUar]);
                                XmlDocument objPropStore = new XmlDocument();
                                objPropStore.LoadXml(sSessionRawUarData);
                                PolicyXUar objUpdateUAROth = (PolicyXUar)EnhancePolicyManager.DataModelFactory.GetDataModelObject("PolicyXUar", false);

                                StartDocument(ref objReturnDoc, ref objRoot, "PolicyXUar");
                                CreateAndSetElement(objRoot, "UarRowId", objUpdateUAROth.UarRowId.ToString());
                                CreateAndSetElement(objRoot, "PolicyId", objUpdateExpsoure.PolicyId.ToString());
                                CreateAndSetElement(objRoot, "ExposureId", objUpdateExpsoure.ExposureId.ToString());
                                CreateAndSetElement(objRoot, "UarId", objPropStore.SelectSingleNode("/PolicyXUar/UarId").InnerText.ToString());
                                CreateAndSetElement(objRoot, "PolicyStatusCode", EnhancePolicyManager.CCacheFunctions.GetCodeIDWithShort("Q", "POLICY_STATUS").ToString());
                                objUpdateUAROth.PopulateObject(objReturnDoc);
                                PolicyEnh.PolicyXExposureEnhList[iExposureId].PolicyXUarList.Add(objUpdateUAROth);
                                int iUarRowID = objUpdateUAROth.UarRowId;
                                if (arrUarDtl.Length > 1)
                                {
                                    arrSched = arrUarDtl[1].Split('|');
                                }
                                for (int iSchedule = 0; iSchedule < arrSched.Length; iSchedule++)
                                {
                                    if (!String.IsNullOrEmpty(arrSched[iSchedule]))
                                    {
                                        string sSessionRawScheduleData = (string)base.m_fda.getCachedObject(arrSched[iSchedule]);
                                        XmlDocument objSchdStore = new XmlDocument();
                                        objSchdStore.LoadXml(sSessionRawScheduleData);
                                        PolicyXPschedEnh objUpdateSchdOth = (PolicyXPschedEnh)EnhancePolicyManager.DataModelFactory.GetDataModelObject("PolicyXPschedEnh", false);

                                        StartDocument(ref objReturnDoc, ref objRoot, "PolicyXPschedEnh");
                                        CreateAndSetElement(objRoot, "PschedRowID", objUpdateSchdOth.PschedRowID.ToString());
                                        CreateAndSetElement(objRoot, "PolicyId", objUpdateUAROth.PolicyId.ToString());
                                        CreateAndSetElement(objRoot, "UarId", iUarRowID.ToString());
                                        CreateAndSetElement(objRoot, "Name", objSchdStore.SelectSingleNode("/Schedule/Name").InnerText.ToString());
                                        CreateAndSetElement(objRoot, "Amount", objSchdStore.SelectSingleNode("/Schedule/Amount").InnerText.ToString());

                                        objUpdateSchdOth.PopulateObject(objReturnDoc);
                                        PolicyEnh.PolicyXExposureEnhList[iExposureId].PolicyXUarList[iUarRowID].PolicyXPschedEnhList.Add(objUpdateSchdOth);
                                    }
                                }
                            }
                        }
                    }
                }
                //Sumit - End
            }

            double dblMaualPremium = 0;
            // int iTransNumber = base.GetSysExDataNodeInt("/SysExData//SelectedTransactionId", true);

            // Edit Manual Premium in SysEx which would get updated while updating Rating Record. 
            foreach (PolicyXExpEnh objPolicyXExpEnh in PolicyEnh.PolicyXExposureEnhList)
            {
                if (objPolicyXExpEnh.TransactionId == iTransNumber)
                    dblMaualPremium += objPolicyXExpEnh.PrAnnPremAmt;
            }

            XmlElement objManualPremiumNode = (XmlElement)SysEx.SelectSingleNode("/SysExData//PremiumAmounts/ManualPremium");
            if (objManualPremiumNode != null)
                objManualPremiumNode.InnerText = dblMaualPremium.ToString();
            else
            {
                // The Manual Premium node does not exist in SysEx so we create a node of the same name
                XmlNode objPremiumNode = SysEx.SelectSingleNode("/SysExData//PremiumAmounts");
                if (objPremiumNode != null)
                {
                    XmlNode objManualPremium = objPremiumNode.OwnerDocument.CreateElement("ManualPremium");
                    objManualPremium.InnerText = dblMaualPremium.ToString();
                    objPremiumNode.AppendChild(objManualPremium);

                }

            }
        }

        #region Update Policy/Quote Detials

        protected override void SetEntityFieldsDetails(string p_sPolicyStatus, string p_sPolicyType, int p_iTransNumber)
        {
            DbReader objReader = null;
            DbReader objReader2 = null;
            string sSQL = string.Empty;
            bool bDisableAll = false;

            int iGreatestTrans = 0;
            string sOtherType = string.Empty;
            string sOtherStatus = string.Empty;
            string sTransactionStatus = string.Empty;

            bool bEditBroker = false;
            bool bEditInsurer = false;

            // Check authority for insure and broker.
            if (EntityAccessCheck())
            {
                if (PolicyEnh.InsurerEntity.EntityId == 0)
                {
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_PEOPMAINT, RMO_CREATE))
                        bEditInsurer = true;
                }
                else
                {
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_PEOPMAINT, RMO_UPDATE))
                        bEditInsurer = true;
                }
            }
            if (EntityAccessCheck())
            {
                if (PolicyEnh.BrokerEntity.EntityId == 0)
                {
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_PEOPMAINT, RMO_CREATE))
                        bEditBroker = true;
                }
                else
                {
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_PEOPMAINT, RMO_UPDATE))
                        bEditBroker = true;
                }
            }

            structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = false;
            structEnhancePolicyUI.CoverageUI.DeleteCoverageEnabled = false;
            structEnhancePolicyUI.ExposureUI.AddExposureEnabled = false;
            structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled = false;
            structEnhancePolicyUI.PolicyUI.PolicyStatusEnabled = false;

            // Need to find the most recent txn that is not an audit.
            // If that is not tran num we have then we need to disable all fields.

            if (!(p_iTransNumber == 0) && !(PolicyEnh.PolicyXTransEnhList.Count == 0) && !(PolicyEnh.PolicyId == 0))
            {
                sSQL = " SELECT * FROM POLICY_X_TRANS_ENH WHERE POLICY_ID=" + PolicyEnh.PolicyId
                    + " ORDER BY TRANSACTION_ID DESC ";
                objReader = PolicyEnh.Context.DbConnLookup.ExecuteReader(sSQL);

                while (objReader.Read())
                {
                    iGreatestTrans = Conversion.ConvertObjToInt(objReader.GetValue("TRANSACTION_ID"), base.ClientId);
                    sOtherType = EnhancePolicyManager.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objReader.GetValue("TRANSACTION_TYPE"), base.ClientId));
                    sOtherStatus = EnhancePolicyManager.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objReader.GetValue("TRANSACTION_STATUS"), base.ClientId));

                    if (sOtherType == "AU")
                    {
                        if (p_iTransNumber != iGreatestTrans)
                        {
                            if (sOtherStatus == "PR")
                            {
                                structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                                bDisableAll = true;
                                break;
                            }
                        }
                        else
                        {
                            p_sPolicyStatus = "AU";
                            break;
                        }
                    }
                    else
                    {
                        if (p_iTransNumber != iGreatestTrans)
                        {
                            if (sOtherStatus != "PR")
                                structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = true;
                            else
                                structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                            bDisableAll = true;
                            break;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                objReader.Close();
            }

            if (bDisableAll)
            {
                #region Disabled All
                structEnhancePolicyUI.PolicyUI.PolicyNameEnabled = false;
                structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled = false;
                structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled = false;
                structEnhancePolicyUI.PolicyUI.PolicyStateEnabled = false;

                structEnhancePolicyUI.PolicyUI.IssueDateEnabled = true;
                structEnhancePolicyUI.PolicyUI.ReviewDateEnabled = true;
                structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled = false;
                structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled = false;
                structEnhancePolicyUI.PolicyUI.RetroDateEnabled = true;

                structEnhancePolicyUI.PolicyUI.CancelReasonEnabled = false;
                structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = false;
                // Shruti
                structEnhancePolicyUI.PolicyUI.MCOEnabled = false;
                structEnhancePolicyUI.PolicyUI.ConvertQuoteEnabled = false;
                structEnhancePolicyUI.PolicyUI.ReinstatePolicyEnabled = false;
                structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = false;
                structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumEnabled = false;
                structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible = false;
                structEnhancePolicyUI.PolicyUI.AcceptTransacationEnabled = false;
                structEnhancePolicyUI.PolicyUI.DeleteTransactionEnabled = false;
                structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = false;
                structEnhancePolicyUI.CoverageUI.DeleteCoverageEnabled = false;
                structEnhancePolicyUI.CoverageUI.EditCoverageEnabled = true;
                structEnhancePolicyUI.ExposureUI.AddExposureEnabled = false;
                structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled = false;
                structEnhancePolicyUI.ExposureUI.EditExposureEnabled = true;
                structEnhancePolicyUI.McoUI.AddMcoEnabled = true;
                structEnhancePolicyUI.McoUI.DeleteMcoEnabled = true;
                structEnhancePolicyUI.McoUI.EditMcoEnabled = true;

                structEnhancePolicyUI.PolicyUI.BrokerEnabled = bEditBroker;
                structEnhancePolicyUI.PolicyUI.InsurerEnabled = bEditInsurer;

                structEnhancePolicyUI.PolicyUI.PrimaryPolicyEnabled = false;
                structEnhancePolicyUI.PolicyUI.CoverageEnabled = false;
                //csingh7 for MITS 21670 : Start
                if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_VIEW))
                    structEnhancePolicyUI.CoverageUI.CoverageEnabled = true;
                if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_VIEW))
                    structEnhancePolicyUI.ExposureUI.ExposureEnabled = true;
                //csingh7 for MITS 21670 : End

                structEnhancePolicyUI.PolicyUI.BankAccountEnabled = true;
                //Start:Nitin Goel:Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                //MGaba2:MITS 11802
                //structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = true;
                structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_PRINT);
                //End:Nitin Goel,Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                if (p_sPolicyType == "EN")
                {
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = true;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = true;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = true;
                }
                else
                {
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = false;
                }
                structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;

                #endregion
                return;
            }

            foreach (PolicyXTransEnh objPolicyXTransEnh in PolicyEnh.PolicyXTransEnhList)
            {
                if (objPolicyXTransEnh.TransactionId == p_iTransNumber)
                {
                    sTransactionStatus = EnhancePolicyManager.LocalCache.GetShortCode(objPolicyXTransEnh.TransactionStatus);
                    break;
                }
            }

            switch (p_sPolicyStatus)
            {
                // New 
                case "":
                    #region New
                    structEnhancePolicyUI.PolicyUI.PolicyNameEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled = true;
                    //Mridul 11/19/09 MITS:18229. Commented as LOB will be hard coded.
                    //structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyStateEnabled = true;

                    structEnhancePolicyUI.PolicyUI.IssueDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ReviewDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.RetroDateEnabled = true;

                    structEnhancePolicyUI.PolicyUI.CancelReasonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;
                    // Shruti
                    structEnhancePolicyUI.PolicyUI.MCOEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ConvertQuoteEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReinstatePolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.BrokerEnabled = bEditBroker;
                    structEnhancePolicyUI.PolicyUI.InsurerEnabled = bEditInsurer;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible = false;
                    structEnhancePolicyUI.PolicyUI.PrimaryPolicyEnabled = true;
                    structEnhancePolicyUI.PolicyUI.CoverageEnabled = true;

                    //Commented by csingh7 : MITS 20427 Start
                    //if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_ACCESS) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_VIEW) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_CREATE))
                    //    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = true;
                    //else
                    //    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = false;
                    //Commented by csingh7 : MITS 20427 End
                    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_CREATE); //Added by csingh7 : MITS 20427
                    structEnhancePolicyUI.CoverageUI.DeleteCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_DELETE);
                    structEnhancePolicyUI.CoverageUI.EditCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_UPDATE);
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_VIEW))
                        structEnhancePolicyUI.CoverageUI.CoverageEnabled = true;
                    //Commented by csingh7 : MITS 20427 Start
                    //if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_ACCESS) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_VIEW) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_CREATE))
                    //    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = true;
                    //else
                    //    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = false;
                    //Commented by csingh7 : MITS 20427 End
                    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_CREATE);   //Added by csingh7 : MITS 20427
                    structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_DELETE);
                    structEnhancePolicyUI.ExposureUI.EditExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_UPDATE);
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_VIEW))
                        structEnhancePolicyUI.ExposureUI.ExposureEnabled = true;
                    //structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_DELETE);Commented by csingh7 MITS 20427
                    //structEnhancePolicyUI.ExposureUI.EditExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_UPDATE);Commented by csingh7 MITS 20427

                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = false;
                    structEnhancePolicyUI.McoUI.AddMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.DeleteMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.EditMcoEnabled = true;
                    structEnhancePolicyUI.PolicyUI.BankAccountEnabled = true;
                    //Start:Nitin Goel:Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    //MGaba2:MITS 11802
                    //structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_PRINT);
                    //End:Nitin Goel,Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    //Mridul. 11/24/09. MITS:18229. Since this is PC policy so we need to set Expense Modification Factor to FALSE by default.
                    //if( EnhancePolicyManager.CCacheFunctions.GetShortCode( PolicyEnh.PolicyType ) == "GL" )
                    //    structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;
                    //else
                    //    structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = true;
                    break;
                    structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;
                    #endregion
                // Audit
                case "AU":
                    #region  Audit
                    structEnhancePolicyUI.PolicyUI.PolicyNameEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyStateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.IssueDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReviewDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RetroDateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.CancelReasonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = false;

                    structEnhancePolicyUI.PolicyUI.ReinstatePolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = true;
                    // Shruti
                    structEnhancePolicyUI.PolicyUI.MCOEnabled = true;
                    if (sTransactionStatus == "PR")
                    {
                        structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                        structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = false;

                        //Commented by csingh7 : MITS 20427 Start
                        //if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_ACCESS) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_VIEW) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_CREATE))
                        //    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = true;
                        //else
                        //    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = false;
                        //Commented by csingh7 : MITS 20427 End
                        structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_CREATE); //Added by csingh7 MITS 20427
                        structEnhancePolicyUI.CoverageUI.DeleteCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_DELETE);
                        structEnhancePolicyUI.CoverageUI.EditCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_UPDATE);
                        if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_VIEW))
                            structEnhancePolicyUI.CoverageUI.CoverageEnabled = true;
                        //Commented by csingh7 : MITS 20427 Start
                        //if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_ACCESS) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_VIEW) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_CREATE))
                        //    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = true;
                        //else
                        //    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = false;
                        //Commented by csingh7 : MITS 20427 End
                        structEnhancePolicyUI.ExposureUI.AddExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_CREATE);   //Added by csingh7 : MITS 20427
                        structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_DELETE);
                        structEnhancePolicyUI.ExposureUI.EditExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_UPDATE);
                        if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_VIEW))
                            structEnhancePolicyUI.ExposureUI.ExposureEnabled = true;
                        //structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_DELETE);Commented by csingh7 MITS 20427
                        //structEnhancePolicyUI.ExposureUI.EditExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_UPDATE);Commented by csingh7 MITS 20427

                        structEnhancePolicyUI.PolicyUI.AcceptTransacationEnabled = true;
                        structEnhancePolicyUI.PolicyUI.DeleteTransactionEnabled = true;
                        structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                        structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = true;
                        structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = true;
                        structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = true;
                        structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = true;
                        structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                        //Mridul. 11/24/09. MITS:18229. Since this is PC policy so we need to set Expense Modification Factor to FALSE by default.
                        //if (EnhancePolicyManager.CCacheFunctions.GetShortCode(PolicyEnh.PolicyType) == "GL")
                        //    structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;
                        //else
                        //    structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = true;
                        structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;
                    }
                    else
                    {
                        structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = false;
                        structEnhancePolicyUI.CoverageUI.DeleteCoverageEnabled = false;
                        structEnhancePolicyUI.ExposureUI.AddExposureEnabled = false;
                        structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled = false;
                        structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                        structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = false;
                        structEnhancePolicyUI.PolicyUI.AcceptTransacationEnabled = false;
                        structEnhancePolicyUI.PolicyUI.DeleteTransactionEnabled = false;
                        structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                        structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                        structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = true;
                        structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = true;
                        structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = true;
                        //Mridul. 11/24/09. MITS:18229. Since this is PC policy so we need to disable the AUDIT functionality.
                        //if (EnhancePolicyManager.LocalCache.GetShortCode(PolicyEnh.PolicyType) == "WC")
                        //    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_AUDIT);
                        //else
                        //    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                        structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                        structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;
                        //csingh7 for MITS 21670 : Start
                        if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_VIEW))
                            structEnhancePolicyUI.CoverageUI.CoverageEnabled = true;
                        if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_VIEW))
                            structEnhancePolicyUI.ExposureUI.ExposureEnabled = true;
                        //csingh7 for MITS 21670 : End
                    }

                    structEnhancePolicyUI.PolicyUI.BrokerEnabled = bEditBroker;
                    structEnhancePolicyUI.PolicyUI.InsurerEnabled = bEditInsurer;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible = false;
                    structEnhancePolicyUI.PolicyUI.PrimaryPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CoverageEnabled = false;
                    structEnhancePolicyUI.McoUI.AddMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.DeleteMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.EditMcoEnabled = true;
                    structEnhancePolicyUI.PolicyUI.BankAccountEnabled = true;
                    //Start:Nitin Goel:Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    //MGaba2:MITS 11802
                    // structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_PRINT);
                    //End:Nitin Goel,Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    break;
                    #endregion
                // Quote
                case "Q":
                    #region Quote
                    structEnhancePolicyUI.PolicyUI.PolicyNameEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled = true;
                    //Mridul 11/19/09 MITS:18229. Commented as LOB will be hard coded.
                    //structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyStateEnabled = true;

                    structEnhancePolicyUI.PolicyUI.IssueDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ReviewDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.RetroDateEnabled = true;

                    structEnhancePolicyUI.PolicyUI.CancelReasonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = false;

                    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;
                    // Shruti
                    structEnhancePolicyUI.PolicyUI.MCOEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ConvertQuoteEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_CONVERT);
                    structEnhancePolicyUI.PolicyUI.ReinstatePolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = false;

                    structEnhancePolicyUI.PolicyUI.BrokerEnabled = bEditBroker;
                    structEnhancePolicyUI.PolicyUI.InsurerEnabled = bEditInsurer;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible = false;
                    structEnhancePolicyUI.PolicyUI.PrimaryPolicyEnabled = true;
                    structEnhancePolicyUI.PolicyUI.CoverageEnabled = true;

                    //Commented by csingh7 : MITS 20427 Start
                    //if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_ACCESS) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_VIEW) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_CREATE))
                    //    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = true;
                    //else
                    //    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = false;
                    //Commented by csingh7 : MITS 20427 End
                    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_CREATE); //Added by csingh7 : MITS 20427
                    structEnhancePolicyUI.CoverageUI.DeleteCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_DELETE);
                    structEnhancePolicyUI.CoverageUI.EditCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_UPDATE);
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_VIEW))
                        structEnhancePolicyUI.CoverageUI.CoverageEnabled = true;
                    //Commented by csingh7 : MITS 20427 Start
                    //if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_ACCESS) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_VIEW) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_CREATE))
                    //    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = true;
                    //else
                    //    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = false;
                    //Commented by csingh7 : MITS 20427 End
                    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_CREATE);   //Added by csingh7 : MITS 20427
                    structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_DELETE);
                    structEnhancePolicyUI.ExposureUI.EditExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_UPDATE);
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_VIEW))
                        structEnhancePolicyUI.ExposureUI.ExposureEnabled = true;
                    //structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_DELETE);Commented by csingh7 MITS 20427
                    //structEnhancePolicyUI.ExposureUI.EditExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_UPDATE);Commented by csingh7 MITS 20427

                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = false;
                    structEnhancePolicyUI.McoUI.AddMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.DeleteMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.EditMcoEnabled = true;
                    structEnhancePolicyUI.PolicyUI.BankAccountEnabled = true;
                    //Start:Nitin Goel:Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    //MGaba2:MITS 11802
                    //structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_PRINT);
                    //End:Nitin Goel,Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    //Mridul. 11/24/09. MITS:18229. Since this is PC policy so we need to set Expense Modification Factor to FALSE by default.
                    //if (EnhancePolicyManager.CCacheFunctions.GetShortCode(PolicyEnh.PolicyType) == "GL")
                    //    structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;
                    //else
                    //    structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = true;
                    structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;
                    break;
                    #endregion
                // Converted Quote
                case "CV":
                    #region Converted Quote
                    structEnhancePolicyUI.PolicyUI.PolicyNameEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyStateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.IssueDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReviewDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RetroDateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.CancelReasonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = false;

                    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;

                    // Shruti
                    structEnhancePolicyUI.PolicyUI.MCOEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ConvertQuoteEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReinstatePolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AcceptTransacationEnabled = false;
                    structEnhancePolicyUI.PolicyUI.DeleteTransactionEnabled = false;

                    structEnhancePolicyUI.PolicyUI.BrokerEnabled = false;
                    structEnhancePolicyUI.PolicyUI.InsurerEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible = false;
                    structEnhancePolicyUI.PolicyUI.PrimaryPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CoverageEnabled = false;

                    structEnhancePolicyUI.CoverageUI.EditCoverageEnabled = true;
                    structEnhancePolicyUI.ExposureUI.EditExposureEnabled = true;
                    //csingh7 for MITS 21670 : Start
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_VIEW))
                        structEnhancePolicyUI.CoverageUI.CoverageEnabled = true;
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_VIEW))
                        structEnhancePolicyUI.ExposureUI.ExposureEnabled = true;
                    //csingh7 for MITS 21670 : End

                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = false;
                    structEnhancePolicyUI.McoUI.AddMcoEnabled = false;
                    structEnhancePolicyUI.McoUI.DeleteMcoEnabled = false;
                    structEnhancePolicyUI.McoUI.EditMcoEnabled = false;
                    //Divya -03/23/2007 Bank Account should be enabled for coverted quote
                    structEnhancePolicyUI.PolicyUI.BankAccountEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;

                    break;
                    #endregion
                // In Effect
                case "I":
                    #region In Effect
                    structEnhancePolicyUI.PolicyUI.PolicyNameEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyStateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.IssueDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ReviewDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RetroDateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.CancelReasonEnabled = false;
                    if (p_sPolicyType == "AU")
                        structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = false;
                    else
                        structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = true;
                    //Mridul. 11/24/09. MITS:18229. Since this is PC policy so need to disable the AUDIT functionality.
                    //if (EnhancePolicyManager.LocalCache.GetShortCode(PolicyEnh.PolicyType) == "WC")
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = true;
                    //else
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;
                    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;
                    // Shruti
                    structEnhancePolicyUI.PolicyUI.MCOEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ConvertQuoteEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReinstatePolicyEnabled = false;

                    if (sTransactionStatus == "PR")
                    {
                        structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                        structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = false;

                        //Commented by csingh7 : MITS 20427 Start
                        //if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_ACCESS) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_VIEW) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_CREATE))
                        //    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = true;
                        //else
                        //    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = false;
                        //Commented by csingh7 : MITS 20427 End
                        structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_CREATE); //Added by csingh7 : MITS 20427
                        structEnhancePolicyUI.CoverageUI.DeleteCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_DELETE);
                        structEnhancePolicyUI.CoverageUI.EditCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_UPDATE);
                        if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_VIEW))
                            structEnhancePolicyUI.CoverageUI.CoverageEnabled = true;
                        //Commented by csingh7 : MITS 20427 Start
                        //if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_ACCESS) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_VIEW) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_CREATE))
                        //    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = true;
                        //else
                        //    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = false;
                        //Commented by csingh7 : MITS 20427 End
                        structEnhancePolicyUI.ExposureUI.AddExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_CREATE);   //Added by csingh7 : MITS 20427
                        structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_DELETE);
                        structEnhancePolicyUI.ExposureUI.EditExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_UPDATE);
                        if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_VIEW))
                            structEnhancePolicyUI.ExposureUI.ExposureEnabled = true;
                        //structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_DELETE);Commented by csingh7 MITS 20427
                        //structEnhancePolicyUI.ExposureUI.EditExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_UPDATE);Commented by csingh7 MITS 20427

                        structEnhancePolicyUI.PolicyUI.AcceptTransacationEnabled = true;
                        structEnhancePolicyUI.PolicyUI.DeleteTransactionEnabled = true;
                        structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                        structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = true;
                        structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = true;
                        structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = true;
                        structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = true;
                        structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    }
                    else
                    {
                        structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = false;
                        structEnhancePolicyUI.CoverageUI.DeleteCoverageEnabled = false;
                        structEnhancePolicyUI.ExposureUI.AddExposureEnabled = false;
                        structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled = false;
                        //pmahli MITS 9773 Coverage and Exposure Edit button enabled in case of acccepted transaction
                        structEnhancePolicyUI.CoverageUI.EditCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_UPDATE);
                        structEnhancePolicyUI.ExposureUI.EditExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_UPDATE);
                        //csingh7 for MITS 21670 : Start
                        if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_VIEW))
                            structEnhancePolicyUI.CoverageUI.CoverageEnabled = true;
                        if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_VIEW))
                            structEnhancePolicyUI.ExposureUI.ExposureEnabled = true;
                        //csingh7 for MITS 21670 : End
                        if (p_sPolicyType != "AU")
                        {
                            structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_AMEND);
                            //MGaba2:MITS 15382
                            //structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_AMEND);
                            structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_CANCEL);
                        }
                        else
                        {
                            structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                            structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = false;
                        }

                        structEnhancePolicyUI.PolicyUI.AcceptTransacationEnabled = false;
                        structEnhancePolicyUI.PolicyUI.DeleteTransactionEnabled = false;

                        if (p_sPolicyType != "AU")
                        {
                            if (PolicyEnh.NonRenewFlag)
                                structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                            else
                                structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_RENEW);
                        }
                        else
                        {
                            sSQL = " SELECT TERM_NUMBER FROM POLICY_X_TRANS_ENH WHERE TRANSACTION_ID = " + p_iTransNumber;
                            objReader = PolicyEnh.Context.DbConnLookup.ExecuteReader(sSQL);
                            if (objReader.Read())
                            {
                                sSQL = " SELECT * FROM POLICY_X_TERM_ENH WHERE POLICY_ID=" + PolicyEnh.PolicyId
                                    + " AND TERM_NUMBER=" + (Conversion.ConvertObjToInt(objReader.GetValue("TERM_NUMBER"), base.ClientId) + 1);

                                objReader2 = PolicyEnh.Context.DbConnLookup.ExecuteReader(sSQL);
                                if (objReader2.Read())
                                    structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                                else
                                    structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_RENEW);

                                objReader2.Close();
                            }
                            objReader.Close();
                        }
                        if (p_sPolicyType == "EN" || p_sPolicyType == "AU")
                        {
                            structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                            structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = true;
                            structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = true;
                            structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = true;
                        }
                        else
                        {
                            structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                            structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = false;
                            structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = false;
                            structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = false;
                        }
                        //Mridul. 11/24/09. MITS:18229. Since this is PC policy so need to disable the AUDIT functionality.
                        //if (EnhancePolicyManager.LocalCache.GetShortCode(PolicyEnh.PolicyType) == "WC")
                        //    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_AUDIT);
                        //else
                        //    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                        structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    }
                    structEnhancePolicyUI.PolicyUI.BrokerEnabled = bEditBroker;
                    structEnhancePolicyUI.PolicyUI.InsurerEnabled = bEditInsurer;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumEnabled = true;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible = true;
                    structEnhancePolicyUI.PolicyUI.PrimaryPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CoverageEnabled = false;

                    structEnhancePolicyUI.McoUI.AddMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.DeleteMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.EditMcoEnabled = true;
                    //Start:Nitin Goel:Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    //MGaba2:MITS 11802
                    //structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_PRINT);
                    //End:Nitin Goel,Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;
                    //Mukul(07/09/2007) Added MITS 9973
                    structEnhancePolicyUI.PolicyUI.BankAccountEnabled = true;
                    break;
                    #endregion
                // Revoked
                case "R":
                    #region Revoked
                    structEnhancePolicyUI.PolicyUI.PolicyNameEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyStateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.IssueDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ReviewDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RetroDateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.CancelReasonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = true;

                    //Mridul. 11/24/09. MITS:18229. Since this is PC policy so need to disable the AUDIT functionality.
                    //if (EnhancePolicyManager.LocalCache.GetShortCode(PolicyEnh.PolicyType) == "WC")
                    //{
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_AUDIT);
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = true;
                    //}
                    //else
                    //{
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;
                    //}
                    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;

                    // Shruti
                    structEnhancePolicyUI.PolicyUI.MCOEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ConvertQuoteEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReinstatePolicyEnabled = false;

                    if (EnhancePolicyManager.LocalCache.GetShortCode(EnhancePolicyManager.GetLatestTran(PolicyEnh, true).TransactionStatus) == "PR")
                    {
                        structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                        structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = false;
                    }
                    else
                    {//MGaba2:MITS 15382:Start
                        // structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_RENEW);
                        // structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_AMEND);
                        structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_AMEND);
                        structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_CANCEL);
                        //MGaba2:MITS 15382:End                        
                    }
                    structEnhancePolicyUI.PolicyUI.BrokerEnabled = false;
                    structEnhancePolicyUI.PolicyUI.InsurerEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumEnabled = true;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible = true;
                    structEnhancePolicyUI.PolicyUI.PrimaryPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CoverageEnabled = false;

                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = false;
                    structEnhancePolicyUI.McoUI.AddMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.DeleteMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.EditMcoEnabled = true;
                    structEnhancePolicyUI.PolicyUI.BankAccountEnabled = true;
                    //Start:Nitin Goel:Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    //MGaba2:MITS 11802
                    //structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_PRINT);
                    //End:Nitin Goel,Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    break;
                    #endregion
                case "E":
                    #region Expired
                    structEnhancePolicyUI.PolicyUI.PolicyNameEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyStateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.IssueDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReviewDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RetroDateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.CancelReasonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = false;

                    //Mridul. 11/24/09. MITS:18229. Since this is PC policy so need to disable the AUDIT functionality.
                    //if (EnhancePolicyManager.LocalCache.GetShortCode(PolicyEnh.PolicyType) == "WC")
                    //{
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_AUDIT);
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = true;
                    //}
                    //else
                    //{
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;
                    //}
                    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;

                    // Shruti
                    structEnhancePolicyUI.PolicyUI.MCOEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ConvertQuoteEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReinstatePolicyEnabled = false;

                    structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;

                    structEnhancePolicyUI.PolicyUI.BrokerEnabled = false;
                    structEnhancePolicyUI.PolicyUI.InsurerEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumEnabled = true;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible = true;
                    structEnhancePolicyUI.PolicyUI.PrimaryPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CoverageEnabled = false;

                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = false;
                    structEnhancePolicyUI.McoUI.AddMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.DeleteMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.EditMcoEnabled = true;
                    structEnhancePolicyUI.PolicyUI.BankAccountEnabled = true;
                    //Start:Nitin Goel:Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    //MGaba2:MITS 11802
                    //structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_PRINT);
                    //End:Nitin Goel,Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    break;
                    #endregion
                case "PR":
                    #region Provisionally Renewed
                    structEnhancePolicyUI.PolicyUI.PolicyNameEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyStateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.IssueDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ReviewDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RetroDateEnabled = true;

                    structEnhancePolicyUI.PolicyUI.CancelReasonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = false;

                    //Mridul. 11/24/09. MITS:18229. Since this is PC policy so need to disable the AUDIT functionality.
                    //if (EnhancePolicyManager.LocalCache.GetShortCode(PolicyEnh.PolicyType) == "WC")
                    //{
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = true;
                    //}
                    //else
                    //{
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;
                    //}
                    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;

                    // Shruti
                    structEnhancePolicyUI.PolicyUI.MCOEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ConvertQuoteEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReinstatePolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = false;

                    structEnhancePolicyUI.PolicyUI.BrokerEnabled = bEditBroker;
                    structEnhancePolicyUI.PolicyUI.InsurerEnabled = bEditInsurer;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumEnabled = true;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible = true;
                    structEnhancePolicyUI.PolicyUI.PrimaryPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CoverageEnabled = false;

                    //Commented by csingh7 : MITS 20427 Start
                    //if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_ACCESS) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_VIEW) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_CREATE))
                    //    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = true;
                    //else
                    //    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = false;
                    //Commented by csingh7 : MITS 20427 End
                    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_CREATE); //Added by csingh7 : MITS 20427
                    structEnhancePolicyUI.CoverageUI.DeleteCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_DELETE);
                    structEnhancePolicyUI.CoverageUI.EditCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_UPDATE);
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_VIEW))
                        structEnhancePolicyUI.CoverageUI.CoverageEnabled = true;
                    //Commented by csingh7 : MITS 20427 Start
                    //if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_ACCESS) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_VIEW) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_CREATE))
                    //    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = true;
                    //else
                    //    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = false;
                    //Commented by csingh7 : MITS 20427 End
                    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_CREATE);   //Added by csingh7 : MITS 20427
                    structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_DELETE);
                    structEnhancePolicyUI.ExposureUI.EditExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_UPDATE);
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_VIEW))
                        structEnhancePolicyUI.ExposureUI.ExposureEnabled = true;
                    //structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_DELETE);Commented by csingh7 MITS 20427
                    //structEnhancePolicyUI.ExposureUI.EditExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_UPDATE);Commented by csingh7 MITS 20427

                    structEnhancePolicyUI.PolicyUI.AcceptTransacationEnabled = true;
                    structEnhancePolicyUI.PolicyUI.DeleteTransactionEnabled = true;

                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = false;
                    structEnhancePolicyUI.McoUI.AddMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.DeleteMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.EditMcoEnabled = true;
                    structEnhancePolicyUI.PolicyUI.BankAccountEnabled = true;
                    //Start:Nitin Goel:Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    //MGaba2:MITS 11802
                    //structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_PRINT);
                    //End:Nitin Goel,Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    //Mridul. 11/24/09. MITS:18229. Since this is PC policy so we need to set Expense Modification Factor to FALSE by default.
                    //if (EnhancePolicyManager.CCacheFunctions.GetShortCode(PolicyEnh.PolicyType) == "GL")
                    //    structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;
                    //else
                    //    structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = true;
                    structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;

                    break;
                    #endregion
                // Cancelled
                case "C":
                case "CPR":
                case "CF":
                    #region Cancelled
                    structEnhancePolicyUI.PolicyUI.PolicyNameEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyStateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.IssueDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReviewDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RetroDateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.CancelReasonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = false;

                    //Mridul. 11/24/09. MITS:18229. Since this is PC policy so need to disable the AUDIT functionality.
                    //if (EnhancePolicyManager.LocalCache.GetShortCode(PolicyEnh.PolicyType) == "WC")
                    //{
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_AUDIT);
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = true;
                    //}
                    //else
                    //{
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;
                    //}
                    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;

                    // Shruti
                    structEnhancePolicyUI.PolicyUI.MCOEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ConvertQuoteEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReinstatePolicyEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_REINSTATE);
                    structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = false;

                    structEnhancePolicyUI.PolicyUI.BrokerEnabled = false;
                    structEnhancePolicyUI.PolicyUI.InsurerEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumEnabled = true;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible = true;
                    structEnhancePolicyUI.PolicyUI.PrimaryPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CoverageEnabled = false;

                    structEnhancePolicyUI.PolicyUI.AcceptTransacationEnabled = false;
                    structEnhancePolicyUI.PolicyUI.DeleteTransactionEnabled = false;

                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = false;
                    structEnhancePolicyUI.McoUI.AddMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.DeleteMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.EditMcoEnabled = true;
                    structEnhancePolicyUI.PolicyUI.BankAccountEnabled = true;
                    //Start:Nitin Goel:Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    //MGaba2:MITS 11802
                    //structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_PRINT);
                    //End:Nitin Goel,Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;
                    //pmahli MITS 9773
                    structEnhancePolicyUI.CoverageUI.EditCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_UPDATE);
                    structEnhancePolicyUI.ExposureUI.EditExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_UPDATE);
                    //csingh7 for MITS 21670 : Start
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_VIEW))
                        structEnhancePolicyUI.CoverageUI.CoverageEnabled = true;
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_VIEW))
                        structEnhancePolicyUI.ExposureUI.ExposureEnabled = true;
                    //csingh7 for MITS 21670 : End

                    break;
                    #endregion
                // Provisional Cancel
                case "PCF":
                case "PCP":
                    #region Provisional Cancel
                    structEnhancePolicyUI.PolicyUI.PolicyNameEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyStateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.IssueDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ReviewDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RetroDateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.CancelReasonEnabled = true;
                    structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = false;

                    //Mridul. 11/24/09. MITS:18229. Since this is PC policy so need to disable the AUDIT functionality.
                    //if (EnhancePolicyManager.LocalCache.GetShortCode(PolicyEnh.PolicyType) == "WC")
                    //{
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = true;
                    //}
                    //else
                    //{
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;
                    //}
                    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;

                    // Shruti
                    structEnhancePolicyUI.PolicyUI.MCOEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ConvertQuoteEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReinstatePolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = false;

                    structEnhancePolicyUI.PolicyUI.BrokerEnabled = bEditBroker;
                    structEnhancePolicyUI.PolicyUI.InsurerEnabled = bEditInsurer;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumEnabled = true;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible = true;
                    structEnhancePolicyUI.PolicyUI.PrimaryPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CoverageEnabled = false;

                    structEnhancePolicyUI.PolicyUI.AcceptTransacationEnabled = true;
                    structEnhancePolicyUI.PolicyUI.DeleteTransactionEnabled = true;

                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = false;
                    structEnhancePolicyUI.McoUI.AddMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.DeleteMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.EditMcoEnabled = true;
                    structEnhancePolicyUI.PolicyUI.BankAccountEnabled = true;
                    //Start:Nitin Goel:Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    //MGaba2:MITS 11802
                    //structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_PRINT);
                    //End:Nitin Goel,Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;
                    //pmahli MITS 9773
                    structEnhancePolicyUI.CoverageUI.EditCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_UPDATE);
                    structEnhancePolicyUI.ExposureUI.EditExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_UPDATE);
                    //csingh7 for MITS 21670 : Start
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_VIEW))
                        structEnhancePolicyUI.CoverageUI.CoverageEnabled = true;
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_VIEW))
                        structEnhancePolicyUI.ExposureUI.ExposureEnabled = true;
                    //csingh7 for MITS 21670 : End

                    break;
                    #endregion
                // Provisional Reinstate
                case "PRN":
                case "PRL":
                    #region Provisional Reinstate
                    structEnhancePolicyUI.PolicyUI.PolicyNameEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyStateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.IssueDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ReviewDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RetroDateEnabled = true;

                    structEnhancePolicyUI.PolicyUI.CancelReasonEnabled = true;
                    structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = false;

                    //Mridul. 11/24/09. MITS:18229. Since this is PC policy so need to disable the AUDIT functionality.
                    //if (EnhancePolicyManager.LocalCache.GetShortCode(PolicyEnh.PolicyType) == "WC")
                    //{
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = true;
                    //}
                    //else
                    //{
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;
                    //}
                    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;

                    // Shruti
                    structEnhancePolicyUI.PolicyUI.MCOEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ConvertQuoteEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReinstatePolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = false;

                    structEnhancePolicyUI.PolicyUI.BrokerEnabled = bEditBroker;
                    structEnhancePolicyUI.PolicyUI.InsurerEnabled = bEditInsurer;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumEnabled = true;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible = true;
                    structEnhancePolicyUI.PolicyUI.PrimaryPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CoverageEnabled = false;

                    structEnhancePolicyUI.PolicyUI.AcceptTransacationEnabled = true;
                    structEnhancePolicyUI.PolicyUI.DeleteTransactionEnabled = true;

                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = false;
                    structEnhancePolicyUI.McoUI.AddMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.DeleteMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.EditMcoEnabled = true;
                    structEnhancePolicyUI.PolicyUI.BankAccountEnabled = true;
                    //Start:Nitin Goel:Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    //MGaba2:MITS 11802
                    //structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_PRINT);
                    //End:Nitin Goel,Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;
                    //pmahli MITS 9773 
                    structEnhancePolicyUI.CoverageUI.EditCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_UPDATE);
                    structEnhancePolicyUI.ExposureUI.EditExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_UPDATE);
                    //csingh7 for MITS 21670 : Start
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_COV_ENH, RMO_VIEW))
                        structEnhancePolicyUI.CoverageUI.CoverageEnabled = true;
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTPC_EXP_ENH, RMO_VIEW))
                        structEnhancePolicyUI.ExposureUI.ExposureEnabled = true;
                    //csingh7 for MITS 21670 : End
                    break;
                    #endregion
            }

            // Set Defaults which need to be READONLY in each case. 
            structEnhancePolicyUI.PolicyUI.CancelDateEnabled = false;

        }
        
        #endregion     
       
    }
}
