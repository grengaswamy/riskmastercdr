using System;
using Riskmaster.DataModel;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for CmXAccommodationListForm.
	/// </summary>
	public class CmXAccommodationListForm: ListFormBase
	{
		const string CLASS_NAME = "CmXAccommodationList";

		public override void Init()
		{
			//If we were sent here from a parent(event) then we'll have an eventid in the SysEx
			//originally from ScreenFlow\SysExData.
			XmlDocument objXML = base.SysEx;
			XmlNode objCasemgtRowId=null;
			try{objCasemgtRowId = objXML.SelectSingleNode("/SysExData/CasemgtRowId");}
			catch{};
			
			//Filter by this CasemgtRowId if Present.
			if(objCasemgtRowId !=null)
			{
				objDataList.BulkLoadFlag = true;
				objDataList.SQLFilter = "CM_ROW_ID=" + objCasemgtRowId.InnerText;
			}
			else
				throw new Riskmaster.ExceptionTypes.CollectionException(String.Format(Globalization.GetString("ListForm.MissingExpectedFilterKey",base.ClientId),"ClaimId"));
			OnUpdateForm();
		}


		public CmXAccommodationListForm(FormDataAdaptor fda):base(fda)
		{
			this.objDataList = fda.Factory.GetDataModelObject(CLASS_NAME,false) as DataCollection;
		}
		private LocalCache objCache{get{return objDataList.Context.LocalCache;}}

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm() //Only called from base classes on Refresh()caused by PostBack.
		{
			
			base.OnUpdateForm ();
			base.CreateSysExData("PiEid","");


			XmlDocument objXML = base.SysEx;
			XmlNode objNew;
			XmlNode objOld ;
			
			objNew = objXML.CreateElement("Screen");
			objNew.Attributes.Append(objXML.CreateAttribute("type"));
			objNew.Attributes["type"].Value="";

			objOld = objXML.SelectSingleNode("//Screen");
			if(objOld !=null)
				;
				//Let it Ride from the Client... 
				//Will be acted on in the Form XSL
				//No Need to clear since next page request should hit a new
				//form altogether anyhow.
				//objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);


			//Clear any existing tablename nodes.
			foreach(XmlNode nd in objXML.SelectNodes("//EntityTableName"))
				nd.ParentNode.RemoveChild(nd);
		}
	}
}
