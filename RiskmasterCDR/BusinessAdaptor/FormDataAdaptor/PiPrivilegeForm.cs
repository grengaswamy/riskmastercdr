using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

//TODO: Auto-Generated Form - migrate and verify behaviors from Legacy RMNet form.

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for PiPrivilege Screen.
	/// </summary>
	public class PiPrivilegeForm : DataEntryFormBase
	{
		const string CLASS_NAME = "PhysicianPrivilege";
		private PhysicianPrivilege PhysicianPrivilege{get{return objData as PhysicianPrivilege;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		const string FILTER_KEY_NAME = "PhysEid";
		public override void InitNew()
		{
			base.InitNew(); 

			this.m_ParentId = base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME,false);
			(objData as INavigation).Filter = 
				objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + this.m_ParentId.ToString();		
			this.PhysicianPrivilege.PhysEid = this.m_ParentId;
		}

		public PiPrivilegeForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

        // npadhy MITS 14200 Validation for Privilege Start Date and Privilege End Date as Per RMWorld
        public override void OnValidate(ref bool Cancel)
        {
            bool bError = false;
            // start bgupta22 MITS 35540
            string sLangCode = base.LanguageCode;
          
            if (!string.IsNullOrEmpty(PhysicianPrivilege.IntDate) && !(string.IsNullOrEmpty(PhysicianPrivilege.EndDate)))
            {
                if (PhysicianPrivilege.IntDate.CompareTo(PhysicianPrivilege.EndDate) > 0)
                {
                    Errors.Add(CommonFunctions.FilterBusinessMessageRetainErrorCode(Globalization.GetString("ValidationError", base.ClientId), sLangCode),
                    String.Format(CommonFunctions.FilterBusinessMessageRetainErrorCode(Globalization.GetString("Validation.MustBeGreaterThanStartDate2", base.ClientId), sLangCode), CommonFunctions.FilterBusinessMessageRetainErrorCode(Globalization.GetString("Field.EndDate", base.ClientId), sLangCode), Conversion.ToDate(PhysicianPrivilege.IntDate, sLangCode, base.ClientId)),
                    BusinessAdaptorErrorType.Error);
                //Errors.Add(Globalization.GetString("ValidationError"),
                //        String.Format(Globalization.GetString("Validation.MustBeGreaterThan"), Globalization.GetString("Field.EndDate"), Conversion.ToDate(PhysicianPrivilege.IntDate).ToShortDateString() + "(" + Globalization.GetString("Field.StartDate") + ")"),
               //        BusinessAdaptorErrorType.Error);
                    
                    // end bgupta22 MITS 35540
                    bError = true;
                }
            }

            // Return true if there were validation errors
            Cancel = bError;
        }

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
			base.OnUpdateForm ();
		}

	/*	public override void BeforeSave(ref bool Cancel)
		{
			XmlDocument objSysExDataXmlDoc = base.SysEx;
			XmlNode objPhysEidNode = null;
			try{objPhysEidNode = objSysExDataXmlDoc.SelectSingleNode("/SysExData/PhysEid");}
			catch{};
			
			if(objPhysEidNode !=null)
				if(this.PhysicianPrivilege.PhysEid==0)
					this.PhysicianPrivilege.PhysEid = Conversion.ConvertStrToInteger(objPhysEidNode.InnerText);

			objPhysEidNode = null;
			objSysExDataXmlDoc = null;

			base.BeforeSave (ref Cancel);		

		}*/
	}
}