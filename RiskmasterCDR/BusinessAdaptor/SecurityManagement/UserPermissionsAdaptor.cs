using System;
using Riskmaster.ExceptionTypes;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.Application.SecurityManagement;
using System.Collections;
using Riskmaster.Security;
using System.Threading;
using Riskmaster.Db;

namespace Riskmaster.BusinessAdaptor
{
	
	///************************************************************** 
	///* $File		: UserPermissionsAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 15-Dec-2005 
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>
	/// Summary description for UserPermissionsAdaptor.
	/// </summary>
	public class UserPermissionsAdaptor:BusinessAdaptorBase
	{
		public UserPermissionsAdaptor(){}

		#region "GetSecuredNavTreeForUser(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)"
		/// Name			: GetSecuredNavTreeForUser
		/// Date Created	: 15-Dec-2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// Summary description for UserPermissionsAdaptor
		/// </summary>
		/// <param name="p_objXmlDocIn"> 
		///	Input paramters in xml format.
		///	Structure:
		///	
		/// </param>
		/// <param name="p_objXmlDocOut"> 
		///	
		///	</param>
		/// <param name="p_objErrors">
		///	Collection of errors/messages.	
		///	</param>
		/// <returns>
		///		true - success
		///		false - failure
		///	</returns>
		public bool GetSecuredNavTreeForUser(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			UserPermissions objPermissions=null;
			
			try
			{
				objPermissions=new UserPermissions(base.ClientId);
				p_objXmlOut= objPermissions.GetSecuredNavTreeForUser(p_objDocIn,userLogin);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException, Globalization.GetString("UserPermissionsAdaptor.GetSecuredNavTreeForUser.error", base.ClientId) ,BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				if (objPermissions != null) objPermissions = null;
			}
		}
		#endregion
		
	}
}
