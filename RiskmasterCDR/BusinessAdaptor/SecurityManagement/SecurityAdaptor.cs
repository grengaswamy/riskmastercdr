﻿using System;
using Riskmaster.ExceptionTypes;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.Application.SecurityManagement;
using System.Collections;
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.Application.AsyncManagement ;

namespace Riskmaster.BusinessAdaptor
{   /// Name		: SecurityAdaptor.cs
	/// Author		: Tanuj Narula
	/// Date Created: 22-Mar-2005		
	///************************************************************
	/// Amendment History
	///************************************************************
	/// Date Amended   *   Amendment   *    Author
	///************************************************************
	/// <summary>
	/// Contains the functions related to XML processing.
	/// </summary>
	public class SecurityAdaptor : BusinessAdaptorBase
	{  
		/// Name		: SecurityAdaptor
		/// Author		: Tanuj Narula
		/// Date Created: 22-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Default constructor
		/// </summary>
		public SecurityAdaptor()
		{
			
			
		}
		

		/// Name		: GetXMLData
		/// Author		: Tanuj Narula
		/// Date Created: 22-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function acts as a wrapper over actual functions for filling various XmlDocuments.
		/// </summary>
		/// <param name="p_objDocIn">
		/// Structure of input xml-:
		/// <Document>
		/// <Template>
		/// Standard xml template.
		/// </Template>
		/// <FormName>
		/// </FormName>
		/// </Document>
		/// </param>
		/// <param name="p_objXmlOut"></param>
		/// <param name="p_objErrOut">This collection will contain errors occured during the function execution.</param>
		/// <returns>True if successful, false if fails.</returns>
		public bool GetXMLData(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{   
			Riskmaster.Application.SecurityManagement.Security objSecurity=null;
			try
			{
				objSecurity=new Riskmaster.Application.SecurityManagement.Security(base.ClientId);
				SetIds(ref p_objDocIn,ref objSecurity);
				p_objXmlOut=p_objDocIn;
				objSecurity.GetXMLData(p_objDocIn.SelectSingleNode("//" + "form").Attributes["name"].InnerText,ref p_objXmlOut);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("OSHALibAdapter.RunReport.Error", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;

			}
			finally
			{
				objSecurity=null;
			}
		}
		/// <summary>
		/// This function fills XmlDocument corresponding to DSNWizardServerLogin.
		/// </summary>
		/// <param name="p_objDocIn">
		/// Structure of input xml:-
		/// <Document>
		/// <Id></Id>
		/// <DsnId></DsnId>
		/// <ServerName></ServerName>
		/// <DBName></DBName>
		/// <LoginName></LoginName>
		/// <LoginPassword></LoginPassword>
		/// <DSNWizardServerLoginDOM>
		/// Standard xml template
		/// </DSNWizardServerLoginDOM>
		/// </Document>
		/// </param>
		/// <param name="p_objXmlOut">Standard xml template</param>
		/// <param name="p_objErrOut">This collection will contain errors occured during the function execution.</param>
		/// <returns>True if successful, false if fails.</returns>
		public bool FillDSNWizardServerLoginDOM(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.SecurityManagement.Security objSecurity=null;
			try
			{
				objSecurity=new Riskmaster.Application.SecurityManagement.Security(base.ClientId);
				SetIds(ref p_objDocIn,ref objSecurity);
				p_objXmlOut.LoadXml(p_objDocIn.SelectSingleNode("//"+ "DSNWizardServerLoginDOM").InnerXml);
				objSecurity.FillDSNWizardServerLoginDOM(ref p_objXmlOut,
					p_objDocIn.SelectSingleNode("//"+ "ServerName").InnerText,
					p_objDocIn.SelectSingleNode("//"+ "DBName").InnerText,
					p_objDocIn.SelectSingleNode("//"+ "LoginName").InnerText,
					p_objDocIn.SelectSingleNode("//"+ "LoginPassword").InnerText);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("OSHALibAdapter.RunReport.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;

			}
			finally
			{
				objSecurity=null;
			}
		}
		/// <summary>
		///This function fills XmlDocument corresponding to DSNWizardDSNLogin.
		/// </summary>
		/// <param name="p_objDocIn">
		/// Structure of input xml:-
		/// <Document>
		/// <Id></Id>
		/// <DsnId></DsnId>
		/// <LoginName></LoginName>
		/// <LoginPassword></LoginPassword>
		/// <DSNWizardDSNLoginDOM>Standard DSNWizardDSNLogin xml.</DSNWizardDSNLoginDOM>
		/// </Document>
		/// </param>
		/// <param name="p_objXmlOut">Standard DSNWizardDSNLogin xml.</param>
		/// <param name="p_objErrOut">This collection will contain errors occured during the function execution.</param>
		/// <returns>True if successful, false if fails.</returns>
		public bool FillDSNWizardDSNLoginDOM(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.SecurityManagement.Security objSecurity=null;
			try
			{
                objSecurity = new Riskmaster.Application.SecurityManagement.Security(base.ClientId);
				SetIds(ref p_objDocIn,ref objSecurity);
				p_objXmlOut.LoadXml(p_objDocIn.SelectSingleNode("//"+ "DSNWizardDSNLoginDOM").InnerXml);
				objSecurity.FillDSNWizardDSNLoginDOM(ref p_objXmlOut,
					p_objDocIn.SelectSingleNode("//"+ "LoginName").InnerText,
					p_objDocIn.SelectSingleNode("//"+ "LoginPassword").InnerText);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("OSHALibAdapter.RunReport.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;

			}
			finally
			{
				objSecurity=null;
			}
		}
		/// <summary>
		/// /// This function fills XmlDocument corresponding to DSNWizardFileLogin.
		/// </summary>
		/// <param name="p_objDocIn">
		/// Structure of input xml:-
		/// <Document>
		/// <Id></Id>
		/// <DsnId></DsnId>
		/// <DbFileName></DbFileName>
		/// <LoginName></DbFileName>
		/// <LoginPassword></LoginPassword>
		/// <DSNWizardFileLoginDOM>Standard DSNWizardFileLoginDOM xml.</DSNWizardFileLoginDOM>
		/// </Document>
		/// </param>
		/// <param name="p_objXmlOut">Standard DSNWizardFileLoginDOM xml.</param>
		/// <param name="p_objErrOut">This collection will contain errors occured during the function execution.</param>
		/// <returns>True if successful, false if fails.</returns>
		public bool FillDSNWizardFileLoginDOM(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.SecurityManagement.Security objSecurity=null;
			try
			{
                objSecurity = new Riskmaster.Application.SecurityManagement.Security(base.ClientId);
				SetIds(ref p_objDocIn,ref objSecurity);
				p_objXmlOut.LoadXml(p_objDocIn.SelectSingleNode("//"+ "DSNWizardFileLoginDOM").InnerXml);
				objSecurity.FillDSNWizardFileLoginDOM(ref p_objXmlOut,
					p_objDocIn.SelectSingleNode("//"+ "DbFileName").InnerText,
					p_objDocIn.SelectSingleNode("//"+ "LoginName").InnerText,
					p_objDocIn.SelectSingleNode("//"+ "LoginPassword").InnerText);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("OSHALibAdapter.RunReport.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;

			}
			finally
			{
				objSecurity=null;
			}
		}
		/// <summary>
		/// This function fills the selected datasource xmldocument.
		/// </summary>
		/// <param name="p_objDocIn">
		/// Structure of input xml:-
		/// <Document>
		/// <Id></Id>
		/// <DsnId></DsnId>
		/// <ItemId></ItemId>
		/// <Template>Standard Template xml.</Template>
		/// </Document>
		/// </param>
		/// <param name="p_objXmlOut"></param>
		/// <param name="p_objErrOut">This collection will contain errors occured during the function execution.</param>
		/// <returns>True if successful, false if fails.</returns>
		public bool GetSelectedDataSource(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.SecurityManagement.Security objSecurity=null;
			try
			{
                objSecurity = new Riskmaster.Application.SecurityManagement.Security(base.ClientId);
				SetIds(ref p_objDocIn,ref objSecurity);
				p_objXmlOut.LoadXml(p_objDocIn.SelectSingleNode("//"+ "DSNWizardFileLoginDOM").InnerXml);
				objSecurity.GetSelectedDataSource(ref p_objXmlOut,
                    Conversion.ConvertObjToInt(p_objDocIn.SelectSingleNode("//" + "ItemId").InnerText, base.ClientId));
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("OSHALibAdapter.RunReport.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;

			}
			finally
			{
				objSecurity=null;
			}

		}
		/// <summary>
		/// This is a common function for setting ID and DSN ID.
		/// </summary>
		/// <param name="p_objXmlDoc">Document containing ID and DSN ID.</param>
		/// <param name="p_objSecurity">Security object.</param>
		private void SetIds(ref XmlDocument p_objXmlDoc,ref Riskmaster.Application.SecurityManagement.Security p_objSecurity)
		{
			try
			{
				if(p_objXmlDoc.SelectSingleNode("//control[@name='userid']")!=null)
					p_objSecurity.ID= Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//control[@name='userid']").InnerText);
                
				if(p_objXmlDoc.SelectSingleNode("//control[@name='dsnid']")!=null)
					p_objSecurity.DSNID= Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//control[@name='dsnid']").InnerText);
			}
			catch(Exception p_objException)
			{

				throw p_objException;
			}
		}
        /// <summary>
        /// This function gets the xml structure for security left tree. 
        /// </summary>
        /// <param name="p_objDocIn">Instance document.</param>
        /// <param name="p_objXmlOut">Will hold the generated xml for security tree. </param>
        /// <param name="p_objErrOut">Error collection.</param>
        /// <returns>True or false.</returns>
		public bool GetXmlStructureForLeftTree(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			SecurityLeftTreeHandler objSecurityTreeHandler;
			try
			{
				objSecurityTreeHandler=new SecurityLeftTreeHandler(base.ClientId);
				objSecurityTreeHandler.GetXmlStructureForLeftTree(ref p_objDocIn,ref p_objXmlOut);
				return true;

			}
			catch(RMAppException p_objException)
			{
                if(p_objErrOut==null)
                    p_objErrOut = new BusinessAdaptorErrors(base.ClientId);
                 p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("OSHALibAdapter.RunReport.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objSecurityTreeHandler=null;
			}
		}
        /// <summary>
        /// This function gets the module group information for a particular dsn id. 
        /// </summary>
        /// <param name="p_objDocIn">Instance document.</param>
        /// <param name="p_objXmlOut">Will hold the generated xml. </param>
        /// <param name="p_objErrOut">Error collection.</param>
        /// <returns>True or false.</returns>
        public bool GetModuleGroupChildrenInfo(XmlDocument p_objDocIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            XmlElement objEle = null;
            int iDsnId = 0;
            bool bResult = false;
            XmlElement objParentEle = null;
            try
            {
                objEle = (XmlElement)p_objDocIn.SelectSingleNode("//DsnIdForLoadingMG");
                if (objEle != null)
                {
                    iDsnId = Conversion.ConvertStrToInteger(objEle.InnerText);
                }
                objParentEle = p_objXmlOut.CreateElement("entry");
                objParentEle.SetAttribute("id", iDsnId.ToString());
                p_objXmlOut.AppendChild(objParentEle);
                PostbackHandler.AppendModuleGroups(ref objParentEle, iDsnId, ref p_objXmlOut, false, base.ClientId);
                return true;

            }
            catch (RMAppException p_objException)
            {
                if (p_objErrOut == null)
                    p_objErrOut = new BusinessAdaptorErrors(base.ClientId);
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("OSHALibAdapter.RunReport.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }


        }
		/// <summary>
		/// This function gets the xml structure for security permission tree. 
		/// </summary>
		/// <param name="p_objDocIn">Instance document.</param>
		/// <param name="p_objXmlOut">Will hold the generated xml for security permission tree. </param>
		/// <param name="p_objErrOut">Error collection.</param>
		/// <returns>True or false.</returns>
		public bool GetXmlStructureForPermissonsTree(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			SecurityPermissionTreeHandler objSecurityPermTreeHandler;
			try
			{
				objSecurityPermTreeHandler=new SecurityPermissionTreeHandler(base.ClientId);
				objSecurityPermTreeHandler.GetXmlStructureForPermissonsTree(ref p_objDocIn,ref p_objXmlOut);
				return true;

			}
			catch(RMAppException p_objException)
			{
				if(p_objErrOut==null)
                    p_objErrOut = new BusinessAdaptorErrors(base.ClientId);
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("OSHALibAdapter.RunReport.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objSecurityPermTreeHandler=null;
			}
		}
		public bool GetXmlStructureForPermissonsTree(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut, AsyncWorkerThread p_objWorkerThread)
		{
			SecurityPermissionTreeHandler objSecurityPermTreeHandler;
			try
			{
                objSecurityPermTreeHandler = new SecurityPermissionTreeHandler(base.ClientId);
				objSecurityPermTreeHandler.GetXmlStructureForPermissonsTree(ref p_objDocIn,ref p_objXmlOut);
				return true;

			}
//			catch(RMAppException p_objException)
//			{
//				if(p_objErrOut==null)
//					p_objErrOut=new BusinessAdaptorErrors();
//				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
//				return false;
//			}
//			catch(Exception p_objException)
//			{
//				p_objErrOut.Add(p_objException,Globalization.GetString("OSHALibAdapter.RunReport.Error"),BusinessAdaptorErrorType.Error);
//				return false;
//			}
			finally
			{
				objSecurityPermTreeHandler=null;
			}
		}


		/// <summary>
		/// checks the complexity of password entered by user
		/// </summary>
		/// <param name="p_objDocIn"></param>
		/// <param name="p_objXmlOut"></param>
		/// <param name="p_objErrOut"></param>
		/// <returns></returns>
        public bool IsPasswordComplex(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
            //added by Ashutosh Kashyap:akashyap3 on 29-Jan-09 to check if password policy is enables.
            if (PublicFunctions.IsPwdPolEnabled())
            {
                int iReturn = 0;
                XmlElement objElement = (XmlElement)p_objDocIn.SelectSingleNode("//form//PasswordPolicy");
                if (objElement != null)
                {
                    objElement.InnerText = "true";
                }
                objElement = p_objDocIn.CreateElement("RetVal"); ;
                try
                {
                    if (p_objDocIn.SelectSingleNode("//LoginName").InnerText != "")
                    {
                        //Added for Mits 18690-Start:Since the Popup was coming before the user was saved..so UserId was fetching the value of logged-in User and not the new user created.
                        string sLastName = string.Empty;
                        string sFirstName = string.Empty;
                        if (p_objDocIn.SelectSingleNode("//LastName") != null)
                            sLastName = p_objDocIn.SelectSingleNode("//LastName").InnerText;
                        if (p_objDocIn.SelectSingleNode("//FirstName") != null)
                            sFirstName = p_objDocIn.SelectSingleNode("//FirstName").InnerText;
                        if( sLastName != "" || sFirstName != "")
                            iReturn = PublicFunctions.IsPasswordComplex(p_objDocIn.SelectSingleNode("//Password").InnerText, 0, sLastName, sFirstName, PublicFunctions.GetUserIDForLoginName(p_objDocIn.SelectSingleNode("//LoginName").InnerText, base.ClientId), base.ClientId);
                        //Added for Mits 18690-End
                        else
                            iReturn = PublicFunctions.IsPasswordComplex(p_objDocIn.SelectSingleNode("//Password").InnerText, PublicFunctions.GetUserIDForLoginName(p_objDocIn.SelectSingleNode("//LoginName").InnerText, base.ClientId), "", "", PublicFunctions.GetUserIDForLoginName(p_objDocIn.SelectSingleNode("//LoginName").InnerText, base.ClientId), base.ClientId);
                    }
                    else
                    {
                        iReturn = PublicFunctions.IsPasswordComplex(p_objDocIn.SelectSingleNode("//Password").InnerText, Conversion.ConvertObjToInt(p_objDocIn.SelectSingleNode("//Userid").InnerText, base.ClientId), p_objDocIn.SelectSingleNode("//LastName").InnerText, p_objDocIn.SelectSingleNode("//FirstName").InnerText, PublicFunctions.GetUserIDForLoginName(p_objDocIn.SelectSingleNode("//LoginName").InnerText, base.ClientId), base.ClientId);
                    }

                    p_objDocIn.SelectSingleNode("//form//SuccessMsg").InnerText = "Typed password meets the password complexity requirements.";
                    return true;

                }
                catch (RMAppException p_objException)
                {
                    if (p_objErrOut == null)
                        p_objErrOut = new BusinessAdaptorErrors(base.ClientId);
                    p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                    return false;
                }
                catch (Exception p_objException)
                {
                    p_objErrOut.Add(p_objException, Globalization.GetString("SecurityAdaptor.IsPasswordComplex.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                finally
                {
                    p_objDocIn.SelectSingleNode("//form//RetVal").InnerText = iReturn.ToString();

                    p_objXmlOut = p_objDocIn;
                }
            }
            else 
            {
                XmlElement objElement = (XmlElement)p_objDocIn.SelectSingleNode("//form//PasswordPolicy");
                if (objElement != null)
                {
                    objElement.InnerText = "false";
                }
                return true; 
            }
		}

        /// <summary>
        /// Checks whether Password Policy is enabled or not
        /// </summary>
        /// <param name="p_objDocIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool IsPasswordPolicyEnabled(XmlDocument p_objDocIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {

            XmlElement objElement = (XmlElement)p_objDocIn.SelectSingleNode("//form//PasswordPolicy");
            if (objElement != null)
            {
                if (PublicFunctions.IsPwdPolEnabled())
                {
                    objElement.InnerText = "true";
                }
                else
                {
                    objElement.InnerText = "false";
                }
            }
            p_objXmlOut = p_objDocIn;
            return true;
        }

	
	}
	
}
