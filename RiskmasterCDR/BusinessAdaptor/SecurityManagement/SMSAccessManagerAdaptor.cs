using System;
using Riskmaster.ExceptionTypes;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.Application.SecurityManagement;




namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for SMSAccessManagerAdaptor.Tanuj
	/// </summary>
	public class SMSAccessManagerAdaptor:BusinessAdaptorBase
	{
		public SMSAccessManagerAdaptor()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		public bool IsValidSMSUser(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{

            SMSAccessManager objSMSAccessMgr = new SMSAccessManager(base.userID, base.ClientId);

			try
			{
             objSMSAccessMgr.IsValidSMSUser( ref p_objDocIn ) ;
			}

			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("SMSAccessManagerAdaptor.Auth.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}

			finally
			{
				objSMSAccessMgr = null ;
			}

            p_objXmlOut = p_objDocIn ;
			return true ;
		}
		public bool IsSMSAdminUser(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{

			SMSUser objSMSAccessMgr = null;
			string sUserLogin = "" ;
			try
			{
				sUserLogin = p_objDocIn.SelectSingleNode("//Session/LoginName").InnerText.Trim().ToLower() ;
                objSMSAccessMgr = new SMSUser(sUserLogin, base.ClientId);
				if (!objSMSAccessMgr.IsSMSAdminUser(false, sUserLogin))//abansal23: MITS 15117 on 04/16/2009
				{
					throw new RMAppException(String.Format("Login Name '{0}' is not authorized to change the Admin password.", sUserLogin)) ;
				}
                     
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error );
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("SMSAccessManagerAdaptor.Auth.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}

			finally
			{
				objSMSAccessMgr = null ;
			}

			p_objXmlOut = p_objDocIn ;
			return true ;
		}
	}
}
