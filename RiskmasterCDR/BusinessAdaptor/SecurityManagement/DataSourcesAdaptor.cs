using System;
using Riskmaster.ExceptionTypes;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.Application.SecurityManagement;
using Riskmaster.Db;
using System.Collections;
using Riskmaster.Security.Encryption;//Deb: Pen Testing

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: DataSourcesAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 21-Mar-2005
	///* $Author	: Raman Bhatia
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************

	/// <summary>
	/// This class is used to call the application layer component for Riskmaster.Application.SecurityManagement.DataSources class 
	/// which is used for connecting,identifying and setting various parameters for Riskmaster DataSources.
	/// </summary>
	public class DataSourcesAdaptor : BusinessAdaptorBase
	{
		
		#region Constructor
		/// Name		: DataSourcesAdaptor
		/// Author		: Raman Bhatia
		/// Date Created: 21-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		///	<summary>
		/// Constructor for the Class
		/// </summary>
		
		public DataSourcesAdaptor()
		{
			
		}
		#endregion

		#region Public Methods

		/// Name		: Load
		/// Author		: Raman Bhatia
		/// Date Created: 21-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///	This method is a wrapper to both of the overloaded DataSources.Load method which populate the object based on 
		///	a particular DsnId or for all the dsn's.
		/// </summary>
		/// <param name="p_objDocIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document for the first overloaded method would be:
		///		<Document>
		///			<DataSources>
		///				<DataSourceId>DataSource Id</DataSourceId>
		///			</DataSources>
		///		</Document> 
		/// 
		///		The structure of input XML document for the second overloaded method would be:
		///		<Document>
		///			<DataSources></DataSources>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document
		///		The structure of output XML document for the first overloaded method would be:
		///		<Document>
		///			<DataSources>
		///				<DataSourceId></DataSourceId>
		///				<DataSourceName></DataSourceName>
		///				<RMUserId></RMUserId>
		///				<RMPassword></RMPassword>
		///				<DbType></DbType>
		///				<ConnectionString></ConnectionString>
		///				<CRC2></CRC2>
		///				<DocPathType></DocPathType>
		///				<GlobalDocPath></GlobalDocPath>
		///				<LicUpdDate></LicUpdDate>
		///				<NumLicenses></NumLicenses>
		///				<Status></Status>
		///			</DataSources>
		///		</Document>
		///		
		///		The output of second overloaded method would simply be a collection of dsn information for all the
		///		dsn's. The format of the output xml document would remain as above.
		///	</param>
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		/// 
		public bool Load(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			DataSources objDataSources = null;					//object of DataSources Class
			XmlElement objTargetElement = null;					//used for manipulating the Xml
			XmlElement objParentElement = null;					//used for manipulating the Xml
			XmlElement objChildElement = null;					//used for manipulating the Xml
			int iDsnId = 0;
			ArrayList arrlstDataSourcesCol=null;
			string sDataSourcesCol = "<DataSourceId></DataSourceId><DataSourceName></DataSourceName><RMUserId></RMUserId><RMPassword></RMPassword><DbType></DbType><ConnectionString></ConnectionString><CRC2></CRC2><DocPathType></DocPathType><GlobalDocPath></GlobalDocPath><LicUpdDate></LicUpdDate><NumLicenses></NumLicenses><Status></Status>";

			try
			{
				//choosing which overloaded function to call depending on input document
				//if DataSourcId parameter has been passsed then we require information about a particular DSN else we 
				//return information about all DSN's.
				if (p_objDocIn.SelectSingleNode("//" + "DataSourceId")!=null)
				{
					objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//" + "DataSourceId"); 
					iDsnId = Conversion.ConvertStrToInteger(objTargetElement.InnerText);

					objDataSources = new DataSources(iDsnId,false, base.ClientId);
				
					//Creating the output XML containing the DataSource information
					objParentElement = p_objXmlOut.CreateElement("Document");
					p_objXmlOut.AppendChild(objParentElement);
					
					objChildElement = p_objXmlOut.CreateElement("DataSources");
					objParentElement.AppendChild(objChildElement);
					objParentElement = objChildElement;

					objChildElement = p_objXmlOut.CreateElement("DataSourceId");
					objChildElement.InnerText= objDataSources.DataSourceId.ToString();
					objParentElement.AppendChild(objChildElement);       
         						
					objChildElement = p_objXmlOut.CreateElement("DataSourceName");
					objChildElement.InnerText= objDataSources.DataSourceName;
					objParentElement.AppendChild(objChildElement);

					objChildElement = p_objXmlOut.CreateElement("RMUserId");
					objChildElement.InnerText= objDataSources.RMUserId;
					objParentElement.AppendChild(objChildElement);
				
					objChildElement = p_objXmlOut.CreateElement("RMPassword");
					objChildElement.InnerText= objDataSources.RMPassword;
					objParentElement.AppendChild(objChildElement);   
				
					objChildElement = p_objXmlOut.CreateElement("DbType");
					objChildElement.InnerText= objDataSources.DbType.ToString();
					objParentElement.AppendChild(objChildElement);       
         		
					objChildElement = p_objXmlOut.CreateElement("ConnectionString");
					objChildElement.InnerText= objDataSources.ConnectionString.ToString();
					objParentElement.AppendChild(objChildElement);

					objChildElement = p_objXmlOut.CreateElement("CRC2");
					objChildElement.InnerText= objDataSources.CRC2.ToString();
					objParentElement.AppendChild(objChildElement);       
         		
					objChildElement = p_objXmlOut.CreateElement("DocPathType");
					objChildElement.InnerText= objDataSources.DocPathType.ToString();
					objParentElement.AppendChild(objChildElement);

					objChildElement = p_objXmlOut.CreateElement("GlobalDocPath");
					objChildElement.InnerText= objDataSources.GlobalDocPath;
					objParentElement.AppendChild(objChildElement);

					objChildElement = p_objXmlOut.CreateElement("LicUpdDate");
					objChildElement.InnerText= objDataSources.LicUpdDate.ToString();
					objParentElement.AppendChild(objChildElement);       
         		
					objChildElement = p_objXmlOut.CreateElement("NumLicenses");
					objChildElement.InnerText= objDataSources.NumLicenses.ToString();
					objParentElement.AppendChild(objChildElement);
				
					objChildElement = p_objXmlOut.CreateElement("Status");
					objChildElement.InnerText= objDataSources.Status.ToString();
					objParentElement.AppendChild(objChildElement);

					return true;
				}
				
				else
				{
					arrlstDataSourcesCol=new ArrayList();
					objDataSources = new DataSources(base.ClientId);
					objDataSources.Load(ref arrlstDataSourcesCol);

					objParentElement = p_objXmlOut.CreateElement("Document");
					foreach(DataSources objDataSourceCol in arrlstDataSourcesCol)
					{
						objChildElement=p_objXmlOut.CreateElement("DataSources");
						objChildElement.InnerXml=sDataSourcesCol;
						objChildElement.SelectSingleNode("//"+ "DataSourceId").InnerText=Conversion.ConvertObjToStr(objDataSourceCol.DataSourceId);
						objChildElement.SelectSingleNode("//"+ "DataSourceName").InnerText=Conversion.ConvertObjToStr(objDataSourceCol.DataSourceName);
						objChildElement.SelectSingleNode("//"+ "RMUserId").InnerText=Conversion.ConvertObjToStr(objDataSourceCol.RMUserId);
						objChildElement.SelectSingleNode("//"+ "RMPassword").InnerText=Conversion.ConvertObjToStr(objDataSourceCol.RMPassword);
						objChildElement.SelectSingleNode("//"+ "DbType").InnerText=Conversion.ConvertObjToStr(objDataSourceCol.DbType);
						objChildElement.SelectSingleNode("//"+ "ConnectionString").InnerText=Conversion.ConvertObjToStr(objDataSourceCol.ConnectionString);
						objChildElement.SelectSingleNode("//"+ "CRC2").InnerText=Conversion.ConvertObjToStr(objDataSourceCol.CRC2);
						objChildElement.SelectSingleNode("//"+ "DocPathType").InnerText=Conversion.ConvertObjToStr(objDataSourceCol.DocPathType);
						objChildElement.SelectSingleNode("//"+ "GlobalDocPath").InnerText=Conversion.ConvertObjToStr(objDataSourceCol.GlobalDocPath);
						objChildElement.SelectSingleNode("//"+ "LicUpdDate").InnerText=Conversion.ConvertObjToStr(objDataSourceCol.LicUpdDate);
						objChildElement.SelectSingleNode("//"+ "NumLicenses").InnerText=Conversion.ConvertObjToStr(objDataSourceCol.NumLicenses);
						objChildElement.SelectSingleNode("//"+ "Status").InnerText=Conversion.ConvertObjToStr(objDataSourceCol.Status);
						objParentElement.AppendChild(objChildElement);
						p_objXmlOut.AppendChild(objParentElement);
                
					}
				}
				return true;
				
				}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException, Globalization.GetString("DataSourcesAdaptor.Load.error", base.ClientId) ,BusinessAdaptorErrorType.Error);
				return false;

			}
			finally
			{
				objDataSources = null;
				objChildElement = null;
				objParentElement = null;
				objTargetElement = null;
				arrlstDataSourcesCol = null;
			}
			
		}

		/// Name		: Save
		/// Author		: Raman Bhatia
		/// Date Created: 21-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///		This method is a wrapper to DataSources.Save method which in turn is a wrapper over SaveData() funuction which actually saves the object state to database.
		///			
		/// </summary>
		/// <param name="p_objDocIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<<Document>
		///			<DataSources>
		///				<DataSourceId></DataSourceId>
		///				<DSName></DSName>
		///				<LoginName></LoginName>
		///				<Password></Password>
		///				<DataSourceType></DataSourceType>
		///				<ConnectionStr></ConnectionStr>
		///				<CRC2></CRC2>
		///				<DocPathType></DocPathType>
		///				<DocumentPath></DocumentPath>
		///				<LicUpdateDate></LicUpdateDate>
		///				<NumberOfLicenses></NumberOfLicenses>
		///				<ModuleSecurityEnabled></ModuleSecurityEnabled>
		///			</DataSources>
		///		</Document>
		 
		/// </param>
		/// <param name="p_objXmlOut">As the function is a wrapper to a save function 
		/// hence it would not return a XML document.
		/// </param>
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		
		public bool Save(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			DataSources objDataSources = null;					//object of DataSources Class
			XmlElement objTargetElement = null;					//used for manipulating the Xml
			string sDataSourceName=""; 
			int iDataSourceId=0;
			bool bStatus=false;
            bool bpwdchanged = false;
			string sRMUserId="";
			string sRMPassword="";
			int iDbType = 0;
			string sConnectionString="";
			int iNumLicenses=0;
			string sLicUpdDate = "";
			string sCRC2="";
			int iDocPathType=0;
			string sGlobalDocPath="";
			string sOrgsecflag = "0";			
            string sUserName = "";                              //gagnihotri MITS 11995 Changes made for Audit table
			try
			{
				//extracting the user information from input XML
				

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='dsnid']"); 
				iDataSourceId = Conversion.ConvertStrToInteger(objTargetElement.InnerText);
                
				objTargetElement=(XmlElement)p_objDocIn.SelectSingleNode("//control[@name='dsname']");
				sDataSourceName = objTargetElement.InnerText;
				
				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='docpath']"); 
				sGlobalDocPath = objTargetElement.InnerText;
               
				objTargetElement=(XmlElement)p_objDocIn.SelectSingleNode("//control[@name='systemloginname']");
				sRMUserId = objTargetElement.InnerText;
                 
                objTargetElement=(XmlElement)p_objDocIn.SelectSingleNode("//control[@name='systempassword']");
                sRMPassword= objTargetElement.InnerText;

                objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='systempwdchanged']");
                bpwdchanged = Conversion.ConvertStrToBool(objTargetElement.InnerText);

				objTargetElement=(XmlElement)p_objDocIn.SelectSingleNode("//control[@name='orgsecflag']");
				sOrgsecflag= objTargetElement.InnerText;

				objTargetElement=(XmlElement)p_objDocIn.SelectSingleNode("//control[@name='connectionstring']");
				sConnectionString = objTargetElement.InnerText;

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='optservers']"); 
				iDocPathType = Conversion.ConvertStrToInteger(objTargetElement.Attributes["value"].Value);

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='licupdatedate']"); 
				sLicUpdDate = objTargetElement.InnerText;
				
				objTargetElement=(XmlElement)p_objDocIn.SelectSingleNode("//control[@name='workstations']");
				iNumLicenses = Conversion.ConvertStrToInteger(objTargetElement.InnerText);
                
				objTargetElement=(XmlElement)p_objDocIn.SelectSingleNode("//control[@name='chkModuleSec']");
				bStatus = Conversion.ConvertStrToBool(objTargetElement.InnerText);
				
                //gagnihotri MITS 11995 Changes made for Audit table
                objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='hdUserLoginName']");
                if (objTargetElement != null)
                    sUserName = objTargetElement.InnerText;
                objDataSources = new DataSources(sUserName, base.ClientId);
				//objDataSources = new DataSources();

				//setting the properties of DataSources class to set Data Sources data.

				objDataSources.DataSourceId = iDataSourceId;
				objDataSources.DataSourceName = sDataSourceName;
                if(iDocPathType == 1)
                 sGlobalDocPath = RMCryptography.DecryptString(sGlobalDocPath);//Deb :Pen Testing
				//MITS 9002 The value from UI could be encrypted connection string
				sGlobalDocPath = Utilities.ParseDocPathCredentials(sGlobalDocPath, iDocPathType);

				objDataSources.GlobalDocPath = sGlobalDocPath;
				objDataSources.RMUserId  = sRMUserId;
                if (bpwdchanged)
                {
                    objDataSources.RMPassword = sRMPassword;
                }
                else
                {
                    objDataSources.RMPassword = RMCryptography.DecryptString(sRMPassword);
                }
				objDataSources.DbType = (eDatabaseType)iDbType;
                objDataSources.ConnectionString = RMCryptography.DecryptString(sConnectionString);//Deb : Pen Testing
				objDataSources.CRC2 = sCRC2;
				objDataSources.DocPathType = iDocPathType;
				objDataSources.LicUpdDate = Conversion.ToDate(sLicUpdDate);
				objDataSources.NumLicenses = iNumLicenses;
				objDataSources.Status = bStatus;
				objDataSources.OrgSecFlag = Conversion.ConvertStrToBool(sOrgsecflag);
				
				//Calling the Save function to write the Data Sources Data
				objDataSources.Save();
				
                //to copy the data - amit
                p_objXmlOut = p_objDocIn;
				return true;


			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("DataSourcesAdaptor.Save.error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;

			}
			finally
			{
				objDataSources = null;
				objTargetElement = null;
			}
			
		}

		/// Name		: Remove
		/// Author		: Raman Bhatia
		/// Date Created: 22-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///		This method is a wrapper to DataSources.Remove method which removes the dsn from database.
		///			
		/// </summary>
		/// <param name="p_objDocIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<DataSources>
		///				<DataSourceId></DataSourceId>
		///			</DataSources>
		///		</Document>
		 
		/// </param>
		/// <param name="p_objXmlOut">As the function is a wrapper to a remove function 
		/// hence it would not return a XML document.
		/// </param>
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		
		public bool Remove(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			DataSources objDataSources = null;					//object of DataSources Class
			XmlElement objTargetElement = null;					//used for manipulating the Xml
			int iDataSourceId = 0;
			
			try
			{
				//extracting the user information from input XML
				
				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//" + "DataSourceId"); 
				iDataSourceId = Conversion.ConvertStrToInteger(objTargetElement.InnerText);

				objDataSources = new DataSources(base.ClientId);
				
				//Calling the Remove function to delete the Data Sources data
				objDataSources.Remove(iDataSourceId);
				return true;

			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("DataSourcesAdaptor.Remove.error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objDataSources = null;
				objTargetElement = null;
			}
			
		}
		#endregion

	}
}
