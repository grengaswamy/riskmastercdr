using System;
using Riskmaster.ExceptionTypes;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.Application.SecurityManagement;
using System.Collections;
using Riskmaster.Security;
using System.Threading;
using Riskmaster.Db;
using System.Diagnostics ;
using Riskmaster.Application.AcrosoftWebserviceWrapper;

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: UserLoginsAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 22-Mar-2005
	///* $Author	: Raman Bhatia
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************

	/// <summary>
	/// This class is used to call the application layer component for Riskmaster.Application.SecurityManagement.UserLogins 
	/// class is used to retrieve the user information,it also check for the user privilege to use the application.	
	/// </summary>
	public class UserLoginsAdaptor : BusinessAdaptorBase
	{
		private bool m_bIsUpdadteHistoryCalled = false;
		public bool UpdadteHistoryCalled
		{
			set
			{
               m_bIsUpdadteHistoryCalled = value;
			}
			get
			{
              return m_bIsUpdadteHistoryCalled ;
			}
		}
        //Raman Bhatia:11/13/2008 MITS 12315
        //This variable would store current DB password to be consumed by mcm change password API
        private string m_sCurrentPasswordInDB = String.Empty;

        private int m_iClientId = 0;
		#region Constructor
		/// Name		: UserLoginsAdaptor
		/// Author		: Raman Bhatia
		/// Date Created: 22-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		///	<summary>
		/// Constructor for the Class
        /// </summary>
        /// Jira RMA-5969
        /// Ash - cloud, Unique case for this Adaptor, which is called from both webservice and another adaptor(UsersAdaptor)
        public UserLoginsAdaptor()
        { }
        // Ash - cloud, Unique case for this Adaptor, which is called from both webservice and another adaptor(UsersAdaptor)
        public UserLoginsAdaptor(int p_iClientId)
        { 
            if (p_iClientId != 0)
                base.ClientId = p_iClientId;
		}
		#endregion

		#region Public Methods
		/// Name		: Load
		/// Author		: Raman Bhatia
		/// Date Created: 21-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///	This method is a wrapper to both of the overloaded UserLogins.Load methods which 
		///	retrieve information from USER_DETAILS_TABLE through DbReader object, that is further passed 
		///	to LoadData(). First retrieves information for a particular UserId and DataSourceId whereas
		///	second method retrieves all rows of the USER_DETAILS_TABLE.
		///	 
		/// </summary>
		/// <param name="p_objDocIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document for the first overloaded method would be:
		///		<Document>
		///			<UserLogins>
		///				<UserId>User Id</UserId>
		///				<DataSourceId></DataSourceId>
		///			</UserLogins>
		///		</Document> 
		///		
		///		The structure of input XML document for the second overloaded method would be:
		///		<Document>
		///			<UserLogins></UserLogins>
		///		</Document>
		/// 
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document
		///		The structure of output XML document for the first overloaded method would be:
		///		<Document>
		///			<UserLogins>
		///				<DatabaseId></DatabaseId>
		///				<UserId></UserId>
		///				<LoginName></LoginName>
		///				<Password></Password>
		///				<PrivilegesExpire></PrivilegesExpire>
		///				<PasswordExpire></PasswordExpire>
		///				<DocumentPath></DocumentPath>
		///				<SunStart></SunStart>
		///				<SunEnd></SunEnd>
		///				<MonStart></MonStart>
		///				<MonEnd></MonEnd>
		///				<TueStart></TueStart>
		///				<TueEnd></TueEnd>
		///				<WedStart></WedStart>
		///				<WedEnd></WedEnd>
		///				<ThuStart></ThuStart>
		///				<ThuEnd></ThuEnd>
		///				<FriStart></FriStart>
		///				<FriEnd></FriEnd>
		///				<SatStart></SatStart>
		///				<SatEnd></SatEnd>
		///			</UserLogins>
		///		</Document>
		///		The output of second overloaded method would simply be a collection of userLogin information for all the
		///		users. The format of the output xml document would remain as above.
		///	</param>
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		
		public bool Load(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			UserLogins objUserLogins = null;					//object of UserLogins Class
			XmlElement objTargetElement = null;					//used for manipulating the Xml
			XmlElement objParentElement = null;					//used for manipulating the Xml
			XmlElement objChildElement = null;					//used for manipulating the Xml
			int iUserId = 0;
			int iDataSourceId = 0;
			ArrayList arrlstUserLoginsCol=null;
			string sUserLoginsCol = "<DatabaseId></DatabaseId><UserId></UserId><LoginName></LoginName><Password></Password><PrivilegesExpire></PrivilegesExpire><PasswordExpire></PasswordExpire><DocumentPath></DocumentPath><SunStart></SunStart><SunEnd></SunEnd><MonStart></MonStart><MonEnd></MonEnd><TueStart></TueStart><TueEnd></TueEnd><WedStart></WedStart><WedEnd></WedEnd><ThuStart></ThuStart><ThuEnd></ThuEnd><FriStart></FriStart><FriEnd></FriEnd><SatStart></SatStart><SatEnd></SatEnd>";
 
			try
			{
				//choosing which overloaded function to call depending on input document
				//if UserId and DsnId parameters have  been passsed then we require information about a particular combination else we 
				//return information about all userLogins..
				if ((p_objDocIn.SelectSingleNode("//" + "UserId")!=null) && (p_objDocIn.SelectSingleNode("//" + "DatabaseId")!=null))
				{
					objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//" + "UserId"); 
					iUserId = Conversion.ConvertStrToInteger(objTargetElement.InnerText);
					
					objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//" + "DatabaseId"); 
					iDataSourceId = Conversion.ConvertStrToInteger(objTargetElement.InnerText);

					objUserLogins = new UserLogins(iUserId , iDataSourceId, base.ClientId);
				
					//Creating the output XML containing the User information
					objParentElement = p_objXmlOut.CreateElement("Document");
					p_objXmlOut.AppendChild(objParentElement);
					
					objChildElement = p_objXmlOut.CreateElement("UserLogins");
					objParentElement.AppendChild(objChildElement);
					objParentElement = objChildElement;
								         		
					objChildElement = p_objXmlOut.CreateElement("DatabaseId");
					objChildElement.InnerText= objUserLogins.DatabaseId.ToString();
					objParentElement.AppendChild(objChildElement);

					objChildElement = p_objXmlOut.CreateElement("UserId");
					objChildElement.InnerText= objUserLogins.UserId.ToString();
					objParentElement.AppendChild(objChildElement);       
					
					objChildElement = p_objXmlOut.CreateElement("LoginName");
					objChildElement.InnerText= objUserLogins.LoginName;
					objParentElement.AppendChild(objChildElement);       
         		
					objChildElement = p_objXmlOut.CreateElement("Password");
					objChildElement.InnerText= objUserLogins.Password;
					objParentElement.AppendChild(objChildElement);

					objChildElement = p_objXmlOut.CreateElement("PrivilegesExpire");
					objChildElement.InnerText= objUserLogins.PrivilegesExpire.ToString();
					objParentElement.AppendChild(objChildElement);       
         		
					objChildElement = p_objXmlOut.CreateElement("PasswordExpire");
					objChildElement.InnerText= objUserLogins.PasswordExpire.ToString();
					objParentElement.AppendChild(objChildElement);
				
					objChildElement = p_objXmlOut.CreateElement("DocumentPath");
					objChildElement.InnerText= objUserLogins.DocumentPath;
					objParentElement.AppendChild(objChildElement);       
         		
					objChildElement = p_objXmlOut.CreateElement("SunStart");
					objChildElement.InnerText= objUserLogins.SunStart.ToString();
					objParentElement.AppendChild(objChildElement);
				
					objChildElement = p_objXmlOut.CreateElement("SunEnd");
					objChildElement.InnerText= objUserLogins.SunEnd.ToString();
					objParentElement.AppendChild(objChildElement);       
         		
					objChildElement = p_objXmlOut.CreateElement("MonStart");
					objChildElement.InnerText= objUserLogins.MonStart.ToString();
					objParentElement.AppendChild(objChildElement);
				
					objChildElement = p_objXmlOut.CreateElement("MonEnd");
					objChildElement.InnerText= objUserLogins.MonEnd.ToString();
					objParentElement.AppendChild(objChildElement);       

					objChildElement = p_objXmlOut.CreateElement("TueStart");
					objChildElement.InnerText= objUserLogins.TueStart.ToString();
					objParentElement.AppendChild(objChildElement);
				
					objChildElement = p_objXmlOut.CreateElement("TueEnd");
					objChildElement.InnerText= objUserLogins.TueEnd.ToString();
					objParentElement.AppendChild(objChildElement);       

					objChildElement = p_objXmlOut.CreateElement("WedStart");
					objChildElement.InnerText= objUserLogins.WedStart.ToString();
					objParentElement.AppendChild(objChildElement);
				
					objChildElement = p_objXmlOut.CreateElement("WedEnd");
					objChildElement.InnerText= objUserLogins.WedEnd.ToString();
					objParentElement.AppendChild(objChildElement);       

					objChildElement = p_objXmlOut.CreateElement("ThuStart");
					objChildElement.InnerText= objUserLogins.ThuStart.ToString();
					objParentElement.AppendChild(objChildElement);
				
					objChildElement = p_objXmlOut.CreateElement("ThuEnd");
					objChildElement.InnerText= objUserLogins.ThuEnd.ToString();
					objParentElement.AppendChild(objChildElement);       

					objChildElement = p_objXmlOut.CreateElement("FriStart");
					objChildElement.InnerText= objUserLogins.FriStart.ToString();
					objParentElement.AppendChild(objChildElement);
				
					objChildElement = p_objXmlOut.CreateElement("FriEnd");
					objChildElement.InnerText= objUserLogins.FriEnd.ToString();
					objParentElement.AppendChild(objChildElement);       

					objChildElement = p_objXmlOut.CreateElement("SatStart");
					objChildElement.InnerText= objUserLogins.SatStart.ToString();
					objParentElement.AppendChild(objChildElement);
				
					objChildElement = p_objXmlOut.CreateElement("SatEnd");
					objChildElement.InnerText= objUserLogins.SatEnd.ToString();
					objParentElement.AppendChild(objChildElement);       

					return true;
				}
				arrlstUserLoginsCol=new ArrayList();
				objUserLogins = new UserLogins(base.ClientId);
			
				objUserLogins.Load(ref arrlstUserLoginsCol);

				objParentElement = p_objXmlOut.CreateElement("Document");
				foreach(UserLogins objUserLoginCol in arrlstUserLoginsCol)
				{
					objChildElement=p_objXmlOut.CreateElement("UserLogins");
					objChildElement.InnerXml=sUserLoginsCol;
					objChildElement.SelectSingleNode("//"+ "DatabaseId").InnerText=Conversion.ConvertObjToStr(objUserLoginCol.DatabaseId);
					objChildElement.SelectSingleNode("//"+ "UserId").InnerText=Conversion.ConvertObjToStr(objUserLoginCol.UserId);
					objChildElement.SelectSingleNode("//"+ "LoginName").InnerText=Conversion.ConvertObjToStr(objUserLoginCol.LoginName);
					objChildElement.SelectSingleNode("//"+ "Password").InnerText=Conversion.ConvertObjToStr(objUserLoginCol.Password);
					objChildElement.SelectSingleNode("//"+ "PrivilegesExpire").InnerText=Conversion.ConvertObjToStr(objUserLoginCol.PrivilegesExpire);
					objChildElement.SelectSingleNode("//"+ "PasswordExpire").InnerText=Conversion.ConvertObjToStr(objUserLoginCol.PasswordExpire);
					objChildElement.SelectSingleNode("//"+ "DocumentPath").InnerText=Conversion.ConvertObjToStr(objUserLoginCol.DocumentPath);
					objChildElement.SelectSingleNode("//"+ "SunStart").InnerText=Conversion.ConvertObjToStr(objUserLoginCol.SunStart);
					objChildElement.SelectSingleNode("//"+ "SunEnd").InnerText=Conversion.ConvertObjToStr(objUserLoginCol.SunEnd);
					objChildElement.SelectSingleNode("//"+ "MonStart").InnerText=Conversion.ConvertObjToStr(objUserLoginCol.MonStart);
					objChildElement.SelectSingleNode("//"+ "MonEnd").InnerText=Conversion.ConvertObjToStr(objUserLoginCol.MonEnd);
					objChildElement.SelectSingleNode("//"+ "TueStart").InnerText=Conversion.ConvertObjToStr(objUserLoginCol.TueStart);
					objChildElement.SelectSingleNode("//"+ "TueEnd").InnerText=Conversion.ConvertObjToStr(objUserLoginCol.TueEnd);
					objChildElement.SelectSingleNode("//"+ "WedStart").InnerText=Conversion.ConvertObjToStr(objUserLoginCol.WedStart);
					objChildElement.SelectSingleNode("//"+ "WedEnd").InnerText=Conversion.ConvertObjToStr(objUserLoginCol.WedEnd);
					objChildElement.SelectSingleNode("//"+ "ThuStart").InnerText=Conversion.ConvertObjToStr(objUserLoginCol.ThuStart);
					objChildElement.SelectSingleNode("//"+ "ThuEnd").InnerText=Conversion.ConvertObjToStr(objUserLoginCol.ThuEnd);
					objChildElement.SelectSingleNode("//"+ "FriStart").InnerText=Conversion.ConvertObjToStr(objUserLoginCol.FriStart);
					objChildElement.SelectSingleNode("//"+ "FriEnd").InnerText=Conversion.ConvertObjToStr(objUserLoginCol.FriEnd);
					objChildElement.SelectSingleNode("//"+ "SatStart").InnerText=Conversion.ConvertObjToStr(objUserLoginCol.SatStart);
					objChildElement.SelectSingleNode("//"+ "SatEnd").InnerText=Conversion.ConvertObjToStr(objUserLoginCol.SatEnd);

					objParentElement.AppendChild(objChildElement);
					p_objXmlOut.AppendChild(objParentElement);
                
				}
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut = new BusinessAdaptorErrors(base.ClientId);
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut = new BusinessAdaptorErrors(base.ClientId);
                p_objErrOut.Add(p_objException, Globalization.GetString("UserLoginsAdaptor.Load.error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objUserLogins = null;
				objParentElement = null;
				objChildElement = null;
				objTargetElement = null;
			}

		}
		
		/// Name		: Save
		/// Author		: Raman Bhatia
		/// Date Created: 22-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///		This method is a wrapper to UserLogins.Save method which in turn is a wrapper over SaveData() funuction which actually saves the object state to database.
		///			
		/// </summary>
		/// <param name="p_objDocIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<UserLogins>
		///				<DatabaseId></DatabaseId>
		///				<UserId></UserId>
		///				<LoginName></LoginName>
		///				<Password></Password>
		///				<PrivilegesExpire></PrivilegesExpire>
		///				<PasswordExpire></PasswordExpire>
		///				<DocumentPath></DocumentPath>
		///				<SunStart></SunStart>
		///				<SunEnd></SunEnd>
		///				<MonStart></MonStart>
		///				<MonEnd></MonEnd>
		///				<TueStart></TueStart>
		///				<TueEnd></TueEnd>
		///				<WedStart></WedStart>
		///				<WedEnd></WedEnd>
		///				<ThuStart></ThuStart>
		///				<ThuEnd></ThuEnd>
		///				<FriStart></FriStart>
		///				<FriEnd></FriEnd>
		///				<SatStart></SatStart>
		///				<SatEnd></SatEnd>
		///			</UserLogins>
		///		</Document>
		 
		/// </param>
		/// <param name="p_objXmlOut">As the function is a wrapper to a save function 
		/// hence it would not return a XML document.
		/// </param>
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		
		public bool Save(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			UserLogins objUserLogins = null;					//object of UserLogins Class
			XmlElement objTargetElement = null;	
			//used for manipulating the Xml
			ModuleGroups objModuleGrps=null;
			string sTempConnStr="";
			int iExistingMGId=0;
            int iModuleGrpId=0;
			int iDatabaseId = 0;
			int iUserId = 0;
			string sLoginName;
			string sPassword= string.Empty ;
			string sForcePwdChange;
			string sPermExpire ;
			string sPermExpireDate="" ;
			string sDocumentPath ;
			string sSunStart =null;
			string sSunEnd =null;
			string sMonStart=null ;
			string sMonEnd=null ;
			string sTueStart =null;
			string sTueEnd =null;
			string sWedStart=null ;
			string sWedEnd =null;
			string sThuStart=null ;
			string sThuEnd =null;
			string sFriStart=null ;
			string sFriEnd=null ;
			string sSatStart=null ;
			string sSatEnd =null;
			Thread objADWorker=null;
            string sUserName = "";          //gagnihotri MITS 11995 Changes made for Audit table Start
            //Arnab: MITS-10662 - Commented the declaration - not needed
            //bool bisAdminUser = false;
			try
			{                  
				//Arnab: MITS-10662 - Comment this 
                /*
				if(p_objDocIn.SelectSingleNode("//control[@name = 'hdIsSMSAdminUser']").InnerText == "True")
				{
					bisAdminUser = true ;
				}*/
				//extracting the user information from input XML
				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='userid']");
				iUserId = Conversion.ConvertStrToInteger(objTargetElement.InnerText);
   
				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='dsnid']");
				iDatabaseId = Conversion.ConvertStrToInteger(objTargetElement.InnerText);
			    
				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='loginname']");
				sLoginName = objTargetElement.InnerText;
                //rsolanki2 : domain authentication updates                
                //if (!Riskmaster.Application.SecurityManagement.Security.bIsDomainAuthenticationEnabled())
                //{
                    
                //}
                //rsolanki2 : Domain authentication: updaes for mits 12251s
                objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='password']");
                if (objTargetElement !=null)
                {
                    sPassword = objTargetElement.InnerText;    
                }                

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='forcepwdchange']");
				if(objTargetElement != null)
					sForcePwdChange = objTargetElement.InnerText;
				else
					sForcePwdChange = "False";

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='permexpires']");
				sPermExpire = objTargetElement.InnerText;
					objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='expdate']");
					sPermExpireDate=objTargetElement.InnerText;
				

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='filepath']");
				sDocumentPath = objTargetElement.InnerText;

                //gagnihotri MITS 11995 Changes made for Audit table Start
                objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='hdUserLoginName']");
                if (objTargetElement != null)
                    sUserName = objTargetElement.InnerText;

				if(p_objDocIn.SelectSingleNode("//control[@name='Sun']").InnerText=="True")
				{
					sSunStart = PublicFunctions.CheckAndSetDefaultVal(p_objDocIn.SelectSingleNode("//control[@name='Sun']").Attributes["value1"].Value);
					sSunEnd = PublicFunctions.CheckAndSetDefaultVal(p_objDocIn.SelectSingleNode("//control[@name='Sun']").Attributes["value2"].Value);
				}
				if(p_objDocIn.SelectSingleNode("//control[@name='Mon']").InnerText=="True")
				{
					
						sMonStart = PublicFunctions.CheckAndSetDefaultVal(p_objDocIn.SelectSingleNode("//control[@name='Mon']").Attributes["value1"].Value);
						sMonEnd = PublicFunctions.CheckAndSetDefaultVal(p_objDocIn.SelectSingleNode("//control[@name='Mon']").Attributes["value2"].Value);
					
				}
				
				if(p_objDocIn.SelectSingleNode("//control[@name='Tue']").InnerText=="True")
				{
					sTueStart = PublicFunctions.CheckAndSetDefaultVal(p_objDocIn.SelectSingleNode("//control[@name='Tue']").Attributes["value1"].Value);
					sTueEnd = PublicFunctions.CheckAndSetDefaultVal(p_objDocIn.SelectSingleNode("//control[@name='Tue']").Attributes["value2"].Value);
				}
				
				if(p_objDocIn.SelectSingleNode("//control[@name='Wed']").InnerText=="True")
				{
					sWedStart = PublicFunctions.CheckAndSetDefaultVal(p_objDocIn.SelectSingleNode("//control[@name='Wed']").Attributes["value1"].Value);
					sWedEnd = PublicFunctions.CheckAndSetDefaultVal(p_objDocIn.SelectSingleNode("//control[@name='Wed']").Attributes["value2"].Value);
			    }
				
				
				if(p_objDocIn.SelectSingleNode("//control[@name='Thu']").InnerText=="True")
				{
					sThuStart = PublicFunctions.CheckAndSetDefaultVal(p_objDocIn.SelectSingleNode("//control[@name='Thu']").Attributes["value1"].Value);
					sThuEnd = PublicFunctions.CheckAndSetDefaultVal(p_objDocIn.SelectSingleNode("//control[@name='Thu']").Attributes["value2"].Value);
				}
				
				if(p_objDocIn.SelectSingleNode("//control[@name='Fri']").InnerText=="True")
				{
					sFriStart = PublicFunctions.CheckAndSetDefaultVal(p_objDocIn.SelectSingleNode("//control[@name='Fri']").Attributes["value1"].Value);
					sFriEnd = PublicFunctions.CheckAndSetDefaultVal(p_objDocIn.SelectSingleNode("//control[@name='Fri']").Attributes["value2"].Value);
				}
				
				if(p_objDocIn.SelectSingleNode("//control[@name='Sat']").InnerText=="True")
				{
					sSatStart = PublicFunctions.CheckAndSetDefaultVal(p_objDocIn.SelectSingleNode("//control[@name='Sat']").Attributes["value1"].Value);
					sSatEnd = PublicFunctions.CheckAndSetDefaultVal(p_objDocIn.SelectSingleNode("//control[@name='Sat']").Attributes["value2"].Value);
				}
				

				//gagnihotri MITS 11995 Changes made for Audit table
                //objUserLogins = new UserLogins(false);
                objUserLogins = new UserLogins(false, sUserName, base.ClientId);
				objUserLogins.UserId = iUserId;
				objUserLogins.DatabaseId = iDatabaseId;
				objUserLogins.LoginName = sLoginName;
				objUserLogins.Password  = sPassword;
				objUserLogins.ForceChangePwd = Conversion.ConvertStrToInteger(sForcePwdChange);
					if(PublicFunctions.GetDateFrmCalDate(sPermExpireDate)==DateTime.MinValue)
					{
                      objUserLogins.PrivilegesExpire=DateTime.MinValue;
					  objUserLogins.PasswordExpire=DateTime.MinValue;
					}
					else
					{
						objUserLogins.PrivilegesExpire = PublicFunctions.GetDateFrmCalDate(sPermExpireDate);
						objUserLogins.PasswordExpire = PublicFunctions.GetDateFrmCalDate(sPermExpireDate);
					}
				objUserLogins.DocumentPath = sDocumentPath;
				objUserLogins.SunStart = sSunStart;
				objUserLogins.SunEnd = sSunEnd;
				objUserLogins.MonStart = sMonStart;
				objUserLogins.MonEnd = sMonEnd;
				objUserLogins.TueStart = sTueStart;
				objUserLogins.TueEnd = sTueEnd;
				objUserLogins.WedStart = sWedStart;
				objUserLogins.WedEnd = sWedEnd;
				objUserLogins.ThuStart = sThuStart;
				objUserLogins.ThuEnd = sThuEnd;
				objUserLogins.FriStart = sFriStart;
				objUserLogins.FriEnd = sFriEnd;
				objUserLogins.SatStart = sSatStart;
				objUserLogins.SatEnd = sSatEnd;
				if(this.LoginNameAlreadyinUse( ref objUserLogins ))
				{
                    throw new RMAppException(String.Format(Globalization.GetString("UserLoginsAdaptor.LoginAlreadyExists.error", base.ClientId), objUserLogins.LoginName));
				}
                
                //Raman: 10/14/2009 MITS 18120
                //We really do not want to touch any SMS permission on this screen.

                //Arnab: MITS-10662 - modify section /**************************************/
                //SMSUser objSMSUser = null;
                //try
                //{
                //    objSMSUser = new SMSUser(objUserLogins.UserId);
                //    if (objSMSUser.RmLoginName != "")
                //    {                        
                //        objSMSUser.RmLoginName = objUserLogins.LoginName;
                //        objSMSUser.RmPwd = objUserLogins.Password;
                //        objSMSUser.Save();
                //    }
                //}
                //finally
                //{
                //    objSMSUser = null;
                //}                              

                //Arnab: MITS-10662 - Comment this section as Access to SMS is moved out of this screen
                #region MITS-10662 commented section
                /*
                SMSUser objSMSUser = null ;
				if(p_objDocIn.SelectSingleNode("//control[@name = 'SMSAccess']").InnerText.Trim().ToLower()=="true")
				{					
					try
					{
						objSMSUser = new SMSUser( objUserLogins.LoginName ) ;
						// objSMSUser.RmPwd =PublicFunctions.Encrypt(objUserLogins.Password);
						objSMSUser.RmPwd =objUserLogins.Password;
						bisAdminUser = objSMSUser.IsSMSAdminUser( bisAdminUser , objUserLogins.LoginName ) ;
						objSMSUser.IsAdminUser = bisAdminUser ;
						if(objSMSUser.RecordId ==0)
						{
							objSMSUser = new SMSUser( iUserId ) ;
							//objSMSUser.RmPwd =PublicFunctions.Encrypt(objUserLogins.Password);
							objSMSUser.IsAdminUser = bisAdminUser ;
							objSMSUser.RmLoginName = objUserLogins.LoginName ;
							// JP 10.06.2006   The SMSUser class does the encryption.     objSMSUser.RmPwd =PublicFunctions.Encrypt(objUserLogins.Password);
							objSMSUser.RmPwd = objUserLogins.Password;
							if(bisAdminUser)
							{
                                  objSMSUser.UserId = (-1) * iUserId ;
							}
							else
							{
								  objSMSUser.UserId = iUserId ;
							}
						}
						else
						{
							if(objSMSUser.UserId.ToString().IndexOf("-")!=-1)
							{
                              objSMSUser.UserId = (-1) * iUserId ;
                              objSMSUser.DeleteSMSUser( iUserId ) ;
							}
						}
						objSMSUser.Save() ;
					}
					finally
					{
						objSMSUser = null;
					}
				}
				else
				{
					try
					{
						objSMSUser = new SMSUser( iUserId ) ;
						objSMSUser.Remove() ;
					}
					finally
					{
						objSMSUser = null;
					}
                }*/
                #endregion                

				if(PublicFunctions.IsPwdPolEnabled())
				{
					if(!this.UpdadteHistoryCalled)
					{
                        PublicFunctions.UpdatePasswordHistory(objUserLogins.UserId, objUserLogins.DatabaseId, sPassword, "", true, base.ClientId); 
					}
				}
                objUserLogins.UpdateLoginPassword(ref objUserLogins);

				//Calling the Save function to write the User Data
				objUserLogins.Save();
				
				if((XmlElement)p_objDocIn.SelectSingleNode("//control[@name='hdSelectedModuleGrpId']")!=null)
				{
					objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='hdSelectedModuleGrpId']");
					iModuleGrpId = Conversion.ConvertStrToInteger(objTargetElement.InnerText);
					if(iModuleGrpId!=0)
					{
						objTargetElement= (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='hdPostback']");
						objTargetElement.InnerText="true";
						sTempConnStr=PublicFunctions.GetConnStr(iDatabaseId, base.ClientId);
					  
						if(!PublicFunctions.CheckUserMembership(ref iExistingMGId,iUserId,sTempConnStr,m_iClientId))
						{
							//gagnihotri MITS 11995 Changes made for Audit table
							 //objModuleGrps=new ModuleGroups(sTempConnStr);
							 objModuleGrps=new ModuleGroups(sTempConnStr, sUserName, base.ClientId); //Ash - cloud
							objModuleGrps.SaveUsers(iModuleGrpId,iUserId);
						}
					}
				}
				
				p_objXmlOut=p_objDocIn;
				return true;
			}
			catch(RMAppException p_objException)
			{
                p_objErrOut = new BusinessAdaptorErrors(base.ClientId);
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.SystemError);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut = new BusinessAdaptorErrors(base.ClientId);
                p_objErrOut.Add(p_objException, Globalization.GetString("UserLoginsAdaptor.Save.error", base.ClientId), BusinessAdaptorErrorType.SystemError);
				return false;
			}
			finally
			{
				objUserLogins = null;
				objTargetElement = null;
			}

			
		}
		
		/// Name		: Remove
		/// Author		: Raman Bhatia
		/// Date Created: 22-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///		This method is a wrapper to UserLogins.Remove method which removes the userLogin from database.
		///			
		/// </summary>
		/// <param name="p_objDocIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<UserLogins>
		///				<DataSourceId></DataSourceId>
		///				<UserId></UserId>
		///			</UserLogins>
		///		</Document>
		 
		/// </param>
		/// <param name="p_objXmlOut">As the function is a wrapper to a remove function 
		/// hence it would not return a XML document.
		/// </param>
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		
		public bool Remove(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			UserLogins objUserLogins = null;					//object of UserLogins Class
			XmlElement objTargetElement = null;					//used for manipulating the Xml
			int iDataSourceId = 0;
			int iUserId = 0;
			
			try
			{
				//extracting the user information from input XML
				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//" + "DataSourceId"); 
				iDataSourceId = Conversion.ConvertStrToInteger(objTargetElement.InnerText);
				
				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//" + "UserId"); 
				iUserId = Conversion.ConvertStrToInteger(objTargetElement.InnerText);

				objUserLogins = new UserLogins(base.ClientId);
				
				//Calling the Remove function to delete the user data
				objUserLogins.Remove(iUserId , iDataSourceId);
				return true;
			}
			catch(RMAppException p_objException)
			{
                p_objErrOut = new BusinessAdaptorErrors(base.ClientId);
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut = new BusinessAdaptorErrors(base.ClientId);
                p_objErrOut.Add(p_objException, Globalization.GetString("UserLoginsAdaptor.Remove.error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objUserLogins = null;
				objTargetElement = null;
			}

			
		}
		
		private bool LoginNameAlreadyinUse(ref UserLogins p_objLoginToSave)
		{
            string sSql = "" ;
			DbReader objReader = null ;
            //MITS-10662 - Replaced the ' with '' for SQL query to run properly
            string p_sLoginToSave = string.Empty;
            p_sLoginToSave = p_objLoginToSave.LoginName.Trim().Replace("'", "''");
            
            try
			{
                //MITS-10662 - Modified the SQL query and used the "p_sLoginToSave"
                //sSql = "Select * from User_Details_Table where user_id not in ( "+p_objLoginToSave.UserId+")" + " and  LOGIN_NAME = '"+ p_objLoginToSave.LoginName.Trim() + "'" ;
                sSql = "Select * from User_Details_Table where user_id not in ( " + p_objLoginToSave.UserId + ")" + " and  LOGIN_NAME = '" + p_sLoginToSave + "'";
				objReader = DbFactory.GetDbReader( SecurityDatabase.GetSecurityDsn(base.ClientId) , sSql ) ;
				if( objReader.Read() )
					return true;
				else
					return false;
			}
			catch(Exception p_objErr)
			{
				throw p_objErr ;
			}
			finally
			{
				if(objReader != null)
				{
					objReader.Close();
					objReader = null ;
				}
			}
		}
		private bool VerifyExistingPassword(ref XmlDocument p_objDoc)
		{
			bool bRetVal = false ;
			DbReader objReader = null ;
			string sLoginName = "" ;
			string sCurrentPasswordInDB = "";
			string sCurrentPasswordFrmUser = "" ;
			try
			{
               //sLoginName = p_objDoc.SelectSingleNode("//LoginInfo//LoginName").InnerText;

               //added by Ashutosh Kashyap on Jan 21,2009
               try
               {
                   sLoginName = p_objDoc.SelectSingleNode("//LoginInfo//LoginName").InnerText;
               }
               catch
               {
                   sLoginName = p_objDoc.SelectSingleNode("//" + "control[@name='hdLoginName']").InnerText;
               }
                
               sCurrentPasswordFrmUser = p_objDoc.SelectSingleNode("//control[ @name = 'oldpasswd' ]").InnerText ;
               //MITS-10662 - Replaced the ' with '' for SQL query to run properly
               //objReader = DbFactory.GetDbReader( SecurityDatabase.GetSecurityDsn(0) ,"Select * from User_Details_Table where Login_Name = '"+ sLoginName +"'") ;
               objReader = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(base.ClientId), "Select * from User_Details_Table where Login_Name = '" + sLoginName.Replace("'", "''") + "'");
				if(objReader.Read())
				{
                  sCurrentPasswordInDB = PublicFunctions.Decrypt( objReader.GetString("PASSWORD") ) ;
                  objReader.Close();
				}
				else
				{
                    throw new RMAppException(String.Format(Globalization.GetString("UserLoginsAdaptor.VerifyExistingPassword.LoginNameError", base.ClientId), sLoginName));
				}
				if( sCurrentPasswordInDB == sCurrentPasswordFrmUser)
				{
					bRetVal = true ;
                    //Raman: MITS 12315
                    m_sCurrentPasswordInDB = sCurrentPasswordInDB;
				}
			}
			finally
			{
				if(objReader!=null)
				{
					objReader.Close() ;
					objReader = null ;
				}
			}
			return bRetVal ;
		}
		/// <summary>
		/// This function is used to update the password info. 
		/// </summary>
		/// <param name="p_objDocIn">Containing password update info.</param>
		/// <param name="p_objXmlOut">null</param>
		/// <param name="p_objErrOut"></param>
		/// <returns></returns>
		public bool UpdatePassword(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			UserLogins objUserLogin = null;					//object of UserLogins Class
			int iDataSourceId = 0 ;
			int iUserId = 0;
			string sNewPassword;
			DbReader objReader = null ;
            string sLoginName = string.Empty;
			SMSUser objSMSUser = null ;
			bool bisAdminUser = false;
			try
			{
               //added by Ashutosh Kashyap on Jan 21,2009
                try
                {
				    sLoginName = p_objDocIn.SelectSingleNode("//LoginInfo//LoginName").InnerText ;
                }
                catch
                {
                    sLoginName = p_objDocIn.SelectSingleNode("//" + "control[@name='hdLoginName']").InnerText;
                }
                
                //Raman Bhatia 10/15/2009 : MITS 17385
                //We really have no reason to deny an admin user the right to change his password from the change password screen.

                /*
                objSMSUser = new SMSUser( sLoginName ) ;
				bisAdminUser = objSMSUser.IsSMSAdminUser( false , sLoginName ) ;
				if(bisAdminUser)
				{
					throw new RMAppException(Globalization.GetString("UserLoginsAdaptor.UpdatePassword.AdminPasswordUpdateError"));
				}
                */
				if(!this.VerifyExistingPassword( ref p_objDocIn ))
				{
                    throw new RMAppException(String.Format(Globalization.GetString("UserLoginsAdaptor.UpdatePassword.PasswordMismatchError", base.ClientId), sLoginName));
				}
                /*Note: I am fully aware that i am  going in a opposite direction by fetching User_Id and DsnId from Login Name, but situation here mandates that,i.e.
				 * As open-Ldap forces login Name to be equivalent of User id and DsnId.
				 * -Tanuj
				 * */
                //MITS-10662 - Replaced the ' with '' for SQL query to run properly
                //objReader = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(0) , "SELECT * FROM USER_DETAILS_TABLE WHERE LOGIN_NAME = '"+ sLoginName +"'") ;
                objReader = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(base.ClientId), "SELECT * FROM USER_DETAILS_TABLE WHERE LOGIN_NAME = '" + sLoginName.Replace("'", "''") + "'");
				 
				if( objReader.Read() )
				{
                  iUserId = objReader.GetInt32("USER_ID");
                  iDataSourceId = objReader.GetInt32("DSNID") ;
				  objReader.Close();
				}

                sNewPassword = p_objDocIn.SelectSingleNode("//" + "control[@name='newpasswd']").InnerText;

                objUserLogin = new UserLogins(iUserId, iDataSourceId, base.ClientId);
				objUserLogin.Password = sNewPassword ;
				if(PublicFunctions.IsPwdPolEnabled())
				{
                    PublicFunctions.UpdatePasswordHistory(iUserId, iDataSourceId, sNewPassword, "", true, base.ClientId);
				}
				objUserLogin.ForceChangePwd = 0;
                objUserLogin.UpdateLoginPassword( ref objUserLogin) ;

				p_objXmlOut = p_objDocIn ;
                //Raman MITS 12315
                //Trying to update MCM password
                try
                {
                    if (PublicFunctions.IsMCMEnabled(objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId))
                    {
                        //rsolanki2 :  start updates for MCM mits 19200 : Adding the check below to change the password in MCM only when the 
                        // flag UseCommonAcrosoftUser is Diabled in web.config.
                        Boolean bSuccess = false;                          
                        if (!Conversion.CastToType<Boolean>(AcrosoftSection.UseCommonAcrosoftUser, out bSuccess))
                        {
                            string sSessionId = "";
                            string sAppExcpXml = "";
                            int iRetVal = 0;
                            Acrosoft objAcrosoft = new Acrosoft(base.ClientId);//dvatsa-cloud
                            iRetVal = objAcrosoft.AuthChangePassword(sLoginName, m_sCurrentPasswordInDB, sNewPassword, out sSessionId, out sAppExcpXml);
                            if (iRetVal != 0)
                            {
                                Log.Write("MCM password change failed with following exception message:" + sAppExcpXml,base.ClientId);//dvatsa-cloud
                            }
                        }
                        //rsolanki2 :  end updates 
                    }
                }
                catch (Exception e)
                {
                    Log.Write("MCM password change failed with following exception message:" + e.ToString(), base.ClientId);
                }
				return true;
			}
			catch(RMAppException p_objException)
			{
                p_objErrOut = new BusinessAdaptorErrors(base.ClientId);
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.SystemError);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut = new BusinessAdaptorErrors(base.ClientId);
                p_objErrOut.Add(p_objException, Globalization.GetString("UserLoginsAdaptor.UpdatePassword.error", base.ClientId), BusinessAdaptorErrorType.SystemError);
				return false;
			}
			finally
			{
				 objUserLogin = null;	
				if(objReader!=null)
				{
					objReader.Close() ;
					objReader = null ;
				}
				//objSMSUser= null;
			}

			
		}
		#endregion
	}

}
