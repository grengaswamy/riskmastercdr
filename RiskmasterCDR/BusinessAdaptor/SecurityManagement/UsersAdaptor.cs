﻿
using System;
using Riskmaster.ExceptionTypes;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.Application.SecurityManagement;
using System.Collections;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Threading;
using System.Collections.Generic; //pmittal5
using Riskmaster.Application.VSSInterface;

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: UserAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 21-Mar-2005
	///* $Author	: Raman Bhatia
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************

	/// <summary>
	/// This class is used to call the application layer component for Riskmaster.Application.SecurityManagement.Users class which 
	///	classifies User�s details on context of Riskmaster,user class contain all the information about the user like Name,City,Country Phone etc..	
	/// </summary>
	public class UsersAdaptor : BusinessAdaptorBase
	{
		#region Constructor
		/// Name		: UserAdaptor
		/// Author		: Raman Bhatia
		/// Date Created: 21-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		///	<summary>
		/// Constructor for the Class
		/// </summary>
		public UsersAdaptor()
		{
			
		}
		#endregion

		#region Public Methods
		/// Name		: Load
		/// Author		: Raman Bhatia
		/// Date Created: 21-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///	This method is a wrapper to both of the overloaded Users.Load methods which use DbFactory to establish connection 
		///	with the database and executes the query to retrieve the entire contents of the USER_TABLE 
		///	for the input userId or for all userId's to a DbReader object and invokes LoadData() passing the DbReader object
		///	as parameter.
		/// </summary>
		/// <param name="p_objDocIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document for the first overloaded method would be:
		///		<Document>
		///			<Users>
		///				<UserId>User Id</UserId>
		///			</Users>
		///		</Document> 
		///		
		///		The structure of input XML document for the second overloaded method would be:
		///		<Document>
		///			<Users>	</Users>
		///		</Document>
		/// 
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document
		///		The structure of output XML document for the first overloaded method would be:
		///		<Document>
		///			<Users>
		///				<UserId></UserId>
		///				<LastName></LastName>
		///				<FirstName></FirstName>
		///				<Address1></Address1>
		///				<Address2></Address2>
		///				<City></City>
		///				<State></State>
		///				<Country></Country>
		///				<ZipCode></ZipCode>
		///				<Email></Email>
		///				<ManagerId></ManagerId>
		///				<HomePhone></HomePhone>
		///				<OfficePhone></OfficePhone>
		///				<Title></Title>
		///				<NlsCode></NlsCode>
		///				<CompanyName></CompanyName>
		///			</Users>
		///		</Document>
		///		The output of second overloaded method would simply be a collection of user information for all the
		///		users. The format of the output xml document would remain as above.
		///	</param>
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		
		public bool Load(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			User objUser = null;					//object of Users Class
			XmlElement objTargetElement = null;		//used for manipulating the Xml
			XmlElement objParentElement = null;
			XmlElement objChildElement = null;
			int iUserId = 0;
			ArrayList arrlstUserCol=null;
			string sUserCol = "<UserId></UserId><LastName></LastName><FirstName></FirstName><Address1></Address1><Address2></Address2><City></City><State></State><Country></Country><ZipCode></ZipCode><Email></Email><ManagerId></ManagerId><HomePhone></HomePhone><OfficePhone></OfficePhone><Title></Title><NlsCode></NlsCode><CompanyName></CompanyName>";

			try
			{
				p_objDocIn=new XmlDocument();
				
				//choosing which overloaded function to call depending on input document
				//if UserId parameter has been passsed then we require information about a particular user else we 
				//return information about all users.
				if (p_objDocIn.SelectSingleNode("//" + "UserId")!=null)
				{
					objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//" + "UserId"); 
					iUserId = Conversion.ConvertStrToInteger(objTargetElement.InnerText);
                    objUser = base.userLogin.objUser;
				
					//Creating the output XML containing the User information
					objParentElement = p_objXmlOut.CreateElement("Document");
					p_objXmlOut.AppendChild(objParentElement);
					
					objChildElement = p_objXmlOut.CreateElement("Users");
					objParentElement.AppendChild(objChildElement);
					objParentElement = objChildElement;
								         		
					objChildElement = p_objXmlOut.CreateElement("UserId");
					objChildElement.InnerText= objUser.UserId.ToString();
					objParentElement.AppendChild(objChildElement);       
         						
					objChildElement = p_objXmlOut.CreateElement("LastName");
					objChildElement.InnerText= objUser.LastName.ToString();
					objParentElement.AppendChild(objChildElement);

					objChildElement = p_objXmlOut.CreateElement("FirstName");
					objChildElement.InnerText= objUser.FirstName.ToString();
					objParentElement.AppendChild(objChildElement);       
         		
					objChildElement = p_objXmlOut.CreateElement("Address1");
					objChildElement.InnerText= objUser.Address1.ToString();
					objParentElement.AppendChild(objChildElement);

					objChildElement = p_objXmlOut.CreateElement("Address2");
					objChildElement.InnerText= objUser.Address2.ToString();
					objParentElement.AppendChild(objChildElement);       
         		
					objChildElement = p_objXmlOut.CreateElement("City");
					objChildElement.InnerText= objUser.City.ToString();
					objParentElement.AppendChild(objChildElement);
				
					objChildElement = p_objXmlOut.CreateElement("State");
					objChildElement.InnerText= objUser.State.ToString();
					objParentElement.AppendChild(objChildElement);       
         		
					objChildElement = p_objXmlOut.CreateElement("Country");
					objChildElement.InnerText= objUser.Country.ToString();
					objParentElement.AppendChild(objChildElement);
				
					objChildElement = p_objXmlOut.CreateElement("ZipCode");
					objChildElement.InnerText= objUser.ZipCode.ToString();
					objParentElement.AppendChild(objChildElement);       
         		
					objChildElement = p_objXmlOut.CreateElement("Email");
					objChildElement.InnerText= objUser.Email.ToString();
					objParentElement.AppendChild(objChildElement);

					objChildElement = p_objXmlOut.CreateElement("ManagerId");
					objChildElement.InnerText= objUser.ManagerId.ToString();
					objParentElement.AppendChild(objChildElement);
				
					objChildElement = p_objXmlOut.CreateElement("HomePhone");
					objChildElement.InnerText= objUser.HomePhone.ToString();
					objParentElement.AppendChild(objChildElement);       
         		
					objChildElement = p_objXmlOut.CreateElement("OfficePhone");
					objChildElement.InnerText= objUser.OfficePhone.ToString();
					objParentElement.AppendChild(objChildElement);

					objChildElement = p_objXmlOut.CreateElement("Title");
					objChildElement.InnerText= objUser.Title.ToString();
					objParentElement.AppendChild(objChildElement);       
         		
					objChildElement = p_objXmlOut.CreateElement("NlsCode");
					objChildElement.InnerText= objUser.NlsCode.ToString();
					objParentElement.AppendChild(objChildElement);

					objChildElement = p_objXmlOut.CreateElement("CompanyName");
					objChildElement.InnerText= objUser.CompanyName.ToString();
					objParentElement.AppendChild(objChildElement);

					return true;
				}
				arrlstUserCol=new ArrayList();
				Users objUsers = new Users(base.ClientId);
			
				objUsers.Load(ref arrlstUserCol);

				objParentElement = p_objXmlOut.CreateElement("Document");
				foreach(Users objUserCol in arrlstUserCol)
				{
					objChildElement=p_objXmlOut.CreateElement("Users");
					objChildElement.InnerXml=sUserCol;
					objChildElement.SelectSingleNode("//"+ "UserId").InnerText=Conversion.ConvertObjToStr(objUserCol.UserId);
					objChildElement.SelectSingleNode("//"+ "FirstName").InnerText=Conversion.ConvertObjToStr(objUserCol.FirstName);
					objChildElement.SelectSingleNode("//"+ "LastName").InnerText=Conversion.ConvertObjToStr(objUserCol.LastName);
					objChildElement.SelectSingleNode("//"+ "Address1").InnerText=Conversion.ConvertObjToStr(objUserCol.Address1);
					objChildElement.SelectSingleNode("//"+ "Address2").InnerText=Conversion.ConvertObjToStr(objUserCol.Address2);
					objChildElement.SelectSingleNode("//"+ "City").InnerText=Conversion.ConvertObjToStr(objUserCol.City);
					objChildElement.SelectSingleNode("//"+ "State").InnerText=Conversion.ConvertObjToStr(objUserCol.State);
					objChildElement.SelectSingleNode("//"+ "Country").InnerText=Conversion.ConvertObjToStr(objUserCol.Country);
					objChildElement.SelectSingleNode("//"+ "ZipCode").InnerText=Conversion.ConvertObjToStr(objUserCol.ZipCode);
					objChildElement.SelectSingleNode("//"+ "Email").InnerText=Conversion.ConvertObjToStr(objUserCol.Email);
					objChildElement.SelectSingleNode("//"+ "ManagerId").InnerText=Conversion.ConvertObjToStr(objUserCol.ManagerId);
					objChildElement.SelectSingleNode("//"+ "HomePhone").InnerText=Conversion.ConvertObjToStr(objUserCol.HomePhone);
					objChildElement.SelectSingleNode("//"+ "OfficePhone").InnerText=Conversion.ConvertObjToStr(objUserCol.OfficePhone);
					objChildElement.SelectSingleNode("//"+ "Title").InnerText=Conversion.ConvertObjToStr(objUserCol.Title);
					objChildElement.SelectSingleNode("//"+ "NlsCode").InnerText=Conversion.ConvertObjToStr(objUserCol.NlsCode);
					objChildElement.SelectSingleNode("//"+ "CompanyName").InnerText=Conversion.ConvertObjToStr(objUserCol.CompanyName);
					objParentElement.AppendChild(objChildElement);
					p_objXmlOut.AppendChild(objParentElement);
                }
				return true;

			}
			catch(RMAppException p_objException)
			{
				p_objErrOut = new BusinessAdaptorErrors(base.ClientId);
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut = new BusinessAdaptorErrors(base.ClientId);
                p_objErrOut.Add(p_objException, Globalization.GetString("UsersAdaptor.Load.error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objUser = null;
				objTargetElement = null;
				objParentElement = null;
				objChildElement = null;
			}
			
		}
		
		/// Name		: Save
		/// Author		: Raman Bhatia
		/// Date Created: 21-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///		This method is a wrapper to Users.Save method which in turn is a wrapper over SaveData() funuction which actually saves the object state to database.
		///			
		/// </summary>
		/// <param name="p_objDocIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<Users>
		///				<UserId></UserId>
		///				<LastName></LastName>
		///				<FirstName></FirstName>
		///				<Address1></Address1>
		///				<Address2></Address2>
		///				<City></City>
		///				<State></State>
		///				<Country></Country>
		///				<ZipCode></ZipCode>
		///				<Email></Email>
		///				<ManagerId></ManagerId>
		///				<HomePhone></HomePhone>
		///				<OfficePhone></OfficePhone>
		///				<Title></Title>
		///				<NlsCode></NlsCode>
		///				<CompanyName></CompanyName>
		///			</Users>
		///		</Document>
		 
		/// </param>
		/// <param name="p_objXmlOut">As the function is a wrapper to a save function 
		/// hence it would not return a XML document.
		/// </param>
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		
		public bool Save(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			User objUser = null;					//object of Users Class
			XmlElement objTargetElement = null;		//used for manipulating the Xml
			int iUserId = 0;
			string sLastName = "";
			string sFirstName = "";
			string sAddress1 = "";
			string sAddress2 = "";
			string sCity = "";
			string sState = "";
			string sCountry = "";
			string sZipCode = "";
			string sEmail = "";
			int iManagerId = 0;
			string sHomePhone = "";
			string sOfficePhone = "";
			string sTitle = "";
			int iNlsCode = 0;
			string sCompanyName = "";
            //Arnab: MITS-10662 - Declared bisAdminUser for SMS Access
            bool bisAdminUser = false;
            UserLogins objUserLogins = null;
            int iDSNID = 0;
            //MITS-10662 - End
            string sUserName = "";      //gagnihotri MITS 11995 Changes made for Audit table
            DbReader objReader = null; //pmittal5
            DbConnection objConn = null; //pmittal5
            int iPrevManagerId = 0; //pmittal5
            DbCommand objCommand = null; //pmittal5
            DbConnection objSecurityConnection = null; //pmittal5
            DbConnection objDBConnection = null;  //pmittal5
            DbCommand objDBCommand = null; //pmital5
			try
			{
				//Arnab: MITS-10662 - Added for SMS Access check box status to the database
				if (p_objDocIn.SelectSingleNode("//control[@name = 'hdIsSMSAdminUser']").InnerText == "True")
				{
					bisAdminUser = true;
				}
				//extracting the user information from input XML
				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='userid']");
				iUserId = Conversion.ConvertStrToInteger(objTargetElement.InnerText);

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='lastname']");
				sLastName = objTargetElement.InnerText;

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='firstname']");
				sFirstName = objTargetElement.InnerText;

				string sLoginName = (sFirstName + sLastName);
				if (p_objDocIn.SelectSingleNode("//control[@name='loginname']") != null)
				{
					if (sLoginName.Length >= 8)
						p_objDocIn.SelectSingleNode("//control[@name='loginname']").InnerText = sLoginName.Substring(0, 8);
					else
					{
						p_objDocIn.SelectSingleNode("//control[@name='loginname']").InnerText = sLoginName.Substring(0, sLoginName.Length);
					}
				}
				//rsolanki2 : domain authentication updates                
				if (!Riskmaster.Application.SecurityManagement.Security.bIsDomainAuthenticationEnabled())
				{
					if (p_objDocIn.SelectSingleNode("//control[@name='password']") != null)
					{
						if (PublicFunctions.IsPwdPolEnabled())
						{
							p_objDocIn.SelectSingleNode("//control[@name='password']").InnerText = p_objDocIn.SelectSingleNode("//control[@name='hdPwdFrmUserScreen']").InnerText;
						}
						else
						{
							p_objDocIn.SelectSingleNode("//control[@name='password']").InnerText = p_objDocIn.SelectSingleNode("//control[@name='loginname']").InnerText;
						}
					}
				}

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='address1']");
				sAddress1 = objTargetElement.InnerText;

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='address2']");
				sAddress2 = objTargetElement.InnerText;

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='city']");
				sCity = objTargetElement.InnerText;

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='state']");
				sState = objTargetElement.InnerText;

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='country']");
				sCountry = objTargetElement.InnerText;

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='zip']");
				sZipCode = objTargetElement.InnerText;

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='email']");
				sEmail = objTargetElement.InnerText;

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='manager']");
				iManagerId = Conversion.ConvertStrToInteger(objTargetElement.Attributes["value"].Value);

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='homephone']");
				sHomePhone = objTargetElement.InnerText;

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='officephone']");
				sOfficePhone = objTargetElement.InnerText;

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='title']");
				sTitle = objTargetElement.InnerText;

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='language']");
				iNlsCode = Conversion.ConvertStrToInteger(objTargetElement.Attributes["value"].Value);

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='company']");
				sCompanyName = objTargetElement.InnerText;

				//gagnihotri MITS 11995 Changes made for Audit table Start
				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='hdUserLoginName']");
				if (objTargetElement != null)
					sUserName = objTargetElement.InnerText;
				//Raman Bhatia: 05/05/2010 : sms direct access implementation
				//TO DO: we need to eliminate the need for accessing userlogin object for normal SMS operations too
				if (base.userLogin != null)
				{
					objUser = base.userLogin.objUser;
				}
				else
				{
					objUser = new Users(sUserName, base.ClientId);
				}
				//objUsers = new Users();
				//gagnihotri MITS 11995 Changes made for Audit table End

				//setting the properties of Users class to set User data.

				objUser.UserId = iUserId;
				objUser.LastName = sLastName;
				objUser.FirstName = sFirstName;
				objUser.Address1 = sAddress1;
				objUser.Address2 = sAddress2;
				objUser.City = sCity;
				objUser.State = sState;
				objUser.Country = sCountry;
				objUser.ZipCode = sZipCode;
				objUser.Email = sEmail;


				if (objUser.UserId != 0)
				{
					if (objUser.UserId != iManagerId)
					{
						objUser.ManagerId = iManagerId;
					}
				}
				else
				{
					objUser.ManagerId = iManagerId;
				}
				//Need to remove
				if (sHomePhone.Trim() == "")
				{
					sHomePhone = "(   )    -";
				}
				if (sOfficePhone.Trim() == "")
				{
					sOfficePhone = "(   )    -";
				}
				objUser.HomePhone = sHomePhone;
				objUser.OfficePhone = sOfficePhone;
				objUser.Title = sTitle;
				if (iNlsCode == 0)
					iNlsCode = 1033;
				objUser.NlsCode = iNlsCode;
				objUser.CompanyName = sCompanyName;

				objConn = DbFactory.GetDbConnection(SecurityDatabase.GetSecurityDsn(base.ClientId));
				objConn.Open();
				objCommand = objConn.CreateCommand();
				if (objUser.UserId != 0)
				{
					objCommand.CommandText = "SELECT MANAGER_ID FROM USER_TABLE WHERE USER_ID = " + objUser.UserId;
                    iPrevManagerId = Conversion.ConvertObjToInt(objCommand.ExecuteScalar().ToString(), base.ClientId);
				}

				//If Manager is changed, set Updated_Flag to -1 for BES groups associated with Previous as well as new manager, 
				//because Views definitions need to be changed for both the managers.
				int iDSNId = 0;
				int iCurrentManagerId = 0;
				List<int> arrManagerIds = new List<int>();
				if (iPrevManagerId != iManagerId)
				{
					for (int i = 0; i < 2; i++)
					{
						if (i == 0)
							iCurrentManagerId = iManagerId;
						else
							iCurrentManagerId = iPrevManagerId;
						//In case previous manager also falls in same supervisory chain as that of new manager, below processing should be done only once for that manager
						while (iCurrentManagerId != 0 && !arrManagerIds.Contains(iCurrentManagerId))
						{
							//if(i==0)
							arrManagerIds.Add(iCurrentManagerId);
							using (objReader = objConn.ExecuteReader("SELECT GROUP_ID,DSN_ID FROM USER_ORGSEC WHERE USER_ID = " + iCurrentManagerId))
							{
								if (objReader != null)
								{
									objSecurityConnection = DbFactory.GetDbConnection(SecurityDatabase.GetSecurityDsn(base.ClientId));
									objSecurityConnection.Open();
									objCommand = objSecurityConnection.CreateCommand();
									while (objReader.Read())
									{
										iDSNId = objReader.GetInt32(1);
										objCommand.CommandText = "SELECT CONF_FLAG FROM DATA_SOURCE_TABLE WHERE DSNID = " + iDSNId;
										if (objCommand.ExecuteScalar() != null && objCommand.ExecuteScalar().ToString() == "-1")
										{
                                            objUserLogins = new UserLogins(iCurrentManagerId, iDSNId, base.ClientId);
                                            //srajindersin 5/24/2014 MITS 36472
                                            if (!string.IsNullOrEmpty(objUserLogins.objRiskmasterDatabase.ConnectionString))
                                            {
                                                objDBConnection = DbFactory.GetDbConnection(objUserLogins.objRiskmasterDatabase.ConnectionString);
                                                objDBConnection.Open();
                                                objDBCommand = objDBConnection.CreateCommand();
                                                objDBCommand.CommandText = "UPDATE GROUP_MAP SET UPDATED_FLAG = -1 WHERE GROUP_ID = " + objReader.GetInt32("GROUP_ID") + " AND SUPER_GROUP_ID IN (SELECT GROUP_ID FROM ORG_SECURITY WHERE GROUP_ENTITIES <> '<ALL>')";
                                                objDBCommand.ExecuteNonQuery();
                                                objDBConnection.Close();
                                            }
										}
									}
									objSecurityConnection.Close();
								}
							}
							objCommand = objConn.CreateCommand();
							objCommand.CommandText = "SELECT MANAGER_ID FROM USER_TABLE WHERE USER_ID = " + iCurrentManagerId;
                            iCurrentManagerId = Conversion.ConvertObjToInt(objCommand.ExecuteScalar().ToString(), base.ClientId);
						}
					}
				}
				//Calling the Save function to write the User Data
				// start: atavaragiri MITS 28449:For insertion,updation and deletion of the USER_TABLE//
				if (p_objDocIn.SelectSingleNode("//control[@name = 'SMSAccess']").InnerText.Trim().ToLower() == "true") 
					objUser.SMSAccess = true;
				else
					objUser.SMSAccess = false;

				if (p_objDocIn.SelectSingleNode("//control[@name = 'UserPrAccess']").InnerText.Trim().ToLower() == "true")
					objUser.UPSAccess = true;

				else
					objUser.UPSAccess = false;
				
				// end:atavaragiri

				objUser.Save();
				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='dsnid']");
				int iDsnId = Conversion.ConvertStrToInteger(objTargetElement.InnerText);
				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='hdPostback']");
				objTargetElement.InnerText = "true";
				//update the instance now...
				p_objDocIn.SelectSingleNode("//control[@name='userid']").InnerText = objUser.UserId.ToString();
				p_objDocIn.SelectSingleNode("//control[@name='dbid']").InnerText = objUser.UserId.ToString();

				if (iDsnId != 0)
				{
					//call the user login object to save the object..
					bool bCallUpdateHistory = false;

					if (p_objDocIn.SelectSingleNode("//control[@name='hdPwdFrmUserScreen']").InnerText.Trim() != "")
					{
						bCallUpdateHistory = true;
					}
					this.AllowUserToDsn(objUser.UserId, iDsnId, ref p_objDocIn, bCallUpdateHistory, ref p_objErrOut);

					//Geeta 07-Oct-06 :Created for fixing MITS no 7757
					Users.SaveUserLayout(objUser.UserId, iDsnId, base.ClientId);
				}

				//Arnab: MITS-10662 - This section is for SMS access status to be saved
				//Creating UserLogins object for the user data (login name/password) 
				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='lstShowDatasources']");
				//If there are DSN selected for the user
				if (objTargetElement.ChildNodes.Count != 0)
				{
					iDSNID = Conversion.ConvertStrToInteger(objTargetElement.ChildNodes[0].Attributes[0].Value);
                    objUserLogins = new UserLogins(objUser.UserId, iDSNID, base.ClientId);
					//If user was not previously permitted to access any DSN
					if (objUserLogins.LoginName == "")
					{
						objUserLogins = new UserLogins(false, base.ClientId);
						objUserLogins.UserId = objUser.UserId;
						if (p_objDocIn.SelectSingleNode("//control[@name='loginname']") != null)
						{
							objUserLogins.LoginName = p_objDocIn.SelectSingleNode("//control[@name='loginname']").InnerText;
						}
						if (p_objDocIn.SelectSingleNode("//control[@name='password']") != null)
						{
							objUserLogins.Password = p_objDocIn.SelectSingleNode("//control[@name='password']").InnerText;
						}
                        // Ash - cloud , VSS interface is not moving to cloud
                        if (base.ClientId == 0)
                        {
                            //Start - Averma62 - Check if dsn id exist export user data to vss
                            if (p_objDocIn.SelectSingleNode("//control[@name='dsnid']") != null)
                            {
                                if (!p_objDocIn.SelectSingleNode("//control[@name='dsnid']").InnerText.Equals("0") && !string.IsNullOrEmpty(p_objDocIn.SelectSingleNode("//control[@name='dsnid']").InnerText))
                                {
                                    UserLogins objuserlog = new UserLogins(objUser.UserId, Conversion.ConvertObjToInt(p_objDocIn.SelectSingleNode("//control[@name='dsnid']").InnerText, base.ClientId), base.ClientId);
                                    UserExportToVss(objuserlog);
                                }
                            }
                            //End - Averma62 - Check if dsn id exist export user data to vss
                        }// Ash - cloud
					}
				}
				else
				{
					//If the user is not permitted to acces any DSN
                    objUserLogins = new UserLogins(false, base.ClientId);
					objUserLogins.UserId = objUser.UserId;
					if (p_objDocIn.SelectSingleNode("//control[@name='loginname']") != null)
					{
						objUserLogins.LoginName = p_objDocIn.SelectSingleNode("//control[@name='loginname']").InnerText;
					}
					if (p_objDocIn.SelectSingleNode("//control[@name='password']") != null)
					{
						objUserLogins.Password = p_objDocIn.SelectSingleNode("//control[@name='password']").InnerText;
					}
                     // Ash - cloud , VSS interface is not moving to cloud
                    if (base.ClientId == 0)
                    {
                        //Start - Averma62 - Check if dsn id exist export user data to vss
                        if (p_objDocIn.SelectSingleNode("//control[@name='dsnid']") != null)
                        {

                            if (!p_objDocIn.SelectSingleNode("//control[@name='dsnid']").InnerText.Equals("0") && !string.IsNullOrEmpty(p_objDocIn.SelectSingleNode("//control[@name='dsnid']").InnerText))
                            {
                                UserLogins objuserlog = new UserLogins(objUser.UserId, Conversion.ConvertObjToInt(p_objDocIn.SelectSingleNode("//control[@name='dsnid']").InnerText, base.ClientId), base.ClientId);
                                UserExportToVss(objuserlog);
                            }
                        }
                        //End - Averma62 - Check if dsn id exist export user data to vss
                    }
				}
                objConn.Close();

				//Saving data for user having access to sms into SMS_USERS
				SMSUser objSMSUser = null;
				if (p_objDocIn.SelectSingleNode("//control[@name = 'SMSAccess']").InnerText.Trim().ToLower() == "true")
				{
				    try
				    {                        
				        objSMSUser = new SMSUser(objUserLogins.UserId, base.ClientId);
				        objSMSUser.RmPwd = objUserLogins.Password;
				        bisAdminUser = objSMSUser.IsSMSAdminUser(bisAdminUser, objUserLogins.LoginName);
				        objSMSUser.IsAdminUser = bisAdminUser;
				        if (objSMSUser.RecordId == 0)
				        {
                            objSMSUser = new SMSUser(objUser.UserId, base.ClientId);                         
				            objSMSUser.IsAdminUser = bisAdminUser;
				            objSMSUser.RmLoginName = objUserLogins.LoginName;                            
				            objSMSUser.RmPwd = objUserLogins.Password;
				            if (bisAdminUser)                            
				                objSMSUser.UserId = (-1) * objUser.UserId;                            
				            else
				                objSMSUser.UserId = objUser.UserId;                            
				        }
				        else
				        {
				            if (objSMSUser.UserId.ToString().IndexOf("-") != -1)
				            {                                
				                objSMSUser.UserId = (-1) * objUser.UserId;

								//atavaragiri MITS 28449 //
				                //objSMSUser.DeleteSMSUser(objUser.UserId);
				            }
				        }
						// atavaragiri MITS 28449 //
				        //  objSMSUser.Save();
				    }
				    finally
				    {
				        objSMSUser = null;
				    }
				}
				else
				{
				    try
				    {
                        objSMSUser = new SMSUser(objUser.UserId, base.ClientId);
						// atavaragiri MITS 28449 //
				       //  objSMSUser.Remove();
				    }
				    finally
				    {
				        objSMSUser = null;
				    }
				}
				//MITS-10662 - save end 
				//skhare7 MITS:22374
				    if (p_objDocIn.SelectSingleNode("//control[@name = 'UserPrAccess']").InnerText.Trim().ToLower() == "true")
				    {
				        try
				        {
                            objSMSUser = new SMSUser(objUserLogins.UserId, base.ClientId);
							// atavaragiri MITS 28449 //
				           // objSMSUser.UserPrivSave();
				        }
				        finally
				        {
				            objSMSUser = null;
				        }
				    }
				    else
				    {
				        try
				        {
                            objSMSUser = new SMSUser(objUser.UserId, base.ClientId);
							// atavaragiri MITS 28449 //
				            // objSMSUser.UserPrivRemove();
				        }
				        finally
				        {
				            objSMSUser = null;
				        }
				    }
				    // End skhare7 MITS:22374
				    p_objXmlOut=p_objDocIn;
				   return true;


				
			}
			catch (RMAppException p_objException)
			{
                p_objErrOut = new BusinessAdaptorErrors(base.ClientId);
				p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
				return false;
			}
			catch (Exception p_objException)
			{
                p_objErrOut = new BusinessAdaptorErrors(base.ClientId);
                p_objErrOut.Add(p_objException, Globalization.GetString("UsersAdaptor.Save.error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
			    objUser = null;
			    objTargetElement = null;
			    if (objConn != null)
			    {
			        objConn.Close();
			        objConn.Dispose();
			    }
			    if (objSecurityConnection != null)
			    {
			        objSecurityConnection.Close();
			        objSecurityConnection.Dispose();
			    }
			    if (objDBConnection != null)
			    {
			        objDBConnection.Close();
			        objDBConnection.Dispose();
			    }
			}
			
		}
		
        //Start - Export user id and password to VSS
        public void UserExportToVss(UserLogins p_objUserLogins)
        { 
            DbConnection objConn = null;
            DbCommand objCommand = null;
            objConn = DbFactory.GetDbConnection(p_objUserLogins.objRiskmasterDatabase.ConnectionString);
            objConn.Open();
            objCommand = objConn.CreateCommand();
            int iVssFlag = 0;
            try
            {
                objCommand.CommandText = "SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='ENABLE_VSS'";
                iVssFlag = Conversion.ConvertObjToInt(objCommand.ExecuteScalar().ToString(), base.ClientId);

                if (iVssFlag.Equals(-1))
                {
                    VssExportAsynCall objVss = new VssExportAsynCall(p_objUserLogins.objRiskmasterDatabase.DataSourceName, p_objUserLogins.LoginName, p_objUserLogins.Password, base.ClientId);
                    objVss.AsynchVssReserveExport(0, 0, 0, 0, 0, p_objUserLogins.LoginName, p_objUserLogins.Password, p_objUserLogins.objUser.Email, p_objUserLogins.objUser.Dsn, "PassUpdate");
                    objVss = null;
                }
            }
            catch (Exception p_objException)
            {
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }
            }
        }
        //End - Export user id and password to VSS
		/// <summary>
		/// This function adds a particular use to a Dsn.
		/// </summary>
		/// <param name="p_iUserId">User id to be added</param>
		/// <param name="p_iDsnId">Dsn id, to which user id to be allowed</param>
		/// <param name="p_objDocUser">Contains the details for new user.</param>
		public  void AllowUserToDsn(int p_iUserId, int p_iDsnId,ref XmlDocument p_objDocUser, bool p_bCallUpdatePwdHistory, ref BusinessAdaptorErrors p_objErr)
		{
			DbConnection objConn = null; 
			DbReader objReader=null;
			XmlDocument objTemp=null;
			//BusinessAdaptorErrors objErrs=null;
			bool bPwdUpdateCalled =  false ;
			try
			{
				if(PublicFunctions.IsPwdPolEnabled())
				{
					if (p_objDocUser.SelectSingleNode("//control[@name='hdPwdFrmUserScreen']").InnerText!="" )
					{
                        PublicFunctions.UpdatePasswordHistory(p_iUserId, p_iDsnId, p_objDocUser.SelectSingleNode("//control[@name='hdPwdFrmUserScreen']").InnerText, "", false, base.ClientId);
						bPwdUpdateCalled = true ;
					}
				}
				string sQuery= "Select * From USER_DETAILS_TABLE " + 
					           "Where DSNID= " +  p_iDsnId + " AND USER_ID = " + p_iUserId;
				objConn = DbFactory.GetDbConnection(SecurityDatabase.GetSecurityDsn(base.ClientId));
				objConn.Open();
				if(!objConn.ExecuteReader(sQuery).Read())
				{
                    UserLoginsAdaptor objAdapUserLogin = new UserLoginsAdaptor(base.ClientId); 
					objAdapUserLogin.UpdadteHistoryCalled = bPwdUpdateCalled ;
					objAdapUserLogin.Save(p_objDocUser,ref objTemp,ref p_objErr);

				}
				
			}
			//catch(Exception p_objException)
			//{
			//	throw p_objException;
			//}
			finally
			{
				objTemp=null;
				//objErrs=null;
				if(objReader!=null)
				{
					//objReader.Close();
					objReader.Dispose();
				}
				if(objConn!=null)
				{
					//if(objConn.State==System.Data.ConnectionState.Open)
					//{
					//	objConn.Close();
						objConn.Dispose();
					//}
				}
			}
		}
		
		/// Name		: Remove
		/// Author		: Raman Bhatia
		/// Date Created: 22-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///		This method is a wrapper to Users.Remove method which removes the user from database.
		///			
		/// </summary>
		/// <param name="p_objDocIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<Users>
		///				<UserId></UserId>
		///			</Users>
		///		</Document>
		 
		/// </param>
		/// <param name="p_objXmlOut">As the function is a wrapper to a remove function 
		/// hence it would not return a XML document.
		/// </param>
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		
		public bool Remove(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			Users objUsers = null;					//object of Users Class
			XmlElement objTargetElement = null;		//used for manipulating the Xml
			int iUserId = 0;
			
			try
			{
				//extracting the user information from input XML
				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//" + "UserId"); 
				iUserId = Conversion.ConvertStrToInteger(objTargetElement.InnerText);

				objUsers = new Users(base.ClientId);
				
				//Calling the Remove function to delete the user data
				objUsers.Remove(iUserId);
				return true;

			}
			catch(RMAppException p_objException)
			{
                p_objErrOut = new BusinessAdaptorErrors(base.ClientId);
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut = new BusinessAdaptorErrors(base.ClientId);
                p_objErrOut.Add(p_objException, Globalization.GetString("UsersAdaptor.Remove.error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objUsers = null;
				objTargetElement = null;
			}
		}

		#endregion
	}

}
