using System;
using System.Xml ;
using Riskmaster.Common ;
using Riskmaster.Security ;
using Riskmaster.ExceptionTypes ;
using Riskmaster.BusinessAdaptor.Common ;
using Riskmaster.Application.ScriptEditor ;

namespace Riskmaster.BusinessAdaptor
{
	/**************************************************************
	 * $File		: ScriptEditorAdaptor.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 12/20/2005
	 * $Author		: Vaibhav Kaushik
	 * $Comment		:  
	 * $Source		:  	
	**************************************************************/
	public class ScriptEditorAdaptor : BusinessAdaptorBase
	{
		public ScriptEditorAdaptor() : base()
		{			
		}


		public bool LoadTreeXml( XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			ScriptEditorManager objScriptEditorManager = null ;
			
			try
			{
                objScriptEditorManager = new ScriptEditorManager(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, base.ClientId);//psharma206 jira 106

				p_objXmlOut = objScriptEditorManager.LoadTreeXml( p_objXmlIn );

				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add( p_objException , BusinessAdaptorErrorType.Error ); 
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("ScriptEditorAdaptor.LoadTreeXml.Error", base.ClientId), BusinessAdaptorErrorType.Error);//psharma206 jira 106
				return false;
			}
			finally
			{
				if( objScriptEditorManager != null )
				{
					objScriptEditorManager.Dispose();
					objScriptEditorManager = null ;					
				}				
			}
		}


		public bool LoadScript( XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			ScriptEditorManager objScriptEditorManager = null ;
			XmlElement objTargetElement = null ;
			int iRowId = 0 ;
			try
			{
				
				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//RowId" );
				if( objTargetElement == null )
				{
                    p_objErrOut.Add("RowIdMissing", Globalization.GetString("ScriptEditorAdaptor.RowIdMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				iRowId = Conversion.ConvertStrToInteger( objTargetElement.InnerText );
                objScriptEditorManager = new ScriptEditorManager(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, base.ClientId);//psharma206 jira 106

				p_objXmlOut = objScriptEditorManager.LoadScript( iRowId );

				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add( p_objException , BusinessAdaptorErrorType.Error );
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("ScriptEditorAdaptor.LoadScript.Error", base.ClientId), BusinessAdaptorErrorType.Error);//psharma206 jira 106
				return false;
			}
			finally
			{
				if( objScriptEditorManager != null )
				{
					objScriptEditorManager.Dispose();
					objScriptEditorManager = null ;					
				}			
			}
		}

		
		public bool NewScript( XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			ScriptEditorManager objScriptEditorManager = null ;
			XmlElement objTargetElement = null ;
			int iScriptType = 0 ;
			string sObjectName = string.Empty ;
			try
			{
				
				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//ScriptType" );
				if( objTargetElement == null )
				{
                    p_objErrOut.Add("ScriptTypeMissing", Globalization.GetString("ScriptEditorAdaptor.ScriptTypeMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				iScriptType = Conversion.ConvertStrToInteger( objTargetElement.InnerText );

				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//ObjectName" );
				if( objTargetElement == null )
				{
                    p_objErrOut.Add("ObjectNameMissing", Globalization.GetString("ScriptEditorAdaptor.ObjectNameMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				sObjectName = objTargetElement.InnerText ;

				objScriptEditorManager = new ScriptEditorManager( userLogin.objRiskmasterDatabase.DataSourceName , userLogin.LoginName ,userLogin.Password,base.ClientId );//psharma206 jira 106

				p_objXmlOut = objScriptEditorManager.NewScript( iScriptType , sObjectName );

				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add( p_objException , BusinessAdaptorErrorType.Error );
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("ScriptEditorAdaptor.NewScript.Error", base.ClientId), BusinessAdaptorErrorType.Error);//psharma206 jira 106
				return false;
			}
			finally
			{
				if( objScriptEditorManager != null )
				{
					objScriptEditorManager.Dispose();
					objScriptEditorManager = null ;					
				}				
			}
		}

		
		public bool SaveScript( XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
            ScriptEditorManager objScriptEditorManager = null;
			
			try
			{
                objScriptEditorManager = new ScriptEditorManager(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, base.ClientId);//psharma206 jira 106

				p_objXmlOut = objScriptEditorManager.SaveScript( p_objXmlIn );

				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add( p_objException , BusinessAdaptorErrorType.Error );
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("ScriptEditorAdaptor.SaveScript.Error", base.ClientId), BusinessAdaptorErrorType.Error);//psharma206 jira 106
				return false;
			}
			finally
			{
				if( objScriptEditorManager != null )
				{
					objScriptEditorManager.Dispose();
					objScriptEditorManager = null ;					
				}				
			}
		}

	
		public bool DeleteScript( XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			ScriptEditorManager objScriptEditorManager = null ;
			XmlElement objTargetElement = null ;
			int iRowId = 0 ;
			try
			{
				p_objXmlOut = new XmlDocument();
				objTargetElement = p_objXmlOut.CreateElement( "Root" );
				p_objXmlOut.AppendChild( objTargetElement );

				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//RowId" );
				if( objTargetElement == null )
				{
                    p_objErrOut.Add("RowIdMissing", Globalization.GetString("ScriptEditorAdaptor.RowIdMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				iRowId = Conversion.ConvertStrToInteger( objTargetElement.InnerText );
                objScriptEditorManager = new ScriptEditorManager(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, base.ClientId);//psharma206 jira 106

				objScriptEditorManager.DeleteScript( iRowId );

				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add( p_objException , BusinessAdaptorErrorType.Error );
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("ScriptEditorAdaptor.DeleteScript.Error", base.ClientId), BusinessAdaptorErrorType.Error);//psharma206 jira 106
				return false;
			}
			finally
			{
				if( objScriptEditorManager != null )
				{
					objScriptEditorManager.Dispose();
					objScriptEditorManager = null ;					
				}				
			}
		}

			
		public bool LoadDataModelObjects( XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			ScriptEditorManager objScriptEditorManager = null ;
		
			try
			{
                objScriptEditorManager = new ScriptEditorManager(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, base.ClientId);//psharma206 jira 106

				p_objXmlOut = objScriptEditorManager.LoadDataModelObjects();

				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add( p_objException , BusinessAdaptorErrorType.Error );
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("ScriptEditorAdaptor.LoadDataModelObjects.Error", base.ClientId), BusinessAdaptorErrorType.Error);//psharma206 jira 106
				return false;
			}
			finally
			{
				if( objScriptEditorManager != null )
				{
					objScriptEditorManager.Dispose();
					objScriptEditorManager = null ;					
				}							
			}
		}

	

	}
}
