﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;
using Riskmaster.BusinessAdaptor.Common;
using System.Configuration;
using Riskmaster.Application.LSSInterface;

namespace Riskmaster.BusinessAdaptor
{
    /// <summary>
    ///Author  :   Shameem Arshad
    ///Dated   :   11/24/2008
    /// Class containing the adaptor methods to access Application layer methods for 
    /// MCIC RMX-LSS Single Signon.
    /// </summary>
    class SingleSignonAdaptor : BusinessAdaptorBase
    {
        //#region Constructor
        public SingleSignonAdaptor()
        { }
        //#endregion
       
        #region Public Methods

        public bool SingleSignonXML(XmlDocument p_objXMLIn, ref XmlDocument p_objXMLOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            SingleSignon objSingleSignon = null;
            string sLSS_ws_username = string.Empty;
            string sLSS_ws_password = string.Empty;

            XmlDocument XmlLSSConfigDoc = new XmlDocument();
            try
            {
                objSingleSignon = new SingleSignon(userLogin.LoginName, userLogin.Password,base.securityConnectionString,base.ClientId );
                p_objXMLOut = objSingleSignon.SingleSignonXML(p_objXMLIn);

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("SingleSignonAdaptor.SingleSignonXML.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objSingleSignon = null;
            }
            return true;
        }

        #endregion
    }
}
