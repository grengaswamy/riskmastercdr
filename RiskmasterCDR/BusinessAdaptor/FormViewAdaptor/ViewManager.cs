using System;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.Settings;
using System.Text;
using System.Collections.Generic;
using System.Data;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for ViewManager.
	/// </summary>
	public class FormViewManager
	{
		const int RMO_GC = 150;
		const int RMO_VA = 6000;
		const int RMO_DI = 60000;
		const int RMO_WC = 3000;
		const int RMO_RM = 1;
		const int RMO_NONE = 0;

		internal BusinessAdaptorErrors m_err = null;
		private UserLogin m_userLogin=null;
        private string m_strUserDbConnStr = string.Empty;
        private string m_strViewDbConnStr = string.Empty;
        private int m_iClientId = 0;
        /// <summary>
        /// Overloaded class constructor
        /// </summary>
        /// <param name="objUser"></param>
        /// <param name="errors"></param>
        /// <param name="iClientId"></param>
        public FormViewManager(UserLogin objUser, BusinessAdaptorErrors errors,int p_iClientId)
        {
            m_err = errors;
            m_userLogin = objUser;
            m_strUserDbConnStr = m_userLogin.objRiskmasterDatabase.ConnectionString;
            m_iClientId = p_iClientId;
            m_strViewDbConnStr = RMConfigurationManager.GetConnectionString("ViewDataSource",m_iClientId);
        }

		public string FetchView(int prevSid, int viewId,int recordId,string sysFormName)
		{
            string SQL = string.Empty;
            string sViewXml = string.Empty;
			string sFormName = FormNameToSpecialized(sysFormName,prevSid);
            string DSNcond = string.Empty;
			if(m_err!=null)
				m_err.Clear();

            try
            {

                // Already tacked on LOB from Security Id if available using FormNameToSpecialized() 
                // If we still don't know which claim screen then try checking the LOB on the targeted
                // existing record. 

                if (sFormName.ToLower() == "claim")
                    sFormName = GetClaimFormNameFromRecord(recordId);

                // Note: the only difference between a power-view and a default system form is the 
                // viewid in the database (and requested) will be zero for "built-in" screens.
                if (DbFactory.IsOracleDatabase(m_strUserDbConnStr))
                {
                    SQL = String.Format(" SELECT VIEW_XML FROM NET_VIEW_FORMS WHERE VIEW_ID={0} AND UPPER(FORM_NAME)='{1}.XML'", viewId, sFormName.ToUpper());
                }
                else
                {
                    SQL = String.Format(" SELECT VIEW_XML FROM NET_VIEW_FORMS WHERE VIEW_ID={0} AND FORM_NAME='{1}.xml'", viewId, sFormName);
                }
                sViewXml = DbFactory.ExecuteScalar(m_strUserDbConnStr, SQL) as string;

                // BSB 11.04.2005 Placing this conditional here allows users to have power-views against ADMTracking tables
                // which would be a new feature...
                // 
                // Handle the case where what we really want is the screen defined for this 
                // particular AdminTracking Table...

                //code added by nadim for MITS 11845

                if (string.IsNullOrEmpty(sViewXml))

                    if (sFormName.Contains("admintracking|"))
                    {
                        SQL = String.Format(" SELECT VIEW_XML FROM NET_VIEW_FORMS WHERE VIEW_ID={0} AND FORM_NAME='admintracking.xml'{1}", viewId, DSNcond);
                        sViewXml = DbFactory.ExecuteScalar(m_strUserDbConnStr, SQL) as string;
                    }

                if (string.IsNullOrEmpty(sViewXml))
                {
                    if (viewId == Constants.Views.EXTENDED_SCREEN_VIEWID)  //Extended Screen View In Use
                        this.m_err.Add(Globalization.GetString("ViewError",m_iClientId),
                        String.Format(Globalization.GetString("FormViewAdaptor.ExtendedScreenViewNotFound",m_iClientId), sFormName),
                        BusinessAdaptorErrorType.Error);
                    else if (viewId != Constants.Views.BASE_VIEWID)  //Power View In Use
                        this.m_err.Add(Globalization.GetString("ViewError",m_iClientId),
                            String.Format(Globalization.GetString("FormViewAdaptor.PowerViewNotFound",m_iClientId), sFormName),
                            BusinessAdaptorErrorType.Error);
                    else
                        this.m_err.Add(Globalization.GetString("ViewError",m_iClientId),
                            String.Format(Globalization.GetString("FormViewAdaptor.SystemViewNotFound",m_iClientId), sFormName),
                            BusinessAdaptorErrorType.Error);

                    //			sViewXml = Globalization.GetString("FormViewAdaptor.MissingViewDefinition");
                }

                if (this.m_err.Count == 0)
                {
                    XmlDocument dom = new XmlDocument();
                    dom.LoadXml(sViewXml);

                    ApplyChildViewSecurityId(dom, prevSid);

                    //For ADMTable, ensure that view has aggregate formname including the SystemTableName.
                    if (sFormName.StartsWith("admintracking|"))
                    {
                        XmlElement elt = dom.SelectSingleNode("//internal[@name='SysFormName']") as XmlElement;
                        if (elt != null)
                            elt.Attributes["value"].Value = sFormName;

                    }

                    //Changed by Gagan for MITS 10376 : Start
                    //UpdateViews(dom);
                    //Changed by Gagan for MITS 10376 : End


                    return dom.DocumentElement.OuterXml;
                }
            }
            finally
            {
            }//finally
            return string.Empty;

		}
        public string FetchViewForRecordSummary(int prevSid, int viewId, int recordId, string sysFormName)
        {
            string SQL = "";
            string sViewXml = "";
            string sFormName = FormNameToSpecialized(sysFormName, prevSid);
            string DSNcond = "";
            if (m_err != null)
                m_err.Clear();

            try
            {
                //Perform Requested Process
                DSNcond = String.Format(" AND DATA_SOURCE_ID ={0}", m_userLogin.objRiskmasterDatabase.DataSourceId);
                // Already tacked on LOB from Security Id if available using FormNameToSpecialized() 
                // If we still don't know which claim screen then try checking the LOB on the targeted
                // existing record. 

                if (sFormName.ToLower() == "claim")
                    sFormName = GetClaimFormNameFromRecord(recordId);

                // Note: the only difference between a power-view and a default system form is the 
                // viewid in the database (and requested) will be zero for "built-in" screens.
                if (DbFactory.IsOracleDatabase(m_strViewDbConnStr))
                {
                    SQL = String.Format(" SELECT VIEW_XML FROM NET_VIEW_FORMS WHERE VIEW_ID={0} AND UPPER(FORM_NAME)='{1}.XML'{2}", viewId, sFormName.ToUpper(), DSNcond);
                }
                else
                {
                    SQL = String.Format(" SELECT VIEW_XML FROM NET_VIEW_FORMS WHERE VIEW_ID={0} AND FORM_NAME='{1}.xml'{2}", viewId, sFormName, DSNcond);
                }
                sViewXml = DbFactory.ExecuteAsType<string>(m_strViewDbConnStr, SQL);

                // BSB 11.04.2005 Placing this conditional here allows users to have power-views against ADMTracking tables
                // which would be a new feature...
                // 
                // Handle the case where what we really want is the screen defined for this 
                // particular AdminTracking Table...

                //code added by nadim for MITS 11845

                if (sViewXml == "")

                    if (sFormName.Contains("admintracking|"))
                    {
                        SQL = String.Format(" SELECT VIEW_XML FROM NET_VIEW_FORMS WHERE VIEW_ID={0} AND FORM_NAME='admintracking.xml'{1}", viewId, DSNcond);
                        sViewXml = DbFactory.ExecuteAsType<string>(m_strViewDbConnStr, SQL);
                    }

                if (sViewXml == "")
                {
                    if (viewId == Constants.Views.EXTENDED_SCREEN_VIEWID)  //Extended Screen View In Use
                        this.m_err.Add(Globalization.GetString("ViewError",m_iClientId),
                        String.Format(Globalization.GetString("FormViewAdaptor.ExtendedScreenViewNotFound",m_iClientId), sFormName),
                        BusinessAdaptorErrorType.Error);
                    else if (viewId != Constants.Views.BASE_VIEWID)  //Power View In Use
                        this.m_err.Add(Globalization.GetString("ViewError",m_iClientId),
                            String.Format(Globalization.GetString("FormViewAdaptor.PowerViewNotFound",m_iClientId), sFormName),
                            BusinessAdaptorErrorType.Error);
                    else
                        this.m_err.Add(Globalization.GetString("ViewError",m_iClientId),
                            String.Format(Globalization.GetString("FormViewAdaptor.SystemViewNotFound",m_iClientId), sFormName),
                            BusinessAdaptorErrorType.Error);

                    //			sViewXml = Globalization.GetString("FormViewAdaptor.MissingViewDefinition");
                }

                if (this.m_err.Count == 0)
                {
                    XmlDocument dom = new XmlDocument();
                    dom.LoadXml(sViewXml);
                    
                    //Changed by Saurabh P Arora Mits 18529: Start
                    //Mits 16664:Asif Start
                    /*
                    if (sFormName == "adjuster" || sFormName == "defendant" || sFormName == "litigation" || sFormName == "claimant")
                    {
                        if (dom.SelectSingleNode("//internal[@name='SysSid']")!=null)
                        {
                            if(dom.SelectSingleNode("//internal[@name='SysSid']").Attributes["value"].Value ==string.Empty)
                            {
                                //passing syssid  value to the xml
                            dom.SelectSingleNode("//internal[@name='SysSid']").Attributes["value"].Value = "3900";
                            }
                        }
                    }
                    */
                    //Mits 16664:Asif End
                    //ApplyChildViewSecurityId(dom, prevSid);
                    //Changed by Saurabh P Arora Mits 18529: End
                    

                    //For ADMTable, ensure that view has aggregate formname including the SystemTableName.
                    if (sFormName.StartsWith("admintracking|"))
                    {
                        XmlElement elt = dom.SelectSingleNode("//internal[@name='SysFormName']") as XmlElement;
                        if (elt != null)
                            elt.Attributes["value"].Value = sFormName;

                    }

                    //Changed by Gagan for MITS 10376 : Start
                    //UpdateViews(dom);
                    //Changed by Gagan for MITS 10376 : End


                    return dom.DocumentElement.OuterXml;
                }
            }
            finally
            {
                
            }
            return string.Empty;

        }

        ////Changed by Gagan for MITS 10376 : Start

        ///// <summary>
        ///// Update all the views as per the dimensions
        ///// for freecode,memo,textml and readonly memo
        ///// </summary>
        ///// <param name="p_objXmlDoc"></param>
        //public void UpdateViews(XmlDocument p_objXmlDoc)
        //{
        //    int iTextMLCols = 0;
        //    int iTextMLRows = 0;
        //    int iFreeCodeCols = 0;
        //    int iFreeCodeRows = 0;
        //    int iReadOnlyMemoCols = 0;
        //    int iReadOnlyMemoRows = 0;
        //    int iMemoCols = 0;
        //    int iMemoRows = 0;
        //    string sSQL = String.Empty;
        //    XmlNodeList objNodeList = null;
        //    XmlAttribute objAttribute = null;
        //    LocalCache objLocalCache = null;

        //    objLocalCache = new LocalCache(m_userLogin.objRiskmasterDatabase.ConnectionString);

        //    iTextMLCols = objLocalCache.GetWidth("TextML");
        //    iFreeCodeCols = objLocalCache.GetWidth("FreeCode");
        //    iReadOnlyMemoCols = objLocalCache.GetWidth("ReadOnlyMemo");
        //    iMemoCols = objLocalCache.GetWidth("Memo");

        //    iTextMLRows = objLocalCache.GetHeight("TextML");
        //    iFreeCodeRows = objLocalCache.GetHeight("FreeCode");
        //    iReadOnlyMemoRows = objLocalCache.GetHeight("ReadOnlyMemo");
        //    iMemoRows = objLocalCache.GetHeight("Memo");

        //    objNodeList = p_objXmlDoc.SelectNodes("//control[@type='textml']");
        //    foreach (XmlNode objNode in objNodeList)
        //    {
        //        if (((XmlElement)objNode).Attributes["cols"] == null)
        //        {
        //            objAttribute = p_objXmlDoc.CreateAttribute("cols");
        //            ((XmlElement)objNode).SetAttributeNode(objAttribute);
        //        }
        //        ((XmlElement)objNode).Attributes["cols"].Value = iTextMLCols.ToString();

        //        if (((XmlElement)objNode).Attributes["rows"] == null)
        //        {
        //            objAttribute = p_objXmlDoc.CreateAttribute("rows");
        //            ((XmlElement)objNode).SetAttributeNode(objAttribute);
        //        }
        //        ((XmlElement)objNode).Attributes["rows"].Value = iTextMLRows.ToString();
        //    }

        //    objNodeList = p_objXmlDoc.SelectNodes("//control[@type='freecode']");
        //    foreach (XmlNode objNode in objNodeList)
        //    {
        //        if (((XmlElement)objNode).Attributes["cols"] == null)
        //        {
        //            objAttribute = p_objXmlDoc.CreateAttribute("cols");
        //            ((XmlElement)objNode).SetAttributeNode(objAttribute);
        //        }
        //        ((XmlElement)objNode).Attributes["cols"].Value = iFreeCodeCols.ToString();
        //        if (((XmlElement)objNode).Attributes["rows"] == null)
        //        {
        //            objAttribute = p_objXmlDoc.CreateAttribute("rows");
        //            ((XmlElement)objNode).SetAttributeNode(objAttribute);
        //        }
        //        ((XmlElement)objNode).Attributes["rows"].Value = iFreeCodeRows.ToString();
        //    }

        //    objNodeList = p_objXmlDoc.SelectNodes("//control[@type='readonlymemo']");
        //    foreach (XmlNode objNode in objNodeList)
        //    {
        //        if (((XmlElement)objNode).Attributes["cols"] == null)
        //        {
        //            objAttribute = p_objXmlDoc.CreateAttribute("cols");
        //            ((XmlElement)objNode).SetAttributeNode(objAttribute);
        //        }
        //        ((XmlElement)objNode).Attributes["cols"].Value = iReadOnlyMemoCols.ToString();
        //        if (((XmlElement)objNode).Attributes["rows"] == null)
        //        {
        //            objAttribute = p_objXmlDoc.CreateAttribute("rows");
        //            ((XmlElement)objNode).SetAttributeNode(objAttribute);
        //        }
        //        ((XmlElement)objNode).Attributes["rows"].Value = iReadOnlyMemoRows.ToString();
        //    }

        //    objNodeList = p_objXmlDoc.SelectNodes("//control[@type='memo']");
        //    foreach (XmlNode objNode in objNodeList)
        //    {
        //        if (((XmlElement)objNode).Attributes["cols"] == null)
        //        {
        //            objAttribute = p_objXmlDoc.CreateAttribute("cols");
        //            ((XmlElement)objNode).SetAttributeNode(objAttribute);
        //        }
        //        ((XmlElement)objNode).Attributes["cols"].Value = iMemoCols.ToString();
        //        if (((XmlElement)objNode).Attributes["rows"] == null)
        //        {
        //            objAttribute = p_objXmlDoc.CreateAttribute("rows");
        //            ((XmlElement)objNode).SetAttributeNode(objAttribute);
        //        }

        //        ((XmlElement)objNode).Attributes["rows"].Value = iMemoRows.ToString();
        //    }

        //    objNodeList = null;
        //    objAttribute = null;
        //}

        ////Changed by Gagan for MITS 10376 : End


		#region Private Utility Functions
		// TODO Take an LOB Specific name and map it to a Generic Name...
		// For example: claimantwc-->claimant
		// If no mapping is found, the original form name is simply returned.
		public string FormNameToSpecialized(string sForm, int Sid)
		{
			if(sForm=="claim")
				return sForm + LobSuffixFromSid(Sid);
			else
				return sForm;
		}
		/// <summary>
		/// Child View will have either:
		/// 1.) An XML mapping from Parent to Child Security Id (LOB Dependent)
		/// 	<security parentid="150" sid="1050" />
		///		<!--GC-->
		///		<security parentid="6000" sid="6900" />
		///		<!--VA-->
		///		<security parentid="3000" sid="3900" />
		///		<!--WC-->
		///		<security parentid="60000" sid="60900" />
		///		<!--DI-->
		/// 2.) An individual Security Id. (Non LOB dependent)
		/// <internal type="hidden" name="SysSid" ref="Instance/UI/FormVariables/SysSid" value="11000"/>
		/// By the end of this function call the view will have a node as in item 2.) and
		/// it will be populated with the "most appropriate" security id available.
		/// If the document is not a legacy XML view form, no change is made.
		/// </summary>
		/// <param name="dom"></param>
		/// <returns></returns>
		private void ApplyChildViewSecurityId(XmlDocument dom, int prevSid)
		{
			string sMappedSid = "";
			XmlElement elt = null;
			XmlAttribute att = null;
			
			//BSB 01.30.2006 Part of Security Fix for List Screens.
			// Not a legacy screen definition.
			//if(dom.DocumentElement.Name!="form")
			//	return;

			//Try to find a security map entry.
			elt = dom.SelectSingleNode(String.Format("//security[@parentid='{0}']",prevSid)) as XmlElement;
			if(elt !=null)
				sMappedSid = elt.Attributes["sid"].Value;
			else
			{
				//BSB Handle Case for "post-back" where the SID sent in is not a "parent" SID but is still accurate.
				elt = dom.SelectSingleNode(String.Format("//security[@sid='{0}']",prevSid)) as XmlElement;
				if(elt!=null)
					sMappedSid = elt.Attributes["sid"].Value;
			}

			//Try to find the bound security node.
			elt = dom.SelectSingleNode("//internal[@name='SysSid']") as XmlElement;
			if(elt ==null)
				elt = dom.SelectSingleNode("//internal[@name='sys_sid']") as XmlElement;
			
			//Make a security node on the fly.
			if(elt==null)
			{
				elt = dom.CreateElement("internal");
				att = dom.CreateAttribute("name");
				att.Value = "SysSid";
				elt.Attributes.Append(att);
				att = dom.CreateAttribute("type");
				att.Value = "hidden";
				elt.Attributes.Append(att);
				att = dom.CreateAttribute("ref");
				att.Value = "Instance/UI/FormVariables/SysSid";
				elt.Attributes.Append(att);
				att = dom.CreateAttribute("value");
				att.Value = "0";
				elt.Attributes.Append(att);
				dom.DocumentElement.AppendChild(elt);
			}

			//Set the SecurityId we want.
			if(sMappedSid !="")
				elt.Attributes["value"].Value=sMappedSid;

			//Throw an error if we expected the legacy screen definition to have a security id.
			if(Conversion.ConvertStrToInteger(elt.Attributes["value"].Value) ==0)
				throw new Riskmaster.ExceptionTypes.RMAppException(String.Format(Globalization.GetString("FormViewAdaptor.ViewMissingSecurityId",m_iClientId)));
		}

		private string LobSuffixFromSid(int Sid)
		{
			int curSid = Sid;
			while(!IsTopLevelSid(curSid))
				curSid = Riskmaster.Security.SecurityDatabase.GetParentFuncId(curSid, m_iClientId);
			
			switch(curSid)
			{
				case FormViewManager.RMO_GC:
					return "gc";
				case FormViewManager.RMO_VA:
					return "va";
				case FormViewManager.RMO_WC:
					return "wc";
				case FormViewManager.RMO_DI:
					return "di";

				case FormViewManager.RMO_NONE:
				case FormViewManager.RMO_RM:
				default:
					return "";
			}		
		}
		private bool IsTopLevelSid(int Sid)
		{
			return( Sid==FormViewManager.RMO_NONE ||
				Sid==FormViewManager.RMO_RM ||
				Sid==FormViewManager.RMO_GC ||
				Sid==FormViewManager.RMO_VA ||
				Sid==FormViewManager.RMO_WC ||
				Sid==FormViewManager.RMO_DI  );
		}
		private bool IsTopLevelSuffix(string suffix)
		{
			string sTmp = suffix.ToLower();
			return( sTmp== "gc" ||
				sTmp=="va" ||
				sTmp=="wc" ||
				sTmp=="di"  );
		}
		public string GetClaimFormNameFromRecord(int claimId)
		{
            if (claimId == 0)
                return string.Empty;

            int LOB = DbFactory.ExecuteAsType<int>(m_strUserDbConnStr, String.Format("SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID={0}", claimId));
            string suffix = DbFactory.ExecuteAsType<string>(m_strUserDbConnStr, String.Format("SELECT SHORT_CODE FROM CODES WHERE CODE_ID={0}", LOB));
			return "claim" + suffix.ToLower();
		}
		
		//Mukul 3/15/2007 MITS 9019
        //avipinsrivas start : Worked for JIRA - 7767
        public string GetEntityFormNameFromRecord(int entityId)
        {
            LocalCache objLocalCache = null;
            SysSettings objSettings = new SysSettings(m_strUserDbConnStr, m_iClientId);

            if (entityId == 0 || objSettings.UseEntityRole)
                return "entitymaint";

            try
            {
                int iTableId = DbFactory.ExecuteAsType<int>(m_strUserDbConnStr, String.Format("SELECT ENTITY_TABLE_ID FROM ENTITY WHERE ENTITY_ID ={0}", entityId));
                objLocalCache = new LocalCache(m_strUserDbConnStr, m_iClientId);

                if (iTableId == objLocalCache.GetTableId("EMPLOYEES"))
                    return "employee";
                else if (iTableId == objLocalCache.GetTableId("PHYSICIANS"))
                    return "physician";
                else if (iTableId == objLocalCache.GetTableId("MEDICAL_STAFF"))
                    return "staff";
                else
                {
                    int iGlossType = objLocalCache.GetGlossaryType(iTableId);
                    if (iGlossType == 7)
                        return "people";
                    else
                        return "entitymaint";
                }
            }
            finally
            {
                objLocalCache = null;
                objSettings = null;
            }
        }
        //public string GetEntityFormNameFromRecord(int entityId)
        //{
        //    if (entityId == 0)
        //        return "entitymaint";
        //    int iTableId = 0;
        //    int iGlossType = 0;

        //    //avipinsrivas Start : Worked for Jira-340
        //    Dictionary<string, int> objDict = null;
        //    SysSettings objSettings = null;
        //    LocalCache objLocalCache = null;
        //    StringBuilder sQry = null;

        //    try
        //    {
        //        objSettings = new SysSettings(m_strUserDbConnStr, m_iClientId);
        //        objLocalCache = new LocalCache(m_strUserDbConnStr, m_iClientId);
        //        objDict = new Dictionary<string, int>();
        //        sQry = new StringBuilder();

        //        if (objSettings.UseEntityRole)
        //            sQry.Append("SELECT EXR.ENTITY_TABLE_ID ");
        //        else
        //            sQry.Append("SELECT E.ENTITY_TABLE_ID ");
        //        sQry.Append(" FROM ENTITY E");
        //        if (objSettings.UseEntityRole)
        //            sQry.Append(" INNER JOIN ENTITY_X_ROLES EXR ON EXR.ENTITY_ID = E.ENTITY_ID ");
        //        sQry.Append(" WHERE E.ENTITY_ID ={0}");

        //        sQry.Replace("{0}", "~ENTITYID~");
        //        objDict.Add("ENTITYID", entityId);


        //        using (DataSet ds = DbFactory.ExecuteDataSet(m_strUserDbConnStr, sQry.ToString(), objDict, m_iClientId))
        //        {
        //            bool bSuccess = false;
        //            bool bFromEntityScreen = false;

        //            if (ds.Tables.Count > 0)
        //            {
        //                if (ds.Tables[0].Rows.Count > 1)
        //                {
        //                    foreach (DataRow dr in ds.Tables[0].Rows)
        //                    {
        //                        iTableId = Conversion.CastToType<int>(dr["ENTITY_TABLE_ID"].ToString(), out bSuccess);
        //                        iGlossType = objLocalCache.GetGlossaryType(iTableId);

        //                        if (int.Equals(iGlossType, 4))
        //                        {
        //                            bFromEntityScreen = true;
        //                            break;
        //                        }
        //                    }

        //                    if (bFromEntityScreen)
        //                        return "entitymaint";
        //                    else
        //                        return "people";
        //                }
        //                else if (int.Equals(ds.Tables[0].Rows.Count, 1))
        //                {
        //                    iTableId = Conversion.CastToType<int>(ds.Tables[0].Rows[0]["ENTITY_TABLE_ID"].ToString(), out bSuccess);
        //                }
        //            }
        //        }
        //        //End avipinsrivas

        //        if (iTableId == objLocalCache.GetTableId("EMPLOYEES"))
        //            return "employee";
        //        else if (iTableId == objLocalCache.GetTableId("PHYSICIANS"))
        //            return "physician";
        //        else if (iTableId == objLocalCache.GetTableId("MEDICAL_STAFF"))
        //            return "staff";
        //        else
        //        {
        //            //int iGlossType = DbFactory.ExecuteAsType<int>(m_strUserDbConnStr, String.Format("SELECT GLOSSARY_TYPE_CODE FROM GLOSSARY WHERE TABLE_ID={0}", iTableId));
        //            iGlossType = objLocalCache.GetGlossaryType(iTableId);
        //            if (iGlossType == 7)
        //                return "people";
        //            else
        //                return "entitymaint";
        //        }
        //    }
        //    finally
        //    {
        //        objSettings = null;
        //        objLocalCache = null;
        //        objDict = null;
        //    }
        //}
        //avipinsrivs End
        //avipinsrivas end
		#endregion
	}
}
