﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.Security.Encryption;

namespace Riskmaster.BusinessAdaptor
{
    class SingleSignonAdaptor : BusinessAdaptorBase
    {
        private string FASUrl = string.Empty;
        #region "Public Method to Fill XML."
        public bool SingleSignonFASXML(XmlDocument p_objXMLIn, ref XmlDocument p_objXMLOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            XmlNode objNode = null;
            try
            {
                NameValueCollection nvColl = RMConfigurationManager.GetNameValueSectionSettings("FASInterFace", userLogin.objRiskmasterDatabase.ConnectionString, base.ClientId);
                if (nvColl != null)
                {
                    FASUrl = nvColl["FASLoginURL"];
                }
                // FAS Login Url
                objNode = (XmlElement)p_objXMLIn.SelectSingleNode("//FASUrl");
                objNode.InnerText = FASUrl;

                // RMX User Name
                objNode = (XmlElement)p_objXMLIn.SelectSingleNode("//loginName");
                objNode.InnerText = userLogin.LoginName;

                // RMX Password
                objNode = (XmlElement)p_objXMLIn.SelectSingleNode("//passwd");
                objNode.InnerText = RMCryptography.EncryptString(userLogin.Password);
                p_objXMLOut = p_objXMLIn;
            }
            catch (Exception ex)
            {
                //throw;
                return false;
            }
            finally
            {
                objNode = null;
            }
            return true;
        }
        #endregion
    }
}
