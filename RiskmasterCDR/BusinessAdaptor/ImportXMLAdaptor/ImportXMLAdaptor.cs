﻿using System;
using System.Xml;
using System.Data;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.ImportXML;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using System.IO;
using Riskmaster.Security;
using System.Xml.Linq;
using Riskmaster.Models;

namespace Riskmaster.BusinessAdaptor
{
    ///************************************************************** 
    ///* $File		: ImportXMLAdaptor.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 22-July-2011
    ///* $Author	: Manika Dhamija
    ///* $Comment	: 
    ///* $Source	: 
    ///**************************************************************
    public class ImportXMLAdaptor : BusinessAdaptorBase
    {
        public ImportXMLAdaptor() { }

        public bool XMLImport(string inputxml, ref BusinessAdaptorErrors p_objErrOut, out XMLOutPut oXMLOutput, bool rVal)
        {
            ImportXML objxml = null;
            bool bresult = false;
            oXMLOutput = new XMLOutPut();
            string logFileContent = string.Empty;
            bool bIsEvent = false;
            try
            {
                XDocument XMLLinqDoc = null;
                XMLLinqDoc = GetLinqXMLDoc(inputxml);
                objxml = new ImportXML(connectionString, userLogin.objRiskmasterDatabase.DataSourceName, userLogin.Password, userLogin.LoginName, base.ClientId);//rkaur27
                objxml.XMLDoc = XMLLinqDoc;
                logFileContent = objxml.ReadXML(out bIsEvent, rVal);
                oXMLOutput.FileContent = logFileContent;
                oXMLOutput.bIsEvent = bIsEvent;
                bresult = true;
            }
            catch(Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                bresult = false;
            }
            return bresult; 
        }
        private XElement GetLinqXML(string p_objXml)
        {
            XElement LinqXML = null;
            LinqXML = XElement.Parse(p_objXml);
            return LinqXML;
        }
        private XDocument GetLinqXMLDoc(string p_objXml)
        {
            XDocument LinqXMLDoc = null;
            LinqXMLDoc = XDocument.Parse(p_objXml);
            return LinqXMLDoc;
        }

        public bool GetSupplementals(XMLTemplate oXMLInput, out XMLTemplate objReturn, ref BusinessAdaptorErrors p_objErrOut)
        {
            ImportXML objxml = null;
            bool bresult = false;
            XElement objEle = null;
            objReturn = new XMLTemplate();
            try
            {
                // XMLLinqDoc = GetLinqXMLDoc(inputxml);
                objxml = new ImportXML(connectionString, userLogin.objRiskmasterDatabase.DataSourceName, userLogin.Password, userLogin.LoginName, base.ClientId);//rkaur27
               // objxml.XMLDoc = XMLLinqDoc;
                objEle=  objxml.GetSupplementals(oXMLInput.NodeName);
        
                objReturn.NodeName = oXMLInput.NodeName;
                objReturn.SuppXml = objEle;
                bresult = true;
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                bresult = false;
            }
            return bresult; 
        }

    }
}
