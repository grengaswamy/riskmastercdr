﻿using System;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.Application.FundManagement;
using Riskmaster.Models;
using System.Xml.Linq;
using Riskmaster.Security;

namespace Riskmaster.BusinessAdaptor
{
    public class DeductibleManagementAdaptor : BusinessAdaptorBase
    {
        #region constructor
        public DeductibleManagementAdaptor()
        {
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Gets All Deductibles for a given claim
        /// </summary>
        /// <param name="p_objXmlIn">Xml Containing Claim Id</param>
        /// <param name="p_objXmlOut">Xml Containing the deductible info</param>
        /// <param name="p_objErrOut">Error, if any</param>
        /// <returns>true if no error</returns>
        public bool LoadDeductiblesForClaim(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            DeductibleManager objDedManamgement = null;
            int iClaimId = 0;
            XmlNode xClaimId = null;
            bool bSuccess = false;

            try
            {
                xClaimId = p_objXmlIn.SelectSingleNode("//ClaimId");
                if (xClaimId != null && !String.IsNullOrEmpty(xClaimId.InnerText))
                {
                    iClaimId = Conversion.CastToType<Int32>(xClaimId.InnerText, out bSuccess);
                }
                if (bSuccess && iClaimId > 0)
                {
                    objDedManamgement = new DeductibleManager(base.connectionString, base.userLogin, base.ClientId);//ddhiman added: Cloud changes, 10/10/2014
                    bSuccess = objDedManamgement.LoadDeductibleForClaim(iClaimId, p_objXmlOut);
                }

               
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("DeductibleManagementAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            //Start - Added by Nikhil.Code review changes
            finally 
            {
                if (objDedManamgement != null)
                {
                    objDedManamgement = null;
                }
                if (xClaimId != null)
                {
                    xClaimId = null;

                }

            }
            return bSuccess;
            //End  - Added by Nikhil.Code review changes
        }

        /// <summary>
        /// Gets the Deductible for the given Deductible Id
        /// </summary>
        /// <param name="p_objXmlIn">Input Xml</param>
        /// <param name="p_objXmlOut">Output Xml</param>
        /// <param name="p_objErrOut">Errors, if any</param>
        /// <returns>true on success</returns>
        public bool LoadDeductiblesForID(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {

            DeductibleManager objDedManamgement = null;
            int iDedId = 0;
            XmlNode xDedId = null;
            int iCvgId = 0;
            XmlNode xCvgId = null;
            bool bSuccess = false;

            try
            {
                xDedId = p_objXmlIn.SelectSingleNode("//CLM_X_POL_DED_ID");
                xCvgId = p_objXmlIn.SelectSingleNode("//POLCVG_ROW_ID");

                if (xDedId != null && !String.IsNullOrEmpty(xDedId.InnerText))
                {
                    iDedId = Conversion.CastToType<Int32>(xDedId.InnerText, out bSuccess);
                }
                if (xCvgId != null && !String.IsNullOrEmpty(xDedId.InnerText))
                {
                    iCvgId = Conversion.CastToType<Int32>(xCvgId.InnerText, out bSuccess);
                }
                if (bSuccess && iCvgId > 0)
                {
                    objDedManamgement = new DeductibleManager(base.connectionString, base.userLogin, base.ClientId);//ddhiman added: Cloud changes, 10/10/2014
                    bSuccess = objDedManamgement.LoadDeductible(iDedId, iCvgId, p_objXmlIn);
                }
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                bSuccess = false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("DeductibleManagementAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                bSuccess = false;
            }
            finally
            {
                p_objXmlOut.InnerXml = p_objXmlIn.InnerXml;
                //Start  - Added by Nikhil.Code review changes
                if (objDedManamgement != null)
                {
                    objDedManamgement = null;
                }
                if (xDedId != null)
                {
                    xDedId = null;
                }
                if (xCvgId != null)
                {
                    xCvgId = null;
                }
                //End  - Added by Nikhil.Code review changes
            }

            return bSuccess;
        }

        /// <summary>
        /// Saves deductible info into database
        /// </summary>
        /// <param name="p_objXmlIn">Input Xml</param>
        /// <param name="p_objXmlOut">Output Xml</param>
        /// <param name="p_objErrOut">Errors, if any</param>
        /// <returns></returns>
        public bool SaveDeductible(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            DeductibleManager objDedManamgement = null;
            int iDedId = 0;
            XmlNode xDedId = null;
            int iCvgId = 0;
            XmlNode xCvgId = null;
            bool bSuccess = false;

            try
            {
                xDedId = p_objXmlIn.SelectSingleNode("//CLM_X_POL_DED_ID");
                xCvgId = p_objXmlIn.SelectSingleNode("//POLCVG_ROW_ID");

                if (xDedId != null && !String.IsNullOrEmpty(xDedId.InnerText))
                {
                    iDedId = Conversion.CastToType<Int32>(xDedId.InnerText, out bSuccess);
                }
                if (xCvgId != null && !String.IsNullOrEmpty(xDedId.InnerText))
                {
                    iCvgId = Conversion.CastToType<Int32>(xCvgId.InnerText, out bSuccess);
                }
                if (bSuccess && iCvgId > 0)
                {
                    objDedManamgement = new DeductibleManager(base.connectionString, base.userLogin, base.ClientId);//ddhiman added: Cloud changes, 10/10/2014
                    bSuccess = objDedManamgement.SaveDeductible(iDedId, iCvgId, p_objXmlIn, p_objXmlOut);
                }
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                bSuccess = false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("DeductibleManagementAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                bSuccess = false;
            }
            finally
            {
                if (!bSuccess)
                {
                    p_objXmlOut = p_objXmlIn;
                }
                //Start  - Added by Nikhil.Code review changes
                if (objDedManamgement != null)
                {
                    objDedManamgement = null;
                }
                if (xDedId != null)
                {
                    xDedId = null;
                }
                if (xCvgId != null)
                {
                    xCvgId = null;
                }
                //End  - Added by Nikhil.Code review changes
            }

            return bSuccess;
        }

        /// <summary>
        /// Gets the History for the deductible
        /// </summary>
        /// <param name="p_objXmlIn">Input Xml</param>
        /// <param name="p_objXmlOut">Output Xml</param>
        /// <param name="p_objErrOut">Errors, if any</param>
        /// <returns>true on success</returns>
        public bool GetDeductibleHistory(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            DeductibleManager objDedManamgement = null;
            int iClaimXPolDedId = 0;
            XmlNode xClaimXPolDedId = null;
            bool bSuccess = false;

            try
            {
                xClaimXPolDedId = p_objXmlIn.SelectSingleNode("//CLM_X_POL_DED_ID");
                if (xClaimXPolDedId != null && !String.IsNullOrEmpty(xClaimXPolDedId.InnerText))
                {
                    iClaimXPolDedId = Conversion.CastToType<Int32>(xClaimXPolDedId.InnerText, out bSuccess);
                }
                if (bSuccess && iClaimXPolDedId > 0)
                {
                    objDedManamgement = new DeductibleManager(base.connectionString, base.userLogin, base.ClientId);//ddhiman added: Cloud changes, 10/10/2014
                    p_objXmlOut = objDedManamgement.GetDeductibleHistory(iClaimXPolDedId);
                }
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                bSuccess = false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("DeductibleManagementAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                bSuccess = false;
            }
            finally
            {
                if (!bSuccess)
                {
                    p_objXmlOut = p_objXmlIn;
                }
                //Start  - Added by Nikhil.Code review changes
                if (objDedManamgement != null)
                {
                    objDedManamgement = null;
                }
                if (xClaimXPolDedId != null)
                {
                    xClaimXPolDedId = null;
                }
               //End  - Added by Nikhil.Code review changes
            }
            return bSuccess;
        }

        /// <summary>
        /// Gets Details for Diminishing Deductible
        /// </summary>
        /// <param name="p_objXmlIn">Input Xml</param>
        /// <param name="oUserLogin">Output Xml</param>
        /// <param name="p_objErrOut">Errors, if any</param>
        /// <returns></returns>
        public bool GetDiminishingDetails(XElement p_objXmlIn, UserLogin oUserLogin, ref BusinessAdaptorErrors p_objErrOut)
        {
            DeductibleManager objDedManamgement = new DeductibleManager(oUserLogin.objRiskmasterDatabase.ConnectionString, oUserLogin, base.ClientId);//ddhiman added: Cloud changes, 10/10/2014
            try
            {
                objDedManamgement.GetDiminishingDetails(p_objXmlIn);
                
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("DeductibleManagementAdaptor.GetDiminishig.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                //start  - Added by Nikhil.Code review changes
                if (objDedManamgement != null)
                {
                    objDedManamgement = null;
                }
               
            }
            return true;
            //End  - Added by Nikhil.Code review changes
        }

        /// <summary>
        /// Verifies if the Diminishing date is present in database
        /// </summary>
        /// <param name="p_objXmlIn">Input Xml</param>
        /// <param name="oRequest">Policy Inquiry object. It will be updated from table if CvgEval Date is not available</param>
        /// <param name="oUserLogin">User Login Object</param>
        /// <returns>If Coverage Eval Date is available - then returns true. Else returns false</returns>
        public bool IsCvgEvalDateAvailable(XElement p_objXmlIn, PolicyEnquiry oRequest, UserLogin oUserLogin)
        {
            DeductibleManager objDedManamgement = null;
            bool bSuccess = false;
            try
            {
                objDedManamgement = new DeductibleManager(base.connectionString, oUserLogin, base.ClientId);//ddhiman added: Cloud changes, 10/10/2014
                bSuccess =  objDedManamgement.IsCoverageEvalDateAvailable(p_objXmlIn, oRequest);
            }
            catch (Exception)
            {
                throw;
            }
            //start  - Added by Nikhil.Code review changes
            finally
            {

                if (objDedManamgement != null)
                {
                    objDedManamgement = null;
                }

            }
            return bSuccess;
            //end  - Added by Nikhil.Code review changes

        }
        /// <summary>
        /// Retrieve aggregate details for coverage group
        /// </summary>
        /// <param name="p_objXmlIn">Input Xml</param>
        /// <param name="p_objXmlOut">Output XML</param>
        /// <param name="p_objErrOut">Error object</param>
        /// <returns>Return aggregate details</returns>
        public bool GetAggregateDetails(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            DeductibleManager objDedManamgement = null;
            bool bSuccess = false;
            try
            {
                objDedManamgement = new DeductibleManager(base.connectionString, base.userLogin, base.ClientId);//ddhiman added: Cloud changes, 10/10/2014
                p_objXmlOut = objDedManamgement.GetAggregateDetails(p_objXmlIn);
                bSuccess = true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                bSuccess = false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("DeductibleManagementAdaptor.GetAggregate.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                bSuccess = false;
            }
            //start  - Added by Nikhil.Code review changes
            finally
            {
                
                if (objDedManamgement != null)
                {
                    objDedManamgement = null;
                }
               
            }
            //end  - Added by Nikhil.Code review changes

            return bSuccess;
        }
        /// <summary>
        /// Saves aggregate info into database
        /// </summary>
        /// <param name="p_objXmlIn">Input Xml</param>
        /// <param name="p_objXmlOut">Output Xml</param>
        /// <param name="p_objErrOut">Errors, if any</param>
        /// <returns></returns>
        public bool SaveAggregateDetails(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            DeductibleManager objDedManamgement = null;
            bool bSuccess = false;
            try
            {
                objDedManamgement = new DeductibleManager(base.connectionString, base.userLogin, base.ClientId);//ddhiman added: Cloud changes, 10/10/2014
                p_objXmlOut = objDedManamgement.SaveAggregateDetails(p_objXmlIn);
                bSuccess = true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                bSuccess = false;
                p_objXmlOut = p_objXmlIn;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("DeductibleManagementAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                bSuccess = false;
                p_objXmlOut = p_objXmlIn;
            }
            finally
            {
                //Start - Changed by Nikhil.Code review changes
                if (objDedManamgement != null)
                {
                    objDedManamgement = null;
                }
                //end - Changed by Nikhil.Code review changes
            }

            return bSuccess;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool GetAggregateHistory(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            DeductibleManager objDedManamgement = null;
            bool bSuccess = false;
            try
            {
                objDedManamgement = new DeductibleManager(base.connectionString, base.userLogin, base.ClientId);//ddhiman added: Cloud changes, 10/10/2014
                p_objXmlOut = objDedManamgement.GetAggregateHistory(p_objXmlIn);
                bSuccess = true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                bSuccess = false;
                p_objXmlOut = p_objXmlIn;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("DeductibleManagementAdaptor.GetAggregate.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                bSuccess = false;
                p_objXmlOut = p_objXmlIn;
            }
            finally
            {
                 //Start - Changed by Nikhil.Code review changes
                if (objDedManamgement != null)
                {
                    objDedManamgement = null;
                }
                  //End - Changed by Nikhil.Code review changes

            }

            return bSuccess;
        }
        #endregion

        #region Event Level Deductible
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool GetDeductibleDetailsForEvent(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            DeductibleManager objDedManamgement = null;
            bool bSuccess = false;
            try
            {
                objDedManamgement = new DeductibleManager(base.connectionString, base.userLogin, base.ClientId);//ddhiman added: Cloud changes, 10/10/2014
                p_objXmlOut = objDedManamgement.GetDeductibleDetailsForEvent(p_objXmlIn);
                bSuccess = true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                bSuccess = false;
                p_objXmlOut = p_objXmlIn;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("DeductibleManagementAdaptor.GetDedDetailsEvent.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                bSuccess = false;
                p_objXmlOut = p_objXmlIn;
            }
            finally
            {
                //Start - Changed by Nikhil.Code review changes
                if (objDedManamgement != null)
                {
                    objDedManamgement = null;
                }
                //End - Changed by Nikhil.Code review changes
            }

            return bSuccess;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool GetCoverageDeductibleForClaim(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            DeductibleManager objDedManamgement = null;
            bool bSuccess = false;
            try
            {
                objDedManamgement = new DeductibleManager(base.connectionString, base.userLogin, base.ClientId);//ddhiman added: Cloud changes, 10/10/2014
                p_objXmlOut = objDedManamgement.GetCoverageDeductibleForClaim(p_objXmlIn);
                bSuccess = true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                bSuccess = false;
                p_objXmlOut = p_objXmlIn;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("DeductibleManagementAdaptor.GetCvgDedForClaim.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                bSuccess = false;
                p_objXmlOut = p_objXmlIn;
            }
            finally
            {
                //Start - Changed by Nikhil.Code review changes
                if (objDedManamgement != null)
                {
                    objDedManamgement = null;
                }
                //End - Changed by Nikhil.Code review changes
            }

            return bSuccess;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool LoadThirdPartyData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            DeductibleManager objDedManamgement = null;
            bool bSuccess = false;
            try
            {
                objDedManamgement = new DeductibleManager(base.connectionString, base.userLogin, base.ClientId);//ddhiman added: Cloud changes, 10/10/2014
                p_objXmlOut = objDedManamgement.LoadThirdPartyData(p_objXmlIn);
                bSuccess = true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                bSuccess = false;
                p_objXmlOut = p_objXmlIn;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("DeductibleManagementAdaptor.GetThirdPartyData.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                bSuccess = false;
                p_objXmlOut = p_objXmlIn;
            }
            finally
            {
                //Start - Changed by Nikhil.Code review changes
                if (objDedManamgement != null)
                {
                    objDedManamgement = null;
                }
                //End - Changed by Nikhil.Code review changes
            }

            return bSuccess;
        }
        #endregion
    }
}
