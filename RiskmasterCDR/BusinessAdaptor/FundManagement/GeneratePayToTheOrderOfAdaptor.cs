﻿using System;
using System.IO;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.FundManagement;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using Riskmaster.Settings;

namespace Riskmaster.BusinessAdaptor
{
    class GeneratePayToTheOrderOfAdaptor : BusinessAdaptorBase 
    {
        public bool GetCodeType(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            GeneratePayToTheOrderOf objGeneratePayToTheOrderOf = null;
            try
            {
                objGeneratePayToTheOrderOf = new GeneratePayToTheOrderOf(connectionString, base.ClientId);
                p_objXmlOut = objGeneratePayToTheOrderOf.GetCodeType(p_objXmlIn, base.userLogin);   //Aman ML Change
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("GeneratePayToTheOrderOfAdaptor.GetCodeType.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objGeneratePayToTheOrderOf = null;
            }
        }

    }
}
