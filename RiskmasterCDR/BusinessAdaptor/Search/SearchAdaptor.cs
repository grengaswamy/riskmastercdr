/**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            *
 **********************************************************************************************
 * 02/11/2014 | 34276  | achouhan3   | Enhancement to Entity ID Type View Permission functionality in RMA 
 * 11/20/2014 | 34278/RMA-345  | achouhan3   | Added methos to get/set User Preferences 
 **********************************************************************************************/

using System;
using System.Xml;
using System.IO;
using Riskmaster.Application.Search;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.DataModel;
using System.Xml.Linq;
using System.Xml.XPath;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// BusinessAdaptor for conducting Searches
	/// </summary>
	public class SearchAdaptor : BusinessAdaptorBase
	{
		#region Private Security code constants
		//Added by Mohit Yadav for Bug No. 001861
		private const int RMO_ACCESS = 0;
		
		private const int RMB_SEARCH = 8000000;
		private const int RMB_SEARCH_CLAIM = 8000001;
		private const int RMB_SEARCH_EVENT = 8000002;
		private const int RMB_SEARCH_EMPLOYEE = 8000003;
		private const int RMB_SEARCH_PEOPLE_ENTITY = 8000004;
		private const int RMB_SEARCH_VEHICLE = 8000005;
		private const int RMB_SEARCH_POLICY = 8000006;
		private const int RMB_SEARCH_PAYMENT = 8000007;
		private const int RMB_SEARCH_PATIENT = 8000008;
		private const int RMB_SEARCH_PHYSICIAN = 8000009;
		private const int RMB_SEARCH_MEDSTAFF = 8000010;
		private const int RMB_SEARCH_DISABILITYPLAN = 8000011;
		private const int RMB_SEARCH_ADMINTRACKING = 8000020;
		private const int RMB_SEARCH_ENHPOLICY = 8000021;
		private const int RMB_SEARCH_ENHPOLICYBILLING = 8000022;
        //Shruti Leave Plan Search
        private const int RMB_SEARCH_LEAVEPLAN = 8000023;
        //Anu Tennyson for MITS 18291 : STARTS - Added for Property Search 10/25/2009
        private const int RMB_SEARCH_PROPERTY = 8000012;
        //Anu Tennyson for MITS 18291 : ENDS
        //skhare7 Dairy search
        private const int RMB_SEARCH_DIARY = 8000024;
        private const int RMB_SEARCH_DRIVER = 8000025;
        //skhare7 Dairy search End
        //achouhan3     MITS#34276  Entity ID Type and Vendor Permission Starts
        private const int RMB_SEARCH_ENT_VENDORTYPE_VIEW = 1220400256;
        private const int RMB_SEARCH_ENT_IDTYPE_VIEW = 1220400251;
        //achouhan3     MITS#34276  Entity ID Type and Vendor Permission ends
        #endregion

        #region Private Member variables
        #endregion

		#region Constructor
		
		/// <summary>
		/// Default Class Constructor
		/// </summary>
		public SearchAdaptor()
		{
        }
		
		#endregion

		#region Public functions
		
			#region CreateSearchView
			#region Comments
			/// <param name="p_objXmlIn">XML containing the input parameters needed
			///		The structure of input XML document would be:
			///		<Document>
			///			<CreateSearchView ViewID="1" 
			///			GroupID="7" 
			///			TableRestrict="Claim" 
			///			SysEx="" 
			///			Settings="" 
			///			TableID="">
			///			</CreateSearchView>
			///		</Document>
			/// </param>

			/// <param name="p_objXmlOut">XML document containing Search screen data.
			///		The structure of output XML document would be:
			///		<Document>
			///		<search id="2" tablerestrict=" " dsnid="0" sys_ex=" " name="Standard Event Search" catid="2" xmlns="Riskmaster.Application.Search">
			///		<searchfields>
			///		<field fieldtype="1" id="FLD10000" issupp="0" table="EVENT" name="EVENT_NUMBER" type="text">Event Number</field>
			///		<field fieldtype="1" id="FLD10150" issupp="0" table="PI_ENTITY" name="LAST_NAME" type="text">PI Last Name</field>
			///		<field fieldtype="1" id="FLD10151" issupp="0" table="PI_ENTITY" name="FIRST_NAME" type="text">PI First Name</field>
			///		<field fieldtype="1" id="FLD10159" issupp="0" table="PI_ENTITY" name="TAX_ID" type="ssn">PI SSN/Tax ID</field>
			///		<field fieldtype="4" id="FLD10004" issupp="0" table="EVENT" name="DEPT_EID" type="orgh" level="0">Department</field>
			///		<field fieldtype="3" id="FLD10002" issupp="0" table="EVENT" name="EVENT_STATUS_CODE" codetable="EVENT_STATUS" type="code">Event Status</field>
			///		<field fieldtype="3" id="FLD10003" issupp="0" table="EVENT" name="EVENT_IND_CODE" codetable="EVENT_INDICATOR" type="code">Event Indicator</field>
			///		<field fieldtype="7" id="FLD10017" issupp="0" table="EVENT" name="DATE_OF_EVENT" type="date">Event/Accident Date</field>
			///		</searchfields>
			///		<displayfields>
			///		<field fieldtype="1" id="FLD10000" issupp="0" table="EVENT" name="EVENT_NUMBER" type="text">Event Number</field>
			///		<field fieldtype="1" id="FLD10150" issupp="0" table="PI_ENTITY" name="LAST_NAME" type="text">PI Last Name</field>
			///		<field fieldtype="1" id="FLD10151" issupp="0" table="PI_ENTITY" name="FIRST_NAME" type="text">PI First Name</field>
			///		<field fieldtype="1" id="FLD10159" issupp="0" table="PI_ENTITY" name="TAX_ID" type="ssn">PI SSN/Tax ID</field>
			///		<field fieldtype="7" id="FLD10017" issupp="0" table="EVENT" name="DATE_OF_EVENT" type="date">Event/Accident Date</field>
			///		<field fieldtype="3" id="FLD10002" issupp="0" table="EVENT" name="EVENT_STATUS_CODE" codetable="EVENT_STATUS" type="code">Event Status</field>
			///		<field fieldtype="3" id="FLD10003" issupp="0" table="EVENT" name="EVENT_IND_CODE" codetable="EVENT_INDICATOR" type="code">Event Indicator</field>
			///		<field fieldtype="4" id="FLD10004" issupp="0" table="EVENT" name="DEPT_EID" type="orgh" level="0">Department</field>
			///		</displayfields>
			///		<views>
			///		<view id="1006" name="3191Test" catid="2" />
			///		<view id="1081" name="test" catid="2" />
			///		</views>
			///		</search>
			///		</Document>
			/// </param>

			/// <summary>
			/// Creates the Search page for the given criteria
			/// </summary>
			/// <param name="p_objXmlIn">XML document containing input parameters for Search page creation</param>
			/// <param name="p_objXmlOut">XML document containing Search screen data.</param>
			/// <param name="p_objErrOut">Error object</param>
			/// <returns>true if operation is successful else false</returns>
			# endregion
			public bool SetUserPrefNode (XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
			{
				SearchResults objResults = null;
				try
				{
					objResults = new SearchResults(base.connectionString,base.ClientId);//sonali
                    // npadhy Jira 6415 Pass User login object to Search as well.
                    objResults.UserLogin = base.userLogin;
					objResults.SetUserPrefNode(p_objXmlIn,base.userID);
				}
				catch(RMAppException p_objException)
				{
					p_objErrOut.Add (p_objException, BusinessAdaptorErrorType.Error);
					return false;
				}
				catch(Exception p_objException)
				{
                    p_objErrOut.Add (p_objException,Globalization.GetString ("SearchAdaptor.SetUserPrefNode.Error",base.ClientId), BusinessAdaptorErrorType.Error);//sonali
					return false;
				}
				finally 
				{
					objResults = null;

				}
				return (true);
			}
		#endregion
		#region CreateSearchView
		#region Comments
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<Document>
		///			<CreateSearchView ViewID="1" 
		///			GroupID="7" 
		///			TableRestrict="Claim" 
		///			SysEx="" 
		///			Settings="" 
		///			TableID="">
		///			</CreateSearchView>
		///		</Document>
		/// </param>

		/// <param name="p_objXmlOut">XML document containing Search screen data.
		///		The structure of output XML document would be:
		///		<Document>
		///		<search id="2" tablerestrict=" " dsnid="0" sys_ex=" " name="Standard Event Search" catid="2" xmlns="Riskmaster.Application.Search">
		///		<searchfields>
		///		<field fieldtype="1" id="FLD10000" issupp="0" table="EVENT" name="EVENT_NUMBER" type="text">Event Number</field>
		///		<field fieldtype="1" id="FLD10150" issupp="0" table="PI_ENTITY" name="LAST_NAME" type="text">PI Last Name</field>
		///		<field fieldtype="1" id="FLD10151" issupp="0" table="PI_ENTITY" name="FIRST_NAME" type="text">PI First Name</field>
		///		<field fieldtype="1" id="FLD10159" issupp="0" table="PI_ENTITY" name="TAX_ID" type="ssn">PI SSN/Tax ID</field>
		///		<field fieldtype="4" id="FLD10004" issupp="0" table="EVENT" name="DEPT_EID" type="orgh" level="0">Department</field>
		///		<field fieldtype="3" id="FLD10002" issupp="0" table="EVENT" name="EVENT_STATUS_CODE" codetable="EVENT_STATUS" type="code">Event Status</field>
		///		<field fieldtype="3" id="FLD10003" issupp="0" table="EVENT" name="EVENT_IND_CODE" codetable="EVENT_INDICATOR" type="code">Event Indicator</field>
		///		<field fieldtype="7" id="FLD10017" issupp="0" table="EVENT" name="DATE_OF_EVENT" type="date">Event/Accident Date</field>
		///		</searchfields>
		///		<displayfields>
		///		<field fieldtype="1" id="FLD10000" issupp="0" table="EVENT" name="EVENT_NUMBER" type="text">Event Number</field>
		///		<field fieldtype="1" id="FLD10150" issupp="0" table="PI_ENTITY" name="LAST_NAME" type="text">PI Last Name</field>
		///		<field fieldtype="1" id="FLD10151" issupp="0" table="PI_ENTITY" name="FIRST_NAME" type="text">PI First Name</field>
		///		<field fieldtype="1" id="FLD10159" issupp="0" table="PI_ENTITY" name="TAX_ID" type="ssn">PI SSN/Tax ID</field>
		///		<field fieldtype="7" id="FLD10017" issupp="0" table="EVENT" name="DATE_OF_EVENT" type="date">Event/Accident Date</field>
		///		<field fieldtype="3" id="FLD10002" issupp="0" table="EVENT" name="EVENT_STATUS_CODE" codetable="EVENT_STATUS" type="code">Event Status</field>
		///		<field fieldtype="3" id="FLD10003" issupp="0" table="EVENT" name="EVENT_IND_CODE" codetable="EVENT_INDICATOR" type="code">Event Indicator</field>
		///		<field fieldtype="4" id="FLD10004" issupp="0" table="EVENT" name="DEPT_EID" type="orgh" level="0">Department</field>
		///		</displayfields>
		///		<views>
		///		<view id="1006" name="3191Test" catid="2" />
		///		<view id="1081" name="test" catid="2" />
		///		</views>
		///		</search>
		///		</Document>
		/// </param>

		/// <summary>
		/// Creates the Search page for the given criteria
		/// </summary>
		/// <param name="p_objXmlIn">XML document containing input parameters for Search page creation</param>
		/// <param name="p_objXmlOut">XML document containing Search screen data.</param>
		/// <param name="p_objErrOut">Error object</param>
		/// <returns>true if operation is successful else false</returns>
		# endregion
		public bool CreateSearchView (XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			int iViewID = 0;
			string sRestrict = "";
			string sFormName = "";
            //Start MITS 11128 by Parag
            string sSettoDefault = "";
            //End MITS 11128 by Parag
			XmlElement objXmlUser = null;
			SearchXML objResults = null;
            //Tushar - Start:MITS#18229
            string sFieldRestrict = string.Empty  ;
            int iFieldRestrictID = 0;
            bool bIsSucess = false;
            //string sUseFullentitySearch = string.Empty;//skhare7 JIRa 340         //avipinsrivas start : Worked for JIRA - 7767
            bool bHideGlobalSearch = true;     //avipinsrivas start : Worked for JIRA - 7767
            string sScreeFlag = string.Empty;
            //End
			//JIRA-RMA-1535 start
            string sLangCode = string.Empty;
            string sPageId = string.Empty;
            XElement oXmlUser = null;
            XElement xNode = null;
			//JIRA-RMA-1535 end
			try
			{
				objXmlUser = (XmlElement) p_objXmlIn.SelectSingleNode("//CreateSearchView");
				iViewID = Conversion.ConvertStrToInteger (objXmlUser.SelectSingleNode("//ViewID").InnerText);
				sRestrict = objXmlUser.SelectSingleNode("//TableRestrict").InnerText;
				sFormName = objXmlUser.SelectSingleNode("//FormName").InnerText;
                //Start MITS 11128 by Parag
                sSettoDefault = objXmlUser.SelectSingleNode("//SettoDefault").InnerText;
                //End MITS 11128 by Parag
                //Tushar - Start:MITS#18229
                if (objXmlUser.SelectSingleNode("//codefieldrestrict") != null)
                {
                    sFieldRestrict = objXmlUser.SelectSingleNode("//codefieldrestrict").InnerText;
                }

                if (objXmlUser.SelectSingleNode("//codefieldrestrictid") != null)
                {
                    iFieldRestrictID = Conversion.CastToType<int>(objXmlUser.SelectSingleNode("//codefieldrestrictid").InnerText, out bIsSucess);
                }
                //avipinsrivas start : Worked for JIRA - 7767
                //skhare7 JIRA 340 start
                //if (objXmlUser.SelectSingleNode("//UseFullEntitySearch") != null)
                //{
                //    sUseFullentitySearch = objXmlUser.SelectSingleNode("//UseFullEntitySearch").InnerText;
                //}
                if (objXmlUser.SelectSingleNode("//HideGlobalSearch") != null && !string.IsNullOrEmpty(objXmlUser.SelectSingleNode("//HideGlobalSearch").InnerText))
                    bHideGlobalSearch = Conversion.CastToType<bool>(objXmlUser.SelectSingleNode("//HideGlobalSearch").InnerText.Trim().ToLower(), out bIsSucess);
                //avipinsrivas end
                if (objXmlUser.SelectSingleNode("//screenflag") != null)
                {
                    sScreeFlag = objXmlUser.SelectSingleNode("//screenflag").InnerText;
                }
                
                  //skhare7 JIRA 340 end
                //End
				//JIRA-RMA-1535 start
                oXmlUser = XElement.Parse(objXmlUser.OuterXml);
                if (!object.ReferenceEquals(oXmlUser, default(XElement)))
                {
                    xNode = oXmlUser.XPathSelectElement("//LangId");
                    if (xNode != null)
                    {
                        sLangCode = xNode.Value.Trim();
                    }
                    xNode = oXmlUser.XPathSelectElement("//PageId");
                    if (xNode != null)
                    {
                        sPageId = xNode.Value.Trim();
                    }
                }
                
				//JIRA-RMA-1535 end
				//Checking for Security Permission
				switch (sFormName)
				{
					case "event":
						if(!userLogin.IsAllowedEx(RMB_SEARCH_EVENT))
							throw new PermissionViolationException(RMPermissions.RMO_SEARCH_EVENT,RMB_SEARCH_EVENT);
						break;
					case "claim":
					case "claimgc":
					case "claimwc":
					case "claimva":
					case "claimdi":
						if(!userLogin.IsAllowedEx(RMB_SEARCH_CLAIM))
							throw new PermissionViolationException(RMPermissions.RMO_SEARCH_CLAIM,RMB_SEARCH_CLAIM);
						break;
					case "piemployee":
					case "EMPLOYEES":
					case "employee":
						if(!userLogin.IsAllowedEx(RMB_SEARCH_EMPLOYEE))
							throw new PermissionViolationException(RMPermissions.RMO_SEARCH_EMPLOYEE,RMB_SEARCH_EMPLOYEE);
						break;
                    //MITS-10998 - Added two conditions for Other Persons and Witness Search
                    case "witness":
                        if (!userLogin.IsAllowedEx(RMB_SEARCH_PEOPLE_ENTITY))
                            throw new PermissionViolationException(RMPermissions.RMO_SEARCH_PEOPLE_WITNESS, RMB_SEARCH_PEOPLE_ENTITY);
                        break;
                    case "otherpeople":                    
                        if (!userLogin.IsAllowedEx(RMB_SEARCH_PEOPLE_ENTITY))
                            throw new PermissionViolationException(RMPermissions.RMO_SEARCH_PEOPLE_OTHER_PERSON, RMB_SEARCH_PEOPLE_ENTITY);
                        break;
                    //MITS-10998 End
                    case "entity":
                    case "people":
					case "entitymaint":
						if(!userLogin.IsAllowedEx(RMB_SEARCH_PEOPLE_ENTITY))
							throw new PermissionViolationException(RMPermissions.RMO_SEARCH_PEOPLE_ENTITY,RMB_SEARCH_PEOPLE_ENTITY);
						break;
					case "vehicle":
						if(!userLogin.IsAllowedEx(RMB_SEARCH_VEHICLE))
							throw new PermissionViolationException(RMPermissions.RMO_SEARCH_VEHICLE,RMB_SEARCH_VEHICLE);
						break;
					case "policy":
						if(!userLogin.IsAllowedEx(RMB_SEARCH_POLICY))
							throw new PermissionViolationException(RMPermissions.RMO_SEARCH_POLICY,RMB_SEARCH_POLICY);
						break;
					case "payment":
						if(!userLogin.IsAllowedEx(RMB_SEARCH_PAYMENT))
							throw new PermissionViolationException(RMPermissions.RMO_SEARCH_PAYMENT,RMB_SEARCH_PAYMENT);
						break;
					case "pipatient":
					case "PATIENTS":
					case "patient":
						if(!userLogin.IsAllowedEx(RMB_SEARCH_PATIENT))
							throw new PermissionViolationException(RMPermissions.RMO_SEARCH_PATIENT,RMB_SEARCH_PATIENT);
						break;
					case "PHYSICIANS":
					case "physician":
						if(!userLogin.IsAllowedEx(RMB_SEARCH_PHYSICIAN))
							throw new PermissionViolationException(RMPermissions.RMO_SEARCH_PHYSICIAN,RMB_SEARCH_PHYSICIAN);
						break;
					case "pimedstaff":
					case "medstaff":
					case "staff":
						if(!userLogin.IsAllowedEx(RMB_SEARCH_MEDSTAFF))
							throw new PermissionViolationException(RMPermissions.RMO_SEARCH_MED_STAFF,RMB_SEARCH_MEDSTAFF);
						break;
					case "disabilityplan":
                    case "dp": //Geeta 06/13/07 : MITS 9623
                    case "plan":                  
						if(!userLogin.IsAllowedEx(RMB_SEARCH_DISABILITYPLAN))
							throw new PermissionViolationException(RMPermissions.RMO_SEARCH_DISABILITY_PLAN,RMB_SEARCH_DISABILITYPLAN);
						break;
					case "admintracking":
						if(!userLogin.IsAllowedEx(RMB_SEARCH_ADMINTRACKING))
							throw new PermissionViolationException(RMPermissions.RMO_SEARCH_ADMIN_TRACKING,RMB_SEARCH_ADMINTRACKING);
						break;
                    // Start Naresh Enhanced Policy Search Changed the form name from enhpolicy to policyenh
					case "policyenh":
                    //Mridul. 12/07/09. MITS#:18229
                    case "policyenhal":
                    case "policyenhgl":
                    case "policyenhpc":
                    case "policyenhwc":
						if(!userLogin.IsAllowedEx(RMB_SEARCH_ENHPOLICY))
							throw new PermissionViolationException(RMPermissions.RMO_SEARCH_ENH_POLICY,RMB_SEARCH_ENHPOLICY);
						break;
                    // End Naresh Enhanced Policy Search Changed the form name from enhpolicy to policyenh
					case "enhpolicybilling":
						if(!userLogin.IsAllowedEx(RMB_SEARCH_ENHPOLICYBILLING))
							throw new PermissionViolationException(RMPermissions.RMO_SEARCH_ENH_POLICY_BILLING,RMB_SEARCH_ENHPOLICYBILLING);
						break;
                    //Shruti Leave Plan Search
                    case "leaveplan":
                        if (!userLogin.IsAllowedEx(RMB_SEARCH_LEAVEPLAN))
                            throw new PermissionViolationException(RMPermissions.RMO_SEARCH_LEAVE_PLAN, RMB_SEARCH_LEAVEPLAN);
                        break;
                    //Anu Tennyson for MITS 18291 : STARTS 10/25/2009
                    case "PROPERTYUNIT":  
                    case "propertyunit":
                        if (!userLogin.IsAllowedEx(RMB_SEARCH_PROPERTY))
                            throw new PermissionViolationException(RMPermissions.RMO_SEARCH_PROPERTY, RMB_SEARCH_PROPERTY);
                        break;
                        //skhare7
                    case "diary":
                        if (!userLogin.IsAllowedEx(RMB_SEARCH_DIARY))
                            throw new PermissionViolationException(RMPermissions.RMO_SEARCH_DIARY, RMB_SEARCH_DIARY);
                        break;
                    //Amandeep Driver Enhancement
                    
                    case "driver":
                        if (!userLogin.IsAllowedEx(RMB_SEARCH_DRIVER))
                            throw new PermissionViolationException(RMPermissions.RMO_SEARCH_DRIVER, RMB_SEARCH_DRIVER);
                        break;
                    //Anu Tennyson for MITS 18291 : ENDS
				}
                //pmittal5  MITS 11506  05/16/08  Implementing "Default search view" functionality
                //Start MITS 11128 by Parag
                //if (sSettoDefault == "true" || sSettoDefault == "1")
                //{
                    //SearchResults objUserPrefNode = new SearchResults(base.connectionString); 
                    //objUserPrefNode.SetUserPrefNode(p_objXmlIn, base.userID);
                    //objUserPrefNode = null;
                //}
                //End MITS 11128 by Parag
				//Initialize the application layer ReserveFunds class
                using (objResults = InitializeSearch(ref p_objXmlOut, ref p_objErrOut, false))
                {
                    //pmittal5  MITS 11506  05/16/08
                    //p_objXmlOut = objResults.CreateSearchView(iViewID, userID, groupID, sRestrict, sFormName, "");
                    //Tushar:MITS#18229
                    //Add sFieldRestricted 
                    p_objXmlOut = objResults.CreateSearchView(iViewID, userID, groupID, sRestrict, sFormName, "", sSettoDefault, sFieldRestrict, sLangCode, sPageId, sScreeFlag, bHideGlobalSearch); //JIRA-RMA-1535       //avipinsrivas start : Worked for JIRA - 7767
                    //End:MITS#18229
                }
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add (p_objException, BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add (p_objException,CommonFunctions.FilterBusinessMessage(Globalization.GetString ("SearchAdaptor.CreateSearchView.Error", base.ClientId),sLangCode), BusinessAdaptorErrorType.Error); //JIRA-RMA-1535 
				return false;
			}
			finally 
			{
				objResults = null;
				objXmlUser = null;
			}
			return (true);
		}
		#endregion

		#region GetSearchResultsForSelectedEntity
		/// <summary>
		/// Retrieves the record for passed in Entity-Id
		/// </summary>
		/// <param name="p_objXmlIn">XML document containing input parameters for Search page creation</param>
		/// <param name="p_objXmlOut">XML document containing Search screen data.</param>
		/// <param name="p_objErrOut">Error object</param>
		/// <returns>true if operation is successful else false</returns>
		public bool GetSearchResultsForSelectedEntity(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			int iEntityId = 0;
			string sType = "";
			int iLookUpType = 0;
            string sFormName = "";

			XmlElement objXmlUser = null;
			SearchXML objSearchXML = null;
			try
			{
				objXmlUser = (XmlElement) p_objXmlIn.SelectSingleNode("//GetEntityData");
				iEntityId = Conversion.ConvertStrToInteger (objXmlUser.SelectSingleNode("//EntityTableId").InnerText);
				sType = objXmlUser.SelectSingleNode("//Type").InnerText ;
				iLookUpType = Conversion.ConvertStrToInteger (objXmlUser.SelectSingleNode("//LookUpType").InnerText) ;
                sFormName = objXmlUser.SelectSingleNode("//FormName").InnerText;
				//Initialize the application layer ReserveFunds class
                using (objSearchXML = InitializeSearch(ref p_objXmlOut, ref p_objErrOut, true))
                {
                    p_objXmlOut = objSearchXML.GetSearchResultsForSelectedEntity(iEntityId, sType, iLookUpType, sFormName);
                }
			}
			catch (XmlOperationException p_objException)
			{
				p_objErrOut.Add (p_objException, BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add (p_objException, BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add (p_objException,Globalization.GetString ("SearchAdaptor.CreateSearchView.Error",base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally 
			{
				objSearchXML = null;
				objXmlUser = null;
			}
			return (true);
		}

		#endregion
        //skhare7 R8 MITS 26969
        #region AddNewEntity
        /// <summary>
        /// saves the New entity
        /// </summary>
        /// <param name="p_objXmlIn">XML document containing input parameters for Search page creation</param>
        /// <param name="p_objXmlOut">XML document containing Search screen data.</param>
        /// <param name="p_objErrOut">Error object</param>
        /// <returns>true if operation is successful else false</returns>
        public bool AddNewEntity(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
           
          
            string sFormName = "";
            string sLastName = String.Empty;
            // Start : MITS No : 29409 ngupta73
            string sFirstName = String.Empty;
            // End : MITS No : 29409 ngupta73

            XmlElement objXmlUser = null;
            SearchXML objSearchXML = null;
            try
            {
                objXmlUser = (XmlElement)p_objXmlIn.SelectSingleNode("//AddEntityData");
               
               
                sFormName = objXmlUser.SelectSingleNode("//FormName").InnerText;
                sLastName = objXmlUser.SelectSingleNode("//LastName").InnerText;
                // Start : MITS No : 29409 ngupta73
                sFirstName = objXmlUser.SelectSingleNode("//FirstName").InnerText;
                // End : MITS No : 29409 ngupta73
                //Initialize the application layer ReserveFunds class
                using (objSearchXML = InitializeSearch(ref p_objXmlOut, ref p_objErrOut, true))
                {
                    // Start : Changed By : ngupta73 MITS No : 29409 
                    p_objXmlOut = objSearchXML.AddNewEntity(sFormName, sLastName, sFirstName);
                    // End : Changed By : ngupta73 MITS No : 29409 
                }
            }
            catch (XmlOperationException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("SearchAdaptor.CreateSearchView.Error",base.ClientId), BusinessAdaptorErrorType.Error);//sonali
                return false;
            }
            finally
            {
                objSearchXML = null;
                objXmlUser = null;
            }
            return (true);
        }

        #endregion

		#region SearchRun
		#region Comments
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<search id="1" tablerestrict="claim" dsnid="0" sys_ex="" name="Standard Claim Search" catid="1" LookUpId="0" PageSize="10" MaxResults="10">
		///		<searchfields>
		///		<field fieldtype="1" id="FLD100" issupp="0" table="CLAIM" name="CLAIM_NUMBER" type="text">Claim Number</field>
		///		<field fieldtype="1" id="FLD1000" issupp="0" table="CLAIMANT_ENTITY" name="LAST_NAME" type="text">Claimant Last Name</field>
		///		<field fieldtype="1" id="FLD1001" issupp="0" table="CLAIMANT_ENTITY" name="FIRST_NAME" type="text">Claimant First Name</field>
		///		<field fieldtype="1" id="FLD1009" issupp="0" table="CLAIMANT_ENTITY" name="TAX_ID" type="ssn" operation="=">Claimant SSN/Tax ID</field>
		///		<field fieldtype="4" id="FLD304" issupp="0" table="EVENT" name="DEPT_EID" type="orgh" level="0">Department</field>
		///		<field fieldtype="3" id="FLD104" issupp="0" table="CLAIM" name="CLAIM_STATUS_CODE" codetable="CLAIM_STATUS" type="code">Claim Status</field>
		///		<field fieldtype="3" id="FLD103" issupp="0" table="CLAIM" name="CLAIM_TYPE_CODE" codetable="CLAIM_TYPE" type="code">Claim Type</field>
		///		<field fieldtype="7" id="FLD101" issupp="0" table="CLAIM" name="DATE_OF_CLAIM" type="date"><startdate></startdate><enddate></enddate></field>
		///		<field fieldtype="7" id="FLD317" issupp="0" table="EVENT" name="DATE_OF_EVENT" type="date"><startdate></startdate><enddate></enddate></field>
		///		<field fieldtype="3" id="FLD102" issupp="0" table="CLAIM" name="LINE_OF_BUS_CODE" codetable="LINE_OF_BUSINESS" type="code">Line of Business</field>
		///		<field fieldtype="1" id="FLD1030" issupp="0" table="CLAIMANT_ATTORNEY" name="LAST_NAME" type="text">Clmnt. Attorney Last Name</field>
		///		<field fieldtype="1" id="FLD1031" issupp="0" table="CLAIMANT_ATTORNEY" name="FIRST_NAME" type="text">Clmnt. Attorney First Name</field>
		///		<field fieldtype="1" id="FLD2500" issupp="0" table="DEFEND_ENTITY" name="LAST_NAME" type="text">Defendant Last Name</field>
		///		<field fieldtype="1" id="FLD2501" issupp="0" table="DEFEND_ENTITY" name="FIRST_NAME" type="text">Defendant First Name</field>
		///		<field fieldtype="1" id="FLD2550" issupp="0" table="DEFEND_ATTORNEY" name="LAST_NAME" type="text">Dfndt. Attorney Last Name</field>
		///		<field fieldtype="1" id="FLD2551" issupp="0" table="DEFEND_ATTORNEY" name="FIRST_NAME" type="text">Dfndt. Attorney First Name</field>
		///		<field fieldtype="1" id="FLD2000" issupp="0" table="ADJUSTER_ENTITY" name="LAST_NAME" type="text">Adjuster Last Name</field>
		///		<field fieldtype="1" id="FLD2001" issupp="0" table="ADJUSTER_ENTITY" name="FIRST_NAME" type="text">Adjuster First Name</field>
		///		<field fieldtype="1" id="FLD1561" issupp="0" table="EXPERT_ENTITY" name="FIRST_NAME" type="text">Expert First Name</field>
		///		<field fieldtype="1" id="FLD450" issupp="0" table="PI_ENTITY" name="LAST_NAME" type="text">PI Last Name</field>
		///		<field fieldtype="1" id="FLD451" issupp="0" table="PI_ENTITY" name="FIRST_NAME" type="text">PI First Name</field>
		///		<field fieldtype="1" id="FLD1530" issupp="0" table="LIT_ATTORNEY" name="LAST_NAME" type="text">Lit. Attorney Last Name</field>
		///		<field fieldtype="1" id="FLD1531" issupp="0" table="LIT_ATTORNEY" name="FIRST_NAME" type="text">Lit. Attorney First Name</field>
		///		<field fieldtype="6" id="FLDSP1875" issupp="-1" table="CLAIM_SUPP" name="CEO_NOTIFY_CODE" codetable="YES_NO" type="code">CEO Notified</field>
		///		<field fieldtype="6" id="FLDSP1875" issupp="-1" table="CLAIM_SUPP" name="CEO_NOTIFY_CODE" codetable="YES_NO" type="code">CEO Notified</field>
		///		<field fieldtype="6" id="FLDSP1875" issupp="-1" table="CLAIM_SUPP" name="CEO_NOTIFY_CODE" codetable="YES_NO" type="code">CEO Notified</field>
		///		</searchfields>
		///		<displayfields>
		///		<field fieldtype="1" id="FLD100" issupp="0" table="CLAIM" name="CLAIM_NUMBER" type="text">Claim Number</field>
		///		<field fieldtype="6" id="FLDSP1875" issupp="-1" table="CLAIM_SUPP" name="CEO_NOTIFY_CODE" codetable="YES_NO" type="code">CEO Notified</field>
		///		<field fieldtype="2" id="FLDSP1874" issupp="-1" table="CLAIM_SUPP" name="SPEC_COLLEC_AMT" type="currency">Special Collection</field>
		///		<field fieldtype="3" id="FLD316" issupp="0" table="EVENT" name="CAUSE_CODE" codetable="CAUSE_CODE" type="code">Cause Code</field>
		///		<field fieldtype="1" id="FLD300" issupp="0" table="EVENT" name="EVENT_NUMBER" type="text">Event Number</field>
		///		</displayfields>
		///		<views>
		///		<view id="1003" name="Claim Number Search" catid="1" />
		///		<view id="1004" name="Claim Claimant Search" catid="1" />
		///		<view id="1005" name="eo" catid="1" />
		///		<view id="1007" name="AdjusterNote" catid="1" />
		///		<view id="1008" name="test" catid="1" />
		///		<view id="1010" name="My Search" catid="1" />
		///		<view id="1050" name="Rupesh Search" catid="1" />
		///		<view id="1063" name="sdfsdfs" catid="1" />
		///		<view id="1071" name="asd" catid="1" />
		///		<view id="1074" name="sdf" catid="1" />
		///		<view id="1075" name="s" catid="1" />
		///		<view id="1076" name="TestBO" catid="1" />
		///		<view id="1079" name="Raj Claim Search " catid="1" />
		///		<view id="1082" name="Claim Search Home Office" catid="1" />
		///		<view id="1085" name="Claim Search Field Office" catid="1" />
		///		</views>
		///		</search>
		/// </param>

		/// <param name="p_objXmlOut">XML containing the output parameters needed
		/// <Document>
		///		<results searchcat="claim" totalrows="500" rowstart="1" rowend="500" resultscapped="1" pagesize="10" totalpages="50" sys_ex="" xmlns="Riskmaster.Application.Search">
		///		<columns>
		///		<column type="text">Claim Number</column>
		///		<column type="code">CEO Notified</column>
		///		<column type="currency">Special Collection</column>
		///		<column type="code">Cause Code</column>
		///		<column type="text">Event Number</column>
		///		</columns>
		///		<data>
		///		<row pid="197">
		///			 <field>WCSATYA123</field>
		///		<field> - </field>
		///		<field>$0.00</field>
		///		<field>FR - Fire</field>
		///		<field>EVT2003000182</field>
		///		</row>
		///		<row pid="1415">
		///			 <field>DFGDFG</field>
		///		<field> - </field>
		///		<field>$0.00</field>
		///		<field>AR. - Alcohol Related</field>
		///		<field>111111111111111111111</field>
		///		</row>
		///	</data>
		///	</results>
		///<Document>
	/// </param>

		/// <summary>
		/// Generates the Search data
		/// </summary>
		/// <param name="p_objXmlIn">XML document containing input parameters for the given Search criteria</param>
		/// <param name="p_objXmlOut">XML document containing Search related data</param>
		/// <param name="p_objErrOut">Error object</param>
		/// <returns>true if operation is successful else false</returns>
		# endregion
		public bool SearchRun (XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			long lLookUp = 0;
			int iPageSize = 0;
			long lMaxResults = 0;
			long lTotalRecords = 0;
			int iPageNo = 0;
			// Defect no. 1817: Implemented sorting of columns
			string sSortColumn = string.Empty, sOrder=string.Empty;

			XmlElement objXmlUser = null; 
			ResultList objResults = null;
            string sLangId=string.Empty; //JIRA-RMA-1535 
			try
			{
				objXmlUser = (XmlElement) p_objXmlIn.SelectSingleNode("//Search");
				lLookUp = Conversion.ConvertStrToLong (objXmlUser.SelectSingleNode("//LookUpId").InnerText);
				iPageSize = Conversion.ConvertStrToInteger(objXmlUser.SelectSingleNode("//PageSize").InnerText);
				lMaxResults = Conversion.ConvertStrToLong (objXmlUser.SelectSingleNode("//MaxResults").InnerText);
				lTotalRecords = Conversion.ConvertStrToLong (objXmlUser.SelectSingleNode("//TotalRecords").InnerText);
				sSortColumn = objXmlUser.SelectSingleNode("//SortColumn").InnerText;
				sOrder = objXmlUser.SelectSingleNode("//Order").InnerText;
				//JIRA-RMA-1535 start
                 if (objXmlUser.SelectSingleNode("//SearchMain/LangId") != null)
                {
                    sLangId = (objXmlUser.SelectSingleNode("//SearchMain/LangId")).InnerText.Trim();
                }
				//JIRA-RMA-1535 end
				if(objXmlUser.SelectSingleNode("PageNo").InnerText != "")
					iPageNo = Conversion.ConvertStrToInteger(objXmlUser.SelectSingleNode("//PageNo").InnerText);
				else
					iPageNo = 1;

				//Initialize the application layer ResultList class
				objResults = InitializeResultList(ref p_objXmlOut,ref p_objErrOut);
				
                //pmittal5 Confidential Record
                objResults.IsOrgSec = base.userLogin.objRiskmasterDatabase.OrgSecFlag;
                //gbhatnagar : Mobile Adjuster
                if (objXmlUser.SelectSingleNode("//caller") != null)
                {
                    if (objXmlUser.SelectSingleNode("//caller").InnerText == "MobileAdjuster")
                    {
                        objResults.IsMobileAdjuster = true;
                    }
                    //akaur9 : Mobile Adjuster
                    else if (objXmlUser.SelectSingleNode("//caller").InnerText == "MobileAdjusterClaimList")
                    {
                        objResults.IsMobileAdjuster = true;
                        objResults.IsMobileAdjusterClaimList = true;
                    }
                    //else if (objXmlUser.SelectSingleNode("//caller").InnerText == "MobileAdjusterClaimInfo")
                    //{
                    //    objResults.IsMobileAdjusterClaimInfo = true;
                    //}
                    else
                    {
                        objResults.IsMobileAdjuster = false;
                        // objResults.IsMobileAdjusterClaimInfo = false;
                        objResults.IsMobileAdjusterClaimList = false;
                    }

                    //rsharma220: Mobility Windows Adjuster
                    if (objXmlUser.SelectSingleNode("//caller").InnerText == "MobilityAdjuster")
                    {
                        objResults.IsMobilityAdjuster = true;
                    }
                    else if (objXmlUser.SelectSingleNode("//caller").InnerText == "MobilityAdjusterClaimList")
                    {
                        objResults.IsMobilityAdjuster = true;
                        objResults.IsMobilityAdjusterClaimList = true;
                    }
                    else
                    {
                        objResults.IsMobilityAdjuster = false;
                        objResults.IsMobilityAdjusterClaimList = false;
                    }
                    if ((objXmlUser.SelectSingleNode("//caller") != null) && (objXmlUser.SelectSingleNode("//caller").InnerText == "MobilityAdjusterEventList"))
                    {
                        objResults.IsMobilityAdjusterEventList = true;
                    }
                    else
                    {
                        objResults.IsMobilityAdjusterEventList = false;
                    }
                }
                //Pass in the existing Session ID rather than a new Session ID
                p_objXmlOut = objResults.SearchRun(userID, groupID, lLookUp, iPageNo, iPageSize, lMaxResults, lTotalRecords, p_objXmlIn, sSortColumn, sOrder, userLogin.LoginName);
                //achouhan3     MITS#34276      Entity ID Type and ID Type Vendor view permission
                XmlNode xResult = p_objXmlOut.SelectSingleNode("//results");
                var searchEntityCat = xResult.Attributes["searchcat"];
                if (searchEntityCat != null && String.Compare(searchEntityCat.Value, "entity", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    XmlAttribute objXmlPermissionAttribute = null;
                    XmlElement objRootElement = p_objXmlOut.CreateElement("results");
                    if (!userLogin.IsAllowedEx(RMB_SEARCH_ENT_IDTYPE_VIEW))
                    {

                        objXmlPermissionAttribute = p_objXmlOut.CreateAttribute("isentidtypeview");
                        objXmlPermissionAttribute.InnerText = "false";
                        p_objXmlOut.DocumentElement.SetAttributeNode(objXmlPermissionAttribute);
                        objXmlPermissionAttribute = null;
                    }
                    if (!userLogin.IsAllowedEx(RMB_SEARCH_ENT_VENDORTYPE_VIEW))
                    {
                        objXmlPermissionAttribute = p_objXmlOut.CreateAttribute("isidtypevendorview");
                        objXmlPermissionAttribute.InnerText = "false";
                        p_objXmlOut.DocumentElement.SetAttributeNode(objXmlPermissionAttribute);
                        objXmlPermissionAttribute = null;
                    }
                }

			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add (p_objException, BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add (p_objException,CommonFunctions.FilterBusinessMessage(Globalization.GetString ("SearchAdaptor.SearchRun.Error", base.ClientId),sLangId), BusinessAdaptorErrorType.Error);//JIRA-RMA-1535 
				return false;
			}
			finally 
			{
				objXmlUser = null;
				objResults = null;
			}
			return (true);
		}


        //MITS 10651 Abhishek
        /// <summary>
        /// Prints the searched results
        /// </summary>
        /// <param name="p_objXmlIn">intput xml</param>
        /// <param name="p_objXmlOut">xml cotaining pdf in binary form</param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool PrintSearch(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            long lLookUp = 0;
            int iPageSize = 0;
            long lMaxResults = 0;
            long lTotalRecords = 0;
            int iPageNo = 0;
            // Defect no. 1817: Implemented sorting of columns
            string sSortColumn = "";
            string sOrder = "";
            MemoryStream objMemory = null;
            XmlElement objXmlUser = null;
            ResultList objResults = null;
            XmlElement objTempElement = null;
            try
            {
                objXmlUser = (XmlElement)p_objXmlIn.SelectSingleNode("//Search");
                lLookUp = Conversion.ConvertStrToLong(objXmlUser.SelectSingleNode("//LookUpId").InnerText);
                iPageSize = Conversion.ConvertStrToInteger(objXmlUser.SelectSingleNode("//PageSize").InnerText);
                lMaxResults = Conversion.ConvertStrToLong(objXmlUser.SelectSingleNode("//MaxResults").InnerText);
                lTotalRecords = Conversion.ConvertStrToLong(objXmlUser.SelectSingleNode("//TotalRecords").InnerText);
                sSortColumn = objXmlUser.SelectSingleNode("//SortColumn").InnerText;
                sOrder = objXmlUser.SelectSingleNode("//Order").InnerText;

                if (objXmlUser.SelectSingleNode("PageNo").InnerText != "")
                    iPageNo = Conversion.ConvertStrToInteger(objXmlUser.SelectSingleNode("//PageNo").InnerText);
                else
                    iPageNo = 1;

                //Initialize the application layer ResultList class
                objResults = InitializeResultList(ref p_objXmlOut, ref p_objErrOut);

                objMemory = objResults.PrintSearch(userID, groupID, lLookUp, iPageNo, iPageSize, lMaxResults, lTotalRecords, p_objXmlIn, sSortColumn, sOrder, userLogin.LoginName, ref p_objXmlOut);

                objTempElement = p_objXmlOut.CreateElement("File");
                objTempElement.SetAttribute("Filename", Path.GetFileName(Path.GetTempFileName()).Replace(".tmp", ".pdf"));
                objTempElement.InnerText = Convert.ToBase64String(objMemory.ToArray());
                p_objXmlOut.FirstChild.AppendChild(objTempElement);
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("SearchAdaptor.PrintSearch.Error",base.ClientId), BusinessAdaptorErrorType.Error);//sonali
                return false;
            }
            finally
            {
                objXmlUser = null;
                objResults = null;
            }
            return true;
        }
        //MITS 10651 end

		#endregion

        #region User Preferences
        //RMA-345 Start     Achouhan3   Methods tp get/set User preferences
        /// </summary>
        /// <param name="p_objXmlIn">XmlDocument</param>
        /// <param name="p_objXmlOut">XmlDocument</param>
        /// <param name="p_objErr">BusinessAdaptorErrors</param>
        /// <returns>bool</returns>
        public bool SetUserPrefSearch(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            ResultList objResultList = null;

            try
            {
                objResultList = new ResultList(connectionString, m_userLogin, base.ClientId);
                objResultList.SetUserPrefSearch(p_objXmlIn);
            }
            catch (Exception p_objException)
            {
                p_objErr.Add(p_objException, Globalization.GetString("ExecutiveSummary.SaveUserPreference.Err", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objResultList = null;
            }
            return true;
        }
        /// </summary>
        /// <param name="p_objXmlIn">XmlDocument</param>
        /// <param name="p_objXmlOut">XmlDocument</param>
        /// <param name="p_objErr">BusinessAdaptorErrors</param>
        /// <returns>bool</returns>
        public bool GetUserPrefSearchXML(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            ResultList objResultList = null;

            try
            {
                objResultList = new ResultList(connectionString, m_userLogin, base.ClientId);
                objResultList.GetUserPrefSearchXML(p_objXmlIn, ref p_objXmlOut);
            }
            catch (Exception p_objException)
            {
                p_objErr.Add(p_objException, Globalization.GetString("ExecutiveSummary.GetUserPreference.Err", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objResultList = null;
            }
            return true;
        }
        //34278 End
        #endregion

		#endregion

		#region Private functions

		#region InitializeSearch
		/// <summary>
		/// This function will instantiate the application layer's Search class.
		/// </summary>
		/// <param name="p_objXmlDoc">Output xml document.</param>
		/// <param name="p_objErr">Error collection.</param>
		/// <param name="p_bDmfFlag">Flag to see if Datamodel object needs to be created or not.</param>
		/// <returns>Instance of application layer's ReserveFunds class.</returns>
		private SearchXML InitializeSearch(ref XmlDocument p_objXmlDoc,ref BusinessAdaptorErrors p_objErr, bool p_bDmfFlag)
		{
			SearchXML objSearch=null;
			DataModelFactory objDmf = null;
			try
			{
				if(p_bDmfFlag)
				{
					objDmf = new DataModelFactory(base.userLogin,base.ClientId); //Ash - cloud
					objSearch = new SearchXML(objDmf);
				}
				else
					objSearch=new SearchXML(base.connectionString, base.ClientId); //Ash - cloud
				
				p_objXmlDoc=new XmlDocument();
				p_objXmlDoc.AppendChild(p_objXmlDoc.CreateElement("Search"));
				p_objErr=new BusinessAdaptorErrors(base.ClientId);
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
				// JP 11.7.2005   if(objDmf != null)
				// JP 11.7.2005   	objDmf.Dispose();
			}
			
			return objSearch;
		}
		
		#endregion

		#region InitializeResultList

		/// <summary>
		/// This function will instantiate the application layer's ResultList class.
		/// </summary>
		/// <param name="p_objInputDoc">Input xml containing the various parameters for search class instantiation.</param>
		/// <param name="p_objXmlDoc">Output xml document.</param>
		/// <param name="p_objErr">Error collection.</param>
		/// <returns>Instance of application layer's ReserveFunds class.</returns>
		private ResultList InitializeResultList(ref XmlDocument p_objXmlDoc,ref BusinessAdaptorErrors p_objErr)
		{
			ResultList objResultList=null;
			try
			{
				//objResultList=new ResultList(base.connectionString, m_userLogin);
                objResultList = new ResultList(base.connectionString, m_userLogin, base.ClientId);
				p_objXmlDoc=new XmlDocument();
				p_objXmlDoc.AppendChild(p_objXmlDoc.CreateElement("Search"));
				p_objErr=new BusinessAdaptorErrors(base.ClientId);
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			return objResultList;
		}
		
		#endregion

		#endregion
	}
}