using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.Application.ClaimHistory;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using System.IO;
using Riskmaster.Db;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class ClaimHistoryAdaptor: BusinessAdaptorBase
    {

        #region Private variables
        
        /// <summary>		
        /// Database connection string.
        /// </summary>
        private string m_sConnectionString = "";
        
        #endregion


        public ClaimHistoryAdaptor()
		{
		}


		#region Public Methods
		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.ClaimHistory.GetClaimHistory() method.
		///		Gets the Claim History list.
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<Document>
		///			<ClaimHistory>
		///				<ClaimantEId>Claimant Id</ClaimantEId>
		///				<DefendantEId>Defendant Id</DefendantEId>
		///				<UnitId>Unit Id</UnitId>
		///				<EventId>Event Id</EventId>
		///				<EventNumber>Event Number</EventNumber>
		///			</ClaimHistory>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be
		/// </param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		public bool GetClaimHistory(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			ClaimHistory objClaimHistory=null;		//Application layer component
			int iClaimantEId;						//Claimant ID
			int iDefendantEId;						//Defendant Id
			int iUnitId;							//Unit Id
			int iEventId;							//Event Id
			int iEventNumber;						//Event Number
            // Ayush: MITS:18230, 01/22/2010. Start - Property Id
            int iPropertyId = 0;
            bool blnSuccess = false;
            // Ayush: MITS:18230, 01/22/2010 End
			XmlElement objElement=null;				//used for parsing the input xml

			try
			{
				//check existence of Claimant Id which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ClaimantEId");
				if (objElement==null)
				{
                    p_objErrOut.Add("ClaimHistoryAdaptorParameterMissing", Globalization.GetString("ClaimHistoryAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				iClaimantEId=Conversion.ConvertStrToInteger(objElement.InnerText);

				//check existence of Defendant Id which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//DefendantEId");
				if (objElement==null)
				{
                    p_objErrOut.Add("ClaimHistoryAdaptorParameterMissing", Globalization.GetString("ClaimHistoryAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				iDefendantEId=Conversion.ConvertStrToInteger(objElement.InnerText);
				
				//check existence of Unit Id which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//UnitId");
				if (objElement==null)
				{
                    p_objErrOut.Add("ClaimHistoryAdaptorParameterMissing", Globalization.GetString("ClaimHistoryAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				iUnitId=Conversion.ConvertStrToInteger(objElement.InnerText);

				//check existence of Event Id which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//EventId");
				if (objElement==null)
				{
                    p_objErrOut.Add("ClaimHistoryAdaptorParameterMissing", Globalization.GetString("ClaimHistoryAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				iEventId=Conversion.ConvertStrToInteger(objElement.InnerText);

				//check existence of Event Number which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//EventNumber");
				if (objElement==null)
				{
                    p_objErrOut.Add("ClaimHistoryAdaptorParameterMissing", Globalization.GetString("ClaimHistoryAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				iEventNumber=Conversion.ConvertStrToInteger(objElement.InnerText);

                // Ayush: MITS:18230, 01/22/2010 Start:
                //check existence of Property Id which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//PropertyId");
                if (objElement == null)
                {
                    p_objErrOut.Add("ClaimHistoryAdaptorParameterMissing", Globalization.GetString("ClaimHistoryAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                iPropertyId = Conversion.CastToType<int>(objElement.InnerText, out blnSuccess);
                // Ayush: MITS:18230, 01/22/2010 End

				objClaimHistory = new ClaimHistory(base.userLogin.objRiskmasterDatabase.DataSourceName,base.userLogin.LoginName,base.userLogin.Password,base.ClientId);//sonali jira-882

				//get Claim History
                p_objXmlOut = objClaimHistory.GetClaimHistory(iClaimantEId, iDefendantEId, iUnitId, iEventId, iEventNumber, iPropertyId);
				
				return true;
			}
			catch(InvalidValueException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("ClaimHistoryAdaptor.GetClaimHistory.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sonali jira 882
				return false;
			}
			finally
			{
				objClaimHistory = null;
				objElement=null;
			}
		}

		
		/// <summary>
		/// Wrapper to Application layer function GetCustomPaymentNotification()
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GetClaimStatusHistory(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			ClaimStatusHistory objClmStsHis = null;
			try
			{
				objClmStsHis = new ClaimStatusHistory(userLogin,userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, base.ClientId);//vkumar258 ML Changes
				p_objXmlOut = objClmStsHis.GetClaimStatusHistory(p_objXmlIn);
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("ClaimHistoryAdaptor.GetClaimStatusHistory.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objClmStsHis = null;
			}
			return true;
		}

        /// <summary>
        /// Added by Ashutosh Kashyap on 9th mar 09 for EDI History functionality.
        /// </summary>
        /// <param name="p_objXmlIn">Input XML</param>
        /// <param name="p_objXmlOut">Output XML</param>
        /// <param name="p_objErrOut">Error string</param>
        /// <returns></returns>
        public bool GetEDIHistory(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            ClaimHistory objClaimHistory = null;
            XmlElement objXMLElement = null;
            try
            {
                int iClaimId = 0;

                //check existence of Claim Id which is required
                objXMLElement = (XmlElement)p_objXmlIn.SelectSingleNode("//ClaimId");
                if (objXMLElement == null)
                {
                    p_objErrOut.Add("ClaimHistoryAdaptorParameterMissing", Globalization.GetString("ClaimHistoryAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                iClaimId = Conversion.ConvertStrToInteger(objXMLElement.InnerText);
                objClaimHistory = new ClaimHistory(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password,base.ClientId);//sonali jira-882
                //get Claim History
                p_objXmlOut = objClaimHistory.GetEDIHistory(iClaimId);

                return true;
            }
            catch (InvalidValueException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ClaimHistory.GetEDIHistory.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objClaimHistory = null;
                objXMLElement = null;
            }

            return true;
        }

        #region Get new element
        /// Name		: GetNewElement
        /// Author		: Sumeet Rathod
        /// Date Created: 11/29/2004		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Creats a new XMLElement with the nodename passed as parameter
        /// </summary>
        /// <param name="p_sNodeName">Node Name</param>
        /// <returns>XML element to be added in the DOM</returns>
        private XmlElement GetNewElement(string p_sNodeName)
        {
            XmlDocument objXDoc;
            XmlElement objXMLElement;

            try
            {
                objXDoc = new XmlDocument();
                objXMLElement = objXDoc.CreateElement(p_sNodeName);
                return objXMLElement;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ClaimHistory.GetNewElement.XMLError",base.ClientId), p_objException);
            }
            finally
            {
                objXDoc = null;
                objXMLElement = null;
            }
        }
        #endregion

        /// Name		: GetNewEleWithValue
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Creats a new XMLElement with the nodename passed as parameter. 
        /// Also sets the Innertext to the new XMLElement
        /// </summary>
        /// <param name="p_sNewNodeName">New Node to be Created</param>
        /// <param name="p_sNodeValue">Value for the node</param>
        /// <returns>New XML Element with Innertext set</returns>
        internal static XmlElement GetNewEleWithValue(string p_sNewNodeName, string p_sNodeValue)
        {
            XmlDocument objXDoc;
            XmlElement objXMLElement;
            objXDoc = new XmlDocument();
            objXMLElement = objXDoc.CreateElement(p_sNewNodeName);
            objXMLElement.InnerText = p_sNodeValue;
            objXDoc = null;
            return objXMLElement;
        }
        
        #endregion
	}
}
