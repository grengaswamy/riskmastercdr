using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.Application.AltWaitPeriods;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: AltWaitPeriodsAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 08/27/2005
	///* $Author	: Mihika Agrawal
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************

	/// <summary>
	/// This class is used to call the application layer component for AltWaitPeriods
	/// </summary>
	public class AltWaitPeriodsAdaptor: BusinessAdaptorBase
	{
		public AltWaitPeriodsAdaptor()
		{
		}

		#region Public Methods

		/// <summary>
		/// Wrapper to Application layer function GetListAltWait()
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GetListAltWait(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			AltWait objAltWait = null;
			try
			{
				objAltWait = new AltWait(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password,base.ClientId);
				p_objXmlOut = objAltWait.GetListAltWait(p_objXmlIn);
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("AltWaitPeriodsAdaptor.GetListAltWait.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
                if (objAltWait != null)
                {
                    objAltWait.Dispose();
                    objAltWait = null;
                }
			}
			return true;
		}


		/// <summary>
		/// Wrapper to Application layer function GetRecordAltWait()
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GetRecordAltWait(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			AltWait objAltWait = null;
			try
			{
				objAltWait = new AltWait(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password,base.ClientId);
				p_objXmlOut = objAltWait.GetRecordAltWait(p_objXmlIn);
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("AltWaitPeriodsAdaptor.GetRecordAltWait.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
                if (objAltWait != null)
                {
                    objAltWait.Dispose();
                    objAltWait = null;
                }
			}
			return true;
		}


		/// <summary>
		/// Wrapper to Application layer function Save()
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Save(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			AltWait objAltWait = null;
			try
			{
				objAltWait = new AltWait(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password,base.ClientId);
				objAltWait.Save(p_objXmlIn);
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("AltWaitPeriodsAdaptor.Save.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
                if (objAltWait != null)
                {
                    objAltWait.Dispose();
                    objAltWait = null;
                }
			}
			return true;
		}


		/// <summary>
		/// Wrapper to Application layer function Delete()
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Delete(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			AltWait objAltWait = null;
			int iDisWaitRowId = 0;
			int iClassRowId = 0;
			try
			{
				objAltWait = new AltWait(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password,base.ClientId);
				iDisWaitRowId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//DisWaitRowId").InnerText);
				iClassRowId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//ClassRowId").InnerText);
				objAltWait.Delete(iDisWaitRowId, iClassRowId);
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("AltWaitPeriodsAdaptor.Delete.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
                if (objAltWait != null)
                {
                    objAltWait.Dispose();
                    objAltWait = null;
                }
			}
			return true;
		}


		#endregion
	}
}
