﻿
using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Application.AltWaitPeriods;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using System.IO;
using Riskmaster.Security;

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: DisClassTdManagerAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 29-Aug-2005
	///* $Author	: Rahul Sharma
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class performs the operations related to Disablity Class Table Data.
	/// </summary>
	public class DisClassTdManagerAdaptor: BusinessAdaptorBase
	{
		#region Constructor
		public DisClassTdManagerAdaptor()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		#endregion

		#region Public Functions
		public bool OnLoad(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.AltWaitPeriods.DisClassTdManager objDisClassTdManager=null; //Application layer component
			
			try
			{
				XmlElement objElement=null;		//used for parsing the input xml
				string sReturnXml=string.Empty; //used for holding the output xml string
				string sInputXml=string.Empty;	//XML to be populated with the Report Admin data
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ClassId");
				if (objElement==null)
				{
					try
					{
						p_objErrOut.Add("DisClassTdManagerParameterMissing",Globalization.GetString("DisClassTdManagerParameterMissing",base.ClientId),BusinessAdaptorErrorType.Error);
					}
					catch(Exception ex)
					{
						string ss= ex.Message;
					}
					return false;
				}
				sInputXml=objElement.InnerXml;

				objDisClassTdManager = new Riskmaster.Application.AltWaitPeriods.DisClassTdManager(base.userLogin,base.ClientId);
				p_objXmlOut=objDisClassTdManager.OnLoad(Conversion.ConvertStrToInteger(sInputXml));
				//create the output xml by including the Output parameter also
				sReturnXml="<ReturnXml><OutputResult>Success</OutputResult></ReturnXml>";
				//p_objXmlOut.LoadXml(sReturnXml);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("ReportAdmin.GetScheduleDetail.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
                if (objDisClassTdManager != null)
                {
                    objDisClassTdManager.Dispose();
                }
			}
		}
		public bool GetBenefitData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.AltWaitPeriods.DisClassTdManager objDisClassTdManager=null; //Application layer component
			
			try
			{
				XmlElement objElement=null;		//used for parsing the input xml
				string sReturnXml=string.Empty; //used for holding the output xml string
				string sInputXml=string.Empty;	//XML to be populated with the Report Admin data
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//rowid");
				if (objElement==null)
				{
					try
					{
						p_objErrOut.Add("DisClassTdManagerParameterMissing",Globalization.GetString("DisClassTdManagerParameterMissing",base.ClientId),BusinessAdaptorErrorType.Error);
					}
					catch(Exception ex)
					{
						string ss= ex.Message;
					}
					return false;
				}
				sInputXml=objElement.InnerXml;

				objDisClassTdManager = new Riskmaster.Application.AltWaitPeriods.DisClassTdManager(base.userLogin,base.ClientId);
				p_objXmlOut=objDisClassTdManager.GetBenefitData(Conversion.ConvertStrToInteger(sInputXml));
				//create the output xml by including the Output parameter also
				sReturnXml="<ReturnXml><OutputResult>Success</OutputResult></ReturnXml>";
				//p_objXmlOut.LoadXml(sReturnXml);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("ReportAdmin.GetScheduleDetail.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
                if (objDisClassTdManager != null)
                {
                    objDisClassTdManager.Dispose();
                }
			}
		}
		public bool SaveBenefitTableData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.AltWaitPeriods.DisClassTdManager objDisClassTdManager=null; //Application layer component
			
			try
			{
				XmlElement objElement=null;		//used for parsing the input xml
				string sReturnXml=string.Empty; //used for holding the output xml string
				string sInputXml=string.Empty;	//XML to be populated with the Input XML
				string sRowId=string.Empty; //Row Id to be populated with the XML;
				string sClassId=string.Empty;
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//BenefitCalcTable");
				if (objElement==null)
				{
					try
					{
						p_objErrOut.Add("DisClassTdManagerParameterMissing",Globalization.GetString("DisClassTdManagerParameterMissing",base.ClientId),BusinessAdaptorErrorType.Error);
					}
					catch(Exception ex)
					{
						string ss= ex.Message;
					}
					return false;
				}
				sInputXml=objElement.InnerXml;
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//rowid");
				if (objElement==null)
				{
					try
					{
						p_objErrOut.Add("DisClassTdManagerParameterMissing",Globalization.GetString("DisClassTdManagerParameterMissing",base.ClientId),BusinessAdaptorErrorType.Error);
					}
					catch(Exception ex)
					{
						string ss= ex.Message;
					}
					return false;
				}
				sRowId=objElement.InnerXml;
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//CLASS_ID");
				if (objElement==null)
				{
					try
					{
						p_objErrOut.Add("DisClassTdManagerParameterMissing",Globalization.GetString("DisClassTdManagerParameterMissing",base.ClientId),BusinessAdaptorErrorType.Error);
					}
					catch(Exception ex)
					{
						string ss= ex.Message;
					}
					return false;
				}
				sClassId=objElement.InnerXml;
				objDisClassTdManager = new Riskmaster.Application.AltWaitPeriods.DisClassTdManager(base.userLogin,base.ClientId);
				//p_objXmlOut=;
				//create the output xml by including the Output parameter also
				sReturnXml=objDisClassTdManager.SaveBenefitTableData(sInputXml,Conversion.ConvertStrToInteger(sRowId),Conversion.ConvertObjToInt(sClassId, base.ClientId));
				//p_objXmlOut.LoadXml(sReturnXml);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("ReportAdmin.GetScheduleDetail.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
                if (objDisClassTdManager != null)
                {
                    objDisClassTdManager.Dispose();
                }
			}
		}
		public bool DeleteRowData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.AltWaitPeriods.DisClassTdManager objDisClassTdManager=null; //Application layer component
			
			try
			{
				XmlElement objElement=null;		//used for parsing the input xml
				string sReturnXml=string.Empty; //used for holding the output xml string
				string sRowId=string.Empty; //Row Id to be populated with the XML;				
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//rowid");
				if (objElement==null)
				{
					try
					{
						p_objErrOut.Add("DisClassTdManagerParameterMissing",Globalization.GetString("DisClassTdManagerParameterMissing",base.ClientId),BusinessAdaptorErrorType.Error);
					}
					catch(Exception ex)
					{
						string ss= ex.Message;
					}
					return false;
				}
				sRowId=objElement.InnerXml;				
				objDisClassTdManager = new Riskmaster.Application.AltWaitPeriods.DisClassTdManager(base.userLogin,base.ClientId);
				//create the output xml by including the Output parameter also
				sReturnXml=objDisClassTdManager.DeleteRowData(Conversion.ConvertStrToInteger(sRowId));
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("DisClassTdManager.DeleteRowData.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
                if (objDisClassTdManager != null)
                {
                    objDisClassTdManager.Dispose();
                }
			}
		}
		#endregion
	}
}
