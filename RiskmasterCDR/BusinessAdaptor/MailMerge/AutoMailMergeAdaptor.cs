﻿using System;
using System.Text;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.Application.DocumentManagement;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.MailMerge;
using Riskmaster.Application.FileStorage;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Settings;

namespace Riskmaster.BusinessAdaptor
{
    ///************************************************************** 
    ///* $File		: AutoMailMergeAdaptor.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 8-Aug-2012
    ///* $Author	: Nitika Gupta
    ///* $Comment	: 
    ///* $Source	: 
    ///**************************************************************

    /// <summary>	
    ///	This class is used to call the application layer component for Mail Merge which 
    ///	contains methods to generate merged documents.	
    /// </summary>
    public class AutoMailMergeAdaptor : BusinessAdaptorBase
    {
        #region Constructor
		/// <summary>
		/// Default Constructor
		/// </summary>
        public AutoMailMergeAdaptor() { }

		#endregion
        #region Public Methods


        /// <summary>
        ///		This method returns available auto mail merge definitions .		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.WPAAutoDiary.xml
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool GetMergeDefList(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            AutoMailMerge objAuto = null; //Application layer component
            try
            {
                objAuto = new AutoMailMerge(userLogin.LoginName, userLogin.Password, userLogin.objRiskmasterDatabase.DataSourceName,base.ClientId);//sonali
                p_objXmlOut = objAuto.GetMergeDefList(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("AutoMailMergeAdaptor.GetMergeDefList.Error",base.ClientId), BusinessAdaptorErrorType.Error);//sonali
                return false;
            }
            finally
            {
                objAuto = null;
            }
        }

        public bool GetFilterData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            AutoMailMerge objAuto = null; //Application layer component
            try
            {
                XmlNode DatabaseNode = null;
                DatabaseNode = p_objXmlIn.SelectSingleNode("WPAAutoMailMergeFilterSetup/Database");
                objAuto = new AutoMailMerge(userLogin.objRiskmasterDatabase.ConnectionString,base.ClientId);//sonali
                p_objXmlOut = objAuto.GetFilterData(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("AutoMailMergeAdaptor.GetFilterData.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sonali
                return false;
            }
            finally
            {
                objAuto = null;
            }
        }

        /// <summary>
        /// This function will bring all the People Involved Details .
        /// </summary>
        /// <param name="objXmlIn">Input XML which contains the information</param>
        /// <param name="objXmlOut">Output XML which will take the Person Involved to the UI layer</param>
        /// <param name="objErrOut">XML which will contain the Error.</param>
        /// <returns>Returns true or false depending upon the success.</returns>
        public bool GetPeopleInvolved(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            AutoMailMerge objAuto = null; //Application layer component
            try
            {
                objAuto = new AutoMailMerge(userLogin.LoginName, userLogin.Password, userLogin.objRiskmasterDatabase.DataSourceName,base.ClientId);//sonali
                p_objXmlOut = objAuto.GetPeopleInvolved(p_objXmlIn);
                return true;
            }
            catch (RMAppException objException)
            {
                p_objErrOut.Add(objException, BusinessAdaptorErrorType.PopupMessage);
                return false;
            }
            catch (Exception objException)
            {
                p_objErrOut.Add(objException, Globalization.GetString("AutoMailMergeAdaptor.GetPeopleInvolved.Error",base.ClientId), BusinessAdaptorErrorType.Error);//sonali
                return false;
            }
            finally
            {
                objAuto = null;
            }
        }

        public bool GetAdditionalFilterData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            AutoMailMerge objAuto = null; //Application layer component
            try
            {
                objAuto = new AutoMailMerge(userLogin.objRiskmasterDatabase.ConnectionString,base.ClientId);//sonali
                p_objXmlOut = objAuto.GetAdditionalFilterData(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("AutoMailMergeAdaptor.GetAdditionalFilterData.Error",base.ClientId), BusinessAdaptorErrorType.Error);//sonali
                return false;
            }
            finally
            {
                objAuto = null;
            }
        }

        public bool GetEntityRoleList(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            AutoMailMerge objAuto = null; //Application layer component
            try
            {
                objAuto = new AutoMailMerge(userLogin.objRiskmasterDatabase.ConnectionString,base.ClientId);//sonali
                p_objXmlOut = objAuto.GetEntityRoleList(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("AutoMailMergeAdaptor.GetEntityRoleList.Error",base.ClientId), BusinessAdaptorErrorType.Error);//sonali
                return false;
            }
            finally
            {
                objAuto = null;
            }
        }

        public bool GetPeopleInvolvedList(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            AutoMailMerge objAuto = null; //Application layer component
            try
            {
                objAuto = new AutoMailMerge(userLogin.objRiskmasterDatabase.ConnectionString, base.ClientId);//sonali
                p_objXmlOut = objAuto.GetPeopleInvolvedList(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("AutoMailMergeAdaptor.GetPeopleInvolvedList.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sonail
                return false;
            }
            finally
            {
                objAuto = null;
            }
        }

        public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            AutoMailMerge objAuto = null; //Application layer component
            try
            {
                objAuto = new AutoMailMerge(userLogin.objRiskmasterDatabase.ConnectionString, base.ClientId);//sonali
                p_objXmlOut = objAuto.Save(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("AutoMailMergeAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sonai
                return false;
            }
            finally
            {
                objAuto = null;
            }
        }

        public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            AutoMailMerge objAuto = null; //Application layer component
            try
            {
                objAuto = new AutoMailMerge(userLogin.objRiskmasterDatabase.ConnectionString, base.ClientId);//sonali
                p_objXmlOut = objAuto.Get(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("AutoMailMergeAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sonali
                return false;
            }
            finally
            {
                objAuto = null;
            }
        }

        public bool Delete(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            AutoMailMerge objAuto = null; //Application layer component
            try
            {
                objAuto = new AutoMailMerge(userLogin.objRiskmasterDatabase.ConnectionString, base.ClientId);//sonali
                objAuto.Delete(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("AutoMailMergeAdaptor.Delete.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sonali
                return false;
            }
            finally
            {
                objAuto = null;
            }
        }




        #endregion
    }
}
