using System;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Collections;
using Riskmaster.Security;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.ExecutiveSummaryReport;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File				: ExecutiveSummaryAdaptor.cs 
	///* $Revision			: 1.0.0.0 
	///* $Creation Date		: 28-Feb-2005
	///* $Author			: Neelima Dabral
	///***************************************************************	
	
	/// <summary>	
	///	This class is used to call the application layer component for Executive Summary.
	/// </summary>
	public class ExecutiveSummaryAdaptor:BusinessAdaptorBase
	{
		#region security codes
		private const int RMO_ACCESS = 0;
		private const int RMO_VIEW = 1;
		
		private const int RMB_EXECUTIVE_SUMMARY=350000;
		private const int RMO_CONFIGURATION=1;
		#region configuration constants

		private const int RMB_CONFIGURATION=350001;
			private const int RMO_EVENT=999;
			private const int RMO_CLAIMSALL=3999;
			private const int RMO_CLAIMSGC=2999;
			private const int RMO_CLAIMSVA=1999;
			private const int RMO_CLAIMSWC=4999;
			private const int RMO_CLAIMSDI=5999;
            private const int RMO_POLICY = 6999;// Sumit - 03/09/2008 Added for Policy Info - MITS 13153
            private const int RMO_CLAIMSPC = 7999;//Sumit - MITS#18145- 10/21/2009 - Security Constant for Property Claims
		#region Event Constants		
		public const int RMB_EVENT=351000;
			private const int RMO_EVENT_INFORMATION=10;
			private const int RMO_EVENT_DETAIL=20;
			private const int RMO_EVENT_DATED_TEXT=30;
			private const int RMO_REPORTED_INFORMATION=40;
			private const int RMO_FOLLOW_UP_INFORMATION=50;
			private const int RMO_OSHA_INFORMATION=60;
			private const int RMO_CLAIM_INFORMATION=70;
			private const int RMO_MEDWATCH_INFORMATION=90;
			private const int RMO_PERSON_INVOLVED=130;
			private const int RMO_QUALITY_MANAGEMENT=140;
			private const int RMO_FALL_INFORMATION=80;
			private const int RMO_COMMENTS=120;
			private const int RMO_DIARY_DETAILS=100;
			private const int RMO_SUPPLEMENTAL_FIELDS=110;
			private const int RMO_INTERVENTION = 150;
            private const int RMO_ENHANCED_NOTES = 160;  //pmittal5 MITS 13517 12/03/08
		
		private const int RMB_MEDWATCH_INFORMATION=351090;
			private const int RMO_EQUIPMENT_INDICATOR=1;
			private const int RMO_MEDWATCH_TEST=4;
			private const int RMO_MEDICATION_INDICATOR=2;
			private const int RMO_CONCOMITANT_PRODUCT=3;

		private const int RMB_PERSON_INVOLVED=351130;
			private const int RMO_OTHER_PERSONS=4;
			private const int RMO_OTHER_MEDICAL_STAFF=3;
			private const int RMO_PATIENTS_INVOLVED=1;
			private const int RMO_PHYSICIANS=2;
        //pmittal5 MITS 13517 12/03/08
        private const int RMB_ENHANCED_NOTES = 351160;
            private const int RMO_ACTIVITY_DATE = 1;
            private const int RMO_DATE_CREATED = 2;
            private const int RMO_ENTERED_BY = 3;
            private const int RMO_NOTE_TYPE = 4;
            private const int RMO_USER_TYPE = 5;
            private const int RMO_NOTE_TEXT = 6;
            private const int RMO_SUBJECT = 7;//zmohammad MITS 31048
        //End - pmittal5
		#endregion
		#region claimsAll constants

		private const int RMB_CLAIMSALL=352000;
			private const int RMO_CLAIMSALL_INFORMATION=10;
			private const int RMO_CLAIM_LEVEL_RESERVES=20;
			private const int RMO_FUNDS_MANAGEMENT=40;
			private const int RMO_PAYMENT_HISTORY=30;
			private const int RMO_CLAIMSALL_EVENT_DETAIL=50;
			private const int RMO_PERSONS_INVOLVED=60;
			private const int RMO_ADJUSTER_INFORMATION=70;
			private const int RMO_LITIGATION_INFORMATION=80;
			private const int RMO_DEPENDANT_INFORMATION=90;
            private const int RMO_CLAIMSALL_ENHANCED_NOTES = 130;  //pmittal5 MITS 13517 12/03/08
            private const int RMO_SUBROGATION_INFORMATION = 200;

		private const int RMB_FUNDS_MANAGEMENT=352040;
			private const int RMO_TRANSACTION_DETAIL=1;
		
		private const int RMB_ADJUSTER_INFORMATION=352070;
			private const int RMO_ADJUSTER_DATED_TEXT=1;

		private const int RMB_LITIGATION_INFORMATION=352080;
			private const int RMO_ATTORNEY_INFORMATION=1;
			private const int RMO_EXPERT_WITNESS=2;
            private const int RMO_LIT_DEMAND_OFFER = 3;

        private const int RMB_SUBROGATION_INFORMATION = 352200;
            private const int RMO_SUBRO_DEMAND_OFFER = 1;
		
		private const int RMB_DEPENDANT_INFORMATION=352090;
			private const int RMO_CLAIMALL_INVOLVEMENT_HISTORY=2;

		private const int RMB_ADJUSTER_DATED_TEXT=352071;
			private const int RMO_ENTEREDUSERFULLNAME = 4;

        //pmittal5 MITS 13517 12/03/08
        private const int RMB_CLAIMSALL_ENHANCED_NOTES = 352130;
            private const int RMO_CLAIMSALL_ACTIVITY_DATE = 1;
            private const int RMO_CLAIMSALL_DATE_CREATED = 2;
            private const int RMO_CLAIMSALL_ENTERED_BY = 3;
            private const int RMO_CLAIMSALL_NOTE_TYPE = 4;
            private const int RMO_CLAIMSALL_USER_TYPE = 5;
            private const int RMO_CLAIMSALL_NOTE_TEXT = 6;
            private const int RMO_CLAIMSALL_SUBJECT = 7;  //zmohammad MITS 31048
        //End - pmittal5

		#endregion
		#region claimsGC constants
		private const int RMB_CLAIMSGC=353000;
			private const int RMO_CLAIMSGC_CLAIMANT_INFORMATION=10;
		
		private const int RMB_CLAIMSGC_CLAIMANT_INFORMATION=353010;
			private const int RMO_GC_ATTORNEY_INFORMATION=10;
			private const int RMO_CLAIM_INVOLVEMENT_HISTORY=20;
			private const int RMO_CLAIMANT_LEVEL_RESERVES=30;
			private const int RMO_CLAIMANT_PAYMENT_HISTORY=40;
			private const int RMO_CLAIMSGC_CLAIMANT_FUNDS_MANAGEMENT=50;
		
		private const int RMB_CLAIMSGC_CLAIMANT_FUNDS_MANAGEMENT=353060;
			private const int RMO_TRANSACTION_DETAILS=1;
		#endregion
		#region claimsVa constants
		private const int RMB_CLAIMSVA=354000;
			private const int RMO_VEHICLE_ACCIDENT=80;
			private const int RMO_CLAIMSVA_CLAIMANT_INFORMATION=10;
				private const int RMO_CLAIMSVA_CLAIMANT_ATTORNEY=10;
				
		private const int RMB_CLAIMSVA_CLAIMANT_INFORMATION=354010;
			private const int RMO_VA_ATTORNEY_INFORMATION=10;
		private const int RMB_CLAIMSVA_CLAIMANT_FUNDS_MANAGEMENT=354060;
			private const int RMO_VA_CLAIMANT_TRANSDETAIL = 10;

		private const int RMB_VEHICLE_ACCIDENT=354080;		
			private const int RMO_UNIT_INFORMATION=10;
		
		private const int RMB_UNIT_INFORMATION=354090;
		private const int RMO_UNIT_CLAIM_INVOLVEMENT_HISTORY=10;
		private const int RMO_PAYMENT_HISTORY_FOR_UNITS=30;
		private const int RMO_UNIT_LEVEL_RESOURCES=40;
		private const int RMO_UNIT_LEVEL_FUNDS=20;

		private const int RMB_UNIT_LEVEL_FUNDS=354130;
		private const int RMO_UNIT_LEVEL_FUNDS_TRANSACTION_DETAILS=10;
#endregion
		#region ClaimsWc Constants
		private const int RMB_CLAIMSWC=355000;
		private const int RMO_CLAIMSWC_EMPLOYEE_INFO=10;
		private const int RMO_OSHA_INFO=80;
		private const int RMO_JURIS_SUPP=90;
		private const int RMO_CLAIMSWC_CASEMANAGEMENT = 100;
		private const int RMO_CLAIMSWC_TREATMENTPLANT = 150;
		private const int RMO_CLAIMSWC_MEDMANAGEMENTSVAING = 160;
		private const int RMO_CLAIMSWC_ACCOMMODATIONS = 170;
		private const int RMO_CLAIMSWC_VOCREHAB = 180;

		private const int RMB_CLAIMSWC_EMPLOYEE_INFO=355010;
		private const int RMO_CLAIMSWC_EMPLOYMENT_INFO=10;
		private const int RMO_CLAIM_INVOLVEMENT_INFO=60;

		private const int RMB_CLAIMSWC_EMPLOYMENT_INFO=355020;
		private const int RMO_DEPENDENT_INFO=10;
		private const int RMO_RESTRICTED_DAYS=20;
		private const int RMO_LOST_DAYS=30;
		private const int RMO_EMPLOYEE_EVENT_DETAILS=40;

		private const int RMB_CLAIMSWC_CASEMANAGEMENT = 355100;
		private const int RMO_CLAIMSWC_CASEMANAGER = 10;
		private const int RMO_CLAIMSWC_RESTRICTEDWORK = 30;
		private const int RMO_CLAIMSWC_WORKLOSS = 40;

		private const int RMB_CLAIMSWC_CASEMANAGER = 355110;
		private const int RMO_CLAIMSWC_CASEMANAGERNOTES = 10;

		#endregion
		#region ClaimsDI constants
		private const int RMB_CLAIMSDI=356000;
		private const int RMO_CLAIMSDI_EMPLOYEE_INFO=10;
		private const int RMO_CLAIMSDI_PLANINFO=80;
		private const int RMO_CLAIMSDI_CASEMANAGEMENT = 100;
		private const int RMO_CLAIMSDI_TREATMENTPLAN = 150;
		private const int RMO_CLAIMSDI_MEDICALMANAGEMENT = 160;
		private const int RMO_CLAIMSDI_ACCOMMODATIONS = 170;
		private const int RMO_CLAIMSDI_VOCREHAB = 180;
		private const int RMO_CLAIMSDI_LEAVE = 190;

		private const int RMB_CLAIMSDI_EMPLOYEE_INFO=356010;
		private const int RMO_CLAIMSDI_EMPLOYMENT_INFO=10;
		private const int RMO_CLAIMDI_INVOLVEMENT_INFO=60;
		
		private const int RMB_CLAIMSDI_EMPLOYMENT_INFO=356020;
		private const int RMO_CLAIMSDI_DEPENDENT_INFO=10;
		private const int RMO_CLAIMSDI_RESTRICTED_DAYS=20;
		private const int RMO_CLAIMSDI_LOST_DAYS=30;
		private const int RMO_CLAIMSDI_EMPLOYEE_EVENT_DETAILS=40;

		private const int RMB_CLAIMSDI_CASEMANAGEMENT = 355100;
		private const int RMO_CLAIMSDI_CASEMANAGER = 10;
		private const int RMO_CLAIMSDI_RESTRICTEDWORK = 30;
		private const int RMO_CLAIMSDI_WORKLOSS = 40;

		private const int RMB_CLAIMSDI_CASEMANAGER = 355110;
		private const int RMO_CLAIMSDI_CASEMANAGERNOTES = 10;

		#endregion

        #region Policy Constants
        // Sumit - 03/09/2008 Added for Policy Info in SMS - MITS 13153
        private const int RMB_POLICY = 357000;
        private const int RMO_POLICY_POLICYINFORMATION = 10;
        private const int RMO_POLICY_INSUREDINFORMATION = 80;
        private const int RMO_POLICY_INSURANCECOMPANYNAME = 120;

        private const int RMB_POLICY_POLICYINFORMATION = 357010;
        private const int RMO_POLICY_POLICYNUMBER = 10;
        private const int RMO_POLICY_EFFECTIVEDATE = 20;
        private const int RMO_POLICY_EXPIRATIONDATE = 30;
        private const int RMO_POLICY_CLAIMLIMIT = 40;
        private const int RMO_POLICY_POLICYLIMIT = 50;
        private const int RMO_POLICY_PERCENTAGEOFAGGREGATE = 60;

        private const int RMB_POLICY_INSUREDINFORMATION = 357080;
        private const int RMO_POLICY_INSURED = 10;
        private const int RMO_POLICY_ADDRESS = 20;
        private const int RMO_POLICY_DEPARTMENTABBREVIATION = 30;

        #endregion
        //Sumit - MITS#18145- 10/21/2009 - Security Constants for Property Claims
        #region claimsPC constants
        //smahajan6 - 02/16/2010 - Start
        //Modified the function id for Property Claim from "357000" to "358000" series
        private const int RMB_CLAIMSPC = 358000;
        private const int RMO_CLAIMSPC_CLAIMANT_INFORMATION = 10;
        private const int RMO_PROPERTY_INFORMATION = 80;

        private const int RMB_CLAIMSPC_CLAIMANT_INFORMATION = 358010;
        private const int RMO_PC_ATTORNEY_INFORMATION = 10;
        private const int RMO_PC_CLAIM_INVOLVEMENT_HISTORY = 20;
        private const int RMO_PC_CLAIMANT_LEVEL_RESERVES = 30;
        private const int RMO_PC_CLAIMANT_PAYMENT_HISTORY = 40;
        private const int RMO_CLAIMSPC_CLAIMANT_FUNDS_MANAGEMENT = 50;

        private const int RMB_CLAIMSPC_CLAIMANT_FUNDS_MANAGEMENT = 358060;
        private const int RMO_PC_TRANSACTION_DETAILS = 10;

        //smahajan6 - MITS# 18230 - 12/30/2009 :Start
        private const int RMB_PROPERTY_INFORMATION = 358080;
        private const int RMO_COPE_DATA= 10;
        private const int RMO_OPTIONAL_COPE_DATA = 20;
        private const int RMO_SCHEDULE = 30;
        //smahajan6 - MITS# 18230 - 12/30/2009 :End
        //smahajan6 - 02/16/2010 - End
        #endregion
        //Sumit - MITS#18145- 10/21/2009 - Security Constants for Property Claims

        #region Policy Management Constants
        // Gagan for MITS 20375
        private const int RMB_POLICYMGMT = 358500;
        private const int RMO_POLICYMGMT_POLICYINFORMATION = 10;
        private const int RMO_POLICYMGMT_POLICYCOVERAGES = 30;
        private const int RMO_POLICYMGMT_POLICYEXPOSURE = 50;

        private const int RMB_POLICYMGMT_POLICYINFORMATION = 358510;
        private const int RMO_POLICYMGMT_POLICYNAME = 1;
        private const int RMO_POLICYMGMT_POLICYNUMBER = 2;
        private const int RMO_POLICYMGMT_POLICYSTATUS = 3;
        private const int RMO_POLICYMGMT_POLICYTYPE = 4;
        private const int RMO_POLICYMGMT_POLICYSTATE = 5;
        private const int RMO_POLICYMGMT_POLICYEFFECTIVEDATE = 6;
        private const int RMO_POLICYMGMT_POLICYEXPIRATIONDATE = 7;
        private const int RMO_POLICYMGMT_POLICYTOTALBILLEDPREMIUM = 8;
        private const int RMO_POLICYMGMT_POLICYINSURED = 9;

        private const int RMB_POLICYMGMT_POLICYINSURED = 358519;                      
        private const int RMO_POLICYMGMT_POLICYINSUREDNAME = 1;
        private const int RMO_POLICYMGMT_POLICYINSUREDADDRESS = 2;

        private const int RMO_POLICYMGMT_POLICYINSURER = 12;


        private const int RMB_POLICYMGMT_POLICYCOVERAGES = 358530;
        private const int RMO_POLICYMGMT_POLICYCOVERAGETYPE = 1;
        private const int RMO_POLICYMGMT_POLICYCOVERAGEDESC = 2;
        private const int RMO_POLICYMGMT_POLICYLIMIT = 3;
        private const int RMO_POLICYMGMT_CLAIMLIMIT = 4;
        private const int RMO_POLICYMGMT_OCCURRENCELIMIT = 5;
        private const int RMO_POLICYMGMT_TOTALPAYMENT = 6;

        
        private const int RMB_POLICYMGMT_POLICYEXPOSURE = 358550;                   
        private const int RMO_POLICYMGMT_POLICYEXPOSURETYPE = 1;
        private const int RMO_POLICYMGMT_POLICYEXPOSUREDESC = 2;
        private const int RMO_POLICYMGMT_POLICYEXPOSUREAMOUNT = 3;
        private const int RMO_POLICYMGMT_POLICYEXPOSURERATE = 4;
        private const int RMO_POLICYMGMT_POLICYEXPOSUREBASERATE = 5;

        

        #endregion




#endregion
		private const int RMB_EQUIPMENT_INDICATOR=351090;
	
		private Hashtable securityConstantsHash = null;
		private struct HashValues
		{
			int secCode;
			bool parent;
			object[] childNodes;
			public HashValues(int a_seccode,bool a_parent,object[] a_childNodes)
			{
				secCode=a_seccode;
				parent=a_parent;
				childNodes=a_childNodes;
			}
		}
	
		#endregion
		#region Constructor

		/// <summary>
		/// Class Constructor.
		/// </summary>
		public ExecutiveSummaryAdaptor()
		{			
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// This method is a wrapper to the EventExecSumm method in Riskmaster.Application.ExecutiveSummaryReport.ExecutiveSummary
		/// </summary>
		/// <param name="p_objXmlIn">Input XML
		/// <Document>
		///		<ExecutiveSummaryReport>
		///			<EventId>2</EventId>
		///		</ExecutiveSummaryReport>
		///	</Document>
		/// </param>
		/// <param name="p_objOutXML">Output XML
		///		<ExecutiveSummaryReport>
		///			<File>data for the file in Base64 encoding </File>
		///		</ExecutiveSummaryReport>
		///	</param>		
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>
		/// <returns>True/False for Success/Failure of the method</returns>
		public bool EventExecSumm(XmlDocument p_objXmlIn, ref XmlDocument p_objOutXML, ref BusinessAdaptorErrors p_objErrOut)
		{
			
			ExecutiveSummary	objExecSumm = null;				
			XmlElement			objElement=null;	
			MemoryStream		objExecSummaryReport=null;
            LocalCache objCache = null;
			int					iEventId=0;
            int iSelectedChoiceThreshholdExceeded = 0;
			try
			{
                //objExecSumm = new ExecutiveSummary(userID, connectionString, securityConnectionString, DSNID); //shradha
                objExecSumm = new ExecutiveSummary(userID, connectionString, securityConnectionString, base.ClientId); //shradha, sbh
                //nkaranam2 - MITS ID : 34643
                objExecSumm.RMO_ENABLE_SALARY = FormBase.RMO_ENABLE_SALARY;
                objExecSumm.RMO_WC_EMPLOYEE = FormBase.RMO_WC_EMPLOYEE;
                //nkaranam2 - MITS ID : 34643
                objExecSumm.m_UserLogin = base.userLogin; //shradha
                objExecSumm.BaseLanguageCode = Conversion.ConvertStrToInteger(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]); //Aman ML Change//sonali for config change
				//Get the Event Id
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//EventId");
				if (objElement==null)
				{
					p_objErrOut.Add("EventIdMissing",Globalization.GetString("EventIdMissing", base.ClientId),
						BusinessAdaptorErrorType.Error);					
					return false;
				}
				iEventId = Conversion.ConvertStrToInteger(objElement.InnerText);
				if(iEventId ==0)
				{
					p_objErrOut.Add("InvalidEventId",Globalization.GetString("InvalidEventId", base.ClientId),
						BusinessAdaptorErrorType.Error);					
					return false;
				}

                //MITS 26441 : Client requests change to custom print process for Exec Summary/Large Notes 
                //modified by Raman Bhatia on 10/21/2011
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//SelectedChoiceThreshholdExceeded");
                if (objElement == null)
                {
                    objExecSumm.GenerateReportThresholdExceeded = (int)ExecutiveSummary.GenerateReportThresholdExceededValues.DoNothing;
                }
                else
                {
                    iSelectedChoiceThreshholdExceeded = Conversion.ConvertStrToInteger(objElement.InnerText);
                    if (iSelectedChoiceThreshholdExceeded == 1)
                        objExecSumm.GenerateReportThresholdExceeded = (int)ExecutiveSummary.GenerateReportThresholdExceededValues.GenerateReport;
                    else if (iSelectedChoiceThreshholdExceeded == 2)
                        objExecSumm.GenerateReportThresholdExceeded = (int)ExecutiveSummary.GenerateReportThresholdExceededValues.ScheduleReport;
                    else
                        objExecSumm.GenerateReportThresholdExceeded = (int)ExecutiveSummary.GenerateReportThresholdExceededValues.DoNothing;
                }

                //objExecSumm = new ExecutiveSummary(userID,connectionString,securityConnectionString , DSNID);
                //Start rsushilaggar 08/06/2010 MITS 20780
                //Check UserLimit Security.
                int eventType = 0;
                objCache = new LocalCache(connectionString,base.ClientId);
                if (objExecSumm.CheckUserAccessLimits(userLogin, iEventId, base.GetSessionObject(), out eventType))
                {
                    p_objErrOut.Add(Globalization.GetString("LimitError", base.ClientId),
                            string.Format(Globalization.GetString("Access.Event.UserLimitFailed", base.ClientId), objCache.GetCodeDesc(eventType)),
                            BusinessAdaptorErrorType.Error);
                    return false;
                }
                //End rsushilaggar               
				objExecSumm.UserName=userLogin.LoginName;
				objExecSummaryReport = objExecSumm.EventExecSumm(iEventId);                
				//create the output xml document containing the data in Base64 format
				p_objOutXML=new XmlDocument();
				objElement=p_objOutXML.CreateElement("ExecutiveSummaryReport");
				p_objOutXML.AppendChild(objElement);
				objElement=p_objOutXML.CreateElement("File");				
				if(objExecSummaryReport != null)
					objElement.InnerText=Convert.ToBase64String(objExecSummaryReport.ToArray());
				p_objOutXML.FirstChild.AppendChild(objElement);
			               
				return true;
			}
            //shradha
            catch (InvalidCriteriaException p_objException)
            {
                throw new InvalidCriteriaException(Globalization.GetString("ExecutiveSummary.ValidateCriteria.ThresholdExceeded", base.ClientId), p_objException);
            }
			catch(RMAppException p_objException)
			{
                //shradha 
                if (p_objException.Message == Globalization.GetString("PrintPDF.ValidateCriteria.ThresholdExceeded", base.ClientId)
                        || (p_objException.InnerException !=null  && p_objException.InnerException.Message == Globalization.GetString("PrintPDF.ValidateCriteria.ThresholdExceeded", base.ClientId)))
                {
                    throw new InvalidCriteriaException(Globalization.GetString("ExecutiveSummary.ValidateCriteria.ThresholdExceeded", base.ClientId), p_objException);
                }

				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                //shradha 
                if (p_objException.Message == Globalization.GetString("PrintPDF.ValidateCriteria.ThresholdExceeded", base.ClientId)
                        || (p_objException.InnerException !=null  && p_objException.InnerException.Message == Globalization.GetString("PrintPDF.ValidateCriteria.ThresholdExceeded", base.ClientId)))
                {
                    throw new InvalidCriteriaException(Globalization.GetString("ExecutiveSummary.ValidateCriteria.ThresholdExceeded", base.ClientId), p_objException);
                }
				p_objErrOut.Add(p_objException,Globalization.GetString("ExecutiveSummaryAdaptor.EventExecSumm.Error", base.ClientId),
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objExecSumm = null;				
				objElement=null;
				if(objExecSummaryReport!=null)
				{
					objExecSummaryReport.Close();
					objExecSummaryReport=null;
				}
                objCache.Dispose();
			}
		}


		/// <summary>
		/// This method is a wrapper to the ClaimExecSumm method in Riskmaster.Application.ExecutiveSummaryReport.ExecutiveSummary
		/// </summary>
		/// <param name="p_objXmlIn">Input XML
		/// <Document>
		///		<ExecutiveSummaryReport>
		///			<ClaimId>8</ClaimId>
		///		</ExecutiveSummaryReport>
		///	</Document>
		/// </param>
		/// <param name="p_objOutXML">Output XML
		///		<ExecutiveSummaryReport>
		///			<File>data for the file in Base64 encoding </File>
		///		</ExecutiveSummaryReport>
		/// </param>		
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>
		/// <returns>True/False for Success/Failure of the method</returns>
		public bool ClaimExecSumm(XmlDocument p_objXmlIn, ref XmlDocument p_objOutXML, ref BusinessAdaptorErrors p_objErrOut)
		{
			
			ExecutiveSummary	objExecSumm = null;				
			XmlElement			objElement=null;	
			MemoryStream		objExecSummaryReport=null;
			int					iClaimId=0;
            int iSelectedChoiceThreshholdExceeded = 0;            
            //akaur9 Mobile Adjuster
            XmlNode xmlCaller = null;
            XmlDocument objExecSummaryXML = null;
            string sClaimNumber = string.Empty;
            XElement oElement = null; 
			try
			{
                //objExecSumm = new ExecutiveSummary(m_connectionString);
                //akaur9 used the if else structure for the Mobile Adjuster
                xmlCaller = p_objXmlIn.SelectSingleNode("//caller");
                if (xmlCaller != null && (xmlCaller.InnerText.Trim().Equals("MobileAdjuster") || xmlCaller.InnerText.Trim().Equals("MobilityAdjuster")))
                {
                    objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//ClaimNumber");
                    if (objElement == null)
                    {
                        p_objErrOut.Add("ClaimNumberMissing", Globalization.GetString("ClaimNumberMissing", base.ClientId),
                            BusinessAdaptorErrorType.Error);
                        return false;
                    }
                    sClaimNumber = objElement.InnerText;
                    objExecSumm = new ExecutiveSummary(userID, connectionString, base.ClientId);//shradha

                    objExecSumm.m_UserLogin = base.userLogin; //shradha
                    objExecSumm.BaseLanguageCode = Conversion.ConvertStrToInteger(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]); //Aman ML Change//sonali
                    
                    objExecSumm.m_UserLogin = base.userLogin; //shradha                    
                    p_objOutXML = new XmlDocument();
                    objElement = p_objOutXML.CreateElement("ExecutiveSummaryReport");
                    p_objOutXML.AppendChild(objElement);
                    if (xmlCaller.InnerText.Trim().Equals("MobileAdjuster"))
                    {
                        objExecSumm.bIsMobileAdjuster = true;
                    }
                        if (xmlCaller.InnerText.Trim().Equals("MobilityAdjuster"))
                    {
                        objExecSumm.bIsMobilityAdjuster = true;

                    }//mbahl3 mits 35616 //code merge psharma206
                    oElement = GetPreferenceTemplate();                    
                    objExecSumm.objExecPreferences = new XmlDocument();
                    objExecSumm.objExecPreferences.LoadXml(oElement.ToString());
                    //rsharma220 Windows Mobility Changes : Added caller
                    objExecSummaryXML = objExecSumm.ClaimExecSummForMobileAdjuster(sClaimNumber, xmlCaller.InnerText);
                }
                else
                { 
                    objExecSumm = new ExecutiveSummary(userID, connectionString, base.ClientId);//shradha
                    //nkaranam2 - MITS ID : 34643
                    objExecSumm.RMO_WC_EMPLOYEE = FormBase.RMO_WC_EMPLOYEE;
                    objExecSumm.RMO_ENABLE_SALARY = FormBase.RMO_ENABLE_SALARY;
                    objExecSumm.RMO_VIEW_SSN = FormBase.RMO_VIEW_SSN;
                    //nkaranam2 - MITS ID : 34643
                    objExecSumm.m_UserLogin = base.userLogin; //shradha
                    objExecSumm.BaseLanguageCode = Conversion.ConvertStrToInteger(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]); //Aman ML Change//sonali
                    //Get the Claim Id
				    objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ClaimId");
				    if (objElement==null)
				    {
					    p_objErrOut.Add("ClaimIdMissing",Globalization.GetString("ClaimIdMissing",base.ClientId),
						    BusinessAdaptorErrorType.Error);					
					    return false;
				    }
				    iClaimId = Conversion.ConvertStrToInteger(objElement.InnerText);
				    if(iClaimId ==0)
				    {
					    p_objErrOut.Add("InvalidClaimId",Globalization.GetString("InvalidClaimId", base.ClientId),
						    BusinessAdaptorErrorType.Error);					
					    return false;
				    }
                    //MITS 26441 : Client requests change to custom print process for Exec Summary/Large Notes 
                    //modified by Raman Bhatia on 10/21/2011
                    objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//SelectedChoiceThreshholdExceeded");
                    if (objElement == null)
                    {
                        objExecSumm.GenerateReportThresholdExceeded = (int)ExecutiveSummary.GenerateReportThresholdExceededValues.DoNothing;
                    }
                    else
                    {
                        iSelectedChoiceThreshholdExceeded = Conversion.ConvertStrToInteger(objElement.InnerText);
                        if (iSelectedChoiceThreshholdExceeded == 1)
                            objExecSumm.GenerateReportThresholdExceeded = (int)ExecutiveSummary.GenerateReportThresholdExceededValues.GenerateReport;
                        else if (iSelectedChoiceThreshholdExceeded == 2)
                            objExecSumm.GenerateReportThresholdExceeded = (int)ExecutiveSummary.GenerateReportThresholdExceededValues.ScheduleReport;
                        else
                            objExecSumm.GenerateReportThresholdExceeded = (int)ExecutiveSummary.GenerateReportThresholdExceededValues.DoNothing;
                    }
                    objExecSumm.UserName = userLogin.LoginName;                  //shradha              
				    objExecSummaryReport = objExecSumm.ClaimExecSumm(iClaimId);
				
				    //create the output xml document containing the data in Base64 format
				    p_objOutXML=new XmlDocument();
				    objElement=p_objOutXML.CreateElement("ExecutiveSummaryReport"); 
				    p_objOutXML.AppendChild(objElement);
                    objExecSumm.bIsMobileAdjuster = false;
                }
				objElement=p_objOutXML.CreateElement("File");
                if ((objExecSumm.bIsMobileAdjuster || objExecSumm.bIsMobilityAdjuster) && p_objOutXML != null)
                {
                    objElement.InnerXml = objExecSummaryXML.OuterXml;                  
                }
                else if(objExecSummaryReport != null)
                {
                    objElement.InnerText = Convert.ToBase64String(objExecSummaryReport.ToArray());
                }
             
				p_objOutXML.FirstChild.AppendChild(objElement);

				return true;
			}
                //shradha
            catch (InvalidCriteriaException p_objException)
            {
                throw new InvalidCriteriaException(Globalization.GetString("ExecutiveSummary.ValidateCriteria.ThresholdExceeded", base.ClientId), p_objException);
            }
			catch(RMAppException p_objException)
			{
                //shradha 
                if (p_objException.Message == Globalization.GetString("PrintPDF.ValidateCriteria.ThresholdExceeded", base.ClientId)
                       || (p_objException.InnerException != null && p_objException.InnerException.Message == Globalization.GetString("PrintPDF.ValidateCriteria.ThresholdExceeded", base.ClientId)))
	            {
                    throw new InvalidCriteriaException(Globalization.GetString("ExecutiveSummary.ValidateCriteria.ThresholdExceeded", base.ClientId), p_objException);
	            }

                p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                //shradha 
                if (p_objException.Message == Globalization.GetString("PrintPDF.ValidateCriteria.ThresholdExceeded",base.ClientId)
                        || (p_objException.InnerException != null && p_objException.InnerException.Message == Globalization.GetString("PrintPDF.ValidateCriteria.ThresholdExceeded", base.ClientId)))
                {
                    throw new InvalidCriteriaException(Globalization.GetString("ExecutiveSummary.ValidateCriteria.ThresholdExceeded", base.ClientId), p_objException);
                }
                p_objErrOut.Add(p_objException, Globalization.GetString("ExecutiveSummaryAdaptor.ClaimExecSumm.Error", base.ClientId),
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objExecSumm = null;				
				objElement=null;
				if(objExecSummaryReport!=null)
				{
					objExecSummaryReport.Close();
					objExecSummaryReport=null;
				}
			}
		}
		/// <summary>
        /// This method is a wrapper to the ClaimantExecSumm method in Riskmaster.Application.ExecutiveSummaryReport.ExecutiveSummary
        /// Created by gbindra 05262014 WWIG GAP15 MITS#34104
        /// </summary>
        /// <param name="p_objXmlIn">Input XML
        /// <Document>
        ///		<ExecutiveSummaryReport>
        ///			<ClaimId>8</ClaimId>
        ///		</ExecutiveSummaryReport>
        ///	</Document>
        /// </param>
        /// <param name="p_objOutXML">Output XML
        ///		<ExecutiveSummaryReport>
        ///			<File>data for the file in Base64 encoding </File>
        ///		</ExecutiveSummaryReport>
        /// </param>		
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>
        /// <returns>True/False for Success/Failure of the method</returns>
        public bool ClaimantExecSumm(XmlDocument p_objXmlIn, ref XmlDocument p_objOutXML, ref BusinessAdaptorErrors p_objErrOut)
        {

            ExecutiveSummary objExecSumm = null;
            XmlElement objElement = null;
            MemoryStream objExecSummaryReport = null;
            int iClaimantId = 0;
            int iSelectedChoiceThreshholdExceeded = 0;
            XmlDocument objExecSummaryXML = null;
            string sClaimNumber = string.Empty;
            bool bSuccess = false;
            try
            {
                
                    objExecSumm = new ExecutiveSummary(userID, connectionString, base.ClientId);//shradha

                    objExecSumm.m_UserLogin = base.userLogin; //shradha
                    objExecSumm.BaseLanguageCode = Conversion.CastToType<int>(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0], out bSuccess); //Aman ML Change//sonali
                    //Get the Claim Id
                    objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//ClaimantId");
                    if (objElement == null)
                    {
                        p_objErrOut.Add("ClaimIdMissing", Globalization.GetString("ClaimIdMissing", base.ClientId),
                            BusinessAdaptorErrorType.Error);
                        return false;
                    }
                    iClaimantId = Conversion.CastToType<int>(objElement.InnerText, out bSuccess);
                    if (iClaimantId == 0)
                    {
                        p_objErrOut.Add("InvalidClaimId", Globalization.GetString("InvalidClaimId", base.ClientId),
                            BusinessAdaptorErrorType.Error);
                        return false;
                    }
                    //MITS 26441 : Client requests change to custom print process for Exec Summary/Large Notes 
                    //modified by Raman Bhatia on 10/21/2011
                    objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//SelectedChoiceThreshholdExceeded");
                    if (objElement == null)
                    {
                        objExecSumm.GenerateReportThresholdExceeded = (int)ExecutiveSummary.GenerateReportThresholdExceededValues.DoNothing;
                    }
                    else
                    {
                        iSelectedChoiceThreshholdExceeded = Conversion.CastToType<int>(objElement.InnerText, out bSuccess);
                        if (iSelectedChoiceThreshholdExceeded == 1)
                            objExecSumm.GenerateReportThresholdExceeded = (int)ExecutiveSummary.GenerateReportThresholdExceededValues.GenerateReport;
                        else if (iSelectedChoiceThreshholdExceeded == 2)
                            objExecSumm.GenerateReportThresholdExceeded = (int)ExecutiveSummary.GenerateReportThresholdExceededValues.ScheduleReport;
                        else
                            objExecSumm.GenerateReportThresholdExceeded = (int)ExecutiveSummary.GenerateReportThresholdExceededValues.DoNothing;
                    }
                    objExecSumm.UserName = userLogin.LoginName;                  //shradha              
                    objExecSummaryReport = objExecSumm.ClaimExecSumm(iClaimantId);

                    //create the output xml document containing the data in Base64 format
                    p_objOutXML = new XmlDocument();
                    objElement = p_objOutXML.CreateElement("ExecutiveSummaryReport");
                    p_objOutXML.AppendChild(objElement);
                    objExecSumm.bIsMobileAdjuster = false;
                
                objElement = p_objOutXML.CreateElement("File");
                if (objExecSumm.bIsMobileAdjuster && p_objOutXML != null)
                {
                    objElement.InnerXml = objExecSummaryXML.OuterXml;
                }
                else if (objExecSummaryReport != null)
                {
                    objElement.InnerText = Convert.ToBase64String(objExecSummaryReport.ToArray());
                }

                p_objOutXML.FirstChild.AppendChild(objElement);

                return true;
            }
            //shradha
            catch (InvalidCriteriaException p_objException)
            {
                throw new InvalidCriteriaException(Globalization.GetString("ExecutiveSummary.ValidateCriteria.ThresholdExceeded", base.ClientId), p_objException);
            }
            catch (RMAppException p_objException)
            {
                //shradha 
                if (p_objException.Message == Globalization.GetString("PrintPDF.ValidateCriteria.ThresholdExceeded", base.ClientId)
                       || (p_objException.InnerException != null && p_objException.InnerException.Message == Globalization.GetString("PrintPDF.ValidateCriteria.ThresholdExceeded", base.ClientId)))
                {
                    throw new InvalidCriteriaException(Globalization.GetString("ExecutiveSummary.ValidateCriteria.ThresholdExceeded", base.ClientId), p_objException);
                }

                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                //shradha 
                if (p_objException.Message == Globalization.GetString("PrintPDF.ValidateCriteria.ThresholdExceeded", base.ClientId)
                        || (p_objException.InnerException != null && p_objException.InnerException.Message == Globalization.GetString("PrintPDF.ValidateCriteria.ThresholdExceeded", base.ClientId)))
                {
                    throw new InvalidCriteriaException(Globalization.GetString("ExecutiveSummary.ValidateCriteria.ThresholdExceeded", base.ClientId), p_objException);
                }
                p_objErrOut.Add(p_objException, Globalization.GetString("ExecutiveSummaryAdaptor.ClaimExecSumm.Error", base.ClientId),
                    BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objExecSumm = null;
                objElement = null;
                if (objExecSummaryReport != null)
                {
                    objExecSummaryReport.Close();
                    objExecSummaryReport = null;
                }
            }
        }


		/// <summary>
		/// This method is a wrapper to the AdmTrackingExecSumm method in Riskmaster.Application.ExecutiveSummaryReport.ExecutiveSummary
		/// </summary>
		/// <param name="p_objXmlIn">Input XML
		/// <Document>
		///		<ExecutiveSummaryReport>
		///			<RecordId>1</RecordId>
		///			<TableName>BOND_ABS</TableName>
		///		</ExecutiveSummaryReport>
		///	</Document>
		/// </param>
		/// <param name="p_objOutXML">Output XML
		///		<ExecutiveSummaryReport>
		///			<File>data for the file in Base64 encoding </File>
		///		</ExecutiveSummaryReport>
		/// </param>		
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>
		/// <returns>True/False for Success/Failure of the method</returns>
		public bool AdmTrackingExecSumm(XmlDocument p_objXmlIn, ref XmlDocument p_objOutXML, ref BusinessAdaptorErrors p_objErrOut)
		{
			
			ExecutiveSummary	objExecSumm = null;				
			XmlElement			objElement=null;	
			MemoryStream		objExecSummaryReport=null;
			int					iRecordId=0;
			string				sTableName="";			
			try
            {

                objExecSumm = new ExecutiveSummary(userID,connectionString, base.ClientId); // shradha

                objExecSumm.m_UserLogin = base.userLogin; // sharadha
                objExecSumm.BaseLanguageCode = Conversion.ConvertStrToInteger(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]); //Aman ML Change//sonali
				//Get the Record Id
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//RecordId");
				if (objElement==null)
				{
                    p_objErrOut.Add("RecordIdMissing", Globalization.GetString("RecordIdMissing", base.ClientId),
						BusinessAdaptorErrorType.Error);					
					return false;
				}
				iRecordId = Conversion.ConvertStrToInteger(objElement.InnerText);
				if(iRecordId ==0)
				{
                    p_objErrOut.Add("InvalidRecordId", Globalization.GetString("InvalidRecordId", base.ClientId),
						BusinessAdaptorErrorType.Error);					
					return false;
				}

				//Get the Table Name
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//TableName");
				if ((objElement==null) || (objElement.InnerText.Trim() == ""))
				{
                    p_objErrOut.Add("TableNameMissing", Globalization.GetString("TableNameMissing", base.ClientId),
						BusinessAdaptorErrorType.Error);					
					return false;
				}
				sTableName = objElement.InnerText;

				
				objExecSummaryReport = objExecSumm.AdmTrackingExecSumm(iRecordId,sTableName);
				
				//create the output xml document containing the data in Base64 format
				p_objOutXML=new XmlDocument();
				objElement=p_objOutXML.CreateElement("ExecutiveSummaryReport");
				p_objOutXML.AppendChild(objElement);
				objElement=p_objOutXML.CreateElement("File");				
				if(objExecSummaryReport != null)
					objElement.InnerText=Convert.ToBase64String(objExecSummaryReport.ToArray());
				p_objOutXML.FirstChild.AppendChild(objElement);
				
				return true;
			}
            catch (InvalidCriteriaException p_objException)
            {
                throw new InvalidCriteriaException(Globalization.GetString("ExecutiveSummary.ValidateCriteria.ThresholdExceeded", base.ClientId), p_objException);
            }
			catch(RMAppException p_objException)
			{
                //shradha 
                if (p_objException.Message == Globalization.GetString("PrintPDF.ValidateCriteria.ThresholdExceeded", base.ClientId)
                        || (p_objException.InnerException != null && p_objException.InnerException.Message == Globalization.GetString("PrintPDF.ValidateCriteria.ThresholdExceeded", base.ClientId)))
                {
                    throw new InvalidCriteriaException(Globalization.GetString("ExecutiveSummary.ValidateCriteria.ThresholdExceeded", base.ClientId), p_objException);
                }

				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                //shradha 
                if (p_objException.Message == Globalization.GetString("PrintPDF.ValidateCriteria.ThresholdExceeded", base.ClientId)
                        || (p_objException.InnerException != null && p_objException.InnerException.Message == Globalization.GetString("PrintPDF.ValidateCriteria.ThresholdExceeded", base.ClientId)))
                {
                    throw new InvalidCriteriaException(Globalization.GetString("ExecutiveSummary.ValidateCriteria.ThresholdExceeded", base.ClientId), p_objException);
                }
                p_objErrOut.Add(p_objException, Globalization.GetString("ExecutiveSummaryAdaptor.AdmTrackingExecSumm.Error", base.ClientId),
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objExecSumm = null;				
				objElement=null;
				if(objExecSummaryReport!=null)
				{
					objExecSummaryReport.Close();
					objExecSummaryReport=null;
				}
			}
		}


		/// <summary>
		/// This method is a wrapper to the GetUserPreference method in Riskmaster.Application.ExecutiveSummaryReport.ExecutiveSummary
		/// </summary>
		/// <param name="p_objXmlIn">Input XML</param>
		/// <param name="p_objOutXML">Output XML</param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>
		/// <returns>True/False for Success/Failure of the method</returns>
		public bool GetUserPreference( XmlDocument p_objXmlIn, ref XmlDocument p_objOutXML, ref BusinessAdaptorErrors p_objErrOut )
		{
			ExecutiveSummary objExecSumm = null;
			XmlElement objElement = null ;	
			bool bIsAdmin = false ;
			int iUserId = 0 ;
			
			try
			{
				//Get the Admin Flag.
				initConstants();
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//IsAdminUser");
				if (objElement==null)
				{
                    p_objErrOut.Add("AdminflagMissing", Globalization.GetString("AdminflagMissing", base.ClientId), BusinessAdaptorErrorType.Error);					
					return false;
				}
				bIsAdmin = Conversion.ConvertStrToBool( objElement.InnerText );
				
				if( bIsAdmin )
				{
					objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//UserId");
					if (objElement==null)
					{
                        p_objErrOut.Add("UserIdMissing", Globalization.GetString("UserIdMissing", base.ClientId), BusinessAdaptorErrorType.Error);					
						return false;
					}
					iUserId = Conversion.ConvertStrToInteger( objElement.InnerText );
				}
				objExecSumm = new ExecutiveSummary( userID , DSNID , connectionString , base.ClientId); //sbh
				p_objOutXML = objExecSumm.GetUserPreference( bIsAdmin , iUserId );	
				SetSecurityAttributes(ref p_objOutXML);
				return true;
			}
            catch (InvalidCriteriaException p_objException)
            {
                throw new InvalidCriteriaException(Globalization.GetString("ExecutiveSummary.ValidateCriteria.ThresholdExceeded", base.ClientId), p_objException);
            }
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("ExecutiveSummaryAdaptor.GetUserPreference.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objExecSumm = null;	
				objElement = null ;			
			}
		}


		/// <summary>
		/// This method is a wrapper to the SaveUserPreference method in Riskmaster.Application.ExecutiveSummaryReport.ExecutiveSummary
		/// </summary>
		/// <param name="p_objXmlIn">Input XML</param>
		/// <param name="p_objOutXML">Output XML</param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>
		/// <returns>True/False for Success/Failure of the method</returns>
		public bool SaveUserPreference( XmlDocument p_objXmlIn, ref XmlDocument p_objOutXML, ref BusinessAdaptorErrors p_objErrOut )
		{
			ExecutiveSummary objExecSumm = null;				
			
			try
			{
				objExecSumm = new ExecutiveSummary( userID , DSNID, connectionString, base.ClientId ); //sbh
				objExecSumm.SaveUserPreference( p_objXmlIn );				
				return true;
			}
            catch (InvalidCriteriaException p_objException)
            {
                throw new InvalidCriteriaException(Globalization.GetString("ExecutiveSummary.ValidateCriteria.ThresholdExceeded", base.ClientId), p_objException);
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("ExecutiveSummaryAdaptor.SaveUserPreference.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objExecSumm = null;								
			}
		}

		private void SetSecurityAttributes( ref XmlDocument p_objOutXML)
		{
			if(!userLogin.IsAllowedEx( RMB_EXECUTIVE_SUMMARY , RMO_ACCESS ))
			{
				XmlNode rootNode = p_objOutXML.SelectSingleNode("//ExecutiveSummaryConfig");
				foreach( XmlNode tempNode in rootNode.ChildNodes)
				{
					SetSecurity( ref p_objOutXML, tempNode,"true");
				}
			}
			else
			{
				if(!userLogin.IsAllowedEx( RMB_EVENT , RMO_ACCESS ))
				{
					XmlNode rootNode = p_objOutXML.SelectSingleNode("//Event");
					foreach( XmlNode tempNode in rootNode.ChildNodes)
					{
						SetSecurity( ref p_objOutXML, tempNode,"true");
					}
				}
				else
				{
					SetSecurityFlags( ref p_objOutXML,"Event");
					SetSecurityFlags( ref p_objOutXML,"Event/MedwatchInfo");
					SetSecurityFlags( ref p_objOutXML,"Event/PersonInvolved");
                    SetSecurityFlags(ref p_objOutXML, "Event/EnhancedNotes_Event"); //pmittal5 MITS 13517 12/03/08
				}
				if(!userLogin.IsAllowedEx( RMB_CLAIMSALL , RMO_ACCESS ))
				{
					XmlNode rootNode = p_objOutXML.SelectSingleNode("//AllClaims");
					foreach( XmlNode tempNode in rootNode.ChildNodes)
					{
						SetSecurity( ref p_objOutXML, tempNode,"true");
					}
				}
				else
				{
					SetSecurityFlags( ref p_objOutXML,"AllClaims");
					SetSecurityFlags( ref p_objOutXML,"AllClaims/FundsManagement");
					SetSecurityFlags( ref p_objOutXML,"AllClaims/AdjusterInformation");
					SetSecurityFlags( ref p_objOutXML,"AllClaims/AdjusterDatedText");
					SetSecurityFlags( ref p_objOutXML,"AllClaims/LitigationInformation");
                    SetSecurityFlags(ref p_objOutXML, "AllClaims/SubrogationInformation");
					SetSecurityFlags( ref p_objOutXML,"AllClaims/DefendantInformation");
                    SetSecurityFlags(ref p_objOutXML, "AllClaims/EnhancedNotes_AllClaims");  //pmittal5 MITS 13517 12/03/08
				}
				if(!userLogin.IsAllowedEx( RMB_CLAIMSGC , RMO_ACCESS ))
				{
					XmlNode rootNode = p_objOutXML.SelectSingleNode("//GCClaims");
					foreach( XmlNode tempNode in rootNode.ChildNodes)
					{
						SetSecurity( ref p_objOutXML, tempNode,"true");
					}
				}
				else
				{
					SetSecurityFlags( ref p_objOutXML,"GCClaims");
					SetSecurityFlags( ref p_objOutXML,"GCClaims/ClaimantInformation_GC");
					SetSecurityFlags( ref p_objOutXML,"GCClaims/ClaimantFundsManagement_GC");
				}
				if(!userLogin.IsAllowedEx( RMB_CLAIMSVA , RMO_ACCESS ))
				{
					XmlNode rootNode = p_objOutXML.SelectSingleNode("//VAClaims");
					foreach( XmlNode tempNode in rootNode.ChildNodes)
					{
						SetSecurity(ref p_objOutXML, tempNode,"true");
					}
				}
				else
				{
					SetSecurityFlags( ref p_objOutXML,"VAClaims");
					SetSecurityFlags( ref p_objOutXML,"VAClaims/ClaimantInformation_VA");
					SetSecurityFlags( ref p_objOutXML,"VAClaims/ClaimantFundsManagement_VA");
					SetSecurityFlags( ref p_objOutXML,"VAClaims/VehicleAccident");
					SetSecurityFlags( ref p_objOutXML,"VAClaims/UnitInformation");
					SetSecurityFlags( ref p_objOutXML,"VAClaims/UnitLevelFunds");
				}
				if(!userLogin.IsAllowedEx( RMB_CLAIMSWC , RMO_ACCESS ))
				{
					XmlNode rootNode = p_objOutXML.SelectSingleNode("//WCClaims");
					foreach( XmlNode tempNode in rootNode.ChildNodes)
					{
						SetSecurity( ref p_objOutXML, tempNode,"true");
					}
				}
				else
				{
					SetSecurityFlags( ref p_objOutXML,"WCClaims");
					SetSecurityFlags( ref p_objOutXML,"WCClaims/EmployeeInformation_WC");
					SetSecurityFlags( ref p_objOutXML,"WCClaims/EmploymentInformation_WC");
					SetSecurityFlags( ref p_objOutXML,"WCClaims/CaseManagementInformation_WC");
					SetSecurityFlags( ref p_objOutXML,"WCClaims/CaseManagerInformation_WC");
				}
				if(!userLogin.IsAllowedEx( RMB_CLAIMSDI , RMO_ACCESS ))
				{
					XmlNode rootNode = p_objOutXML.SelectSingleNode("//DIClaims");
					foreach( XmlNode tempNode in rootNode.ChildNodes)
					{
						SetSecurity( ref p_objOutXML, tempNode,"true");
					}
				}
				else
				{
					SetSecurityFlags( ref p_objOutXML,"DIClaims");
					SetSecurityFlags( ref p_objOutXML,"DIClaims/EmployeeInformation_DI");
					SetSecurityFlags( ref p_objOutXML,"DIClaims/EmploymentInformation_DI");
					SetSecurityFlags( ref p_objOutXML,"DIClaims/CaseManagementInformation_DI");
					SetSecurityFlags( ref p_objOutXML,"DIClaims/CaseManagerInformation_DI");
				}

                // Sumit - 03/09/2008 Added for Policy security flags - MITS 13153
                if (!userLogin.IsAllowedEx(RMB_POLICY, RMO_ACCESS))
                {
                    XmlNode rootNode = p_objOutXML.SelectSingleNode("//Policy");
                    foreach (XmlNode tempNode in rootNode.ChildNodes)
                    {
                        SetSecurity(ref p_objOutXML, tempNode, "true");
                    }
                }
                else
                {
                    SetSecurityFlags(ref p_objOutXML, "Policy");
                    SetSecurityFlags(ref p_objOutXML, "Policy/PolicyInformation");
                    SetSecurityFlags(ref p_objOutXML, "Policy/InsuredInformation");
                }
                //Sumit - MITS#18145- 10/21/2009 - Claims (PC) tab Access
                if (!userLogin.IsAllowedEx(RMB_CLAIMSPC, RMO_ACCESS))
                {
                    XmlNode rootNode = p_objOutXML.SelectSingleNode("//PCClaims");
                    foreach (XmlNode tempNode in rootNode.ChildNodes)
                    {
                        SetSecurity(ref p_objOutXML, tempNode, "true");
                    }
                }
                else
                {
                    SetSecurityFlags(ref p_objOutXML, "PCClaims");
                    SetSecurityFlags(ref p_objOutXML, "PCClaims/ClaimantInformation_PC");
                    SetSecurityFlags(ref p_objOutXML, "PCClaims/ClaimantFundsManagement_PC");
                    SetSecurityFlags(ref p_objOutXML, "PCClaims/PropertyInformation");
                }


                //Gagan for MITS 20375
                if (!userLogin.IsAllowedEx(RMB_POLICYMGMT, RMO_ACCESS))
                {
                    XmlNode rootNode = p_objOutXML.SelectSingleNode("//PolicyMgmt");
                    foreach (XmlNode tempNode in rootNode.ChildNodes)
                    {
                        SetSecurity(ref p_objOutXML, tempNode, "true");
                    }
                }
                else
                {
                    SetSecurityFlags(ref p_objOutXML, "PolicyMgmt");
                    SetSecurityFlags(ref p_objOutXML, "PolicyMgmt/PolicyInfo");
                    SetSecurityFlags(ref p_objOutXML, "PolicyMgmt/PolicyCvgs");
                    SetSecurityFlags(ref p_objOutXML, "PolicyMgmt/PolicyExposure");
                    SetSecurityFlags(ref p_objOutXML, "PolicyMgmt/PolicyInsured");
                }
                                
                
			}
		}
		private void SetSecurity( ref XmlDocument p_objOutXml,XmlNode rootNode,string attribval )
		{
			XmlAttribute tempAttribute = p_objOutXml.CreateAttribute("readonly");
			tempAttribute.Value = attribval;
			rootNode.Attributes.Append(tempAttribute);
		}

		private void SetSecurityFlags( ref XmlDocument p_objOutXml,string a_xPath )
		{
			string parentNode=a_xPath;
			if( a_xPath.IndexOf("/")!=-1)
				parentNode = a_xPath.Substring( a_xPath.IndexOf("/")+1);
			
			string nodeName = a_xPath;
			string xPath="";
			if ( a_xPath.LastIndexOf("/")!=-1)
				nodeName= a_xPath.Substring( a_xPath.LastIndexOf("/")+1);
			Hashtable tempHash = (Hashtable)securityConstantsHash[nodeName];
			IDictionaryEnumerator enu = tempHash.GetEnumerator();
			while(enu.MoveNext())
			{
				int iSecCodeParent = Convert.ToInt32(((Hashtable)securityConstantsHash["ParentSecurityCodes"])[parentNode]) ;
				int iSecCodeChild =  Convert.ToInt32( enu.Value.ToString());
				if( a_xPath.IndexOf("/")!=-1)
					a_xPath = a_xPath.Substring( 0,a_xPath.IndexOf("/"));
				xPath = "//"+a_xPath+"/"+enu.Key;
				if(!userLogin.IsAllowedEx( iSecCodeParent , iSecCodeChild ))
				{
					XmlNode currentNode = p_objOutXml.SelectSingleNode(xPath);
					XmlAttribute tempAttribute = p_objOutXml.CreateAttribute("readonly");
					tempAttribute.Value = "true";
					currentNode.Attributes.Append(tempAttribute);
				}
				else
				{
					XmlNode currentNode = p_objOutXml.SelectSingleNode(xPath);
					XmlAttribute tempAttribute = p_objOutXml.CreateAttribute("readonly");
					tempAttribute.Value = "false";
					currentNode.Attributes.Append(tempAttribute);
				}
			}
		}

		private void initConstants()
		{
			securityConstantsHash = new Hashtable();
			Hashtable tempHash = new Hashtable();
			tempHash.Add("Event",RMB_EVENT);
			tempHash.Add("AllClaims",RMB_CLAIMSALL);
			tempHash.Add("GCClaims",RMB_CLAIMSGC);
			tempHash.Add("VAClaims",RMB_CLAIMSVA);
			tempHash.Add("WCClaims",RMB_CLAIMSWC);
			tempHash.Add("DIClaims",RMB_CLAIMSDI);
            tempHash.Add("PCClaims", RMB_CLAIMSPC);//Sumit - MITS#18145- 10/21/2009 - Add PC constant to hash table
			tempHash.Add("MedwatchInfo",RMB_MEDWATCH_INFORMATION);
			tempHash.Add("PersonInvolved",RMB_PERSON_INVOLVED);
			tempHash.Add("FundsManagement",RMB_FUNDS_MANAGEMENT);
			tempHash.Add("AdjusterInformation",RMB_ADJUSTER_INFORMATION);
			tempHash.Add("AdjusterDatedText", RMB_ADJUSTER_DATED_TEXT);
			tempHash.Add("LitigationInformation",RMB_LITIGATION_INFORMATION);
            tempHash.Add("SubrogationInformation", RMB_SUBROGATION_INFORMATION);
			tempHash.Add("DefendantInformation",RMB_DEPENDANT_INFORMATION);
			tempHash.Add("ClaimantInformation_GC",RMB_CLAIMSGC_CLAIMANT_INFORMATION);
			tempHash.Add("ClaimantFundsManagement_GC",RMB_CLAIMSGC_CLAIMANT_FUNDS_MANAGEMENT);
			tempHash.Add("ClaimantInformation_VA",RMB_CLAIMSVA_CLAIMANT_INFORMATION);
			tempHash.Add("ClaimantFundsManagement_VA",RMB_CLAIMSVA_CLAIMANT_FUNDS_MANAGEMENT);
            tempHash.Add("ClaimantInformation_PC", RMB_CLAIMSPC_CLAIMANT_INFORMATION);//Sumit - MITS#18145- 10/21/2009
            tempHash.Add("ClaimantFundsManagement_PC", RMB_CLAIMSPC_CLAIMANT_FUNDS_MANAGEMENT);//Sumit - MITS#18145- 10/21/2009
			tempHash.Add("VehicleAccident",RMB_VEHICLE_ACCIDENT);
            tempHash.Add("PropertyInformation", RMB_PROPERTY_INFORMATION);//Sumit - MITS#18145- 10/21/2009
			tempHash.Add("UnitInformation",RMB_UNIT_INFORMATION);
			tempHash.Add("UnitLevelFunds",RMB_UNIT_LEVEL_FUNDS);
			tempHash.Add("EmployeeInformation_WC",RMB_CLAIMSWC_EMPLOYEE_INFO);
			tempHash.Add("EmploymentInformation_WC",RMB_CLAIMSWC_EMPLOYMENT_INFO);
			tempHash.Add("CaseManagementInformation_WC",RMB_CLAIMSWC_CASEMANAGEMENT);
			tempHash.Add("CaseManagerInformation_WC",RMB_CLAIMSWC_CASEMANAGER);
			tempHash.Add("EmployeeInformation_DI",RMB_CLAIMSDI_EMPLOYEE_INFO);
			tempHash.Add("EmploymentInformation_DI",RMB_CLAIMSDI_EMPLOYMENT_INFO);
			tempHash.Add("CaseManagementInformation_DI",RMB_CLAIMSDI_CASEMANAGEMENT);
			tempHash.Add("CaseManagerInformation_DI",RMB_CLAIMSDI_CASEMANAGER);
            //pmittal5 MITS 13517 12/03/08
            tempHash.Add("EnhancedNotes_Event", RMB_ENHANCED_NOTES);
            tempHash.Add("EnhancedNotes_AllClaims", RMB_CLAIMSALL_ENHANCED_NOTES);
            //End - pmittal5
            // Sumit - 05/09/2008 Policy Info - MITS 13153
            tempHash.Add("Policy", RMB_POLICY);
            tempHash.Add("PolicyInformation", RMB_POLICY_POLICYINFORMATION);
            tempHash.Add("InsuredInformation", RMB_POLICY_INSUREDINFORMATION);

            //gbhatnagar changed for MITS 20375
            tempHash.Add("PolicyMgmt", RMB_POLICYMGMT);
            tempHash.Add("PolicyInfo", RMB_POLICYMGMT_POLICYINFORMATION);
            tempHash.Add("PolicyInsured", RMB_POLICYMGMT_POLICYINSURED);
            tempHash.Add("PolicyCvgs", RMB_POLICYMGMT_POLICYCOVERAGES);
            tempHash.Add("PolicyExposure", RMB_POLICYMGMT_POLICYEXPOSURE); 
            
			securityConstantsHash.Add("ParentSecurityCodes",tempHash);

			//Event
			tempHash = new Hashtable();
			tempHash.Add("EventInformation",RMO_EVENT_INFORMATION);
			tempHash.Add("EventDetail",RMO_EVENT_DETAIL);
			tempHash.Add("EventDatedText",RMO_EVENT_DATED_TEXT);
			tempHash.Add("ReportedInfo",RMO_REPORTED_INFORMATION);
			tempHash.Add("FollowupInfo",RMO_FOLLOW_UP_INFORMATION);
			tempHash.Add("OshaInfo",RMO_OSHA_INFORMATION);
			tempHash.Add("ClaimInfo",RMO_CLAIM_INFORMATION);
			tempHash.Add("MedwatchInfo",RMO_MEDWATCH_INFORMATION);
			tempHash.Add("PersonInvolved",RMO_PERSON_INVOLVED);
			tempHash.Add("QualityManagement",RMO_QUALITY_MANAGEMENT);
			tempHash.Add("FallInfo",RMO_FALL_INFORMATION);
			tempHash.Add("Comment",RMO_COMMENTS);
			tempHash.Add("DiaryDetails",RMO_DIARY_DETAILS);
			tempHash.Add("Supp",RMO_SUPPLEMENTAL_FIELDS);
			tempHash.Add("Intervention", RMO_INTERVENTION);
            tempHash.Add("EnhancedNotes", RMO_ENHANCED_NOTES);  //pmittal5 MITS 13517 12/03/08
			securityConstantsHash.Add("Event",tempHash);
					
					tempHash = new Hashtable();
					tempHash.Add("EquipIndicator",RMO_EQUIPMENT_INDICATOR);
					tempHash.Add("MedicationIndicator",RMO_MEDICATION_INDICATOR);
					tempHash.Add("ConcomitantProduct",RMO_CONCOMITANT_PRODUCT);
					tempHash.Add("MedwatchTest ",RMO_MEDWATCH_TEST);
					securityConstantsHash.Add("MedwatchInfo",tempHash);

					tempHash = new Hashtable();
					tempHash.Add("Patient",RMO_PATIENTS_INVOLVED);
					tempHash.Add("Physicians",RMO_PHYSICIANS);
					tempHash.Add("OtherMedicalStaff",RMO_OTHER_MEDICAL_STAFF);
					tempHash.Add("OtherPersons",RMO_OTHER_PERSONS);
					securityConstantsHash.Add("PersonInvolved",tempHash);

                    //pmittal5 MITS 13517 12/03/08
                    tempHash = new Hashtable();
                    tempHash.Add("ActivityDate", RMO_ACTIVITY_DATE);
                    tempHash.Add("DateCreated", RMO_DATE_CREATED);
                    tempHash.Add("EnteredBy", RMO_ENTERED_BY);
                    tempHash.Add("NoteType", RMO_NOTE_TYPE);
                    tempHash.Add("UserType", RMO_USER_TYPE);
                    tempHash.Add("NoteText", RMO_NOTE_TEXT);
                    tempHash.Add("Subject", RMO_SUBJECT);  //zmohammad MITS 31048
                    securityConstantsHash.Add("EnhancedNotes_Event", tempHash);
                    //End - pmittal5

			//claim All
			tempHash = new Hashtable();
			tempHash.Add("ClaimInformation",RMO_CLAIMSALL_INFORMATION);
			tempHash.Add("ClaimLevelReserves",RMO_CLAIM_LEVEL_RESERVES);
			tempHash.Add("FundsManagement",RMO_FUNDS_MANAGEMENT);
			tempHash.Add("PaymentHistory",RMO_PAYMENT_HISTORY);
			tempHash.Add("EventDetail",RMO_CLAIMSALL_EVENT_DETAIL);
			tempHash.Add("PersonsInvolved",RMO_PERSONS_INVOLVED);
			tempHash.Add("AdjusterInformation",RMO_ADJUSTER_INFORMATION);
			tempHash.Add("LitigationInformation",RMO_LITIGATION_INFORMATION);
            tempHash.Add("SubrogationInformation", RMO_SUBROGATION_INFORMATION);
			tempHash.Add("DefendantInformation",RMO_DEPENDANT_INFORMATION);
			tempHash.Add("DiaryDetails",RMO_DIARY_DETAILS);
			tempHash.Add("SupplementalFields",RMO_SUPPLEMENTAL_FIELDS);
			tempHash.Add("Comments",RMO_COMMENTS);
            tempHash.Add("EnhancedNotes", RMO_CLAIMSALL_ENHANCED_NOTES);  //pmittal5 MITS 13517 12/03/08
			securityConstantsHash.Add("AllClaims",tempHash);
	
					tempHash = new Hashtable();
					tempHash.Add("TransactionDetail",RMO_TRANSACTION_DETAIL);
					securityConstantsHash.Add("FundsManagement",tempHash);

					tempHash = new Hashtable();
					tempHash.Add("AdjusterDatedText",RMO_ADJUSTER_DATED_TEXT);
					securityConstantsHash.Add("AdjusterInformation",tempHash);

						tempHash = new Hashtable();
						tempHash.Add("EnteredUserFullName", RMO_ENTEREDUSERFULLNAME);
						securityConstantsHash.Add("AdjusterDatedText",tempHash);

					tempHash = new Hashtable();
					tempHash.Add("AttorneyInformation",RMO_ATTORNEY_INFORMATION);
					tempHash.Add("ExpertWitness",RMO_EXPERT_WITNESS);
                    tempHash.Add("LitDemandOffer", RMO_LIT_DEMAND_OFFER);
					securityConstantsHash.Add("LitigationInformation",tempHash);

                    tempHash = new Hashtable();
                    tempHash.Add("SubroDemandOffer", RMO_SUBRO_DEMAND_OFFER);
                    securityConstantsHash.Add("SubrogationInformation", tempHash);

					tempHash = new Hashtable();
					tempHash.Add("DefAttorneyInformation",RMO_ATTORNEY_INFORMATION);
					tempHash.Add("ClaimInvolvementHistory",RMO_CLAIMALL_INVOLVEMENT_HISTORY);
					securityConstantsHash.Add("DefendantInformation",tempHash);

                    //pmittal5 MITS 13517 12/03/08
                    tempHash = new Hashtable();
                    tempHash.Add("ActivityDate", RMO_CLAIMSALL_ACTIVITY_DATE);
                    tempHash.Add("DateCreated", RMO_CLAIMSALL_DATE_CREATED);
                    tempHash.Add("EnteredBy", RMO_CLAIMSALL_ENTERED_BY);
                    tempHash.Add("NoteType", RMO_CLAIMSALL_NOTE_TYPE);
                    tempHash.Add("UserType", RMO_CLAIMSALL_USER_TYPE);
                    tempHash.Add("NoteText", RMO_CLAIMSALL_NOTE_TEXT);
                    tempHash.Add("Subject", RMO_CLAIMSALL_SUBJECT);  //zmohammad MITS 31048
                    securityConstantsHash.Add("EnhancedNotes_AllClaims", tempHash);
                    //End - pmittal5

			//Claim GC
			tempHash = new Hashtable();
			tempHash.Add("ClaimantInformation",RMO_CLAIMSGC_CLAIMANT_INFORMATION);
			securityConstantsHash.Add("GCClaims",tempHash);

					tempHash = new Hashtable();
					tempHash.Add("ClaimInvolvementHistory",RMO_CLAIM_INVOLVEMENT_HISTORY);
					tempHash.Add("ClaimantLevelReserves",RMO_CLAIMANT_LEVEL_RESERVES);
					tempHash.Add("ClaimantPaymentHistory",RMO_CLAIMANT_PAYMENT_HISTORY);
					tempHash.Add("ClaimantFundsManagement",RMO_CLAIMSGC_CLAIMANT_FUNDS_MANAGEMENT);
					tempHash.Add("AttorneyInformation",RMO_GC_ATTORNEY_INFORMATION);
					securityConstantsHash.Add("ClaimantInformation_GC",tempHash);

					tempHash = new Hashtable();
					tempHash.Add("TransactionDetail",RMO_TRANSACTION_DETAILS);
					securityConstantsHash.Add("ClaimantFundsManagement_GC",tempHash);

			//Claim VA
			tempHash = new Hashtable();
			tempHash.Add("VehicleAccident",RMO_VEHICLE_ACCIDENT);
			tempHash.Add("ClaimantInformation",RMO_CLAIMSVA_CLAIMANT_INFORMATION);
			securityConstantsHash.Add("VAClaims",tempHash);

					tempHash = new Hashtable();
					tempHash.Add("ClaimInvolvementHistory",RMO_CLAIM_INVOLVEMENT_HISTORY);
					tempHash.Add("ClaimantLevelReserves",RMO_CLAIMANT_LEVEL_RESERVES);
					tempHash.Add("ClaimantPaymentHistory",RMO_CLAIMANT_PAYMENT_HISTORY);
					tempHash.Add("ClaimantFundsManagement",RMO_CLAIMSGC_CLAIMANT_FUNDS_MANAGEMENT);
					tempHash.Add("AttorneyInformation",RMO_VA_ATTORNEY_INFORMATION);
					securityConstantsHash.Add("ClaimantInformation_VA",tempHash);

					tempHash = new Hashtable();
					tempHash.Add("TransactionDetail",RMO_VA_CLAIMANT_TRANSDETAIL);
					securityConstantsHash.Add("ClaimantFundsManagement_VA",tempHash);

					tempHash = new Hashtable();
					tempHash.Add("UnitInformation",RMO_UNIT_INFORMATION);
					securityConstantsHash.Add("VehicleAccident",tempHash);

					tempHash = new Hashtable();
					tempHash.Add("ClaimInvolvementHistoryUnit",RMO_UNIT_CLAIM_INVOLVEMENT_HISTORY);
					tempHash.Add("PaymentHistoryForUnit",RMO_PAYMENT_HISTORY_FOR_UNITS);
					tempHash.Add("UnitLevelReserves",RMO_UNIT_LEVEL_RESOURCES);
					tempHash.Add("UnitLevelFunds",RMO_UNIT_LEVEL_FUNDS);
					securityConstantsHash.Add("UnitInformation",tempHash);

					tempHash = new Hashtable();
					tempHash.Add("TransactionDetailUnitLevel",RMO_UNIT_LEVEL_FUNDS_TRANSACTION_DETAILS);
					securityConstantsHash.Add("UnitLevelFunds",tempHash);

			//Claims WC
			tempHash = new Hashtable();
			tempHash.Add("EmployeeInformation",RMO_CLAIMSWC_EMPLOYEE_INFO);
			tempHash.Add("OSHAInformation",RMO_OSHA_INFO);
			tempHash.Add("JurisdictionalSupplementals",RMO_JURIS_SUPP);
			tempHash.Add("CaseManagementInformation", RMO_CLAIMSWC_CASEMANAGEMENT);
			tempHash.Add("TreatmentPlan", RMO_CLAIMSWC_TREATMENTPLANT );
			tempHash.Add("MedicalManagementSavings", RMO_CLAIMSWC_MEDMANAGEMENTSVAING);
			tempHash.Add("Accommodations", RMO_CLAIMSWC_ACCOMMODATIONS);
			tempHash.Add("VocationalRehabilitation", RMO_CLAIMSWC_VOCREHAB);
			securityConstantsHash.Add("WCClaims",tempHash);

					tempHash = new Hashtable();
					tempHash.Add("EmploymentInformation",RMO_CLAIMSWC_EMPLOYMENT_INFO);
					tempHash.Add("ClaimInvolvementHistory",RMO_CLAIM_INVOLVEMENT_INFO);
					securityConstantsHash.Add("EmployeeInformation_WC",tempHash);

					tempHash = new Hashtable();
					tempHash.Add("DependentInformation",RMO_DEPENDENT_INFO);
					tempHash.Add("RestrictedDays",RMO_RESTRICTED_DAYS);
					tempHash.Add("LostDays",RMO_LOST_DAYS);
					tempHash.Add("EmployeeEventDetail",RMO_EMPLOYEE_EVENT_DETAILS);
					securityConstantsHash.Add("EmploymentInformation_WC",tempHash);

					tempHash = new Hashtable();
					tempHash.Add("CaseManagerInformation", RMO_CLAIMSWC_CASEMANAGER);
					tempHash.Add("RestrictedWork", RMO_CLAIMSWC_RESTRICTEDWORK);
					tempHash.Add("WorkLoss", RMO_CLAIMSWC_WORKLOSS);
					securityConstantsHash.Add("CaseManagementInformation_WC", tempHash);

					tempHash = new Hashtable();
					tempHash.Add("CaseManagerNotes", RMO_CLAIMSWC_CASEMANAGERNOTES);
					securityConstantsHash.Add("CaseManagerInformation_WC", tempHash);

			//Claims DI
			tempHash = new Hashtable();
			tempHash.Add("EmployeeInformation",RMO_CLAIMSDI_EMPLOYEE_INFO);
			tempHash.Add("PlanInfo",RMO_CLAIMSDI_PLANINFO);
			tempHash.Add("CaseManagementInformation", RMO_CLAIMSDI_CASEMANAGEMENT);
			tempHash.Add("TreatmentPlan", RMO_CLAIMSDI_TREATMENTPLAN );
			tempHash.Add("MedicalManagementSavings", RMO_CLAIMSDI_MEDICALMANAGEMENT);
			tempHash.Add("Accommodations", RMO_CLAIMSDI_ACCOMMODATIONS);
			tempHash.Add("VocationalRehabilitation", RMO_CLAIMSDI_VOCREHAB);
            //Geeta 02/21/07 : Leave Info Flag Added for FMLA Enhancement
            tempHash.Add("LeaveInfo", RMO_CLAIMSDI_LEAVE);

			securityConstantsHash.Add("DIClaims",tempHash);

					tempHash = new Hashtable();
					tempHash.Add("EmploymentInformation",RMO_CLAIMSWC_EMPLOYMENT_INFO);
					tempHash.Add("ClaimInvolvementHistory",RMO_CLAIM_INVOLVEMENT_INFO);
					securityConstantsHash.Add("EmployeeInformation_DI",tempHash);

					tempHash = new Hashtable();
					tempHash.Add("DependentInformation",RMO_DEPENDENT_INFO);
					tempHash.Add("RestrictedDays",RMO_RESTRICTED_DAYS);
					tempHash.Add("LostDays",RMO_LOST_DAYS);
					tempHash.Add("EmployeeEventDetail",RMO_EMPLOYEE_EVENT_DETAILS);
					securityConstantsHash.Add("EmploymentInformation_DI",tempHash);

					tempHash = new Hashtable();
					tempHash.Add("CaseManagerInformation", RMO_CLAIMSDI_CASEMANAGER);
					tempHash.Add("RestrictedWork", RMO_CLAIMSDI_RESTRICTEDWORK);
					tempHash.Add("WorkLoss", RMO_CLAIMSDI_WORKLOSS);
					securityConstantsHash.Add("CaseManagementInformation_DI", tempHash);

					tempHash = new Hashtable();
					tempHash.Add("CaseManagerNotes", RMO_CLAIMSDI_CASEMANAGERNOTES);
					securityConstantsHash.Add("CaseManagerInformation_DI", tempHash);

                    // Sumit - 03/09/2008 Policy Info - MITS 13153
                    tempHash = new Hashtable();
                    tempHash.Add("PolicyInformation", RMO_POLICY_POLICYINFORMATION);
                    tempHash.Add("InsuredInformation", RMO_POLICY_INSUREDINFORMATION);
                    tempHash.Add("InsuranceCompanyName", RMO_POLICY_INSURANCECOMPANYNAME);
                    securityConstantsHash.Add("Policy", tempHash);

                    tempHash = new Hashtable();
                    tempHash.Add("PolicyNumber", RMO_POLICY_POLICYNUMBER);
                    tempHash.Add("EffectiveDate", RMO_POLICY_EFFECTIVEDATE);
                    tempHash.Add("ExpirationDate", RMO_POLICY_EXPIRATIONDATE);
                    tempHash.Add("ClaimLimit", RMO_POLICY_CLAIMLIMIT);
                    tempHash.Add("PolicyLimit", RMO_POLICY_POLICYLIMIT);
                    tempHash.Add("PercentageofAggregate", RMO_POLICY_PERCENTAGEOFAGGREGATE);
                    securityConstantsHash.Add("PolicyInformation", tempHash);

                    tempHash = new Hashtable();
                    tempHash.Add("Insured", RMO_POLICY_INSURED);
                    tempHash.Add("Address", RMO_POLICY_ADDRESS);
                    tempHash.Add("DepartmentAbbreviation", RMO_POLICY_DEPARTMENTABBREVIATION);
                    securityConstantsHash.Add("InsuredInformation", tempHash);
                    //Sumit - MITS#18145- 10/21/2009 - Claim PC
                    tempHash = new Hashtable();
                    tempHash.Add("PropertyInformation", RMO_PROPERTY_INFORMATION);
                    tempHash.Add("ClaimantInformation", RMO_CLAIMSPC_CLAIMANT_INFORMATION);
                    securityConstantsHash.Add("PCClaims", tempHash);

                    tempHash = new Hashtable();
                    tempHash.Add("ClaimInvolvementHistory", RMO_PC_CLAIM_INVOLVEMENT_HISTORY);
                    tempHash.Add("ClaimantLevelReserves", RMO_PC_CLAIMANT_LEVEL_RESERVES);
                    tempHash.Add("ClaimantPaymentHistory", RMO_PC_CLAIMANT_PAYMENT_HISTORY);
                    tempHash.Add("ClaimantFundsManagement", RMO_CLAIMSPC_CLAIMANT_FUNDS_MANAGEMENT);
                    tempHash.Add("AttorneyInformation", RMO_PC_ATTORNEY_INFORMATION);
                    securityConstantsHash.Add("ClaimantInformation_PC", tempHash);

                    tempHash = new Hashtable();
                    tempHash.Add("TransactionDetail", RMO_PC_TRANSACTION_DETAILS);
                    securityConstantsHash.Add("ClaimantFundsManagement_PC", tempHash);
                    
                    //smahajan6 - MITS# 18230 - 12/30/2009 :Start
                    tempHash = new Hashtable();
                    tempHash.Add("COPEData", RMO_COPE_DATA);
                    tempHash.Add("OptionalCOPEData", RMO_OPTIONAL_COPE_DATA);
                    tempHash.Add("Schedule", RMO_SCHEDULE);
                    securityConstantsHash.Add("PropertyInformation", tempHash);
                    //smahajan6 - MITS# 18230 - 12/30/2009 :End

                    //gbhatnagar 7/30/2010 : MITS 20375
                    tempHash = new Hashtable();
                    tempHash.Add("PolicyInfo", RMO_POLICYMGMT_POLICYINFORMATION);
                    tempHash.Add("PolicyCvgs", RMO_POLICYMGMT_POLICYCOVERAGES);
                    tempHash.Add("PolicyExposure", RMO_POLICYMGMT_POLICYEXPOSURE);
                    securityConstantsHash.Add("PolicyMgmt", tempHash);

                    tempHash = new Hashtable();
                    tempHash.Add("PolicyName", RMO_POLICYMGMT_POLICYNAME);
                    tempHash.Add("PolicyNumber", RMO_POLICYMGMT_POLICYNUMBER);
                    tempHash.Add("PolicyStatus", RMO_POLICYMGMT_POLICYSTATUS);
                    tempHash.Add("PolicyType", RMO_POLICYMGMT_POLICYTYPE);
                    tempHash.Add("PolicyState", RMO_POLICYMGMT_POLICYSTATE);
                    tempHash.Add("EffecDate", RMO_POLICYMGMT_POLICYEFFECTIVEDATE);
                    tempHash.Add("ExpiDate", RMO_POLICYMGMT_POLICYEXPIRATIONDATE);
                    tempHash.Add("TotalBilledPremium", RMO_POLICYMGMT_POLICYTOTALBILLEDPREMIUM);
                    tempHash.Add("PolicyInsured", RMO_POLICYMGMT_POLICYINSURED);
                    tempHash.Add("PolicyInsurer", RMO_POLICYMGMT_POLICYINSURER);
                    securityConstantsHash.Add("PolicyInfo", tempHash);
                    
                    tempHash = new Hashtable();
                    tempHash.Add("PolicyInsuredName", RMO_POLICYMGMT_POLICYINSUREDNAME);
                    tempHash.Add("PolicyInsuredAddress", RMO_POLICYMGMT_POLICYINSUREDADDRESS );
                    securityConstantsHash.Add("PolicyInsured", tempHash);
                    
                    
                    tempHash = new Hashtable();
                    tempHash.Add("PolicyCvgType", RMO_POLICYMGMT_POLICYCOVERAGETYPE);
                    tempHash.Add("PolicyCvgDesc", RMO_POLICYMGMT_POLICYCOVERAGEDESC);
                    tempHash.Add("PolicyLimit", RMO_POLICYMGMT_POLICYLIMIT);
                    tempHash.Add("ClaimLimit", RMO_POLICYMGMT_CLAIMLIMIT);
                    tempHash.Add("OccurrenceLimit", RMO_POLICYMGMT_OCCURRENCELIMIT);
                    tempHash.Add("TotalPayment", RMO_POLICYMGMT_TOTALPAYMENT);
                    securityConstantsHash.Add("PolicyCvgs", tempHash);


                    tempHash = new Hashtable();
                    tempHash.Add("ExposureType", RMO_POLICYMGMT_POLICYEXPOSURETYPE);
                    tempHash.Add("ExposureDesc", RMO_POLICYMGMT_POLICYEXPOSUREDESC);
                    tempHash.Add("ExposureAmount", RMO_POLICYMGMT_POLICYEXPOSUREAMOUNT);
                    tempHash.Add("Rate", RMO_POLICYMGMT_POLICYEXPOSURERATE);
                    tempHash.Add("BaseRate", RMO_POLICYMGMT_POLICYEXPOSUREBASERATE );
                    
                    securityConstantsHash.Add("PolicyExposure", tempHash);            
               
		}
        //Amandeep  Mobile Adjuster
        private XElement GetPreferenceTemplate()
        {            
            XElement oTemplate = XElement.Parse(@"
            <setting>
  <DiaryConfig>
    <ShowActiveDiaryChecked selected='1' />
    <ShowNotes selected='0' />
    <Priority selected='1' />
    <Text selected='1' />
    <WorkActivity selected='1' />
    <Due selected='1' />
    <Claimant selected='1' />
    <ClaimStatus selected='1' />
    <Department selected='1' />
    <AttachedRecord selected='1' />
    <PriorityHeader value='Priority' />
    <TextHeader value='Task Name' />
    <WorkActivityHeader value='Work Activity' />
    <DueHeader value='Due' />
    <ClaimantHeader value='Claimant' />
    <ClaimStatusHeader value='Claim Status' />
    <DepartmentHeader value='Department' />
    <AttachedRecordHeader value='Attached Record' />
    <ColspanValue value='0' />
    <CalendarView value='0' />
    <UserSortOrder value='0' />
    <ShowRegarding selected='1' />
    <AssignedUser selected='1' />
    <AssigningUser selected='1' />
    <AssignedUserHeader value='Assigned User' />
    <AssigningUserHeader value='Assigning User' />
  </DiaryConfig>       
  <execsummconfig>
    <report type='execsumm'>
      <data>
        <events functionID='351000' tabIndex='1' f351010='0' f351020='0' f351030='0' f351040='0' f351050='0' f351060='0' f351070='0' f351080='0' f351090='0' f351091='0' f351092='0' f351093='0' f351094='0' f351100='1' f351110='0' f351120='0' f351130='1' f351131='1' f351132='1' f351133='1' f351134='1' f351135='1' f351136='1' f351140='0' f351150='0' f351160='1' f351170='1' f351180='1' f351190='1' f351200='1' f351210='1' f351220='1' f351380='1' />
        <allclaims functionID='352000' tabIndex='2' f352010='1' f352020='0' f352030='1' f352040='0' f352041='0' f352050='0' f352060='1' f352070='1' f352071='0' f352075='0' f352080='0' f352081='0' f352082='0' f352090='0' f352091='0' f352092='0' f352100='1' f352110='0' f352120='0' f352130='1' f352140='1' f352150='1' f352160='1' f352170='1' f352180='1' f352190='1' f352230='1'/>
        <gcclaims functionID='353000' tabIndex='3' f353010='1' f353020='0' f353030='0' f353040='0' f353050='0' f353060='0' f353061='0' />
        <vaclaims functionID='354000' tabIndex='4' f354010='1' f354020='0' f354030='0' f354040='0' f354050='0' f354060='0' f354070='0' f354080='1' f354090='1' f354100='0' f354110='0' f354120='0' f354130='0' f354140='0' />
        <wcclaims functionID='355000' tabIndex='5' f355010='1' f355020='1' f355030='1' f355040='0' f355050='0' f355060='0' f355070='0' f355080='0' f355090='0' f355100='0' f355110='0' f355120='0' f355130='0' f355140='0' f355150='0' f355160='0' f355170='0' f355180='0' />
        <diclaims functionID='356000' tabIndex='6' f356010='1' f356020='1' f356030='1' f356040='0' f356050='0' f356060='0' f356070='0' f356080='0' f356100='0' f356110='0' f356120='0' f356130='0' f356140='0' f356150='0' f356160='0' f356170='0' f356180='0' f356190='0' />
        <policy functionID='357000' tabIndex='7' f357010='1' f357011='1' f357020='1' f357030='1' f357040='1' f357050='1' f357060='1' f357070='1' f357080='1' f357090='1' f357100='1' f357110='1' f357120='1' />
        <pcclaims functionID='358000' tabIndex='8' f358010='1' f358020='0' f358030='0' f358040='0' f358050='0' f358060='0' f358070='0' f358080='1' f358090='1' f358100='1' f358110='1' />
        <policymgmt functionID='359000' tabIndex='9' f358510='1' f358511='1' f358512='1' f358513='1' f358514='1' f358515='1' f358516='1' f358517='1' f358518='1' f358522='1' f358519='1' f358520='1' f358521='1' f358530='1' f358531='1' f358532='1' f358533='1' f358534='1' f358535='1' f358536='1' f358550='1' f358551='1' f358552='1' f358553='1' f358554='1' f358555='1' />
      </data>
    </report>
  </execsummconfig>
</setting>
            ");

            return oTemplate;
        }
        //Amandeep  Mobile Adjuster
        //zmohammad MITS 31048
		#endregion
	
	}
}

