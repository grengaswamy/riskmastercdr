using System;
using System.Xml;
using Riskmaster.Application.LeaveLibrary;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.Security;


namespace Riskmaster.BusinessAdaptor
{

	///************************************************************** 
	///* $File		: LeavePlanAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 03-Jan-2007
	///* $Author	: Aashish Bhateja
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	
	/// <summary>	
	///	This class is used to call the application layer component for Leave Library.
	/// </summary>
	public class LeavePlanAdaptor:BusinessAdaptorBase
	{

		#region Constructor
        /// <summary>
        /// Constructor.
        /// </summary>
		public LeavePlanAdaptor()
		{}
		#endregion

        /// <summary>
        /// Loads all the leave plans while updating leave history and leave
        /// records from utilities.
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
		public bool LoadLeavePlans(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			LeavePlan objLeavePlan = null;			
			try
			{
				objLeavePlan = new LeavePlan(connectionString, base.ClientId);			
				p_objXmlOut = objLeavePlan.LoadLeavePlans(p_objXmlIn);
				return true;

			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException, Globalization.GetString("LeavePlanAdaptor.LoadLeavePlans.Error", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objLeavePlan = null;
			}
		}

        /// <summary>
        /// Updates the leave history from utilities.
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
		public bool UpdateLeaveHistory(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			int iLeavePlanId = 0;
			int iEmpEid = 0;
			double dHoursWorked = 0.0;
			bool bResult = false;
			XmlElement objElement = null;				//used for Xml manipulation
			string sEligDate = string.Empty;
			UpdateLeaveHistory objUpdtLeaveHist = null;			
			try
			{
				objUpdtLeaveHist = new UpdateLeaveHistory(connectionString, base.ClientId);	//rkaur27		

				//extracting the data source information from input XML
				objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "planid"); 
				
				if(objElement.InnerText != "")
					iLeavePlanId = Convert.ToInt32(objElement.InnerText);

				//-- Below line will be removed before delivering the code.
				//-- Need to set this value from the UI.
				sEligDate = Conversion.GetDate(DateTime.Now.ToString()); 

				bResult = objUpdtLeaveHist.UpdateLeaveHist(sEligDate,iLeavePlanId,iEmpEid,dHoursWorked);
				return true;

			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException, Globalization.GetString("LeavePlanAdaptor.UpdateLeaveHistory.Error", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objUpdtLeaveHist = null;
				objElement = null;
			}
		}
        /// <summary>
        /// Updates leave records from utilities.
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
		public bool UpdateLeaveRecords(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			int iLeavePlanId = 0;
			bool bResult = false;
			XmlElement objElement = null;				//used for Xml manipulation

			UpdateLeaveRecords objUpdtLeaveRec = null;			
			try
			{
				objUpdtLeaveRec = new UpdateLeaveRecords(connectionString, base.ClientId);//rkaur27			

				//extracting the data source information from input XML
				objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "planid"); 
				
				if(objElement.InnerText != "")
					iLeavePlanId = Convert.ToInt32(objElement.InnerText);

				bResult = objUpdtLeaveRec.Update(iLeavePlanId);
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("LeavePlanAdaptor.UpdateLeaveRecords.Error", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objUpdtLeaveRec = null;
				objElement = null;
			}
		}
        //public bool UpdateLeave(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
        //{
        //    int iLeavePlanId = 0;
        //    bool bResult = false;
        //    XmlElement objElement = null;				//used for Xml manipulation

        //    UpdateLeaveRecords objUpdtLeaveRec = null;			
        //    try
        //    {
        //        objUpdtLeaveRec = new UpdateLeaveRecords(connectionString);			

        //        //extracting the data source information from input XML
        //        objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "planid"); 
				
        //        if(objElement.InnerText != "")
        //            iLeavePlanId = Convert.ToInt32(objElement.InnerText);

        //        bResult = objUpdtLeaveRec.Update(iLeavePlanId);
        //        return true;

        //    }
        //    catch( RMAppException p_objException )
        //    {
        //        p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
        //        return false;
        //    }
        //    catch(Exception p_objException)
        //    {			
        //        p_objErrOut.Add(p_objException,Globalization.GetString("LeavePlanAdaptor.UpdateLeaveRecords.Error"),BusinessAdaptorErrorType.Error);
        //        return false;
        //    }
        //    finally
        //    {
        //        objUpdtLeaveRec = null;
        //        objElement = null;
        //    }
        //}

		/// <summary>
		/// Used to search the leave plans based on the given criteria.
		/// </summary>
		/// <param name="p_objXmlIn"></param>
		/// <param name="p_objXmlOut"></param>
		/// <param name="p_objErrOut"></param>
		/// <returns></returns>
        public bool GetLeavePlans(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            LeaveEmp objEmp = null;
            Generic objGeneric = new Generic(connectionString, base.ClientId);
            XmlElement objElement = null;
            XmlElement objRootElement = null;

            int iEmpId = 0;
            int iPiRowId = 0;
            int i = 0;
            long lPlanId = 0;
            long lLastPlanID = 0;
            string sEligDate = string.Empty;
            string sPlan = string.Empty;

            bool bResult = false;

            try
            {
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//EmpEid");
                if (objElement == null)
                {
                    return false;
                }
                if (objElement.InnerText.Trim() != string.Empty)
                    iPiRowId = Conversion.ConvertStrToInteger(objElement.InnerText);

                iEmpId = objGeneric.GetPiEmpID(iPiRowId);
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//EligibilityDt");
                if (objElement == null)
                {
                    return false;
                }
                if (objElement.InnerText.Trim() != string.Empty)
                    sEligDate = Conversion.ToDbDate(Convert.ToDateTime(objElement.InnerText));

                objEmp = new LeaveEmp(base.m_connectionString, base.ClientId);

                bResult = objEmp.LoadData(iEmpId, sEligDate);
                if (bResult)
                {
                    objRootElement = p_objXmlOut.CreateElement("LeavePlans");
                    if (objEmp.EmpXPlans.Count > 0)
                    {
                        foreach(string Key in objEmp.EmpXPlans.Keys())
                        //foreach (LeavePlan objPlan in objEmp.EmpXPlans.g)
                            {
                                objElement = p_objXmlOut.CreateElement("Plan");

                            objElement.SetAttribute("LPRowId", objEmp.EmpXPlans.Get(Key).LPRowId.ToString());
                            objElement.SetAttribute("PlanNumber", objEmp.EmpXPlans.Get(Key).LeavePlanCode);
                            objElement.SetAttribute("PlanName", objEmp.EmpXPlans.Get(Key).LeavePlanName);
                            objElement.SetAttribute("EffDate", Conversion.GetDBDateFormat(objEmp.EmpXPlans.Get(Key).EffectiveDate.Trim(), "d") + " - " + Conversion.GetDBDateFormat(objEmp.EmpXPlans.Get(Key).ExpirationDate.Trim(), "d"));
							objElement.SetAttribute("UseLeavesFlag", objEmp.EmpXPlans.Get(Key).UseLeavesFlag.ToString());

                                objRootElement.AppendChild(objElement);

                            }
                        p_objXmlOut.AppendChild(objRootElement);
                    }
                    return true;
                }

                return false;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("LeavePlanAdaptor.GetLeavePlans.Error", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
            {
                objElement = null;
                objRootElement = null;
                objEmp = null;
            }
        }
        /// <summary>
        ///		This method is a wrapper to Riskmaster.Application.LeaveHistory.GetLeaveHistory() method.
        ///		Gets the Leave History list.
        /// </summary>
        /// <param name="p_objXmlIn">XML containing the input parameters needed
        ///		The structure of input XML document would be:
        ///		<Document>
        ///			<LeaveHistory>
        ///				<ClaimantRowId>ClaimantRowId</ClaimantRowId>
        ///				<ClaimId>ClaimId</ClaimId>		
        ///			</LeaveHistory>
        ///		</Document>
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///		The structure of the output XML document would be
        /// </param>
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool GetLeaveHistory(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            LeaveHistory objLeaveHistory = null;		
            int iClaimantRowId;						

            XmlElement objElement = null;			

            try
            {
                //Check existence of Claimant Row Id which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//ClaimantRowId");
                if (objElement == null)
                {
                    p_objErrOut.Add("LeaveHistoryAdaptorParameterMissing", Globalization.GetString("LeaveHistoryAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                iClaimantRowId = Convert.ToInt32(objElement.InnerText.ToString());
                //Get Leave History
                objLeaveHistory = new LeaveHistory(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password,base.ClientId);//psharma206
                p_objXmlOut = objLeaveHistory.GetLeaveHistory(iClaimantRowId);

                return true;
            }
            catch (InvalidValueException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("LeavePlanAdaptor.GetLeaveHistory.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objLeaveHistory != null)
                    objLeaveHistory.Dispose();
                objElement = null;
            }
        }

	}
}
