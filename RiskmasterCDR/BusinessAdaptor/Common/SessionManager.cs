﻿
using System;
using System.Collections;
using System.Collections.Generic;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.BusinessAdaptor.Common
{
	
	/// <summary>
	/// SessionManager class contains the properties and methods to manipulate the
	/// Custom Session Object.
	/// </summary>
	public class SessionManager: IDictionary, IDisposable
	{

	    private int m_iUserData1 = 0;
	    private Hashtable m_Items = new Hashtable();
	    private Hashtable m_BinaryItems = null;
	    private string m_sConnectionString = "";
	    private string m_sSID = "";
	    private int m_RowId = 0;

        /// <summary>
        /// Constructor for SessionManager class.
        /// </summary>
        /// <param name="strSessionConnString">connection string to the RMXSession database</param>
        public SessionManager(string strSessionConnString)
        {
            m_sConnectionString = strSessionConnString;
        }

		/// <summary>
		/// Retrieves and increments the current unique id counter for "tableName" from the Session Glossary.</summary>
		/// <param name="tableName">The name of the table who's next unique row id should be retrieved and incremented.</param>
		/// <returns>Integer containing the requested unique id value.</returns>
        /// <remarks>Collision checking is unnecessary since collisions are so infrequent even 
        /// in high volume transaction databases.  This was more of an issue with MS Access and should no longer
        /// be an issue with platforms such as SQL Server or Oracle.  Collision checking for sessions simply wastes CPU resources.</remarks>
		private int GetNextUID(string tableName)
		{
            string SQL = string.Empty;
			int nextUID=0;
			int origUID=0;
			int rows=0;
			
			using(DbConnection objConn = DbFactory.GetDbConnection(this.m_sConnectionString))
	        {
                //Open the database connection
                objConn.Open();

                nextUID = Conversion.ConvertObjToInt(o, m_iClientIdbjConn.ExecuteScalar(String.Format("SELECT NEXT_UNIQUE_ID FROM SESSION_IDS WHERE SYSTEM_TABLE_NAME = '{0}'", tableName)));

                //Increment the value
                //Compute next id
				origUID = nextUID;

                nextUID++;

                //try to reserve id (searched update)
                SQL = String.Format("UPDATE SESSION_IDS SET NEXT_UNIQUE_ID = {0} WHERE SYSTEM_TABLE_NAME = '{1}'", nextUID, tableName);

                try
                {
                    rows = objConn.ExecuteNonQuery(SQL);
                } // try
                catch (System.Data.DataException ex)
                {
                    throw new RMAppException(Globalization.GetString("SessionManager.GetNextUID.Exception.ErrorModifyingDB"),ex);
                } // catch
	        }//using

            return nextUID;
		}


		
		
		/// <summary>
		/// Returns the connection string.
		/// </summary>
		public string ConnectionString
		{
			get
			{
				return m_sConnectionString;
			}
		}

		/// <summary>
		/// Checks for the existence of a session with key p_sSessionId in the current
		/// Session Database.
		/// </summary>
		/// <returns>Boolean (True if requested Session Found, False otherwise.)</returns>
		public bool SessionExists(string p_sSessionId)
		{
			bool blnSessionExists = false;

			using (DbConnection objConn = DbFactory.GetDbConnection(m_sConnectionString))
			{
                objConn.Open();

                try
                {
                    object objSID = objConn.ExecuteScalar(String.Format("SELECT SID FROM SESSIONS WHERE SID='{0}'",p_sSessionId));

                    //Verify that the result set is not null
                    if (objSID != DBNull.Value || objSID != null)
                    {
                        blnSessionExists = true;
                    } // if

                } // try
                catch(Exception e)
    			{
	    			throw new InitializationException(Globalization.GetString("SessionManager.SessoionExists.Exception"),e);
			    }
			} // using
			
			return blnSessionExists;
		}//method: SessionExists()

        /// <summary>
        /// Generates a new Session GUID to be used 
        /// for initiating session instances
        /// </summary>
        /// <returns>string containing the generated Guid</returns>
        internal string GenerateSessionGuid()
        {
            return Guid.NewGuid().ToString();
        } // method: GenerateSessionGuid


		/// <summary>
		/// Returns all the Sessions with their related information.Used by the 
		/// administrator to get the information for all the users that are logged in.
		/// </summary>
		/// <returns>An arrayList that contains the information about all the sessions.
		/// </returns>
        public ArrayList GetAllSessions()
        {
            DbConnection objConn = DbFactory.GetDbConnection(m_sConnectionString);
            DbReader objReader = null;
            ArrayList arrList;
            try
            {
                objConn.Open();
                objReader = objConn.ExecuteReader("SELECT SID,DATA,USERDATA1,LASTCHANGE FROM SESSIONS");
                arrList = new System.Collections.ArrayList();

                while (objReader.Read())
                {
                    string sData = objReader.GetString("DATA");
                    string sSID = objReader.GetString("SID");
                    SessionManager objSession = new SessionManager(m_sConnectionString);
                    objSession.InitFromString(sSID, sData);
                    int iUserData = objReader.GetInt("USERDATA1");
                    objSession.UserData1 = iUserData;
                    string sLastchanged = objReader.GetString("LASTCHANGE");
                    objSession.LastChanged = sLastchanged;
                    arrList.Add(objSession);
                }
            }
            catch (Exception e)
            {
                throw new DataModelException(Globalization.GetString("SessionManager.GetAllSessions.NotLoaded"), e);
            }
            finally
            {
                objConn.Close();
                objConn.Dispose();
                objReader.Close();
                objReader.Dispose();
            }
            return arrList;
        }

		/// <summary>
		/// Releases the resources used by the Component.
		/// Responsible for disposing of the SessionManager object.
		/// </summary>
		virtual public void Dispose()
		{
			m_Items = null;
			m_BinaryItems = null;
			m_sConnectionString = null;
			m_sSID = null;
		}

		/// <summary>
		/// Called from GetAllSessions(), used to get data for a particular session.
		/// </summary>
        /// <param name="p_sSessionId">SessionId to fetch data from.</param>
        /// <param name="p_sData">Pipe separated data provided to this function which
		/// gets converted in the form of name-value pair. Eg. User=xyz can be
		/// accessed as objSession["User"] = "xyz" after this function gets
		/// executed.</param>
		public void InitFromString(string p_sSessionId,string p_sData)
		{
			m_sSID = p_sSessionId;
			try
			{
				string[] arrData = p_sData.Split('|');

				foreach(string arrPair in arrData)
				{
					if (arrPair.IndexOf('=') > -1)
					{
						string[] arrNameValue = arrPair.Split(new char[]{'='},2);
						m_Items[arrNameValue[0]] = arrNameValue[1].Replace("%124%","|");
					}
				}
			}
			catch(Exception e)
			{
				throw new DataModelException(Globalization.GetString("SessionManager.InitFromString.DataNotLoaded"),e);
			}
		}

		/// <summary>
		/// Used to initialize the session.
		/// </summary>
		/// <param name="sessionId">SessionId to be initialized.Blank if new 
		/// SessionId is to be created.</param>
		/// <returns>The SessionId that has been initialized.</returns>
		public string Init(string p_sSessionId)
		{
			try
			{
                if (string.IsNullOrEmpty(p_sSessionId))
                {
                    m_sSID = GenerateSessionGuid();
                } // if
                else
                {
                    m_sSID = p_sSessionId;
                } // else

				LoadSession( true );
			}
			catch(Exception e)
			{
				throw new DataModelException(Globalization.GetString("SessionManager.Init.InitializationError"),e);
			}
			return m_sSID;
		}

		/// <summary>
		/// Used to initialize the session.
		/// </summary>
		/// <param name="sessionId">SessionId to be initialized.Blank if new 
		/// <param name="bUpdateLastAccess">If the last access field should be updated</param>
		/// SessionId is to be created.</param>
		/// <returns>The SessionId that has been initialized.</returns>
		public string Init(string p_sSessionId, bool bUpdateLastAccess)
		{
			try
			{
				m_sSID = p_sSessionId;
				LoadSession( bUpdateLastAccess );
			}
			catch(Exception e)
			{
				throw new LoginException(Globalization.GetString("SessionManager.Init.InitializationError"),e);
			}
			return m_sSID;
		}

		/// <summary>
		/// Called from Init(). Loads the existing Session.
		/// </summary>
		private void LoadSession(bool bUpdateLastAccess)
		{
			// BSB Clear any previously loaded binary item collection for the property accessor to load 
			// a fresh collection on demand.
			m_BinaryItems = null;

            using (DbConnection objConn = DbFactory.GetDbConnection(m_sConnectionString))
            {
                try
			    {
                    //Open the database connection
				    objConn.Open();

                    //Get the current time
                    this.LastChanged = Conversion.ToDbDateTime(DateTime.Now);

                    using (DbReader objReader = objConn.ExecuteReader(String.Format("SELECT DATA,USERDATA1,LASTCHANGE,SESSION_ROW_ID FROM SESSIONS WHERE SID='{0}'", m_sSID)))
                    {
                        if (!objReader.Read())
                        {
                            Add("SID", GenerateSessionGuid());
                        }//if
                        else
                        {
                            m_RowId = objReader.GetInt("SESSION_ROW_ID");
                            string sData = objReader.GetString("DATA");
                            objReader.Close();

                            // Update session with current timestamp
                            objConn.ExecuteNonQuery(String.Format("UPDATE SESSIONS SET LASTCHANGE = '{0}' WHERE SESSION_ROW_ID={1}", this.LastChanged, m_RowId));
                           
                            //Parse String Data
                            if (!string.IsNullOrEmpty(sData))
                            {
                                string[] arrData = sData.Split('|');
                                foreach (string arrPair in arrData)
                                {
                                    string[] arrNameValue = arrPair.Split(new char[] { '=' }, 2);
                                    m_Items[arrNameValue[0]] = arrNameValue[1].Replace("%124%", "|");
                                }//foreach
                            }//if
                        }//else
                    } // using
			    }//try
			    catch(Exception e)
			    { 
				    throw new InitializationException(Globalization.GetString("SessionManager.LoadSession.LoadError"),e);
			    }//catch

            }//using
		}

		/// <summary>
		/// Used to get the value of the session once key is provided. Calls SaveSession
		/// to save the Session values to the database.
		/// </summary>
		public string this[string p_Key]
		{
			get
			{
				try
				{
					return m_Items[p_Key].ToString();
				}
				catch(Exception e)
				{
					throw new CollectionException(Globalization.GetString("SessionManager.this.getError"),e);
				}
			}
			set
			{
				try
				{
					if (m_Items.ContainsKey(p_Key) && m_Items[p_Key].ToString() == value)
						return; //Don't save it if the value is already there and hasn't changed.
					else
					{
						m_Items[p_Key] = value;
						SaveSession();
					}
				}
				catch(Exception e)
				{
					throw new CollectionException(Globalization.GetString("SessionManager.this.setError"),e);
				}
			}
		}

		private void LoadBinaryItems()
		{
			DbConnection objConn=null;
			DbReader objReader=null;

			try
			{
				//Can't look up any valid chilren w/o a parent record id.
				if(this.m_RowId==0)
					return;

				if(m_BinaryItems==null) //Binaries must be "lazy loaded"
				{
					m_BinaryItems = new System.Collections.Hashtable();
					objConn = DbFactory.GetDbConnection(m_sConnectionString);
					objConn.Open();
					objReader = objConn.ExecuteReader(String.Format("SELECT * FROM SESSIONS_X_BINARY WHERE SESSION_ROW_ID={0}",this.m_RowId));
				
					while(objReader.Read())
					{
						BinaryItem objItem = new BinaryItem();
						objItem.IsDirty = false;
						objItem.IsNew = false;
						objItem.SessionRowId = m_RowId;
						objItem.SessionXBinaryRowId = objReader.GetInt("SESSION_X_BIN_ROW_ID");
						objItem.Bytes = objReader.GetAllBytes("BINARY_VALUE");
						//objItem.BinaryType = objReader.GetString("BINARY_TYPE");
						objItem.BinaryName = objReader.GetString("BINARY_NAME");
						m_BinaryItems.Add(objItem.BinaryName,objItem);
					}
				}
			}
			catch(Exception e)
			{
				throw new DataModelException(Globalization.GetString("SessionManager.BinaryItems.HashtableError"),e);
			}
			finally
			{
				if(objReader !=null)
					objReader.Close();
				objReader = null;

				if(objConn !=null)
					objConn.Close();
				objConn= null;
			}
			return;
		}

		public bool BinaryItemExists(string name)
		{
			if(m_BinaryItems ==null)
				LoadBinaryItems();
			
			return(m_BinaryItems.ContainsKey(name));
			
		}
		/// <summary>
		/// Used to get individual Binary items by name.
		/// </summary>
		public byte[] GetBinaryItem(string name)
		{
				if(m_BinaryItems==null) //Binaries must be "lazy loaded"
					LoadBinaryItems();
               
				if(m_BinaryItems.ContainsKey(name))
					return (m_BinaryItems[name] as BinaryItem).Bytes;

			return null;
		}

		/// <summary>
		/// Used to get individual Binary items by name.
		/// </summary>
		public void SetBinaryItem(string name, byte[] bytes)
		{
			if(m_BinaryItems==null) //Binaries must be "lazy loaded"
				LoadBinaryItems();
               
			if(m_BinaryItems.ContainsKey(name)) //Existing Item
			{
				BinaryItem tmp = (m_BinaryItems[name] as BinaryItem);
				tmp.BinaryName = name;
				tmp.IsDirty = true;
				tmp.Bytes = (byte[]) bytes.Clone();
				m_BinaryItems[name] = tmp;
			}
			else //New Item
			{
				BinaryItem tmp = new BinaryItem();
				tmp.BinaryName = name;
				tmp.IsDirty = true;
				tmp.IsNew = true;
				tmp.Bytes = (byte[]) bytes.Clone();
				m_BinaryItems.Add(name,tmp);
			}

			SaveSessionBinary();
		}

		/// <summary>
		/// Used to remove individual Binary items by name.
		/// </summary>
		public void RemoveBinaryItem(string name)
		{
			if(m_BinaryItems==null) //Binaries must be "lazy loaded"
				LoadBinaryItems();
               
			if(m_BinaryItems.ContainsKey(name)) //Existing Item
			{
				DbConnection objConn = DbFactory.GetDbConnection(m_sConnectionString);
				objConn.Open();
                objConn.ExecuteNonQuery(String.Format("DELETE FROM SESSIONS_X_BINARY WHERE SESSION_X_BIN_ROW_ID = {0}", (m_BinaryItems[name] as BinaryItem).SessionXBinaryRowId)); 
				
				m_BinaryItems.Remove(name);
			}
		}

		/// <summary>
		/// Used to get the items of type Hashtable.
		/// </summary>
		public Hashtable Items
		{
			get 
			{ 
				try
				{
					return m_Items;
				}
				catch(Exception e)
				{
					throw new DataModelException(Globalization.GetString("SessionManager.Items.HashtableError"),e);
				}
			}
		}

		/// <summary>
		/// Used to save the existing session and the related values.
		/// Note: This function CANNOT be used to create a new session. You MUST use Init() to
		/// fully create a valid web farm session.
		/// </summary>
		internal void SaveSession()
		{
			string sData = "";
			DbWriter objWriter = null;
			try
			{
				foreach(System.Collections.DictionaryEntry item in m_Items)
				{
					if (sData == "")
                        sData = (String.Format("{0}={1}", item.Key, item.Value.ToString().Replace("|", "%124%")));
					else
                        sData += String.Format("|{0}", (item.Key.ToString() + "=" + item.Value.ToString().Replace("|", "%124%")));
				}

				///*********************************************
				/// Store SESSION Name Value Pairs.
				///*********************************************
				objWriter = DbFactory.GetDbWriter(m_sConnectionString);
				objWriter.Tables.Add("SESSIONS");
				objWriter.Fields.Add("LASTCHANGE",Riskmaster.Common.Conversion.ToDbDateTime(DateTime.Now));
				objWriter.Fields.Add("USERDATA1",m_iUserData1);
				objWriter.Fields.Add("DATA",sData);
				objWriter.Fields.Add("SID",m_sSID);
                objWriter.Where.Add(String.Format("SID='{0}'", m_sSID));
				if (m_sSID != "" && m_sSID != null)
					objWriter.Execute();
			}
			catch(Exception e)
			{
				throw new DataModelException(Globalization.GetString("SessionManager.SaveSession.SaveError"),e);
			}
			finally
			{
				objWriter = null;
			}

			//Save the Binary SubTable Rows 
			SaveSessionBinary();
		}
		
		private void SaveSessionBinary()
		{
			//Don't save child records until parent record is saved and row_id is available.
			if(this.m_RowId ==0)
			{
				this.SaveSession();
				return;
			}

			DbWriter objWriter = null;
			objWriter = DbFactory.GetDbWriter(m_sConnectionString);
			BinaryItem item = null;
			foreach(System.Collections.DictionaryEntry o in m_BinaryItems)
			{
				item = o.Value as BinaryItem;
				if(!item.IsDirty)
					continue;

				objWriter.Reset(true);

				///*********************************************
				/// Store SESSION_X_BINARY Name Value Pairs.
				///*********************************************
				objWriter.Tables.Add("SESSIONS_X_BINARY");
				objWriter.Fields.Add("SESSION_ROW_ID",			this.m_RowId);

				if(item.IsNew)
					objWriter.Fields.Add("SESSION_X_BIN_ROW_ID",this.GetNextUID("SESSIONS_X_BINARY"));
				else				
					objWriter.Fields.Add("SESSION_X_BIN_ROW_ID",item.SessionXBinaryRowId);

				//objWriter.Fields.Add("BINARY_TYPE",				item.BinaryType);
				objWriter.Fields.Add("BINARY_NAME",				item.BinaryName);
				objWriter.Fields.Add("BINARY_VALUE",			item.Bytes);

				if(!item.IsNew)
					objWriter.Where.Add("SESSION_X_BIN_ROW_ID='"+item.SessionXBinaryRowId+"'");

				objWriter.Execute();
				item.IsNew = false;
				item.IsDirty =false;
			}
		}
		/// <summary>
		/// Deletes the Session whose SessionId is provided.
		/// </summary>
        /// <param name="p_sSessionId">SID of the session to be deleted.</param>
		public void DeleteSession(string p_sSessionId)
		{
            using (DbConnection objConn = DbFactory.GetDbConnection(m_sConnectionString))
            {
                //Open the database connection
                objConn.Open();

                try
                {
                    int rowId = objConn.ExecuteInt(String.Format("SELECT SESSION_ROW_ID FROM SESSIONS WHERE SID='{0}'", p_sSessionId));
                    objConn.ExecuteNonQuery(String.Format("DELETE FROM SESSIONS WHERE SID='{0}'", p_sSessionId));
                    objConn.ExecuteNonQuery(String.Format("DELETE FROM SESSIONS_X_BINARY WHERE SESSION_ROW_ID='{0}'", rowId));
                }
                catch (System.Data.DataException ex)
                {
                    throw new Exception(ex.Message, ex);
                }//catch
            }//using
			
			
		}
		
		/// <summary>
		/// Deletes the Session when SessionId is not provided.
		/// </summary>
		public void DeleteSession(){DeleteSession(m_sSID);}
	
		/// <summary>
		/// Used to get/set the UserData.
		/// </summary>
		public int UserData1
		{
			get
			{
				return m_iUserData1;
			}
			set
			{
				m_iUserData1 = value;
			}
		}
		
		/// <summary>
		/// Used to get the SessionId.
		/// </summary>
		public string SessionId
		{
			get
			{
				return m_sSID;
			}
		}
		
		/// <summary>
		/// Used to get/set the time when the Session was last changed.
		/// </summary>
        public string LastChanged
        {
            get;
            set;
        }

        #region IDictionary Members
        /// <summary>
        /// Adds a new Session record to the database
        /// </summary>
        /// <param name="key">object containing the Session ID (SID)</param>
        /// <param name="value">object containing the value for the Session ID (SID)</param>
        public void Add(object key, object value)
        {
            using (DbConnection objConn = DbFactory.GetDbConnection(m_sConnectionString))
            {
                //Open the database connection
                objConn.Open();

                //Assign the resultant value to the private member variable
                m_sSID = value.ToString();
                m_RowId = this.GetNextUID("SESSIONS");
                //Get the current time
                this.LastChanged = Conversion.ToDbDateTime(DateTime.Now);

                objConn.ExecuteNonQuery(String.Format("INSERT INTO SESSIONS (SID,LASTCHANGE,SESSION_ROW_ID) VALUES('{0}','{1}',{2})", m_sSID, this.LastChanged, m_RowId));
            } // using   
        }

        /// <summary>
        /// Determines whether or not an existing session exists
        /// </summary>
        /// <param name="key">object containing the user Session ID (SID)</param>
        /// <returns>boolean indicating whether or not the current session exists</returns>
        public bool Contains(object key)
        {
            return SessionExists(key.ToString());
        }

        IDictionaryEnumerator IDictionary.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public bool IsFixedSize
        {
            get { return false; }
        }

        public ICollection Keys
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Removes the specified session from the database
        /// </summary>
        /// <param name="key"></param>
        public void Remove(object key)
        {
            DeleteSession(key.ToString());
        }

        public ICollection Values
        {
            get { throw new NotImplementedException(); }
        }

        public object this[object key]
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        #region ICollection Members

        public void CopyTo(Array array, int index)
        {
            throw new NotImplementedException();
        }

        public bool IsSynchronized
        {
            get { throw new NotImplementedException(); }
        }

        public object SyncRoot
        {
            get { throw new NotImplementedException(); }
        }

        #endregion

        #region IDictionary Members

        /// <summary>
        /// Removes all current sessions in the database
        /// </summary>
        public void Clear()
        {
            using (DbConnection objConn = DbFactory.GetDbConnection(m_sConnectionString))
            {
                //Open the database connection
                objConn.Open();

                objConn.ExecuteNonQuery("DELETE FROM SESSIONS");
                objConn.ExecuteNonQuery("DELETE FROM SESSIONS_X_BINARY");
            }//using
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        #endregion

        #region ICollection Members


        public int Count
        {
            get 
            {
                int rowCount = 0;

                using (DbConnection objConn = DbFactory.GetDbConnection(m_sConnectionString))
                {
                    //Open the database connection
                    objConn.Open();

                    rowCount = objConn.ExecuteInt("SELECT COUNT(*) FROM SESSIONS");
                }//using                

                return rowCount;
            }
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        #endregion

        private class BinaryItem
        {
            public byte[] Bytes;
            //public string BinaryType;
            public string BinaryName;
            public int SessionRowId;
            public int SessionXBinaryRowId;
            public bool IsDirty;
            public bool IsNew;
        }
    }
}
