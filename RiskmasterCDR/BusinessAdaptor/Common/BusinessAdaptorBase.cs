using System;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;

namespace Riskmaster.BusinessAdaptor.Common
{
   
    /// <summary>
    /// Delegate defines the method signature that business service entry-point
    /// methods are expected to follow.
    /// </summary>
    public delegate bool BusinessAdaptorMethod(XmlDocument xmlIn, ref XmlDocument xmlOut, ref BusinessAdaptorErrors errOut);
    /// <summary>
    /// Base class implementation for all business service adaptors. 
    /// Method signature for adaptor entry point methods (i.e. methods
    /// called from common web service) must be (note that individual parameter names are not important):
    ///		bool MethodName(XmlDocument xmlIn, ref XmlDocument xmlOut, ref BusinessAdaptorErrors errOut)
    ///	Method is supplied xmlIn and must provide xmlOut as an XmlDocument containing its output message.
    ///	errOut must be a collection of errors or null if no errors occurred.
    ///	The method must return true for success or false for failure (note that error messages may still be returned
    ///	even if the result of the call is true).
    /// </summary>
    abstract public class BusinessAdaptorBase : IBusinessAdaptor 
    {
        protected UserLogin m_userLogin = null;
        protected int m_userID = 0;
        protected int m_DSNID = 0;
        protected int m_groupID = 0;
        protected string m_connectionString = "";
        protected string m_securityConnectionString = "";
        protected string m_loginName = "";
        private SessionManager m_oSession = null;  //Pointer to session manager handed from CWS via IBusinessAdaptor interface
        private UserSession m_objUserSession = null;
        // Public property userLogin. This is the core property for security functionality.
        // Many common properties/methods are broken out below from this object for
        // simpler coding.
        protected int m_LangCode = 0;//vkumar258 ML Changes
        private int m_ClientId = 0;
        /// <summary>
        /// Default class constructor
        /// </summary>
        public BusinessAdaptorBase()
        {
 
        } // constructor


        /// <summary>
        /// Returns the Riskmaster.Security.UserLogin object for the logged in user.
        /// This is information about the user login including login name, doc path, and permission set.
        /// </summary>
        /// <returns>Riskmaster.Security.UserLogin object for logged in user.</returns>
        public UserLogin userLogin
        {
            get { return m_userLogin; }
        }

        // Public properties - these are shortcuts to common information.
        // All are read-only and all are accessible through userLogin object as well.
        // These are broken out to simply coding.

        /// <summary>
        /// Returns the database connection string to the "data" database.
        /// </summary>
        public string connectionString
        {
            get { return m_connectionString; }
        }

        /// <summary>
        /// Returns the database connection string to the "security" database.
        /// </summary>
        public string securityConnectionString
        {
            get { return m_securityConnectionString; }
        }

        /// <summary>
        /// Returns the login name of the currently logged in user.
        /// </summary>
        public string loginName
        {
            get { return m_loginName; }
        }

        /// <summary>
        /// Returns the user id number for the logged in user.
        /// </summary>
        public int userID
        {
            get { return m_userID; }
        }

        /// <summary>
        /// Returns the DSN id number for the data source that web service is connected to.
        /// </summary>
        public int DSNID
        {
            get { return m_DSNID; }
        }

        /// <summary>
        /// Returns the group id number for the logged in user.
        /// </summary>
        public int groupID
        {
            get { return m_groupID; }
        }
        /// <summary>
        /// Returns the language code number for the logged in user.
        /// </summary>
        public int LangCode
        {
            get { return m_LangCode; }
        }
        /// <summary>
        /// Returns the Client Id number for the logged in user.
        /// </summary>
        public int ClientId
        {
            get { return m_ClientId; }
            set { m_ClientId = value; }
        }

        
        /// <summary>
        /// Sets security context (i.e. user and login) into the business adaptor.
        /// </summary>
        /// <param name="userLogin">Authenticated Riskmaster.Security.userLogin object for use by the business adaptor.</param>
        public void SetSecurityInfo(UserLogin userLogin, int p_iClientId)
        {
            //Set the sec info for application layer.
            //base.SetSecurityInfo(userLogin, iClientId);
            // Set main user login reference
            m_userLogin = userLogin;
            m_ClientId = p_iClientId;
            // Break out some frequently used items for convience
            if (m_userLogin != null)
            {
                m_userID = userLogin.UserId;
                m_DSNID = userLogin.DatabaseId;
                m_groupID = userLogin.GroupId;
                m_connectionString = userLogin.objRiskmasterDatabase.ConnectionString;
                m_securityConnectionString = SecurityDatabase.GetSecurityDsn(m_ClientId);
                m_loginName = userLogin.LoginName;
                m_LangCode = userLogin.objUser.NlsCode;
            }
        }

        #region Legacy Session Management
        public void SetSessionObject(SessionManager oSession)
        {
            m_oSession = oSession;
        }

        public SessionManager GetSessionObject()
        {
            if (m_oSession == null)
            {
                throw new RMAppException(Globalization.GetString("CacheNoSessionFailure",m_ClientId));
            }

            return m_oSession;
        } 
        #endregion

        #region RMX New Session Management
        /// <summary>
        /// Instantiates the BusinessAdaptor layer with UserSession 
        /// rather than the legacy SessionManager class
        /// </summary>
        /// <param name="objUserSession"></param>
        public void SetSessionObject(UserSession objUserSession)
        {
            m_objUserSession = objUserSession;
        }//method: SetSessionObject


        /// <summary>
        /// Retrieves the UserSession instance 
        /// rather than the legacy SessionManager class
        /// </summary>
        /// <returns></returns>
        public UserSession GetUserSession()
        {
            return m_objUserSession;
        }//method: GetUserSession() 
        #endregion

        /// <summary>
        /// Retrieves a named object from session, deserializes it and returns it.
        /// Note that exceptions may be thrown directly from this function if any failure occurs.
        /// It is imperative that it be called within a try/catch block.
        /// </summary>
        /// <param name="sObjectName">Name of object to return. Usually defined as a constant in the class that is being requested.</param>
        /// <returns>Deserialized object. Must be cast to expected time.</returns>
        public Object getCachedObject(string sObjectName)
        {
            m_oSession = GetSessionObject();
            return Utilities.BinaryDeserialize(m_oSession.GetBinaryItem(sObjectName));
        }
        /// <summary>
        /// Stores a named object into session. Handles serialization automatically..
        /// Note that exceptions may be thrown directly from this function if any failure occurs.
        /// It is imperative that it be called within a try/catch block.
        /// </summary>
        /// <param name="sObjectName">Name of object to return. Usually defined as a constant in the class that is being requested.</param>
        public void setCachedObject(string sObjectName, Object obj)
        {
            m_oSession = GetSessionObject();
            m_oSession.SetBinaryItem(sObjectName, Utilities.BinarySerialize(obj), ClientId);

            return;
        }

        /// <summary>
        /// Removes a named binary object from session.
        /// </summary>
        /// <param name="sObjectName">Name of object to return. Usually defined as a constant in the class that is being requested.</param>
        public void removeCachedObject(string sObjectName)
        {
            m_oSession = GetSessionObject();
            m_oSession.RemoveBinaryItem(sObjectName);
            
            return;
        }


        /// <summary>
        /// Returns true if cached named object exists.
        /// </summary>
        /// <param name="sObjectName">Name of cached object.</param>
        /// <returns>true if object exists.</returns>
        public bool cachedObjectExists(string sObjectName)
        {
            m_oSession = GetSessionObject();
            return m_oSession.BinaryItems.ContainsKey(sObjectName);
        }


        //rsolanki2 : mits 18828: moving this function to riskmaster.common.CommonFunction class

        ///// <summary>
        ///// created by Nitin in order to implement recent claims and events functionality in R5
        ///// </summary>
        ///// <param name="recordType"></param>
        ///// <param name="recordId"></param>
        ///// <param name="recordNumber"></param>
        //protected void UpdateRecentRecords(string recordType, string recordId, string recordNumber)
        //{
        //    //rsolanki2: recent claim updates : MITS 18828

        //    string sSql = string.Empty;
        //    string sXml = string.Empty;
        //    Riskmaster.Db.DbReader objReader = null;
        //    XmlDocument objPrefXml = null;
        //    XmlNode recentRecordNode = null;
        //    XmlNode tempNode = null;
        //    XmlElement newRecentNode = null;
        //    XmlNodeList recentRecordList = null;
        //    XmlAttribute atbRecordType = null;
        //    XmlAttribute atbRecordId = null;
        //    XmlAttribute atbRecordNumber = null;
        //    Riskmaster.Db.DbConnection objConn = null;
        //    Riskmaster.Db.DbCommand objCommand = null;
        //    Riskmaster.Db.DbParameter objParam = null;
        //    int recentRecordCount = 0;

        //    try
        //    {
        //        objConn = Riskmaster.Db.DbFactory.GetDbConnection(this.connectionString);
        //        objConn.Open();
        //        objCommand = objConn.CreateCommand();

        //        sSql = "SELECT PREF_XML FROM USER_PREF_XML WHERE USER_ID=" + this.userID;
        //        objReader = Riskmaster.Db.DbFactory.GetDbReader(this.connectionString, sSql);
        //        if (objReader.Read())
        //        {
        //            sXml = objReader.GetValue("PREF_XML").ToString();
        //        }
        //        objReader.Close();
        //        //objReader.NextResult()
        //        if (sXml != string.Empty)
        //        {
        //            try
        //            {
        //                objPrefXml = new XmlDocument();
        //                objPrefXml.LoadXml(sXml);
        //            }
        //            catch (Exception)
        //            {
        //                //delete bad formatted xml string from database
        //                sSql = "DELETE FROM USER_PREF_XML WHERE USER_ID=" + this.userID;
        //                objCommand.Parameters.Clear();
        //                objCommand.CommandText = sSql;
        //                objCommand.ExecuteNonQuery();
        //                sXml = string.Empty;
        //            }
        //        }

        //        if (sXml != string.Empty)
        //        {
        //            recentRecordNode = objPrefXml.SelectSingleNode("setting/RecentRecords");

        //            if (recentRecordNode != null)
        //            {
        //                //if same node already exists then don't do any thing and return 
        //                if (recentRecordNode.SelectSingleNode("record[(@recordid='" + recordId + "') and (@recordtype='" + recordType + "')]") != null)
        //                {
        //                    return;
        //                }

        //                recentRecordList = recentRecordNode.SelectNodes(string.Concat("record[@recordtype='" , recordType , "']"));
        //                recentRecordCount = recentRecordList.Count;

        //                if (recentRecordCount == 10)
        //                {
        //                    //remove the very first record and inser this new record at last postition
        //                    tempNode = recentRecordList.Item(0);
        //                    recentRecordNode.RemoveChild(tempNode);
        //                }
        //            }
        //            else
        //            {
        //                recentRecordNode = objPrefXml.CreateNode(XmlNodeType.Element, "RecentRecords", null);
        //                objPrefXml.SelectSingleNode("setting").AppendChild(recentRecordNode);
        //            }

        //            newRecentNode = objPrefXml.CreateElement("record");

        //            atbRecordType = objPrefXml.CreateAttribute("recordtype");
        //            atbRecordType.Value = recordType;
        //            newRecentNode.Attributes.Append(atbRecordType);

        //            atbRecordId = objPrefXml.CreateAttribute("recordid");
        //            atbRecordId.Value = recordId;
        //            newRecentNode.Attributes.Append(atbRecordId);

        //            atbRecordNumber = objPrefXml.CreateAttribute("recordnumber");
        //            atbRecordNumber.Value = recordNumber;
        //            newRecentNode.Attributes.Append(atbRecordNumber);

        //            recentRecordNode.AppendChild(newRecentNode);

        //            sXml = objPrefXml.InnerXml;

        //            //Update USER_PREF_XML table with the updated XML
        //            sSql = "UPDATE USER_PREF_XML SET PREF_XML=~PXML~ WHERE USER_ID=" + this.userID;
        //            objCommand.Parameters.Clear();
        //            objParam = objCommand.CreateParameter();
        //            objParam.Direction = System.Data.ParameterDirection.Input;
        //            objParam.Value = sXml;
        //            objParam.ParameterName = "PXML";
        //            objParam.SourceColumn = "PREF_XML";
        //            objCommand.Parameters.Add(objParam);
        //            objCommand.CommandText = sSql;
        //            objCommand.ExecuteNonQuery();
        //        }
        //        else
        //        {
        //            //if user xml is not present in the database then insert a record for the same
        //            sXml = "<setting><RecentRecords><record recordtype='" 
        //                + recordType 
        //                + "' recordid='" 
        //                + recordId 
        //                + "' recordnumber='" 
        //                + recordNumber 
        //                + "'/></RecentRecords></setting>";

        //            sSql = string.Concat("INSERT INTO USER_PREF_XML(USER_ID,PREF_XML) VALUES(" 
        //                , this.userID 
        //                , ", ~PXML~)");
        //            objCommand.Parameters.Clear();
        //            objParam = objCommand.CreateParameter();
        //            objParam.Direction = System.Data.ParameterDirection.Input;
        //            objParam.Value = sXml;
        //            objParam.ParameterName = "PXML";
        //            objParam.SourceColumn = "PREF_XML";
        //            objCommand.Parameters.Add(objParam);
        //            objCommand.CommandText = sSql;
        //            objCommand.ExecuteNonQuery();
        //        }
        //    }
        //    //Riskmaster.Db is throwing following exception so need to catch the same
        //    catch (RMAppException p_objExp)
        //    {
        //        throw p_objExp;
        //    }
        //    catch (Exception p_objExp)
        //    {
        //        throw new RMAppException(Globalization.GetString("OrgHierarchy.GetOrgPreferenceXml.GeneralError"),
        //            p_objExp);
        //    }
        //    finally
        //    {
        //        if (objReader != null)
        //        {
        //            if (!objReader.IsClosed)
        //            {
        //                objReader.Close();
        //            }
        //            objReader.Dispose();
        //        }

        //        if (objConn != null)
        //        {
        //            if (objConn.State == System.Data.ConnectionState.Open)
        //            {
        //                objConn.Close();
        //            }
        //            objConn.Dispose();
        //        }
        //    }
        //}

        ///// <summary>
        ///// created by Nitin in order to implement recent claims and events functionality in R5
        ///// </summary>
        ///// <param name="recordType"></param>
        ///// <param name="recordId"></param>        
        //protected void UpdateRecentRecords(string recordType, string recordId)
        //{
        //    //rsolanki2: recent claim updates : MITS 18828

        //    string sSql = string.Empty;
        //    string sXml = string.Empty;
        //    Riskmaster.Db.DbReader objReader = null;
        //    Riskmaster.Db.DbConnection objConn = null;
        //    Riskmaster.Db.DbCommand objCommand = null;            
        //    string recordNumber = string.Empty;

        //    try
        //    {
        //        objConn = Riskmaster.Db.DbFactory.GetDbConnection(this.connectionString);
        //        objConn.Open();
        //        objCommand = objConn.CreateCommand();

        //        if (recordType =="claim")
        //        {
        //            sSql = "SELECT CLAIM_NUMBER FROM CLAIM WHERE CLAIM_ID=" + recordId;
        //        }
        //        else if (recordType =="event")
        //        {
        //            sSql = "SELECT EVENT_NUMBER FROM EVENT WHERE EVENT_ID=" + recordId;
        //        }

        //        objReader = Riskmaster.Db.DbFactory.GetDbReader(this.connectionString, sSql);

        //        if (objReader.Read())
        //        {
        //            recordNumber = objReader.GetValue(0).ToString();
        //        }
        //        else
        //        {
        //            throw new Exception("Invalid record id");
        //        }

        //        UpdateRecentRecords(recordType, recordId, recordNumber);
        //    }
        //    //Riskmaster.Db is throwing following exception so need to catch the same
        //    catch (RMAppException p_objExp)
        //    {
        //        throw p_objExp;
        //    }
        //    catch (Exception p_objExp)
        //    {
        //        throw new RMAppException(Globalization.GetString("OrgHierarchy.GetOrgPreferenceXml.GeneralError"),
        //            p_objExp);
        //    }
        //    finally
        //    {
        //        if (objReader != null)
        //        {
        //            if (!objReader.IsClosed)
        //            {
        //                objReader.Close();
        //            }
        //            objReader.Dispose();
        //        }

        //        if (objConn != null)
        //        {
        //            if (objConn.State == System.Data.ConnectionState.Open)
        //            {
        //                objConn.Close();
        //            }
        //            objConn.Dispose();
        //        }
        //    }
        //}
    }
}
