using System;

namespace Riskmaster.BusinessAdaptor.Common
	{
		/// <summary>
		/// AppConstants used to access Application and Session variables
		/// The only real value that this gives is that a developer can look in here and 
		/// know what variables should be available in each state object.
		/// </summary>
		public class AppConstants
		{
			public const string	APP_NAMESPACE = "http://csc.com/Riskmaster/Webservice/Common";

			public  const string APP_SMDSN="SM_DSN";
			public  const string APP_SMTPSERVER="SMTP_SERVER";
			public  const string APP_DATAPATH="APP_DATA_PATH";
			public  const string APP_PATH="APP_PATH";
			public  const string APP_BINARIESPATH="BINARIES_PATH";
			public  const string APP_RISKSERVERNAME="RISK_SERVER_NAME";
			public  const string APP_USERDIRECTORYHOME="USER_DIRECTORY_HOME";
			public  const string APP_CUSTOMDICTIONARY =  "CustomDict";

			public  const string SESSION_OBJ_USER="OBJ_USER";

			public  const string SESSION_EVENT_TYPE_LIMITS =  "EventTypeLimits";
			public  const string SESSION_CLAIM_TYPE_LIMITS="ClaimTypeLimits";

			public  const string SESSION_QUICKENTRY="QuickEntry";
			public  const string SESSION_QUICKENTRYVIEWNAME="QuickEntryViewName";
			public  const string SESSION_QUICKENTRYHOMEPAGE="QuickEntryHomePage";
			public  const string SESSION_QUEST="Quest";
		}
	}

