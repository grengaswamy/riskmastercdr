﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.Security;
using Riskmaster.Common;

namespace Riskmaster.BusinessAdaptor.Common
{
    /// <summary>
    /// Interface for communication from the common web service into the business adaptor.
    /// All business adaptors must implement this interface.
    /// </summary>
    public interface IBusinessAdaptor
    {

        /// <summary>
        /// Sets security context (i.e. user and login) into the business adaptor.
        /// </summary>
        /// <param name="userLogin">Authenticated Riskmaster.Security.userLogin object for use by the business adaptor.</param>
        void SetSecurityInfo(UserLogin userLogin,int p_iClientId);
        /// <summary>
        /// Passes an instance of the legacy SessionManager class
        /// </summary>
        /// <param name="oSession">instance of the SessionManager class</param>
        void SetSessionObject(SessionManager oSession);

        /// <summary>
        /// Retrieves the current instance of the legacy SessionManager class
        /// </summary>
        /// <returns>current instance of the legacy SessionManager class</returns>
        SessionManager GetSessionObject();

        /// <summary>
        /// Passes an instance of the UserSession class
        /// </summary>
        /// <param name="objUserSession">instance of the UserSession class</param>
        void SetSessionObject(UserSession objUserSession);

        /// <summary>
        /// Retrieves the current instance of the UserSession class
        /// </summary>
        /// <returns>current instance of the UserSession class</returns>
        UserSession GetUserSession();

    }
}
