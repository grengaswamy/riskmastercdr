using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using System.IO;

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: Sentinel.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 10-Feb-2005
	///* $Author	: Nikhil Kr. Garg
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for Sentinel Reports which 
	///	contains methods to fetch the data related to a specific event and event sentinel 
	///	and then generate the Root Cause Analysis Report for that event.	
	/// </summary>
	public class SentinelAdaptor: BusinessAdaptorBase
	{

        /// <summary>
        /// Default class constructor
        /// </summary>
        public SentinelAdaptor()
        {
        }

		#region Public Methods
		/// <summary>
		/// This method is a wrapper to Riskmaster.Application.Sentinel.Report.GetRootCauseAnalysisReport() method.
		/// This method will call methods to generate the Root Cause Analysis
		///	Report for the specified Event. It will return the binary stream for the report generated.	
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<Document>
		///			<Sentinel>
		///				<EventId>EventId</EventId>
		///			</Sentinel>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of output XML document would be:
		///		<Sentinel>
		///			<File Name="name of file">
		///				data for the file in Base64 encoding
		///			</File>
		///		</Sentinel>
		/// </param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		public bool GetRootCauseAnalysisReport(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.Sentinel.Report objReport=null;					//object of Sentinel.Report class
			int iEventId=0;							//Event id passed in Input xml document
			MemoryStream objSentinelReport=null;	//stream of report
			XmlElement objOutElement=null;			//used for creating the Output Xml
			XmlElement objElement=null;				//used for parsing the input xml
            string sLangCode = string.Empty;
			try
			{
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//LangCode");
                if (objElement != null)
                {
                    sLangCode = objElement.InnerText;
                }

				//check existence of EventId which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//EventId");
				if (objElement==null)
				{
					p_objErrOut.Add("SentinelAdaptorParameterMissing",CommonFunctions.FilterBusinessMessage(Globalization.GetString("SentinelAdaptorParameterMissing", base.ClientId),sLangCode),BusinessAdaptorErrorType.Error);
					return false;
				}

				iEventId=Conversion.ConvertStrToInteger(objElement.InnerText);


                

				objReport = new Riskmaster.Application.Sentinel.Report(base.connectionString, base.ClientId);

				//read the other parameters/settings from the config file
                objReport.TemplateFileName = RMConfigurator.CombineFilePath(RMConfigurator.AppFilesPath, SentinelMessages_us_en.TemplateFileName);
                objReport.JCAHOImagePath = RMConfigurator.CombineFilePath(RMConfigurator.AppFilesPath, SentinelMessages_us_en.JCAHOImagePath);
				//get the report
                objReport.LangCode = sLangCode;
                objReport.GetRootCauseAnalysisReport(iEventId, out objSentinelReport);
					
				//create the output xml document containing the data in Base64 format
				objOutElement=p_objXmlOut.CreateElement("Sentinel");
				p_objXmlOut.AppendChild(objOutElement);
				objOutElement=p_objXmlOut.CreateElement("File");
				objOutElement.SetAttribute("Name",GetFileName("SentinelReport","pdf"));
				objOutElement.InnerText=Convert.ToBase64String(objSentinelReport.ToArray());
				p_objXmlOut.FirstChild.AppendChild(objOutElement);
				return true;
			}
			catch(InvalidValueException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(FileInputOutputException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,CommonFunctions.FilterBusinessMessage(Globalization.GetString("SentinelAdaptor.GetRootCauseAnalysisReport.Error", base.ClientId),sLangCode),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objReport = null;
				objElement=null;
				objSentinelReport=null;
			}
		}

		#endregion

		#region Private Functions
		/// <summary>
		/// Generates a unique file Name 
		/// </summary>
		/// <param name="p_sFileName">Intiials of the file name </param>
		/// <param name="p_sFileExt">file extension</param>
		/// <returns>Complete file name</returns>	
		private string GetFileName(string p_sFileName, string p_sFileExt)
		{
			string sFileName=string.Empty;	//for holding file name

			sFileName=p_sFileName + "_" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + 
				DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() +
				DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString() + "." + p_sFileExt;

			return sFileName;
		}
		#endregion
	}
}
