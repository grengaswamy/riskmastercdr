using System;
using System.IO ;
using System.Xml ;
using Amyuni.PDFCreator;
using Riskmaster.Common ;
using Riskmaster.Security ;
using Riskmaster.ExceptionTypes ;
using Riskmaster.BusinessAdaptor.Common ;
using Riskmaster.Application.PrintEOB ;

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: PrintEobAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 23-Mar-2005
	///* $Author	: Vaibhav Kaushik
	///* $Comment	: Implements adaptor layer over the PrintEOB AL component.
	///* $Source	: 
	///**************************************************************			
	public class PrintEOBAdaptor : BusinessAdaptorBase
	{

		#region Constants
		/// <summary>
		/// Constant for output Xml document root node name.
		/// </summary>
		private string XML_OUT_HEADER_NODE_NAME = "PrintEOB" ;
		#endregion 

		#region Constructor
		/// <summary>
		/// Constructor, for the default initialization of class level variables.
		/// </summary>
		public PrintEOBAdaptor()
		{			
		}
		#endregion 

		#region Public Methods

		#region PrintEOBAR 
		/// <summary>
		///		This method is a wrapper to PrintEOBAR.PostCheckEOB() method.
		///		Print the Post Check EOB report.
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<PrintEOB>
		///				<AccountId>Account Id</AccountId>
		///				<BatchNumber>Batch Number</BatchNumber>
		///				<PostcheckDate>Date</PostcheckDate>
		///				<OrderBy>Order By Field</OrderBy>
		///			</PrintEOB>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document</param>
		/// 
		///		The structure of output XML document would be:
		///		<PrintEOB>
		///			<File Name="" Type="PostCheckEOB"></File>
		///			<File Name="" Type="PostCheckEOB"></File>
		///			<File Name="" Type="PostCheckEOB"></File>
		///		</PrintEOB>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool PostCheckEOB( XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			PrintEOBAR objPrintEOBAR = null ;					
			XmlElement objTargetElement = null ;
			XmlElement objRootElement = null ;
			XmlElement objFilesNode = null ;

			int iAccountId = 0 ;
			int iBatchNumber = 0 ;
			int iIndex = 0 ;
			// npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
            int iDistributionType = 0;
			// npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
			string sDate = string.Empty ;
			string sOrderByField = string.Empty ;
			string sPDFSaveFilePaths = string.Empty ; 
			string sPDFSaveFilepath = string.Empty ; 
			string sTemp = "" ;
			string[] arrPDFSaveFilepath = null ;
			string[] arrPDFFileNameAndId = null ;
			bool bFirst = true ;
            string sFileNames = "";
            bool bPrintCheckPdfOrDirect = false;
			try
			{
				// Get the Account Id.
				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//AccountId" );
				if( objTargetElement == null )
				{
					p_objErrOut.Add( "AccountIdMissing" , Globalization.GetString("PrintEOBAdaptor.PostCheckEOB.AccountIdNotFound",base.ClientId) , BusinessAdaptorErrorType.Error );//sharishkumar Jira 835
					return false;
				}
				iAccountId = Conversion.ConvertStrToInteger( objTargetElement.InnerText );
				
				// Get the Batch Number.
				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//BatchNumber" );
				if( objTargetElement == null )
				{
                    p_objErrOut.Add("BatchNumberMissing", Globalization.GetString("PrintEOBAdaptor.PostCheckEOB.BatchNumberNotFound", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 835
					return false;
				}
				iBatchNumber = Conversion.ConvertStrToInteger( objTargetElement.InnerText );

				// Get the Post Check Date
				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//PostcheckDate" );
				if( objTargetElement == null )
				{
                    p_objErrOut.Add("DateMissing", Globalization.GetString("PrintEOBAdaptor.PostCheckEOB.DateNotFound", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 835
					return false;
				}
				sDate = objTargetElement.InnerText ;

				// Get the Post Check Order By Field
				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//OrderBy" );
				if( objTargetElement == null )
				{
                    p_objErrOut.Add("OrderByFieldMissing", Globalization.GetString("PrintEOBAdaptor.PostCheckEOB.OrderByFieldNotFound", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 835
					return false;
				}
				sOrderByField = objTargetElement.InnerText ;
				// npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
                // Get the Distribution Type
                objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//DistributionType");
                if (objTargetElement == null)
                {
                    p_objErrOut.Add("DistributionTypeMissing", Globalization.GetString("PrintEOBAdaptor.DistributionTypeNotFound", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                iDistributionType = Conversion.ConvertStrToInteger(objTargetElement.InnerText);
				
				// Initialize the PrintEOBAR object.
				objPrintEOBAR = new PrintEOBAR( userLogin.LoginName , userLogin.Password , userLogin.objRiskmasterDatabase.DataSourceName , userLogin.objRiskmasterDatabase.DataSourceId.ToString(), base.ClientId);//rkaur27
                bPrintCheckPdfOrDirect = objPrintEOBAR.m_bDirectToPrinter;
				// Call PostCheckEOB Function.
				objPrintEOBAR.PostCheckEOB( iAccountId, sOrderByField, iBatchNumber, sDate, iDistributionType, out sPDFSaveFilePaths );
				// npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
                //Deb: Check for not null or blank
                if (!string.IsNullOrEmpty(sPDFSaveFilePaths) && bPrintCheckPdfOrDirect == false)
                {
                    sFileNames = CreateMergedFile(sPDFSaveFilePaths) + ",";
                }
                //Deb Check for not null or blank
				// Copy the binary content of file in the p_objXmlOut doc.
				objRootElement = p_objXmlOut.CreateElement( XML_OUT_HEADER_NODE_NAME );
                // Adding attribute which tell us on UI that are we Printing directly to Printer or not
                if (bPrintCheckPdfOrDirect == true && sPDFSaveFilePaths != string.Empty)
                {
                    objRootElement.SetAttribute("PrintDirectlyToPrinter", "true");
                }
                else
                {
                    objRootElement.SetAttribute("PrintDirectlyToPrinter", "false");
                }
				p_objXmlOut.AppendChild( objRootElement );

                //arrPDFSaveFilepath = sPDFSaveFilePaths.Split( ",".ToCharArray() );
                //for( iIndex = 0 ; iIndex <= arrPDFSaveFilepath.GetUpperBound( 0 ) ; iIndex++ )
                //{
                //    if( bFirst )
                //    {
                //        objFilesNode = p_objXmlOut.CreateElement( "Files" );
                //        objRootElement.AppendChild( objFilesNode );
                //        bFirst = false ;
                //    }
                //    sTemp = arrPDFSaveFilepath[iIndex] ;
                //    arrPDFFileNameAndId = sTemp.Split( "|".ToCharArray() );
                //    if( arrPDFFileNameAndId.Length >= 2 )
                //    {
                //        if( arrPDFFileNameAndId[0] != "" )
                //            sFileNames+=this.CreateNodeWithFileContentForEOB(arrPDFFileNameAndId[1], objFilesNode, arrPDFFileNameAndId[0])+",";
                //    }
                //}
                if (sFileNames.Length > 1)
                    sFileNames = sFileNames.Substring(0, sFileNames.Length - 1);
                objRootElement.SetAttribute("FileNames", System.IO.Path.GetFileName(sFileNames));
                return( true );
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add( p_objException , BusinessAdaptorErrorType.Error );
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("PrintEOBAdaptor.PostCheckEOB.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 835
				return false;
			}
			finally
			{
				if (objPrintEOBAR != null)
                    objPrintEOBAR.Dispose();
				objTargetElement = null;
				objRootElement = null ;
			}
		}
        
        //private string CreateMergedFile(string sPath)
        //{
        //    acPDFCreatorLib.Initialize();
        //    string firstfilepath = "";
        //    string [] arr=sPath.Split(",".ToCharArray()[0]);
        //    if (arr.Length <= 1)
        //    {
        //        firstfilepath = arr[0].Split("|".ToCharArray())[1];
        //        return firstfilepath;
        //    }
        //    int i=0;
        //    foreach (string fPath in arr)
        //    {
        //        string filename = "";
        //        if (i == 0)
        //        {
        //            firstfilepath = fPath.Split("|".ToCharArray())[1];
        //            continue;
        //        }
        //        filename = fPath.Split("|".ToCharArray())[1];
        //        string sTempFileName = RMConfigurator.CombineFilePath(RMConfigurator.UserDataPath, "EOB") +"\\" + System.IO.Path.GetTempFileName() + ".pdf";

        //        //Initialize library and set the license key                         
        //        acPDFCreatorLib.SetLicenseKey("CSC Financial Services Group", "07EFCDAB01000100C9D1AB2346F36D68B637892D59E2B2D416C18F0397811F5604016685DBC54D7928B23578A87E09BFEB3D9FC4FE9F");
        //        using (FileStream file1 = new FileStream(firstfilepath, FileMode.Open, FileAccess.ReadWrite))
        //        {
        //            using (FileStream file2 = new FileStream(filename, FileMode.Open, FileAccess.Read))
        //            {
        //                using (IacDocument doc1 = new IacDocument(null))
        //                {
        //                    IacDocument doc2 = new IacDocument(null);
        //                    // Opend the pdf file we want to add watermarks to
        //                    doc1.Open(file1, "");

        //                    // Open the watermark pdf file
        //                    doc2.Open(file2, "");

        //                    doc1.Append(doc2);

        //                    doc1.Save(file1);                            
        //                }
        //            }
        //        }
        //        acPDFCreatorLib.Terminate();
        //    }
        //    return firstfilepath;
        //}

        private string CreateFileName(string BasePath)
        {
            string sTempFileName = Path.Combine(BasePath, System.IO.Path.GetRandomFileName() + ".pdf");
            while (File.Exists(sTempFileName))
            {
                sTempFileName = Path.Combine(BasePath, System.IO.Path.GetRandomFileName() + ".pdf");
            }
            return sTempFileName;
        }
        private string CreateMergedFile(string sPath)
        {
            acPDFCreatorLib.Initialize();
            //Initialize library and set the license key                         
            acPDFCreatorLib.SetLicenseKey("CSC Financial Services Group","07EFCDAB01000100C9D1AB2346F36D68B637892D59E2B2D416C18F0397811F5604016685DBC54D7928B23578A87E09BFEB3D9FC4FE9F");


            string BasePath = RMConfigurator.CombineFilePath(RMConfigurator.UserDataPath, "EOB");
            string firstfilepath = "";
            string[] arr = sPath.Split(",".ToCharArray()[0]);
            if (arr.Length == 1)
            {
                firstfilepath = arr[0].Split("|".ToCharArray())[0];
                return firstfilepath;
            }
            if (arr.Length > 0)
            {
                firstfilepath = arr[0].Split("|".ToCharArray())[0];
            }
            int i = 0;
            using (FileStream file1 = new FileStream(Path.Combine(BasePath, firstfilepath), FileMode.Open, FileAccess.ReadWrite))
            {
                using (IacDocument doc1 = new IacDocument(null))
                {
                    // Opend the pdf file we want to add watermarks to
                    doc1.Open(file1, "");
                    foreach (string fPath in arr)
                    {
                        string filename = "";
                        if (i == 0)
                        {
                            i++;
                            continue;
                        }
                        filename = fPath.Split("|".ToCharArray())[0];


                        using (FileStream file2 = new FileStream(Path.Combine(BasePath, filename), FileMode.Open, FileAccess.Read))
                        {

                            using (IacDocument doc2 = new IacDocument(null))
                            {
                                // Opend the pdf file we want to add watermarks to
                                // doc1.Open(file1, "");

                                // Open the watermark pdf file
                                doc2.Open(file2, "");

                                doc1.Append(doc2);
                                //doc1.Save(file1);
                            }
                        }
                    }
                    doc1.Save(file1);
                }
            }
            acPDFCreatorLib.Terminate();
            return firstfilepath;
        }
		/// <summary>
		///		This method is a wrapper to PrintEOBAR.PrintEOBReport() method.
		///		Print the EOB report.
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<PrintEOBARReport>
		///				<TransId>Trans Id</TransId>
		///				<IsSilent>Is Silent</IsSilent>
		///				<State>State</State>
		///				<IsPostCheck>Is Post Check</IsPostCheck>
		///			</PrintEOBARReport>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document</param>
		/// 
		///		The structure of output XML document would be:
		///		<PrintEOB>
		///			<File Name="" Type="PrintEOBARReport">
		///			</File>
		///		</PrintEOB>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool PrintEOBARReport( XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			PrintEOBAR objPrintEOBAR = null ;					
			XmlElement objTargetElement = null ;
			XmlElement objRootElement = null ;
			
			int iTransId = 0 ;
			bool bIsSilent = false ;
			bool bIsPostCheck = false ;
			string sState = string.Empty ;
			string sPDFSaveFilepath = string.Empty ; 
			
			try
			{
				// Get the Trans Id.
				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//TransId" );
				if( objTargetElement == null )
				{
                    p_objErrOut.Add("TransIdMissing", Globalization.GetString("PrintEOBAdaptor.TransIdNotFound", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 835
					return false;
				}
				iTransId = Conversion.ConvertStrToInteger( objTargetElement.InnerText );
				
				// Get the Is Silent Flag.
				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//IsSilent" );
				if( objTargetElement == null )
				{
                    p_objErrOut.Add("IsSilentMissing", Globalization.GetString("PrintEOBAdaptor.IsSilentNotFound", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 835
					return false;
				}
				bIsSilent = Conversion.ConvertStrToBool( objTargetElement.InnerText );

				// Get the State
				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//State" );
				if( objTargetElement == null )
				{
                    p_objErrOut.Add("StateMissing", Globalization.GetString("PrintEOBAdaptor.PrintEOBARReport.StateNotFound", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 835
					return false;
				}
				sState = objTargetElement.InnerText ;

				// Get the Is Post Check Flag
				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//IsPostCheck" );
				if( objTargetElement == null )
				{
                    p_objErrOut.Add("IsPostCheckMissing", Globalization.GetString("PrintEOBAdaptor.PrintEOBARReport.IsPostCheckNotFound", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 835
					return false;
				}
				bIsPostCheck = Conversion.ConvertStrToBool( objTargetElement.InnerText );
				
				// Initialize the PrintEOBAR object.
				objPrintEOBAR = new PrintEOBAR( userLogin.LoginName , userLogin.Password , userLogin.objRiskmasterDatabase.DataSourceName , userLogin.objRiskmasterDatabase.DataSourceId.ToString(), base.ClientId);//rkaur27 

				// Call PrintEOBReport Function.
				objPrintEOBAR.PrintEOBReport( iTransId, bIsSilent, sState, bIsPostCheck, out sPDFSaveFilepath );
				
				// Copy the binary content of file in the p_objXmlOut doc.
				objRootElement = p_objXmlOut.CreateElement( XML_OUT_HEADER_NODE_NAME );
				p_objXmlOut.AppendChild( objRootElement );
				this.CreateNodeWithFileContent( "PrintEOBARReport", objRootElement , sPDFSaveFilepath );

				return( true );
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add( p_objException , BusinessAdaptorErrorType.Error );
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("PrintEOBAdaptor.PrintEOBARReport.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 835
				return false;
			}
			finally
			{
				if (objPrintEOBAR != null)
                    objPrintEOBAR.Dispose();
				objTargetElement = null;
				objRootElement = null ;
			}
		}

		#endregion 

		#region PrintEOBBRS

		/// <summary>
		///		This method is a wrapper to PrintEOBBRS.PrintEOBReport() method.
		///		Print the EOB report.
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<PrintEOBBRSReport>
		///				<TransId>Trans Id</TransId>
		///				<IsSilent>Is Silent</IsSilent>
		///			</PrintEOBBRSReport>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document</param>
		///
		///		The structure of output XML document would be:
		///		<PrintEOB>
		///			<File Name="" Type="PrintEOBBRSReport">
		///			</File>
		///		</PrintEOB>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool PrintEOBBRSReport( XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			PrintEOBBRS objPrintEOBBRS = null ;					
			XmlElement objTargetElement = null ;
			XmlElement objRootElement = null ;

			int iTransId = 0 ;
			bool bIsSilent = false ;
			string sPDFFileName = string.Empty ;
            string sFileName = string.Empty;
            string sFiles = string.Empty;
            bool bPrintCheckPdfOrDirect = false;
            string sClaimId = string.Empty;
			try
			{
				// Get the Trans Id.
				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//TransId" );
				if( objTargetElement == null )
				{
                    p_objErrOut.Add("TransIdMissing", Globalization.GetString("PrintEOBAdaptor.TransIdNotFound", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 835
					return false;
				}
				iTransId = Conversion.ConvertStrToInteger( objTargetElement.InnerText );
				
				// Get the Is Silent Flag.
				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//IsSilent" );
				if( objTargetElement == null )
				{
                    p_objErrOut.Add("IsSilentMissing", Globalization.GetString("PrintEOBAdaptor.IsSilentNotFound", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 835
					return false;
				}
				bIsSilent = Conversion.ConvertStrToBool( objTargetElement.InnerText );

                // Get the State
                objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//ClaimId");
                if (objTargetElement == null)
                {
                    p_objErrOut.Add("ClaimMissing", Globalization.GetString("PrintEOBAdaptor.ClaimNotFound", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 835
                    return false;
                }
                sClaimId = objTargetElement.InnerText;

				// Copy the binary content of file in the p_objXmlOut doc.
				objRootElement = p_objXmlOut.CreateElement( XML_OUT_HEADER_NODE_NAME );
                // Initialize the PrintEOBBRS object.
                objPrintEOBBRS = new PrintEOBBRS(userLogin.LoginName, userLogin.Password, userLogin.objRiskmasterDatabase.DataSourceName, userLogin.objRiskmasterDatabase.DataSourceId.ToString(), base.ClientId);//rkaur27
                bPrintCheckPdfOrDirect = objPrintEOBBRS.m_bDirectToPrinter;
                // Adding attribute which tell us on UI that are we Printing directly to Printer or not
                if (bPrintCheckPdfOrDirect == true)
                {
                    if(objPrintEOBBRS.m_bIsPrinterSelected)
                    {
                        objRootElement.SetAttribute("PrintDirectlyToPrinter", "true");
                    } // if
                    else
                    {
                        objRootElement.SetAttribute("PrintDirectlyToPrinter", "NoValue");
                    } // else

                }
                else
                {
                    objRootElement.SetAttribute("PrintDirectlyToPrinter", "false");
                }
                p_objXmlOut.AppendChild(objRootElement);

				PrintCheckStub objPrintCheckStub = new PrintCheckStub( userLogin.LoginName , userLogin.Password , userLogin.objRiskmasterDatabase.DataSourceName , userLogin.objRiskmasterDatabase.DataSourceId.ToString(), base.ClientId);//rkaur27
               
				if( objPrintCheckStub.PrintDetail( iTransId , out sPDFFileName , out sFileName ) )
				{
                    this.CreateNodeWithFileContent("StubDetail", objRootElement, sPDFFileName, sFileName);
                    objRootElement.SetAttribute("StubDetail", "true");
				} // if
                else
                {
                    objRootElement.SetAttribute("StubDetail", "false");
                } // else
                sFiles = sFileName;
				
				try
				{
					// Call PrintEOBReport Function.
                    //Debabrata Biswas
                    //call HTML report template if Riskmaster.config has the EOBClaimHTMLFormatFile and EOBPayeeHTMLFormatFile nodes in
                    
                    //if(RMConfigurationManager.GetDictionarySectionSettings("PrintEOB")["EOBClaimHTMLFormatFile"] != null &&
                    //    RMConfigurationManager.GetDictionarySectionSettings("PrintEOB")["EOBPayeeHTMLFormatFile"] != null)
                    //if(RMConfigurationManager.GetNameValueSectionSettings("PrintEOB")["EOBClaimHTMLFormatFile"]!=null 
                    //   && RMConfigurationManager.GetNameValueSectionSettings("PrintEOB")["EOBPayeeHTMLFormatFile"] != null)                    
                    //    objPrintEOBBRS.PrintEOBHTMLReport(iTransId, bIsSilent, out sPDFFileName, out sFileName);
                    //else
                        //objPrintEOBBRS.PrintEOBReport(iTransId, bIsSilent, out sPDFFileName, out sFileName);
                    objPrintEOBBRS.PrintEOBReport(iTransId, bIsSilent, sClaimId, out sPDFFileName, out sFileName);

					if( File.Exists( sPDFFileName ) )
					{
                        this.CreateNodeWithFileContent("PrintEOBBRSReport", objRootElement, sPDFFileName, sFileName);
                        objRootElement.SetAttribute("PrintEOBBRSReport", "true");
					} // if
                    else
                    {
                        objRootElement.SetAttribute("PrintEOBBRSReport", "false");
                    } // else
				}
				catch(Exception p_objException)
				{
					p_objErrOut.Add( p_objException , BusinessAdaptorErrorType.PopupMessage );
					return false;
				}
                if(sFileName != "")
                {
                    sFiles = sFiles + "," + sFileName;
                } // if
                XmlElement objElement = null;
                objElement = objRootElement.OwnerDocument.CreateElement("FileNames");
                objElement.InnerText = sFiles;
                objRootElement.AppendChild(objElement);
				return( true );
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add( p_objException , BusinessAdaptorErrorType.Error );
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("PrintEOBAdaptor.PrintEOBBRSReport.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 835
				return false;
			}
			finally
			{
				if (objPrintEOBBRS != null)
                    objPrintEOBBRS.Dispose();
				objTargetElement = null;
				objRootElement = null ;
			}
		}

		#endregion 

		#endregion 

		#region Private Methods

        private string CreateNodeWithFileContentForEOB(string p_sNodeName, XmlElement p_objParentNode, string p_sOutFilepath)
        {
            XmlElement objElement = null;
            MemoryStream objMemoryStream = null;
            FileStream objFileStream = null;
            BinaryReader objBReader = null;
            byte[] arrRet = null;
            string sFilename = "";
            try
            {
                // Check the file existence.
                if (!File.Exists(p_sOutFilepath))
                    throw new RMAppException(Globalization.GetString("PrintChecksAdaptor.CreateNodeWithFileContent.FileNotFound", base.ClientId));//sharishkumar Jira 835

                // Read the file in memory stream.


                // Delete the temp file.
                //File.Delete( p_sOutFilepath );

                // Insert the memory stream as base64 string into the Xml document.
                objElement = p_objParentNode.OwnerDocument.CreateElement("File");
                objElement.SetAttribute("Name", p_sNodeName);
                objElement.SetAttribute("FileName", System.IO.Path.GetFileName(p_sOutFilepath));
                sFilename = System.IO.Path.GetFileName(p_sOutFilepath);
                p_objParentNode.AppendChild(objElement);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("PrintEOBAdaptor.CreateNodeWithFileContent.Error", base.ClientId), p_objException);//sharishkumar Jira 835
            }
            finally
            {
                objElement = null;
                if (objMemoryStream != null)
                {
                    objMemoryStream.Close();
                    objMemoryStream = null;
                }
                if (objFileStream != null)
                {
                    objFileStream.Close();
                    objFileStream = null;
                }
                if (objBReader != null)
                {
                    objBReader.Close();
                    objBReader = null;
                }
            }
            return sFilename;
        }
        public bool DeleteEobFiles(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            string[] arrFileNames = null;
            string sFullFileName = string.Empty;
            try
            {
                if (p_objXmlIn.SelectSingleNode("//FileNames") != null)
                {
                    arrFileNames = p_objXmlIn.SelectSingleNode("//FileNames").InnerText.Split(",".ToCharArray()[0]);
                }
                if (arrFileNames != null)
                {
                    foreach (string sTmpFileName in arrFileNames)
                    {
                        sFullFileName = Path.Combine(RMConfigurator.CombineFilePath(RMConfigurator.UserDataPath, "EOB"), sTmpFileName);

                        if (File.Exists(sFullFileName))
                        {
                            File.Delete(sFullFileName);
                        }
                    }
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("PrintEOBAdaptor.CreateNodeWithFileContent.Error", base.ClientId), p_objException);//sharishkumar Jira 835
            }
            return (true);

        }
		/// <summary>
		/// This function appends the output file to the output xml document.
		/// </summary>
		/// <param name="p_sNodeName">Name of the Node</param>
		/// <param name="p_objParentNode">Parent Node</param>
		/// <param name="p_sOutFilepath">Complete Path of the file.</param>
		private void CreateNodeWithFileContent( string p_sNodeName, XmlElement p_objParentNode, string p_sOutFilepath )
		{
			XmlElement objElement = null;
			MemoryStream objMemoryStream = null;
			FileStream objFileStream = null ;
			BinaryReader objBReader = null ;
			byte[] arrRet = null ;
			try
			{
				// Check the file existence.
				if( ! File.Exists( p_sOutFilepath ) )
                    throw new RMAppException(Globalization.GetString("PrintChecksAdaptor.CreateNodeWithFileContent.FileNotFound", base.ClientId));//sharishkumar Jira 835
				
				// Read the file in memory stream.
				objFileStream = new FileStream( p_sOutFilepath ,FileMode.Open);
				objBReader = new BinaryReader( objFileStream );
				arrRet = objBReader.ReadBytes( (int)objFileStream.Length );				
				objMemoryStream = new MemoryStream( arrRet );
				
				objFileStream.Close();

				// Delete the temp file.
				//File.Delete( p_sOutFilepath );
				
				// Insert the memory stream as base64 string into the Xml document.
				objElement = p_objParentNode.OwnerDocument.CreateElement( "File" );											
				objElement.SetAttribute( "Name", p_sNodeName ); 																
				objElement.InnerText = Convert.ToBase64String( objMemoryStream.ToArray() );
				p_objParentNode.AppendChild( objElement );				
			}
			catch( Exception p_objException )
			{
                throw new RMAppException(Globalization.GetString("PrintEOBAdaptor.CreateNodeWithFileContent.Error", base.ClientId), p_objException);//sharishkumar Jira 835
			}
			finally
			{
				objElement = null;
				if( objMemoryStream != null ) 
				{
					objMemoryStream.Close();
					objMemoryStream = null;
				}
				if( objFileStream != null )
				{
					objFileStream.Close();
					objFileStream = null ;
				}
				if( objBReader != null )
				{
					objBReader.Close();
					objBReader = null ;
				}
			}
		}

        private void CreateNodeWithFileContent(string p_sNodeName, XmlElement p_objParentNode, string p_sOutFilepath, string p_sFileName)
        {
            XmlElement objElement = null;
            MemoryStream objMemoryStream = null;
            FileStream objFileStream = null;
            BinaryReader objBReader = null;
            byte[] arrRet = null;
            try
            {
                // Check the file existence.
                if (!File.Exists(p_sOutFilepath))
                    throw new RMAppException(Globalization.GetString("PrintChecksAdaptor.CreateNodeWithFileContent.FileNotFound", base.ClientId));//sharishkumar Jira 835

                // Read the file in memory stream.
                objFileStream = new FileStream(p_sOutFilepath, FileMode.Open);
                objBReader = new BinaryReader(objFileStream);
                arrRet = objBReader.ReadBytes((int)objFileStream.Length);
                objMemoryStream = new MemoryStream(arrRet);

                objFileStream.Close();

                // Delete the temp file.
                //File.Delete( p_sOutFilepath );

                // Insert the memory stream as base64 string into the Xml document.
                objElement = p_objParentNode.OwnerDocument.CreateElement("File");
                objElement.SetAttribute("Name", p_sNodeName);
                objElement.SetAttribute("FileName", p_sFileName);
                objElement.InnerText = Convert.ToBase64String(objMemoryStream.ToArray());
                p_objParentNode.AppendChild(objElement);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("PrintEOBAdaptor.CreateNodeWithFileContent.Error", base.ClientId), p_objException);//sharishkumar Jira 835
            }
            finally
            {
                objElement = null;
                if (objMemoryStream != null)
                {
                    objMemoryStream.Close();
                    objMemoryStream = null;
                }
                if (objFileStream != null)
                {
                    objFileStream.Close();
                    objFileStream = null;
                }
                if (objBReader != null)
                {
                    objBReader.Close();
                    objBReader = null;
                }
            }
        }

		#endregion
	}
}
