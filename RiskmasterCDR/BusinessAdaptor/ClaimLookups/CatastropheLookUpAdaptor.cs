﻿using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.Application.ClaimLookups;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.BusinessAdaptor
{
    ///************************************************************** 
    ///* $File		: CatastropheLookUpAdaptor.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 13 June 2012
    ///* $Author	: Amandeep Kaur
    ///* $Comment	: 
    ///* $Source	: 
    ///**************************************************************

    /// <summary>	
    ///	This class is used to call the Business layer component for Claim Catastrohpe which 
    ///	contains method to get ClaimCatastrophe List that meet the Criteria.	
    /// </summary>
    public class CatastropheLookUpAdaptor : BusinessAdaptorBase
    {
        #region Constructor
        
        public CatastropheLookUpAdaptor() { }
        
        #endregion

        #region Public Methods
        /// <summary>
        ///		This function returns xml containing data to be shown on screen
        /// </summary>
        /// <param name="p_objXmlIn">Input parameters as xml</param>
        /// <param name="p_objXmlOut">Result as Output xml
        ///		Output structure is as follows-:
        /// </param>
        /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
        /// <returns>Success -True or Failure -false in execution of the function</returns>
        public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            CatastropheLookUp objCatastropheLookUp = null;
			XmlElement objElement=null;				//used for parsing the input xml           
            int iCatCodeId = 0;                   
			string sEventDate=string.Empty;
            string sTableName = string.Empty;
            int iCatState = 0;//dvatsa JIRA RMA-4495
			try
			{				             
				//check existence of Event Date which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//EventDate");
				if (objElement==null)
				{
                    p_objErrOut.Add("ClaimCatastropheAdaptorParameterMissing", Globalization.GetString("ClaimCatastropheAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				if (objElement.InnerText.Trim()!=string.Empty)
					sEventDate=Conversion.ToDbDate(Convert.ToDateTime(objElement.InnerText));

                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//CatCodeId");
                if (objElement == null)
                {
                    p_objErrOut.Add("CatastropheLookUpAdaptorParameterMissing", Globalization.GetString("CatastropheLookUpAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                iCatCodeId = Conversion.ConvertObjToInt(objElement.InnerText, base.ClientId);

                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//TableName");
                if (objElement == null)
                {
                    p_objErrOut.Add("CatastropheLookUpAdaptorParameterMissing", Globalization.GetString("CatastropheLookUpAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                sTableName = objElement.InnerText;
                //dvatsa JIRA RMA-4495 (start)
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//CatState");
                if (objElement != null)
                {
                    iCatState = Convert.ToInt32(objElement.InnerText);
                }
                else
                {
                    p_objErrOut.Add("CatastropheLookUpAdaptorParameterMissing", Globalization.GetString("CatastropheLookUpAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                //dvatsa JIRA RMA-4495 (end)
                objCatastropheLookUp = new CatastropheLookUp(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId);

                p_objXmlOut = objCatastropheLookUp.GetCatastropheLookUp(sEventDate, iCatCodeId, iCatState, sTableName);//dvatsa JIRA RMA-4495 
		
				return true;
			}
			catch(InvalidValueException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.SystemError);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("ClaimPolicyAdaptor.Get.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objCatastropheLookUp = null;
				objElement=null;
			}
        }

        #endregion
    }
}
