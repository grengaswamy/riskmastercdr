using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;
using Riskmaster.Application.ClaimLookups;

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: PiDiagHistLookUpAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 06-October-2005
	///* $Author	: Rahul Sharma
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for Primary Diagnosis History
	/// </summary>
	public class PiDiagHistLookUpAdaptor: BusinessAdaptorBase
	{
		#region Constructor
		public PiDiagHistLookUpAdaptor()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		#endregion

		#region Public Functions
		#region "LoadListDiagHistory(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)"
		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.ClaimLookups.PiDiagHistLookUp.LoadListDiagHistory() method.
		///		Gets the Primary Diagnosis History in XML format
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of XML document would be:
		///		<Document>
		///			<PiDiagHist>
		///				<ClaimId>Claim Id</ClaimId>
		///			</PiDiagHist>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be
		/// </param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		
		public bool LoadListDiagHistory(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.ClaimLookups.PiDiagHistLookUp  objPiDiagHistLookUp=null; //Application layer component
			int iClaimId = 0;
            int iPiRowId = 0;
			XmlElement objElement=null;					//used for parsing the input xml

			try
			{//Neha Fetch history for person involved as well
				//check existence of Claim Id which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ClaimId");
				if (objElement==null)
				{
					p_objErrOut.Add("PiDiagHistLookUpAdaptor.LoadListDiagHistory.Error",Globalization.GetString("PiDiagHistLookUpAdaptor.LoadListDiagHistory.IdMissing",base.ClientId),BusinessAdaptorErrorType.PopupMessage);
					return false;
				}
                iClaimId = Conversion.ConvertStrToInteger(objElement.InnerText);
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//PiRowId");
                if (objElement == null)
                {
                    p_objErrOut.Add("PiDiagHistLookUpAdaptor.LoadListDiagHistory.Error", Globalization.GetString("PiDiagHistLookUpAdaptor.LoadListDiagHistory.IdMissing",base.ClientId), BusinessAdaptorErrorType.PopupMessage);
                    return false;
                }
                iPiRowId = Conversion.ConvertStrToInteger(objElement.InnerText);

                using (objPiDiagHistLookUp = new Riskmaster.Application.ClaimLookups.PiDiagHistLookUp(m_userLogin,userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, userLogin.UserId, userLogin.GroupId,base.ClientId)) //mbahl3 //vkumar258 ML Changes
                {
                    if(iClaimId>0)
                    p_objXmlOut = objPiDiagHistLookUp.LoadListDiagHistory(iClaimId);
                    else
                   p_objXmlOut = objPiDiagHistLookUp.LoadListDiagHistoryForPI(iPiRowId);
                }
                
                
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("PiDiagHistLookUpAdaptor.LoadListDiagHistory.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
                if (objPiDiagHistLookUp != null)
                    objPiDiagHistLookUp.Dispose();

				objElement=null;
			}
		}

		#endregion

		#endregion
	}
}
