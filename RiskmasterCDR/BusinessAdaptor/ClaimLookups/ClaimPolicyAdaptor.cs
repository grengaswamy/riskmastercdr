using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.Application.ClaimLookups;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: ClaimPolicyAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 24-Aug-2005
	///* $Author	: Nikhil Kr. Garg
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	
	/// <summary>	
	///	This class is used to call the application layer component for Claim Policy which 
	///	contains method to get ClaimPolicy List.	
	/// </summary>
	public class ClaimPolicyAdaptor: BusinessAdaptorBase
	{
		#region Constructor
		public ClaimPolicyAdaptor(){}
		#endregion
		
		#region Public methods

		/// <summary>
		///		This function returns xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///		Output structure is as follows-:
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Get(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			LookupClaimPolicy objLookupClaimPolicy=null;
			XmlElement objElement=null;				//used for parsing the input xml
			int iClaimTypeCode=0;
			string sEventDate=string.Empty;
			string sClaimDate=string.Empty;
			int iDeptEID=0;
			int iLOB=0;
            int iJurisdictionId = 0; //Safeway: Gagan Safeway Retrofit Policy Jursidiction / REM: Added by rkaur7 on 05/22/2009 - MITS 16668            
//ABAHl3: MITS 25163- Policy Interface Implementation
            int iPageNumber = 0;
            string  sCurrencyType = string.Empty;
            int iEventStateId = 0;
			try
			{
				//check existence of Claim Type Code which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ClaimTypeCode");
				if (objElement==null)
				{
					p_objErrOut.Add("ClaimPolicyAdaptorParameterMissing",Globalization.GetString("ClaimPolicyAdaptorParameterMissing",base.ClientId),BusinessAdaptorErrorType.Error);
					return false;
				}
				iClaimTypeCode=Conversion.ConvertStrToInteger(objElement.InnerText);

				//check existence of Event Date which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//EventDate");
				if (objElement==null)
				{
					p_objErrOut.Add("ClaimPolicyAdaptorParameterMissing",Globalization.GetString("ClaimPolicyAdaptorParameterMissing",base.ClientId),BusinessAdaptorErrorType.Error);
					return false;
				}
				if (objElement.InnerText.Trim()!=string.Empty)
					sEventDate=Conversion.ToDbDate(Convert.ToDateTime(objElement.InnerText));

				//check existence of Claim Date which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ClaimDate");
				if (objElement==null)
				{
					p_objErrOut.Add("ClaimPolicyAdaptorParameterMissing",Globalization.GetString("ClaimPolicyAdaptorParameterMissing",base.ClientId),BusinessAdaptorErrorType.Error);
					return false;
				}
				if (objElement.InnerText.Trim()!=string.Empty)
					sClaimDate=Conversion.ToDbDate(Convert.ToDateTime(objElement.InnerText));

				//check existence of Dept EID which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//DeptEID");
				if (objElement==null)
				{
					p_objErrOut.Add("ClaimPolicyAdaptorParameterMissing",Globalization.GetString("ClaimPolicyAdaptorParameterMissing",base.ClientId),BusinessAdaptorErrorType.Error);
					return false;
				}
				iDeptEID=Conversion.ConvertStrToInteger(objElement.InnerText);

				//check existence of Dept EID which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//LOB");
				if (objElement==null)
				{
					p_objErrOut.Add("ClaimPolicyAdaptorParameterMissing",Globalization.GetString("ClaimPolicyAdaptorParameterMissing",base.ClientId),BusinessAdaptorErrorType.Error);
					return false;
				}
				iLOB=Conversion.ConvertStrToInteger(objElement.InnerText);

                //Gagan Safeway Retrofit Policy Jursidiction : - Jurisdiction also required for policy lookup screen / REM: Added by rkaur7 on 05/22/2009 - MITS 16668
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//Jurisdiction");
                if (objElement == null)
                {
                    p_objErrOut.Add("ClaimPolicyAdaptorParameterMissing", Globalization.GetString("ClaimPolicyAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                iJurisdictionId = Conversion.ConvertStrToInteger(objElement.InnerText);
                //Gagan Safeway Retrofit Policy Jursidiction : END / REM: END

				objLookupClaimPolicy = new LookupClaimPolicy(base.userLogin.objRiskmasterDatabase.DataSourceName,base.userLogin.LoginName,base.userLogin.Password, base.ClientId);

				//get Policy List

                //Gagan Safeway Retrofit Policy Jursidiction - A new parameter iJurisdictionId is also passed as is required for policy lookup / Added by rkaur7 on 05/22/2009 - MITS 16668
//ABAHl3: MITS 25163- Policy Interface Implementation
                
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//PageNumber");
                if (objElement == null)
                {
                    iPageNumber = 1;
                }
                else
                {
                    iPageNumber = Conversion.ConvertStrToInteger(objElement.InnerText);
                }
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//CurrencyType");
                if (objElement != null)
                {
                    sCurrencyType = objElement.InnerText;
                }
                //ABAHl3 End: MITS 25163- Policy Interface Implementation		

                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//EventState");
                if (objElement != null)
                {
                    iEventStateId =Conversion.ConvertStrToInteger(objElement.InnerText);
                }
                p_objXmlOut = objLookupClaimPolicy.GetLookupClaimPolicyList(iClaimTypeCode, sEventDate, sClaimDate, iDeptEID, iLOB, iJurisdictionId,iPageNumber,sCurrencyType,iEventStateId);
               
		
				return true;
			}
			catch(InvalidValueException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.SystemError);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("ClaimPolicyAdaptor.Get.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objLookupClaimPolicy = null;
				objElement=null;
			}
		}


  

        #endregion
	}
}
