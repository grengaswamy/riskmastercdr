using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.Application.ClaimLookups;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: ClaimEntityInstructionsAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 28-Aug-2005
	///* $Author	: Nikhil Kr. Garg
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	
	/// <summary>	
	///	This class is used to call the application layer component for Claim Entity Instructions which 
	///	contains method to get Instructions for the Claim Entity.	
	/// </summary>
	public class ClaimEntityInstructionsAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		public ClaimEntityInstructionsAdaptor(){}
		#endregion

		#region Public methods

		/// <summary>
		///		This function returns xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///		Output structure is as follows-:
		///		<Instructions>Claim Instructions</Instructions>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Get(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			ClaimEntityInstructions objClaimEntityInstructions=null;
			XmlElement objElement=null;				//used for parsing the input xml
			int iEId=0;

			try
			{
				//check existence of EntityId which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//EId");
				if (objElement==null)
				{
					p_objErrOut.Add("ClaimEntityInstructionsAdaptorParameterMissing",Globalization.GetString("ClaimEntityInstructionsAdaptorParameterMissing",base.ClientId),BusinessAdaptorErrorType.Error);
					return false;
				}
				iEId=Conversion.ConvertStrToInteger(objElement.InnerText);

                objClaimEntityInstructions = new ClaimEntityInstructions(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId);

				//get Instructions
				p_objXmlOut=objClaimEntityInstructions.GetClaimEntityInstructions(iEId);
				
				return true;
			}
			catch(InvalidValueException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("ClaimEntityInstructionsAdaptor.Get.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objClaimEntityInstructions = null;
				objElement=null;
			}

		}

		#endregion
	}
}
