using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Application.OrgHierarchy;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using System.IO;


namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: OrgHierarchyAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 03-Mar-2005
	///* $Author	: Nikhil Kr. Garg
	///* $Comment	: 
	///* $Source	: 
	///* Amendment  -->
	///  1.Date		: 7 Feb 06 
	///    Desc		: Replacing RMO_ACCESS use, and changing params of permission
	///				  violation exception so it can be correctly called. 
	///    Author	: Sumit
	///**************************************************************
	/// <summary>	
	///	This class performs the operations related to Organization Hierarchy functionality.
	/// </summary>
	public class OrgHierarchyAdaptor: BusinessAdaptorBase
	{   
		private const int RMB_ORG = 17000;
		private const int RMB_ORG_EXPO = 17150;
        private const string NO_ENTITY = "no entity";
		//private const int RMO_ACCESS = 0;
		//private const int RMO_VIEW = 1;
		//private const int RMO_UPDATE = 2;
		//private const int RMO_CREATE = 3;
		//private const int RMO_DELETE = 4;
		#region Constructor
		/// <summary>
		/// Constructor for the class.
		/// </summary>
		public OrgHierarchyAdaptor()
		{
		}
		#endregion
        
		#region Public Methods
		public bool UpdateCompanyInformation(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{   // Nitesh 05Jan2006 Check security and fail if sufficient privilege doesn't exist
			if (!userLogin.IsAllowedEx(RMB_ORG, RMPermissions.RMO_ACCESS))
				throw new PermissionViolationException(RMPermissions.RMO_ACCESS,RMB_ORG);

			Riskmaster.Application.OrgHierarchy.OrgHierarchy objOrgHierarchy=null; //Application layer component
			string sCompanyInfo=string.Empty;		//Company Information passed in the input XML document
			int iEntityId=0;						//EntityId passed in the input XML document
			XmlElement objElement=null;				//used for parsing the input xml
			try
			{
				//check existence of company information which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//CompanyInfo");
				if (objElement==null)
				{
					p_objErrOut.Add("OrgHierarchyParameterMissing",Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId),BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
					return false;
				}
				sCompanyInfo=objElement.InnerXml;

				//check existence of entity id which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//EntityId");
				if (objElement==null)
				{
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
					return false;
				}
				iEntityId=Conversion.ConvertStrToInteger(objElement.InnerText);

                //objOrgHierarchy = new Riskmaster.Application.OrgHierarchy.OrgHierarchy(connectionString);
                objOrgHierarchy = new OrgHierarchy(connectionString, base.ClientId);
				string strRetVal="";
				XmlElement objChild=null;		
				// add the company information

				// Nitesh 05Jan2006 Check security and fail if sufficient privilege doesn't exist
				if(iEntityId == 0)
				{
					if (!userLogin.IsAllowedEx(RMB_ORG, RMPermissions.RMO_CREATE))
						throw new PermissionViolationException(RMPermissions.RMO_CREATE, RMB_ORG );
				}
				else
				{
						if (!userLogin.IsAllowedEx(RMB_ORG, RMPermissions.RMO_UPDATE))
						throw new PermissionViolationException(RMPermissions.RMO_UPDATE, RMB_ORG);
				}


				strRetVal=objOrgHierarchy.UpdateCompanyInformation(iEntityId,sCompanyInfo,userLogin.LoginName,userLogin.Password,userLogin.objRiskmasterDatabase.DataSourceName,base.connectionString);
				objChild=p_objXmlOut.CreateElement("EntityID");
				objChild.InnerXml=strRetVal;
				p_objXmlOut.AppendChild(objChild);
				if(Conversion.ConvertStrToInteger(strRetVal)==0)
				{
					return false;
				}
				else
				{
					return true;
				}

			}
			catch (XmlOperationException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch (DataModelException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("OrgHierarchy.AddCompanyInformation.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
				return false;
			}
			finally
			{
				objOrgHierarchy = null;
				objElement=null;
			}
		}
		
		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.OrgHierarchy.OrgHierarchy.AddCompanyInformation() method.
		///		Adds the company information in the database.
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<Document>
		///			<OrgHierarchy>
		///				<EntityId>Entity Id</EntityId>
		///				<CompanyInfo>Company Information</CompanyInfo>
		///			</OrgHierarchy>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Null value.Function only returns a true/false</param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		
		public bool AddCompanyInformation(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.OrgHierarchy.OrgHierarchy objOrgHierarchy=null; //Application layer component
			string sCompanyInfo=string.Empty;		//Company Information passed in the input XML document
			int iEntityId=0;						//EntityId passed in the input XML document
			XmlElement objElement=null;				//used for parsing the input xml

			try
			{
				//check existence of company information which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//CompanyInfo");
				if (objElement==null)
				{
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
					return false;
				}
				sCompanyInfo=objElement.InnerXml;

				//check existence of entity id which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//EntityId");
				if (objElement==null)
				{
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
					return false;
				}
				iEntityId=Conversion.ConvertStrToInteger(objElement.InnerText);

				//objOrgHierarchy = new Riskmaster.Application.OrgHierarchy.OrgHierarchy(connectionString);
                objOrgHierarchy = new OrgHierarchy(connectionString, base.ClientId);
				string strRetVal="";
				XmlElement objChild=null;		
				// add the company information
				strRetVal=objOrgHierarchy.AddCompanyInformation(iEntityId,sCompanyInfo,userLogin.LoginName,userLogin.Password,userLogin.objRiskmasterDatabase.DataSourceName);
				objChild=p_objXmlOut.CreateElement("EntityID");
				objChild.InnerXml=strRetVal;
				p_objXmlOut.AppendChild(objChild);
				if(Conversion.ConvertStrToInteger(strRetVal)==0)
				{
					return false;
				}
				else
				{
					return true;
				}

			}
			catch (XmlOperationException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch (DataModelException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("OrgHierarchy.AddCompanyInformation.Error",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
				return false;
			}
			finally
			{
				objOrgHierarchy = null;
				objElement=null;
			}
		}

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.OrgHierarchy.OrgHierarchy.EditCompanyInformation() method.
		///		Edits the company information for the give Entity Id. 		
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<Document>
		///			<OrgHierarchy>
		///				<EntityId>Entity Id</EntityId>
		///				<CompanyInfo>Company Information</CompanyInfo>
		///			</OrgHierarchy>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be
		///		<DepartmentInfo>
		///			<LastUpdate /><BusinessTypeCode /><County /><NatureOfBusiness /><SicCode /><SicCodeDesc />
		///			<NAICSCode /><WCFillingNumber /><EntityTableId /><LastName /><LastNameSoundex /><FirstName />
		///			<AlsoKnownAs /><Abbreviation /><CCCode /><Addr1 /><Addr2 /><City /><CountryCode /><StateId />
		///			<ZipCode /><ParentEID /><TaxID /><Contact /><Comments /><EmailTypeCode /><EmailAddress />
		///			<SexCode /><BirthDate /><Phone1 /><Phone2 /><FaxNumber /><DeletedFlag /><EffectiveStartDate />
		///			<EffectiveEndDate /><TriggerDateField /><RMUSerID /><FreezePayments /><Parent1099EID />
		///			<Report1099Flag /><MiddleName /><Title /><EntityID /><DateTimeAdded /><AddedByUser />
		///			<OperationInfo>
		///				<Operating>
		///					<Initial /><OperatingName /><OperatingID /><OperatingName /><Initial /><OperatingID />
		///				</Operating>
		///			</OperationInfo> 
		///			<ContactInfo>
		///				<Contact>
		///					<ContactID /><ContactName /><ContactTitle /><ContactInitials /><Addr1 /><Addr2 />
		///					<ContactCity /><StateID /><ZipCode /><Phone /><FaxNumber /><EMailAddress />
		///					
		///				</Contact>
		///			</ContactInfo>
		///			<OrgHierarchyInfo>
		///				<TableName /><TableValue />
		///			</OrgHierarchyInfo> 
		///			<ClientLimits>
		///				<LOB_CODE /><RESERVE_TYPE_CODE /><MAX_AMOUNT /><PAYMENT_FLAG />
		///			</ClientLimits>
		///			<SupplementalInfo />
		///			<ExposureInfo>
		///				<ExposureRowID /><StartDate /><EndDate /><NoOfEmployees /><WorkHrs /><PayRollAmt />
		///				<AssetValue /><SquareFootage /><VehicleCount /><TotalRevenue /><OtherBase />
		///				<RiskMgtOverHead /><UserGeneratedFlag /><NoOfEmployees /><WorkHrs /><PayRollAmt />
		///				<StartDate /><AssetValue /><SquareFootage /><VehicleCount /><TotalRevenue /><OtherBase />
		///				<RiskMgtOverHead /><UserGeneratedFlag /><NoOfEmployees /><WorkHrs /><PayRollAmt />
		///				
		///				
		///			</ExposureInfo>
		///			<SelfInsuredInfo>
		///				<SelfInsured>
		///					<SIRowId /><EntityId /><Jurisdiction /><DeletedFlag /><LegalEntityId />
		///					<CertificateNumber /><EffectiveDate /><ExpirationDate /><Authorization />
		///					<Organization /><DttmRcdAdded /><DttmRcdLastUpd /><UpdatedByUser />	<AddedByUser />
		///					<DeletedFlag /><LegalEntityId /><CertificateNumber /><EffectiveDate />
		///					<ExpirationDate /><Authorization /><Organization /><DttmRcdAdded /><DttmRcdLastUpd />
		///					<UpdatedByUser /><AddedByUser />
		///				</SelfInsured>
		///			</SelfInsuredInfo>
		///		</DepartmentInfo>
		/// </param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool EditCompanyInformation(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			// Nitesh 05Jan2006 Check security and fail if sufficient privilege doesn't exist
			if (!userLogin.IsAllowedEx(RMB_ORG, RMPermissions.RMO_ACCESS))
					throw new PermissionViolationException(RMPermissions.RMO_ACCESS,RMB_ORG);
			if (!userLogin.IsAllowedEx(RMB_ORG, RMPermissions.RMO_VIEW))
					throw new PermissionViolationException(RMPermissions.RMO_VIEW , RMB_ORG);
			Riskmaster.Application.OrgHierarchy.OrgHierarchy objOrgHierarchy=null; //Application layer component
			string sCompanyInfo=string.Empty;		//Company Information passed in the input XML document
			int iEntityId=0;						//EntityId passed in the input XML document
			XmlElement objElement=null;				//used for parsing the input xml
			int iParentEid=0;			

			try
			{
				//check existence of company information which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//CompanyInfo");
				if (objElement==null)
				{
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
					return false;
				}
				sCompanyInfo=objElement.InnerXml;

				//check existence of entity id which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//EntityId");
				if (objElement==null)
				{
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
					return false;
				}
				iEntityId=Conversion.ConvertStrToInteger(objElement.InnerText);

				// Mihika 27-Feb-2006 Defect# 2331
				// Used to populate the parent eid in case entity does not exist
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ParentEid");
				if (objElement!=null)
				{
					iParentEid=Conversion.ConvertStrToInteger(objElement.InnerText);
				}
				
				//objOrgHierarchy = new Riskmaster.Application.OrgHierarchy.OrgHierarchy(connectionString);
                objOrgHierarchy = new OrgHierarchy(connectionString, base.ClientId);
				// edit the company information
				p_objXmlOut.LoadXml(objOrgHierarchy.EditCompanyInformation(iEntityId,userLogin.LoginName,userLogin.Password,userLogin.objRiskmasterDatabase.DataSourceName,sCompanyInfo, iParentEid));
				p_objXmlOut.LoadXml(GetExpoInfoSecurity(p_objXmlOut.OuterXml ));
				return true;

			}
			catch (XmlOperationException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch (DataModelException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("OrgHierarchy.EditCompanyInformation.Error",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
				return false;
			}
			finally
			{
				objOrgHierarchy = null;
				objElement=null;
			}
		}
		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.OrgHierarchy.OrgHierarchy.GetOrgHierarchyXml() method.
		///		Fetches the Organization Hierarchy information.
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<Document>
		///				<InputXml>XML to be populated with the Parent Eid</InputXml>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be
		///		<ReturnXml>
		///			<OutputResult>
		///					<xml>
		///						<parent id="" name="" />
		///					</xml>			
		///			</OutputResult>
		///		</ReturnXml>
		/// </param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		
		public bool DeleteCompanyInformation(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			// Nitesh 05Jan2006 Check security and fail if sufficient privilege doesn't exist
			if (!userLogin.IsAllowedEx(RMB_ORG, RMPermissions.RMO_DELETE))
				throw new PermissionViolationException(RMPermissions.RMO_DELETE,RMB_ORG );
			Riskmaster.Application.OrgHierarchy.OrgHierarchy objOrgHierarchy=null; //Application layer component
			string sCompanyInfo=string.Empty;		//Company Information passed in the input XML document
			int iEntityId=0;						//EntityId passed in the input XML document
			XmlElement objElement=null;				//used for parsing the input xml

			try
			{
				//check existence of company information which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//EntityId");
				if (objElement==null)
				{
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
					return false;
				}
				iEntityId=Conversion.ConvertStrToInteger(objElement.InnerText);
				//objOrgHierarchy = new Riskmaster.Application.OrgHierarchy.OrgHierarchy(connectionString);
                objOrgHierarchy = new OrgHierarchy(connectionString, base.ClientId);
				string strRetVal="";
				// Delete the company information
				strRetVal=objOrgHierarchy.DeleteCompanyInformation(iEntityId,userLogin.LoginName,userLogin.Password,userLogin.objRiskmasterDatabase.DataSourceName);
				if(strRetVal.Equals("Success"))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			
			catch (XmlOperationException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch (DataModelException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("OrgHierarchy.AddCompanyInformation.Error",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
				return false;
			}
			finally
			{
				objOrgHierarchy = null;
				objElement=null;
			}
		}
		
		
		public bool GetSpecificData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.OrgHierarchy.OrgHierarchy objOrgHierarchy=null; //Application layer component
			XmlElement objElement=null;		//used for parsing the input xml
			string sReturnXml=string.Empty; //used for holding the output xml string
			string sInputXml=string.Empty;	//XML to be populated with the Org Hierarchy data

			objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//OrgHierarchy");
			if (objElement==null)
			{
				try
				{
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
				}
				catch(Exception ex)
				{
					string ss= ex.Message;
				}
				return false;
			}
			sInputXml=objElement.InnerXml;

			//objOrgHierarchy = new Riskmaster.Application.OrgHierarchy.OrgHierarchy(connectionString);
            objOrgHierarchy = new OrgHierarchy(connectionString, base.ClientId);

			//get org hierarchy xml
			sReturnXml=objOrgHierarchy.GetSpecificData(sInputXml);
			//create the output xml by including the Output parameter also
			sReturnXml="<ReturnXml><OutputResult>" + sReturnXml + "</OutputResult></ReturnXml>";
				
			try
			{
				p_objXmlOut.LoadXml(sReturnXml);
			}
			catch(Exception ex)
			{
				string ss1= ex.Message;
			}

			return true;
		}

		/// <summary>
		/// This function fetches parent list..
		/// </summary>
		/// <param name="p_objXmlIn"></param>
		/// <param name="p_objXmlOut"></param>
		/// <param name="p_objErrOut"></param>
		/// <returns></returns>
        public bool GetParentList(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.OrgHierarchy.OrgHierarchy objOrgHierarchy=null; //Application layer component
			XmlElement objElement=null;		//used for parsing the input xml
			string sReturnXml=string.Empty; //used for holding the output xml string
			string sInputXml=string.Empty;	//XML to be populated with the Org Hierarchy data

            try
            {
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//OrgHierarchy");
                if (objElement == null)
                {
                    try
                    {
                        p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
                    }
                    catch (Exception ex)
                    {
                        string ss = ex.Message;
                    }
                    return false;
                }
                sInputXml = objElement.InnerXml;

                //objOrgHierarchy = new Riskmaster.Application.OrgHierarchy.OrgHierarchy(connectionString);
                objOrgHierarchy = new OrgHierarchy(connectionString, base.ClientId);

                //get org hierarchy xml
                sReturnXml = objOrgHierarchy.GetParentList(sInputXml);
                //create the output xml by including the Output parameter also
                sReturnXml = "<ReturnXml><OutputResult>" + sReturnXml + "</OutputResult></ReturnXml>";


                p_objXmlOut.LoadXml(sReturnXml);
            }


            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("OrgHierarchy.ParentInformation.Error",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
                return false;
            }

			return true;
		}

		
		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.OrgHierarchy.OrgHierarchy.GetOrgHierarchyXml() method.
		///		Fetches the Organization Hierarchy information.
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<Document>
		///			<OrgHierarchy>
		///				<Expand>Whether the Org Hierarchy node is expanded or not</Expand>
		///				<Level>Level of the entities</Level>
		///				<Node>Node id</Node>
		///				<NodCat>Node category</NodCat>
		///				<Parent>Parent of the current node</Parent>
		///				<TableName>Table Name</TableName>
		///				<RowId>Row Id</RowId>
		///				<InputXml>XML to be populated with the Org Hierarchy data</InputXml>
		///			</OrgHierarchy>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be
		///		<ReturnXml>
		///			<OutputResult>
		///				<form name="" title="" cid="" topbuttons="" actionname="" bcolor="">
		///					<body1 req_func="" func=""/>
		///					<group name="" title="">
		///						<control name="" num_level="" type="" cmblevel="">
		///							<org name="" ischild="" child_name="" level="" parent="">
		///								<client name="" parent="" ischild="" child_name="" code="" id="" level="">
		///									<company name="" parent="" id="" child_name="" ischild="" code="" level="">
		///										<operation name="" parent="" id="" child_name="" ischild="" code="" level="">
		///											<region name="" parent="" id="" child_name="" ischild="" code="" level="">
		///												<division name="" parent="" id="" child_name="" ischild="" code="" level="">
		///													<location name="" parent="" id="" child_name="" ischild="" code="" isSelect="" level=""> 
		///														<facility name="" parent="" id="" child_name="" ischild="" code="" level="">
		///															<department name="" id="" isSelect="" ischild="" level="" />
		///														</facility>
		///													</location>
		///												</division>
		///											</region>
		///										</operation>
		///									</company>
		///								</client>
		///							<org>
		///						</control>
		///					</group>
		///					<levellist name="">
		///						<option value=""></option>
		///					</levellist>
		///				</form>
		///			</OutputResult>
		///			<EntityEnh></EntityEnh>
		///		</ReturnXml>
		/// </param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
        public bool GetOrgHierarchyXml(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            Riskmaster.Application.OrgHierarchy.OrgHierarchy objOrgHierarchy = null; //Application layer component
            bool bExpand = false;				//Whether the Org Hierarchy node is expanded or not
            long lLevel = 0;					//Level of the entities
            long lNode = 0;					//Node id
            string sNodCat = string.Empty;	//Node category 
            string sParent = string.Empty;	//Parent of the current node
            string sTableName = string.Empty;	//Table name
            long lRowId = 0;					//Row Id
            string sInputXml = string.Empty;	//XML to be populated with the Org Hierarchy data
            int iEntityEnh = 0;				//Flag to check whether enhanced entity search needs to be shown or not
            XmlElement objElement = null;		//used for parsing the input xml
            string sReturnXml = string.Empty; //used for holding the output xml string
            string sEventDate = string.Empty; //used for holding the event date if passed from UI
            string sClaimDate = string.Empty; //used for holding the claim date if passed from UI
            string sPolicyDate = string.Empty; //Added by Shivendu for MITS 17249
            try
            {
                //check existence of Expand which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//Expand");
                if (objElement == null)
                {
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
                    return false;
                }
                bExpand = Conversion.ConvertStrToBool(objElement.InnerText);

                //check existence of Level of the entities which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//Level");
                if (objElement == null)
                {
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
                    return false;
                }
                lLevel = Conversion.ConvertStrToLong(objElement.InnerText);

                //check existence of Node Id which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//Node");
                if (objElement == null)
                {
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
                    return false;
                }
                lNode = Conversion.ConvertStrToLong(objElement.InnerText);

                //check existence of Node Category which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//NodCat");
                if (objElement == null)
                {
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
                    return false;
                }
                sNodCat = objElement.InnerText;

                //check existence of Parent which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//Parent");
                if (objElement == null)
                {
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
                    return false;
                }
                sParent = objElement.InnerText;

                //check existence of Table Name which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//TableName");
                if (objElement == null)
                {
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
                    return false;
                }
                sTableName = objElement.InnerText;

                //check existence of Row Id which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//RowId");
                if (objElement == null)
                {
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
                    return false;
                }
                lRowId = Conversion.ConvertStrToLong(objElement.InnerText);

                //MITS 22530 Set user_id value here
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//InputXml//control[@name='dlevel']");
                if (objElement != null)
                {
                    if (objElement.Attributes["userid"] != null)
                        objElement.Attributes["userid"].Value = m_userID.ToString();
                }

				//check existence of Input Xml which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//InputXml");
				if (objElement==null)
				{
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
					return false;
				}
				sInputXml=objElement.InnerXml;

                //TR1740 effective date trigger fails in case of new records
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//EventDate");
                if (objElement != null)
                    sEventDate = objElement.InnerText;
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//ClaimDate");
                if (objElement != null)
                    sClaimDate = objElement.InnerText;
                //Start by Shivendu for MITS 17249
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//PolicyDate");
                if (objElement != null)
                    sPolicyDate = objElement.InnerText;
                //End by Shivendu for MITS 17249

                //objOrgHierarchy = new OrgHierarchy(connectionString);
                objOrgHierarchy = new OrgHierarchy(connectionString, base.ClientId);
                //pmittal5 Confidential Record
                objOrgHierarchy.IsBesEnabled = userLogin.objRiskmasterDatabase.OrgSecFlag;
                objOrgHierarchy.UserID = userLogin.UserId;

                XmlNode objXmlNode = null;
                //get org hierarchy xml......sPolicyDate added by Shivendu for MITS 17249
                sReturnXml = objOrgHierarchy.GetOrgHierarchyXml(bExpand, lLevel, lNode, sNodCat, sParent, sTableName, lRowId, sInputXml, out iEntityEnh, sEventDate, sClaimDate, sPolicyDate);
                //create the output xml by including the Output parameter also
                sReturnXml = "<ReturnXml><OutputResult>" + sReturnXml + "</OutputResult><EntityEnh>" + iEntityEnh.ToString() + "</EntityEnh></ReturnXml>";
                p_objXmlOut.LoadXml(sReturnXml);

                objXmlNode = p_objXmlOut.ImportNode(objOrgHierarchy.LoadOrgList(1005, 1012).DocumentElement, true);
                p_objXmlOut.SelectSingleNode(@"//ReturnXml").AppendChild(objXmlNode);

                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("OrgHierarchy.GetOrgHierarchyXml.Error",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
                return false;
            }
            finally
            {
                objOrgHierarchy = null;
                objElement = null;
            }
        }

        /// <summary>
        /// Raman Bhatia: Gets the children for a particular node.
        /// </summary>
        /// <param name="p_objDocIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool GetChildNodes(XmlDocument p_objDocIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            OrgHierarchy objOrgHierarchy = null;
            XmlNode objXmlNode = null;
            
            try
            {
                //objOrgHierarchy = new OrgHierarchy(connectionString);
                objOrgHierarchy = new OrgHierarchy(connectionString, base.ClientId);
                //pmittal5 Confidential Record
                objOrgHierarchy.IsBesEnabled = userLogin.objRiskmasterDatabase.OrgSecFlag;
                objOrgHierarchy.UserID = userLogin.UserId; 

                p_objXmlOut = objOrgHierarchy.GetChildNodes(p_objDocIn);

                objXmlNode = p_objXmlOut.ImportNode(objOrgHierarchy.LoadOrgList(1005, 1012).DocumentElement, true);
                p_objXmlOut.SelectSingleNode(@"//ReturnXml").AppendChild(objXmlNode);

                return true;
			}

            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("OrgHierarchy.GetChildNodes.Error",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
                return false;
            }
            finally
            {
                objOrgHierarchy = null;
            }
        }

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.OrgHierarchy.OrgHierarchy.GetSpecificOrgHierarchy() method.
		///		Gets the specific Organization Hierarchy information.	
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<Document>
		///			<OrgHierarchy>
		///				<NodCat>Node Category</NodCat>
		///				<InputXml>XML to be populated with the Org Hierarchy data</InputXml>
		///				<Level>Level of the entities</Level>
		///				<Node>Current node Id</Node>
		///				<TreeNode>Tree node</TreeNode>
		///				<TableName>Table Name</TableName>
		///				<RowId>Row Id</RowId>
		///			</OrgHierarchy>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be
		///		<form name="" title="" cid="" topbuttons="" actionname="" bcolor="">
		///			<body1 req_func="" func=""/>
		///				<group name="" title="">
		///					<control name="" num_level="" type="" cmblevel="">
		///						<org name="" ischild="" child_name="" level="" parent="">
		///							<client name="" parent="" ischild="" child_name="" code="" id="" level="">
		///								<company name="" parent="" id="" child_name="" ischild="" code="" level="">
		///									<operation name="" parent="" id="" child_name="" ischild="" code="" level="">
		///										<region name="" parent="" id="" child_name="" ischild="" code="" level="">
		///											<division name="" parent="" id="" child_name="" ischild="" code="" level="">
		///												<location name="" parent="" id="" child_name="" ischild="" code="" isSelect="" level=""> 
		///													<facility name="" parent="" id="" child_name="" ischild="" code="" level="">
		///														<department name="" id="" isSelect="" ischild="" level="" />
		///													</facility>
		///												</location>
		///											</division>
		///										</region>
		///									</operation>
		///								</company>
		///							</client>
		///						<org>
		///					</control>
		///				</group>
		///				<levellist name="">
		///					<option value=""></option>
		///				</levellist>
		///			</form>
		/// </param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool GetSpecificOrgHierarchy(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.OrgHierarchy.OrgHierarchy objOrgHierarchy=null; //Application layer component
			string sNodCat=string.Empty;		//Node category
			string sInputXml=string.Empty;		//XML to be populated with the Org Hierarchy data
			string sReturnXml = string.Empty;
			long lLevel=0;						//Level of the entities
			long lNode=0;						//Current node Id
			string sTreeNode=string.Empty;		//Tree node
			string sTableName=string.Empty;		//Table name
			long lRowId=0;						//Row Id
			XmlElement objElement=null;			//used for parsing the input xml

			try
			{
				//check existence of Node Category which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//NodCat");
				if (objElement==null)
				{
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
					return false;
				}
				sNodCat=objElement.InnerText;

				//check existence of Input Xml which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//InputXml");
				if (objElement==null)
				{
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
					return false;
				}
				sInputXml=objElement.InnerXml;

				//check existence of Level of the entities which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//Level");
				if (objElement==null)
				{
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
					return false;
				}
				lLevel=Conversion.ConvertStrToLong(objElement.InnerText);

				//check existence of Node Id which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//Node");
				if (objElement==null)
				{
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
					return false;
				}
				lNode=Conversion.ConvertStrToLong(objElement.InnerText);

				//check existence of Tree node which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//TreeNode");
				if (objElement==null)
				{
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
					return false;
				}
				sTreeNode=objElement.InnerText;

				//check existence of Table Name which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//TableName");
				if (objElement==null)
				{
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
					return false;
				}
				sTableName=objElement.InnerText;

				//check existence of Row Id which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//RowId");
				if (objElement==null)
				{
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
					return false;
				}
				lRowId=Conversion.ConvertStrToLong(objElement.InnerText);

				//objOrgHierarchy = new Riskmaster.Application.OrgHierarchy.OrgHierarchy(connectionString);
                objOrgHierarchy = new OrgHierarchy(connectionString, base.ClientId);
				sReturnXml = objOrgHierarchy.GetSpecificOrgHierarchy(sNodCat,sInputXml,lLevel,lNode,sTreeNode,sTableName,lRowId);
				// add the company information
				p_objXmlOut.LoadXml("<ReturnXml><OutputResult>"+sReturnXml+"</OutputResult></ReturnXml>");

				return true;

			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("OrgHierarchy.GetSpecificOrgHierarchy.Error",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
				return false;
			}
			finally
			{
				objOrgHierarchy = null;
				objElement=null;
			}
		}


		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.OrgHierarchy.OrgHierarchy.GetOrgHierarchybyLevel() method.
		///		Fetches the Organization Hierarchy information by level.
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<Document>
		///			<OrgHierarchy>
		///				<Search>Search String</Search>
		///				<NodCat>Node category</NodCat>
		///				<InputXml>XML to be populated with the Org Hierarchy data</InputXml>
		///				<Level>Level of the entities</Level>
		///				<Node>Current node Id</Node>
		///				<TreeNode>Tree node</TreeNode>
		///				<TableName>Table Name</TableName>
		///				<RowId>Row Id</RowId>
		///			</OrgHierarchy>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be
		///		<form name="" title="" cid="" topbuttons="" actionname="" bcolor="">
		///			<body1 req_func="" func=""/>
		///				<group name="" title="">
		///					<control name="" num_level="" type="">
		///						<org name="" ischild="" child_name="" level="" parent="">
		///							<client name="" parent="" ischild="" child_name="" code="" id="" level="">
		///								<company name="" parent="" id="" child_name="" ischild="" code="" level="">
		///									<operation name="" parent="" id="" child_name="" ischild="" code="" level="">
		///										<region name="" parent="" id="" child_name="" ischild="" code="" level="">
		///											<division name="" parent="" id="" child_name="" ischild="" code="" level="">
		///												<location name="" parent="" id="" child_name="" ischild="" code="" isSelect="" level=""> 
		///													<facility name="" parent="" id="" child_name="" ischild="" code="" level="">
		///														<department name="" id="" isSelect="" ischild="" level="" />
		///													</facility>
		///												</location>
		///											</division>
		///										</region>
		///									</operation>
		///								</company>
		///							</client>
		///						<org>
		///					</control>
		///				</group>
		///				<levellist name="">
		///					<option value=""></option>
		///				</levellist>
		///			</form>
		/// </param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool GetOrgHierarchybyLevel(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.OrgHierarchy.OrgHierarchy objOrgHierarchy=null; //Application layer component
			string sSearch=string.Empty;		//Search String
            string sCity = string.Empty;		//City String MITS 16771- Pankaj 5/29/09
            long lState = 0;		            //State Id
            string sZip = string.Empty;		    //Zip Code
            long lEntityId = 0;                 //MITS 17157 Pankaj 7/15/09
            string sNodCat = string.Empty;		//Node category
			string sInputXml=string.Empty;		//XML to be populated with the Org Hierarchy data
			long lLevel=0;						//Level of the entities
			long lNode=0;						//Current node Id
			string sTreeNode=string.Empty;		//Tree node
			string sTableName=string.Empty;		//Table name
			long lRowId=0;						//Row Id
			XmlElement objElement=null;			//used for parsing the input xml
			int iEntityEnh=0;
			string sReturnXml=string.Empty; //used for holding the output xml string

			try
			{
				//check existence of Search String which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//Search");
                if (objElement == null)
                {
                    //p_objErrOut.Add("OrgHierarchyParameterMissing",Globalization.GetString("OrgHierarchyParameterMissing"),BusinessAdaptorErrorType.Error);
                    //return false;

                }
                else
                {
                    sSearch = objElement.InnerText;
                }

				//check existence of Node Category which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//NodCat");
				if (objElement==null)
				{
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
					return false;
				}
				sNodCat=objElement.InnerText;

				//check existence of Input Xml which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//InputXml");
				if (objElement==null)
				{
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
					return false;
				}
				sInputXml=objElement.InnerXml;

				//check existence of Level of the entities which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//Level");
				if (objElement==null)
				{
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
					return false;
				}
				lLevel=Conversion.ConvertStrToLong(objElement.InnerText);

				//check existence of Node Id which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//Node");
				if (objElement==null)
				{
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
					return false;
				}
				lNode=Conversion.ConvertStrToLong(objElement.InnerText);

				//check existence of Tree node which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//TreeNode");
				if (objElement==null)
				{
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
					return false;
				}
				sTreeNode=objElement.InnerText;

				//check existence of Table Name which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//TableName");
				if (objElement==null)
				{
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
					return false;
				}
				sTableName=objElement.InnerText;

				//check existence of Row Id which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//RowId");
				if (objElement==null)
				{
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
					return false;
				}
				lRowId=Conversion.ConvertStrToLong(objElement.InnerText);
                //MITS 16771- Pankaj 5/29/09
                //City
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//City");
                if (objElement != null)
                    sCity = objElement.InnerText;

                //State
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//State");
                if (objElement != null)
                    lState = Conversion.ConvertStrToLong(objElement.InnerText);

                //Zip
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//Zip");
                if (objElement != null)
                    sZip = objElement.InnerText;

                //Entity Id //MITS 17157 Pankaj 7/15/09
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//EntityId");
                if (objElement != null)
                    lEntityId = Conversion.ConvertStrToLong(objElement.InnerText);

				//*******************Mohit Yadav for Bug No. 000080*************************
				//objOrgHierarchy = new Riskmaster.Application.OrgHierarchy.OrgHierarchy(connectionString);
                objOrgHierarchy = new OrgHierarchy(connectionString, base.ClientId);
                //pmittal5 Confidential Record
                objOrgHierarchy.IsBesEnabled = userLogin.objRiskmasterDatabase.OrgSecFlag;
                objOrgHierarchy.UserID = userLogin.UserId; 

				XmlNode objXmlNode = null;
				//get organization hierarchy by level
                sReturnXml = objOrgHierarchy.GetOrgHierarchybyLevel(sSearch, sNodCat, sInputXml, lLevel, lNode, sTreeNode, sTableName, lRowId, sCity, lState, sZip, lEntityId );
                //MITS 16771- Pankaj 5/29/09, city state zip //MITS 17157 Pankaj 7/15/09 entity id

				//create the output xml by including the Output parameter also
				sReturnXml="<ReturnXml><OutputResult>" + sReturnXml + "</OutputResult><EntityEnh>" + iEntityEnh.ToString() + "</EntityEnh></ReturnXml>";
				p_objXmlOut.LoadXml(sReturnXml);

				objXmlNode = p_objXmlOut.ImportNode(objOrgHierarchy.LoadOrgList(1005 , 1012).DocumentElement , true);
				p_objXmlOut.SelectSingleNode(@"//ReturnXml").AppendChild(objXmlNode);
				//*******************Mohit Yadav for Bug No. 000080*************************
				return true;

			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("OrgHierarchy.GetOrgHierarchybyLevel.Error",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
				return false;
			}
			finally
			{
				objOrgHierarchy = null;
				objElement=null;
			}
		}


		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.OrgHierarchy.OrgHierarchy.GetOrganizationsWithAddr() method.
		///		Fetches the Organization Hierarchy with address.
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<Document>
		///			<OrgHierarchy>
		///				<Name>Name of the node</Name>
		///				<NodCat>Node Category</NodCat>
		///				<Level>Level of the entities</Level>
		///				<Node>Current node Id</Node>
		///				<TreeNode>Tree node</TreeNode>
		///				<SearchAddr>Indicates whether address check box is clicked or not</SearchAddr>
		///			</OrgHierarchy>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be
		///		<form>
		///			<body1 req_func="" func="" /> 
		///			<group name="">
		///				<control type="" name="">
		///					<search>
		///						<result cName="" pName="" eid="" abr="" city="" addr="" state="" zip="" level="" searchin="" /> 
		///					 </search>
		///				</control>
		///			</group>
		///		</form>
		/// </param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool GetOrganizationsWithAddr(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.OrgHierarchy.OrgHierarchy objOrgHierarchy=null; //Application layer component
			string sName=string.Empty;			//Name of the Node
			string sNodCat=string.Empty;		//Node category
			long lLevel=0;						//Level of the entities
			long lNode=0;						//Current node Id
			string sTreeNode=string.Empty;		//Tree node
			int iSearchAddr=0;					//Indicates whether address check box is clicked or not
			int iScreen=0;						//Indicates screen type:popup or maintenance.
			XmlElement objElement=null;			//used for parsing the input xml
			string sReturnXml = string.Empty;

			try
			{
				//check existence of Node Name which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//Name");
				if (objElement==null)
				{
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
					return false;
				}
				sName=objElement.InnerText;

				//check existence of Node Category which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//NodCat");
				if (objElement==null)
				{
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
					return false;
				}
				sNodCat=objElement.InnerText;

				//check existence of Level of the entities which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//Level");
				if (objElement==null)
				{
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
					return false;
				}
				lLevel=Conversion.ConvertStrToLong(objElement.InnerText);

				//check existence of Node Id which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//Node");
				if (objElement==null)
				{
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
					return false;
				}
				lNode=Conversion.ConvertStrToLong(objElement.InnerText);

				//check existence of Tree node which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//TreeNode");
				if (objElement==null)
				{
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
					return false;
				}
				sTreeNode=objElement.InnerText;

				//check existence of Table Name which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//SearchAddr");
				if (objElement==null)
				{
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
					return false;
				}
				iSearchAddr=Conversion.ConvertStrToInteger(objElement.InnerText);
				
				//Check Screen : Whether popup or maintenance
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//Screen");
				if (objElement==null)
				{
					iScreen=0; // Set Default to Popup.
					//p_objErrOut.Add("OrgHierarchyParameterMissing",Globalization.GetString("OrgHierarchyParameterMissing"),BusinessAdaptorErrorType.Error);
					//return false;
				}
				iScreen = Conversion.ConvertStrToInteger(objElement.InnerText);
				//objOrgHierarchy = new Riskmaster.Application.OrgHierarchy.OrgHierarchy(connectionString);
                objOrgHierarchy = new OrgHierarchy(connectionString, base.ClientId);
				sReturnXml =  objOrgHierarchy.GetOrganizationsWithAddr(sName,sNodCat,lLevel,lNode,sTreeNode,iSearchAddr,iScreen);
				// add the company information
				p_objXmlOut.LoadXml("<ReturnXml><OutputResult>"+sReturnXml+"</OutputResult></ReturnXml>");

				return true;

			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("OrgHierarchy.GetOrganizationsWithAddr.Error",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
				return false;
			}
			finally
			{
				objOrgHierarchy = null;
				objElement=null;
			}
		}


		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.OrgHierarchy.OrgHierarchy.GetEnitySearch() method.
		///		Fetches the results of entity search.
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<Document>
		///			<OrgHierarchy>
		///				<EntityId>Entity Id</EntityId>
		///				<Lookup>Lookup String</Lookup>
		///			</OrgHierarchy>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be
		///		<NewDataSet>
		///			<Table>
		///				<ENTITY_ID />
		///				<ABBREVIATION />
		///				<LAST_NAME />
		///			</Table>
		///		</NewDataSet>
		/// </param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool GetEnitySearch(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.OrgHierarchy.OrgHierarchy objOrgHierarchy=null; //Application layer component
			long lEntityId=0;				//Entity Id
			string sLookUp=string.Empty;	//Look up String
			XmlElement objElement=null;		//used for parsing the input xml
            string sCity = string.Empty;	//City String//MITS 16771- Pankaj 5/29/09
            long lState = 0;		        //State Id
            string sZip = string.Empty;		//Zip Code
            string sReturnXml = string.Empty; //used for holding the output xml string

			try
			{
				//check existence of Entity Id which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//EntityId");
				if (objElement==null)
				{
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
					return false;
				}
				lEntityId=Conversion.ConvertStrToLong(objElement.InnerText);

				//check existence of Lookup string which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//Lookup");
				if (objElement==null)
				{
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
					return false;
				}
				sLookUp=objElement.InnerText;
                //MITS 16771- Pankaj 5/29/09
                //City
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//City");
                if (objElement != null)
                    sCity = objElement.InnerText;

                //State
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//State");
                if (objElement != null)
                    lState = Conversion.ConvertStrToLong(objElement.InnerText);

                //Zip
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//Zip");
                if (objElement != null)
                    sZip = objElement.InnerText;

                //MITS 11662 : Raman Bhatia: List View only searching wherever first few characters are given in the search field whereas
                //it should search for the characters anywhere in the name.
                //sgoel6 04/08/2009 MITS 14875 | Not required in Org search
                //if (sLookUp.EndsWith("%"))
                //{
                //    sLookUp = "%" + sLookUp;
                //}
				//objOrgHierarchy = new Riskmaster.Application.OrgHierarchy.OrgHierarchy(connectionString);
                objOrgHierarchy = new OrgHierarchy(connectionString, base.ClientId);

				// search the entity
				//**************************Mohit Yadav for Bug No. 000080*****************
				//p_objXmlOut.LoadXml(objOrgHierarchy.GetEnitySearch(lEntityId,sLookUp));
				//*************************************************************************
				XmlNode objXmlNode = null;
				XmlDocument objTempXmlDoc = new XmlDocument();
				//get org hierarchy xml	

                //pmittal5 Confidential Record
                objOrgHierarchy.IsBesEnabled = userLogin.objRiskmasterDatabase.OrgSecFlag;
                objOrgHierarchy.UserID = userLogin.UserId; 

                sReturnXml = objOrgHierarchy.GetEnitySearch(lEntityId, sLookUp, sCity, lState, sZip); //MITS 16771- Pankaj 5/29/09
                objTempXmlDoc.LoadXml(sReturnXml);

				objElement = objTempXmlDoc.CreateElement("TABLE_ID");
				objElement.InnerText = lEntityId.ToString();

				objXmlNode = objTempXmlDoc.SelectSingleNode("//Table");
				if (objXmlNode != null)
					objXmlNode.PrependChild(objElement);

				p_objXmlOut.LoadXml(objTempXmlDoc.InnerXml.ToString());
				//**************************Mohit Yadav for Bug No. 000080*****************
				return true;

			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("OrgHierarchy.GetEnitySearch.Error",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
				return false;
			}
			finally
			{
				objOrgHierarchy = null;
				objElement=null;
			}
		}

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.OrgHierarchy.CreateClone() method.
		///		Creates the Clones of a particular hierarchy level.
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<Document>
		///			<OrgHierarchy>
		///				<CompanyInfo>Company Information</CompanyInfo>
		///				<CloningLevelPreference></CloningLevelPreference>
		///				<DesiredCloningLevel></DesiredCloningLevel>
		///				<SelectedLevelId></SelectedLevelId>	
		///			</OrgHierarchy>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be
		///		<ReturnXml>
		///		</ReturnXml>
		/// </param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool CreateClone(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.OrgHierarchy.OrgHierarchy objOrgHierarchy=null; //Application layer component
			XmlElement objElement = null;
			XmlDocument objCompanyInfoXML = null;
			bool bCloneMultipleLevels = false;
			long lDesiredCloningLevel = 0;
			long lSelectedLevelId = 0;
			string sReturnXml=string.Empty; //used for holding the output xml string

			try
			{
                //rjhamb Mits 21006:Able to clone Org hierarchy levels w/o Create permission
                if (!userLogin.IsAllowedEx(RMB_ORG, RMPermissions.RMO_CREATE))
                    throw new PermissionViolationException(RMPermissions.RMO_CREATE, RMB_ORG);
                //rjhamb Mits 21006:Able to clone Org hierarchy levels w/o Create permission

                //check existence of Expand which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//CloningLevelPreference");
				if (objElement==null)
				{
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
					return false;
				}
				bCloneMultipleLevels = Conversion.ConvertStrToBool(objElement.InnerText);

				//check existence of Level of the entities which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//DesiredCloningLevel");
				if (objElement==null)
				{
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
					return false;
				}
				lDesiredCloningLevel = Conversion.ConvertStrToLong(objElement.InnerText);

				//check existence of Level of the entities which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//SelectedLevelId");
				if (objElement==null)
				{
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
					return false;
				}
				lSelectedLevelId = Conversion.ConvertStrToLong(objElement.InnerText);
				
				//check existence of company information which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//CompanyInfo");
				if (objElement==null)
				{
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
					return false;
				}
				objCompanyInfoXML = new XmlDocument();
				objCompanyInfoXML.InnerXml = objElement.OuterXml;
								
				//objOrgHierarchy = new Riskmaster.Application.OrgHierarchy.OrgHierarchy(connectionString);
                objOrgHierarchy = new OrgHierarchy(connectionString, base.ClientId);
				
				p_objXmlOut = objOrgHierarchy.CreateClone(bCloneMultipleLevels , lDesiredCloningLevel , lSelectedLevelId , userLogin.LoginName ,userLogin.Password , userLogin.objRiskmasterDatabase.DataSourceName , connectionString , objCompanyInfoXML);
				
				return true;

			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("OrgHierarchy.CreateClone.Error",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
				return false;
			}
			finally
			{
				objOrgHierarchy = null;
				objElement=null;
			}
		}

		//Changed by Gagan for MITS 8599 : Start

		/// <summary>
		/// Creates the Org Hierarchy Print Report
		/// </summary>
		/// <param name="p_objXmlIn"></param>
		/// <param name="p_objXmlOut"></param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool PrintOrg(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			MemoryStream objMemory=null;
			OrgHierarchy objOrgHierarchy = null;
			XmlElement objTempElement = null;

			try
			{
				//objOrgHierarchy = new OrgHierarchy(connectionString);
                objOrgHierarchy = new OrgHierarchy(connectionString, base.ClientId);
				objOrgHierarchy.CreateOrgHierarchyReport(p_objXmlIn, out objMemory);			
				
				objTempElement=p_objXmlOut.CreateElement("PrintOrg");
				p_objXmlOut.AppendChild(objTempElement);
				objTempElement=p_objXmlOut.CreateElement("File");				
				objTempElement.InnerText=Convert.ToBase64String(objMemory.ToArray());
				p_objXmlOut.FirstChild.AppendChild(objTempElement);


				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("OrgHierarchy.CreateReport.Error",base.ClientId), BusinessAdaptorErrorType.Error);		//sharishkumar Rmacloud		
				return false;
			}
			finally
			{
				objOrgHierarchy = null;
				objMemory = null;
			}
		}

		//Changed by Gagan for MITS 8599 : End
        /// <summary>
        /// Get  Self Insured Details
        /// </summary>
        /// <param name="p_objXmlIn">Input parameters as xml</param>
        /// <param name="p_objXmlOut">Result as Output xml
        /// </param>
        /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		
        public bool GetEntityXSelfInsuredData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {

            Riskmaster.Application.OrgHierarchy.OrgHierarchy objOrgHierarchy = null;
            int iEntityId = 0;
            XmlElement objElement = null;
            string sSelfInsuredInfo = "";

            try
            {

                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//EntityId");
                if (objElement == null)
                {
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
                    return false;
                }
                iEntityId = Conversion.ConvertStrToInteger(objElement.InnerText);

                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//SelfInsuredInfo");
                if (objElement == null)
                {
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
                    return false;
                }
                sSelfInsuredInfo = objElement.OuterXml;

                //objOrgHierarchy = new Riskmaster.Application.OrgHierarchy.OrgHierarchy(connectionString);
                objOrgHierarchy = new OrgHierarchy(connectionString, base.ClientId);

                p_objXmlOut = objOrgHierarchy.GetEntityXSelfInsuredData(iEntityId, sSelfInsuredInfo, userLogin.LoginName, userLogin.Password, userLogin.objRiskmasterDatabase.DataSourceName);


            }

            catch (XmlOperationException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (DataModelException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("OrgHierarchy.GetSelfInsuredInformation.Error",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
                return false;
            }
            finally
            {
                objOrgHierarchy = null;
                objElement = null;
            }
            return true;
        }
        /// <summary>
        /// Deletes  Self Insured Details
        /// </summary>
        /// <param name="p_objXmlIn">Input parameters as xml</param>
        /// <param name="p_objXmlOut">Result as Output xml
        /// </param>
        /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
        /// <returns>Success -True or Failure -false in execution of the function</returns>
        public bool DeleteEntityXSelfInsuredData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            Riskmaster.Application.OrgHierarchy.OrgHierarchy objOrgHierarchy = null;
            XmlElement objElement = null;
            int iRowId = 0;
            try
            {
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//SIRowId");
                if (objElement == null)
                {
                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
                    return false;
                }
                iRowId = Conversion.ConvertStrToInteger(objElement.InnerText);

                //objOrgHierarchy = new Riskmaster.Application.OrgHierarchy.OrgHierarchy(connectionString);
                objOrgHierarchy = new OrgHierarchy(connectionString, base.ClientId);
                objOrgHierarchy.DeleteEntityXSelfInsuredData(iRowId, connectionString);
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("OrgHierarchy.DeleteSelfInsuredInformation.Error",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
                return false;
            }
            finally
            {
                objOrgHierarchy = null;
            }
        }
        /// <summary>
        /// Saves  Self Insured Details
        /// </summary>
        /// <param name="p_objXmlIn">Input parameters as xml</param>
        /// <param name="p_objXmlOut">Result as Output xml
        /// </param>
        /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
        /// <returns>Success -True or Failure -false in execution of the function</returns>
        public bool SaveEntityXSelfInsuredData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            Riskmaster.Application.OrgHierarchy.OrgHierarchy objOrgHierarchy = null;
            XmlElement objElement = null;
            string sInputXml = string.Empty;
            try
            {

                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//OrgHierarchy");
                if (objElement == null)
                {

                    p_objErrOut.Add("OrgHierarchyParameterMissing", Globalization.GetString("OrgHierarchyParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
                    return false;


                }
                sInputXml = objElement.InnerXml;

                //objOrgHierarchy = new Riskmaster.Application.OrgHierarchy.OrgHierarchy(connectionString);
                objOrgHierarchy = new OrgHierarchy(connectionString, base.ClientId);


                objOrgHierarchy.SaveEntityXSelfInsuredData(sInputXml, userLogin.LoginName, userLogin.Password, userLogin.objRiskmasterDatabase.DataSourceName);

                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("OrgHierarchy.AddSelfInsuredInformation.Error",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Rmacloud
                return false;
            }
            finally
            {
                objOrgHierarchy = null;
            }
        }

		#endregion
	    #region  Private Methods 
		private string GetExpoInfoSecurity(string p_sCompanyInfo )
		{	XmlDocument objXML = null;
			XmlNodeList objExpoInfoSecurityNode = null;
			XmlElement objXmlElem = null;
			objXML = new XmlDocument ();
			objXML.LoadXml (p_sCompanyInfo);
			objExpoInfoSecurityNode = objXML.GetElementsByTagName("ExpInfoSecurity");
			objXmlElem=(XmlElement)objExpoInfoSecurityNode[0];
			if (!userLogin.IsAllowedEx(RMB_ORG, RMPermissions.RMO_UPDATE) || !userLogin.IsAllowedEx(RMB_ORG_EXPO, RMPermissions.RMO_ACCESS) || !userLogin.IsAllowedEx(RMB_ORG_EXPO, RMPermissions.RMO_VIEW))
				objXmlElem.GetElementsByTagName("View")[0].InnerXml= Conversion.ConvertObjToStr(1);
			else
				objXmlElem.GetElementsByTagName("View")[0].InnerXml= Conversion.ConvertObjToStr(0);

			if (!userLogin.IsAllowedEx(RMB_ORG_EXPO, RMPermissions.RMO_UPDATE ))
				objXmlElem.GetElementsByTagName("Edit")[0].InnerXml= Conversion.ConvertObjToStr(1);
			else
				objXmlElem.GetElementsByTagName("Edit")[0].InnerXml= Conversion.ConvertObjToStr(0);
			return objXML.InnerXml ;
		}
		#endregion
	}
}
