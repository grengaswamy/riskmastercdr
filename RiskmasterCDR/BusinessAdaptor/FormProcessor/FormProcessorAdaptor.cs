﻿
using System;
using System.Xml; 
using Riskmaster.ExceptionTypes ;
using Riskmaster.Common ;
using Riskmaster.BusinessAdaptor.Common; 
using Riskmaster.Application.FormProcessor;
using Riskmaster.Application.DocumentManagement ;
using System.IO;
using Riskmaster.Application.FileStorage ;
using Riskmaster.Application.FormProcessor;
using Riskmaster.Settings;
using System.Xml.Linq;

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: FormProcessorAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 18-May-2005
	///* $Author	: Tanuj Narula
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	
	/// <summary>	
	///	This class is a wrapper over formprocessor application component.
	/// </summary>
	public class FormProcessorAdaptor:BusinessAdaptorBase
	{
		/// <summary>
		/// Default constructor.
		/// </summary>
		public FormProcessorAdaptor()
		{
		}
		/// Name			: SavePdfForm
		/// Author			: Tanuj Narula
		/// Date Created	: 18-May-2006
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///	17-Jan-2008		*	MITS-10753	*    Arnab Mukherjee
        ///					*				*	
		/// ************************************************************
		/// <summary>
		/// This function saves the PdF data.
		/// </summary
		/// <param name="p_objXmlIn">Input data as xml</param>
		/// <param name="p_objXmlOut">Success -no relevance or Failure -error message in execution of the function</param>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool SavePdfForm(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
            FormManager objFormMgr = null ; 
			string sNewFDFFilePathAndName = "";
			string sPaths = "";

			string sFDFFileName = "";
			SysSettings objSettings = null;
            string sSessionRawData = string.Empty;
            XmlDocument objWCForm = new XmlDocument();
            SessionManager objSessionManager = null;
            XmlNode srcNode = null;
            XmlNode desnNode = null;
			try
			{
                //mkaran2-  WC Forms Session - Start
                objSessionManager = base.GetSessionObject();
                sSessionRawData = Utilities.BinaryDeserialize(objSessionManager.GetBinaryItem("XML_WCPDFFORMS")).ToString();              
                objWCForm.LoadXml(sSessionRawData);
                
                if (objWCForm.ChildNodes.Count != 0)
                {
                    srcNode = p_objXmlIn.SelectSingleNode("//ClaimId");
                    desnNode = objWCForm.SelectSingleNode("//ClaimId");
                    if (srcNode != null && desnNode != null)
                        srcNode.InnerText = desnNode.InnerText;

                    srcNode = p_objXmlIn.SelectSingleNode("//FormId");
                    desnNode = objWCForm.SelectSingleNode("//FormId");
                    if (srcNode != null && desnNode != null)
                        srcNode.InnerText = desnNode.InnerText;

                    srcNode = p_objXmlIn.SelectSingleNode("//StorageId");
                    desnNode = objWCForm.SelectSingleNode("//StorageId");
                    if (srcNode != null && desnNode != null)
                        srcNode.InnerText = desnNode.InnerText;

                    srcNode = p_objXmlIn.SelectSingleNode("//State");
                    desnNode = objWCForm.SelectSingleNode("//State");
                    if (srcNode != null && desnNode != null)
                        srcNode.InnerText = desnNode.InnerText;

                    srcNode = p_objXmlIn.SelectSingleNode("//OrgHierarchy");
                    desnNode = objWCForm.SelectSingleNode("//OrgHierarchy");
                    if (srcNode != null && desnNode != null)
                        srcNode.InnerText = desnNode.InnerText;                  
                }
                //mkaran2-  WC Forms Session - End
                objFormMgr = new FormManager(m_userLogin,base.ClientId) ;
                   
				sPaths = p_objXmlIn.SelectSingleNode("//name[text() = 'FDF_FILENAME']").NextSibling.InnerText ;

				if(sPaths.Trim()==null || sPaths.Trim()=="")
				{
					throw new RMAppException(Globalization.GetString("FormProcessorAdaptor.SavePdfForm.NoFDFNameError",base.ClientId)) ;
				}
				sFDFFileName =sPaths.Substring(0 ,sPaths.IndexOf("||")) ;

				if(sFDFFileName.Trim()==null || sFDFFileName.Trim()=="")
				{
					throw new RMAppException(Globalization.GetString("FormProcessorAdaptor.SavePdfForm.NoFDFNameError",base.ClientId)) ;
				}

				//Raman Bhatia.. 08/08/2006
				//If Acrosoft is enabled then we do not want to use the legacy document management solution
				//Document would be posted to Acrosoft
	
				objSettings = new SysSettings(connectionString, base.ClientId); //Ash - cloud
				if((m_userLogin.objRiskmasterDatabase.DocPathType == 0) && objSettings.UseAcrosoftInterface == false)
				{      
					sNewFDFFilePathAndName = m_userLogin.objRiskmasterDatabase.GlobalDocPath+"\\"+sFDFFileName ;
				    File.Delete(sNewFDFFilePathAndName) ; 
				}
				else
				{
                    sNewFDFFilePathAndName = RMConfigurator.CombineFilePath(RMConfigurator.UserDataPath, String.Format(@"froi\\{0}", sFDFFileName));
				  
				}

                //Start rsushilaggar Date 03/28/2011
                if (this.userLogin.DocumentPath.Length > 0 && objSettings.UseAcrosoftInterface == false)
                {
                    // use StoragePath set for the user
                    sNewFDFFilePathAndName = this.userLogin.DocumentPath + "\\" + sFDFFileName ; //Nanda to fix issue in saving $ printing pdf.
                }
                //End

				FileStream objFs = null;
				try
				{
					objFs =  File.Create(sNewFDFFilePathAndName) ;
				}
				finally
				{
					if(objFs!=null)
					{
						objFs.Close();
						objFs = null;
					}
				}

				objFormMgr.SavePdfForm (ref p_objXmlIn,  sNewFDFFilePathAndName ) ;

                //MITS-10753 - Checking the utility settings for States and OrgHirarchy
                //If the setting is OFF it will insert a row in the database for modification, which is done
                //automatically in case of setting is ON
                Application.SupportScreens.Helper objHelper = new Application.SupportScreens.Helper(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password,base.ClientId);
                if (!objHelper.GetWCPDFAttachToClaimFlag(p_objXmlIn))
                {
                    string p_sInnerXML = "<CLAIM><ATTACH><AttachTable>CLAIM</AttachTable>";
                    p_sInnerXML += "<AttachRecordId>" + p_objXmlIn.SelectSingleNode("//ClaimId").InnerText + "</AttachRecordId>";
                    // Changed by Amitosh for mits 24149 (03/02/2011)
                    // p_sInnerXML += "<Name>FROI</Name>";
                    p_sInnerXML += "<Name>SROI</Name>";

                    //pmittal5 MITS 13126 08/26/08 - Tag added so that Module Access Security Permissions are not checked in case of WCPdf forms.
                    //p_sInnerXML += "<FileName>" + sNewFDFFilePathAndName + "</FileName></ATTACH></CLAIM>";
                    p_sInnerXML += "<FileName>" + sNewFDFFilePathAndName + "</FileName></ATTACH>";
                    p_sInnerXML += "<WCPdfForm/></CLAIM>";
                    //End - pmittal5

                    //If doc path type is File Server
                    if (m_userLogin.objRiskmasterDatabase.DocPathType == 0)
                        this.AttachDocumentForJurisOptions(p_sInnerXML, sNewFDFFilePathAndName, Conversion.ConvertObjToInt(p_objXmlIn.SelectSingleNode("//StorageId").InnerText, base.ClientId));                        
                    
                    //If doc path type = DB and UseAcrosoftInterface == false
                    if ((m_userLogin.objRiskmasterDatabase.DocPathType == 1) && (objSettings.UseAcrosoftInterface == false))
                    {
                        this.AttachDocumentForJurisOptions(p_sInnerXML, sNewFDFFilePathAndName, Conversion.ConvertObjToInt(p_objXmlIn.SelectSingleNode("//StorageId").InnerText, base.ClientId));
                        File.Delete(sNewFDFFilePathAndName);
                    }

                    // ABhateja, 07.19.2007
                    // MITS 10053, Delete the file "only" when UseAcrosoftInterface is true.
                    else if (objSettings.UseAcrosoftInterface == true)
                    {
                        if (File.Exists(sNewFDFFilePathAndName))
                        {
                            File.Delete(sNewFDFFilePathAndName);
                        } // if

                    } // else
                }
                else
                {
                    //Raman Bhatia.. 08/08/2006
                    //If Acrosoft is enabled then we do not want to use the legacy document management solution
                    //Document would be posted to Acrosoft

                    if ((m_userLogin.objRiskmasterDatabase.DocPathType == 1) && (objSettings.UseAcrosoftInterface == false))
                    {
                        //MITS 17484
                        XDocument xDoc = new XDocument(
                            new XElement("CLAIM",
                                new XElement("ATTACH",
                                    new XElement("AttachTable", "CLAIM"),
                                    new XElement("AttachRecordId", p_objXmlIn.SelectSingleNode("//ClaimId").InnerText),
                                    new XElement("Name", "FROI"),
                                    new XElement("FileName", sNewFDFFilePathAndName)),
                                new XElement("WCPdfForm")));

                        this.AttachDocument(xDoc.ToString(), sNewFDFFilePathAndName, Conversion.ConvertObjToInt(p_objXmlIn.SelectSingleNode("//StorageId").InnerText, base.ClientId));

                        File.Delete(sNewFDFFilePathAndName);
                    }

                    // ABhateja, 07.19.2007
                    // MITS 10053, Delete the file "only" when UseAcrosoftInterface is true.
                    else if (objSettings.UseAcrosoftInterface == true)
                    {
                        if (File.Exists(sNewFDFFilePathAndName))
                        {
                            File.Delete(sNewFDFFilePathAndName);
                        } // if

                    } // else
                }                
                //MITS-10753 End

				p_objXmlOut =p_objXmlIn ;
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("FormProcessorAdaptor.SavePdfForm.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				if(objFormMgr!=null)
				{
					objFormMgr.Dispose();
					objFormMgr = null ;
				}
				objSettings = null;
			}
		}


		/// Name			: AttachDocument
		/// Author			: Tanuj Narula
		/// Date Created	: 18-May-2006
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		
		/// <summary>
		/// This function attaches the FDF file to the Claim
		/// </summary>
		/// <param name="p_sFilePath">Physical location of the file to be attached.</param>
		/// <param name="p_iStorageId">Storage id of the already attached file.</param>
		private void AttachDocument(string s_XML, string p_sFilePath, int p_iStorageId )
		{
			MemoryStream objMemory=null;
			DocumentManager objDocumentManager=null;
			long lDocumentId=0;
			try
			{
				objDocumentManager=new DocumentManager(m_userLogin,base.ClientId);
				objDocumentManager.StorageId = p_iStorageId ;
				objDocumentManager.ConnectionString=connectionString;
				objDocumentManager.UserLoginName =userLogin.LoginName;
				if (userLogin.objRiskmasterDatabase.DocPathType == 0)
				{
					objDocumentManager.DocumentStorageType=StorageType.FileSystemStorage;
				}
				else
				{
					objDocumentManager.DocumentStorageType=StorageType.DatabaseStorage;
				}
				objDocumentManager.DestinationStoragePath=userLogin.objRiskmasterDatabase.GlobalDocPath;

                objMemory = Utilities.ConvertFilestreamToMemorystream(p_sFilePath, base.ClientId);

                objDocumentManager.AddDocument(s_XML, objMemory, 3, out lDocumentId);
			}
			finally
			{
			    objDocumentManager=null;
                if(objMemory!=null)
                {
				    objMemory.Close();
                    objMemory.Dispose();
                }
			}

		}

        /// Name			: AttachDocument
        /// Author			: Arnab Mukherjee
        /// Date Created	: 21-Jan-2008
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
		
        /// <summary> MITS - 10753 - Method overloading to provide an Utility setting
        /// This method is overloaded to accomodate the XML text to be sent to AddDocument() which will take
        /// this XML text to initialize an object
        /// </summary>
        /// <param name="p_strXMLDoc">Created XML document's inner XML</param>
        /// <param name="p_sFilePath">File path and Name</param>
        /// <param name="p_iStorageId">Storage ID to be stored in the database</param>
        private void AttachDocumentForJurisOptions(string p_strXMLDoc, string p_sFilePath, int p_iStorageId)
        {
            MemoryStream objMemory = null;            
            DocumentManager objDocumentManager = null;
            long lDocumentId = 0;
            try
            {

                objDocumentManager = new DocumentManager(m_userLogin,base.ClientId);
                objDocumentManager.StorageId = p_iStorageId;
                objDocumentManager.ConnectionString = connectionString;
                objDocumentManager.UserLoginName = userLogin.LoginName;
                if (userLogin.objRiskmasterDatabase.DocPathType == 0)
                {
                    objDocumentManager.DocumentStorageType = StorageType.FileSystemStorage;
                }
                else
                {
                    objDocumentManager.DocumentStorageType = StorageType.DatabaseStorage;
                }
                objDocumentManager.DestinationStoragePath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                objMemory = Utilities.ConvertFilestreamToMemorystream(p_sFilePath, base.ClientId);
                objDocumentManager.AddDocument(p_strXMLDoc, objMemory, 3, out lDocumentId);
            }
            finally
            {                
                objDocumentManager = null;
                if (objMemory != null)
                {
                    objMemory.Close();
                    objMemory.Dispose();
                }
            }

        }
        //MITS-10753 End

        //Added for FROi Migration Starts Added for Froi Migration- Mits 33585,33586,33587

        public void ProcessForm(string sPDFLink, string p_sScriptFileName, string p_sOutputFileName, object objExt)
        {
            Riskmaster.Application.FormProcessor.FormProcessor objFormProcessor = null;

            try
            {
                objFormProcessor = new Application.FormProcessor.FormProcessor();
                objFormProcessor.ProcessForm(sPDFLink, p_sScriptFileName, p_sOutputFileName, objExt);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FormProcessorAdaptor.ProcessForm.Error",base.ClientId), p_objEx);
            }
            finally
            {
                objFormProcessor = null;
            }
        }

        public void MergeFormHttp(string p_sOutputFileName, string p_sScriptFileName, object pForm)
        {
            Riskmaster.Application.FormProcessor.FormProcessor objFormProcessor = null;

            try
            {
                objFormProcessor = new Application.FormProcessor.FormProcessor();
                objFormProcessor.MergeFormHttp(p_sOutputFileName,  p_sScriptFileName,  pForm,base.ClientId);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FormProcessorAdaptor.MergeFormHttp.Error",base.ClientId), p_objEx);
            }
            finally
            {
                objFormProcessor = null;
            }
        
        }

        public void MergeFormHttpToTempFile(string p_sOutputFileName, string p_sScriptFileName, object pForm)
        {
            Riskmaster.Application.FormProcessor.FormProcessor objFormProcessor = null;

            try
            {
                objFormProcessor = new Application.FormProcessor.FormProcessor();
                objFormProcessor.MergeFormHttpToTempFile(p_sOutputFileName,  p_sScriptFileName,  pForm);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FormProcessorAdaptor.MergeFormHttpToTempFile.Error",base.ClientId), p_objEx);
            }
            finally
            {
                objFormProcessor = null;
            }
        }

        public void ProcessFormHttp(string p_sScriptFileName)
        {
            Riskmaster.Application.FormProcessor.FormProcessor objFormProcessor = null;

            try
            {
                objFormProcessor = new Application.FormProcessor.FormProcessor();
                objFormProcessor.ProcessFormHttp(p_sScriptFileName);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FormProcessorAdaptor.ProcessFormHttp.Error",base.ClientId), p_objEx);
            }
            finally
            {
                objFormProcessor = null;
            }
        }
        //Added for FROi Migration Ends
	}
}
