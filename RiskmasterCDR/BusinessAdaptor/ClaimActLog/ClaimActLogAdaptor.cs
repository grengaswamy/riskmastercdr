﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.Application.ClaimActLog;
using System.Xml;

namespace Riskmaster.BusinessAdaptor
{
    /// <summary>
    /// 
    /// </summary>
    public class ClaimActLogAdaptor : BusinessAdaptorBase
    {
        /// <summary>
        /// Default Constructor
        /// </summary>
        public ClaimActLogAdaptor()
        {
        }
        /// <summary>
        /// Wrapper to fetch the claim activity log from database
        /// </summary>
        /// <param name="p_objXmlIn">xml object containing the claim id and form name</param>
        /// <param name="p_objXmlOut">xml object</param>
        /// <param name="p_objErr">Bussiness adaptor errors object conatianing error information</param>
        /// <returns>true/false based upon operation completion</returns>
        public bool GetClaimActivityLog(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            ClaimActLog objClaimActLog = null;
            try
            {
                objClaimActLog = new ClaimActLog(base.userLogin, connectionString,base.ClientId);//sharishkumar Jira 825
                p_objXmlOut =  objClaimActLog.GetClaimActivityLog(p_objXmlIn);
            }
            catch (RMAppException p_objException)
            {
                p_objErr.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErr.Add(p_objException, Globalization.GetString("ClaimActLogAdaptor.GetClaimActivityLog.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 825
                return false;
            }
            finally
            {
                objClaimActLog = null;
            }
            return (true);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErr"></param>
        /// <returns></returns>
        public bool GetConfig(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            ClaimActLog objClaimActLog = null; 
            try
            {
                objClaimActLog = new ClaimActLog(connectionString, base.ClientId);//sharishkumar Jira 825
                p_objXmlOut = objClaimActLog.GetConfig(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErr.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErr.Add(p_objException, Globalization.GetString("ClaimActLogAdaptor.GetConfig.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 825
                return false;
            }
            finally
            {
                objClaimActLog = null;
            }

            return (true);
        }
        /// <summary>
        /// Wrapper for the claim activity log condig details
        /// </summary>
        /// <param name="p_objXmlIn">xml object containing the log id</param>
        /// <param name="p_objXmlOut">xml object</param>
        /// <param name="p_objErr">Bussiness adaptor errors object conatianing error information</param>
        /// <returns>true/false based upon operation completion</returns>
        public bool GetConfigDetails(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            ClaimActLog objClaimActLog = null;
            try
            {
                objClaimActLog = new ClaimActLog(connectionString,base.ClientId);
                p_objXmlOut = objClaimActLog.GetConfigDetails(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErr.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErr.Add(p_objException, Globalization.GetString("ClaimActLogAdaptor.GetConfig.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 825
                return false;
            }
            finally
            {
                objClaimActLog = null;
            }
        }
        /// <summary>
        /// Wrapper for saving the claim activity log
        /// </summary>
        /// <param name="p_objXmlIn">xml object containing the log id</param>
        /// <param name="p_objXmlOut">xml object</param>
        /// <param name="p_objErr">Bussiness adaptor errors object conatianing error information</param>
        /// <returns>true/false based upon operation completion</returns>
        public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            ClaimActLog objClaimActLog = null;
            try
            {
                objClaimActLog = new ClaimActLog(this.userLogin, connectionString, base.ClientId);
                p_objXmlOut = objClaimActLog.Save(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErr.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErr.Add(p_objException, Globalization.GetString("ClaimActLogAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 825
                return false;
            }
            finally
            {
                objClaimActLog = null;
            }
        }
        /// <summary>
        /// Wrapper for deleting claim activity log config from database
        /// </summary>
        /// <param name="p_objXmlIn">xml object containing the log id</param>
        /// <param name="p_objXmlOut">xml object</param>
        /// <param name="p_objErr">Bussiness adaptor errors object conatianing error information</param>
        /// <returns>true/false based upon the operation completion</returns>
        public bool Delete(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            ClaimActLog objClaimActLog = null;
            try
            {
                objClaimActLog = new ClaimActLog(connectionString, base.ClientId);
                p_objXmlOut = objClaimActLog.Delete(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErr.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErr.Add(p_objException, Globalization.GetString("ClaimActLogAdaptor.Delete.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 825
                return false;
            }
            finally
            {
                objClaimActLog = null;
            }
        }

        #region User Preferences
        
        //JIRA-6405 Start vchouhan6   Methods tp get/set User preferences
        /// </summary>
        /// <param name="p_objXmlIn">XmlDocument</param>
        /// <param name="p_objXmlOut">XmlDocument</param>
        /// <param name="p_objErr">BusinessAdaptorErrors</param>
        /// <returns>bool</returns>
        public bool SetUserPref(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            try
            {
                Riskmaster.Common.CommonFunctions.SetUserPref(p_objXmlIn, m_userLogin.UserId, connectionString, base.ClientId);
            }
            catch (Exception p_objException)
            {
                p_objErr.Add(p_objException, Globalization.GetString("ExecutiveSummary.SaveUserPreference.Err", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            return true;
        }
        /// </summary>
        /// <param name="p_objXmlIn">XmlDocument</param>
        /// <param name="p_objXmlOut">XmlDocument</param>
        /// <param name="p_objErr">BusinessAdaptorErrors</param>
        /// <returns>bool</returns>
        public bool GetUserPrefXML(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            try
            {
                Riskmaster.Common.CommonFunctions.GetUserPrefXML(p_objXmlIn, ref p_objXmlOut, m_userLogin.UserId, connectionString, base.ClientId);
            }
            catch (Exception p_objException)
            {
                p_objErr.Add(p_objException, Globalization.GetString("ExecutiveSummary.GetUserPreference.Err", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            return true;
        }
        //JIRA-6405 End vchouhan6
        #endregion

    }
}
