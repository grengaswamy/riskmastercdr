﻿/***************************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                            
 ***************************************************************************************************
 * 06/04/2014 | 33371           | ajohari2   | Added changes for GetBESStatus method
 * 7/24/2014  | RMA-718         | ajohari2   | Changes for TPA Access ON/OFF
 * 11/08/2014 | 34257/ RMA-429  | ajohari2   | Default Claim Type by Policy LOB (if only single claim type is there for that LOB)
 ***************************************************************************************************/
using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.PolicySystemInterface;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.Linq;
using Riskmaster.Settings;//JIRA RMA-718 ajohari2
using Riskmaster.Application.RMUtilities; //sanoopsharma - for integral
using System.Xml.XPath; //sanoopshamra - for integral
namespace Riskmaster.BusinessAdaptor
{
  public class SetUpPolicySystemAdaptor : BusinessAdaptorBase
    {
        //sanoopsharma start 
        const int iScheduleType = 0;
        const string TaskName = "Policy System Update";
        const string ScheduleTypeText = "Periodically";
        const string TaskType = "9";
        const string Interval = "1";
        //sanoopsharma end
        public bool GetById(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            XmlElement objElement = null;
            SetUpPolicySystem objSetUpPolicySystemAdaptor = null;

            try
            {

                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//PolicySys");
                if (objElement == null)
                {
                    p_objErrOut.Add("SetUpPolicySystemAdaptor.GetById.Error", Globalization.GetString("SetUpPolicySystemAdaptor.InputXMLMissing",base.ClientId), BusinessAdaptorErrorType.Warning);
                    return false;
                }

                objSetUpPolicySystemAdaptor = new SetUpPolicySystem(loginName, connectionString,base.ClientId);
                p_objXmlOut = objSetUpPolicySystemAdaptor.GetById(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("SetUpPolicySystemAdaptor.GetById.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objSetUpPolicySystemAdaptor = null;
            }
        }

        

        public bool GetVersionInfo(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            XmlElement objElement = null;
            SetUpPolicySystem objSetUpPolicySystemAdaptor = null;

            try
            {

                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//PolicySys/Versions");
                if (objElement == null)
                {
                    p_objErrOut.Add("SetUpPolicySystemAdaptor.GetVersionInfo.Error", Globalization.GetString("SetUpPolicySystemAdaptor.InputXMLMissing",base.ClientId), BusinessAdaptorErrorType.Warning);
                    return false;
                }

                objSetUpPolicySystemAdaptor = new SetUpPolicySystem(loginName, connectionString,base.ClientId);
                p_objXmlOut = objSetUpPolicySystemAdaptor.GetVersionDetails(objElement).OwnerDocument;
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("SetUpPolicySystemAdaptor.GetById.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objSetUpPolicySystemAdaptor = null;
            }
        }

        public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            XmlElement objElement = null;
            SetUpPolicySystem objSetUpPolicySystem = null;

            try
            {

                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//PolicySys");
                if (objElement == null)
                {
                    p_objErrOut.Add("SetUpPolicySystemAdaptor.Get.Error", Globalization.GetString("SetUpPolicySystemAdaptor.InputXMLMissing",base.ClientId), BusinessAdaptorErrorType.Warning);
                    return false;
                }

                objSetUpPolicySystem = new SetUpPolicySystem(loginName, connectionString,base.ClientId);
                objSetUpPolicySystem.Save(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("SetUpPolicySystemAdaptor.Save.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objSetUpPolicySystem = null;
            }
        }

        public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            SetUpPolicySystem objSetUpPolicySystem = null;
            bool blnSuccess = false;
            int iCurrentPage = 0;
            try
            {
                objSetUpPolicySystem = new SetUpPolicySystem(loginName, connectionString,base.ClientId);
                iCurrentPage = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("/Document/PolicySystemList/CurrentPage").InnerText, out blnSuccess);
                if (iCurrentPage == 0)
                    iCurrentPage = 1;
                objSetUpPolicySystem.PolicySysCurrentPage = iCurrentPage;
                objSetUpPolicySystem.PolicySysPageSize = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("/Document/PolicySystemList/hdnPageSize").InnerText, out blnSuccess);

                p_objXmlOut = objSetUpPolicySystem.LoadData();
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("SetUpPolicySystemAdaptor.Get.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objSetUpPolicySystem = null;
            }
        }

        public bool UpdatePassword(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            XmlElement objElement = null;
            SetUpPolicySystem objSetUpPolicySystem = null;

            try
            {

                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//PolicySys");
                if (objElement == null)
                {
                    p_objErrOut.Add("SetUpPolicySystemAdaptor.Get.Error", Globalization.GetString("SetUpPolicySystemAdaptor.InputXMLMissing",base.ClientId), BusinessAdaptorErrorType.Warning);
                    return false;
                }

                objSetUpPolicySystem = new SetUpPolicySystem(loginName, connectionString,base.ClientId);
                objSetUpPolicySystem.UpdatePassword(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("SetUpPolicySystemAdaptor.Save.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objSetUpPolicySystem = null;
            }
        }

        public bool Delete(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            SetUpPolicySystem objSetUpPolicySystem = null; //Application layer component			
            try
            {
                objSetUpPolicySystem = new SetUpPolicySystem(loginName, connectionString,base.ClientId);
                p_objXmlOut = objSetUpPolicySystem.Delete(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("SetUpPolicySystemAdaptor.Delete.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objSetUpPolicySystem = null;
            }
        }

      //rupal:start
        public bool RefreshPolicySystems(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            SetUpPolicySystem objSetUpPolicySystem = null; //Application layer component			
            try
            {
                objSetUpPolicySystem = new SetUpPolicySystem(loginName, connectionString,base.ClientId);
                return objSetUpPolicySystem.RefreshPolicySystems();
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("SetUpPolicySystemAdaptor.RefreshPolicySystems.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objSetUpPolicySystem = null;
            }
        }
      //rupal:end

        //MITS:33371 ajohari2:Start
        #region MITS 33371
        private XmlDocument m_objDocument = null;
        /// <summary>
        /// Create Element and set inner text.
        /// </summary>
        /// <param name="p_objParentNode">Parent node</param>
        /// <param name="p_sNodeName">Node Name</param>
        /// <param name="sText">Text</param>		
        private void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, bool sText)
        {
            try
            {
                XmlElement objChildNode = null;
                this.CreateAndSetElement(p_objParentNode, p_sNodeName, sText, ref objChildNode);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PolicySystemInterfaceAdaptor.CreateAndSetElement.Error",base.ClientId), p_objEx);
            }
        }

        /// <summary>
        /// Create Element
        /// </summary>
        /// <param name="p_objParentNode">Parent Node</param>
        /// <param name="p_objChildNode">Child Node Name</param>
        /// <param name="p_objChildNode">Child Node</param>
        private void CreateElement(XmlElement p_objParentNode, string p_sNodeName, ref XmlElement p_objChildNode)
        {
            try
            {
                p_objChildNode = p_objParentNode.OwnerDocument.CreateElement(p_sNodeName);
                p_objParentNode.AppendChild(p_objChildNode);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PolicySystemInterfaceAdaptor.CreateElement.Error", base.ClientId), p_objEx);
            }

        }

        /// <summary>
        /// Create Element and set inner text.
        /// </summary>
        /// <param name="p_objParentNode">Parent node</param>
        /// <param name="p_sNodeName">Node Name</param>
        /// <param name="sText">Text</param>
        /// <param name="p_objChildNode">Child Node</param>
        private void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, bool sText, ref XmlElement p_objChildNode)
        {
            try
            {
                CreateElement(p_objParentNode, p_sNodeName, ref p_objChildNode);
                p_objChildNode.InnerText = Convert.ToString(sText);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PolicySystemInterfaceAdaptor.CreateAndSetElement.Error",base.ClientId), p_objEx);
            }
        }

        /// <summary>
        /// Initialize the document
        /// </summary>
        /// <param name="p_objRootNode">Root Node</param>
        /// <param name="p_sRootNodeName">Root Node Name</param>
        private void StartDocument(ref XmlElement p_objRootNode, string p_sRootNodeName)
        {
            try
            {
                m_objDocument = new XmlDocument();
                p_objRootNode = m_objDocument.CreateElement(p_sRootNodeName);
                m_objDocument.AppendChild(p_objRootNode);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PolicySystemInterfaceAdaptor.StartDocument.ErrorDocInit",base.ClientId), p_objEx);
            }
        }

        /// <summary>
        /// Get BESStatus
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool GetBESStatus(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            bool bIsValid = true; // aaggarwal29 : MITS 37594 ,JIRA 6599 changed the default value to true since it was getting displayed in wcf log file everytime
            XmlElement objRootNode = null;
            SysSettings oSettings = null; //JIRA RMA-718 ajohari2

            try
            {
                //JIRA RMA-718 ajohari2: Start
                bool bBESStatusValue = false;
                bool bEnableTPAAccess = false;
                bool bMultiCovgPerClm = false; //MITS:34257 - JIRA RMA-429 ajohari2
                oSettings = new SysSettings(m_userLogin.objRiskmasterDatabase.ConnectionString,base.ClientId);
                if (m_userLogin.objRiskmasterDatabase.OrgSecFlag && (oSettings.MultiCovgPerClm == -1))
                {
                    bBESStatusValue = true;
                }
                bEnableTPAAccess = oSettings.EnableTPAAccess;
                if (oSettings.MultiCovgPerClm == -1) //MITS:34257 - JIRA RMA-429 ajohari2
                {
                    bMultiCovgPerClm = true; //MITS:34257 - JIRA RMA-429 ajohari2
                }

                objRootNode = null;
                this.StartDocument(ref objRootNode, "BESStatusInfo");
                this.CreateAndSetElement(objRootNode, "BESStatus", bBESStatusValue);
                this.CreateAndSetElement(objRootNode, "EnableTPAAccess", bEnableTPAAccess);
                this.CreateAndSetElement(objRootNode, "MultiCovgPerClm", bMultiCovgPerClm); //MITS:34257 - JIRA RMA-429 ajohari2
                p_objXmlOut = objRootNode.OwnerDocument;
                //JIRA RMA-718 ajohari2: End
            }
            catch (RMAppException p_objEx)
            {
                bIsValid = false; //aaggarwal29: added for MITS 37594,JIRA 6599
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                bIsValid = false; //aaggarwal29: added for MITS 37594,JIRA 6599
                throw new RMAppException(Globalization.GetString("SetUpPolicySystemAdaptor.GetBESStatus.Error",base.ClientId), p_objEx);
            }
            finally
            {
                objRootNode = null;
                oSettings = null; //JIRA RMA-718 ajohari2
            }
            return (bIsValid);


        }
        #endregion
        //MITS:33371 ajohari2:End

        public bool ValidatePolicyDSN(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            SetUpPolicySystem objSetUpPolicySystem = null; //Application layer component	
            string sConnectionString;
            int p_iPolicySystemId = 0;
            string[] sDSNDetails = new string[7];
            XmlElement objElement = null;
            bool bIsValid = false;
            try
            {
                objSetUpPolicySystem = new SetUpPolicySystem(loginName, connectionString,base.ClientId);
                //sDSNDetails[0] = (XElement)p_objXmlIn.SelectSingleNode("//inputservername");// ("./Document/form/keys/loginscreenkeys/inputservername");
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//inputdrivername");
                if (objElement != null)
                {
                    sDSNDetails[0] = objElement.InnerText;
                }
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//inputservername");
                if (objElement != null)
                {
                    sDSNDetails[1] = objElement.InnerText;
                }
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//inputdatabasename");
                if (objElement != null)
                {
                    sDSNDetails[2] = objElement.InnerText;
                }
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//inputlibrarylist");
                if (objElement != null)
                {
                    sDSNDetails[3] = objElement.InnerText;
                }
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//inputloginuser");
                if (objElement != null)
                {
                    sDSNDetails[4] = objElement.InnerText;
                }

                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//inputpassword");
                if (objElement != null)
                {
                    sDSNDetails[5] = objElement.InnerText;
                }
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//dsntype");
                if (objElement != null)
                    {
                    sDSNDetails[6] = objElement.InnerText;
                    }
                //p_objXMLForm.SelectSingleNode("//control[@name='hdInvokedFrom']").InnerText
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//control[@name='hdnPolicySystemId']");
                if (objElement != null)
                {
                    p_iPolicySystemId = Conversion.ConvertStrToInteger(objElement.Attributes["value"].Value);
                }
                bIsValid = objSetUpPolicySystem.IsValidPolicyDSN(sDSNDetails, p_iPolicySystemId,out sConnectionString);
                ((XmlElement)p_objXmlIn.SelectSingleNode("//connkey")).InnerText = sConnectionString;
                if (objElement != null)
                {
                    objElement.InnerText = sConnectionString;
                }
                ((XmlElement)p_objXmlIn.SelectSingleNode("//dsnmessage")).Attributes["message"].Value = "Database Connection Wizard has successfully connected to the specified database.The Database appears to be a valid Policy system database.";
                ((XmlElement)p_objXmlIn.SelectSingleNode("//dsnmessage")).Attributes["mode"].Value = "connectionmessage";

            }
            catch (Exception p_objException)
            {
                ((XmlElement)p_objXmlIn.SelectSingleNode("//dsnmessage")).Attributes["message"].Value = p_objException.Message;
                p_objXmlOut = p_objXmlIn;
               // p_objErrOut.Add(p_objException, Globalization.GetString("SetUpPolicySystemAdaptor.GetPolicyDSNDetails.Error"), BusinessAdaptorErrorType.Error);
            }
            finally
            {
                objSetUpPolicySystem = null;
                ((XmlElement)p_objXmlIn.SelectSingleNode("//dsnmessage")).Attributes["iserror"].Value = bIsValid ? "No" : "Yes";
                p_objXmlOut = p_objXmlIn;
            }
            return bIsValid;
        }
      //sanoopsharma - start 

        public bool ScheduleTask(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {

            string sXml = string.Empty;
            XElement objTempElement = null;            
            string PolicySystemID = string.Empty;
            TaskManager objTaskManagement = new TaskManager(userLogin,ClientId);
            string sDSN = m_userLogin.objRiskmasterDatabase.DataSourceName;
		
            try
            { 
                XElement objXElement = XElement.Load(new XmlNodeReader(p_objXmlIn));

                objTempElement = objXElement.XPathSelectElement("./PolicySystems");
                if (objTempElement != null)
                    PolicySystemID = objXElement.XPathSelectElement("./PolicySystems").Value;

                if (!objTaskManagement.CheckForDuplicateSchedule(TaskName, PolicySystemID,"1",Interval,sDSN)) 
                    return false;


                objTempElement = objXElement.XPathSelectElement("./TaskTypeText");
                if (objTempElement != null)
                    objTempElement.Value = TaskName;
                else
                    objXElement.Add(new XElement("TaskTypeText", TaskName));


                objTempElement = objXElement.XPathSelectElement("./TaskNameLabel");
                if (objTempElement != null)
                    objTempElement.Value = TaskName;
                else
                    objXElement.Add(new XElement("TaskNameLabel", TaskName));

                objTempElement = objXElement.XPathSelectElement("./TaskName");
                if (objTempElement != null)
                    objTempElement.Value = TaskName;
                else
                    objXElement.Add(new XElement("TaskName", TaskName));


                objTempElement = objXElement.XPathSelectElement("./ScheduleTypeText");
                if (objTempElement != null)
                    objTempElement.Value = ScheduleTypeText;
                else
                    objXElement.Add(new XElement("ScheduleTypeText", ScheduleTypeText));


                objTempElement = objXElement.XPathSelectElement("./Date");
                if (objTempElement != null)
                    objTempElement.Value = DateTime.Now.ToShortDateString();
                else
                    objXElement.Add(new XElement("Date",DateTime.Now.ToShortDateString()));

                objTempElement = objXElement.XPathSelectElement("./Time");
                if (objTempElement != null)
                    objTempElement.Value = DateTime.Now.Add(TimeSpan.FromMinutes(5.0)).ToString("HH:mm");
                else
                    objXElement.Add(new XElement("Time", DateTime.Now.Add(TimeSpan.FromMinutes(5.0)).ToString("HH:mm")));


                objTempElement = objXElement.XPathSelectElement("./TaskType");
                if (objTempElement != null)
                    objTempElement.Value = TaskType;
                else
                    objXElement.Add(new XElement("TaskType", TaskType));

                objTempElement = objXElement.XPathSelectElement("./IsDataIntegratorTask");
                if (objTempElement != null)
                    objTempElement.Value = "-1";
                else
                    objXElement.Add(new XElement("IsDataIntegratorTask", "-1"));

                objTempElement = objXElement.XPathSelectElement("./SystemModuleName");
                if (objTempElement != null)
                    objTempElement.Value = "-1";
                else
                    objXElement.Add(new XElement("SystemModuleName", "PolicySystemUpdate"));


                objTempElement = objXElement.XPathSelectElement("./IntervalType");
                if (objTempElement != null)
                    objTempElement.Value = "1";
                else
                objXElement.Add(new XElement("IntervalType", "1"));

                objTempElement = objXElement.XPathSelectElement("./IntervalTypeId");
                if (objTempElement != null)
                    objTempElement.Value = "1";
                else
                objXElement.Add(new XElement("IntervalTypeId", "1"));

                objTempElement = objXElement.XPathSelectElement("./Interval");
                if (objTempElement != null)
                    objTempElement.Value = Interval;
                else
                objXElement.Add(new XElement("Interval", Interval));

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                XmlWriterSettings xws = new XmlWriterSettings();
                xws.OmitXmlDeclaration = true;
                xws.Indent = true;
                using (XmlWriter xw = XmlWriter.Create(sb, xws))
                     objXElement.WriteTo(xw);
                Console.WriteLine(sb.ToString());
                
                p_objXmlIn.LoadXml(sb.ToString());
                objTaskManagement.SavePeriodicalSettings(p_objXmlIn);
                return true;
            
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                //objSetUpPolicySystem = null;
                objTaskManagement = null; 
            }
        }

        public bool DeleteTask(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {

            string sXml = string.Empty;
            XElement objTempElement = null;
            string PolicySystemID = string.Empty;
            TaskManager objTaskManagement = new TaskManager(userLogin,ClientId);
            string sDSN = m_userLogin.objRiskmasterDatabase.DataSourceName;

           	
            try
            {
                XElement objXElement = XElement.Load(new XmlNodeReader(p_objXmlIn));
                objTempElement = objXElement.XPathSelectElement("./PolicySystems");
                if (objTempElement != null)
                    PolicySystemID = objXElement.XPathSelectElement("./PolicySystems").Value;

                if (!objTaskManagement.CheckForDuplicateSchedule(TaskName, PolicySystemID, "1", Interval, sDSN))
                {
                    objTaskManagement.DeleteSchedule(TaskName, PolicySystemID, "1", Interval, sDSN);
                    return true;
                }
                else
                    return false;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("SetUpPolicySystemAdaptor.Delete.Error",ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objTaskManagement = null;
            }
        }
      //sanoopsharma - end 
    }
}
