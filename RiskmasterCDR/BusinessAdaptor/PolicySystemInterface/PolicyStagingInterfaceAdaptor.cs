﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.PolicySystemInterface;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;
using Riskmaster.Models;
using Riskmaster.DataModel;
using Riskmaster.Settings;
using System.Xml;

namespace Riskmaster.BusinessAdaptor
{
    public class PolicyStagingInterfaceAdaptor : BusinessAdaptorBase
    {
        public string GetPolicyDataFromStaging(int p_iPolicyStagingId, UserLogin p_oUserLogin, ref BusinessAdaptorErrors p_oSystemErrors)
        {
            PolicyStagingInterface oPolicyStagingInterface = null;
            string sResponse = string.Empty;
            try
            {
                oPolicyStagingInterface = new PolicyStagingInterface(p_oUserLogin,base.ClientId);
                sResponse = oPolicyStagingInterface.GetPolicyDataFromStaging(p_iPolicyStagingId);
            }
            catch (Exception ex)
            {
                p_oSystemErrors.Add(ex, BusinessAdaptorErrorType.Error);
            }
            return sResponse;
        }

        public string GetUnitList(int p_iPolicyStagingId, UserLogin p_oUserLogin, ref BusinessAdaptorErrors p_oSystemErrors)
        {
            PolicyStagingInterface oPolicyStagingInterface = null;
            string sResponse = string.Empty;
            try
            {
                oPolicyStagingInterface = new PolicyStagingInterface(p_oUserLogin,base.ClientId);
                sResponse = oPolicyStagingInterface.GetUnitList(p_iPolicyStagingId);
            }
            catch (Exception ex)
            {
                p_oSystemErrors.Add(ex, BusinessAdaptorErrorType.Error);
            }
            return sResponse;
        }        

        public string GetUnitDetailResult(int p_iPolicyStagingId, int p_iStagingUnitRowId, UserLogin p_oUserLogin, ref BusinessAdaptorErrors p_oSystemErrors)
        {
            PolicyStagingInterface oPolicyStagingInterface = null;
            string sResponse = string.Empty;
            try
            {
                oPolicyStagingInterface = new PolicyStagingInterface(p_oUserLogin,base.ClientId);
                sResponse = oPolicyStagingInterface.GetUnitDetailResult(p_iPolicyStagingId, p_iStagingUnitRowId);
            }
            catch (Exception ex)
            {
                p_oSystemErrors.Add(ex, BusinessAdaptorErrorType.Error);
            }
            return sResponse;
        }

        public string GetUnitCoverages(int p_iUnitStagingRowId, UserLogin p_oUserLogin, ref BusinessAdaptorErrors p_oSystemErrors)
        {
            PolicyStagingInterface oPolicyStagingInterface = null;
            string sResponse = string.Empty;
            try
            {
                oPolicyStagingInterface = new PolicyStagingInterface(p_oUserLogin,base.ClientId);
                sResponse = oPolicyStagingInterface.GetUnitCoverages(p_iUnitStagingRowId);
            }
            catch (Exception ex)
            {
                p_oSystemErrors.Add(ex, BusinessAdaptorErrorType.Error);
            }
            return sResponse;
        }

        //added by swati
        public string GetUnitInterestList(int p_iUnitStagingRowId, UserLogin p_oUserLogin, ref BusinessAdaptorErrors p_oSystemErrors)
        {
            PolicyStagingInterface oPolicyStagingInterface = null;
            string sResponse = string.Empty;
            try
            {
                oPolicyStagingInterface = new PolicyStagingInterface(p_oUserLogin,base.ClientId);
                sResponse = oPolicyStagingInterface.GetUnitInterestList(p_iUnitStagingRowId);
            }
            catch (Exception ex)
            {
                p_oSystemErrors.Add(ex, BusinessAdaptorErrorType.Error);
            }
            return sResponse;
        }

        public string GetPSEndorsementData(int p_iPolicyId, string p_sTableName, int p_iUnitRowId, UserLogin p_oUserLogin, ref BusinessAdaptorErrors p_oSystemErrors)
        {
            PolicyStagingInterface oPolicyStagingInterface = null;
            string sResponse = string.Empty;
            try
            {
                oPolicyStagingInterface = new PolicyStagingInterface(p_oUserLogin,base.ClientId);
                sResponse = oPolicyStagingInterface.GetPSEndorsementData(p_iPolicyId, p_sTableName, p_iUnitRowId);
            }
            catch (Exception ex)
            {
                p_oSystemErrors.Add(ex, BusinessAdaptorErrorType.Error);
            }
            return sResponse;
        }

        public string GetStgngPolicyInterestDetail(int p_iEntityStagingId, UserLogin p_oUserLogin, ref BusinessAdaptorErrors p_oSystemErrors)
        {
            PolicyStagingInterface oPolicyStagingInterface = null;
            string sResponse = string.Empty;
            try
            {
                oPolicyStagingInterface = new PolicyStagingInterface(p_oUserLogin,base.ClientId);
                sResponse = oPolicyStagingInterface.GetStgngPolicyInterestDetail(p_iEntityStagingId);
            }
            catch (Exception ex)
            {
                p_oSystemErrors.Add(ex, BusinessAdaptorErrorType.Error);
            }
            return sResponse;
        }

        public void SavePSEndorsementData(int iPolicyId, string sTableName, int iRowId, int iDownloadedPolicyId, int iDownloadedUnitId, ref BusinessAdaptorErrors p_objErrOut)
        {
            PolicyStagingInterface oPolicyStagingInterface = null;
            string sRecordIds = string.Empty;
            string sMode = string.Empty;

            try
            {
                oPolicyStagingInterface = new PolicyStagingInterface(m_userLogin,base.ClientId);
                oPolicyStagingInterface.SavePSEndorsementData(iPolicyId, sTableName, iRowId, iDownloadedPolicyId, iDownloadedUnitId);

            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
            }
            finally
            {
                oPolicyStagingInterface = null;
            }
        }
        //change end here by swati

        /// <summary>
        /// Gets Policy Interest list 
        /// </summary>
        /// <param name="p_iPolicyStagingId"></param>
        /// <param name="p_oUserLogin"></param>
        /// <param name="p_oSystemErrors"></param>
        /// <returns></returns>
        /// Added by Pradyumna - WWIG GAP 16 - 1/6/2013
        public string GetPolicyInterestList(int p_iPolicyStagingId, UserLogin p_oUserLogin, ref BusinessAdaptorErrors p_oSystemErrors)
        {
            PolicyStagingInterface oPolicyStagingInterface = null;
            string sResponse = string.Empty;
            try
            {
                oPolicyStagingInterface = new PolicyStagingInterface(p_oUserLogin, base.ClientId);
                sResponse = oPolicyStagingInterface.GetPolicyInterestList(p_iPolicyStagingId);
            }
            catch (RMAppException ex)
            {
                p_oSystemErrors.Add(ex, BusinessAdaptorErrorType.Error);
            }
            catch (Exception e)
            {
                p_oSystemErrors.Add(e, BusinessAdaptorErrorType.Error);
            }
            return sResponse;
        }

        /// <summary>
        /// Gets Policy Driver list 
        /// </summary>
        /// <param name="p_iPolicyStagingId"></param>
        /// <param name="p_oUserLogin"></param>
        /// <param name="p_oSystemErrors"></param>
        /// <returns></returns>
        /// Added by Pradyumna - WWIG GAP 16 - 1/8/2013
        public string GetDriverList(int p_iPolicyStagingId, UserLogin p_oUserLogin, ref BusinessAdaptorErrors p_oSystemErrors)
        {
            PolicyStagingInterface oPolicyStagingInterface = null;
            string sResponse = string.Empty;
            try
            {
                oPolicyStagingInterface = new PolicyStagingInterface(p_oUserLogin,base.ClientId);
                sResponse = oPolicyStagingInterface.GetDriverList(p_iPolicyStagingId);
            }
            catch (RMAppException ex)
            {
                p_oSystemErrors.Add(ex, BusinessAdaptorErrorType.Error);
            }
            catch (Exception e)
            {
                p_oSystemErrors.Add(e, BusinessAdaptorErrorType.Error);
            }
            return sResponse;
        }

        /// <summary>
        /// Gets Driver Details from Staging Tables
        /// </summary>
        /// <param name="p_iStagingEntityId"></param>
        /// <param name="p_oUserLogin"></param>
        /// <param name="p_oSystemErrors"></param>
        /// <returns></returns>
        /// Added by Pradyumna 1/10/13 - WWIG GAP 16 - 
        public string GetStgngDriverDetail(int p_iStagingEntityId, UserLogin p_oUserLogin, ref BusinessAdaptorErrors p_oSystemErrors)
        {
            PolicyStagingInterface oPolicyStagingInterface = null;
            string sResponse = string.Empty;
            try
            {
                oPolicyStagingInterface = new PolicyStagingInterface(p_oUserLogin,base.ClientId);
                sResponse = oPolicyStagingInterface.GetDriverDetails(p_iStagingEntityId);
            }
            catch (RMAppException ex)
            {
                p_oSystemErrors.Add(ex, BusinessAdaptorErrorType.Error);
            }
            catch (Exception e)
            {
                p_oSystemErrors.Add(e, BusinessAdaptorErrorType.Error);
            }
            return sResponse;
        }

        public bool GetXmlFromStaging(int p_iStagingPolicyId, string p_sRequestMode, out DisplayFileData oDisplayFileData, ref BusinessAdaptorErrors p_objErrOut)
        {
            PolicyStagingInterface oPolicyStagingInterface = null;
            PolicySystemInterface oPolicySystem = null;
            XmlDocument oDataList = null;
            bool bresult = false;
            string sMode = string.Empty;
            string sWCUnitId = string.Empty;
            oDisplayFileData = new DisplayFileData();
            string oAcordAutoRequest = string.Empty;
            string oAcordAutoResponse = string.Empty;

            try
            {
                oPolicyStagingInterface = new PolicyStagingInterface(m_userLogin,base.ClientId);
                oPolicySystem = new PolicySystemInterface(m_userLogin,base.ClientId);

                oDataList = oPolicyStagingInterface.GetXmlData(p_iStagingPolicyId, p_sRequestMode, ref sMode);
                if (oDataList != null)
                    oDisplayFileData.ResponseXML = oDataList.OuterXml;
                oDisplayFileData.Mode = sMode;
                if (string.Equals(sMode, "entity", StringComparison.InvariantCultureIgnoreCase) || (string.Equals(sMode, "unitinterest", StringComparison.InvariantCultureIgnoreCase)))
                {
                    oDisplayFileData.EntityTypelist = oPolicySystem.GetEntityList().OuterXml;
                }
                bresult = true;
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                oDataList = null;
                bresult = false;
            }
            finally
            {
                oPolicyStagingInterface = null;
            }
            return bresult;

        }

        /// <summary>
        /// Gets Policy search result from Staging table in XML format. MITS 33414 12/11/2013
        /// </summary>
        /// <param name="oSearchRequest"></param>
        /// <param name="oPolicySearchResponse"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns>bool</returns>
        public bool GetStgPolicySearchResult(PolicySearch p_oSearchRequest, out PolicySearch p_oPolicySearchResponse, ref BusinessAdaptorErrors p_objErrOut)
        {
            PolicyStagingInterface objPolStgInterface = null;
            XmlDocument oXMLPolicySearchResult = null;
            bool bresult = false;
            p_oPolicySearchResponse = new PolicySearch();
            p_oPolicySearchResponse.ResponseXML = string.Empty;
            try
            {
                objPolStgInterface = new PolicyStagingInterface(m_userLogin,base.ClientId);
                oXMLPolicySearchResult = objPolStgInterface.GetStgPolicySearchResult(p_oSearchRequest);
                if (!String.IsNullOrEmpty(oXMLPolicySearchResult.OuterXml))
                {
                    p_oPolicySearchResponse.ResponseXML = oXMLPolicySearchResult.OuterXml;
                    //tanwar2 - Set bResult to true
                    bresult = true;
                }
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                oXMLPolicySearchResult = null;
                bresult = false;
            }
            finally
            {
                objPolStgInterface = null;
                if (oXMLPolicySearchResult != null)
                    oXMLPolicySearchResult = null;
            }

            return bresult;
        }

        /// <summary>
        /// Download Policy data from Staging Area/Atables into rmA
        /// </summary>
        /// <param name="iClaimId"></param>
        /// <param name="oAcordResponse"></param>
        /// <param name="oSaveResponse"></param>
        /// <param name="p_objErrOut"></param>
        /// <param name="sPolicySystemId"></param>
        /// <returns></returns>
        public bool SavePolicyFrmStaging(int p_iStagingPolicyID, int p_iClaimId, ref PolicySaveRequest p_oSaveResponse, ref BusinessAdaptorErrors p_objErrOut)
        {
            p_oSaveResponse.Result = false;
            PolicyStagingInterface objManager = null;
            Policy objPolicy = null;
            string sAgentNumber = string.Empty;
            string sAgentName = string.Empty;

            try
            {
                //DeleteFile(sFileName);
                //oDataModelTemplate = XElement.Parse(GetXMLTemplateByType("Policy", ref p_objErrOut));

                objManager = new PolicyStagingInterface(m_userLogin,base.ClientId);
                objPolicy = objManager.SavePolicyFrmStaging(p_iStagingPolicyID, p_iClaimId);

                p_oSaveResponse.AddedPolicyId = objPolicy.PolicyId;
                //objManager.SaveAgents(sAgentNumber, sAgentName, oSaveResponse.AddedPolicyId);
                p_oSaveResponse.Result = true;
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                p_oSaveResponse.Result = false;
            }
            finally
            {
                if (objManager != null)
                    objManager = null;
                if (objPolicy != null)
                    objPolicy.Dispose();
            }

            return p_oSaveResponse.Result;
        }

        /// <summary>
        /// Save selected entity into rmA
        /// </summary>
        /// <param name="oSaveDownload"></param>
        /// <param name="sSelEntityStgId"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public int SaveOptionsFrmStaging(SaveDownloadOptions p_oSaveDownload, string p_sSelEntityStgId, UserLogin p_oUserLogin, ref BusinessAdaptorErrors p_objErrOut)
        {
            int iRecordId = 0;
            PolicyStagingInterface objPolicyStgInterface = null;

            try
            {
                objPolicyStgInterface = new PolicyStagingInterface(p_oUserLogin,base.ClientId);
                iRecordId = objPolicyStgInterface.SaveOptionsFrmStaging(p_oSaveDownload.PolicyId, p_oSaveDownload.StagingPolicyId, p_sSelEntityStgId, p_oSaveDownload.EntityRole, p_oSaveDownload.Mode);
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
            }
            finally
            {
                objPolicyStgInterface = null;
            }
            return iRecordId;
        }

        /// <summary>
        /// Fetches Policy search system settings of utilities
        /// </summary>
        /// <param name="objXmlIn"></param>
        /// <param name="objXmlOut"></param>
        /// <param name="objErrOut"></param>
        /// <returns></returns>
        /// <Author>Added by PSHEKHAWAT on 12/12/12</Author>
        public bool GetPolicySearchSystem(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            bool bRet = false;
            SysSettings objSettings;
            XmlNode oPolSearchSettings = null;
            try
            {
                objSettings = new SysSettings(connectionString,base.ClientId);

                oPolSearchSettings = p_objXmlOut.AppendChild(p_objXmlOut.CreateElement("PolicySearchSystem"));
                oPolSearchSettings.InnerText = objSettings.UseStagingPolicySystem.ToString();
                ((XmlElement)oPolSearchSettings).SetAttribute("NoRequiredField", objSettings.NoReqFieldsForPolicySearch.ToString());

                bRet = true;
            }
            catch (RMAppException objException)
            {
                p_objErrOut.Add(objException, BusinessAdaptorErrorType.Error);
            }
            finally
            {
                objSettings = null;
            }
            return bRet;
        }

        /// <summary>
        /// To Save selected Unit(s) in Download Units screen
        /// </summary>
        /// <param name="p_oSaveDownloadOptions"></param>
        /// <param name="p_iStagingUnitRowId"></param>
        /// <param name="p_iDownloadedUnitRowId"></param>
        /// <param name="p_oUserLogin"></param>
        /// <param name="p_objErrOut"></param>
        public void SaveUnit(ref SaveDownloadOptions p_oSaveDownloadOptions, int p_iStagingUnitRowId, ref int p_iDownloadedUnitRowId, UserLogin p_oUserLogin, ref BusinessAdaptorErrors p_objErrOut)
        {
            PolicyStagingInterface oPolicyStagingInterface = null;
            string sResponse = string.Empty;
            bool bIsSuccess = false; // Added by Pradyumna 03182014 MITS# 35296
            try
            {
                oPolicyStagingInterface = new PolicyStagingInterface(p_oUserLogin,base.ClientId);
                bIsSuccess = oPolicyStagingInterface.SaveUnit(p_oSaveDownloadOptions.PolicyId, p_iStagingUnitRowId, ref p_iDownloadedUnitRowId);
                // Pradyumna 03182014 MITS# 35296 - Start
                if (!bIsSuccess)
                {
                    p_objErrOut.Add("UnitNotDownloaded", "A Unit already exists for this policy. Only one unit per Policy can be downloaded.", BusinessAdaptorErrorType.Message); 
                }
                // Pradyumna 03182014 MITS# 35296 - End
            }
            
            catch (Exception ex)
            {
                p_objErrOut.Add(ex, BusinessAdaptorErrorType.Error);
            }
        } 

        public string GetStagingCoverage(int p_iStagingPolCvgRowId, int p_iStagingUnitRowId, UserLogin p_oUserLogin, ref BusinessAdaptorErrors p_oSystemErrors)
        {
            PolicyStagingInterface oPolicyStagingInterface = null;
            string sResponse = string.Empty;
            try
            {
                oPolicyStagingInterface = new PolicyStagingInterface(p_oUserLogin,base.ClientId);
                sResponse = oPolicyStagingInterface.GetStagingCoverage(p_iStagingPolCvgRowId, p_iStagingUnitRowId);
            }
            catch (Exception ex)
            {
                p_oSystemErrors.Add(ex, BusinessAdaptorErrorType.Error);
            }
            return sResponse;
        }

        /// <summary>
        /// Fetches Policy Interface config setting for Single/multiple Unit Download setting
        /// </summary>
        /// <param name="objXmlIn"></param>
        /// <param name="objXmlOut"></param>
        /// <param name="objErrOut"></param>
        /// <returns></returns>
        /// <Author>Added by PSHEKHAWAT on 01142014</Author>
        public bool GetPolicyConfig(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            bool bRet = false;
            PolicyStagingInterface oPolicyStgngInterface = null;
            XmlNode oUnitDwnloadSettings = null;
            try
            {
                oPolicyStgngInterface = new PolicyStagingInterface(m_userLogin,base.ClientId);
                bRet = oPolicyStgngInterface.GetPolicyUnitConfigSetting();
                if (!bRet)
                {
                    if (p_objXmlOut.SelectSingleNode("//Document//MultiUnitDownload") != null)
                    {
                        p_objXmlOut.SelectSingleNode("//Document//MultiUnitDownload").InnerText = "false";
                    }
                    oUnitDwnloadSettings = p_objXmlOut.AppendChild(p_objXmlOut.CreateElement("IsMultiUnitDownload"));
                    oUnitDwnloadSettings.InnerText = "false";
                }
            }
            catch (RMAppException objException)
            {
                p_objErrOut.Add(objException, BusinessAdaptorErrorType.Error);
            }
            finally
            {
                if (oPolicyStgngInterface != null)
                    oPolicyStgngInterface = null;
            }
            return bRet;
        }
    }
}
