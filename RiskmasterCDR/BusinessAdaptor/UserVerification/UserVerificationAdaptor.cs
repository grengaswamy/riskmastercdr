using System;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using System.IO;
using Riskmaster.Security;
using System.Collections;
using Riskmaster.Models;
using System.Collections.Generic;
using Riskmaster.Application.UserVerification;

namespace Riskmaster.BusinessAdaptor
{
    public class UserVerificationAdaptor : BusinessAdaptorBase
    {
        public bool GetVerificationTable(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            bool bResult = false;
            UserVerification objverification = null;
            //XmlNode ModuleName = p_objXmlIn.SelectSingleNode("//UserVerification//ModuleName");
            XmlNode ImportArea = p_objXmlIn.SelectSingleNode("//UserVerification//ImportArea");
            try
            {
                objverification = new UserVerification(base.ClientId);
                objverification.DAConnectionString = RMConfigurationManager.GetConfigConnectionString("RMXSTAGINGDATASOURCE", base.ClientId);
                bResult = objverification.GetVerificationTable(ImportArea != null ? ImportArea.InnerText : "", ref p_objXmlOut);
            }
            catch(Exception ex)
            {
                p_objErrOut.Add(ex, ex.Message, BusinessAdaptorErrorType.Error);
            }
            return bResult;
        }

        public bool GetImportArea(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            bool bResult = false;
            int iOptionSet_Id = 0;
            UserVerification objverification = null;
            XmlNode ModuleName = p_objXmlIn.SelectSingleNode("//UserVerification//ModuleName");
            XmlNode JobId = p_objXmlIn.SelectSingleNode("//UserVerification//JobId");
            try
            {
                objverification = new UserVerification(base.ClientId);
                objverification.TaskManagerConnectionString = RMConfigurationManager.GetConfigConnectionString("TaskManagerDataSource", base.ClientId);
                iOptionSet_Id = objverification.GetOptionSet_Id(Convert.ToInt32(JobId.InnerText), ref p_objXmlOut);
                objverification.DAConnectionString = RMConfigurationManager.GetConfigConnectionString("RMXSTAGINGDATASOURCE", base.ClientId);
                bResult = objverification.GetImportArea(ModuleName != null ? ModuleName.InnerText : "", Convert.ToInt32(JobId.InnerText),iOptionSet_Id,ref p_objXmlOut);
                //bResult = objverification.GetImportArea(ModuleName != null ? ModuleName.InnerText : "", ref p_objXmlOut);
            }
            catch (Exception ex)
            {
                p_objErrOut.Add(ex, ex.Message, BusinessAdaptorErrorType.Error);
            }
            return bResult;
        }

        public bool GetTableData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            bool bResult = false;
            UserVerification objverification = null;
            XmlNode TableName = p_objXmlIn.SelectSingleNode("//UserVerification//TableName");
            XmlNode ImportArea = p_objXmlIn.SelectSingleNode("//UserVerification//ImportArea");
            XmlNode JobId = p_objXmlIn.SelectSingleNode("//UserVerification//JobId");
            XmlNode ModuleName = p_objXmlIn.SelectSingleNode("//UserVerification//ModuleName");
            XmlNode DataOption = p_objXmlIn.SelectSingleNode("//UserVerification//DataOption");
            try
            {
                objverification = new UserVerification(base.ClientId);
                objverification.DAConnectionString = RMConfigurationManager.GetConfigConnectionString("RMXSTAGINGDATASOURCE", base.ClientId);
                bResult = objverification.GetTableData(ModuleName.InnerText,TableName.InnerText,ImportArea.InnerText, Convert.ToInt32(JobId.InnerText),DataOption.InnerText, ref p_objXmlOut);
            }
            catch(Exception ex)
            {
                p_objErrOut.Add(ex, ex.Message, BusinessAdaptorErrorType.Error);
            }
            return bResult;
        }

        public bool ReVerifyJob(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            bool bResult = false;
            UserVerification objverification = null;
            XmlNode JobId = p_objXmlIn.SelectSingleNode("//UserVerification//JobId");
            try
            {
                objverification = new UserVerification(base.ClientId);
                objverification.TaskManagerConnectionString = RMConfigurationManager.GetConfigConnectionString("TaskManagerDataSource", base.ClientId);
                bResult = objverification.ReVerifyJob(Convert.ToInt32(JobId.InnerText), ref p_objXmlOut);
            }
            catch (Exception ex)
            {
                bResult = false;
                p_objErrOut.Add(ex, ex.Message, BusinessAdaptorErrorType.Error);
            }
            return bResult;
        }
        //ipuri Start
        public bool ResumeTask(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            bool bResult = false;
            UserVerification objverification = null;
            XmlNode JobId = p_objXmlIn.SelectSingleNode("//UserVerification//JobId");
            try
            {
                objverification = new UserVerification(base.ClientId);
                objverification.TaskManagerConnectionString = RMConfigurationManager.GetConfigConnectionString("TaskManagerDataSource", base.ClientId);
                bResult = objverification.ResumeTask(Convert.ToInt32(JobId.InnerText), ref p_objXmlOut);
            }
            catch (Exception ex)
            {
                bResult = false;
                p_objErrOut.Add(ex, ex.Message, BusinessAdaptorErrorType.Error);
            }
            return bResult;
        }

        public bool GetJobId(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            bool bResult = false;
            int iOptionSet_Id = 0;
            int iMax_Jobid = 0;
            UserVerification objverification = null;
            XmlNode JobId = p_objXmlIn.SelectSingleNode("//UserVerification//JobId");
            try
            {
                objverification = new UserVerification(base.ClientId);
                objverification.TaskManagerConnectionString = RMConfigurationManager.GetConfigConnectionString("TaskManagerDataSource", base.ClientId);
                iOptionSet_Id = objverification.GetOptionSet_Id(Convert.ToInt32(JobId.InnerText), ref p_objXmlOut);
                objverification.TaskManagerConnectionString = RMConfigurationManager.GetConfigConnectionString("TaskManagerDataSource", base.ClientId);
                iMax_Jobid = objverification.MaxJobid(Convert.ToInt32(JobId.InnerText), iOptionSet_Id, ref p_objXmlOut);
                objverification.DAConnectionString = RMConfigurationManager.GetConfigConnectionString("RMXSTAGINGDATASOURCE",base.ClientId);
                bResult = objverification.UpdateJobId(Convert.ToInt32(JobId.InnerText), iMax_Jobid,iOptionSet_Id, ref p_objXmlOut);

            }
            catch (Exception ex)
            {
                bResult = false;
                p_objErrOut.Add(ex, ex.Message, BusinessAdaptorErrorType.Error);
            }
            return bResult;

        }
        public bool StopVerification(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            bool bResult = false;
            UserVerification objverification = null;
            XmlNode ModuleName = p_objXmlIn.SelectSingleNode("//UserVerification//ModuleName");
            XmlNode StopVerify = p_objXmlIn.SelectSingleNode("//UserVerification//StopVerify");
            try
            {
                objverification = new UserVerification(base.ClientId);
                objverification.DAConnectionString = RMConfigurationManager.GetConfigConnectionString("RMXSTAGINGDATASOURCE", base.ClientId);
                bResult = objverification.StopVerification(ModuleName != null ? ModuleName.InnerText : "",StopVerify != null ? StopVerify.InnerText : "false", ref p_objXmlOut);
            }
            catch (Exception ex)
            {
                bResult = false;
                p_objErrOut.Add(ex, ex.Message, BusinessAdaptorErrorType.Error);
            }
            return bResult;
        }
        //ipuri End
        public bool AbondonJob(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            bool bResult = false;
            UserVerification objverification = null;
            XmlNode JobId = p_objXmlIn.SelectSingleNode("//UserVerification//JobId");
            try
            {
                objverification = new UserVerification(base.ClientId);
                objverification.TaskManagerConnectionString = RMConfigurationManager.GetConfigConnectionString("TaskManagerDataSource", base.ClientId);
                bResult = objverification.AbondonJob(Convert.ToInt32(JobId.InnerText), ref p_objXmlOut);
                bResult = true;
            }
            catch (Exception ex)
            {
                bResult = false;
                p_objErrOut.Add(ex, ex.Message, BusinessAdaptorErrorType.Error);
            }
            return bResult;
        }

        public bool GetErrorDescription(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            bool bResult = false;
            UserVerification objverification = null;
            XmlNode TableName = p_objXmlIn.SelectSingleNode("//UserVerification//TableName");
            XmlNode JobId = p_objXmlIn.SelectSingleNode("//UserVerification//JobId");
            XmlNode RowId = p_objXmlIn.SelectSingleNode("//UserVerification//RowId");
            try
            {
                objverification = new UserVerification(base.ClientId);
                objverification.DAConnectionString = RMConfigurationManager.GetConfigConnectionString("RMXSTAGINGDATASOURCE", base.ClientId);
                bResult = objverification.GetErrorDescription(Convert.ToInt32(JobId.InnerText), Convert.ToInt32(RowId.InnerText), TableName.InnerText, ref p_objXmlOut);
            }
            catch (Exception ex)
            {
                bResult = false;
                p_objErrOut.Add(ex, ex.Message, BusinessAdaptorErrorType.Error);
            }
            return bResult;
        }
        public bool UpdateTableData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            bool bResult = false;
            UserVerification objverification = null;
            try
            {
                objverification = new UserVerification(base.ClientId);
                objverification.DAConnectionString = RMConfigurationManager.GetConfigConnectionString("RMXSTAGINGDATASOURCE", base.ClientId);
                bResult = objverification.UpdateTableData(p_objXmlIn, ref p_objXmlOut);
            }
            catch (Exception ex)
            {
                bResult = false;
                p_objErrOut.Add(ex, ex.Message, BusinessAdaptorErrorType.Error);
            }
            return bResult;
        }

    }
}
