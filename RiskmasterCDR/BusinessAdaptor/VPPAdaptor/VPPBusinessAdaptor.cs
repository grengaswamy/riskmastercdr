﻿using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Models;
using Riskmaster.Db;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.BusinessAdaptor.VPPAdaptor
{
    public class VPPBusinessAdaptor : BusinessAdaptorBase
    {
        private string m_ViewDBConnectionString = string.Empty;

        /// <summary>
        /// Default class constructor
        /// </summary>
        public VPPBusinessAdaptor(int p_iClientId)
        {
            m_ViewDBConnectionString = RMConfigurationManager.GetConnectionString("ViewDataSource", p_iClientId);
        }
        /// <summary>
        /// Retrieve aspx page content from View database.
        /// </summary>
        /// <param name="request">request for this method</param>
        /// <param name="response">response back from the method call</param>
        /// <param name="p_objErrOut">errors occurred</param>
        /// <returns>ture for success, false for fail</returns>
        public bool GetFileContent(VPPFileRequest request, out VPPFileResponse response, ref BusinessAdaptorErrors p_objErrOut)
        {
            string sFileContent = string.Empty;
            response = new VPPFileResponse(); ;
            bool bReturnValue = true;

            string sDsnId = request.DataSourceId.ToString();
            string sViewId = request.ViewId.ToString();
            string sPageName = request.PageName;

            //It may need throw exception if any of there value is blank
            try
            {
                // akaushik5 Added for MITS 30290 Starts
                //if ((RMConfigurationManager.GetAppSetting("DynamicViews") + ".aspx").ToLower() == sPageName.ToLower())
                //{
                //    sPageName = (RMConfigurationManager.GetAppSetting("DynamicViews") + "d.aspx").ToLower();
                //}
                // akaushik5 Added for MITS 30290 Ends
                //kkaur8 added for MITS 33072 Starts
                if (RMConfigurationManager.GetAppSetting("DynamicViews") != null)
                {
                    string[] DynamicViews = RMConfigurationManager.GetAppSetting("DynamicViews").Split(',');
                    foreach (string DyamicView in DynamicViews)
                        if (((DyamicView + ".aspx").ToLower()).Trim() == sPageName.ToLower())
                        {
                            sPageName = (DyamicView + "d.aspx").ToLower().Trim();
                        }
                }
                //kkaur8 added for MITS 33072 Ends
                if (!(string.IsNullOrEmpty(sDsnId) || string.IsNullOrEmpty(sViewId) || string.IsNullOrEmpty(sPageName)))
                {
                    StringBuilder sbSQL = new StringBuilder();
                    sbSQL.Append("SELECT PAGE_CONTENT FROM NET_VIEW_FORMS WHERE DATA_SOURCE_ID=" + sDsnId);
                    sbSQL.Append(" AND VIEW_ID=" + sViewId);
                    sbSQL.Append(" AND LOWER(PAGE_NAME)='" + sPageName.ToLower() + "'");

                    using (DbReader oDbReader = DbFactory.GetDbReader(m_ViewDBConnectionString, sbSQL.ToString()))
                    {
                        if (oDbReader.Read())
                        {
                            sFileContent = oDbReader["PAGE_CONTENT"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                bReturnValue = false;
                p_objErrOut.Add(ex, BusinessAdaptorErrorType.Error);
            }

            response.FileContent = sFileContent;
            return bReturnValue;
        }

        /// <summary>
        /// Get max value of the last_updated value for all forms, and delete records in the log table 
        /// which are more than 30 days old.
        /// </summary>
        /// <param name="request">request for this method</param>
        /// <param name="response">response back from the method call</param>
        /// <param name="p_objErrOut">errors occurred</param>
        /// <returns></returns>
        public bool GetInitialTimeStamp(RMServiceType request, out VPPTimeStampResponse response, ref BusinessAdaptorErrors p_objErrOut)
        {
            string sMaxLastUpdated = string.Empty;
            response = new VPPTimeStampResponse();
            bool bReturnValue = true;

            try
            {
                string sSQL = "SELECT MAX(LAST_UPDATED) FROM NET_VIEW_LOGS";
                using (DbReader oDbReader = DbFactory.GetDbReader(m_ViewDBConnectionString, sSQL))
                {
                    if (oDbReader.Read())
                    {
                        sMaxLastUpdated = oDbReader[0].ToString();
                    }
                }

                //Delete records more than 30 days old in the log table because they are no longer needed
                string sLastUpdated = Conversion.ToDbDateTime(DateTime.Now.AddDays(-30));
                sSQL = string.Format("DELETE FROM NET_VIEW_LOGS WHERE LAST_UPDATED <='{0}'", sLastUpdated);
                DbFactory.ExecuteNonQuery(m_ViewDBConnectionString, sSQL);
            }
            catch (Exception ex)
            {
                bReturnValue = false;
                p_objErrOut.Add(ex, BusinessAdaptorErrorType.Error);
            }

            response.TimeStamp = sMaxLastUpdated;
            return bReturnValue;
        }


        /// <summary>
        /// Get time stamp for the specified page file
        /// </summary>
        /// <param name="request">request for this method</param>
        /// <param name="response">response back from the method call</param>
        /// <param name="p_objErrOut">errors occurred</param>
        /// <returns></returns>
        public bool GetFileTimeStamp(VPPFileRequest request, out VPPTimeStampResponse response, ref BusinessAdaptorErrors p_objErrOut)
        {
            response = new VPPTimeStampResponse();
            string sLastUpdated = string.Empty;
            bool bReturnValue = true;

            try
            {
                string sDsnId = request.DataSourceId.ToString();
                string sViewId = request.ViewId.ToString();
                string sPageName = request.PageName;

                if (!(string.IsNullOrEmpty(sDsnId) || string.IsNullOrEmpty(sViewId) || string.IsNullOrEmpty(sPageName)))
                {
                    StringBuilder sbSQL = new StringBuilder();
                    sbSQL.Append("SELECT LAST_UPDATED FROM NET_VIEW_FORMS WHERE DATA_SOURCE_ID=" + sDsnId);
                    sbSQL.Append(" AND VIEW_ID=" + sViewId);
                    sbSQL.Append(" AND PAGE_NAME='" + sPageName + "'");
                    using (DbReader oDbReader = DbFactory.GetDbReader(m_ViewDBConnectionString, sbSQL.ToString()))
                    {
                        if (oDbReader.Read())
                        {
                            sLastUpdated = oDbReader["LAST_UPDATED"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                bReturnValue = false;
                p_objErrOut.Add(ex, BusinessAdaptorErrorType.Error);
            }

            response.TimeStamp = sLastUpdated;
            return bReturnValue;
        }


        /// <summary>
        /// Get the form files which were modified after last pooling cycle
        /// </summary>
        /// <param name="request">request for this method</param>
        /// <param name="response">response back from the method call</param>
        /// <param name="p_objErrOut">errors occurred</param>
        /// <returns></returns>
        public bool GetModifiedPageList(VPPModifiedPagesRequest request, out VPPModifiedPagesResponse response, ref BusinessAdaptorErrors p_objErrOut)
        {
            string sLastUpdated = request.LastUpdated;
            List<string> oPageList = new List<string>();
            string sMaxLastUpdated = request.LastUpdated;
            response = new VPPModifiedPagesResponse();
            bool bReturnValue = true;

            try
            {
                string sSQL = "SELECT DATA_SOURCE_ID, VIEW_ID, PAGE_NAME, LAST_UPDATED FROM NET_VIEW_LOGS WHERE LAST_UPDATED > '" + sLastUpdated + "'";
                using (DbReader oDbReader = DbFactory.GetDbReader(m_ViewDBConnectionString, sSQL))
                {
                    while (oDbReader.Read())
                    {
                        string sDataSourceID = oDbReader["DATA_SOURCE_ID"].ToString();
                        string sViewID = oDbReader["VIEW_ID"].ToString();
                        string sPageName = oDbReader["PAGE_NAME"].ToString();
                        sLastUpdated = oDbReader["LAST_UPDATED"].ToString();

                        //Notify all the related ViewCacheDependency objects
                        if (!string.IsNullOrEmpty(sDataSourceID) && !string.IsNullOrEmpty(sViewID) && !string.IsNullOrEmpty(sPageName))
                        {
                            string sKeyName = sDataSourceID + "_" + sViewID + "_" + request.ClientId + "_" + sPageName.ToLower();
                            oPageList.Add(sKeyName);

                            if (sMaxLastUpdated.CompareTo(sLastUpdated) < 0)
                                sMaxLastUpdated = sLastUpdated;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                bReturnValue = false;
                p_objErrOut.Add(ex, BusinessAdaptorErrorType.Error);
            }

            response.ModifiedPageList = oPageList;
            response.MaxLastUpdated = sMaxLastUpdated;
            return bReturnValue;
        }
    }
}
