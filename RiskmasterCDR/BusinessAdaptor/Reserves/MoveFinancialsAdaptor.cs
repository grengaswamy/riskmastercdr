﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Riskmaster.Application.Reserves;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.BusinessAdaptor
{
  public  class MoveFinancialsAdaptor : BusinessAdaptorBase
    {
        
      /// <summary>
      /// Load initial policy details for move financial processing
      /// </summary>
      /// <param name="p_objXmlIn"></param>
      /// <param name="p_objXmlOut"></param>
      /// <param name="p_objErrOut"></param>
      /// <returns></returns>
      public bool GetFinancialData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            int iClaimID = 0;
            int iPolicyId = 0;
            int iPolCvgID = 0;
            int iPolicyUnitRowId = 0;
            int iLOB = 0;
            MoveFinancials objMoveFinancials = null;
            try
            {
                iClaimID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//ClaimID").InnerText);
                iPolicyId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//PolicyId").InnerText);
                iPolCvgID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//CovgID").InnerText);
                iPolicyUnitRowId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//PolicyUnitRowId").InnerText);
                iLOB = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//LOB").InnerText);
                objMoveFinancials = new MoveFinancials(this.m_userLogin, base.ClientId);
                p_objXmlOut = objMoveFinancials.GetFinancialData(iClaimID, iPolCvgID, iPolicyUnitRowId, iPolicyId, iLOB);

            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objMoveFinancials = null;
            }
            return true;
        }
      
      /// <summary>
      /// Load policy details to render policy tree for move
      /// </summary>
      /// <param name="p_objXmlIn"></param>
      /// <param name="p_objXmlOut"></param>
      /// <param name="p_objErrOut"></param>
      /// <returns></returns>
      public bool GetTreeData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            int iClaimID = 0;
            MoveFinancials objMoveFinancials = null;
            string sChildNode = string.Empty;
            string sPolicyId = string.Empty;
            try
            {
                iClaimID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//ClaimID").InnerText);
                sPolicyId = p_objXmlIn.SelectSingleNode("//PolicyId").InnerText;
                objMoveFinancials = new MoveFinancials(this.m_userLogin, base.ClientId);
                p_objXmlOut = objMoveFinancials.GetTreeData(iClaimID, sPolicyId);
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objMoveFinancials = null;
            }
            return true;
        }
      /// <summary>
      /// Move financials
      /// </summary>
      /// <param name="p_objXmlIn"></param>
      /// <param name="p_objXmlOut"></param>
      /// <param name="p_objErrOut"></param>
      /// <returns></returns>
      public bool BulkFinancialMove(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
      {
          int iClaimID = 0;
          string sMappedIds=string.Empty;
          MoveFinancials objMoveFinancials = null;
          int iTargetPolicySystemId = 0;
          int iSourcePolicySystemId = 0;
          try
          {
              iClaimID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//ClaimID").InnerText);
              sMappedIds =p_objXmlIn.SelectSingleNode("//MappedIds").InnerText;
            
              objMoveFinancials = new MoveFinancials(this.m_userLogin, base.ClientId);
              iTargetPolicySystemId = objMoveFinancials.GetPolicySystemId(Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//TargetPolicyId").InnerText));
              iSourcePolicySystemId = objMoveFinancials.GetPolicySystemId(Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//SourcePolicyId").InnerText));
              objMoveFinancials.BulkFinancialMove(sMappedIds, iClaimID,iTargetPolicySystemId,iSourcePolicySystemId);
          }
          catch (Exception p_objException)
          {
              p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
              return false;
          }
          finally
          {
              objMoveFinancials = null;
          }
          return true;
      }

      /// <summary>
      /// Detach or Invalid Policy 
      /// </summary>
      /// <param name="p_objXmlIn"></param>
      /// <param name="p_objXmlOut"></param>
      /// <param name="p_objErrOut"></param>
      /// <returns></returns>
      public bool MarkPolicyInvalid(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
      {
            int iPolicyId = 0;
          MoveFinancials objMoveFinancials = null;
         
          try
          {
              iPolicyId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//SourcePolicyId").InnerText);
             


              objMoveFinancials = new MoveFinancials(this.m_userLogin, base.ClientId);
              objMoveFinancials.MarkPolicyInvalid(iPolicyId);
          }
        
          catch (Exception p_objException)
          {
              p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
              return false;
          }
          finally
          {
              objMoveFinancials = null;
          }
          return true;
      }
      /// <summary>
      /// Move Reserve 
      /// </summary>
      /// <param name="p_objXmlIn"></param>
      /// <param name="p_objXmlOut"></param>
      /// <param name="p_objErrOut"></param>
      /// <returns></returns>
      public bool EditReserveKey(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
      {
          int iClaimID = 0;
          MoveFinancials objMoveFinancials = null;
          int iRCRowId = 0;
          int iLob=0;
          int iClaimantEid=0;
          int iLossType=0;
          int iRsvType = 0;
          int iTargetPolicySystemId = 0;
          try
          {
              iClaimID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//ClaimID").InnerText);
              iRCRowId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//RCRowId").InnerText);
              iClaimantEid = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//NewClaimantEid").InnerText);
              iLossType = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//NewLossType").InnerText);
              iRsvType = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//NewReserveType").InnerText);
              iLob = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//LOB").InnerText);



              objMoveFinancials = new MoveFinancials(this.m_userLogin, base.ClientId);
              iTargetPolicySystemId = objMoveFinancials.GetPolicySystemId(Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//TargetPolicyId").InnerText));

              objMoveFinancials.EditReserveKey(iRCRowId, iClaimID, iClaimantEid, iLossType, iRsvType,iLob,iTargetPolicySystemId);
          }
          catch (Exception p_objException)
          {
              p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
              return false;
          }
          finally
          {
              objMoveFinancials = null;
          }
          return true;
      }
      
    }
}
