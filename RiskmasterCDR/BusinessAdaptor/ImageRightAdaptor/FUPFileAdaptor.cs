﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Riskmaster.Application.ImagRightWrapper;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;

namespace Riskmaster.BusinessAdaptor
{
    public class FUPFileAdaptor : BusinessAdaptorBase
    {
        public bool GetFupFileLayout(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            FUPFileMappings oFupFileMappings = null;
            try
            {
                oFupFileMappings = new FUPFileMappings(base.userLogin,base.ClientId);
                p_objXmlOut = oFupFileMappings.GetFupFileLayout();
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ImageRightAdaptor.ApproveMethodError", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            return true;
        }

        public bool SaveFupFileLayout(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            FUPFileMappings oFupFileMappings = null;
            try
            {
                oFupFileMappings = new FUPFileMappings(base.userLogin,base.ClientId);
                p_objXmlOut = oFupFileMappings.SaveFUPFileLayout(p_objXmlIn);
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ImageRightAdaptor.ApproveMethodError", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            return true;
        }

        public bool GetUserDataMapping(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            FUPFileMappings oFupFileMappings = null;
            string sUserdataId = string.Empty;
            string sType = string.Empty;
            XmlNode xId = null;
            XmlNode xType = null;
            try
            {
                oFupFileMappings = new FUPFileMappings(base.userLogin,base.ClientId);
                xId = p_objXmlIn.SelectSingleNode("//UserDataMapId");
                xType = p_objXmlIn.SelectSingleNode("//UserDataType");
                if (xId!=null)
                {
                    sUserdataId = xId.InnerText;
                }
                if (xType != null)
                {
                    sType = xType.InnerText;
                }
                p_objXmlOut = oFupFileMappings.GetUserDataMapping(sUserdataId, sType);
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ImageRightAdaptor.ApproveMethodError", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            return true;
        }

        public bool SaveUserDataMapping(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            FUPFileMappings oFupFileMappings = null;
            try
            {
                oFupFileMappings = new FUPFileMappings(base.userLogin,base.ClientId);
                p_objXmlOut = oFupFileMappings.SaveUserDataMapping(p_objXmlIn);
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ImageRightAdaptor.ApproveMethodError", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            return true;
        }

        public bool DeleteUserDataMapping(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            FUPFileMappings oFupFileMappings = null;
            string sUserdataId = string.Empty;
            string sType = string.Empty;
            XmlNode xId = null;
            try
            {
                oFupFileMappings = new FUPFileMappings(base.userLogin,base.ClientId);
                xId = p_objXmlIn.SelectSingleNode("//UserDataMapId");
                if (xId != null)
                {
                    sUserdataId = xId.InnerText;
                }
                oFupFileMappings.DeleteUserDataMapping(sUserdataId);
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ImageRightAdaptor.ApproveMethodError", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            return true;
        }

        public bool GetSuppValue(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            FUPFileMappings oFupFileMappings = null;
            try
            {
                if (p_objXmlIn.SelectSingleNode("//SuppTable") != null)
                {
                    oFupFileMappings = new FUPFileMappings(base.userLogin,base.ClientId);
                    p_objXmlOut = oFupFileMappings.GetSuppValue(p_objXmlIn.SelectSingleNode("//SuppTable").InnerText);
                }
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ImageRightAdaptor.ApproveMethodError", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            return true;
        }

        public bool GetSuppType(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            FUPFileMappings oFupFileMappings = null;
            try
            {
                if (p_objXmlIn.SelectSingleNode("//SuppTable") != null && p_objXmlIn.SelectSingleNode("//FieldName") != null)
                {
                    oFupFileMappings = new FUPFileMappings(base.userLogin,base.ClientId);
                    p_objXmlOut = oFupFileMappings.GetSuppType(p_objXmlIn.SelectSingleNode("//SuppTable").InnerText, 
                        p_objXmlIn.SelectSingleNode("//FieldName").InnerText);
                }
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ImageRightAdaptor.ApproveMethodError", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            return true;
        }

        public bool GetFileMarks(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            FUPFileMappings oFupFileMappings = null;
            XmlNode xId = null;
            string sFileMarkMappingId = string.Empty;
            try
            {
                oFupFileMappings = new FUPFileMappings(base.userLogin,base.ClientId);
                xId = p_objXmlIn.SelectSingleNode("//IRFileMarkMappingId");
                if (xId != null)
                {
                    sFileMarkMappingId = xId.InnerText;
                }
                p_objXmlOut = oFupFileMappings.GetFileMarks(sFileMarkMappingId);
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("FUPFile.SaveFileMarksError", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            return true;
        }

        public bool SaveFileMarks(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            FUPFileMappings oFupFileMappings = null;
            try
            {
                oFupFileMappings = new FUPFileMappings(base.userLogin,base.ClientId);
                p_objXmlOut = oFupFileMappings.SaveFileMarks(p_objXmlIn);
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("FUPFile.SaveFileMarksError", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            return true;
        }

 		/// <summary>
        /// Function to Delete the File Mark Mapping
        /// </summary>
        /// <createdby>Varun - vchouhan6</createdby>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns>bool</returns>
        public bool DeleteFileMarkMapping(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            bool bSuccess = false;
            FUPFileMappings oFupFileMappings = null;
            XmlNode xId = null;
            string sFileMarkMappingId = string.Empty;
            try
            {
                oFupFileMappings = new FUPFileMappings(base.userLogin,base.ClientId);
                xId = p_objXmlIn.SelectSingleNode("//IRFileMarkMappingId");
                if (xId != null)
                {
                    sFileMarkMappingId = xId.InnerText;
                }
                bSuccess = oFupFileMappings.DeleteFileMarkMapping(sFileMarkMappingId);
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("FUPFile.DeleteFileMarkMappingError", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            return bSuccess;
        }
        public bool GenerateFUPFile(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            FUPGenerator oFupGenerator = null;
            try
            {
                oFupGenerator = new FUPGenerator(base.userLogin,base.ClientId);
                p_objXmlOut = oFupGenerator.GenerateFUP(p_objXmlIn);
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("FUPFile.GenerateFUPFileError", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            return true;
        }
    }
}
