﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.ImagRightWrapper;
using Riskmaster.Common;

namespace Riskmaster.BusinessAdaptor
{
    public class ImageRightAdaptor : BusinessAdaptorBase
    {
        /// <summary>
        /// ImageRight Approve functionality.
        /// The RISKMASTER document is copied to the designated folder of ImageRight.
        /// Catch: Once the document is moved to ImageRight directory - it is responsibility of ImageRight to import it to correct folder.
        /// Catch: There is no way for RISKMASTER to know if the document was imported to correct directory or not.
        /// </summary>
        /// <param name="p_objXmlIn">Input XmlDocument</param>
        /// <param name="p_objXmlOut">XmlDocument containing ImageRight Document details</param>
        /// <param name="p_objErrOut">Error if any</param>
        /// <returns>true if operation is successful - else false</returns>
        public bool Approve(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            bool bSuccess = false;
            string sClaimId = string.Empty;
            string sDocIds= string.Empty;
            ImageRightFileHandling oIRFileHandling = null;
            try
            {
                if (p_objXmlIn.SelectSingleNode("//ClaimId") != null)
                {
                    sClaimId = p_objXmlIn.SelectSingleNode("//ClaimId").InnerText;
                }
                if (p_objXmlIn.SelectSingleNode("//DocumentIds") != null)
                {
                    sDocIds = p_objXmlIn.SelectSingleNode("//DocumentIds").InnerText;
                }
                oIRFileHandling = new ImageRightFileHandling(base.userLogin, base.ClientId);
                bSuccess = oIRFileHandling.SaveToImageRight(sDocIds, sClaimId);
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ImageRightAdaptor.ApproveMethodError", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }

            return bSuccess;
        }

        /// <summary>
        /// Gets Document from ImageRight
        /// </summary>
        /// <param name="p_objXmlIn">Input XmlDocument</param>
        /// <param name="p_objXmlOut">XmlDocument containing ImageRight Document details</param>
        /// <param name="p_objErrOut">Error if any</param>
        /// <returns>true if operation is successful - else false</returns>
        public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            bool bSuccess = false;
            string sClaimId = string.Empty;
            ImageRight oImageRight = null;
            try
            {
                if (p_objXmlIn.SelectSingleNode("//ClaimId") != null)
                {
                    sClaimId = p_objXmlIn.SelectSingleNode("//ClaimId").InnerText;
                }
                oImageRight = new ImageRight(base.connectionString,base.ClientId);
                p_objXmlOut = oImageRight.GetDocumentsForClaim(sClaimId);
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ImageRightAdaptor.GetMethodError", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (oImageRight!=null)
                {
                    oImageRight.Dispose();
                }
            }

            return bSuccess;
        }
    }
}
