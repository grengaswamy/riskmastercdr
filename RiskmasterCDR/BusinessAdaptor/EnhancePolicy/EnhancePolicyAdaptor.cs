using System;
using System.IO;
using System.Xml;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Security;
using Riskmaster.Application.EnhancePolicy;
namespace Riskmaster.BusinessAdaptor
{
    ///************************************************************** 
    ///* $File		: EnhancePolicyAdaptor.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 22-Dec-2006
    ///* $Author	: Divya Bhagchandani
    ///* $Comment	: 
    ///* $Source	: 
    ///**************************************************************
    /// <summary>	
    ///	This class is used to call the application layer component for EnhancePolicy .
    /// </summary>
    public class EnhancePolicyAdaptor : BusinessAdaptorBase
    {
        public EnhancePolicyAdaptor()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public UserLogin userLogin
        {
            get { return m_userLogin; }
        }

        public bool GetForms(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            PrintManager objPolicy = null;
            int lPolicyID;
            string sDisplayForms = "";
            try
            {
                lPolicyID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//policyid").InnerText);
                //Mona
                sDisplayForms = p_objXmlIn.SelectSingleNode("//DisplayFormIds").InnerText;
                // Naresh Connection Leak
                using (objPolicy = new PrintManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    if (sDisplayForms == "")
                    {
                        p_objXmlOut = objPolicy.GetPolicies(lPolicyID);
                    }
                    else
                    {
                        p_objXmlOut = objPolicy.GetPolicies(lPolicyID, sDisplayForms);
                    }

                }
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }

            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("EnhancePolicyAdaptor.GetForms.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objPolicy != null)
                {
                    objPolicy = null;
                }
            }
        }

        public bool SaveForms(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            PrintManager objPolicy = null;
            string sSelectedFormIds;
            // npadhy Start MITS 18860 If there are form on the Page which are not saved, then after postback we need to retain those Forms
            string sAddedForms = string.Empty;
            // npadhy End MITS 18860 If there are form on the Page which are not saved, then after postback we need to retain those Forms
            bool bRetVal = false;
            XmlElement objResult = null;
            int lPolicyID;
            try
            {
                lPolicyID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//formDetails/policyid").InnerText);

                sSelectedFormIds = p_objXmlIn.SelectSingleNode("//formDetails/selectedFormIds").InnerText;
                
                // npadhy Start MITS 18860 If there are form on the Page which are not saved, then after postback we need to retain those Forms
                sAddedForms = p_objXmlIn.SelectSingleNode("//formDetails/DisplayFormIds").InnerText;
                // npadhy End MITS 18860 If there are form on the Page which are not saved, then after postback we need to retain those Forms

                // Naresh Connection Leak
                using (objPolicy = new PrintManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    // npadhy Start MITS 18860 If there are form on the Page which are not saved, then after postback we need to retain those Forms
                    if(string.IsNullOrEmpty(sAddedForms))
                        p_objXmlOut = objPolicy.SaveForms(sSelectedFormIds, lPolicyID);
                    else
                        p_objXmlOut = objPolicy.SaveForms(sSelectedFormIds, lPolicyID,sAddedForms);
                    // npadhy End MITS 18860 If there are form on the Page which are not saved, then after postback we need to retain those Forms
                }
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }

            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("EnhancePolicyAdaptor.SaveForms.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objPolicy != null)
                {
                    objPolicy = null;
                }
            }
            return true;
        }

        public bool DeleteForms(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            PrintManager objPolicy = null;
            string sSelectedFormIds;
            // npadhy Start MITS 18860 If there are form on the Page which are not saved, then after postback we need to retain those Forms
            string sAddedForms = string.Empty;
            // npadhy End MITS 18860 If there are form on the Page which are not saved, then after postback we need to retain those Forms

            int lPolicyID;
            try
            {
                lPolicyID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//formDetails/policyid").InnerText);

                sSelectedFormIds = p_objXmlIn.SelectSingleNode("//formDetails/selectedFormIds").InnerText;
                // npadhy Start MITS 18860 If there are form on the Page which are not saved, then after postback we need to retain those Forms
                sAddedForms = p_objXmlIn.SelectSingleNode("//formDetails/DisplayFormIds").InnerText;
                // npadhy End MITS 18860 If there are form on the Page which are not saved, then after postback we need to retain those Forms

                // Naresh Connection Leak
                using (objPolicy = new PrintManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    // npadhy Start MITS 18860 If there are form on the Page which are not saved, then after postback we need to retain those Forms
                    if(String.IsNullOrEmpty(sAddedForms))
                        p_objXmlOut = objPolicy.DeleteForms(sSelectedFormIds, lPolicyID);
                    else
                        p_objXmlOut = objPolicy.DeleteForms(sSelectedFormIds, lPolicyID,sAddedForms);
                    // npadhy End MITS 18860 If there are form on the Page which are not saved, then after postback we need to retain those Forms
                }
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }

            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("EnhancePolicyAdaptor.GetForms.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objPolicy != null)
                {
                    objPolicy = null;
                }
            }
            return true;
        }

        //pmahli EPA -1/24/2007
        public bool AuditPolicy(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            ValidationManager objValidManager = null;
            int lPolicyID;
            try
            {
                lPolicyID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//AuditPolicy/PolicyID").InnerText);
                // Naresh Connection Leak
                using (objValidManager = new ValidationManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    p_objXmlOut = objValidManager.RenderAuditTermList(lPolicyID);
                }
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.PopupMessage);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("EnhancePolicyAdaptor.AmendTransactionDate.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objValidManager != null)
                {
                    objValidManager = null;
                }

            }
            return true;

        }
        //pmahli EPA
        //pmahli EPA -1/23/2007
        public bool AmendTransactionDate(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {

            ValidationManager objValidManager = null;

            string sAmendTransDate;
            int lPolicyID;
            bool bValidate = false;
            try
            {
                lPolicyID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//AmendTransDate/PolicyID").InnerText);

                sAmendTransDate = p_objXmlIn.SelectSingleNode("//AmendTransDate/TransDate").InnerText;

                // Naresh Connection Leak
                using (objValidManager = new ValidationManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    bValidate = objValidManager.ValidateAmendTransactionDate(lPolicyID, sAmendTransDate);
                }

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.PopupMessage);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("EnhancePolicyAdaptor.AmendTransactionDate.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objValidManager != null)
                {
                    objValidManager = null;
                }

            }
            return bValidate;
        }

        //pmahli EPA 1/23/2007
        //pmahli EPA Range Selection 2/2/2007
        public bool RangeSelection(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            PremiumManager objPremiumManager = null;
            int iDiscountRowID;
            try
            {
                iDiscountRowID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//RangeSelection/DiscountRowId").InnerText);
                // Naresh Connection Leak
                using (objPremiumManager = new PremiumManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    p_objXmlOut = objPremiumManager.GetDiscountRange(iDiscountRowID);
                }

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.PopupMessage);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("EnhancePolicyAdaptor.RangeSelection.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objPremiumManager != null)
                {
                    objPremiumManager = null;
                }

            }
            return true;

        }
        //Cancel Policy
        public bool ValidateCancelPolicy(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            int p_iPolicyID;
            string p_sCancelDate, p_sCancelType, p_sCancelReason;
            ValidationManager objValidate = null;
            bool bValidate = false;
            try
            {

                p_iPolicyID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//CancelPolicy/PolicyID").InnerText);

                p_sCancelDate = p_objXmlIn.SelectSingleNode("//CancelPolicy/CancelDate").InnerText;
                if (p_sCancelDate == "")
                    p_sCancelDate = p_objXmlIn.SelectSingleNode("//CancelPolicy/EffDate").InnerText;

                p_sCancelType = p_objXmlIn.SelectSingleNode("//CancelPolicy//CancelType").InnerText;
                p_sCancelReason = p_objXmlIn.SelectSingleNode("//CancelPolicy//CancelReason").InnerText;

                // Naresh Connection Leak
                using (objValidate = new ValidationManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    p_objXmlOut = objValidate.GetCancelPolicyCodeData(p_objXmlIn);
                    bValidate = objValidate.ValidateCancelPolicy(p_sCancelType, p_sCancelDate, p_sCancelReason, p_iPolicyID);
                }
            }
            catch (RMAppException p_objEx)
            {
                p_objErrOut.Add(p_objEx, BusinessAdaptorErrorType.PopupMessage);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ValidateCancelPolicy_Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objValidate != null)
                {
                    objValidate = null;
                }

            }
            return bValidate;

        }
        //pmahli - 6/4/2007 Validation for Reinstate policy -Start
        public bool ValidateReinstateDate(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            int p_iPolicyID;
            string p_sReinstateDate;
            string sLapseFlag;
            string sWithoutLapseFlag;
            bool p_bWithLapse = true;
            ValidationManager objValidate = null;
            bool bValidate = false;
            try
            {
                p_iPolicyID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//ReinstatePolicy/PolicyID").InnerText);
                sLapseFlag = p_objXmlIn.SelectSingleNode("//ReinstatePolicy/ReinstateWithLapseFlag").InnerText;
                sWithoutLapseFlag = p_objXmlIn.SelectSingleNode("//ReinstatePolicy/ReinstateWithoutLapseFlag").InnerText;
                if ((sLapseFlag == "" || sLapseFlag == "true") && sWithoutLapseFlag == "")
                    p_bWithLapse = true;
                else if (sWithoutLapseFlag == "true")
                    p_bWithLapse = false;
                p_sReinstateDate = p_objXmlIn.SelectSingleNode("//ReinstatePolicy//ReinstateDate").InnerText;
                // Naresh Connection Leak
                using (objValidate = new ValidationManager(base.userLogin, base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    bValidate = objValidate.ValidateReinstateDate(p_iPolicyID, p_sReinstateDate, p_bWithLapse);
                }
                return bValidate;
            }
            catch (RMAppException p_objEx)
            {
                p_objErrOut.Add(p_objEx, BusinessAdaptorErrorType.PopupMessage);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("EnhancePolicyAdaptor.ValidateReinstateDate.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objValidate != null)
                {
                    objValidate = null;
                }
            }
            return bValidate;
        }
        //pmahli - 6/4/2007 -End

        //
        //pmahli EPA EarnedPremium  2/8/2007
        public bool EarnedPremium(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            ValidationManager objValidationManager = null;
            int iPolicyID;
            try
            {
                iPolicyID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//EarnedPremium/PolicyID").InnerText);
                // Naresh Connection Leak
                using (objValidationManager = new ValidationManager(userLogin, base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    p_objXmlOut = objValidationManager.LaunchEarnedPremium(iPolicyID);
                }
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.PopupMessage);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("EnhancePolicyAdaptor.EarnedPremium.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objValidationManager != null)
                {
                    objValidationManager = null;
                }

            }
        }
        //pmahli EPA EarnedPremium
        //pmahli EPA EarnedPremiumcalculation  2/8/2007
        public bool CalculateEarnedPremium(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            PremiumManager objPremiumManager = null;
            int iPolicyID;
            string sFromDate = string.Empty;
            string sToDate = string.Empty;

            try
            {
                iPolicyID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//EarnedPremium/PolicyID").InnerText);
                sFromDate = Conversion.GetDate(p_objXmlIn.SelectSingleNode("//EarnedPremium/EffectiveDate").InnerText);
                sToDate = Conversion.GetDate(p_objXmlIn.SelectSingleNode("//EarnedPremium/TransDate").InnerText);
                // Naresh Connection Leak
                using (objPremiumManager = new PremiumManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    p_objXmlOut = objPremiumManager.CalculateEarnedPremium(sFromDate, sToDate, iPolicyID);
                }
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.PopupMessage);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("EnhancePolicyAdaptor.CalculateEarnedPremium.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objPremiumManager != null)
                {
                    objPremiumManager = null;
                }

            }


        }
        //pmahli EPA EarnedPremiumcalculation
        public bool LoadAddForms(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            PrintManager objPolicy = null;

            try
            {
                // Naresh Connection Leak
                using (objPolicy = new PrintManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    p_objXmlOut = objPolicy.LoadAddForms();
                }
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }

            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("EnhancePolicyAdaptor.LoadAddForms.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objPolicy != null)
                {
                    objPolicy = null;
                }
            }
        }

        // Start Naresh Fetch Look up Data for Policy Billing Tab in Enhanced Policy
        #region Get Billing Code
        /// <summary>
        /// Fetch the Billing Codes from the Database
        /// </summary>
        /// <param name="p_objXmlIn"> The Input XML which has Input Parameters, In this case it will not have any </param>
        /// <param name="p_objXmlOut"> The Output XML which Contains the Billing Codes Data</param>
        /// <param name="p_objErrOut"> The Error XML which has errors if any.</param>
        /// <returns> The Execution Status, true if execution is without any error, false if there is any error</returns>
        public bool GetBillingCode(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            ValidationManager objPolicy = null;
            try
            {
                // Naresh Connection Leak
                using (objPolicy = new ValidationManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    p_objXmlOut = objPolicy.FetchBillingRules();
                }
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }

            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("EnhancePolicyAdaptor.GetBillingCode.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objPolicy != null)
                {
                    objPolicy = null;
                }
            }
        }
        #endregion Get Billing Code
        #region Get Pay Plans
        /// <summary>
        /// Fetch the Pay Plans from the Database
        /// </summary>
        /// <param name="p_objXmlIn"> The Input XML which has Input Parameters, In this case it will be LOB and State</param>
        /// <param name="p_objXmlOut"> The Output XML which Contains the Pay Plans Data corresponding to the particular state and LOB</param>
        /// <param name="p_objErrOut"> The Error XML which has errors if any.</param>
        /// <returns> The Execution Status, true if execution is without any error, false if there is any error.</returns>
        public bool GetPayPlans(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            ValidationManager objPolicy = null;
            string sLob = "";
            string sState = "";
            XmlElement objElement = null;
            try
            {
                // Naresh Connection Leak
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "PQType");
                if (objElement.InnerText != "")
                    sLob = objElement.InnerText;
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "PQState");
                if (objElement.InnerText != "")
                    sState = objElement.InnerText;
                using (objPolicy = new ValidationManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    p_objXmlOut = objPolicy.FetchPayPlans(sLob, sState);
                }
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }

            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("EnhancePolicyAdaptor.GetPayPlans.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objPolicy != null)
                {
                    objPolicy = null;
                }
            }
        }

        // Start Naresh Fetch Look up Data for Policy Billing Tab in Enhanced Policy
        #endregion

        public bool GetCancelPolicyCodeData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            ValidationManager objValManager = null;
            try
            {
                // Naresh Connection Leak
                using (objValManager = new ValidationManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    p_objXmlOut = objValManager.GetCancelPolicyCodeData(p_objXmlIn);
                }
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objValManager = null;

            }

        }

        public bool GetExposure(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            ExposureManager objExposureManager = null;
            int iExposureId = 0;
            string sSessionId = "";
            int iTransactionId = 0;
            int iPolicyId = 0;
            string sEffDate = "";
            string sExpDate = "";
            int iState = 0;
            //Anu Tennyson --- Exposure for MITS 18229 STARTS 12/23/2009
            string sFormName = string.Empty;
            //Anu Tennyson --- Exposure for MITS 18229 ENDS

            //Sumit - Start(03/16/2010) - MITS# 18229
            string sDeletedUarRowIds = string.Empty;
            string sEditRowUarSessionIds = string.Empty;
            //Sumit - End

            try
            {
                iExposureId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//Exposure/ExposureId").InnerText);
                sSessionId = p_objXmlIn.SelectSingleNode("//Exposure/SessionId").InnerText;
                iPolicyId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//Exposure/PolId").InnerText);
                iTransactionId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//Exposure/TransId").InnerText);
                sExpDate = p_objXmlIn.SelectSingleNode("//Exposure/ExpirationDate").InnerText;
                sEffDate = p_objXmlIn.SelectSingleNode("//Exposure/EffectiveDate").InnerText;
                iState = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//Exposure/State").InnerText);
                //Anu Tennyson --- Exposure for MITS 18229 STARTS 12/23/2009
                if (p_objXmlIn.SelectSingleNode("//Exposure/FormName") != null)
                {
                    sFormName = p_objXmlIn.SelectSingleNode("//Exposure/FormName").InnerText;
                }
                //Anu Tennyson --- Exposure for MITS 18229 ENDS

                //Sumit - Start(03/16/2010) - MITS# 18229
                if (p_objXmlIn.SelectSingleNode("//Exposure/DeletedUarRowIds") != null)
                {
                    sDeletedUarRowIds = p_objXmlIn.SelectSingleNode("//Exposure/DeletedUarRowIds").InnerText;
                }

                if (p_objXmlIn.SelectSingleNode("//Exposure/EditRowUarSessionIds") != null)
                {
                    sEditRowUarSessionIds = p_objXmlIn.SelectSingleNode("//Exposure/EditRowUarSessionIds").InnerText;
                }
                //Sumit - End
                

                // Naresh Connection Leak
                using (objExposureManager = new ExposureManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    //Anu Tennyson --- Exposure for MITS 18229 STARTS 12/23/2009
                    //p_objXmlOut = objExposureManager.GetExposure(base.GetSessionObject(), iExposureId, sSessionId, iPolicyId, iTransactionId, sEffDate, sExpDate, iState);

                    //Sumit - Start(03/16/2010) - MITS# 18229 - Added DeletedUarRowIds , EditRowUarSessionIds
                    //p_objXmlOut = objExposureManager.GetExposure(base.GetSessionObject(), iExposureId, sSessionId, iPolicyId, iTransactionId, sEffDate, sExpDate, iState, sFormName);
                    p_objXmlOut = objExposureManager.GetExposure(base.GetSessionObject(), iExposureId, sSessionId, iPolicyId, iTransactionId, sEffDate, sExpDate, iState, sFormName, sDeletedUarRowIds, sEditRowUarSessionIds);
                    //Sumit - End
                    //Anu Tennyson --- Exposure for MITS 18229 ENDS
                }
                return true;

            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("TODO", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objExposureManager != null)
                {
                    objExposureManager = null;
                }

            }


        }

        public bool SetExposure(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            ExposureManager objExposureManager = null;

            try
            {
                // Naresh Connection Leak
                using (objExposureManager = new ExposureManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    p_objXmlOut = objExposureManager.SetExposure(base.GetSessionObject(), p_objXmlIn);
                }
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.PopupMessage);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("TODO", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objExposureManager != null)
                {
                    objExposureManager = null;
                }

            }


        }

        //Tushar:MITS#18229
        private bool UpdateUARDetailList(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            ExposureManager objExposureManager = null;

            try
            {
                using (objExposureManager = new ExposureManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    p_objXmlOut = objExposureManager.RenderVehicleUARDetail(base.GetSessionObject(), p_objXmlIn);
                }
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.PopupMessage);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("EnhancePolicyAdaptor.UpdateUARDetailList.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objExposureManager != null)
                {
                    objExposureManager = null;
                }
            }
        }

        /// <summary>
        /// Author : Sumit Kumar
        /// Date: 04/14/2010
        /// MITS#: 18229
        /// Description: Method to update property data.
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        private bool UpdatePropertyUARDetailList(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            ExposureManager objExposureManager = null;

            try
            {
                using (objExposureManager = new ExposureManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    p_objXmlOut = objExposureManager.RenderPropertyUARDetail(base.GetSessionObject(), p_objXmlIn);
                }
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.PopupMessage);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("EnhancePolicyAdaptor.UpdateUARDetailList.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objExposureManager != null)
                {
                    objExposureManager = null;
                }
            }
        }
        /// <summary>
        /// Author : Sumit Kumar
        /// Date: 04/28/2010
        /// MITS#: 20483
        /// Description: Method to update property schedule data.
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        private bool UpdateScheduleList(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            ExposureManager objExposureManager = null;

            try
            {
                using (objExposureManager = new ExposureManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    p_objXmlOut = objExposureManager.RenderScheduleList(base.GetSessionObject(), p_objXmlIn);
                }
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.PopupMessage);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("EnhancePolicyAdaptor.UpdateScheduleList.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objExposureManager != null)
                {
                    objExposureManager = null;
                }
            }
        }
        //Anu Tennyson for MITS 18229 STARTS 12/22/2009
        /// <summary>
        /// Function to calculate amount when the deductible button is disable.
        /// </summary>
        /// <param name="objXmlIn">Input XML from UI layer.</param>
        /// <param name="objXmlOut">Output XML containing the result(Deductible).</param>
        /// <param name="objErrOut">For error handling.</param>
        /// <returns>True or False depending on the success of the called function.</returns>
        public bool CalculateDeductible(XmlDocument objXmlIn, ref XmlDocument objXmlOut, ref BusinessAdaptorErrors objErrOut)
        {                     
            try
            {
                using (CoverageManager objCoverageManager = new CoverageManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.DSNID, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    objXmlOut = objCoverageManager.CalculateDeductible(base.GetSessionObject(), objXmlIn);
                }
                return true;
            }
            catch (Exception objException)
            {
                objErrOut.Add(objException, Globalization.GetString("EnhancePolicyAdaptor.CalculateDeductible.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            
        }
        //Anu Tennyson for MITS 18229 ENDS 


        public bool GetRate(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            ExposureManager objExposureManager = null;
            int iExposureId = 0;
            string sSessionId = "";
            int iPolicyId = 0;
            double dblPrevPrPrem = 0;
            double dblPrevFullPrem = 0;
            string sTransactionType = "";
            try
            {
                // Naresh Connection Leak
                using (objExposureManager = new ExposureManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    p_objXmlOut = objExposureManager.CalculateRate(p_objXmlIn);
                }
                return true;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("TODO", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objExposureManager != null)
                {
                    objExposureManager = null;
                }

            }


        }
        public bool GetCoverages(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            CoverageManager objCoverageManager = null;
            int iCoveragesID = 0;
            string sSessionId = "";
            int iPolicyId = 0;
            int iTransactionId = 0;
            string sEffDate = "";
            string sExpDate = "";
            //Anu Tennyson for MITS 18229 Starts 12/23/2009 

            int iState = 0;
            string sLob = string.Empty;
            string sOrg = string.Empty;
            string sMode = string.Empty;
            //Anu Tennyson for MITS 18229 Ends
            try
            {
                iCoveragesID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//Coverages/CoveragesId").InnerText);
                sSessionId = p_objXmlIn.SelectSingleNode("//Coverages/SessionId").InnerText;
                iPolicyId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//Coverages/PolId").InnerText);
                iTransactionId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//Coverages/TransId").InnerText);
                sEffDate = p_objXmlIn.SelectSingleNode("//Coverages/EffectiveDate").InnerText;
                sExpDate = p_objXmlIn.SelectSingleNode("//Coverages/ExpirationDate").InnerText;
               //Anu Tennyson for MITS 18229 Starts 12/23/2009
                iState = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//Coverages/State").InnerText);
                sLob = p_objXmlIn.SelectSingleNode("//Coverages/LOB").InnerText;
                sOrg = p_objXmlIn.SelectSingleNode("//Coverages/Org").InnerText;
                sMode = p_objXmlIn.SelectSingleNode("//Coverages/Mode").InnerText;
                //Anu Tennyson for MITS 18229 Ends
                // Naresh Connection Leak
                using (objCoverageManager = new CoverageManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.DSNID, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    //Anu Tennyson for MITS 18229 Starts 12/23/2009 - Commented
                    //p_objXmlOut = objCoverageManager.GetCoverages(base.GetSessionObject(), iCoveragesID, sSessionId, iPolicyId, iTransactionId,
                    //            sEffDate, sExpDate);
                    //Anu Tennyson for MITS 18229 Ends 12/23/2009
                    p_objXmlOut = objCoverageManager.GetCoverages(base.GetSessionObject(), iCoveragesID, sSessionId, iPolicyId, iTransactionId,
                                sEffDate, sExpDate, iState, sLob, sOrg, sMode);
                }
                return true;

            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("TODO", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objCoverageManager != null)
                {
                    objCoverageManager = null;
                }

            }


        }
        public bool SetCoverages(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            CoverageManager objCoverageManager = null;

            try
            {
                // Naresh Connection Leak
                using (objCoverageManager = new CoverageManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.DSNID, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    p_objXmlOut = objCoverageManager.SetCoverages(base.GetSessionObject(), p_objXmlIn);
                }
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.PopupMessage);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("TODO", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objCoverageManager != null)
                {
                    objCoverageManager = null;
                }

            }


        }

        //Anu Tennyson for MITS 18229 STARTS 12/22/2009
        /// <summary>
        /// Function to check whether to show the deductible button or not.
        /// </summary>
        /// <param name="objXmlIn">Input XML from the UI Layer.</param>
        /// <param name="objXmlOut">Output XML containing the result(Deductible).</param>
        /// <param name="objErrOut">For error handling.</param>
        /// <returns>True or False depending on the success of the called function.</returns>
        public bool CheckDeductiblebtn(XmlDocument objXmlIn, ref XmlDocument objXmlOut, ref BusinessAdaptorErrors objErrOut)
        {         
            try
            {
                using (CoverageManager objCoverageManager = new CoverageManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.DSNID, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    //objXmlOut = objCoverageManager.CheckDeductiblebtn(objXmlIn);
                    objXmlOut = objCoverageManager.CheckDeductiblebtn(base.GetSessionObject(), objXmlIn);
                }
              
            }
            catch (RMAppException objException)
            {
                objErrOut.Add(objException, BusinessAdaptorErrorType.PopupMessage);
                return false;
            }
            catch (Exception objException)
            {
                objErrOut.Add(objException, Globalization.GetString("EnhancePolicyAdaptor.CheckDeductiblebtn.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            return true;
        }

        //Anu Tennyson for MITS 18229 12/23/2009 
        /// <summary>
        /// This function is called when deductible button is enabled and it will calculate the deductible.
        /// </summary>
        /// <param name="objXmlIn">Input XML from UI layer.</param>
        /// <param name="objXmlOut">Output XML containing the result(Deductible).</param>
        /// <param name="objErrOut">For error handling.</param>
        /// <returns>True or False depending on the success of the called function.</returns>
        public bool GetDeductible(XmlDocument objXmlIn, ref XmlDocument objXmlOut, ref BusinessAdaptorErrors objErrOut)
        {
            try
            {
                using (CoverageManager objCoverageManager = new CoverageManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.DSNID, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    objXmlOut = objCoverageManager.GetDeductible(base.GetSessionObject(), objXmlIn);
                }

            }
            catch (RMAppException objException)
            {
                objErrOut.Add(objException, BusinessAdaptorErrorType.PopupMessage);
                return false;
            }
            catch (Exception objException)
            {
                objErrOut.Add(objException, Globalization.GetString("EnhancePolicyAdaptor.GetDeductible.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            return true;

        }

        //Anu Tennyson for MITS 18229 ENDS

        //Anu Tennyson for MITS 18229 1/20/2010 STARTS--exposure - For Vehicle UAR 
        /// <summary>
        /// Author : Anu Tennyson
        /// Date: 1/20/2010
        /// MITS#: 18229
        /// Description: Method to  store Vehicle/Property Data in session. 
        /// </summary>
        /// <param name="objXmlIn">Input XML.</param>
        /// <param name="objXmlOut">Output XML.</param>
        /// <param name="objErrOut">Error XML.</param>
        /// <returns>Returns true or false depending upon the success.</returns>
        public bool PopulateSearchVehUAR(XmlDocument objXmlIn, ref XmlDocument objXmlOut, ref BusinessAdaptorErrors objErrOut)
        {
            try
            {
                using (ExposureManager objExposureManager = new ExposureManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    objXmlOut = objExposureManager.PopulateSearchVehUAR(objXmlIn);
                }

            }
            catch (RMAppException objException)
            {
                objErrOut.Add(objException, BusinessAdaptorErrorType.PopupMessage);
                return false;
            }
            catch (Exception objException)
            {
                objErrOut.Add(objException, Globalization.GetString("EnhancePolicyAdaptor.PopulateSearchVehUAR.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                //TODO global.resx
                return false;
            }
            
            return true;


        }
        //Anu Tennyson for MITS 18229 1/20/2010 ENDS--exposure - For Vehicle UAR 

        /// <summary>
        /// Author : Sumit Kumar
        /// Date: 03/16/2010
        /// MITS#: 18229
        /// Description: Method to  store Vehicle/Property Data in session.
        /// </summary>
        /// <param name="objXmlIn">Input XML.</param>
        /// <param name="objXmlOut">Output XML.</param>
        /// <param name="objErrOut">Error XML.</param>
        /// <returns>Returns true or false depending upon the success.</returns>
        public bool SetUAR(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            ExposureManager objExposureManager = null;

            try
            {
                using (objExposureManager = new ExposureManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    p_objXmlOut = objExposureManager.SetUAR(base.GetSessionObject(), p_objXmlIn);
                }
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.PopupMessage);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("EnhancePolicyAdaptor.SetUAR.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objExposureManager != null)
                {
                    objExposureManager = null;
                }

            }
        }
        /// <summary>
        /// Author : Sumit Kumar
        /// Date: 04/28/2010
        /// MITS#: 20483
        /// Description: Method to  store Property Schedule Data in session.
        /// </summary>
        /// <param name="objXmlIn">Input XML.</param>
        /// <param name="objXmlOut">Output XML.</param>
        /// <param name="objErrOut">Error XML.</param>
        /// <returns>Returns true or false depending upon the success.</returns>
        public bool SetSchedule(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            ExposureManager objExposureManager = null;

            try
            {
                using (objExposureManager = new ExposureManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    p_objXmlOut = objExposureManager.SetSchedule(base.GetSessionObject(), p_objXmlIn);
                }
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.PopupMessage);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("EnhancePolicyAdaptor.SetSchedule.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objExposureManager != null)
                {
                    objExposureManager = null;
                }

            }
        }
        /// <summary>
        /// Author : Sumit Kumar
        /// Date: 04/14/2010
        /// MITS#: 18229
        /// Description: Method to populate property data.
        /// </summary>
        /// <param name="objXmlIn">Input XML.</param>
        /// <param name="objXmlOut">Output XML.</param>
        /// <param name="objErrOut">Error XML.</param>
        /// <returns>Returns true or false depending upon the success.</returns>
        public bool PopulateSearchPropUAR(XmlDocument objXmlIn, ref XmlDocument objXmlOut, ref BusinessAdaptorErrors objErrOut)
        {
            try
            {
                using (ExposureManager objExposureManager = new ExposureManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    objXmlOut = objExposureManager.PopulateSearchPropUar(objXmlIn, base.GetSessionObject());
                }
            }
            catch (RMAppException objException)
            {
                objErrOut.Add(objException, BusinessAdaptorErrorType.PopupMessage);
                return false;
            }
            catch (Exception objException)
            {
                objErrOut.Add(objException, Globalization.GetString("EnhancePolicyAdaptor.PopulateSearchPropUAR.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            return true;
        }
        /// <summary>
        /// Author : Sumit Kumar
        /// Date: 05/03/2010
        /// MITS#: 20483
        /// Description: Method to fetch property schedule data.
        /// </summary>
        //// <param name="objXmlIn">Input XML.</param>
        /// <param name="objXmlOut">Output XML.</param>
        /// <param name="objErrOut">Error XML.</param>
        /// <returns>Returns true or false depending upon the success.</returns>
        public bool GetSchedule(XmlDocument objXmlIn, ref XmlDocument objXmlOut, ref BusinessAdaptorErrors objErrOut)
        {
            try
            {
                using (ExposureManager objExposureManager = new ExposureManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    objXmlOut = objExposureManager.GetSchedule(objXmlIn, base.GetSessionObject());
                }
            }
            catch (RMAppException objException)
            {
                objErrOut.Add(objException, BusinessAdaptorErrorType.PopupMessage);
                return false;
            }
            catch (Exception objException)
            {
                objErrOut.Add(objException, Globalization.GetString("EnhancePolicyAdaptor.GetSchedule.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            return true;
        }

        /// <summary>
        /// This function is for editing the Modifier range during premium Calculation
        /// </summary>
        /// <param name="objXmlIn">Input XML.</param>
        /// <param name="objXmlOut">Output XML.</param>
        /// <param name="objErrOut">Error XML.</param>
        /// <returns>Returns true or false depending upon the success.</returns>
        public bool ModifierRangeSelection(XmlDocument objXmlIn, ref XmlDocument objXmlOut, ref BusinessAdaptorErrors objErrOut)
        {
            PremiumManager objPremiumManager = null;
            int iExpRateRowID;
            string sExpRateRowID = string.Empty;
            bool bIsSuccess = false;
            try
            {
                if (objXmlIn.SelectSingleNode("//ModifierRangeSelection/ExpRateRowID") != null)
                {
                    sExpRateRowID = objXmlIn.SelectSingleNode("//ModifierRangeSelection/ExpRateRowID").InnerText;
                }
                iExpRateRowID = Conversion.CastToType<int>(sExpRateRowID, out bIsSuccess);
                
                using (objPremiumManager = new PremiumManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    objXmlOut = objPremiumManager.GetModifierRange(iExpRateRowID);
                }

            }
            catch (RMAppException p_objException)
            {
                objErrOut.Add(p_objException, BusinessAdaptorErrorType.PopupMessage);
                return false;
            }
            catch (Exception p_objException)
            {
                objErrOut.Add(p_objException, Globalization.GetString("EnhancePolicyAdaptor.ModifierRangeSelection.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objPremiumManager != null)
                {
                    objPremiumManager = null;
                }

            }
            return true;

        }
        //Anu Tennyson for MITS 18229 ENDS
        
        //Anu Tennyson for MITS 18229 STARTS
        /// <summary>
        /// This function will bring all the UAR Details added to a UAR.
        /// </summary>
        /// <param name="objXmlIn">Input XML which contains the information(Session Id and the Start Point) for finding the UAR Details.</param>
        /// <param name="objXmlOut">Output XML which will take the UARs to the UI layer</param>
        /// <param name="objErrOut">XML which will contain the Error.</param>
        /// <returns>Returns true or false depending upon the success.</returns>
        public bool GetUARDetailsAdded(XmlDocument objXmlIn, ref XmlDocument objXmlOut, ref BusinessAdaptorErrors objErrOut)
        {
            try
            {
                using (CoverageManager objCoverageManager = new CoverageManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.DSNID, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    objXmlOut = objCoverageManager.GetUARDetailsAdded(base.GetSessionObject(), objXmlIn);
                }
            }
            catch (RMAppException objException)
            {
                objErrOut.Add(objException, BusinessAdaptorErrorType.PopupMessage);
                return false;
            }
            catch (Exception objException)
            {
                objErrOut.Add(objException, Globalization.GetString("EnhancePolicyAdaptor.GetUARDetailsAdded.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            return true;
        }
        //Anu Tennyson for MITS 18229 ENDS
}
}
