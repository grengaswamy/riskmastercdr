using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.Application.LookupData;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: LookupDataAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 08/24/2005
	///* $Author	: Mihika Agrawal
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************

	/// <summary>
	/// This class is used to call the application layer component for Look Up Data.
	/// </summary>
	public class LookupDataAdaptor: BusinessAdaptorBase
	{	private const int RMB_LOOKUP =7000000;
		private const int CLAIM_LOOKUP = 1;
		private const int EVENT_LOOKUP = 2;
		private const int EMPLOYEE_LOOKUP = 3;
		private const int ENTITY_LOOKUP = 4;
		private const int VEHICLE_LOOKUP = 5;
		private const int POLICY_LOOKUP = 6;
		private const int PAYMENT_LOOKUP = 7;

		private const int PATIENT_LOOKUP = 8;
		private const int PHYSICIAN_LOOKUP = 9;
		private const int MED_STAFF_LOOKUP = 10;
		private const int DIS_PLAN_LOOKUP =11;
		
		private const int AT_LOOKUP= 20;

		private const int BANK_ACCT_LOOKUP = 30;
		private const int BANK_ACCT_DEP_LOOKUP = 31;
        private const int PROPERTY_LOOKUP = 12; //Mridul 11/23/09 MITS:18291
        //Bharani - MITS : 31858 - Change 1 - Start
        private const int RMB_VIEW = 1;
        string sLOB = string.Empty;
        //Bharani - MITS : 31858 - Change 1 - End

        public LookupDataAdaptor()
        {
        }


		#region Public Methods
	
		/// <summary>
		/// Wrapper to Application layer function GetLookUpData()
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GetLookupData(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Lookup objLookup = null;
			string sFormName = string.Empty;
            string sAdjusterRowId = string.Empty; ////Bharani - MITS : 31858 Change 2
			try
			{                
				//BSB 12.2.2005 Perf and LicenseCountExceeded Fix.
				// If we pass in the existing UserLogin object we will be significantly faster
				// since login is an expensive operation... (That's why we cache the UserLogin object to begin with.)
				if (p_objXmlIn.SelectSingleNode("//Data").Attributes.GetNamedItem("form") != null)
				{		
                    sFormName = p_objXmlIn.SelectSingleNode("//Data").Attributes.GetNamedItem("form").InnerText;

                    //Bharani - MITS : 31858 Change 3 - Start
                    if (sFormName.Equals("adjusterdatedtext"))
                    {
                        sAdjusterRowId = p_objXmlIn.SelectSingleNode("//Data").Attributes.GetNamedItem("ParentID").InnerText;
                        DataModelFactory m_objDataModelFactory = new DataModelFactory(m_userLogin, base.ClientId);
                        ClaimAdjuster oAdjuster = (ClaimAdjuster)m_objDataModelFactory.GetDataModelObject("ClaimAdjuster", false);
                        oAdjuster.MoveTo(Convert.ToInt32(sAdjusterRowId));
                        Claim objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                        objClaim.MoveTo(oAdjuster.ClaimId);
                        sLOB = objClaim.LineOfBusCode.ToString();
                    }
                    //Bharani - MITS : 31858 Change 3 - End
					ApplySecurity(sFormName);
				}

				using(objLookup = new Lookup(userLogin, base.ClientId))
				{	//objLookup = new Lookup(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password);
					p_objXmlOut = objLookup.GetLookUpData(p_objXmlIn);
				}
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("LookupDataAdaptor.GetLookupData.Error", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
                //Modified by Shivendu
                if (objLookup != null)
                    objLookup.Dispose();
                
			}
			return true;
		}
		#endregion
		private void ApplySecurity(string p_sFormName)
		{	if (!userLogin.IsAllowedEx(RMB_LOOKUP))
				throw new PermissionViolationException(RMPermissions.RMO_ACCESS,RMB_LOOKUP);
			p_sFormName=p_sFormName.ToLower() ;
			switch (p_sFormName)
			{
				case "event":
					if (!userLogin.IsAllowedEx(RMB_LOOKUP,EVENT_LOOKUP))
						throw new PermissionViolationException(RMPermissions.RMO_EVENT_LOOKUP,RMB_LOOKUP);
					break;
				case "eventdatedtext":
					break;
				case "claimgc":
				case "claimva":
				case "claimwc":
				case "claimdi":
					if (!userLogin.IsAllowedEx(RMB_LOOKUP,CLAIM_LOOKUP))
						throw new PermissionViolationException(RMPermissions.RMO_CLAIM_LOOKUP,RMB_LOOKUP);
					break;
				case "adjuster":

					break;
				case "adjusterdatedtext":
                    //Bharani - MITS : 31858 - Change 4 - Start
                    switch (sLOB)
                    {
                        case "241":
                            if (!userLogin.IsAllowedEx(RMPermissions.RMO_GC_ADJUSTERDATEDTEXT, RMB_VIEW))
                                throw new PermissionViolationException(RMPermissions.RMO_VIEW, RMPermissions.RMO_GC_ADJUSTERDATEDTEXT);
                            break;
                        case "242":
                            if (!userLogin.IsAllowedEx(RMPermissions.RMO_VA_ADJUSTERDATEDTEXT, RMB_VIEW))
                                throw new PermissionViolationException(RMPermissions.RMO_VIEW, RMPermissions.RMO_VA_ADJUSTERDATEDTEXT);
                            break;
                        case "243":
                            if (!userLogin.IsAllowedEx(RMPermissions.RMO_WC_ADJUSTERDATEDTEXT, RMB_VIEW))
                                throw new PermissionViolationException(RMPermissions.RMO_VIEW, RMPermissions.RMO_VA_ADJUSTERDATEDTEXT);
                            break;
                        case "844":
                            if (!userLogin.IsAllowedEx(RMPermissions.RMO_DI_ADJUSTERDATEDTEXT, RMB_VIEW))
                                throw new PermissionViolationException(RMPermissions.RMO_VIEW, RMPermissions.RMO_VA_ADJUSTERDATEDTEXT);
                            break;
                        case "845":
                            if (!userLogin.IsAllowedEx(RMPermissions.RMO_PC_ADJUSTERDATEDTEXT, RMB_VIEW))
                                throw new PermissionViolationException(RMPermissions.RMO_VIEW, RMPermissions.RMO_VA_ADJUSTERDATEDTEXT);
                            break;
                    }
                    //Bharani - MITS : 31858 - Change 4 - End
					break;
				case "litigation":

					break;
				case "expert":

					break;
				case "claimant":

					break;
				case "defendant":

					break;
				case "unit":

					break;
				case "policy":
					if (!userLogin.IsAllowedEx(RMB_LOOKUP,POLICY_LOOKUP))
						throw new PermissionViolationException(RMPermissions.RMO_POLICY_LOOKUP,RMB_LOOKUP);
					break;
				case "policycoverage":

					break;
				case "policymco":
					break;
				case "vehicle":
					if (!userLogin.IsAllowedEx(RMB_LOOKUP,VEHICLE_LOOKUP))
						throw new PermissionViolationException(RMPermissions.RMO_VEHICLE_LOOKUP,RMB_LOOKUP);
					break;
				case "vehicleinspections":

					break;
				case "employee":
					if (!userLogin.IsAllowedEx(RMB_LOOKUP,EMPLOYEE_LOOKUP))
						throw new PermissionViolationException(RMPermissions.RMO_EMPLOYEE_LOOKUP,RMB_LOOKUP);
					break;
				case "violation":

					break;
				case "dependent":

					break;

				case "people":
				case "entitymaint":
					if (!userLogin.IsAllowedEx(RMB_LOOKUP,ENTITY_LOOKUP ))
						throw new PermissionViolationException(RMPermissions.RMO_ENTITY_LOOKUP,RMB_LOOKUP);
					break;
				case "patient":
					if (!userLogin.IsAllowedEx(RMB_LOOKUP,PATIENT_LOOKUP ))
						throw new PermissionViolationException(RMPermissions.RMO_PATIENT_LOOKUP,RMB_LOOKUP);
					break;
				case "patientprocedure":

					break;
				case "physician":
					if (!userLogin.IsAllowedEx(RMB_LOOKUP,PHYSICIAN_LOOKUP ))
						throw new PermissionViolationException(RMPermissions.RMO_PHYSICIAN_LOOKUP,RMB_LOOKUP);
					break;
				case "physicianprivilege":

					break;
				case "physiciancertification":

					break;
				case "physicianeducation":

					break;
				case "physicianprevhospital":

					break;
				case "staff":
					if (!userLogin.IsAllowedEx(RMB_LOOKUP,MED_STAFF_LOOKUP))
						throw new PermissionViolationException(RMPermissions.RMO_MED_STAFF_LOOKUP,RMB_LOOKUP);
					break;
				case "staffprivilege":

					break;
				case "staffcertification":

					break;
				case "bankaccount":
					if (!userLogin.IsAllowedEx(RMB_LOOKUP,BANK_ACCT_LOOKUP))
						throw new PermissionViolationException(RMPermissions.RMO_BANK_ACCT_LOOKUP,RMB_LOOKUP);
					break;
				case "bankaccountsub":

					break;
				case "deposit":
					if (!userLogin.IsAllowedEx(RMB_LOOKUP,BANK_ACCT_DEP_LOOKUP))
						throw new PermissionViolationException(RMPermissions.RMO_BANK_ACCT_DEP_LOOKUP,RMB_LOOKUP);
					break;
				case "piprivilege":

					break;
				case "picertification":

					break;
				case "pieducation":

					break;
				case "piprevhospital":

					break;
				case "pidependent":

					break;
				case "pirestriction":

					break;
				case "piworkloss":

					break;
				case "piqprocedure":

					break;
				case "piprocedure":

					break;
				case "pimedstaffprivilege":

					break;
				case "pimedstaffcertification":

					break;
				case "autoclaimchecks":

					break;
				case "funds":
					if (!userLogin.IsAllowedEx(RMB_LOOKUP,PAYMENT_LOOKUP  ))
						throw new PermissionViolationException(RMPermissions.RMO_PAYMENT_LOOKUP,RMB_LOOKUP);
					break;
				case "plan":
					if (!userLogin.IsAllowedEx(RMB_LOOKUP,DIS_PLAN_LOOKUP  ))
						throw new PermissionViolationException(RMPermissions.RMO_DIS_PLAN_LOOKUP,RMB_LOOKUP);
					break;
                //Mridul 11/23/09 MITS:18291
                case "property":
                    //This is not needed because MDI hides the lookup button in absence of permission. Writing commented code to keep in sync with the above cases.
                    //If you uncomment then you need to define RMPermissions.RMO_PROPERTY_LOOKUP as well.
                    //if (!userLogin.IsAllowedEx(RMB_LOOKUP, PROPERTY_LOOKUP))
                    //    throw new PermissionViolationException(RMPermissions.RMO_PROPERTY_LOOKUP, RMB_LOOKUP);
                    break;
				default :
					if (p_sFormName.StartsWith("admintracking"))
					{
						if (!userLogin.IsAllowedEx(RMB_LOOKUP,AT_LOOKUP ))
							throw new PermissionViolationException(RMPermissions.RMO_AT_LOOKUP,RMB_LOOKUP);
					}
					break;
			} // end of switch
		}
        #region Show Unit Coverage summary MITS 30700 spahariya
        public bool GetUnitCoverageSummary(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            Lookup objLookup = null;
            //MITS 12091 Raman Bhatia 
            //If claimid is present then filter batchlist by claimid 
            XmlElement objElem = null;
            int iClaimID = 0;
            string sFormName = string.Empty;
            try
            {
                
                objElem = (XmlElement)p_objXmlIn.SelectSingleNode("//ClaimID");
                if (objElem != null)
                {
                    iClaimID = Conversion.ConvertStrToInteger(objElem.InnerText);
                }
                objElem = (XmlElement)p_objXmlIn.SelectSingleNode("//FormName");
                if (objElem != null)
                {
                    sFormName = objElem.InnerText;
                }
                using(objLookup = new Lookup(userLogin, base.ClientId))
				{
                    p_objXmlOut = objLookup.GetUnitCovLst(iClaimID, sFormName);
				}
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }

            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("FundManagementAdaptor.GetAllAutoChecksBatch.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objLookup != null)
                {
                    objLookup.Dispose();
                    objLookup = null;
                }
            }
        }

        #endregion 
	}
}
