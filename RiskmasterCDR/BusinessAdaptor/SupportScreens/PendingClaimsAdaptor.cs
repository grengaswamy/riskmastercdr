﻿using System;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.SupportScreens;

namespace Riskmaster.BusinessAdaptor.SupportScreensAdaptor
{

    class PendingClaimsAdaptor : BusinessAdaptorBase
    {

        #region Constructor
        public PendingClaimsAdaptor() { }
        #endregion

        #region Public methods
        /// <summary>
        ///		This function gets the list of person involved
        /// </summary>
        public bool GetPendingClaims(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            PendingClaims objPendingClaims = null;
            try
            {
                string sGridId = "";
                objPendingClaims = new PendingClaims(connectionString, userID, loginName, base.ClientId, base.LangCode);
                p_objXmlOut = objPendingClaims.GetPendingClaimRecrods(p_objXmlIn, base.DSNID, "gridPenClaims");

                return true;
            }
            catch (InvalidValueException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("PendingClaims.GetPendingClaims.Error", base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa
                return false;
            }
        }


        public bool GetDDItems(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            PendingClaims objPendingClaims = null;
            try
            {
                objPendingClaims = new PendingClaims(connectionString, userID, loginName, base.ClientId, base.LangCode);
                p_objXmlOut = objPendingClaims.GetDDItems(p_objXmlIn);

                return true;
            }
            catch (InvalidValueException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("PendingClaims.GetDDList.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
        }


        /// <summary>
        /// This function is used to get default user pref for the grid
        /// </summary>
        public bool RestoreDefaults(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            string sPageName = "";
            PendingClaims objPendingClaims = null;
            string sGridId = "";
            string sUserPrefString = "";
            XmlElement oUserPrefElement;
            try
            {
                objPendingClaims = new PendingClaims(connectionString, userID, loginName, base.ClientId, base.LangCode);
                if (p_objXmlIn.SelectSingleNode("//PageName") != null)
                    sPageName = p_objXmlIn.SelectSingleNode("//PageName").InnerText.Trim();
                if (p_objXmlIn.SelectSingleNode("//GridId") != null)
                    sGridId = p_objXmlIn.SelectSingleNode("//GridId").InnerText.Trim();
                sUserPrefString = objPendingClaims.RestoreDefaultUserPref(m_userLogin.UserId, sPageName, m_userLogin.DatabaseId, sGridId);
                oUserPrefElement = p_objXmlOut.CreateElement("UserPref");
                oUserPrefElement.InnerText = sUserPrefString;
                p_objXmlOut.AppendChild(oUserPrefElement);
            }
            catch (Exception p_objException)
            {
                p_objErr.Add(p_objException, Globalization.GetString("GridPreference.RestoreDefaults.Err", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            return true;
        }
        #endregion
    }
}
