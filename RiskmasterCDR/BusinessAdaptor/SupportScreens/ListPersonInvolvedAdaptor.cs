using System;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.SupportScreens;

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File				: ListPersonInvolvedAdaptor.cs 
	///* $Revision			: 1.0.0.0 
	///* $Creation Date		: 21-Dec-2005
	///* $Author			: Mohit Yadav
	///***************************************************************	
	/// <summary>
	/// Summary description for ListPersonInvolvedAdaptor.
	/// </summary>
	public class ListPersonInvolvedAdaptor: BusinessAdaptorBase
	{
        private string sLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").Split('|')[0].ToString();
		#region Constructor
		public ListPersonInvolvedAdaptor(){}
		#endregion

		#region Public methods
		/// <summary>
		///		This function gets the list of person involved
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GetPIList(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			bool bReturnValue=false;
			ListPersonInvolved objListPersInv = null;
			string sDocumentsXml = string.Empty;
			XmlElement objRootElement=null;			//used for creating the Output Xml
			XmlElement objElement=null;				//used for parsing the input xml
			int iClaimId = 0;
			int iEventId = 0;
			
			try
			{
				objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//GetPIList/ClaimId");

				if (objElement!=null)
					iClaimId = Conversion.ConvertStrToInteger(objElement.InnerText);
				else  // see if EventId is there if claimid isn't (the ClaimList page uses EventId)
					objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//GetPIList/EventId");

				if(objElement!=null)
					iEventId = Conversion.ConvertStrToInteger(objElement.InnerText);
				else
				{ 
					bReturnValue = false;
					p_objErrOut.Add("ListPersonInvolvedAdaptor.GetPIList.MissingInputParameter",Globalization.GetString("ListPersonInvolvedAdaptor.GetPIList.MissingClaimId",base.ClientId,sLangCode),BusinessAdaptorErrorType.Error);
					return bReturnValue;
				}
					
				objListPersInv = new ListPersonInvolved(userLogin.objRiskmasterDatabase.DataSourceName,userLogin.LoginName,userLogin.Password,userLogin.UserId, userLogin.GroupId,base.ClientId);
				objListPersInv.GetPIList(iClaimId, iEventId, out sDocumentsXml); 

				//objRootElement = p_objXmlOut.CreateElement("GetPIListResult");  
				
				//objRootElement.InnerXml = sDocumentsXml;
				p_objXmlOut.InnerXml = sDocumentsXml;
				return true;
			}
			catch(InvalidValueException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("ListPersonInvolvedAdaptor.GetPIList.Error", base.ClientId, sLangCode), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
                if (objListPersInv != null)
                    objListPersInv.Dispose();


			}
		}

        /// <summary>
        ///		This function gets the list of person involved
        /// </summary>
        /// <param name="p_objXmlIn">Input parameters as xml</param>
        /// <param name="p_objXmlOut">Result as Output xml</param>
        /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
        /// <returns>Success -True or Failure -false in execution of the function</returns>
        public bool GetPILevel(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            bool bReturnValue = false;
            ListPersonInvolved objListPersInv = null;
            string sDocumentsXml = string.Empty;
            XmlElement objRootElement = null;			//used for creating the Output Xml
            XmlElement objElement = null;				//used for parsing the input xml
            int iClaimId = 0;
            int iEventId = 0;
            string sPageID = string.Empty;
            try
            {

                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//PageId");
                if (objElement != null)
                    sPageID = objElement.InnerText;

                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//GetPILevel/EventId");

                if (objElement != null)
                    iEventId = Conversion.ConvertStrToInteger(objElement.InnerText);
                else
                {
                    bReturnValue = false;
                    p_objErrOut.Add("ListPersonInvolvedAdaptor.GetPILevel.MissingInputParameter", Globalization.GetString("ListPersonInvolvedAdaptor.GetPILevel.MissingClaimId", base.ClientId, sLangCode), BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                objListPersInv = new ListPersonInvolved(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, userLogin.UserId, userLogin.GroupId, base.ClientId, userLogin.objUser.NlsCode.ToString()); //ttumula2 on 11 Feb 2015 for RMA-6959
                sLangCode = userLogin.objUser.NlsCode.ToString();
                objListPersInv.GetPILevel(iEventId, out sDocumentsXml, sPageID);

                //objRootElement = p_objXmlOut.CreateElement("GetPIListResult");  

                //objRootElement.InnerXml = sDocumentsXml;
                p_objXmlOut.InnerXml = sDocumentsXml;
                return true;
            }
            catch (InvalidValueException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ListPersonInvolvedAdaptor.GetPIList.Error", base.ClientId, sLangCode), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objListPersInv != null)
                    objListPersInv.Dispose();


            }
        }

		#endregion

	}
}
