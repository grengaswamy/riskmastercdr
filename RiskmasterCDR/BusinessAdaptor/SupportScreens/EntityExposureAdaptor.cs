using System;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.SupportScreens;

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File				: EntityExposureAdaptor.cs 
	///* $Revision			: 1.0.0.0 
	///* $Creation Date		: 05-Oct-2005
	///* $Author			: Nikhil Garg
	///***************************************************************	
	/// <summary>	
	///	This class Rolls up the Exposure Records and Prints the RollUp Log Report	
	/// </summary>
	public class EntityExposureAdaptor: BusinessAdaptorBase
	{
		#region Constructor
		public EntityExposureAdaptor(){}
		#endregion

		#region Public methods
		/// <summary>
		///		This function Rolls up the Exposure Records and Prints the RollUp Log Report	
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///		Output structure is as follows-:
		///		<RollUpReport>
		///			<File name="RollUpReport.pdf">Base 64 data</File>
		///		</RollUpReport>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool RollUp(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			EntityExposure objEntityExposure=null;
			string sPdfDocPath=string.Empty;		//pdf file path
			bool bReturnValue=false;
			XmlElement objRootElement=null;			//used for creating the Output Xml
            int iCompleted = 0;
            int iFailed = 0;
			
			try
			{
				objEntityExposure = new EntityExposure(base.userLogin.objRiskmasterDatabase.DataSourceName,base.userLogin.LoginName,base.userLogin.Password, base.ClientId);//rkaur27

				//Rollup records and get PDF Data
				bReturnValue = objEntityExposure.RollUp(ref sPdfDocPath, ref iCompleted, ref iFailed);

				// If return value true then copy the binary content of file in the p_objXmlOut doc.
				if( bReturnValue )
				{					
					p_objXmlOut = new XmlDocument();
					objRootElement = p_objXmlOut.CreateElement( "RollUp" );
					p_objXmlOut.AppendChild( objRootElement );
                    objRootElement = p_objXmlOut.CreateElement("RollUpReport");
                    p_objXmlOut.DocumentElement.AppendChild(objRootElement);
					this.CreateNodeWithFileContent( GetFileName("RollUpReport","pdf"), objRootElement , sPdfDocPath );
                    objRootElement = p_objXmlOut.CreateElement("StatusDesc");
                    objRootElement.InnerText = String.Format(Globalization.GetString("EntityExposure.RollUp.ExposureRollUpComplete", base.ClientId), iCompleted, iFailed);//rkaur27
                    p_objXmlOut.DocumentElement.AppendChild(objRootElement);
					return( true );
				}
				return false;
			}
			catch(InvalidValueException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("EntityExposureAdaptor.RollUp.Error", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
				return false;
			}
			finally
			{
                //Added by Shivendu
                if (objEntityExposure != null)
                    objEntityExposure.Dispose(); 
				objEntityExposure = null;
			}
		}

		#endregion

		#region Private Functions
		/// <summary>
		/// Generates a unique file Name 
		/// </summary>
		private string GetFileName(string p_sFileName, string p_sFileExt)
		{
			string sFileName=string.Empty;	//for holding file name

			sFileName=p_sFileName + "_" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + 
				DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() +
				DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString() + "." + p_sFileExt;

			return sFileName;
		}

		/// <summary>
		/// Generates a PDf file and creates an XML node with pdf data in base 64 format
		/// </summary>
		private void CreateNodeWithFileContent( string p_sNodeName, XmlElement p_objParentNode, string p_sOutFilepath )
		{
			XmlElement objElement = null;
			MemoryStream objMemoryStream = null;
			FileStream objFileStream = null ;
			BinaryReader objBReader = null ;
			byte[] arrRet = null ;
			try
			{
				// Check the file existence.
				if( ! File.Exists( p_sOutFilepath ) )
					throw new RMAppException( Globalization.GetString( "EntityExposureAdaptor.CreateNodeWithFileContent.FileNotFound" , base.ClientId));//rkaur27
				
				// Read the file in memory stream.
				objFileStream = new FileStream( p_sOutFilepath ,FileMode.Open);
				objBReader = new BinaryReader( objFileStream );
				arrRet = objBReader.ReadBytes( (int)objFileStream.Length );				
				objMemoryStream = new MemoryStream( arrRet );
				
				objFileStream.Close();

				// Delete the temp file.
				File.Delete( p_sOutFilepath );
				
				// Insert the memory stream as base64 string into the Xml document.
				objElement = p_objParentNode.OwnerDocument.CreateElement( "File" );											
				objElement.SetAttribute( "Name", p_sNodeName );                 																
				objElement.InnerText = Convert.ToBase64String( objMemoryStream.ToArray() );
				p_objParentNode.AppendChild( objElement );				
			}
			catch( Exception p_objException )
			{
				throw new RMAppException( Globalization.GetString("EntityExposureAdaptor.CreateNodeWithFileContent.Error" , base.ClientId), p_objException ) ;//rkaur27
			}
			finally
			{
				objElement = null;
				if( objMemoryStream != null ) 
				{
					objMemoryStream.Close();
					objMemoryStream = null;
				}
				if( objFileStream != null )
				{
					objFileStream.Close();
					objFileStream = null ;
				}
				if( objBReader != null )
				{
					objBReader.Close();
					objBReader = null ;
				}
			}
		}
		#endregion

	}
}
