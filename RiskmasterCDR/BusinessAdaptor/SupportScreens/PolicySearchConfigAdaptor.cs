﻿using System;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.SupportScreens;

namespace Riskmaster.BusinessAdaptor
{
    ///************************************************************** 
    ///* $File				: PolicySearchConfigAdaptor.cs 
    ///* $Revision			: 1.0.0.0 
    ///* $Creation Date		: 03-13-2014
    ///* $Author			: Pradyumna Singh Shekhawat
    ///***************************************************************	
    /// <summary>
    /// Summary description for PolicySearchConfigAdaptor.
    /// </summary>
    public class PolicySearchConfigAdaptor : BusinessAdaptorBase
    {
        #region Constructor
        public PolicySearchConfigAdaptor() { }
        #endregion

        #region Public methods

        /// <summary>
        /// Gets Policy Search Configuration settings
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErr"></param>
        /// <returns></returns>
        public bool GetSavedPolicySrchConfig(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            PolicySearchConfig objPolicySearch = null;

            objPolicySearch = new PolicySearchConfig(connectionString, DSNID, base.ClientId);//rkaur27
            p_objXmlOut = objPolicySearch.GetPolicySearchConfig(DSNID);
            return true;
        }
       
        /// <summary>
        /// Save Policy Search Configuration settings
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErr"></param>
        /// <returns></returns>
        public bool SavePolicySrchConfig(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            PolicySearchConfig objPolicySearch = null;

            objPolicySearch = new PolicySearchConfig(connectionString, DSNID, base.ClientId);//rkaur27
            objPolicySearch.SavePolicySrchConfig(p_objXmlIn);
            return true;
        }

        #endregion Recent Claim Configuration Header

    }
}
