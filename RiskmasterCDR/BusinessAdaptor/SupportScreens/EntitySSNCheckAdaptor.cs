using System;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.SupportScreens;

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File				: EntitySSNCheckAdaptor.cs 
	///* $Revision			: 1.0.0.0 
	///* $Creation Date		: 02-Jan-2006
	///* $Author			: Nikhil Garg
	///***************************************************************	
	/// <summary>	
	///	This class checks for duplicate SSN while saving the Entity Record
	/// </summary>
	public class EntitySSNCheckAdaptor: BusinessAdaptorBase
	{
		#region Constructor
		public EntitySSNCheckAdaptor(){}
		#endregion

		#region Public methods
		/// <summary>
		///		This function checks for duplicate SSN while saving the Entity Record
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///		Output structure is as follows-:
		///		<Entities dupexists="0/1">
		///			<Entity id="">Last_Name, First_Name</Entity>
		///			<Entity id="">Last_Name, First_Name</Entity>
		///		</Entities>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool CheckSSN(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			EntitySSNCheck objEntitySSNCheck=null;
			string sTaxId=string.Empty;
			string sEntityId = string.Empty;
			XmlElement objElement=null;				//used for parsing the input xml
			
			try
			{
				//check existence of ssnid which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//TaxId");
				if (objElement==null)
				{
					p_objErrOut.Add("EntitySSNCheckAdaptorParameterMissing",Globalization.GetString("EntitySSNCheckAdaptorParameterMissing", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
					return false;
				}
				sTaxId=objElement.InnerXml;

				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//EntityId");
				if (objElement==null)
				{
                    p_objErrOut.Add("EntitySSNCheckAdaptorParameterMissing", Globalization.GetString("EntitySSNCheckAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				sEntityId=objElement.InnerXml;

                objEntitySSNCheck = new EntitySSNCheck(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.userLogin.UserId, base.ClientId);

				p_objXmlOut=objEntitySSNCheck.CheckSSN(sTaxId,sEntityId);

				return true ;
			}
			catch(InvalidValueException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("EntitySSNCheckAdaptor.CheckSSN.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
                if (objEntitySSNCheck != null)
                {
                    objEntitySSNCheck.Dispose();
                }
				objEntitySSNCheck = null;
			}
		}

		/// <summary>
		///		This function updates the user pref xml if user has opted for ignoring future warnings
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">None</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool IgnoreSSNChecking(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			EntitySSNCheck objEntitySSNCheck=null;
			bool bIgnoreFuture=false;
			XmlElement objElement=null;				//used for parsing the input xml
			
			try
			{
				//check existence of ssnid which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//IgnoreForFuture");
				if (objElement==null)
				{
					p_objErrOut.Add("EntitySSNCheckAdaptorParameterMissing",Globalization.GetString("EntitySSNCheckAdaptorParameterMissing", base.ClientId),BusinessAdaptorErrorType.Error);
					return false;
				}
				bIgnoreFuture=Conversion.ConvertStrToBool(objElement.InnerXml);

				objEntitySSNCheck = new EntitySSNCheck(base.userLogin.objRiskmasterDatabase.DataSourceName,base.userLogin.LoginName,base.userLogin.Password,base.userLogin.UserId,base.ClientId);

				objEntitySSNCheck.IgnoreSSNChecking(bIgnoreFuture);

				return true ;
			}
			catch(InvalidValueException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("EntitySSNCheckAdaptor.IgnoreSSNChecking.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
                if (objEntitySSNCheck != null)
                {
                    objEntitySSNCheck.Dispose();
                }
                objEntitySSNCheck = null;
			}
		}

		#endregion
	}
}
