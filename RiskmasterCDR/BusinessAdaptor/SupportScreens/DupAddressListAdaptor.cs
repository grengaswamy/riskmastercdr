using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.SupportScreens;

namespace Riskmaster.BusinessAdaptor
{
	//RMA-8753 nshah28 start
	/// <summary>	
	///	This class lists the possible Duplicate Claims for a new claim
	/// </summary>
	public class DupAddressListAdaptor: BusinessAdaptorBase
	{
		#region Constructor
        public DupAddressListAdaptor()
		{
		}
		#endregion

		#region Public Methods
		/// <summary>
		///		This function gets the Duplicate address list
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///		Output structure is as follows-:
		///		
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GetDupAddressList(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			DupAddressList objDupAddress = null;
			
			try
			{
                objDupAddress = new DupAddressList(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId);//rkaur27
				p_objXmlOut = objDupAddress.GetDupAddressList(p_objXmlIn);
				return true;
			}
			catch(InvalidValueException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("DupAddressListAdaptor.GetDupAddressList.Error", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
				return false;
			}
			finally
			{
                objDupAddress = null;
			}
		}

		#endregion
	}
}
