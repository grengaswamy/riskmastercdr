﻿using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.SupportScreens;

namespace Riskmaster.BusinessAdaptor
{
    ///************************************************************** 
    ///* $File				: ConfRecPermsAdaptor.cs 
    ///* $Revision			: 1.0.0.0 
    ///* $Creation Date		: 03/24/2010
    ///* $Author			: Priya Mittal
    ///***************************************************************	
    /// <summary>	
    ///	This class lists the Users in User Permissions screen for a Confidential Record
    /// </summary>
    public class ConfRecPermsAdaptor : BusinessAdaptorBase
    {
        #region Constructor
        public ConfRecPermsAdaptor()
		{
		}
		#endregion

        #region Public Methods
        /// <summary>
        ///	    This function is a wrapper for GetAvailablePermList function in "ConfRecPerms" class
        /// </summary>
        /// <param name="p_objXmlIn">Input parameters as xml</param>
        /// <param name="p_objXmlOut">Result as Output xml
        ///	</param>
        /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
        /// <returns>Success -True or Failure -false in execution of the function</returns>
        public bool GetAvailablePermList(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            ConfRecPerms objConfRecPerms = null;        
            try
            {
                objConfRecPerms = new ConfRecPerms(base.userLogin.objRiskmasterDatabase.DataSourceId, base.userLogin.objRiskmasterDatabase.ConnectionString,base.userLogin.UserId,base.securityConnectionString, base.ClientId);//rkaur27
                p_objXmlOut = objConfRecPerms.GetAvailablePermList(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ConfRecPermsAdaptor.GetAvailablePermList.Error", base.ClientId), BusinessAdaptorErrorType.Error);//rkaur27
                return false;
            }
            finally
            {
                objConfRecPerms = null;
            }
        }

        #endregion
    }
}
