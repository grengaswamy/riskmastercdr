using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.SupportScreens;

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File				: AdjDtdTxtAdaptor.cs 
	///* $Revision			: 1.0.0.0 
	///* $Creation Date		: 23-Dec-2005
	///* $Author			: Mihika Agrawal
	///***************************************************************	
	/// <summary>	
	///	This class lists the Adjuster Dated Text Information for an Adjuster
	/// </summary>
	public class AdjDtdTxtAdaptor: BusinessAdaptorBase

	{
		#region Constructor
		public AdjDtdTxtAdaptor()
		{
		}
		#endregion

		#region Public Methods
		/// <summary>
		///		This function gets the Adjuster Dated Text Information for an Adjuster	
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///		Output structure is as follows-:
		///		
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GetAdjDtdTxt(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			AdjDtdTxt objAdjText = null;
			
			try
			{
				objAdjText = new AdjDtdTxt(base.userLogin.objRiskmasterDatabase.DataSourceName,base.userLogin.LoginName,base.userLogin.Password, base.ClientId);
				p_objXmlOut = objAdjText.GetAdjDatedText(p_objXmlIn);
				return true;
			}
			catch(InvalidValueException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("AdjDtdTxtAdaptor.GetAdjDtdTxt.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objAdjText = null;
			}
		}

		#endregion
	}
}
