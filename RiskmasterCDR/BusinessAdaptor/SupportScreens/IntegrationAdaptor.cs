﻿using System;
using System.Collections.Generic;
using System.Text;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.SupportScreens;
using System.Xml;
namespace Riskmaster.BusinessAdaptor
{
    public class IntegrationAdaptor : BusinessAdaptorBase
    {
        #region Public methods
        /// <summary>
        ///		This function gets the list of person involved
        /// </summary>
        /// <param name="p_objXmlIn">Input parameters as xml</param>
        /// <param name="p_objXmlOut">Result as Output xml</param>
        /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
        /// <returns>Success -True or Failure -false in execution of the function</returns>
        public bool GetActualPageNameWithParams(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            bool bReturnValue = false;
            Integate objAdaptor = null;
            try
            {
                objAdaptor = new Integate();
                SessionManager objSession = GetSessionObject();
                string sessionid = objSession.SessionId;
                objSession.Dispose();
                p_objXmlOut = objAdaptor.GetActualPageNameWithParams(p_objXmlIn, sessionid);
                return true;
            }
            catch (InvalidValueException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("IntegrationAdaptor.GetActualPageNameWithParams.Error"), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
               
            }
        }

        #endregion
    }
}
