﻿using System;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.SupportScreens;

namespace Riskmaster.BusinessAdaptor
{
    ///************************************************************** 
    ///* $File				: RecentRecordsAdaptor.cs 
    ///* $Revision			: 1.0.0.0 
    ///* $Creation Date		: 03-31-2009
    ///* $Author			: Nitin Sharma
    ///***************************************************************	
    /// <summary>
    /// Summary description for RecentRecordsAdaptor.
    /// </summary>
    public class RecentRecordsAdaptor : BusinessAdaptorBase
    {
        #region Constructor
        public RecentRecordsAdaptor() { }
        #endregion

        #region Public methods
        /// <summary>
        ///		This function gets the list of person involved
        /// </summary>
        /// <param name="p_objXmlIn">Input parameters as xml</param>
        /// <param name="p_objXmlOut">Result as Output xml</param>
        /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
        /// <returns>Success -True or Failure -false in execution of the function</returns>
        public bool GetRecentSavedRecords(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            RecentRecords objRecentRecords = null;
            try
            {
                objRecentRecords = new RecentRecords(connectionString, userID, base.ClientId);//rkaur27
                p_objXmlOut = objRecentRecords.GetRecentSavedRecrods(p_objXmlIn); 

                return true;
            }
            catch (InvalidValueException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ListPersonInvolvedAdaptor.GetPIList.Error",base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa
                return false;
            }
        }

        #endregion
         #region Recent Claim Configuration Header 
        /// <summary>
        /// Neha Mits 23633 05/16/2011 getting header values from the database
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErr"></param>
        /// <returns></returns>
        public bool GetRecentClaimConfig(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            RecentRecords objRecentRecords = null;

            objRecentRecords = new RecentRecords(connectionString, userID, base.ClientId);//rkaur27
            p_objXmlOut = objRecentRecords.GetRecentClaimConfig(userID);
            return true;
        }
       
        /// <summary>
        /// Neha Mits 23633 saving  header values to  the database
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErr"></param>
        /// <returns></returns>
        public bool SetRecentClaimConfig(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            RecentRecords objRecentRecords = null;

            objRecentRecords = new RecentRecords(connectionString, userID, base.ClientId);//rkaur27
            objRecentRecords.SetRecentClaimConfig(p_objXmlIn);
            return true;
        }

        #endregion Recent Claim Configuration Header

    }

    
}
