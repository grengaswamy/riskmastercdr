using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.Application.NonOccPayments;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using System.IO;

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: NonOccPaymentsAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 23-Sep-2005
	///* $Author	: Nikhil Kr. Garg
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	
	/// <summary>	
	///	This class is used to call the application layer component for NonOcc Payments.	
	/// </summary>
	public class NonOccPaymentsAdaptor: BusinessAdaptorBase
	{
		#region Constructor
		public NonOccPaymentsAdaptor(){}
		#endregion
		
		#region Public methods

		public bool GetPrintedPayments(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			NonOccPaymentsManager objNonOccPaymentsManager=null;
			XmlElement objElement=null;				//used for parsing the input xml
			int iClaimId=0;
			int iPiEid=0;
			string sTaxArray=string.Empty;

			try
			{
				//check existence of Claim Id which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//claimid");
				if (objElement==null)
				{
					p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing",Globalization.GetString("NonOccPaymentsAdaptorParameterMissing",base.ClientId),BusinessAdaptorErrorType.Error);
					return false;
				}
				iClaimId=Conversion.ConvertStrToInteger(objElement.InnerText);

				//check existence of PiEid which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//pieid");
				if (objElement==null)
				{
					p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing",Globalization.GetString("NonOccPaymentsAdaptorParameterMissing",base.ClientId),BusinessAdaptorErrorType.Error);
					return false;
				}
				iPiEid=Conversion.ConvertStrToInteger(objElement.InnerText);

				//check existence of TaxArray which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//taxarray");
				if (objElement==null)
				{
					p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing",Globalization.GetString("NonOccPaymentsAdaptorParameterMissing",base.ClientId),BusinessAdaptorErrorType.Error);
					return false;
				}
				sTaxArray=objElement.InnerText;

                using (objNonOccPaymentsManager = new NonOccPaymentsManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    //get Printed Payments
                    p_objXmlOut = objNonOccPaymentsManager.GetPrintedPayments(iClaimId, iPiEid, sTaxArray);
                }
				
				return true;
			}
			catch(InvalidValueException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("NonOccPaymentsAdaptor.GetPrintedPayments.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objNonOccPaymentsManager = null;
				objElement=null;
			}
		}


		public bool GetFuturePayments(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			NonOccPaymentsManager objNonOccPaymentsManager=null;
			XmlElement objElement=null;				//used for parsing the input xml
			int iClaimId=0;
			int iBatchId=0;
			string sTaxArray=string.Empty;
			bool bBatchRestrict=false;

			try
			{
				//check existence of Claim Id which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//claimid");
				if (objElement==null)
				{
					p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing",Globalization.GetString("NonOccPaymentsAdaptorParameterMissing",base.ClientId),BusinessAdaptorErrorType.Error);
					return false;
				}
				iClaimId=Conversion.ConvertStrToInteger(objElement.InnerText);

				//check existence of PiEid which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//batchid");
				if (objElement==null)
				{
					p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing",Globalization.GetString("NonOccPaymentsAdaptorParameterMissing",base.ClientId),BusinessAdaptorErrorType.Error);
					return false;
				}
				iBatchId=Conversion.ConvertStrToInteger(objElement.InnerText);

				//check existence of TaxArray which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//taxarray");
				if (objElement==null)
				{
					p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing",Globalization.GetString("NonOccPaymentsAdaptorParameterMissing",base.ClientId),BusinessAdaptorErrorType.Error);
					return false;
				}
				sTaxArray=objElement.InnerText;

				//check existence of TaxArray which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//batchrestrict");
				if (objElement==null)
				{
					p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing",Globalization.GetString("NonOccPaymentsAdaptorParameterMissing",base.ClientId),BusinessAdaptorErrorType.Error);
					return false;
				}
				bBatchRestrict=Conversion.ConvertStrToBool(objElement.InnerText);

                using (objNonOccPaymentsManager = new NonOccPaymentsManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    //get Future Payments
                    p_objXmlOut = objNonOccPaymentsManager.GetFuturePayments(iClaimId, sTaxArray, bBatchRestrict, iBatchId);
                }
				
				return true;
			}
			catch(InvalidValueException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("NonOccPaymentsAdaptor.GetFuturePayments.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objNonOccPaymentsManager = null;
				objElement=null;
			}
		}


		public bool DeletePayments(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			NonOccPaymentsManager objNonOccPaymentsManager=null;
			XmlElement objElement=null;				//used for parsing the input xml
            XmlElement objElementBatch=null; //Parijat: MITS 12126--for checking if delete bacth is called.	
            bool batchFlag = false;
			string sDeleteList=string.Empty; 

			try
			{
				//check existence of Delete List which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//deletelist");
				if (objElement==null)
				{
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				sDeleteList=objElement.InnerText;
                //Parijat--Mits 12126-- check if batch delete is called and call batch deleting function accordingly
                #region Parijat :12126: Batch Delete
                objElementBatch = (XmlElement)p_objXmlIn.SelectSingleNode("//batchDelete");
                if (objElementBatch != null)
                {
                   if(objElementBatch.InnerText =="true")
                   {
                       batchFlag = true;
                   }
                }
                if (batchFlag)
                {
                    using (objNonOccPaymentsManager = new NonOccPaymentsManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                    {
                        //Delete Batch Payments
                        return objNonOccPaymentsManager.DeleteBatchPayments(sDeleteList);
                    }
                }
                else
                {
                    using (objNonOccPaymentsManager = new NonOccPaymentsManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                    {
                        //Delete Selected Payments
                        return objNonOccPaymentsManager.DeletePayments(sDeleteList);
                    }
                }
                #endregion

            }
			catch(InvalidValueException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("NonOccPaymentsAdaptor.DeletePayments.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objNonOccPaymentsManager = null;
				objElement=null;
			}
		}


		public bool EditPayment(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			NonOccPaymentsManager objNonOccPaymentsManager=null;
			XmlElement objElement=null;				//used for parsing the input xml
			int iBatchId=0; 
			int iPaymentNum=0; 

			try
			{
				//check existence of Batch Id which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//batchid");
				if (objElement==null)
				{
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				iBatchId=Conversion.ConvertStrToInteger(objElement.InnerText);

				//check existence of payment number which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//paymentnum");
				if (objElement==null)
				{
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				iPaymentNum=Conversion.ConvertStrToInteger(objElement.InnerText);

                //check existence of payment number which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//autosplitid");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                int iAutoSplitId = Conversion.ConvertStrToInteger(objElement.InnerText);

                using (objNonOccPaymentsManager = new NonOccPaymentsManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    //Get Data for Payment to Edit
                    p_objXmlOut = objNonOccPaymentsManager.EditPayment(iBatchId, iPaymentNum, iAutoSplitId);
                }

				return true;
				
			}
			catch(InvalidValueException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("NonOccPaymentsAdaptor.EditPayment.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objNonOccPaymentsManager = null;
				objElement=null;
			}
		}


		public bool SaveEdit(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			NonOccPaymentsManager objNonOccPaymentsManager=null;
			XmlElement objElement=null;				//used for parsing the input xml
			string sFrom=string.Empty;
			string sTo=string.Empty;
			double dblGross=0;
			int iClaimId=0;
            int iBatchId=0;
			int iPaymentNum=0;
			bool bReCalc=false;
            string sClaimNumber = "";

			try
			{
				//check existence of from Date which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//fromdate");
				if (objElement==null)
				{
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				sFrom=Conversion.GetDate(objElement.InnerText);

				//check existence of to Date which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//todate");
				if (objElement==null)
				{
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				sTo=Conversion.GetDate(objElement.InnerText);

                //check existence of Print Date which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//printdate");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                string sPrintDate = Conversion.GetDate(objElement.InnerText);
                
                //check existence of gross which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//gross");
				if (objElement==null)
				{
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				dblGross=Conversion.ConvertStrToDouble(objElement.InnerText);

                //check existence of pensionoffset which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//pensionoffset");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                double dPensionoffset = Conversion.ConvertStrToDouble(objElement.InnerText);

                //check existence of ssoffset which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//ssoffset");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                double dSsoffset = Conversion.ConvertStrToDouble(objElement.InnerText);

                //check existence of oioffset which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//oioffset");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                double dOioffset = Conversion.ConvertStrToDouble(objElement.InnerText);

                //*********************************************************************
                //GHS Enhancement 
                //Added by Ravneet Kaur   //pmittal5 Mits 14841

                //check existence of other offset #1 which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//othoffset1");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                double dOtherOffSet1 = Conversion.ConvertStrToDouble(objElement.InnerText);

                //check existence of other offset #2 which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//othoffset2");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                double dOtherOffSet2 = Conversion.ConvertStrToDouble(objElement.InnerText);

                //check existence of other offset #3 which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//othoffset3");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                double dOtherOffSet3 = Conversion.ConvertStrToDouble(objElement.InnerText);

                //check existence of post tax deduction #1 which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//psttaxded1");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                double dPostTaxDed1 = Conversion.ConvertStrToDouble(objElement.InnerText);

                //check existence of post tax deduction #2 which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//psttaxded2");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                double dPostTaxDed2 = Conversion.ConvertStrToDouble(objElement.InnerText);
                //check existence of supppayment which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//supppayment");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                double dblSuppPayment = Conversion.ConvertStrToDouble(objElement.InnerText);

				//check existence of claim id which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//claimid");
				if (objElement==null)
				{
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				iClaimId=Conversion.ConvertStrToInteger(objElement.InnerText);

                //check existence of autosplitid which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//autosplitid");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                long lAutoSplitId = Conversion.ConvertStrToLong(objElement.InnerText);

               //to do check claim number
                //check existence of claim number
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//claimnumber");
                if (objElement != null)
                {
                    sClaimNumber = objElement.InnerText;
                }
                

				//check existence of Batch Id which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//batchid");
				if (objElement==null)
				{
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				iBatchId=Conversion.ConvertStrToInteger(objElement.InnerText);

				//check existence of Payment Num which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//paymentnum");
				if (objElement==null)
				{
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				iPaymentNum=Conversion.ConvertStrToInteger(objElement.InnerText);

				//check existence of ReCalc which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//recalc");
				if (objElement==null)
				{
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				bReCalc=Conversion.ConvertStrToBool(objElement.InnerText);

                //check existence of amountchange which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//amountchange");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                bool bAmountChange = Conversion.ConvertStrToBool(objElement.InnerText);

                //check existence of checkmemo which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//checkmemo");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                string sCheckMemo = objElement.InnerText;

                using (objNonOccPaymentsManager = new NonOccPaymentsManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    //Get Data for Payment to Edit
                    p_objXmlOut = objNonOccPaymentsManager.SaveEdit(sFrom, sTo, dblGross, iClaimId, sClaimNumber, iBatchId, iPaymentNum, bReCalc, lAutoSplitId, dblSuppPayment, dPensionoffset, dSsoffset, dOioffset,
                                                                    dOtherOffSet1, dOtherOffSet2, dOtherOffSet3, dPostTaxDed1 ,dPostTaxDed2, bAmountChange, sPrintDate, sCheckMemo);
                }

				return true;
				
			}
			catch(InvalidValueException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("NonOccPaymentsAdaptor.SaveEdit.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objNonOccPaymentsManager = null;
				objElement=null;
			}
		}


		public bool CalculatePayments(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			NonOccPaymentsManager objNonOccPaymentsManager=null;
			XmlElement objElement=null;				//used for parsing the input xml
			int iClaimId=0;
			int iPiEid=0;
			int iSysCmd=0;
			int iTransTypeCode=0;
			int iAccountId=0;
            string sBenCalcPayStart = "";
            string sBenCalcPayEnd = "";

			try
			{
				//check existence of ClaimId which is required

				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//claimid");
				if (objElement==null)
				{
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				iClaimId=Conversion.ConvertStrToInteger(objElement.InnerText);

				//check existence of PiEid which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//pieid");
				if (objElement==null)
				{
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				iPiEid=Conversion.ConvertStrToInteger(objElement.InnerText);

				//check existence of syscmd which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//syscmd");
				if (objElement==null)
				{
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				iSysCmd=Conversion.ConvertStrToInteger(objElement.InnerText);

				//check existence of transtypecode which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//transtype");
				if (objElement==null)
				{
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				iTransTypeCode=Conversion.ConvertStrToInteger(objElement.InnerText);

				//check existence of AccountId which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//acct");
				if (objElement==null)
				{
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				iAccountId=Conversion.ConvertStrToInteger(objElement.InnerText);

                //check existence of payst which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//payto");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                sBenCalcPayEnd = Conversion.GetDate(objElement.InnerText);

                //check existence of payst which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//payst");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                sBenCalcPayStart = Conversion.GetDate(objElement.InnerText);

                //check existence of payst which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//iNetPayment");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                double dNetPayment = Conversion.ConvertStrToDouble(objElement.InnerText);

                //check existence of dWeeklyBenefit which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//dWeeklyBenefit");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                double dWeeklyBenefit = Conversion.ConvertStrToDouble(objElement.InnerText);

                //check existence of payst which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//iGrossCalculatedPayment");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                double dGrossCalculatedPayment = Conversion.ConvertStrToDouble(objElement.InnerText);

                //check existence of payst which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//iGrossCalculatedSupplement");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                double dGrossCalculatedSupplement = Conversion.ConvertStrToDouble(objElement.InnerText);

                //check existence of payst which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//iGrossTotalNetOffsets");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                double dGrossTotalNetOffsets = Conversion.ConvertStrToDouble(objElement.InnerText);

                //check existence of iFederalTax which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//iFederalTax");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                double dFederalTax = Conversion.ConvertStrToDouble(objElement.InnerText);

                //check existence of iSocialSecurityAmount which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//iSocialSecurityAmount");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                double dSocialSecurityAmount = Conversion.ConvertStrToDouble(objElement.InnerText);

                //check existence of iMedicareAmount which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//iMedicareAmount");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                double dMedicareAmount = Conversion.ConvertStrToDouble(objElement.InnerText);

                //check existence of iStateAmount which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//iStateAmount");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                double dStateAmount = Conversion.ConvertStrToDouble(objElement.InnerText);

                //check existence of payst which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//dailyamount");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                double dDailyAmount = Conversion.ConvertStrToDouble(objElement.InnerText);

                //check existence of benst which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//benst");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                string sBenefitStartDate = Conversion.GetDate(objElement.InnerText);

                //check existence of bento which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//bento");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                string sBenefitThroughDate = Conversion.GetDate(objElement.InnerText);

                //check existence of dailysuppamount which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//dailysuppamount");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                double dDailySuppAmount = Conversion.ConvertStrToDouble(objElement.InnerText);

                //check existence of dailysuppamount which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//dPension");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                double dPension = Conversion.ConvertStrToDouble(objElement.InnerText);

                //check existence of dailysuppamount which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//dSS");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                double dSS = Conversion.ConvertStrToDouble(objElement.InnerText);

                //check existence of dailysuppamount which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//dOther");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                double dOther = Conversion.ConvertStrToDouble(objElement.InnerText);

                //check existence of iDaysWorkingInMonth which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//iDaysWorkingInMonth");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                int iDaysWorkingInMonth = Conversion.ConvertStrToInteger(objElement.InnerText);

                //check existence of iDaysWorkingInMonth which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//iDaysWorkingInWeek");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                int iDaysWorkingInWeek = Conversion.ConvertStrToInteger(objElement.InnerText);

                //check existence of pensionoffset which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//pensionoffset");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                double dPensionOffSet = Conversion.ConvertStrToDouble(objElement.InnerText);

                //check existence of ssoffset which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//ssoffset");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                double dSSOffSet = Conversion.ConvertStrToDouble(objElement.InnerText);

                //check existence of oioffset which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//oioffset");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                double dOiOffSet = Conversion.ConvertStrToDouble(objElement.InnerText);

                //*********************************************************************
                //Added by Ravneet Kaur  //pmittal5 Mits 14841
                //check existence of other offset #1 which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//othoffset1");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                double dOtherOffSet1 = Conversion.ConvertStrToDouble(objElement.InnerText);

                //check existence of other offset #2 which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//othoffset2");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                double dOtherOffSet2 = Conversion.ConvertStrToDouble(objElement.InnerText);

                //check existence of other offset #3 which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//othoffset3");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                double dOtherOffSet3 = Conversion.ConvertStrToDouble(objElement.InnerText);

                //check existence of post tax deduction #1 which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//psttaxded1");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                double dPostTaxDed1 = Conversion.ConvertStrToDouble(objElement.InnerText);

                //check existence of post tax deduction #2 which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//psttaxded2");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                double dPostTaxDed2 = Conversion.ConvertStrToDouble(objElement.InnerText);

                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//doffset1");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                double dOther1 = Conversion.ConvertStrToDouble(objElement.InnerText);

                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//doffset2");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                double dOther2 = Conversion.ConvertStrToDouble(objElement.InnerText);

                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//doffset3");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                double dOther3 = Conversion.ConvertStrToDouble(objElement.InnerText);


                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//dPostTaxDed1");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                double dPostTax1= Conversion.ConvertStrToDouble(objElement.InnerText);

                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//dPostTaxDed2");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                double dPostTax2 = Conversion.ConvertStrToDouble(objElement.InnerText);
                ///Ended by RKaur

                //*********************************************************************

                //check existence of iOffsetCalc which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//iOffsetCalc");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                int iOffsetCalc = Conversion.ConvertStrToInteger(objElement.InnerText);

                //check existence of iOffsetCalc which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//ilblDaysIncluded");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                int ilblDaysIncluded = Conversion.ConvertStrToInteger(objElement.InnerText);

                //check existence of dSuppRate which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//dSuppRate");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                double dSuppRate = Conversion.ConvertStrToDouble(objElement.InnerText);

                using (objNonOccPaymentsManager = new NonOccPaymentsManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    //Calculate Payments
                    p_objXmlOut = objNonOccPaymentsManager.CalculatePayments(iClaimId, iPiEid, iSysCmd, iTransTypeCode, iAccountId, sBenCalcPayStart, sBenCalcPayEnd, dDailyAmount, dDailySuppAmount, sBenefitStartDate, sBenefitThroughDate, 
                                                                            dPension, dSS, dOther, iDaysWorkingInMonth, iDaysWorkingInWeek, iOffsetCalc, dPensionOffSet, dSSOffSet, dOiOffSet, dFederalTax, dSocialSecurityAmount, dMedicareAmount, dStateAmount, dNetPayment, dGrossCalculatedPayment, dGrossCalculatedSupplement, dGrossTotalNetOffsets, ilblDaysIncluded, dSuppRate, dWeeklyBenefit,
                                                                            dOther1,dOther2 ,dOther3,dPostTax1,dPostTax2,dOtherOffSet1,dOtherOffSet2 ,dOtherOffSet3,dPostTaxDed1 ,dPostTaxDed2);
                }
				
				return true;
			}
			catch(InvalidValueException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("NonOccPaymentsAdaptor.CalculatePayments.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objNonOccPaymentsManager = null;
				objElement=null;
			}
		}


		public bool GetCollectionScreenInformation(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			NonOccPaymentsManager objNonOccPaymentsManager=null;
			XmlElement objElement=null;				//used for parsing the input xml
			int iClaimId=0;
			
			try
			{
				//check existence of ClaimId which is required

				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ClaimId");
				if (objElement==null)
				{
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				iClaimId=Conversion.ConvertStrToInteger(objElement.InnerText);


                using (objNonOccPaymentsManager = new NonOccPaymentsManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    //Calculate Payments
                    p_objXmlOut = objNonOccPaymentsManager.GetCollectionScreenInformation(iClaimId);
                }
				
				return true;
			}
			catch(InvalidValueException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("NonOccPaymentsAdaptor.GetCollectionScreenInformation.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objNonOccPaymentsManager = null;
				objElement=null;
			}
		}


		public bool SaveAllCollections(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			NonOccPaymentsManager objNonOccPaymentsManager=null;
			XmlElement objElement=null;				//used for parsing the input xml
			int iClaimId=0;
			
			try
			{
				//check existence of ClaimId which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ClaimId");
				if (objElement==null)
				{
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				iClaimId=Conversion.ConvertStrToInteger(objElement.InnerText);

                using (objNonOccPaymentsManager = new NonOccPaymentsManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    //Calculate Payments
                    objNonOccPaymentsManager.SaveAllCollections(iClaimId, p_objXmlIn);
                }
				
				return true;
			}
			catch(InvalidValueException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("NonOccPaymentsAdaptor.SaveAllCollections.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objNonOccPaymentsManager = null;
				objElement=null;
			}
		}
        public bool ManualPaymentDetails(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            NonOccPaymentsManager objNonOccPaymentsManager = null;
            XmlElement objElement = null;				//used for parsing the input xml
            int iAutoSplitId = 0;

            try
            {
                //check existence of AutoSplitId which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//autosplitid");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                iAutoSplitId = Conversion.ConvertStrToInteger(objElement.InnerText);

                using (objNonOccPaymentsManager = new NonOccPaymentsManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    //Calculate Payments
                    p_objXmlOut = objNonOccPaymentsManager.ManualPaymentDetails(iAutoSplitId);
                }

                return true;
            }
            catch (InvalidValueException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("NonOccPaymentsAdaptor.SaveAllCollections.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objNonOccPaymentsManager = null;
                objElement = null;
            }
        }

        public bool ProcessManualPayments(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            NonOccPaymentsManager objNonOccPaymentsManager = null;
            XmlElement objElement = null;				//used for parsing the input xml
            XmlElement objParentElement = null;
            int iAutoTransID = 0;
            long lCheckNumber = 0;
            string sCheckDate = "";
            bool bSuccess = false;
            int Transid = 0; //igupta3 Mits : 28566
            int iAccountID = 0;
            try
            {
                //check existence of AutoTransID which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//AutoTransID");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                iAutoTransID = Conversion.ConvertStrToInteger(objElement.InnerText);

                //check existence of CheckNumber which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//CheckNumber");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                //Abhishek MITS 11072
                lCheckNumber = Conversion.ConvertStrToLong(objElement.InnerText);

                //check existence of CheckDate which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//CheckDate");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                sCheckDate = objElement.InnerText;

                //pmittal5 Mits 17636 09/11/09 
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//AccountID");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                iAccountID = Conversion.ConvertStrToInteger(objElement.InnerText);
                //End - pmittal5

                using (objNonOccPaymentsManager = new NonOccPaymentsManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    //MITS 11072 changed check number to long from int
                    //Calculate Payments
                    //pmittal5 Mits 17636 09/11/09 - Passed Account Id to check if it is of Gross or Supplement Payment
                    //bSuccess = objNonOccPaymentsManager.ProcessManualPayment(iAutoTransID, sCheckDate, lCheckNumber);
                    bSuccess = objNonOccPaymentsManager.ProcessManualPayment(iAutoTransID, sCheckDate, lCheckNumber, iAccountID,ref Transid); //igupta3 Mits 28566
                }

                p_objXmlOut = new XmlDocument();
                objParentElement = p_objXmlOut.CreateElement("NonOccPayment");
                p_objXmlOut.AppendChild(objParentElement);
                objElement = p_objXmlOut.CreateElement("ManualPaymentProcessedFlag");
                objElement.InnerText = bSuccess.ToString();
                objParentElement.AppendChild(objElement);

                objElement = p_objXmlOut.CreateElement("TransID");
                objElement.InnerText = Transid.ToString(); //igupta3 Mits 28566
                objParentElement.AppendChild(objElement);

                return true;
            }
            catch (InvalidValueException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("NonOccPaymentsAdaptor.SaveAllCollections.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objNonOccPaymentsManager = null;
                objElement = null;
            }
        }

        //igupta3 Mits : 28566 changes starts
        public bool CreateWorkLossRestrictions(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            NonOccPaymentsManager objNonOccPaymentsManager = null;
            XmlElement objElement = null;				//used for parsing the input xml
            bool bSuccess = false;
            int itransid = 0;

            try
            {
                //check existence of AutoSplitId which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//TransId");
                if (objElement == null)
                {
                    p_objErrOut.Add("NonOccPaymentsAdaptorParameterMissing", Globalization.GetString("NonOccPaymentsAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                itransid = Conversion.ConvertStrToInteger(objElement.InnerText);

                using (objNonOccPaymentsManager = new NonOccPaymentsManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId))
                {
                    //create workloss
                    bSuccess = objNonOccPaymentsManager.CreateWorkLossRestrictions(itransid);
                }

                return true;
            }
            catch (InvalidValueException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("NonOccPaymentsAdaptor.WorklossCreation.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objNonOccPaymentsManager = null;
                objElement = null;
            }
        }
        //igupta3 Mits : 28566 changes ends
		#endregion
	}
}
