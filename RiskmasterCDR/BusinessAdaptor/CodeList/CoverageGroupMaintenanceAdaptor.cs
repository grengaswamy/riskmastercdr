﻿using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.CoverageGroupMaintenance;
using Riskmaster.BusinessAdaptor.Common; 


namespace Riskmaster.BusinessAdaptor
{///************************************************************** 
    ///* $File		: CoverageGroupMaintenanceAdaptor.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 13-Aug-2013
    ///* $Author	:Nitin Goel
    ///* $Comment	: 
    ///* $Source	: 
    ///**************************************************************
    /// <summary>	
    ///	This class is used to call the application layer component for State Maintenance
    /// </summary>
    public class CoverageGroupMaintenanceAdaptor : BusinessAdaptorBase
    {
        private const int RMB_COVGROUP = 1220400300;
        //		private const int RMO_ACCESS = 0;
        //		private const int RMO_VIEW = 1;
        //		private const int RMO_UPDATE = 2;
        //		private const int RMO_CREATE = 3;
        //		private const int RMO_DELETE = 4;


        public CoverageGroupMaintenanceAdaptor() { }

        #region Public Methods

        /// <summary>
        ///		This method is a wrapper to Riskmaster.Application.RMUtilities.CoverageGroupAdaptor.Get() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.Holidays.xml
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            // Nitesh 12Jan2006 Check security and fail if sufficient privilege doesn't exist
            if (!userLogin.IsAllowedEx(RMB_COVGROUP, RMPermissions.RMO_ACCESS))
                throw new PermissionViolationException(RMPermissions.RMO_ACCESS, RMB_COVGROUP);
            CoverageGroupMaintenance objLst = null; //Application layer component			
            try
            {
                objLst = new CoverageGroupMaintenance(connectionString, userLogin, base.ClientId);//ddhiman added: Cloud changes, 10/10/2014
                p_objXmlOut = objLst.Get(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("CoverageGroupMaintenanceAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objLst = null;
            }
        }
        /// <summary>
        ///		This method is a wrapper to Riskmaster.Application.RMUtilities.CoverageGroupAdaptor.Get() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.Holidays.xml
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool GetSelectedCoverageGroupInfo(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            if (!userLogin.IsAllowedEx(RMB_COVGROUP, RMPermissions.RMO_ACCESS))
                throw new PermissionViolationException(RMPermissions.RMO_ACCESS, RMB_COVGROUP);
            XmlElement objElement = null;

            CoverageGroupMaintenance objLst = null; //Application layer component			
            try
            {

                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//control[@name='CovGroupId']");
                if (objElement != null)
                {
                    if (objElement.InnerText == "")
                    {		// Nitesh 12Jan2006 Check security and fail if sufficient privilege doesn't exist
                        if (!userLogin.IsAllowedEx(RMB_COVGROUP, RMPermissions.RMO_CREATE))
                            throw new PermissionViolationException(RMPermissions.RMO_CREATE, RMB_COVGROUP);
                    }
                    else
                    {		// Nitesh 12Jan2006 Check security and fail if sufficient privilege doesn't exist
                        if (!userLogin.IsAllowedEx(RMB_COVGROUP, RMPermissions.RMO_VIEW))
                            throw new PermissionViolationException(RMPermissions.RMO_VIEW, RMB_COVGROUP);
                    }
                }
                objLst = new CoverageGroupMaintenance(connectionString, userLogin, base.ClientId);//ddhiman added: Cloud changes, 10/10/2014
                p_objXmlOut = objLst.GetSelectedCoverageGroupInfo(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("CoverageGroupMaintenanceAdaptor.GetSelectedStateInfo.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objLst = null;
            }
        }
               
      

        /// <summary>
        ///		This method is a wrapper to Riskmaster.Application.RMUtilities.CoverageGroupAdaptor.Get() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.Holidays.xml
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {	// Nitesh 12Jan2006 Check security and fail if sufficient privilege doesn't exist
            if (!userLogin.IsAllowedEx(RMB_COVGROUP, RMPermissions.RMO_UPDATE))
                throw new PermissionViolationException(RMPermissions.RMO_UPDATE, RMB_COVGROUP);

            CoverageGroupMaintenance objLst = null; //Application layer component			
            try
            {
                objLst = new CoverageGroupMaintenance(connectionString, userLogin, base.ClientId); //ddhiman added: Cloud changes, 10/10/2014
                p_objXmlOut = objLst.Save(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("CoverageGroupMaintenanceAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objLst = null;
            }
        }
        /// <summary>
        ///		This method is a wrapper to Riskmaster.Application.RMUtilities.StateAdaptor.Get() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.Holidays.xml
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool Delete(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            // Nitesh 12Jan2006 Check security and fail if sufficient privilege doesn't exist
            if (!userLogin.IsAllowedEx(RMB_COVGROUP, RMPermissions.RMO_DELETE))
                throw new PermissionViolationException(RMPermissions.RMO_DELETE, RMB_COVGROUP);
            CoverageGroupMaintenance objLst = null; //Application layer component			
            try
            {
                objLst = new CoverageGroupMaintenance(connectionString, userLogin, base.ClientId); //ddhiman added: Cloud changes, 10/10/2014
                p_objXmlOut = objLst.Delete(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("CoverageGroupMaintenanceAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objLst = null;
            }
        }
        /// <summary>
        ///		This method is a wrapper to Riskmaster.Application.RMUtilities.CoverageGroupAdaptor.Get() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.Holidays.xml
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool GetAdditionalLangCode(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            // Nitesh 12Jan2006 Check security and fail if sufficient privilege doesn't exist
            if (!userLogin.IsAllowedEx(RMB_COVGROUP, RMPermissions.RMO_ACCESS))
                throw new PermissionViolationException(RMPermissions.RMO_ACCESS, RMB_COVGROUP);
            CoverageGroupMaintenance objLst = null; //Application layer component			
            try
            {
                objLst = new CoverageGroupMaintenance(connectionString, userLogin, base.ClientId);
                p_objXmlOut = objLst.GetAdditionalLangCode(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("CoverageGroupMaintenanceAdaptor.GetAdditionalLangCode.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objLst = null;
            }
        }
        /// <summary>
        ///		This method is a wrapper to Riskmaster.Application.RMUtilities.StateAdaptor.Get() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.Holidays.xml
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool DeleteAdditionalLangCode(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            //  Check security and fail if sufficient privilege doesn't exist
            if (!userLogin.IsAllowedEx(RMB_COVGROUP, RMPermissions.RMO_DELETE))
                throw new PermissionViolationException(RMPermissions.RMO_DELETE, RMB_COVGROUP);
            CoverageGroupMaintenance objLst = null; //Application layer component			
            try
            {
                objLst = new CoverageGroupMaintenance(connectionString, userLogin, base.ClientId);
                p_objXmlOut = objLst.DeleteAdditionalLangCode(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("CoverageGroupMaintenanceAdaptor.DeleteAdditionalLangCode.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objLst = null;
            }
        }
       
        /// <summary>
        ///		This method is a wrapper to Riskmaster.Application.RMUtilities.CoverageGroupAdaptor.Get() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.Holidays.xml
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool SaveAdditionalLangDesc(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {	// Nitesh 12Jan2006 Check security and fail if sufficient privilege doesn't exist
            if (!userLogin.IsAllowedEx(RMB_COVGROUP, RMPermissions.RMO_UPDATE))
                throw new PermissionViolationException(RMPermissions.RMO_UPDATE, RMB_COVGROUP);

            CoverageGroupMaintenance objLst = null; //Application layer component			
            try
            {
                objLst = new CoverageGroupMaintenance(connectionString, userLogin, base.ClientId);
                p_objXmlOut = objLst.SaveAdditionalLangDesc(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("CoverageGroupMaintenanceAdaptor.SaveAdditionalLangDesc.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objLst = null;
            }
        }
        /// <summary>
        ///		This method is a wrapper to Riskmaster.Application.RMUtilities.CoverageGroupAdaptor.Get() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.Holidays.xml
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool GetSelectedAdditionalLangCode(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            if (!userLogin.IsAllowedEx(RMB_COVGROUP, RMPermissions.RMO_ACCESS))
                throw new PermissionViolationException(RMPermissions.RMO_ACCESS, RMB_COVGROUP);
            XmlElement objElement = null;

            CoverageGroupMaintenance objLst = null; //Application layer component			
            try
            {

                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//control[@name='CovGroupId']");
                if (objElement != null)
                {
                    if (objElement.InnerText == "")
                    {		//  Check security and fail if sufficient privilege doesn't exist
                        if (!userLogin.IsAllowedEx(RMB_COVGROUP, RMPermissions.RMO_CREATE))
                            throw new PermissionViolationException(RMPermissions.RMO_CREATE, RMB_COVGROUP);
                    }
                    else
                    {		//  Check security and fail if sufficient privilege doesn't exist
                        if (!userLogin.IsAllowedEx(RMB_COVGROUP, RMPermissions.RMO_VIEW))
                            throw new PermissionViolationException(RMPermissions.RMO_VIEW, RMB_COVGROUP);
                    }
                }
                objLst = new CoverageGroupMaintenance(connectionString, userLogin, base.ClientId);
                p_objXmlOut = objLst.GetSelectedAdditionalLangCode(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("CoverageGroupMaintenanceAdaptor.GetSelectedAdditionalLangCode.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objLst = null;
            }
        }
        #endregion
    }
}
