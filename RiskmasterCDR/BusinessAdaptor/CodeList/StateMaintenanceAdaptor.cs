using System;
using System.Text;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.StateMaintenance;
using Riskmaster.BusinessAdaptor.Common; 

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: StateMaintenanceAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 29-Dec-2005
	///* $Author	: Parag Sarin
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	///* Amendment  -->
	///  1.Date		: 7 Feb 06 
	///    Desc		: Replacing RMO_ACCESS use, and changing params of permission
	///				  violation exception so it can be correctly called. 
	///    Author	: Sumit
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for State Maintenance
	/// </summary>
	public class StateMaintenanceAdaptor:BusinessAdaptorBase
	{	//Nitesh: Security Constants
		private const int RMB_STATES = 18000;
//		private const int RMO_ACCESS = 0;
//		private const int RMO_VIEW = 1;
//		private const int RMO_UPDATE = 2;
//		private const int RMO_CREATE = 3;
//		private const int RMO_DELETE = 4;
		//Nitesh: Security Constants

		public StateMaintenanceAdaptor(){}
	
		#region Public Methods

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.StateAdaptor.Get() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.Holidays.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			// Nitesh 12Jan2006 Check security and fail if sufficient privilege doesn't exist
			if (!userLogin.IsAllowedEx(RMB_STATES, RMPermissions.RMO_ACCESS ))
				throw new PermissionViolationException(RMPermissions.RMO_ACCESS ,RMB_STATES);
			StateMaintenance   objLst=null; //Application layer component			
			try
			{
				objLst = new StateMaintenance(connectionString,base.ClientId);
				p_objXmlOut=objLst.Get(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException, Globalization.GetString("StateMaintenanceAdaptor.Get.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objLst = null;
			}
		}
		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.StateAdaptor.Get() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.Holidays.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool GetSelectedStateInfo(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{   // Nitesh 12Jan2006 Check security and fail if sufficient privilege doesn't exist
			if (!userLogin.IsAllowedEx(RMB_STATES, RMPermissions.RMO_ACCESS ))
				throw new PermissionViolationException(RMPermissions.RMO_ACCESS,RMB_STATES);
			XmlElement objElement=null;

			StateMaintenance   objLst=null; //Application layer component			
			try
			{
					
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//control[@name='RowId']");
				if(objElement!=null)
				{		if( objElement.InnerText == "")
						{		// Nitesh 12Jan2006 Check security and fail if sufficient privilege doesn't exist
								if (!userLogin.IsAllowedEx(RMB_STATES, RMPermissions.RMO_CREATE ))
								throw new PermissionViolationException(RMPermissions.RMO_CREATE,RMB_STATES);
						}
						else
						{		// Nitesh 12Jan2006 Check security and fail if sufficient privilege doesn't exist
								if (!userLogin.IsAllowedEx(RMB_STATES, RMPermissions.RMO_VIEW ))
								throw new PermissionViolationException(RMPermissions.RMO_VIEW,RMB_STATES);
						}
				}
				objLst = new StateMaintenance(connectionString,base.ClientId);
				p_objXmlOut=objLst.GetSelectedStateInfo(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException, Globalization.GetString("StateMaintenanceAdaptor.GetSelectedStateInfo.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objLst = null;
			}
		}
		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.StateAdaptor.Get() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.Holidays.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{	// Nitesh 12Jan2006 Check security and fail if sufficient privilege doesn't exist
			if (!userLogin.IsAllowedEx(RMB_STATES, RMPermissions.RMO_UPDATE ))
				throw new PermissionViolationException(RMPermissions.RMO_UPDATE,RMB_STATES);

			StateMaintenance   objLst=null; //Application layer component			
			try
			{
				objLst = new StateMaintenance(connectionString,base.ClientId);
				p_objXmlOut=objLst.Save(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException, Globalization.GetString("StateMaintenanceAdaptor.Save.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objLst = null;
			}
		}
		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.StateAdaptor.Get() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.Holidays.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool Delete(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			// Nitesh 12Jan2006 Check security and fail if sufficient privilege doesn't exist
			if (!userLogin.IsAllowedEx(RMB_STATES, RMPermissions.RMO_DELETE ))
				throw new PermissionViolationException(RMPermissions.RMO_DELETE,RMB_STATES);
			StateMaintenance   objLst=null; //Application layer component			
			try
			{
				objLst = new StateMaintenance(connectionString,base.ClientId);
				p_objXmlOut=objLst.Delete(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException, Globalization.GetString("StateMaintenanceAdaptor.Save.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objLst = null;
			}
		}
	
	
		#endregion
	}
}
