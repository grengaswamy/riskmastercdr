using System;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.EmailDocuments;
using Riskmaster.Application.ExecutiveSummaryReport;
using Riskmaster.Security;
using Riskmaster.Models;
using Riskmaster.Settings;

namespace Riskmaster.BusinessAdaptor

{
	///************************************************************** 
	///* $File				: ExecutiveSummaryAdaptor.cs 
	///* $Revision			: 1.0.0.0 
	///* $Creation Date		: 30-Aug-2005
	///* $Author			: Raman Bhatia
	///***************************************************************	
	
	/// <summary>	
	///	This class is used to call the application layer component for Email Documents.
	/// </summary>
	public class EmailDocumentsAdaptor:BusinessAdaptorBase

	{
		#region Constructor

		/// <summary>
		/// Class Constructor.
		/// </summary>
		public EmailDocumentsAdaptor()
		{			
		}

		#endregion

		#region Public Methods
		
		/// Name			: EmailDocuments
		/// Author			: Raman Bhatia
		/// Date Created	: 30-Aug-2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// Wrapper to EmailDocuments.SendEmail of application
		/// layer that send emails to specified users. It is also capable of attaching the executive 
		/// summary report.
		/// </summary>
		/// <param name="p_objXmlDocIn"> 
		///	Input paramters in xml format.
		///	Structure:
		///		<EmailDocuments>
		///			<EmailIds>
		///				Comma separated list of EmailIds.
		///			</EmailIds>
		///			<Subject>
		///				Subject of the mail
		///			</Subject>
		///			<Message>
		///				Body of the mail
		///			</Message>	
		///			<AttachExecutiveSummaryReport FormName AttachRecordid>
		///				true/false
		///			</AttachExecutiveSummaryReport>
		///			<AttachFileName>
		///				FileName with complete path
		///			</AttachFileName>
		///		</EmailDocuments>
		/// </param>
		/// <param name="p_objXmlDocOut"> 
		///		Empty
		///	</param>
		/// <param name="p_objErrOut">
		///	Collection of errors/messages.	
		///	</param>
		/// <returns>
		///		true - success
		///		false - failure
		///	</returns>
		public bool EmailDocuments(XmlDocument p_objXmlDocIn,ref XmlDocument p_objXmlDocOut,
			ref BusinessAdaptorErrors p_objErrOut)
		{
			bool bReturnValue = false;
			EmailDocuments objEmailDocuments = null;
            ExecutiveSummary objExecSumm = null;
			
			XmlNode objXmlNode = null;
			XmlElement objXmlElement = null;
			Random objRandom = new Random();
			
			string sEmailIdsTo = string.Empty; 
			string sEmailIdsCc = string.Empty; 
			string sMessage = string.Empty; 
			string sSubject = string.Empty;
			string sFormName = string.Empty;
			string sAttachFileName = string.Empty;
            	
			bool bAttachExecutiveSummaryReport = false;
			bool bAttachFile = false;
			int iAttachRecordId = 0;
			int iRandomNumber = 0;
			try
			{
                // MITS 27197 : Ishan 
                objExecSumm = new ExecutiveSummary(userID, connectionString,base.ClientId);  
                objExecSumm.m_UserLogin = base.userLogin; 
                // Ishan : end 

				objXmlNode = p_objXmlDocIn.SelectSingleNode("//EmailDocuments/EmailIdsTo");
				
				if(objXmlNode==null)
				{ 
					bReturnValue = false;
					p_objErrOut.Add("EmailDocumentsAdaptor.EmailDocuments.MissingInputParameter",
						Globalization.GetString("EmailDocumentsAdaptor.EmailDocuments.MissingEmailIdsNode",base.ClientId),BusinessAdaptorErrorType.Error);  //sonali
					return bReturnValue;
				}

				sEmailIdsTo = objXmlNode.InnerText; 
				
				objXmlNode = p_objXmlDocIn.SelectSingleNode("//EmailDocuments/EmailIdsCc");
				if(objXmlNode!=null)
					sEmailIdsCc = objXmlNode.InnerText; 
				
				objXmlNode = null;
				objXmlNode = p_objXmlDocIn.SelectSingleNode("//EmailDocuments/Message");
				
				if(objXmlNode==null)
				{ 
					bReturnValue = false;
					p_objErrOut.Add("EmailDocumentsAdaptor.EmailDocuments.MissingInputParameter",
                        Globalization.GetString("EmailDocumentsAdaptor.EmailDocuments.MissingMessageNode", base.ClientId),BusinessAdaptorErrorType.Error);  //sonali
					return bReturnValue;
				}

				sMessage = objXmlNode.InnerText; 

				objXmlNode = null;
				objXmlNode = p_objXmlDocIn.SelectSingleNode("//EmailDocuments/Subject");
				
				if(objXmlNode==null)
				{ 
					bReturnValue = false;
					p_objErrOut.Add("EmailDocumentsAdaptor.EmailDocuments.MissingInputParameter",
                        Globalization.GetString("EmailDocumentsAdaptor.EmailDocuments.MissingMessageNode", base.ClientId),BusinessAdaptorErrorType.Error);  //sonali
					return bReturnValue;
				}

				sSubject = objXmlNode.InnerText; 
								
				objXmlElement = null;
				objXmlElement = (XmlElement) p_objXmlDocIn.SelectSingleNode("//EmailDocuments/AttachExecutiveSummaryReport");
				
				if((objXmlElement!=null) && (objXmlElement.InnerText.ToLower()=="true"))
				{ 
					bAttachExecutiveSummaryReport = true;
					sFormName = objXmlElement.GetAttribute("FormName");
					iAttachRecordId = Conversion.ConvertStrToInteger(objXmlElement.GetAttribute("AttachRecordId"));
					if((sFormName=="") || (iAttachRecordId==0))
					{
						bReturnValue = false;
						p_objErrOut.Add("EmailDocumentsAdaptor.EmailDocuments.MissingInputParameter",
                            Globalization.GetString("EmailDocumentsAdaptor.EmailDocuments.MissingInputParameter", base.ClientId),BusinessAdaptorErrorType.Error);  
						return bReturnValue;
					}

				}

				//check whether an attachment path has been provided
				if(!bAttachExecutiveSummaryReport)
				{
                    objXmlNode = null;
					objXmlNode = p_objXmlDocIn.SelectSingleNode("//EmailDocuments/AttachFileName");
				
					if(objXmlNode!=null)
					{ 
						bAttachFile = true;
						sAttachFileName = objXmlNode.InnerText;
					}
				}
				
				//Creating the Executive Summary Report

				if(bAttachExecutiveSummaryReport)
				{
					//Generating a random number to get a unique filename
					
					do
					{
						iRandomNumber =objRandom.Next(100000);
                        sAttachFileName = String.Format(@"{0}\ExecutiveSummaryReport_{1}.pdf", RMConfigurator.UserDataPath, iRandomNumber);
					}
					while(File.Exists(sAttachFileName));
					
                  //  objExecSumm = new ExecutiveSummary(userID,connectionString);

					switch(sFormName)
					{
						case "event": objExecSumm.EventExecSumm(iAttachRecordId , sAttachFileName);
									  break;
						
						case "claim": objExecSumm.ClaimExecSumm(iAttachRecordId , sAttachFileName);
									  break;	

						default:	
                            //Parijat: Mits 11497 ----executive summary for admin Tracking
                                      if (iAttachRecordId != 0 && sAttachFileName != "" && sFormName!="")
                                objExecSumm.AdmTrackingExecSumm(iAttachRecordId,sFormName,sAttachFileName);
                            break;
					}

				}
				
				objEmailDocuments = new EmailDocuments(base.userLogin.objRiskmasterDatabase.DataSourceName , base.userLogin.LoginName, base.userLogin.Password,base.userLogin.UserId,base.userLogin.GroupId,base.ClientId);//sonali
				if(sAttachFileName!="")
					bReturnValue = objEmailDocuments.SendEmail(loginName , this.securityConnectionString , sEmailIdsTo , sEmailIdsCc , sSubject , sMessage , sAttachFileName);
				else
					bReturnValue = objEmailDocuments.SendEmail(loginName , this.securityConnectionString , sEmailIdsTo , sEmailIdsCc , sSubject , sMessage);

				//deleting temperory file
				if (!sAttachFileName.Trim().Equals(""))
				File.Delete(sAttachFileName);
			}

			catch(RMAppException p_objRMAppException)
			{
				bReturnValue = false;
				p_objErrOut.Add(p_objRMAppException,BusinessAdaptorErrorType.Error);   
				return bReturnValue;
			}
			catch(Exception p_objException)
			{
				bReturnValue = false;
                p_objErrOut.Add(p_objException, Globalization.GetString("EmailDocumentsAdaptor.EmailDocuments.Error", base.ClientId), BusinessAdaptorErrorType.Error);   
				return bReturnValue;
			}
			finally
			{
				objXmlNode = null;
                if (objEmailDocuments != null)
                {
                    objEmailDocuments.Dispose();
                }
			}

			return bReturnValue;
		}
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_objXmlDocIn"></param>
        /// <param name="p_objXmlDocOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool CheckOutlookSetting(DocumentType objDocument, out bool bIsUseOutlookEnabled,
            ref BusinessAdaptorErrors p_objErrors)
        {
            SysSettings objSettings = null;
            bool bReturnValue = false;
            bIsUseOutlookEnabled = false;

            try
            {
                objSettings = new SysSettings(connectionString, base.ClientId); //Ash - cloud
                if (objSettings.UseOutlook)
                {
                    bIsUseOutlookEnabled = true;
                }
                else
                {
                    bIsUseOutlookEnabled = false;
                }

                bReturnValue = true;
            }
            catch (InitializationException p_objInitializationException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInitializationException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInvalidValueException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (RMAppException p_objRMAppException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objRMAppException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException,
                    Globalization.GetString("EmailDocumentsAdaptor.GetOutlookSetting.Error", base.ClientId),BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }

            return bReturnValue;
        }
        /// <summary>
        /// This function generates executive summary file content for a 
        /// particular record
        /// </summary>
        /// <param name="objDocument">DocumentType object with record and form name</param>
        /// <param name="outDocument">DocumentType object with executive summary contents</param>
        /// <param name="p_objErrors">errors</param>
        /// <returns>true - success
        ///		false - failure</returns>
        public bool GetExecutiveSummaryFileDetails(DocumentType objDocument, out DocumentType outDocument,

            ref BusinessAdaptorErrors p_objErrors)
        {
            bool bReturnValue = false;
            
            ExecutiveSummary objExecSumm = null;
            Random objRandom = new Random();
            string sFormName = string.Empty;
            int iAttachRecordId = 0;
            int iRandomNumber = 0;
            string sAttachFileName = string.Empty;
            outDocument = new DocumentType();
            try
            {
                //Generating a random number to get a unique filename
                sFormName = objDocument.AttachTable;
                if (sFormName.ToLower().IndexOf("claim") > -1)
                {
                    sFormName = "claim";
                }
                iAttachRecordId = objDocument.AttachRecordId;
                
                do
                {
                    iRandomNumber = objRandom.Next(100000);
                    sAttachFileName = String.Format(@"{0}\ExecutiveSummaryReport_{1}.pdf", RMConfigurator.UserDataPath, iRandomNumber);
                }
                while (File.Exists(sAttachFileName));

                objExecSumm = new ExecutiveSummary(userID, connectionString, base.ClientId);// sbh
                objExecSumm.m_UserLogin = base.userLogin; //nnithiyanand 31959
                switch (sFormName)
                {
                    case "event": objExecSumm.EventExecSumm(iAttachRecordId, sAttachFileName);
                        break;

                    case "claim": objExecSumm.ClaimExecSumm(iAttachRecordId, sAttachFileName);
                        break;

                    default:
                        //Parijat: Mits 11497 ----executive summary for admin Tracking
                        if (iAttachRecordId != 0 && sAttachFileName != "" && sFormName != "")
                            objExecSumm.AdmTrackingExecSumm(iAttachRecordId, sFormName, sAttachFileName);
                        break;
                }
                if (!sAttachFileName.Trim().Equals("") && File.Exists(sAttachFileName))
                {
                    outDocument.FileContents = File.ReadAllBytes(sAttachFileName);
                    outDocument.FileName = "ExecutiveSummaryReport.pdf";
                    //deleting temperory file
                   File.Delete(sAttachFileName);
                }
                bReturnValue = true;
            }
            catch (InitializationException p_objInitializationException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInitializationException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInvalidValueException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (RMAppException p_objRMAppException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objRMAppException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException,
                    Globalization.GetString("EmailDocumentsAdaptor.EmailDocuments.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            
            return bReturnValue;
        }
		
		/// Name			: GetUsers
		/// Author			: Raman Bhatia
		/// Date Created	: 30-Aug-2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// Wrapper to EmailDocuments.GetUsers of Application
		/// layer that returns users in form of XML
		/// </summary>
		/// <param name="p_objXmlDocIn"> 
		///	Input paramters in xml format.
		///	Structure:
		///		<GetUsers/>
		/// </param>
		/// <param name="p_objXmlDocOut"> 
		///	Method output in xml format.
		///	Structure:
		///		<GetUsersResult>
		///			<data>
		///				<user firstname="" lastname="" userid="">
		///			</data>
		///		</GetUsersResult>
		///	</param>
		/// <param name="p_objErrors">
		///	Collection of errors/messages.	
		///	</param>
		/// <returns>
		///		true - success
		///		false - failure
		///	</returns>
		
		public bool GetUsers(XmlDocument p_objXmlDocIn, ref XmlDocument p_objXmlDocOut, 
			ref BusinessAdaptorErrors p_objErrors)
		{
			bool bReturnValue = false;
			string sCurrentUserName = "";

			XmlElement objRootElement = null;
			
			EmailDocuments objEmailDocuments=null;

			String objXmlDocOut;		

			try	
			{
				string sCurrentDataSourceName = this.userLogin.objRiskmasterDatabase.DataSourceName;
				sCurrentUserName = this.userLogin.LoginName; 

				objEmailDocuments = new EmailDocuments(base.userLogin.objRiskmasterDatabase.DataSourceName , base.userLogin.LoginName, base.userLogin.Password,base.userLogin.UserId,base.userLogin.GroupId,base.ClientId);//sonali
				objEmailDocuments.GetUsers(sCurrentDataSourceName,out objXmlDocOut);

				objRootElement = p_objXmlDocOut.CreateElement("GetUsersResult"); 
				objRootElement.InnerXml = objXmlDocOut;
				
				//adding attribute LoginUser to retrieve the current user..raman bhatia
				objRootElement.SetAttribute("LoginUser" , sCurrentUserName);
				
				//adding attribute to retrieve the current date and cuurent time..raman bhatia
				objRootElement.SetAttribute("CurrentDate" , String.Format("{0:MM/dd/yyyy}", DateTime.Now ));
				objRootElement.SetAttribute("CurrentTime" , String.Format("{0:hh:mm tt}", DateTime.Now ));

				p_objXmlDocOut.AppendChild(objRootElement);
				
				bReturnValue = true;

			}
			catch(InitializationException p_objInitializationException)
			{
				bReturnValue = false;
				p_objErrors.Add(p_objInitializationException,BusinessAdaptorErrorType.Error);   
				return bReturnValue;
			}
			catch(RMAppException p_objRMAppException)
			{
				bReturnValue = false;
				p_objErrors.Add(p_objRMAppException,BusinessAdaptorErrorType.Error);   
				return bReturnValue;
			}
			catch(Exception p_objException)
			{
				bReturnValue = false;
                p_objErrors.Add(p_objException, Globalization.GetString("EmailDocumentsAdaptor.GetUsers.Error", base.ClientId), BusinessAdaptorErrorType.Error);   
				return bReturnValue;
			}
			finally
			{
				objRootElement =  null;
                if (objEmailDocuments != null)
                {
                    objEmailDocuments.Dispose();
                }
			}

			return bReturnValue;
		}

		#endregion

	}
}

