﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Text;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.FNOLReserve;
using Riskmaster.Security;
namespace Riskmaster.BusinessAdaptor
{
    ///************************************************************** 
    ///* $File				: FNOLReserveAdaptor.cs 
    ///* $Revision			: 1.0.0.0 
    ///* $Creation Date		: 02-15-2013
    ///* $Author			: Sonam Pahariya
    ///***************************************************************	
    /// <summary>	
    ///	FNOLReserveAdaptor definition 
    /// </summary>
    public class FNOLReserveAdaptor: BusinessAdaptorBase
    {
        /// <summary>
        ///		This method is a wrapper to Riskmaster.Application.FNOLClaimReserve.Get() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results        
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            FNOLClaimReserve objFNOLClmRes = null;
            try
            {
                objFNOLClmRes = new FNOLClaimReserve(connectionString, userLogin,base.ClientId); //dvatsa-JIRA 3012               
                p_objXmlOut = objFNOLClmRes.Get(p_objXmlIn);
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("FNOLReserveAdaptor.Get.Error",base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-JIRA 3012
                return false;
            }
            finally
            {
                //objFundsDormancy = null;
            }
        }

        /// <summary>
        ///		This method is a wrapper to Riskmaster.Application.FNOLClaimReserve.Save() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results        
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            FNOLClaimReserve objFNOLClmRes = null;			
            try
            {
                objFNOLClmRes = new FNOLClaimReserve(connectionString, userLogin,base.ClientId);//dvatsa-JIRA 3012
                p_objXmlOut = objFNOLClmRes.Save(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("FNOLReserveAdaptor.Save.Error",base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-JIRA 3012
                return false;
            }
            finally
            {
                //objFundsDormancy = null;
            }
        }

        /// <summary>
        ///		This method is a wrapper to Riskmaster.Application.FNOLClaimReserve.GetResSetUp() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results        
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool GetResSetUp(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            FNOLClaimReserve objFNOLClmRes = null;            
            int iClaimId = 0;
            try
            {
                objFNOLClmRes = new FNOLClaimReserve(connectionString, userLogin,base.ClientId);//dvatsa-JIRA 3012
                iClaimId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//ClaimID").InnerText);                
                p_objXmlOut = objFNOLClmRes.GetResSetUp(iClaimId);
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("FNOLReserveAdaptor.GetResSetUp.Error",base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-JIRA 3012
                return false;
            }
            finally
            {
                //objFundsDormancy = null;
            }
        }

        /// <summary>
        ///		This method is a wrapper to Riskmaster.Application.FNOLClaimReserve.SaveResSetUp() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results        
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool SaveResSetUp(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            FNOLClaimReserve objFNOLClmRes = null;
            try
            {
                objFNOLClmRes = new FNOLClaimReserve(connectionString, userLogin,base.ClientId);//dvatsa-JIRA 3012
                p_objXmlOut = objFNOLClmRes.SaveResSetUp(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("FNOLReserveAdaptor.SaveResSetUp.Error",base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-JIRA 3012
                return false;
            }
            finally
            {
                //objFundsDormancy = null;
            }
        }


        /// <summary>
        ///		This method is a wrapper to Riskmaster.Application.FNOLClaimReserve.Delete() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results        
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool Delete(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            FNOLClaimReserve objFNOLClmRes = null;
            try
            {
                objFNOLClmRes = new FNOLClaimReserve(connectionString, userLogin,base.ClientId);//dvatsa-JIRA 3012
                p_objXmlOut = objFNOLClmRes.Delete(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("FNOLReserveAdaptor.Delete.Error",base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-JIRA 3012
                return false;
            }
            finally
            {
                //objFundsDormancy = null;
            }
        }
    }
}
