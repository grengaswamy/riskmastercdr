﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using System.Xml;

namespace Riskmaster.BusinessAdaptor
{
    /// <summary>
    /// GridPreferenceAdaptor
    /// </summary>
    public class GridPreferenceAdaptor : BusinessAdaptorBase
    {

        public bool SaveUserHeaderAndPreference(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            
            string UserHeaderPrefJson = String.Empty;
            string sPageName = "";
            string sGridId = "";
            try
            {
                if (p_objXmlIn.SelectSingleNode("//PageName") != null)
                    sPageName = p_objXmlIn.SelectSingleNode("//PageName").InnerText.Trim();
                if (p_objXmlIn.SelectSingleNode("//GridId") != null)
                    sGridId = p_objXmlIn.SelectSingleNode("//GridId").InnerText.Trim();
                if (p_objXmlIn.SelectSingleNode("//UserPref") != null)
                UserHeaderPrefJson = p_objXmlIn.SelectSingleNode("//UserPref").InnerText.Trim();
                UserHeaderPrefJson = CommonFunctions.HTMLCustomDecode(UserHeaderPrefJson);
                Riskmaster.Common.GridCommonFunctions.SaveUserHeaderAndPreference(UserHeaderPrefJson, m_userLogin.UserId, base.ClientId, sPageName, m_userLogin.DatabaseId, sGridId);
            }
            catch (Exception p_objException)
            {
                p_objErr.Add(p_objException, Globalization.GetString("GridPreferenceAdaptor.SaveUserPreference.Err", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            return true;
        }

        public bool RestoreDefaults(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {            
            string sPageName = "";
            string sGridId = "";
            try
            {
                if (p_objXmlIn.SelectSingleNode("//PageName") != null)
                    sPageName = p_objXmlIn.SelectSingleNode("//PageName").InnerText.Trim();
                if (p_objXmlIn.SelectSingleNode("//GridId") != null)
                    sGridId = p_objXmlIn.SelectSingleNode("//GridId").InnerText.Trim();
                Riskmaster.Common.GridCommonFunctions.DeleteUserPreference(m_userLogin.UserId, base.ClientId, sPageName, m_userLogin.DatabaseId, sGridId);
            }
            catch (Exception p_objException)
            {
                p_objErr.Add(p_objException, Globalization.GetString("GridPreferenceAdaptor.RestoreDefaults.Err", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            return true;
        }
    }
}
