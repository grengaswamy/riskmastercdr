using System;
using System.Xml ;

using Riskmaster.Common ;
using Riskmaster.Security ;
using Riskmaster.ExceptionTypes ;
using Riskmaster.BusinessAdaptor.Common ;
using Riskmaster.Application.EventExplorer ;
using Riskmaster.DataModel;


namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: EventExplorerAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 23-Feb-2005
	///* $Author	: Vaibahv Kaushik
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for Event Explorer which 
	///	contains methods to generate RiskMaster Quick Summary Report.	
	/// </summary>
	public class EventExplorerAdaptor : BusinessAdaptorBase
	{	private const int RMB_DISPLAY_QUICK_SUMMARY = 15000;
        //Bharani - MITS : 31858 - Change 1 - Start
        static XmlElement objLOB = null; 
        private const int RMB_VIEW = 1;
        //Bharani - MITS : 31858 - Change 1 - End
		#region Constructor
		/// Name		: EventExplorerAdaptor
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/22/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		///	<summary>
		/// Constructor, initializes the variables to the default value
		/// </summary>
		public EventExplorerAdaptor( ) : base()
		{						
		}		
		#endregion 

		#region Public Methods
		/// Name		: GetData
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/22/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///		This method is a wrapper to EventExplorerManager.GetData method.
		///		This method will return the XML contains the data for the input query. 		
		/// </summary>
		/// <param name="p_objXmlIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<EventExplorer>
		///				<EventId>Event Id</EventId>
		///				<Key>Key string</Key>
		///			</EventExplorer>
		///		</Document> 
		/// 
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document</param>
		/// 
		///		The structure of output XML document would be:
		///		<data type="list" title="OSHA Details - 1101">
		///			<header>
		///				<col /> 
		///				<col /> 
		///			</header>
		///			<rows>
		///				<row>
		///					<col>Date Carrier Notified:</col> 
		///					<col /> 
		///				</row>
		///				<row>
		///					<col>Treatment Given:</col> 
		///					<col>No</col> 
		///				</row>
		///				<row>
		///					<col>Release Signed:</col> 
		///					<col>No</col> 
		///				</row>		
		///			</rows>
		///		</data>
		///		
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		public bool GetData( XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
            string sNodeName = "";
            string sClassName = "";
            int iClaimID = 0;
            int iRequestedInfoID = 0;
            int iRequestedInfoChildID = 0;
			//Nitesh 04/24/06 Tr 2311 Starts
			if (!userLogin.IsAllowedEx(RMB_DISPLAY_QUICK_SUMMARY,0 ))
				throw new PermissionViolationException(RMPermissions.RMO_DISPLAY_QUICK_SUMMARY,RMB_DISPLAY_QUICK_SUMMARY);
			//Nitesh 04/24/06 Tr 2311 Ends
			EventExplorerManager objEventExplorerManager = null;
			XmlElement objTargetElement = null;

			int iEventId = 0;
            int iClaimId = 0;// tmalhotra2
			string sKey = string.Empty ;						

			try
			{
				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//EventId" );
				if( objTargetElement == null )
				{
					p_objErrOut.Add( "EventIdMissing",Globalization.GetString("EventIdMissing",base.ClientId),BusinessAdaptorErrorType.Error);
					return false;
				}
				iEventId = Conversion.ConvertStrToInteger( objTargetElement.InnerText );

				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//Key" );
				if( objTargetElement == null )
				{
					p_objErrOut.Add( "KeyMissing",Globalization.GetString("KeyMissing",base.ClientId),BusinessAdaptorErrorType.Error);
					return false;
				}
				sKey = objTargetElement.InnerText ;

                //tmalhotra2 MITS-29787 Re# 5.1.13
                objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//ClaimId");
                if (objTargetElement == null)
                {
                    p_objErrOut.Add("ClaimIdMissing", Globalization.GetString("ClaimIdMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                iClaimId = Conversion.ConvertStrToInteger(objTargetElement.InnerText);

                DataModelFactory m_objDataModelFactory = new DataModelFactory(base.userLogin, base.ClientId);
				objEventExplorerManager = new EventExplorerManager( userLogin.objRiskmasterDatabase.DataSourceName , userLogin.LoginName ,userLogin.Password , base.ClientId);
                objEventExplorerManager.m_UserLogin = base.userLogin; //Aman ML Change
                objEventExplorerManager.SplitKey(sKey, ref sNodeName, ref sClassName, ref iClaimID, ref iRequestedInfoID, ref iRequestedInfoChildID);
                Claim objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                objClaim.MoveTo(iClaimID);
                int iLob = 0;
                if (objClaim != null)
                {
                    iLob = objClaim.LineOfBusCode;
                }
                //Bharani - MITS : 31858 - Change 2 - Start
                string[] sNode = sKey.Split(',');
                if(sNode[0].Equals("name=adjuster"))
                {
                    if (sKey.Contains("subid=") && iLob != 0)
                    {
                        switch (iLob)
                        {
                            case 241:
                                if (!userLogin.IsAllowedEx(RMPermissions.RMO_GC_ADJUSTERDATEDTEXT, RMB_VIEW))
                                throw new PermissionViolationException(RMPermissions.RMO_VIEW, RMPermissions.RMO_GC_ADJUSTERDATEDTEXT);
                            break;
                            case 242:
                            if (!userLogin.IsAllowedEx(RMPermissions.RMO_VA_ADJUSTERDATEDTEXT, RMB_VIEW))
                                throw new PermissionViolationException(RMPermissions.RMO_VIEW, RMPermissions.RMO_VA_ADJUSTERDATEDTEXT);
                            break;
                            case 243:
                            if (!userLogin.IsAllowedEx(RMPermissions.RMO_WC_ADJUSTERDATEDTEXT, RMB_VIEW))
                                throw new PermissionViolationException(RMPermissions.RMO_VIEW, RMPermissions.RMO_VA_ADJUSTERDATEDTEXT);
                            break;
                            case 844:
                            if (!userLogin.IsAllowedEx(RMPermissions.RMO_DI_ADJUSTERDATEDTEXT, RMB_VIEW))
                                throw new PermissionViolationException(RMPermissions.RMO_VIEW, RMPermissions.RMO_DI_ADJUSTERDATEDTEXT);
                            break;
                            case 845:
                            if (!userLogin.IsAllowedEx(RMPermissions.RMO_PC_ADJUSTERDATEDTEXT, RMB_VIEW))
                                throw new PermissionViolationException(RMPermissions.RMO_VIEW, RMPermissions.RMO_PC_ADJUSTERDATEDTEXT);
                            break;
                        }
                    }
                }
                //Bharani - MITS : 31858 - Change 2 - End

	
                p_objXmlOut = objEventExplorerManager.GetData(iEventId, sKey, iClaimId);

                //nkaranam2 - 34643
                if (!userLogin.IsAllowedEx(FormBase.RMO_WC_EMPLOYEE, FormBase.RMO_VIEW_SSN))
                {
                    if (p_objXmlOut.SelectNodes("/data/rows/row[col='Social Security #:']").Count != 0)
                    {
                        XmlNodeList xmlNL = (p_objXmlOut.SelectNodes("/data/rows/row[col='Social Security #:']"));
                        foreach (XmlNode xmlN in xmlNL)
                            xmlN.ParentNode.RemoveChild(xmlN);
                    }
                }
                //nkaranam2 - 34643

				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("EventExplorerAdaptor.GetData.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objEventExplorerManager = null;
				objTargetElement = null;
			}
		}

		/// Name		: GetEventSummaryXml
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/22/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///		This method is a wrapper to EventExplorerManager.GetEventSummaryXml method.
		///		This method will return the quick summary tree Xml for the given EventId. 		
		/// </summary>
		/// <param name="p_objXmlIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<EventExplorer>
		///				<EventId>Event Id</EventId>				
		///			</EventExplorer>
		///		</Document> 
		/// 
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document</param>
		///		
		///		The structure of output XML document would be:
		///		<eventsummary>
		///			<node key="name=event,eventid=245" text="Event - EVTTH19850026XLS (Event Date: 9/14/1985)">
		///				<node key="name=eventfinancials,eventid=245" text="Current Financials" /> 
		///				<node key="name=eventdiaries,eventid=245" text="Diaries" /> 
		///				<node key="name=eventdetails,eventid=245" text="Event Details" /> 
		///				<node key="name=eventfollowup,eventid=245" text="Follow Up" /> 
		///				<node key="name=eventlocation,eventid=245" text="Location Info" /> 
		///				<node key="name=eventosha,eventid=245" text="OSHA Information" /> 
		///				<node key="name=claim,eventid=245,claimid=257" text="General Claim Nr. TTH19850026XLS">
		///					<node key="name=notes,eventid=245,claimid=257" text="Claim Notes" /> 
		///					<node key="name=diaries,eventid=245,claimid=257" text="Diaries" /> 
		///					<node key="name=currentreserves,eventid=245,claimid=257" text="Current Financials">
		///						<node key="name=paymenthist,eventid=245,claimid=257" text="Payment History">
		///							<node key="name=bypayee,eventid=245,claimid=257" text="By Payee" /> 
		///							<node key="name=byreserve,eventid=245,claimid=257" text="By Reserve Type" /> 
		///							<node key="name=bybenefit,eventid=245,claimid=257" text="By Benefit, Summary" /> 
		///							<node key="name=bytranstype,eventid=245,claimid=257" text="By Transaction Type" /> 
		///						</node>
		///						<node key="name=futurepayments,eventid=245,claimid=257" text="Future Auto Payments" /> 
		///					</node>
		///					<node key="name=claimants,eventid=245,claimid=257" text="Claimant">
		///						<node key="name=claimant,eventid=245,claimid=257,subid=204" text="Andros, Phyllis" /> 
		///					</node>
		///				</node>
		///			</node>
		///		</eventsummary>
		///
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		public bool GetEventSummaryXml( XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			//Nitesh 04/24/06 Tr 2311 Starts
			if (!userLogin.IsAllowedEx(RMB_DISPLAY_QUICK_SUMMARY,0))
				throw new PermissionViolationException(RMPermissions.RMO_DISPLAY_QUICK_SUMMARY ,RMB_DISPLAY_QUICK_SUMMARY);
			//Nitesh 04/24/06 Tr 2311 Ends
			EventExplorerManager objEventExplorerManager = null;
			XmlElement objTargetElement = null;

			int iEventId = 0;			
			int iClaimId = 0;			
			string sLob = "0";	
			try
			{
				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//EventId" );
                objLOB = (XmlElement)p_objXmlIn.SelectSingleNode("//Lob"); //Bharani - MITS : 31858 - Change 3
				if( objTargetElement == null )
				{
                    p_objErrOut.Add("EventIdMissing", Globalization.GetString("EventIdMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				iEventId = Conversion.ConvertStrToInteger( objTargetElement.InnerText );
				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//ClaimId" );
				if( objTargetElement == null )
				{
                    p_objErrOut.Add("ClaimIdMissing", Globalization.GetString("EventIdMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				
				
				iClaimId= Conversion.ConvertStrToInteger( objTargetElement.InnerText );
				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//Lob" );
				if( objTargetElement == null )
				{
                    p_objErrOut.Add("Lob", Globalization.GetString("EventIdMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				
				
				sLob=( objTargetElement.InnerText );
				objEventExplorerManager = new EventExplorerManager( userLogin.objRiskmasterDatabase.DataSourceName , userLogin.LoginName ,userLogin.Password, base.ClientId );
                objEventExplorerManager.m_UserLogin = base.userLogin;
				p_objXmlOut = objEventExplorerManager.GetEventSummaryXml( iEventId,iClaimId,sLob );                
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("EventExplorerAdaptor.GetEventSummaryXml.Error",base.ClientId),BusinessAdaptorErrorType.Error);//sonali for cloud
				return false;
			}
			finally
			{
				objEventExplorerManager = null;
				objTargetElement = null;
			}
		}

		/// Name		: GetPayeeDetails
		/// Author		: Vaibhav Kaushik
		/// Date Created: 04/07/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///		This method is a wrapper to EventExplorerManager.GetPayeeDetails method.
		///		This method will return the XML contains Payee Details. 		
		/// </summary>
		/// <param name="p_objXmlIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<PayeeDetail>
		///				<ClaimId>Claim Id</ClaimId>
		///				<PayeeId>Payee Id</PayeeId>
		///				<SortCol>Sort Column</SortCol>
		///				<Ascending>Ascending Flag</Ascending>
		///			</PayeeDetail>
		///		</Document> 
		/// 
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document</param>
		/// 
		///		The structure of output XML document would be:
		///		<payeeinfo>
		///			<row>
		///				<CtlNumber>fghfghfghfgh</CtlNumber> 
		///				<TransNumber>0</TransNumber> 
		///				<TransDate>9/30/2004</TransDate> 
		///				<Amount>$8,440.00</Amount> 
		///				<Date>-</Date> 
		///				<InvoiceNumber /> 
		///				<CodeDesc>MJ</CodeDesc> 
		///				<SplitAmount>$450.00</SplitAmount> 
		///			</row>
		///			- <row>
		///				<CtlNumber>"</CtlNumber> 
		///				<TransNumber>"</TransNumber> 
		///				<TransDate>"</TransDate> 
		///				<Amount>"</Amount> 
		///				<Date>-</Date> 
		///				<InvoiceNumber /> 
		///				<CodeDesc>MJ</CodeDesc> 
		///				<SplitAmount>$4,535.00</SplitAmount> 
		///			</row>
		///			<TotalAll>$8,440.00</TotalAll> 
		///		</payeeinfo>
		///		
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		public bool GetPayeeDetails( XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			EventExplorerManager objEventExplorerManager = null;
			XmlElement objTargetElement = null;

			int iClaimId = 0 ;
			int iPayeeId = 0 ; 
			string sSortCol = string.Empty ; 
			bool bAscending = false ;

			try
			{
				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//ClaimId" );
				if( objTargetElement == null )
				{
					p_objErrOut.Add("ClaimIdMissing",Globalization.GetString("ClaimIdMissing",base.ClientId),BusinessAdaptorErrorType.Error);//sonali
					return false;
				}
				iClaimId = Conversion.ConvertStrToInteger( objTargetElement.InnerText );

				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//PayeeId" );
				if( objTargetElement == null )
				{
					p_objErrOut.Add("PayeeIdMissing",Globalization.GetString("PayeeIdMissing",base.ClientId),BusinessAdaptorErrorType.Error);//sonali
					return false;
				}
				iPayeeId = Conversion.ConvertStrToInteger( objTargetElement.InnerText );

				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//SortCol" );
				if( objTargetElement == null )
				{
                    p_objErrOut.Add("SortColMissing", Globalization.GetString("SortColMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				sSortCol = objTargetElement.InnerText ;

				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//Ascending" );
				if( objTargetElement == null )
				{
                    p_objErrOut.Add("AscendingMissing", Globalization.GetString("AscendingMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				bAscending = Conversion.ConvertStrToBool( objTargetElement.InnerText );
	
				objEventExplorerManager = new EventExplorerManager( userLogin.objRiskmasterDatabase.DataSourceName , userLogin.LoginName ,userLogin.Password, base.ClientId );
				p_objXmlOut = objEventExplorerManager.GetPayeeDetails( iClaimId, iPayeeId, sSortCol, bAscending );
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("EventExplorerAdaptor.GetPayeeDetails.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sonali
				return false;
			}
			finally
			{
				objEventExplorerManager = null;
				objTargetElement = null;
			}
		}

		#endregion 
	}
}
