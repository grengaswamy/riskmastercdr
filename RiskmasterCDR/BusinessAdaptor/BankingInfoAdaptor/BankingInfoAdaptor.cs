﻿using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.Application.BankingInfoManager;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using System.IO;
using Riskmaster.Db;

namespace Riskmaster.BusinessAdaptor.BankingInfoAdaptor
{
    class BankingInfoAdaptor : BusinessAdaptorBase
    {
        public BankingInfoAdaptor()
        {
        }

        public bool GetBankingStatusHistory(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            BankingInfoManager objBankingInfo = null;
            try
            {
                objBankingInfo = new BankingInfoManager(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, base.ClientId);//rkaur27
                p_objXmlOut = objBankingInfo.GetClaimStatusHistory(p_objXmlIn);
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("BankingInfoAdaptor.GetClaimStatusHistory.Error", base.ClientId), BusinessAdaptorErrorType.Error);//rkaur27
                return false;
            }
            finally
            {
                objBankingInfo = null;
            }
            return true;
        }



    }
}
