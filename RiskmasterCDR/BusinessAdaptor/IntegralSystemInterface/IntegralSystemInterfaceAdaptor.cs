﻿using System;
using System.Xml;
using System.Data;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using System.IO;
using Riskmaster.Security;
using System.Xml.Linq;
using Riskmaster.Models;
using Riskmaster.Application.IntegralSystemInterface;
using System.Xml.XPath;
using Riskmaster.Settings;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
    public class IntegralSystemInterfaceAdaptor : BusinessAdaptorBase
    {
        public IntegralSystemInterfaceAdaptor() { }

        public bool SavePolicyAllData(ExternalPolicyData oExternalPolicyData, ref PolicySaveRequest oSaveResponse, ref BusinessAdaptorErrors p_objErrOut)
        {
            XElement oDataModelTemplate = null;
            XElement oDataModelElement = null;
            int iCodeId = 0;
            oSaveResponse.Result = false;
            IntegralSystemInterface objManager = null;
            SysSettings objSettings = null;
            int iInsuredEid = 0;
            int iInsurerEid = 0;
            int iPolicyId = 0;
            int iEntityID = 0;
            int iAddressId = 0;
            bool bSuccess = false;
            string sEntityRole = string.Empty;
            string sRoleCode = string.Empty;
            bool bIsNewPolicy = true, bIsOrphan = true;
            string sAgentNumber = string.Empty;
            string sAgentName = string.Empty;
            string sPolicyLOB = string.Empty;
            string sPolCompny = string.Empty;
            string sPolicyName = string.Empty;
            DataTable dtPolicyData;
            string sShortCd = string.Empty;
            string sStatesTableNm;
            string sEntityDataModelData = string.Empty;
            try
            {
                if (oExternalPolicyData != null)
                {
                    objManager = new IntegralSystemInterface(m_connectionString,base.ClientId);
                    oDataModelTemplate = XElement.Parse(GetXMLTemplateByType("Policy", ref p_objErrOut));
                    if (oDataModelTemplate != null)
                    {
                        if (!string.IsNullOrEmpty(oExternalPolicyData.PolicyData.StatusCode))
                        {
                            iCodeId = (new IntegralSystemInterface(connectionString, base.ClientId)).GetRMXCodeIdFromPSMappedCode(oExternalPolicyData.PolicyData.StatusCode, "POLICY_STATUS", "Policy Status on GetPolicySaveFormattedResult", oExternalPolicyData.PolicySystemID.ToString());
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/PolicyStatusCode");
                            if (oDataModelElement != null)
                                oDataModelElement.Attribute("codeid").Value = iCodeId.ToString();
                        }
                        if (!string.IsNullOrEmpty(oExternalPolicyData.PolicyData.Currency))
                        {
                            iCodeId = (new IntegralSystemInterface(connectionString, base.ClientId)).GetRMXCodeIdFromPSMappedCode(oExternalPolicyData.PolicyData.Currency, "CURRENCY_TYPE", "Currency on GetPolicySaveFormattedResult", oExternalPolicyData.PolicySystemID.ToString());
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/CurrencyCode");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = iCodeId.ToString();
                        }
                        if (!string.IsNullOrEmpty(oExternalPolicyData.PolicyData.Symbol))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/PolicySymbol");
                            if (oDataModelElement != null)
                            {
                                oDataModelElement.Value = oExternalPolicyData.PolicyData.Symbol;
                                sPolicyName = oExternalPolicyData.PolicyData.Symbol;
                            }
                        }
                        if (!string.IsNullOrEmpty(oExternalPolicyData.PolicyData.PolicyNumber))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/PolicyNumber");
                            if (oDataModelElement != null)
                            {
                                oDataModelElement.Value = oExternalPolicyData.PolicyData.PolicyNumber;
                                sPolicyName = sPolicyName + " " + oExternalPolicyData.PolicyData.PolicyNumber;
                            }
                        }
                        if (!string.IsNullOrEmpty(oExternalPolicyData.PolicyData.RenewalNumber))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/Module");
                            if (oDataModelElement != null)
                            {
                                oDataModelElement.Value = oExternalPolicyData.PolicyData.RenewalNumber;
                                sPolicyName = sPolicyName + " " + oExternalPolicyData.PolicyData.RenewalNumber;
                            }
                        }
                        if (!sPolicyName.Equals(""))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/PolicyName");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = sPolicyName;
                            oSaveResponse.PolicyName = sPolicyName;
                        }
                        if (!string.IsNullOrEmpty(oExternalPolicyData.PolicyData.EffectiveDate))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/EffectiveDate");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = oExternalPolicyData.PolicyData.EffectiveDate;
                        }
                        if (!string.IsNullOrEmpty(oExternalPolicyData.PolicyData.ExpiryDate))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/ExpirationDate");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = oExternalPolicyData.PolicyData.ExpiryDate;
                        }
                        if (!string.IsNullOrEmpty(oExternalPolicyData.PolicyData.LOB))
                        {
                            sPolicyLOB = oExternalPolicyData.PolicyData.LOB;
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/PolicyLOB");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = sPolicyLOB;
                        }
                        if (!string.IsNullOrEmpty(oExternalPolicyData.PolicyData.PrimiumData.GrossPremium))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/Premium");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = oExternalPolicyData.PolicyData.PrimiumData.GrossPremium;
                        }
                        if (!string.IsNullOrEmpty(oExternalPolicyData.PolicyData.MasterCompany))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/MasterCompany");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = oExternalPolicyData.PolicyData.MasterCompany;
                        }
                        if (!string.IsNullOrEmpty(oExternalPolicyData.PolicyData.Country))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/LocationCompany");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = oExternalPolicyData.PolicyData.Country;
                        }
                        if (!string.IsNullOrEmpty(oExternalPolicyData.PolicyData.ServiceBranch))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/BranchCode");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = oExternalPolicyData.PolicyData.ServiceBranch;
                        }
                        foreach (EntityData oAgentData in oExternalPolicyData.PolicyData.Entities)
                        {
                            if (oAgentData.Role == "Agent")
                            {
                                if (oAgentData.TypeCode == "P")
                                    sAgentName = oAgentData.FirstName + oAgentData.LastName;
                                else
                                    sAgentName = oAgentData.BusinessName;
                                sAgentNumber = oAgentData.AgentNumber; // sanoopsharma field renamed for integral
                                break;
                            }
                        }
                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/PrimaryPolicyFlg");
                        if (oDataModelElement != null)
                            oDataModelElement.Value = "True";
                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/PolicySystemId");
                        if (oDataModelElement != null)
                            oDataModelElement.Value = oExternalPolicyData.PolicySystemID.ToString();
                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/TriggerClaimFlag");
                        if (oDataModelElement != null)
                        {
                            objSettings = new SysSettings(m_connectionString,base.ClientId);
                            if (objSettings.PolicyCvgType.ToString() == "0")
                                oDataModelElement.Value = "True";
                            else
                                oDataModelElement.Value = "False";
                        }
                    }
                    objManager = new IntegralSystemInterface(m_userLogin,base.ClientId);
                    iPolicyId = objManager.CheckPolicyDuplication(oExternalPolicyData.ClaimID, oExternalPolicyData.PolicyData.PolicyNumber, oExternalPolicyData.PolicyData.Symbol, oExternalPolicyData.PolicyData.RenewalNumber, oExternalPolicyData.PolicyData.MasterCompany, oExternalPolicyData.PolicySystemID, ref bIsOrphan);
                    if (iPolicyId > 0)
                    {
                        bIsNewPolicy = false;
                        if (bIsOrphan)
                        {
                            objManager.OrphanPolicyCleanUp(iPolicyId);
                        }
                    }
                    else
                        bIsNewPolicy = true;
                    foreach (EntityData oEntityData in oExternalPolicyData.PolicyData.Entities)
                    {
                        if (Constants.listInsured.Contains(oEntityData.Role.ToLower()))
                        {
                            sEntityDataModelData = GetEntitySaveFormattedResult(oEntityData, oExternalPolicyData.PolicySystemID, string.Empty, ref p_objErrOut);
                            if (sEntityDataModelData != "")
                            {
                                iInsuredEid = objManager.SaveEntity(sEntityDataModelData, oEntityData.Role.ToLower(), ref iAddressId, ref sEntityRole);
                                sEntityDataModelData = string.Empty;
                                break;
                            }
                        }
                    }
                    sEntityDataModelData = GetInsurerSaveFormattedResult(oExternalPolicyData, ref p_objErrOut, oExternalPolicyData.PolicySystemID, sPolCompny);
                    if (sEntityDataModelData != "")
                        iInsurerEid = objManager.SaveInsurerEntity(base.GetSessionObject().SessionId, sEntityDataModelData);
                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/PolicyXInsured");
                    if (oDataModelElement != null)
                        oDataModelElement.Attribute("codeid").Value = iInsuredEid.ToString();
                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/InsurerEid");
                    if (oDataModelElement != null)
                        oDataModelElement.Attribute("codeid").Value = iInsurerEid.ToString();
                    oSaveResponse.AddedPolicyId = objManager.SavePolicy(iPolicyId, base.GetSessionObject().SessionId, oDataModelTemplate.ToString());
                    if (oSaveResponse.AddedPolicyId != 0)
                        oSaveResponse.Result = true;
                    objManager.AddAddressIDToPolicyInsured(oSaveResponse.AddedPolicyId, iInsuredEid, iAddressId);
                    sEntityDataModelData = string.Empty;
                    objManager.SaveEndorsementData(oExternalPolicyData, oSaveResponse.AddedPolicyId, "Policy", oSaveResponse.AddedPolicyId);
                    objManager.SavePolicyDownloadXMLData(oSaveResponse.AddedPolicyId, oExternalPolicyData.PolicyData.PolicySoapXML, objManager.GetPolicySystemId(oSaveResponse.AddedPolicyId), "POLICY");
                    SaveUnit(oExternalPolicyData, sPolicyLOB, oExternalPolicyData.PolicySystemID, oSaveResponse.AddedPolicyId, ref p_objErrOut);
                    foreach (EntityData oEntityData in oExternalPolicyData.PolicyData.Entities)
                    {
                        if (string.Equals(oEntityData.Selected, "True", StringComparison.InvariantCultureIgnoreCase) && !string.Equals(oEntityData.Role, "Insurer", StringComparison.InvariantCultureIgnoreCase) && !string.Equals(oEntityData.Role, "Policy Owner", StringComparison.InvariantCultureIgnoreCase))
                        {
                            sEntityDataModelData = GetEntitySaveFormattedResult(oEntityData, oExternalPolicyData.PolicySystemID, string.Empty, ref p_objErrOut);                           
                            sEntityRole = oEntityData.TableID;
                            sRoleCode = oEntityData.Role.ToLower();
                            if (sEntityDataModelData != "")
                            {
                                if (Constants.listDriver.Contains(sRoleCode))
                                {
                                    iEntityID = objManager.SaveEntity(sEntityDataModelData, sRoleCode, ref iAddressId, ref sEntityRole, oSaveResponse.AddedPolicyId);
                                    objManager.SaveDriver(sEntityDataModelData, iEntityID);
                                }
                                else
                                {
                                    iEntityID = objManager.SaveEntity(sEntityDataModelData, sRoleCode, ref iAddressId, ref sEntityRole, oSaveResponse.AddedPolicyId);
                                }                                
                                iEntityID = objManager.SavePolicyXEntity(oSaveResponse.AddedPolicyId, iEntityID, Conversion.CastToType<int>(sEntityRole, out bSuccess), 0, sRoleCode, iAddressId);
                                objManager.SavePolicyDownloadXMLData(iEntityID, oEntityData.PartySoapXML, objManager.GetPolicySystemId(oSaveResponse.AddedPolicyId), "POLICY_X_ENTITY");
                                sEntityDataModelData = string.Empty;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                oSaveResponse.Result = false;
            }
            finally
            {
                if (oDataModelTemplate != null)
                    oDataModelTemplate = null;
                if (oDataModelElement != null)
                    oDataModelElement = null;
                if (objManager != null)
                    objManager = null;
                if (objSettings != null)
                    objSettings = null;
            }
            return oSaveResponse.Result;
        }
        private void SaveUnit(ExternalPolicyData oPolicyData, string sLOB, int iPolicySystemId, int iPolicyId, ref BusinessAdaptorErrors p_objErrOut)
        {
            string sUnitType = string.Empty;
            string sEntityDataModelData = string.Empty;
            int iinsertedId = 0;
            int iEntityID = 0;
            int iAddressId = 0;
            bool bSuccess = false;
            string sEntityRole = string.Empty;
            string sRoleCode = string.Empty;
            IntegralSystemInterface objManager = null;
            objManager = new IntegralSystemInterface(m_userLogin, base.ClientId);
            foreach (UnitData oUnitData in oPolicyData.PolicyData.Units)
            {
                if (oUnitData.Selected == "True")
                {
                    sUnitType = oUnitData.UnitTypeInd;
                    if (CommonFunctions.GetUnitTypeIndicator(sUnitType) == Constants.UNIT_TYPE_INDICATOR.VEHICLE)
                        iinsertedId = SaveVehicleUnit(oUnitData, iPolicyId, ref p_objErrOut);
                    if (CommonFunctions.GetUnitTypeIndicator(sUnitType) == Constants.UNIT_TYPE_INDICATOR.PROPERTY)
                        iinsertedId = SavePropertyUnit(oUnitData, iPolicyId, ref p_objErrOut);
                    if (iinsertedId > 0)
                        SaveCoverages(oUnitData, sLOB, ref p_objErrOut, iinsertedId, iPolicyId);
                    if (iinsertedId > 0)
                    {
                        foreach (EntityData oEntityData in oUnitData.Entities)
                        {
                            if (string.Equals(oEntityData.Selected, "True", StringComparison.InvariantCultureIgnoreCase))
                            {
                                sEntityDataModelData = GetEntitySaveFormattedResult(oEntityData, oPolicyData.PolicySystemID, iinsertedId.ToString(), ref p_objErrOut);
                                sEntityRole = oEntityData.TableID;
                                sRoleCode = oEntityData.Role.ToLower();
                                if (sEntityDataModelData != "")
                                {
                                    iEntityID = objManager.SaveEntity(sEntityDataModelData, sRoleCode, ref iAddressId, ref sEntityRole, iPolicyId);
                                    iEntityID = objManager.SavePolicyXEntity(iPolicyId, iEntityID, Conversion.CastToType<int>(sEntityRole, out bSuccess), iinsertedId, sRoleCode, iAddressId);
                                    objManager.SavePolicyDownloadXMLData(iEntityID, oEntityData.PartySoapXML, objManager.GetPolicySystemId(iPolicyId), "POLICY_X_ENTITY");
                                    sEntityDataModelData = string.Empty;
                                }
                            }
                        }
                    }
                }
            }
        }
        private int SaveVehicleUnit(UnitData oUnitData, int iPolicyId, ref BusinessAdaptorErrors p_objErrOut)
        {
            XElement oDataModelTemplate = null;
            XElement oDataModelElement = null;
            IntegralSystemInterface objManager = null;
            StringBuilder objBuilder = null;
            int iRecordId = 0;
            string sUnitNumber = string.Empty;
            string sRiskLoc = string.Empty;
            string sRiskSubLoc = string.Empty;
            string sProductCode = string.Empty;
            string sInsLine = string.Empty;
            int iPolicySystemID = Int32.MinValue;
            string sVehDesc = string.Empty;
            try
            {
                objManager = new IntegralSystemInterface(connectionString,base.ClientId);
                if (oUnitData != null)
                {
                    objBuilder = new StringBuilder();
                    sUnitNumber = oUnitData.UnitNumber;
                    oDataModelTemplate = XElement.Parse(GetXMLTemplateByType("Vehicle", ref p_objErrOut));
                    if (oDataModelTemplate != null)
                    {
                        iPolicySystemID = objManager.GetPolicySystemId(iPolicyId);
                        if (!string.IsNullOrEmpty(oUnitData.Model))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Vehicle/VehicleModel");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = oUnitData.Model;
                        }
                        if (!string.IsNullOrEmpty(oUnitData.UnitNumber))
                        {
                            sVehDesc = oUnitData.UnitNumber + " - ";
                        }
                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Vehicle/Vin");
                        if (oDataModelElement != null)
                        {
                            if (!string.IsNullOrEmpty(oUnitData.RegistrationNumber))
                            {
                                oDataModelElement.Value = oUnitData.RegistrationNumber;
                                sVehDesc = sVehDesc + oUnitData.RegistrationNumber;
                            }
                        }
                        if (!string.IsNullOrEmpty(oUnitData.VehicleMake))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Vehicle/VehicleMake");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = oUnitData.VehicleMake;
                            sVehDesc = sVehDesc + " " + oUnitData.VehicleMake;
                        }
                        if (!string.IsNullOrEmpty(oUnitData.Year))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Vehicle/VehicleYear");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = oUnitData.Year;
                            sVehDesc = sVehDesc + " " + oUnitData.Year;
                        }
                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Vehicle/VehDesc");
                        if (oDataModelElement != null)
                        {
                            oDataModelElement.Value = sVehDesc;
                        }
                        //Payal:RMA-7893 --STARTS
                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Vehicle/RiskClause");
                        if (oDataModelElement != null && oUnitData.RiskClauses.Count != 0)
                        {
                            foreach (RiskClause objRiskClause in oUnitData.RiskClauses)
                            {
                                if (string.IsNullOrEmpty(oDataModelElement.Value))
                                    oDataModelElement.Value = objRiskClause.RiskCode + "-" + objRiskClause.RiskDesc + Environment.NewLine;
                                else
                                    oDataModelElement.Value = oDataModelElement.Value + objRiskClause.RiskCode + "-" + objRiskClause.RiskDesc + Environment.NewLine;
                            }
                        }
                        //Payal:RMA-7893 --ENDS
                        objManager = new IntegralSystemInterface(m_userLogin, base.ClientId);
                        iRecordId = objManager.SaveVehicle(oDataModelTemplate.ToString(), iPolicyId, oUnitData);
                        if (iRecordId > 0)
                        {
                            objManager.SavePolicyUnitData(iRecordId, Constants.UNIT_TYPE_INDICATOR.VEHICLE, sUnitNumber);
                            iRecordId = objManager.SavePolicyXUnit(iPolicyId, iRecordId, Constants.UNIT_TYPE_INDICATOR.VEHICLE, iPolicySystemID, oUnitData);
                            objManager.SavePolicyDownloadXMLData(iRecordId, oUnitData.UnitSoapXML, iPolicySystemID, "POLICY_X_UNIT");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
            }
            finally
            {
                if (oDataModelTemplate != null)
                    oDataModelTemplate = null;
                if (oDataModelElement != null)
                    oDataModelElement = null;
                if (objBuilder != null)
                    objBuilder = null;
                if (objManager != null)
                    objManager = null;
            }
            return iRecordId;
        }
        private int SavePropertyUnit(UnitData oUnitData, int iPolicyId, ref BusinessAdaptorErrors p_objErrOut)
        {
            XElement oDataModelTemplate = null;
            XElement oDataModelElement = null;
            StringBuilder objBuilder = null;
            IntegralSystemInterface objManager = null;
            int iRecordId = 0;
            int iClsCodeID = 0;
            int iCatCode = 0;
            string sUnitNumber = string.Empty;
            string sStatUnitNumber = string.Empty;
            string sRiskLoc = string.Empty;
            string sRiskSubLoc = string.Empty;
            string sProductCode = string.Empty;
            string sInsLine = string.Empty;
            int iPolicySystemId = Int32.MinValue;
            LocalCache objCache = new LocalCache(connectionString, base.ClientId);
            string sPIN = string.Empty;
            try
            {
                if (oUnitData != null)
                {
                    objBuilder = new StringBuilder();
                    sUnitNumber = oUnitData.UnitNumber;
                    objManager = new IntegralSystemInterface(m_userLogin, base.ClientId);
                    oDataModelTemplate = XElement.Parse(GetXMLTemplateByType("Property", ref p_objErrOut));
                    if (oDataModelTemplate != null)
                    {
                        iPolicySystemId = objManager.GetPolicySystemId(iPolicyId);
                        //sanoopsharma - Jira 5354
                        if (!string.IsNullOrEmpty(oUnitData.Business))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./PropertyUnit/Business");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = oUnitData.Business;
                            sPIN = oUnitData.Business;
                        }
                        else if (!string.IsNullOrEmpty(oUnitData.RiskRating))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./PropertyUnit/RiskRating");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = oUnitData.RiskRating;
                            sPIN = oUnitData.RiskRating;
                        }                       
                        if (!string.IsNullOrEmpty(oUnitData.Premises))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./PropertyUnit/Addr1");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = oUnitData.Premises;
                            sPIN += " " + oUnitData.Premises;
                        }
                        else if (!string.IsNullOrEmpty(oUnitData.Situation))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./PropertyUnit/Addr1");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = oUnitData.Situation;
                            sPIN += " " + oUnitData.Situation;
                        }
                        if (!string.IsNullOrEmpty(oUnitData.PostalCode))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./PropertyUnit/ZipCode");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = oUnitData.PostalCode;
                        }
                        if (!string.IsNullOrEmpty(oUnitData.RiskAccumulationState))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./PropertyUnit/CountryCode");
                            if (oDataModelElement != null)
                                oDataModelElement.Attribute("codeid").Value = objCache.GetCodeId(oUnitData.RiskAccumulationState,"COUNTRY").ToString();
                        }
                        if (!string.IsNullOrEmpty(oUnitData.ConstructionType))
                        {
                            //oDataModelElement = oDataModelTemplate.XPathSelectElement("./PropertyUnit/ConstructionType");
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./PropertyUnit/ClassOfConstruction");
                            if (oDataModelElement != null)
                            {
                                iClsCodeID = objManager.GetRMXCodeIdFromPSMappedCode(oUnitData.ConstructionType, "CLASS_OF_CONSTRUCTION", "ClassOfConstruction on SavePropertyUnit", iPolicySystemId.ToString());
                                oDataModelElement.Attribute("codeid").Value = iClsCodeID.ToString();
                            }
                        }

                        if (!string.IsNullOrEmpty(oUnitData.OccupiedAs))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./PropertyUnit/CategoryCode");
                            if (oDataModelElement != null)
                            {
                                iCatCode = objManager.GetRMXCodeIdFromPSMappedCode(oUnitData.OccupiedAs, "CATEGORY", "PropertyTypeCode on SavePropertyUnit", iPolicySystemId.ToString());
                                oDataModelElement.Attribute("codeid").Value = iCatCode.ToString();
                            }
                        }

                        if (!string.IsNullOrEmpty(oUnitData.RiskAccumulationLocality))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./PropertyUnit/RiskAccumulationLocality");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = oUnitData.RiskAccumulationLocality;
                            if (sPIN != string.Empty)
                                sPIN = sPIN + " -" + oUnitData.RiskAccumulationLocality;
                            else
                                sPIN = oUnitData.RiskAccumulationLocality;
                        }
                        if (!string.IsNullOrEmpty(oUnitData.ProtectedBy))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./PropertyUnit/ProtectedBy");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = oUnitData.ProtectedBy;
                        }
                        if (!string.IsNullOrEmpty(oUnitData.BuildingStorey))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./PropertyUnit/BuildingStorey");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = oUnitData.BuildingStorey;
                        }
                        if (!string.IsNullOrEmpty(oUnitData.ConstructionYear))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./PropertyUnit/YearOfConstruction");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = oUnitData.ConstructionYear;
                        }
                        if (!string.IsNullOrEmpty(oUnitData.Attached))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./PropertyUnit/Attached");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = oUnitData.Attached;
                        }
                        if (!string.IsNullOrEmpty(oUnitData.FloorArea))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./PropertyUnit/FloorArea");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = oUnitData.FloorArea;
                        }
                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PropertyUnit/Pin");
                        if (oDataModelElement != null)
                        {
                            oDataModelElement.Value = sPIN;
                        }
                        //Payal:RMA-7893 --STARTS
                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PropertyUnit/RiskClause");
                        if (oDataModelElement != null && oUnitData.RiskClauses.Count!=0)
                        {
                            foreach (RiskClause objRiskClause in oUnitData.RiskClauses)
                            {
                                if (string.IsNullOrEmpty(oDataModelElement.Value))
                                    oDataModelElement.Value = objRiskClause.RiskCode + "-" + objRiskClause.RiskDesc + Environment.NewLine;
                                else
                                    oDataModelElement.Value = oDataModelElement.Value + objRiskClause.RiskCode + "-" + objRiskClause.RiskDesc + Environment.NewLine;
                            }
                        }
                        iRecordId = objManager.SaveProperty(oDataModelTemplate.ToString(), iPolicyId, oUnitData);
                        if (iRecordId > 0)
                        {
                            objManager.SavePolicyUnitData(iRecordId, Constants.UNIT_TYPE_INDICATOR.PROPERTY, sUnitNumber);
                            iRecordId = objManager.SavePolicyXUnit(iPolicyId, iRecordId, Constants.UNIT_TYPE_INDICATOR.PROPERTY, iPolicySystemId, oUnitData);
                            objManager.SavePolicyDownloadXMLData(iRecordId, oUnitData.UnitSoapXML, iPolicySystemId, "POLICY_X_UNIT");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
            }
            finally
            {
                if (oDataModelTemplate != null)
                    oDataModelTemplate = null;
                if (oDataModelElement != null)
                    oDataModelElement = null;
                if (objBuilder != null)
                    objBuilder = null;
                if (objManager != null)
                    objManager = null;
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
            }
            return iRecordId;
        }
        private void SaveCoverages(UnitData oUnitData, string sLOB, ref BusinessAdaptorErrors p_objErrOut, int iPolicyUnitId, int iPolicyId)
        {
            XElement oDataModelTemplate = null;
            XElement oElementNode = null;
            XElement oDataModelElement = null;
            int iRecordId = 0;
            IntegralSystemInterface objManager = null;
            LocalCache objCache = new LocalCache(connectionString, base.ClientId);
            int iCvgCodeId = 0;
            string sCvgCode = string.Empty;
            string sCvgDesc = string.Empty;
            string sCvgText = string.Empty;
            string sCvgClass = string.Empty;
            string sCvgKey = string.Empty;
            int iPolicySystemId = Int32.MinValue;
            try
            {
                objManager = new IntegralSystemInterface(m_userLogin, base.ClientId);
                iPolicySystemId = objManager.GetPolicySystemId(iPolicyId);
                foreach (CoverageData oCoverageData in oUnitData.Coverages)
                {
                    oDataModelTemplate = XElement.Parse(GetXMLTemplateByType("Coverage", ref p_objErrOut));
                    if (oDataModelTemplate != null)
                    {
                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/PolicyUnitRowId");
                        if (oDataModelElement != null)
                            oDataModelElement.Value = iPolicyUnitId.ToString();
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/CvgSequenceNo");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = "0";
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/TransSequenceNo");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = "0";
                        if (!string.IsNullOrEmpty(oCoverageData.PrimiumData.PremiumClass))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/CvgClassCode");
                            if (oDataModelElement != null)
                            {
                                oDataModelElement.Value = oCoverageData.PrimiumData.PremiumClass;
                                sCvgClass = oDataModelElement.Value;
                            }
                        }
                        if (!string.IsNullOrEmpty(oCoverageData.CoverageType))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/CoverageTypeCode");
                            if (oDataModelElement != null)
                            {
                                oDataModelElement.Value = oCoverageData.CoverageType;
                                iCvgCodeId = objManager.GetRMXCodeIdFromPSMappedCode(oCoverageData.CoverageType, "COVERAGE_TYPE", "CoverageType on SaveCoverages", iPolicySystemId.ToString());
                                oDataModelElement.Attribute("codeid").Value = iCvgCodeId.ToString();
                                sCvgCode = objCache.GetShortCode(iCvgCodeId);
                            }
                        }
                        if (!string.IsNullOrEmpty(oCoverageData.PrimiumData.NetPremium))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/OrginialPremium");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = oCoverageData.PrimiumData.NetPremium;
                        }
                        if (!string.IsNullOrEmpty(oCoverageData.PrimiumData.GrossPremium))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/TotalWrittenPremium");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = oCoverageData.PrimiumData.GrossPremium;
                        }
                        if (!string.IsNullOrEmpty(oCoverageData.LimitSumInsured))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/OccurrenceLimit");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = oCoverageData.LimitSumInsured;
                        }
                        if (!string.IsNullOrEmpty(oCoverageData.PrimiumData.TotalSumInsured))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/Limit");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = oCoverageData.PrimiumData.TotalSumInsured;
                        }
                        if (!string.IsNullOrEmpty(oCoverageData.CoverageType))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/CoverageText");
                            if (oDataModelElement != null)
                            {
                                sCvgDesc = objCache.GetCodeDesc(iCvgCodeId);
                                oDataModelElement.Value = sCvgCode + " " + sCvgDesc + "-" + sCvgClass;
                                sCvgText = oDataModelElement.Value;
                            }
                        }
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/CoverageKey");
                            if (oDataModelElement != null)
                            {
                                sCvgKey = oUnitData.UnitNumber + "," + sCvgCode + "," + sCvgClass;
                                oDataModelElement.Value = sCvgKey;                               
                            }
                        int iPolicyCVGRowId = 0;
                        iPolicyCVGRowId = objManager.CheckCoverageDuplication(iPolicyUnitId, sCvgKey, iPolicySystemId);
                        iRecordId = objManager.SaveCoverage(oDataModelTemplate.ToString(), iPolicyCVGRowId);
                        objManager.SavePolicyDownloadXMLData(iRecordId, oCoverageData.CoverageSoapXML, iPolicySystemId, "POLICY_X_CVG_TYPE");
                    }
                }
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
            }
            finally
            {
                if (oElementNode != null)
                    oElementNode = null;
                if (oDataModelTemplate != null)
                    oDataModelTemplate = null;
                if (oDataModelElement != null)
                    oDataModelElement = null;
            }
        }
       
        private string GetInsurerSaveFormattedResult(ExternalPolicyData oExternalPolicyData, ref BusinessAdaptorErrors p_objErrOut, int iPolicySystemID, string p_PolCmpny)
        {
            XElement xInsurerTemplate = null;
            XElement xTemplateElement = null;
            int iCodeId = 0;
            IntegralSystemInterface objManager = null;
            StringBuilder objBuilder = null;
            LocalCache objCache = null;
            string sCity = string.Empty, sStateCd = string.Empty;
            string sMasterCmpny = string.Empty, sLocatnCompny = string.Empty, sPolCompny = string.Empty;
            try
            {
                objManager = new IntegralSystemInterface(m_userLogin, base.ClientId);
                objBuilder = new StringBuilder();
                foreach (EntityData oInsurerData in oExternalPolicyData.PolicyData.Entities)
                {
                    if (oInsurerData.Role == "Insurer")
                    {
                        xInsurerTemplate = XElement.Parse(GetXMLTemplateByType("Entity", ref p_objErrOut));
                        if (xInsurerTemplate != null)
                        {
                            xTemplateElement = xInsurerTemplate.XPathSelectElement("./Entity/TaxId");
                            if (xTemplateElement != null)
                                xTemplateElement.Value = "";
                            if (!string.IsNullOrEmpty(oInsurerData.BusinessName))
                            {
                                xTemplateElement = xInsurerTemplate.XPathSelectElement("./Entity/LastName");
                                if (xTemplateElement != null)
                                    xTemplateElement.Value = oInsurerData.BusinessName;
                            }
                            foreach (AddressData oAddress in oInsurerData.Addresses)
                            {
                                if (!string.IsNullOrEmpty(oAddress.AddressLine1))
                                {
                                    xTemplateElement = xInsurerTemplate.XPathSelectElement("./Entity/Addr1");
                                    if (xTemplateElement != null)
                                        xTemplateElement.Value = oAddress.AddressLine1;
                                }
                                xTemplateElement = xInsurerTemplate.XPathSelectElement("./Entity/Addr2");
                                if (xTemplateElement != null)
                                    xTemplateElement.Value = "";
                                if (!string.IsNullOrEmpty(oAddress.ZipCode))
                                {//sanoopsharma field renamed for integral
                                    xTemplateElement = xInsurerTemplate.XPathSelectElement("./Entity/ZipCode");
                                    if (xTemplateElement != null)
                                        xTemplateElement.Value = oAddress.ZipCode;
                                }
                                if (!string.IsNullOrEmpty(oAddress.City))
                                {//sanoopsharma field renamed for integral
                                    string[] sAddress;
                                    sAddress = oInsurerData.Addresses[0].City.Split(','); // since insurercity has data like "Columbia, S.C.  or Coulmbia, SC" 
                                    sCity = sAddress[0].Trim();
                                    if (sAddress.Length == 2)
                                        sStateCd = sAddress[1].Trim();
                                    sStateCd = sStateCd.Replace(".", ""); // to change S.C. to SC
                                    xTemplateElement = xInsurerTemplate.XPathSelectElement("./Entity/City");
                                    if (xTemplateElement != null)
                                        xTemplateElement.Value = sCity;
                                }
                                if (!string.IsNullOrEmpty(oAddress.Type))
                                {
                                    xTemplateElement = xInsurerTemplate.XPathSelectElement("./Entity/EntityXAddressesList/EntityXAddresses/AddressType");
                                    if (xTemplateElement != null)
                                    {
                                        iCodeId = (new IntegralSystemInterface(connectionString, base.ClientId)).GetRMXCodeIdFromPSMappedCode(oAddress.Type, "ADDRESS_TYPE", "Address Type on GetInsurerSaveFormattedResult", oExternalPolicyData.PolicySystemID.ToString());
                                        xTemplateElement.Value = iCodeId.ToString();
                                    }
                                }
                                xTemplateElement = xInsurerTemplate.XPathSelectElement("./Entity/StateId");
                                if (xTemplateElement != null && sStateCd.Trim() != string.Empty)
                                    xTemplateElement.Attribute("codeid").Value = objManager.GetRMXCodeIdFromPSMappedCode(sStateCd, "STATES", "StateProvCd on GetInsurerSaveFormattedResult", iPolicySystemID.ToString()).ToString(); // aaggarwal29: Code mapping change
                                if (!string.IsNullOrEmpty(oAddress.Country))
                                {
                                    xTemplateElement = xInsurerTemplate.XPathSelectElement("./Entity/CountryCode");
                                    if (xTemplateElement != null)
                                        xTemplateElement.Attribute("codeid").Value = oAddress.Country;
                                }
                            }
                            if (!string.IsNullOrEmpty(oInsurerData.ClientNumber))
                            {
                                xTemplateElement = xInsurerTemplate.XPathSelectElement("./Entity/ClientSequenceNumber");
                                if (xTemplateElement != null)
                                    xTemplateElement.Value = oInsurerData.ClientNumber;
                            }
                            xTemplateElement = xInsurerTemplate.XPathSelectElement("./Entity/EntityTableId");
                            if (xTemplateElement != null)
                            {
                                xTemplateElement.Value = (new LocalCache(connectionString, base.ClientId)).GetTableId("INSURERS").ToString();
                            }
                            if (!string.IsNullOrEmpty(oExternalPolicyData.PolicyData.MasterCompany))
                            {
                                sMasterCmpny = oExternalPolicyData.PolicyData.MasterCompany;
                            }
                            sPolCompny = p_PolCmpny;
                            xTemplateElement = xInsurerTemplate.XPathSelectElement("./Entity/ReferenceNumber");
                            if (xTemplateElement != null)
                            {
                                xTemplateElement.Value = sMasterCmpny;
                            }
                            objBuilder.Append(xInsurerTemplate.ToString());
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
            }
            finally
            {
                if (xInsurerTemplate != null)
                    xInsurerTemplate = null;
                if (xTemplateElement != null)
                    xTemplateElement = null;
                if (objManager != null)
                    objManager = null;
            }
            if (objBuilder != null)
                return objBuilder.ToString();
            else
                return string.Empty;
        }        
        private string GetEntitySaveFormattedResult(EntityData oEntityData, int iPolicySystemID, string sUnitRowId, ref BusinessAdaptorErrors p_objErrOut)
        {
            XElement xEntityTemplate = null;
            XElement xEntityElement = null;
            XElement xEntityTemplateElement = null;
            List<EntityData> EntityDataList = new List<EntityData>();           
            int iCodeId = 0;          
            IntegralSystemInterface objManager = null;
            string sRoleCode = string.Empty;
            string sEntityBusinessRole = string.Empty;
            string sExculdeEntities = string.Empty;
            string sOtherParty = string.Empty;
            string sEntityRole = string.Empty;          
            LocalCache objCache = new LocalCache(connectionString,base.ClientId);
            try
            {
                if (oEntityData != null)
                {
                    objManager = new IntegralSystemInterface(m_userLogin, base.ClientId);
                    sExculdeEntities = objManager.GetPolicySystemParamteres("EXCLUDE_INTEREST", iPolicySystemID).Replace(" ", "");
                    sEntityBusinessRole = objManager.GetPolicySystemParamteres("BUSINESS_ROLES", iPolicySystemID).Replace(" ", "");
                    xEntityTemplate = XElement.Parse(GetXMLTemplateByType("Entity", ref p_objErrOut));
                    if (xEntityTemplate != null)
                    {
                        if (!string.IsNullOrEmpty(oEntityData.TaxID))
                        {
                            xEntityElement = xEntityTemplate.XPathSelectElement("./Entity/TaxId");
                            if (xEntityElement != null)
                                xEntityElement.Value = oEntityData.TaxID;
                        }
                        if (!string.IsNullOrEmpty(oEntityData.RoleCode))
                        {
                            sRoleCode = oEntityData.RoleCode;
                        }
                        if (!string.IsNullOrEmpty(sEntityBusinessRole) && !string.IsNullOrEmpty(sRoleCode) && sExculdeEntities.Split(',').Contains(sRoleCode))
                        {
                            return string.Empty;
                        }
                        if ((oEntityData.TypeCode == "C") ||
                            (!string.IsNullOrEmpty(sEntityBusinessRole) && !string.IsNullOrEmpty(sRoleCode) && sEntityBusinessRole.Split(',').Contains(sRoleCode)))
                        {
                            xEntityElement = xEntityTemplate.XPathSelectElement("./Entity/LastName");
                            if (xEntityElement != null)
                                xEntityElement.Value = oEntityData.BusinessName;

                            xEntityElement = xEntityTemplate.XPathSelectElement("./Entity/NameType");
                            if (xEntityElement != null)
                                xEntityElement.Attribute("codeid").Value = objCache.GetCodeId("BUS", "ENTITY_NAME_TYPE").ToString();
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(oEntityData.FirstName))
                            {
                                xEntityElement = xEntityTemplate.XPathSelectElement("./Entity/FirstName");
                                if (xEntityElement != null)
                                    xEntityElement.Value = oEntityData.FirstName;
                            }
                            if (!string.IsNullOrEmpty(oEntityData.LastName))
                            {
                                xEntityElement = xEntityTemplate.XPathSelectElement("./Entity/LastName");
                                if (xEntityElement != null)
                                    xEntityElement.Value = oEntityData.LastName;
                            }

                            xEntityElement = xEntityTemplate.XPathSelectElement("./Entity/NameType");
                            if (xEntityElement != null)
                                xEntityElement.Attribute("codeid").Value = objCache.GetCodeId("IND", "ENTITY_NAME_TYPE").ToString();
                        }
                        if (!string.IsNullOrEmpty(oEntityData.BirthDate))
                        {
                            xEntityElement = xEntityTemplate.XPathSelectElement("./Entity/BirthDate");
                            if (xEntityElement != null)
                                xEntityElement.Value = oEntityData.BirthDate;
                        }
                        if (!string.IsNullOrEmpty(oEntityData.Sex))
                        {
                            xEntityElement = xEntityTemplate.XPathSelectElement("./Entity/SexCode");
                            if (xEntityElement != null)
                                xEntityElement.Value = oEntityData.Sex;
                        }
                        if (!string.IsNullOrEmpty(oEntityData.EmailAddress))
                        {
                            xEntityElement = xEntityTemplate.XPathSelectElement("./Entity/EmailAddress");
                            if (xEntityElement != null)
                                xEntityElement.Value = oEntityData.EmailAddress;
                        }
                        foreach (AddressData oAddress in oEntityData.Addresses)
                        {
                            if (!string.IsNullOrEmpty(oAddress.AddressLine1))
                            {
                                xEntityElement = xEntityTemplate.XPathSelectElement("./Entity/Addr1");
                                xEntityTemplateElement = xEntityTemplate.XPathSelectElement("./Entity/EntityXAddressesList/EntityXAddresses/Addr1");
                                if (xEntityElement != null)
                                    xEntityElement.Value = oAddress.AddressLine1;
                                if (xEntityTemplateElement != null)
                                    xEntityTemplateElement.Value = oAddress.AddressLine1;
                            }
                            if (!string.IsNullOrEmpty(oAddress.AddressLine2))
                            {
                                xEntityElement = xEntityTemplate.XPathSelectElement("./Entity/Addr2");
                                xEntityTemplateElement = xEntityTemplate.XPathSelectElement("./Entity/EntityXAddressesList/EntityXAddresses/Addr2");
                                if (xEntityElement != null)
                                    xEntityElement.Value = oAddress.AddressLine2;
                                if (xEntityTemplateElement != null)
                                    xEntityTemplateElement.Value = oAddress.AddressLine2;
                            }
                            if (!string.IsNullOrEmpty(oAddress.ZipCode))
                            {//sanoopsharma field renamed for integral
                                xEntityElement = xEntityTemplate.XPathSelectElement("./Entity/ZipCode");
                                xEntityTemplateElement = xEntityTemplate.XPathSelectElement("./Entity/EntityXAddressesList/EntityXAddresses/ZipCode");
                                if (xEntityElement != null)
                                    xEntityElement.Value = oAddress.ZipCode;
                                if (xEntityTemplateElement != null)
                                    xEntityTemplateElement.Value = oAddress.ZipCode;
                            }
                            if (!string.IsNullOrEmpty(oAddress.City))
                            {
                                xEntityElement = xEntityTemplate.XPathSelectElement("./Entity/City");
                                xEntityTemplateElement = xEntityTemplate.XPathSelectElement("./Entity/EntityXAddressesList/EntityXAddresses/City");
                                if (xEntityElement != null)
                                    xEntityElement.Value = oAddress.City;
                                if (xEntityTemplateElement != null)
                                    xEntityTemplateElement.Value = oAddress.City;
                            }
                            if (!string.IsNullOrEmpty(oAddress.Country))
                            {
                                xEntityElement = xEntityTemplate.XPathSelectElement("./Entity/CountryCode");
                                xEntityTemplateElement = xEntityTemplate.XPathSelectElement("./Entity/EntityXAddressesList/EntityXAddresses/Country");
                                if (xEntityElement != null)
                                    xEntityElement.Attribute("codeid").Value = objCache.GetCodeId(oAddress.Country,"COUNTRY").ToString();
                                if (xEntityTemplateElement != null)
                                    xEntityTemplateElement.Attribute("codeid").Value = objCache.GetCodeId(oAddress.Country, "COUNTRY").ToString(); 
                            }
                            if (!string.IsNullOrEmpty(oAddress.Type))
                            {
                                xEntityElement = xEntityTemplate.XPathSelectElement("./Entity/EntityXAddressesList/EntityXAddresses/AddressType");
                                if (xEntityElement != null)
                                {
                                    iCodeId = (new IntegralSystemInterface(connectionString, base.ClientId)).GetRMXCodeIdFromPSMappedCode(oAddress.Type, "ADDRESS_TYPE", "Address Type on GetInsuredSaveFormattedResult", iPolicySystemID.ToString());
                                    xEntityElement.Value = iCodeId.ToString();
                                }
                            }
                        }
                        if (!string.IsNullOrEmpty(oEntityData.ClientNumber))
                        {//sanoopsharma field renamed for integral
                            xEntityElement = xEntityTemplate.XPathSelectElement("./Entity/ClientSequenceNumber");
                            if (xEntityElement != null)
                                xEntityElement.Value = oEntityData.ClientNumber;
                        }
                        if (oEntityData.TableID != null)
                        {
                            xEntityElement = xEntityTemplate.XPathSelectElement("./Entity/EntityTableId");
                            if (xEntityElement != null)
                            {
                                xEntityElement.Value = oEntityData.TableID;
                                sEntityRole = oEntityData.TableID;
                            }
                        }
                        if (Constants.listAgent.Contains(oEntityData.Role.ToLower()))
                        {
                            if (!string.IsNullOrEmpty(oEntityData.AgentNumber))
                            {
                                xEntityElement = xEntityTemplate.XPathSelectElement("./Entity/ReferenceNumber");
                                if (xEntityElement != null)
                                    xEntityElement.Value = oEntityData.AgentNumber;
                            }
                            if (oEntityData.TypeCode == "C")
                            {
                                xEntityElement = xEntityTemplate.XPathSelectElement("./Entity/LastName");
                                if (xEntityElement != null)
                                    xEntityElement.Value = oEntityData.BusinessName;
                            }
                            else
                            {
                                xEntityElement = xEntityTemplate.XPathSelectElement("./Entity/LastName");
                                if (xEntityElement != null)
                                    xEntityElement.Value = oEntityData.FirstName + " " + oEntityData.LastName;
                            }
                            xEntityElement = xEntityTemplate.XPathSelectElement("./Entity/EntityTableId");
                            if (xEntityElement != null)
                            {
                                xEntityElement.Value = (new LocalCache(connectionString, base.ClientId)).GetTableId("AGENTS").ToString();
                                sEntityRole = xEntityElement.Value;
                            }
                        }
                        else if (Constants.listDriver.Contains(oEntityData.Role.ToLower()))
                        {
                            xEntityElement = xEntityTemplate.XPathSelectElement("./Entity/EntityTableId");
                            if (xEntityElement != null)
                            {
                                xEntityElement.Value = (new LocalCache(connectionString, base.ClientId)).GetTableId("DRIVERS").ToString();
                            }                           
                        }
                        else if (Constants.listInsured.Contains(oEntityData.Role.ToLower()))
                        {
                            xEntityElement = xEntityTemplate.XPathSelectElement("./Entity/EntityTableId");
                            if (xEntityElement != null)
                            {
                                xEntityElement.Value = (new LocalCache(connectionString, base.ClientId)).GetTableId("POLICY_INSURED").ToString();
                            }                          
                        }
                    }
                }
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
            }
            finally
            {                
                if (xEntityElement != null)
                    xEntityElement = null;
                if (objManager != null)
                    objManager = null;
                if (objCache != null)
                    objCache.Dispose();
            }
            if (xEntityTemplate != null)
                return xEntityTemplate.ToString();
            else
                return string.Empty;
        }
        public string GetInterestListRoles(ref BusinessAdaptorErrors p_objErrOut)
        {
            IntegralSystemInterface oSystemInterface = null;
            string sRoles = string.Empty;
            try
            {
                oSystemInterface = new IntegralSystemInterface(m_userLogin, base.ClientId);
                sRoles = oSystemInterface.GetInterestListRoles().OuterXml;
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
            }
            finally
            {
                if (oSystemInterface != null)
                    oSystemInterface = null;
            }
            return sRoles;
        }
        public string GetPolicyLOBRelatedShortCode(string p_sShortCode, string p_sTableName, ref BusinessAdaptorErrors p_objErrOut)
        {
            string sRelatedShortCode = string.Empty;
            LocalCache objcache = new LocalCache(connectionString, base.ClientId);
            sRelatedShortCode = objcache.GetRelatedShortCode(objcache.GetCodeId(p_sShortCode.Trim(), p_sTableName));
            if (string.IsNullOrEmpty(sRelatedShortCode))
            {
                //throw new RMAppException("Policy System Base LOB Line Mapping Error: Missing Base LOB Line.");
                throw new RMAppException(Globalization.GetString("IntegralSystemInterface.GetPolicyLOBRelatedShortCode.MissingBaseLOB", base.ClientId, base.LangCode.ToString()));
            }
            return sRelatedShortCode;
        }
        private string GetXMLTemplateByType(string Type, ref BusinessAdaptorErrors p_objErrOut)
        {
            string sFilePath = string.Empty;
            string sFileName = string.Empty;
            XmlDocument objXmlDocument = null;
            string sFileContent = string.Empty;
            try
            {
                switch (Type)
                {
                    case "Policy": sFileName = "Policy_Template.xml"; break;
                    case "Entity": sFileName = "Entity_Template.xml"; break;
                    case "Vehicle": sFileName = "Vehicle_Template.xml"; break;
                    case "Property": sFileName = "Property_Template.xml"; break;
                    case "EntityList": sFileName = "EntityList_Template.xml"; break;
                    case "UnitList": sFileName = "UnitList_Template.xml"; break;
                    case "SiteUnit": sFileName = "Site_Template.xml"; break;
                    case "Coverage": sFileName = "Coverage_Template.xml"; break;
                    case "UnitInterestList": sFileName = "UnitInterestlist_Template.xml"; break;
                    case "CoverageList": sFileName = "CoverageList_Template.xml"; break;
                }
                if (File.Exists(RMConfigurator.BasePath + "\\userdata\\PolicyInterface\\" + sFileName))
                {
                    sFilePath = RMConfigurator.BasePath + "\\userdata\\PolicyInterface\\" + sFileName;
                    sFileContent = File.ReadAllText(sFilePath);
                    if (!string.IsNullOrEmpty(sFileContent))
                    {
                        objXmlDocument = new XmlDocument();
                        objXmlDocument.LoadXml(sFileContent);
                        sFileContent = objXmlDocument.OuterXml;
                    }
                }
                else
                {
                    throw new RMAppException(Globalization.GetString("PolicySystemInterfaceAdaptor.GetDataModelTemplateByType.DataModelTemplateError",base.ClientId, base.LangCode.ToString()));
                }
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                sFileContent = string.Empty;
            }
            finally
            {
                if (objXmlDocument != null)
                    objXmlDocument = null;
            }
            return sFileContent;
        }        
        public DataTable GetPolicySystemInfoById(int iPolicySystemId, ref BusinessAdaptorErrors p_objErrOut)
        {
            IntegralSystemInterface oIntegralSystemInterface = null;
            DataTable oDT = null;
            try
            {
                oIntegralSystemInterface = new IntegralSystemInterface(connectionString, base.ClientId);
                oDT = oIntegralSystemInterface.GetPolicySystemInfoById(iPolicySystemId);
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                oDT = null;
            }
            finally
            {

                if (oIntegralSystemInterface != null)
                    oIntegralSystemInterface = null;
            }
            return oDT;
        }
        public DataSet GetPolicyTypesFromLOB(int iLOBCodeID)
        {
            string sSQL = string.Empty;
            string sPolicySystemName = string.Empty;
            DataSet dsPolicyTypes = null;
            IntegralSystemInterface oIntegralSystemInterface = null;

            try
            {
                if (iLOBCodeID != 0)
                {
                    oIntegralSystemInterface = new IntegralSystemInterface(m_userLogin, base.ClientId);
                    dsPolicyTypes = oIntegralSystemInterface.GetPolicyTypesFromLOB(iLOBCodeID);
                }
            }
            catch (RMAppException p_objRMExp)
            {
                throw p_objRMExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("IntegralSystemInterface.GetPolicyTypesFromLOB.NoRecordsFound",base.ClientId, base.LangCode.ToString()), p_objExp);
            }
            finally
            {
                if (oIntegralSystemInterface != null)
                    oIntegralSystemInterface = null;
            }
            return dsPolicyTypes;
        }


    }
}



