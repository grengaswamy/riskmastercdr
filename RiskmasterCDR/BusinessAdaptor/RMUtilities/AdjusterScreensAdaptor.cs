﻿using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
    ///************************************************************** 
    ///* $File		: AdjusterScreensAdaptor.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 22/10/2006
    ///* $Author	: Animesh
    ///* $Comment	: 
    ///* $Source	: 
    ///**************************************************************
    /// <summary>	
    ///	This class is used to call the application layer component.
    ///	which implements the functionality of Adjuster List.
    /// </summary>
    class AdjusterScreensAdaptor : BusinessAdaptorBase
    {
        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        public AdjusterScreensAdaptor() { }

        #endregion

        #region Public Methods
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///
        /// </param>
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            AdjusterScreens objAdjLst = null; //Application layer component			
            try
            {
                objAdjLst = new AdjusterScreens(connectionString, base.ClientId);//rkaur27
                p_objXmlOut = objAdjLst.Get(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("AdjusterScreensAdaptorErrorGet", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objAdjLst = null;
            }
        }

        #endregion
    }
}
