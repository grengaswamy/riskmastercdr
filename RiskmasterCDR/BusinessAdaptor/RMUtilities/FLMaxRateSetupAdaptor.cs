using System;
using System.Text;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common; 

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
    ///************************************************************** 
    ///* $File		: FLMaxRateSetupAdaptor.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 08/21/2007
    ///* $Author	: Shruti
    ///* $Comment	: 
    ///* $Source	: 
    ///**************************************************************

    /// <summary>	
    ///	This class is used to call the application layer component for FLMaxRateSetup class  
    /// </summary>
    public class FLMaxRateSetupAdaptor : BusinessAdaptorBase
    {
        #region Constructor
        public FLMaxRateSetupAdaptor()
        {
        }
        #endregion

        #region Public Function
        /// <summary>
        /// This function xml containing data to be shown on screen
        /// </summary>
        /// <param name="p_objXmlIn">Input parameters as xml</param>
        /// <param name="p_objXmlOut">Result as Output xml
        /// </param>
        /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
        /// <returns>Success -True or Failure -false in execution of the function</returns>
        public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            FLMaxRateSetup objMaxRate = null;
            try
            {
                objMaxRate = new FLMaxRateSetup(connectionString, base.ClientId);
                p_objXmlOut = objMaxRate.Get(p_objXmlIn);
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("FLMaxRateSetupAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objMaxRate = null;
            }
        }
        #endregion
    }
}
