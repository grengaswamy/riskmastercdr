using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;  

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: DiscountListAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 26/05/2005
	///* $Author	: Pankaj
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for RM Utilities
	///	which implements the functionality of Discount List.
	/// </summary>
	public class DiscountListAdaptor :  BusinessAdaptorBase
	{
		#region Constructor
		/// <summary>
		/// Default Constructor
		/// </summary>
		public DiscountListAdaptor(){}

		#endregion

		#region Public Methods

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.DiscountListAdaptor.Get() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.DiscountList.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			DiscountList objDscLst=null; //Application layer component			
			try
			{
                objDscLst = new DiscountList(connectionString, base.ClientId);
				p_objXmlOut=objDscLst.Get(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("DiscountList.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objDscLst = null;
			}
		}

		#endregion
	}
}