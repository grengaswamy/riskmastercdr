﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Linq;
using Riskmaster.Settings;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using System.Collections;
namespace Riskmaster.BusinessAdaptor.RMUtilities
{
    class BRSSettingAdaptor : BusinessAdaptorBase
    {
        private const string m_FieldDelimiter = "|";

        public BRSSettingAdaptor()
        {
        }


        /// <summary>
        /// This method is used to retrieve BRS settings from the database
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {		
            try
            {
                GetSettings(null, ref p_objXmlOut);

                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("BRSSettingrAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error); //mbahl3 jira [RMACLOUD-60]
                return false;
            }
            finally
            {
            }
        }


        /// <summary>
        /// Generate the XML docuemnt which will be return back to UI layer
        /// </summary>
        /// <param name="p_objBrsSetting">BRSSettings object</param>
        /// <param name="p_objXmlout">XML DOM object return back</param>
        private void GetSettings(BrsSettings  p_objBrsSetting, ref XmlDocument p_objXmlOut)
        {
            if (p_objBrsSetting == null)
            {
                p_objBrsSetting = new BrsSettings(connectionString, base.ClientId);
            }

            XElement oXmlOut = XElement.Parse("<Settings/>");
            if (oXmlOut != null)
            {
                string sFilePath = RMConfigurator.CombineFilePath(RMConfigurator.AppFilesPath, @"BRS\BRSFields.xml");
                XElement oFieldsElement = XElement.Load(sFilePath);
                IEnumerable<XElement> oFields = from el in oFieldsElement.Elements("control")
                                                orderby (string)el.Attribute("title")
                                                select el;

                //If the field already in the KeepOnNext list, it should not be added to the
                //candiate field list again
                XElement oCandidateFields = XElement.Parse("<CandidateFields/>");
                XElement oKeepOnNextFields = XElement.Parse("<KeepOnNextFields/>");
                string sKeepOnNextFields = string.Format("{0}{1}{0}", m_FieldDelimiter, p_objBrsSetting.KeepOnNext);
                foreach (XElement oField in oFields)
                {
                    if (sKeepOnNextFields.IndexOf(string.Format("{0}{1}{0}", m_FieldDelimiter, oField.Attribute("name").Value)) < 0)
                    {
                        oCandidateFields.Add(XElement.Parse(string.Format("<option value='{0}'>{1}</option>", oField.Attribute("name").Value, oField.Attribute("title").Value)));
                    }
                    else
                    {
                        oKeepOnNextFields.Add(XElement.Parse(string.Format("<option value='{0}'>{1}</option>", oField.Attribute("name").Value, oField.Attribute("title").Value)));
                    }
                }
                oXmlOut.Add(oCandidateFields);
                oXmlOut.Add(oKeepOnNextFields);

                //RMWorld may use 1 to indicate true instead of -1
                string sGenEOBCodeFlag = p_objBrsSetting.GenEOBCodeFlag != 0 ? "True" : "False";
                oXmlOut.Add(XElement.Parse(string.Format("<{0}>{1}</{0}>", "GenEOBCodeFlag", sGenEOBCodeFlag)));
                
                string sDiscOnSchdFlag = p_objBrsSetting.DiscOnSchdFlag != 0 ? "True" : "False";
                oXmlOut.Add(XElement.Parse(string.Format("<{0}>{1}</{0}>", "DiscOnSchdFlag", sDiscOnSchdFlag)));

                string sDiscOnBillFlag = p_objBrsSetting.DiscOnBillFlag != 0 ? "True" : "False";
                oXmlOut.Add(XElement.Parse(string.Format("<{0}>{1}</{0}>", "DiscOnBillFlag", sDiscOnBillFlag)));
                
                string sDisc2ndSchdFlag = p_objBrsSetting.Disc2ndSchdFlag != 0 ? "True" : "False";
                oXmlOut.Add(XElement.Parse(string.Format("<{0}>{1}</{0}>", "Disc2ndSchdFlag", sDisc2ndSchdFlag)));

                //Mona: preventing duplication of BRS splits:start                
                string sDupSplitsFlag = p_objBrsSetting.DupSplitsFlag != 0 ? "True" : "False";
                oXmlOut.Add(XElement.Parse(string.Format("<{0}>{1}</{0}>", "DupSplitsFlag", sDupSplitsFlag)));

                string sDupSplitsCriteria = p_objBrsSetting.DupSplitsCriteria.ToString() ;
                //oXmlOut.Add(XElement.Parse(string.Format("<{0}>{1}</{0}>", "DupSplitsCriteria", sDupSplitsCriteria)));
                XElement xDupSplitsCriteria = new XElement("DupSplitsCriteria"); 
                xDupSplitsCriteria.SetAttributeValue("value", sDupSplitsCriteria);
               
                Hashtable objTable = new Hashtable();
                objTable.Add("1", "1-From Date, To Date and Billing Code");

                XElement objElement = null;
                IDictionaryEnumerator objEnum = objTable.GetEnumerator();
                while (objEnum.MoveNext())
                {
                    objElement = new XElement("option");
                    objElement.SetAttributeValue("value", objEnum.Key.ToString());
                    objElement.Value = objEnum.Value.ToString();
                    xDupSplitsCriteria.Add(objElement);
                }
                oXmlOut.Add(xDupSplitsCriteria);
                //Mona: preventing duplication of BRS splits:End

                //This field is not used anywhere in the code, just leave here.
                string sEOBMiPdf = p_objBrsSetting.EOBMiPdf != 0 ? "True" : "False";
                oXmlOut.Add(XElement.Parse(string.Format("<{0}>{1}</{0}>", "EOBMiPdf", sEOBMiPdf)));
            }

            if (p_objXmlOut == null)
            {
                p_objXmlOut = new XmlDocument();
            }
            p_objXmlOut.LoadXml(oXmlOut.ToString());
        }
 
        /// <summary>
        /// This method is used to save BRS settings
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            bool bSuccess = false;
            try
            {
                XElement oXmlInput = XElement.Parse(p_objXmlIn.OuterXml);
                BrsSettings oBrsSetting = new BrsSettings(connectionString, base.ClientId);

                XElement oKeepOnNext = oXmlInput.XPathSelectElement("//SelectedField");
                if (oKeepOnNext != null)
                {
                    oBrsSetting.KeepOnNext = oKeepOnNext.Value;
                }
                
                XElement oGenEOBCodeFlag = oXmlInput.XPathSelectElement("//GenEOBCodeFlag");
                if (oGenEOBCodeFlag != null)
                {
                    oBrsSetting.GenEOBCodeFlag = Conversion.ConvertStrToInteger(oGenEOBCodeFlag.Value);
                }

                XElement oDiscOnSchdFlag = oXmlInput.XPathSelectElement("//DiscOnSchdFlag");
                if (oDiscOnSchdFlag != null)
                {
                    oBrsSetting.DiscOnSchdFlag = Conversion.ConvertStrToInteger(oDiscOnSchdFlag.Value);
                }

                XElement oDiscOnBillFlag = oXmlInput.XPathSelectElement("//DiscOnBillFlag");
                if (oDiscOnBillFlag != null)
                {
                    oBrsSetting.DiscOnBillFlag = Conversion.ConvertStrToInteger(oDiscOnBillFlag.Value);
                }

                XElement oDisc2ndSchdFlag = oXmlInput.XPathSelectElement("//Disc2ndSchdFlag");
                if (oDisc2ndSchdFlag != null)
                {
                    oBrsSetting.Disc2ndSchdFlag = Conversion.ConvertStrToInteger(oDisc2ndSchdFlag.Value);
                }

                XElement oEOBMiPdf = oXmlInput.XPathSelectElement("//EOBMiPdf");
                if (oEOBMiPdf != null)
                {
                    oBrsSetting.EOBMiPdf = Conversion.ConvertStrToInteger(oEOBMiPdf.Value);
                }

                XElement oDupSplitsFlag = oXmlInput.XPathSelectElement("//DupSplitsFlag");
                if (oDupSplitsFlag != null)
                {
                    oBrsSetting.DupSplitsFlag = Conversion.ConvertStrToInteger(oDupSplitsFlag.Value);
                }

                XAttribute oDupSplitsCriteria = oXmlInput.XPathSelectElement("//DupSplitsCriteria").Attribute("value");
                if (oDupSplitsCriteria != null)
                {
                    oBrsSetting.DupSplitsCriteria = Conversion.CastToType<Int32>(oDupSplitsCriteria.Value, out bSuccess);
                }

                oBrsSetting.SaveSettings();

                GetSettings(oBrsSetting, ref p_objXmlOut);

                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("BRSSettingrAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error); //mbahl3 jira [RMACLOUD-60]
                return false;
            }
            finally
            {
            }
        }
    }
}
