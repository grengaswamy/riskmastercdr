using System;
using System.Xml;
using Riskmaster.Common; 
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: MinBillThresholdAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 02-June-2005
	///* $Author	: Anurag Agarwal
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for MinBillThreshold class  
	/// </summary>
	public class MinBillThresholdAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		public MinBillThresholdAdaptor()
		{
		}
		#endregion

		#region Public Function
		/// <summary>
		/// This function xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">
		///		Input parameters as xml
		///		<document>
		///			<RowId></RowId>
		///		</document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml
		///		Output structure is as follows-:
		///		See Xml file named MinBillThreshold.xml in Xml folder
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Get(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			MinBillThreshold objMinBillThreshold=null;	
			try
			{
                objMinBillThreshold = new MinBillThreshold(userLogin, userLogin.LoginName, connectionString, base.ClientId);//vkumar258 ML changes
				if (p_objXmlIn.SelectSingleNode("//control[@name='RowId']").InnerText != string.Empty) 
					p_objXmlOut=objMinBillThreshold.Get(p_objXmlIn);
				else
					p_objXmlOut=p_objXmlIn;
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("MinBillThresholdAdaptor.Get.Error", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objMinBillThreshold=null;
			}
		}

		/// <summary>
		///		This function Saves Layout data
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Save(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			MinBillThreshold objMinBillThreshold=null;	
			try
			{
                objMinBillThreshold = new MinBillThreshold(userLogin.LoginName, connectionString, base.ClientId);				
				objMinBillThreshold.Save(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("MinBillThresholdAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objMinBillThreshold=null;
			}
		}
		#endregion
	}
}

