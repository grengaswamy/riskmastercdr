using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;  

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: WPAAutoDiarySetupAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 01/06/2005
	///* $Author	: Pankaj
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for RM Utilities
	///	which implements the functionality of WPA Auto Diary Setup form.
	/// </summary>
	public class WPAAutoDiarySetupAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		/// <summary>
		/// Default Constructor
		/// </summary>
		public WPAAutoDiarySetupAdaptor(){}

		#endregion

		#region Public Methods


		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.WPAAutoDiarySetup.Get() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.WPAAutoDiarySetup.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			WPAAutoDiarySetup objAuto=null; //Application layer component
			try
			{
				objAuto = new WPAAutoDiarySetup(userLogin.LoginName,userLogin.Password,userLogin.objRiskmasterDatabase.DataSourceName,base.ClientId);//sonali
				p_objXmlOut=objAuto.Get(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException, Globalization.GetString("WPAAutoDiarySetupAdaptor.Get.Error",base.ClientId),BusinessAdaptorErrorType.Error);//sonali
				return false;
			}
			finally
			{
				objAuto = null;
			}
		}

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.WPAAutoDiarySetup.Save() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.WPAAutoDiarySetup.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			WPAAutoDiarySetup objAuto=null; //Application layer component
			try
			{
				objAuto = new WPAAutoDiarySetup(userLogin.LoginName,userLogin.Password,userLogin.objRiskmasterDatabase.DataSourceName,base.ClientId);//sonali
				p_objXmlOut=objAuto.Save(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException, Globalization.GetString("WPAAutoDiarySetupAdaptor.Save.Error",base.ClientId),BusinessAdaptorErrorType.Error);//sonali
				return false;
			}
			finally
			{
				objAuto = null;
			}
		}

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.WPAAutoDiarySetup.LoadInfoDef() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.WPAAutoDiarySetup.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool LoadInfoDef(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			WPAAutoDiarySetup objAuto=null; //Application layer component
			XmlElement objElm=null;
			int iDefId=0;
			try
			{
				objElm=(XmlElement)p_objXmlIn.SelectSingleNode("//DefId"); 
				if(objElm==null)
                    throw new RMAppException(Globalization.GetString("WPAAutoDiarySetupAdaptor.LoadInfoDef.IdMissing", base.ClientId));  
				else
					iDefId= Conversion.ConvertStrToInteger(objElm.InnerText);  
				objAuto = new WPAAutoDiarySetup(userLogin.LoginName,userLogin.Password,userLogin.objRiskmasterDatabase.DataSourceName,base.ClientId);//sonali
				p_objXmlOut=objAuto.LoadInfoDef(p_objXmlIn,iDefId);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("WPAAutoDiarySetupAdaptor.LoadInfoDef.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sonali
				return false;
			}
			finally
			{
				objAuto = null;
			}
		}


		public bool GetAutoDiaryWorkActivities(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			WPAAutoDiarySetup objAuto=null; //Application layer component
			try
			{
                objAuto = new WPAAutoDiarySetup(userLogin.LoginName, userLogin.Password, userLogin.objRiskmasterDatabase.DataSourceName, base.ClientId);//sonali
				p_objXmlOut=objAuto.GetAutoDiaryWorkActivities(p_objXmlIn,base.userLogin); //Aman ML Change
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("WPAAutoDiarySetupAdaptor.GetAutoDiaryWorkActivities.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sonali
				return false;
			}
			finally
			{
				objAuto = null;
			}
		}


		#endregion
	}
}