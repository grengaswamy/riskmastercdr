using System;
using System.IO;
using System.Xml;
using System.Text;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: FROIOptionsAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 09/29/2005
	///* $Author	: Mihika Agrawal
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************

	/// <summary>
	/// This class is used to call the application layer component for FROI Options
	/// </summary>
	public class FROIOptionsAdaptor: BusinessAdaptorBase
	{
		#region Constructor
		public FROIOptionsAdaptor()
		{
		}
		#endregion

		#region Public Methods
		/// <summary>
		/// Wrapper to Application layer function GetFROIOptions()
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GetFROIOptions(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			FROIOptions objFROIOptions = null;
			try
			{
                objFROIOptions = new FROIOptions(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, base.ClientId);
				p_objXmlOut = objFROIOptions.GetFROIOptions(p_objXmlIn);
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("FROIOptionsAdaptor.GetFROIOptions.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objFROIOptions = null;
			}
			return true;
		}
		
		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.FROIOptions.Save() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			FROIOptions objFROIOptions = null; // Application layer component			
			try
			{
                objFROIOptions = new FROIOptions(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, base.ClientId);
				p_objXmlOut = objFROIOptions.Save(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("FROIOptionsAdaptor.Save.Error", base.ClientId),
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objFROIOptions = null;
			}
		}

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.FROIOptions.Delete() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool Delete(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			FROIOptions objFROIOptions = null; //Application layer component	
			try
			{
                objFROIOptions = new FROIOptions(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, base.ClientId);
				p_objXmlOut = objFROIOptions.Delete(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("FROIOptionsAdaptor.Delete.Error", base.ClientId),
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objFROIOptions = null;
			}
		}
		
		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.FROIOptions.New() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool New(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			FROIOptions objFROIOptions = null; // Application layer component			
			try
			{
                objFROIOptions = new FROIOptions(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, base.ClientId);
				p_objXmlOut = objFROIOptions.New(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("FROIOptionsAdaptor.New.Error", base.ClientId),
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objFROIOptions = null;
			}
		}		

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.FROIOptions.GetFROIHistory() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool GetFROIHistory(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			FROIOptions objFROIOptions = null; // Application layer component			
			try
			{
                objFROIOptions = new FROIOptions(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, base.ClientId);
				p_objXmlOut = objFROIOptions.GetFROIHistory(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("FROIOptionsAdaptor.GetFROIHistory.Error", base.ClientId),
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objFROIOptions = null;
			}
		}		
		
		#endregion
	}
}
