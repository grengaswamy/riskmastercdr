using System;
using System.Text;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common; 

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: WPAUtilAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 24-May-2005
	///* $Author	: Parag Sarin
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	
	/// <summary>	
	///	This class is used to call the application layer component for WPAUtil class
	/// </summary>
	public class WPAUtilAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		public WPAUtilAdaptor(){}
		#endregion

		#region Public Function
		/// <summary>
		/// This function xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		/*
		 <WPAUtil>
			<Owners>
			<User userid="2">parag sarin</User>
			<User userid="3">test test</User>
			</Owners>
			<Transfer>
			<FromUser />
			<ToUser />
			<DiaryStatus>
			<Open />
			<Close />
			</DiaryStatus>
			<Dates>
			<From />
			<To />
			</Dates>
			</Transfer>
			<Purge>
			<User />
			<DiaryStatus>
			<Open />
			<Close />
			</DiaryStatus>
			<Dates>
			<From />
			<To />
			</Dates>
			</Purge>
		</WPAUtil>
		*/
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Get(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			WPAUtil objUtil=null;			
			try
			{
				objUtil=new WPAUtil(userLogin.objRiskmasterDatabase.DataSourceId.ToString(),userID.ToString(),securityConnectionString,connectionString,base.ClientId);	//psharma206 jira 101		
				
				p_objXmlOut=objUtil.Get();
				
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("WPAUtilAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);//psharma206	jira 101
				return false;
			}
			finally
			{
				objUtil=null;
			}
		}
		/// <summary>
		/// This function Saves Diary data
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Input structure is as follows-:
		/*
		 <WPAUtil>
			<Owners>
			<User userid="2">parag sarin</User>
			<User userid="3">test test</User>
			</Owners>
			<Transfer>
			<FromUser />
			<ToUser />
			<DiaryStatus>
			<Open />
			<Close />
			</DiaryStatus>
			<Dates>
			<From />
			<To />
			</Dates>
			</Transfer>
			<Purge>
			<User />
			<DiaryStatus>
			<Open />
			<Close />
			</DiaryStatus>
			<Dates>
			<From />
			<To />
			</Dates>
			</Purge>
		</WPAUtil>
		*/
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool SaveDiaryTransfer(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			WPAUtil objUtil=null;
            int iRowsAffected = 0;
			try
			{
				objUtil=new WPAUtil(userLogin.objRiskmasterDatabase.DataSourceId.ToString(),userID.ToString(),securityConnectionString,connectionString,base.ClientId);			
				
				objUtil.SaveDiaryTransfer(p_objXmlIn,ref iRowsAffected);
                
                System.Exception exc = new Exception(iRowsAffected.ToString() + " Rows affected.");
                p_objErrOut.Add(exc, iRowsAffected.ToString() + " Rows affected.", BusinessAdaptorErrorType.PopupMessage);

				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("WPAUtilAdaptor.SaveDiaryTransfer.Error", base.ClientId), BusinessAdaptorErrorType.Error);//psharma206	jira 101
				return false;
			}
			finally
			{
				objUtil=null;
			}
		}
		/// <summary>
		/// This function Saves Diary data
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Input structure is as follows-:
		/*
		 <WPAUtil>
			<Owners>
			<User userid="2">parag sarin</User>
			<User userid="3">test test</User>
			</Owners>
			<Transfer>
			<FromUser />
			<ToUser />
			<DiaryStatus>
			<Open />
			<Close />
			</DiaryStatus>
			<Dates>
			<From />
			<To />
			</Dates>
			</Transfer>
			<Purge>
			<User />
			<DiaryStatus>
			<Open />
			<Close />
			</DiaryStatus>
			<Dates>
			<From />
			<To />
			</Dates>
			</Purge>
		</WPAUtil>
		*/
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool SaveDiaryPurge(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			WPAUtil objUtil=null;				
			try
			{
				objUtil=new WPAUtil(userLogin.objRiskmasterDatabase.DataSourceId.ToString(),userID.ToString(),securityConnectionString,connectionString,base.ClientId);			
				
				objUtil.SaveDiaryPurge(p_objXmlIn);
				
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("WPAUtilAdaptor.SaveDiaryPurge.Error", base.ClientId), BusinessAdaptorErrorType.Error); //psharma206	jira 102
				return false;
			}
			finally
			{
				objUtil=null;
			}
		}
		#endregion
	}
}
