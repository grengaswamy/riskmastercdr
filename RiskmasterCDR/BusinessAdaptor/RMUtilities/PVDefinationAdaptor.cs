using System;
using System.Xml;
using Riskmaster.Common; 
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: PVDefinationAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 27-May-2005
	///* $Author	: Anurag Agarwal
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for PVDefination class  
	/// </summary>
	public class PVDefinationAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		public PVDefinationAdaptor()
		{
		}
		#endregion

		#region Public Function
		/// <summary>
		/// This function xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">
		///		Input parameters as xml
		///		<Document>
		///			<RowId></RowId>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml
		///		Output structure is as follows-:
		///		See Xml file named PVDefination.xml in Xml folder
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Get(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			PVDefination objPVDefination=null;	

			try
			{
				objPVDefination=new  PVDefination(userLogin.LoginName,userLogin.Password,userLogin.objRiskmasterDatabase.DataSourceName,userLogin.objRiskmasterDatabase.DataSourceId.ToString(),connectionString, base.ClientId); //mbahl3 JIRA [RMACLOUD-123]
				p_objXmlOut=objPVDefination.Get(p_objXmlIn);

				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("PVDefinationAdaptor.Get.Error", base.ClientId),BusinessAdaptorErrorType.Error); //mbahl3 JIRA [RMACLOUD-123]
				return false;
			}
			finally
			{
				objPVDefination=null;
			}
		}

		/// <summary>
		///		This function Saves Layout data
		/// </summary>
		/// <param name="p_objXmlIn">
		/// Input parameters as xml
		///		<Document>
		///			<RowId>69</RowId>
		///			<SelectedForms>eventdatedtext.xml|concomitant.xml|osha.xml|</SelectedForms>
		///			<SelectedFormsCaption>___Event Dated Text|___Concomitant Products|___OSHA|</SelectedFormsCaption>
		///			<SelectedUsers>___Event Dated Text|___Concomitant Products|___OSHA|</SelectedUsers>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Save(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			PVDefination objPVDefination=null;	
			try
			{
				objPVDefination=new PVDefination(userLogin.LoginName,userLogin.Password,userLogin.objRiskmasterDatabase.DataSourceName,userLogin.objRiskmasterDatabase.DataSourceId.ToString(),connectionString, base.ClientId); //mbahl3 	JIRA [RMACLOUD-123]			
				objPVDefination.Save(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("PVDefinationAdaptor.Save.Error", base.ClientId),BusinessAdaptorErrorType.Error); //mbahl3 JIRA [RMACLOUD-123]
				return false;
			}
			finally
			{
				objPVDefination=null;
			}
		}
		#endregion
	}
}
