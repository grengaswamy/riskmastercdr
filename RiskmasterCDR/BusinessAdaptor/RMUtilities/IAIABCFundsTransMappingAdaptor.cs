using System;
using System.Text;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common; 

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: IAIABCFundsTransMappingAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 24-May-2005
	///* $Author	: Parag Sarin
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	
	/// <summary>	
	///	This class is used to call the application layer component for IAIABCFundsTransMapping class
	/// </summary>
	public class IAIABCFundsTransMappingAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		public IAIABCFundsTransMappingAdaptor(){}
		#endregion

		#region Public Function
		/// <summary>
		/// This function xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///	See Xml file named IAIABCFundsTransMapping.xml in Xml folder
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Get(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			IAIABCFundsTransMapping objTransMapping=null;	
			try
			{
				objTransMapping=new IAIABCFundsTransMapping(userLogin.LoginName,connectionString,base.ClientId);//sonali			
				p_objXmlOut=objTransMapping.Get(p_objXmlIn,base.userLogin);//Aman ML Change
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("IAIABCFundsTransMappingAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sonali
				return false;
			}
			finally
			{
				objTransMapping=null;
			}
		}
		/// <summary>
		/// This function Saves acord and claim type setup data
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Input structure is as follows-:
		///	See Xml file named IAIABCFundsTransMapping.xml in Xml folder
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool DeleteBenefitsMapping(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			IAIABCFundsTransMapping objTransMapping=null;				
			try
			{

                objTransMapping = new IAIABCFundsTransMapping(userLogin.LoginName, connectionString, base.ClientId);	//sonali		
				objTransMapping.DeleteBenefitsMapping(p_objXmlIn);
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("IAIABCFundsTransMappingAdaptor.DeleteBenefitsMapping.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sonali
				return false;
			}
			finally
			{
				objTransMapping=null;
			}
		}
		/// <summary>
		/// This function Saves acord and claim type setup data
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Input structure is as follows-:
		///	See Xml file named IAIABCFundsTransMapping.xml in Xml folder
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool DeletePaidToDateMapping(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			IAIABCFundsTransMapping objTransMapping=null;				
			try
			{
			
				objTransMapping=new IAIABCFundsTransMapping(userLogin.LoginName,connectionString,base.ClientId);	//sonali		
				objTransMapping.DeletePaidToDateMapping(p_objXmlIn);
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("IAIABCFundsTransMappingAdaptor.DeletePaidToDateMapping.Error",base.ClientId),BusinessAdaptorErrorType.Error);//sonali
				return false;
			}
			finally
			{
				objTransMapping=null;
			}
		}
		/// <summary>
		/// This function saves paid to date mapping data
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Input structure is as follows-:
		///	See Xml file named IAIABCFundsTransMapping.xml in Xml folder
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool SavePaidToDate(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			IAIABCFundsTransMapping objTransMapping=null;				
			try
			{

                objTransMapping = new IAIABCFundsTransMapping(userLogin.LoginName, connectionString, base.ClientId);	//sonali		
				objTransMapping.SavePaidToDate(p_objXmlIn);
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("IAIABCFundsTransMappingAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sonali
				return false;
			}
			finally
			{
				objTransMapping=null;
			}
		}
		/// <summary>
		/// This function benefits mapping data
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Input structure is as follows-:
		///	See Xml file named IAIABCFundsTransMapping.xml in Xml folder
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool SaveBenefits(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			IAIABCFundsTransMapping objTransMapping=null;				
			try
			{

                objTransMapping = new IAIABCFundsTransMapping(userLogin.LoginName, connectionString, base.ClientId);//sonali			
				objTransMapping.SaveBenefits(p_objXmlIn);
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("IAIABCFundsTransMappingAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sonali
				return false;
			}
			finally
			{
				objTransMapping=null;
			}
		}
		#endregion
	}
}
