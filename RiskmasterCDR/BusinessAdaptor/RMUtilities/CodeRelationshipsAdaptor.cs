﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.Application.RMUtilities;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using Riskmaster.Common;
namespace Riskmaster.BusinessAdaptor
{
    /// <summary>
    ///Author  :   Neha Sharma
    ///Dated   :   30th May,2011
    ///Purpose :   Contains the methods for Code Relationships.
    /// </summary>
    public class CodeRelationshipsAdaptor: BusinessAdaptorBase
    {
        /// <summary>
        /// Adapter for Code realtionship Class for intial load
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            CodeRelationships objFormSetup = null;
            try
            {
                objFormSetup = new CodeRelationships(connectionString, userLogin.LoginName, base.ClientId);//Add by kuladeep for Cloud Jira-69
                objFormSetup.LanguageCode = base.userLogin.objUser.NlsCode; //Aman ML Change
                p_objXmlOut = objFormSetup.Get(p_objXmlIn);

                return true;

            }
           catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("CodeRelationshipsAdaptor.Get.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objFormSetup = null;
            }
        }


        public bool GetSubData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            CodeRelationships objFormSetup = null;
            try
            {
                objFormSetup = new CodeRelationships(connectionString, userLogin.LoginName, base.ClientId);//Add by kuladeep for Cloud Jira-69
                objFormSetup.LanguageCode = base.userLogin.objUser.NlsCode; //Aman ML Change
                p_objXmlOut = objFormSetup.GetSubData(p_objXmlIn);

                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("CodeRelationshipsAdaptor.GetSubData.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objFormSetup = null;
            }
        }

        /// <summary>
        /// Loads the data on selection of the code 1
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool GetRelatedLossComponents(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            CodeRelationships objSys = null; //Application layer component		
            try
            {

                objSys = new CodeRelationships(connectionString, userLogin.LoginName, base.ClientId);//Add by kuladeep for Cloud Jira-69
                p_objXmlOut = objSys.GetRelatedLossComponents(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("CodeRelationshipsAdaptor.Save.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objSys = null;
            }
        }

        /// <summary>
        /// Adaptor method to save the data in the database
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            CodeRelationships objSys = null; //Application layer component		
            try
            {

                objSys = new CodeRelationships(connectionString, userLogin.LoginName, base.ClientId);//Add by kuladeep for Cloud Jira-69
                p_objXmlOut = objSys.Save(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("CodeRelationshipsAdaptor.Save.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objSys = null;
            }
        }

    }
}
