﻿using System;
using System.Text;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
    class PolicyCodeMappingAdaptor:BusinessAdaptorBase
    {

        public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            PolicyCodeMapping objPolicyCodeMapping = null;
            try
            {
                objPolicyCodeMapping = new PolicyCodeMapping(connectionString);
                p_objXmlOut = objPolicyCodeMapping.Get(p_objXmlIn);
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("PolicyCodeMappingAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objPolicyCodeMapping = null;
            }
        }
        public bool GetCodeDetails(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            PolicyCodeMapping objPolicyCodeMapping = null;
            try
            {
                objPolicyCodeMapping = new PolicyCodeMapping(connectionString);
                p_objXmlOut = objPolicyCodeMapping.GetCodeDetails(p_objXmlIn);
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("PolicyCodeMappingAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objPolicyCodeMapping = null;
            }
        }
        public bool SavePolicyCodeInfo(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            XmlElement objElement = null;
            PolicyCodeMapping objPolicyCodeMapping = null;

            try
            {
                //rupal:start
                //check existence of RMCode which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//RMCode");
                if (objElement != null && (objElement.InnerText == string.Empty || objElement.InnerText == "0"))
                {
                    p_objErrOut.Add("PolicyCodeMappingAdaptorRMCodeMissing", Globalization.GetString("PolicyCodeMappingAdaptor.SavePolicyCodeInfo.RMCodeMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }

                //check existence of PolSysCode which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//PolSysCode");
                if (objElement != null && (objElement.InnerText == string.Empty || objElement.InnerText == "0"))
                {
                    p_objErrOut.Add("PolicyCodeMappingAdaptorPolSysCodeMissing", Globalization.GetString("PolicyCodeMappingAdaptor.SavePolicyCodeInfo.PolSysCodeMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }

                //check existence of CodeType which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//CodeType");
                if (objElement != null && (objElement.InnerText == string.Empty || objElement.InnerText == "0"))
                {
                    p_objErrOut.Add("PolicyCodeMappingAdaptorCodeTypeMissing", Globalization.GetString("PolicyCodeMappingAdaptor.SavePolicyCodeInfo.CodeTypeMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }

                //check existence of PolicySystemId which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//PolicySystemId");
                if (objElement != null && (objElement.InnerText == string.Empty || objElement.InnerText == "0"))
                {
                    p_objErrOut.Add("PolicyCodeMappingAdaptorPolicySystemIdMissing", Globalization.GetString("PolicyCodeMappingAdaptor.SavePolicyCodeInfo.PolicySystemIdMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                //rupal:end
                objPolicyCodeMapping = new PolicyCodeMapping(connectionString);
                objPolicyCodeMapping.SavePolicyCodeInfo(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("PolicyCodeMappingAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objPolicyCodeMapping = null;
            }
        }
        public bool DeleteCodeMapping(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            PolicyCodeMapping objPolicyCodeMapping = null; //Application layer component			
            try
            {
                objPolicyCodeMapping = new PolicyCodeMapping(connectionString);
                p_objXmlOut = objPolicyCodeMapping.DeleteCodeMapping(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("PolicyCodeMappingAdaptor.Delete.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objPolicyCodeMapping = null;
            }
        }

        public bool GetClaimTypes(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            PolicyCodeMapping objPolicyCodeMapping = null; 			
            try
            {
                objPolicyCodeMapping = new PolicyCodeMapping(connectionString);
                p_objXmlOut = objPolicyCodeMapping.GetClaimTypes(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("PolicyCodeMappingAdaptor.GetClaimType.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objPolicyCodeMapping = null;
            }
        }
    }

}
