﻿using System;
using System.Text;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common; 

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
    public class AutoDiscTransReserveMappingAdapter : BusinessAdaptorBase
    {

        #region Constructor
        public AutoDiscTransReserveMappingAdapter() { }
		#endregion



        public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            AutoDiscTransReserveMapping objAutoDiscTransReserveMapping = null;
            try
            {
                objAutoDiscTransReserveMapping = new AutoDiscTransReserveMapping(userLogin.LoginName, connectionString, base.ClientId);
                objAutoDiscTransReserveMapping.LanguageCode = base.userLogin.objUser.NlsCode; //Aman ML Change
                p_objXmlOut = objAutoDiscTransReserveMapping.Get();
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("AutoDiscTransReserveMapping.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objAutoDiscTransReserveMapping = null;
            }
        }

        public bool DeleteMapping(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            AutoDiscTransReserveMapping objAutoDiscTransReserveMapping = null;
            try
            {

                objAutoDiscTransReserveMapping = new AutoDiscTransReserveMapping(userLogin.LoginName, connectionString, base.ClientId);
                objAutoDiscTransReserveMapping.DeleteMapping(p_objXmlIn);
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("AutoDiscTransReserveMapping.DeleteMapping.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objAutoDiscTransReserveMapping = null;
            }
        }


        public bool SaveMapping(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            AutoDiscTransReserveMapping objAutoDiscTransReserveMapping = null;
            try
            {

                objAutoDiscTransReserveMapping = new AutoDiscTransReserveMapping(userLogin.LoginName, connectionString, base.ClientId);
                objAutoDiscTransReserveMapping.SaveMapping(p_objXmlIn);
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("AutoDiscTransReserveMapping.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objAutoDiscTransReserveMapping = null;
            }
        }

    }
}
