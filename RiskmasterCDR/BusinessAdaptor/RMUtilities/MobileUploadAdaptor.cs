﻿using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
    ///************************************************************** 
    ///* $File		: MobileUploadAdaptor.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 09-aug-2011
    ///* $Author	: Amandeep Kaur
    ///* $Comment	: 
    ///* $Source	: 
    ///**************************************************************
    /// <summary>	
    ///	This class is used to call the application layer component for RM Utilities
    ///	which will check for the Mobile Upload related jobs like Diary Status.
    /// </summary>
    public class MobileUploadAdaptor : BusinessAdaptorBase
    {
        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        public MobileUploadAdaptor() { }

        #endregion

        #region Public Methods

        /// <summary>
        ///	This method is a wrapper to Riskmaster.Application.RMUtilities.MobileUpload.GetStatusofDiary() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///	The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.MobileUpload.xml
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>

        public bool GetStatus(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            MobileUpload objMobile = null;      //Application layer component
            try
            {
                objMobile = new MobileUpload(userLogin.LoginName, userLogin.Password, userLogin.objRiskmasterDatabase.DataSourceName, base.ClientId);
                p_objXmlOut = objMobile.GetStatusofDiary(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("MobileUploadAdaptor.GetStatus.Error", base.ClientId), p_objException);
            }
            finally
            {
                objMobile = null;
            }
        }

        #endregion
    }
}
