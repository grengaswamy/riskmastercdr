using System;
using System.Text;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: AdjusterTransferAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 24-May-2005
	///* $Author	: Parag Sarin
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	
	/// <summary>	
	///	This class is used to call the application layer component for Adjuster Transfer which 
	///	contains methods to transfer adjusters.	
	/// </summary>
	public class AdjusterTransferAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		public AdjusterTransferAdaptor(){}
		#endregion

		#region Public methods

		/// <summary>
		/// This function returns xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///	<Adjuster>
		///		<control name="AvailClaims" type="combobox" title="Available Claims:">
		///		<option value="295">DI2003000295</option>
		///		</control>
		///		<control name="FromAdjuster" type="combobox" title="From Adjuster" value="168">
		///		<option value="">
		///			</option>
		///			<option value="168">Adjuster, Anna</option>
		///			 
		///		</control>
		///		<control name="ToAdjuster" type="combobox" title="To Adjuster" value="218">
		///		<option value="">
		///			</option>
		///			<option value="168">Adjuster, Anna</option>
		///		</control>
		///		<control name="SelectedClaims" type="combobox" title="Selected Claims" value="295">
		///		</control>
		///		<control name="OnlyOpenClaims" type="checkbox" title="Only Open Claims" value="1" />
		/// </Adjuster>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Get(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			AdjusterTransfer objAdjTransfer=null;			
			try
			{
                objAdjTransfer = new AdjusterTransfer(connectionString, base.ClientId);		//psharma206 jira     98		
				p_objXmlOut=objAdjTransfer.Get();
                return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("AdjusterTransferAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);//psharma206 jira  98
				return false;
			}
			finally
			{
				objAdjTransfer=null;
			}
		}

		/// <summary>
		/// This function retrieves Claims data to load on to the screen
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///	<Adjuster>
		///		<control name="AvailClaims" type="combobox" title="Available Claims:">
		///		<option value="295">DI2003000295</option>
		///		</control>
		///		<control name="FromAdjuster" type="combobox" title="From Adjuster" value="168">
		///		<option value="">
		///			</option>
		///			<option value="168">Adjuster, Anna</option>
		///			 
		///		</control>
		///		<control name="ToAdjuster" type="combobox" title="To Adjuster" value="218">
		///		<option value="">
		///			</option>
		///			<option value="168">Adjuster, Anna</option>
		///		</control>
		///		<control name="SelectedClaims" type="combobox" title="Selected Claims" value="295">
		///		</control>
		///		<control name="OnlyOpenClaims" type="checkbox" title="Only Open Claims" value="1" />
		/// </Adjuster>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GetAvailableClaims(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			AdjusterTransfer objAdjTransfer=null;			
			try
			{
                objAdjTransfer = new AdjusterTransfer(connectionString, base.ClientId);		//psharma206 jira     98	
				p_objXmlOut=objAdjTransfer.GetAvailableClaims(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("AdjusterTransferAdaptor.GetAvailableClaims.Error", base.ClientId), BusinessAdaptorErrorType.Error);//psharma206 jira  98
				return false;
			}
			finally
			{
				objAdjTransfer=null;
			}
		}
		/// <summary>
		/// This function transfers Adjusters
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///	<Adjuster>
		///		<control name="AvailClaims" type="combobox" title="Available Claims:">
		///		<option value="295">DI2003000295</option>
		///		</control>
		///		<control name="FromAdjuster" type="combobox" title="From Adjuster" value="168">
		///		<option value="">
		///			</option>
		///			<option value="168">Adjuster, Anna</option>
		///			 
		///		</control>
		///		<control name="ToAdjuster" type="combobox" title="To Adjuster" value="218">
		///		<option value="">
		///			</option>
		///			<option value="168">Adjuster, Anna</option>
		///		</control>
		///		<control name="SelectedClaims" type="combobox" title="Selected Claims" value="295">
		///		</control>
		///		<control name="OnlyOpenClaims" type="checkbox" title="Only Open Claims" value="1" />
		/// </Adjuster>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Save(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			AdjusterTransfer objAdjTransfer=null;			
			try
			{
                objAdjTransfer = new AdjusterTransfer(connectionString, base.ClientId);	//psharma206 jira     98			
				objAdjTransfer.Save(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("AdjusterTransferAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);//psharma206 jira  98
				return false;
			}
			finally
			{
				objAdjTransfer=null;
			}
		}
		#endregion

	}
}
