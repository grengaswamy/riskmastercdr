﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using System.Xml;
using Riskmaster.Common;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
    class HistorySetUpAdaptor : BusinessAdaptorBase
    {
        # region public methods
        public bool UpdateHistoryFlags(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            HistorySetUp objHS = null;
            bool bReturnValue= false ;
            try
            {
                objHS = new HistorySetUp(userLogin.LoginName, connectionString, base.ClientId);//psharma206
                bReturnValue = objHS.UpdateHistoryFlags(p_objXmlIn);
                return bReturnValue;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("HistorySetUpAdaptor.UpdateHistoryFlags.Error", base.ClientId), BusinessAdaptorErrorType.Error);     //psharma206 jira   102                            
                return false;
            }
            finally
            {
                objHS = null;
            }            
        }
        public bool GetHistoryFlag(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            HistorySetUp objHS = null;
            bool bReturnValue = false;
            try
            {
                objHS = new HistorySetUp(connectionString, base.ClientId);//psharma206	jira 102
                bReturnValue = objHS.GetHistoryFlag(p_objXmlIn, ref p_objXmlOut);
                return bReturnValue;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("HistorySetUpAdaptor.GetHistoryFlag.Error", base.ClientId), BusinessAdaptorErrorType.Error); //psharma206 jira   102                
                return false;
            }
            finally
            {
                objHS = null;
            }

        }
        # endregion
    }
}
