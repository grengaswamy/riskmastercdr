using System;
using System.Xml;
using Riskmaster.Common; 
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: PVFormEditAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 25-May-2005
	///* $Author	: Anurag Agarwal
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for PVFormEdit class  
	/// </summary>
	public class PVFormEditAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		public PVFormEditAdaptor()
		{
		}
		#endregion

		#region Public Function
		/// <summary>
		/// This function xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">
		///		Input parameters as xml
		///		<Document>
		///			<Reset></Reset>
		///			<FormName></FormName>
		///			<RowId></RowId>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml
		///		Output structure is as follows-:
		///		See Xml file named PVFormEdit.xml in Xml folder
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Get(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			PVFormEdit objPVFormEdit=null;	
			try
			{
				objPVFormEdit=new PVFormEdit(userLogin.LoginName,userLogin.Password,userLogin.objRiskmasterDatabase.DataSourceName,userLogin.objRiskmasterDatabase.DataSourceId.ToString(),connectionString, base.ClientId); //mbahl3 JIRA [RMACLOUD-123]
				p_objXmlOut=objPVFormEdit.Get(p_objXmlIn);

                // akaushik5 Added for MITS 30290 Starts
                if (this.IsDynamicView(p_objXmlIn.SelectSingleNode("//FormName").InnerText.Replace(".xml", string.Empty)))
                {
                    objPVFormEdit.GetrequiredTabs(ref p_objXmlOut);
                }
                // akaushik5 Added for MITS 30290 Ends

				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("PVFormEditAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error); //mbahl3 JIRA [RMACLOUD-123] 
				return false;
			}
			finally
			{
				objPVFormEdit=null;
			}
		}

		/// <summary>
		/// T	his function Saves Layout data
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Save(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			PVFormEdit objPVFormEdit=null;	
			try
			{
                objPVFormEdit = new PVFormEdit(userLogin.LoginName, userLogin.Password, userLogin.objRiskmasterDatabase.DataSourceName, userLogin.objRiskmasterDatabase.DataSourceId.ToString(), connectionString, base.ClientId);	 //mbahl3 JIRA [RMACLOUD-123]		
				objPVFormEdit.Save(p_objXmlIn);
                // akaushik5 Added for MITS 30290 Starts
                if (this.IsDynamicView(p_objXmlIn.SelectSingleNode("//FormName").InnerText))
                {
                    p_objXmlIn.SelectSingleNode("//FormName").InnerText = p_objXmlIn.SelectSingleNode("//FormName").InnerText + "d.xml";
                    objPVFormEdit.Save(p_objXmlIn);
                }
                // akaushik5 Added for MITS 30290 Ends
                return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("PVFormEditAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error); //mbahl3 JIRA [RMACLOUD-123]
				return false;
			}
			finally
			{
				objPVFormEdit=null;
			}
		}

        /// <summary>
        /// Determines whether [is dynamic view] [the specified p_obj XML in].
        /// </summary>
        /// <param name="p_objXmlIn">The p_obj XML in.</param>
        /// <returns>
        ///   <c>true</c> if [is dynamic view] [the specified p_obj XML in]; otherwise, <c>false</c>.
        /// </returns>
        // akaushik5 Added for MITS 30290 Starts
        private bool IsDynamicView(string formName)
        {
            return !string.IsNullOrEmpty(RMConfigurationManager.GetAppSetting("DynamicViews")) && RMConfigurationManager.GetAppSetting("DynamicViews").Contains(formName);
        }
        // akaushik5 Added for MITS 30290 Ends

		#endregion
	}
}
