using System;
using System.Data;
using System.Collections;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;
using System.Text;  

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: TableManagerAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 07/08/2005
	///* $Author	: Pankaj
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for RM Utilities
	///	which implements the functionality of Administrative Tracking.
	/// </summary>
	public class TableManagerAdaptor :  BusinessAdaptorBase
	{
		/// <summary>
		/// Default Constructor
		/// </summary>
		public TableManagerAdaptor(){}

		#region Public Methods

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.TableManager.CreateTable() method.
		///		Creates table in the Database
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		/// </param>
		/// <param name="p_objXmlOut">Null</param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool CreateTable(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			TableManager  objTblMgr=null; //Application layer component
			string sSysTableName=string.Empty;			//System Table Name passed in the input XML document
			string sUserTableName=string.Empty;			//User Table Name passed in the input XML document
			XmlElement objElement=null;					//used for parsing the input xml
			string sTableType="";

			try
			{
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//control[@name='SysTableName']");
				sSysTableName=objElement.InnerText;

				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//control[@name='UserTableName']");
				sUserTableName=objElement.InnerText;

				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//control[@name='TableType']");
				sTableType=objElement.InnerText;
                //gagnihotri MITS 11995 Changes made for Audit table
				//objTblMgr = new TableManager(connectionString);
                objTblMgr = new TableManager(connectionString, userLogin.LoginName, base.ClientId);

				objTblMgr.CreateTable(sSysTableName,sUserTableName,sTableType);

				return true;
			}
			catch(RMAppException p_objException)
			{
				//Geeta 07-Oct-06 - Modified for MITS no 8020 for duplicate tables
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.SystemError);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException, Globalization.GetString("TableManagerAdaptor.CreateTable.Error",base.ClientId),BusinessAdaptorErrorType.Error);//psharma206 jira 57
				return false;
			}
			finally
			{
				objTblMgr = null;
				objElement=null;
			}
		}

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.TableManager.DeleteTable() method.
		///		deletes table in the Database
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		/// </param>
		/// <param name="p_objXmlOut">Null</param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool DeleteTable(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			TableManager  objTblMgr=null; //Application layer component
			string sSysTableName=string.Empty;			//System Table Name passed in the input XML document
			string sUserTableName=string.Empty;			//User Table Name passed in the input XML document
			XmlElement objElement=null;					//used for parsing the input xml

			try
			{
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//control[@name='SysTableName']");
				sSysTableName=objElement.InnerText.ToUpper().Trim();

				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//control[@name='UserTableName']");
				sUserTableName=objElement.InnerText.Trim();

				objTblMgr = new TableManager(connectionString, base.ClientId);

				objTblMgr.DeleteTable(sSysTableName,sUserTableName);

				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("TableManagerAdaptor.DeleteTable.Error", base.ClientId), BusinessAdaptorErrorType.Error);//psharma206 jira 57
				return false;
			}
			finally
			{
				objTblMgr = null;
				objElement=null;
			}
		}

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.FieldList.Get() method.
		///		deletes table in the Database
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		/// </param>
		/// <param name="p_objXmlOut">Null</param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool GetFieldList(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			FieldList objFltLst=null; //Application layer component
			IndexList objIndexLst=null;
			try
			{
				objFltLst = new FieldList(connectionString, base.ClientId);
				p_objXmlOut=objFltLst.Get(p_objXmlIn);

				objIndexLst = new IndexList(connectionString, base.ClientId);
				p_objXmlOut=objIndexLst.GetIndexList(p_objXmlOut);   
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("TableManagerAdaptor.GetFieldList.Error", base.ClientId), BusinessAdaptorErrorType.Error);//psharma206 jira 57
				return false;
			}
			finally
			{
				objFltLst = null;
				objIndexLst=null;
			}
		}

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.TableField.Get() method.
		///		deletes table in the Database
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		/// </param>
		/// <param name="p_objXmlOut">Null</param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool GetField(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			TableField  objFld=null; //Application layer component
			try
			{
			//Mgaba2:R7:Need Login name in case Hist Tracking is ON
				//objFld = new TableField(connectionString);
                objFld = new TableField(connectionString, userLogin.LoginName, base.ClientId);                
				p_objXmlOut=objFld.Get(p_objXmlIn, userLogin);
				//p_objXmlOut=objFld.Get(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("TableManagerAdaptor.GetField.Error", base.ClientId), BusinessAdaptorErrorType.Error);//psharma206 jira 57
				return false;
			}
			finally
			{
				objFld = null;
			}
		}

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.TableField.Save() method.
		///		deletes table in the Database
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		/// </param>
		/// <param name="p_objXmlOut">Null</param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool SaveField(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			TableField  objFld=null; //Application layer component
            //string[] sXmlFormName;
			try
			{
                //gagnihotri MITS 11995 Changes made for Audit table
				//objFld = new TableField(connectionString);
                //objFld = new TableField(connectionString, userLogin.LoginName);
                objFld = new TableField(userLogin.LoginName, userLogin.Password, userLogin.objRiskmasterDatabase.DataSourceName, userLogin.objRiskmasterDatabase.DataSourceId.ToString(), connectionString, base.ClientId);
              
				p_objXmlOut=objFld.Save(p_objXmlIn);
                
                //Abhishek Created new function for this----- Start

                //Abhishek: To upgrade Supplemental fields in aspx
                //XmlNode  objNod = p_objXmlIn.SelectSingleNode("//control[@name='XmlFormToUpgrade']");
                //if (objNod != null && objNod.InnerXml != "")
                //{
                //    sXmlFormName = objNod.InnerXml.Split(new Char[] { ',' });

                //    for (int i = 0; i < sXmlFormName.Length; i++)
                //    {
                //      objFld.UpgradeXmlToAspx(sXmlFormName[i]);
                //    }
                //}

                //// Naresh : To upgrade Juris fields in aspx
                //objNod = p_objXmlIn.SelectSingleNode("//control[@name='TableType']");
                //if (objNod != null && objNod.InnerText == "9")
                //{
                //    objNod = p_objXmlIn.SelectSingleNode("//control[@name='SysTableName']");

                //    if (objNod != null && objNod.InnerText != null)
                //    {
                //        objFld.UpgradeXmlToAspxJuris(objNod.InnerText);
                //    }
                //}

                //Abhishek Created new function for this----- End
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("TableManagerAdaptor.SaveField.Error", base.ClientId), BusinessAdaptorErrorType.Error);//psharma206 jira 57
				return false;
			}
			finally
			{
				objFld = null;
			}
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool SaveField_PVUpgrade(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            TableField objFld = null; //Application layer component
            string[] sXmlFormName;
            //RMA-4691 - start - Supplementals not getting update in Riskmaster views case of Non-Powerviewable FDM pages
            XmlNode objPowerviewableNode = null;
            bool bIsPowerviewable = true;
            bool bSuccess = false;
            //RMA-4691 - end
            try
            {
                objFld = new TableField(userLogin.LoginName, userLogin.Password, userLogin.objRiskmasterDatabase.DataSourceName, userLogin.objRiskmasterDatabase.DataSourceId.ToString(), connectionString, base.ClientId);

                //Abhishek: To upgrade Supplemental fields in aspx
                XmlNode objNod = p_objXmlIn.SelectSingleNode("//control[@name='XmlFormToUpgrade']");
                if (objNod != null && objNod.InnerXml != "")
                {
                    sXmlFormName = objNod.InnerXml.Split(new Char[] { ',' });

                    for (int i = 0; i < sXmlFormName.Length; i++)
                    {
                        //RMA-4691 - start - Supplementals not getting update in Riskmaster views case of Non-Powerviewable FDM pages
                        objPowerviewableNode = p_objXmlIn.SelectSingleNode("//control[@name='Powerviewable']");
                        if (objPowerviewableNode != null && !string.IsNullOrWhiteSpace(objPowerviewableNode.InnerText))
                            bIsPowerviewable = Conversion.CastToType<bool>(objPowerviewableNode.InnerText, out bSuccess);
                        objFld.UpgradeXmlToAspx(sXmlFormName[i], bIsPowerviewable);
                        //objFld.UpgradeXmlToAspx(sXmlFormName[i]);
                        //RMA-4691 - end
                    }
                }

                // Naresh : To upgrade Juris fields in aspx
                objNod = p_objXmlIn.SelectSingleNode("//control[@name='TableType']");
                if (objNod != null && objNod.InnerText == "9")
                {
                    objNod = p_objXmlIn.SelectSingleNode("//control[@name='SysTableName']");

                    if (objNod != null && objNod.InnerText != null)
                    {
                        objFld.UpgradeXmlToAspxJuris(objNod.InnerText);
                    }
                }

                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, "TableManagerAdaptor.SaveField_PVUpgrade.Error", BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objFld = null;
                objPowerviewableNode = null;
            }
        }


		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.FieldList.GetAddIndex() method.
		///		deletes table in the Database
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		/// </param>
		/// <param name="p_objXmlOut">Null</param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool GetAddIndex(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			IndexList objIndexLst=null;
			try
			{
				objIndexLst = new IndexList(connectionString, base.ClientId);
				p_objXmlOut=objIndexLst.GetAddIndex(p_objXmlIn);   
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("TableManagerAdaptor.GetAddIndex.Error", base.ClientId), BusinessAdaptorErrorType.Error);//psharma206 jira 57
				return false;
			}
			finally
			{
				objIndexLst=null;
			}
		}

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.FieldList.CreateIndex() method.
		///		deletes table in the Database
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		/// </param>
		/// <param name="p_objXmlOut">Null</param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool CreateIndex(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			IndexList objIndexLst=null;
			try
			{
				objIndexLst = new IndexList(connectionString, base.ClientId);
				p_objXmlOut=objIndexLst.CreateIndex(p_objXmlIn);   
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("TableManagerAdaptor.CreateIndex.Error", base.ClientId), BusinessAdaptorErrorType.Error);//psharma206 jira 57
				return false;
			}
			finally
			{
				objIndexLst=null;
			}
		}


		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.TableField.Get() method.
		///		deletes table in the Database
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		/// </param>
		/// <param name="p_objXmlOut">Null</param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool SwapField(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			FieldList  objFld=null; //Application layer component
			try
			{
                //gagnihotri MITS 11995 Changes made for Audit table
				//objFld = new FieldList(connectionString);
                objFld = new FieldList(connectionString, userLogin.LoginName, base.ClientId);
				p_objXmlOut=objFld.SwapField(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("TableManagerAdaptor.SwapField.Error", base.ClientId), BusinessAdaptorErrorType.Error);//psharma206 jira 57
				return false;
			}
			finally
			{
				objFld = null;
			}
		}
        /// <summary>
        /// Added by Shivendu for Supplemental Grid.
        /// Gets Max Min Rows,Grid Columns in edit mode
        /// </summary>
        /// <param name="p_objXmlIn">Input XML</param>
        /// <param name="p_objXmlOut">Output XML</param>
        /// <param name="p_objErrOut">true/false for success/failure</param>
        /// <returns></returns>
        public bool GetGridInformation(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            TableField objFld = null; //Application layer component
            try
            {
                objFld = new TableField(connectionString, base.ClientId);
                p_objXmlOut = objFld.GetGridInformation(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("TableManagerAdaptor.SaveField.Error", base.ClientId), BusinessAdaptorErrorType.Error);//psharma206 jira 57
                return false;
            }
            finally
            {
                objFld = null;
            }
        }
        //Asharma326 MITS 32386 Starts
        public bool UpdateAllFASFields(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            string s_TableName = "";
            string s_CheckUncheck = "";
            TableField objFld = null;
            try
            {
                XmlNode objNod = p_objXmlIn.SelectSingleNode("//control[@name='SysTableName']");
                if (objNod != null && objNod.InnerXml != "")
                {
                    s_TableName = objNod.InnerText;
                }
                objNod = p_objXmlIn.SelectSingleNode("//control[@name='chkAllFASReportable']");
                if (objNod != null && objNod.InnerXml != "")
                {
                    s_CheckUncheck = objNod.InnerText;
                }
                //call applciation
                objFld = new TableField(connectionString, base.ClientId);
                objFld.UpdateAllFASFields(s_TableName, s_CheckUncheck);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("UpdateAllFASFields.SaveField.Error", base.ClientId), BusinessAdaptorErrorType.Error);//psharma206 jira 57
                return false;
            }
            finally
            {
                objFld = null;
            }
        }
        //Asharma326 MITS 32386 Ends
		#endregion
	}
}