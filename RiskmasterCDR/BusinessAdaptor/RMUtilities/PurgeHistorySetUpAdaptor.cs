﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using System.Xml;
using Riskmaster.Common;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
    class PurgeHistorySetUpAdaptor : BusinessAdaptorBase
    {
        # region public methods
        public bool GetPurgeEntries(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            PurgeHistorySetUp objPHS = null;
            bool bReturnValue = false;
            try
            {
                objPHS = new PurgeHistorySetUp(userLogin,connectionString, m_loginName, base.ClientId);//psharma206 jira 103
                bReturnValue = objPHS.GetPurgeEntries(p_objXmlIn, ref p_objXmlOut);
                return bReturnValue;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("PurgeHistorysetUpAdaptor.GetPurgeEntries.Error", base.ClientId), BusinessAdaptorErrorType.Error);    //psharma206 jira  103                               
                return false;
            }
            finally
            {
                objPHS = null;
            }
        }

        public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            PurgeHistorySetUp objPHS=null ;
            bool bReturnValue = false;
            try
            {
                objPHS = new PurgeHistorySetUp(connectionString, m_loginName, base.ClientId);
                bReturnValue = objPHS.Save(p_objXmlIn, ref p_objXmlOut);
                return bReturnValue;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("PurgeHistorysetUpAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);     //psharma206 jira    103                           
                return false;
            }
            finally
            {
                objPHS = null;
            }

        }

        public bool Delete(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            PurgeHistorySetUp objPHS = null;
            bool bReturnValue = false;
            try
            {
                objPHS = new PurgeHistorySetUp(connectionString, m_loginName, base.ClientId);
                bReturnValue = objPHS.Delete(p_objXmlIn, ref p_objXmlOut);
                return bReturnValue;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("PurgeHistorysetUpAdaptor.Delete.Error", base.ClientId), BusinessAdaptorErrorType.Error);      //psharma206 jira    103                           
                return false;
            }
            finally
            {
                objPHS = null;
            }
        }

        public bool GetTableList(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            PurgeHistorySetUp objPHS = null;
            bool bReturnValue = false;
            try
            {
                objPHS = new PurgeHistorySetUp(connectionString, m_loginName, base.ClientId);
                bReturnValue = objPHS.GetTableList(ref p_objXmlOut);
                return bReturnValue;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("PurgeHistorysetUpAdaptor.GetTableList.Error", base.ClientId), BusinessAdaptorErrorType.Error);  //psharma206 jira    103                               
                return false;
            }
            finally
            {
                objPHS = null;
            }
        }
        # endregion
    }
}
