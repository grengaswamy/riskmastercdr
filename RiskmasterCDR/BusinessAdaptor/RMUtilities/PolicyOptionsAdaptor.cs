using System;
using System.Text;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: PolicyOptionsAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 25-May-2005
	///* $Author	: Anurag Agarwal
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	
	/// <summary>	
	///	This class is used to call the application layer component for PolicyOptions class  
	/// </summary>
	public class PolicyOptionsAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		public PolicyOptionsAdaptor()
		{
		}
		#endregion

		#region Public Function
		/// <summary>
		/// This function xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///		Output structure is as follows-:
				#region "Output XML"
		///		<Document>
		///			<DiscountsSetUpList>
		///				<option>
		///					<discountname>AnuragTest</discountname>
		///					<lob>GL</lob>
		///					<state>AC</state>
		///					<discounttype>F</discounttype>
		///					<amount>$34.00</amount>
		///					<effectivedate>3/5/2005 12:00:00 AM</effectivedate>
		///					<expirationdate>3/8/2005 12:00:00 AM</expirationdate>
		///					<discountrowid>1</discountrowid>
		///					<tag>AnuragTest,5188,73,34,5237,3/5/2005 12:00:00 AM,3/8/2005 12:00:00 AM,1</tag>
		///				</option>
		///				<option>
		///					<discountname>asa</discountname>
		///					<lob>GL</lob>
		///					<state>AC</state>
		///					<discounttype>F</discounttype>
		///					<amount>$0.00</amount>
		///					<effectivedate>3/16/2005 12:00:00 AM</effectivedate>
		///					<expirationdate>1/1/0001 12:00:00 AM</expirationdate>
		///					<discountrowid>2</discountrowid>
		///					<tag>asa,5188,73,0,5237,3/16/2005 12:00:00 AM,1/1/0001 12:00:00 AM,2</tag>
		///				</option>
		///			</DiscountsSetUpList>
		///			<RoundAmounts>0</RoundAmounts>
		///			<UseYear>1</UseYear>
		///			<Placement enabled="1">5239</Placement>
		///			<ExpConstList>
		///				<option>
		///					<use>Y</use>
		///					<lob>GL</lob>
		///					<state>AC</state>
		///					<expcontype>F</expcontype>
		///					<amount>$23.00</amount>
		///					<effectivedate>4/6/2005 12:00:00 AM</effectivedate>
		///					<expirationdate>4/18/2005 12:00:00 AM</expirationdate>
		///					<expconrowid>1</expconrowid>
		///					<tag>True,5188,73,23,5237,4/6/2005 12:00:00 AM,4/18/2005 12:00:00 AM,1</tag>
		///				</option>
		///			</ExpConstList>
		///			<TierList>
		///				<option>
		///					<use>Y</use>
		///					<tiername>Modified Premium</tiername>
		///					<lob>\" \"</lob>
		///					<state></state>
		///					<effectivedate>1/1/0001 12:00:00 AM</effectivedate>
		///					<expirationdate>1/1/0001 12:00:00 AM</expirationdate>
		///					<applicablelevel></applicablelevel>
		///					<disctierrowid>1</disctierrowid>
		///					<tag>True,0,0,1/1/0001 12:00:00 AM,1/1/0001 12:00:00 AM,,1</tag>
		///				</option>
		///				<option>
		///					<use>Y</use>
		///					<tiername>ffsd</tiername>
		///					<lob>WC</lob>
		///					<state>AR</state>
		///					<effectivedate>3/16/2005 12:00:00 AM</effectivedate>
		///					<expirationdate>4/20/2005 12:00:00 AM</expirationdate>
		///					<applicablelevel>Modified Premium</applicablelevel>
		///					<disctierrowid>5</disctierrowid>
		///					<tag>True,5187,5,3/16/2005 12:00:00 AM,4/20/2005 12:00:00 AM,Modified Premium,5</tag>
		///				</option>
		///				<option>
		///					<use>N</use>
		///					<tiername>zxc</tiername>
		///					<lob>GL</lob>
		///					<state>AZ</state>
		///					<effectivedate>4/22/2005 12:00:00 AM</effectivedate>
		///					<expirationdate>4/28/2005 12:00:00 AM</expirationdate>
		///					<applicablelevel>Modified Premium</applicablelevel>
		///					<disctierrowid>6</disctierrowid>
		///					<tag>False,5188,4,4/22/2005 12:00:00 AM,4/28/2005 12:00:00 AM,Modified Premium,6</tag>
		///				</option>
		///			</TierList>
		///			<ThresholdsList>
		///				<option>
		///					<use>Y</use>
		///					<lob>WC</lob>
		///					<state>AL</state>
		///					<amount>$23.00</amount>
		///					<effectivedate>3/2/2005 12:00:00 AM</effectivedate>
		///					<expirationdate>3/3/2005 12:00:00 AM</expirationdate>
		///					<expconrowid>1</expconrowid>
		///					<tag>True,5187,1,23,3/2/2005 12:00:00 AM,3/3/2005 12:00:00 AM,1</tag>
		///				</option>
		///			</ThresholdsList>
        ///			<PolicyTaxList>
        ///				<option>
        ///					<use>Y</use>
        ///					<lob>GL</lob>
        ///					<state>AC</state>
        ///					<taxtype>F</taxtype>
        ///					<amount>$23.00</amount>
        ///					<effectivedate>4/6/2005 12:00:00 AM</effectivedate>
        ///					<expirationdate>4/18/2005 12:00:00 AM</expirationdate>
        ///					<PolicyTaxrowid>1</PolicyTaxrowid>
        ///					<tag>True,5188,73,23,5237,4/6/2005 12:00:00 AM,4/18/2005 12:00:00 AM,1</tag>
        ///				</option>
        ///			</PolicyTaxList>
		///			<RenewalInfoList>
		///				<option>
		///					<lob>GL</lob>
		///					<state>AK</state>
		///					<termlength>$1.00</termlength>
		///					<termcode>D</termcode>
		///					<renewalinforowid>1</renewalinforowid>
		///					<tag>5188,2,1,5220,1</tag>
		///				</option>
		///			</RenewalInfoList>
		///			<DiscountsList>
		///				<option>
		///					<use></use>
		///					<discountname></discountname>
		///					<lob></lob>
		///					<state></state>
		///					<applicablelevel></applicablelevel>
		///					<useredit></useredit>
		///					<seldiscrowid></seldiscrowid>
		///					<tag></tag>
		///				</option>
		///			</DiscountsList>
		///			<ExpRatesList>
		///				<option>
		///					<exposurecode></exposurecode>
		///					<exposuredescription></exposuredescription>
		///					<state></state>
		///					<flatorpercent></flatorpercent>
		///					<amount></amount>
		///					<fixedorprorate></fixedorprorate>
		///					<baserate></baserate>
		///					<effectivedate></effectivedate>
		///					<expirationdate></expirationdate>
		///					<expraterowid></expraterowid>
		///					<tag></tag>
		///				</option>
		///			</ExpRatesList>
		///		</Document>
		/// </param>
		#endregion
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Get(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			PolicyOptions objPolicyOptions=null;
            bool blnSuccess = false;
            int iCurrentPage = 0;
			try
			{
                objPolicyOptions = new PolicyOptions(loginName, connectionString, base.ClientId);
                #region rsushilaggar - Added pagination on the Policy setup grid MITS 22371
                //Renewal Info Setup
                iCurrentPage = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("/Document/RenewalInfoList/CurrentPage").InnerText, out blnSuccess);
                if (iCurrentPage == 0)
                    iCurrentPage = 1;
                objPolicyOptions.TermCurrentPage = iCurrentPage;
                objPolicyOptions.TermPageSize = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("/Document/RenewalInfoList/hdnPageSize").InnerText, out blnSuccess);

                //Expense Constant Setup
                iCurrentPage = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("/Document/ExpConstList/CurrentPage").InnerText, out blnSuccess);
                if (iCurrentPage == 0)
                    iCurrentPage = 1;
                objPolicyOptions.ExpConstCurrentPage = iCurrentPage;
                objPolicyOptions.ExpConstPageSize = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("/Document/ExpConstList/hdnPageSize").InnerText, out blnSuccess);

                //Tax Setup
                iCurrentPage = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("/Document/PolicyTaxList/CurrentPage").InnerText, out blnSuccess);
                if (iCurrentPage == 0)
                    iCurrentPage = 1;
                objPolicyOptions.PolTaxCurrentPage = iCurrentPage;
                objPolicyOptions.PolTaxPageSize = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("/Document/PolicyTaxList/hdnPageSize").InnerText, out blnSuccess);

                //Min Bill Threshhold
                iCurrentPage = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("/Document/ThresholdsList/CurrentPage").InnerText, out blnSuccess);
                if (iCurrentPage == 0)
                    iCurrentPage = 1;
                objPolicyOptions.MinBillCurrentPage = iCurrentPage;
                objPolicyOptions.MinBillPageSize = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("/Document/ThresholdsList/hdnPageSize").InnerText, out blnSuccess);

                //Discounts Setup
                iCurrentPage = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("/Document/DiscountsList/CurrentPage").InnerText, out blnSuccess);
                if (iCurrentPage == 0)
                    iCurrentPage = 1;
                objPolicyOptions.DiscountCurrentPage = iCurrentPage;
                objPolicyOptions.DiscountPageSize = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("/Document/DiscountsList/hdnPageSize").InnerText, out blnSuccess);

                //Discount Tier Setup
                iCurrentPage = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("/Document/DiscountsSetUpList/CurrentPage").InnerText, out blnSuccess);
                if (iCurrentPage == 0)
                    iCurrentPage = 1;
                objPolicyOptions.DisTierCurrentPage = iCurrentPage;
                objPolicyOptions.DisTierPageSize = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("/Document/DiscountsSetUpList/hdnPageSize").InnerText, out blnSuccess);

                //TierList  setup
                iCurrentPage = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("/Document/TierList/CurrentPage").InnerText, out blnSuccess);
                if (iCurrentPage == 0)
                    iCurrentPage = 1;
                objPolicyOptions.TierCurrentPage= iCurrentPage;
                objPolicyOptions.TierPageSize = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("/Document/TierList/hdnPageSize").InnerText, out blnSuccess);

                //Rate Setup
                iCurrentPage = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("/Document/RateSetUpList/CurrentPage").InnerText, out blnSuccess);
                if (iCurrentPage == 0)
                    iCurrentPage = 1;
                objPolicyOptions.RateCurrentPage = iCurrentPage;
                objPolicyOptions.RatePageSize = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("/Document/RateSetUpList/hdnPageSize").InnerText, out blnSuccess);

                //Exposure Rates Setup
                iCurrentPage = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("/Document/ExpRatesList/CurrentPage").InnerText, out blnSuccess);
                if (iCurrentPage == 0)
                    iCurrentPage = 1;
                objPolicyOptions.ExpRatesCurrentPage = iCurrentPage;
                objPolicyOptions.ExpRatesPageSize = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("/Document/ExpRatesList/hdnPageSize").InnerText, out blnSuccess);

                //Coverages Setup
                iCurrentPage = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("/Document/CovRatesList/CurrentPage").InnerText, out blnSuccess);
                if (iCurrentPage == 0)
                    iCurrentPage = 1;
                objPolicyOptions.CoverageCurrentPage = iCurrentPage;
                objPolicyOptions.CoveragePageSize = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("/Document/CovRatesList/hdnPageSize").InnerText, out blnSuccess);

                //UAR Setup
                iCurrentPage = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("/Document/UARRatesList/CurrentPage").InnerText, out blnSuccess);
                if (iCurrentPage == 0)
                    iCurrentPage = 1;
                objPolicyOptions.UARCurrentPage = iCurrentPage;
                objPolicyOptions.UARPageSize = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("/Document/UARRatesList/hdnPageSize").InnerText, out blnSuccess);
                #endregion

                p_objXmlOut =objPolicyOptions.LoadData();
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("PolicyOptionsAdaptor.Get.Error", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objPolicyOptions=null;
			}
		}

		/// <summary>
		/// This function xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">
		///		Input parameters as xml
		///		<Document>
		///			<PolOptRowId></PolOptRowId>
		///			<RoundAmounts>0</RoundAmounts>
		///			<UseYear>1</UseYear>
		///			<Placement>5239</Placement>
		///		</Document>
		///	</param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Save(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			XmlElement objElement=null;					//used for parsing the input xml
			PolicyOptions objPolicyOptions=null;	

			try
			{
				//check existence of input xml which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//Document");
				if (objElement==null)
				{
					p_objErrOut.Add("PolicyOptionsAdaptor.Get.Error",Globalization.GetString("PolicyOptionsAdaptor.InputXMLMissing", base.ClientId),BusinessAdaptorErrorType.Warning);
					return false;
				}

				objPolicyOptions=new PolicyOptions(loginName,connectionString, base.ClientId);			
				objPolicyOptions.Save(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("PolicyOptionsAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objPolicyOptions=null;
			}
		}

		/// <summary>
		/// This function xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">
		///		Input parameters as xml
		///		<Document>
		///			<DeletionListName></DeletionListName>
		///			<DeletionId></DeletionId>
		///		</Document>
		///	</param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Delete(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			XmlElement objElement=null;					//used for parsing the input xml
			PolicyOptions objPolicyOptions=null;	

			try
			{
				//check existence of input xml which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//Document");
				if (objElement==null)
				{
					p_objErrOut.Add("PolicyOptionsAdaptor.Get.Error",Globalization.GetString("PolicyOptionsAdaptor.InputXMLMissing", base.ClientId),BusinessAdaptorErrorType.Warning);
					return false;
				}

                objPolicyOptions = new PolicyOptions(loginName, connectionString, base.ClientId);			
				objPolicyOptions.Delete(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("PolicyOptionsAdaptor.Delete.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objPolicyOptions=null;
			}
		}

		#endregion
	}
}
