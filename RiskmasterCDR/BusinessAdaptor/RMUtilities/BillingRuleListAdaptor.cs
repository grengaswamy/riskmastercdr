using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;  

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: BillingRuleListAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 24/05/2005
	///* $Author	: Pankaj
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for RM Utilities
	///	which implements the functionality of BillingRuleList.
	/// </summary>
	public class BillingRuleListAdaptor :  BusinessAdaptorBase
	{
		/// <summary>
		/// Default Constructor
		/// </summary>
		public BillingRuleListAdaptor(){}

		#region Public Methods

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.BillingRuleList.Get() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in Riskmaster.Application.RMUtilities.BillingRuleList.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			BillingRuleList objBillList=null; //Application layer component			
			try
			{
                objBillList = new BillingRuleList(connectionString, base.ClientId);
				//get the data
				p_objXmlOut=objBillList.Get();
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("BillingRuleList.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objBillList = null;
			}
		}
		#endregion
	}
}