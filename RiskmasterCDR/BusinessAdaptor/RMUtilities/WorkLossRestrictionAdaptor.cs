using System;
using System.Text;
using System.Xml;
using System.IO;
using System.Security;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
    ///************************************************************** 
    ///* $File		: WorkLossRestrictionAdaptor.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 03-Aug-2009
    ///* $Author	: Raman
    ///* $Comment	: 
    ///* $Source	: 
    ///**************************************************************

    /// <summary>	
    ///	This class is used to call the application layer component for WorkLossRestriction class  
    /// </summary>
    public class WorkLossRestrictionAdaptor : BusinessAdaptorBase
    {
        public WorkLossRestrictionAdaptor()
        {
        }
        /// <summary>
        /// This function xml containing data to be shown on screen
        /// </summary>
        /// <param name="p_objXmlIn">Input parameters as xml</param>
        /// <param name="p_objXmlOut">Result as Output xml
        ///	Input structure is as follows-:
        ///	<WorkLossRestriction><RowId></RowId></WorkLossRestriction>
        /// </param>
        /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
        /// <returns>Success -True or Failure -false in execution of the function</returns>
        public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            WorkLossRestriction objOption = null;
            try
            {
                objOption = new WorkLossRestriction(connectionString, base.ClientId);
                p_objXmlOut = objOption.Get(p_objXmlIn);
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("WorkLossRestrictionAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objOption = null;
            }
        }
        /// <summary>
        /// Deletes the WorkLossRestriction information
        /// </summary>
        /// <param name="p_objXmlIn">Input parameters as xml</param>
        /// <param name="p_objXmlOut">Result as Output xml
        ///	Output structure is as follows-:
        ///	See Xml file named WorkLossRestriction.xml in Xml folder
        /// </param>
        /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
        /// <returns>Success -True or Failure -false in execution of the function</returns>
        public bool Delete(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            WorkLossRestriction objOption = null;
            try
            {
                objOption = new WorkLossRestriction(connectionString, base.ClientId);
                objOption.Delete(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("WorkLossRestrictionAdaptor.Delete.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objOption = null;
            }
        }

        /// <summary>
        /// Saves the WorkLossRestriction
        /// </summary>
        /// <param name="p_objXmlIn">Input parameters as xml</param>
        /// <param name="p_objXmlOut">Result as Output xml
        ///	Input structure is as follows-:
        ///	See Xml file named WorkLossRestriction.xml in Xml folder
        /// </param>
        /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
        /// <returns>Success -True or Failure -false in execution of the function</returns>
        public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            WorkLossRestriction objOption = null;
            try
            {
                objOption = new WorkLossRestriction(connectionString, base.ClientId);
                objOption.Save(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("WorkLossRestrictionAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objOption = null;
            }
        }
    }
}

