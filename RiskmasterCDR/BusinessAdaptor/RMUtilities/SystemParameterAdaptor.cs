using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;  

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: SystemParameterAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 30/05/2005
	///* $Author	: Pankaj
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for RM Utilities
	///	which implements the functionality of System Parameter Setup data.
	/// </summary>
	public class SystemParameterAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		/// <summary>
		/// Default Constructor
		/// </summary>
		public SystemParameterAdaptor(){}

		#endregion

		#region Public Methods

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.SystemParameter.Get() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.SystemParameter.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			SystemParameter objSys=null; //Application layer component			
			try
			{
                //Ashish Ahuja
				//objSys = new SystemParameter(connectionString);
                objSys = new SystemParameter(connectionString, userLogin.UserId, base.ClientId);
				p_objXmlOut=objSys.Get(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException, Globalization.GetString("SystemParameterAdaptor.Get.Error", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
				return false;
			}
			finally
			{
				objSys=null;
			}
		}

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.SystemParameter.Save() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.SystemParameter.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
            SystemParameter objSys = null; //Application layer component		
			try
			{
                //gagnihotri MITS 11995 Changes made for Audit table
				//objSys = new SystemParameter(connectionString);
                objSys = new SystemParameter(connectionString,userLogin.LoginName, base.ClientId);//rkaur27
				p_objXmlOut=objSys.Save(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException, Globalization.GetString("SystemParameterAdaptor.Save.Error", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
				return false;
			}
			finally
			{
				objSys=null;
			}
		}


		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.SystemParameter.UseCaseMgt() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.SystemParameter.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool UseCaseMgt(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			SystemParameter objSys=null; //Application layer component			
			try
			{
                objSys = new SystemParameter(connectionString, base.ClientId);
				p_objXmlOut=objSys.UseCaseMgt(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException, Globalization.GetString("SystemParameterAdaptor.UseCaseMgt.Error", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
				return false;
			}
			finally
			{
				objSys=null;
			}
		}

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.SystemParameter.UseScriptEditor() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.SystemParameter.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool UseScriptEditor(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			SystemParameter objSys=null; //Application layer component			
			try
			{
                objSys = new SystemParameter(connectionString, base.ClientId);
				p_objXmlOut=objSys.UseScriptEditor(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException, Globalization.GetString("SystemParameterAdaptor.UseScriptEditor.Error", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
				return false;
			}
			finally
			{
				objSys=null;
			}
		}

        /// <summary>
        ///		This method is a wrapper to Riskmaster.Application.RMUtilities.SystemParameter.UseCarrierClaims() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.SystemParameter.xml
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool UseCarrierClaims(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            SystemParameter objSys = null; //Application layer component			
            try
            {
                objSys = new SystemParameter(connectionString, base.ClientId);
                p_objXmlOut = objSys.UseCarrierClaims(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("SystemParameterAdaptor.UseCarrierClaims.Error", base.ClientId), BusinessAdaptorErrorType.Error);//rkaur27
                return false;
            }
            finally
            {
                objSys = null;
            }
        }

        /// <summary>
        /// Author: Sumit Kumar
        /// Date : 02/07/2010
        /// Description: Method to activate BRS
        ///	This method is a wrapper to Riskmaster.Application.RMUtilities.SystemParameter.UseBRS() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///	The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.SystemParameter.xml
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool UseBRS(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            SystemParameter objSys = null; //Application layer component			
            try
            {
                objSys = new SystemParameter(connectionString, base.ClientId);
                p_objXmlOut = objSys.UseBRS(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("SystemParameterAdaptor.UseBRS.Error", base.ClientId), BusinessAdaptorErrorType.Error);//rkaur27
                return false;
            }
            finally
            {
                objSys = null;
            }
        }

        /// <summary>
        /// Author: Sumit Kumar
        /// Date : 02/07/2010
        /// Description: Method to activate Lines of Business (GC,DI,PC,VA and WC)
        ///	This method is a wrapper to Riskmaster.Application.RMUtilities.SystemParameter.UseLOB() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///	The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.SystemParameter.xml
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool UseLOB(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            SystemParameter objSys = null;
            XmlNode objNode = null;
            string sLOB = string.Empty;
            try
            {
                objSys = new SystemParameter(connectionString, base.ClientId);
                objNode = p_objXmlIn.SelectSingleNode("//control[@name='LOB']");
                if (objNode != null)
                    sLOB = objNode.InnerText;
                
                p_objXmlOut = objSys.UseLOB(p_objXmlIn,sLOB);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("SystemParameterAdaptor.Use" + sLOB +"LOB.Error", base.ClientId), BusinessAdaptorErrorType.Error);//rkaur27
                return false;
            }
            finally
            {
                objSys = null;
            }
        }

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.SystemParameter.UseEnhPol() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.SystemParameter.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool UseEnhPol(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			SystemParameter objSys=null; //Application layer component			
			try
			{
                objSys = new SystemParameter(connectionString, base.ClientId);
				p_objXmlOut=objSys.UseEnhPol(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException, Globalization.GetString("SystemParameterAdaptor.UseEnhPol.Error", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
				return false;
			}
			finally
			{
				objSys=null;
			}
		}

        /// <summary>
        ///		This method is a wrapper to Riskmaster.Application.RMUtilities.SystemParameter.UsePolicyBilling() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.SystemParameter.xml
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool UsePolicyBilling(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            SystemParameter objSys = null; //Application layer component			
            try
            {
                objSys = new SystemParameter(connectionString, base.ClientId);
                p_objXmlOut = objSys.UsePolicyBilling(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("SystemParameterAdaptor.UsePolicyBilling.Error", base.ClientId), BusinessAdaptorErrorType.Error);//rkaur27
                return false;
            }
            finally
            {
                objSys = null;
            }
        }

//		/// <summary>
//		///		This method is a wrapper to Riskmaster.Application.RMUtilities.SystemParameter.SubBankAcct() method.		
//		/// </summary>
//		/// <param name="p_objXmlIn">Input XML document
//		/// </param>
//		/// <param name="p_objXmlOut">XML containing the results
//		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.SystemParameter.xml
//		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
//		/// <returns>True/False for success or failure of the function</returns>
//		public bool SubBankAcct(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
//		{
//			SystemParameter objSys=null; //Application layer component			
//			try
//			{
//				objSys = new SystemParameter(connectionString);
//				p_objXmlOut=objSys.LoadTandEBankAcc(p_objXmlIn);
//				return true;
//			}
//			catch(RMAppException p_objException)
//			{
//				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
//				return false;
//			}
//			catch(Exception p_objException)
//			{
//				p_objErrOut.Add(p_objException, Globalization.GetString("SystemParameterAdaptor.SubBankAcct.Error"),BusinessAdaptorErrorType.Error);
//				return false;
//			}
//			finally
//			{
//				objSys=null;
//			}
//		}


		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.SystemParameter.UseEnhPol() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.SystemParameter.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
//		public bool FinHistChange(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
//		{
//			SystemParameter objSys=null; //Application layer component			
//			try
//			{
//				objSys = new SystemParameter(connectionString);
//				p_objXmlOut=objSys.FinHistChange(p_objXmlIn);
//				return true;
//			}
//			catch(RMAppException p_objException)
//			{
//				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
//				return false;
//			}
//			catch(Exception p_objException)
//			{
//				p_objErrOut.Add(p_objException, Globalization.GetString("SystemParameterAdaptor.FinHistChange.Error"),BusinessAdaptorErrorType.Error);
//				return false;
//			}
//			finally
//			{
//				objSys=null;
//			}
//		}
		//Raman Bhatia --- ACROSOFT INTEGRATION -- RMX PHASE 2
		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.SystemParameter.UseAcrosoftInterface() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.SystemParameter.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool UseAcrosoftInterface(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			SystemParameter objSys=null; //Application layer component			
			try
			{
                objSys = new SystemParameter(connectionString, base.ClientId);
				p_objXmlOut=objSys.UseAcrosoftInterface(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException, Globalization.GetString("SystemParameterAdaptor.UseAcrosoftInterface.Error", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
				return false;
			}
			finally
			{
				objSys=null;
			}
		}

        ///Raman Bhatia:R8: Use Media View Setting : July 05 2011
        /// <summary>
        ///		This method is a wrapper to Riskmaster.Application.RMUtilities.SystemParameter.UseMediaViewInterface() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.SystemParameter.xml
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
		
		 public bool UsePSOReportingSys(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            SystemParameter objSys = null; //Application layer component			
            try
            {
                objSys = new SystemParameter(connectionString, base.ClientId);
                p_objXmlOut = objSys.UsePSOReportingSys(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("SystemParameterAdaptor.UsePSOReportingSys.Error", base.ClientId), BusinessAdaptorErrorType.Error);//rkaur27
                return false;
            }
            finally
            {
                objSys = null;
            }
        }
		
        public bool UseMediaViewInterface(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            SystemParameter objSys = null; //Application layer component			
            try
            {
                objSys = new SystemParameter(connectionString, base.ClientId);
                p_objXmlOut = objSys.UseMediaViewInterface(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("SystemParameterAdaptor.UseMediaViewInterface.Error", base.ClientId), BusinessAdaptorErrorType.Error);//rkaur27
                return false;
            }
            finally
            {
                objSys = null;
            }
        }

        //***************************************************
        //Mona:PaperVisionMerge:Animesh Inserted For PaperVision Enhancement
        //***************************************************
        public bool UsePaperVisionInterface(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            SystemParameter objSys = null; //Application layer component			
            try
            {
                objSys = new SystemParameter(connectionString, base.ClientId);
                p_objXmlOut = objSys.UsePaperVisionInterface(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("SystemParameterAdaptor.UsePaperVisionInterface.Error", base.ClientId), BusinessAdaptorErrorType.Error);//rkaur27
                return false;
            }
            finally
            {
                objSys = null;
            }
        }
        //***************************************************
		#endregion
	}
}