/**********************************************************************************************
*   Date     |  MITS   | Programmer | Description                                            *
**********************************************************************************************
* 06/04/2014 | 33371   | ajohari2   | Created new adaptor class for TPA changes
**********************************************************************************************/

using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
    //MITS 33371 ajohari2
    ///************************************************************** 
    ///* $File		: PolicySearchDataMappingAdaptor.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 7th,May 2014
    ///* $Author	: Ankit Johari
    ///* $Comment	: 
    ///* $Source	: 
    ///**************************************************************
    /// <summary>	
    ///	This class is used to call the application layer component for RM Utilities
    ///	which implements the functionality of Policy Search Data Mapping List.
    /// </summary>
    public class PolicySearchDataMappingAdaptor : BusinessAdaptorBase
    {
        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        public PolicySearchDataMappingAdaptor() { }

        #endregion

        #region Public Methods

        /// <summary>
        ///		This method is a wrapper to Riskmaster.Application.RMUtilities.PolicySearchDataMappingAdaptor.Get() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.PolicySearchDataMapping.xml
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            PolicySearchDataMapping objPSDMLst = null; //Application layer component			
            try
            {
                objPSDMLst = new PolicySearchDataMapping(connectionString, base.ClientId);
                p_objXmlOut = objPSDMLst.Get(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("PolicySearchDataMappingAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objPSDMLst = null;
            }
        }

        #endregion
    }
}