using System;
using System.Text;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common; 

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
    ///* $File		: DCIBenefitCodeMappingAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 30-August-2014
	///* $Author	: Neha Goel/Ankit Gupta
    ///* $Comment	: MITS# 36997 GAP 06 - RMA-5497
	///* $Source	: 
	///**************************************************************
	
	/// <summary>	
	///	This class is used to call the application layer component for DCI Mapping class
	/// </summary>
    public class DCIMappingAdaptor : BusinessAdaptorBase
	{
		#region Constructor
        public DCIMappingAdaptor() { }
		#endregion

		#region Public Function
		/// <summary>
		/// This function xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Get(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
            DCIBenefitCodeMapping objTransMapping = null;
            DCIFinancialFieldsMapping objFieldsMapping = null;
			try
			{
                objTransMapping = new DCIBenefitCodeMapping(userLogin.LoginName, connectionString, base.ClientId);			
				p_objXmlOut=objTransMapping.Get(p_objXmlIn,base.userLogin);
                //Financial
                objFieldsMapping = new DCIFinancialFieldsMapping(userLogin.LoginName, connectionString, base.ClientId);
                p_objXmlOut = objFieldsMapping.Get(p_objXmlOut, base.userLogin);//Aman ML Change

				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("DCIMappingAdaptor.Get.Error",base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objTransMapping=null;
			}
		}
		/// <summary>
		/// This function Saves acord and claim type setup data
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool DeleteBenefitsMapping(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
            DCIBenefitCodeMapping objTransMapping = null;				
			try
			{
                objTransMapping = new DCIBenefitCodeMapping(userLogin.LoginName, connectionString, base.ClientId);			
				objTransMapping.DeleteBenefitsMapping(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("DCIMappingAdaptor.DeleteBenefitsMapping.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objTransMapping=null;
			}
		}
		/// <summary>
		/// This function benefits mapping data
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool SaveBenefits(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
            DCIBenefitCodeMapping objTransMapping = null;				
			try
			{
                objTransMapping = new DCIBenefitCodeMapping(userLogin.LoginName, connectionString, base.ClientId);			
				objTransMapping.SaveBenefits(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("DCIMappingAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objTransMapping=null;
			}
		}

        /// <summary>
        /// Gets the benefit amount.
        /// </summary>
        /// <param name="p_objXmlIn">The p_obj XML in.</param>
        /// <param name="p_objXmlOut">The p_obj XML out.</param>
        /// <param name="p_objErrOut">The p_obj error out.</param>
        /// <returns></returns>
        public bool GetBenefitAmount(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
            DCIBenefitCodeMapping objTransMapping = null;	
			try
			{
                objTransMapping = new DCIBenefitCodeMapping(userLogin.LoginName, connectionString, base.ClientId);
                p_objXmlOut = objTransMapping.GetBenefitAmount(p_objXmlIn, base.userLogin);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("DCIMappingAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objTransMapping=null;
			}
		}

        /// <summary>
        /// This function xml containing data to be shown on screen
        /// </summary>
        /// <param name="p_objXmlIn">Input parameters as xml</param>
        /// <param name="p_objXmlOut">Result as Output xml
        ///	Output structure is as follows-:
        ///	See Xml file named DCIFinancialFieldsMapping.xml in Xml folder
        /// </param>
        /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
        /// <returns>Success -True or Failure -false in execution of the function</returns>
        public bool GetDCIFieldsTransMapping(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            DCIFinancialFieldsMapping objFieldsMapping = null;
            try
            {
                objFieldsMapping = new DCIFinancialFieldsMapping(userLogin.LoginName, connectionString, base.ClientId);
                p_objXmlOut = objFieldsMapping.Get(p_objXmlIn, base.userLogin);//Aman ML Change
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("DCIMappingAdaptor.GetDCIFieldsTransMapping.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objFieldsMapping = null;
            }
        }


        /// <summary>
        /// This function Saves acord and claim type setup data
        /// </summary>
        /// <param name="p_objXmlIn">Input parameters as xml</param>
        /// <param name="p_objXmlOut">Result as Output xml
        ///	Input structure is as follows-:
        ///	See Xml file named DCIFinancialFieldsMapping.xml in Xml folder
        /// </param>
        /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
        /// <returns>Success -True or Failure -false in execution of the function</returns>
        public bool DeleteDCIFieldsTransMapping(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            DCIFinancialFieldsMapping objFieldsMapping = null;
            try
            {

                objFieldsMapping = new DCIFinancialFieldsMapping(userLogin.LoginName, connectionString, base.ClientId);
                objFieldsMapping.Delete(p_objXmlIn);
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("DCIMappingAdaptor.DeleteDCIFieldsTransMapping.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objFieldsMapping = null;
            }
        }


        /// <summary>
        /// This function benefits mapping data
        /// </summary>
        /// <param name="p_objXmlIn">Input parameters as xml</param>
        /// <param name="p_objXmlOut">Result as Output xml
        ///	Input structure is as follows-:
        ///	See Xml file named DCIFinancialFieldsMapping.xml in Xml folder
        /// </param>
        /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
        /// <returns>Success -True or Failure -false in execution of the function</returns>
        public bool SaveDCIFieldsTransMapping(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            DCIFinancialFieldsMapping objFieldsMapping = null;
            try
            {

                objFieldsMapping = new DCIFinancialFieldsMapping(userLogin.LoginName, connectionString, base.ClientId);
                objFieldsMapping.Save(p_objXmlIn);
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("DCIMappingAdaptor.SaveDCIFieldsTransMapping.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objFieldsMapping = null;
            }
        }



		#endregion
	}
}
