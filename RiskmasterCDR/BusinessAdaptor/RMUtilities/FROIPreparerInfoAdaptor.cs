using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;  

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	/// <summary>
	/// Summary description for FROIPreparerInfoAdaptor.
	/// </summary>
	public class FROIPreparerInfoAdaptor:BusinessAdaptorBase
	{
		public FROIPreparerInfoAdaptor()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			FROIPreparerInfo objFROIPreparerInfo=null; //Application layer component			
			try
			{
                objFROIPreparerInfo = new FROIPreparerInfo(userLogin.UserId.ToString(), connectionString, base.ClientId);
				p_objXmlOut=objFROIPreparerInfo.Get(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("FROIPreparerInfoAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objFROIPreparerInfo=null;
			}
		}

		public bool Save(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			FROIPreparerInfo objFROIPreparerInfo = null;			
			try
			{
                objFROIPreparerInfo = new FROIPreparerInfo(userLogin.UserId.ToString(), connectionString, base.ClientId);
				objFROIPreparerInfo.Save(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("FROIPreparerInfoAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objFROIPreparerInfo=null;
			}
		}

	}
}
