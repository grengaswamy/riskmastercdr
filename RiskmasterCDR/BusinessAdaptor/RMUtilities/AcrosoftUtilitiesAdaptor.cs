using System;
using System.IO;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;  
using Riskmaster.Settings;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: AcrosoftUtilitiesAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 08/15/2006
	///* $Author	: Raman
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for RM Utilities
	///	which implements the Acrosoft Pre-Fill Folder Strucure
	/// </summary>
	public class AcrosoftUtilitiesAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		/// <summary>
		/// Default Constructor
		/// </summary>
		public AcrosoftUtilitiesAdaptor(){}

		#endregion

		#region Public Methods
		
		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.AcrosoftUtilities() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.ActivityAdd.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool PreFillFolders(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			AcrosoftUtil objAcrosoftUtil = null; //Application layer component			
			MemoryStream objMemory = null;
			XmlNode objNode=null;
			XmlElement objTempElement=null;
			
			try
			{
                // Ash - Cloud
                //objAcrosoftUtil = new AcrosoftUtil(userLogin);
                objAcrosoftUtil = new AcrosoftUtil(userLogin, base.ClientId);
				objMemory=objAcrosoftUtil.PreFillFolders();
				objNode=p_objXmlOut.CreateElement("AcrosoftUploadReport");
				p_objXmlOut.AppendChild(objNode);
				objTempElement=p_objXmlOut.CreateElement("Report");
				objTempElement.InnerText=Convert.ToBase64String(objMemory.ToArray());
				p_objXmlOut.FirstChild.AppendChild(objTempElement);
				return true;
				
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("ActivityAddAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				
			}
		}
		/// <summary>
		///		This method verifies the current acrosoft status		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool VerifyAcrosoftStatus(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			SysSettings objSettings = null;
			XmlNode objNode = null;
			XmlElement objTempElement = null;
			try
			{

                objSettings = new SysSettings(connectionString, base.ClientId);
				
				objNode=p_objXmlOut.CreateElement("AcrosoftStatus");
				p_objXmlOut.AppendChild(objNode);
				objTempElement=p_objXmlOut.CreateElement("CurrentStatus");
				objTempElement.InnerText=objSettings.UseAcrosoftInterface.ToString();
				p_objXmlOut.FirstChild.AppendChild(objTempElement);
				objTempElement=p_objXmlOut.CreateElement("DocumentPathType");
				objTempElement.InnerText=userLogin.objRiskmasterDatabase.DocPathType.ToString();
				p_objXmlOut.FirstChild.AppendChild(objTempElement);
				objTempElement=p_objXmlOut.CreateElement("DocumentPath");
				objTempElement.InnerText=userLogin.objRiskmasterDatabase.GlobalDocPath;
				p_objXmlOut.FirstChild.AppendChild(objTempElement);
				
				return true;
				
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("ActivityAddAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objSettings = null;
			}
		}
		#endregion
	}
}