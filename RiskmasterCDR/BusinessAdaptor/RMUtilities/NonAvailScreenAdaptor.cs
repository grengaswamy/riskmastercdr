﻿using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
    ///************************************************************** 
    ///* $File		: AdjusterScreenAdaptor.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 2/Nov/2007
    ///* $Author	: Animesh Sahai 
    ///* $Comment	: 
    ///* $Source	: 
    ///**************************************************************
    /// <summary>	
    ///	This class is used to call the application layer component for Custom
    ///	which implements the functionality of Adjuster Screen form.
    /// </summary>
    class NonAvailScreenAdaptor : BusinessAdaptorBase
    {
        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        public NonAvailScreenAdaptor() { }

        #endregion

        #region Public Methods

        /// <summary>
        ///		This method is a wrapper to Riskmaster.Application.Custom.NonAvailScreen.New() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///		The structure of the output XML document would be as in  in Riskmaster.Application.Custom.AdjusterScreen.xml
        /// </param>
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool New(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            NonAvailScreen objNonAvailScreen = null; //Application layer component			
            try
            {
                objNonAvailScreen = new NonAvailScreen(base.userLogin.LoginName, base.userLogin.Password, base.userLogin.objRiskmasterDatabase.DataSourceName, connectionString, base.ClientId);//Add by kuladeep for Cloud. Jira-58    //Rijul : Worked for 7573 (Issue of 4634 - Epic 340)
                p_objXmlOut = objNonAvailScreen.New(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("NewError", base.ClientId), BusinessAdaptorErrorType.Error);//Add by kuladeep for Cloud. Jira-58
                return false;
            }
            finally
            {
                objNonAvailScreen = null;
            }
        }

        /// <summary>
        ///		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///	
        /// </param>
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            NonAvailScreen objNonAvailScreen = null; //Application layer component			
            try
            {
                objNonAvailScreen = new NonAvailScreen(base.userLogin.LoginName, base.userLogin.Password, base.userLogin.objRiskmasterDatabase.DataSourceName, connectionString, base.ClientId);//Add by kuladeep for Cloud. Jira-58    //Rijul : Worked for 7573 (Issue of 4634 - Epic 340)
                p_objXmlOut = objNonAvailScreen.Get(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("NAGetError", base.ClientId), BusinessAdaptorErrorType.Error);//Add by kuladeep for Cloud. Jira-58
                return false;
            }
            finally
            {
                objNonAvailScreen = null;
                //				objDOC=null;
                //				objConfig=null;
            }
        }

        /// <summary>
        ///	
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///
        /// </param>
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            NonAvailScreen objNonAvailScreen = null; //Application layer component			
            try
            {
                objNonAvailScreen = new NonAvailScreen(base.userLogin.LoginName, base.userLogin.Password, base.userLogin.objRiskmasterDatabase.DataSourceName, connectionString, base.ClientId);//Add by kuladeep for Cloud. Jira-58    //Rijul : Worked for 7573 (Issue of 4634 - Epic 340)
                p_objXmlOut = objNonAvailScreen.Save(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("NASaveError", base.ClientId), BusinessAdaptorErrorType.Error);//Add by kuladeep for Cloud. Jira-58
                return false;
            }
            finally
            {
                objNonAvailScreen = null;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///
        /// </param>
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool Delete(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            NonAvailScreen objNonAvailScreen = null; //Application layer component			
            try
            {
                objNonAvailScreen = new NonAvailScreen(base.userLogin.LoginName, base.userLogin.Password, base.userLogin.objRiskmasterDatabase.DataSourceName, connectionString, base.ClientId);//Add by kuladeep for Cloud. Jira-58 //Rijul : Worked for 7573 (Issue of 4634 - Epic 340)
                p_objXmlOut = objNonAvailScreen.Delete(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("NADeleteError", base.ClientId), BusinessAdaptorErrorType.Error);//Add by kuladeep for Cloud. Jira-58
                return false;
            }
            finally
            {
                objNonAvailScreen = null;
            }
        }

        # endregion

    }
}
