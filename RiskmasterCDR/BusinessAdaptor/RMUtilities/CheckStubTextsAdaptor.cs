using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
    ///************************************************************** 
    ///* $File		: CheckStubTextsAdaptor.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 04/07/2010
    ///* $Author	: Michael Capps
    ///* $Comment	: 
    ///* $Source	: 
    ///**************************************************************
    /// <summary>	
    ///	This class is used to call the application layer component for RM Utilities
    ///	which implements the functionality of Claim Type Change Options List.
    /// </summary>
    public class CheckStubTextsAdaptor : BusinessAdaptorBase
    {
        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        public CheckStubTextsAdaptor() { }

        #endregion

        #region Public Methods

        /// <summary>
        ///		This method is a wrapper to Riskmaster.Application.RMUtilities.CheckStubTexts.Get() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.CheckStubTexts.xml
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            CheckStubTexts objCheckStubtext = null; //Application layer component			
            try
            {
                objCheckStubtext = new CheckStubTexts(connectionString, base.ClientId);
                p_objXmlOut = objCheckStubtext.Get(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("CheckStubTextsAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objCheckStubtext = null;
            }
        }
        #endregion
    }
}