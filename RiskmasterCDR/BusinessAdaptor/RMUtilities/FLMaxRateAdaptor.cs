using System;
using System.Text;
using System.Xml;
using System.IO;
using System.Security;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
    ///************************************************************** 
    ///* $File		: FLMaxRateAdaptor.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 08/22/2007
    ///* $Author	: Shruti
    ///* $Comment	: 
    ///* $Source	: 
    ///**************************************************************

    /// <summary>	
    ///	This class is used to call the application layer component for FLMaxRate class  
    /// </summary>
    public class FLMaxRateAdaptor : BusinessAdaptorBase
    {
        #region Constructor
        public FLMaxRateAdaptor() { }
        #endregion

        #region Public Function
        /// <summary>
        /// This function xml containing data to be shown on screen
        /// </summary>
        /// <param name="p_objXmlIn">Input parameters as xml</param>
        /// <param name="p_objXmlOut">Result as Output xml
        ///	Input structure is as follows-:
        ///	<FLMaxRate><MaxRateId></MaxRateId></FLMaxRate>
        /// </param>
        /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
        /// <returns>Success -True or Failure -false in execution of the function</returns>
        public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            FLMaxRate objMaxRate = null;
            try
            {
                objMaxRate = new FLMaxRate(userLogin.LoginName, connectionString, base.ClientId);
                p_objXmlOut = objMaxRate.Get(p_objXmlIn);
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("FLMaxRateAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objMaxRate  = null;
            }
        }
        /// <summary>
        /// Deletes the Max Rate information
        /// </summary>
        /// <param name="p_objXmlIn">Input parameters as xml</param>
        /// <param name="p_objXmlOut">Result as Output xml
        /// </param>
        /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
        /// <returns>Success -True or Failure -false in execution of the function</returns>
        public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            FLMaxRate objMaxRate = null;
            XmlNode objXmlNode = null;
            try
            {
                objMaxRate = new FLMaxRate(userLogin.LoginName, connectionString, base.ClientId);
                objMaxRate.Save(p_objXmlIn);
                p_objXmlIn.SelectSingleNode("//control[@name='MaxRateId']").InnerText = objMaxRate.KeyFieldValue;
                //Shruti for MITS 9268 starts
                objXmlNode = p_objXmlIn.SelectSingleNode("//SaveMessage");
                if (objXmlNode != null)
                    objXmlNode.InnerText = "Saved";
                //Shruti for MITS 9268 ends
                p_objXmlOut = p_objXmlIn;
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("FLMaxRateAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objMaxRate = null;
            }
        }
        /// <summary>
        /// Deletes the Max Rate information
        /// </summary>
        /// <param name="p_objXmlIn">Input parameters as xml</param>
        /// <param name="p_objXmlOut">Result as Output xml
        /// </param>
        /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
        /// <returns>Success -True or Failure -false in execution of the function</returns>
        public bool Delete(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            FLMaxRate objMaxRate = null;
            try
            {
                objMaxRate = new FLMaxRate(userLogin.LoginName, connectionString, base.ClientId);
                objMaxRate.Delete(p_objXmlIn);
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("FLMaxRateAdaptor.Delete.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objMaxRate = null;
            }
        }
        #endregion
    }
}
