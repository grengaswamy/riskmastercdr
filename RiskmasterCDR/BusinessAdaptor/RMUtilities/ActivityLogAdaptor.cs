using System;
using System;
using System.Text;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common; 

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for ActivityLogAdaptor.
	/// </summary>
	public class ActivityLogAdaptor:BusinessAdaptorBase
	{
		public ActivityLogAdaptor(){}
		public bool Search(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			ActivityLog objActivityLog=null;			
			try
			{
				objActivityLog=new ActivityLog(connectionString,base.ClientId);//sonali-cloud
				p_objXmlOut=objActivityLog.Search(p_objXmlIn);
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("AssignLayoutsAdaptor.Get.Error",base.ClientId),BusinessAdaptorErrorType.Error);//sonali-cloud
				return false;
			}
			finally
			{
				objActivityLog=null;
			}
		}
	}
}
