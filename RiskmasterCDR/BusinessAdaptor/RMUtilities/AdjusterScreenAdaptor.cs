﻿using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
    ///************************************************************** 
    ///* $File		: AdjusterScreenAdaptor.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 2/Nov/2007
    ///* $Author	: Animesh Sahai 
    ///* $Comment	: 
    ///* $Source	: 
    ///**************************************************************
    /// <summary>	
    ///	This class is used to call the application layer component for Custom
    ///	which implements the functionality of Adjuster Screen form.
    /// </summary>
    class AdjusterScreenAdaptor : BusinessAdaptorBase
    {
        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        public AdjusterScreenAdaptor() { }

        #endregion

        #region Public Methods

        /// <summary>
        ///		This method is a wrapper to Riskmaster.Application.Custom.AdjusterScreen.New() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///		The structure of the output XML document would be as in  in Riskmaster.Application.Custom.AdjusterScreen.xml
        /// </param>
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool New(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            AdjusterScreen objAdjScreen = null; //Application layer component			
            try
            {
                objAdjScreen = new AdjusterScreen(base.userLogin.LoginName, base.userLogin.Password, base.userLogin.objRiskmasterDatabase.DataSourceName, connectionString, base.ClientId);//Add by kuladeep for Cloud. Jira-58 //Rijul : Worked for 7573 (Issue of 4634 - Epic 340)
                p_objXmlOut = objAdjScreen.New(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("NewError", base.ClientId), BusinessAdaptorErrorType.Error);//Add by kuladeep for Cloud. Jira-58
                return false;
            }
            finally
            {
                objAdjScreen = null;
            }
        }

        /// <summary>
        ///		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///		
        /// </param>
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            AdjusterScreen objAdjScreen = null; //Application layer component			
            try
            {
                objAdjScreen = new AdjusterScreen(base.userLogin.LoginName, base.userLogin.Password, base.userLogin.objRiskmasterDatabase.DataSourceName, connectionString, base.ClientId);//Add by kuladeep for Cloud. Jira-58 //Rijul : Worked for 7573 (Issue of 4634 - Epic 340)
                p_objXmlOut = objAdjScreen.Get(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("AdjusterGetError", base.ClientId), BusinessAdaptorErrorType.Error);//Add by kuladeep for Cloud. Jira-58
                return false;
            }
            finally
            {
                objAdjScreen = null;
            }
        }

        /// <summary>
        ///	
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///	
        /// </param>
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            AdjusterScreen objAdjScreen = null; //Application layer component			
            try
            {
                objAdjScreen = new AdjusterScreen(base.userLogin.LoginName, base.userLogin.Password, base.userLogin.objRiskmasterDatabase.DataSourceName, connectionString, base.ClientId);//Add by kuladeep for Cloud. Jira-58 //Rijul : Worked for 7573 (Issue of 4634 - Epic 340)
                p_objXmlOut = objAdjScreen.Save(p_objXmlIn, base.userLogin.LoginName);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("AdjusterSaveError", base.ClientId), BusinessAdaptorErrorType.Error);//Add by kuladeep for Cloud. Jira-58
                return false;
            }
            finally
            {
                objAdjScreen = null;
            }
        }

        /// <summary>
        ///	
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///	
        /// </param>
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool Delete(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            AdjusterScreen objAdjScreen = null; //Application layer component			
            try
            {
                objAdjScreen = new AdjusterScreen(base.userLogin.LoginName, base.userLogin.Password, base.userLogin.objRiskmasterDatabase.DataSourceName, connectionString, base.ClientId);//Add by kuladeep for Cloud. Jira-58 //Rijul : Worked for 7573 (Issue of 4634 - Epic 340)
                p_objXmlOut = objAdjScreen.Delete(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("AdjusterDeleteError", base.ClientId), BusinessAdaptorErrorType.Error);//Add by kuladeep for Cloud. Jira-58
                return false;
            }
            finally
            {
                objAdjScreen = null;
            }
        }

        /// <summary>
        ///		This method is a wrapper to Riskmaster.Application.Custom.AdjusterScreen.ReloadNonAvailPlan() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///
        /// </param>
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>


        public bool ReloadNonAvailPlan(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            AdjusterScreen objAdjScreen = null; //Application layer component			
            try
            {
                objAdjScreen = new AdjusterScreen(base.userLogin.LoginName, base.userLogin.Password, base.userLogin.objRiskmasterDatabase.DataSourceName, connectionString, base.ClientId);//Add by kuladeep for Cloud. Jira-58 //Rijul : Worked for 7573 (Issue of 4634 - Epic 340)
                p_objXmlOut = objAdjScreen.ReloadNonAvailPlan(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("AdjusterReloadError", base.ClientId), BusinessAdaptorErrorType.Error);//Add by kuladeep for Cloud. Jira-58
                return false;
            }
            finally
            {
                objAdjScreen = null;
            }
        }

        #endregion
    }
}

