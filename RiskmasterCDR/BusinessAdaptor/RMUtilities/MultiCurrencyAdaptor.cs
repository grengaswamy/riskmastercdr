﻿using System;
using System.Text;
using System.Xml;
using System.IO;
using System.Security;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
  ///************************************************************** 
  ///* $File		: MultiCurrencyAdaptor.cs 
  ///* $Revision	: 1.0.0.0 
  ///* $Date		: 12/16/2011
  ///* $Author	: Ishan Jha
  ///* $Comment	: 
  ///* $Source	: 
  ///**************************************************************

  /// <summary>	
  ///	This class is used to call the application layer component for MultiCurrency class  
  /// </summary>
  public class MultiCurrencyAdaptor : BusinessAdaptorBase
  {
    #region Constructor
    public MultiCurrencyAdaptor() { }
    #endregion

    #region Public Function
    /// <summary>
    /// This function xml containing data to be shown on screen
    /// </summary>
    /// <param name="p_objXmlIn">Input parameters as xml</param>
    /// <param name="p_objXmlOut">Result as Output xml
    ///	Input structure is as follows-:
    ///	<MultiCurrency><MultiCurrencyId></MultiCurrencyId></MultiCurrency>
    /// </param>
    /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
    /// <returns>Success -True or Failure -false in execution of the function</returns>
    public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
    {
      MultiCurrency objCurrency = null;
      try
      {
        objCurrency = new MultiCurrency(userLogin.LoginName, connectionString,base.ClientId);
        p_objXmlOut = objCurrency.Get(p_objXmlIn);
        return true;

      }
      catch (RMAppException p_objException)
      {
        p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
        return false;
      }
      catch (Exception p_objException)
      {
          p_objErrOut.Add(p_objException, Globalization.GetString("MultiCurrencyAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
        return false;
      }
      finally
      {
        objCurrency = null;
      }
    }
    /// <summary>
    /// Saves the Multi Currency information
    /// </summary>
    /// <param name="p_objXmlIn">Input parameters as xml</param>
    /// <param name="p_objXmlOut">Result as Output xml
    /// </param>
    /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
    /// <returns>Success -True or Failure -false in execution of the function</returns>
    public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
    {
      MultiCurrency objCurrency = null;

      try
      {
        objCurrency = new MultiCurrency(loginName, connectionString,base.ClientId);//sonali
        p_objXmlOut = objCurrency.Save(p_objXmlIn);
        return true;

      }
      catch (RMAppException p_objException)
      {
        p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
        return false;
      }
      catch (Exception p_objException)
      {
        p_objErrOut.Add(p_objException, Globalization.GetString("MultiCurrencyAdaptor.Save.Error",base.ClientId), BusinessAdaptorErrorType.Error);
        return false;
      }
      finally
      {
        objCurrency = null;
      }
    }
    /// <summary>
    /// Deletes the Multi Currency information
    /// </summary>
    /// <param name="p_objXmlIn">Input parameters as xml</param>
    /// <param name="p_objXmlOut">Result as Output xml
    /// </param>
    /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
    /// <returns>Success -True or Failure -false in execution of the function</returns>
    public bool Delete(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
    {
      MultiCurrency objCurrency = null;
      try
      {
        objCurrency = new MultiCurrency(userLogin.LoginName, connectionString,base.ClientId);//sonali
        objCurrency.Delete(p_objXmlIn);
        return true;

      }
      catch (RMAppException p_objException)
      {
        p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
        return false;
      }
      catch (Exception p_objException)
      {
        p_objErrOut.Add(p_objException, Globalization.GetString("MultiCurrencyAdaptor.Delete.Error",base.ClientId), BusinessAdaptorErrorType.Error);
        return false;
      }
      finally
      {
        objCurrency = null;
      }
    }

    /// <summary>
    ///		This method is a wrapper to Riskmaster.Application.MultiCurrencyAdaptor.GetSelectedMultiCurrencyInfo() method.		
    /// </summary>
    /// <param name="p_objXmlIn">Input XML document
    /// </param>
    /// <param name="p_objXmlOut">XML containing the results        
    /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
    /// <returns>True/False for success or failure of the function</returns>

    public bool GetSelectedMultiCurrencyInfo(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
    {
      MultiCurrency objCurrency = null; //Application layer component			
      try
      {
        objCurrency = new MultiCurrency(loginName, connectionString,base.ClientId);//sonali
        p_objXmlOut = objCurrency.GetSelectedMultiCurrencyInfo(p_objXmlIn);
        return true;
      }
      catch (RMAppException p_objException)
      {
        p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
        return false;
      }
      catch (Exception p_objException)
      {
        p_objErrOut.Add(p_objException, Globalization.GetString("MultiCurrencyAdaptor.GetSelectedMultiCurrencyInfo.Error",base.ClientId), BusinessAdaptorErrorType.Error);
        return false;
      }
      finally
      {
        objCurrency = null;
      }
    }

    #endregion
  }
}
