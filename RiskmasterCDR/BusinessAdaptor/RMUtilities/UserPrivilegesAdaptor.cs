using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;  

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: UserPrivilegesAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 31/05/2005
	///* $Author	: Pankaj
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for RM Utilities
	///	which implements the functionality of User Privileges Setup form.
	/// </summary>
	public class UserPrivilegesAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		/// <summary>
		/// Default Constructor
		/// </summary>
		public UserPrivilegesAdaptor(){}

		#endregion

		#region Public Methods


		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.UserPrivileges.Get() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.UserPrivileges.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			UserPrivileges objPriv=null; //Application layer component
			try
			{
				objPriv = new UserPrivileges(userLogin.LoginName,userLogin.Password,userLogin.objRiskmasterDatabase.DataSourceName, base.ClientId);
                objPriv.LanguageCode = userLogin.objUser.NlsCode; //Aman ML Change
				p_objXmlOut=objPriv.Get(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("UserPrivilegesAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objPriv = null;
			}
		}

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.UserPrivileges.Save() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.UserPrivileges.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			UserPrivileges objPriv=null; //Application layer component
            XmlElement objElm=null;
			try
			{
				objPriv = new UserPrivileges(userLogin.LoginName,userLogin.Password,userLogin.objRiskmasterDatabase.DataSourceName, base.ClientId);
                objElm = (XmlElement)p_objXmlIn.SelectSingleNode("//Groups");
                //rkotak : rma 7476, since the Listbox case been added in databinding helper, our existing XML are being hampered. the code below is just an haack to deal with the newly added code in generic file
                if (objElm != null)
                    objElm.InnerText = string.Empty;
                objElm = (XmlElement)p_objXmlIn.SelectSingleNode("//Users");
                if (objElm != null)
                    objElm.InnerText = string.Empty;
                //rma - 7476 end
				p_objXmlOut=objPriv.Save(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("UserPrivilegesAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objPriv = null;
                objElm = null;
			}
		}

		#endregion
	}
}