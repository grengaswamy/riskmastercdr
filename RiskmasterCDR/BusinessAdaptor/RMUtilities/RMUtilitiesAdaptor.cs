using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: RMUtilitiesAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 
	///* $Author	: Anurag,Pankaj,Parag
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for RM Utilities
	///	which implements the functionality of RM Utilities Forms.
	/// </summary>
	public class RMUtilitiesAdaptor :  BusinessAdaptorBase
	{
		/// <summary>
		/// Default Constructor
		/// </summary>
		public RMUtilitiesAdaptor(){}

		#region Public Methods

        /// <summary>
        /// Gets base currency value. this function is declared in RMUtilitiesAdaptor class so that it is available for all the utility classes that require base currency value.
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool GetBaseCurrency(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            LocalCache objCache = null;
            string sShortCode = string.Empty;
            string sDesc = string.Empty;
            XmlElement oElement = null;
            try
            {

                objCache = new LocalCache(connectionString,base.ClientId);
                oElement = p_objXmlOut.CreateElement("BaseCurrencyType");
                objCache.GetCodeInfo(CommonFunctions.GetBaseCurrencyCode(connectionString), ref sShortCode, ref sDesc);
                oElement.InnerText = sDesc.Split('|')[1];
                p_objXmlOut.AppendChild(oElement);
                //oSysExNode.AppendChild(oElement);
                //sSysFormEx = oxmlDocSysEx.OuterXml;
            }
            catch (RMAppException p_objRMExp)
            {
                throw p_objRMExp;
            }
            catch(Exception ex)
            {
                throw new RMAppException(Globalization.GetString("RMUtilitiesAdaptor.RMUtilitiesAdaptor.Error", base.ClientId), ex);
            }
            finally
            {
                if (objCache != null)
                {
                    objCache = null;
                }
                if (oElement != null)
                {
                    oElement = null;
                }
            }
            return true;
        }

		#endregion



	}
}