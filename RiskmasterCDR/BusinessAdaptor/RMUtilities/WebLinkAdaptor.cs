﻿using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
    class WebLinkAdaptor : BusinessAdaptorBase
    {
        public WebLinkAdaptor() { }

        public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            WebLink objWebLink = null;

            string sSelectedURL = null;


            try
            {
                // objWebLink = new WebLink(userLogin.LoginName, userLogin.Password, userLogin.objRiskmasterDatabase.DataSourceName);
                objWebLink = new WebLink(userLogin.LoginName, userLogin.Password, userLogin.objRiskmasterDatabase.DataSourceName, base.ClientId); //rkaur27
                p_objXmlOut = objWebLink.Get(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("WebLinksAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);//rkaur27
                return false;
            }
            finally
            {
                objWebLink = null;

            }
        }

        public bool LoadURL(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrout)
        {
            WebLink objWebLink = null;
            try
            {
                objWebLink = new WebLink(userLogin.LoginName, userLogin.Password, userLogin.objRiskmasterDatabase.DataSourceName, base.ClientId);//rkaur27
                p_objXmlOut = objWebLink.LoadUrl(p_objXmlIn);
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrout.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrout.Add(p_objException, Globalization.GetString("WebLinksAdaptor.Geturl.Error", base.ClientId), BusinessAdaptorErrorType.Error); //rkaur27
                return false;
            }
            finally
            {
                objWebLink = null;
            }


        }
        public bool SaveUrl(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrout)
        {
            WebLink objWebLink = null;

            try
            {
                objWebLink = new WebLink(userLogin.LoginName, userLogin.Password, userLogin.objRiskmasterDatabase.DataSourceName, base.ClientId);//rkaur27
                p_objXmlOut = objWebLink.SaveUrl(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrout.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrout.Add(p_objException, Globalization.GetString("WebLinksAdaptor.SaveUrl.Error", base.ClientId), BusinessAdaptorErrorType.Error);//rkaur27
                return false;
            }
            finally
            {
                objWebLink = null;
            }
        }

        public bool DeleteUrl(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrout)
        {
            WebLink objWebLink = null;

            try
            {
                objWebLink = new WebLink(userLogin.LoginName, userLogin.Password, userLogin.objRiskmasterDatabase.DataSourceName, base.ClientId);//rkaur27
                p_objXmlOut = objWebLink.DeleteUrl(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrout.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrout.Add(p_objException, Globalization.GetString("WebLinksAdaptor.Delete.Error", base.ClientId), BusinessAdaptorErrorType.Error);//rkaur27
                return false;
            }
            finally
            {
                objWebLink = null;
            }
        }

        public bool GetUrlData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            WebLink objWebLink = null;
            string sSelectedURL = null;


            try
            {
                objWebLink = new WebLink(userLogin.LoginName, userLogin.Password, userLogin.objRiskmasterDatabase.DataSourceName, base.ClientId);//rkaur27
                p_objXmlOut = objWebLink.GetURLData(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("WebLinksAdaptor.GetData.Error", base.ClientId), BusinessAdaptorErrorType.Error);//rkaur27
                return false;
            }
            finally
            {
                objWebLink = null;
            }
        }

    }
}
