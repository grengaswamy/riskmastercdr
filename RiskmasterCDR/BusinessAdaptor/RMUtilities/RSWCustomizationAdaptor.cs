﻿using System;
using System.Data;
using System.IO;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;
using Riskmaster.Settings;
using Riskmaster.Security;
using System.Collections;
using Riskmaster.Application.SecurityManagement;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
    public class RSWCustomizationAdaptor: BusinessAdaptorBase
    {
        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        public RSWCustomizationAdaptor()
        { }

        #endregion
        public bool GetLOB(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            RSWCustomization objParms = null;
            try
            {
                objParms = new RSWCustomization(connectionString, base.ClientId);
                objParms.LanguageCode = base.userLogin.objUser.NlsCode;  //Aman MITS 31626
                p_objXmlOut = objParms.GetLOB(p_objXmlIn);
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("RSWCustomizationAdaptor.GetLOB.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objParms = null;
            }
        }
        public bool GetTranstype(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            RSWCustomization objParms = null;
            try
            {
                objParms = new RSWCustomization(connectionString, base.ClientId);
                objParms.LanguageCode = base.userLogin.objUser.NlsCode; //Aman MITS 31626
                p_objXmlOut = objParms.GetTranstype(p_objXmlIn);
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("RSWCustomizationAdaptor.GetLOB.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objParms = null;
            }
        }
        public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            RSWCustomization objParms = null;
            try
            {

                objParms = new RSWCustomization(connectionString, base.ClientId);
                objParms.Save(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("RSWCustomizationAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objParms = null;
            }
        }
	
        	public bool GetReserveType(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			LOCParms objParms=null;	
			try
			{
                objParms = new LOCParms(connectionString, base.ClientId);
				p_objXmlOut=objParms.GetReserveType(p_objXmlIn);
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("RSWCustomizationAdaptor.GetReserveType.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objParms=null;
			}
		}
		
	
    }
}
