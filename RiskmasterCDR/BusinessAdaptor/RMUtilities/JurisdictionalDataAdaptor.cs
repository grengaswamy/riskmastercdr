using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;  

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: JurisdictionalDataAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 27/05/2005
	///* $Author	: Pankaj
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for RM Utilities
	///	which implements the functionality of Jurisdictional data.
	/// </summary>
	public class JurisdictionalDataAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		/// <summary>
		/// Default Constructor
		/// </summary>
		public JurisdictionalDataAdaptor(){}

		#endregion

		#region Public Methods

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.JurisdictionalData.Get() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.JurisdictionalData.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			JurisdictionalData objJuris=null; //Application layer component			
			try
			{
                objJuris = new JurisdictionalData(connectionString, base.ClientId);
				p_objXmlOut=objJuris.Get(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("JurisdictionalDataAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objJuris = null;
			}
		}
		#endregion
	}
}