using System;
using System.Text;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common; 

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: GrantReportAccessAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 24-May-2005
	///* $Author	: Parag Sarin
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	
	/// <summary>	
	///	This class is used to call the application layer component for GrantReportAccess class
	/// </summary>
	public class GrantReportAccessAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		public GrantReportAccessAdaptor(){}
		#endregion

		#region Public Function
		/// <summary>
		/// This function xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		/*
			<ReportsAccess>
				<Reports>
					<option value="315">07072004 - </option>
				</Reports>
				<Users>
					<option value="2">p (parag sarin)</option>
					<option value="3">t (test test)</option>
					<option value="4">m (manager manager)</option>
				</Users>
			</ReportsAccess>
		*/
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Get(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			GrantReportAccess objAccess=null;
            string sReportServerConnectString = string.Empty;
			
			try
			{
                sReportServerConnectString = RMConfigurationManager.GetConnectionString("ReportServer_ConnectionString", base.ClientId); //mbahl3 JIRA RMACLOUD-130
                objAccess = new GrantReportAccess(sReportServerConnectString, securityConnectionString, base.ClientId);			
				p_objXmlOut=objAccess.Get();
				
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("GrantReportAccessAdaptor.Get.Error", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
                objAccess = null;				
			}
		}
		/// <summary>
		/// This function Saves Report Access data
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Input structure is as follows-:
		/*
			<ReportsAccess>
				<Reports>
					<option value="315">07072004 - </option>
				</Reports>
				<Users>
					<option value="2">p (parag sarin)</option>
					<option value="3">t (test test)</option>
					<option value="4">m (manager manager)</option>
				</Users>
			</ReportsAccess>
		*/
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Save(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			GrantReportAccess objAccess=null;
            string sReportServerConnectString = string.Empty;
			try
			{

                sReportServerConnectString = RMConfigurationManager.GetConnectionString("ReportServer_ConnectionString", base.ClientId); //mbahl3 JIRA RMACLOUD-130
                objAccess = new GrantReportAccess(sReportServerConnectString, securityConnectionString, base.ClientId);			
				objAccess.Save(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("GrantReportAccessAdaptor.Save.Error", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objAccess=null;
			}
		}
		#endregion
	}
}
