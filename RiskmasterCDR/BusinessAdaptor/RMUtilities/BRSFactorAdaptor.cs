using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;  

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: BRSFactorAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 24/05/2005
	///* $Author	: Pankaj
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for RM Utilities
	///	which implements the functionality of BRS factor Details.
	/// </summary>
	public class BRSFactorAdaptor :  BusinessAdaptorBase
	{
		#region Constructor
		/// <summary>
		/// Default Constructor
		/// </summary>
		public BRSFactorAdaptor(){}

		#endregion

		#region Public Methods

		/// <summary>
		/// This method is a wrapper to Riskmaster.Application.RMUtilities.BRSFactor.Get() method.
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document</param>
		/// <param name="p_objXmlOut">XML containing the results.The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.BRSFactor.xml</param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>
		/// <returns>True/False for success or failure of the function</returns>
		public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			BRSFactor objBRSFct=null; //Application layer component			
			try
			{
				objBRSFct = new BRSFactor(connectionString,base.ClientId);
				//get the data
				p_objXmlOut=objBRSFct.Get(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException, Globalization.GetString("BRSFactorAdaptor.Get.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objBRSFct = null;
			}
		}


		/// <summary>
		/// This method is a wrapper to Riskmaster.Application.RMUtilities.BRSFactor.New() method.
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document</param>
		/// <param name="p_objXmlOut">XML containing the results.The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.BRSFactor.xml</param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>
		/// <returns>True/False for success or failure of the function</returns>
		public bool New(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			BRSFactor objBRSFct=null; //Application layer component			
			try
			{
				objBRSFct = new BRSFactor(connectionString,base.ClientId);
				p_objXmlOut=objBRSFct.New(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException, Globalization.GetString("BRSFactorAdaptor.New.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objBRSFct = null;
			}
		}


		/// <summary>
		/// This method is a wrapper to Riskmaster.Application.RMUtilities.BRSFactor.Save() method.
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document</param>
		/// <param name="p_objXmlOut">XML containing the results.The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.BRSFactor.xml</param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>
		/// <returns>True/False for success or failure of the function</returns>
		public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			BRSFactor objBRSFct=null; //Application layer component			
			try
			{
				objBRSFct = new BRSFactor(connectionString,base.ClientId);
				//get the data
				p_objXmlOut=objBRSFct.Save(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException, Globalization.GetString("BRSFactorAdaptor.Save.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objBRSFct = null;
			}
		}

		/// <summary>
		/// This method is a wrapper to Riskmaster.Application.RMUtilities.BRSFactor.Delete() method.
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document</param>
		/// <param name="p_objXmlOut">XML containing the results.The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.BRSFactor.xml</param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>
		/// <returns>True/False for success or failure of the function</returns>
		public bool Delete(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			BRSFactor objBRSFct=null; //Application layer component			
			try
			{
				objBRSFct = new BRSFactor(connectionString,base.ClientId);
				//get the data
				p_objXmlOut=objBRSFct.Delete(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException, Globalization.GetString("BRSFactorAdaptor.Delete.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objBRSFct = null;
			}
		}
		#endregion
	}
}