using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;  

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: RateSetupListAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 13/11/2009
	///* $Author	: Tushar
	///* $Comment	: 
	///* $Source	: MITS 18231
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for RM Utilities
	///	which implements the functionality of Discount List.
	/// </summary>
	public class RateSetupListAdaptor :  BusinessAdaptorBase
	{
		#region Constructor
		/// <summary>
		/// Default Constructor
		/// </summary>
        public RateSetupListAdaptor() { }

		#endregion

		#region Public Methods

		/// <summary>
        ///		This method is a wrapper to Riskmaster.Application.RMUtilities.RateListListAdaptor.Get() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
        ///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.RateListList.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
        /// Added bY Tushar MITS 18231
		public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
            //Application layer component	
			RateList  objRateLst=null; 		
			try
			{
                objRateLst = new RateList(connectionString, base.ClientId);
                p_objXmlOut = objRateLst.Get(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("RateList.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
                objRateLst = null;
			}
		}

		#endregion
	}
}