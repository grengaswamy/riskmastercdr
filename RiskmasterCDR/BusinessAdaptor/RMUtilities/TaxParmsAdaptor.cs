﻿using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
    ///************************************************************** 
    ///* $File		: TaxParmsAdaptor.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 30-07-2009
    ///* $Author	: Animesh Sahai
    ///* $Comment	: 
    ///* $Source	: 
    ///**************************************************************
    /// <summary>	
    ///	This class is used to call the application layer component for TaxParms class  
    /// </summary>
    class TaxParmsAdaptor : BusinessAdaptorBase
    {
        #region Constructor
        public TaxParmsAdaptor()
		{

		}
		#endregion
        #region Public Function
        /// <summary>
        /// This function xml containing data to be shown on screen
        /// </summary>
        /// <param name="p_objXmlIn">
        ///		Input parameters as xml
        ///			<Document>
        ///				<Tax>
        ///					<use></use>
        ///					<amount></amount>
        ///					<effectivedate></effectivedate>
        ///					<expirationdate></expirationdate>
        ///					<taxtype></taxtype>
        ///					<lob></lob>
        ///					<state></state>
        ///					<PolicyTaxrowid></PolicyTaxrowid>
        ///				</Tax>
        ///			</Document>
        ///	</param>
        /// <param name="p_objXmlOut">
        ///		Result as Output xml Output structure is as follows-: 
        ///			<Document>
        ///				<Tax>
        ///					<use></use>
        ///					<amount></amount>
        ///					<effectivedate></effectivedate>
        ///					<expirationdate></expirationdate>
        ///					<taxtype></taxtype>
        ///					<lob></lob>
        ///					<state></state>
        ///					<PolicyTaxrowid></PolicyTaxrowid>
        ///				</Tax>
        ///			</Document>
        /// </param>
        /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
        /// <returns>Success -True or Failure -false in execution of the function</returns>
        public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            XmlElement objElement = null;					//used for parsing the input xml
            TaxParms objTaxParms = null;

            try
            {
                //check existence of input xml which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//Tax");
                if (objElement == null)
                {
                    p_objErrOut.Add("TaxParmsAdaptor.Get.Error", Globalization.GetString("TaxParmsAdaptor.InputXMLMissing", base.ClientId), BusinessAdaptorErrorType.Warning);
                    return false;
                }

                objTaxParms = new TaxParms(loginName, connectionString, base.ClientId);
                p_objXmlOut = objTaxParms.LoadData(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("TaxParmsAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objTaxParms = null;
            }
        }

        /// <summary>
        /// This function xml containing data to be shown on screen
        /// </summary>
        /// <param name="p_objXmlIn">
        ///		Input parameters as xml
        ///			<Document>
        ///				<Tax>
        ///					<use>1</use>
        ///					<amount>28</amount>
        ///					<effectivedate>20050406</effectivedate>
        ///					<expirationdate>20050418</expirationdate>
        ///					<taxtype>5237</taxtype>
        ///					<lob>5188</lob>
        ///					<state>73</state>
        ///					<PolicyTaxrowid>4</PolicyTaxrowid> OR <expconrowid></expconrowid>(for new record)
        ///				</Tax>
        ///			</Document>
        ///	</param>
        /// <param name="p_objXmlOut">Result as Output xml</param>
        /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
        /// <returns>Success -True or Failure -false in execution of the function</returns>
        public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            XmlElement objElement = null;					//used for parsing the input xml
            TaxParms objTaxParms = null;

            try
            {
                //check existence of input xml which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//Tax");
                if (objElement == null)
                {
                    p_objErrOut.Add("TaxParmsAdaptor.Get.Error", Globalization.GetString("TaxParmsAdaptor.InputXMLMissing", base.ClientId), BusinessAdaptorErrorType.Warning);
                    return false;
                }

                objTaxParms = new TaxParms(loginName, connectionString, base.ClientId);
                objTaxParms.Save(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {   
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.PopupMessage);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("TaxParmsAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objTaxParms = null;
            }
        }
        #endregion

    }
}
