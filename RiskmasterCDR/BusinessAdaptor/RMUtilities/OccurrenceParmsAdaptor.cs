using System;
using System.Xml;
using Riskmaster.Common; 
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: OccurrenceParmsAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 03-June-2005
	///* $Author	: Anurag Agarwal
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for OccurrenceParms class  
	/// </summary>
	public class OccurrenceParmsAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		public OccurrenceParmsAdaptor()
		{
		}
		#endregion

		#region Public Function
		/// <summary>
		/// This function xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">
		///		Input parameters as xml
		///		<document>
		///			<RowId></RowId>
		///		</document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml
		///		Output structure is as follows-:
		///		See Xml file named OccurrenceParms.xml in Xml folder
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Get(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			OccurrenceParms objOccurrenceParms=null;	

			try
			{
                //gagnihotri MITS 11995 Changes made for Audit Table
				//objOccurrenceParms=new  OccurrenceParms(connectionString); 
                objOccurrenceParms = new OccurrenceParms(connectionString, userLogin.LoginName, base.ClientId);//rkaur27
                objOccurrenceParms.LanguageCode = base.userLogin.objUser.NlsCode; //Aman ML Change
				p_objXmlOut=objOccurrenceParms.Get(p_objXmlIn);
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("OccurrenceParmsAdaptor.Get.Error", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objOccurrenceParms=null;
			}
		}

		/// <summary>
		///		This function Saves Layout data
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Save(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			OccurrenceParms objOccurrenceParms=null;	
			try
			{
                //gagnihotri MITS 11995 Changes made for Audit table
				//objOccurrenceParms=new OccurrenceParms(connectionString);
                objOccurrenceParms = new OccurrenceParms(connectionString, userLogin.LoginName, base.ClientId);//rkaur27
				objOccurrenceParms.Save(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("OccurrenceParmsAdaptor.Save.Error", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objOccurrenceParms=null;
			}
		}
		#endregion
	}
}


