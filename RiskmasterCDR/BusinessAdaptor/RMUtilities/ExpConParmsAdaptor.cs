using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: ExpConParmsAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 30-May-2005
	///* $Author	: Anurag Agarwal
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for ExpConParms class  
	/// </summary>
	public class ExpConParmsAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		public ExpConParmsAdaptor()
		{
		}
		#endregion

		#region Public Function
		/// <summary>
		/// This function xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">
		///		Input parameters as xml
		///			<Document>
		///				<ExpCon>
		///					<use></use>
		///					<amount></amount>
		///					<effectivedate></effectivedate>
		///					<expirationdate></expirationdate>
		///					<expcontype></expcontype>
		///					<lob></lob>
		///					<state></state>
		///					<expconrowid>4</expconrowid>
		///				</ExpCon>
		///			</Document>
		///	</param>
		/// <param name="p_objXmlOut">
		///		Result as Output xml Output structure is as follows-: 
		///			<Document>
		///				<ExpCon>
		///					<use></use>
		///					<amount></amount>
		///					<effectivedate></effectivedate>
		///					<expirationdate></expirationdate>
		///					<expcontype></expcontype>
		///					<lob></lob>
		///					<state></state>
		///					<expconrowid>4</expconrowid>
		///				</ExpCon>
		///			</Document>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Get(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			XmlElement objElement=null;					//used for parsing the input xml
			ExpConParms objExpConParms=null;	

			try
			{
				//check existence of input xml which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ExpCon");
				if (objElement==null)
				{
					p_objErrOut.Add("ExpConParmsAdaptor.Get.Error",Globalization.GetString("ExpConParmsAdaptor.InputXMLMissing", base.ClientId),BusinessAdaptorErrorType.Warning);
					return false;
				}

                objExpConParms = new ExpConParms(loginName, connectionString, base.ClientId);			
				p_objXmlOut=objExpConParms.LoadData(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("ExpConParmsAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objExpConParms=null;
			}
		}

		/// <summary>
		/// This function xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">
		///		Input parameters as xml
		///			<Document>
		///				<ExpCon>
		///					<use>1</use>
		///					<amount>28</amount>
		///					<effectivedate>20050406</effectivedate>
		///					<expirationdate>20050418</expirationdate>
		///					<expcontype>5237</expcontype>
		///					<lob>5188</lob>
		///					<state>73</state>
		///					<expconrowid>4</expconrowid> OR <expconrowid></expconrowid>(for new record)
		///				</ExpCon>
		///			</Document>
		///	</param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Save(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			XmlElement objElement=null;					//used for parsing the input xml
			ExpConParms objExpConParms=null;	

			try
			{
				//check existence of input xml which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ExpCon");
				if (objElement==null)
				{
                    p_objErrOut.Add("ExpConParmsAdaptor.Get.Error", Globalization.GetString("ExpConParmsAdaptor.InputXMLMissing", base.ClientId), BusinessAdaptorErrorType.Warning);
					return false;
				}

                objExpConParms = new ExpConParms(loginName, connectionString, base.ClientId);			
				objExpConParms.Save(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
            {   // 06/12/2007 MITS 9674: Umesh
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.PopupMessage);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("ExpConParmsAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objExpConParms=null;
			}
		}
		#endregion
	}
}
