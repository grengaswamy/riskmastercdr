using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: CPCredentialsAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 1-June-2005
	///* $Author	: Anurag Agarwal
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for CPCredentials class  
	/// </summary>
	public class CPCredentialsAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		public CPCredentialsAdaptor()
		{
		}
		#endregion

	}
}
