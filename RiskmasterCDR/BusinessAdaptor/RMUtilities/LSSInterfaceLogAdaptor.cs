using System;
using System.Text;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common; 

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for LSSInterfaceLogAdaptor.
	/// </summary>
	public class LSSInterfaceLogAdaptor:BusinessAdaptorBase
	{
		public LSSInterfaceLogAdaptor(){}
		public bool Search(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			LSSInterfaceLog objLSSInterfaceLog = null;
			try
			{
                objLSSInterfaceLog = new LSSInterfaceLog(connectionString, base.ClientId);
                objLSSInterfaceLog.LanguageCode = base.userLogin.objUser.NlsCode; //tmalhotra3 ML Change
				p_objXmlOut = objLSSInterfaceLog.Search(p_objXmlIn);
				return true;

			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException, Globalization.GetString("LSSInterfaceLog.Get.Error", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objLSSInterfaceLog = null;
			}
		}
	}
}
