/**********************************************************************************************
*   Date     |  MITS/JIRA   | Programmer | Description                                            *
**********************************************************************************************
* 06/04/2014 | 33371        | ajohari2   | Changes for GetUserList for New TPA changes
* 7/24/2014  | RMA-718      | ajohari2   | Changes for TPA Access ON/OFF
**********************************************************************************************/
using System;
using System.Text;
using System.Xml;
using System.IO;
using System.Security;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Settings; //MITS:33371 ajohari2

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
    ///************************************************************** 
    ///* $File		: CustomizeUserListAdaptor.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 24-May-2005
    ///* $Author	: Rahul Solanki
    ///* $Comment	: Customized User List Enhancement R4
    ///* $Source	: 
    ///**************************************************************

    /// <summary>	
    ///	This class is used to call the application layer component for CustomizeUserList class  
    /// </summary>
    public class CustomizeUserListAdaptor : BusinessAdaptorBase
    {
        #region Constructor
        public CustomizeUserListAdaptor() { }
        #endregion

        #region Public Function
        /// <summary>
        /// This function returns xml containing user list to be shown on screen
        /// </summary>
        /// <param name="p_objXmlIn">Input parameters as xml</param>
        /// <param name="p_objXmlOut">Result as Output xml
        ///	Output structure is as follows-:
        ///	Input Xml format is
        ///	<WorkersCompTransMapping><WCJurisdictions></WCJurisdictions></WorkersCompTransMapping>
        /// </param>
        /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
        /// <returns>Success -True or Failure -false in execution of the function</returns>	

        public bool GetUserList(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            CustomizeUserList objCustomizeUserList = null;
            SysSettings oSettings = null;//MITS:33371 ajohari2
            XmlNode callerNode; //akaur9 Mobile Adjuster
            try
            {
                objCustomizeUserList = new CustomizeUserList(connectionString, userLogin.objRiskmasterDatabase.DataSourceId.ToString(),
                    securityConnectionString, userLogin.UserId, base.ClientId);
                //akaur9 Mobile Adjuster
                callerNode = p_objXmlIn.SelectSingleNode("//caller");                
                if (callerNode == null)
                {
                    //MITS:33371 ajohari2: Start
                    //p_objXmlOut = objCustomizeUserList.GetUserList(p_objXmlIn);
                    oSettings = new SysSettings(m_userLogin.objRiskmasterDatabase.ConnectionString, base.ClientId);
                    if (m_userLogin.objRiskmasterDatabase.OrgSecFlag && (oSettings.MultiCovgPerClm == -1) && oSettings.EnableTPAAccess) //JIRA RMA-718 ajohari2
                    {
                        p_objXmlOut = objCustomizeUserList.GetUserListByGroup(p_objXmlIn, m_userLogin.GroupId);
                    }
                    else
                    {
                        p_objXmlOut = objCustomizeUserList.GetUserList(p_objXmlIn);
                    }
                    //MITS:33371 ajohari2: End
                }
                else if (callerNode != null && (callerNode.InnerText == "MobileAdjuster" || callerNode.InnerText == "MobilityAdjuster"))
                {
                    p_objXmlOut = objCustomizeUserList.GetUserListForMobileAdjuster(p_objXmlIn);
                }

                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("CustomizeUserListAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objCustomizeUserList = null;
                oSettings = null;//MITS:33371 ajohari2
            }
        }

        #endregion
    }
}
