using System;
using System.Text;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: FiscalYearWizardAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 24-May-2005
	///* $Author	: Parag Sarin
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	
	/// <summary>	
	///	This class is used to call the application layer component for FiscalYearWizard class
	/// </summary>
	public class FiscalYearWizardAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		public FiscalYearWizardAdaptor(){}
		#endregion

		#region Public methods

		/// <summary>
		/// This function returns xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		/*
		 <FiscalYearWizard>
			<LineOfBusiness>0</LineOfBusiness>
			<Organization>0</Organization>
			<NoOfPeriods>5</NoOfPeriods>
			<FiscalYear>2006</FiscalYear>
			<FiscalWeekDay>4</FiscalWeekDay>
			<FiscalYearStart>4/22/2006</FiscalYearStart>
			<FiscalYearEnd>6/21/2006</FiscalYearEnd>
			<FiscalQ1Start>4/22/2006</FiscalQ1Start>
			<FiscalQ1End>7/21/2006</FiscalQ1End>
			<FiscalQ2Start>7/22/2006</FiscalQ2Start>
			<FiscalQ2End>10/21/2006</FiscalQ2End>
			<FiscalQ3Start>10/22/2006</FiscalQ3Start>
			<FiscalQ3End>1/21/2007</FiscalQ3End>
			<FiscalQ4Start>1/22/2007</FiscalQ4Start>
			<FiscalQ4End>4/21/2007</FiscalQ4End>
			<Periods>
				<Period>
				<StartDate>4/22/2006</StartDate>
				<EndDate>
				</EndDate>
				</Period>
				<Period>
				<StartDate>
				</StartDate>
				<EndDate>
				</EndDate>
				</Period>
				<Period>
				<StartDate>
				</StartDate>
				<EndDate>
				</EndDate>
				</Period>
				<Period>
				<StartDate>
				</StartDate>
				<EndDate>
				</EndDate>
				</Period>
				<Period>
				<StartDate>
				</StartDate>
				<EndDate>6/21/2006</EndDate>
				</Period>
			</Periods>
		</FiscalYearWizard>
		*/
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Get(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			FiscalYearWizard objWizard=null;		
			try
			{
                objWizard = new FiscalYearWizard(connectionString, base.ClientId);//sonali-cloud
                objWizard.LanguageCode = base.userLogin.objUser.NlsCode;
				p_objXmlOut=objWizard.Get(p_objXmlIn);
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("FiscalYearWizardAdaptor.Get.Error",base.ClientId),BusinessAdaptorErrorType.Error);//sonali-cloud
				return false;
			}
			finally
			{
				objWizard=null;
			}
		}
		/// <summary>
		///  This function saves fiscal years information
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Input structure is as follows-:
		/*
		  <FiscalYearWizard>
			<LineOfBusiness>0</LineOfBusiness>
			<Organization>0</Organization>
			<NoOfPeriods>5</NoOfPeriods>
			<FiscalYear>2006</FiscalYear>
			<FiscalWeekDay>4</FiscalWeekDay>
			<FiscalYearStart>4/22/2006</FiscalYearStart>
			<FiscalYearEnd>6/21/2006</FiscalYearEnd>
			<FiscalQ1Start>4/22/2006</FiscalQ1Start>
			<FiscalQ1End>7/21/2006</FiscalQ1End>
			<FiscalQ2Start>7/22/2006</FiscalQ2Start>
			<FiscalQ2End>10/21/2006</FiscalQ2End>
			<FiscalQ3Start>10/22/2006</FiscalQ3Start>
			<FiscalQ3End>1/21/2007</FiscalQ3End>
			<FiscalQ4Start>1/22/2007</FiscalQ4Start>
			<FiscalQ4End>4/21/2007</FiscalQ4End>
			<Periods>
				<Period>
				<StartDate>4/22/2006</StartDate>
				<EndDate>
				</EndDate>
				</Period>
				<Period>
				<StartDate>
				</StartDate>
				<EndDate>
				</EndDate>
				</Period>
				<Period>
				<StartDate>
				</StartDate>
				<EndDate>
				</EndDate>
				</Period>
				<Period>
				<StartDate>
				</StartDate>
				<EndDate>
				</EndDate>
				</Period>
				<Period>
				<StartDate>
				</StartDate>
				<EndDate>6/21/2006</EndDate>
				</Period>
			</Periods>
		</FiscalYearWizard>
		*/
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Save(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			FiscalYearWizard objWizard=null;					
			try
			{
				objWizard=new FiscalYearWizard(connectionString,base.ClientId);//sonali-cloud
                objWizard.LanguageCode = base.userLogin.objUser.NlsCode;
				objWizard.Save(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("FiscalYearWizardAdaptor.Save.Error",base.ClientId),BusinessAdaptorErrorType.Error);//sonali-cloud
				return false;
			}
			finally
			{
				objWizard=null;
			}
		}
		/// <summary>
		///  This function saves tax mapping information
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Input structure is as follows-:
		/*
		 <FiscalYearWizard>
			<LineOfBusiness>0</LineOfBusiness>
			<Organization>0</Organization>
			<NoOfPeriods>5</NoOfPeriods>
			<FiscalYear>2006</FiscalYear>
			<FiscalWeekDay>4</FiscalWeekDay>
			<FiscalYearStart>4/22/2006</FiscalYearStart>
			<FiscalYearEnd>6/21/2006</FiscalYearEnd>
			<FiscalQ1Start>4/22/2006</FiscalQ1Start>
			<FiscalQ1End>7/21/2006</FiscalQ1End>
			<FiscalQ2Start>7/22/2006</FiscalQ2Start>
			<FiscalQ2End>10/21/2006</FiscalQ2End>
			<FiscalQ3Start>10/22/2006</FiscalQ3Start>
			<FiscalQ3End>1/21/2007</FiscalQ3End>
			<FiscalQ4Start>1/22/2007</FiscalQ4Start>
			<FiscalQ4End>4/21/2007</FiscalQ4End>
			<Periods>
				<Period>
				<StartDate>4/22/2006</StartDate>
				<EndDate>
				</EndDate>
				</Period>
				<Period>
				<StartDate>
				</StartDate>
				<EndDate>
				</EndDate>
				</Period>
				<Period>
				<StartDate>
				</StartDate>
				<EndDate>
				</EndDate>
				</Period>
				<Period>
				<StartDate>
				</StartDate>
				<EndDate>
				</EndDate>
				</Period>
				<Period>
				<StartDate>
				</StartDate>
				<EndDate>6/21/2006</EndDate>
				</Period>
			</Periods>
		</FiscalYearWizard>
		*/
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool BuildPeriods(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			FiscalYearWizard objWizard=null;		
			try
			{
                objWizard = new FiscalYearWizard(connectionString,base.ClientId);//sonali-cloud
                objWizard.LanguageCode = base.userLogin.objUser.NlsCode;
				p_objXmlOut=objWizard.BuildPeriods(p_objXmlIn);
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("FiscalYearWizardAdaptor.BuildPeriods.Error",base.ClientId),BusinessAdaptorErrorType.Error);//sonali-cloud
				return false;
			}
			finally
			{
				objWizard=null;
			}
		}
		/// <summary>
		/// This function saves tax mapping information
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Input structure is as follows-:
		/*
	 <FiscalYearWizard>
			<LineOfBusiness>0</LineOfBusiness>
			<Organization>0</Organization>
			<NoOfPeriods>5</NoOfPeriods>
			<FiscalYear>2006</FiscalYear>
			<FiscalWeekDay>4</FiscalWeekDay>
			<FiscalYearStart>4/22/2006</FiscalYearStart>
			<FiscalYearEnd>6/21/2006</FiscalYearEnd>
			<FiscalQ1Start>4/22/2006</FiscalQ1Start>
			<FiscalQ1End>7/21/2006</FiscalQ1End>
			<FiscalQ2Start>7/22/2006</FiscalQ2Start>
			<FiscalQ2End>10/21/2006</FiscalQ2End>
			<FiscalQ3Start>10/22/2006</FiscalQ3Start>
			<FiscalQ3End>1/21/2007</FiscalQ3End>
			<FiscalQ4Start>1/22/2007</FiscalQ4Start>
			<FiscalQ4End>4/21/2007</FiscalQ4End>
			<Periods>
				<Period>
				<StartDate>4/22/2006</StartDate>
				<EndDate>
				</EndDate>
				</Period>
				<Period>
				<StartDate>
				</StartDate>
				<EndDate>
				</EndDate>
				</Period>
				<Period>
				<StartDate>
				</StartDate>
				<EndDate>
				</EndDate>
				</Period>
				<Period>
				<StartDate>
				</StartDate>
				<EndDate>
				</EndDate>
				</Period>
				<Period>
				<StartDate>
				</StartDate>
				<EndDate>6/21/2006</EndDate>
				</Period>
			</Periods>
		</FiscalYearWizard>
		*/
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool BuildQuaters(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			FiscalYearWizard objWizard=null;		
			try
			{
                objWizard = new FiscalYearWizard(connectionString, base.ClientId);//sonali-cloud
                objWizard.LanguageCode = base.userLogin.objUser.NlsCode;
				p_objXmlOut=objWizard.BuildQuaters(p_objXmlIn);
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("FiscalYearWizardAdaptor.BuildQuaters.Error",base.ClientId),BusinessAdaptorErrorType.Error);//sonali-cloud
				return false;
			}
			finally
			{
				objWizard=null;
			}
		}
		#endregion

	}
}
