using System;
using System.Text;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: PVListAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 25-May-2005
	///* $Author	: Anurag Agarwal
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	
	/// <summary>	
	///	This class is used to call the application layer component for PVList class  
	/// </summary>
	public class PVListAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		public PVListAdaptor()
		{
		}
		#endregion

		#region Public Function
		/// <summary>
		/// This function xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///	See Xml file named PVList.xml in Xml folder
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Get(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			PVList objPVList=null;	
			try
			{
                objPVList = new PVList(connectionString, userLogin.objRiskmasterDatabase.DataSourceId.ToString(), base.ClientId);	//mbahl3 JIRA [RMACLOUD-123]		
				p_objXmlOut=objPVList.Get(p_objXmlIn);
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("PVListAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error); // mbahl3 JIRA [RMACLOUD-123]
				return false;
			}
			finally
			{
				objPVList=null;
			}
		}
		#endregion
	}
}
