﻿using System;
using System.Text;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
    public class TPACodeMappingAdaptor : BusinessAdaptorBase
    {
        public bool GetTPASystem(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            TPACodeMapping objTPACodeMapping = null;
            try
            {
                objTPACodeMapping = new TPACodeMapping(connectionString, base.ClientId);
                p_objXmlOut = objTPACodeMapping.GetTPASystem(p_objXmlIn);
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("TPACodeMappingAdaptor.GetTPASystem.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objTPACodeMapping = null;
            }
        }
        
        public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            TPACodeMapping objTPACodeMapping = null;
            try
            {
                objTPACodeMapping = new TPACodeMapping(connectionString, base.ClientId);
                p_objXmlOut = objTPACodeMapping.Get(p_objXmlIn);
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("TPACodeMappingAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objTPACodeMapping = null;
            }
        }

        public bool GetCodeDetails(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            TPACodeMapping objTPACodeMapping = null;
            try
            {
                objTPACodeMapping = new TPACodeMapping(connectionString, base.ClientId);
                p_objXmlOut = objTPACodeMapping.GetCodeDetails(p_objXmlIn);
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("TPACodeMappingAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objTPACodeMapping = null;
            }
        }

        public bool SaveTPACodeInfo(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            XmlElement objElement = null;
            TPACodeMapping objTPACodeMapping = null;
            string sError = string.Empty;
            try
            {
                //rupal:start
                //check existence of RMCode which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//RMCode");
                if (objElement != null && (objElement.InnerText == string.Empty || objElement.InnerText == "0"))
                {
                    p_objErrOut.Add("TPACodeMappingAdaptorRMCodeMissing", Globalization.GetString("TPACodeMappingAdaptor.SaveTPACodeInfo.RMCodeMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }

                //check existence of PolSysCode which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//TPASysCode");
                if (objElement != null && (objElement.InnerText == string.Empty || objElement.InnerText == "0"))
                {
                    p_objErrOut.Add("TPACodeMappingAdaptorTPASysCodeMissing", Globalization.GetString("TPACodeMappingAdaptor.SaveTPACodeInfo.TPASysCodeMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }

                //check existence of CodeType which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//CodeType");
                if (objElement != null && (objElement.InnerText == string.Empty || objElement.InnerText == "0"))
                {
                    p_objErrOut.Add("TPACodeMappingAdaptorCodeTypeMissing", Globalization.GetString("TPACodeMappingAdaptor.SaveTPACodeInfo.CodeTypeMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }

                objTPACodeMapping = new TPACodeMapping(connectionString, base.ClientId);
                objTPACodeMapping.SaveTPACodeInfo(p_objXmlIn,out sError);
                //changed by swati
                if (sError != string.Empty)
                {
                    p_objErrOut.Add("TPACodeMappingAdaptorCodeMappingAlreadyExist", Globalization.GetString("TPACodeMappingAdaptorCodeMappingAlreadyExist", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                else
                {
                    return true;
                }
                //return true;
                //end here by swati
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("TPACodeMappingAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objTPACodeMapping = null;
            }
        }

        public bool DeleteTPACodeMapping(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            TPACodeMapping objTPACodeMapping = null; //Application layer component			
            try
            {
                objTPACodeMapping = new TPACodeMapping(connectionString, base.ClientId);
                p_objXmlOut = objTPACodeMapping.DeleteTPACodeMapping(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("TPACodeMappingAdaptor.Delete.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objTPACodeMapping = null;
            }
        }

        public bool AddNewMapping(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            XmlElement objElement = null;
            TPACodeMapping objTPACodeMapping = null;
            string sError = string.Empty;

            try
            {
                objTPACodeMapping = new TPACodeMapping(connectionString, base.ClientId);
                objTPACodeMapping.AddNewMapping(p_objXmlIn, out sError);
                if (sError != string.Empty)
                {
                    p_objErrOut.Add("TPACodeMappingAdaptorTPAAlreadyExist", Globalization.GetString("TPACodeMappingAdaptorTPAAlreadyExist", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("TPACodeMappingAdaptor.TPAAdd.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objTPACodeMapping = null;
            }
        }

        public bool GetTPAMapping(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            TPACodeMapping objTPACodeMapping = null; //Application layer component	
            try
            {
                objTPACodeMapping = new TPACodeMapping(connectionString, base.ClientId);
                p_objXmlOut = objTPACodeMapping.GetTPAMapping(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("TPACodeMappingAdaptor.GetTPA.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objTPACodeMapping = null;
            }
        }

        public bool DeleteTPAMapping(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            TPACodeMapping objTPACodeMapping = null; //Application layer component			
            try
            {
                objTPACodeMapping = new TPACodeMapping(connectionString, base.ClientId);
                p_objXmlOut = objTPACodeMapping.DeleteTPAMapping(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("TPACodeMappingAdaptor.DeleteTPA.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objTPACodeMapping = null;
            }
        }

        public bool RefershNewMapping(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            XmlElement objElement = null;
            TPACodeMapping objTPACodeMapping = null;
            string sError = string.Empty;

            try
            {
                objTPACodeMapping = new TPACodeMapping(connectionString, base.ClientId);
                p_objXmlOut = objTPACodeMapping.RefershNewMapping(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("TPACodeMappingAdaptor.GetTPA.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objTPACodeMapping = null;
            }
        }

        //added by swati for DCI and CLUE
        public bool GetProcessTypes(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            TPACodeMapping objTPACodeMapping = null;
            try
            {
                objTPACodeMapping = new TPACodeMapping(connectionString, base.ClientId);
                p_objXmlOut = objTPACodeMapping.GetProcessTypes(p_objXmlIn);
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("TPACodeMappingAdaptor.GetAvailableProcess.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objTPACodeMapping = null;
            }
        }


        public bool GetAvailableProcess(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            TPACodeMapping objTPACodeMapping = null; //Application layer component	
            try
            {
                objTPACodeMapping = new TPACodeMapping(connectionString, base.ClientId);
                p_objXmlOut = objTPACodeMapping.GetAvailableProcess(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("TPACodeMappingAdaptor.GetTPA.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objTPACodeMapping = null;
            }
        }

        public bool AddNewProcess(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            XmlElement objElement = null;
            TPACodeMapping objTPACodeMapping = null;
            string sError = string.Empty;

            try
            {
                objTPACodeMapping = new TPACodeMapping(connectionString, base.ClientId);
                objTPACodeMapping.AddNewProcess(p_objXmlIn, out sError);
                if (sError != string.Empty)
                {
                    p_objErrOut.Add("TPACodeMappingAdaptorProcessAlreadyExist", Globalization.GetString("TPACodeMappingAdaptorProcessAlreadyExist", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("TPACodeMappingAdaptor.ProcessAdd.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objTPACodeMapping = null;
            }
        }

        public bool DeleteProcessMapping(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            TPACodeMapping objTPACodeMapping = null; //Application layer component			
            try
            {
                objTPACodeMapping = new TPACodeMapping(connectionString, base.ClientId);
                p_objXmlOut = objTPACodeMapping.DeleteProcessMapping(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("TPACodeMappingAdaptor.DeleteTPA.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objTPACodeMapping = null;
            }
        }

        public bool GetProcessName(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            TPACodeMapping objTPACodeMapping = null;
            try
            {
                objTPACodeMapping = new TPACodeMapping(connectionString, base.ClientId);
                p_objXmlOut = objTPACodeMapping.GetProcessName(p_objXmlIn);
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("TPACodeMappingAdaptor.GetProcessName.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objTPACodeMapping = null;
            }
        }

        public bool GetTableDetails(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            TPACodeMapping objTPACodeMapping = null;
            try
            {
                objTPACodeMapping = new TPACodeMapping(connectionString, base.ClientId);
                p_objXmlOut = objTPACodeMapping.GetTableDetails(p_objXmlIn);
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("TPACodeMappingAdaptor.GetTableDetails.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objTPACodeMapping = null;
            }
        }

        public bool GetTableMapping(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            TPACodeMapping objTPACodeMapping = null; //Application layer component	
            try
            {
                objTPACodeMapping = new TPACodeMapping(connectionString, base.ClientId);
                p_objXmlOut = objTPACodeMapping.GetTableMapping(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("TPACodeMappingAdaptor.GetTPA.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objTPACodeMapping = null;
            }
        }

        public bool AddNewTableMapping(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {            
            TPACodeMapping objTPACodeMapping = null;
            string sError = string.Empty;

            try
            {
                objTPACodeMapping = new TPACodeMapping(connectionString, base.ClientId);
                objTPACodeMapping.AddNewTableMapping(p_objXmlIn, out sError);
                if (sError != string.Empty)
                {
                    p_objErrOut.Add("TPACodeMappingAdaptorTPAAlreadyExist", Globalization.GetString("TPACodeMappingAdaptorTPAAlreadyExist", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("TPACodeMappingAdaptor.TPAAddTable.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objTPACodeMapping = null;
            }
        }

        public bool DeleteTableMapping(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            TPACodeMapping objTPACodeMapping = null; //Application layer component			
            try
            {
                objTPACodeMapping = new TPACodeMapping(connectionString, base.ClientId);
                p_objXmlOut = objTPACodeMapping.DeleteTableMapping(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("TPACodeMappingAdaptor.DeleteTableMapping.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objTPACodeMapping = null;
            }
        }
        //change end here by swati


    }
}
