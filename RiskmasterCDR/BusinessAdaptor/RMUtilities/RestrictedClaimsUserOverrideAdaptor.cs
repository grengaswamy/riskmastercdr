﻿using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities; 

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
    /// <summary>
    ///Author  :   Neha Goel MITS# 33409 WWIG Gap 10
    ///Dated   :   03 March 2014
    ///Purpose :  Adaptor Functions for Restrcited claim sysytem wide users
    /// </summary>
    class RestrictedClaimsUserOverrideAdaptor : BusinessAdaptorBase
    {
        /// <summary>
        /// Constructor defination
        /// </summary>
        public RestrictedClaimsUserOverrideAdaptor() { }

        /// <summary>
        /// For fetching system wide restricted claim users
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            RestrictedClaimsUserOverride objRestrictedClaims = null;     
          
            try
            {
                objRestrictedClaims = new RestrictedClaimsUserOverride(userLogin.LoginName, userLogin.Password, userLogin.objRiskmasterDatabase.DataSourceName);
                p_objXmlOut = objRestrictedClaims.Get(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("RestrictedClaimsUserOverrideAdaptor.Get.Error"), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objRestrictedClaims = null;
                
            }
        }
       
        /// <summary>
        /// For saving system wide restricted claim users
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrout"></param>
        /// <returns></returns>
        public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrout)
        {
            RestrictedClaimsUserOverride objRestrictedClaims = null;

            try
            {
                objRestrictedClaims = new RestrictedClaimsUserOverride(userLogin.LoginName, userLogin.Password, userLogin.objRiskmasterDatabase.DataSourceName);
                p_objXmlOut = objRestrictedClaims.Save(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrout.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrout.Add(p_objException, Globalization.GetString("RestrictedClaimsUserOverrideAdaptor.Save.Error"), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objRestrictedClaims = null;
            }
        }  

    }
}
