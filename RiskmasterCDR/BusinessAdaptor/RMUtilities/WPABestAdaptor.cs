using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;  

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: WPABestAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 18-Jul-2005
	///* $Author	: Nikhil Garg
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for RM Utilities
	///	which implements the functionality of WPA Best Practice form.
	/// </summary>
	public class WPABestAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		/// <summary>
		/// Default Constructor
		/// </summary>
		public WPABestAdaptor(){}

		#endregion

		#region Public Methods


		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.WPABestAdaptor.Get() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.WPABest.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			WPABest objAuto=null; //Application layer component
			try
			{
                objAuto = new WPABest(userLogin.LoginName, userLogin.Password, userLogin.objRiskmasterDatabase.DataSourceName, base.ClientId);
				p_objXmlOut=objAuto.Get(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("WPABestAdaptor.Get.GetErr", base.ClientId), p_objException);
			}
			finally
			{
				objAuto = null;
			}
		}

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.WPABest.Save() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.WPABest.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			WPABest objAuto=null; //Application layer component
			try
			{
                objAuto = new WPABest(userLogin.LoginName, userLogin.Password, userLogin.objRiskmasterDatabase.DataSourceName, base.ClientId);
				p_objXmlOut=objAuto.Save(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("WPABestAdaptor.Save.Err", base.ClientId), p_objException);
			}
			finally
			{
				objAuto = null;
			}
		}

		#endregion
	}
}