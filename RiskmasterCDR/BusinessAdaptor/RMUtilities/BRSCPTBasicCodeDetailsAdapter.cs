using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;  

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: BRSCPTBasicCodeDetailsAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 24/05/2005
	///* $Author	: Pankaj
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for RM Utilities
	///	which implements the functionality of BRS CPT Basic Code Details.
	/// </summary>
	public class BRSCPTBasicCodeDetailsAdaptor :  BusinessAdaptorBase
	{
		#region Constructor
		/// <summary>
		/// Default Constructor
		/// </summary>
		public BRSCPTBasicCodeDetailsAdaptor(){}

		#endregion

		#region Public Methods

		/// <summary>
		/// This method is a wrapper to Riskmaster.Application.RMUtilities.BRSCPTBasicCodeDetailsAdaptor.Get() method.
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document</param>
		/// <param name="p_objXmlOut">XML containing the results.The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.BRSCPTBasicCodeDetails.xml</param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>
		/// <returns>True/False for success or failure of the function</returns>
		public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			BRSCPTBasicCodeDetails objBRSCPT=null; //Application layer component			
			try
			{
                //objBRSCPT = new BRSCPTBasicCodeDetails(connectionString,base.ClientId);
                objBRSCPT = new BRSCPTBasicCodeDetails(userLogin, connectionString, base.ClientId); //JIRA RMA-9168 nshah28(commenting above line and add "userLogin" param)
				//get the data
				p_objXmlOut=objBRSCPT.Get(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException, Globalization.GetString("BRSCPTBasicCodeDetailsAdaptor.Get.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objBRSCPT = null;
			}
		}

		/// <summary>
		/// This method is a wrapper to Riskmaster.Application.RMUtilities.BRSCPTBasicCodeDetailsAdaptor.Save() method.
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document</param>
		/// <param name="p_objXmlOut">XML containing the results.The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.BRSCPTBasicCodeDetails.xml</param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>
		/// <returns>True/False for success or failure of the function</returns>
		public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			BRSCPTBasicCodeDetails objBRSCPT=null; //Application layer component			
			try
			{
				objBRSCPT = new BRSCPTBasicCodeDetails(connectionString,base.ClientId);
				//get the data
                //Added:Yukti, MITS 34530
				//p_objXmlOut=objBRSCPT.Save(p_objXmlIn);
                p_objXmlOut = objBRSCPT.Save(p_objXmlIn, base.userLogin);
                //Ended:Yukti, MITS 34530
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException, Globalization.GetString("BRSCPTBasicCodeDetailsAdaptor.Save.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objBRSCPT = null;
			}
		}      
		#endregion
	}
}