using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;  

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: SearchWizardAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 30/05/2005
	///* $Author	: Pankaj
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for RM Utilities
	///	which implements the functionality of Search Wizard Form.
	/// </summary>
	public class SearchWizardAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		/// <summary>
		/// Default Constructor
		/// </summary>
		public SearchWizardAdaptor(){}

		#endregion

		#region Public Methods

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.SearchWizard.GetStep1() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.SearchWizard.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool GetStep1(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			SearchWizard objSrchWzrd=null; //Application layer component			
			XmlElement objElm=null;
			try
			{
                //dvatsa
				objSrchWzrd = new SearchWizard(userLogin.LoginName ,userLogin.Password ,userLogin.objRiskmasterDatabase.DataSourceName,base.ClientId);
				objElm = (XmlElement)p_objXmlIn.SelectSingleNode("//ViewID");
                p_objXmlOut=objSrchWzrd.GetStep1(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException, Globalization.GetString("SearchWizardAdaptor.GetStep1.Error",base.ClientId),
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objSrchWzrd=null;
				objElm=null;
			}
		}

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.SearchWizard.GetStep2n3() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.SearchWizard.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool GetStep2n3(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			SearchWizard objSrchWzrd=null; //Application layer component			
			try
			{
                //dvatsa
				objSrchWzrd = new SearchWizard(userLogin.LoginName ,userLogin.Password ,userLogin.objRiskmasterDatabase.DataSourceName,base.ClientId);
				p_objXmlOut=objSrchWzrd.GetStep2n3(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                //dvatsa
				p_objErrOut.Add(p_objException, Globalization.GetString("SearchWizardAdaptor.GetStep2n3.Error",base.ClientId),
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objSrchWzrd=null;
			}
		}

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.SearchWizard.GetStep4() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.SearchWizard.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool GetStep4(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			SearchWizard objSrchWzrd=null; //Application layer component			
			try
			{
                //dvatsa
				objSrchWzrd = new SearchWizard(userLogin.LoginName ,userLogin.Password ,userLogin.objRiskmasterDatabase.DataSourceName,base.ClientId);
				p_objXmlOut=objSrchWzrd.GetStep4(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                //dvatsa
				p_objErrOut.Add(p_objException, Globalization.GetString("SearchWizardAdaptor.GetStep4.Error",base.ClientId),
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objSrchWzrd=null;
			}
		}

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.SearchWizard.GetAdmTrckStep() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.SearchWizard.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool GetAdmTrckStep(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			SearchWizard objSrchWzrd=null; //Application layer component			
			try
			{
                //dvatsa
				objSrchWzrd = new SearchWizard(userLogin.LoginName ,userLogin.Password ,userLogin.objRiskmasterDatabase.DataSourceName,base.ClientId);
				p_objXmlOut=objSrchWzrd.GetAdmTrckStep(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                //dvatsa
				p_objErrOut.Add(p_objException, Globalization.GetString("SearchWizardAdaptor.GetAdmTrckStep.Error",base.ClientId),
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objSrchWzrd=null;
			}
		}

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.SearchWizard.Save() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.SearchWizard.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			SearchWizard objSrchWzrd=null; //Application layer component			
			try
			{
                //dvatsa
				objSrchWzrd = new SearchWizard(userLogin.LoginName ,userLogin.Password ,userLogin.objRiskmasterDatabase.DataSourceName,base.ClientId);
				p_objXmlOut=objSrchWzrd.Save(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                //dvatsa
				p_objErrOut.Add(p_objException, Globalization.GetString("SearchWizardAdaptor.Save.Error",base.ClientId),
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objSrchWzrd=null;
			}
		}


		#endregion
	}
}