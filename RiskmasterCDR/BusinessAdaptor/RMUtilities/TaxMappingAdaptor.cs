using System;
using System.Text;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: TaxMappingAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 24-May-2005
	///* $Author	: Parag Sarin
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	
	/// <summary>	
	///	This class is used to call the application layer component for FiscalYearsList class
	/// </summary>
	public class TaxMappingAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		public TaxMappingAdaptor(){}
		#endregion

		#region Public methods

		/// <summary>
		/// This function returns xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		/*
				/*
			<TaxMapping>
				<TaxMapValues>
					<FederalTaxes value="" stateId="" payee="" transTypeid="" payeeid="" transType="" taxPercent="" />
					<StateTax value="" payee="" payeeid="" transTypeid="" transType="" taxPercent="" stateId=""></StateTax>
				</TaxMapValues>
				<Transactions>
					<Transaction value=""></Transaction> 
					<Transaction value=""></Transaction>
				</Transactions>
				<States>
					<State value=""></State>
					<State value=""></State>
				</States>
			</TaxMapping>
			*/
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Get(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			TaxMapping objMapping=null;			
			try
			{
                objMapping = new TaxMapping(userLogin.LoginName, connectionString, userLogin.objRiskmasterDatabase.DataSourceName, userLogin.Password, base.ClientId);			//sonali //Rijul 340
				p_objXmlOut=objMapping.Get();
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("TaxMappingAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sonali
				return false;
			}
			finally
			{
				objMapping=null;
			}
		}
		/// <summary>
		///  This function deletes tax mapping information
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Input structure is as follows-:
		/*
			<TaxMapping>
				<TaxMapValues>
					<FederalTaxes value="" stateId="" payee="" transTypeid="" payeeid="" transType="" taxPercent="" />
					<StateTax value="" payee="" payeeid="" transTypeid="" transType="" taxPercent="" stateId=""></StateTax>
				</TaxMapValues>
				<Transactions>
					<Transaction value=""></Transaction> 
					<Transaction value=""></Transaction>
				</Transactions>
				<States>
					<State value=""></State>
					<State value=""></State>
				</States>
			</TaxMapping>
			*/
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool DeleteStateTaxInfo(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			TaxMapping objMapping=null;				
			try
			{
                objMapping = new TaxMapping(userLogin.LoginName, connectionString, userLogin.objRiskmasterDatabase.DataSourceName, userLogin.Password, base.ClientId);		//sonali	//Rijul 340	
				objMapping.DeleteStateTaxInfo(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("TaxMappingAdaptor.DeleteStateTaxInfo.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objMapping=null;
			}
		}
		/// <summary>
		///  This function saves tax mapping information
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Input structure is as follows-:
		/*
			<TaxMapping>
				<TaxMapValues>
					<FederalTaxes value="" stateId="" payee="" transTypeid="" payeeid="" transType="" taxPercent="" />
					<StateTax value="" payee="" payeeid="" transTypeid="" transType="" taxPercent="" stateId=""></StateTax>
				</TaxMapValues>
				<Transactions>
					<Transaction value=""></Transaction> 
					<Transaction value=""></Transaction>
				</Transactions>
				<States>
					<State value=""></State>
					<State value=""></State>
				</States>
			</TaxMapping>
			*/
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool SaveFederalTaxInfo(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			TaxMapping objMapping=null;				
			try
			{
                objMapping = new TaxMapping(userLogin.LoginName, connectionString, userLogin.objRiskmasterDatabase.DataSourceName, userLogin.Password, base.ClientId);		//sonali	//Rijul 340			
				objMapping.SaveFederalTaxInfo(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("TaxMappingAdaptor.SaveFederalTaxInfo.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objMapping=null;
			}
		}
		/// <summary>
		///  This function saves tax mapping information
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Input structure is as follows-:
		/*
			<TaxMapping>
				<TaxMapValues>
					<FederalTaxes value="" stateId="" payee="" transTypeid="" payeeid="" transType="" taxPercent="" />
					<StateTax value="" payee="" payeeid="" transTypeid="" transType="" taxPercent="" stateId=""></StateTax>
				</TaxMapValues>
				<Transactions>
					<Transaction value=""></Transaction> 
					<Transaction value=""></Transaction>
				</Transactions>
				<States>
					<State value=""></State>
					<State value=""></State>
				</States>
			</TaxMapping>
			*/
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool SaveOffsets(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			TaxMapping objMapping=null;				
			try
			{
                objMapping = new TaxMapping(userLogin.LoginName, connectionString, userLogin.objRiskmasterDatabase.DataSourceName, userLogin.Password, base.ClientId);		//sonali	//Rijul 340		
				objMapping.SaveOffsets(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("TaxMappingAdaptor.SaveFederalTaxInfo.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sonali
				return false;
			}
			finally
			{
				objMapping=null;
			}
		}
		/// <summary>
		/// This function saves tax mapping information
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Input structure is as follows-:
		/*
			<TaxMapping>
				<TaxMapValues>
					<FederalTaxes value="" stateId="" payee="" transTypeid="" payeeid="" transType="" taxPercent="" />
					<StateTax value="" payee="" payeeid="" transTypeid="" transType="" taxPercent="" stateId=""></StateTax>
				</TaxMapValues>
				<Transactions>
					<Transaction value=""></Transaction> 
					<Transaction value=""></Transaction>
				</Transactions>
				<States>
					<State value=""></State>
					<State value=""></State>
				</States>
			</TaxMapping>
			*/
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool SaveStateTaxInfo(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			TaxMapping objMapping=null;				
			try
			{
                objMapping = new TaxMapping(userLogin.LoginName, connectionString, userLogin.objRiskmasterDatabase.DataSourceName, userLogin.Password, base.ClientId);		//sonali	//Rijul 340				
				objMapping.SaveStateTaxInfo(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("TaxMappingAdaptor.SaveStateTaxInfo.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sonali
				return false;
			}
			finally
			{
				objMapping=null;
			}
		}
		#endregion

	}
}
