﻿using System;
using System.Xml;
using System.Data;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.Common;
using System.IO;
using System.Collections;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
    public class HeaderConfigAdaptor : BusinessAdaptorBase
    {

        public bool GetHeaderConfig(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            HeaderConfiguration objHeader = null;
            try
            {
                objHeader = new HeaderConfiguration(connectionString,base.userID);
                p_objXmlOut = objHeader.GetHeaderConfig(userID);
            }
            catch (Exception ex)
            {

            }
            finally
            {
                objHeader = null;
            }
            return true;
        }

        public bool SetHeaderConfig(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            HeaderConfiguration objHeader = null;
            try
            {
                objHeader = new HeaderConfiguration(connectionString,base.userID);
                objHeader.SetHeaderConfig(p_objXmlIn);
                return true;
            }
            catch(Exception ex)
            {
            }
            finally
            {
                objHeader = null;
            }
            return true;
        }

        //WWIG GAP20A - agupta298 - MITS 36804 - Start - JIRA - 4691
        /// <summary>
        /// Getting Diary Supplemental HeaderConfig from USER_PREF_XML
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool GetDiarySuppHeaderConfig(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            HeaderConfiguration objHeader = null;
            try
            {
                objHeader = new HeaderConfiguration(connectionString, base.userID);
                p_objXmlOut = objHeader.GetDiarySuppHeaderConfig(p_objXmlIn);
                return true;
            }
            catch (InvalidValueException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("HeaderConfigAdaptor.GetDiarySuppHeaderConfig.Error"), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objHeader = null;
            }
        }

        /// <summary>
        /// Saving the Diary Supplemental Header Config in USER_PREF_XML table.
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool SaveDiarySuppHeaderConfig(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            HeaderConfiguration objHeader = null;
            try
            {
                objHeader = new HeaderConfiguration(connectionString, base.userID);
                objHeader.SaveDiarySuppHeaderConfig(p_objXmlIn);
                return true;
            }
            catch (InvalidValueException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("HeaderConfigAdaptor.SaveDiarySuppHeaderConfig.Error"), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objHeader = null;
            }
        }
        //WWIG GAP20A - agupta298 - MITS 36804 - End - JIRA - 4691

    }
}
