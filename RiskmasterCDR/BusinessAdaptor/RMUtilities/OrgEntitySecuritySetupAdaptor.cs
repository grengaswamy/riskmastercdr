using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: OrgEntitySecuritySetupAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 30-May-2005
	///* $Author	: Anurag Agarwal
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	
	/// <summary>	
	///	This class is used to call the application layer component for OrgEntitySecuritySetup class  
	/// </summary>
	public class OrgEntitySecuritySetupAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		public OrgEntitySecuritySetupAdaptor()
		{
		}
		#endregion

	}
}
