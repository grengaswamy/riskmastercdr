﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Models;

namespace Riskmaster.BusinessAdaptor
{
    public class PVUpgradeAdaptor : BusinessAdaptorBase
    {
        public PVUpgradeAdaptor() { }
        public bool UpgradeXmlTagToAspxForm(PVUpgradeData request, out PVUpgradeData objReturn, ref BusinessAdaptorErrors p_objErrOut)
		{
            PowerViewUpgrade objPowerViewUpgrade = null;
            objReturn = new PVUpgradeData();
            string convertedAspx = "";
            StringBuilder ConvertedAspx = new StringBuilder(convertedAspx);
            XmlDocument ViewXml = new XmlDocument();
            string viewXml = request.viewXml;
            bool bReadOnly = request.breadonly;
            bool bTopDown = request.bTopDown;
            string divName = request.divName;
            ViewXml.LoadXml(viewXml);
			try
			{

                objPowerViewUpgrade = new PowerViewUpgrade(base.ClientId);
                objPowerViewUpgrade.UpgradeXmlTagToAspxForm(bReadOnly,bTopDown,ViewXml,divName,out ConvertedAspx);
                convertedAspx = ConvertedAspx.ToString();
                objReturn.convertedAspx = convertedAspx;
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("AssignLayoutsAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
                objPowerViewUpgrade = null;
			}
		}

        public bool FetchJurisdictionalData(PVUpgradeData request, out PVUpgradeData objReturn, ref BusinessAdaptorErrors p_objErrOut)
        {
            //Raman 12/31/2010 : R7 Performance Improvement
            //PowerViewUpgrade objPowerViewUpgrade = null;
            string jurisdictionalAspx = "";
            int stateID = request.stateID;
            objReturn = new PVUpgradeData();
            try
            {
                //Raman 12/31/2010 : R7 Performance Improvement
                //objPowerViewUpgrade = new PowerViewUpgrade();
                PowerViewUpgrade.FetchJurisdictionalData(stateID, DSNID, out jurisdictionalAspx, base.ClientId);
                objReturn.jurisdictionalAspx = jurisdictionalAspx;
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, "Error while fetching jurisdictional data.", BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                //objPowerViewUpgrade = null;
            }
        }
    }
}
