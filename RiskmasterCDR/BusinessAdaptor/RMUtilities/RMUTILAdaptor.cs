using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;  

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: RMUTILAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 27/05/2005
	///* $Author	: Pankaj
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for RM Utilities
	///	which implements the functionality of Techician.
	/// </summary>
	public class RMUTILAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		/// <summary>
		/// Default Constructor
		/// </summary>
		public RMUTILAdaptor(){}

		#endregion

		#region Public Methods

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.RMUTIL.FixSoundexValues() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">There is no xml to be returned.
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool FixSoundexValues(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, 
			ref BusinessAdaptorErrors p_objErrOut)
		{
			try
			{
                RMUTIL.FixSoundexValues(connectionString, base.ClientId);  //psharma206 jira 95
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("RMUTILAdaptor.FixSoundexValues.Error", base.ClientId), BusinessAdaptorErrorType.Error); //psharma206 jira 95
				return false;
			}
		}

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.RMUTIL.RebuildOrgHierarchy() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">There is no xml to be returned.
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool RebuildOrgHierarchy(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, 
			ref BusinessAdaptorErrors p_objErrOut)
		{
			try
			{
                RMUTIL.RebuildOrgHierarchy(connectionString, base.ClientId);  //psharma206 jira 94
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("RMUTILAdaptor.RebuildOrgHierarchy.Error", base.ClientId), BusinessAdaptorErrorType.Error); //psharma206 jira 94
				return false;
			}
		}

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.RMUTIL.RebuildGlossValues() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">There is no xml to be returned.
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool RebuildGlossValues(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, 
			ref BusinessAdaptorErrors p_objErrOut)
		{
			try
			{
				//objElm=(XmlElement)p_objXmlIn.SelectSingleNode("//FileName");
				//if(objElm==null)
				//	throw new RMAppException(Globalization.GetString("RMUTILAdaptor.RebuildGlossValues.MissingFileName"));
                RMUTIL.RebuildGlossValues(connectionString, base.ClientId);  //psharma206 jira 93
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("RMUTILAdaptor.RebuildGlossValues.Error", base.ClientId), BusinessAdaptorErrorType.Error);//psharma206 jira 93
				return false;
			}
		}
		#endregion
	}
}