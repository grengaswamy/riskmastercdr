﻿using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities; 
namespace Riskmaster.BusinessAdaptor.RMUtilities
{
    public class FundsDormancyAdaptor : BusinessAdaptorBase
    {
        /// Name			: Riskmaster.FundsDormancyAdaptor
        /// Author			: Rupal Kotak
        /// Date Created	: 05 Sep 2011		





        /// <summary>
        ///		This method is a wrapper to Riskmaster.Application.FundsDormancy.Get() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results        
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            FundsDormancy objFundsDormancy = null;

            try
            {
                objFundsDormancy = new FundsDormancy(loginName, connectionString, base.ClientId);
                p_objXmlOut = objFundsDormancy.Get(p_objXmlIn);
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("FundsDormancyAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objFundsDormancy = null;
            }
        }


        /// <summary>
        ///		This method is a wrapper to Riskmaster.Application.FundsDormancy.GetSelectedFundsDormancyInfo() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results        
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>

        public bool GetSelectedFundsDormancyInfo(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            FundsDormancy objFundsDormancy = null; //Application layer component			
            try
            {
                objFundsDormancy = new FundsDormancy(loginName, connectionString, base.ClientId);
                p_objXmlOut = objFundsDormancy.GetSelectedFundsDormancyInfo(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("FundsDormancyAdaptor.GetSelectedFundsDormancyInfo.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objFundsDormancy = null;
            }
        }

        /// <summary>
        ///		This method is a wrapper to Riskmaster.Application.FundsDormancy.Save() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results        
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            FundsDormancy objFundsDormancy = null; //Application layer component			
            try
            {
                objFundsDormancy = new FundsDormancy(loginName, connectionString, base.ClientId);
                p_objXmlOut = objFundsDormancy.Save(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("FundsDormancyAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objFundsDormancy = null;
            }
        }

        /// <summary>
        ///		This method is a wrapper to Riskmaster.Application.FundsDormancy.Delete() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results        
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool Delete(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            FundsDormancy objFundsDormancy = null; //Application layer component			
            try
            {
                objFundsDormancy = new FundsDormancy(loginName, connectionString, base.ClientId);
                p_objXmlOut = objFundsDormancy.Delete(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("FundsDormancyAdaptor.Delete.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objFundsDormancy = null;
            }
        }
    }
}
