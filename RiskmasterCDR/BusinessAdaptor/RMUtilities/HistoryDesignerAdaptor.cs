﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using System.Xml;
using Riskmaster.Common;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
    class HistoryDesignerAdaptor : BusinessAdaptorBase
    {
        /// <summary>
        /// Default Constructor
        /// </summary>
        public HistoryDesignerAdaptor() { }

        # region public methods
        public bool GetTables(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            HistoryDesigner objHD = null;
            try
            {
                objHD = new HistoryDesigner(userLogin.LoginName, connectionString, base.ClientId);
                p_objXmlOut = objHD.GetTables(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("HistoryDesignerAdaptor.GetTables.Error", base.ClientId), BusinessAdaptorErrorType.Error);                                
                return false;
            }
            finally
            {
                objHD = null; 
            }
        }
        public bool GetColumns(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            HistoryDesigner objHD = null;
            try
            {
                objHD = new HistoryDesigner(userLogin.LoginName, connectionString, base.ClientId);
                p_objXmlOut = objHD.GetColumns(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("HistoryDesignerAdaptor.GetColumns.Error", base.ClientId), BusinessAdaptorErrorType.Error);                                
                return false;
            }
            finally
            {
                objHD = null;
            }
        }
        public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            HistoryDesigner objHD = null;
            try
            {
                objHD = new HistoryDesigner(userLogin.LoginName, connectionString, base.ClientId);
                objHD.Save(p_objXmlIn);
                p_objXmlOut = objHD.GetTables(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("HistoryDesignerAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);                                
                return false;
            }
            finally
            {
                objHD = null;
            }
        }
#endregion

    }
}
