using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;  

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: WPAAutoDiaryFilterSetupAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 12/07/2005
	///* $Author	: Nikhil Garg
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for RM Utilities
	///	which implements the functionality of WPA Auto Diary Setup form.
	/// </summary>
	public class WPAAutoDiaryFilterSetupAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		/// <summary>
		/// Default Constructor
		/// </summary>
		public WPAAutoDiaryFilterSetupAdaptor(){}

		#endregion

		#region Public Methods


		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.WPAAutoDiaryFilterSetup.Get() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.WPAAutoDiaryFilterSetup.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			WPAAutoDiaryFilterSetup objAuto=null; //Application layer component
			try
			{
                //rupal:start, r8 auto diary setup enh
                //need to handle queries that require security database
                XmlNode DatabaseNode = null;
                DatabaseNode = p_objXmlIn.SelectSingleNode("WPAAutoDiaryFilterSetup/Database");
                if (DatabaseNode != null && DatabaseNode.InnerText.Equals("Security"))
                {
                    objAuto = new WPAAutoDiaryFilterSetup(securityConnectionString, base.ClientId);
                }
                else
                {
                    objAuto = new WPAAutoDiaryFilterSetup(userLogin.objRiskmasterDatabase.ConnectionString, base.ClientId);
                }
                
                //rupal:end R8 Auto Diary 
                
				p_objXmlOut=objAuto.Get(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("WPAAutoDiaryFilterSetup.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objAuto = null;
			}
		}
		#endregion
	}
}