﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using Riskmaster.Models;//abhal3 MITS 36046

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
  public  class LossCodeMappingAdaptor : BusinessAdaptorBase //abhal3 MITS 36046
    {
        public bool GetCodeType(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            LossCodeMapping objLossCodeMapping = null;
            try
            {
                objLossCodeMapping = new LossCodeMapping(connectionString);
                p_objXmlOut = objLossCodeMapping.GetCodeType(p_objXmlIn, base.userLogin);   //Aman ML Change
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("LossCodeMappingAdaptor.GetCodeType.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objLossCodeMapping = null;
            }
        }
        public bool GetLossType(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            LossCodeMapping objLossCodeMapping = null;
            try
            {
                objLossCodeMapping = new LossCodeMapping(connectionString);
                p_objXmlOut = objLossCodeMapping.GetLossType(p_objXmlIn, base.userLogin);  //Aman ML Change
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("LossCodeMappingAdaptor.GetCodeType.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objLossCodeMapping = null;
            }
        }

        public bool AddNewMapping(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            XmlElement objElement = null;
            LossCodeMapping objLossCodeMapping = null;

            try
            {
                //check existence of Policy_LOB which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//PolLob");
                if (objElement != null && (objElement.InnerText == string.Empty || objElement.InnerText == "0"))
                {
                    p_objErrOut.Add("LossCodeMappingAdaptorPolLobCodeMissing", Globalization.GetString("LossCodeMappingAdaptor.AddNewMapping.PolLobCodeMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }

                //check existence of Coverage_Type_Code which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//CvgTypeCode");
                if (objElement != null && (objElement.InnerText == string.Empty || objElement.InnerText == "0"))
                {
                    p_objErrOut.Add("LossCodeMappingAdaptorCvgTypeCodeMissing", Globalization.GetString("LossCodeMappingAdaptor.AddNewMapping.CvgTypeCodeMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }

                //check existence of Loss_Code which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//LossCode");
                if (objElement != null && (objElement.InnerText == string.Empty || objElement.InnerText == "0"))
                {
                    p_objErrOut.Add("LossCodeMappingAdaptorLossCodeMissing", Globalization.GetString("LossCodeMappingAdaptor.AddNewMapping.LossCodeMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }

               
                objLossCodeMapping = new LossCodeMapping(connectionString);
                objLossCodeMapping.AddNewMapping(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("LossCodeMappingAdaptor.Add.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objLossCodeMapping = null;
            }
        }

        public bool GetAllMapping(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            LossCodeMapping objLossCodeMapping = null;
            try
            {
                objLossCodeMapping = new LossCodeMapping(connectionString);
                p_objXmlOut = objLossCodeMapping.GetAllMapping(p_objXmlIn,base.userLogin); //Aman ML Change
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("PolicyCodeMappingAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objLossCodeMapping = null;
            }
        }

        public bool DeleteCodeMapping(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            LossCodeMapping objLossCodeMapping = null; //Application layer component			
            try
            {
                objLossCodeMapping = new LossCodeMapping(connectionString);
                objLossCodeMapping.DeleteCodeMapping(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("LossCodeMappingAdaptor.Delete.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objLossCodeMapping = null;
            }
        }

	
       
        
        public bool AddReserveNewMapping(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            XmlElement objElement = null;
            LossCodeMapping objLossCodeMapping = null;
            string sError = string.Empty;

            try
            {
                //remove '$' from reserve amount
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//ReserveAmount");
                if (objElement != null && !string.IsNullOrEmpty(objElement.InnerText))
                {
                    //objElement.InnerText = objElement.InnerText.Substring(1);
                    objElement.InnerText = objElement.InnerText.Replace(",", "");
                }

                objLossCodeMapping = new LossCodeMapping(connectionString);
                objLossCodeMapping.AddReserveNewMapping(p_objXmlIn, out sError);
                if (sError != string.Empty)
                {
                    p_objErrOut.Add("LossCodeMappingAdaptorReserveAlreadyExist", Globalization.GetString("LossCodeMappingAdaptorReserveAlreadyExist", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("LossCodeMappingAdaptor.Add.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objLossCodeMapping = null;
            }
        }

        public bool GetReserveMapping(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            LossCodeMapping objLossCodeMapping = null; //Application layer component	
            try
            {
                objLossCodeMapping = new LossCodeMapping(connectionString);
                p_objXmlOut = objLossCodeMapping.GetReserveMapping(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("LossCodeMappingAdaptor.GetReserve.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objLossCodeMapping = null;
            }
        }
		//abhal3 MITS 36046 start
        public bool ExportMapping(int iPolicySystemId,ref string sFileContent, ref BusinessAdaptorErrors p_objErrOut)
        {
            LossCodeMapping objLossCodeMapping = null;
            try
            {
                objLossCodeMapping = new LossCodeMapping(connectionString);
                sFileContent = objLossCodeMapping.ExportMapping(iPolicySystemId, base.GetSessionObject().SessionId);
                return true;
            }
                        catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objLossCodeMapping = null;
            }
        }
        public bool ImportMappingFromFile(LossCodeMappingFileContent objRequest,out LossCodeMappingOutput oResponse, ref BusinessAdaptorErrors p_objErrOut)
        {
                 oResponse= new LossCodeMappingOutput();
               LossCodeMapping objLossCodeMapping = null;
            string sFileContent=string.Empty;
            bool bTaskScheduled=false;
            try
            {
                objLossCodeMapping = new LossCodeMapping(connectionString, m_loginName, m_userLogin.objRiskmasterDatabase.DataSourceName);
                bTaskScheduled = objLossCodeMapping.ImportMapping(objRequest.FileContent, objRequest.PolicySystemIds, ref sFileContent);
                oResponse.FileContent = sFileContent;
                oResponse.TaskScheduled = bTaskScheduled;
                if (string.IsNullOrEmpty(sFileContent))
                {
                    oResponse.HasError = false;
                }
                else
                {
                    oResponse.HasError = true;
                }
                             return true;
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objLossCodeMapping = null;
            }
       
        }
        public bool RepicateMapping(ReplicateLossCodeMapping objRequest, out LossCodeMappingOutput oResponse, ref BusinessAdaptorErrors p_objErrOut)
        {
            oResponse = new LossCodeMappingOutput();
            LossCodeMapping objLossCodeMapping = null;
            string sFileName = string.Empty;
            string sFileContent = string.Empty;
            bool bTaskScheduled = false;
                  
            try
            {
                objLossCodeMapping = new LossCodeMapping(connectionString, m_loginName, m_userLogin.objRiskmasterDatabase.DataSourceName);
                bTaskScheduled = objLossCodeMapping.ReplicateMapping(Conversion.ConvertStrToInteger(objRequest.ReplicateFromPolicySystemId), objRequest.ReplicateToPolicySystemIds, ref sFileContent);
                oResponse.FileContent = sFileContent;
                oResponse.TaskScheduled = bTaskScheduled;
                if (string.IsNullOrEmpty(sFileContent))
                {
                    oResponse.HasError = false;
                }
                else
                {
                    oResponse.HasError = true;
                }
                return true;
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objLossCodeMapping = null;
                        }
        }
		//abhal3 MITS 36046 end
    }
}
