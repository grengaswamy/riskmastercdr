﻿using System;
using System.Xml;
using System.Xml.Linq;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	/// <summary>	
	///	This class is used to call the application layer component for RM Utilities
	///	which implements the functionality of Navigation Tree Customization.
	/// </summary>
    public class NavigationTreeCustomizationAdaptor:BusinessAdaptorBase
    {
        private string m_strUtilityImagesPath = string.Empty;
		/// <summary>
		/// Default Constructor
		/// </summary>
		public NavigationTreeCustomizationAdaptor()
        {
            m_strUtilityImagesPath = RMConfigurationManager.GetNameValueSectionSettings("RMUtilities", base.connectionString, base.ClientId)["RMUtilImagesPath"];
        }
		#region Public Methods
        /// <summary>
        ///		This method is a wrapper to Riskmaster.Application.RMUtilities.NavigationTreeCustomizationAdaptor.SaveCustomization() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///		The structure of the output XML document would be as in Riskmaster.Application.RMUtilities.NavigationTreeCustomizationAdaptor xml
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        /// 
        public bool SaveCustomization(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            NavigationTreeCustomization objAppNavTree = null; //Application layer component		
            try
            {
                objAppNavTree = new NavigationTreeCustomization(this.connectionString);
                objAppNavTree.SaveCustomization(XDocument.Load(p_objXmlIn.CreateNavigator().ReadSubtree()));
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("NavigationTreeCustomizationAdaptor.SaveCustomization.Error", base.ClientId),
                    BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objAppNavTree = null;
            }
        }

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.AutoAssignAdjusterCustomization.GetCustomization() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in Riskmaster.Application.RMUtilitiesAutoAssignAdjusterCustomizationxml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool GetCustomization(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
            NavigationTreeCustomization objAppNavTree = null; //Application layer component
            try
			{
                objAppNavTree = new NavigationTreeCustomization(this.connectionString);
                p_objXmlOut.Load(objAppNavTree.GetCustomization(XDocument.Load(p_objXmlIn.CreateNavigator().ReadSubtree())).CreateReader());
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("NavigationTreeCustomizationAdaptor.GetCustomization.Error", base.ClientId),
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
                objAppNavTree = null;
			}
		}

		#endregion
	}
}
