using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;  

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: QueryDesignerAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 27/05/2005
	///* $Author	: Pankaj
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for RM Utilities
	///	which implements the functionality of Query Designer Form.
	/// </summary>
	public class QueryDesignerAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		/// <summary>
		/// Default Constructor
		/// </summary>
		public QueryDesignerAdaptor(){}

		#endregion

		#region Public Methods

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.QueryDesigner.Get() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.QueryDesigner.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			QueryDesigner objQueryD=null; //Application layer component			
			try
			{
                //dvatsa
				objQueryD = new QueryDesigner(connectionString,base.ClientId);
				p_objXmlOut=objQueryD.Get(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                //dvatsa-added clientID

				p_objErrOut.Add(p_objException, Globalization.GetString("QueryDesignerAdaptor.Get.Error",base.ClientId),
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objQueryD=null;
			}
		}

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.QueryDesigner.Delete() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.QueryDesigner.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool Delete(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			QueryDesigner objQueryD=null; //Application layer component	
			try
			{
                //dvatsa
				objQueryD = new QueryDesigner(connectionString,base.ClientId);
				p_objXmlOut=objQueryD.Delete(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                //dvatsa
				p_objErrOut.Add(p_objException, Globalization.GetString("QueryDesignerAdaptor.Delete.Error",base.ClientId),
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objQueryD=null;
			}
		}
		#endregion
	}
}