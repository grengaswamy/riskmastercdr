using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
    ///************************************************************** 
    ///* $File		: WorkLossRestrictionsAdaptor.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 2nd,May 2008
    ///* $Author	: Navdeep
    ///* $Comment	: 
    ///* $Source	: 
    ///**************************************************************
    /// <summary>	
    ///  Safeway - Auto Create Work Loss/Restrictions 
    /// </summary>
    public class WorkLossRestrictionsAdaptor : BusinessAdaptorBase
    {
        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        public WorkLossRestrictionsAdaptor() { }

        #endregion

        #region Public Methods

        /// <summary>
        ///		This method is a wrapper to Riskmaster.Application.RMUtilities.WorkLossRestrictions.Get() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.WorkLossRestrictions.xml
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            WorkLossRestrictions objClmTyp = null; //Application layer component			
            try
            {
                objClmTyp = new WorkLossRestrictions(connectionString, base.ClientId);
                p_objXmlOut = objClmTyp.Get(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("WorkLossRestrictionsAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objClmTyp = null;
            }
        }
        #endregion
    }
}