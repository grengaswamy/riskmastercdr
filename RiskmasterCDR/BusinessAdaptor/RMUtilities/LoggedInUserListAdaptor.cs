using System;
using System.Text;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common; 

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: LoggedInUserListAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 24-May-2005
	///* $Author	: Parag Sarin
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	
	/// <summary>	
	///	This class is used to call the application layer component for LoggedInUserList class
	/// </summary>
	public class LoggedInUserListAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		public LoggedInUserListAdaptor(){}
		#endregion

		#region Public Function
		/// <summary>
		/// This function xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		/*
			<Sessions>
				<option email="" name="c sc (csc)" database="BROADSPIRE" phone="(   )    -    " datetime="5/5/2005 5:21 PM">11e3bba7-f659-4660-8fb4-e4f9a369e26c</option>
			</Sessions>
		*/
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Get(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			LoggedInUserList objList=null;
            string sSessionServer = string.Empty;
			try
			{

                sSessionServer = RMConfigurationSettings.GetSessionDSN(base.ClientId); //mbahl3 Jira[RMACLOUD-132]
                objList = new LoggedInUserList(sSessionServer, securityConnectionString, base.ClientId);			
				p_objXmlOut=objList.Get(AppConstants.SESSION_OBJ_USER);
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("LoggedInUserListAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error); //mbahl3 Jira[RMACLOUD-132]
				return false;
			}
			finally
			{
				objList=null;
			}
		}
		/// <summary>
		/// This function Saves Report Access data
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Input structure is as follows-:
		/*
			<Sessions>
				<option email="" name="c sc (csc)" database="BROADSPIRE" phone="(   )    -    " datetime="5/5/2005 5:21 PM">11e3bba7-f659-4660-8fb4-e4f9a369e26c</option>
			</Sessions>
		*/
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool DeleteSession(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			LoggedInUserList objList=null;
            string sSessionServer = string.Empty;
			try
			{

                sSessionServer = RMConfigurationSettings.GetSessionDSN(base.ClientId); //mbahl3 Jira[RMACLOUD-132]
                objList = new LoggedInUserList(sSessionServer, securityConnectionString, base.ClientId);			
				
				objList.DeleteSession(p_objXmlIn);
				
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("LoggedInUserListAdaptor.DeleteSession.Error", base.ClientId), BusinessAdaptorErrorType.Error); //mbahl3 Jira[RMACLOUD-132]
				return false;
			}
			finally
			{
				objList=null;
			}
		}
		#endregion
	}
}
