using System;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Common;
using Riskmaster.Application.SortMaster;
using Riskmaster.Db;//Add by kuladeep for Sortmaster mits:32377
using Riskmaster.ExceptionTypes;
using System.Collections.Generic;

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: SortMasterAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 10-Aug-2005
	///* $Author	: Neelima Dabral
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************

	/// <summary>
	/// This class contains method that is used to fetch the values related to the logged-in user 
	/// that are available as properties/members in BusinessAdaptorBase class.
	/// These values are then passed as query string to the SortMaster Web application.
	/// </summary>
	public class SortMasterAdaptor:BusinessAdaptorBase
	{
		/// Name		: SortMasterAdaptor
		/// Author		: Neelima Dabral
		/// Date Created: 10-Aug-2005	
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		///	<summary>
		/// Constructor for the Class
		/// </summary>
		public SortMasterAdaptor()
		{			
		}
        //Add by kuladeep for SortMaster Changes:Start
        private string m_netSMFormsConnString = "";
        //Add by kuladeep for SortMaster Changes:End	

		/// Name		: GetLoginDetails
		/// Author		: Neelima Dabral
		/// Date Created: 10-Aug-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This method is used to fetch the values related to the logged-in user 
		/// that are available as properties/members in BusinessAdaptorBase class.
		/// </summary>
		/// <param name="p_objDocIn">Blank XML
		///		
		///		The structure of input XML document for the first overloaded method would be:
		///		<Document>
		///			<SortMaster>		
		///			</SortMaster>
		///		</Document> 		
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document
		///		The structure of output XML document for the first overloaded method would be:		
		///			<UserLoginDetails>
		///				<UserId>3</UserId>		
		///				<DSNId>2</DSNId>		
		///				<ConnectionString>Driver={SQL Server};Server=rmserver-1;Database=SalesQA_7.0;UID=sa;PWD=;</ConnectionString>		
		///				<DocStorageType>0</DocStorageType>		
		///				<DocStoragePath>C:\RMNetUserData\ReportData</DocStoragePath>		
		///				<GroupId>1</GroupId>		
		///				<DSNName>WizardDSN</DSNName>		
		///				<FirstName>CSC</FirstName>		
		///				<LastName>CSC</LastName>		
		///				<LoginName>C</LoginName>		
		///				<Email>C@csc.com</Email>		
		///				<AdminRights>1</AdminRights>		
		///				<DBStatus>1</DBStatus>		
		///			</UserLoginDetails>		
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		/// 
		public bool GetLoginDetails (XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
		{
			XmlElement		objElement	=	null;
            string sSMNetServer = string.Empty;
            //Add by kuladeep for SortMaster mits:32377 Start
            DbWriter objWriter = null;
            int iSMUserId = 0;
            SessionManager ssMgr = base.GetSessionObject();
            //Add by kuladeep for SortMaster mits:32377 End
			try
			{
				p_objXmlOut = new XmlDocument();
 
				objElement=	   p_objXmlOut.CreateElement("UserLoginDetails");
				p_objXmlOut.AppendChild(objElement);
				objElement = null;

                // akaushik5 Commneted for MITS 38389 Starts
                //sSMNetServer = RMConfigurationManager.GetAppSetting("SMNetServer");				
                //objElement=p_objXmlOut.CreateElement("SMNetServer");								
                //objElement.InnerText=sSMNetServer;
                //p_objXmlOut.FirstChild.AppendChild(objElement);
                //objElement=null;
                // akaushik5 Commneted for MITS 38389 Ends

				objElement=p_objXmlOut.CreateElement("UserId");								
				objElement.InnerText=Convert.ToString(userID);
				p_objXmlOut.FirstChild.AppendChild(objElement);
				objElement=null;

				objElement=p_objXmlOut.CreateElement("DSNId");								
				objElement.InnerText=Convert.ToString(DSNID);
				p_objXmlOut.FirstChild.AppendChild(objElement);
				objElement=null;

				objElement=p_objXmlOut.CreateElement("ConnectionString");								
				objElement.InnerText=connectionString;
				p_objXmlOut.FirstChild.AppendChild(objElement);
				objElement=null;

				objElement=p_objXmlOut.CreateElement("DocStorageType");								
				objElement.InnerText=Convert.ToString(userLogin.objRiskmasterDatabase.DocPathType);
				p_objXmlOut.FirstChild.AppendChild(objElement);
				objElement=null;

				objElement=p_objXmlOut.CreateElement("DocStoragePath");								
				objElement.InnerText=userLogin.objRiskmasterDatabase.GlobalDocPath;
				p_objXmlOut.FirstChild.AppendChild(objElement);
				objElement=null;

				objElement=p_objXmlOut.CreateElement("GroupId");								
				objElement.InnerText=Convert.ToString(userLogin.GroupId);
				p_objXmlOut.FirstChild.AppendChild(objElement);
				objElement=null;	
				
				objElement=p_objXmlOut.CreateElement("DSNName");								
				objElement.InnerText=userLogin.objRiskmasterDatabase.DataSourceName;
				p_objXmlOut.FirstChild.AppendChild(objElement);
				objElement=null;

				objElement=p_objXmlOut.CreateElement("FirstName");								
				objElement.InnerText=userLogin.objUser.FirstName;
				p_objXmlOut.FirstChild.AppendChild(objElement);
				objElement=null;

				objElement=p_objXmlOut.CreateElement("LastName");								
				objElement.InnerText=userLogin.objUser.LastName;
				p_objXmlOut.FirstChild.AppendChild(objElement);
				objElement=null;

				objElement=p_objXmlOut.CreateElement("LoginName");								
				objElement.InnerText=userLogin.LoginName;
				p_objXmlOut.FirstChild.AppendChild(objElement);
				objElement=null;

				objElement=p_objXmlOut.CreateElement("Email");								
				objElement.InnerText=userLogin.objUser.Email;
				p_objXmlOut.FirstChild.AppendChild(objElement);
				objElement=null;
				
				objElement=p_objXmlOut.CreateElement("AdminRights");								
				if (userLogin.IsAllowedEx(100000))
					objElement.InnerText="1";
				else
					objElement.InnerText="0";
				p_objXmlOut.FirstChild.AppendChild(objElement);
				objElement=null;

				objElement=p_objXmlOut.CreateElement("DBStatus");								
				objElement.InnerText=Convert.ToString(Convert.ToInt32(userLogin.objRiskmasterDatabase.Status));
				p_objXmlOut.FirstChild.AppendChild(objElement);
				objElement=null;

                //Add by kuladeep for Sortmaster:Start---Insert entry in Session Database for Query String  mits:32377
                m_netSMFormsConnString = RMConfigurationManager.GetConnectionString("SessionDataSource", ClientId); //mbahl3 Jira[RMACLOUD-124]
                // akaushik5 Commented for MITS 37481 Starts
                //iSMUserId = Utilities.GetNextUID(connectionString, "SM_USER_DATA", base.ClientId);
                // akaushik5 Commented for MITS 37481 Ends
                objWriter = DbFactory.GetDbWriter(m_netSMFormsConnString);

                objWriter.Tables.Add("SM_USER_DATA");
                // akaushik5 Commented for MITS 37481 Starts
                //objWriter.Fields.Add("SM_ROW_ID", iSMUserId);//Used from Glossary Table(As Next Uniq Id)
                // akaushik5 Commented for MITS 37481 Ends
                objWriter.Fields.Add("SID", ssMgr.SessionId);
                objWriter.Fields.Add("USERID", userID);
                objWriter.Fields.Add("DSNID", DSNID);
                objWriter.Fields.Add("CONNECTIONSTRING", connectionString);
                objWriter.Fields.Add("DOCSTORAGETYPE", userLogin.objRiskmasterDatabase.DocPathType);
                objWriter.Fields.Add("DOCSTORAGEPATH", userLogin.objRiskmasterDatabase.GlobalDocPath);
                objWriter.Fields.Add("GROUPID", userLogin.GroupId);
                objWriter.Fields.Add("DSNNAME", userLogin.objRiskmasterDatabase.DataSourceName);
                objWriter.Fields.Add("FIRSTNAME", userLogin.objUser.FirstName);
                objWriter.Fields.Add("LASTNAME", userLogin.objUser.LastName);
                objWriter.Fields.Add("LOGINNAME", userLogin.LoginName);
                objWriter.Fields.Add("EMAIL", userLogin.objUser.Email);
                if (userLogin.IsAllowedEx(100000))
                    objWriter.Fields.Add("ADMINRIGHTS", "1");
                else
                    objWriter.Fields.Add("ADMINRIGHTS", "0");
                objWriter.Fields.Add("DBSTATUS", Convert.ToString(Convert.ToInt32(userLogin.objRiskmasterDatabase.Status)));

                // akaushik5 Changed for MITS 37481 Starts
                //objWriter.Execute();
                if (DbFactory.GetDatabaseType(m_netSMFormsConnString).Equals(eDatabaseType.DBMS_IS_ORACLE))
                {
                    iSMUserId = Convert.ToInt32(DbFactory.ExecuteScalar(m_netSMFormsConnString, "SELECT SEQ_SM_USER_DATA_ROW_ID.NEXTVAL FROM DUAL"));
                    objWriter.Fields.Add("SM_ROW_ID", iSMUserId);
                    objWriter.Execute();
                }
                else
                {
                    objWriter.Fields.Add("SM_ROW_ID", 0, System.Data.ParameterDirection.Output);
                    objWriter.JoinSQL = "SELECT @SM_ROW_ID = SCOPE_IDENTITY();";
                    Dictionary<string, object> retValues = objWriter.Execute(DbWriter.ExecuteMode.OutParam);
                    iSMUserId = Convert.ToInt32(retValues["SM_ROW_ID"]);
                }
                // akaushik5 Changed for MITS 37481 Ends

                objElement = p_objXmlOut.CreateElement("SMKeyId");
                objElement.InnerText = iSMUserId.ToString();
                p_objXmlOut.FirstChild.AppendChild(objElement);
                objElement = null;

                //Add by kuladeep for Sortmaster:End---Insert entry in Session Database for Query String  mits:32377
				
			}
			catch(Exception p_objExcepion)
			{
				throw p_objExcepion;
			}			
			finally
			{
				objElement = null;
				// objConfig  = null; //commented by Akashyap3 on 15-July-09 for Changset fix 61631 by Svaidya3
			}
			return true;

		}


        /// <summary>
        ///Author  :   Rakhel Kulavil
        ///Dated   :   15th May 2013
        ///Purpose :   This function is used to get users while posting reports.
        /// </summary>
        public bool GetUsers(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            SortMaster objSM = new SortMaster(m_userLogin, base.ClientId);
            bool sValue = objSM.GetUsers(ref p_objXmlOut);
            return true;
        }

        /// <summary>
        ///Author  :   Rakhel Kulavil
        ///Dated   :   22nd May 2013
        ///Purpose :   This function is used to save reports.
        /// </summary>
        public bool PostReports(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            XmlElement objElement = null;
            try
            {
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//Report");
                SortMaster objSM = new SortMaster(m_userLogin, base.ClientId);
                bool sValue = objSM.PostReports(p_objXmlIn);
                return true;
            }
            catch (Exception p_objExcepion)
            {
                throw p_objExcepion;
            }
        }

        /// <summary>
        ///Author  :   Rakhel Kulavil
        ///Dated   :   3rd June 2013
        ///Purpose :   This function is used to schedule osha reports.
        /// </summary>
        public bool ScheduleReports(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            XmlElement objTmpElement = null;
            SortMaster objSM = null;
            try
            {
                objSM = new SortMaster(m_userLogin, base.ClientId);
                objSM.ScheduleReports(p_objXmlIn);
                int ScheduleTypeId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//ScheduleTypeIdSM").InnerText);
                p_objXmlOut = p_objXmlIn;
                objTmpElement = (XmlElement)p_objXmlOut.SelectSingleNode("//saved");
                if (objTmpElement != null)
                {
                    objTmpElement.InnerText = "true";
                }
            }
            catch (RMAppException objException)
            {
                p_objErrOut.Add(objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception objException)
            {
                p_objErrOut.Add(objException, Globalization.GetString("Riskmaster.BusinessAdaptor.RMUtilities.TaskManagementAdaptor.SaveMonthlySettings.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objSM != null)
                    objSM = null;
            }

            return true;
        }

        /// <summary>
        ///Author  :   Rakhel Kulavil
        ///Dated   :   4th June 2013
        ///Purpose :   This function is used to get details of reports.
        /// </summary>
        public bool GetReportDetails(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            SortMaster objSM = new SortMaster(m_userLogin, base.ClientId);
            bool sValue = objSM.GetReportDetails(p_objXmlIn, ref p_objXmlOut);
            return true;
        }

        /// <summary>
        ///Author  :   Rakhel Kulavil
        ///Dated   :  5th June 2013
        ///Purpose :   This function is used to delete reports.
        /// </summary>
        public bool DeleteReports(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            SortMaster objSM = new SortMaster(m_userLogin, base.ClientId);
            bool sValue = objSM.DeleteReports(p_objXmlIn);
            return true;
        }

        /// <summary>
        ///Author  :   Jaladi Ramkumar
        ///Dated   :   31st May 2013
        ///Purpose :   This function is used to get Report XML.
        /// </summary>
        public bool GetXMLByReportID(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            SortMaster s = new SortMaster(m_userLogin, base.ClientId);
            //String ReportValue = "";
            //int ReportID = 0;

            //ReportValue = p_objXmlIn.SelectSingleNode("ReportID").InnerText;
            //ReportID = Convert.ToInt32(ReportValue);
            //bool sValue = s.GetXMLByReportID(ReportID, ref p_objXmlOut);

            bool sValue = s.GetXMLByReportID(p_objXmlIn, ref p_objXmlOut);
            return true;

        }

        /// <summary>
        ///Author  :   Rakhel Kulavil
        ///Dated   :   6th June 2013
        ///Purpose :   This function is used to get details of reports.
        /// </summary>
        public bool GetJobDetails(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            SortMaster objSM = new SortMaster(m_userLogin, base.ClientId); //mbahl3 Jira[RMACLOUD-124]
            bool sValue = objSM.GetJobDetails(p_objXmlIn, ref p_objXmlOut);
            return true;
        }

        /// <summary>
        ///Author  :   Rakhel Kulavil
        ///Dated   :   11th June 2013
        ///Purpose :   This function is used to schedule osha reports.
        /// </summary>
        public bool RunReport(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            XmlElement objTmpElement = null;
            SortMaster objSM = null;
            try
            {
                objSM = new SortMaster(m_userLogin, base.ClientId);
                objSM.RunReport(p_objXmlIn);
                p_objXmlOut = p_objXmlIn;
                objTmpElement = (XmlElement)p_objXmlOut.SelectSingleNode("//saved");
                if (objTmpElement != null)
                {
                    objTmpElement.InnerText = "true";
                }
            }
            catch (RMAppException objException)
            {
                p_objErrOut.Add(objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception objException)
            {
                p_objErrOut.Add(objException, Globalization.GetString("Riskmaster.BusinessAdaptor.RMUtilities.TaskManagementAdaptor.SaveMonthlySettings.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objSM != null)
                    objSM = null;
            }

            return true;
        }

        /// <summary>
        ///Author  :   Rakhel Kulavil
        ///Dated   :   13th June 2013
        ///Purpose :   This function is used to get details of scheduled reports.
        /// </summary>
        public bool GetScheduleDetails(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            SortMaster objSM = new SortMaster(m_userLogin, base.ClientId); //mbahl3 Jira [RMACLOUD-125]
            bool sValue = objSM.GetScheduleDetails(p_objXmlIn, ref p_objXmlOut);
            return true;
        }

        /// <summary>
        ///Author  :   Rakhel Kulavil
        ///Dated   :  13th June 2013
        ///Purpose :   This function is used to delete scheduled reports.
        /// </summary>
        public bool DeleteScheduledReports(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            SortMaster objSM = new SortMaster(m_userLogin, base.ClientId);
            bool sValue = objSM.DeleteScheduledReports(p_objXmlIn);
            return true;
        }

        /// <summary>
        ///Author  :   Rakhel Kulavil
        ///Dated   :  14th June 2013
        ///Purpose :   This function is used to delete scheduled reports.
        /// </summary>
        public bool DeleteJobs(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            SortMaster objSM = new SortMaster(m_userLogin, base.ClientId);
            bool sValue = objSM.DeleteJobs(p_objXmlIn);
            return true;
        }

        /// <summary>
        ///Author  :   Rakhel Kulavil
        ///Dated   :  14th June 2013
        ///Purpose :   This function is used to send mails.
        /// </summary>
        public bool sendMail(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            SortMaster objSM = new SortMaster(m_userLogin, base.ClientId);
            bool sValue = objSM.sendMail(p_objXmlIn);
            return true;
        }

        /// <summary>
        ///Author  :   Rakhel Kulavil
        ///Dated   :   14th June 2013
        ///Purpose :   This function is used to get users while sending job files.
        /// </summary>
        public bool GetUsersForMail(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            SortMaster objSM = new SortMaster(m_userLogin, base.ClientId);
            bool sValue = objSM.GetUsersForMail(ref p_objXmlOut);
            return true;
        }

        /// <summary>
        ///Author  :   Jaladi Ramkumar
        ///Dated   :   11th June 2013
        ///Purpose :   This function is used to get Schedule Details.
        /// </summary>
        public bool GetScheduleDetailsSM(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            SortMaster s = new SortMaster(m_userLogin, base.ClientId);
            String ScheduleValue = "";
            int ScheduleID = 0;

            ScheduleValue = p_objXmlIn.SelectSingleNode("ScheduleID").InnerText;
            ScheduleID = Convert.ToInt32(ScheduleValue);
            bool sValue = s.GetScheduleDetailsSM(ScheduleID, ref p_objXmlOut);
            return true;
        }

        /// <summary>
        ///Author  :   Jaladi Ramkumar
        ///Dated   :   18th June 2013
        ///Purpose :   This function is used to update Jobs Data.
        /// </summary>
        public bool ArchiveJob(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            SortMaster objSM = new SortMaster(m_userLogin, base.ClientId);
            bool sValue = objSM.ArchiveJob(p_objXmlIn);
            return true;
        }
        /// <summary>
        ///Author  :   Rakhel Kulavil
        ///Dated   :   18th June 2013
        ///Purpose :   This function is used to get status message from JOB_LOG table.
        /// </summary>
        public bool GetJobMessage(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            SortMaster objSM = new SortMaster(m_userLogin, base.ClientId);
            bool sValue = objSM.GetJobMessage(p_objXmlIn, ref p_objXmlOut);
            return true;
        }

        /// <summary>
        ///Author  :   Rakhel Kulavil
        ///Dated   :   18th June 2013
        ///Purpose :   This function is used to get status message from JOB_LOG table.
        /// </summary>
        public bool GetFoldersForArchive(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            SortMaster objSM = new SortMaster(m_userLogin, base.ClientId);
            bool sValue = objSM.GetFoldersForArchive(ref p_objXmlOut);
            return true;
        }

        /// <summary>
        ///Author  :   Rakhel Kulavil
        ///Dated   :   26th June 2013
        ///Purpose :   This function is used to OutputPath from JOBS table.
        /// </summary>
        public bool GetOutputPathByJobID(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            SortMaster objSM = new SortMaster(m_userLogin, base.ClientId);
            bool sValue = objSM.GetOutputPathByJobID(p_objXmlIn, ref p_objXmlOut);
            return true;
        }
    }
}
