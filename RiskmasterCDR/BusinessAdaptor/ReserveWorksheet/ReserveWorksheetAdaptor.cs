using System;
using System.Text;
using Riskmaster.Application.ReserveWorksheet;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;
using Riskmaster.Models;
namespace Riskmaster.BusinessAdaptor
{

    ///************************************************************** 
    ///* $File		: ReserveWorksheetAdaptor.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 09-09-2008
    ///* $Author	: Mohit Yadav
    ///* $Comment	: 
    ///* $Source	: 
    ///**************************************************************
    /// <summary>	
    ///	A class representing the Reserve Worksheet adaptor. It exposes various methods for retrieving
    /// and modifying the reserve amounts. 
    /// </summary>
    public class ReserveWorksheetAdaptor : BusinessAdaptorBase
    {
        #region Constructor
        /// <summary>
        /// This is the default constructor.
        /// </summary>
        public ReserveWorksheetAdaptor()
        {
        }

        #endregion

        #region Public functions Reserve Worksheet Adaptors

        public ReserveWorksheetResponse GetReserveWorksheet(ReserveWorksheetRequest worksheetRequest, ref BusinessAdaptorErrors p_objErr)
        {
            XmlDocument objXmlDocIn = null;
            XmlDocument objXmlDocOut = null;
            ReserveWorksheetFactory<ReserveWorksheetGeneric> objResWSFactory = null;
            ReserveWorksheetGeneric objGenResWS = null;
            ReserveWorksheetResponse objResponse = null;
            
            try
            {
                objXmlDocIn = new XmlDocument();
                objXmlDocIn.LoadXml(worksheetRequest.WorksheetInput);

                objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetGeneric>(base.userLogin.LoginName, base.userLogin.Password, base.userID, base.userLogin.objRiskmasterDatabase.DataSourceName, base.connectionString, base.m_securityConnectionString, base.m_DSNID, base.userLogin.objUser.ManagerId);
                objGenResWS = objResWSFactory.GetReserveWorksheet("Generic");
                
                objGenResWS.GetReserveWorksheet(objXmlDocIn,ref objXmlDocOut);

                objResponse = new ReserveWorksheetResponse();
                objResponse.WorksheetOutput = objXmlDocOut.OuterXml;
                return objResponse;
            }
            catch (RMAppException p_objException)
            {
                p_objErr.Add(p_objException, BusinessAdaptorErrorType.Error);
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                p_objErr.Add(p_objException, Globalization.GetString("ReserveWorksheetAdaptor.LoadMainData.Error"), BusinessAdaptorErrorType.Error);
                throw p_objException;
            }
            finally
            {
                objResWSFactory = null;

            }
        }

        public ReserveWorksheetResponse OnLoadInformation(ReserveWorksheetRequest worksheetRequest, ref BusinessAdaptorErrors p_objErr)
        {
            XmlDocument objXmlDocIn = null;
            XmlDocument objXmlDocOut = null;
            ReserveWorksheetFactory<ReserveWorksheetGeneric> objResWSFactory = null;
            ReserveWorksheetGeneric objGenResWS = null;
            ReserveWorksheetResponse objResponse = null;

            try
            {
                objXmlDocIn = new XmlDocument();
                objXmlDocIn.LoadXml(worksheetRequest.WorksheetInput);

                objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetGeneric>(base.userLogin.LoginName, base.userLogin.Password, base.userID, base.userLogin.objRiskmasterDatabase.DataSourceName, base.connectionString, base.m_securityConnectionString, base.m_DSNID, base.userLogin.objUser.ManagerId);
                objGenResWS = objResWSFactory.GetReserveWorksheet("Generic");

                objGenResWS.OnLoadInformation(objXmlDocIn, ref objXmlDocOut,base.userLogin);

                objResponse = new ReserveWorksheetResponse();
                objResponse.WorksheetOutput = objXmlDocOut.OuterXml;
                return objResponse;
            }
            catch (RMAppException p_objException)
            {
                p_objErr.Add(p_objException, BusinessAdaptorErrorType.Error);
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                p_objErr.Add(p_objException, Globalization.GetString("ReserveWorksheetAdaptor.LoadMainData.Error"), BusinessAdaptorErrorType.Error);
                throw p_objException;
            }
            finally
            {
                objResWSFactory = null;

            }
        }

       
        public ReserveWorksheetResponse SubmitReserveWorksheet(ReserveWorksheetRequest worksheetRequest, ref BusinessAdaptorErrors p_objErr)
        {
            XmlDocument objXmlDocIn = null;
            XmlDocument objXmlDocOut = null;
            ReserveWorksheetFactory<ReserveWorksheetGeneric> objResWSFactory = null;
            ReserveWorksheetGeneric objGenResWS = null;
            ReserveWorksheetResponse objResponse = null;
            
            try
            {
                objXmlDocIn = new XmlDocument();
                objXmlDocIn.LoadXml(worksheetRequest.WorksheetInput);

                objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetGeneric>(base.userLogin.LoginName, base.userLogin.Password, base.userID, base.userLogin.objRiskmasterDatabase.DataSourceName, base.connectionString, base.m_securityConnectionString, base.m_DSNID, base.userLogin.objUser.ManagerId);
                objGenResWS = objResWSFactory.GetReserveWorksheet("Generic");
                objGenResWS.SubmitWorksheet(objXmlDocIn,ref objXmlDocOut, base.groupID, base.userLogin);

                objResponse = new ReserveWorksheetResponse();
                objResponse.WorksheetOutput = objXmlDocOut.OuterXml;
                return objResponse;
            }
            catch (RMAppException p_objException)
            {
                p_objErr.Add(p_objException, BusinessAdaptorErrorType.Error);
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                p_objErr.Add(p_objException, Globalization.GetString("ReserveWorksheetAdaptor.LoadMainData.Error"), BusinessAdaptorErrorType.Error);
                throw p_objException;
            }
            finally
            {
                objResWSFactory = null;
            }
        }

        public ReserveWorksheetResponse SaveReserveWorksheet(ReserveWorksheetRequest worksheetRequest, ref BusinessAdaptorErrors p_objErr)
        {
            XmlDocument objXmlDocIn = null;
            XmlDocument objXmlDocOut = null;
            ReserveWorksheetFactory<ReserveWorksheetGeneric> objResWSFactory = null;
            ReserveWorksheetGeneric objGenResWS = null;
            ReserveWorksheetResponse objResponse = null;
            
            try
            {
                objXmlDocIn = new XmlDocument();
                objXmlDocIn.LoadXml(worksheetRequest.WorksheetInput);

                objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetGeneric>(base.userLogin.LoginName, base.userLogin.Password, base.userID, base.userLogin.objRiskmasterDatabase.DataSourceName, base.connectionString, base.m_securityConnectionString, base.m_DSNID, base.userLogin.objUser.ManagerId);
                objGenResWS = objResWSFactory.GetReserveWorksheet("Generic");

                objGenResWS.SaveData(objXmlDocIn,ref objXmlDocOut);

                objResponse = new ReserveWorksheetResponse();
                objResponse.WorksheetOutput = objXmlDocOut.OuterXml;
                return objResponse;
            }
            catch (RMAppException p_objException)
            {
                p_objErr.Add(p_objException, BusinessAdaptorErrorType.Error);
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                p_objErr.Add(p_objException, Globalization.GetString("ReserveWorksheetAdaptor.LoadMainData.Error"), BusinessAdaptorErrorType.Error);
                throw p_objException;
            }
            finally
            {
                objResWSFactory = null;
            }
        }

        public ReserveWorksheetResponse DeleteReserveWorksheet(ReserveWorksheetRequest worksheetRequest, ref BusinessAdaptorErrors p_objErr)
        {
            XmlDocument objXmlDocIn = null;
            XmlDocument objXmlDocOut = null;
            ReserveWorksheetFactory<ReserveWorksheetGeneric> objResWSFactory = null;
            ReserveWorksheetGeneric objGenResWS = null;
            ReserveWorksheetResponse objResponse = null;

            try
            {
                objXmlDocIn = new XmlDocument();
                objXmlDocIn.LoadXml(worksheetRequest.WorksheetInput);

                objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetGeneric>(base.userLogin.LoginName, base.userLogin.Password, base.userID, base.userLogin.objRiskmasterDatabase.DataSourceName, base.connectionString, base.m_securityConnectionString, base.m_DSNID, base.userLogin.objUser.ManagerId);
                objGenResWS = objResWSFactory.GetReserveWorksheet("Generic");

                objGenResWS.DeleteWorksheet(objXmlDocIn, ref objXmlDocOut);

                objResponse = new ReserveWorksheetResponse();
                objResponse.WorksheetOutput = objXmlDocOut.OuterXml;
                return objResponse;
            }
            catch (RMAppException p_objException)
            {
                p_objErr.Add(p_objException, BusinessAdaptorErrorType.Error);
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                p_objErr.Add(p_objException, Globalization.GetString("ReserveWorksheetAdaptor.LoadMainData.Error"), BusinessAdaptorErrorType.Error);
                throw p_objException;
            }
            finally
            {
                objResWSFactory = null;
            }
        }
         /// <summary>
        /// MGaba2:r6:Functioning of Reject Button
        /// </summary>
        /// <param name="worksheetRequest"></param>
        /// <param name="p_objErr"></param>
        /// <returns></returns>
        public ReserveWorksheetResponse RejectReserveWorksheet(ReserveWorksheetRequest worksheetRequest, ref BusinessAdaptorErrors p_objErr)
        {
            XmlDocument objXmlDocIn = null;
            XmlDocument objXmlDocOut = null;
            ReserveWorksheetFactory<ReserveWorksheetGeneric> objResWSFactory = null;
            ReserveWorksheetGeneric objGenResWS = null;
            ReserveWorksheetResponse objResponse = null;
            XmlNode objNode = null;
            string sRSWStatus = string.Empty;
            string sReservesWithinLimit = "No";
            try
            {
                objXmlDocIn = new XmlDocument();
                objXmlDocIn.LoadXml(worksheetRequest.WorksheetInput);

                objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetGeneric>(base.userLogin.LoginName, base.userLogin.Password, base.userID, base.userLogin.objRiskmasterDatabase.DataSourceName, base.connectionString, base.m_securityConnectionString, base.m_DSNID, base.userLogin.objUser.ManagerId);
                objGenResWS = objResWSFactory.GetReserveWorksheet("Generic");

                objGenResWS.RejectWorksheet(objXmlDocIn, ref objXmlDocOut, base.groupID);
                

                objResponse = new ReserveWorksheetResponse();
                objResponse.WorksheetOutput = objXmlDocOut.OuterXml;
                return objResponse;
            }
            catch (RMAppException p_objException)
            {
                p_objErr.Add(p_objException, BusinessAdaptorErrorType.Error);
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                p_objErr.Add(p_objException, Globalization.GetString("ReserveWorksheetAdaptor.RejectReserveWorksheet.Error"), BusinessAdaptorErrorType.Error);
                throw p_objException;
            }
            finally
            {
                objResWSFactory = null;
            }
            //MGaba2:r6:Functioning of Approve Button:End
        }

        /// <summary>
        /// MGaba2:r6:Functioning of Approve Button
        /// </summary>
        /// <param name="worksheetRequest"></param>
        /// <param name="p_objErr"></param>
        /// <returns></returns>
        public ReserveWorksheetResponse ApproveReserveWorksheet(ReserveWorksheetRequest worksheetRequest, ref BusinessAdaptorErrors p_objErr)
        {
            XmlDocument objXmlDocIn = null;
            XmlDocument objXmlDocOut = null;
            ReserveWorksheetFactory<ReserveWorksheetGeneric> objResWSFactory = null;
            ReserveWorksheetGeneric objGenResWS = null;
            ReserveWorksheetResponse objResponse = null;
            try
            {
                objXmlDocIn = new XmlDocument();
                objXmlDocIn.LoadXml(worksheetRequest.WorksheetInput);

                objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetGeneric>(base.userLogin.LoginName, base.userLogin.Password, base.userID, base.userLogin.objRiskmasterDatabase.DataSourceName, base.connectionString, base.m_securityConnectionString, base.m_DSNID, base.userLogin.objUser.ManagerId);
                objGenResWS = objResWSFactory.GetReserveWorksheet("Generic");

                    objGenResWS.ApproveReserveWorksheet(objXmlDocIn, ref objXmlDocOut, base.groupID, base.userLogin);

                objResponse = new ReserveWorksheetResponse();
                objResponse.WorksheetOutput = objXmlDocOut.OuterXml;
                return objResponse;
            }
            catch (RMAppException p_objException)
            {
                p_objErr.Add(p_objException, BusinessAdaptorErrorType.Error);
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                p_objErr.Add(p_objException, Globalization.GetString("ReserveWorksheetAdaptor.ApproveReserveWorksheet.Error"), BusinessAdaptorErrorType.Error);
                throw p_objException;
            }
            finally
            {
                objResWSFactory = null;
            }
            //MGaba2:r6:Functioning of Approve Button:End
        }
        /// <summary>
        /// MGaba2:r6:Functioning of Print Button
        /// </summary>
        /// <param name="worksheetRequest"></param>
        /// <param name="p_objErr"></param>
        /// <returns></returns>
        public ReserveWorksheetResponse PrintReserveWorksheet(ReserveWorksheetRequest worksheetRequest, ref BusinessAdaptorErrors p_objErr)
        {
            XmlDocument objXmlDocIn = null;
            XmlDocument objXmlDocOut = null;
            ReserveWorksheetFactory<ReserveWorksheetGeneric> objResWSFactory = null;
            ReserveWorksheetGeneric objGenResWS = null;
            ReserveWorksheetResponse objResponse = null;
            //XmlNode objNode = null;
            //int iRSWId = 0; //Reserve Worksheet ID
            //MemoryStream objMemory = null;
            //XmlElement objTempElement = null;

            try
            {
                objXmlDocIn = new XmlDocument();
                
                objXmlDocIn.LoadXml(worksheetRequest.WorksheetInput);

                objResWSFactory = new ReserveWorksheetFactory<ReserveWorksheetGeneric>(base.userLogin.LoginName, base.userLogin.Password, base.userID, base.userLogin.objRiskmasterDatabase.DataSourceName, base.connectionString, base.m_securityConnectionString, base.m_DSNID, base.userLogin.objUser.ManagerId);
                objGenResWS = objResWSFactory.GetReserveWorksheet("Generic");

                //objNode = objXmlDocIn.SelectSingleNode("//control[@name='RSWId']");
                //if (objNode != null)
                //{
                //    iRSWId = Conversion.ConvertStrToInteger(objNode.InnerText.Trim());
                //}

                //objGenResWS.PrintWorksheet(iRSWId.ToString(), out objMemory, false, "", "");
                objGenResWS.PrintWorksheet(objXmlDocIn, ref objXmlDocOut);
                //objTempElement = objXmlDocOut.CreateElement("RSW");
                //objXmlDocOut.AppendChild(objTempElement);
                //objTempElement = objXmlDocOut.CreateElement("File");
                //objTempElement.SetAttribute("Filename", Path.GetFileName(Path.GetTempFileName()).Replace(".tmp", ".pdf"));
                //objTempElement.InnerText = Convert.ToBase64String(objMemory.ToArray());
                //objXmlDocOut.FirstChild.AppendChild(objTempElement);

                objResponse = new ReserveWorksheetResponse();
                objResponse.WorksheetOutput = objXmlDocOut.OuterXml;
                return objResponse;
            }
            catch (RMAppException p_objException)
            {
                p_objErr.Add(p_objException, BusinessAdaptorErrorType.Error);
                return objResponse;
            }
            catch (Exception p_objException)
            {
                p_objErr.Add(p_objException, Globalization.GetString("ReserveWorksheetAdaptor.PrintWorksheet.Error"), BusinessAdaptorErrorType.Error);
                return objResponse;
            }
            finally
            {
                objResWSFactory = null;
            }
           
        }
        #endregion
    }
}
