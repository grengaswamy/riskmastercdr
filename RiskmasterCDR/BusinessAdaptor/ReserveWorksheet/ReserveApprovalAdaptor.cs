﻿using System;
using System.Text;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.ReserveWorksheet;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.BusinessAdaptor
{
    ///************************************************************** 
    ///* $File		: ReserveApprovalAdaptor.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 8-Sep-2008
    ///* $Author	: Vaibhav Mathur
    ///* $Comment	: 
    ///* $Source	: 
    ///**************************************************************
    ///**************************************************************
    ///* Amendment  -->
    ///  1.Date		: 7 Feb 06 
    ///    Desc		: Replacing RMO_ACCESS use, and changing params of permission
    ///				  violation exception so it can be correctly called. 
    ///    Author	: Sumit
    ///**************************************************************
    /// <summary>	
    ///	A class representing the Supervisory Approval Adaptor.
    /// </summary>
    public class ReserveApprovalAdaptor : BusinessAdaptorBase
    {
        private const int RMB_FUNDS = 9500;
        private const int RMO_FUNDS_TRANSACT = 150;
        private const int RMO_FUNDS_BNKACCT = 300;
        private const int RMO_FUNDS_PRINTCHK = 600;
        private const int RMO_FUNDS_CLEARCHK = 700;
        private const int RMO_FUNDS_VOIDCHK = 750;
        private const int RMO_FUNDS_AUTOCHK = 900;
        private const int RMO_FUNDS_APPPAYCOV = 1000;
        private const int RMO_FUNDS_SUP_APPROVE_PAYMENTS = 1100;
        private const int RMO_FUNDS_DEPOSIT = 1150;

        #region Class Constructor
        public ReserveApprovalAdaptor() { }
        #endregion

        #region Public Functions



        /// <summary>
        /// Lists funds transactions
        /// </summary>
        /// <param name="p_objXmlIn">Input parameters as xml</param>
        /// <param name="p_objXmlOut">Result as Output xml
        /// Output structure is as follows-:
        /// <ReserveApproval>
        ///		<AllTransactions>
        ///			<Transaction>
        ///				<CtlNumber></CtlNumber>
        ///				<TransDate></TransDate>
        ///				<PayeeName></PayeeName>
        ///				<ClaimNumber></ClaimNumber>
        ///				<PaymentAmount></PaymentAmount>
        ///				<User></User>
        ///				<TransactionType></TransactionType>
        ///				<SplitAmount></SplitAmount>
        ///				<FromToDate>  </FromToDate>
        ///				<TransId></TransId>
        ///				</Transaction>
        ///		</AllTransactions>
        ///	</ReserveApproval>
        /// </param>
        /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
        /// <returns>Success -True or Failure -false in execution of the function</returns>
        public bool ListFundsTransactions(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            ReserveApproval objResApprove = null;
            XmlDocument objAllTrans = null;

            try
            {
                objResApprove = new ReserveApproval(connectionString, userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, m_securityConnectionString, base.DSNID);
                objResApprove.UserId = userLogin.objUser.UserId;
                objResApprove.UserName = userLogin.LoginName;
                objResApprove.ListFundsTransactions(out objAllTrans);

                if (objAllTrans != null)
                {
                    if (objAllTrans.SelectSingleNode("//ReserveApproval/Approvals") != null)
                    {
                        if (objAllTrans.SelectSingleNode("//ReserveApproval/Approvals").InnerXml.Length > 0)
                        {
                            p_objXmlIn.SelectSingleNode("//ReserveApprovals").InnerXml = objAllTrans.SelectSingleNode("//ReserveApproval").InnerXml;
                        }
                    }
                }
                p_objXmlOut.LoadXml(p_objXmlIn.SelectSingleNode("//ReserveApproval").OuterXml.ToString());
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ReserveApprovalAdaptor.ListFundsTransactions.Error"), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objResApprove.Dispose(); // important to kill DataModelFactory
                objResApprove = null;
                objAllTrans = null;
            }
            return true;
        }
        #endregion

    }
}
