using System;
using System.Xml;
using System.IO;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.BankAccountManagement;

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: BankAccReconciliationAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 13-Sep-2005
	///* $Author	: Neelima Dabral
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	A class representing the Bank Account Management Adaptor
	/// </summary>
	public class BankAccountManagementAdaptor:BusinessAdaptorBase
	{

		#region Constructor
		/// <summary>
		/// constructor
		/// </summary>
		public BankAccountManagementAdaptor()
		{			
		}

		#endregion

		#region Public Methods

		#region "SaveNewBalanceStatementInformation(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)"
		/// Name			: SaveNewBalanceStatementInformation
		/// Author			: Neelima Dabral
		/// Date Created	: Sep-2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************	
		/// <summary>
		/// This method will save balance statement details for a bank account in the database.
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml
		///		The structure of input XML document would be:
		///		<Document>
		///		 <AccountBalance>
		///			<AccountId></AccountId>
		///			<BeginDate></BeginDate>
		///			<EndDate></EndDate>
		///			<Balance></Balance>
		///		 </AccountBalance>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool SaveNewBalanceStatementInformation(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			BankAccReconciliation	objBankAccReconciliation=null;											
			int						iAccountId=0;
			string					sBeginDate="";
			string					sEndDate="";
			double					dblBalance=0;
			XmlElement				objElement=null;

			try
			{

                objBankAccReconciliation = new BankAccReconciliation(connectionString,base.ClientId);							
				//check existence of Account Id
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//AccountId");
				if (objElement==null)
				{
					p_objErrOut.Add("BankAccAdaptorParameterMissing",Globalization.GetString("BankAccAdaptorParameterMissing",base.ClientId),BusinessAdaptorErrorType.Error);//dvatsa-cloud
					return false;
				}
				iAccountId=Conversion.ConvertStrToInteger(objElement.InnerText);

				//check existence of Statement Begin Date
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//BeginDate");
				if (objElement==null)
				{
                    p_objErrOut.Add("BankAccAdaptorParameterMissing", Globalization.GetString("BankAccAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-cloud
					return false;
				}
				sBeginDate=Conversion.GetDate(objElement.InnerText);

				//check existence of Statement End Date
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//EndDate");
				if (objElement==null)
				{
                    p_objErrOut.Add("BankAccAdaptorParameterMissing", Globalization.GetString("BankAccAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-cloud
					return false;
				}
				sEndDate=Conversion.GetDate(objElement.InnerText);				
			
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//Balance");
				if (objElement!=null)
					dblBalance = Conversion.ConvertStrToDouble(objElement.InnerText);
				

				objBankAccReconciliation.SaveNewBalanceStatementInformation(iAccountId,sBeginDate,sEndDate,dblBalance);
				return true;
				
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("BankAccountManagementAdaptor.SaveNewBalanceStatementInformation.Error", base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objBankAccReconciliation=null;
			}			
		}

		#endregion

		#region "ProcessBalanceFile(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)"
		/// Name			: ProcessBalanceFile
		/// Author			: Neelima Dabral
		/// Date Created	: Sep-2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// This method will read balance file and mark the checks as cleared.
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml
		///		The structure of input XML document would be:
		///		<Document>
		///			<AccountBalance>
		///				<AccountNoLength />
		///				<files>
		///					<file filename=""></file>
		///				</files>
		///			</AccountBalance>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool ProcessBalanceFile(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			BankAccReconciliation	objBankAccReconciliation=null;		
			int						iLayout=0;
			string					sFileName="";
			string					sOutputFileName="";
			XmlNode					objOutXmlNode = null;
			string					sFileContent="";
			XmlElement				objElement=null;
			try
			{
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//files/file");
				if (objElement != null)
				{
					sFileContent=objElement.InnerText;
					sFileName = objElement.GetAttribute("filename");					
				}
				else
				{
                    p_objErrOut.Add("BankAccAdaptorParameterMissing", Globalization.GetString("BankAccAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-cloud
					return false;
				}
			
				if(sFileName.Trim() == "")
				{
                    p_objErrOut.Add("BankAccAdaptorInvalidParameterValue", Globalization.GetString("BankAccAdaptorInvalidParameterValue", base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-cloud
					return false;
				}

				sFileName = GetFileName(sFileName);

                Riskmaster.Common.Utilities.FileTranfer(sFileContent, sFileName, base.ClientId);

				objBankAccReconciliation=new BankAccReconciliation(connectionString,base.ClientId);	//sonali	
				
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//AccountNoLength");
				if (objElement != null)
					iLayout=Conversion.ConvertStrToInteger(objElement.InnerText);
				
				
				sOutputFileName = objBankAccReconciliation.ProcessBalanceFileFromBank(iLayout,sFileName);

				//If required the file which is processed can be deleted
				//File.Delete(sFileName);
				
				objOutXmlNode=p_objXmlOut.CreateElement("BalanceReconciliationAuditReport");
				p_objXmlOut.AppendChild(objOutXmlNode);
				objOutXmlNode=p_objXmlOut.CreateElement("File");
				objOutXmlNode.InnerText=Convert.ToBase64String(FileAsByteArray(sOutputFileName));
				p_objXmlOut.FirstChild.AppendChild(objOutXmlNode);	

				// Delete the pdf file generated by application layer component
				File.Delete(sOutputFileName);
			
				return true;				
			}
			catch(FileNotFoundException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.PopupMessage);
				return false;
			}

			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("BankAccountManagementAdaptor.ProcessBalanceFile.Error", base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objBankAccReconciliation=null;
				objOutXmlNode = null;
				objElement=null;
			}	
		}		

		#endregion

		#region "ShowReport(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)"
		/// Name			: ShowReport
		/// Author			: Neelima Dabral
		/// Date Created	: Sep-2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// This method will generate the requested bank report.
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml
		///		The structure of input XML document would be:
		///		<Document>
		///			<Reports>
		///				<AccountId />
		///				<ReportSelected/>		
		///			</Reports>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool ShowReport(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			Reports			objBankReport = null;
			int				iAccountId = 0;
			string			sReportType="";
			XmlNode			objOutXmlNode = null;
			XmlElement		objElement=null;
			string			sOutputFileName="";
			try
			{
				objBankReport = new Reports(connectionString, base.ClientId);				

				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//AccountId");
				if (objElement != null)				
					iAccountId =Conversion.ConvertStrToInteger(objElement.InnerText);
				else
				{
                    p_objErrOut.Add("BankAccAdaptorParameterMissing", Globalization.GetString("BankAccAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);	//dvatsa-cloud				
					return false;
				}

				if(iAccountId == 0)
				{
                    p_objErrOut.Add("BankAccAdaptorInvalidParameterValue", Globalization.GetString("BankAccAdaptorInvalidParameterValue", base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-cloud
					return false;
				}
				
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ReportSelected");
				if (objElement != null)
					sReportType=objElement.InnerText;
				else
				{
                    p_objErrOut.Add("BankAccAdaptorParameterMissing", Globalization.GetString("BankAccAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-cloud
					return false;
				}

				if(sReportType.Trim() == "")
				{
                    p_objErrOut.Add("BankAccAdaptorInvalidParameterValue", Globalization.GetString("BankAccAdaptorInvalidParameterValue", base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-cloud
					return false;
				}

				switch(sReportType.Trim())
				{
					case "1":
						sOutputFileName = objBankReport.MasterAccountBalReport(iAccountId);
						break;
					case "2":
						sOutputFileName = objBankReport.MonthyBankReconReport(iAccountId);
						break;
					case "3":
						sOutputFileName = objBankReport.MasterAccountBalReportSpecial(iAccountId);
						break;
					case "4":
						sOutputFileName = objBankReport.MonthyBankReconReportSpecial(iAccountId);
						break;
				}				
				
				objOutXmlNode=p_objXmlOut.CreateElement("Report");
				p_objXmlOut.AppendChild(objOutXmlNode);
				objOutXmlNode=p_objXmlOut.CreateElement("File");
				objOutXmlNode.InnerText=Convert.ToBase64String(FileAsByteArray(sOutputFileName));
				p_objXmlOut.FirstChild.AppendChild(objOutXmlNode);	

				// Delete the pdf file generated by application layer component
				File.Delete(sOutputFileName);
			
				return true;				
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("BankAccountManagementAdaptor.ShowReport.Error", base.ClientId), BusinessAdaptorErrorType.Error);	//dvatsa-cloud			
				return false;
			}
			finally
			{
				objBankReport = null;
				objOutXmlNode = null;
				objElement=null;
			}	
		}		
		#endregion

		#region "LoadAccountBalanceInformation(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)"
		/// Name			: LoadAccountBalanceInformation
		/// Author			: Raman Bhatia
		/// Date Created	: Jan-2006
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************	
		/// <summary>
		/// This method will save balance statement details for a bank account in the database.
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml
		///		The structure of input XML document would be:
		///		<Document>
		///		 <AccountBalance>
		///			<AccountId></AccountId>
		///			<RecordId></RecordId>
		///		 </AccountBalance>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool LoadAccountBalanceInformation(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			BankAccReconciliation	objBankAccReconciliation=null;											
			int						iAccountId=0;
			int						iRecordId=0;
			XmlElement				objElement=null;

			try
			{
				
				objBankAccReconciliation=new BankAccReconciliation(connectionString,base.ClientId);	//sonali for jira RMACLOUD 2177					
				//check existence of Account Id
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//AccountId");
				if (objElement==null)
				{
                    p_objErrOut.Add("BankAccAdaptorParameterMissing", Globalization.GetString("BankAccAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-cloud
					return false;
				}
				iAccountId=Conversion.ConvertStrToInteger(objElement.InnerText);

				//check existence of Record Id
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//RecordId");
				if (objElement==null)
				{
                    p_objErrOut.Add("BankAccAdaptorParameterMissing", Globalization.GetString("BankAccAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-cloud
					return false;
				}
				iRecordId=Conversion.ConvertStrToInteger(objElement.InnerText);

                p_objXmlOut = objBankAccReconciliation.LoadAccountBalanceInfo(iAccountId, iRecordId);
				return true;
				
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("BankAccountManagementAdaptor.LoadAccountBalanceInformation.Error", base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objBankAccReconciliation=null;
			}			
		}

		
		#endregion

		/// Name			: LoadSubBankDisbursementAccount
		/// Author			: Neelima Dabral
		/// Date Created	: Jan-2006
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************	
		/// <summary>
		/// This method will load the details corresponding to Sub bank Disbursement Account
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml
		///		The structure of input XML document would be:
		///		<Document>		
		///			<AccountBalance>
		///				<AccountId></AccountId>
		///				<SubAccountId></SubAccountId>
		///				<RecordId></RecordId>		
		///			</AccountBalance>AccountBalance>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool LoadSubBankDisbursementAccount(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			BankAccReconciliation	objBankAccReconciliation=null;											
			int						iSubAccountId=0;
			int						iRecordId=0;
			int						iAccountId=0;
			XmlElement				objElement=null;

			try
			{
				
				objBankAccReconciliation=new BankAccReconciliation(connectionString,base.ClientId);	//sonali for jira RMACLOUD 2177				
				//check existence of Account Id
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//AccountId");
				if (objElement==null)
				{
                    p_objErrOut.Add("BankAccAdaptorParameterMissing", Globalization.GetString("BankAccAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-cloud
					return false;
				}
				iAccountId=Conversion.ConvertStrToInteger(objElement.InnerText);

				//check existence of Sub Account Id
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//SubAccountId");
				if (objElement==null)
				{
                    p_objErrOut.Add("BankAccAdaptorParameterMissing", Globalization.GetString("BankAccAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-cloud
					return false;
				}
				iSubAccountId=Conversion.ConvertStrToInteger(objElement.InnerText);

				//check existence of Record Id
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//RecordId");
				if (objElement==null)
				{
                    p_objErrOut.Add("BankAccAdaptorParameterMissing", Globalization.GetString("BankAccAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-cloud
					return false;
				}
				iRecordId=Conversion.ConvertStrToInteger(objElement.InnerText);

				p_objXmlOut = objBankAccReconciliation.LoadSubBankDisbursementAccount(iAccountId,iSubAccountId , iRecordId);
				return true;
				
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("BankAccountManagementAdaptor.LoadSubBankDisbursementAccount.Error", base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objBankAccReconciliation=null;
			}		
		}


		/// Name			: LoadMoneyMarketAccountBalance
		/// Author			: Neelima Dabral
		/// Date Created	: Jan-2006
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************	
		/// <summary>
		/// This method will load the details corresponding to Money Market Account
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml
		///		The structure of input XML document would be:
		///		<Document>		
		///			<AccountBalance>		
		///				<SubAccountId></SubAccountId>
		///				<MoneyMktAccount></MoneyMktAccount>
		///				<StartDate></StartDate>
		///				<EndDate></EndDate>
		///			</AccountBalance>AccountBalance>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool LoadMoneyMarketAccountBalance(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			BankAccReconciliation	objBankAccReconciliation=null;											
			int						iSubAccountId=0;
			int						iMoneyMktAccount=0;
			string					sStartDate="";
			string					sEndDate="";
			XmlElement				objElement=null;

			try
			{

                objBankAccReconciliation = new BankAccReconciliation(connectionString, base.ClientId);	//sonali for jira RMACLOUD 2177								
				
				//check existence of Sub Account Id
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//SubAccountId");
				if (objElement==null)
				{
                    p_objErrOut.Add("BankAccAdaptorParameterMissing", Globalization.GetString("BankAccAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-cloud
					return false;
				}
				iSubAccountId=Conversion.ConvertStrToInteger(objElement.InnerText);

				//check existence of MoneyMktAccount
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//MoneyMktAccount");
				if (objElement==null)
				{
                    p_objErrOut.Add("BankAccAdaptorParameterMissing", Globalization.GetString("BankAccAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-cloud
					return false;
				}
				iMoneyMktAccount=Conversion.ConvertStrToInteger(objElement.InnerText);

				//check existence of StartDate
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//StartDate");
				if (objElement==null)
				{
                    p_objErrOut.Add("BankAccAdaptorParameterMissing", Globalization.GetString("BankAccAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-cloud
					return false;
				}
				sStartDate=objElement.InnerText;

				//check existence of EndDate
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//EndDate");
				if (objElement==null)
				{
                    p_objErrOut.Add("BankAccAdaptorParameterMissing", Globalization.GetString("BankAccAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-cloud
					return false;
				}
				sEndDate=objElement.InnerText;


				p_objXmlOut = objBankAccReconciliation.LoadMoneyMarketAccountBalance(iSubAccountId , iMoneyMktAccount, Conversion.GetDate(sStartDate),Conversion.GetDate(sEndDate));
				return true;
				
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("BankAccountManagementAdaptor.LoadMoneyMarketAccountBalance.Error", base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objBankAccReconciliation=null;
			}		
		}


		/// Name			: GetDeposits
		/// Author			: Neelima Dabral
		/// Date Created	: Feb-2006
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************	
		/// <summary>
		/// This method will load the deposits for the selected bank account.
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml
		///		The structure of input XML document would be:
		///		<Document>		
		///			<Deposits>		
		///				<AccountId></AccountId>
		///				<SubAccountId></SubAccountId>		
		///			</Deposits>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GetDeposits(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			BankAccReconciliation	objBankAccReconciliation=null;											
			int						iAccountId=0;
			int						iSubAccountId=0;		
			XmlElement				objElement=null;

			try
			{
                objBankAccReconciliation = new BankAccReconciliation(connectionString, base.ClientId);//sonali for jira RMACLOUD 2177								

				//check existence of Account Id
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//AccountId");
				if (objElement==null)
				{
                    p_objErrOut.Add("BankAccAdaptorParameterMissing", Globalization.GetString("BankAccAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-cloud
					return false;
				}
				iAccountId=Conversion.ConvertStrToInteger(objElement.InnerText);
				
				//check existence of Sub Account Id
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//SubAccountId");
				if (objElement==null)
				{
                    p_objErrOut.Add("BankAccAdaptorParameterMissing", Globalization.GetString("BankAccAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-cloud
					return false;
				}
				iSubAccountId=Conversion.ConvertStrToInteger(objElement.InnerText);
				
				p_objXmlOut = objBankAccReconciliation.GetDeposits(iSubAccountId , iAccountId);
				return true;
				
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("BankAccountManagementAdaptor.GetDeposits.Error", base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objBankAccReconciliation=null;
			}		
		}


		/// Name			: ClearDeposits
		/// Author			: Neelima Dabral
		/// Date Created	: Feb-2006
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************	
		/// <summary>
		/// This method will clear multiple deposits.
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml
		///		The structure of input XML document would be:
		///		<Document>		
		///			<Deposits>		
		///				<DepositIds></DepositIds>		
		///			</Deposits>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool ClearDeposits(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			BankAccReconciliation	objBankAccReconciliation=null;											
			string						sDepositIds="";			
			XmlElement				objElement=null;
			try
			{
                objBankAccReconciliation = new BankAccReconciliation(connectionString, base.ClientId);//sonali for jira RMACLOUD 2177							

				//check existence of Deposit Ids
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//DepositIds");
				if (objElement==null)
				{
                    p_objErrOut.Add("BankAccAdaptorParameterMissing", Globalization.GetString("BankAccAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-cloud
					return false;
				}
				sDepositIds=objElement.InnerText;
										
				objBankAccReconciliation.ClearDeposits(sDepositIds ,loginName );
				return true;
				
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("BankAccountManagementAdaptor.ClearDeposits.Error", base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objBankAccReconciliation=null;
			}		
		}

	

		#endregion
		
		#region Private Methods

		/// <summary>
		///		This method will convert the contents of the file to the binary stream. 
		/// </summary>
		/// <param name="p_sFileName">
		///		File Name
		/// </param>
		/// <returns>Binary stream containing file contents</returns>
		private byte[] FileAsByteArray(string p_sFileName)
		{
			byte[]			arrRet  = null;
			FileStream		objFStream=null;
			BinaryReader	objBReader=null;
			try
			{
				objFStream = new FileStream(p_sFileName,FileMode.Open);
				objBReader = new BinaryReader(objFStream);
				arrRet = objBReader.ReadBytes((int)objFStream.Length);
			}
			catch(Exception p_objException)
			{
				throw new RMAppException
                    (Globalization.GetString("BankAccountManagementAdaptor.FileAsByteArray.Error", base.ClientId), p_objException);	//dvatsa-cloud					
			}
			finally
			{
				if( objFStream != null) 
				{
					objFStream.Close();
					objFStream = null;
				}
				if( objBReader != null) 
				{
					objBReader.Close();
					objBReader = null;					
				}
			}			
			return arrRet;
		}
	
		/// <summary>
		/// This method will build the file name (along with the complete path) 
		/// onto which the balance file contents passed from will be written.
		/// </summary>
		/// <param name="p_sFileName">File name passed from the UI</param>
		/// <returns>string</returns>
		private string GetFileName(string p_sFileName)
		{
			string			sStoragePath="";
			int				iLocation=0;
			string			sFileNameOnly="";
			string			sCompleteFileName="";

            sStoragePath = RMConfigurator.CombineFilePath(RMConfigurator.TempPath, "AccountBalanceFile");

			iLocation = p_sFileName.LastIndexOf(@"\");
			sFileNameOnly = p_sFileName.Substring(iLocation+1);
            sCompleteFileName = String.Format(@"{0}\{1}", sStoragePath, sFileNameOnly);			
			return sCompleteFileName;
			
		}

		#endregion

	}
}
