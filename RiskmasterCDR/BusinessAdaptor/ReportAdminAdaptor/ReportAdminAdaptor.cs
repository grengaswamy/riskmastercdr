using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Application.ReportAdmin;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using System.IO;
using Riskmaster.Security;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class ReportAdminAdaptor: BusinessAdaptorBase
	{
		public ReportAdminAdaptor()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		public bool GetScheduleReports(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.ReportAdmin.ReportAdmin objReportAdmin=null; //Application layer component
			
			try
			{
				XmlElement objElement=null;		//used for parsing the input xml
				string sReturnXml=string.Empty; //used for holding the output xml string
				string sInputXml=string.Empty;	//XML to be populated with the Report Admin data
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ReportAdmin");
				if (objElement==null)
				{
					try
					{
						p_objErrOut.Add("ReportAdminParameterMissing",Globalization.GetString("ReportAdminParameterMissing"),BusinessAdaptorErrorType.Error);
					}
					catch(Exception ex)
					{
						string ss= ex.Message;
					}
					return false;
				}
				sInputXml=objElement.InnerXml;

				objReportAdmin = new Riskmaster.Application.ReportAdmin.ReportAdmin();
				sReturnXml=objReportAdmin.GetScheduleReports(sInputXml);
				//create the output xml by including the Output parameter also
				sReturnXml="<ReturnXml><OutputResult>" + sReturnXml + "</OutputResult></ReturnXml>";
				p_objXmlOut.LoadXml(sReturnXml);
				return true;
			}
			
			catch (XmlOperationException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch (DataModelException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("ReportAdmin.GetScheduleReports.Error"),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				
			}
		}
		public bool GetQueueReports(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.ReportAdmin.ReportAdmin objReportAdmin=null; //Application layer component
			
			try
			{
				XmlElement objElement=null;		//used for parsing the input xml
				string sReturnXml=string.Empty; //used for holding the output xml string
				string sInputXml=string.Empty;	//XML to be populated with the Report Admin data
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ReportAdmin");
				if (objElement==null)
				{
					try
					{
						p_objErrOut.Add("ReportAdminParameterMissing",Globalization.GetString("ReportAdminParameterMissing"),BusinessAdaptorErrorType.Error);
					}
					catch(Exception ex)
					{
						string ss= ex.Message;
					}
					return false;
				}
				sInputXml=objElement.InnerXml;

				objReportAdmin = new Riskmaster.Application.ReportAdmin.ReportAdmin();
				sReturnXml=objReportAdmin.GetQueueReports(sInputXml,base.loginName,base.DSNID);
				//create the output xml by including the Output parameter also
				sReturnXml="<ReturnXml><OutputResult>" + sReturnXml + "</OutputResult></ReturnXml>";
				p_objXmlOut.LoadXml(sReturnXml);
				return true;
			}
			
			catch (XmlOperationException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch (DataModelException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("ReportAdmin.GetScheduleReports.Error"),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				
			}
		}
		public bool GetScheduleDetail(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.ReportAdmin.ReportAdmin objReportAdmin=null; //Application layer component
			
			try
			{
				XmlElement objElement=null;		//used for parsing the input xml
				string sReturnXml=string.Empty; //used for holding the output xml string
				string sInputXml=string.Empty;	//XML to be populated with the Report Admin data
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ReportAdmin");
				if (objElement==null)
				{
					try
					{
						p_objErrOut.Add("ReportAdminParameterMissing",Globalization.GetString("ReportAdminParameterMissing"),BusinessAdaptorErrorType.Error);
					}
					catch(Exception ex)
					{
						string ss= ex.Message;
					}
					return false;
				}
				sInputXml=objElement.InnerXml;

				objReportAdmin = new Riskmaster.Application.ReportAdmin.ReportAdmin();
				sReturnXml=objReportAdmin.GetScheduleDetail(sInputXml,base.userID);
				//create the output xml by including the Output parameter also
				sReturnXml="<ReturnXml><OutputResult>" + sReturnXml + "</OutputResult></ReturnXml>";
				p_objXmlOut.LoadXml(sReturnXml);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("ReportAdmin.GetScheduleDetail.Error"),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				
			}
		}
		public bool PostNewReport(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.ReportAdmin.ReportAdmin objReportAdmin=null; //Application layer component
			
			try
			{
				XmlElement objElement=null;		//used for parsing the input xml
				string sReturnXml=string.Empty; //used for holding the output xml string
				string sInputXml=string.Empty;	//XML to be populated with the Report Admin data
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//PostReport");
				if (objElement==null)
				{
					try
					{
						p_objErrOut.Add("ReportAdminParameterMissing",Globalization.GetString("ReportAdminParameterMissing"),BusinessAdaptorErrorType.Error);
					}
					catch(Exception ex)
					{
						string ss= ex.Message;
					}
					return false;
				}
				sInputXml=objElement.InnerXml;

				objReportAdmin = new Riskmaster.Application.ReportAdmin.ReportAdmin();
				//sReturnXml=objReportAdmin.GetReportXML(sInputXml,base.userID);
				sReturnXml=objReportAdmin.PostNewReport(sInputXml,base.m_userLogin,m_connectionString).ToString();//********************To be remove as user id is hard coded
				//create the output xml by including the Output parameter also
				sReturnXml="<ReturnXml><OutputResult>" + sReturnXml + "</OutputResult></ReturnXml>";
				p_objXmlOut.LoadXml(sReturnXml);
				return true;
			}			
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("ReportAdmin.GetReportXML.Error"),BusinessAdaptorErrorType.Error);
				return false;
			}
			
		}
		public bool GetReportXML(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.ReportAdmin.ReportAdmin objReportAdmin=null; //Application layer component
			
			try
			{
				XmlElement objElement=null;		//used for parsing the input xml
				string sReturnXml=string.Empty; //used for holding the output xml string
				string sInputXml=string.Empty;	//XML to be populated with the Report Admin data
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ReportAdmin");
				if (objElement==null)
				{
					try
					{
						p_objErrOut.Add("ReportAdminParameterMissing",Globalization.GetString("ReportAdminParameterMissing"),BusinessAdaptorErrorType.Error);
					}
					catch(Exception ex)
					{
						string ss= ex.Message;
					}
					return false;
				}
				sInputXml=objElement.InnerXml;

				objReportAdmin = new Riskmaster.Application.ReportAdmin.ReportAdmin();
				//sReturnXml=objReportAdmin.GetReportXML(sInputXml,base.userID);
				sReturnXml=objReportAdmin.GetReportXML(sInputXml,2);//********************To be remove as user id is hard coded
				//create the output xml by including the Output parameter also
				sReturnXml="<ReturnXml><OutputResult>" + sReturnXml + "</OutputResult></ReturnXml>";
				p_objXmlOut.LoadXml(sReturnXml);
				return true;
			}			
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("ReportAdmin.GetReportXML.Error"),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				
			}
		}
		public bool GetUsersList(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.ReportAdmin.ReportAdmin objReportAdmin=null; //Application layer component
			
			try
			{
				XmlElement objElement=null;		//used for parsing the input xml
				string sReturnXml=string.Empty; //used for holding the output xml string
				string sInputXml=string.Empty;	//XML to be populated with the Report Admin data
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ReportAdmin");
				if (objElement==null)
				{
					try
					{
						p_objErrOut.Add("ReportAdminParameterMissing",Globalization.GetString("ReportAdminParameterMissing"),BusinessAdaptorErrorType.Error);
					}
					catch(Exception ex)
					{
						string ss= ex.Message;
					}
					return false;
				}
				sInputXml=objElement.InnerXml;

				objReportAdmin = new Riskmaster.Application.ReportAdmin.ReportAdmin();
				sReturnXml=objReportAdmin.GetUsersList(base.DSNID);
				//create the output xml by including the Output parameter also
				sReturnXml="<ReturnXml><OutputResult>" + sReturnXml + "</OutputResult></ReturnXml>";
				p_objXmlOut.LoadXml(sReturnXml);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("ReportAdmin.GetUsersList.Error"),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				
			}
		}
		public bool DeleteReport(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.ReportAdmin.ReportAdmin objReportAdmin=null; //Application layer component
			
			try
			{
				XmlElement objElement=null;		//used for parsing the input xml
				string sReturnXml=string.Empty; //used for holding the output xml string
				bool bReturnXml=false;
				string sInputXml=string.Empty;	//XML to be populated with the Report Admin data
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ReportAdmin");
				if (objElement==null)
				{
					try
					{
						p_objErrOut.Add("ReportAdminParameterMissing",Globalization.GetString("ReportAdminParameterMissing"),BusinessAdaptorErrorType.Error);
					}
					catch(Exception ex)
					{
						string ss= ex.Message;
					}
					return false;
				}
				sInputXml=objElement.InnerXml;

				objReportAdmin = new Riskmaster.Application.ReportAdmin.ReportAdmin();
				bReturnXml=objReportAdmin.DeleteReport(sInputXml);
				//create the output xml by including the Output parameter also
				sReturnXml="<ReturnXml><OutputResult>" + bReturnXml.ToString() + "</OutputResult></ReturnXml>";
				p_objXmlOut.LoadXml(sReturnXml);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("ReportAdmin.GetUsersList.Error"),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				
			}
		}
		public bool GetXMLDetail(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.ReportAdmin.ReportAdmin objReportAdmin=null; //Application layer component
			
			try
			{
				XmlElement objElement=null;		//used for parsing the input xml
				string sReturnXml=string.Empty; //used for holding the output xml string
				string sInputXml=string.Empty;	//XML to be populated with the Report Admin data
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ReportAdmin");
				if (objElement==null)
				{
					try
					{
						p_objErrOut.Add("ReportAdminParameterMissing",Globalization.GetString("ReportAdminParameterMissing"),BusinessAdaptorErrorType.Error);
					}
					catch(Exception ex)
					{
						string ss= ex.Message;
					}
					return false;
				}
				sInputXml=objElement.InnerXml;

				objReportAdmin = new Riskmaster.Application.ReportAdmin.ReportAdmin();
				sReturnXml=objReportAdmin.GetXMLDetail(sInputXml);
				//create the output xml by including the Output parameter also
				sReturnXml="<ReturnXml><OutputResult>" + sReturnXml + "</OutputResult></ReturnXml>";
				p_objXmlOut.LoadXml(sReturnXml);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("ReportAdmin.GetXMLDetail.Error"),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				
			}
		}
		public bool GetReportInfo(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.ReportAdmin.ReportAdmin objReportAdmin=new ReportAdmin(base.connectionString); //Application layer component
			
			try
			{
			
				XmlElement objElement=null;		//used for parsing the input xml
				string sReturnXml=string.Empty; //used for holding the output xml string
				string sInputXml=string.Empty;	//XML to be populated with the Report Admin data
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ReportAdmin");
				if (objElement==null)
				{
					try
					{
						p_objErrOut.Add("ReportAdminParameterMissing",Globalization.GetString("ReportAdminParameterMissing"),BusinessAdaptorErrorType.Error);
					}
					catch(Exception ex)
					{
						string ss= ex.Message;
					}
					return false;
				}
				sInputXml=objElement.InnerXml;
				sReturnXml=objReportAdmin.GetReportInfo(sInputXml,base.userID,"");
				//create the output xml by including the Output parameter also
				sReturnXml="<ReturnXml><OutputResult>" + sReturnXml + "</OutputResult></ReturnXml>";
				p_objXmlOut.LoadXml(sReturnXml);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("ReportAdmin.GetXMLDetail.Error"),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objReportAdmin=null;
			}

		}
	}
}
