﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Configuration;
using Riskmaster.Application.AcrosoftWebserviceWrapper;
using Riskmaster.Application.DocumentManagement;
using Riskmaster.Application.FileStorage;
using Riskmaster.Application.SecurityManagement;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using Riskmaster.Models;
using Riskmaster.Settings;
using Riskmaster.Application.PaperVisionWrapper;
using Riskmaster.Db;

namespace Riskmaster.BusinessAdaptor
{
    ///************************************************************** 
    ///* $File		: DocumentManagerAdaptor.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 15-Mar-2005 
    ///* $Author	: Aditya Babbar
    ///* $Comment	: 
    ///* $Source	: 
    ///**************************************************************
    ///**************************************************************
    ///* Amendment  -->
    ///  1.Date		: 7 Feb 06 
    ///    Desc		: Replacing RMO_ACCESS use, and changing params of permission
    ///				  violation exception so it can be correctly called. 
    ///    Author	: Sumit
    ///**************************************************************
    /// <summary>	
    /// Provides access to the application layer functionalities
    /// related to documents, attachments and folders; including add,
    /// update, copy, transfer and move.
    /// </summary>
    public class DocumentManagementAdaptor : BusinessAdaptorBase
    {

        //Nikhil Garg		Dated: 27-Jan-2006
        //Permission Codes for checking for View Attachments for Claim.
        #region security codes
        private const int RMO_UD_VIEW = 1;
        private const int RMO_UD_DELETE = 32;
        private const int RMO_UD_VIEWPLAY = 33;
        private const int RMO_UD_PRINT = 34;
        private const int RMO_UD_DETACHALL = 35;
        private const int RMO_UD_EXPORT = 36;
        private const int RMO_UD_TRANSFERTO = 37;
        private const int RMO_UD_COPYTO = 38;
        private const int RMO_ATTACHMENT = 6;
        private const int RMO_ATT_VIEWPLAYPRINT = 7;
        private const int RMO_ATT_ATTACHTO = 8;
        private const int RMO_ATT_DETACHFROM = 9;
        private const int RMO_ATT_EXPORT = 10;
        //MITS 11524 Abhishek  
        private const int ORG_ATTACH_PERMISSION = 17006;
        //Abhishek -- Safeway Retrofit Entity Specification -- start
        private const int ENT_VENDOR_ATTACH_PERMISSION = 13006;
        //Abhishek -- Safeway Retrofit Entity Specification -- end
		
        #endregion

        #region Constructor
        bool m_bSoapAttachmentCachedToDisk = false;
        /// <summary>
        /// Default class constructor
        /// </summary>
        public DocumentManagementAdaptor()
        {
            //Initiate all Acrosoft settings
            //TODO: Determine if this instantiation needs to be present in multiple classes
            //TODO: or if it can be instantiated and managed in a central location
            RMConfigurationManager.GetAcrosoftSettings();
            //Mona:PaperVisionMerge : Animesh Inserted for PaperVision Enhancement 
            RMConfigurationManager.GetPaperVisionSettings();
        }

        #endregion

        #region Public Methods

        private enum ASSOCIATED_RECORD_TYPE
        {
            Claim,
            Event,
            Policy,
            User,
            Misc
        }
        
        //Mona:PaperVision Merge: Checking whether PaperVision is ON or OFF
        public bool GetPaperVisionSetting(ref DocumentType p_DocOut, ref BusinessAdaptorErrors p_objErrors)
        {
            bool bReturnValue = false;
            SysSettings objSettings = null;
         
            try
            {
                objSettings = new SysSettings(connectionString, base.ClientId);
                p_DocOut.IsAtPaperVision = (objSettings.UsePaperVisionInterface?"-1":"0");               
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException,
                    Globalization.GetString("DocumentManagementAdaptor.GetPaperVisionSetting.Error",base.ClientId),
                    BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            return bReturnValue;
        }

        //rbhatia4:R8: Use Media View Setting : July 05 2011
        public void GetDocumentManagementVendorDetails(DocumentVendorDetails p_DocIn, out DocumentVendorDetails p_DocOut, ref BusinessAdaptorErrors p_objErrors,ref bool p_bUsePaperVision)
        {
            SysSettings objSettings = null;
            p_DocOut = new DocumentVendorDetails();

            try
            {
                objSettings = new SysSettings(connectionString, base.ClientId);


				//mbahl3 mits 29236
                p_bUsePaperVision = objSettings.UsePaperVisionInterface;
			if (p_DocIn.OpenMediaView == true && objSettings.ShowMediaViewButton)
                {
                    RMConfigurationManager.GetMediaViewSettings();
                    p_DocOut.DocumentManagementVendorName = Constants.DOCUMENT_MANAGEMENT_VENDOR_NAME.MediaView.ToString();
                    p_DocOut.MediaViewDetails.MediaViewUrl = MediaViewSection.MediaViewUrl;
                    //p_DocOut.MediaViewDetails.Source = MediaViewSection.Source;
                    p_DocOut.MediaViewDetails.Enterprise = MediaViewSection.Enterprise;
                    p_DocOut.MediaViewDetails.Level = MediaViewSection.Level;
                    p_DocOut.MediaViewDetails.UserType = MediaViewSection.UserType;
                    p_DocOut.MediaViewDetails.MMAgent = MediaViewSection.MMAgent;
                    p_DocOut.MediaViewDetails.MMAgency = MediaViewSection.MMAgency;
                    p_DocOut.MediaViewDetails.CommonUser = MediaViewSection.CommonUser;
                    //Anu Tennyson : Media View different for Claim and Policy System Starts
                    switch (p_DocIn.AssociatedRecordInfo.TableName.ToUpper())
                    {
                        case "CLAIM":
                            p_DocOut.MediaViewDetails.AppConfigList = MediaViewSection.AppConfigListCLM;
                            p_DocOut.MediaViewDetails.Source = MediaViewSection.SourceCLM;
                            break;
                        case "POLICY":
                            p_DocOut.MediaViewDetails.AppConfigList = MediaViewSection.AppConfigListPOL;
                            p_DocOut.MediaViewDetails.Source = MediaViewSection.SourcePOL;
                            break;
                        case "EVENT":
                            p_DocOut.MediaViewDetails.AppConfigList = MediaViewSection.AppConfigListEvent;
                            p_DocOut.MediaViewDetails.Source = MediaViewSection.SourceEvent;
                            break;
                        case "USER":
                            p_DocOut.MediaViewDetails.AppConfigList = MediaViewSection.AppConfigListMM;
                            p_DocOut.MediaViewDetails.Source = MediaViewSection.SourceMM;
                            break;
                        default:
                            p_DocOut.MediaViewDetails.AppConfigList = MediaViewSection.AppConfigList;
                            p_DocOut.MediaViewDetails.Source = MediaViewSection.Source;
                            break;
                    }
                    //Anu Tennyson : Media View different for Claim and Policy System Ends
                    //p_DocOut.MediaViewDetails.AppConfigList = MediaViewSection.AppConfigList;

                    //Fetching associated record data

                    p_DocOut.AssociatedRecordInfo = GetRecordInfoForMediaView(p_DocIn.AssociatedRecordInfo.TableName, p_DocIn.AssociatedRecordInfo.FormName, Conversion.ConvertStrToLong(p_DocIn.AssociatedRecordInfo.RecordId));
                    return;
                
                }
                if (objSettings.UseMediaViewInterface)
                {
                    RMConfigurationManager.GetMediaViewSettings();
                    p_DocOut.DocumentManagementVendorName = Constants.DOCUMENT_MANAGEMENT_VENDOR_NAME.MediaView.ToString();
                    p_DocOut.MediaViewDetails.MediaViewUrl = MediaViewSection.MediaViewUrl;
                    //p_DocOut.MediaViewDetails.Source = MediaViewSection.Source;
                    p_DocOut.MediaViewDetails.Enterprise = MediaViewSection.Enterprise;
                    p_DocOut.MediaViewDetails.Level = MediaViewSection.Level;
                    p_DocOut.MediaViewDetails.UserType = MediaViewSection.UserType;
                    p_DocOut.MediaViewDetails.MMAgent = MediaViewSection.MMAgent;
                    p_DocOut.MediaViewDetails.MMAgency = MediaViewSection.MMAgency;
                    p_DocOut.MediaViewDetails.CommonUser = MediaViewSection.CommonUser;
                    //Anu Tennyson : Media View different for Claim and Policy System Starts
                    switch (p_DocIn.AssociatedRecordInfo.TableName.ToUpper())
                    {
                        case "CLAIM":
                            p_DocOut.MediaViewDetails.AppConfigList = MediaViewSection.AppConfigListCLM;
                            p_DocOut.MediaViewDetails.Source = MediaViewSection.SourceCLM;
                            break;
                        case "POLICY":
                            p_DocOut.MediaViewDetails.AppConfigList = MediaViewSection.AppConfigListPOL;
                            p_DocOut.MediaViewDetails.Source = MediaViewSection.SourcePOL;
                            break;
                        case "EVENT":
                            p_DocOut.MediaViewDetails.AppConfigList = MediaViewSection.AppConfigListEvent;
                            p_DocOut.MediaViewDetails.Source = MediaViewSection.SourceEvent;
                            break;
                        case "USER":
                            p_DocOut.MediaViewDetails.AppConfigList = MediaViewSection.AppConfigListMM;
                            p_DocOut.MediaViewDetails.Source = MediaViewSection.SourceMM;
                            break;
                        default:
                            p_DocOut.MediaViewDetails.AppConfigList = MediaViewSection.AppConfigList;
                            p_DocOut.MediaViewDetails.Source = MediaViewSection.Source;
                            break;
                    }
                    //Anu Tennyson : Media View different for Claim and Policy System Ends
                    //p_DocOut.MediaViewDetails.AppConfigList = MediaViewSection.AppConfigList;

                    //Fetching associated record data

                    p_DocOut.AssociatedRecordInfo = GetRecordInfoForMediaView(p_DocIn.AssociatedRecordInfo.TableName , p_DocIn.AssociatedRecordInfo.FormName , Conversion.ConvertStrToLong(p_DocIn.AssociatedRecordInfo.RecordId));
                    
                    

                }
                else if (objSettings.UsePaperVisionInterface)
                {
                    p_DocOut.DocumentManagementVendorName = Constants.DOCUMENT_MANAGEMENT_VENDOR_NAME.PaperVision.ToString();
                
                }
                else if (objSettings.UseAcrosoftInterface)
                {
                    p_DocOut.DocumentManagementVendorName = Constants.DOCUMENT_MANAGEMENT_VENDOR_NAME.Acrosoft.ToString();
                }
                else
                {
                    p_DocOut.DocumentManagementVendorName = Constants.DOCUMENT_MANAGEMENT_VENDOR_NAME.RiskmasterX.ToString();
                }


            }
            catch (Exception p_objException)
            {
                p_objErrors.Add(p_objException,
                    Globalization.GetString("DocumentManagementAdaptor.GetMediaViewSetting.Error",base.ClientId),
                    BusinessAdaptorErrorType.Error);
            
            }
            finally {
                objSettings = null;
            }
            
        }

       
        /// Name			: AddFolder
        /// Author			: Aditya Babbar
        /// Date Created	: 15-Mar-2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Wrapper to DocumentManager.AddFolder method of application layer
        /// that creates new folder record in the database table DOC_FOLDERS. 
        /// </summary>
        /// <param name="p_objXmlDocIn"> 
        /// Input Xml structure.
        ///		<AddFolder>
        ///			<ParentId>Parent FolderId of the folder to be created</ParentId>
        ///			<FolderName>Name of the folder to be created</FolderName>			
        ///		</AddFolder>	
        /// </param>
        /// <param name="p_objXmlDocOut">
        /// Method output in xml format.
        /// Structure:
        ///		<AddFolderResult>
        ///			<FolderId>FolderId of newly created folder</FolderId>		
        ///		</AddFolderResult>
        ///	</param>
        /// <param name="p_objErrors">
        ///		Collection of errors/messages
        ///	</param>
        /// <returns>
        ///		true - success
        ///		false - failure
        ///	</returns>
        public bool AddFolderNew(FolderTypeRequest p_objRequest,
             ref BusinessAdaptorErrors p_objErrors)
        {
            bool bReturnValue = false;
            DocumentManager objDocumentManager = null;

            XmlElement objXmlElement = null;
            XmlElement objRootElement = null;
            XmlNode objXmlNode = null;

            // O and -1 are valid folder id values. Hence initialization is
            // being done with -2.
            long lParentId = -2;
            long lFolderId = -2;

            string sFolderName = string.Empty;

            try
            {
                if (p_objRequest.ParentId == null || p_objRequest.ParentId == "")
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.AddFolder.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.AddFolder.MissingParentIdNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                lParentId = Conversion.ConvertStrToLong(p_objRequest.ParentId);



                if (p_objRequest.FolderName == null || p_objRequest.FolderName == "")
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.AddFolder.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.AddFolder.MissingFolderNameNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                sFolderName = p_objRequest.FolderName;

                objDocumentManager = new DocumentManager(m_userLogin,base.ClientId);
                objDocumentManager.ConnectionString = this.connectionString;
                objDocumentManager.SecurityConnectionString = this.securityConnectionString; //MITS 27764 hlv 5/23/12
                objDocumentManager.UserLoginName = this.loginName;

                objDocumentManager.AddFolder(lParentId, sFolderName, out lFolderId);

                /*objRootElement = p_objXmlDocOut.CreateElement("AddFolderResult");

                objXmlElement = p_objXmlDocOut.CreateElement("FolderId");
                objXmlElement.InnerText = lFolderId.ToString();
                objRootElement.AppendChild(objXmlElement);
                p_objXmlDocOut.AppendChild(objRootElement);*/

                bReturnValue = true;

            }
            catch (InitializationException p_objInitializationException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInitializationException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInvalidValueException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (RMAppException p_objRMAppException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objRMAppException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException,
                    Globalization.GetString("DocumentManagementAdaptor.AddFolder.Error",base.ClientId),
                    BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            finally
            {
                objXmlNode = null;
                objXmlElement = null;
                objRootElement = null;
                objDocumentManager = null;
            }

            return bReturnValue;
        }
        /// Name			: AddFolder
        /// Author			: Aditya Babbar
        /// Date Created	: 15-Mar-2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Wrapper to DocumentManager.AddFolder method of application layer
        /// that creates new folder record in the database table DOC_FOLDERS. 
        /// </summary>
        /// <param name="p_objXmlDocIn"> 
        /// Input Xml structure.
        ///		<AddFolder>
        ///			<ParentId>Parent FolderId of the folder to be created</ParentId>
        ///			<FolderName>Name of the folder to be created</FolderName>			
        ///		</AddFolder>	
        /// </param>
        /// <param name="p_objXmlDocOut">
        /// Method output in xml format.
        /// Structure:
        ///		<AddFolderResult>
        ///			<FolderId>FolderId of newly created folder</FolderId>		
        ///		</AddFolderResult>
        ///	</param>
        /// <param name="p_objErrors">
        ///		Collection of errors/messages
        ///	</param>
        /// <returns>
        ///		true - success
        ///		false - failure
        ///	</returns>
        public bool AddFolder(XmlDocument p_objXmlDocIn,
            ref XmlDocument p_objXmlDocOut,
            ref BusinessAdaptorErrors p_objErrors)
        {
            bool bReturnValue = false;
            DocumentManager objDocumentManager = null;

            XmlElement objXmlElement = null;
            XmlElement objRootElement = null;
            XmlNode objXmlNode = null;

            // O and -1 are valid folder id values. Hence initialization is
            // being done with -2.
            long lParentId = -2;
            long lFolderId = -2;

            string sFolderName = string.Empty;

            try
            {
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//AddFolder/ParentId");

                if (objXmlNode == null)
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.AddFolder.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.AddFolder.MissingParentIdNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                lParentId = Conversion.ConvertStrToLong(objXmlNode.InnerText);

                objXmlNode = null;
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//AddFolder/FolderName");

                if (objXmlNode == null)
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.AddFolder.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.AddFolder.MissingFolderNameNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                sFolderName = objXmlNode.InnerText;

                objDocumentManager = new DocumentManager(m_userLogin, base.ClientId);
                objDocumentManager.ConnectionString = this.connectionString;
                objDocumentManager.SecurityConnectionString = this.securityConnectionString; //MITS 27764 hlv 5/23/12
                objDocumentManager.UserLoginName = this.loginName;

                objDocumentManager.AddFolder(lParentId, sFolderName, out lFolderId);

                objRootElement = p_objXmlDocOut.CreateElement("AddFolderResult");

                objXmlElement = p_objXmlDocOut.CreateElement("FolderId");
                objXmlElement.InnerText = lFolderId.ToString();
                objRootElement.AppendChild(objXmlElement);
                p_objXmlDocOut.AppendChild(objRootElement);

                bReturnValue = true;

            }
            catch (InitializationException p_objInitializationException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInitializationException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInvalidValueException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (RMAppException p_objRMAppException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objRMAppException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException,
                    Globalization.GetString("DocumentManagementAdaptor.AddFolder.Error",base.ClientId),
                    BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            finally
            {
                objXmlNode = null;
                objXmlElement = null;
                objRootElement = null;
                objDocumentManager = null;
            }

            return bReturnValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_objXmlDocIn"></param>
        /// <param name="p_objXmlDocOut"></param>
        /// <param name="p_objErrors"></param>
        /// <returns></returns>
        public bool GetAttachments(XmlDocument p_objXmlDocIn,
            ref XmlDocument p_objXmlDocOut,
            ref BusinessAdaptorErrors p_objErrors)
        {
            bool bReturnValue = false;
            DocumentManager objDocumentManager = null;

            XmlElement objRootElement = null;
            XmlNode objXmlNode = null;
            XmlElement objTempElement = null;

            string sFormName = string.Empty;
            string sTableName = string.Empty;
            long lRecordId = -1;
            int iPSID = 0;
            string sAttachmentsXml = string.Empty;
            SysSettings objSettings = null;
            RMConfigurator objConfig = null;
            Acrosoft objAcrosoft = null;
            Claim objClaim = null;
            Claimant objClaimant = null;
            Defendant objDefendant = null;
            Funds objFunds = null;
            Event objEvent = null;
            DataModelFactory objDataModelFactory = null;
            PiEmployee objPIEmployee = null;
            PiPatient objPIPatient = null;
            PiDriver objPIDriver = null;
            PiWitness objPIWitness = null;
            EventXDatedText objDatedText = null;







            //skhare7
            AdjustDatedText objAdjDatedText = null;
            //DisabilityPlan objPlan = null;
            ClaimXLitigation objLitigation = null;
            Expert objExpert = null;
            ClaimAdjuster objClaimAdj = null;
            //skhare7

            Policy objPolicy = null;
            UnitXClaim objUnitXClaim = null;
            int lSortId = 0; // JP 12.1.2006: Added support for sorting in attachments mode

            try
            {
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//GetAttachments/TableName");

                if (objXmlNode == null)
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.GetAttachments.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.GetAttachments.MissingTableNameNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                sTableName = objXmlNode.InnerText;

                // JP 12-1-2006: *BEGIN* Crack out the sort id
                objXmlNode = null;

                objXmlNode = p_objXmlDocIn.SelectSingleNode("//GetAttachments/SortOrder");

                if (objXmlNode == null)
                {
                    lSortId = 0;
                }
                else
                {
                    lSortId = Conversion.ConvertStrToInteger(objXmlNode.InnerText);
                }
                // JP 12-1-2006: *END* Crack out the sort id

                objXmlNode = null;
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//GetAttachments/FormName");

                if (objXmlNode == null)
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.GetAttachments.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.GetAttachments.MissingFormNameNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                sFormName = objXmlNode.InnerText;

                objXmlNode = null;
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//GetAttachments/RecordId");

                //MITS 11524 - Org Hierarchy permission check start
                lRecordId = Conversion.ConvertStrToLong(objXmlNode.InnerText);

                if (IsOrgHierarchyType(lRecordId, sTableName))
                {
                    if (!(base.userLogin.IsAllowedEx(ORG_ATTACH_PERMISSION)))
                        throw new PermissionViolationException(RMPermissions.RMO_ATTACHMENT, ORG_ATTACH_PERMISSION);

                }
                //MITS 11524 - Org Hierarchy permission check end

                //Abhishek -- Safeway Retrofit Entity Specification -- start

                if (IsVendorType(lRecordId, sTableName))
                {
                    if (!(base.userLogin.IsAllowedEx(ENT_VENDOR_ATTACH_PERMISSION)))
                        throw new PermissionViolationException(RMPermissions.RMO_ATTACHMENT, ENT_VENDOR_ATTACH_PERMISSION);

                }
                //Abhishek -- Safeway Retrofit Entity Specification -- end
				
                if (objXmlNode == null)
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.GetAttachments.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.GetAttachments.MissingRecordIdNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                lRecordId = Conversion.ConvertStrToLong(objXmlNode.InnerText);

                //Raman Bhatia..08/3/2005
                //Check if Acrosoft Interface is enabled..if enabled then we need to bypass this code

                objSettings = new SysSettings(connectionString, base.ClientId);
                if (objSettings.UseAcrosoftInterface == false)
                {
                    //Mohit Yadav 02/24/2006 for Security Permission implementation
                    //Get psid value
                    objXmlNode = p_objXmlDocIn.SelectSingleNode("//psid");
                    if (objXmlNode != null)
                    {
                        if (objXmlNode.InnerText.Trim() != "")
                        {
                            iPSID = int.Parse(objXmlNode.InnerText);
                        }
                    }
                    objXmlNode = null;


                    //Mohit Yadav 02/24/2006 for Security Permission implementation
                    //Changed code
                    //Checking for Permissions to view attachments for this claim lob
                    int iBaseSecurityId = 0;
                    iBaseSecurityId = iPSID;

                    objDocumentManager = new DocumentManager(m_userLogin, iPSID,base.ClientId);
                    objDocumentManager.ConnectionString = this.connectionString;
                    objDocumentManager.SecurityConnectionString = this.securityConnectionString; //MITS 27764 hlv 5/23/12
                    objDocumentManager.UserLoginName = this.loginName;
                    objDocumentManager.TableName = sTableName;

                    if (sFormName.ToLower() == "funds")
                    {
                        iBaseSecurityId = UserPermissions.RMB_FUNDS_TRANSACT;
                        iPSID = iBaseSecurityId;
                        if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_ATTACHMENT))
                            throw new PermissionViolationException(RMPermissions.RMO_ATTACHMENT, iBaseSecurityId);
                    }
                    else
                    {
                        iBaseSecurityId = objDocumentManager.GetBaseSecurityIDForTable(iPSID, sTableName);
                        if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_ATTACHMENT))
                            throw new PermissionViolationException(RMPermissions.RMO_ATTACHMENT, iBaseSecurityId);
                    }
                    objDocumentManager.GetAttachments(lSortId, sFormName, lRecordId, out sAttachmentsXml);
                    objRootElement = p_objXmlDocOut.CreateElement("GetAttachmentsResult");
                    objRootElement.InnerXml = sAttachmentsXml;
                    p_objXmlDocOut.AppendChild(objRootElement);

                    // 3/12/2008 Added By Abhishek, MITS 13013

                    objTempElement = p_objXmlDocOut.CreateElement("ViewAttachment");
                    iBaseSecurityId = objDocumentManager.GetBaseSecurityIDForTable(iPSID, sTableName);
                    if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_ATT_VIEWPLAYPRINT))
                    {
                        objTempElement.InnerXml = "false";
                    }
                    else
                    {
                        objTempElement.InnerXml = "true";
                    }
                    objRootElement.AppendChild(objTempElement);
                    // 3/12/2008 end MITS 13013
                    objTempElement = p_objXmlDocOut.CreateElement("JumpToAcrosoft");
                    objTempElement.InnerXml = "false";
                    objRootElement.AppendChild(objTempElement);
                }
                else
                {
                    objRootElement = p_objXmlDocOut.CreateElement("GetAttachmentsResult");
                    p_objXmlDocOut.AppendChild(objRootElement);

						//MGaba2:Commented the following change as MCM doesnt needs it and also screen was crashing in case of MCM
                    // 3/12/2008 Added By Abhishek, MITS 13013
                    //int iBaseSecurityId = 0;
                    //objTempElement = p_objXmlDocOut.CreateElement("ViewAttachment");
                    //iBaseSecurityId = objDocumentManager.GetBaseSecurityIDForTable(iPSID, sTableName);
                    //if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_ATT_VIEWPLAYPRINT))
                    //{
                    //    objTempElement.InnerXml = "false";
                    //}
                    //else
                    //{
                    //    objTempElement.InnerXml = "true";
                    //}
                    //objRootElement.AppendChild(objTempElement);
                    // 3/12/2008 end MITS 13013
                    objTempElement = p_objXmlDocOut.CreateElement("JumpToAcrosoft");
                    objTempElement.InnerXml = "true";
                    objRootElement.AppendChild(objTempElement);

                    objTempElement = p_objXmlDocOut.CreateElement("TableName");
                    objTempElement.InnerXml = sTableName;
                    objRootElement.AppendChild(objTempElement);

                    objTempElement = p_objXmlDocOut.CreateElement("FormName");
                    objTempElement.InnerXml = sFormName;
                    objRootElement.AppendChild(objTempElement);

                    objTempElement = p_objXmlDocOut.CreateElement("RecordId");
                    objTempElement.InnerXml = lRecordId.ToString();
                    objRootElement.AppendChild(objTempElement);

                    //TO DO : Work Out implementation of other screens apart from Event and Claim

                    if (sTableName.ToLower() == "claimant")
                    {
                        objDataModelFactory = new DataModelFactory(base.userLogin,base.ClientId);
                        objClaimant = (Claimant)objDataModelFactory.GetDataModelObject("Claimant", false);
                        objClaimant.MoveTo((int)lRecordId);

                        objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                        objClaim.MoveTo(objClaimant.ClaimId);

                        objTempElement = p_objXmlDocOut.CreateElement("ClaimNumber");
                        objTempElement.InnerXml = objClaim.ClaimNumber;
                        objRootElement.AppendChild(objTempElement);

                        objTempElement = p_objXmlDocOut.CreateElement("EventNumber");
                        objTempElement.InnerXml = objClaim.EventNumber;
                        objRootElement.AppendChild(objTempElement);

                    } // if
                    else
                    if (sTableName.ToLower() == "defendant")
                    {
                        objDataModelFactory = new DataModelFactory(base.userLogin, base.ClientId);
                        objDefendant = (Defendant)objDataModelFactory.GetDataModelObject("Defendant", false);
                        objDefendant.MoveTo((int)lRecordId);

                        objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                        objClaim.MoveTo(objDefendant.ClaimId);

                        objTempElement = p_objXmlDocOut.CreateElement("ClaimNumber");
                        objTempElement.InnerXml = objClaim.ClaimNumber;
                        objRootElement.AppendChild(objTempElement);

                        objTempElement = p_objXmlDocOut.CreateElement("EventNumber");
                        objTempElement.InnerXml = objClaim.EventNumber;
                        objRootElement.AppendChild(objTempElement);

                    } // if
                    else
                    if (sTableName.ToLower() == "funds")
                    {
                        objDataModelFactory = new DataModelFactory(base.userLogin, base.ClientId);
                        objFunds = (Funds)objDataModelFactory.GetDataModelObject("Funds", false);
                        objFunds.MoveTo((int)lRecordId);

                        objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                        objClaim.MoveTo(objFunds.ClaimId);

                        objTempElement = p_objXmlDocOut.CreateElement("ClaimNumber");
                        objTempElement.InnerXml = objClaim.ClaimNumber;
                        objRootElement.AppendChild(objTempElement);

                        objTempElement = p_objXmlDocOut.CreateElement("EventNumber");
                        objTempElement.InnerXml = objClaim.EventNumber;
                        objRootElement.AppendChild(objTempElement);

                    } // if

                    //MITS 8410 : Umesh
                    else
                    if (sTableName.ToLower() == "unit")
                    {
                        objDataModelFactory = new DataModelFactory(base.userLogin, base.ClientId);
                        objUnitXClaim = (UnitXClaim)objDataModelFactory.GetDataModelObject("UnitXClaim", false);
                        objUnitXClaim.MoveTo((int)lRecordId);

                        objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                        objClaim.MoveTo(objUnitXClaim.ClaimId);

                        objTempElement = p_objXmlDocOut.CreateElement("ClaimNumber");
                        objTempElement.InnerXml = objClaim.ClaimNumber;
                        objRootElement.AppendChild(objTempElement);

                        objTempElement = p_objXmlDocOut.CreateElement("EventNumber");
                        objTempElement.InnerXml = objClaim.EventNumber;
                        objRootElement.AppendChild(objTempElement);

                    }
                    else //skhare7:MITS 22743
                        if (sTableName.ToLower() == "adjuster dated text")
                        {
                            objDataModelFactory = new DataModelFactory(base.userLogin, base.ClientId);
                            objAdjDatedText = (AdjustDatedText)objDataModelFactory.GetDataModelObject("AdjustDatedText", false);
                            objAdjDatedText.MoveTo(Convert.ToInt32(lRecordId));
                            lRecordId = Convert.ToInt64(objAdjDatedText.AdjRowId);
                            objClaimAdj = (ClaimAdjuster)objDataModelFactory.GetDataModelObject("ClaimAdjuster", false);
                            objClaimAdj.MoveTo(Convert.ToInt32(lRecordId));
                            lRecordId = Convert.ToInt64(objClaimAdj.ClaimId);
                            objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                            objClaim.MoveTo(Convert.ToInt32(lRecordId));
                            lRecordId = Convert.ToInt64(objClaim.EventId);

                            objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
                            objEvent.MoveTo(Convert.ToInt32(lRecordId));
                            objTempElement = p_objXmlDocOut.CreateElement("ClaimNumber");
                            objTempElement.InnerXml = objClaim.ClaimNumber;
                            objRootElement.AppendChild(objTempElement);

                            objTempElement = p_objXmlDocOut.CreateElement("EventNumber");
                            objTempElement.InnerXml = objEvent.EventNumber;
                            objRootElement.AppendChild(objTempElement);

                        }
                        //else if (sTableName.ToLower() == "plan")
                        //{
                        //    objDataModelFactory = new DataModelFactory(base.userLogin);
                        //    objPlan = (DisabilityPlan)objDataModelFactory.GetDataModelObject("DisabilityPlan", false);
                        //    objPlan.MoveTo(Convert.ToInt32(lRecordId));
                        //    lRecordId = Convert.ToInt64(objPlan.PlanId);

                        //    objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                        //    objClaim.MoveTo(Convert.ToInt32(lRecordId));
                        //    lRecordId = Convert.ToInt64(objClaim.EventId);

                        //    objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
                        //    objEvent.MoveTo(Convert.ToInt32(lRecordId));
                        //    objTempElement = p_objXmlDocOut.CreateElement("ClaimNumber");
                        //    objTempElement.InnerXml = objClaim.ClaimNumber;
                        //    objRootElement.AppendChild(objTempElement);

                        //    objTempElement = p_objXmlDocOut.CreateElement("EventNumber");
                        //    objTempElement.InnerXml = objEvent.EventNumber;
                        //    objRootElement.AppendChild(objTempElement);

                        //}
                        else if (sTableName.ToLower() == "expert")
                        {
                            objDataModelFactory = new DataModelFactory(base.userLogin, base.ClientId);
                            objExpert = (Expert)objDataModelFactory.GetDataModelObject("Expert", false);
                            objExpert.MoveTo(Convert.ToInt32(lRecordId));
                            lRecordId = Convert.ToInt64(objExpert.LitigationRowId);
                            objLitigation = (ClaimXLitigation)objDataModelFactory.GetDataModelObject("ClaimXLitigation", false);
                            objLitigation.MoveTo(Convert.ToInt32(lRecordId));
                            lRecordId = Convert.ToInt64(objLitigation.ClaimId);
                            objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                            objClaim.MoveTo(Convert.ToInt32(lRecordId));
                            lRecordId = Convert.ToInt64(objClaim.EventId);

                            objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
                            objEvent.MoveTo(Convert.ToInt32(lRecordId));
                            objTempElement = p_objXmlDocOut.CreateElement("ClaimNumber");
                            objTempElement.InnerXml = objClaim.ClaimNumber;
                            objRootElement.AppendChild(objTempElement);

                            objTempElement = p_objXmlDocOut.CreateElement("EventNumber");
                            objTempElement.InnerXml = objEvent.EventNumber;
                            objRootElement.AppendChild(objTempElement);

                        }
                        else if (sTableName.ToLower() == "litigation")
                        {
                            objDataModelFactory = new DataModelFactory(base.userLogin, base.ClientId);
                            objLitigation = (ClaimXLitigation)objDataModelFactory.GetDataModelObject("ClaimXLitigation", false);
                            objLitigation.MoveTo(Convert.ToInt32(lRecordId));
                            lRecordId = Convert.ToInt64(objLitigation.ClaimId);

                            objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                            objClaim.MoveTo(Convert.ToInt32(lRecordId));
                            lRecordId = Convert.ToInt64(objClaim.EventId);

                            objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
                            objEvent.MoveTo(Convert.ToInt32(lRecordId));
                            objTempElement = p_objXmlDocOut.CreateElement("ClaimNumber");
                            objTempElement.InnerXml = objClaim.ClaimNumber;
                            objRootElement.AppendChild(objTempElement);

                            objTempElement = p_objXmlDocOut.CreateElement("EventNumber");
                            objTempElement.InnerXml = objEvent.EventNumber;
                            objRootElement.AppendChild(objTempElement);

                        }
                    //skhare7:MITS 22743 End
                    //End MITS 8410 
                    objConfig = new RMConfigurator();

                    objTempElement = p_objXmlDocOut.CreateElement("AcrosoftAttachmentsTypeKey");
                    objTempElement.InnerXml = AcrosoftSection.AcrosoftAttachmentsTypeKey;
                    objRootElement.AppendChild(objTempElement);

                    objTempElement = p_objXmlDocOut.CreateElement("AcrosoftPolicyTypeKey");
                    objTempElement.InnerXml = AcrosoftSection.AcrosoftPolicyTypeKey;
                    objRootElement.AppendChild(objTempElement);

                    objTempElement = p_objXmlDocOut.CreateElement("JumpDestination");
                    objTempElement.InnerXml = "Attachments";
                    objRootElement.AppendChild(objTempElement);

                    objTempElement = p_objXmlDocOut.CreateElement("EventFolderFriendlyName");
                    objTempElement.InnerXml = AcrosoftSection.EventFolderFriendlyName;
                    objRootElement.AppendChild(objTempElement);

                    objTempElement = p_objXmlDocOut.CreateElement("ClaimFolderFriendlyName");
                    objTempElement.InnerXml = AcrosoftSection.ClaimFolderFriendlyName;
                    objRootElement.AppendChild(objTempElement);

                    objTempElement = p_objXmlDocOut.CreateElement("PolicyFolderFriendlyName");
                    objTempElement.InnerXml = AcrosoftSection.PolicyFolderFriendlyName;
                    objRootElement.AppendChild(objTempElement);

                    objTempElement = p_objXmlDocOut.CreateElement("AsAnywhereLink");
                    objTempElement.InnerXml = AcrosoftSection.AsAnywhereLink;
                    objRootElement.AppendChild(objTempElement);

                    objTempElement = p_objXmlDocOut.CreateElement("AcrosoftSkin");
                    objTempElement.InnerXml = AcrosoftSection.AcrosoftSkin;
                    objRootElement.AppendChild(objTempElement);
                    //skhare7  MITS 23131
                    objTempElement = p_objXmlDocOut.CreateElement("SearchFolderBeforeCreate");
                    objTempElement.InnerXml = AcrosoftSection.AcrosoftSkin;
                    objRootElement.AppendChild(objTempElement);
                    //skhare7  MITS 23131 End

                    string sAppExcpXml = "";
                    string sSessionID = "";
                    objAcrosoft = new Acrosoft(base.ClientId);//dvatsa-cloud
                    //rsolanki2 :  start updates for MCM mits 19200 
                    Boolean bSuccess = false;
                    string sTempAcrosoftUserId = base.userLogin.LoginName;
                    string sTempAcrosoftPassword = base.userLogin.Password;

                    if (Conversion.CastToType<Boolean>(AcrosoftSection.UseCommonAcrosoftUser, out bSuccess))
                    {
                        if (AcrosoftSection.AcroUserMapSection.UserMap[base.loginName] != null
                            && !string.IsNullOrEmpty(AcrosoftSection.AcroUserMapSection.UserMap[base.loginName].RmxUser))
                        {                            
                            sTempAcrosoftUserId = AcrosoftSection.AcroUserMapSection.UserMap[base.loginName].AcrosoftUserId;
                            sTempAcrosoftPassword = AcrosoftSection.AcroUserMapSection.UserMap[base.loginName].AcrosoftPassword;
                        }
                        else
                        {
                            sTempAcrosoftUserId = AcrosoftSection.AcrosoftUsername;
                            sTempAcrosoftPassword = AcrosoftSection.AcrosoftUserPassword;
                        }
                    }                    

                    objAcrosoft.Authenticate(sTempAcrosoftUserId, sTempAcrosoftPassword, out sSessionID, out sAppExcpXml);

                    //objAcrosoft.Authenticate(base.userLogin.LoginName, base.userLogin.Password, out sSessionID, out sAppExcpXml);

                    //rsolanki2 :  end updates for MCM mits 19200 

                    objTempElement = p_objXmlDocOut.CreateElement("sid");
                    if (sSessionID != null) objTempElement.InnerXml = sSessionID;
                    else objTempElement.InnerXml = "";
                    objRootElement.AppendChild(objTempElement);
                    // rsolanki2 : start updates for MCM mits 19200 :- replacing the Uid and pass in params being passsed to MCM web service
                    //Creating Folder in Acrosoft
                    objConfig = new RMConfigurator();
                    objDataModelFactory = new DataModelFactory(base.userLogin, base.ClientId);
                    if (Conversion.CastToType<Boolean>(AcrosoftSection.UseCommonAcrosoftUser, out bSuccess))
                    switch (sTableName.ToLower())
                    {
                        case "piemployee":
                            objPIEmployee = (PiEmployee)objDataModelFactory.GetDataModelObject("PiEmployee", false);
                            objPIEmployee.MoveTo(Convert.ToInt32(lRecordId));
                            lRecordId = Convert.ToInt64(objPIEmployee.EventId);
                            objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
                            objEvent.MoveTo(Convert.ToInt32(lRecordId));
                            objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword, 
                                AcrosoftSection.AcrosoftAttachmentsTypeKey, objEvent.EventNumber, "", 
                                AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                            break;
                        case "pipatient":
                            objPIPatient = (PiPatient)objDataModelFactory.GetDataModelObject("PiPatient", false);
                            objPIPatient.MoveTo(Convert.ToInt32(lRecordId));
                            lRecordId = Convert.ToInt64(objPIPatient.EventId);
                            objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
                            objEvent.MoveTo(Convert.ToInt32(lRecordId));
                            objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                AcrosoftSection.AcrosoftAttachmentsTypeKey, objEvent.EventNumber, "", 
                                AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                            break;
                            //Aman Driver Enh
                        case "pidriver":
                            objPIDriver = (PiDriver)objDataModelFactory.GetDataModelObject("PiDriver", false);
                            objPIDriver.MoveTo(Convert.ToInt32(lRecordId));
                            lRecordId = Convert.ToInt64(objPIDriver.EventId);
                            objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
                            objEvent.MoveTo(Convert.ToInt32(lRecordId));
                            objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                AcrosoftSection.AcrosoftAttachmentsTypeKey, objEvent.EventNumber, "",
                                AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                            break;
                        //Aman Driver Enh
                        case "piwitness":
                            objPIWitness = (PiWitness)objDataModelFactory.GetDataModelObject("PiWitness", false);
                            objPIWitness.MoveTo(Convert.ToInt32(lRecordId));
                            lRecordId = Convert.ToInt64(objPIWitness.EventId);
                            objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
                            objEvent.MoveTo(Convert.ToInt32(lRecordId));
                            objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                AcrosoftSection.AcrosoftAttachmentsTypeKey, objEvent.EventNumber, 
                                string.Empty, AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                            break;
                        case "eventdatedtext":
                            objDatedText = (EventXDatedText)objDataModelFactory.GetDataModelObject("EventXDatedText", false);
                            objDatedText.MoveTo(Convert.ToInt32(lRecordId));
                            lRecordId = Convert.ToInt64(objDatedText.EventId);
                            objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
                            objEvent.MoveTo(Convert.ToInt32(lRecordId));
                            objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword, 
                                AcrosoftSection.AcrosoftAttachmentsTypeKey, objEvent.EventNumber, string.Empty, 
                                AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                            break;
                            //skhare7
                      
                        case "event":
                            objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
                            objEvent.MoveTo(Convert.ToInt32(lRecordId));
                            objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                AcrosoftSection.AcrosoftAttachmentsTypeKey, objEvent.EventNumber, "",AcrosoftSection.EventFolderFriendlyName,
                                AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                            break;
                            //skhare7:MITS 22743
                        case "claimant":
                        case "defendant":
                        case "funds":
                        case "unit":
                        //case "plan":
                        case "litigation":
                        case "expert":
                        case "adjuster dated text":
                            objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                AcrosoftSection.AcrosoftAttachmentsTypeKey, p_objXmlDocOut.SelectSingleNode("//EventNumber").InnerText, 
                                p_objXmlDocOut.SelectSingleNode("//ClaimNumber").InnerText, 
                                AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                            break;
                        //skhare7:MITS 22743 End
                        case "claim":
                            objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                            objClaim.MoveTo(Convert.ToInt32(lRecordId));
                            objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                AcrosoftSection.AcrosoftAttachmentsTypeKey, objClaim.EventNumber, objClaim.ClaimNumber, 
                                AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                            break;
                        case "policy":
                            objPolicy = (Policy)objDataModelFactory.GetDataModelObject("Policy", false);
                            objPolicy.MoveTo(Convert.ToInt32(lRecordId));
                            objAcrosoft.CreatePolicyFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                AcrosoftSection.AcrosoftPolicyTypeKey, objPolicy.PolicyName, 
                                AcrosoftSection.PolicyFolderFriendlyName, out sAppExcpXml);
                            break;

                        default:

                            break;
                    } // switch
                    //rsolanki2 :  end updates for MCM mits 19200 

                }

                bReturnValue = true;

            }
            catch (InitializationException p_objInitializationException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInitializationException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (RMAppException p_objRMAppException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objRMAppException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException, Globalization.GetString("DocumentManagementAdaptor.GetAttachments.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            finally
            {
                objXmlNode = null;
                objRootElement = null;
                objDocumentManager = null;
                objConfig = null;
                objSettings = null;
                objAcrosoft = null;
                if (objClaim != null)
                    objClaim.Dispose();
                if (objClaimant != null)
                    objClaimant.Dispose();
                if (objDefendant != null)
                    objDefendant.Dispose();
                if (objFunds != null)
                    objFunds.Dispose();
                if (objEvent != null)
                    objEvent.Dispose();
                if (objDataModelFactory != null)
                {
                    objDataModelFactory.Dispose();
                }
                if (objPIEmployee != null)
                    objPIEmployee.Dispose();
                if (objPIPatient != null)
                    objPIPatient.Dispose();
                if (objPIDriver != null) //Aman Driver Enh
                    objPIDriver.Dispose();
                if (objPIWitness != null)
                    objPIWitness.Dispose();
                if (objDatedText != null)
                    objDatedText.Dispose();
                if (objPolicy != null)
                    objPolicy.Dispose();
                if (objUnitXClaim != null)
                    objUnitXClaim.Dispose();
            }

            return bReturnValue;
        }
         public bool GetAttachmentsNew(DocumentListRequest p_objRequest,
            ref DocumentList p_objDocOut,
            ref BusinessAdaptorErrors p_objErrors)
        {
            bool bReturnValue = false;
            DocumentManager objDocumentManager = null;

            XmlElement objRootElement = null;
            XmlNode objXmlNode = null;
            XmlElement objTempElement = null;
          
            string sFormName = string.Empty;
            string sTableName = string.Empty;
            long lRecordId = -1;
            int iPSID = 0;
            string sAttachmentsXml = string.Empty;
            SysSettings objSettings = null;
            RMConfigurator objConfig = null;
            Acrosoft objAcrosoft = null;
            Claim objClaim = null;
            Claimant objClaimant = null;
            Defendant objDefendant = null;
            Funds objFunds = null;
            Event objEvent = null;
            DataModelFactory objDataModelFactory = null;
            PiEmployee objPIEmployee = null;
            PiPatient objPIPatient = null;
            PiDriver objPIDriver = null; //Aman Driver Enh
            PiWitness objPIWitness = null;
            EventXDatedText objDatedText = null;
            Policy objPolicy = null;
            //MITS 18136: Raman Bhatia
            //Adding case for enhanced policy
            PolicyEnh objPolicyEnh = null;
            UnitXClaim objUnitXClaim = null;
            //Mona:PaperVisionMerge : Animesh Inserted 
            PVWrapper objPaperVision = null;
            PaperVisionDocumentManager objPVDocumentManager = null;
           //skhare7  MITS 23131
             int iReturn=0;
             //skhare7  MITS 23131 End
            AdjustDatedText objAdjDatedText = null;
            DisabilityPlan objPlan = null;
            ClaimXLitigation objLitigation = null;
            Expert objExpert = null;
            ClaimAdjuster objClaimAdj = null;
            //skhare7



            int lSortId = 0; // JP 12.1.2006: Added support for sorting in attachments mode
            long lPageNumber = 0;

            try
            {


                if (p_objRequest.TableName == "")
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.GetAttachments.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.GetAttachments.MissingTableNameNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                sTableName = p_objRequest.TableName;

                // JP 12-1-2006: *BEGIN* Crack out the sort id
                objXmlNode = null;

                //objXmlNode = p_objXmlDocIn.SelectSingleNode("//GetAttachments/SortOrder");


                if (p_objRequest.SortOrder == "")
                {
                    lSortId = 0;
                }
                else
                {
                    lSortId = Conversion.ConvertStrToInteger(p_objRequest.SortOrder);
                }
                // JP 12-1-2006: *END* Crack out the sort id

                if (p_objRequest.PageNumber == "")
                {
                    lPageNumber = 0;
                }
                else
                {
                    lPageNumber = Conversion.ConvertStrToLong(p_objRequest.PageNumber);
                }

                objXmlNode = null;
                //objXmlNode = p_objXmlDocIn.SelectSingleNode("//GetAttachments/FormName");

                if (p_objRequest.FormName=="")
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.GetAttachments.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.GetAttachments.MissingFormNameNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                sFormName = p_objRequest.FormName;

                objXmlNode = null;
                //objXmlNode = p_objXmlDocIn.SelectSingleNode("//GetAttachments/RecordId");

                //MITS 11524 - Org Hierarchy permission check start
                lRecordId = Conversion.ConvertStrToLong(p_objRequest.RecordId);

                if (IsOrgHierarchyType(lRecordId, sTableName))
                {
                    if (!(base.userLogin.IsAllowedEx(ORG_ATTACH_PERMISSION)))
                        throw new PermissionViolationException(RMPermissions.RMO_ATTACHMENT, ORG_ATTACH_PERMISSION);

                }
                //MITS 11524 - Org Hierarchy permission check end

                //Abhishek -- Safeway Retrofit Entity Specification -- start

                if (IsVendorType(lRecordId, sTableName))
                {
                    if (!(base.userLogin.IsAllowedEx(ENT_VENDOR_ATTACH_PERMISSION)))
                        throw new PermissionViolationException(RMPermissions.RMO_ATTACHMENT, ENT_VENDOR_ATTACH_PERMISSION);

                }
                //Abhishek -- Safeway Retrofit Entity Specification -- end

                if (p_objRequest.RecordId == "")
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.GetAttachments.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.GetAttachments.MissingRecordIdNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                lRecordId = Conversion.ConvertStrToLong(p_objRequest.RecordId);

                //Raman Bhatia..08/3/2005
                //Check if Acrosoft Interface is enabled..if enabled then we need to bypass this code

                objSettings = new SysSettings(connectionString, base.ClientId);
                p_objDocOut.ShowMCMForFunds = AcrosoftSection.ShowMCMForFunds; //sachin :For mits 30809
                // akaushik5 Added for MITS 30117 Starts
                bool bUseRMADocumentManagement = !object.ReferenceEquals(AcrosoftSection.UseRMADocumentManagementForEntity, null) && AcrosoftSection.UseRMADocumentManagementForEntity.ToLower().Equals("true");
                List<string> entitySearchPages = new List<string>() 
                {"entitymaint", "people", "physician", "employee", "staff", "driver", "patient", "orghierarchymaint"}; 
                bool bInEntitySearch = entitySearchPages.Contains(p_objRequest.FormName.ToLower());
                // akaushik5 Added for MITS 30117 Ends
                //Mona:PaperVisionMerge : Animesh Inserted 
                if (objSettings.UsePaperVisionInterface == true)
                {
                    if (p_objRequest.Psid != null)
                    {
                        if (p_objRequest.Psid.Trim() != "")
                        {
                            iPSID = int.Parse(p_objRequest.Psid);
                        }
                    }
                    objXmlNode = null;
                    if (sTableName == "claim") 
                    {
                        int iBaseSecurityId = 0;
                        iBaseSecurityId = iPSID;
                        objPVDocumentManager = new PaperVisionDocumentManager(m_userLogin,iPSID, base.ClientId);
                        objPVDocumentManager.ConnectionString = this.connectionString;
                        objPVDocumentManager.UserLoginName = this.loginName;
                        objPVDocumentManager.TableName = sTableName;

                        iBaseSecurityId = objPVDocumentManager.GetBaseSecurityIDForTable(iPSID, sTableName);
                        if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_ATTACHMENT))
                            throw new PermissionViolationException(RMPermissions.RMO_ATTACHMENT, iBaseSecurityId);
                        objPaperVision = new PVWrapper();
                        objConfig = new RMConfigurator();
                        objPaperVision.PVEntityID = PaperVisionSection.EntityID;
                        objPaperVision.PVLoginUrl=PaperVisionSection.LoginUrl;
                        objPaperVision.PVProjID = PaperVisionSection.ProjectID;
                        objPaperVision.PVShowDocLinkUrl = PaperVisionSection.DocumentLink;
                        objPaperVision.PVServerAddress = PaperVisionSection.PVServerAddress;   
                        //Animesh Inserted MITS 18345
                        objPaperVision.SharedNetworkLocationPath = PaperVisionSection.SharedNetworkLocationPath;
                        objPaperVision.DomainName = PaperVisionSection.DomainName;
                        objPaperVision.UserID = PaperVisionSection.UserID;
                        objPaperVision.Password = PaperVisionSection.Password;   
                        //Animesh Insertion ends
                        objPVDocumentManager.GetAttachmentsNew(userLogin.LoginName,userLogin.Password,lSortId, sFormName, lRecordId,objPaperVision,out p_objDocOut,lPageNumber);
                        p_objDocOut.PaperVisionActive = "True"; //Animesh Inserted MITS 18345  
                    }
                    else
                    {
                        bReturnValue = false;
                        p_objErrors.Add("DocumentManagementAdaptor.PaperVisionActive.Error",
                        Globalization.GetString("DocumentManagementAdaptor.PaperVisionActive.Error",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                        return bReturnValue;  
                    }

                }
                //Animesh Insertion ends
                
                //Raman Bhatia..08/3/2005
                //Check if Acrosoft Interface is enabled..if enabled then we need to bypass this code
                //Merge: 13502
                // else if (objSettings.UseAcrosoftInterface == false || p_objRequest.NonMCMFormName == "admintracking") //sachin :For mits 30809
                // akaushik5 Changed for MITS 30117 Starts
                //else if (objSettings.UseAcrosoftInterface == false || p_objRequest.NonMCMFormName == "admintracking" || (p_objRequest.FormName == "funds" && p_objDocOut.ShowMCMForFunds == "false")) 
                else if (objSettings.UseAcrosoftInterface == false || p_objRequest.NonMCMFormName == "admintracking" || (p_objRequest.FormName == "funds" && p_objDocOut.ShowMCMForFunds == "false") || (bInEntitySearch && bUseRMADocumentManagement))
                // akaushik5 Changed for MITS 30117 Ends
                {
                    //Mohit Yadav 02/24/2006 for Security Permission implementation
                    //Get psid value
                    //objXmlNode = p_objXmlDocIn.SelectSingleNode("//psid");
                    if (p_objRequest.Psid != null)
                    {
                        if (p_objRequest.Psid.Trim() != "")
                        {
                            iPSID = int.Parse(p_objRequest.Psid);
                        }
                    }
                    objXmlNode = null;


                    //Mohit Yadav 02/24/2006 for Security Permission implementation
                    //Changed code
                    //Checking for Permissions to view attachments for this claim lob
                    int iBaseSecurityId = 0;
                    iBaseSecurityId = iPSID;

                    objDocumentManager = new DocumentManager(m_userLogin, iPSID, base.ClientId);
                    objDocumentManager.ConnectionString = this.connectionString;
                    objDocumentManager.SecurityConnectionString = this.securityConnectionString; //MITS 27764 hlv 5/23/12
                    objDocumentManager.UserLoginName = this.loginName;
                    objDocumentManager.TableName = sTableName;

                    if (sFormName.ToLower() == "funds")
                    {
                        iBaseSecurityId = UserPermissions.RMB_FUNDS_TRANSACT;
                        iPSID = iBaseSecurityId;
                        if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_ATTACHMENT))
                            throw new PermissionViolationException(RMPermissions.RMO_ATTACHMENT, iBaseSecurityId);
                    }
                    else
                    {
                        //Added by Amitosh for mits 27409
                        if (string.Equals(sTableName, "unit", StringComparison.InvariantCultureIgnoreCase))
                        {
                            iBaseSecurityId = objDocumentManager.GetBaseSecurityIDForUnit(p_objRequest.RecordId);
                            objDocumentManager.SecurityId = iBaseSecurityId;
                        }
                        else//End Amitosh
                            iBaseSecurityId = objDocumentManager.GetBaseSecurityIDForTable(iPSID, sTableName);
                        if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_ATTACHMENT))
                            throw new PermissionViolationException(RMPermissions.RMO_ATTACHMENT, iBaseSecurityId);
                    }

                    if (!SetDocumentStorageTypeAndPath(ref objDocumentManager, ref p_objErrors))
                    {
                        return false;
                    }

                    objDocumentManager.GetAttachmentsNew(lSortId, sFormName, lRecordId, lPageNumber,out p_objDocOut);
                    //objRootElement = p_objXmlDocOut.CreateElement("GetAttachmentsResult");
                    //objRootElement.InnerXml = sAttachmentsXml;
                    //p_objXmlDocOut.AppendChild(objRootElement);


                    //objTempElement = p_objXmlDocOut.CreateElement("JumpToAcrosoft");
                    //objTempElement.InnerXml = "false";
                    //objRootElement.AppendChild(objTempElement);

                    // 3/12/2008 Added By Abhishek, MITS 13013

                    iBaseSecurityId = objDocumentManager.GetBaseSecurityIDForTable(iPSID, sTableName);
                    if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_ATT_VIEWPLAYPRINT))
                    {
                        p_objDocOut.ViewAttachment = "false";
                    }
                    else
                    {
                        p_objDocOut.ViewAttachment = "true";
                    }

                    //tanwar2 - ImageRight - start
                    p_objDocOut.UseImageRight = objSettings.UseImgRight;
                    p_objDocOut.UseImageRightWS = objSettings.UseImgRightWebService;
                    //tanwar2 - ImageRight - end

                    p_objDocOut.JumpToAcrosoft = "0";
                }
                //rbhatia4:R8: Use Media View Setting : July 11 2011
                else if (objSettings.UseAcrosoftInterface)
                {
                    //objRootElement = p_objXmlDocOut.CreateElement("GetAttachmentsResult");
                    //p_objXmlDocOut.AppendChild(objRootElement);

                    //objTempElement = p_objXmlDocOut.CreateElement("JumpToAcrosoft");
                    //objTempElement.InnerXml = "true";
                    //objRootElement.AppendChild(objTempElement);

                    //objTempElement = p_objXmlDocOut.CreateElement("TableName");
                    //objTempElement.InnerXml = sTableName;
                    //objRootElement.AppendChild(objTempElement);

                    //objTempElement = p_objXmlDocOut.CreateElement("FormName");
                    //objTempElement.InnerXml = sFormName;
                    //objRootElement.AppendChild(objTempElement);

                    //objTempElement = p_objXmlDocOut.CreateElement("RecordId");
                    //objTempElement.InnerXml = lRecordId.ToString();
                    //objRootElement.AppendChild(objTempElement);


                    p_objDocOut.JumpToAcrosoft = "1";
                    p_objDocOut.AttachTable = sTableName;
                    p_objDocOut.FormName = sFormName;
                    p_objDocOut.AttachRecordId = lRecordId.ToString();

                    //igupta3 Jira RMA-846 starts
                    if (p_objRequest.Psid != null)
                    {
                        if (p_objRequest.Psid.Trim() != "")
                        {
                            iPSID = int.Parse(p_objRequest.Psid);
                        }
                    }

                    //Checking for Permissions to view attachments for this claim lob
                    int iBaseSecurityId = 0;
                    iBaseSecurityId = iPSID;

                    objDocumentManager = new DocumentManager(m_userLogin, iPSID, base.ClientId);
                    objDocumentManager.ConnectionString = this.connectionString;
                    objDocumentManager.SecurityConnectionString = this.securityConnectionString; //MITS 27764 hlv 5/23/12
                    objDocumentManager.UserLoginName = this.loginName;
                    objDocumentManager.TableName = sTableName;

                    if (sFormName.ToLower() == "funds")
                    {
                        iBaseSecurityId = UserPermissions.RMB_FUNDS_TRANSACT;
                        iPSID = iBaseSecurityId;
                        if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_ATTACHMENT))
                            throw new PermissionViolationException(RMPermissions.RMO_ATTACHMENT, iBaseSecurityId);
                    }
                    else
                    {
                        //Added by Amitosh for mits 27409
                        if (string.Equals(sTableName, "unit", StringComparison.InvariantCultureIgnoreCase))
                        {
                            iBaseSecurityId = objDocumentManager.GetBaseSecurityIDForUnit(p_objRequest.RecordId);
                            objDocumentManager.SecurityId = iBaseSecurityId;
                        }
                        else//End Amitosh
                            iBaseSecurityId = objDocumentManager.GetBaseSecurityIDForTable(iPSID, sTableName);
                        if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_ATTACHMENT))
                            throw new PermissionViolationException(RMPermissions.RMO_ATTACHMENT, iBaseSecurityId);
                    }
                    //igupta3 Jira RMA-846 ends
                    //TO DO : Work Out implementation of other screens apart from Event and Claim

                    if (sTableName.ToLower() == "claimant")
                    {
                        objDataModelFactory = new DataModelFactory(base.userLogin, base.ClientId);
                        objClaimant = (Claimant)objDataModelFactory.GetDataModelObject("Claimant", false);
                        objClaimant.MoveTo((int)lRecordId);

                        objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                        objClaim.MoveTo(objClaimant.ClaimId);

                        //objTempElement = p_objXmlDocOut.CreateElement("ClaimNumber");
                        //objTempElement.InnerXml = objClaim.ClaimNumber;
                        //objRootElement.AppendChild(objTempElement);

                        //objTempElement = p_objXmlDocOut.CreateElement("EventNumber");
                        //objTempElement.InnerXml = objClaim.EventNumber;
                        //objRootElement.AppendChild(objTempElement);
                        p_objDocOut.ClaimNumber = objClaim.ClaimNumber;
                        p_objDocOut.EventNumber = objClaim.EventNumber;
                    } // if
                    else
                        if (sTableName.ToLower() == "defendant")
                        {
                            objDataModelFactory = new DataModelFactory(base.userLogin, base.ClientId);
                            objDefendant = (Defendant)objDataModelFactory.GetDataModelObject("Defendant", false);
                            objDefendant.MoveTo((int)lRecordId);

                            objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                            objClaim.MoveTo(objDefendant.ClaimId);

                            //objTempElement = p_objXmlDocOut.CreateElement("ClaimNumber");
                            //objTempElement.InnerXml = objClaim.ClaimNumber;
                            //objRootElement.AppendChild(objTempElement);

                            //objTempElement = p_objXmlDocOut.CreateElement("EventNumber");
                            //objTempElement.InnerXml = objClaim.EventNumber;
                            //objRootElement.AppendChild(objTempElement);
                            p_objDocOut.ClaimNumber = objClaim.ClaimNumber;
                            p_objDocOut.EventNumber = objClaim.EventNumber;

                        } // if
                        else
                            if (sTableName.ToLower() == "funds")
                            {
                                objDataModelFactory = new DataModelFactory(base.userLogin, base.ClientId);
                                objFunds = (Funds)objDataModelFactory.GetDataModelObject("Funds", false);
                                objFunds.MoveTo((int)lRecordId);

                                objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                                objClaim.MoveTo(objFunds.ClaimId);

                                //objTempElement = p_objXmlDocOut.CreateElement("ClaimNumber");
                                //objTempElement.InnerXml = objClaim.ClaimNumber;
                                //objRootElement.AppendChild(objTempElement);

                                //objTempElement = p_objXmlDocOut.CreateElement("EventNumber");
                                //objTempElement.InnerXml = objClaim.EventNumber;
                                //objRootElement.AppendChild(objTempElement);
                                p_objDocOut.ClaimNumber = objClaim.ClaimNumber;
                                p_objDocOut.EventNumber = objClaim.EventNumber;

                            } // if

                            //MITS 8410 : Umesh
                            else
                                if (sTableName.ToLower() == "unit")
                                {
                                    objDataModelFactory = new DataModelFactory(base.userLogin, base.ClientId);
                                    objUnitXClaim = (UnitXClaim)objDataModelFactory.GetDataModelObject("UnitXClaim", false);
                                    objUnitXClaim.MoveTo((int)lRecordId);

                                    objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                                    objClaim.MoveTo(objUnitXClaim.ClaimId);

                                    //objTempElement = p_objXmlDocOut.CreateElement("ClaimNumber");
                                    //objTempElement.InnerXml = objClaim.ClaimNumber;
                                    //objRootElement.AppendChild(objTempElement);

                                    //objTempElement = p_objXmlDocOut.CreateElement("EventNumber");
                                    //objTempElement.InnerXml = objClaim.EventNumber;
                                    //objRootElement.AppendChild(objTempElement);
                                    p_objDocOut.ClaimNumber = objClaim.ClaimNumber;
                                    p_objDocOut.EventNumber = objClaim.EventNumber;

                                }
                                // else

                                else
                                    if (sTableName.ToLower() == "adjuster dated text")
                                    {
                                        objDataModelFactory = new DataModelFactory(base.userLogin, base.ClientId);
                                        objAdjDatedText = (AdjustDatedText)objDataModelFactory.GetDataModelObject("AdjustDatedText", false);
                                        objAdjDatedText.MoveTo(Convert.ToInt32(lRecordId));
                                        lRecordId = Convert.ToInt64(objAdjDatedText.AdjRowId);
                                        objClaimAdj = (ClaimAdjuster)objDataModelFactory.GetDataModelObject("ClaimAdjuster", false);
                                        objClaimAdj.MoveTo(Convert.ToInt32(lRecordId));
                                        lRecordId = Convert.ToInt64(objClaimAdj.ClaimId);
                                        objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                                        objClaim.MoveTo(Convert.ToInt32(lRecordId));
                                        lRecordId = Convert.ToInt64(objClaim.EventId);

                                        objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
                                        objEvent.MoveTo(Convert.ToInt32(lRecordId));
                                        p_objDocOut.ClaimNumber = objClaim.ClaimNumber;
                                        p_objDocOut.EventNumber = objEvent.EventNumber;

                                    }
                                    else if (sTableName.ToLower() == "plan")
                                    {
                                        objDataModelFactory = new DataModelFactory(base.userLogin, base.ClientId);
                                        objPlan = (DisabilityPlan)objDataModelFactory.GetDataModelObject("DisabilityPlan", false);
                                        objPlan.MoveTo(Convert.ToInt32(lRecordId));
                                        lRecordId = Convert.ToInt64(objPlan.PlanId);

                                        objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                                        objClaim.MoveTo(Convert.ToInt32(lRecordId));
                                        lRecordId = Convert.ToInt64(objClaim.EventId);

                                        objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
                                        objEvent.MoveTo(Convert.ToInt32(lRecordId));
                                        p_objDocOut.ClaimNumber = objClaim.ClaimNumber;
                                        p_objDocOut.EventNumber = objEvent.EventNumber;
                                    }
                                    else if (sTableName.ToLower() == "expert")
                                    {
                                        objDataModelFactory = new DataModelFactory(base.userLogin, base.ClientId);
                                        objExpert = (Expert)objDataModelFactory.GetDataModelObject("Expert", false);
                                        objExpert.MoveTo(Convert.ToInt32(lRecordId));
                                        lRecordId = Convert.ToInt64(objExpert.LitigationRowId);
                                        objLitigation = (ClaimXLitigation)objDataModelFactory.GetDataModelObject("ClaimXLitigation", false);
                                        objLitigation.MoveTo(Convert.ToInt32(lRecordId));
                                        lRecordId = Convert.ToInt64(objLitigation.ClaimId);
                                        objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                                        objClaim.MoveTo(Convert.ToInt32(lRecordId));
                                        lRecordId = Convert.ToInt64(objClaim.EventId);

                                        objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
                                        objEvent.MoveTo(Convert.ToInt32(lRecordId));
                                        p_objDocOut.ClaimNumber = objClaim.ClaimNumber;
                                        p_objDocOut.EventNumber = objEvent.EventNumber;
                                    }
                                    else if (sTableName.ToLower() == "litigation")
                                    {
                                        objDataModelFactory = new DataModelFactory(base.userLogin, base.ClientId);
                                        objLitigation = (ClaimXLitigation)objDataModelFactory.GetDataModelObject("ClaimXLitigation", false);
                                        objLitigation.MoveTo(Convert.ToInt32(lRecordId));
                                        lRecordId = Convert.ToInt64(objLitigation.ClaimId);

                                        objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                                        objClaim.MoveTo(Convert.ToInt32(lRecordId));
                                        lRecordId = Convert.ToInt64(objClaim.EventId);

                                        objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
                                        objEvent.MoveTo(Convert.ToInt32(lRecordId));
                                        p_objDocOut.ClaimNumber = objClaim.ClaimNumber;
                                        p_objDocOut.EventNumber = objEvent.EventNumber;
                                    }
                    //End skhare7:MITS 22743
                    //End MITS 8410 
                    p_objDocOut.AcrosoftUsersTypeKey = AcrosoftSection.AcrosoftUsersTypeKey;

                    p_objDocOut.JumpDestination = "Attachments";

                    p_objDocOut.AsAnywhereLink = AcrosoftSection.AsAnywhereLink;

                    p_objDocOut.AcrosoftSkin = AcrosoftSection.AcrosoftSkin;

                    p_objDocOut.EventFolderFriendlyName = AcrosoftSection.EventFolderFriendlyName;

                    p_objDocOut.ClaimFolderFriendlyName = AcrosoftSection.ClaimFolderFriendlyName;

                    p_objDocOut.PolicyFolderFriendlyName = AcrosoftSection.PolicyFolderFriendlyName;

                    p_objDocOut.AcrosoftPolicyTypeKey = AcrosoftSection.AcrosoftPolicyTypeKey;

                    p_objDocOut.AcrosoftAttachmentsTypeKey = AcrosoftSection.AcrosoftAttachmentsTypeKey;
                    //skhare7  MITS 23131

                    p_objDocOut.SearchFolderBeforeCreate = AcrosoftSection.SearchFolderBeforeCreate;
                    //skhare7  MITS 23131 End
                    // p_objDocOut.s = AcrosoftSection.AcrosoftAttachmentsTypeKey;

                    string sAppExcpXml = "";
                    string sSessionID = "";

                    //rsolanki2 :  start updates for MCM mits 19200 
                    Boolean bSuccess = false;
                    string sTempAcrosoftUserId = base.userLogin.LoginName;
                    string sTempAcrosoftPassword = base.userLogin.Password;

                    if (Conversion.CastToType<Boolean>(AcrosoftSection.UseCommonAcrosoftUser, out bSuccess))
                    {
                        if (AcrosoftSection.AcroUserMapSection.UserMap[base.loginName] != null
                            && !string.IsNullOrEmpty(AcrosoftSection.AcroUserMapSection.UserMap[base.loginName].RmxUser))
                        {
                            sTempAcrosoftUserId = AcrosoftSection.AcroUserMapSection.UserMap[base.loginName].AcrosoftUserId;
                            sTempAcrosoftPassword = AcrosoftSection.AcroUserMapSection.UserMap[base.loginName].AcrosoftPassword;
                        }
                        else
                        {
                            sTempAcrosoftUserId = AcrosoftSection.AcrosoftUsername;
                            sTempAcrosoftPassword = AcrosoftSection.AcrosoftUserPassword;
                        }
                    }
                    //rsolanki2 :  end updates for MCM mits 19200 

                    objAcrosoft = new Acrosoft(base.ClientId);//dvatsa-cloud
                    objAcrosoft.Authenticate(sTempAcrosoftUserId, sTempAcrosoftPassword, out sSessionID, out sAppExcpXml);
                    //objAcrosoft.Authenticate(base.userLogin.LoginName, base.userLogin.Password, out sSessionID, out sAppExcpXml);

                    //objTempElement = p_objXmlDocOut.CreateElement("sid");
                    //if (sSessionID != null) objTempElement.InnerXml = sSessionID;
                    //else objTempElement.InnerXml = "";
                    //objRootElement.AppendChild(objTempElement);
                    p_objDocOut.AcrosoftSession = sSessionID;

                    //rsolanki2: start updates for mits 19200 : replacing the uid/pass in params being passed to MCM web service.
                    //Creating Folder in Acrosoft
                    objConfig = new RMConfigurator();
                    objDataModelFactory = new DataModelFactory(base.userLogin, base.ClientId);
                    //skhare7  MITS 23131                    
                    if (Conversion.CastToType<Boolean>(AcrosoftSection.SearchFolderBeforeCreate, out bSuccess))
                    {
                        //rsolanki2: July 24: MITS 22326, 25213 :- updating the parameters passed to SearchFolder and CreateAttachmentFolder below 
                        // to pick values from Datamodel rather than passed values from webservice.
                        switch (sTableName.ToLower())
                        {
                            case "piemployee":
                                objPIEmployee = (PiEmployee)objDataModelFactory.GetDataModelObject("PiEmployee", false);
                                objPIEmployee.MoveTo(Convert.ToInt32(lRecordId));
                                lRecordId = Convert.ToInt64(objPIEmployee.EventId);
                                objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
                                objEvent.MoveTo(Convert.ToInt32(lRecordId));
                                iReturn = objAcrosoft.SearchFolder(sTempAcrosoftUserId, sTempAcrosoftPassword, objEvent.EventNumber, "", AcrosoftSection.GeneralFolderFriendlyName, out sAppExcpXml);
                                if (iReturn != 0)
                                    objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword, AcrosoftSection.AcrosoftAttachmentsTypeKey
                                        , objEvent.EventNumber, "",
                                        AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                                break;
                            case "pipatient":
                                objPIPatient = (PiPatient)objDataModelFactory.GetDataModelObject("PiPatient", false);
                                objPIPatient.MoveTo(Convert.ToInt32(lRecordId));
                                lRecordId = Convert.ToInt64(objPIPatient.EventId);
                                objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
                                objEvent.MoveTo(Convert.ToInt32(lRecordId));
                                iReturn = objAcrosoft.SearchFolder(sTempAcrosoftUserId, sTempAcrosoftPassword, objEvent.EventNumber, "", AcrosoftSection.GeneralFolderFriendlyName, out sAppExcpXml);
                                if (iReturn != 0)
                                    objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword, AcrosoftSection.AcrosoftAttachmentsTypeKey
                                        , objEvent.EventNumber, "", AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                                break;
                                //Aman Driver Enh
                            case "pidriver":  
                                objPIDriver = (PiDriver)objDataModelFactory.GetDataModelObject("PiDriver", false);
                                objPIDriver.MoveTo(Convert.ToInt32(lRecordId));
                                lRecordId = Convert.ToInt64(objPIDriver.EventId);
                                objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
                                objEvent.MoveTo(Convert.ToInt32(lRecordId));
                                iReturn = objAcrosoft.SearchFolder(sTempAcrosoftUserId, sTempAcrosoftPassword, objEvent.EventNumber, "", AcrosoftSection.GeneralFolderFriendlyName, out sAppExcpXml);
                                if (iReturn != 0)
                                    objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword, AcrosoftSection.AcrosoftAttachmentsTypeKey
                                        , objEvent.EventNumber, "", AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                                break;
                            case "piwitness":
                                objPIWitness = (PiWitness)objDataModelFactory.GetDataModelObject("PiWitness", false);
                                objPIWitness.MoveTo(Convert.ToInt32(lRecordId));
                                lRecordId = Convert.ToInt64(objPIWitness.EventId);
                                objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
                                objEvent.MoveTo(Convert.ToInt32(lRecordId));
                                iReturn = objAcrosoft.SearchFolder(sTempAcrosoftUserId, sTempAcrosoftPassword, objEvent.EventNumber, "", AcrosoftSection.GeneralFolderFriendlyName, out sAppExcpXml);
                                if (iReturn != 0)
                                    objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                        AcrosoftSection.AcrosoftAttachmentsTypeKey, objEvent.EventNumber, "",
                                        AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                                break;
                            case "eventdatedtext":
                                objDatedText = (EventXDatedText)objDataModelFactory.GetDataModelObject("EventXDatedText", false);
                                objDatedText.MoveTo(Convert.ToInt32(lRecordId));
                                lRecordId = Convert.ToInt64(objDatedText.EventId);
                                objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
                                objEvent.MoveTo(Convert.ToInt32(lRecordId));
                                iReturn = objAcrosoft.SearchFolder(sTempAcrosoftUserId, sTempAcrosoftPassword, objEvent.EventNumber, "", AcrosoftSection.GeneralFolderFriendlyName, out sAppExcpXml);
                                if (iReturn != 0)
                                    objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                        AcrosoftSection.AcrosoftAttachmentsTypeKey, objEvent.EventNumber, "",
                                        AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                                break;
                            case "event":
                                objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
                                objEvent.MoveTo(Convert.ToInt32(lRecordId));
                                iReturn = objAcrosoft.SearchFolder(sTempAcrosoftUserId, sTempAcrosoftPassword, objEvent.EventNumber, "", AcrosoftSection.GeneralFolderFriendlyName, out sAppExcpXml);
                                if (iReturn != 0)
                                    objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                        AcrosoftSection.AcrosoftAttachmentsTypeKey, objEvent.EventNumber, "",
                                        AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                                break;
                            //skhare7:MITS 22743
                            case "claimant":
                            case "defendant":
                            case "funds":
                            case "unit":
                            case "plan":
                            case "litigation":
                            case "expert":
                            case "adjuster dated text":
                                iReturn = objAcrosoft.SearchFolder(sTempAcrosoftUserId, sTempAcrosoftPassword, p_objDocOut.EventNumber, p_objDocOut.ClaimNumber, AcrosoftSection.GeneralFolderFriendlyName, out sAppExcpXml);
                                if (iReturn != 0)
                                    objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                        AcrosoftSection.AcrosoftAttachmentsTypeKey, p_objDocOut.EventNumber,
                                        p_objDocOut.ClaimNumber, AcrosoftSection.EventFolderFriendlyName,
                                        AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                                break;
                            //skhare7:MITS 22743 End
                            case "claim":
                                objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                                objClaim.MoveTo(Convert.ToInt32(lRecordId));
                                iReturn = objAcrosoft.SearchFolder(sTempAcrosoftUserId, sTempAcrosoftPassword, objClaim.EventNumber, objClaim.ClaimNumber, AcrosoftSection.GeneralFolderFriendlyName, out sAppExcpXml);
                                if (iReturn != 0)
                                    objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                        AcrosoftSection.AcrosoftAttachmentsTypeKey,
                                        objClaim.EventNumber, objClaim.ClaimNumber, AcrosoftSection.EventFolderFriendlyName,
                                        AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                                break;
                            case "policy":
                                objPolicy = (Policy)objDataModelFactory.GetDataModelObject("Policy", false);
                                objPolicy.MoveTo(Convert.ToInt32(lRecordId));
                                iReturn = objAcrosoft.SearchFolder(sTempAcrosoftUserId, sTempAcrosoftPassword, objPolicy.PolicyName, "P", AcrosoftSection.GeneralFolderFriendlyName, out sAppExcpXml);
                                if (iReturn != 0)
                                    objAcrosoft.CreatePolicyFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                        AcrosoftSection.AcrosoftPolicyTypeKey,
                                        objPolicy.PolicyName, AcrosoftSection.PolicyFolderFriendlyName, out sAppExcpXml);
                                break;
                            //MITS 18136: Raman Bhatia
                            //Adding case for enhanced policy
                            //changed by skhare7 MITS  21889
                            case "policyenh":
                            case "policyenhgl":
                            case "policyenhpc":
                            case "policyenhal":
                            case "policyenhwc":
                                objPolicyEnh = (PolicyEnh)objDataModelFactory.GetDataModelObject("PolicyEnh", false);
                                objPolicyEnh.MoveTo(Convert.ToInt32(lRecordId));
                                iReturn = objAcrosoft.SearchFolder(sTempAcrosoftUserId, sTempAcrosoftPassword, objPolicyEnh.PolicyName, "P", AcrosoftSection.GeneralFolderFriendlyName, out sAppExcpXml);
                                if (iReturn != 0)
                                    objAcrosoft.CreatePolicyFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                        AcrosoftSection.AcrosoftPolicyTypeKey,
                                        objPolicyEnh.PolicyName, AcrosoftSection.PolicyFolderFriendlyName, out sAppExcpXml);
                                break;

                            default:

                                break;
                        }
                        //skhare7  MITS 23131 End
                    }
                    else
                    {
                        switch (sTableName.ToLower())
                        {
                            case "piemployee":
                                objPIEmployee = (PiEmployee)objDataModelFactory.GetDataModelObject("PiEmployee", false);
                                objPIEmployee.MoveTo(Convert.ToInt32(lRecordId));
                                lRecordId = Convert.ToInt64(objPIEmployee.EventId);
                                objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
                                objEvent.MoveTo(Convert.ToInt32(lRecordId));
                                objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword, AcrosoftSection.AcrosoftAttachmentsTypeKey, objEvent.EventNumber, "",
                                    AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                                break;
                            case "pipatient":
                                objPIPatient = (PiPatient)objDataModelFactory.GetDataModelObject("PiPatient", false);
                                objPIPatient.MoveTo(Convert.ToInt32(lRecordId));
                                lRecordId = Convert.ToInt64(objPIPatient.EventId);
                                objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
                                objEvent.MoveTo(Convert.ToInt32(lRecordId));
                                objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword, AcrosoftSection.AcrosoftAttachmentsTypeKey, objEvent.EventNumber, "", AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                                break;
                                //Aman Driver Enh
                            case "pidriver":
                                objPIDriver = (PiDriver)objDataModelFactory.GetDataModelObject("PiDriver", false);
                                objPIDriver.MoveTo(Convert.ToInt32(lRecordId));
                                lRecordId = Convert.ToInt64(objPIDriver.EventId);
                                objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
                                objEvent.MoveTo(Convert.ToInt32(lRecordId));
                                objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword, AcrosoftSection.AcrosoftAttachmentsTypeKey, objEvent.EventNumber, "", AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                                break;
                            //Aman Driver Enh
                            case "piwitness":
                                objPIWitness = (PiWitness)objDataModelFactory.GetDataModelObject("PiWitness", false);
                                objPIWitness.MoveTo(Convert.ToInt32(lRecordId));
                                lRecordId = Convert.ToInt64(objPIWitness.EventId);
                                objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
                                objEvent.MoveTo(Convert.ToInt32(lRecordId));
                                objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                    AcrosoftSection.AcrosoftAttachmentsTypeKey, objEvent.EventNumber, "",
                                    AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                                break;
                            case "eventdatedtext":
                                objDatedText = (EventXDatedText)objDataModelFactory.GetDataModelObject("EventXDatedText", false);
                                objDatedText.MoveTo(Convert.ToInt32(lRecordId));
                                lRecordId = Convert.ToInt64(objDatedText.EventId);
                                objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
                                objEvent.MoveTo(Convert.ToInt32(lRecordId));
                                objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                    AcrosoftSection.AcrosoftAttachmentsTypeKey, objEvent.EventNumber, "",
                                    AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                                break;
                            case "event":
                                objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
                                objEvent.MoveTo(Convert.ToInt32(lRecordId));
                                objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                    AcrosoftSection.AcrosoftAttachmentsTypeKey, objEvent.EventNumber, "",
                                    AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                                break;
                            //skhare7:MITS 22743
                            case "claimant":
                            case "defendant":
                            case "funds":
                            case "unit":
                            case "plan":
                            case "litigation":
                            case "expert":
                            case "adjuster dated text":
                                objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                    AcrosoftSection.AcrosoftAttachmentsTypeKey, p_objDocOut.EventNumber,
                                    p_objDocOut.ClaimNumber, AcrosoftSection.EventFolderFriendlyName,
                                    AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                                break;
                            //skhare7:MITS 22743 End
                            case "claim":
                                objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                                objClaim.MoveTo(Convert.ToInt32(lRecordId));
                                objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                    AcrosoftSection.AcrosoftAttachmentsTypeKey,
                                    objClaim.EventNumber, objClaim.ClaimNumber, AcrosoftSection.EventFolderFriendlyName,
                                    AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                                break;
                            case "policy":
                                objPolicy = (Policy)objDataModelFactory.GetDataModelObject("Policy", false);
                                objPolicy.MoveTo(Convert.ToInt32(lRecordId));
                                objAcrosoft.CreatePolicyFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                    AcrosoftSection.AcrosoftPolicyTypeKey,
                                    objPolicy.PolicyName, AcrosoftSection.PolicyFolderFriendlyName, out sAppExcpXml);
                                break;
                            //MITS 18136: Raman Bhatia
                            //Adding case for enhanced policy
                            //changed by skhare7 MITS  21889
                            case "policyenh":
                            case "policyenhgl":
                            case "policyenhpc":
                            case "policyenhal":
                            case "policyenhwc":
                                objPolicyEnh = (PolicyEnh)objDataModelFactory.GetDataModelObject("PolicyEnh", false);
                                objPolicyEnh.MoveTo(Convert.ToInt32(lRecordId));
                                objAcrosoft.CreatePolicyFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                    AcrosoftSection.AcrosoftPolicyTypeKey,
                                    objPolicyEnh.PolicyName, AcrosoftSection.PolicyFolderFriendlyName, out sAppExcpXml);
                                break;

                            default:

                                break;
                        } // switch
                        //changed by skhare7 MITS  21889
                    }
                }
                
                //smishra25 Start:Flag which should be set when Use Outlook to email is 
                //turned on in General System Parameter Setup
                p_objDocOut.UseOutlookToEmail = objSettings.UseOutlook;
                p_objDocOut.SizeLimitOutlook = objSettings.FileSizeLimitForOutlook;

                //smishra25 End:Flag which should be set when Use Outlook to email is 
                bReturnValue = true;

            }
            catch (InitializationException p_objInitializationException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInitializationException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (RMAppException p_objRMAppException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objRMAppException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException, Globalization.GetString("DocumentManagementAdaptor.GetAttachments.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            finally
            {
                objXmlNode = null;
                objRootElement = null;
                objDocumentManager = null;
                objConfig = null;
                objSettings = null;
                objAcrosoft = null;
                if (objClaim != null)
                    objClaim.Dispose();
                if (objClaimant != null)
                    objClaimant.Dispose();
                if (objDefendant != null)
                    objDefendant.Dispose();
                if (objFunds != null)
                    objFunds.Dispose();
                if (objEvent != null)
                    objEvent.Dispose();
                if (objDataModelFactory != null)
                {
                    objDataModelFactory.Dispose();
                }
                if (objPIEmployee != null)
                    objPIEmployee.Dispose();
                if (objPIPatient != null)
                    objPIPatient.Dispose();
                if (objPIDriver != null)  //Aman Driver Enh
                    objPIDriver.Dispose();
                if (objPIWitness != null)
                    objPIWitness.Dispose();
                if (objDatedText != null)
                    objDatedText.Dispose();
                if (objPolicy != null)
                    objPolicy.Dispose();
                //MITS 18136: Raman Bhatia
                //Adding case for enhanced policy
                if (objPolicyEnh != null)
                    objPolicyEnh.Dispose();
                if (objUnitXClaim != null)
                    objUnitXClaim.Dispose();
                //Mona:PaperVisionMerge : Animesh Inserted 
                objPaperVision = null;
                objPVDocumentManager = null;  
                //Animesh insertion ends
            }

            return bReturnValue;
        }
    
    /// <summary>
    /// Raman Bhatia 11 July 2011: Get all Associated Record Info using datamodel
    /// </summary>
    /// <param name="p_sTableName"></param>
    /// <param name="p_sFormName"></param>
    /// <param name="p_lRecordId"></param>
    /// <returns></returns>
        private AssociatedRecordInfo GetRecordInfoForMediaView(string p_sTableName , string p_sFormName , long p_lRecordId)
         {
             AssociatedRecordInfo response = null;
             DataModelFactory objDataModelFactory = null;
             Claimant objClaimant = null;
             Claim objClaim = null;
             Defendant objDefendant = null;
             Event objEvent = null;
             ClaimXLitigation objLitigation = null;
             DisabilityPlan objPlan = null;
             Funds objFunds = null;
             UnitXClaim objUnitXClaim = null;
             ClaimXPropertyLoss objClaimXPropertyLoss = null;   //kkaur8 added MITS 33842
             AdjustDatedText objAdjDatedText = null;
             ClaimAdjuster objClaimAdj = null;
             Expert objExpert = null;
             long lRecordId = 0;
             PiEmployee objPIEmployee = null;
             PiPatient objPIPatient = null;
             PiDriver objPIDriver = null; //Aman Driver Enh
             PiWitness objPIWitness = null;
             EventXDatedText objDatedText = null;
             Policy objPolicy = null;
             PolicyEnh objPolicyenh = null;
             CmXAccommodation objCmXAccommodation = null;
             CmXCmgrHist objCmXCmgrist = null;
             CaseMgrNotes objCaseMgrNotes = null;
             CmXVocrehab objCmXVocrehab = null;
             CaseManagement objCaseManagement = null;
             int iLastIndex = 0;


             try
             {
                 response = new AssociatedRecordInfo();
                 response.TableName = p_sTableName;
                 response.FormName = p_sFormName;
                 response.RecordId = p_lRecordId.ToString();
                 objDataModelFactory = new DataModelFactory(base.userLogin, base.ClientId);

                 switch (p_sTableName.ToLower())
                 {

                     case "claim":

                        
                         objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                         objClaim.MoveTo((int)p_lRecordId);
                         response.AssociatedRecordNumber = objClaim.ClaimNumber;
                         response.AssociatedRecordType = ASSOCIATED_RECORD_TYPE.Claim.ToString();
                         break;

                     case "event":

                         
                         objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
                         objEvent.MoveTo((int)p_lRecordId);
                         response.AssociatedRecordNumber = objEvent.EventNumber;
                         response.AssociatedRecordType = ASSOCIATED_RECORD_TYPE.Event.ToString();
                         break;


                     case "claimant":

                        
                         objClaimant = (Claimant)objDataModelFactory.GetDataModelObject("Claimant", false);
                         objClaimant.MoveTo((int)p_lRecordId);
                         objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                         objClaim.MoveTo(objClaimant.ClaimId);
                         response.AssociatedRecordNumber = objClaim.ClaimNumber;
                         response.AssociatedRecordType = ASSOCIATED_RECORD_TYPE.Claim.ToString();
                         break;

                     case "defendant":
                         
                         objDefendant = (Defendant)objDataModelFactory.GetDataModelObject("Defendant", false);
                         objDefendant.MoveTo((int)p_lRecordId);
                         objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                         objClaim.MoveTo(objDefendant.ClaimId);
                         response.AssociatedRecordNumber = objClaim.ClaimNumber;
                         response.AssociatedRecordType = ASSOCIATED_RECORD_TYPE.Claim.ToString();
                         break;

                     case "funds":
                         
                         objFunds = (Funds)objDataModelFactory.GetDataModelObject("Funds", false);
                         objFunds.MoveTo((int)p_lRecordId);
                         objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                         objClaim.MoveTo(objFunds.ClaimId);
                         response.AssociatedRecordNumber = objClaim.ClaimNumber;
                         response.AssociatedRecordType = ASSOCIATED_RECORD_TYPE.Claim.ToString();
                         break;


                     case "unit":
                         
                         objUnitXClaim = (UnitXClaim)objDataModelFactory.GetDataModelObject("UnitXClaim", false);
                         objUnitXClaim.MoveTo((int)p_lRecordId);
                         objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                         objClaim.MoveTo(objUnitXClaim.ClaimId);
                         response.AssociatedRecordNumber = objClaim.ClaimNumber;
                         response.AssociatedRecordType = ASSOCIATED_RECORD_TYPE.Claim.ToString();
                         break;
                     //kkaur8 added for MITS 33842 starts
                     case "propertyloss":

                         objClaimXPropertyLoss = (ClaimXPropertyLoss)objDataModelFactory.GetDataModelObject("ClaimXPropertyLoss", false);
                         objClaimXPropertyLoss.MoveTo((int)p_lRecordId);
                         objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                         objClaim.MoveTo(objClaimXPropertyLoss.ClaimId);
                         response.AssociatedRecordNumber = objClaim.ClaimNumber;
                         response.AssociatedRecordType = ASSOCIATED_RECORD_TYPE.Claim.ToString();
                         break;

                     //kkaur8 added for MITS 33842 ends

                     case "adjuster dated text":
                         
                         objAdjDatedText = (AdjustDatedText)objDataModelFactory.GetDataModelObject("AdjustDatedText", false);
                         objAdjDatedText.MoveTo(Convert.ToInt32(p_lRecordId));
                         lRecordId = Convert.ToInt64(objAdjDatedText.AdjRowId);
                         objClaimAdj = (ClaimAdjuster)objDataModelFactory.GetDataModelObject("ClaimAdjuster", false);
                         objClaimAdj.MoveTo(Convert.ToInt32(lRecordId));
                         lRecordId = Convert.ToInt64(objClaimAdj.ClaimId);
                         objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                         objClaim.MoveTo(Convert.ToInt32(lRecordId));
                         response.AssociatedRecordNumber = objClaim.EventNumber;
                         response.AssociatedRecordType = ASSOCIATED_RECORD_TYPE.Event.ToString();
                         break;

                     case "plan":
                         
                         objPlan = (DisabilityPlan)objDataModelFactory.GetDataModelObject("DisabilityPlan", false);
                         objPlan.MoveTo(Convert.ToInt32(p_lRecordId));
                         lRecordId = Convert.ToInt64(objPlan.PlanId);
                         objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                         objClaim.MoveTo(Convert.ToInt32(lRecordId));
                         response.AssociatedRecordNumber = objClaim.ClaimNumber;
                         response.AssociatedRecordType = ASSOCIATED_RECORD_TYPE.Claim.ToString();
                         break;
                     case "expert":
                         
                         objExpert = (Expert)objDataModelFactory.GetDataModelObject("Expert", false);
                         objExpert.MoveTo(Convert.ToInt32(p_lRecordId));
                         lRecordId = Convert.ToInt64(objExpert.LitigationRowId);
                         objLitigation = (ClaimXLitigation)objDataModelFactory.GetDataModelObject("ClaimXLitigation", false);
                         objLitigation.MoveTo(Convert.ToInt32(lRecordId));
                         lRecordId = Convert.ToInt64(objLitigation.ClaimId);
                         objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                         objClaim.MoveTo(Convert.ToInt32(lRecordId));
                         response.AssociatedRecordNumber = objClaim.ClaimNumber;
                         response.AssociatedRecordType = "Claim";
                         break;


                     case "litigation":
                         {
                             
                             objLitigation = (ClaimXLitigation)objDataModelFactory.GetDataModelObject("ClaimXLitigation", false);
                             objLitigation.MoveTo(Convert.ToInt32(p_lRecordId));
                             lRecordId = Convert.ToInt64(objLitigation.ClaimId);
                             objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                             objClaim.MoveTo(Convert.ToInt32(lRecordId));
                             response.AssociatedRecordType = "Claim";
                             response.AssociatedRecordNumber = objClaim.ClaimNumber;
                             break;
                         }

                     case "piemployee":
                         
                         objPIEmployee = (PiEmployee)objDataModelFactory.GetDataModelObject("PiEmployee", false);
                         objPIEmployee.MoveTo(Convert.ToInt32(p_lRecordId));
                         lRecordId = Convert.ToInt64(objPIEmployee.EventId);
                         objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
                         objEvent.MoveTo(Convert.ToInt32(lRecordId));
                         response.AssociatedRecordType = ASSOCIATED_RECORD_TYPE.Event.ToString();
                         response.AssociatedRecordNumber = objEvent.EventNumber;
                         break;
                     case "pipatient":
                         
                         objPIPatient = (PiPatient)objDataModelFactory.GetDataModelObject("PiPatient", false);
                         objPIPatient.MoveTo(Convert.ToInt32(p_lRecordId));
                         lRecordId = Convert.ToInt64(objPIPatient.EventId);
                         objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
                         objEvent.MoveTo(Convert.ToInt32(lRecordId));
                         response.AssociatedRecordType = ASSOCIATED_RECORD_TYPE.Event.ToString();
                         response.AssociatedRecordNumber = objEvent.EventNumber;
                         break;
                         //Aman Driver Enh
                     case "pidriver":

                         objPIDriver = (PiDriver)objDataModelFactory.GetDataModelObject("PiDriver", false);
                         objPIDriver.MoveTo(Convert.ToInt32(p_lRecordId));
                         lRecordId = Convert.ToInt64(objPIDriver.EventId);
                         objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
                         objEvent.MoveTo(Convert.ToInt32(lRecordId));
                         response.AssociatedRecordType = ASSOCIATED_RECORD_TYPE.Event.ToString();
                         response.AssociatedRecordNumber = objEvent.EventNumber;
                         break;
                     //Aman Driver Enh
                     case "piwitness":
                         
                         objPIWitness = (PiWitness)objDataModelFactory.GetDataModelObject("PiWitness", false);
                         objPIWitness.MoveTo(Convert.ToInt32(p_lRecordId));
                         lRecordId = Convert.ToInt64(objPIWitness.EventId);
                         objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
                         objEvent.MoveTo(Convert.ToInt32(lRecordId));
                         response.AssociatedRecordType = ASSOCIATED_RECORD_TYPE.Event.ToString();
                         response.AssociatedRecordNumber = objEvent.EventNumber;
                         break;
                     case "eventdatedtext":
                         
                         objDatedText = (EventXDatedText)objDataModelFactory.GetDataModelObject("EventXDatedText", false);
                         objDatedText.MoveTo(Convert.ToInt32(p_lRecordId));
                         lRecordId = Convert.ToInt64(objDatedText.EventId);
                         objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
                         objEvent.MoveTo(Convert.ToInt32(lRecordId));
                         response.AssociatedRecordType = ASSOCIATED_RECORD_TYPE.Event.ToString();
                         response.AssociatedRecordNumber = objEvent.EventNumber;
                         break;
                    
                     case "policy":

                         objPolicy = (Policy)objDataModelFactory.GetDataModelObject("Policy" , false);
                         objPolicy.MoveTo(Convert.ToInt32(p_lRecordId));
                         response.AssociatedRecordType = ASSOCIATED_RECORD_TYPE.Policy.ToString();
                         if (!string.IsNullOrEmpty(objPolicy.PolicyName))
                         {
                             iLastIndex = objPolicy.PolicyName.LastIndexOf(" ");
                             response.AssociatedRecordNumber = objPolicy.PolicyName.Remove(iLastIndex, 1).Insert(iLastIndex, string.Empty); // Changed by bsharma33 05/16/2013 : Policy number format in Media view is "CPP 000000105" (space between policy symbol and policy number, and no space between policy number(0000001) and module(05).)
                         }
                         break;

                     case "policyenh":
                     case "policyenhgl":
                     case "policyenhpc":
                     case "policyenhwc":
                     case "policyenhal": 

                         objPolicyenh = (PolicyEnh)objDataModelFactory.GetDataModelObject("PolicyEnh", false);
                         objPolicyenh.MoveTo(Convert.ToInt32(p_lRecordId));
                         response.AssociatedRecordType = ASSOCIATED_RECORD_TYPE.Policy.ToString();
                         response.AssociatedRecordNumber = objPolicyenh.PolicyName;
                         break;

                     case "user":

                         response.AssociatedRecordType = ASSOCIATED_RECORD_TYPE.User.ToString();
                         response.AssociatedRecordNumber = loginName;
                         break;

                     //MITS 28798: Ishan : Attachment in accomodation, case managers and vocational rehabilitation.
                     case "cmxcmgrhist":          
                                      
                         objCmXCmgrist = (CmXCmgrHist)objDataModelFactory.GetDataModelObject("CmXCmgrHist", false);
                         objCmXCmgrist.MoveTo((int)p_lRecordId);
                         lRecordId = Convert.ToInt64(objCmXCmgrist.CasemgtRowId);
                         objCaseManagement = (CaseManagement)objDataModelFactory.GetDataModelObject("CaseManagement", false);
                         objCaseManagement.MoveTo((int)lRecordId);
                         lRecordId = Convert.ToInt64(objCaseManagement.ClaimId);
                         objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                         objClaim.MoveTo((int)lRecordId);
                         response.AssociatedRecordNumber = objClaim.ClaimNumber;
                         response.AssociatedRecordType = ASSOCIATED_RECORD_TYPE.Claim.ToString();
                         break;

                     case "cmxvocrehab":

                         objCmXVocrehab = (CmXVocrehab)objDataModelFactory.GetDataModelObject("CmXVocrehab", false);
                         objCmXVocrehab.MoveTo((int)p_lRecordId);
                         lRecordId = Convert.ToInt64(objCmXVocrehab.CasemgtRowId);
                         objCaseManagement = (CaseManagement)objDataModelFactory.GetDataModelObject("CaseManagement", false);
                         objCaseManagement.MoveTo((int)lRecordId);
                         lRecordId = Convert.ToInt64(objCaseManagement.ClaimId);
                         objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                         objClaim.MoveTo((int)lRecordId);
                         response.AssociatedRecordNumber = objClaim.ClaimNumber;
                         response.AssociatedRecordType = ASSOCIATED_RECORD_TYPE.Claim.ToString();
                         break;

                     case "casemgrnotes":

                         objCaseMgrNotes = (CaseMgrNotes)objDataModelFactory.GetDataModelObject("CaseMgrNotes", false);
                         objCaseMgrNotes.MoveTo((int)p_lRecordId);
                         lRecordId = Convert.ToInt64(objCaseMgrNotes.CmgrNotesRowId);
                         objCmXCmgrist = (CmXCmgrHist)objDataModelFactory.GetDataModelObject("CmXCmgrHist", false);
                         objCmXCmgrist.MoveTo((int)p_lRecordId);
                         lRecordId = Convert.ToInt64(objCmXCmgrist.CasemgtRowId);
                         objCaseManagement = (CaseManagement)objDataModelFactory.GetDataModelObject("CaseManagement", false);
                         objCaseManagement.MoveTo((int)lRecordId);
                         lRecordId = Convert.ToInt64(objCaseManagement.ClaimId);
                         objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                         objClaim.MoveTo((int)lRecordId);
                         response.AssociatedRecordNumber = objClaim.ClaimNumber;
                         response.AssociatedRecordType = ASSOCIATED_RECORD_TYPE.Claim.ToString();
                         break;

                     case "cmxaccommodation":
                         
                         objCmXAccommodation = (CmXAccommodation)objDataModelFactory.GetDataModelObject("CmXAccommodation", false);                                                  
                         objCmXAccommodation.MoveTo((int)p_lRecordId);
                         lRecordId = Convert.ToInt64(objCmXAccommodation.CmRowId);
                         objCaseManagement = (CaseManagement)objDataModelFactory.GetDataModelObject("CaseManagement", false);
                         objCaseManagement.MoveTo((int)lRecordId);
                         lRecordId = Convert.ToInt64(objCaseManagement.ClaimId);
                         objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                         objClaim.MoveTo((int)lRecordId);
                         response.AssociatedRecordNumber = objClaim.ClaimNumber;
                         response.AssociatedRecordType = ASSOCIATED_RECORD_TYPE.Claim.ToString();
                         break;
                         // Ishan End

                 }

                 return response;
             }
             catch (Exception e)
             {
                 return response;
             }
                 
            
             finally
             {
                 if (objClaim != null)
                     objClaim.Dispose();
                 if (objClaimant != null)
                     objClaimant.Dispose();
                 if (objDefendant != null)
                     objDefendant.Dispose();
                 if (objFunds != null)
                     objFunds.Dispose();
                 if (objEvent != null)
                     objEvent.Dispose();
                 if (objDataModelFactory != null)
                 {
                     objDataModelFactory.Dispose();
                 }
                 if (objUnitXClaim != null)
                     objUnitXClaim.Dispose();

                 if (objDatedText != null)
                     objDatedText.Dispose();
                 if (objPIEmployee != null)
                     objPIEmployee.Dispose();
                 if (objPIPatient != null)
                     objPIPatient.Dispose();
                 if (objPIDriver != null)  //Aman Driver Enh
                     objPIDriver.Dispose();
                 if (objPIWitness != null)
                     objPIWitness.Dispose();
                 if (objPolicy != null)
                     objPolicy.Dispose();
             }
        } 
        
        
        //Abhishek - MITS 11524 check for org hierarchy permission -Start
        /// <summary>
        /// Function check for orgnisation hierarchy permissions, restricts user to 
        /// edit, add company,dept client.. depending upon permission
        /// </summary>
        /// <param name="lRecordId">entity id</param>
        /// /// <param name="sTableName">table name</param>
        private bool IsOrgHierarchyType(long lRecordId, string sTableName)
        {

            int intEntityTableId = 0;
            LocalCache objCache = new LocalCache(this.connectionString, base.ClientId);

            if (sTableName == "entitymaint")
            {
                intEntityTableId = objCache.GetOrgTableId(Conversion.ConvertStrToInteger(lRecordId.ToString()));
                //Check for org hierarchy table
                if (intEntityTableId >= 1005 && intEntityTableId <= 1012)
                {
                    return true;
                }
            }

            return false;

        }
        //Abhishek -- Safeway Entity Specification -- start
        /// <summary>
        /// Function check for Entity Vendor Type,
        /// </summary>
        /// <param name="lRecordId">entity id</param>
        /// <param name="sTableName">table name</param>
        private bool IsVendorType(long lRecordId, string sTableName)
        {
            LocalCache objCache = null;
            try
            {
                int intEntityTableId = 0;
                objCache = new LocalCache(this.connectionString, base.ClientId);

                if (sTableName == "entity")
                {
                    intEntityTableId = objCache.GetOrgTableId(Conversion.ConvertStrToInteger(lRecordId.ToString()));
                    //Check for Vendor entity type
                    if (intEntityTableId == objCache.GetTableId("VENDOR"))
                    {
                        return true;
                    }
                }

                return false;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            finally
            {
                if (objCache != null)
                    objCache.Dispose();
            }

        }
        //Abhishek -- Safeway Entity Specification -- end
	

        /// Name			: GetFolderDetails
        /// Author			: Aditya Babbar
        /// Date Created	: 15-Mar-2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Wrapper to DocumentManager.GetFolderDetails of application
        /// layer that retrieves details for a folder.
        /// </summary>
        /// <param name="p_objXmlDocIn"> 
        ///	Input paramters in xml format.
        ///	Structure:
        ///		<GetFolderDetails>
        ///			<FolderId>
        ///				Id of the folder whose details are required
        ///			</FolderId>
        ///		</GetFolderDetails>
        /// </param>
        /// <param name="p_objXmlDocOut"> 
        ///	Method output in xml format.
        ///	Structure:
        ///		<GetFolderDetailsResult>
        ///			<data name="FolderDetails">
        ///				<folder folderid="" pid="" folderpath="" 
        ///					userid="">
        ///				</folder>
        ///			</data>
        ///		</GetFolderDetailsResult>		
        ///	</param>
        /// <param name="p_objErrors">
        ///	Collection of errors/messages.	
        ///	</param>
        /// <returns>
        ///		true - success
        ///		false - failure
        ///	</returns>
        public bool GetFolderDetails(XmlDocument p_objXmlDocIn,
            ref XmlDocument p_objXmlDocOut,
            ref BusinessAdaptorErrors p_objErrors)
        {
            bool bReturnValue = false;
            DocumentManager objDocumentManager = null;

            XmlElement objRootElement = null;
            XmlNode objXmlNode = null;

            // O and -1 are valid folder id values. Hence initialization is
            // being done with -2.
            long lFolderId = -2;
            string sFolderDetailsXml = string.Empty;


            try
            {
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//GetFolderDetails/FolderId");

                if (objXmlNode == null)
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.GetFolderDetails.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.GetFolderDetails.MissingFolderIdNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                lFolderId = Conversion.ConvertStrToLong(objXmlNode.InnerText);


                objDocumentManager = new DocumentManager(m_userLogin, base.ClientId);
                objDocumentManager.ConnectionString = this.connectionString;
                objDocumentManager.SecurityConnectionString = this.securityConnectionString; //MITS 27764 hlv 5/23/12
                objDocumentManager.UserLoginName = this.loginName;

                objDocumentManager.GetFolderDetails(lFolderId, out sFolderDetailsXml);

                objRootElement = p_objXmlDocOut.CreateElement("GetFolderDetailsResult");

                objRootElement.InnerXml = sFolderDetailsXml;
                p_objXmlDocOut.AppendChild(objRootElement);

                bReturnValue = true;
            }
            catch (InitializationException p_objInitializationException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInitializationException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (RMAppException p_objRMAppException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objRMAppException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException, Globalization.GetString("DocumentManagementAdaptor.GetFolderDetails.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            finally
            {
                objXmlNode = null;
                objRootElement = null;
                objDocumentManager = null;
            }

            return bReturnValue;
        }

        /// Name			: GetFoldersAsOption
        /// Author			: Aditya Babbar
        /// Date Created	: 15-Mar-2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Wrapper to DocumentManager.GetFoldersAsOption of Application
        /// layer that returns folder details in form of XML including the 
        /// folder hierarchy.
        /// </summary>
        /// <param name="p_objXmlDocIn"> 
        ///	Input paramters in xml format.
        ///	Structure:
        ///		<GetFoldersAsOption>
        ///			<SelectFolderId>
        ///				A specific attribute 'def' is set in the xml 
        ///				tag for this selected folder id.
        ///			</SelectFolderId>
        ///		</GetFoldersAsOption>
        /// </param>
        /// <param name="p_objXmlDocOut"> 
        ///	Method output in xml format.
        ///	Structure:
        ///		<GetFoldersAsOptionResult>
        ///			<data>
        ///				<dir1 parent="" id="">
        ///					<dir2 parent="33" id="34">
        ///						<dir3 parent="34" id="35">
        ///						</dir3>
        ///					</dir2>
        ///				</dir1>
        ///			</data>
        ///		</GetFoldersAsOptionResult>
        ///	</param>
        /// <param name="p_objErrors">
        ///	Collection of errors/messages.	
        ///	</param>
        /// <returns>
        ///		true - success
        ///		false - failure
        ///	</returns>
        public bool GetFoldersAsOption(XmlDocument p_objXmlDocIn,
            ref XmlDocument p_objXmlDocOut,
            ref BusinessAdaptorErrors p_objErrors)
        {
            bool bReturnValue = false;
            DocumentManager objDocumentManager = null;

            XmlElement objRootElement = null;
            XmlNode objXmlNode = null;

            // O and -1 are valid folder id values. Hence initialization is
            // being done with -2.
            long lSelectFolderId = -2;
            string sFoldersXml = string.Empty;


            try
            {
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//GetFoldersAsOption/SelectFolderId");

                if (objXmlNode == null)
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.GetFolderAsOption.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.GetFolderAsOption.MissingSelectFolderIdNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                lSelectFolderId = Conversion.ConvertStrToLong(objXmlNode.InnerText);


                objDocumentManager = new DocumentManager(m_userLogin, base.ClientId);
                objDocumentManager.ConnectionString = this.connectionString;
                objDocumentManager.SecurityConnectionString = this.securityConnectionString; //MITS 27764 hlv 5/23/12
                objDocumentManager.UserLoginName = this.loginName;

                objDocumentManager.GetFoldersAsOption(lSelectFolderId, out sFoldersXml);

                objRootElement = p_objXmlDocOut.CreateElement("GetFoldersAsOptionResult");

                objRootElement.InnerXml = sFoldersXml;
                p_objXmlDocOut.AppendChild(objRootElement);

                bReturnValue = true;
            }
            catch (InitializationException p_objInitializationException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInitializationException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (RMAppException p_objRMAppException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objRMAppException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException, Globalization.GetString("DocumentManagementAdaptor.GetFoldersAsOption.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            finally
            {
                objXmlNode = null;
                objRootElement = null;
                objDocumentManager = null;
            }

            return bReturnValue;
        }
        public bool GetFoldersAsOptionNew(Folder objFolder,
            ref List<Folder> p_objList,
            ref BusinessAdaptorErrors p_objErrors)
        {
            bool bReturnValue = false;
            DocumentManager objDocumentManager = null;

            XmlElement objRootElement = null;
            XmlNode objXmlNode = null;

            // O and -1 are valid folder id values. Hence initialization is
            // being done with -2.
            long lSelectFolderId = -2;
            string sFoldersXml = string.Empty;


            try
            {
                //objXmlNode = p_objXmlDocIn.SelectSingleNode("//GetFoldersAsOption/SelectFolderId");

                if (objFolder.FolderID == null || objFolder.FolderID == "")
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.GetFolderAsOption.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.GetFolderAsOption.MissingSelectFolderIdNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                lSelectFolderId = Conversion.ConvertStrToLong(objFolder.FolderID);


                objDocumentManager = new DocumentManager(m_userLogin, base.ClientId);
                objDocumentManager.ConnectionString = this.connectionString;
                objDocumentManager.SecurityConnectionString = this.securityConnectionString; //MITS 27764 hlv 5/23/12
                objDocumentManager.UserLoginName = this.loginName;
                p_objList = new List<Folder>();
                objDocumentManager.GetFoldersAsOptionNew(lSelectFolderId, ref p_objList);

                bReturnValue = true;
            }
            catch (InitializationException p_objInitializationException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInitializationException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (RMAppException p_objRMAppException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objRMAppException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException, Globalization.GetString("DocumentManagementAdaptor.GetFoldersAsOption.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            finally
            {
                objXmlNode = null;
                objRootElement = null;
                objDocumentManager = null;
            }

            return bReturnValue;
        }
        public bool GetDocuments(XmlDocument p_objXmlDocIn, ref XmlDocument p_objXmlDocOut,
    ref BusinessAdaptorErrors p_objErrors)
        {
            bool bReturnValue = false;
            DocumentManager objDocumentManager = null;

            XmlElement objRootElement = null;
            XmlElement objTempElement = null;
            XmlNode objXmlNode = null;

            // O and -1 are valid folder id values. Hence initialization is
            // being done with -2.
            long lFolderId = -2;
            long lParentFolderId = -2;
            //Shruti
            // By default the records will be sorted on column 
            int lSortId = 0;

            int iPSID = 0;

            string sFolderName = string.Empty;
            string sParentFolderName = string.Empty;

            string sDocumentsXml = string.Empty;
            RMConfigurator objConfig = null;
            SysSettings objSettings = null;
            Acrosoft objAcrosoft = null;

            //************************************************
            // Changes made by Mohit Yadav to including paging
            //************************************************
            long lPageNumber = 0;
            //************************************************

            try
            {
                objDocumentManager = new DocumentManager(this.m_userLogin, base.ClientId);
                objDocumentManager.ConnectionString = this.connectionString;
                objDocumentManager.SecurityConnectionString = this.securityConnectionString; //MITS 27764 hlv 5/23/12
                objDocumentManager.UserLoginName = this.loginName;

                //Raman Bhatia..08/3/2005
                //Check if Acrosoft Interface is enabled..if enabled then we need to bypass this code

                objSettings = new SysSettings(connectionString, base.ClientId);
                if (objSettings.UseAcrosoftInterface == false)
                {

                    //************************************************
                    // Changes made by Mohit Yadav to including paging
                    //************************************************
                    objXmlNode = p_objXmlDocIn.SelectSingleNode("//GetDocuments/PageNumber");

                    if (objXmlNode == null)
                    {
                        lPageNumber = 0;
                    }
                    else
                    {
                        lPageNumber = Conversion.ConvertStrToLong(objXmlNode.InnerText);
                    }

                    objXmlNode = null;

                    objXmlNode = p_objXmlDocIn.SelectSingleNode("//GetDocuments/SortOrder");

                    if (objXmlNode == null)
                    {
                        lSortId = 0;
                    }
                    else
                    {
                        lSortId = Conversion.ConvertStrToInteger(objXmlNode.InnerText);
                    }

                    objXmlNode = null;
                    //***********************************************

                    //Mohit Yadav "Security Permission implementation"
                    //Get psid value
                    objXmlNode = p_objXmlDocIn.SelectSingleNode("//psid");
                    if (objXmlNode != null)
                    {
                        if (objXmlNode.InnerText.Trim() != "")
                        {
                            iPSID = int.Parse(objXmlNode.InnerText);
                        }
                    }
                    objXmlNode = null;

                    int iBaseSecurityId = 0;
                    iBaseSecurityId = iPSID;

                    if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_UD_VIEW))
                        throw new PermissionViolationException(RMPermissions.RMO_UD_VIEWPLAY, iBaseSecurityId);


                    objXmlNode = p_objXmlDocIn.SelectSingleNode("//GetDocuments/FolderId");

                    if (objXmlNode == null)
                    {
                        bReturnValue = false;
                        p_objErrors.Add("DocumentManagementAdaptor.GetDocuments.MissingInputParameter",
                            Globalization.GetString("DocumentManagementAdaptor.GetDocuments.MissingFolderIdNode",base.ClientId),
                            BusinessAdaptorErrorType.Error);
                        return bReturnValue;
                    }

                    if (objXmlNode.InnerText.Length == 0)
                    {
                        // FolderId is not provided as input.
                        objDocumentManager.GetDocuments(lSortId, lPageNumber, out sDocumentsXml);
                    }
                    else
                    {
                        lFolderId = Conversion.ConvertStrToLong(objXmlNode.InnerText);

                        objXmlNode = null;
                        objXmlNode = p_objXmlDocIn.SelectSingleNode("//GetDocuments/ParentFolderId");

                        if (objXmlNode == null)
                        {
                            bReturnValue = false;
                            p_objErrors.Add("DocumentManagementAdaptor.GetDocuments.MissingInputParameter",
                                Globalization.GetString("DocumentManagementAdaptor.GetDocuments.MissingParentFolderIdNode",base.ClientId),
                                BusinessAdaptorErrorType.Error);
                            return bReturnValue;
                        }

                        if (objXmlNode.InnerText.Length == 0)
                        {
                            // ParentFolderId is not provided as input.
                            objDocumentManager.GetDocuments(lSortId, lFolderId, lPageNumber, out sDocumentsXml);
                        }
                        else
                        {
                            lParentFolderId = Conversion.ConvertStrToLong(objXmlNode.InnerText);

                            objXmlNode = null;
                            objXmlNode = p_objXmlDocIn.SelectSingleNode("//GetDocuments/FolderName");

                            if (objXmlNode == null)
                            {
                                bReturnValue = false;
                                p_objErrors.Add("DocumentManagementAdaptor.GetDocuments.MissingInputParameter",
                                    Globalization.GetString("DocumentManagementAdaptor.GetDocuments.MissingFolderNameNode",base.ClientId),
                                    BusinessAdaptorErrorType.Error);
                                return bReturnValue;
                            }

                            sFolderName = objXmlNode.InnerText;

                            objXmlNode = null;
                            objXmlNode = p_objXmlDocIn.SelectSingleNode("//GetDocuments/ParentFolderName");

                            if (objXmlNode == null)
                            {
                                bReturnValue = false;
                                p_objErrors.Add("DocumentManagementAdaptor.GetDocuments.MissingInputParameter",
                                    Globalization.GetString("DocumentManagementAdaptor.GetDocuments.MissingParentFolderNameNode",base.ClientId),
                                    BusinessAdaptorErrorType.Error);
                                return bReturnValue;
                            }

                            sParentFolderName = objXmlNode.InnerText;

                            //************************************************
                            // Changes made by Mohit Yadav to including paging
                            //************************************************
                            objDocumentManager.GetDocuments(lSortId, lFolderId, lParentFolderId, sFolderName, sParentFolderName,
                                lPageNumber, out sDocumentsXml);
                            //************************************************
                        }
                    }

                    objRootElement = p_objXmlDocOut.CreateElement("GetDocumentsResult");

                    objRootElement.InnerXml = sDocumentsXml;
                    p_objXmlDocOut.AppendChild(objRootElement);

                    objTempElement = p_objXmlDocOut.CreateElement("JumpToAcrosoft");
					objTempElement.InnerXml = "false";
					objRootElement.AppendChild(objTempElement); 
                    //Mgaba2:MITS 13013:Documents were not opening when selected from Documents menu:start
                    objTempElement = p_objXmlDocOut.CreateElement("ViewAttachment");
                    if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_UD_VIEWPLAY))
                    {
                        objTempElement.InnerXml = "false";
                    }
                    else
                    {
                        objTempElement.InnerXml = "true";
                    }
                    objRootElement.AppendChild(objTempElement);
                    //Mgaba2:MITS 13013
                }
                else
                {
                    objConfig = new RMConfigurator();

                    objRootElement = p_objXmlDocOut.CreateElement("GetDocumentsResult");
                    p_objXmlDocOut.AppendChild(objRootElement);

                    objTempElement = p_objXmlDocOut.CreateElement("JumpToAcrosoft");
                    objTempElement.InnerXml = "true";
                    objRootElement.AppendChild(objTempElement);

                    objTempElement = p_objXmlDocOut.CreateElement("AcrosoftUsersTypeKey");
                    objTempElement.InnerXml = AcrosoftSection.AcrosoftUsersTypeKey;
                    objRootElement.AppendChild(objTempElement);

                    objTempElement = p_objXmlDocOut.CreateElement("JumpDestination");
                    objTempElement.InnerXml = "Users";
                    objRootElement.AppendChild(objTempElement);

                    objTempElement = p_objXmlDocOut.CreateElement("AsAnywhereLink");
                    objTempElement.InnerXml = AcrosoftSection.AsAnywhereLink;
                    objRootElement.AppendChild(objTempElement);

                    objTempElement = p_objXmlDocOut.CreateElement("AcrosoftSkin");
                    objTempElement.InnerXml = AcrosoftSection.AcrosoftSkin;
                    objRootElement.AppendChild(objTempElement);

                    objTempElement = p_objXmlDocOut.CreateElement("LoginName");
                    objTempElement.InnerXml = base.loginName;
                    objRootElement.AppendChild(objTempElement);

                    objTempElement = p_objXmlDocOut.CreateElement("UsersFolderFriendlyName");
                    objTempElement.InnerXml = AcrosoftSection.UsersFolderFriendlyName;
                    objRootElement.AppendChild(objTempElement);

                    string sAppExcpXml = "";
                    string sSessionID = "";
                    objAcrosoft = new Acrosoft(base.ClientId);//dvatsa-cloud

                    //rsolanki2 :  start updates for MCM mits 19200 
                    Boolean bSuccess = false;
                    string sTempAcrosoftUserId = base.userLogin.LoginName;
                    string sTempAcrosoftPassword = base.userLogin.Password;

                    if (Conversion.CastToType<Boolean>(AcrosoftSection.UseCommonAcrosoftUser, out bSuccess))
                    {
                        if (AcrosoftSection.AcroUserMapSection.UserMap[base.loginName] != null
                            && !string.IsNullOrEmpty(AcrosoftSection.AcroUserMapSection.UserMap[base.loginName].RmxUser))
                        {
                            sTempAcrosoftUserId = AcrosoftSection.AcroUserMapSection.UserMap[base.loginName].AcrosoftUserId;
                            sTempAcrosoftPassword = AcrosoftSection.AcroUserMapSection.UserMap[base.loginName].AcrosoftPassword;
                        }
                        else
                        {
                            sTempAcrosoftUserId = AcrosoftSection.AcrosoftUsername;
                            sTempAcrosoftPassword = AcrosoftSection.AcrosoftUserPassword;
                        }
                    }
                   
                    objAcrosoft.Authenticate(sTempAcrosoftUserId, sTempAcrosoftPassword, out sSessionID, out sAppExcpXml);
                    //objAcrosoft.Authenticate(base.userLogin.LoginName, base.userLogin.Password, out sSessionID, out sAppExcpXml);
                    
                    objTempElement = p_objXmlDocOut.CreateElement("sid");
                    if (sSessionID != null) objTempElement.InnerXml = sSessionID;
                    else objTempElement.InnerXml = "";
                    objRootElement.AppendChild(objTempElement);


                    //Creating Folder in Acrosoft
                    objAcrosoft.CreateUserFolder(sTempAcrosoftUserId, sTempAcrosoftPassword, AcrosoftSection.AcrosoftUsersTypeKey, 
                        sTempAcrosoftUserId, AcrosoftSection.UsersFolderFriendlyName, out sAppExcpXml);
                    //rsolanki2 :  end updates for MCM mits 19200 
                } // else

                bReturnValue = true;
            }
            catch (InitializationException p_objInitializationException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInitializationException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInvalidValueException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (RMAppException p_objRMAppException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objRMAppException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException, Globalization.GetString("DocumentManagementAdaptor.GetDocuments.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            finally
            {
                objXmlNode = null;
                objRootElement = null;
                objDocumentManager = null;
                objConfig = null;
                objSettings = null;
                objAcrosoft = null;
            }

            return bReturnValue;
        }
        public bool GetDocumentsNew(DocumentListRequest p_objIn, ref DocumentList p_objOut,
            ref BusinessAdaptorErrors p_objErrors)
        {
            bool bReturnValue = false;
            DocumentManager objDocumentManager = null;

            XmlElement objRootElement = null;
            XmlElement objTempElement = null;
            XmlNode objXmlNode = null;

            // O and -1 are valid folder id values. Hence initialization is
            // being done with -2.
            long lFolderId = -2;
            long lParentFolderId = -2;
            //Shruti
            // By default the records will be sorted on column 
            int lSortId = 0;

            int iPSID = 0;

            string sFolderName = string.Empty;
            string sParentFolderName = string.Empty;

            string sDocumentsXml = string.Empty;
            RMConfigurator objConfig = null;
            SysSettings objSettings = null;
            Acrosoft objAcrosoft = null;

            //Mona:PaperVisionMerge : Animesh Inserted 
            PVWrapper objPaperVision = null;
            PaperVisionDocumentManager objPvDocManager = null;
            //Animesh Insertion Ends
            //************************************************
            // Changes made by Mohit Yadav to including paging
            //************************************************
            long lPageNumber = 0;
            //************************************************

            try
            {
                p_objOut = new DocumentList();
                objDocumentManager = new DocumentManager(this.m_userLogin, base.ClientId);
                objDocumentManager.ConnectionString = this.connectionString;
                objDocumentManager.SecurityConnectionString = this.securityConnectionString; //MITS 27764 hlv 5/23/12
                objDocumentManager.UserLoginName = this.loginName;

                //Raman Bhatia..08/3/2005
                //Check if Acrosoft Interface is enabled..if enabled then we need to bypass this code

                objSettings = new SysSettings(connectionString,base.ClientId);
                //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                if (objSettings.UsePaperVisionInterface)
                {
                    //Animesh Inserted MITS 18345
                    p_objOut.PaperVisionActive = "True";  
                    //Animesh Insertion Ends
                    objConfig = new RMConfigurator();
                    objPvDocManager = new PaperVisionDocumentManager(this.m_userLogin, base.ClientId);
                    objPvDocManager.ConnectionString = this.connectionString;
                    objPvDocManager.UserLoginName = this.loginName;
                    objPaperVision = new PVWrapper();
                    objPaperVision.PVEntityID = PaperVisionSection.EntityID;
                    objPaperVision.PVLoginUrl = PaperVisionSection.LoginUrl;
                    objPaperVision.PVProjID = PaperVisionSection.ProjectID;
                    objPaperVision.PVServerAddress = PaperVisionSection.PVServerAddress;
                    objPaperVision.PVShowDocLinkUrl = PaperVisionSection.DocumentLink;
                    //Animesh Inserted MITS 18345
                    objPaperVision.SharedNetworkLocationPath = PaperVisionSection.SharedNetworkLocationPath;
                    //objPaperVision.MappedDriveName = PaperVisionSection.MappedDriveName;    
                    objPaperVision.DomainName = PaperVisionSection.DomainName;
                    objPaperVision.UserID = PaperVisionSection.UserID;
                    objPaperVision.Password = PaperVisionSection.Password;   
                    //Animesh Insertion ends
                    if (p_objIn.PageNumber == "")
                    {
                        lPageNumber = 0;
                    }
                    else
                    {
                        lPageNumber = Conversion.ConvertStrToLong(p_objIn.PageNumber);
                    }
                    if (p_objIn.SortOrder == "")
                    {
                        lSortId = 0;
                    }
                    else
                    {
                        lSortId = Conversion.ConvertStrToInteger(p_objIn.SortOrder);
                    }
                    objXmlNode = null;
                    if (p_objIn.Psid != null)
                    {
                        if (p_objIn.Psid.Trim() != "")
                        {
                            iPSID = int.Parse(p_objIn.Psid);
                        }
                    }
                    objXmlNode = null;
                    int iBaseSecurityId = 0;
                    iBaseSecurityId = iPSID;
                    if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_UD_VIEW))
                        throw new PermissionViolationException(RMPermissions.RMO_UD_VIEWPLAY, iBaseSecurityId);
                    if (p_objIn.FolderId == null)
                    {
                        bReturnValue = false;
                        p_objErrors.Add("DocumentManagementAdaptor.GetDocuments.MissingInputParameter",
                            Globalization.GetString("DocumentManagementAdaptor.GetDocuments.MissingFolderIdNode",base.ClientId),
                            BusinessAdaptorErrorType.Error);
                        return bReturnValue;
                    }
                    objPvDocManager.GetDocumentsNew(lSortId, lPageNumber, out p_objOut);
                }
                //Animesh Insertion ends
                else if (objSettings.UseAcrosoftInterface == false)
                {

                    //************************************************
                    // Changes made by Mohit Yadav to including paging
                    //************************************************
                    //objXmlNode = p_objXmlDocIn.SelectSingleNode("//GetDocuments/PageNumber");

                    if (p_objIn.PageNumber == "")
                    {
                        lPageNumber = 0;
                    }
                    else
                    {
                        lPageNumber = Conversion.ConvertStrToLong(p_objIn.PageNumber);
                    }

                    //objXmlNode = null;

                    //objXmlNode = p_objXmlDocIn.SelectSingleNode("//GetDocuments/SortOrder");

                    if (p_objIn.SortOrder == "")
                    {
                        lSortId = 0;
                    }
                    else
                    {
                        lSortId = Conversion.ConvertStrToInteger(p_objIn.SortOrder);
                    }

                    objXmlNode = null;
                    //***********************************************

                    //Mohit Yadav "Security Permission implementation"
                    //Get psid value

                    if (p_objIn.Psid != null)
                    {
                        if (p_objIn.Psid.Trim() != "")
                        {
                            iPSID = int.Parse(p_objIn.Psid);
                        }
                    }
                    objXmlNode = null;

                    int iBaseSecurityId = 0;
                    iBaseSecurityId = iPSID;

                    if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_UD_VIEW))
                        throw new PermissionViolationException(RMPermissions.RMO_UD_VIEWPLAY, iBaseSecurityId);

                    if (!SetDocumentStorageTypeAndPath(ref objDocumentManager, ref p_objErrors))
                    {
                        return false;
                    }
                    //objXmlNode = p_objXmlDocIn.SelectSingleNode("//GetDocuments/FolderId");

                    if (p_objIn.FolderId == null)
                    {
                        bReturnValue = false;
                        p_objErrors.Add("DocumentManagementAdaptor.GetDocuments.MissingInputParameter",
                            Globalization.GetString("DocumentManagementAdaptor.GetDocuments.MissingFolderIdNode",base.ClientId),
                            BusinessAdaptorErrorType.Error);
                        return bReturnValue;
                    }

                    if (p_objIn.FolderId.Length == 0)
                    {
                        // FolderId is not provided as input.
                        objDocumentManager.GetDocumentsNew(lSortId, lPageNumber, out p_objOut);
                    }
                    else
                    {
                        lFolderId = Conversion.ConvertStrToLong(p_objIn.FolderId);

                        //objXmlNode = null;
                        //p_objInp_objInobjXmlNode = p_objXmlDocIn.SelectSingleNode("//GetDocuments/ParentFolderId");

                        if (p_objIn.ParentFolderId == null)
                        {
                            bReturnValue = false;
                            p_objErrors.Add("DocumentManagementAdaptor.GetDocuments.MissingInputParameter",
                                Globalization.GetString("DocumentManagementAdaptor.GetDocuments.MissingParentFolderIdNode",base.ClientId),
                                BusinessAdaptorErrorType.Error);
                            return bReturnValue;
                        }

                        if (p_objIn.ParentFolderId.Length == 0)
                        {
                            // ParentFolderId is not provided as input.
                            objDocumentManager.GetDocumentsNew(lSortId, lFolderId,lPageNumber,out p_objOut);
                        }
                        else
                        {
                            lParentFolderId = Conversion.ConvertStrToLong(p_objIn.ParentFolderId);

                            objXmlNode = null;
                            //objXmlNode = p_objXmlDocIn.SelectSingleNode("//GetDocuments/FolderName");

                            if (p_objIn.FolderName == null)
                            {
                                bReturnValue = false;
                                p_objErrors.Add("DocumentManagementAdaptor.GetDocuments.MissingInputParameter",
                                    Globalization.GetString("DocumentManagementAdaptor.GetDocuments.MissingFolderNameNode",base.ClientId),
                                    BusinessAdaptorErrorType.Error);
                                return bReturnValue;
                            }

                            sFolderName = p_objIn.FolderName;

                            objXmlNode = null;
                            //objXmlNode = p_objXmlDocIn.SelectSingleNode("//GetDocuments/ParentFolderName");

                            if (p_objIn.ParentFolderName == null)
                            {
                                bReturnValue = false;
                                p_objErrors.Add("DocumentManagementAdaptor.GetDocuments.MissingInputParameter",
                                    Globalization.GetString("DocumentManagementAdaptor.GetDocuments.MissingParentFolderNameNode",base.ClientId),
                                    BusinessAdaptorErrorType.Error);
                                return bReturnValue;
                            }

                            sParentFolderName = p_objIn.ParentFolderName;

                            //************************************************
                            // Changes made by Mohit Yadav to including paging
                            //************************************************
                            objDocumentManager.GetDocumentsNew(lSortId, lFolderId, lParentFolderId, sFolderName, sParentFolderName,
                                lPageNumber, out p_objOut);
                            //************************************************
                        }
                    }


                    p_objOut.JumpToAcrosoft = "0";

                    //Mgaba2:MITS 13013:Documents were not opening when selected from Documents menu:start
                    if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_UD_VIEWPLAY))
                    {
                        p_objOut.ViewAttachment = "false";
                    }
                    else
                    {
                        p_objOut.ViewAttachment = "true";
                    }
                    //Mgaba2:MITS 13013
                    //smishra25 Start:Flag which should be set when Use Outlook to email is 
                    //turned on in General System Parameter Setup
                    p_objOut.UseOutlookToEmail = objSettings.UseOutlook;
                    p_objOut.SizeLimitOutlook = objSettings.FileSizeLimitForOutlook;
                    //smishra25 End:Flag which should be set when Use Outlook to email is

                }
                else
                {
                    objConfig = new RMConfigurator();

                    p_objOut.JumpToAcrosoft = "1";

                    p_objOut.AcrosoftUsersTypeKey = AcrosoftSection.AcrosoftUsersTypeKey;

                    p_objOut.JumpDestination = "Users";

                    p_objOut.AsAnywhereLink = AcrosoftSection.AsAnywhereLink;

                    p_objOut.AcrosoftSkin = AcrosoftSection.AcrosoftSkin;

                    p_objOut.UsersFolderFriendlyName = AcrosoftSection.UsersFolderFriendlyName;

                    //p_objOut.AcrosoftLoginName = base.loginName;

                    p_objOut.UsersFolderFriendlyName = AcrosoftSection.UsersFolderFriendlyName;


                    string sAppExcpXml = "";
                    string sSessionID = "";
                    objAcrosoft = new Acrosoft(base.ClientId);//dvatsa-cloud
                    //rsolanki2 :  start updates for MCM mits 19200 
                    Boolean bSuccess = false;
                    string sTempAcrosoftUserId = base.userLogin.LoginName;
                    string sTempAcrosoftPassword = base.userLogin.Password;

                    if (Conversion.CastToType<Boolean>(AcrosoftSection.UseCommonAcrosoftUser, out bSuccess))
                    {
                        if (AcrosoftSection.AcroUserMapSection.UserMap[base.loginName] != null
                            && !string.IsNullOrEmpty(AcrosoftSection.AcroUserMapSection.UserMap[base.loginName].RmxUser))
                        {
                            sTempAcrosoftUserId = AcrosoftSection.AcroUserMapSection.UserMap[base.loginName].AcrosoftUserId;
                            sTempAcrosoftPassword = AcrosoftSection.AcroUserMapSection.UserMap[base.loginName].AcrosoftPassword;
                        }
                        else
                        {
                            sTempAcrosoftUserId = AcrosoftSection.AcrosoftUsername;
                            sTempAcrosoftPassword = AcrosoftSection.AcrosoftUserPassword;
                        }
                    }
                    objAcrosoft.Authenticate(sTempAcrosoftUserId, sTempAcrosoftPassword, out sSessionID, out sAppExcpXml);
                    //objAcrosoft.Authenticate(base.userLogin.LoginName, base.userLogin.Password, out sSessionID, out sAppExcpXml);
                    //rsolanki2 :  end updates for MCM mits 19200 

                    p_objOut.AcrosoftLoginName = sTempAcrosoftUserId;                    

                    p_objOut.AcrosoftSession = sSessionID;


                    //Creating Folder in Acrosoft
                    objAcrosoft.CreateUserFolder(sTempAcrosoftUserId, sTempAcrosoftPassword, AcrosoftSection.AcrosoftUsersTypeKey, 
                        sTempAcrosoftUserId, AcrosoftSection.UsersFolderFriendlyName, out sAppExcpXml);
                } // else

                bReturnValue = true;
            }
            catch (InitializationException p_objInitializationException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInitializationException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInvalidValueException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (RMAppException p_objRMAppException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objRMAppException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException, Globalization.GetString("DocumentManagementAdaptor.GetDocuments.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            finally
            {
                objXmlNode = null;
                objRootElement = null;
                objDocumentManager = null;
                objConfig = null;
                objSettings = null;
                objAcrosoft = null;
                //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                objPaperVision = null;
                objPvDocManager = null;
                //Animesh Inserted MITS 16697
            }

            return bReturnValue;
        }

        /// Name			: GetDocumentDetails
        /// Author			: Aditya Babbar
        /// Date Created	: 15-Mar-2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Wrapper to DocumentManager.GetDocumentDetails method of 
        /// application layer that retrieves details for a document. 
        /// </summary>
        /// <param name="p_objXmlDocIn"> 
        ///	Input paramters in xml format.
        ///	Structure:
        ///		<GetDocumentDetails>
        ///			<DocumentId>
        ///				Id of the document whose details are to be retrieved
        ///			</DocumentId>	
        ///		</GetDocumentDetails>
        /// </param>
        /// <param name="objDocument"> 
        ///	Method output in xml format.
        ///	Structure:
        ///		<GetDocumentDetailsResult>
        ///			<data>
        ///				<Document>
        ///					<DocumentId/>
        ///					<FolderId/>
        ///					<CreateDate/>
        ///					<Category/>
        ///					<Name/>
        ///					<Class/>
        ///					<Subject/>
        ///					<Type/>
        ///					<Notes/>
        ///					<UserLoginName/>
        ///					<FileName/>
        ///					<FilePath/>
        ///					<Keywords/>
        ///					<AttachTable/>
        ///					<AttachRecordId/>
        ///				</Document>
        ///			</data>
        ///		</GetDocumentDetailsResult>
        ///	</param>
        ///	<param name="outDocument">
        /// <param name="p_objErrors">
        ///	Collection of errors/messages.	
        ///	</param>
        /// <returns>
        ///		true - success
        ///		false - failure
        ///	</returns>
        public bool GetDocumentDetailsIntegrated(DocumentType objDocument, out DocumentType outDocument,

            ref BusinessAdaptorErrors p_objErrors)
        {
            bool bReturnValue = false;
            int iPSID = 0;
            DocumentManager objDocumentManager = null;
            int lDocumentId = 0;
            //Mohit Yadav for Security Permission implementation
            string sScreenFlag = string.Empty;
            string sScreenName = string.Empty;
            string sDocumentXml = string.Empty;
            outDocument = new DocumentType();
            //smishra25 :Flag which should be set when Use Outlook to email is 
            //turned on in General System Parameter Setup
            SysSettings objSettings = null;
            //Mona:PaperVisionMerge : Animesh Inserted 
            PVWrapper objPaperVision = null;
            PaperVisionDocumentManager objPVDocumentManager = null;
            RMConfigurator objConfig = null;
            //Animesh Insertion Ends
            try
            {
                //smishra25 :Flag which should be set when Use Outlook to email is 
                //turned on in General System Parameter Setup
                objSettings = new SysSettings(connectionString,base.ClientId);
                if (objDocument.DocumentId == 0)
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.GetDocumentDetails.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.GetDocumentDetails.MissingDocumentIdNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }
                lDocumentId = objDocument.DocumentId;
                sScreenFlag = objDocument.ScreenFlag;
                sScreenName = objDocument.FormName;

                //Animesh Inserted 
                objConfig = new RMConfigurator();
                if (objSettings.UsePaperVisionInterface == true)
                {
                    objPVDocumentManager = new PaperVisionDocumentManager(m_userLogin, base.ClientId);
                    objPVDocumentManager.ConnectionString = this.connectionString;
                    objPVDocumentManager.UserLoginName = this.loginName;
                    if (objDocument.PsId != null && objDocument.PsId != 0)
                        iPSID = objDocument.PsId;
                    int iBaseSecurityId = 0;
                    iBaseSecurityId = iPSID;
                    if (sScreenFlag == "Files")
                    {
                        if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_UD_VIEWPLAY))
                            throw new PermissionViolationException(RMPermissions.RMO_UD_VIEWPLAY, iBaseSecurityId);
                    }
                    else
                    {
                        if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_ATT_VIEWPLAYPRINT))
                            throw new PermissionViolationException(RMPermissions.RMO_ATT_VIEWPLAYPRINT, iBaseSecurityId);
                    }
                    objPaperVision = new PVWrapper();
                    objConfig = new RMConfigurator();
                    objPaperVision.PVEntityID = PaperVisionSection.EntityID;
                    objPaperVision.PVLoginUrl = PaperVisionSection.LoginUrl;
                    objPaperVision.PVProjID = PaperVisionSection.ProjectID;
                    objPaperVision.PVShowDocLinkUrl = PaperVisionSection.DocumentLink;
                    objPaperVision.PVServerAddress = PaperVisionSection.PVServerAddress;
                    //Animesh Inserted MITS 18345
                    objPaperVision.SharedNetworkLocationPath = PaperVisionSection.SharedNetworkLocationPath;
                    //objPaperVision.MappedDriveName = PaperVisionSection.MappedDriveName;    
                    objPaperVision.DomainName = PaperVisionSection.DomainName;
                    objPaperVision.UserID = PaperVisionSection.UserID;
                    objPaperVision.Password = PaperVisionSection.Password;   
                    //Anmiemsh insertion ends
                    objPVDocumentManager.GetDocumentDetails(userLogin.LoginName, userLogin.Password, objPaperVision, lDocumentId, iBaseSecurityId, sScreenFlag, out sDocumentXml);
                    XmlDocument p_objXmlDocOut = new XmlDocument();
                    p_objXmlDocOut.LoadXml(sDocumentXml);
                    outDocument.View = Conversion.ConvertStrToInteger(p_objXmlDocOut.SelectSingleNode("//@view_allowed").InnerText);
                    outDocument.Create = Conversion.ConvertStrToInteger(p_objXmlDocOut.SelectSingleNode("//@create_allowed").InnerText);
                    outDocument.Edit = Conversion.ConvertStrToInteger(p_objXmlDocOut.SelectSingleNode("//@edit_allowed").InnerText);
                    outDocument.Delete = Conversion.ConvertStrToInteger(p_objXmlDocOut.SelectSingleNode("//@delete_allowed").InnerText);
                    outDocument.Transfer = Conversion.ConvertStrToInteger(p_objXmlDocOut.SelectSingleNode("//@transfer_allowed").InnerText);
                    outDocument.Copy = Conversion.ConvertStrToInteger(p_objXmlDocOut.SelectSingleNode("//@copy_allowed").InnerText);
                    outDocument.Download = Conversion.ConvertStrToInteger(p_objXmlDocOut.SelectSingleNode("//@download_allowed").InnerText);
                    outDocument.Email = Conversion.ConvertStrToInteger(p_objXmlDocOut.SelectSingleNode("//@email_allowed").InnerText);
                    outDocument.Move = Conversion.ConvertStrToInteger(p_objXmlDocOut.SelectSingleNode("//@move_allowed").InnerText);
                    outDocument.DocumentId = Conversion.ConvertStrToInteger(p_objXmlDocOut.SelectSingleNode("//DocumentId").InnerText);
                    outDocument.Title = p_objXmlDocOut.SelectSingleNode("//Name").InnerText;
                    outDocument.Notes = p_objXmlDocOut.SelectSingleNode("//Notes").InnerText;
                    outDocument.FileName = p_objXmlDocOut.SelectSingleNode("//FileName").InnerText;
                    outDocument.CreateDate = p_objXmlDocOut.SelectSingleNode("//CreateDate").InnerText;
                    outDocument.Category.Desc = p_objXmlDocOut.SelectSingleNode("//Category").InnerText;
                    outDocument.Category.Id = Conversion.ConvertStrToInteger(p_objXmlDocOut.SelectSingleNode("//Category-cid").InnerText);
                    outDocument.Class.Desc = p_objXmlDocOut.SelectSingleNode("//Class").InnerText;
                    outDocument.Class.Id = Conversion.ConvertStrToInteger(p_objXmlDocOut.SelectSingleNode("//Class-cid").InnerText);
                    outDocument.Subject = p_objXmlDocOut.SelectSingleNode("//Subject").InnerText;
                    outDocument.Type.Desc = p_objXmlDocOut.SelectSingleNode("//Type").InnerText;
                    outDocument.Type.Id = Conversion.ConvertStrToInteger(p_objXmlDocOut.SelectSingleNode("//Type-cid").InnerText);
                    outDocument.UserName = p_objXmlDocOut.SelectSingleNode("//UserLoginName").InnerText;
                    outDocument.Keywords = p_objXmlDocOut.SelectSingleNode("//Keywords").InnerText;
                    outDocument.AttachTable = p_objXmlDocOut.SelectSingleNode("//AttachTable").InnerText;
                    outDocument.AttachRecordId = Conversion.ConvertStrToInteger(p_objXmlDocOut.SelectSingleNode("//AttachRecordId").InnerText);
                    outDocument.PsId = Conversion.ConvertStrToInteger(p_objXmlDocOut.SelectSingleNode("//psid").InnerText);
                    outDocument.IsAtPaperVision = Conversion.ConvertObjToStr(Conversion.ConvertStrToInteger(p_objXmlDocOut.SelectSingleNode("//PaperVisionFlag").InnerText));
                    outDocument.PaperVisionUrl = p_objXmlDocOut.SelectSingleNode("//PapervisionUrl").InnerText;
                    bReturnValue = true;
                }
                else
                {
                    //Animesh Insertion Ends

                objDocumentManager = new DocumentManager(m_userLogin,base.ClientId);
                objDocumentManager.ConnectionString = this.connectionString;
                objDocumentManager.SecurityConnectionString = this.securityConnectionString; //MITS 27764 hlv 5/23/12
                objDocumentManager.UserLoginName = this.loginName;

                if (this.userLogin.DocumentPath.Length > 0)
                {
                    // use StoragePath set for the user
                    objDocumentManager.DestinationStoragePath =
                        this.userLogin.DocumentPath;
                    //mgaba2:MITS 11704-04/07/2008-doc path entered at user level has to be file system
                    objDocumentManager.DocumentStorageType = StorageType.FileSystemStorage;
                }
                else if (this.userLogin.objRiskmasterDatabase.GlobalDocPath.Length > 0)
                {
                    // use StoragePath set for the DSN
                    objDocumentManager.DestinationStoragePath =
                        this.userLogin.objRiskmasterDatabase.GlobalDocPath;
                    //mgaba2:MITS 11704-04/07/2008-if no doc path entered at user level
                    objDocumentManager.DocumentStorageType =
                    (StorageType)this.userLogin.objRiskmasterDatabase.DocPathType;
                }
                else
                {
                    System.Exception exc = new Exception("Document path invalid or not set. Consult your System Administrator. Tip: You can define Document Path in Security Management System.");
                    p_objErrors.Add(exc, "Document path invalid or not set. Consult your System Administrator. Tip: You can define Document Path in Security Management System.", BusinessAdaptorErrorType.Error);
                    return false;
                }


                if (objDocument.PsId != null && objDocument.PsId != 0)
                    iPSID = objDocument.PsId;
                //Mohit Yadav for Security Permission implementation
                int iBaseSecurityId = 0;
                iBaseSecurityId = iPSID;

                //MGaba2:MITS 13013:Added if condition to handle funds case separately:start
                //Documents were not opened  from funds screen if Module Security is ON
                if (sScreenName.ToLower() == "funds")
                {
                    iBaseSecurityId = UserPermissions.RMB_FUNDS_TRANSACT;
                }
                else
                {//MGaba2:MITS 13013:End
                    //For Admin tracking Security: TR-2527
                    iBaseSecurityId = objDocumentManager.GetBaseSecurityIDForTable(iBaseSecurityId, sScreenName);
                }//MGaba2:MITS 13013

               
                if (sScreenFlag == "Files")
                {
                    if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_UD_VIEWPLAY))
                        throw new PermissionViolationException(RMPermissions.RMO_UD_VIEWPLAY, iBaseSecurityId);
                }
                else
                {
                    if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_ATT_VIEWPLAYPRINT))
                        throw new PermissionViolationException(RMPermissions.RMO_ATT_VIEWPLAYPRINT, iBaseSecurityId);
                }

                //If error was occured while uploading this doc in UI ,then need to update Upload Status to 3
                if (objDocument.Upload_Status == 3)
                {
                    objDocumentManager.UpdateUploadStatusOnUIError(lDocumentId, objDocument.Upload_Status);
                }

                // JP 12.18.2006    Wrong security id passed in.    objDocumentManager.GetDocumentDetails(lDocumentId, iPSID, out sDocumentXml);
                objDocumentManager.GetDocumentDetails(lDocumentId, iBaseSecurityId, sScreenFlag, out sDocumentXml);//Umesh
                XmlDocument p_objXmlDocOut = new XmlDocument();
                p_objXmlDocOut.LoadXml(sDocumentXml);
                outDocument.View = Conversion.ConvertStrToInteger(p_objXmlDocOut.SelectSingleNode("//@view_allowed").InnerText);
                outDocument.Create = Conversion.ConvertStrToInteger(p_objXmlDocOut.SelectSingleNode("//@create_allowed").InnerText);
                outDocument.Edit = Conversion.ConvertStrToInteger(p_objXmlDocOut.SelectSingleNode("//@edit_allowed").InnerText);
                outDocument.Delete = Conversion.ConvertStrToInteger(p_objXmlDocOut.SelectSingleNode("//@delete_allowed").InnerText);
                outDocument.Transfer = Conversion.ConvertStrToInteger(p_objXmlDocOut.SelectSingleNode("//@transfer_allowed").InnerText);
                outDocument.Copy = Conversion.ConvertStrToInteger(p_objXmlDocOut.SelectSingleNode("//@copy_allowed").InnerText);
                outDocument.Download = Conversion.ConvertStrToInteger(p_objXmlDocOut.SelectSingleNode("//@download_allowed").InnerText);
                outDocument.Email = Conversion.ConvertStrToInteger(p_objXmlDocOut.SelectSingleNode("//@email_allowed").InnerText);
                outDocument.Move = Conversion.ConvertStrToInteger(p_objXmlDocOut.SelectSingleNode("//@move_allowed").InnerText);
                outDocument.DocumentId = Conversion.ConvertStrToInteger(p_objXmlDocOut.SelectSingleNode("//DocumentId").InnerText);
                outDocument.Title = p_objXmlDocOut.SelectSingleNode("//Name").InnerText;
                outDocument.Notes = p_objXmlDocOut.SelectSingleNode("//Notes").InnerText;
                outDocument.FileName = p_objXmlDocOut.SelectSingleNode("//FileName").InnerText;
                outDocument.FolderId = p_objXmlDocOut.SelectSingleNode("//FolderId").InnerText;
                outDocument.CreateDate = p_objXmlDocOut.SelectSingleNode("//CreateDate").InnerText;
                outDocument.Category.Desc = p_objXmlDocOut.SelectSingleNode("//Category").InnerText;
                outDocument.Category.Id = Conversion.ConvertStrToInteger(p_objXmlDocOut.SelectSingleNode("//Category-cid").InnerText);
                outDocument.Class.Desc = p_objXmlDocOut.SelectSingleNode("//Class").InnerText;
                outDocument.Class.Id = Conversion.ConvertStrToInteger(p_objXmlDocOut.SelectSingleNode("//Class-cid").InnerText);
                outDocument.Subject = p_objXmlDocOut.SelectSingleNode("//Subject").InnerText;
                outDocument.Type.Desc = p_objXmlDocOut.SelectSingleNode("//Type").InnerText;
                outDocument.Type.Id = Conversion.ConvertStrToInteger(p_objXmlDocOut.SelectSingleNode("//Type-cid").InnerText);
                outDocument.UserName = p_objXmlDocOut.SelectSingleNode("//UserLoginName").InnerText;
                outDocument.FilePath = p_objXmlDocOut.SelectSingleNode("//FilePath").InnerText;
                outDocument.Keywords = p_objXmlDocOut.SelectSingleNode("//Keywords").InnerText;
                outDocument.AttachTable = p_objXmlDocOut.SelectSingleNode("//AttachTable").InnerText;
                outDocument.DocInternalType = p_objXmlDocOut.SelectSingleNode("//DocInternalType").InnerText;
                outDocument.AttachRecordId = Conversion.ConvertStrToInteger(p_objXmlDocOut.SelectSingleNode("//AttachRecordId").InnerText);
                outDocument.PsId = Conversion.ConvertStrToInteger(p_objXmlDocOut.SelectSingleNode("//psid").InnerText);
                //Added by Nitin for Async Upload document
                outDocument.Upload_Status = Conversion.ConvertStrToInteger(p_objXmlDocOut.SelectSingleNode("//UploadStatus").InnerText);
                
                //Get Document Size
                outDocument.FileSize = objDocumentManager.GetFileSize(outDocument.FileName); 

                //smishra25 :Flag which should be set when Use Outlook to email is 
                //turned on in General System Parameter Setup
                outDocument.UseOutlookToEmail = objSettings.UseOutlook;
                //smishra25:Outlook file size limit
                outDocument.SizeLimitOutlook = objSettings.FileSizeLimitForOutlook;
                bReturnValue = true;
                }
            }
            catch (InitializationException p_objInitializationException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInitializationException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (RMAppException p_objRMAppException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objRMAppException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException, Globalization.GetString("DocumentManagementAdaptor.GetDocumentDetails.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            finally
            {
                objDocumentManager = null;
                //Mona:PaperVisionMerge : Animesh Inserted 
                objPaperVision = null;
                objPVDocumentManager = null;
                objConfig = null; 
                //Animesh insertion ends

                objSettings = null;
            }

            return bReturnValue;
        }

        public bool GetDocumentDetails(XmlDocument p_objXmlDocIn,
            ref XmlDocument p_objXmlDocOut,
            ref BusinessAdaptorErrors p_objErrors)
        {
            bool bReturnValue = false;
            int iPSID = 0;
            DocumentManager objDocumentManager = null;

            XmlElement objRootElement = null;
            XmlNode objXmlNode = null;

            long lDocumentId = 0;
            //Mohit Yadav for Security Permission implementation
            string sScreenFlag = string.Empty;
            string sScreenName = string.Empty;

            string sDocumentXml = string.Empty;

            try
            {
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//GetDocumentDetails/DocumentId");

                if (objXmlNode == null)
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.GetDocumentDetails.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.GetDocumentDetails.MissingDocumentIdNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                lDocumentId = Conversion.ConvertStrToLong(objXmlNode.InnerText);

                //Mohit Yadav for Security Permission implementation
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//GetDocumentDetails/ScreenFlag");
                sScreenFlag = objXmlNode.InnerText.ToString();
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//GetDocumentDetails/FormName");
                sScreenName = objXmlNode.InnerText.ToString();


                objDocumentManager = new DocumentManager(m_userLogin, base.ClientId);
                objDocumentManager.ConnectionString = this.connectionString;
                objDocumentManager.SecurityConnectionString = this.securityConnectionString; //MITS 27764 hlv 5/23/12
                objDocumentManager.UserLoginName = this.loginName;

                //Get psid value
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//psid");
                if (objXmlNode != null)
                {
                    if (objXmlNode.InnerText.Trim() != "")
                    {
                        iPSID = int.Parse(objXmlNode.InnerText);
                    }
                }
                objXmlNode = null;

                //Mohit Yadav for Security Permission implementation
                int iBaseSecurityId = 0;
                iBaseSecurityId = iPSID;

                //MGaba2:MITS 13013:Added if condition to handle funds case separately:start
                //Documents were not opened  from funds screen if Module Security is ON
                if (sScreenName.ToLower() == "funds")
                {
                    iBaseSecurityId = UserPermissions.RMB_FUNDS_TRANSACT;
                }
                else
                {//MGaba2:MITS 13013:End
				//For Admin tracking Security: TR-2527
				iBaseSecurityId = objDocumentManager.GetBaseSecurityIDForTable(iBaseSecurityId,sScreenName); 
                }//MGaba2:MITS 13013
				if(sScreenFlag=="Files")
                {
                    if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_UD_VIEWPLAY))
                        throw new PermissionViolationException(RMPermissions.RMO_UD_VIEWPLAY, iBaseSecurityId);
                }
                else
                {
                    if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_ATT_VIEWPLAYPRINT))
                        throw new PermissionViolationException(RMPermissions.RMO_ATT_VIEWPLAYPRINT, iBaseSecurityId);
                }

                // JP 12.18.2006    Wrong security id passed in.    objDocumentManager.GetDocumentDetails(lDocumentId, iPSID, out sDocumentXml);
                objDocumentManager.GetDocumentDetails(lDocumentId, iBaseSecurityId, sScreenFlag, out sDocumentXml);//Umesh

                objRootElement = p_objXmlDocOut.CreateElement("GetDocumentDetailsResult");

                objRootElement.InnerXml = sDocumentXml;
                p_objXmlDocOut.AppendChild(objRootElement);

                bReturnValue = true;
            }
            catch (InitializationException p_objInitializationException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInitializationException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (RMAppException p_objRMAppException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objRMAppException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException, Globalization.GetString("DocumentManagementAdaptor.GetDocumentDetails.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            finally
            {
                objXmlNode = null;
                objRootElement = null;
                objDocumentManager = null;
            }

            return bReturnValue;
        }

        /// Name			: MoveDocuments
        /// Author			: Aditya Babbar
        /// Date Created	: 15-Mar-2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Wrapper to DocumentManager.MoveDocuments of application layer
        /// that moves documents to a different folder.
        /// </summary>
        /// <param name="p_objXmlDocIn"> 
        ///	Input paramters in xml format.
        ///	Structure:
        ///		<MoveDocuments>
        ///			<DocumentIds>
        ///				Comma separated list of DocumentIds to be moved
        ///			</DocumentIds>
        ///			<NewFolderId>
        ///				Id of the new folder
        ///			</NewFolderId>	
        ///		</MoveDocuments>
        /// </param>
        /// <param name="p_objXmlDocOut"> 
        ///		Empty		
        ///	</param>
        /// <param name="p_objErrors">
        ///	Collection of errors/messages.	
        ///	</param>
        /// <returns>
        ///		true - success
        ///		false - failure
        ///	</returns>
        public bool MoveDocuments(XmlDocument p_objXmlDocIn,
            ref XmlDocument p_objXmlDocOut,
            ref BusinessAdaptorErrors p_objErrors)
        {
            bool bReturnValue = false;
            DocumentManager objDocumentManager = null;

            XmlNode objXmlNode = null;

            string sDocumentIds = string.Empty;

            // O and -1 are valid folder id values. Hence initialization is
            // being done with -2.
            long lNewFolderId = -2;

            try
            {
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//MoveDocuments/DocumentIds");

                if (objXmlNode == null)
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.MoveDocuments.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.MoveDocuments.MissingDocumentIdsNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                sDocumentIds = objXmlNode.InnerText;

                objXmlNode = null;
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//MoveDocuments/NewFolderId");

                if (objXmlNode == null)
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.MoveDocuments.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.MoveDocuments.MissingNewFolderIdNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                lNewFolderId = Conversion.ConvertStrToLong(objXmlNode.InnerText);


                objDocumentManager = new DocumentManager(m_userLogin, base.ClientId);
                objDocumentManager.ConnectionString = this.connectionString;
                objDocumentManager.SecurityConnectionString = this.securityConnectionString; //MITS 27764 hlv 5/23/12
                objDocumentManager.UserLoginName = this.loginName;

                objDocumentManager.MoveDocuments(sDocumentIds, lNewFolderId);

                bReturnValue = true;
            }
            catch (InitializationException p_objInitializationException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInitializationException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInvalidValueException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (RMAppException p_objRMAppException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objRMAppException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException, Globalization.GetString("DocumentManagementAdaptor.MoveDocuments.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            finally
            {
                objXmlNode = null;
                objDocumentManager = null;
            }

            return bReturnValue;
        }
        public bool MoveDocumentsNew(MoveDocumentsRequest p_objDocs,
              ref BusinessAdaptorErrors p_objErrors)
        {
            bool bReturnValue = false;
            DocumentManager objDocumentManager = null;

            XmlNode objXmlNode = null;

            string sDocumentIds = string.Empty;

            // O and -1 are valid folder id values. Hence initialization is
            // being done with -2.
            long lNewFolderId = -2;

            try
            {
                // objXmlNode = p_objXmlDocIn.SelectSingleNode("//MoveDocuments/DocumentIds");

                if (p_objDocs.Documents == null || p_objDocs.Documents == "")
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.MoveDocuments.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.MoveDocuments.MissingDocumentIdsNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                sDocumentIds = p_objDocs.Documents;

                objXmlNode = null;
                //objXmlNode = p_objXmlDocIn.SelectSingleNode("//MoveDocuments/NewFolderId");

                if (p_objDocs.NewFolderId == null || p_objDocs.NewFolderId == "")
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.MoveDocuments.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.MoveDocuments.MissingNewFolderIdNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                lNewFolderId = Conversion.ConvertStrToLong(p_objDocs.NewFolderId);


                objDocumentManager = new DocumentManager(m_userLogin, base.ClientId);
                objDocumentManager.ConnectionString = this.connectionString;
                objDocumentManager.SecurityConnectionString = this.securityConnectionString; //MITS 27764 hlv 5/23/12
                objDocumentManager.UserLoginName = this.loginName;

                objDocumentManager.MoveDocuments(sDocumentIds, lNewFolderId);

                bReturnValue = true;
            }
            catch (InitializationException p_objInitializationException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInitializationException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInvalidValueException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (RMAppException p_objRMAppException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objRMAppException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException, Globalization.GetString("DocumentManagementAdaptor.MoveDocuments.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            finally
            {
                objXmlNode = null;
                objDocumentManager = null;
            }

            return bReturnValue;
        }
        /// Name			: UpdateDocument
        /// Author			: Aditya Babbar
        /// Date Created	: 15-Mar-2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Wrapper to DocumentManager.UpdateDocument of application layer
        /// that updates document details.
        /// </summary>
        /// <param name="p_objXmlDocIn"> 
        ///	Input paramters in xml format.
        ///	Structure:
        ///		<UpdateDocument>
        ///			<DocumentXml>
        ///				<data>
        ///					<Document>
        ///						<DocumentId/>
        ///						<FolderId/>
        ///						<CreateDate/>
        ///						<Category/>
        ///						<Name/>
        ///						<Class/>
        ///						<Subject/>
        ///						<Type/>
        ///						<Notes/>
        ///						<UserLoginName/>
        ///						<FileName/>
        ///						<FilePath/>
        ///						<Keywords/>
        ///						<AttachTable/>
        ///						<AttachRecordId/>
        ///					</Document>
        ///				</data>	
        ///			</DocumentXml>	
        ///		</UpdateDocument>
        /// </param>
        /// <param name="p_objXmlDocOut"> 
        ///		Empty.
        ///	</param>
        /// <param name="p_objErrors">
        ///	Collection of errors/messages.	
        ///	</param>
        /// <returns>
        ///		true - success
        ///		false - failure
        ///	</returns>
        public bool UpdateDocument(XmlDocument p_objXmlDocIn,
            ref XmlDocument p_objXmlDocOut,
            ref BusinessAdaptorErrors p_objErrors)
        {
            bool bReturnValue = false;
            DocumentManager objDocumentManager = null;

            XmlNode objXmlNode = null;
            int iPSID = 0;
            string sDocumentXml = string.Empty;

            try
            {
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//UpdateDocument/DocumentXml");

                if (objXmlNode == null)
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.UpdateDocument.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.UpdateDocument.MissingDocumentXmlNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                sDocumentXml = objXmlNode.InnerXml;

                objDocumentManager = new DocumentManager(m_userLogin, base.ClientId);
                objDocumentManager.ConnectionString = this.connectionString;
                objDocumentManager.SecurityConnectionString = this.securityConnectionString; //MITS 27764 hlv 5/23/12
                objDocumentManager.UserLoginName = this.loginName;

                //Get psid value
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//psid");
                if (objXmlNode != null)
                {
                    if (objXmlNode.InnerText.Trim() != "")
                    {
                        iPSID = int.Parse(objXmlNode.InnerText);
                    }
                }
                objXmlNode = null;

                objDocumentManager.UpdateDocument(sDocumentXml, iPSID);

                bReturnValue = true;
            }
            catch (InitializationException p_objInitializationException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInitializationException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (RMAppException p_objRMAppException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objRMAppException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException, Globalization.GetString("DocumentManagementAdaptor.UpdateDocument.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            finally
            {
                objXmlNode = null;
                objDocumentManager = null;
            }

            return bReturnValue;
        }
        public bool UpdateDocumentNew(DocumentType p_Document,
            ref BusinessAdaptorErrors p_objErrors)
        {
            bool bReturnValue = false;
            DocumentManager objDocumentManager = null;

            XmlNode objXmlNode = null;
            int iPSID = 0;
            string sDocumentXml = string.Empty;

            try
            {

                objDocumentManager = new DocumentManager(m_userLogin, base.ClientId);
                objDocumentManager.ConnectionString = this.connectionString;
                objDocumentManager.SecurityConnectionString = this.securityConnectionString; //MITS 27764 hlv 5/23/12
                objDocumentManager.UserLoginName = this.loginName;

                //Get psid value


                iPSID = p_Document.PsId;


                objDocumentManager.UpdateDocument(p_Document, iPSID);

                bReturnValue = true;
            }
            catch (InitializationException p_objInitializationException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInitializationException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (RMAppException p_objRMAppException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objRMAppException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException, Globalization.GetString("DocumentManagementAdaptor.UpdateDocument.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            finally
            {
                objXmlNode = null;
                objDocumentManager = null;
            }

            return bReturnValue;
        }

        public bool UpdateDocumentFile(DocumentType doc, out DocumentType outdoc,
         ref BusinessAdaptorErrors p_objErrors)
        {
            // TODO - Confirm output type to be given for the file attachment
            // in accordance with Orbeon 

            bool bReturnValue = false;
            DocumentManager objDocumentManager = null;

            long lDocumentId = 0;
            int iPSID = 0;

            //Mohit Yadav for Security Permission implementation
            string sScreenFlag = string.Empty;

            MemoryStream objDocumentManagementFile = null;	//stream of file
            outdoc = new DocumentType();
            try
            {
                lDocumentId = doc.DocumentId;
                if (lDocumentId == 0)
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.RetrieveDocument.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.RetrieveDocument.MissingDocumentXmlNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }


                objDocumentManager = new DocumentManager(m_userLogin, base.ClientId);
                objDocumentManager.ConnectionString = this.connectionString;
                objDocumentManager.SecurityConnectionString = this.securityConnectionString; //MITS 27764 hlv 5/23/12
                objDocumentManager.UserLoginName = this.loginName;

                objDocumentManager.DocumentStorageType =
                    (StorageType)this.userLogin.objRiskmasterDatabase.DocPathType;

                if (this.userLogin.DocumentPath.Length > 0)
                {
                    // use StoragePath set for the user
                    objDocumentManager.DestinationStoragePath =
                        this.userLogin.DocumentPath;
                }
                else
                {
                    // use StoragePath set for the DSN
                    objDocumentManager.DestinationStoragePath =
                        this.userLogin.objRiskmasterDatabase.GlobalDocPath;
                }

                //Get psid value
                iPSID = doc.PsId;

                sScreenFlag = doc.ScreenFlag;

                int iBaseSecurityId = 0;
                iBaseSecurityId = iPSID;

                if (sScreenFlag == "Files")
                {
                    if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_UD_PRINT))
                        throw new PermissionViolationException(RMPermissions.RMO_UD_PRINT, iBaseSecurityId);
                }
                else
                {
                    if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_ATT_VIEWPLAYPRINT))
                        throw new PermissionViolationException(RMPermissions.RMO_ATT_VIEWPLAYPRINT, iBaseSecurityId);
                }

                objDocumentManager.UpdateDocumentFile(lDocumentId, iPSID, doc.FileContents);
               
                bReturnValue = true;
            }
            catch (InitializationException p_objInitializationException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInitializationException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (RMAppException p_objRMAppException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objRMAppException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException, Globalization.GetString("DocumentManagementAdaptor.RetrieveDocument.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            finally
            {
                //BSB 03.24.2006 Close any MemoryStream if necessary.
                if (objDocumentManagementFile != null)
                    objDocumentManagementFile.Close();
                objDocumentManagementFile = null;
                objDocumentManager = null;
            }

            return bReturnValue;
        }
        #region RetrieveDocument(XmlDocument p_objXmlDocIn, ref XmlDocument p_objXmlDocOut, ref BusinessAdaptorErrors p_objErrors)
        /// Name			: RetrieveDocument
        /// Author			: Aditya Babbar
        /// Date Created	: 15-Mar-2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///	05/05/2005		*	To convert the contents of file to Base64 string * Mohit Yadav
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// For retrieving the document & passing the contents as base64 string in XML		/// 
        /// </summary>
        /// <param name="p_objXmlDocIn"> 
        ///	Input paramters in xml format.
        ///	Structure:
        ///		
        /// </param>
        /// <param name="p_objXmlDocOut"> 
        ///	Method output in xml format.
        ///	Structure:
        ///		
        ///	</param>
        /// <param name="p_objErrors">
        ///	Collection of errors/messages.	
        ///	</param>
        /// <returns>
        ///		true - success
        ///		false - failure
        ///	</returns>
        public bool RetrieveDocument(DocumentType doc, out DocumentType outdoc,
            ref BusinessAdaptorErrors p_objErrors)
        {
            // TODO - Confirm output type to be given for the file attachment
            // in accordance with Orbeon 

            bool bReturnValue = false;
            DocumentManager objDocumentManager = null;

            long lDocumentId = 0;
            int iPSID = 0;

            //Mohit Yadav for Security Permission implementation
            string sScreenFlag = string.Empty;

            MemoryStream objDocumentManagementFile = null;	//stream of file
            outdoc = new DocumentType();
            try
            {
                lDocumentId = doc.DocumentId;
                if (lDocumentId == 0)
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.RetrieveDocument.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.RetrieveDocument.MissingDocumentXmlNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }


                objDocumentManager = new DocumentManager(m_userLogin, base.ClientId);
                objDocumentManager.ConnectionString = this.connectionString;
                objDocumentManager.SecurityConnectionString = this.securityConnectionString; //MITS 27764 hlv 5/23/12
                objDocumentManager.UserLoginName = this.loginName;

                //mgaba2:MITS 11704-04/07/2008-inserted the following code in else part
                /* objDocumentManager.DocumentStorageType = 
                    (StorageType) this.userLogin.objRiskmasterDatabase.DocPathType;*/

                if (this.userLogin.DocumentPath.Length > 0)
                {
                    // use StoragePath set for the user
                    objDocumentManager.DestinationStoragePath =
                        this.userLogin.DocumentPath;
                    //mgaba2:MITS 11704-04/07/2008-doc path entered at user level has to be file system
                    objDocumentManager.DocumentStorageType = StorageType.FileSystemStorage;
                }
                else
                {
                    // use StoragePath set for the DSN
                    objDocumentManager.DestinationStoragePath =
                        this.userLogin.objRiskmasterDatabase.GlobalDocPath;
                    //mgaba2:MITS 11704-04/07/2008-if no doc path entered at user level
                    objDocumentManager.DocumentStorageType =
                    (StorageType)this.userLogin.objRiskmasterDatabase.DocPathType;
                }				


                //Get psid value
                iPSID = doc.PsId;

                sScreenFlag = doc.ScreenFlag;

                int iBaseSecurityId = 0;
                iBaseSecurityId = iPSID;

                if (sScreenFlag == "Files")
                {
                    if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_UD_PRINT))
                        throw new PermissionViolationException(RMPermissions.RMO_UD_PRINT, iBaseSecurityId);
                }
                else
                {
                    if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_ATT_VIEWPLAYPRINT))
                        throw new PermissionViolationException(RMPermissions.RMO_ATT_VIEWPLAYPRINT, iBaseSecurityId);
                }

                objDocumentManager.RetrieveDocument(lDocumentId, iPSID, out objDocumentManagementFile);
                outdoc.FileContents = objDocumentManagementFile.ToArray();
                bReturnValue = true;
            }
            catch (InitializationException p_objInitializationException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInitializationException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (RMAppException p_objRMAppException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objRMAppException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException, Globalization.GetString("DocumentManagementAdaptor.RetrieveDocument.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            finally
            {
                //BSB 03.24.2006 Close any MemoryStream if necessary.
                if (objDocumentManagementFile != null)
                    objDocumentManagementFile.Close();
                objDocumentManagementFile = null;
                objDocumentManager = null;
            }

            return bReturnValue;
        }


        public bool RetrieveLargeDocument(StreamedDocumentType objRequest, out StreamedDocumentType outdoc,ref bool p_bIsFileStorage,
            ref BusinessAdaptorErrors p_objErrors)
        {
            // TODO - Confirm output type to be given for the file attachment
            // in accordance with Orbeon 

            bool bReturnValue = false;
            DocumentManager objDocumentManager = null;
            string outputStreamFilePath = string.Empty;
            long lDocumentId = 0;
            int iPSID = 0;

            //Mohit Yadav for Security Permission implementation
            string sScreenFlag = string.Empty;

            Stream objDocumentManagementFile = null;	//stream of file
            outdoc = new StreamedDocumentType();
            try
            {
                lDocumentId = objRequest.DocumentId;  
                if (lDocumentId == 0)
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.RetrieveDocument.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.RetrieveDocument.MissingDocumentXmlNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                objDocumentManager = new DocumentManager(m_userLogin, base.ClientId);
                objDocumentManager.ConnectionString = this.connectionString;
                objDocumentManager.SecurityConnectionString = this.securityConnectionString; //MITS 27764 hlv 5/23/12
                objDocumentManager.UserLoginName = this.loginName;

                //mgaba2:MITS 11704-04/07/2008-inserted the following code in else part
                /* objDocumentManager.DocumentStorageType = 
                    (StorageType) this.userLogin.objRiskmasterDatabase.DocPathType;*/

                if (this.userLogin.DocumentPath.Length > 0)
                {
                    // use StoragePath set for the user
                    objDocumentManager.DestinationStoragePath =
                        this.userLogin.DocumentPath;
                    //mgaba2:MITS 11704-04/07/2008-doc path entered at user level has to be file system
                    objDocumentManager.DocumentStorageType = StorageType.FileSystemStorage;
                }
                else
                {
                    // use StoragePath set for the DSN
                    objDocumentManager.DestinationStoragePath =
                        this.userLogin.objRiskmasterDatabase.GlobalDocPath;
                    //mgaba2:MITS 11704-04/07/2008-if no doc path entered at user level
                    objDocumentManager.DocumentStorageType =(StorageType)this.userLogin.objRiskmasterDatabase.DocPathType;
                }

                //Get psid value
                iPSID = objRequest.PsId;
                sScreenFlag = objRequest.ScreenFlag;

                int iBaseSecurityId = 0;
                iBaseSecurityId = iPSID;
                //rsushilaggar MITS 23234
                //MGaba2:MITS 13013:Added if condition to handle funds case separately:start
                //Documents were not opened  from funds screen if Module Security is ON
                if (objRequest.FormName.ToLower() == "funds")
                {
                    iBaseSecurityId = UserPermissions.RMB_FUNDS_TRANSACT;
                }
                else
                {//MGaba2:MITS 13013:End
                    //For Admin tracking Security: TR-2527
                    iBaseSecurityId = objDocumentManager.GetBaseSecurityIDForTable(iBaseSecurityId, objRequest.FormName);
                }//
                //End rsushilaggar
                if (sScreenFlag == "Files")
                {
                    if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_UD_PRINT))
                        throw new PermissionViolationException(RMPermissions.RMO_UD_PRINT, iBaseSecurityId);
                }
                else
                {
                    if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_ATT_VIEWPLAYPRINT))
                        throw new PermissionViolationException(RMPermissions.RMO_ATT_VIEWPLAYPRINT, iBaseSecurityId);
                }

                objDocumentManager.RetrieveLargeDocument(lDocumentId, iPSID, out objDocumentManagementFile,ref outputStreamFilePath);
                if (objDocumentManager.DocumentStorageType == StorageType.FileSystemStorage)
                {
                    p_bIsFileStorage = true;
                }
                //outdoc.fileStream = objDocumentManagementFile;
                //outdoc.FileContents = StreamToByte(objDocumentManagementFile);
                outdoc.FilePath = outputStreamFilePath;
                bReturnValue = true;
            }
            catch (InitializationException p_objInitializationException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInitializationException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (RMAppException p_objRMAppException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objRMAppException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException, Globalization.GetString("DocumentManagementAdaptor.RetrieveDocument.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            finally
            {
                //BSB 03.24.2006 Close any MemoryStream if necessary.
                if (objDocumentManagementFile != null)
                    objDocumentManagementFile.Close();
                objDocumentManagementFile = null;
                objDocumentManager = null;
            }

            return bReturnValue;
        }

        public bool RetrieveLargeDocument(StreamDocType objRequest, out StreamDocType outdoc, ref bool p_bIsFileStorage,
           ref BusinessAdaptorErrors p_objErrors)
        {
            // TODO - Confirm output type to be given for the file attachment
            // in accordance with Orbeon 

            bool bReturnValue = false;
            DocumentManager objDocumentManager = null;
            string outputStreamFilePath = string.Empty;
            long lDocumentId = 0;
            int iPSID = 0;

            //Mohit Yadav for Security Permission implementation
            string sScreenFlag = string.Empty;

            Stream objDocumentManagementFile = null;	//stream of file
            outdoc = new StreamDocType();
            try
            {
                lDocumentId = objRequest.DocumentId;
                if (lDocumentId == 0)
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.RetrieveDocument.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.RetrieveDocument.MissingDocumentXmlNode", base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                objDocumentManager = new DocumentManager(m_userLogin, base.ClientId);
                objDocumentManager.ConnectionString = this.connectionString;
                objDocumentManager.SecurityConnectionString = this.securityConnectionString; //MITS 27764 hlv 5/23/12
                objDocumentManager.UserLoginName = this.loginName;

                //mgaba2:MITS 11704-04/07/2008-inserted the following code in else part
                /* objDocumentManager.DocumentStorageType = 
                    (StorageType) this.userLogin.objRiskmasterDatabase.DocPathType;*/

                if (this.userLogin.DocumentPath.Length > 0)
                {
                    // use StoragePath set for the user
                    objDocumentManager.DestinationStoragePath =
                        this.userLogin.DocumentPath;
                    //mgaba2:MITS 11704-04/07/2008-doc path entered at user level has to be file system
                    objDocumentManager.DocumentStorageType = StorageType.FileSystemStorage;
                }
                else
                {
                    // use StoragePath set for the DSN
                    objDocumentManager.DestinationStoragePath =
                        this.userLogin.objRiskmasterDatabase.GlobalDocPath;
                    //mgaba2:MITS 11704-04/07/2008-if no doc path entered at user level
                    objDocumentManager.DocumentStorageType = (StorageType)this.userLogin.objRiskmasterDatabase.DocPathType;
                }

                //Get psid value
                iPSID = objRequest.PsId;
                sScreenFlag = objRequest.ScreenFlag;

                int iBaseSecurityId = 0;
                iBaseSecurityId = iPSID;
                //rsushilaggar MITS 23234
                //MGaba2:MITS 13013:Added if condition to handle funds case separately:start
                //Documents were not opened  from funds screen if Module Security is ON
                if (objRequest.FormName.ToLower() == "funds")
                {
                    iBaseSecurityId = UserPermissions.RMB_FUNDS_TRANSACT;
                }
                else
                {//MGaba2:MITS 13013:End
                    //For Admin tracking Security: TR-2527
                    iBaseSecurityId = objDocumentManager.GetBaseSecurityIDForTable(iBaseSecurityId, objRequest.FormName);
                }//
                //End rsushilaggar
                if (sScreenFlag == "Files")
                {
                    if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_UD_PRINT))
                        throw new PermissionViolationException(RMPermissions.RMO_UD_PRINT, iBaseSecurityId);
                }
                else
                {
                    if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_ATT_VIEWPLAYPRINT))
                        throw new PermissionViolationException(RMPermissions.RMO_ATT_VIEWPLAYPRINT, iBaseSecurityId);
                }

                objDocumentManager.RetrieveLargeDocument(lDocumentId, iPSID, out objDocumentManagementFile, ref outputStreamFilePath);
                if (objDocumentManager.DocumentStorageType == StorageType.FileSystemStorage)
                {
                    p_bIsFileStorage = true;
                }
                //outdoc.fileStream = objDocumentManagementFile;
                //outdoc.FileContents = StreamToByte(objDocumentManagementFile);
                outdoc.FilePath = outputStreamFilePath;
                bReturnValue = true;
            }
            catch (InitializationException p_objInitializationException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInitializationException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (RMAppException p_objRMAppException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objRMAppException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException, Globalization.GetString("DocumentManagementAdaptor.RetrieveDocument.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            finally
            {
                //BSB 03.24.2006 Close any MemoryStream if necessary.
                if (objDocumentManagementFile != null)
                    objDocumentManagementFile.Close();
                objDocumentManagementFile = null;
                objDocumentManager = null;
            }

            return bReturnValue;
        }

        public static byte[] StreamToByte(Stream input)
        {
            try
            {
                byte[] buffer = new byte[16 * 1024];
                using (MemoryStream ms = new MemoryStream())
                {
                    int read;
                    while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        ms.Write(buffer, 0, read);
                    }
                    return ms.ToArray();
                }
            }
            catch
            {
                return null;
            }
        }
        /*public bool RetrieveDocument(XmlDocument p_objXmlDocIn, ref XmlDocument p_objXmlDocOut, 
            ref BusinessAdaptorErrors p_objErrors)
        {
            // TODO - Confirm output type to be given for the file attachment
            // in accordance with Orbeon 

            bool bReturnValue = false;
            DocumentManager objDocumentManager = null;

            long lDocumentId = 0;
            XmlNode objXmlNode = null;
            XmlNode objOutXmlNode = null;
            int iPSID = 0;

            //Mohit Yadav for Security Permission implementation
            string sScreenFlag = string.Empty;
			
            MemoryStream objDocumentManagementFile=null;	//stream of file
		
            try
            {
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//RetrieveDocument/DocumentId");
                if(objXmlNode==null)
                { 
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.RetrieveDocument.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.RetrieveDocument.MissingDocumentXmlNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);  
                    return bReturnValue;
                }
 
                lDocumentId=Conversion.ConvertStrToLong(objXmlNode.InnerText);

                objDocumentManager = new DocumentManager( m_userLogin);
                objDocumentManager.ConnectionString = this.connectionString;
                objDocumentManager.UserLoginName = this.loginName;

                objDocumentManager.DocumentStorageType = 
                    (StorageType) this.userLogin.objRiskmasterDatabase.DocPathType;
				
                if(this.userLogin.DocumentPath.Length>0)
                {
                    // use StoragePath set for the user
                    objDocumentManager.DestinationStoragePath = 
                        this.userLogin.DocumentPath;
                }
                else
                {
                    // use StoragePath set for the DSN
                    objDocumentManager.DestinationStoragePath = 
                        this.userLogin.objRiskmasterDatabase.GlobalDocPath;   
                }

                //Get psid value
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//psid");
                if( objXmlNode != null)
                {
                    if(objXmlNode.InnerText.Trim() != "" )
                    {
                        iPSID = int.Parse(objXmlNode.InnerText);
                    }
                }
                objXmlNode = null;

                //Mohit Yadav for Security Permission implementation
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//RetrieveDocument/ScreenFlag");
                sScreenFlag = objXmlNode.InnerText.ToString();
				
                int iBaseSecurityId=0;
                iBaseSecurityId = iPSID;
				
                if(sScreenFlag=="Files")
                {
                    if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_UD_PRINT))
                        throw new PermissionViolationException(RMPermissions.RMO_UD_PRINT,iBaseSecurityId);
                }
                else
                {
                    if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_ATT_VIEWPLAYPRINT))
                        throw new PermissionViolationException(RMPermissions.RMO_ATT_VIEWPLAYPRINT,iBaseSecurityId);
                }

                objDocumentManager.RetrieveDocument(lDocumentId, iPSID, out objDocumentManagementFile);				

                //************************************************
                // Changes made by Mohit Yadav to including paging
                //************************************************
                //create the output xml document containing the data in Base64 format
                objOutXmlNode=p_objXmlDocOut.CreateElement("RetrieveDocumentsResult");
                p_objXmlDocOut.AppendChild(objOutXmlNode);
                objOutXmlNode=p_objXmlDocOut.CreateElement("File");
                if(m_bSoapAttachmentCachedToDisk) 
                {
                    //Attribute required to properly trigger replacement by SoapExtension.
                    XmlAttribute attAnyURI =p_objXmlDocOut.CreateAttribute("xsi:type","http://www.w3.org/2001/XMLSchema-instance");
                    attAnyURI.Value="xs:anyURI";
                    objOutXmlNode.Attributes.Append(attAnyURI);

                    // BSB Throw a temp copy of the file onto disk. 
                    // File will be inserted into message and cleaned from disk by soap extension logic.
                    string sDiskUri = Path.GetTempFileName();
                    using (FileStream stmTempFile = File.OpenWrite(sDiskUri))
                    {
                        Utilities.CopyStream(objDocumentManagementFile,stmTempFile);
                        stmTempFile.Flush();
                        stmTempFile.Close();
                    }
                    objOutXmlNode.InnerText=sDiskUri;
                }
                else
                    objOutXmlNode.InnerText=Convert.ToBase64String(objDocumentManagementFile.ToArray());
                p_objXmlDocOut.FirstChild.AppendChild(objOutXmlNode);				
                //************************************************
 								
                bReturnValue = true;
								
            }
            catch(InitializationException p_objInitializationException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInitializationException,BusinessAdaptorErrorType.Error);   
                return bReturnValue;
            }
            catch(RMAppException p_objRMAppException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objRMAppException,BusinessAdaptorErrorType.Error);   
                return bReturnValue;
            }
            catch(Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException,Globalization.GetString("DocumentManagementAdaptor.RetrieveDocument.Error",base.ClientId),BusinessAdaptorErrorType.Error);   
                return bReturnValue;
            }
            finally
            {
                //BSB 03.24.2006 Close any MemoryStream if necessary.
                if(objDocumentManagementFile!=null)
                    objDocumentManagementFile.Close();
                objDocumentManagementFile=null;
                objXmlNode = null;
                objDocumentManager = null;				
            }

            return bReturnValue;
        }*/


        #endregion

        /// Name			: CopyDocuments
        /// Author			: Aditya Babbar
        /// Date Created	: 15-Mar-2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Wrapper to DocumentManager.CopyDocuments of application layer
        /// that creates copy of documents for specified users.
        /// </summary>
        /// <param name="p_objXmlDocIn"> 
        ///	Input paramters in xml format.
        ///	Structure:
        ///		<CopyDocuments>
        ///			<DocumentIds>
        ///				Comma separated list of DocumentIds.
        ///			</DocumentIds>
        ///			<UserLoginNames>
        ///				Comma separated list of UserLoginNames.
        ///			</UserLoginNames>	
        ///		</CopyDocuments>
        /// </param>
        /// <param name="p_objXmlDocOut"> 
        ///		Empty.
        ///	</param>
        /// <param name="p_objErrors">
        ///	Collection of errors/messages.	
        ///	</param>
        /// <returns>
        ///		true - success
        ///		false - failure
        ///	</returns>
        public bool CopyDocuments(XmlDocument p_objXmlDocIn,
            ref XmlDocument p_objXmlDocOut,
            ref BusinessAdaptorErrors p_objErrors)
        {
            bool bReturnValue = false;
            DocumentManager objDocumentManager = null;

            XmlNode objXmlNode = null;

            string sDocumentIds = string.Empty;
            string sUserLoginNames = string.Empty;

            //Mohit Yadav for Security Permission implementation
            int iPSID = 0;
            string sScreenFlag = string.Empty;
            //Nitesh
            string sTableName = string.Empty;
            try
            {
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//CopyDocuments/DocumentIds");

                if (objXmlNode == null)
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.CopyDocuments.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.CopyDocuments.MissingDocumentIdsNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                sDocumentIds = objXmlNode.InnerText;

                objXmlNode = null;
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//CopyDocuments/UserLoginNames");

                if (objXmlNode == null)
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.CopyDocuments.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.CopyDocuments.MissingUserLoginNamesNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                sUserLoginNames = objXmlNode.InnerText;

                //Get psid value
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//psid");
                if (objXmlNode != null)
                {
                    if (objXmlNode.InnerText.Trim() != "")
                    {
                        iPSID = int.Parse(objXmlNode.InnerText);
                    }
                }
                objXmlNode = null;
                //Nitesh TR 2527  Admin Tracking
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//TableName");
                if (objXmlNode != null)
                {
                    sTableName = objXmlNode.InnerText;
                }
                objXmlNode = null;

                //Mohit Yadav for Security Permission implementation
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//CopyDocuments/ScreenFlag");
                sScreenFlag = objXmlNode.InnerText.ToString();

                objDocumentManager = new DocumentManager(m_userLogin, base.ClientId);
                objDocumentManager.ConnectionString = this.connectionString;
                objDocumentManager.SecurityConnectionString = this.securityConnectionString; //MITS 27764 hlv 5/23/12
                objDocumentManager.UserLoginName = this.loginName;

                int iBaseSecurityId = 0;
                iBaseSecurityId = iPSID;
                // Admin Tracking TR 2527
                iBaseSecurityId = objDocumentManager.GetBaseSecurityIDForTable(iBaseSecurityId, sTableName);
                if (sScreenFlag == "Files")
                {
                    if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_UD_COPYTO))
                        throw new PermissionViolationException(RMPermissions.RMO_USERDOC_COPY, iBaseSecurityId);
                }
                else
                {
                    if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_ATT_ATTACHTO))
                        throw new PermissionViolationException(RMPermissions.RMO_ATT_ATTACHTO, iBaseSecurityId);
                }

                // WARNING - Integer value returned from RiskmasterDatabase object
                // is being casted to StorageType enum.
                objDocumentManager.DocumentStorageType =
                    (StorageType)this.userLogin.objRiskmasterDatabase.DocPathType;

                if (this.userLogin.DocumentPath.Length > 0)
                {
                    // use StoragePath set for the user
                    objDocumentManager.DestinationStoragePath =
                        this.userLogin.DocumentPath;
                }
                else
                {
                    // use StoragePath set for the DSN
                    objDocumentManager.DestinationStoragePath =
                        this.userLogin.objRiskmasterDatabase.GlobalDocPath;
                }

                objDocumentManager.CopyDocuments(sDocumentIds, sUserLoginNames);

                bReturnValue = true;
            }
            catch (InitializationException p_objInitializationException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInitializationException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInvalidValueException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (RMAppException p_objRMAppException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objRMAppException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException, Globalization.GetString("DocumentManagementAdaptor.CopyDocuments.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            finally
            {
                objXmlNode = null;
                objDocumentManager = null;
            }

            return bReturnValue;
        }
        public bool CopyDocumentsNew(TransferDocumentsRequest objRequest,
            ref BusinessAdaptorErrors p_objErrors)
        {
            bool bReturnValue = false;
            DocumentManager objDocumentManager = null;

            XmlNode objXmlNode = null;

            string sDocumentIds = string.Empty;
            string sUserLoginNames = string.Empty;

            //Mohit Yadav for Security Permission implementation
            int iPSID = 0;
            string sScreenFlag = string.Empty;
            //Nitesh
            string sTableName = string.Empty;
            try
            {
                if (objRequest.DocumentIds == null || objRequest.DocumentIds == "")
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.CopyDocuments.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.TransferDocuments.MissingDocumentIdsNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                sDocumentIds = objRequest.DocumentIds;

                objXmlNode = null;
                //objXmlNode = p_objXmlDocIn.SelectSingleNode("//TransferDocuments/UserLoginNames");

                if (objRequest.UserLoginNames == null || objRequest.UserLoginNames == "")
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.CopyDocuments.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.TransferDocuments.MissingUserLoginNamesNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                sUserLoginNames = objRequest.UserLoginNames;

                //Get psid value
                //objXmlNode = p_objXmlDocIn.SelectSingleNode("//psid");
                if (objRequest.Psid != null && objRequest.Psid != "")
                {
                    if (objRequest.Psid.Trim() != "")
                    {
                        iPSID = int.Parse(objRequest.Psid);
                    }
                }
                objXmlNode = null;
                //Nitesh TR 2527
                //objXmlNode = p_objXmlDocIn.SelectSingleNode("//TableName");
                if (objRequest.TableName != null && objRequest.TableName != "")
                {
                    sTableName = objRequest.TableName;
                }
                objXmlNode = null;

                //Mohit Yadav for Security Permission implementation
                //objXmlNode = p_objXmlDocIn.SelectSingleNode("//TransferDocuments/ScreenFlag");
                sScreenFlag = objRequest.ScreenFlag;

                int iBaseSecurityId = 0;
                iBaseSecurityId = iPSID;

                objDocumentManager = new DocumentManager(m_userLogin, base.ClientId);
                objDocumentManager.ConnectionString = this.connectionString;
                objDocumentManager.SecurityConnectionString = this.securityConnectionString; //MITS 27764 hlv 5/23/12
                objDocumentManager.UserLoginName = this.loginName;
                //Nitesh TR: 2527 



                iBaseSecurityId = objDocumentManager.GetBaseSecurityIDForTable(iBaseSecurityId, sTableName);
                if (sScreenFlag == "Files")
                {
                    if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_UD_COPYTO))
                        throw new PermissionViolationException(RMPermissions.RMO_USERDOC_COPY, iBaseSecurityId);
                }
                else
                {
                    if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_ATT_ATTACHTO))
                        throw new PermissionViolationException(RMPermissions.RMO_ATT_ATTACHTO, iBaseSecurityId);
                }

                // WARNING - Integer value returned from RiskmasterDatabase object
                // is being casted to StorageType enum.
                objDocumentManager.DocumentStorageType =
                    (StorageType)this.userLogin.objRiskmasterDatabase.DocPathType;

                if (this.userLogin.DocumentPath.Length > 0)
                {
                    // use StoragePath set for the user
                    objDocumentManager.DestinationStoragePath =
                        this.userLogin.DocumentPath;
                }
                else
                {
                    // use StoragePath set for the DSN
                    objDocumentManager.DestinationStoragePath =
                        this.userLogin.objRiskmasterDatabase.GlobalDocPath;
                }

                objDocumentManager.CopyDocuments(sDocumentIds, sUserLoginNames);

                bReturnValue = true;
            }
            catch (InitializationException p_objInitializationException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInitializationException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInvalidValueException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (RMAppException p_objRMAppException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objRMAppException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException, Globalization.GetString("DocumentManagementAdaptor.CopyDocuments.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            finally
            {
                objXmlNode = null;
                objDocumentManager = null;
            }

            return bReturnValue;
        }
        /// Name			: TransferDocuments
        /// Author			: Aditya Babbar
        /// Date Created	: 15-Mar-2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Wrapper to DocumentManager.TransferDocuments of application
        /// layer that transfers documents to specified users. 
        /// </summary>
        /// <param name="p_objXmlDocIn"> 
        ///	Input paramters in xml format.
        ///	Structure:
        ///		<TransferDocuments>
        ///			<DocumentIds>
        ///				Comma separated list of DocumentIds.
        ///			</DocumentIds>
        ///			<UserLoginNames>
        ///				Comma separated list of UserIds.
        ///			</UserLoginNames>	
        ///		</TransferDocuments>
        /// </param>
        /// <param name="p_objXmlDocOut"> 
        ///		Empty
        ///	</param>
        /// <param name="p_objErrors">
        ///	Collection of errors/messages.	
        ///	</param>
        /// <returns>
        ///		true - success
        ///		false - failure
        ///	</returns>
        public bool TransferDocuments(XmlDocument p_objXmlDocIn,
            ref XmlDocument p_objXmlDocOut,
            ref BusinessAdaptorErrors p_objErrors)
        {
            bool bReturnValue = false;
            DocumentManager objDocumentManager = null;

            XmlNode objXmlNode = null;

            string sDocumentIds = string.Empty;
            string sUserLoginNames = string.Empty;
            string sTableName = string.Empty;
            //Mohit Yadav for Security Permission implementation
            int iPSID = 0;
            string sScreenFlag = string.Empty;

            try
            {
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//TransferDocuments/DocumentIds");

                if (objXmlNode == null)
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.TransferDocuments.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.TransferDocuments.MissingDocumentIdsNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                sDocumentIds = objXmlNode.InnerText;

                objXmlNode = null;
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//TransferDocuments/UserLoginNames");

                if (objXmlNode == null)
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.TransferDocuments.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.TransferDocuments.MissingUserLoginNamesNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                sUserLoginNames = objXmlNode.InnerText;

                //Get psid value
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//psid");
                if (objXmlNode != null)
                {
                    if (objXmlNode.InnerText.Trim() != "")
                    {
                        iPSID = int.Parse(objXmlNode.InnerText);
                    }
                }
                objXmlNode = null;
                //Nitesh TR 2527
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//TableName");
                if (objXmlNode != null)
                {
                    sTableName = objXmlNode.InnerText;
                }
                objXmlNode = null;

                //Mohit Yadav for Security Permission implementation
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//TransferDocuments/ScreenFlag");
                sScreenFlag = objXmlNode.InnerText.ToString();

                int iBaseSecurityId = 0;
                iBaseSecurityId = iPSID;

                objDocumentManager = new DocumentManager(m_userLogin, base.ClientId);
                objDocumentManager.ConnectionString = this.connectionString;
                objDocumentManager.SecurityConnectionString = this.securityConnectionString; //MITS 27764 hlv 5/23/12
                objDocumentManager.UserLoginName = this.loginName;
                //Nitesh TR: 2527 
                iBaseSecurityId = objDocumentManager.GetBaseSecurityIDForTable(iBaseSecurityId, sTableName);
                if (sScreenFlag == "Files")
                {
                    if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_UD_TRANSFERTO))
                        throw new PermissionViolationException(RMPermissions.RMO_USERDOC_TRANSFER, iBaseSecurityId);
                }
                else
                {
                    if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_ATT_ATTACHTO))
                        throw new PermissionViolationException(RMPermissions.RMO_ATT_ATTACHTO, iBaseSecurityId);
                }
                // WARNING - Integer value returned from RiskmasterDatabase object
                // is being casted to StorageType enum.
                objDocumentManager.DocumentStorageType =
                    (StorageType)this.userLogin.objRiskmasterDatabase.DocPathType;

                if (this.userLogin.DocumentPath.Length > 0)
                {
                    // use StoragePath set for the user
                    objDocumentManager.DestinationStoragePath =
                        this.userLogin.DocumentPath;
                }
                else
                {
                    // use StoragePath set for the DSN
                    objDocumentManager.DestinationStoragePath =
                        this.userLogin.objRiskmasterDatabase.GlobalDocPath;
                }

                objDocumentManager.TransferDocuments(sDocumentIds, sUserLoginNames);

                bReturnValue = true;
            }
            catch (InitializationException p_objInitializationException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInitializationException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInvalidValueException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (RMAppException p_objRMAppException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objRMAppException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException, Globalization.GetString("DocumentManagementAdaptor.TransferDocuments.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            finally
            {
                objXmlNode = null;
                objDocumentManager = null;
            }

            return bReturnValue;
        }
        public bool TransferDocumentsNew(TransferDocumentsRequest objRequest,
        ref BusinessAdaptorErrors p_objErrors)
        {
            bool bReturnValue = false;
            DocumentManager objDocumentManager = null;

            XmlNode objXmlNode = null;

            string sDocumentIds = string.Empty;
            string sUserLoginNames = string.Empty;
            string sTableName = string.Empty;
            //Mohit Yadav for Security Permission implementation
            int iPSID = 0;
            string sScreenFlag = string.Empty;

            try
            {
                //objXmlNode = p_objXmlDocIn.SelectSingleNode("//TransferDocuments/DocumentIds");

                if (objRequest.DocumentIds == null || objRequest.DocumentIds == "")
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.TransferDocuments.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.TransferDocuments.MissingDocumentIdsNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                sDocumentIds = objRequest.DocumentIds;

                objXmlNode = null;
                //objXmlNode = p_objXmlDocIn.SelectSingleNode("//TransferDocuments/UserLoginNames");

                if (objRequest.UserLoginNames == null || objRequest.UserLoginNames == "")
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.TransferDocuments.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.TransferDocuments.MissingUserLoginNamesNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                sUserLoginNames = objRequest.UserLoginNames;

                //Get psid value
                //objXmlNode = p_objXmlDocIn.SelectSingleNode("//psid");
                if (objRequest.Psid != null && objRequest.Psid != "")
                {
                    if (objRequest.Psid.Trim() != "")
                    {
                        iPSID = int.Parse(objRequest.Psid);
                    }
                }
                objXmlNode = null;
                //Nitesh TR 2527
                //objXmlNode = p_objXmlDocIn.SelectSingleNode("//TableName");
                if (objRequest.TableName != null && objRequest.TableName != "")
                {
                    sTableName = objRequest.TableName;
                }
                objXmlNode = null;

                //Mohit Yadav for Security Permission implementation
                //objXmlNode = p_objXmlDocIn.SelectSingleNode("//TransferDocuments/ScreenFlag");
                sScreenFlag = objRequest.ScreenFlag;

                int iBaseSecurityId = 0;
                iBaseSecurityId = iPSID;

                objDocumentManager = new DocumentManager(m_userLogin, base.ClientId);
                objDocumentManager.ConnectionString = this.connectionString;
                objDocumentManager.SecurityConnectionString = this.securityConnectionString; //MITS 27764 hlv 5/23/12
                objDocumentManager.UserLoginName = this.loginName;
                //Nitesh TR: 2527 
                iBaseSecurityId = objDocumentManager.GetBaseSecurityIDForTable(iBaseSecurityId, sTableName);
                if (sScreenFlag == "Files")
                {
                    if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_UD_TRANSFERTO))
                        throw new PermissionViolationException(RMPermissions.RMO_USERDOC_TRANSFER, iBaseSecurityId);
                }
                else
                {
                    if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_ATT_ATTACHTO))
                        throw new PermissionViolationException(RMPermissions.RMO_ATT_ATTACHTO, iBaseSecurityId);
                }
                // WARNING - Integer value returned from RiskmasterDatabase object
                // is being casted to StorageType enum.
                objDocumentManager.DocumentStorageType =
                    (StorageType)this.userLogin.objRiskmasterDatabase.DocPathType;

                if (this.userLogin.DocumentPath.Length > 0)
                {
                    // use StoragePath set for the user
                    objDocumentManager.DestinationStoragePath =
                        this.userLogin.DocumentPath;
                }
                else
                {
                    // use StoragePath set for the DSN
                    objDocumentManager.DestinationStoragePath =
                        this.userLogin.objRiskmasterDatabase.GlobalDocPath;
                }

                objDocumentManager.TransferDocuments(sDocumentIds, sUserLoginNames);

                bReturnValue = true;
            }
            catch (InitializationException p_objInitializationException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInitializationException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInvalidValueException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (RMAppException p_objRMAppException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objRMAppException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException, Globalization.GetString("DocumentManagementAdaptor.TransferDocuments.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            finally
            {
                objXmlNode = null;
                objDocumentManager = null;
            }

            return bReturnValue;
        }

        #region public bool EmailDocuments(XmlDocument p_objXmlDocIn,ref XmlDocument p_objXmlDocOut,ref BusinessAdaptorErrors p_objErrors)
        /// Name			: EmailDocuments
        /// Author			: Mohit Yadav
        /// Date Created	: 05-June-2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Wrapper to DocumentManager.EmailDocuments of application
        /// layer that send emails with documents to specified users. 
        /// </summary>
        /// <param name="p_objXmlDocIn"> 
        ///	Input paramters in xml format.
        ///	Structure:
        ///		<EmailDocuments>
        ///			<EmailIds>
        ///				Comma separated list of EmailIds.
        ///			</EmailIds>
        ///			<DocumentIds>
        ///				Comma separated list of DocumentIds.
        ///			</DocumentIds>
        ///			<Message>
        ///				Comma separated list of UserIds.
        ///			</Message>	
        ///		</EmailDocuments>
        /// </param>
        /// <param name="p_objXmlDocOut"> 
        ///		Empty
        ///	</param>
        /// <param name="p_objErrors">
        ///	Collection of errors/messages.	
        ///	</param>
        /// <returns>
        ///		true - success
        ///		false - failure
        ///	</returns>
        public bool EmailDocuments(XmlDocument p_objXmlDocIn, ref XmlDocument p_objXmlDocOut,
            ref BusinessAdaptorErrors p_objErrors)
        {
            bool bReturnValue = false;
            DocumentManager objDocumentManager = null;

            XmlNode objXmlNode = null;

            string sEmailIds = string.Empty;
            string sDocumentIds = string.Empty;
            string sMessage = string.Empty;
            string sTableName = string.Empty;//Nitesh
            string sSubject = string.Empty;//pmahli MITS 10739 11/20/2007
            int iPSID = 0;

            try
            {
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//EmailDocuments/UserEmailIds");
                if (objXmlNode != null)
                    sEmailIds = objXmlNode.InnerText;

                objXmlNode = p_objXmlDocIn.SelectSingleNode("//EmailDocuments/OtherEmailIds");
                if (objXmlNode != null)
                {
                    if (objXmlNode.InnerText.Length > 0)
                    {
                        if (sEmailIds.Length > 0)
                            sEmailIds += ","; //pmahli MITS 10739 ';' replaced with ','
                        sEmailIds += objXmlNode.InnerText;
                    }
                }

                if (sEmailIds.Length == 0)
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.EmailDocuments.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.EmailDocuments.MissingEmailIdsNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                objXmlNode = null;
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//EmailDocuments/DocumentIds");
                if (objXmlNode == null)
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.EmailDocuments.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.EmailDocuments.MissingDocumentIdsNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                sDocumentIds = objXmlNode.InnerText;

                //pmahli MITS 10739
                objXmlNode = null;
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//EmailDocuments/Subject");
                if (objXmlNode == null)
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.EmailDocuments.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.EmailDocuments.MissingDocumentIdsNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                sSubject = objXmlNode.InnerText;
                //pmahli MITS 10739

                objXmlNode = null;
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//EmailDocuments/Message");

                if (objXmlNode == null)
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.EmailDocuments.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.EmailDocuments.MissingMessageNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                sMessage = objXmlNode.InnerText;

                objDocumentManager = new DocumentManager(m_userLogin, base.ClientId);
                objDocumentManager.ConnectionString = this.connectionString;
                objDocumentManager.SecurityConnectionString = this.securityConnectionString; //MITS 27764 hlv 5/23/12
                objDocumentManager.UserLoginName = this.loginName;

                // WARNING - Integer value returned from RiskmasterDatabase object
                // is being casted to StorageType enum.
                objDocumentManager.DocumentStorageType =
                    (StorageType)this.userLogin.objRiskmasterDatabase.DocPathType;

                if (this.userLogin.DocumentPath.Length > 0)
                {
                    // use StoragePath set for the user
                    objDocumentManager.DestinationStoragePath =
                        this.userLogin.DocumentPath;
                }
                else
                {
                    // use StoragePath set for the DSN
                    objDocumentManager.DestinationStoragePath =
                        this.userLogin.objRiskmasterDatabase.GlobalDocPath;
                }

                objXmlNode = p_objXmlDocIn.SelectSingleNode("//psid");
                if (objXmlNode != null)
                {
                    if (objXmlNode.InnerText.Trim() != "")
                    {
                        iPSID = int.Parse(objXmlNode.InnerText);
                    }
                }
                objXmlNode = null;
                //Nitesh TR 2527
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//TableName");
                if (objXmlNode != null)
                {
                    sTableName = objXmlNode.InnerText;
                }
                objXmlNode = null;
                //For Admin Traking
                iPSID = objDocumentManager.GetBaseSecurityIDForTable(iPSID, sTableName);
                //pmahli MITS 10739 11/20/2007 
                //added parameter subject 
                objDocumentManager.EmailDocuments(objDocumentManager.UserLoginName, this.securityConnectionString,
                    sEmailIds, sMessage, objDocumentManager.DestinationStoragePath, sDocumentIds, iPSID, sSubject);

                bReturnValue = true;
            }
            catch (InitializationException p_objInitializationException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInitializationException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInvalidValueException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (RMAppException p_objRMAppException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objRMAppException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException, Globalization.GetString("DocumentManagementAdaptor.EmailDocuments.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            finally
            {
                objXmlNode = null;
                objDocumentManager = null;
            }

            return bReturnValue;
        }
        public bool EmailDocumentsNew(EmailDocumentsRequest p_objDocIn,
            ref BusinessAdaptorErrors p_objErrors)
        {
            bool bReturnValue = false;
            DocumentManager objDocumentManager = null;

            XmlNode objXmlNode = null;

            string sEmailIds = string.Empty;
            string sDocumentIds = string.Empty;
            string sMessage = string.Empty;
            string sTableName = string.Empty;//Nitesh
            string sSubject = string.Empty;//pmahli MITS 10739 11/20/2007
            int iPSID = 0;

            try
            {

                if (p_objDocIn.UserEmailIds != null && p_objDocIn.UserEmailIds != "")
                    sEmailIds = p_objDocIn.UserEmailIds;


                if (p_objDocIn.OtherEmailIds != null && p_objDocIn.OtherEmailIds != "")
                {
                    if (p_objDocIn.OtherEmailIds.Length > 0)
                    {
                        if (sEmailIds.Length > 0)
                            sEmailIds += ","; //pmahli MITS 10739 ';' replaced with ','
                        sEmailIds += p_objDocIn.OtherEmailIds;
                    }
                }

                if (sEmailIds.Length == 0)
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.EmailDocuments.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.EmailDocuments.MissingEmailIdsNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }


                if (p_objDocIn.Documents == null || p_objDocIn.Documents == "")
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.EmailDocuments.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.EmailDocuments.MissingDocumentIdsNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                sDocumentIds = p_objDocIn.Documents;

                //pmahli MITS 10739

                if (p_objDocIn.Subject == null)
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.EmailDocuments.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.EmailDocuments.MissingDocumentIdsNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                sSubject = p_objDocIn.Subject;
                //pmahli MITS 10739



                if (p_objDocIn.Message == null)
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.EmailDocuments.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.EmailDocuments.MissingMessageNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                sMessage = p_objDocIn.Message;

                objDocumentManager = new DocumentManager(m_userLogin, base.ClientId);
                objDocumentManager.ConnectionString = this.connectionString;
                objDocumentManager.SecurityConnectionString = this.securityConnectionString; //MITS 27764 hlv 5/23/12
                objDocumentManager.UserLoginName = this.loginName;

                // WARNING - Integer value returned from RiskmasterDatabase object
                // is being casted to StorageType enum.
                objDocumentManager.DocumentStorageType =
                    (StorageType)this.userLogin.objRiskmasterDatabase.DocPathType;

                if (this.userLogin.DocumentPath.Length > 0)
                {
                    // use StoragePath set for the user
                    objDocumentManager.DestinationStoragePath =
                        this.userLogin.DocumentPath;
                }
                else
                {
                    // use StoragePath set for the DSN
                    objDocumentManager.DestinationStoragePath =
                        this.userLogin.objRiskmasterDatabase.GlobalDocPath;
                }


                if (p_objDocIn.Psid != null && p_objDocIn.Psid != "")
                {
                    if (p_objDocIn.Psid.Trim() != "")
                    {
                        iPSID = int.Parse(p_objDocIn.Psid);
                    }
                }

                //Nitesh TR 2527

                if (p_objDocIn.TableName != null && p_objDocIn.TableName != "")
                {
                    sTableName = p_objDocIn.TableName; 
                }

                //For Admin Traking
                iPSID = objDocumentManager.GetBaseSecurityIDForTable(iPSID, sTableName);
                //pmahli MITS 10739 11/20/2007 
                //added parameter subject 
                objDocumentManager.EmailDocuments(objDocumentManager.UserLoginName, this.securityConnectionString,
                    sEmailIds, sMessage, objDocumentManager.DestinationStoragePath, sDocumentIds, iPSID, sSubject);

                bReturnValue = true;
            }
            catch (InitializationException p_objInitializationException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInitializationException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInvalidValueException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (RMAppException p_objRMAppException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objRMAppException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException, Globalization.GetString("DocumentManagementAdaptor.EmailDocuments.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            finally
            {
                objXmlNode = null;
                objDocumentManager = null;
            }

            return bReturnValue;
        }

        #endregion

        #region public bool AddDocument(XmlDocument p_objXmlDocIn,ref XmlDocument p_objXmlDocOut,ref BusinessAdaptorErrors p_objErrors)
        /// Name			: AddDocument
        /// Author			: Mohit Yadav
        /// Date Created	: 12-May-2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Adding the document to the specified folder 
        /// </summary>
        /// <param name="p_objXmlDocIn"> 
        ///	Input paramters in xml format.
        ///	Structure:
        ///	<AddDocument>
        ///		<DocumentXml>
        ///			<data>
        ///				<Document>
        ///					<DocumentId></DocumentId>
        ///					<FolderId>20</FolderId>
        ///					<CreateDate>20040617172300</CreateDate>
        ///					<Category></Category>
        ///					<Name>New</Name>
        ///					<Class></Class>
        ///					<Subject></Subject>
        ///					<Type>0</Type>
        ///					<Notes>NN</Notes>
        ///					<UserLoginName>ABABBAR3</UserLoginName>
        ///					<FileName>Cmd-2.txt</FileName>
        ///					<FilePath></FilePath>
        ///					<Keywords>Key</Keywords>
        ///					<AttachTable></AttachTable>
        ///					<AttachRecordId></AttachRecordId>
        ///				</Document>
        ///			</data>
        ///		</DocumentXml>
        ///		<File></File>
        ///	</AddDocument>
        /// </param>
        /// <param name="p_objXmlDocOut"> 
        ///	Method output in xml format.
        ///	Structure:
        ///		
        ///	</param>
        /// <param name="p_objErrors">
        ///	Collection of errors/messages.	
        ///	</param>
        /// <returns>
        ///		true - success
        ///		false - failure
        ///	</returns>

        public bool AddDocument(DocumentType p_objDocument,
            ref BusinessAdaptorErrors p_objErrors)
        {

            bool bReturnValue = false;

            XmlElement objXmlElement = null;
            XmlElement objRootElement = null;
            XmlNode objXmlNode = null;

            XmlDocument objXmlDocument = null;

            string sXmlDocIn = string.Empty;
            DateTime dtCreateDate = System.DateTime.Now;

            string sDocumentId = string.Empty;
            string sCategory = string.Empty;
            string sClass = string.Empty;
            string sSubject = string.Empty;
            string sType = string.Empty;
            string sFilePath = string.Empty;
            string sAttachTable = string.Empty;
            string sAttachRecordId = string.Empty;

            string sFolderId = string.Empty;
            string sDocTitle = string.Empty;
            string sNotes = string.Empty;
            string sFileName = string.Empty;
            string sKeywords = string.Empty;
            string sFile = string.Empty;

            long lDocumentId = 0;
            MemoryStream objDocumentManagementFile = null;
            int iPSID = 0;

            //Mona:PaperVisionMerge : Animesh Inserted For paperVision Enhancement MITS 16697
            SysSettings objSettings;
            RMConfigurator objConfig;
            PVWrapper objPaperVision;
            PaperVisionDocumentManager objPVDocManager;
            DbReader objReader = null;
            //Animesh Insertion Ends
            try
            {
                //Mona:PaperVisionMerge : Animesh Inserted 
                objSettings = new SysSettings(connectionString,base.ClientId);
                objConfig = new RMConfigurator();
                objPaperVision = new PVWrapper();  
                //Animesh Insertion Ends

                sFolderId = p_objDocument.FolderId;


                sDocTitle = p_objDocument.Title;


                sSubject = p_objDocument.Subject;

                
                if (p_objDocument.AttachRecordId == -19)//smishra25: when the call is from Outlook add in
                {
                    sType = "0";sClass = "0";sCategory = "0";
                    
                    
                }
                else
                {

                sType = p_objDocument.Type.Id.ToString();


                sClass = p_objDocument.Class.Id.ToString();


                sCategory = p_objDocument.Category.Id.ToString();
                }


                sNotes = p_objDocument.Notes;



                sFileName = p_objDocument.FileName;


                //Shruti, in case of oracle, RMWorld cud not pick documents attached from RMX
                sAttachTable = p_objDocument.AttachTable.ToUpper();



                sAttachRecordId = p_objDocument.AttachRecordId.ToString();

                //Aman Mobile Adjuster -- If condition for the Caller to be Mobile adjuster
                if (p_objDocument.bMobileAdj || p_objDocument.bMobilityAdj)
                {
                    if (p_objDocument.ClaimId < 0)
                    {
                        string sSQL = "SELECT CLAIM_ID,EVENT_ID,LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_NUMBER='" + p_objDocument.Claimnumber + "'";

                        objReader = DbFactory.ExecuteReader(this.connectionString, sSQL);
                        if (objReader.Read())
                        {
                            p_objDocument.AttachRecordId = Conversion.ConvertObjToInt(objReader.GetValue("CLAIM_ID"), base.ClientId);
                            sAttachRecordId = Conversion.ConvertObjToStr(objReader.GetValue("CLAIM_ID"));
                            switch (Conversion.ConvertObjToInt(objReader.GetValue("LINE_OF_BUS_CODE"), base.ClientId))
                            {
                                case 241:
                                    p_objDocument.PsId = 150;
                                    break;
                                case 243:
                                    p_objDocument.PsId = 3000;
                                    break;
                                case 242:
                                    p_objDocument.PsId = 6000;
                                    break;
                                case 844:
                                    p_objDocument.PsId = 60000;
                                    break;
                                case 845:
                                    p_objDocument.PsId = 40000;
                                    break;
                            }
                        }
                    }
                    else if (p_objDocument.EventId < 0)  //rsharma220: Added for mobile changes
                    {
                        string sSQL = "SELECT EVENT_ID FROM EVENT WHERE EVENT_NUMBER='" + p_objDocument.Eventnumber + "'";

                        objReader = DbFactory.ExecuteReader(this.connectionString, sSQL);
                        if (objReader.Read())
                        {
                            p_objDocument.AttachRecordId = Conversion.ConvertObjToInt(objReader.GetValue("EVENT_ID"), base.ClientId);
                            sAttachRecordId = Conversion.ConvertObjToStr(objReader.GetValue("EVENT_ID"));
                        }
                    }
                }
                //Aman Mobile Adjuster end
                string[] arrsFileNameParts = null;
                //string sDelimiter = "\";
                if (sFileName != "")
                {
                    arrsFileNameParts = sFileName.Split("\\".ToCharArray());
                    foreach (string sFName in arrsFileNameParts)
                    {
                        if (sFName.IndexOf('.') > 0)
                        {
                            sFileName = sFName;
                        }
                    }
                }



                sFilePath = p_objDocument.FilePath;



                sKeywords = p_objDocument.Keywords;


                byte[] FileContents = p_objDocument.FileContents;

                objXmlNode = null;

                dtCreateDate = System.DateTime.Now;

                //Mona:PaperVisionMerge : Animesh Inserted 
                if (objSettings.UsePaperVisionInterface == true)
                {
                    if (sAttachTable == "CLAIM")
                    {
                        objPaperVision.PVEntityID = PaperVisionSection.EntityID;
                        objPaperVision.PVLoginUrl = PaperVisionSection.LoginUrl;
                        objPaperVision.PVProjID = PaperVisionSection.ProjectID;
                        objPaperVision.PVServerAddress = PaperVisionSection.PVServerAddress;
                        objPaperVision.PVShowDocLinkUrl = PaperVisionSection.DocumentLink;
                        //Animesh Insertion MITS 18345
                        objPaperVision.SharedNetworkLocationPath = PaperVisionSection.SharedNetworkLocationPath;
                        //objPaperVision.MappedDriveName = PaperVisionSection.MappedDriveName;    
                        objPaperVision.DomainName = PaperVisionSection.DomainName;
                        objPaperVision.UserID = PaperVisionSection.UserID;
                        objPaperVision.Password = PaperVisionSection.Password;   
                        //Animesh Insertion Ends
                        objPVDocManager = new PaperVisionDocumentManager(m_userLogin, base.ClientId);
                        objPVDocManager.ConnectionString = this.connectionString;
                        objPVDocManager.UserLoginName = this.loginName;
                        objPVDocManager.TableName = sAttachTable;
                        objXmlDocument = new XmlDocument();
                        objRootElement = objXmlDocument.CreateElement("data");
                        objXmlElement = objXmlDocument.CreateElement("Document");
                        objXmlDocument.AppendChild(objRootElement);
                        objRootElement.AppendChild(objXmlElement);
                        objXmlNode = objXmlDocument.CreateElement("DocumentId");
                        objXmlNode.InnerText = sDocumentId.ToString();
                        objXmlElement.AppendChild(objXmlNode);
                        objXmlNode = null;
                        objXmlNode = objXmlDocument.CreateElement("CreateDate");
                        objXmlNode.InnerText = dtCreateDate.ToString();
                        objXmlElement.AppendChild(objXmlNode);
                        objXmlNode = null;
                        objXmlNode = objXmlDocument.CreateElement("Category");
                        objXmlNode.InnerText = sCategory.ToString();
                        objXmlElement.AppendChild(objXmlNode);
                        objXmlNode = null;
                        objXmlNode = objXmlDocument.CreateElement("Name");
                        objXmlNode.InnerText = sDocTitle.ToString();
                        objXmlElement.AppendChild(objXmlNode);
                        objXmlNode = null;
                        objXmlNode = objXmlDocument.CreateElement("Class");
                        objXmlNode.InnerText = sClass.ToString();
                        objXmlElement.AppendChild(objXmlNode);
                        objXmlNode = null;
                        objXmlNode = objXmlDocument.CreateElement("Subject");
                        objXmlNode.InnerText = sSubject.ToString();
                        objXmlElement.AppendChild(objXmlNode);
                        objXmlNode = null;
                        objXmlNode = objXmlDocument.CreateElement("Type");
                        objXmlNode.InnerText = sType.ToString();
                        objXmlElement.AppendChild(objXmlNode);
                        objXmlNode = null;
                        objXmlNode = objXmlDocument.CreateElement("Notes");
                        objXmlNode.InnerText = sNotes.ToString();
                        objXmlElement.AppendChild(objXmlNode);
                        objXmlNode = null;
                        objXmlNode = objXmlDocument.CreateElement("UserLoginName");
                        objXmlNode.InnerText = objPVDocManager.UserLoginName;
                        objXmlElement.AppendChild(objXmlNode);
                        objXmlNode = null;
                        objXmlNode = objXmlDocument.CreateElement("FileName");
                        objXmlNode.InnerText = sFileName.ToString();
                        objXmlElement.AppendChild(objXmlNode);
                        objXmlNode = null;
                        objXmlNode = objXmlDocument.CreateElement("Keywords");
                        objXmlNode.InnerText = sKeywords.ToString();
                        objXmlElement.AppendChild(objXmlNode);
                        objXmlNode = null;
                        objXmlNode = objXmlDocument.CreateElement("AttachTable");
                        objXmlNode.InnerText = sAttachTable.ToString();
                        objXmlElement.AppendChild(objXmlNode);
                        objXmlNode = null;
                        objXmlNode = objXmlDocument.CreateElement("AttachRecordId");
                        objXmlNode.InnerText = sAttachRecordId.ToString();
                        objXmlElement.AppendChild(objXmlNode);
                        objXmlNode = null;
                        iPSID = p_objDocument.PsId;
                        sXmlDocIn = objXmlDocument.OuterXml;
                        objDocumentManagementFile = new MemoryStream(p_objDocument.FileContents);
                        objPVDocManager.AddDocument(sXmlDocIn, objDocumentManagementFile, iPSID, objPaperVision, out lDocumentId);
                        bReturnValue = true;
                    }
                    else
                    {
                        System.Exception exc = new Exception("PaperVision Document System is active. Attachments functionality will only be available for claims.");
                        p_objErrors.Add(exc, "PaperVision Document System is active. Attachments functionality will only be available for claims.", BusinessAdaptorErrorType.Error);
                        return false;
                    }
                }
                else
                {
                    //Animesh Insertion Ends
                DocumentManager objDocumentManager = null;

                objDocumentManager = new DocumentManager(m_userLogin, base.ClientId);
                objDocumentManager.ConnectionString = this.connectionString;
                objDocumentManager.SecurityConnectionString = this.securityConnectionString; //MITS 27764 hlv 5/23/12
                objDocumentManager.UserLoginName = this.loginName;

                if (p_objDocument.AttachRecordId == -19)//smishra25: when the call is from Outlook add in
                {
                    FetchLOBAndClaimId(ref p_objDocument, ref sAttachRecordId, ref sFolderId, ref objDocumentManager);
                }

                // WARNING - Integer value returned from RiskmasterDatabase object
                // is being casted to StorageType enum.
                //mgaba2:MITS 11704-04/07/2008-inserted the following code in else part
                /* objDocumentManager.DocumentStorageType = 
                    (StorageType) this.userLogin.objRiskmasterDatabase.DocPathType; */

                //mgaba2:MITS 11704-04/07/2008-Changed the condition
                //if( objDocumentManager.DocumentStorageType==StorageType.FileSystemStorage && this.userLogin.DocumentPath.Length>0)
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    // use StoragePath set for the user
                    objDocumentManager.DestinationStoragePath =
                        this.userLogin.DocumentPath;
                    //mgaba2:MITS 11704-04/07/2008-doc path entered at user level has to be file system
                    objDocumentManager.DocumentStorageType = StorageType.FileSystemStorage;
                }
                else if (this.userLogin.objRiskmasterDatabase.GlobalDocPath.Length > 0)
                {
                    // use StoragePath set for the DSN
                    objDocumentManager.DestinationStoragePath =
                        this.userLogin.objRiskmasterDatabase.GlobalDocPath;
                    //mgaba2:MITS 11704-04/07/2008-if no doc path entered at user level
                    objDocumentManager.DocumentStorageType =
                    (StorageType)this.userLogin.objRiskmasterDatabase.DocPathType;
                }
                else
                {
                    System.Exception exc = new Exception("Document path invalid or not set. Consult your System Administrator. Tip: You can define Document Path in Security Management System.");
                    p_objErrors.Add(exc, "Document path invalid or not set. Consult your System Administrator. Tip: You can define Document Path in Security Management System.", BusinessAdaptorErrorType.Error);
                    return false;
                }

                objXmlDocument = new XmlDocument();

                objRootElement = objXmlDocument.CreateElement("data");
                objXmlElement = objXmlDocument.CreateElement("Document");
                objXmlDocument.AppendChild(objRootElement);
                objRootElement.AppendChild(objXmlElement);

                objXmlNode = objXmlDocument.CreateElement("DocumentId");
                objXmlNode.InnerText = sDocumentId.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("FolderId");
                objXmlNode.InnerText = sFolderId.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("CreateDate");
                objXmlNode.InnerText = dtCreateDate.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Category");
                objXmlNode.InnerText = sCategory.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Name");
                objXmlNode.InnerText = sDocTitle.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Class");
                objXmlNode.InnerText = sClass.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Subject");
                objXmlNode.InnerText = sSubject.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Type");
                objXmlNode.InnerText = sType.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Notes");
                objXmlNode.InnerText = sNotes.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("UserLoginName");
                objXmlNode.InnerText = objDocumentManager.UserLoginName;
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("FileName");
                objXmlNode.InnerText = sFileName.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("FilePath");
                objXmlNode.InnerText = sFilePath.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Keywords");
                objXmlNode.InnerText = sKeywords.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("AttachTable");
                objXmlNode.InnerText = sAttachTable.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("AttachRecordId");
                objXmlNode.InnerText = sAttachRecordId.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                iPSID = p_objDocument.PsId;


                sXmlDocIn = objXmlDocument.OuterXml;

                //BSB 03.24.2006 Add case to handle situation if "FileProcessor" Soap Extension was enabled
                // in web.config and Document was cached to a file on disk during soap message handling.
                /* if (m_bSoapAttachmentCachedToDisk)
                     objDocumentManagementFile = Riskmaster.Common.Utilities.ReadFileMemoryStream(sFile);
                 else
                     objDocumentManagementFile = new MemoryStream(Convert.FromBase64String(sFile));
                 */
                objDocumentManagementFile = new MemoryStream(p_objDocument.FileContents);
                objDocumentManager.AddDocument(sXmlDocIn, objDocumentManagementFile, iPSID, out lDocumentId);

                bReturnValue = true;
                }
            }

            catch (InitializationException p_objInitializationException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInitializationException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInvalidValueException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (RMAppException p_objRMAppException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objRMAppException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException,
                    Globalization.GetString("DocumentManagementAdaptor.AddDocument.Error",base.ClientId),
                    BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            finally
            {
                objXmlNode = null;
                objXmlElement = null;
                objRootElement = null;
                //objDocumentManager = null;				
                //Mona:PaperVisionMerge : Animesh inserted 
                objSettings = null;
                objConfig = null;
                objPaperVision = null;
                objPVDocManager = null;
                //Animesh insertion ends
            }

            return bReturnValue;
        }
        /*public bool AddDocument(XmlDocument p_objXmlDocIn,ref XmlDocument p_objXmlDocOut,
            ref BusinessAdaptorErrors p_objErrors)
        {
			
            bool bReturnValue = false;

            XmlElement objXmlElement = null;
            XmlElement objRootElement = null;
            XmlNode objXmlNode = null;

            XmlDocument objXmlDocument = null;

            string sXmlDocIn = string.Empty;
            DateTime dtCreateDate = System.DateTime.Now;

            string sDocumentId = string.Empty;
            string sCategory = string.Empty;
            string sClass = string.Empty;
            string sSubject = string.Empty;
            string sType = string.Empty;
            string sFilePath = string.Empty;
            string sAttachTable = string.Empty;
            string sAttachRecordId = string.Empty;

            string sFolderId = string.Empty;
            string sDocTitle = string.Empty;
            string sNotes = string.Empty;
            string sFileName = string.Empty;
            string sKeywords = string.Empty;
            string sFile = string.Empty; 

            long lDocumentId = 0;
            MemoryStream objDocumentManagementFile=null;
            int iPSID = 0;

            try
            {
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//AddDocument/DocumentXml/data/Document/FolderId");
				
                if(objXmlNode==null)
                { 
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.AddDocument.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.AddDocument.MissingFolderIdNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);  
                    return bReturnValue;
                }

                sFolderId = objXmlNode.InnerText;

                objXmlNode = null;
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//AddDocument/DocumentXml/data/Document/Name");
				
                if(objXmlNode==null)
                { 
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.AddDocument.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.AddDocument.MissingNameNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);  
                    return bReturnValue;
                }

                sDocTitle = objXmlNode.InnerText;

                objXmlNode = null;
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//AddDocument/DocumentXml/data/Document/Subject");
				
                if(objXmlNode==null)
                { 
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.AddDocument.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.AddDocument.MissingSubjectNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);  
                    return bReturnValue;
                }

                sSubject = objXmlNode.InnerText;

                objXmlNode = null;
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//AddDocument/DocumentXml/data/Document/Type");
				
                if(objXmlNode==null)
                { 
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.AddDocument.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.AddDocument.MissingTypeNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);  
                    return bReturnValue;
                }

                sType = objXmlNode.InnerText;

                objXmlNode = null;
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//AddDocument/DocumentXml/data/Document/Class");
				
                if(objXmlNode==null)
                { 
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.AddDocument.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.AddDocument.MissingClassNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);  
                    return bReturnValue;
                }

                sClass = objXmlNode.InnerText;

                objXmlNode = null;
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//AddDocument/DocumentXml/data/Document/Category");
				
                if(objXmlNode==null)
                { 
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.AddDocument.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.AddDocument.MissingCategoryNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);  
                    return bReturnValue;
                }

                sCategory = objXmlNode.InnerText;

                objXmlNode = null;
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//AddDocument/DocumentXml/data/Document/Notes");
				
                if(objXmlNode==null)
                { 
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.AddDocument.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.AddDocument.MissingNotesNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);  
                    return bReturnValue;
                }

                sNotes = objXmlNode.InnerText;

                objXmlNode = null;
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//AddDocument/DocumentXml/data/Document/FileName");
				
                if(objXmlNode==null)
                { 
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.AddDocument.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.AddDocument.MissingFileNameNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);  
                    return bReturnValue;
                }

                sFileName = objXmlNode.InnerText;

                objXmlNode = null;
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//AddDocument/DocumentXml/data/Document/AttachTable");
				
                if(objXmlNode==null)
                { 
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.AddDocument.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.AddDocument.MissingAttachTableNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);  
                    return bReturnValue;
                }
                //Shruti, in case of oracle, RMWorld cud not pick documents attached from RMX
                sAttachTable = objXmlNode.InnerText.ToUpper();

                objXmlNode = null;
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//AddDocument/DocumentXml/data/Document/AttachRecordId");
				
                if(objXmlNode==null)
                { 
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.AddDocument.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.AddDocument.MissingAttachRecordIdNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);  
                    return bReturnValue;
                }

                sAttachRecordId = objXmlNode.InnerText;
				
                string [] arrsFileNameParts = null;
                //string sDelimiter = "\";
                if (sFileName != "")
                {
                    arrsFileNameParts = sFileName.Split("\\".ToCharArray());
                    foreach(string sFName in arrsFileNameParts)
                    {
                        if (sFName.IndexOf('.') > 0)
                        {
                            sFileName = sFName;
                        }
                    }					
                }

                objXmlNode = null;
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//AddDocument/DocumentXml/data/Document/FilePath");
				
                if(objXmlNode==null)
                { 
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.AddDocument.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.AddDocument.MissingFilePathNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);  
                    return bReturnValue;
                }

                sFilePath = objXmlNode.InnerText;

                objXmlNode = null;
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//AddDocument/DocumentXml/data/Document/Keywords");
				
                if(objXmlNode==null)
                { 
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.AddDocument.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.AddDocument.MissingKeywordsNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);  
                    return bReturnValue;
                }

                sKeywords = objXmlNode.InnerText;

                objXmlNode = null;
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//AddDocument/DocumentXml/data/Document/File");
				
                if(objXmlNode==null)
                { 
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.AddDocument.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.AddDocument.MissingFileNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);  
                    return bReturnValue;
                }

                sFile = objXmlNode.InnerText;
                //bSoapAttachmentCachedToDisk = (""!=Riskmaster.Common.RMConfigurator.Value(@"\RMSettings\CommonWebServiceHandler\UseAttachmentServiceExtension"));
                //bSoapAttachmentCachedToDisk = ((objXmlNode as XmlElement).GetAttribute("att-cached")=="1");

                objXmlNode = null;

                dtCreateDate = System.DateTime.Now;
				
                DocumentManager objDocumentManager = null;

                objDocumentManager = new DocumentManager( m_userLogin );
                objDocumentManager.ConnectionString = this.connectionString;
                objDocumentManager.UserLoginName = this.loginName;
				
                // WARNING - Integer value returned from RiskmasterDatabase object
                // is being casted to StorageType enum.
                objDocumentManager.DocumentStorageType = 
                    (StorageType) this.userLogin.objRiskmasterDatabase.DocPathType;
				
                if( objDocumentManager.DocumentStorageType==StorageType.FileSystemStorage && this.userLogin.DocumentPath.Length>0)
                {
                    // use StoragePath set for the user
                    objDocumentManager.DestinationStoragePath = 
                        this.userLogin.DocumentPath;
                }
                else if(this.userLogin.objRiskmasterDatabase.GlobalDocPath.Length>0)
                {
                    // use StoragePath set for the DSN
                    objDocumentManager.DestinationStoragePath = 
                        this.userLogin.objRiskmasterDatabase.GlobalDocPath;   
                }
                else
                {
                    System.Exception exc=new Exception("Document path invalid or not set. Consult your System Administrator. Tip: You can define Document Path in Security Management System.");
                    p_objErrors.Add(exc,"Document path invalid or not set. Consult your System Administrator. Tip: You can define Document Path in Security Management System.",BusinessAdaptorErrorType.Error);
                    return false;
                }	

                objXmlDocument = new XmlDocument();

                objRootElement = objXmlDocument.CreateElement("data");
                objXmlElement = objXmlDocument.CreateElement("Document");
                objXmlDocument.AppendChild(objRootElement);
                objRootElement.AppendChild(objXmlElement);

                objXmlNode = objXmlDocument.CreateElement("DocumentId");
                objXmlNode.InnerText = sDocumentId.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;
				
                objXmlNode = objXmlDocument.CreateElement("FolderId");
                objXmlNode.InnerText = sFolderId.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("CreateDate");
                objXmlNode.InnerText = dtCreateDate.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Category");
                objXmlNode.InnerText = sCategory.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Name");
                objXmlNode.InnerText = sDocTitle.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Class");
                objXmlNode.InnerText = sClass.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Subject");
                objXmlNode.InnerText = sSubject.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Type");
                objXmlNode.InnerText = sType.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Notes");
                objXmlNode.InnerText = sNotes.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("UserLoginName");
                objXmlNode.InnerText = objDocumentManager.UserLoginName;
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("FileName");
                objXmlNode.InnerText = sFileName.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("FilePath");
                objXmlNode.InnerText = sFilePath.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Keywords");
                objXmlNode.InnerText = sKeywords.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;	
			
                objXmlNode = objXmlDocument.CreateElement("AttachTable");
                objXmlNode.InnerText = sAttachTable.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;	

                objXmlNode = objXmlDocument.CreateElement("AttachRecordId");
                objXmlNode.InnerText = sAttachRecordId.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;	

                objXmlNode = p_objXmlDocIn.SelectSingleNode("//psid");
                if( objXmlNode != null)
                {
                    if(objXmlNode.InnerText.Trim() != "" )
                    {
                        iPSID = int.Parse(objXmlNode.InnerText);
                    }
                }
                objXmlNode = null;

                sXmlDocIn = objXmlDocument.OuterXml;

                //BSB 03.24.2006 Add case to handle situation if "FileProcessor" Soap Extension was enabled
                // in web.config and Document was cached to a file on disk during soap message handling.
                if(m_bSoapAttachmentCachedToDisk)
                    objDocumentManagementFile = Riskmaster.Common.Utilities.ReadFileMemoryStream(sFile);
                else
                    objDocumentManagementFile = new MemoryStream(Convert.FromBase64String(sFile));
				
                objDocumentManager.AddDocument(sXmlDocIn, objDocumentManagementFile, iPSID, out lDocumentId);
				
                bReturnValue = true;
            }
				
            catch(InitializationException p_objInitializationException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInitializationException,BusinessAdaptorErrorType.Error);   
                return bReturnValue;
            }
            catch(InvalidValueException p_objInvalidValueException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInvalidValueException,BusinessAdaptorErrorType.Error);   
                return bReturnValue;
            }
            catch(RMAppException p_objRMAppException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objRMAppException,BusinessAdaptorErrorType.Error);   
                return bReturnValue;
            }
            catch(Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException,
                    Globalization.GetString("DocumentManagementAdaptor.AddDocument.Error",base.ClientId),
                    BusinessAdaptorErrorType.Error);   
                return bReturnValue;
            }
            finally
            {
                objXmlNode = null;
                objXmlElement = null;
                objRootElement =  null;
                //objDocumentManager = null;				
            }

            return bReturnValue;
        }*/

        public bool AddDocument(StreamedDocumentType p_objDocument, ref BusinessAdaptorErrors p_objErrors)
        {
            bool bReturnValue = false;

            XmlElement objXmlElement = null;
            XmlElement objRootElement = null;
            XmlNode objXmlNode = null;

            XmlDocument objXmlDocument = null;

            string sXmlDocIn = string.Empty;
            DateTime dtCreateDate = System.DateTime.Now;

            string sDocumentId = string.Empty;
            string sCategory = string.Empty;
            string sClass = string.Empty;
            string sSubject = string.Empty;
            string sType = string.Empty;
            string sFilePath = string.Empty;
            string sAttachTable = string.Empty;
            string sAttachRecordId = string.Empty;

            string sFolderId = string.Empty;
            string sDocTitle = string.Empty;
            string sNotes = string.Empty;
            string sFileName = string.Empty;
            string sKeywords = string.Empty;
            string sFile = string.Empty;
            string[] arrsFileNameParts = null;

            Stream objDocumentManagementFile = null;
            DocumentManager objDocumentManager = null;
            SysSettings objSettings = null;
            RMConfigurator objConfig = null;
            Acrosoft objAcrosoft = null;
            int iPSID = 0;
            DbReader objReader = null;    //Aman Mobile Adjuster
            try
            {
                sFolderId = p_objDocument.FolderId;
                sDocTitle = p_objDocument.Title;
                sSubject = p_objDocument.Subject;
                sType = p_objDocument.Type.Id.ToString();
                sClass = p_objDocument.Class.Id.ToString();
                sCategory = p_objDocument.Category.Id.ToString();
                sNotes = p_objDocument.Notes;
                sFileName = p_objDocument.FileName;
                //Shruti, in case of oracle, RMWorld cud not pick documents attached from RMX
                sAttachTable = p_objDocument.AttachTable.ToUpper();
                sAttachRecordId = p_objDocument.AttachRecordId.ToString();
                sDocumentId = p_objDocument.DocumentId.ToString();

                //string sDelimiter = "\";
                if (sFileName != "")
                {
                    arrsFileNameParts = sFileName.Split("\\".ToCharArray());
                    foreach (string sFName in arrsFileNameParts)
                    {
                        if (sFName.IndexOf('.') > 0)
                        {
                            sFileName = sFName;
                        }
                    }
                }

                sFilePath = p_objDocument.FilePath;
                sKeywords = p_objDocument.Keywords;
                
                objXmlNode = null;
                dtCreateDate = System.DateTime.Now;

                objDocumentManager = new DocumentManager(m_userLogin, base.ClientId);
                objDocumentManager.ConnectionString = this.connectionString;
                objDocumentManager.SecurityConnectionString = this.securityConnectionString; //MITS 27764 hlv 5/23/12
                objDocumentManager.UserLoginName = this.loginName;

                 //Aman Mobile Adjuster -- If condition for the Caller to be Mobile adjuster
                if (p_objDocument.bMobileAdj || p_objDocument.bMobilityAdj)
                {
                    string sSQL = "SELECT CLAIM_ID,EVENT_ID,LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_NUMBER='" + p_objDocument.Claimnumber + "'";

                    objReader = DbFactory.ExecuteReader(this.connectionString, sSQL);
                    if (objReader.Read())
                    {
                        p_objDocument.AttachRecordId = Conversion.ConvertObjToInt(objReader.GetValue("CLAIM_ID"), base.ClientId);
                        sAttachRecordId = Conversion.ConvertObjToStr(objReader.GetValue("CLAIM_ID"));
                        switch (Conversion.ConvertObjToInt(objReader.GetValue("LINE_OF_BUS_CODE"), base.ClientId))
                        {
                            case 241:
                                p_objDocument.PsId = 150;
                                break;
                            case 243:
                                p_objDocument.PsId = 3000;
                                break;
                            case 242:
                                p_objDocument.PsId = 6000;
                                break;
                            case 844:
                                p_objDocument.PsId = 60000;
                                break;
                            case 845:
                                p_objDocument.PsId = 40000;
                                break;
                        }
                    }

                }
                //Aman Mobile Adjuster -- If condition for the Caller to be Mobile adjuster

                if (p_objDocument.AttachRecordId == -19)//smishra25: when the call is from Outlook add in
                {
                    FetchLOBAndClaimId(ref p_objDocument, ref sAttachRecordId, ref sFolderId, ref objDocumentManager);
                }
                //rsharma220 MITS 35659
                else if (p_objDocument.AttachRecordId == -20)
                {
                    FetchEventId(ref p_objDocument, ref sAttachRecordId, ref sFolderId, ref objDocumentManager);
                }
               
                // WARNING - Integer value returned from RiskmasterDatabase object
                // is being casted to StorageType enum.
                //mgaba2:MITS 11704-04/07/2008-inserted the following code in else part
                /* objDocumentManager.DocumentStorageType = 
                    (StorageType) this.userLogin.objRiskmasterDatabase.DocPathType; */

                //mgaba2:MITS 11704-04/07/2008-Changed the condition
                //if( objDocumentManager.DocumentStorageType==StorageType.FileSystemStorage && this.userLogin.DocumentPath.Length>0)
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    // use StoragePath set for the user
                    objDocumentManager.DestinationStoragePath = this.userLogin.DocumentPath;
                    //mgaba2:MITS 11704-04/07/2008-doc path entered at user level has to be file system
                    objDocumentManager.DocumentStorageType = StorageType.FileSystemStorage; 
                }
                else if (this.userLogin.objRiskmasterDatabase.GlobalDocPath.Length > 0)
                {
                    // use StoragePath set for the DSN
                    objDocumentManager.DestinationStoragePath = this.userLogin.objRiskmasterDatabase.GlobalDocPath;
                    //mgaba2:MITS 11704-04/07/2008-if no doc path entered at user level
                    objDocumentManager.DocumentStorageType =   (StorageType)this.userLogin.objRiskmasterDatabase.DocPathType;
                }
                else
                {
                    System.Exception exc = new Exception("Document path invalid or not set. Consult your System Administrator. Tip: You can define Document Path in Security Management System.");
                    p_objErrors.Add(exc, "Document path invalid or not set. Consult your System Administrator. Tip: You can define Document Path in Security Management System.", BusinessAdaptorErrorType.Error);
                    return false;
                }

                

                objXmlDocument = new XmlDocument();

                objRootElement = objXmlDocument.CreateElement("data");
                objXmlElement = objXmlDocument.CreateElement("Document");
                objXmlDocument.AppendChild(objRootElement);
                objRootElement.AppendChild(objXmlElement);

                objXmlNode = objXmlDocument.CreateElement("DocumentId");
                objXmlNode.InnerText = sDocumentId.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("FolderId");
                objXmlNode.InnerText = sFolderId.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("CreateDate");
                objXmlNode.InnerText = dtCreateDate.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Category");
                objXmlNode.InnerText = sCategory.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Name");
                objXmlNode.InnerText = sDocTitle.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Class");
                objXmlNode.InnerText = sClass.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Subject");
                objXmlNode.InnerText = sSubject.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Type");
                objXmlNode.InnerText = sType.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Notes");
                objXmlNode.InnerText = sNotes.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("UserLoginName");
                objXmlNode.InnerText = objDocumentManager.UserLoginName;
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("FileName");
                objXmlNode.InnerText = sFileName.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("FilePath");
                objXmlNode.InnerText = sFilePath.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Keywords");
                objXmlNode.InnerText = sKeywords.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("AttachTable");
                objXmlNode.InnerText = sAttachTable.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("AttachRecordId");
                objXmlNode.InnerText = sAttachRecordId.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                iPSID = p_objDocument.PsId;


                sXmlDocIn = objXmlDocument.OuterXml;

                //BSB 03.24.2006 Add case to handle situation if "FileProcessor" Soap Extension was enabled
                // in web.config and Document was cached to a file on disk during soap message handling.
                /* if (m_bSoapAttachmentCachedToDisk)
                     objDocumentManagementFile = Riskmaster.Common.Utilities.ReadFileMemoryStream(sFile);
                 else
                     objDocumentManagementFile = new MemoryStream(Convert.FromBase64String(sFile));
                 */
                
                //Aman Mobile Adjuster
                if (!p_objDocument.bMobileAdj && !p_objDocument.bMobilityAdj)
                {
                    objDocumentManagementFile = new MemoryStream(p_objDocument.FileContents);
                }
                else
                {
                    objDocumentManagementFile = new MemoryStream(p_objDocument.FileContents);
                }
                //Aman Mobile Adjuster

                //Raman Bhatia..06/14/2012
                //Post the document to Acrosoft if Acrosoft Interface is enabled
                objSettings = new SysSettings(m_connectionString, base.ClientId);
                if (objSettings.UseAcrosoftInterface && sAttachTable.ToLower() == "claim")
                {
                    int iClaimID = Conversion.ConvertStrToInteger(sAttachRecordId);
                    Claim objClaim = null;
                    DataModelFactory objDataModelFactory = new DataModelFactory(base.userLogin, base.ClientId);
                    objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                    objClaim.MoveTo(iClaimID);
                    objDataModelFactory.UnInitialize();

                    string sClaimNumber = objClaim.ClaimNumber;
                    string sEventNumber = objClaim.EventNumber;
                    string sAppExcpXml = "";
                    objAcrosoft = new Acrosoft(base.ClientId);//dvatsa-cloud
                    //rsolanki2 :  start updates for MCM mits 19200 
                    Boolean bSuccess = false;
                    string sTempAcrosoftUserId = base.userLogin.LoginName;
                    string sTempAcrosoftPassword = base.userLogin.Password;

                    if (Conversion.CastToType<Boolean>(AcrosoftSection.UseCommonAcrosoftUser, out bSuccess))
                    {
                        if (AcrosoftSection.AcroUserMapSection.UserMap[base.userLogin.LoginName] != null
                            && !string.IsNullOrEmpty(AcrosoftSection.AcroUserMapSection.UserMap[base.userLogin.LoginName].RmxUser))
                        {
                            sTempAcrosoftUserId = AcrosoftSection.AcroUserMapSection.UserMap[base.userLogin.LoginName].AcrosoftUserId;
                            sTempAcrosoftPassword = AcrosoftSection.AcroUserMapSection.UserMap[base.userLogin.LoginName].AcrosoftPassword;
                        }
                        else
                        {
                            sTempAcrosoftUserId = AcrosoftSection.AcrosoftUsername;
                            sTempAcrosoftPassword = AcrosoftSection.AcrosoftUserPassword;
                        }
                    }

                    byte[] byteArray;
                    //Amandeep Mobile Adjuster
                    if (!p_objDocument.bMobileAdj && !p_objDocument.bMobilityAdj)
                    {
                        using (BinaryReader br = new BinaryReader(objDocumentManagementFile))
                        {
                            byteArray = br.ReadBytes(Convert.ToInt32(p_objDocument.FileSize));
                        }
                    }
                    else
                    {
                        byteArray = p_objDocument.FileContents;
                    }
                    //Amandeep Mobile Adjuster
                    //creating folder
                    objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                        AcrosoftSection.AcrosoftAttachmentsTypeKey, sEventNumber, sClaimNumber,
                        AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                    //storing document
                    objAcrosoft.StoreObjectBuffer(sTempAcrosoftUserId , sTempAcrosoftPassword , sFileName , byteArray , sFileName , "" , sEventNumber , sClaimNumber , AcrosoftSection.AcrosoftAttachmentsTypeKey,
                        "", "", sTempAcrosoftUserId, out sAppExcpXml);

                    //rsolanki2 :  end updates for MCM mits 19200 

                    ////creating folder
                    //objAcrosoft.CreateAttachmentFolder(base.userLogin.LoginName , base.userLogin.Password , AcrosoftSection.AcrosoftAttachmentsTypeKey , sEventNumber , sClaimNumber , 
                    //    AcrosoftSection.EventFolderFriendlyName , AcrosoftSection.ClaimFolderFriendlyName , out sAppExcpXml); 
                    ////storing document
                    //objAcrosoft.StoreObjectBuffer(base.userLogin.LoginName, base.userLogin.Password, sFilePath, sDocTitle, "" , sEventNumber , sClaimNumber, AcrosoftSection.AcrosoftAttachmentsTypeKey, 
                    //    "", "" , userLogin.LoginName , out sAppExcpXml);
                }
                else
                {

                    objDocumentManager.AddLargeStream(sXmlDocIn, objDocumentManagementFile, iPSID, p_objDocument.FileSize);
                }

                bReturnValue = true;
            }

            catch (InitializationException p_objInitializationException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInitializationException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInvalidValueException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (RMAppException p_objRMAppException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objRMAppException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException,
                    Globalization.GetString("DocumentManagementAdaptor.AddDocument.Error",base.ClientId),
                    BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            finally
            {
                objXmlNode = null;
                objXmlElement = null;
                objRootElement = null;
                //objDocumentManager = null;				
            }

            return bReturnValue;

        }

        public bool AddDocument(StreamDocType p_objDocument, ref BusinessAdaptorErrors p_objErrors)
        {
            bool bReturnValue = false;

            XmlElement objXmlElement = null;
            XmlElement objRootElement = null;
            XmlNode objXmlNode = null;

            XmlDocument objXmlDocument = null;

            string sXmlDocIn = string.Empty;
            DateTime dtCreateDate = System.DateTime.Now;

            string sDocumentId = string.Empty;
            string sCategory = string.Empty;
            string sClass = string.Empty;
            string sSubject = string.Empty;
            string sType = string.Empty;
            string sFilePath = string.Empty;
            string sAttachTable = string.Empty;
            string sAttachRecordId = string.Empty;

            string sFolderId = string.Empty;
            string sDocTitle = string.Empty;
            string sNotes = string.Empty;
            string sFileName = string.Empty;
            string sKeywords = string.Empty;
            string sFile = string.Empty;
            string[] arrsFileNameParts = null;

            Stream objDocumentManagementFile = null;
            DocumentManager objDocumentManager = null;
            SysSettings objSettings = null;
            RMConfigurator objConfig = null;
            Acrosoft objAcrosoft = null;
            int iPSID = 0;
            DbReader objReader = null;    //Aman Mobile Adjuster
            try
            {
                sFolderId = p_objDocument.FolderId;
                sDocTitle = p_objDocument.Title;
                sSubject = p_objDocument.Subject;
                sType = p_objDocument.Type.Id.ToString();
                sClass = p_objDocument.Class.Id.ToString();
                sCategory = p_objDocument.Category.Id.ToString();
                sNotes = p_objDocument.Notes;
                sFileName = p_objDocument.FileName;
                //Shruti, in case of oracle, RMWorld cud not pick documents attached from RMX
                sAttachTable = p_objDocument.AttachTable.ToUpper();
                sAttachRecordId = p_objDocument.AttachRecordId.ToString();
                sDocumentId = p_objDocument.DocumentId.ToString();

                //string sDelimiter = "\";
                if (sFileName != "")
                {
                    arrsFileNameParts = sFileName.Split("\\".ToCharArray());
                    foreach (string sFName in arrsFileNameParts)
                    {
                        if (sFName.IndexOf('.') > 0)
                        {
                            sFileName = sFName;
                        }
                    }
                }

                sFilePath = p_objDocument.FilePath;
                sKeywords = p_objDocument.Keywords;

                objXmlNode = null;
                dtCreateDate = System.DateTime.Now;

                objDocumentManager = new DocumentManager(m_userLogin, base.ClientId);
                objDocumentManager.ConnectionString = this.connectionString;
                objDocumentManager.SecurityConnectionString = this.securityConnectionString; //MITS 27764 hlv 5/23/12
                objDocumentManager.UserLoginName = this.loginName;

                //Aman Mobile Adjuster -- If condition for the Caller to be Mobile adjuster
                if (p_objDocument.bMobileAdj || p_objDocument.bMobilityAdj)
                {
                    string sSQL = "SELECT CLAIM_ID,EVENT_ID,LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_NUMBER='" + p_objDocument.Claimnumber + "'";

                    objReader = DbFactory.ExecuteReader(this.connectionString, sSQL);
                    if (objReader.Read())
                    {
                        p_objDocument.AttachRecordId = Conversion.ConvertObjToInt(objReader.GetValue("CLAIM_ID"), base.ClientId);
                        sAttachRecordId = Conversion.ConvertObjToStr(objReader.GetValue("CLAIM_ID"));
                        switch (Conversion.ConvertObjToInt(objReader.GetValue("LINE_OF_BUS_CODE"), base.ClientId))
                        {
                            case 241:
                                p_objDocument.PsId = 150;
                                break;
                            case 243:
                                p_objDocument.PsId = 3000;
                                break;
                            case 242:
                                p_objDocument.PsId = 6000;
                                break;
                            case 844:
                                p_objDocument.PsId = 60000;
                                break;
                            case 845:
                                p_objDocument.PsId = 40000;
                                break;
                        }
                    }

                }
                //Aman Mobile Adjuster -- If condition for the Caller to be Mobile adjuster

                if (p_objDocument.AttachRecordId == -19)//smishra25: when the call is from Outlook add in
                {
                    FetchLOBAndClaimId(ref p_objDocument, ref sAttachRecordId, ref sFolderId, ref objDocumentManager);
                }
                //rsharma220 MITS 35659
                else if (p_objDocument.AttachRecordId == -20)
                {
                    FetchEventId(ref p_objDocument, ref sAttachRecordId, ref sFolderId, ref objDocumentManager);
                }

                // WARNING - Integer value returned from RiskmasterDatabase object
                // is being casted to StorageType enum.
                //mgaba2:MITS 11704-04/07/2008-inserted the following code in else part
                /* objDocumentManager.DocumentStorageType = 
                    (StorageType) this.userLogin.objRiskmasterDatabase.DocPathType; */

                //mgaba2:MITS 11704-04/07/2008-Changed the condition
                //if( objDocumentManager.DocumentStorageType==StorageType.FileSystemStorage && this.userLogin.DocumentPath.Length>0)
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    // use StoragePath set for the user
                    objDocumentManager.DestinationStoragePath = this.userLogin.DocumentPath;
                    //mgaba2:MITS 11704-04/07/2008-doc path entered at user level has to be file system
                    objDocumentManager.DocumentStorageType = StorageType.FileSystemStorage;
                }
                else if (this.userLogin.objRiskmasterDatabase.GlobalDocPath.Length > 0)
                {
                    // use StoragePath set for the DSN
                    objDocumentManager.DestinationStoragePath = this.userLogin.objRiskmasterDatabase.GlobalDocPath;
                    //mgaba2:MITS 11704-04/07/2008-if no doc path entered at user level
                    objDocumentManager.DocumentStorageType = (StorageType)this.userLogin.objRiskmasterDatabase.DocPathType;
                }
                else
                {
                    System.Exception exc = new Exception("Document path invalid or not set. Consult your System Administrator. Tip: You can define Document Path in Security Management System.");
                    p_objErrors.Add(exc, "Document path invalid or not set. Consult your System Administrator. Tip: You can define Document Path in Security Management System.", BusinessAdaptorErrorType.Error);
                    return false;
                }



                objXmlDocument = new XmlDocument();

                objRootElement = objXmlDocument.CreateElement("data");
                objXmlElement = objXmlDocument.CreateElement("Document");
                objXmlDocument.AppendChild(objRootElement);
                objRootElement.AppendChild(objXmlElement);

                objXmlNode = objXmlDocument.CreateElement("DocumentId");
                objXmlNode.InnerText = sDocumentId.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("FolderId");
                objXmlNode.InnerText = sFolderId.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("CreateDate");
                objXmlNode.InnerText = dtCreateDate.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Category");
                objXmlNode.InnerText = sCategory.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Name");
                objXmlNode.InnerText = sDocTitle.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Class");
                objXmlNode.InnerText = sClass.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Subject");
                // akaushik5 Added null check for Outlook Integration fix in rmA 14.3 CHP1
                //objXmlNode.InnerText = sSubject.ToString();
                objXmlNode.InnerText = !object.ReferenceEquals(sSubject, null) ? sSubject.ToString() : string.Empty;
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Type");
                objXmlNode.InnerText = sType.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Notes");
                objXmlNode.InnerText = sNotes.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("UserLoginName");
                objXmlNode.InnerText = objDocumentManager.UserLoginName;
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("FileName");
                objXmlNode.InnerText = sFileName.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("FilePath");
                objXmlNode.InnerText = sFilePath.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Keywords");
                objXmlNode.InnerText = sKeywords.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("AttachTable");
                objXmlNode.InnerText = sAttachTable.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("AttachRecordId");
                objXmlNode.InnerText = sAttachRecordId.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                iPSID = p_objDocument.PsId;


                sXmlDocIn = objXmlDocument.OuterXml;

                //BSB 03.24.2006 Add case to handle situation if "FileProcessor" Soap Extension was enabled
                // in web.config and Document was cached to a file on disk during soap message handling.
                /* if (m_bSoapAttachmentCachedToDisk)
                     objDocumentManagementFile = Riskmaster.Common.Utilities.ReadFileMemoryStream(sFile);
                 else
                     objDocumentManagementFile = new MemoryStream(Convert.FromBase64String(sFile));
                 */

                //Aman Mobile Adjuster
                if (!p_objDocument.bMobileAdj && !p_objDocument.bMobilityAdj)
                {
                    if (p_objDocument.FileContents != null)
                        objDocumentManagementFile = new MemoryStream(p_objDocument.FileContents);
                    else
                        objDocumentManagementFile = p_objDocument.fileStream;
                }
                else
                {
                    objDocumentManagementFile = new MemoryStream(p_objDocument.FileContents);
                }
                //Aman Mobile Adjuster

                //Raman Bhatia..06/14/2012
                //Post the document to Acrosoft if Acrosoft Interface is enabled
                objSettings = new SysSettings(m_connectionString, base.ClientId);
                if (objSettings.UseAcrosoftInterface && sAttachTable.ToLower() == "claim")
                {
                    int iClaimID = Conversion.ConvertStrToInteger(sAttachRecordId);
                    Claim objClaim = null;
                    DataModelFactory objDataModelFactory = new DataModelFactory(base.userLogin, base.ClientId);
                    objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                    objClaim.MoveTo(iClaimID);
                    objDataModelFactory.UnInitialize();

                    string sClaimNumber = objClaim.ClaimNumber;
                    string sEventNumber = objClaim.EventNumber;
                    string sAppExcpXml = "";
                    objAcrosoft = new Acrosoft(base.ClientId);//dvatsa-cloud
                    //rsolanki2 :  start updates for MCM mits 19200 
                    Boolean bSuccess = false;
                    string sTempAcrosoftUserId = base.userLogin.LoginName;
                    string sTempAcrosoftPassword = base.userLogin.Password;

                    if (Conversion.CastToType<Boolean>(AcrosoftSection.UseCommonAcrosoftUser, out bSuccess))
                    {
                        if (AcrosoftSection.AcroUserMapSection.UserMap[base.userLogin.LoginName] != null
                            && !string.IsNullOrEmpty(AcrosoftSection.AcroUserMapSection.UserMap[base.userLogin.LoginName].RmxUser))
                        {
                            sTempAcrosoftUserId = AcrosoftSection.AcroUserMapSection.UserMap[base.userLogin.LoginName].AcrosoftUserId;
                            sTempAcrosoftPassword = AcrosoftSection.AcroUserMapSection.UserMap[base.userLogin.LoginName].AcrosoftPassword;
                        }
                        else
                        {
                            sTempAcrosoftUserId = AcrosoftSection.AcrosoftUsername;
                            sTempAcrosoftPassword = AcrosoftSection.AcrosoftUserPassword;
                        }
                    }

                    byte[] byteArray;
                    //Amandeep Mobile Adjuster
                    if (!p_objDocument.bMobileAdj && !p_objDocument.bMobilityAdj)
                    {
                        using (BinaryReader br = new BinaryReader(objDocumentManagementFile))
                        {
                            byteArray = br.ReadBytes(Convert.ToInt32(p_objDocument.FileSize));
                        }
                    }
                    else
                    {
                        byteArray = p_objDocument.FileContents;
                    }
                    //Amandeep Mobile Adjuster
                    //creating folder
                    objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                        AcrosoftSection.AcrosoftAttachmentsTypeKey, sEventNumber, sClaimNumber,
                        AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                    //storing document
                    objAcrosoft.StoreObjectBuffer(sTempAcrosoftUserId, sTempAcrosoftPassword, sFileName, byteArray, sFileName, "", sEventNumber, sClaimNumber, AcrosoftSection.AcrosoftAttachmentsTypeKey,
                        "", "", sTempAcrosoftUserId, out sAppExcpXml);

                    //rsolanki2 :  end updates for MCM mits 19200 

                    ////creating folder
                    //objAcrosoft.CreateAttachmentFolder(base.userLogin.LoginName , base.userLogin.Password , AcrosoftSection.AcrosoftAttachmentsTypeKey , sEventNumber , sClaimNumber , 
                    //    AcrosoftSection.EventFolderFriendlyName , AcrosoftSection.ClaimFolderFriendlyName , out sAppExcpXml); 
                    ////storing document
                    //objAcrosoft.StoreObjectBuffer(base.userLogin.LoginName, base.userLogin.Password, sFilePath, sDocTitle, "" , sEventNumber , sClaimNumber, AcrosoftSection.AcrosoftAttachmentsTypeKey, 
                    //    "", "" , userLogin.LoginName , out sAppExcpXml);
                }
                else
                {

                    objDocumentManager.AddLargeStream(sXmlDocIn, objDocumentManagementFile, iPSID, p_objDocument.FileSize);
                }

                bReturnValue = true;
            }

            catch (InitializationException p_objInitializationException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInitializationException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInvalidValueException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (RMAppException p_objRMAppException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objRMAppException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException,
                    Globalization.GetString("DocumentManagementAdaptor.AddDocument.Error", base.ClientId),
                    BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            finally
            {
                objXmlNode = null;
                objXmlElement = null;
                objRootElement = null;
                //objDocumentManager = null;				
            }

            return bReturnValue;

        }

        /// <summary>
        /// Fetches the LOB and Id of the claim to which Outlook Email is to be attached.
        /// </summary>
        /// <param name="p_objDocument">Document Object</param>
        /// <param name="sAttachRecordId">ClaimId value is assigned to this</param>
        /// <param name="sFolderId">ClaimId value is assigned to this</param>
        public void FetchLOBAndClaimId(ref StreamedDocumentType p_objDocument, ref string sAttachRecordId, ref string sFolderId, ref DocumentManager objDocumentManager)
        {
            try
            {
               
                    int iLOB = 0;
                    int iClaimID = objDocumentManager.FetchLOBAndClaimId(p_objDocument.FolderId, out iLOB);
                    sAttachRecordId = iClaimID.ToString();
                    sFolderId = iClaimID.ToString();
                    switch (iLOB)
                    {
                        case 241: //GC
                            p_objDocument.FormName = "claimgc";
                            break;
                        case 242: //VA
                            p_objDocument.FormName = "claimva";
                            break;
                        case 243: //WC
                            p_objDocument.FormName = "claimwc";
                            break;
                        case 844: //DI
                            p_objDocument.FormName = "claimdi";
                            break;
                        case 845: //PC
                            p_objDocument.FormName = "claimpc";
                            break;
                        default:
                            throw new RMAppException(Globalization.GetString("DocumentManagementAdaptor.LOB.Error",base.ClientId));


                    }
                
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
                
            }
            catch (Exception)
            {
                throw new Exception(Globalization.GetString("DocumentManagementAdaptor.AddDocument.Error",base.ClientId));
               
                            
            }
            return;
        }

        public void FetchLOBAndClaimId(ref StreamDocType p_objDocument, ref string sAttachRecordId, ref string sFolderId, ref DocumentManager objDocumentManager)
        {
            try
            {

                int iLOB = 0;
                int iClaimID = objDocumentManager.FetchLOBAndClaimId(p_objDocument.FolderId, out iLOB);
                sAttachRecordId = iClaimID.ToString();
                sFolderId = iClaimID.ToString();
                switch (iLOB)
                {
                    case 241: //GC
                        p_objDocument.FormName = "claimgc";
                        break;
                    case 242: //VA
                        p_objDocument.FormName = "claimva";
                        break;
                    case 243: //WC
                        p_objDocument.FormName = "claimwc";
                        break;
                    case 844: //DI
                        p_objDocument.FormName = "claimdi";
                        break;
                    case 845: //PC
                        p_objDocument.FormName = "claimpc";
                        break;
                    default:
                        throw new RMAppException(Globalization.GetString("DocumentManagementAdaptor.LOB.Error", base.ClientId));


                }

            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;

            }
            catch (Exception)
            {
                throw new Exception(Globalization.GetString("DocumentManagementAdaptor.AddDocument.Error", base.ClientId));


            }
            return;
        }

        /// <summary>
        /// Fetches the LOB and Id of the claim to which Outlook Email is to be attached.
        /// </summary>
        /// <param name="p_objDocument">Document Object</param>
        /// <param name="sAttachRecordId">ClaimId value is assigned to this</param>
        /// <param name="sFolderId">ClaimId value is assigned to this</param>
        public void FetchLOBAndClaimId(ref DocumentType p_objDocument, ref string sAttachRecordId, ref string sFolderId, ref DocumentManager objDocumentManager)
        {
            try
            {

                int iLOB = 0;
                int iClaimID = objDocumentManager.FetchLOBAndClaimId(p_objDocument.FolderId, out iLOB);
                sAttachRecordId = iClaimID.ToString();
                sFolderId = iClaimID.ToString();
                switch (iLOB)
                {
                    case 241: //GC
                        p_objDocument.FormName = "claimgc";
                        break;
                    case 242: //VA
                        p_objDocument.FormName = "claimva";
                        break;
                    case 243: //WC
                        p_objDocument.FormName = "claimwc";
                        break;
                    case 844: //DI
                        p_objDocument.FormName = "claimdi";
                        break;
                    default:
                        throw new RMAppException(Globalization.GetString("DocumentManagementAdaptor.LOB.Error",base.ClientId));


                }

            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;

            }
            catch (Exception)
            {
                throw new Exception(Globalization.GetString("DocumentManagementAdaptor.AddDocument.Error",base.ClientId));


            }
            return;
        }

        /// <summary>
        /// Fetches the Id of the event to which Outlook Email is to be attached.
        /// </summary>
        /// <param name="p_objDocument">Document Object</param>
        /// <param name="sAttachRecordId">EventId value is assigned to this</param>
        /// <param name="sFolderId">EventId value is assigned to this</param>
        //rsharma220 MITS 35659
        public void FetchEventId(ref StreamedDocumentType p_objDocument, ref string sAttachRecordId, ref string sFolderId, ref DocumentManager objDocumentManager)
        {
            try
            {


                int iEventID = objDocumentManager.FetchEventId(p_objDocument.FolderId);
                sAttachRecordId = Convert.ToString(iEventID);
                sFolderId = Convert.ToString(iEventID);
                p_objDocument.FormName = "event";

            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;

            }
            catch (Exception)
            {
                throw new Exception(Globalization.GetString("DocumentManagementAdaptor.AddDocument.Error", base.ClientId));


            }


        }

        public void FetchEventId(ref StreamDocType p_objDocument, ref string sAttachRecordId, ref string sFolderId, ref DocumentManager objDocumentManager)
        {
            try
            {


                int iEventID = objDocumentManager.FetchEventId(p_objDocument.FolderId);
                sAttachRecordId = Convert.ToString(iEventID);
                sFolderId = Convert.ToString(iEventID);
                p_objDocument.FormName = "event";

            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;

            }
            catch (Exception)
            {
                throw new Exception(Globalization.GetString("DocumentManagementAdaptor.AddDocument.Error",base.ClientId));


            }


        }
        /// <summary>
        /// Sets the storage path and document storage type
        /// </summary>
        /// <param name="objDocumentManager">DocumentManager object</param>
        /// <param name="p_objErrors">BusinessAdaptor errors object</param>
        /// <returns>true-if all operations are successful;false - if document path is invalid or not set</returns>
        public bool SetDocumentStorageTypeAndPath(ref DocumentManager objDocumentManager, ref BusinessAdaptorErrors p_objErrors)
        {
            try
            {
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    // use StoragePath set for the user
                    objDocumentManager.DestinationStoragePath =
                        this.userLogin.DocumentPath;
                    //mgaba2:MITS 11704-04/07/2008-doc path entered at user level has to be file system
                    objDocumentManager.DocumentStorageType = StorageType.FileSystemStorage;
                }
                else if (this.userLogin.objRiskmasterDatabase.GlobalDocPath.Length > 0)
                {
                    // use StoragePath set for the DSN
                    objDocumentManager.DestinationStoragePath =
                        this.userLogin.objRiskmasterDatabase.GlobalDocPath;
                    //mgaba2:MITS 11704-04/07/2008-if no doc path entered at user level
                    objDocumentManager.DocumentStorageType =
                    (StorageType)this.userLogin.objRiskmasterDatabase.DocPathType;
                }
                else
                {
                    System.Exception exc = new Exception("Document path invalid or not set. Consult your System Administrator. Tip: You can define Document Path in Security Management System.");
                    p_objErrors.Add(exc, "Document path invalid or not set. Consult your System Administrator. Tip: You can define Document Path in Security Management System.", BusinessAdaptorErrorType.Error);
                    return false;
                }
                return true;
            }
            catch (Exception)
            {
                throw new Exception(Globalization.GetString("DocumentManagementAdaptor.GetAttachments.Error",base.ClientId));
               
            }
            
        }
        
        /// <summary>
        /// Used to transfer BRS fee schedule files
        /// </summary>
        /// <param name="p_objDocument"></param>
        /// <param name="p_objErrors"></param>
        /// <returns></returns>
        public bool UploadBRSFeeScheuldeFile(StreamedBRSDocumentType p_objDocument, ref BusinessAdaptorErrors p_objErrors)
        {
            bool bReturnValue = false;
            string sFileName = string.Empty;

            try
            {
                sFileName = p_objDocument.FileName;

                string sBRSSourcePath = RMConfigurator.CombineFilePath(RMConfigurator.UserDataPath, "BRSSourceFolder");
                string sBRSSourceFile = sBRSSourcePath + "\\" + sFileName;
                
                BinaryWriter objBinaryWriter = null;
                string tempFilename = string.Empty;
                const int BufferLen = 4096;
                byte[] buffer = new byte[BufferLen];
                int count = 0;

                using (FileStream targetStream = new FileStream(sBRSSourceFile, FileMode.Create))
                {
                    objBinaryWriter = new BinaryWriter(targetStream);
                    Stream sourceStream = new MemoryStream(p_objDocument.FileContents);
                    buffer = new byte[BufferLen];
                    count = 0;
                    while ((count = sourceStream.Read(buffer, 0, BufferLen)) > 0)
                    {
                        objBinaryWriter.Write(buffer, 0, count);
                        objBinaryWriter.Flush();
                    }
                }

                bReturnValue = true;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException,
                    Globalization.GetString("DocumentManagementAdaptor.UploadBRSFeeScheduleFile.Error",base.ClientId),
                    BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            finally
            {				
            }

            return bReturnValue;

        }

        /// <summary>
        /// Used to transfer BRS fee schedule files
        /// </summary>
        /// <param name="p_objDocument"></param>
        /// <param name="p_objErrors"></param>
        /// <returns></returns>
        public bool UploadBRSFeeScheuldeFile(StreamBRSDocType p_objDocument, ref BusinessAdaptorErrors p_objErrors)
        {
            bool bReturnValue = false;
            string sFileName = string.Empty;

            try
            {
                sFileName = p_objDocument.FileName;

                string sBRSSourcePath = RMConfigurator.CombineFilePath(RMConfigurator.UserDataPath, "BRSSourceFolder");
                string sBRSSourceFile = sBRSSourcePath + "\\" + sFileName;

                BinaryWriter objBinaryWriter = null;
                string tempFilename = string.Empty;
                const int BufferLen = 4096;
                byte[] buffer = new byte[BufferLen];
                int count = 0;

                using (FileStream targetStream = new FileStream(sBRSSourceFile, FileMode.Create))
                {
                    objBinaryWriter = new BinaryWriter(targetStream);
                    Stream sourceStream = p_objDocument.fileStream;
                    buffer = new byte[BufferLen];
                    count = 0;
                    while ((count = sourceStream.Read(buffer, 0, BufferLen)) > 0)
                    {
                        objBinaryWriter.Write(buffer, 0, count);
                        objBinaryWriter.Flush();
                    }
                }

                bReturnValue = true;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException,
                    Globalization.GetString("DocumentManagementAdaptor.UploadBRSFeeScheduleFile.Error", base.ClientId),
                    BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            finally
            {
            }

            return bReturnValue;

        }

        //mbahl3 jira RMACLOUD-2383

        // npadhy RMACLOUD-2418 - ISO Compatible with Cloud
        // We can not make ConnString Optional as this function is called across service.
        // And service does not recognise methods with optional paremeter
        // So the Service will call the function with two parameters with Connection string set of main RM DB
        // From the DataIntegratorAdapter will call the same function but change the Connection String of Staging DB

        // The structure of Permanent Table in DA is different. We store the OptionSet id, Module Name and Document Type in DB as well
        // Made code changes to incorporate the change in DB Schema. The same code can be used if in future we decide to add same columns in 
        // PMT_DOCUMENT_STORAGE in Main RM DB
        public bool UploadFile(StreamedUploadDocumentType p_objDocument, ref BusinessAdaptorErrors p_objErrors, string p_sConnString)
        {
            bool bReturnValue = false;
            string sFileName = string.Empty;
           string sSourcePath = string.Empty;
            string sSourceFile = string.Empty;
            BinaryWriter objBinaryWriter = null;
            string tempFilename = string.Empty;
            const int BufferLen = 4096;
            byte[] buffer = null;
            int count = 0;
            DbReader objReader = null;
            string sModuleName = string.Empty;
            CommonFunctions.FileType currentFileType = CommonFunctions.FileType.None;
            try
            {
                sFileName = p_objDocument.FileName;

                try
                {
                    currentFileType = (CommonFunctions.FileType)Enum.Parse(typeof(CommonFunctions.FileType), p_objDocument.DocumentType);
                }
                catch (Exception)
                {

                }

                if (!string.IsNullOrEmpty(p_objDocument.ModuleName))
                    sModuleName = p_objDocument.ModuleName;

                // Changed the Location from Reserve Worksheet folder to TempFiles4Cloud
                sSourcePath = RMConfigurator.CombineFilePath(RMConfigurator.TempPath, "TempFiles4Cloud");
                sSourceFile = sSourcePath + "\\" + sFileName;

                buffer = new byte[BufferLen];

                using (FileStream targetStream = new FileStream(sSourceFile, FileMode.Create))
                {
                    objBinaryWriter = new BinaryWriter(targetStream);
                    Stream sourceStream = new MemoryStream(p_objDocument.FileContents);
                    buffer = new byte[BufferLen];
                    count = 0;
                    while ((count = sourceStream.Read(buffer, 0, BufferLen)) > 0)
                    {
                        objBinaryWriter.Write(buffer, 0, count);
                        objBinaryWriter.Flush();
                    }
                }

                string PmtSQL = "SELECT * FROM PMT_DOCUMENT_STORAGE WHERE FILENAME=" + "'" + sFileName + "'";
                objReader = DbFactory.GetDbReader(p_sConnString, PmtSQL);

                if (objReader.Read())
                {

                    string DelSQL = "DELETE FROM PMT_DOCUMENT_STORAGE WHERE FILENAME =" + "'" + sFileName + "'";
                    objReader = DbFactory.GetDbReader(p_sConnString, DelSQL);
                    //CommonFunctions.InsertTempFileIntoDB(p_objDocument.FileName, sRswSourceFile, m_connectionString, p_objDocument.ClientId, true);
                }

                CommonFunctions.InsertTempFileIntoDB(sFileName, sSourceFile, p_sConnString, p_objDocument.ClientId, CommonFunctions.StorageType.Permanent, sModuleName, currentFileType, p_objDocument.RelatedId);

                bReturnValue = true;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException,
                    Globalization.GetString("DocumentManagementAdaptor.UploadFiles.Error", base.ClientId),
                    BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            finally
            {
                objBinaryWriter = null;
                buffer = null;

                // npadhy RMACLOUD-2418 - ISO Compatible with Cloud
                // Delete the Temporary files which we created while saving the files to DB

                try
                {
                    File.Delete(sSourceFile);
                }
                catch (Exception e)
                {

                }
                finally
                {
                }
            }

            return bReturnValue;

        }

        // npadhy RMACLOUD-2418 - ISO Compatible with Cloud
        // The service will call this function with Connection string of Main RM DB
        public bool UploadFile(StreamedUploadDocumentType p_objDocument, ref BusinessAdaptorErrors p_objErrors)
        {
            return UploadFile(p_objDocument, ref p_objErrors, m_connectionString);
        }

        public bool UploadFile(StreamUploadDocType p_objDocument, ref BusinessAdaptorErrors p_objErrors)
        {
            return UploadFile(p_objDocument, ref p_objErrors, m_connectionString);
        }
        public bool UploadFile(StreamUploadDocType p_objDocument, ref BusinessAdaptorErrors p_objErrors, string p_sConnString)
        {
            bool bReturnValue = false;
            string sFileName = string.Empty;
            string sSourcePath = string.Empty;
            string sSourceFile = string.Empty;
            BinaryWriter objBinaryWriter = null;
            string tempFilename = string.Empty;
            const int BufferLen = 4096;
            byte[] buffer = null;
            int count = 0;
            DbReader objReader = null;
            string sModuleName = string.Empty;
            CommonFunctions.FileType currentFileType = CommonFunctions.FileType.None;
            try
            {
                sFileName = p_objDocument.FileName;

                try
                {
                    currentFileType = (CommonFunctions.FileType)Enum.Parse(typeof(CommonFunctions.FileType), p_objDocument.DocumentType);
                }
                catch (Exception)
                {

                }

                if (!string.IsNullOrEmpty(p_objDocument.ModuleName))
                    sModuleName = p_objDocument.ModuleName;

                // Changed the Location from Reserve Worksheet folder to TempFiles4Cloud
                sSourcePath = RMConfigurator.CombineFilePath(RMConfigurator.TempPath, "TempFiles4Cloud");
                sSourceFile = sSourcePath + "\\" + sFileName;

                buffer = new byte[BufferLen];

                using (FileStream targetStream = new FileStream(sSourceFile, FileMode.Create))
                {
                    objBinaryWriter = new BinaryWriter(targetStream);
                    Stream sourceStream = p_objDocument.fileStream;
                    buffer = new byte[BufferLen];
                    count = 0;
                    while ((count = sourceStream.Read(buffer, 0, BufferLen)) > 0)
                    {
                        objBinaryWriter.Write(buffer, 0, count);
                        objBinaryWriter.Flush();
                    }
                }

                string PmtSQL = "SELECT ROW_ID FROM PMT_DOCUMENT_STORAGE WHERE FILENAME=" + "'" + sFileName + "'";
                objReader = DbFactory.GetDbReader(p_sConnString, PmtSQL);

                if (objReader.Read())
                {

                    string DelSQL = "DELETE FROM PMT_DOCUMENT_STORAGE WHERE FILENAME =" + "'" + sFileName + "'";
                    objReader = DbFactory.GetDbReader(p_sConnString, DelSQL);
                    //CommonFunctions.InsertTempFileIntoDB(p_objDocument.FileName, sRswSourceFile, m_connectionString, p_objDocument.ClientId, true);
                }

                CommonFunctions.InsertTempFileIntoDB(sFileName, sSourceFile, p_sConnString, p_objDocument.ClientId, CommonFunctions.StorageType.Permanent, sModuleName, currentFileType, p_objDocument.RelatedId);

                bReturnValue = true;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException,
                    Globalization.GetString("DocumentManagementAdaptor.UploadFiles.Error", base.ClientId),
                    BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            finally
            {
                objBinaryWriter = null;
                buffer = null;

                // npadhy RMACLOUD-2418 - ISO Compatible with Cloud
                // Delete the Temporary files which we created while saving the files to DB

                try
                {
                    File.Delete(sSourceFile);
                }
                catch (Exception e)
                {

                }
                finally
                {
                }
            }

            return bReturnValue;

        }
        //mbahl3 jira RMACLOUD-2383
        /// <summary>
        /// Added by Nitin, to create New Document Entry,in case of AsyncStoring of large documents
        /// </summary>
        /// <param name="p_objDocument"></param>
        /// <param name="p_objErrors"></param>
        /// <returns></returns>
        public void CreateNewDocument(DocumentType p_objDocument,out DocumentType outDocument,ref BusinessAdaptorErrors p_objErrors)
        {
            XmlElement objXmlElement = null;
            XmlElement objRootElement = null;
            XmlNode objXmlNode = null;
            XmlDocument objXmlDocument = null;

            string sXmlDocIn = string.Empty;
            DateTime dtCreateDate = System.DateTime.Now;

            string sDocumentId = string.Empty;
            string sCategory = string.Empty;
            string sClass = string.Empty;
            string sSubject = string.Empty;
            string sType = string.Empty;
            string sFilePath = string.Empty;
            string sAttachTable = string.Empty;
            string sAttachRecordId = string.Empty;

            string sFolderId = string.Empty;
            string sDocTitle = string.Empty;
            string sNotes = string.Empty;
            string sFileName = string.Empty;
            string sKeywords = string.Empty;
            string sFile = string.Empty;
            string sNewFileName = string.Empty;
            long lNewDocumentId = 0;
            string[] arrsFileNameParts = null;
            int iPSID = 0;

            DocumentManager objDocumentManager = null;
            outDocument = null;

            try
            {
                sFolderId = p_objDocument.FolderId;
                sDocTitle = p_objDocument.Title;
                sSubject = p_objDocument.Subject;
                sType = p_objDocument.Type.Id.ToString();
                sClass = p_objDocument.Class.Id.ToString();
                sCategory = p_objDocument.Category.Id.ToString();
                sNotes = p_objDocument.Notes;
                sFileName = p_objDocument.FileName;
                //Shruti, in case of oracle, RMWorld cud not pick documents attached from RMX
                sAttachTable = p_objDocument.AttachTable.ToUpper();
                sAttachRecordId = p_objDocument.AttachRecordId.ToString();

                //string sDelimiter = "\";
                if (sFileName != "")
                {
                    arrsFileNameParts = sFileName.Split("\\".ToCharArray());
                    foreach (string sFName in arrsFileNameParts)
                    {
                        if (sFName.IndexOf('.') > 0)
                        {
                            sFileName = sFName;
                        }
                    }
                }

                sFilePath = p_objDocument.FilePath;
                sKeywords = p_objDocument.Keywords;
                objXmlNode = null;

                dtCreateDate = System.DateTime.Now;

                objDocumentManager = new DocumentManager(m_userLogin,base.ClientId);
                objDocumentManager.ConnectionString = this.connectionString;
                objDocumentManager.SecurityConnectionString = this.securityConnectionString; //MITS 27764 hlv 5/23/12
                objDocumentManager.UserLoginName = this.loginName;

                // WARNING - Integer value returned from RiskmasterDatabase object
                // is being casted to StorageType enum.
                //mgaba2:MITS 11704-04/07/2008-inserted the following code in else part
                /* objDocumentManager.DocumentStorageType = 
                    (StorageType) this.userLogin.objRiskmasterDatabase.DocPathType; */

                //mgaba2:MITS 11704-04/07/2008-Changed the condition
                //if( objDocumentManager.DocumentStorageType==StorageType.FileSystemStorage && this.userLogin.DocumentPath.Length>0)
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    // use StoragePath set for the user
                    objDocumentManager.DestinationStoragePath =
                        this.userLogin.DocumentPath;
                    //mgaba2:MITS 11704-04/07/2008-doc path entered at user level has to be file system
                    objDocumentManager.DocumentStorageType = StorageType.FileSystemStorage;
                }//Changed by bsharma33 MITS 26253 : previously condition was this.userLogin.objRiskmasterDatabase.GlobalDocPath.Length > 0
                else if (((StorageType)this.userLogin.objRiskmasterDatabase.DocPathType == StorageType.FileSystemStorage) && (Directory.Exists(this.userLogin.objRiskmasterDatabase.GlobalDocPath) && HasWriteAccessToFolder(this.userLogin.objRiskmasterDatabase.GlobalDocPath)))
                {
                    // use StoragePath set for the DSN
                    objDocumentManager.DestinationStoragePath =
                        this.userLogin.objRiskmasterDatabase.GlobalDocPath;
                    //mgaba2:MITS 11704-04/07/2008-if no doc path entered at user level
                    objDocumentManager.DocumentStorageType =
                    (StorageType)this.userLogin.objRiskmasterDatabase.DocPathType;
                }
                else if (((StorageType)this.userLogin.objRiskmasterDatabase.DocPathType == StorageType.DatabaseStorage) && this.userLogin.objRiskmasterDatabase.GlobalDocPath.Length > 0)
                {
                    // use StoragePath set for the DSN
                    objDocumentManager.DestinationStoragePath =
                        this.userLogin.objRiskmasterDatabase.GlobalDocPath;
                    //mgaba2:MITS 11704-04/07/2008-if no doc path entered at user level
                    objDocumentManager.DocumentStorageType =
                    (StorageType)this.userLogin.objRiskmasterDatabase.DocPathType;
                }
                else if (((StorageType)this.userLogin.objRiskmasterDatabase.DocPathType == StorageType.S3BucketStorage) && this.userLogin.objRiskmasterDatabase.GlobalDocPath.Length > 0)
                {
                    // use StoragePath set for the DSN
                    objDocumentManager.DestinationStoragePath =
                        this.userLogin.objRiskmasterDatabase.GlobalDocPath;
                    objDocumentManager.DocumentStorageType =
                    (StorageType)this.userLogin.objRiskmasterDatabase.DocPathType;
                }
                else
                {
                    //Changed by bsharma33 MITS 26253
                    System.Exception exc = new Exception("Document path invalid or not set. Consult your System Administrator. Tip: You can define Document Path in Security Management System.");
                   // p_objErrors.Add(exc, "Document path invalid or not set. Consult your System Administrator. Tip: You can define Document Path in Security Management System.", BusinessAdaptorErrorType.Error);
                    throw exc;
                    //bsharma33 MITS 26253 ENDS
                }

                objXmlDocument = new XmlDocument();

                objRootElement = objXmlDocument.CreateElement("data");
                objXmlElement = objXmlDocument.CreateElement("Document");
                objXmlDocument.AppendChild(objRootElement);
                objRootElement.AppendChild(objXmlElement);

                objXmlNode = objXmlDocument.CreateElement("DocumentId");
                objXmlNode.InnerText = sDocumentId.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("FolderId");
                objXmlNode.InnerText = sFolderId.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("CreateDate");
                objXmlNode.InnerText = dtCreateDate.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Category");
                objXmlNode.InnerText = sCategory.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Name");
                objXmlNode.InnerText = sDocTitle.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Class");
                objXmlNode.InnerText = sClass.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Subject");
                objXmlNode.InnerText = sSubject.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Type");
                objXmlNode.InnerText = sType.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Notes");
                objXmlNode.InnerText = sNotes.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("UserLoginName");
                objXmlNode.InnerText = objDocumentManager.UserLoginName;
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("FileName");
                objXmlNode.InnerText = sFileName.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("FilePath");
                objXmlNode.InnerText = sFilePath.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Keywords");
                objXmlNode.InnerText = sKeywords.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("AttachTable");
                objXmlNode.InnerText = sAttachTable.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("AttachRecordId");
                objXmlNode.InnerText = sAttachRecordId.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                iPSID = p_objDocument.PsId;
                
                sXmlDocIn = objXmlDocument.OuterXml;

                //BSB 03.24.2006 Add case to handle situation if "FileProcessor" Soap Extension was enabled
                // in web.config and Document was cached to a file on disk during soap message handling.
                /* if (m_bSoapAttachmentCachedToDisk)
                     objDocumentManagementFile = Riskmaster.Common.Utilities.ReadFileMemoryStream(sFile);
                 else
                     objDocumentManagementFile = new MemoryStream(Convert.FromBase64String(sFile));
                 */
               objDocumentManager.CreateNewDocument(sXmlDocIn,iPSID,ref sNewFileName,ref lNewDocumentId);

               outDocument = new DocumentType();
               outDocument.DocumentId = (int)lNewDocumentId;
               outDocument.FileName = sNewFileName;
            }
            catch (InitializationException p_objInitializationException)
            {
                p_objErrors.Add(p_objInitializationException, BusinessAdaptorErrorType.Error);
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                p_objErrors.Add(p_objInvalidValueException, BusinessAdaptorErrorType.Error);
            }
            catch (RMAppException p_objRMAppException)
            {
                p_objErrors.Add(p_objRMAppException, BusinessAdaptorErrorType.Error);
            }
            catch (Exception p_objException)
            {
                p_objErrors.Add(p_objException,
                    Globalization.GetString("DocumentManagementAdaptor.AddDocument.Error",base.ClientId),
                    BusinessAdaptorErrorType.Error);
                throw p_objException;
            }
            finally
            {
                objXmlNode = null;
                objXmlElement = null;
                objRootElement = null;
                //objDocumentManager = null;				
            }
        }

        #endregion

        //Function added by bsharma33 MITS 26253
        /// <summary>
        /// The following function will create and write to a file in the location passed as argument.
        /// The function will return true if write is successful and will return false if the write failed for any reason.
        /// </summary>
        /// <param name="folderPath">path where the access has to be checked.</param>
        /// <returns>return true if folder exists and required write permissions are there.</returns>
        private bool HasWriteAccessToFolder(string folderPath)
        {

            try
            {

                Random random = new Random(DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Second + DateTime.Now.Millisecond);
                string fileName = folderPath + "\\" + random.Next() + ".txt";
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(fileName))
                {
                    file.Close();
                    System.IO.File.Delete(fileName);//delete the file if no exception during write.
                    return true;
                }

            }
            catch (Exception ex)
            {

                return false;
            }
            finally
            {


            }
        }
        //Function Ends : added by bsharma33 MITS 26253
        /// Name			: Delete
        /// Author			: Aditya Babbar
        /// Date Created	: 15-Mar-2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Wrapper to DocumentManager.Delete of application layer that
        /// deletes the specified documents, folders and all the child folders
        /// along with the documents inside recursively. 
        /// </summary>
        /// <param name="p_objXmlDocIn"> 
        ///	Input paramters in xml format.
        ///	Structure:
        ///		<Delete>
        ///			<DocumentIds>
        ///				Comma separated list of DocumentIds.
        ///			</DocumentIds>
        ///			<FolderIds>
        ///				Comma separated list of FolderIds.
        ///			</FolderIds>
        ///		</Delete>
        /// </param>
        /// <param name="p_objXmlDocOut"> 
        ///		Empty		
        ///	</param>
        /// <param name="p_objErrors">
        ///	Collection of errors/messages.	
        ///	</param>
        /// <returns>
        ///		true - success
        ///		false - failure
        ///	</returns>
        public bool DeleteNew(DocumentsDeleteRequest p_objRequest,
                   ref BusinessAdaptorErrors p_objErrors)
        {
            bool bReturnValue = false;
            DocumentManager objDocumentManager = null;

            XmlNode objXmlNode = null;

            string sDocumentIds = string.Empty;
            string sFolderIds = string.Empty;
            int iPSID = 0;

            //Mohit Yadav for Security Permission implementation
            string sScreenFlag = string.Empty;
            string sTableName = string.Empty;
            //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
            PVWrapper objPaperVision = null;
            PaperVisionDocumentManager objPVDocManager = null;
            SysSettings objSettings = null;
            RMConfigurator objConfig = null; 
            //Animesh Insertion Ends
            try
            {
                //Mona:PaperVisionMerge : Animesh Inserted Mits 16697
                objSettings = new SysSettings(this.connectionString, base.ClientId);   
                //Animesh Insertion ends

                if (p_objRequest.DocumentIds == null)
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.Delete.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.Delete.MissingDocumentIdsNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                sDocumentIds = p_objRequest.DocumentIds;



                if (p_objRequest.FolderIds == null)
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.Delete.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.Delete.MissingFolderIdsNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                sFolderIds = p_objRequest.FolderIds;

                //Mona:PaperVisionMerge : Animesh Inserted Mits 16697
                if (objSettings.UsePaperVisionInterface == true)
                {
                    objPVDocManager = new PaperVisionDocumentManager(m_userLogin, base.ClientId);
                    objPVDocManager.ConnectionString = this.connectionString;
                    objPVDocManager.UserLoginName = this.loginName;
                    //Get psid value
                    if (p_objRequest.Psid != null && p_objRequest.Psid != "")
                    {
                        if (p_objRequest.Psid != "")
                        {
                            iPSID = int.Parse(p_objRequest.Psid);
                        }
                    }
                    objXmlNode = null;
                    if (p_objRequest.TableName != null && p_objRequest.TableName != "")
                    {
                        sTableName = p_objRequest.TableName;
                    }
                    objXmlNode = null;
                    if (p_objRequest.ScreenFlag != null)
                    {
                        sScreenFlag = p_objRequest.ScreenFlag;
                    }
                    int iBaseSecurityId = 0;
                    iBaseSecurityId = iPSID;
                    iBaseSecurityId = objPVDocManager.GetBaseSecurityIDForTable(iBaseSecurityId, sTableName);
                    if (sScreenFlag == "Files")
                    {
                        if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_UD_DELETE))
                            throw new PermissionViolationException(RMPermissions.RMO_UD_DELETE, iBaseSecurityId);
                    }
                    else
                    {
                        if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_ATT_DETACHFROM))
                            throw new PermissionViolationException(RMPermissions.RMO_ATT_DETACHFROM, iBaseSecurityId);
                    }
                    objPaperVision = new PVWrapper();
                    objPaperVision.PVEntityID = PaperVisionSection.EntityID;
                    objPaperVision.PVLoginUrl = PaperVisionSection.LoginUrl;
                    objPaperVision.PVProjID = PaperVisionSection.ProjectID;
                    objPaperVision.PVServerAddress = PaperVisionSection.PVServerAddress;
                    objPaperVision.PVShowDocLinkUrl = PaperVisionSection.DocumentLink;
                    //Animesh inserted MITS 18345
                    objPaperVision.SharedNetworkLocationPath = PaperVisionSection.SharedNetworkLocationPath;
                    //objPaperVision.MappedDriveName = PaperVisionSection.MappedDriveName;    
                    objPaperVision.DomainName = PaperVisionSection.DomainName;
                    objPaperVision.UserID = PaperVisionSection.UserID;
                    objPaperVision.Password = PaperVisionSection.Password;   
                    //Animesh insertion ends
                    objPVDocManager.Delete(sDocumentIds,iPSID,objPaperVision);
                    bReturnValue = true;
                }
                else
                {
                    //**********************************************************
                    objDocumentManager = new DocumentManager(m_userLogin, base.ClientId);
                objDocumentManager.ConnectionString = this.connectionString;
                objDocumentManager.SecurityConnectionString = this.securityConnectionString; //MITS 27764 hlv 5/23/12
                objDocumentManager.UserLoginName = this.loginName;

                // WARNING - Integer value returned from RiskmasterDatabase object
                // is being casted to StorageType enum.
                objDocumentManager.DocumentStorageType =
                    (StorageType)this.userLogin.objRiskmasterDatabase.DocPathType;

                if (this.userLogin.DocumentPath.Length > 0)
                {
                    // use StoragePath set for the user
                    objDocumentManager.DestinationStoragePath =
                        this.userLogin.DocumentPath;
                }
                else
                {
                    // use StoragePath set for the DSN
                    objDocumentManager.DestinationStoragePath =
                        this.userLogin.objRiskmasterDatabase.GlobalDocPath;
                }

                //Get psid value

                if (p_objRequest.Psid != null && p_objRequest.Psid != "")
                {
                    if (p_objRequest.Psid != "")
                    {
                        iPSID = int.Parse(p_objRequest.Psid);
                    }
                }
                objXmlNode = null;
                //Nitesh TR 2527 Get TableName value

                if (p_objRequest.TableName != null && p_objRequest.TableName != "")
                {
                    sTableName = p_objRequest.TableName;
                }
                objXmlNode = null;

                //Mohit Yadav for Security Permission implementation

                if (p_objRequest.ScreenFlag != null)
                {
                    sScreenFlag = p_objRequest.ScreenFlag;
                }
                int iBaseSecurityId = 0;
                iBaseSecurityId = iPSID;
                //TR : 2527 Nitesh 25May2006
                iBaseSecurityId = objDocumentManager.GetBaseSecurityIDForTable(iBaseSecurityId, sTableName);
                if (sScreenFlag == "Files")
                {
                    if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_UD_DELETE))
                        throw new PermissionViolationException(RMPermissions.RMO_UD_DELETE, iBaseSecurityId);
                }
                else
                {
                    if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_ATT_DETACHFROM))
                        throw new PermissionViolationException(RMPermissions.RMO_ATT_DETACHFROM, iBaseSecurityId);
                }

                objDocumentManager.Delete(sDocumentIds, sFolderIds, iPSID);

                bReturnValue = true;
                }
            }
            catch (InitializationException p_objInitializationException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInitializationException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInvalidValueException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (RMAppException p_objRMAppException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objRMAppException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException, Globalization.GetString("DocumentManagementAdaptor.Delete.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            finally
            {
                objXmlNode = null;
                objDocumentManager = null;
            }

            return bReturnValue;
        }

        /// Name			: Delete
        /// Author			: Aditya Babbar
        /// Date Created	: 15-Mar-2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Wrapper to DocumentManager.Delete of application layer that
        /// deletes the specified documents, folders and all the child folders
        /// along with the documents inside recursively. 
        /// </summary>
        /// <param name="p_objXmlDocIn"> 
        ///	Input paramters in xml format.
        ///	Structure:
        ///		<Delete>
        ///			<DocumentIds>
        ///				Comma separated list of DocumentIds.
        ///			</DocumentIds>
        ///			<FolderIds>
        ///				Comma separated list of FolderIds.
        ///			</FolderIds>
        ///		</Delete>
        /// </param>
        /// <param name="p_objXmlDocOut"> 
        ///		Empty		
        ///	</param>
        /// <param name="p_objErrors">
        ///	Collection of errors/messages.	
        ///	</param>
        /// <returns>
        ///		true - success
        ///		false - failure
        ///	</returns>
        public bool Delete(XmlDocument p_objXmlDocIn,
            ref XmlDocument p_objXmlDocOut,
            ref BusinessAdaptorErrors p_objErrors)
        {
            bool bReturnValue = false;
            DocumentManager objDocumentManager = null;

            XmlNode objXmlNode = null;

            string sDocumentIds = string.Empty;
            string sFolderIds = string.Empty;
            int iPSID = 0;

            //Mohit Yadav for Security Permission implementation
            string sScreenFlag = string.Empty;
            string sTableName = string.Empty;
            try
            {
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//Delete/DocumentIds");

                if (objXmlNode == null)
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.Delete.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.Delete.MissingDocumentIdsNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                sDocumentIds = objXmlNode.InnerText;

                objXmlNode = null;
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//Delete/FolderIds");

                if (objXmlNode == null)
                {
                    bReturnValue = false;
                    p_objErrors.Add("DocumentManagementAdaptor.Delete.MissingInputParameter",
                        Globalization.GetString("DocumentManagementAdaptor.Delete.MissingFolderIdsNode",base.ClientId),
                        BusinessAdaptorErrorType.Error);
                    return bReturnValue;
                }

                sFolderIds = objXmlNode.InnerText;

                objDocumentManager = new DocumentManager(m_userLogin, base.ClientId);
                objDocumentManager.ConnectionString = this.connectionString;
                objDocumentManager.SecurityConnectionString = this.securityConnectionString; //MITS 27764 hlv 5/23/12
                objDocumentManager.UserLoginName = this.loginName;

                // WARNING - Integer value returned from RiskmasterDatabase object
                // is being casted to StorageType enum.
                objDocumentManager.DocumentStorageType =
                    (StorageType)this.userLogin.objRiskmasterDatabase.DocPathType;

                if (this.userLogin.DocumentPath.Length > 0)
                {
                    // use StoragePath set for the user
                    objDocumentManager.DestinationStoragePath =
                        this.userLogin.DocumentPath;
                }
                else
                {
                    // use StoragePath set for the DSN
                    objDocumentManager.DestinationStoragePath =
                        this.userLogin.objRiskmasterDatabase.GlobalDocPath;
                }

                //Get psid value
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//psid");
                if (objXmlNode != null)
                {
                    if (objXmlNode.InnerText.Trim() != "")
                    {
                        iPSID = int.Parse(objXmlNode.InnerText);
                    }
                }
                objXmlNode = null;
                //Nitesh TR 2527 Get TableName value
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//TableName");
                if (objXmlNode != null)
                {
                    sTableName = objXmlNode.InnerText;
                }
                objXmlNode = null;

                //Mohit Yadav for Security Permission implementation
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//Delete/ScreenFlag");
                sScreenFlag = objXmlNode.InnerText.ToString();

                int iBaseSecurityId = 0;
                iBaseSecurityId = iPSID;
                //TR : 2527 Nitesh 25May2006
                iBaseSecurityId = objDocumentManager.GetBaseSecurityIDForTable(iBaseSecurityId, sTableName);
                if (sScreenFlag == "Files")
                {
                    if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_UD_DELETE))
                        throw new PermissionViolationException(RMPermissions.RMO_UD_DELETE, iBaseSecurityId);
                }
                else
                {
                    if (!m_userLogin.IsAllowedEx(iBaseSecurityId, RMO_ATT_DETACHFROM))
                        throw new PermissionViolationException(RMPermissions.RMO_ATT_DETACHFROM, iBaseSecurityId);
                }

                objDocumentManager.Delete(sDocumentIds, sFolderIds, iPSID);

                bReturnValue = true;
            }
            catch (InitializationException p_objInitializationException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInitializationException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInvalidValueException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (RMAppException p_objRMAppException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objRMAppException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException, Globalization.GetString("DocumentManagementAdaptor.Delete.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            finally
            {
                objXmlNode = null;
                objDocumentManager = null;
            }

            return bReturnValue;
        }


        #region public bool GetUsers(XmlDocument p_objXmlDocIn, ref XmlDocument p_objXmlDocOut, ref BusinessAdaptorErrors p_objErrors)
        /// Name			: GetUsers
        /// Author			: Mohit Yadav
        /// Date Created	: 09-May-2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Wrapper to DocumentManager.GetUsers of Application
        /// layer that returns users in form of XML
        /// </summary>
        /// <param name="p_objXmlDocIn"> 
        ///	Input paramters in xml format.
        ///	Structure:
        ///		<GetUsers/>
        /// </param>
        /// <param name="p_objXmlDocOut"> 
        ///	Method output in xml format.
        ///	Structure:
        ///		<GetUsersResult>
        ///			<data>
        ///				<user firstname="" lastname="" userid="">
        ///			</data>
        ///		</GetUsersResult>
        ///	</param>
        /// <param name="p_objErrors">
        ///	Collection of errors/messages.	
        ///	</param>
        /// <returns>
        ///		true - success
        ///		false - failure
        ///	</returns>

        public bool GetUsers(XmlDocument p_objXmlDocIn, ref XmlDocument p_objXmlDocOut,
            ref BusinessAdaptorErrors p_objErrors)
        {
            bool bReturnValue = false;
            string sCurrentUserName = "";
            int iPSID = 0;
            XmlNode objXmlNode = null;

            XmlElement objRootElement = null;

            DocumentManager objDocumentManager = null;

            String objXmlDocOut;

            try
            {
                string sCurrentDataSourceName = this.userLogin.objRiskmasterDatabase.DataSourceName;
                sCurrentUserName = this.userLogin.LoginName;

                //get psid value
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//psid");
                if (objXmlNode != null)
                {
                    if (objXmlNode.InnerText.Trim() != "")
                    {
                        iPSID = int.Parse(objXmlNode.InnerText);
                    }
                }
                objXmlNode = null;

                objDocumentManager = new DocumentManager(m_userLogin, base.ClientId);
                objDocumentManager.GetUsers(sCurrentDataSourceName, iPSID, out objXmlDocOut);

                objRootElement = p_objXmlDocOut.CreateElement("GetUsersResult");
                objRootElement.InnerXml = objXmlDocOut;

                //adding attribute LoginUser to retrieve the current user..raman bhatia
                objRootElement.SetAttribute("LoginUser", sCurrentUserName);

                //adding attribute to retrieve the current date and cuurent time..raman bhatia
                objRootElement.SetAttribute("CurrentDate", String.Format("{0:MM/dd/yyyy}", DateTime.Now));
                objRootElement.SetAttribute("CurrentTime", String.Format("{0:hh:mm tt}", DateTime.Now));


                p_objXmlDocOut.AppendChild(objRootElement);

                bReturnValue = true;

            }
            catch (InitializationException p_objInitializationException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInitializationException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (RMAppException p_objRMAppException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objRMAppException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException, Globalization.GetString("DocumentManagementAdaptor.GetUsers.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            finally
            {
                objRootElement = null;
            }

            return bReturnValue;
        }
        public bool GetUsersNew(DocumentUserRequest p_objInfo, ref UsersList objList,
            ref BusinessAdaptorErrors p_objErrors)
        {
            bool bReturnValue = false;
            string sCurrentUserName = "";
            int iPSID = 0;
            XmlNode objXmlNode = null;

            XmlElement objRootElement = null;

            DocumentManager objDocumentManager = null;

            String objXmlDocOut;

            try
            {
                string sCurrentDataSourceName = this.userLogin.objRiskmasterDatabase.DataSourceName;
                sCurrentUserName = this.userLogin.LoginName;

                //get psid value
                //objXmlNode = p_objXmlDocIn.SelectSingleNode("//psid");
                if (p_objInfo.Psid != null && p_objInfo.Psid != "")
                {
                    // if (objXmlNode.InnerText.Trim() != "")
                    //{
                    iPSID = int.Parse(p_objInfo.Psid);
                    //}
                }
                objXmlNode = null;

                objDocumentManager = new DocumentManager(m_userLogin, base.ClientId);
                objDocumentManager.GetUsersNew(sCurrentDataSourceName, iPSID, out objList);

                /*objRootElement = p_objXmlDocOut.CreateElement("GetUsersResult");
                objRootElement.InnerXml = objXmlDocOut;

                //adding attribute LoginUser to retrieve the current user..raman bhatia
                objRootElement.SetAttribute("LoginUser", sCurrentUserName);

                //adding attribute to retrieve the current date and cuurent time..raman bhatia
                objRootElement.SetAttribute("CurrentDate", String.Format("{0:MM/dd/yyyy}", DateTime.Now));
                objRootElement.SetAttribute("CurrentTime", String.Format("{0:hh:mm tt}", DateTime.Now));*/


                //p_objXmlDocOut.AppendChild(objRootElement);
                objList.LoginUser = sCurrentUserName;
                objList.CurrentDate = String.Format("{0:MM/dd/yyyy}", DateTime.Now);
                objList.CurrentTime = String.Format("{0:hh:mm tt}", DateTime.Now);

                bReturnValue = true;

            }
            catch (InitializationException p_objInitializationException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objInitializationException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (RMAppException p_objRMAppException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objRMAppException, BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            catch (Exception p_objException)
            {
                bReturnValue = false;
                p_objErrors.Add(p_objException, Globalization.GetString("DocumentManagementAdaptor.GetUsers.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return bReturnValue;
            }
            finally
            {
                objRootElement = null;
            }

            return bReturnValue;
        }
        #endregion

        #endregion

    }
}

