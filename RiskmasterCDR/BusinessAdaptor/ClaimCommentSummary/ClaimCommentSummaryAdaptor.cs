using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;
using Riskmaster.Application.ClaimCommentSummary;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: ClaimCommentSummaryAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 16-Aug-2005
	///* $Author	: Mohit Yadav
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for Claim Comment Summary
	///	which implements the functionality of Claim Comment Summary. 
	/// </summary>
	public class ClaimCommentSummaryAdaptor : BusinessAdaptorBase
	{
		
		#region Constructor
		/// <summary>
		/// Constructor for the class.
		/// </summary>
		public ClaimCommentSummaryAdaptor()
		{
		}
		#endregion

		#region Public Methods

		#region "GetXmlInfo(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)"
		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.ClaimCommentSummary.ClaimCommentSummary.GetXmlInfo() method.
		///		Gets the Comment Summary in XML format
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of XML document would be:
		///		<Document>
		///			<ClaimCommentSummary>
		///				<ScreenFlag>Name of the screen</ScreenFlag>
		///				<Id>Claim/Event Id</Id>
		///			</ClaimCommentSummary>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be
		/// </param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		
		public bool GetXmlInfo(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.ClaimCommentSummary.ClaimCommentSummary  objClaimCommentSummary=null; //Application layer component

			string sScreenName = string.Empty;
			int iClaimEventId = 0;
			XmlElement objElement=null;					//used for parsing the input xml
            string sLangCode = string.Empty;
			try
			{
				//check existence of Screen Flag which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ScreenFlag");
				if (objElement==null)
				{
                    p_objErrOut.Add("ClaimCommentSummaryAdaptor.GetXmlInfo.Error", Globalization.GetString("ClaimCommentSummaryAdaptor.GetXmlInfo.ScreenMissing", base.ClientId), BusinessAdaptorErrorType.PopupMessage);//sharishkumar Jira 818
					return false;
				}
				sScreenName=objElement.InnerText.ToString();

				//check existence of Claim / Event Id which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//Id");
				if (objElement==null)
				{
                    p_objErrOut.Add("ClaimCommentSummaryAdaptor.GetXmlInfo.Error", Globalization.GetString("ClaimCommentSummaryAdaptor.GetXmlInfo.IdMissing", base.ClientId), BusinessAdaptorErrorType.PopupMessage);//sharishkumar Jira 818
					return false;
				}
				iClaimEventId=Conversion.ConvertStrToInteger(objElement.InnerText);

                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//LangCode");
                sLangCode = objElement.InnerText.ToString();

                objClaimCommentSummary = new Riskmaster.Application.ClaimCommentSummary.ClaimCommentSummary(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, userLogin.UserId, userLogin.GroupId, base.ClientId);//sharishkumar Jira 818

                p_objXmlOut = objClaimCommentSummary.GetXmlInfo(iClaimEventId, sScreenName, sLangCode);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("ClaimCommentSummaryAdaptor.GetXmlInfo.Error", base.ClientId),BusinessAdaptorErrorType.Error);//sharishkumar Jira 818
				return false;
			}
			finally
			{
                if (objClaimCommentSummary != null)
                    objClaimCommentSummary.Dispose();
				objElement=null;
			}
		}

		#endregion

		#endregion
	}
}
