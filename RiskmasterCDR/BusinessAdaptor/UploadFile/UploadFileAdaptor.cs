using System;
using System.Text;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.UploadFile;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: UploadFileAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 13-September-2005
	///* $Author	: Ratheen Chaturvedi
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	A class representing the Upload File Adaptor.
	/// </summary>
	public class UploadFileAdaptor:BusinessAdaptorBase
	{
		#region Class Constructor
		public UploadFileAdaptor(){}
		#endregion

		#region Public Functions
		/// <summary>
		/// Nullify/Void checks
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml
		///		The structure of input XML document would be:
		///		<Document>
		///		 <UploadFile>
		///				<FileContent>FileContent</FileContent>
		///				<FileName>FileName</FileName>
		///				<TargetDirectory>TargetDirectory</TargetDirectory>
		///		 </UploadFile>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool FileTransfer(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			UploadFile objUploadFile=null; //Application layer component
			XmlElement objElement=null;	
			string sFileContent = string.Empty;
			string sFileName = string.Empty;
			string strExt = string.Empty;
			string sFolderPath	= string.Empty;
			string sFolderPathXmlTag = string.Empty;
			RMConfigurator objRMConfigurator = null;
			try
			{
				objRMConfigurator=new RMConfigurator();
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//FileContent");
				sFileContent=objElement.InnerText;
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//FileName");
				sFileName = objElement.InnerText;
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//FolderPath");
				sFolderPathXmlTag = objElement.InnerText;
				sFolderPath = objRMConfigurator.GetValue(sFolderPathXmlTag);

				string [] arrsFileNameParts = null;

				//For multiple files separated by space

				if (sFileName != "")
				{
					arrsFileNameParts = sFileName.Split(" ".ToCharArray());
					
					sFileName = arrsFileNameParts[arrsFileNameParts.Length-1];//last Filename is picked up
										
				}
				//For separating filename from the location e.g.c:\\codes.txt will change to codes.txt
				if (sFileName != "")
				{
					arrsFileNameParts = sFileName.Split("\\".ToCharArray());
					foreach(string sFName in arrsFileNameParts)
					{
						if (sFName.IndexOf('.') > 0)
						{
							sFileName = sFName;
						}
					}					
				}
				DirectoryInfo objDir = new DirectoryInfo(sFolderPath);
				if(!objDir.Exists)
					objDir.Create();
				sFileName = sFolderPath+"\\"+sFileName;
				objUploadFile = new UploadFile ();
				//Tranfer contents
				if(!objUploadFile.FileTranfer(sFileContent,sFileName))
					return false;

				p_objXmlOut = null;
				return true; 
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException, Globalization.GetString("UploadFileAdaptor.FileTransfer.Error"),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				if(objUploadFile != null)
				{
					objUploadFile=null;
				}
			}
		}
		#endregion

	}
}
