﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Net.Mail; //Needed for SMTP mailing
using System.Text.RegularExpressions; //RegEX
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using Riskmaster.Security;
using Riskmaster.Db;
using Riskmaster.BusinessAdaptor;
using Riskmaster.Application.FileStorage;
using Riskmaster.Application.AcrosoftWebserviceWrapper;
using Riskmaster.Settings;
using Riskmaster.Application.DocumentManagement;
using Amyuni.PDFCreator;


namespace Riskmaster.BusinessAdaptor.AutoFroiAcord
{
    public class AutoFroiAcordAdaptor : IDisposable
    {


        #region Variable Declaration
        /// <summary>
        /// Private variable to store connection string
        /// </summary>
        private string m_sConnectionString = string.Empty;
        private bool isDisposed = false;
        private string m_sRequestHost = string.Empty;
        private int m_iClientId = 0;
        
        // Geeta 19341  : Split architecture issue  ACORD and FROI
        public string RequestHost
        {
            get
            {
                return m_sRequestHost;
            }
            set
            {
                m_sRequestHost = value;
            }
        }

        #endregion


        /// <summary>
        /// Default class constructor
        /// </summary>
        public AutoFroiAcordAdaptor(int p_iClientId)
        {

            m_iClientId = p_iClientId;
            //Initialize the Amyuni license information at the class level
            acPDFCreatorLib.Initialize();
            acPDFCreatorLib.SetLicenseKey(FroiAcord_en_us.AmyuniTechLicenseCompany, FroiAcord_en_us.AmyuniTechLicenseKey);
        }//method: 


        /// <summary>
        /// 
        /// </summary>
        /// <param name="objClaim"></param>
        /// <returns></returns>
        public string generateFROIACORD(Claim objClaim)
        {
            string sSQL = "";
            string sOrgEid = "";
            string[] sOrgList;
            string sPassword = "";
            DbReader rdr = null;
            string sEmail = "";
            string sFroiFormId = "";
            string sAcordFormId = "";
            string sSubjectAttach = "";
            string sSubjectPwd = "";
            string sBodyPwd = "";
            string sBodyAttach = "";
            string sAtchSub = "";
            string sFDFPath = "";
            string sPDFPath = "";
            string sPDFFile = "";
            bool bFileGenerated = false;
            string sError = "";
            bool bMailSent = false;
            string sMergePDFPath = "";
            string sMergePwdPDFPath = "";
            MemoryStream objMemory = null;
            XmlDocument p_objXmlIn;
            XmlDocument p_objXmlOut;
            BusinessAdaptorErrors p_objErrOut;
            
            BusinessAdaptor.FROIAdaptor objAutoFROI = null;
            DbConnection objCon = null;
            string sAttachFilePath = "";
            bool bFroi = false;
            bool bAcord = false;
            Event objEvent = null;
            int iErrorType = 0; //MGABA2:MITS 19613
            bool bParse = false;//MGABA2:MITS 19613
            try
            {
                p_objXmlIn = new XmlDocument();
                p_objXmlOut = new XmlDocument();
                p_objErrOut = new BusinessAdaptorErrors(m_iClientId);

                objEvent = (Event)objClaim.Context.Factory.GetDataModelObject("Event", false);
                objEvent.MoveTo(objClaim.EventId);

                m_sConnectionString = objClaim.Context.DbConnLookup.ConnectionString;

                objCon = DbFactory.GetDbConnection(m_sConnectionString);
                objCon.Open();

                //Fetch All the Org Levels
                sSQL = "SELECT * FROM ORG_HIERARCHY WHERE DEPARTMENT_EID = " + objEvent.DeptEid;
                using (rdr = objClaim.Context.DbConnLookup.ExecuteReader(sSQL))
                {
                    if (rdr.Read())
                    {
                        sOrgEid = rdr.GetInt("DEPARTMENT_EID") + "," + rdr.GetInt("FACILITY_EID") + "," + rdr.GetInt("LOCATION_EID") + "," + rdr.GetInt("DIVISION_EID") + "," + rdr.GetInt("REGION_EID") + "," + rdr.GetInt("OPERATION_EID") + "," + rdr.GetInt("COMPANY_EID") + "," + rdr.GetInt("CLIENT_EID");
                    }
                    rdr.Close();
                }

                sOrgList = sOrgEid.Split(new char[] { ',' });

                //Fetch Password for Both FROI AND ACORD
                //Password will be searched from Dept Level to Client Level in the same order.
                //Moment its found, it will exit the Loop.
                foreach (string sOrgLevel in sOrgList)
                {
                    sSQL = "SELECT FROI_ACORD_PASSWORD FROM ENTITY WHERE ENTITY_ID = " + sOrgLevel;
                    sPassword = objCon.ExecuteString(sSQL);
                    if (! string.IsNullOrEmpty(sPassword))
                    {
                        break;
                    }
                }


                objAutoFROI = new FROIAdaptor();
                if (!string.IsNullOrEmpty(sPassword))
                {
                    if (objClaim.LineOfBusCode == 243)
                    {
                        //Fetch Email List for FROI - Contact Should Have E-mail FROI checkbox checked and should be WC Contact
                        //E-mail Address should not be blank
                        sSQL = "SELECT EMAIL_ADDRESS FROM ENT_X_CONTACTINFO,CONTACT_LOB WHERE ENTITY_ID IN (" + sOrgEid + ") AND EMAIL_FROI = -1";
                        sSQL = sSQL + " AND EMAIL_ADDRESS IS NOT NULL AND EMAIL_ADDRESS <> ' ' AND ENT_X_CONTACTINFO.CONTACT_ID = CONTACT_LOB.CONTACT_ID AND CONTACT_LOB.LINE_OF_BUS_CODE = " + objClaim.LineOfBusCode;
                        using (rdr = objClaim.Context.DbConnLookup.ExecuteReader(sSQL))
                        {
                            while (rdr.Read())
                            {
                                sEmail = rdr.GetString("EMAIL_ADDRESS") + "," + sEmail;
                            }
                        }
                        //add e-mail Address from the Claim Screen too.
                        sEmail = sEmail + objClaim.EmailAddresses;
                        if (sEmail != "")
                        {
                            //FormID for that Particular Jurisdiction
                            sFroiFormId = objCon.ExecuteString("SELECT FORM_ID FROM JURIS_FORMS,CLAIM WHERE CLAIM_ID = " + objClaim.ClaimId + " AND STATE_ROW_ID = FILING_STATE_ID AND PRIMARY_FORM_FLAG = -1");
                            //HardCode XML to Invode AutoFROI function
                            p_objXmlIn = GetFROIXml(objClaim.ClaimId, sFroiFormId);

                            //01/19/10 Geeta 19341  : Split architecture issue  ACORD and FROI
                            p_objXmlIn.SelectSingleNode("//RequestHost").InnerText = m_sRequestHost;
                            bFroi = objAutoFROI.InvokeAutoFROI(p_objXmlIn, objClaim, ref p_objXmlOut, ref p_objErrOut, ref sFDFPath, ref sPDFPath, ref sMergePDFPath, ref sMergePwdPDFPath);
                            //MGaba2:MITS 19613:Start
                            //Checking if some error has been returned from legacy froinet6 dll.In case yes,then that error should be saved in db
                            bParse= int.TryParse(sFDFPath,out iErrorType);                            
                            if (bParse)
                            {
                                switch (iErrorType)
                                {
                                    case (int)FROIAdaptor.FROIFails.FF_NCCIBodyParts:
                                        return(FroiAcord_en_us.AutoFroiAcordAdaptor_generateFROIACORD_NCCIBodyParts);
                                    case (int)FROIAdaptor.FROIFails.FF_NCCICauseCode:
                                        return (FroiAcord_en_us.AutoFroiAcordAdaptor_generateFROIACORD_NCCICause);
                                    case (int)FROIAdaptor.FROIFails.FF_NCCIIllness:
                                        return(FroiAcord_en_us.AutoFroiAcordAdaptor_generateFROIACORD_NCCIInjury_Illness);
                                    case (int)FROIAdaptor.FROIFails.FF_NonNumericClassCode:
                                        return(FroiAcord_en_us.AutoFroiAcordAdaptor_generateFROIACORD_NonNumericClassCode);
                                    case (int)FROIAdaptor.FROIFails.FF_NCCIBodyParts + (int)FROIAdaptor.FROIFails.FF_NCCICauseCode:
                                        return(FroiAcord_en_us.AutoFroiAcordAdaptor_generateFROIACORD_NCCIBodyParts +  FroiAcord_en_us.AutoFroiAcordAdaptor_generateFROIACORD_NCCICause );
                                    case (int)FROIAdaptor.FROIFails.FF_NCCIBodyParts + (int)FROIAdaptor.FROIFails.FF_NCCIIllness:
                                        return (FroiAcord_en_us.AutoFroiAcordAdaptor_generateFROIACORD_NCCIBodyParts + FroiAcord_en_us.AutoFroiAcordAdaptor_generateFROIACORD_NCCIInjury_Illness );                                        
                                    case (int)FROIAdaptor.FROIFails.FF_NCCIBodyParts + (int)FROIAdaptor.FROIFails.FF_NonNumericClassCode:
                                        return (FroiAcord_en_us.AutoFroiAcordAdaptor_generateFROIACORD_NCCIBodyParts + FroiAcord_en_us.AutoFroiAcordAdaptor_generateFROIACORD_NonNumericClassCode);                                        
                                    case (int)FROIAdaptor.FROIFails.FF_NCCICauseCode + (int)FROIAdaptor.FROIFails.FF_NCCIIllness:
                                        return (FroiAcord_en_us.AutoFroiAcordAdaptor_generateFROIACORD_NCCICause + FroiAcord_en_us.AutoFroiAcordAdaptor_generateFROIACORD_NCCIInjury_Illness);                                        
                                    case (int)FROIAdaptor.FROIFails.FF_NCCICauseCode + (int)FROIAdaptor.FROIFails.FF_NonNumericClassCode:
                                        return (FroiAcord_en_us.AutoFroiAcordAdaptor_generateFROIACORD_NCCICause + FroiAcord_en_us.AutoFroiAcordAdaptor_generateFROIACORD_NonNumericClassCode);                                        
                                    case (int)FROIAdaptor.FROIFails.FF_NCCIIllness + (int)FROIAdaptor.FROIFails.FF_NonNumericClassCode:
                                        return (FroiAcord_en_us.AutoFroiAcordAdaptor_generateFROIACORD_NCCIInjury_Illness + FroiAcord_en_us.AutoFroiAcordAdaptor_generateFROIACORD_NonNumericClassCode );                                        
                                    case (int)FROIAdaptor.FROIFails.FF_NCCIBodyParts + (int)FROIAdaptor.FROIFails.FF_NCCICauseCode + (int)FROIAdaptor.FROIFails.FF_NCCIIllness:
                                        return (FroiAcord_en_us.AutoFroiAcordAdaptor_generateFROIACORD_NCCIBodyParts + FroiAcord_en_us.AutoFroiAcordAdaptor_generateFROIACORD_NCCICause + FroiAcord_en_us.AutoFroiAcordAdaptor_generateFROIACORD_NCCIInjury_Illness);                                        
                                    case (int)FROIAdaptor.FROIFails.FF_NCCIBodyParts + (int)FROIAdaptor.FROIFails.FF_NCCIIllness + (int)FROIAdaptor.FROIFails.FF_NonNumericClassCode:
                                        return (FroiAcord_en_us.AutoFroiAcordAdaptor_generateFROIACORD_NCCIBodyParts + FroiAcord_en_us.AutoFroiAcordAdaptor_generateFROIACORD_NCCIInjury_Illness + FroiAcord_en_us.AutoFroiAcordAdaptor_generateFROIACORD_NonNumericClassCode);           
                                    case (int)FROIAdaptor.FROIFails.FF_NCCICauseCode + (int)FROIAdaptor.FROIFails.FF_NCCIIllness + (int)FROIAdaptor.FROIFails.FF_NonNumericClassCode:
                                        return (FroiAcord_en_us.AutoFroiAcordAdaptor_generateFROIACORD_NCCICause + FroiAcord_en_us.AutoFroiAcordAdaptor_generateFROIACORD_NCCIInjury_Illness + FroiAcord_en_us.AutoFroiAcordAdaptor_generateFROIACORD_NonNumericClassCode);
                                    case (int)FROIAdaptor.FROIFails.FF_NCCIBodyParts + (int)FROIAdaptor.FROIFails.FF_NCCICauseCode + (int)FROIAdaptor.FROIFails.FF_NCCIIllness + (int)FROIAdaptor.FROIFails.FF_NonNumericClassCode:
                                        return (FroiAcord_en_us.AutoFroiAcordAdaptor_generateFROIACORD_NCCIBodyParts_NCCICause_NCCIInjury_Illness_NonNumericClassCode);
                                    case (int)FROIAdaptor.FROIFails.FF_NCCIBodyParts + (int)FROIAdaptor.FROIFails.FF_NCCICauseCode + (int)FROIAdaptor.FROIFails.FF_NonNumericClassCode:
                                        return (FroiAcord_en_us.AutoFroiAcordAdaptor_generateFROIACORD_NCCIBodyParts   + FroiAcord_en_us.AutoFroiAcordAdaptor_generateFROIACORD_NCCICause  + FroiAcord_en_us.AutoFroiAcordAdaptor_generateFROIACORD_NonNumericClassCode );                                         
                                    case (int)FROIAdaptor.FROIFails.FF_NCCIClassCode:
                                        return (FroiAcord_en_us.AutoFroiAcordAdaptor_generateFROIACORD_NCCIClass);                                         
                                    case (int)FROIAdaptor.FROIFails.FF_NoDepartmentAssigned:
                                        return (FroiAcord_en_us.AutoFroiAcordAdaptor_generateFROIACORD_NoDepartmentAssigned);                                        
                                    case (int)FROIAdaptor.FROIFails.FF_NoClaimant:
                                        return (FroiAcord_en_us.AutoFroiAcordAdaptor_generateFROIACORD_NoClaimant);                                        
                                    case (int)FROIAdaptor.FROIFails.FF_LoadFROIOptionsFailed:
                                        return (FroiAcord_en_us.AutoFroiAcordAdaptor_generateFROIACORD_LoadFROIOptionsFailed);                                        
                                }
                            }//MGaba2: MITS 19613 :End
                            if (sMergePDFPath != "")
                                //PDF template path and Name to which FDF is pointing to.
                                sPDFFile = sPDFPath + objCon.ExecuteString("SELECT PDF_FILE_NAME FROM JURIS_FORMS WHERE FORM_ID =  " + sFroiFormId);
                           
                        }
                        else
                        {
                            return "No E-mail addresses were found at Claim Level and at any level of Organization Hierarchy.";
                        }
                    }
                    else if ((objClaim.LineOfBusCode == 241) || (objClaim.LineOfBusCode == 242))
                    {
                        //Fetch Email List for ACORD- Contact Should Have E-mail FROI checkbox checked and should be WC Contact
                        //E-mail Address should not be blank

                        //Geeta : Mits 18718 for Acord Comma Seperated List Issue
                        sSQL = "SELECT EMAIL_ADDRESS FROM ENT_X_CONTACTINFO,CONTACT_LOB WHERE ENTITY_ID IN (" + sOrgEid + ") AND EMAIL_ACORD = -1";
                        sSQL = sSQL + " AND EMAIL_ADDRESS IS NOT NULL AND EMAIL_ADDRESS <> ' ' AND ENT_X_CONTACTINFO.CONTACT_ID = CONTACT_LOB.CONTACT_ID AND CONTACT_LOB.LINE_OF_BUS_CODE = " + objClaim.LineOfBusCode;
                        using (rdr = objClaim.Context.DbConnLookup.ExecuteReader(sSQL))
                        {
                            while (rdr.Read())
                            {
                                sEmail = rdr.GetString("EMAIL_ADDRESS") + "," + sEmail;
                            }
                        }
                        //add e-mail Address from the Claim Screen too.
                        sEmail = sEmail + objClaim.EmailAddresses;
                        if (sEmail != "")
                        {
                            //FormID for that Particular Claim Type Code
                            sAcordFormId = objCon.ExecuteString("SELECT ACORD_FORM_ID FROM CLAIM_ACCORD_MAPPI WHERE CLAIM_TYPE_CODE = " + objClaim.ClaimTypeCode + " AND ACORD_FORM_ID > 0");
                            //HardCode XML to Invode AutoAcord function
                            p_objXmlIn = GetFROIXml(objClaim.ClaimId, sAcordFormId);
                            p_objXmlIn.SelectSingleNode("//RequestHost").InnerText = m_sRequestHost;
 
                            bAcord = objAutoFROI.InvokeAutoACORD(p_objXmlIn, objClaim, ref p_objXmlOut, ref p_objErrOut, ref sFDFPath, ref sPDFPath, ref sMergePDFPath, ref sMergePwdPDFPath);
                            if (sMergePDFPath != "")
                                //PDF template path and Name to which FDF is pointing to.
                                sPDFFile = sPDFPath + objCon.ExecuteString("SELECT FILE_NAME FROM CL_FORMS WHERE FORM_ID = " + sAcordFormId);
                        }
                        else
                        {
                            return "No E-mail addresses found at Claim Level and at any level of Organization Hierarchy.";
                        }
                    }
                }
                else
                {
                    return "Email FROI-ACORD Password is not set at any level of Organization Hierarchy.";
                }

                //if FDF file was made, then Merge FDF and PDF USING Acrobat Writer
                if ((bFroi || bAcord) && (sMergePDFPath != ""))
                {                                        
                    //Name of the file to be attached to Claim
                    sAttachFilePath = Path.GetTempPath() + objClaim.ClaimNumber;
                    if (objClaim.LineOfBusCode == 243)
                        sAttachFilePath = sAttachFilePath + "_FROIATTACH.pdf";
                    if ((objClaim.LineOfBusCode == 241) || (objClaim.LineOfBusCode == 242))
                        sAttachFilePath = sAttachFilePath + "_ACORDATTACH.pdf";
                    //Merge FDF and PDF into one PDF File
                    if (MergeFDFTOPDF(sPDFFile, sFDFPath, sAttachFilePath, ref sError))
                    {
                        if (EncryptMergePDF(sAttachFilePath,  sMergePwdPDFPath, sPassword, ref sError))
                            bFileGenerated = true;
                        else
                            return sError;
                    }
                    else
                        return sError;                             
                }
                else
                {
                    return "FROI ACORD PDF/FDF form was not generated.";
                }
                //Merged File was created using Adobe writer
                if (bFileGenerated)
                {
                    //objMemory = new MemoryStream();
                    ////get the binary format of the file to be attached to Claim
                    //GetFileStream(sAttachFilePath, ref objMemory);
                    ////PDF Form Generated - it will be attached to Claim
                    //AttachDocument(objMemory, objClaim);

                    #region Extra fields to be attached in the Email for ACORD and FROI
                    //Devender start
                    //Mar 30, 2010
                    //MITS 20166 
                    //This functionality is to provide some data fields to the Client in the body of the Email for ACORD FROI
                    //Data will be shown in the following format

                //Name of Claiment      : {CLAIMANT_NAME}
                //Claim Number          : {CLAIM_NO}
                //City, State of Injury : {CITY_STATE_INJURY}
                //Occupation            : {OCCUPATION}
                //Date Reported      	: {DATE_REPORTED}
                //Location Code         : {LOCATION_CODE}
                //Insured Name          : {INSURED_NAME,CLIENT}


                   
                Entity objEntity = null;
                string sStateShortCode = string.Empty;
                string sStateDesc = string.Empty;
                string sPositionCode = string.Empty;
                string str = string.Empty;
                string sClaimantName = string.Empty;
                string sEventState = string.Empty;
                string sLocationShortCode = string.Empty;
                string sLocationDesc = string.Empty;
                string sInsuredName = string.Empty;
                //int iDepartment_EID=0;
                int iOrgHier_EID = 0; //roslanki2: chubb custom email format updates 
                string sOrgType = string.Empty;
                string sOrgTypeShort = string.Empty;
                // rsolanki2: not using compiled regex here as it would just be used once or twice.
                string sRegex = @"{INSURED_NAME,([A-Za-z]{0,12})}";
                //Regex rxInsuredNameOrgHier = new Regex(sRegex);
                //MatchEvaluator meInsuredNameOrgHier = new MatchEvaluator(@"{INSURED_NAME,([A-Za-z]{0,12})}");
                string sTemp = string.Empty;
                //Match rxmInsuredNameOrgHierMatch;
                //MatchCollection rxmInsuredNameOrgHierCollection;
                string sStateCity = string.Empty;
                //int iRegion_EID=0;
                int iAcordEmailFieldsEnabled = 0;
                int iFroiEmailFieldsEnabled =0;
                StreamReader strReader;
                //Fetching the setting values, whether these settings are enabled or not
                sSQL = "SELECT USE_CUS_FLD_FROI_FLAG, USE_CUS_FLD_ACORD_FLAG FROM EMAIL_FROI_ACORD";
                using (rdr = objClaim.Context.DbConnLookup.ExecuteReader(sSQL))
                {
                  while (rdr.Read())
                    {
                        iFroiEmailFieldsEnabled =rdr.GetInt("USE_CUS_FLD_FROI_FLAG");
                        iAcordEmailFieldsEnabled =rdr.GetInt("USE_CUS_FLD_ACORD_FLAG");
                    }
                }
                if (bFroi)
                {
                    //Check whether the setting is enabled for ACORD Email : -1 for Enabled
                    if (iFroiEmailFieldsEnabled==-1)
                    {
                        #region Email will contain the Extra data fields

                        foreach (Claimant objClaimant in objClaim.ClaimantList)
                        {
                            if (objClaimant.PrimaryClmntFlag)
                            {
                                objEntity = (Entity)objClaim.Context.Factory.GetDataModelObject("Entity", false);
                                objEntity.MoveTo(objClaimant.ClaimantEid);
                                sClaimantName = objEntity.LastName;//Claimant Name
                                if (objEntity.FirstName.Trim().Length != 0)
                                {
                                    sClaimantName = sClaimantName + ", " + objEntity.FirstName;
                                }
                                sPositionCode = objEntity.Title;//Occupation
                                break;
                            }
                        }
                        //Reading the Email body template file
                        strReader = new StreamReader(RMConfigurator.AppFilesPath + @"\froi\FROI_EMAIL.txt");
                        sBodyAttach = strReader.ReadToEnd();

                        //rsolanki2: replacing all templates with appropraite values. 
                        sTemp = Regex.Replace(sBodyAttach, sRegex, delegate(Match rxmInsuredNameOrgHierMatch)
                        {
                            //return match.ToString().Substring(0, 2);
                            sInsuredName = string.Empty;
                            
                            sOrgTypeShort = sOrgType = rxmInsuredNameOrgHierMatch.Groups[1].Value.Trim();

                            if (sOrgType.Equals("DEPARTMENT")
                                || sOrgType.Equals("FACILITY")
                                || sOrgType.Equals("LOCATION")
                                || sOrgType.Equals("DIVISION")
                                || sOrgType.Equals("REGION")
                                || sOrgType.Equals("OPERATION")
                                || sOrgType.Equals("COMPANY")
                                || sOrgType.Equals("CLIENT")
                                )
                            {
                                sOrgType = string.Concat(sOrgType.ToUpper(), "_EID");

                                sSQL = string.Concat("SELECT * FROM ORG_HIERARCHY WHERE DEPARTMENT_EID = ", objEvent.DeptEid);

                                using (rdr = objClaim.Context.DbConnLookup.ExecuteReader(sSQL))
                                {
                                    if (rdr.Read())
                                    {
                                        iOrgHier_EID = rdr.GetInt(sOrgType);
                                        //iDepartment_EID = rdr.GetInt("DEPARTMENT_EID");
                                        //iRegion_EID = rdr.GetInt("REGION_EID");
                                    }
                                    rdr.Close();
                                }

                                objEntity.MoveTo(iOrgHier_EID);
                                if (objEntity.FirstName.Trim().Length != 0)
                                {
                                    sInsuredName = objEntity.LastName + ", " + objEntity.FirstName;
                                }
                                else
                                {
                                    sInsuredName = objEntity.LastName;
                                }

                                //sBodyAttach = sBodyAttach.Replace("{INSURED_NAME,", sInsuredName);

                                //sBodyAttach = Regex.Replace(sBodyAttach, sOrgTypeShort + "}", string.Empty);
                                //sBodyAttach = string.Concat(
                                //           sBodyAttach.Substring(0, rxmInsuredNameOrgHierMatch.Index)
                                //           , sInsuredName
                                //           , sBodyAttach.Substring(rxmInsuredNameOrgHierMatch.Index + rxmInsuredNameOrgHierMatch.Length)
                                //    );

                            }


                            return sInsuredName;
                        });
                        sBodyAttach = sTemp;

                        //rsolanki2: retriving the regex matches for required Org hier type. 
                        //rxmInsuredNameOrgHier =  rxInsuredNameOrgHier.Match(sBodyAttach);
                        //rxmInsuredNameOrgHierCollection = rxInsuredNameOrgHier.Matches(sBodyAttach);

                        //foreach (Match rxmInsuredNameOrgHierMatch in rxmInsuredNameOrgHierCollection)
                        //{
                        //    if (rxmInsuredNameOrgHierMatch.Success) // Not really required for MatchCollection
                        //    {   
                        //        sOrgTypeShort = sOrgType = rxmInsuredNameOrgHierMatch.Groups[1].Value.Trim();

                        //        if (sOrgType.Equals("DEPARTMENT")
                        //            || sOrgType.Equals("FACILITY")
                        //            || sOrgType.Equals("LOCATION")
                        //            || sOrgType.Equals("DIVISION")
                        //            || sOrgType.Equals("REGION")
                        //            || sOrgType.Equals("OPERATION")
                        //            || sOrgType.Equals("COMPANY")
                        //            || sOrgType.Equals("CLIENT")
                        //            )
                        //        {
                        //            sOrgType = string.Concat(sOrgType.ToUpper(), "_EID");

                        //            sSQL = string.Concat("SELECT * FROM ORG_HIERARCHY WHERE DEPARTMENT_EID = ", objEvent.DeptEid);

                        //            using (rdr = objClaim.Context.DbConnLookup.ExecuteReader(sSQL))
                        //            {
                        //                if (rdr.Read())
                        //                {
                        //                    iOrgHier_EID = rdr.GetInt(sOrgType);
                        //                    //iDepartment_EID = rdr.GetInt("DEPARTMENT_EID");
                        //                    //iRegion_EID = rdr.GetInt("REGION_EID");
                        //                }
                        //                rdr.Close();
                        //            }

                        //            objEntity.MoveTo(iOrgHier_EID);
                        //            if (objEntity.FirstName.Trim().Length != 0)
                        //            {
                        //                sInsuredName = objEntity.LastName + ", " + objEntity.FirstName;
                        //            }
                        //            else
                        //            {
                        //                sInsuredName = objEntity.LastName;
                        //            }

                        //            //sBodyAttach = sBodyAttach.Replace("{INSURED_NAME,", sInsuredName);
                                    
                        //            //sBodyAttach = Regex.Replace(sBodyAttach, sOrgTypeShort + "}", string.Empty);
                        //            sBodyAttach = string.Concat(
                        //                       sBodyAttach.Substring(0, rxmInsuredNameOrgHierMatch.Index)
                        //                       ,sInsuredName
                        //                       ,sBodyAttach.Substring(rxmInsuredNameOrgHierMatch.Index + rxmInsuredNameOrgHierMatch.Length)                                        
                        //                );
                                          
                        //        }
                        //        //else
                        //        //{
                        //        //    // no match possibly due to spelling error etc in template 
                        //        //    //rsolanki2: we replace the template with Blank string.
                        //        //    sBodyAttach = sBodyAttach.Replace(rxmInsuredNameOrgHierMatch.Value, string.Empty);
                        //        //}

                        //    } // if of match replace ends here
                        //}// foreach

                        
                        objClaim.Context.LocalCache.GetCodeInfo(objEvent.PrimaryLocCode, ref sLocationShortCode, ref sLocationDesc);
                        sStateCity = objEvent.City;
                        objClaim.Context.LocalCache.GetStateInfo(objEvent.StateId, ref sStateShortCode, ref sStateDesc);
                        if (sStateShortCode.Length != 0)
                        {
                            sStateCity = string.Concat(sStateCity, ", ", sStateShortCode);                        
                        }
                        
                        //rsolanki2: replacing templated fields with actual values.
                        sBodyAttach = sBodyAttach.Replace("{CLAIMANT_NAME}", sClaimantName)
                            .Replace("{CLAIM_NO}", objClaim.ClaimNumber)
                            .Replace("{CITY_STATE_INJURY}", sStateCity)
                            .Replace("{DATE_REPORTED}", Riskmaster.Common.Conversion.GetDBDateFormat( objClaim.DateOfClaim,"MM/dd/yyyy"))
                            .Replace("{OCCUPATION}", sPositionCode)
                            .Replace("{DATE_OF_LOSS}", Riskmaster.Common.Conversion.GetDBDateFormat(objEvent.DateOfEvent, "MM/dd/yyyy"))
                            .Replace("{LOCATION_CODE}", (sLocationShortCode + "-" + sLocationDesc));                        
                        
                        #endregion
                    }
                    else
                    {
                        sBodyAttach = string.Concat("Attached is the First Report of Injury for Claim "
                            , objClaim.ClaimNumber ,"." , "\r\n" , "\r\n" ,
                            "The attached file has been encrypted and password protected for your data security."
                            ,"\r\n" , "\r\n" , "Password has been sent in a separate email." , "\r\n");
                    }

                    sSubjectAttach = objClaim.ClaimNumber + ", First Report of Injury";
                    sSubjectPwd = objClaim.ClaimNumber + ", First Report of Injury Password ";
                    sBodyPwd = "Password of First Report of Injury for Claim " + objClaim.ClaimNumber + " is " + sPassword + ".";
                    sAtchSub = "First Report of Injury or Illness";

                }
                else if (bAcord)
                {
                    //Check whether the setting is done for ACORD Email
                    if (iAcordEmailFieldsEnabled==-1)
                    {
                        #region Email will contain the Extra data fields
                        //rsushilaggar MITs 22282 Date 10/14/2010
                        objEntity = (Entity)objClaim.Context.Factory.GetDataModelObject("Entity", false);
                        foreach (Claimant objClaimant in objClaim.ClaimantList)
                        {
                            if (objClaimant.PrimaryClmntFlag)
                            {
                                
                                objEntity.MoveTo(objClaimant.ClaimantEid);
                                sClaimantName = objEntity.LastName;//Claimant Name
                                if (objEntity.FirstName.Trim().Length != 0)
                                {
                                    sClaimantName = sClaimantName + ", " + objEntity.FirstName;
                                }
                                sPositionCode = objEntity.Title;//Occupation
                                break;
                            }
                        }
                        //Reading the Email body template file
                        strReader = new StreamReader(RMConfigurator.AppFilesPath + @"\froi\ACORD_EMAIL.txt");
                        sBodyAttach = strReader.ReadToEnd();

                        //rsolanki2: replacing all templates with appropraite values. 
                        sTemp = Regex.Replace(sBodyAttach, sRegex, delegate(Match rxmInsuredNameOrgHierMatch)
                        {
                            //return match.ToString().Substring(0, 2);
                            sInsuredName = string.Empty;

                            sOrgTypeShort = sOrgType = rxmInsuredNameOrgHierMatch.Groups[1].Value.Trim();

                            if (sOrgType.Equals("DEPARTMENT")
                                || sOrgType.Equals("FACILITY")
                                || sOrgType.Equals("LOCATION")
                                || sOrgType.Equals("DIVISION")
                                || sOrgType.Equals("REGION")
                                || sOrgType.Equals("OPERATION")
                                || sOrgType.Equals("COMPANY")
                                || sOrgType.Equals("CLIENT")
                                )
                            {
                                sOrgType = string.Concat(sOrgType.ToUpper(), "_EID");

                                sSQL = string.Concat("SELECT * FROM ORG_HIERARCHY WHERE DEPARTMENT_EID = ", objEvent.DeptEid);

                                using (rdr = objClaim.Context.DbConnLookup.ExecuteReader(sSQL))
                                {
                                    if (rdr.Read())
                                    {
                                        iOrgHier_EID = rdr.GetInt(sOrgType);
                                        //iDepartment_EID = rdr.GetInt("DEPARTMENT_EID");
                                        //iRegion_EID = rdr.GetInt("REGION_EID");
                                    }
                                    rdr.Close();
                                }

                                objEntity.MoveTo(iOrgHier_EID);
                                if (objEntity.FirstName.Trim().Length != 0)
                                {
                                    sInsuredName = objEntity.LastName + ", " + objEntity.FirstName;
                                }
                                else
                                {
                                    sInsuredName = objEntity.LastName;
                                }

                                //sBodyAttach = sBodyAttach.Replace("{INSURED_NAME,", sInsuredName);

                                //sBodyAttach = Regex.Replace(sBodyAttach, sOrgTypeShort + "}", string.Empty);
                                //sBodyAttach = string.Concat(
                                //           sBodyAttach.Substring(0, rxmInsuredNameOrgHierMatch.Index)
                                //           , sInsuredName
                                //           , sBodyAttach.Substring(rxmInsuredNameOrgHierMatch.Index + rxmInsuredNameOrgHierMatch.Length)
                                //    );

                            }


                            return sInsuredName;
                        });
                        sBodyAttach = sTemp;

                        //rsolanki2: retriving the regex matches for required Org hier type. 
                        //rxmInsuredNameOrgHier = rxInsuredNameOrgHier.Match(sBodyAttach);

                        //if (rxmInsuredNameOrgHier.Success)
                        //{
                        //    sOrgTypeShort = sOrgType = rxmInsuredNameOrgHier.Groups[1].Value.Trim();

                        //    if (sOrgType.Equals("DEPARTMENT", StringComparison.OrdinalIgnoreCase)
                        //        || sOrgType.Equals("FACILITY", StringComparison.OrdinalIgnoreCase)
                        //        || sOrgType.Equals("LOCATION", StringComparison.OrdinalIgnoreCase)
                        //        || sOrgType.Equals("DIVISION", StringComparison.OrdinalIgnoreCase)
                        //        || sOrgType.Equals("REGION", StringComparison.OrdinalIgnoreCase)
                        //        || sOrgType.Equals("OPERATION", StringComparison.OrdinalIgnoreCase)
                        //        || sOrgType.Equals("COMPANY", StringComparison.OrdinalIgnoreCase)
                        //        || sOrgType.Equals("CLIENT", StringComparison.OrdinalIgnoreCase)
                        //        )
                        //    {
                        //        sOrgType = string.Concat(sOrgType.ToUpper(), "_EID");

                        //        sSQL = string.Concat("SELECT * FROM ORG_HIERARCHY WHERE DEPARTMENT_EID = ", objEvent.DeptEid);

                        //        using (rdr = objClaim.Context.DbConnLookup.ExecuteReader(sSQL))
                        //        {
                        //            if (rdr.Read())
                        //            {
                        //                iOrgHier_EID = rdr.GetInt(sOrgType);
                        //                //iDepartment_EID = rdr.GetInt("DEPARTMENT_EID");
                        //                //iRegion_EID = rdr.GetInt("REGION_EID");
                        //            }
                        //            rdr.Close();
                        //        }

                        //        objEntity.MoveTo(iOrgHier_EID);
                        //        if (objEntity.FirstName.Trim().Length != 0)
                        //        {
                        //            sInsuredName = objEntity.LastName + ", " + objEntity.FirstName;
                        //        }
                        //        else
                        //        {
                        //            sInsuredName = objEntity.LastName;
                        //        }

                        //        sBodyAttach = sBodyAttach.Replace("{INSURED_NAME,", sInsuredName);
                        //        sBodyAttach = Regex.Replace(sBodyAttach, sOrgTypeShort + "}", string.Empty, RegexOptions.IgnoreCase);

                        //    }
                        //    else
                        //    {
                        //        // no match possibly due to spelling error etc in template 
                        //        //rsolanki2: we replace the template with Blank string.
                        //        sBodyAttach = sBodyAttach.Replace(rxmInsuredNameOrgHier.Value, string.Empty);
                        //    }

                        //}
                        objClaim.Context.LocalCache.GetCodeInfo(objEvent.PrimaryLocCode, ref sLocationShortCode, ref sLocationDesc);
                        sStateCity = objEvent.City;
                        objClaim.Context.LocalCache.GetStateInfo(objEvent.StateId, ref sStateShortCode, ref sStateDesc);
                        if (sStateShortCode.Length != 0)
                        {
                            sStateCity = string.Concat(sStateCity, ", ", sStateShortCode);
                        }

                        //rsolanki2: replacing templated fields with actual values.
                        sBodyAttach = sBodyAttach.Replace("{CLAIMANT_NAME}", sClaimantName)
                            .Replace("{CLAIM_NO}", objClaim.ClaimNumber)
                            .Replace("{CITY_STATE_INJURY}", sStateCity)
                            .Replace("{DATE_REPORTED}", Riskmaster.Common.Conversion.GetDBDateFormat(objClaim.DateOfClaim, "MM/dd/yyyy"))
                            .Replace("{OCCUPATION}", sPositionCode)
                            .Replace("{DATE_OF_LOSS}", Riskmaster.Common.Conversion.GetDBDateFormat(objEvent.DateOfEvent, "MM/dd/yyyy"))
                            .Replace("{LOCATION_CODE}", (sLocationShortCode + "-" + sLocationDesc));

                        #endregion
                    }
                    else
                    {
                        sBodyAttach = String.Concat("Attached is the ACORD Form for Claim " , 
                            objClaim.ClaimNumber , "." , "\r\n" , "\r\n" , 
                            "The attached file has been encrypted and password protected for your data security." 
                            , "\r\n" , "\r\n" , "Password has been sent in a separate email." , "\r\n");
                    }
                    sSubjectAttach = objClaim.ClaimNumber + ", ACORD Form";
                    sSubjectPwd = objClaim.ClaimNumber + ", ACORD Form Password ";
                    sBodyPwd = "Password of ACORD Form for Claim " + objClaim.ClaimNumber + " is " + sPassword + ".";
                    sAtchSub = "ACORD Form";
                }

#endregion
                    
                    //Devender Singh 
                    //MITS 20166 
                    //Following lines of code have been commented to add the new functionality
                    //if (bFroi)
                    //{
                    //    sSubjectAttach = objClaim.ClaimNumber + ", First Report of Injury";
                    //    sSubjectPwd = objClaim.ClaimNumber + ", First Report of Injury Password ";
                    //    sBodyPwd = "Password of First Report of Injury for Claim " + objClaim.ClaimNumber + " is " + sPassword + ".";
                    //    sBodyAttach = "Attached is the First Report of Injury for Claim " + objClaim.ClaimNumber + "." + "\r\n" + "\r\n" + "The attached file has been encrypted and password protected for your data security." + "\r\n" + "\r\n" + "Password has been sent in a separate email." + "\r\n";
                    //    sAtchSub = "First Report of Injury or Illness";
                    //}
                    //else if (bAcord)
                    //{
                    //    sSubjectAttach = objClaim.ClaimNumber + ", ACORD Form";
                    //    sSubjectPwd = objClaim.ClaimNumber + ", ACORD Form Password ";
                    //    sBodyPwd = "Password of ACORD Form for Claim " + objClaim.ClaimNumber + " is " + sPassword + ".";
                    //    sBodyAttach = "Attached is the ACORD Form for Claim " + objClaim.ClaimNumber + "." + "\r\n" + "\r\n" + "The attached file has been encrypted and password protected for your data security." + "\r\n" + "\r\n" + "Password has been sent in a separate email." + "\r\n";
                    //    sAtchSub = "ACORD Form";
                    //}
                    //End of commented lines

                //Devender End
                //Mar 30, 2010
                //MITS 20166 

                    //First Mail with the attachement
                    bMailSent = sendMail(sSubjectAttach, sBodyAttach, sEmail, sMergePwdPDFPath, ref sError);
                    if (bMailSent)
                    {
                        //Second Mail with the details of Password.
                        bMailSent = sendMail(sSubjectPwd, sBodyPwd, sEmail, "", ref sError);
                        if (bMailSent)
                        {
                            objClaim.MailSent = Riskmaster.Common.Conversion.ToDbDateTime(DateTime.Now);
                            objClaim.Save();                            
                            objMemory = new MemoryStream();
                            //get the binary format of the file to be attached to Claim
                            GetFileStream(sAttachFilePath, ref objMemory);
                            //PDF Form Generated - it will be attached to Claim
                            AttachDocument(objMemory, objClaim);
                            return "Success";
                        }
                        else
                        {
                            return sError;
                        }
                    }
                    else
                    {
                        return sError;
                    }
                }
                else
                {
                    //return "Merged PDF was not generated.";
                    return sError;
                }
            }
            catch (Exception ex)
            {
                return "Error Message in generateFROIACORD function: " + ex.Message;
            }
            finally
            {
                if (System.IO.File.Exists(sAttachFilePath))
                    System.IO.File.Delete(sAttachFilePath);
                if (System.IO.File.Exists(sMergePDFPath))
                    System.IO.File.Delete(sMergePDFPath);
                if (System.IO.File.Exists(sFDFPath))
                    System.IO.File.Delete(sFDFPath);
                if (System.IO.File.Exists(sMergePwdPDFPath))
                    System.IO.File.Delete(sMergePwdPDFPath);
                objAutoFROI = null;
                if (objCon != null)
                {
                    objCon.Dispose();
                }
                if (objClaim != null)
                {
                    objClaim.Dispose();
                }
                if (objEvent != null)
                {
                    objEvent.Dispose();
                }                
                if (rdr != null)
					rdr.Close();
				rdr = null;
            }

        }

        /// <summary>
        /// FROI/ACORD documents will be attached to Claim
        /// If MCM is turned on, then it will be attached using MCM.
        /// </summary>
        /// <param name="objMemory"></param>
        /// <param name="objClaim"></param>
        private string AttachDocument(MemoryStream objMemory, Claim objClaim)
        {
            XmlDocument objDOM = null;
            DocumentManager objDocumentManager = null;
            XmlElement objElemParent = null;
            XmlElement objElemChild = null;
            XmlElement objElemTemp = null;
            long lDocumentId = 0;
            SysSettings objSettings = null;
            Acrosoft objAcrosoft = null;
            BinaryReader binaryRdr = null;
            Byte[] fileBuffer = null;
            string p_sSubject = "";
            string p_sFileName = "";
            int iRetCd = 0;
            string sAppExcpXml = "";
            objAcrosoft = new Acrosoft(m_iClientId);//dvatsa-cloud
            //Claim objClaim = null;
            Event objEvent = null;
            DataModelFactory objDataModelFactory = null;
            int iPSID = 0;

            try
            {
                objDocumentManager = new DocumentManager(objClaim.Context.RMUser,m_iClientId);
                objDocumentManager.ConnectionString = objClaim.Context.DbConnLookup.ConnectionString;
                objDocumentManager.UserLoginName = objClaim.Context.RMUser.LoginName;

                if (objClaim.Context.RMUser.DocumentPath.Length > 0)
                {
                    objDocumentManager.DocumentStorageType = StorageType.FileSystemStorage;
                }
                else
                {
                    if (objClaim.Context.RMUser.objRiskmasterDatabase.DocPathType == 0)
                    {
                        objDocumentManager.DocumentStorageType = StorageType.FileSystemStorage;
                    }
                    else
                    {
                        objDocumentManager.DocumentStorageType = StorageType.DatabaseStorage;
                    }
                }

                if (objClaim.Context.RMUser.DocumentPath.Length > 0)
                {
                    objDocumentManager.DestinationStoragePath = objClaim.Context.RMUser.DocumentPath;
                }
                else
                {
                    objDocumentManager.DestinationStoragePath = objClaim.Context.RMUser.objRiskmasterDatabase.GlobalDocPath;
                }

                objDOM = new XmlDocument();
                objElemParent = objDOM.CreateElement("data");
                objDOM.AppendChild(objElemParent);

                objElemChild = objDOM.CreateElement("Document");

                objElemTemp = objDOM.CreateElement("DocumentId");
                objElemTemp.InnerText = "0";
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("FolderId");
                objElemTemp.InnerText = "0";
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("Notes");
                objElemTemp.InnerText = "";
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("Class");
                objElemTemp.InnerText = "0";
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("Category");
                objElemTemp.InnerText = "0";
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("Type");
                objElemTemp.InnerText = "0";
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("CreateDate");
                objElemTemp.InnerText = Riskmaster.Common.Conversion.ToDbDateTime(DateTime.Now);
                objElemChild.AppendChild(objElemTemp);


                objElemTemp = objDOM.CreateElement("Name");
                if (objClaim.LineOfBusCode == 243)
                {
                    objElemTemp.InnerText = "Auto FROI";
                }
                else
                {
                    objElemTemp.InnerText = "Auto ACORD";
                }
                objElemChild.AppendChild(objElemTemp);


                objElemTemp = objDOM.CreateElement("Subject");
                if (objClaim.LineOfBusCode == 243)
                {
                    objElemTemp.InnerText = "FROI Form";
                }
                else
                {
                    objElemTemp.InnerText = "ACORD Form";
                }
                p_sSubject = objElemTemp.InnerText;
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("UserLoginName");
                objElemTemp.InnerText = objClaim.Context.RMUser.LoginName;
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("FileName");
                if (objClaim.LineOfBusCode == 243)
                {
                    objElemTemp.InnerText = objClaim.ClaimNumber + "_FROIs.pdf";
                }
                else
                {
                    objElemTemp.InnerText = objClaim.ClaimNumber + "_ACORDs.pdf";
                }
                p_sFileName = objElemTemp.InnerText;
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("FilePath");
                objElemTemp.InnerText = "";
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("Keywords");
                objElemTemp.InnerText = "";
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("AttachTable");
                objElemTemp.InnerText = "CLAIM";
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("AttachRecordId");
                objElemTemp.InnerText = objClaim.ClaimId.ToString();
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("FormName");
                objElemTemp.InnerText = "Claim";
                objElemChild.AppendChild(objElemTemp);

                objDOM.FirstChild.AppendChild(objElemChild);

                if (objClaim.LineOfBusCode == 241)
                    iPSID = 150;
                if (objClaim.LineOfBusCode == 242)
                    iPSID = 6000;
                if (objClaim.LineOfBusCode == 243)
                    iPSID = 3000;

                objSettings = new SysSettings(objClaim.Context.DbConnLookup.ConnectionString,m_iClientId);
                if (objSettings.UseAcrosoftInterface == false)
                {
                    objDocumentManager.AddDocument(objDOM.InnerXml, objMemory, iPSID, out lDocumentId);
                } // if
                else
                {
                    binaryRdr = new BinaryReader(objMemory);
                    fileBuffer = binaryRdr.ReadBytes(Convert.ToInt32(objMemory.Length));
                    binaryRdr.Close();
                   
                    objDataModelFactory = new DataModelFactory(objClaim.Context.RMUser,m_iClientId);
                    //objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                    //objClaim.MoveTo(Conversion.ConvertStrToInteger(p_sRecordId));
                    string sClaimNumber = objClaim.ClaimNumber;
                    string sEventNumber = objClaim.EventNumber;
                    //rsolanki2 :  start updates for MCM mits 19200 
                    
                    Boolean bSuccess = false;
                    string sTempAcrosoftUserId = objClaim.Context.RMUser.LoginName;
                    string sTempAcrosoftPassword = objClaim.Context.RMUser.Password;

                    if (Conversion.CastToType<Boolean>(AcrosoftSection.UseCommonAcrosoftUser, out bSuccess))
                    {
                        if (AcrosoftSection.AcroUserMapSection.UserMap[objClaim.Context.RMUser.LoginName] != null
                            && !string.IsNullOrEmpty(AcrosoftSection.AcroUserMapSection.UserMap[objClaim.Context.RMUser.LoginName].RmxUser))
                        {
                            sTempAcrosoftUserId = AcrosoftSection.AcroUserMapSection.UserMap[objClaim.Context.RMUser.LoginName].AcrosoftUserId;
                            sTempAcrosoftPassword = AcrosoftSection.AcroUserMapSection.UserMap[objClaim.Context.RMUser.LoginName].AcrosoftPassword;
                        }
                        else
                        {
                            sTempAcrosoftUserId = AcrosoftSection.AcrosoftUsername;
                            sTempAcrosoftPassword = AcrosoftSection.AcrosoftUserPassword;
                        }
                    }

                    objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                        AcrosoftSection.AcrosoftAttachmentsTypeKey, sEventNumber, sClaimNumber,
                    AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);

                    iRetCd = objAcrosoft.StoreObjectBuffer(sTempAcrosoftUserId, sTempAcrosoftPassword,
                        p_sFileName, fileBuffer, p_sFileName, "0", sEventNumber, sClaimNumber, AcrosoftSection.AcrosoftAttachmentsTypeKey, "", "",
                        sTempAcrosoftUserId, out sAppExcpXml);
              
                   
                    //rsolanki2 :  end updates for MCM mits 19200 

                    //objAcrosoft.CreateAttachmentFolder(objClaim.Context.RMUser.LoginName, objClaim.Context.RMUser.Password, AcrosoftSection.AcrosoftAttachmentsTypeKey, sEventNumber, sClaimNumber,
                    //    AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                    //iRetCd = objAcrosoft.StoreObjectBuffer(objClaim.Context.RMUser.LoginName, objClaim.Context.RMUser.Password, p_sFileName, fileBuffer, p_sFileName, "0", sEventNumber, sClaimNumber, AcrosoftSection.AcrosoftAttachmentsTypeKey, "", "", objClaim.Context.RMUser.LoginName, out sAppExcpXml);
                    objClaim = null;
                    objAcrosoft = null;
                } // else
                return "Success";
            }
            catch (Exception ex)
            {
                FROIACORDERRLOG(objClaim, "Unable to Attach the FROI/ACORD form to Claim. But the Email has been sent out. The exact error is: " + ex.Message);
                return "Unable to Attach the FROI/ACORD form to Claim. The exact error is: " + ex.Message;                
            }
            finally
            {
                if (objDataModelFactory != null)
                {
                    objDataModelFactory.Dispose();
                }
                objDOM = null;
                objDocumentManager = null;
                objElemParent = null;
                objElemChild = null;
                objElemTemp = null;
                objSettings = null;
                if (objClaim != null)
                {
                    objClaim.Dispose();
                }
                if (objEvent != null)
                {
                    objEvent.Dispose();
                }
            }
        }

        /// <summary>
        /// Function to Log Errors in Auto FROI ACORD functionality
        ///IF its successful, same will be written into FROI_ACORD_ERR_LOG Table
        /// </summary>
        /// <param name="objClaim"></param>
        /// <param name="sReason"></param>

        public void FROIACORDERRLOG(Claim objClaim, string sReason)
        {
            string sSQL = "";
            //sbhatnagar21 - jira RMA-9183
            Dictionary<string, object> dictParams = null;
           
           
            Riskmaster.Db.DbCommand objCommand = null;
           
            try
            {
                // To avoid "String or binary data would be truncated error"
                if(sReason.Trim().Length > 198)
                    sReason = sReason.Trim().Substring(0,198);

                //sSQL = "INSERT INTO FROI_ACORD_ERR_LOG(CLAIM_ID,CLAIM_NUMBER,DTTM_RCD_ADDED,REASON) VALUES(";
                //sSQL = sSQL + objClaim.ClaimId + ",'" + objClaim.ClaimNumber + "'," + Riskmaster.Common.Conversion.ToDbDateTime(DateTime.Now) + "," + "'" + sReason.Replace("'", "''") + "')";
                //objClaim.Context.DbConnLookup.ExecuteString(sSQL);
                
      
                dictParams = new Dictionary<string, object>();
                objCommand = objClaim.Context.DbConn.CreateCommand();
                sSQL = String.Format(@"INSERT INTO FROI_ACORD_ERR_LOG (CLAIM_ID,CLAIM_NUMBER,DTTM_RCD_ADDED,REASON) VALUES({0},{1},{2},{3})", "~ClaimId~", "~ClmNumber~", "~DttmRcvd~", "~Rsn~");
                
                dictParams.Add("ClaimId", objClaim.ClaimId);
                dictParams.Add("ClmNumber", objClaim.ClaimNumber);
                dictParams.Add("DttmRcvd", Riskmaster.Common.Conversion.ToDbDateTime(DateTime.Now));
                dictParams.Add("Rsn", sReason.Replace("'", "''"));
                objCommand.CommandText = sSQL;
                DbFactory.ExecuteNonQuery(objCommand, dictParams);



            }
            finally
            {
                if (objClaim != null)
                {
                    objClaim.Dispose();
                }
                if (objCommand != null)
                {
                    objCommand = null;
                }
                if (dictParams != null)
                {
                    dictParams = null;
                }

            }


        }

        /// <summary>
        ///Function to Prepare Hard Code XML for Invoking 
        ///FROI and ACORD     
        /// </summary>
        /// <param name="iClaimId"></param>
        /// <param name="iFormId"></param>
        /// <returns></returns>

        private XmlDocument GetFROIXml(int iClaimId, string iFormId)
        {

            XmlDocument objXmlDocument = null;
            StringBuilder sbSql = null;

            objXmlDocument = new XmlDocument();
            sbSql = new StringBuilder();
            sbSql.Append("<FROI>");
            sbSql.Append("<ClaimId>" + iClaimId + "</ClaimId>");
            sbSql.Append("<FormId>" + iFormId + "</FormId>");
            sbSql.Append("<State></State>");
            sbSql.Append("<Name></Name>");
            sbSql.Append("<Title></Title>");
            sbSql.Append("<Phone></Phone>");
            sbSql.Append("<DocumentPath></DocumentPath>");
            sbSql.Append("<AttachForm></AttachForm>");
            sbSql.Append("<DocStorageType></DocStorageType>");
            sbSql.Append("<RequestHost></RequestHost>");
            sbSql.Append("</FROI>");
            objXmlDocument.LoadXml(sbSql.ToString());
            return objXmlDocument;
        }

        /// <summary>
        ///Function to Send Mail to Contacts from Org Hierarchy
        ///From Address in fetched from Table "EMAIL_FROI_ACORD"
        ///Because of this and Error tracking this function was made seperately
        /// </summary>
        /// <param name="sSubject"></param>
        /// <param name="sBody"></param>
        /// <param name="sToAddress"></param>
        /// <param name="sAttachmentPath"></param>
        /// <param name="sError"></param>
        /// <returns></returns>
        public bool sendMail(string sSubject, string sBody, string sToAddress, string sAttachmentPath, ref string sError)
        {
            MailMessage objMailMessage = null;         
            Attachment objAttachment = null;
            string m_sSecurityDSN = string.Empty, sSQL = string.Empty, sFromAddress = string.Empty;

            try
            {

                sSQL = "SELECT EMAILFROIACORDFROM FROM EMAIL_FROI_ACORD";
                string strEmailAcordFrom = DbFactory.ExecuteScalar(m_sConnectionString, sSQL) as string;
                if (!string.IsNullOrEmpty(strEmailAcordFrom))
                {
                    sFromAddress = strEmailAcordFrom;
                }

                sToAddress = validEmails(sToAddress);
                objMailMessage = new MailMessage(sFromAddress, sToAddress);
                objMailMessage.Subject = sSubject;
                objMailMessage.Body = sBody;             
                if (File.Exists(sAttachmentPath))
                {
                    objAttachment = new Attachment(sAttachmentPath);
                    objMailMessage.Attachments.Add(objAttachment);
                }
                Mailer.SendMail(objMailMessage, m_iClientId);
                return true;
            }
            catch (SmtpException ex)
            {
                sError = "Failed in Send Mail Function, Error Message: " + ex.Message;
                return false;
            }
            finally
            {
                if (objMailMessage != null)
                    objMailMessage.Dispose();
                if (objAttachment != null)
                    objAttachment.Dispose();
            }
        }


        //Function to Validate e-mail addresses seperated by comma
        public string validEmails(string strEmails)
        {
            try
            {
                string sValidAddresses;
                string[] strEmail = strEmails.Split(new char[] { ',' });
                bool blnReturn = true;
                sValidAddresses = "";
                foreach (string strMail in strEmail)
                {
                    blnReturn = validateEmailAddress(strMail);
                    if (blnReturn)
                    {
                        sValidAddresses = sValidAddresses + strMail + ",";
                    }
                }
                if (sValidAddresses != "")
                { sValidAddresses = sValidAddresses.Substring(0, sValidAddresses.Length - 1); }

                return sValidAddresses;
            }
            catch (Exception ex)
            {
                return "validEmails, Error Message is:" + ex.Message;
            }
        }
        
        /// <summary>
        /// Function to Validate Single e-mail address
        /// </summary>
        /// <param name="strEmailAdd"></param>
        /// <returns></returns>
        public bool validateEmailAddress(string strEmailAdd)
        {
            Regex objRegex = null;
            try
            {
                objRegex = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
                {
                    if (objRegex.IsMatch(strEmailAdd))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch
            {
                return false;
            }
            finally
            { objRegex = null; }
        }

      
        /// <summary>
        /// Merge FDF and PDF using Amyuni DLL
        /// </summary>
        /// <param name="sTemplateFile"></param>
        /// <param name="sFDFFile"></param>
        /// <param name="finalFile"></param>
        /// <returns></returns>
        public bool MergeFDFTOPDF(string sTemplateFile, string sFDFFile, string finalFile , ref string sError)
        {

            try
            {

                using (FileStream file1 = new FileStream(sTemplateFile, FileMode.Open, FileAccess.Read))
                using (FileStream file2 = new FileStream(sFDFFile, FileMode.Open, FileAccess.Read))
                using (FileStream file3 = new FileStream(finalFile, FileMode.Create, FileAccess.Write))               
                using (IacDocument doc2 = new IacDocument(null))
                using (IacDocument doc1 = new IacDocument(null))
                {
                    // Opend the pdf file we want to add watermarks to
                    doc1.Open(file1, "");

                    // Open the watermark pdf file
                    doc2.Open(file2, "");

                    // merge the two documents: doc2 is merged into doc1.
                    doc1.Merge(doc2, 1);  // 2nd arg: 1=merge doc2 to every page of doc1
                    //          2=merge doc2 to the first page of doc1

                    // save the result to a third file                    
                    doc1.Save(file3);
                    file3.Close();
                    file3.Dispose();
                    doc1.Dispose();

                    //01/19/10 Geeta 19341  : Split architecture issue  ACORD and FROI
                    if (!(sTemplateFile.Contains("pdf-forms")))
                    {
                        file1.Close();
                        file1.Dispose();
                        File.Delete(sTemplateFile);
                    }
 
                    return true;
                }
            }
            catch (Exception ex)
            {
                sError= "Failed, Error Message: " + ex.Message;
                return false;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Encrypt Merged PDF using Amyuni DLL
        /// </summary>
        /// <param name="sMergeFinalPDF"></param>
        /// <param name="sSecuredFile"></param>
        /// <param name="sPassword"></param>
        /// <returns></returns>
        public bool EncryptMergePDF(string sMergeFinalPDF, string sSecuredFile, string sPassword , ref string sError)
        {
            
            UInt32 permissions;
            IacDocument document = new IacDocument(null);
            try
            {
                using (FileStream MergedFile = new System.IO.FileStream(sMergeFinalPDF, FileMode.Open))
                {
                    using (FileStream encryptfile = new System.IO.FileStream(sSecuredFile, FileMode.Create))
                    {

                        permissions = 0xFFFFFFC0 + 0x4 + 0x8;
                                      
                        //Open the pdf document, and save the Encrypted file
                        document.Open(MergedFile, "");
                        document.Encrypt128(sPassword, sPassword, permissions);
                        document.Save(encryptfile, 0);

                        //Close and Dispose Encrypted File
                        encryptfile.Close();
                    }//using

                    //Close and Dispose Merged File
                    MergedFile.Close();
                }//using

                document.Dispose();

                return true ;
            }
            catch (Exception ex)
            {
                sError = "Failed, Error Message: " + ex.Message;
                return false;
            }
            finally
            {
                if (document != null)
                    document.Dispose();
            }
        }
        
        /// <summary>
        /// Function to Get the Binary format of a File 
        /// </summary>
        /// <param name="p_sPath"></param>
        /// <param name="p_objMemoryStream"></param>
        internal void GetFileStream(string p_sPath, ref MemoryStream p_objMemoryStream)
        {
            FileStream objFileStream = null;
            BinaryReader objBReader = null;
            Byte[] arrRet = null;
            try
            {
                objFileStream = new FileStream(p_sPath, FileMode.Open);
                objBReader = new BinaryReader(objFileStream);
                arrRet = objBReader.ReadBytes((int)objFileStream.Length);
                p_objMemoryStream = new MemoryStream(arrRet);
                objFileStream.Close();
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("GetFileStream.Error",m_iClientId), p_objEx);
            }
        }

        #region IDisposable Members

        /// <summary>
        /// Dispose method implemented as part of the IDisposable interface
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Overloaded Dispose method
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!isDisposed)
            {
                if (disposing)
                {
                    acPDFCreatorLib.Terminate();
                }//if
            }//if
            isDisposed = true;
        }//method: Dispose

        /// <summary>
        /// Class destructor
        /// </summary>
        ~AutoFroiAcordAdaptor()
        {
            Dispose(false);
        }
        #endregion
    }
}
