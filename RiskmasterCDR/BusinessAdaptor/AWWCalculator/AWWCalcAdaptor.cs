using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.AWWCalculator; 

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: AWWCalcAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 08/16/2005
	///* $Author	: Pankaj
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for AWW Calculator
	/// </summary>
	public class AWWCalcAdaptor :  BusinessAdaptorBase
	{
		#region Constructor
		/// <summary>
		/// Default Constructor
		/// </summary>
		public AWWCalcAdaptor(){}

		#endregion

		#region Public Methods

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.AWWCalculator.AWWCalc.Get() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.AWWCalculator.AWWCalc.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			AWWCalc objCalc=null; //Application layer component			
			try
			{
				objCalc = new AWWCalc(userLogin.LoginName,userLogin.Password,userLogin.objRiskmasterDatabase.DataSourceName,base.ClientId);
				p_objXmlOut=objCalc.Get(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException, Globalization.GetString("AWWClacAdaptor.Get.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objCalc = null;
			}
		}


		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.AWWCalculator.Save() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.AWWCalculator.AWWCalc.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			AWWCalc  objCalc = null; //Application layer component			
			try
			{
				objCalc = new AWWCalc(userLogin.LoginName,userLogin.Password,userLogin.objRiskmasterDatabase.DataSourceName,base.ClientId);
				p_objXmlOut=objCalc.Save(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException, Globalization.GetString("AWWClacAdaptor.Save.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objCalc = null;
			}
		}

		#endregion
	}
}