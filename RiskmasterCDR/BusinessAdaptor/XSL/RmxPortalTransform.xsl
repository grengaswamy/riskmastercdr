<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions">
<!--unique ID values at global level has not yet been implemented (though Unique ID values within Nodes has been implemented)-->
	<xsl:template match="/">
		<ul>			
			<xsl:for-each select="//SystemUserGroups/UserTypes/GroupName">
				<xsl:variable name="sGroupName" select="text()"/>
				<xsl:variable name="iPosition" select="position()"/>
				<xsl:if test="count(/form/control/listrow/rowtext[@name='User Type' and @title=$sGroupName])>0">
					<li populated="true"  type="group"><xsl:attribute name="id" select="$iPosition"/><xsl:value-of select="$sGroupName"/>
						<ul>
							<xsl:for-each select="//form/control/listrow/rowtext[@name='LoginName']/@title">
								<xsl:variable name="sLoginName" select="."/>							
								<xsl:variable name="iLoginIdPosition" select="position()"/>
								<xsl:variable name="sLoginGroupName" select="//form/control/listrow[$iLoginIdPosition]/rowtext[@name='User Type']/@title"/>
								<!--//form/control/listrow[iLoginIdPosition]/rowtext/@title='AllowALL-->
								<xsl:choose>
									<xsl:when test="$sLoginGroupName=$sGroupName">
										<li populated="true" class="select"  type="user"><xsl:attribute name="id" select="$iLoginIdPosition"/><xsl:value-of select="$sLoginName"/></li>
									</xsl:when>
									<xsl:otherwise>
										<!--<li/>-->
									</xsl:otherwise>
								</xsl:choose>							
							</xsl:for-each>
						</ul>
					</li>
				</xsl:if>
			</xsl:for-each>				
			<xsl:for-each select="//form/control/listrow[(rowtext/@name='User Type') and (rowtext/@title='')]/rowtext[@name='LoginName']/@title">
				<li populated="true" id="0" class="select" type="user">
					<xsl:attribute name="id"><xsl:value-of select="position()"/></xsl:attribute>
					<xsl:value-of select="."/>
				</li>	
			</xsl:for-each>
		</ul>
	</xsl:template>
</xsl:stylesheet>
