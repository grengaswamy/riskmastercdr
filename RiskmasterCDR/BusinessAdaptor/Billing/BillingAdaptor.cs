using System;
using System.Xml;
using System.IO;
using System.Collections;
using Riskmaster.Application.EnhancePolicy.Billing;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Security;

namespace Riskmaster.BusinessAdaptor
{
    ///************************************************************** 
    ///* $File		: BillingAdaptor.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 12-Jan-2007
    ///* $Author	: Nitesh Deedwania
    ///* $Comment	: 
    ///* $Source	: 
    ///**************************************************************
    /// <summary>	
    ///	This class is used to call the application layer component for BillingAdaptor.
    /// </summary>
    /// 

    public class BillingAdaptor : BusinessAdaptorBase
    {
        # region Constants

        private const string REFRESH = "Refresh";
        private const string SELECT = "Select";
        private const string CLOSE = "Close";
        private const string REVERSE = "Reverse";
        private const string ABORT = "Abort";

        private const int RMB_POLBILL_ENH = 1200500180;
        private const int RMB_POLBILL_ENH_EXPORTS = 1200500188;
        private const int RMB_POLBILL_ENH_RECEIPTS = 1200500189;
        private const int RMB_POLBILL_ENH_ADJUSTMENTS = 1200500190;
        private const int RMB_POLBILL_ENH_DISBURSEMENTS = 1200500191;
        private const int RMB_POLBILL_ENH_RECONCILIATION = 1200500192;
        private const int RMB_POLBILL_ENH_RECONCILIATION_RECONCILE = 1200500195;
        private const int RMB_POLBILL_ENH_PRINT_DISBURSEMENTS = 1200500196;
        private const int RMB_POLBILL_ENH_BATCHES = 1200500197;
        private const int RMB_POLBILL_ENH_BATCHES_CLOSE = 1200500200;
        private const int RMB_POLBILL_ENH_BATCHES_REVERSE = 1200500201;
        private const int RMB_POLBILL_ENH_BATCHES_ABORT = 1200500202;


        #endregion
        #region Constructor
        public BillingAdaptor()
		{

        }
        #endregion
        #region Public Functions
        public bool PrintDisbursement(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            BillingManager objBillingManager = null;
            string sSelectedBillingItemRowID = string.Empty;

            try
            {
                if (!userLogin.IsAllowedEx(RMB_POLBILL_ENH_PRINT_DISBURSEMENTS))
                    throw new PermissionViolationException(RMPermissions.RMO_BILLING_PRINT_DISBURSEMENTS, RMB_POLBILL_ENH_PRINT_DISBURSEMENTS);

                if (p_objXmlIn.SelectSingleNode("//SelectedBillingItem") != null)
                {
                    sSelectedBillingItemRowID = p_objXmlIn.SelectSingleNode("//SelectedBillingItem").InnerText;
                }

                using (objBillingManager = new BillingManager(userLogin.LoginName, userLogin.Password, userLogin.objRiskmasterDatabase.DataSourceName, base.ClientId))
                {
                    p_objXmlOut = objBillingManager.PrintDisbursement(sSelectedBillingItemRowID);
                }

            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {

                p_objErrOut.Add(p_objException, Globalization.GetString("BillingAdaptor.PrintDisbursement.Error",base.ClientId),
                    BusinessAdaptorErrorType.Error);
                p_objXmlOut = p_objXmlIn;
                return true;
            }
            finally
            {

                objBillingManager = null;

            }
            return true;
        }

        public bool PostPrintDisb(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            BillingManager objBillingManager = null;
            string sSelectedBillingItemRowID = string.Empty;

            try
            {
                if (!userLogin.IsAllowedEx(RMB_POLBILL_ENH_PRINT_DISBURSEMENTS))
                    throw new PermissionViolationException(RMPermissions.RMO_BILLING_PRINT_DISBURSEMENTS, RMB_POLBILL_ENH_PRINT_DISBURSEMENTS);

                using (objBillingManager = new BillingManager(userLogin, userLogin.LoginName, userLogin.Password, userLogin.objRiskmasterDatabase.DataSourceName, base.ClientId))//vkumar258 ML Changes
                {

                    p_objXmlOut = objBillingManager.PostPrintDisb(p_objXmlIn);
                }

                return true;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {

                p_objErrOut.Add(p_objException, Globalization.GetString("BillingAdaptor.PostPrintDisb.Error", base.ClientId),
                    BusinessAdaptorErrorType.Error);
                p_objXmlOut = p_objXmlIn;
                return false;
            }
            finally
            {

                objBillingManager = null;

            }
            
        }

        public bool GetReconciliationDetails(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {

            string[] strarrBillItemsRowId = { "" };
            BillingManager objBillingManager = null;
            
            XmlNode objSelectedRodIDsNode = null; 
            string sSelectedBillingItemRowID = string.Empty;
            string sbatchTotal = string.Empty;
            //Changed for MITS 9775 by Gagan : Start 
            bool bIsCheckPaidInFull = false;
            //Changed for MITS 9775 by Gagan : End

            try
            {
                if (!userLogin.IsAllowedEx(RMB_POLBILL_ENH_RECONCILIATION))
                    throw new PermissionViolationException(RMPermissions.RMO_BILLING_RECOCNILE, RMB_POLBILL_ENH_RECONCILIATION);
                
                objSelectedRodIDsNode = p_objXmlIn.SelectSingleNode("//sSelectedBillingItems");
                sSelectedBillingItemRowID = objSelectedRodIDsNode.InnerText;
                strarrBillItemsRowId = sSelectedBillingItemRowID.Split('_');

                using (objBillingManager = new BillingManager(userLogin.LoginName, userLogin.Password, userLogin.objRiskmasterDatabase.DataSourceName, base.ClientId))
                {
                    //Changed for MITS 9775 by Gagan : Start 
                    sbatchTotal = objBillingManager.GetReconciliationDetails(strarrBillItemsRowId, ref bIsCheckPaidInFull);
                }

                p_objXmlIn.SelectSingleNode("//CheckPaidInFull").InnerText = bIsCheckPaidInFull.ToString();
                p_objXmlIn.SelectSingleNode("//BatchTotal").InnerText = sbatchTotal;

                p_objXmlOut = p_objXmlIn;

                //objRootElement = p_objXmlOut.CreateElement("BatchTotal");
                //objRootElement.InnerText = sbatchTotal;
                //p_objXmlOut.AppendChild(objRootElement);

                //Changed for MITS 9775 by Gagan : End
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				p_objXmlOut=p_objXmlIn;
				return false;
			}
			catch(Exception p_objException)
			{

                p_objErrOut.Add(p_objException, Globalization.GetString("ToDo", base.ClientId),
					BusinessAdaptorErrorType.Error);
				p_objXmlOut=p_objXmlIn;
				return true;
			}
            finally
            {

                strarrBillItemsRowId = null;
                objBillingManager = null;

            }
             return true;
		}

        public bool GetReceiptData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            ReceiptManager objReceiptManager=null;
            XmlNode objXmlNode = null;
            int iPolicyId = 0;
            int iTermNumber = 0;
            string sPolicyNumber = string.Empty;
            int iBillItemRowId = 0;
            try
            {
                if (!userLogin.IsAllowedEx(RMB_POLBILL_ENH_RECEIPTS))
                    throw new PermissionViolationException(RMPermissions.RMO_BILLING_RECEIPTS, RMB_POLBILL_ENH_RECEIPTS);

                objXmlNode = p_objXmlIn.SelectSingleNode("//PolicyId");
                if (objXmlNode != null)
                    iPolicyId = Conversion.ConvertStrToInteger(objXmlNode.InnerText);

                objXmlNode = p_objXmlIn.SelectSingleNode("//TermNumber");
                if (objXmlNode != null)
                    iTermNumber = Conversion.ConvertStrToInteger(objXmlNode.InnerText);

                objXmlNode = p_objXmlIn.SelectSingleNode("//PolicyNumber");
                if (objXmlNode != null)
                    sPolicyNumber = objXmlNode.InnerText;
                objXmlNode = p_objXmlIn.SelectSingleNode("//BillingItemRowId");
                if (objXmlNode != null)
                    iBillItemRowId = Conversion.ConvertStrToInteger(objXmlNode.InnerText);

                using (objReceiptManager = new ReceiptManager(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, base.ClientId))
                {
                    if (iBillItemRowId == 0)
                        p_objXmlOut = objReceiptManager.GetNewReceiptData(iPolicyId, iTermNumber, sPolicyNumber);
                    else
                        p_objXmlOut = objReceiptManager.GetReceiptData(iBillItemRowId);
                }

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                p_objXmlOut = p_objXmlIn;
                return true;
            }
            catch (Exception p_objException)
            {

                p_objErrOut.Add(p_objException, Globalization.GetString("ToDo", base.ClientId),
                    BusinessAdaptorErrorType.Error);
                p_objXmlOut = p_objXmlIn;
                return true;
            }
            finally
            {
                objReceiptManager = null;
            }
             return true;
		}

        public bool SaveReceipt(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            ReceiptManager objReceiptManager=null;
            XmlNode objXmlNode = null;
            int iBillingItemRowId=0;
            try
            {
                if (!userLogin.IsAllowedEx(RMB_POLBILL_ENH_RECEIPTS))
                    throw new PermissionViolationException(RMPermissions.RMO_BILLING_RECEIPTS, RMB_POLBILL_ENH_RECEIPTS);

                objXmlNode = p_objXmlIn.SelectSingleNode("//BillingItemRowId");
                if (objXmlNode != null)
                    iBillingItemRowId = Conversion.ConvertStrToInteger(objXmlNode.InnerText);

                using (objReceiptManager = new ReceiptManager(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, base.ClientId))
                {
                    objReceiptManager.SaveReceipt(p_objXmlIn, iBillingItemRowId);
                }

                objXmlNode = p_objXmlIn.SelectSingleNode("//SaveMessage");
                if (objXmlNode != null)
                    objXmlNode.InnerText = "Saved";

                p_objXmlOut = p_objXmlIn;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                p_objXmlOut = p_objXmlIn;
                return true;
            }
            catch (Exception p_objException)
            {

                p_objErrOut.Add(p_objException, Globalization.GetString("ToDo", base.ClientId),
                    BusinessAdaptorErrorType.Error);
                p_objXmlOut = p_objXmlIn;
                return true;
            }
            finally
            {
                objReceiptManager = null;
                objXmlNode = null;
            }
            return true;
        }

        public bool GetAdjustmentData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            AdjustmentManager objAdjustmentManager = null;
            XmlNode objXmlNode = null;
            int iPolicyId = 0;
            int iTermNumber = 0;
            string sPolicyNumber = string.Empty;
            int iBillItemRowId = 0;
            try
            {
                if (!userLogin.IsAllowedEx(RMB_POLBILL_ENH_ADJUSTMENTS))
                    throw new PermissionViolationException(RMPermissions.RMO_BILLING_ADJUSTMENTS, RMB_POLBILL_ENH_ADJUSTMENTS);
                objXmlNode = p_objXmlIn.SelectSingleNode("//PolicyId");
                if (objXmlNode != null)
                    iPolicyId = Conversion.ConvertStrToInteger(objXmlNode.InnerText);

                objXmlNode = p_objXmlIn.SelectSingleNode("//TermNumber");
                if (objXmlNode != null)
                    iTermNumber = Conversion.ConvertStrToInteger(objXmlNode.InnerText);

                objXmlNode = p_objXmlIn.SelectSingleNode("//PolicyNumber");
                if (objXmlNode != null)
                    sPolicyNumber = objXmlNode.InnerText;
                objXmlNode = p_objXmlIn.SelectSingleNode("//BillingItemRowId");
                if (objXmlNode != null)
                    iBillItemRowId = Conversion.ConvertStrToInteger(objXmlNode.InnerText);

                using (objAdjustmentManager = new AdjustmentManager(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password,base.ClientId))
                {
                    if (iBillItemRowId == 0)
                        p_objXmlOut = objAdjustmentManager.GetNewAdjustmentData(iPolicyId, iTermNumber, sPolicyNumber);
                    else
                        p_objXmlOut = objAdjustmentManager.GetAdjustmentData(iBillItemRowId);
                }

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                p_objXmlOut = p_objXmlIn;
                return true;
            }
            catch (Exception p_objException)
            {

                p_objErrOut.Add(p_objException, Globalization.GetString("ToDo", base.ClientId),
                    BusinessAdaptorErrorType.Error);
                p_objXmlOut = p_objXmlIn;
                return true;
            }
            finally
            {
                objAdjustmentManager = null;
            }
           return true;
        }

        public bool SaveAdjustment(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            AdjustmentManager objAdjustmentManager = null;
            XmlNode objXmlNode = null;
            int iBillingItemRowId = 0;
            try
            {
                if (!userLogin.IsAllowedEx(RMB_POLBILL_ENH_ADJUSTMENTS))
                    throw new PermissionViolationException(RMPermissions.RMO_BILLING_ADJUSTMENTS, RMB_POLBILL_ENH_ADJUSTMENTS);

                objXmlNode = p_objXmlIn.SelectSingleNode("//BillingItemRowId");
                if (objXmlNode != null)
                    iBillingItemRowId = Conversion.ConvertStrToInteger(objXmlNode.InnerText);

                using (objAdjustmentManager = new AdjustmentManager(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, base.ClientId))
                {
                    objAdjustmentManager.SaveAdjustment(p_objXmlIn, iBillingItemRowId);
                }
                objXmlNode = p_objXmlIn.SelectSingleNode("//SaveMessage");
                if (objXmlNode != null)
                    objXmlNode.InnerText = "Saved";
                p_objXmlOut = p_objXmlIn;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                p_objXmlOut = p_objXmlIn;
                return true;
            }
            catch (Exception p_objException)
            {

                p_objErrOut.Add(p_objException, Globalization.GetString("ToDo", base.ClientId),
                    BusinessAdaptorErrorType.Error);
                p_objXmlOut = p_objXmlIn;
                return true;
            }
            finally
            {
                objAdjustmentManager = null;
                objXmlNode = null;
            }
            return true;
        }

        public bool GetDisbursementData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            DisbursementManager objDisbursementManager = null;
            XmlNode objXmlNode = null;
            int iPolicyId = 0;
            int iTermNumber = 0;
            string sPolicyNumber = string.Empty;
            int iBillItemRowId = 0;
            try
            {
                if (!userLogin.IsAllowedEx(RMB_POLBILL_ENH_DISBURSEMENTS))
                    throw new PermissionViolationException(RMPermissions.RMO_BILLING_DISBURSEMENTS, RMB_POLBILL_ENH_DISBURSEMENTS);


                objXmlNode = p_objXmlIn.SelectSingleNode("//PolicyId");
                if (objXmlNode != null)
                    iPolicyId = Conversion.ConvertStrToInteger(objXmlNode.InnerText);

                objXmlNode = p_objXmlIn.SelectSingleNode("//TermNumber");
                if (objXmlNode != null)
                    iTermNumber = Conversion.ConvertStrToInteger(objXmlNode.InnerText);

                objXmlNode = p_objXmlIn.SelectSingleNode("//PolicyNumber");
                if (objXmlNode != null)
                    sPolicyNumber = objXmlNode.InnerText;
                objXmlNode = p_objXmlIn.SelectSingleNode("//BillingItemRowId");
                if (objXmlNode != null)
                    iBillItemRowId = Conversion.ConvertStrToInteger(objXmlNode.InnerText);

                using (objDisbursementManager = new DisbursementManager(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password,base.ClientId))
                {
                    if (iBillItemRowId == 0)
                        p_objXmlOut = objDisbursementManager.GetNewDisbursementData(iPolicyId, iTermNumber, sPolicyNumber);
                    else
                        p_objXmlOut = objDisbursementManager.GetDisbursementData(iBillItemRowId);
                }

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                p_objXmlOut = p_objXmlIn;
                return true;
            }
            catch (Exception p_objException)
            {

                p_objErrOut.Add(p_objException, Globalization.GetString("ToDo", base.ClientId),
                    BusinessAdaptorErrorType.Error);
                p_objXmlOut = p_objXmlIn;
                return true;
            }
            finally
            {
                if(objDisbursementManager != null)
                {
                    objDisbursementManager = null;
                }
            }

            return true;
        }

        public bool SaveDisbursement(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            DisbursementManager objDisbursementManager = null;
            XmlNode objXmlNode = null;
            int iBillingItemRowId = 0;
            try
            {
                if (!userLogin.IsAllowedEx(RMB_POLBILL_ENH_DISBURSEMENTS))
                    throw new PermissionViolationException(RMPermissions.RMO_BILLING_DISBURSEMENTS, RMB_POLBILL_ENH_DISBURSEMENTS);

                objXmlNode = p_objXmlIn.SelectSingleNode("//BillingItemRowId");
                if (objXmlNode != null)
                    iBillingItemRowId = Conversion.ConvertStrToInteger(objXmlNode.InnerText);

                using (objDisbursementManager = new DisbursementManager(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password,base.ClientId))
                {
                    objDisbursementManager.SaveDisbursement(p_objXmlIn, iBillingItemRowId);
                }
                objXmlNode = p_objXmlIn.SelectSingleNode("//SaveMessage");
                if (objXmlNode != null)
                    objXmlNode.InnerText = "Saved";

                p_objXmlOut = p_objXmlIn;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                p_objXmlOut = p_objXmlIn;
                return true;
            }
            catch (Exception p_objException)
            {

                p_objErrOut.Add(p_objException, Globalization.GetString("ToDo", base.ClientId),
                    BusinessAdaptorErrorType.Error);
                p_objXmlOut = p_objXmlIn;
                return true;
            }
            finally
            {
                objDisbursementManager = null;
                objXmlNode = null;
            }
            return true;
        }

        public bool GetPremiumItemData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            PremiumItemManager objPremiumItemManager = null;
            XmlNode objXmlNode = null;
            int iPolicyId = 0;
            int iTermNumber = 0;
            string sPolicyNumber = string.Empty;
            int iBillItemRowId = 0;
            try
            {
                objXmlNode = p_objXmlIn.SelectSingleNode("//PolicyId");
                if (objXmlNode != null)
                    iPolicyId = Conversion.ConvertStrToInteger(objXmlNode.InnerText);

                objXmlNode = p_objXmlIn.SelectSingleNode("//TermNumber");
                if (objXmlNode != null)
                    iTermNumber = Conversion.ConvertStrToInteger(objXmlNode.InnerText);

                objXmlNode = p_objXmlIn.SelectSingleNode("//PolicyNumber");
                if (objXmlNode != null)
                    sPolicyNumber = objXmlNode.InnerText;
                objXmlNode = p_objXmlIn.SelectSingleNode("//BillingItemRowId");
                if (objXmlNode != null)
                    iBillItemRowId = Conversion.ConvertStrToInteger(objXmlNode.InnerText);

                using (objPremiumItemManager = new PremiumItemManager(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password,base.ClientId))
                {
                    if (iBillItemRowId != 0)
                        p_objXmlOut = objPremiumItemManager.GetPremiumItemData(iBillItemRowId);
                    else
                        throw new RMAppException("TODO");
                }
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                p_objXmlOut = p_objXmlIn;
                return true;
            }
            catch (Exception p_objException)
            {

                p_objErrOut.Add(p_objException, Globalization.GetString("ToDo", base.ClientId),
                    BusinessAdaptorErrorType.Error);
                p_objXmlOut = p_objXmlIn;
                return true;
            }
            finally
            {
                objPremiumItemManager = null;
            }

            return true;
        }

        /// <summary>
        /// This method is a wrapper to the Onload, OnCloseBatch, OnReverseBatch
        /// and OnAbortBatch method in Riskmaster.Application.EnhancePolicy.Billing
        /// Performs action specified in the request XML
        /// and refreshes the screen
        /// </summary>                
        /// <param name="p_objXmlIn">Request XML</param>
        /// <param name="p_objXmlOut">Response XML</param>
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>
        /// <returns>True/False for Success/Failure of the method</returns>
        public bool PerformAction(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            BatchManager objBatch = null;
            XmlDocument p_objXmlBatches = null;

            string sAction = String.Empty;
            int iBatchRowId = 0;
            int iReconciliationType = 0;
            bool bCancelPayPlan = false;

            try
            {
                //Action to ber performed on the selected batch
                sAction = p_objXmlIn.SelectSingleNode("//Filter/Action").InnerText;

                if (p_objXmlIn.SelectSingleNode("//Filter/SelectedBatch").InnerText != "")
                    iBatchRowId = int.Parse(p_objXmlIn.SelectSingleNode("//BatchId").InnerText);

                if (sAction == SELECT || sAction == ABORT || sAction == CLOSE || sAction == REVERSE)
                {
                    if (p_objXmlIn.SelectSingleNode("//BatchId").InnerText == "" || iBatchRowId == 0)
                        throw new RMAppException("No batch selected");
                }

                if (p_objXmlIn.SelectSingleNode("//Filter/ReconcileType").InnerText != "")
                    iReconciliationType = int.Parse(p_objXmlIn.SelectSingleNode("//Filter/ReconcileType").InnerText);

                // Create instance of BatchManager
                using (objBatch = new BatchManager(userLogin.objRiskmasterDatabase.DataSourceName,
                                            userLogin.LoginName, userLogin.Password,base.ClientId))
                {
                    //Invoke method based on Action
                    switch (sAction)
                    {
                        //MGaba2:MITS 22611: No Message was coming when view permission is revoked from SMS
                        //Corrected the offset
                        case REFRESH: if (!userLogin.IsAllowedEx(RMB_POLBILL_ENH_BATCHES, RMPermissions.RMO_VIEW))
                                throw new PermissionViolationException(RMPermissions.RMO_VIEW,RMB_POLBILL_ENH_BATCHES);
                            //if (!userLogin.IsAllowedEx(RMB_POLBILL_ENH_BATCHES))
                                //throw new PermissionViolationException(RMPermissions.RMO_ACCESS, RMB_POLBILL_ENH_BATCHES);
                            p_objXmlBatches = objBatch.OnLoad(p_objXmlIn.SelectSingleNode("//Filter"));
                            break;
                        case CLOSE: if (!userLogin.IsAllowedEx(RMB_POLBILL_ENH_BATCHES_CLOSE))
                            {
                                //MGaba2:MITS 22611: No Message was coming when permission is revoked from SMS
                                p_objXmlOut = objBatch.OnLoad(p_objXmlIn.SelectSingleNode("//Filter"));                               
                                throw new PermissionViolationException(RMPermissions.RMO_BATCHES_CLOSE, RMB_POLBILL_ENH_BATCHES_CLOSE);
                            }
                            if (p_objXmlIn.SelectSingleNode("//CancelPayPlan").InnerText == "")
                                p_objXmlIn.SelectSingleNode("//CancelPayPlan").InnerText = "false";

                            bCancelPayPlan = bool.Parse(p_objXmlIn.SelectSingleNode("//CancelPayPlan").InnerText);
                            objBatch.OnCloseBatch(iBatchRowId, iReconciliationType, bCancelPayPlan);
                            p_objXmlBatches = objBatch.OnLoad(p_objXmlIn.SelectSingleNode("//Filter"));
                            break;
                        case REVERSE: if (!userLogin.IsAllowedEx(RMB_POLBILL_ENH_BATCHES_REVERSE))
                            {
                                //MGaba2:MITS 22611: No Message was coming when permission is revoked from SMS
                                p_objXmlOut = objBatch.OnLoad(p_objXmlIn.SelectSingleNode("//Filter"));  
                                throw new PermissionViolationException(RMPermissions.RMO_BATCHES_REVERSE, RMB_POLBILL_ENH_BATCHES_REVERSE);
                            }
                            objBatch.OnReverseBatch(iBatchRowId);
                            p_objXmlBatches = objBatch.OnLoad(p_objXmlIn.SelectSingleNode("//Filter"));
                            break;
                        case ABORT: if (!userLogin.IsAllowedEx(RMB_POLBILL_ENH_BATCHES_ABORT))
                            {
                                //MGaba2:MITS 22611: No Message was coming when permission is revoked from SMS   
                                p_objXmlOut = objBatch.OnLoad(p_objXmlIn.SelectSingleNode("//Filter"));  
                                throw new PermissionViolationException(RMPermissions.RMO_BATCHES_ABORT, RMB_POLBILL_ENH_BATCHES_ABORT);
                            }
                            objBatch.OnAbortBatch(iBatchRowId);
                            p_objXmlBatches = objBatch.OnLoad(p_objXmlIn.SelectSingleNode("//Filter"));
                            break;
                        case SELECT: if (!userLogin.IsAllowedEx(RMB_POLBILL_ENH_BATCHES))
                                throw new PermissionViolationException(RMPermissions.RMO_ACCESS, RMB_POLBILL_ENH_BATCHES);
                            p_objXmlBatches = objBatch.OnSelect(p_objXmlIn.SelectSingleNode("//Filter"));
                            break;
                        default:
                            //MGaba2:MITS 22611: No Message was coming when view permission is revoked from SMS
                            if (!userLogin.IsAllowedEx(RMB_POLBILL_ENH_BATCHES, RMPermissions.RMO_VIEW))
                                throw new PermissionViolationException(RMPermissions.RMO_VIEW,RMB_POLBILL_ENH_BATCHES);                              
                                //if (!userLogin.IsAllowedEx(RMB_POLBILL_ENH_BATCHES))                                
                                    //throw new PermissionViolationException(RMPermissions.RMO_ACCESS, RMB_POLBILL_ENH_BATCHES);
                            p_objXmlBatches = objBatch.OnLoad(p_objXmlIn.SelectSingleNode("//Filter"));
                            break;
                    }

                }
                p_objXmlOut = p_objXmlBatches;


                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return true;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("BillingAdaptor.PerformAction.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return true;
            }
            finally
            {
                if (objBatch != null)
                {
                    objBatch = null;
                }
            }
        }
        
        /// <summary>
        /// This method is a wrapper to the IsCheckPaidInFull
        /// method in Riskmaster.Application.EnhancePolicy.Billing         
        /// </summary>                
        /// <param name="p_objXmlIn">Request XML</param>
        /// <param name="p_objXmlOut">Response XML</param>
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>
        /// <returns>True/False for Success/Failure of the method</returns>
        public bool GetBatchReconciliationDetails(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            BatchManager objBatch = null;
            int iBatchRowId = 0;
            bool bIsCheckPaidInFull;

            try
            {                
                if (p_objXmlIn.SelectSingleNode("//BatchId").InnerText != "")
                    iBatchRowId = int.Parse(p_objXmlIn.SelectSingleNode("//BatchId").InnerText);

                if (p_objXmlIn.SelectSingleNode("//BatchId").InnerText == "" || iBatchRowId == 0)
                    throw new RMAppException("No batch selected");

                // Create instance of BatchManager
                objBatch = new BatchManager(userLogin.objRiskmasterDatabase.DataSourceName,
                                            userLogin.LoginName, userLogin.Password, base.ClientId);

                //Checks whether premium has been paid in full
                bIsCheckPaidInFull = objBatch.IsCheckPaidInFull(iBatchRowId);

                p_objXmlIn.SelectSingleNode("//CheckPaidInFull").InnerText =
                                            bIsCheckPaidInFull.ToString();

                p_objXmlOut = p_objXmlIn;

                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return true;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("BillingAdaptor.GetReconciliationDetails.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return true;
            }
            finally
            {
                if (objBatch != null)
                {
                    objBatch.Dispose();
                    objBatch = null;
                }
            }
        }



        /// <summary>                       
        /// Generates a single batch pdf for invoices
        /// </summary>                
        /// <param name="p_objXmlIn">Request XML</param>
        /// <param name="p_objXmlOut">Response XML</param>
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>
        /// <returns>True/False for Success/Failure of the method</returns>
        public bool PrintInvoice(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            InvoiceManager objInvoiceManager = null;

            try
            {
                using (objInvoiceManager = new InvoiceManager(base.userLogin.objRiskmasterDatabase.DataSourceName,
                                                       base.userLogin.LoginName, base.userLogin.Password,base.ClientId))
                {
                    objInvoiceManager.PrintInvoice();
                }
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return true;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("BillingAdaptor.PrintInvoice.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return true;
            }
            finally
            {
                if (objInvoiceManager != null)
                {
                    objInvoiceManager = null;
                }
            }
        }



        /// <summary>                       
        /// Generates a single batch pdf for notices
        /// </summary>                
        /// <param name="p_objXmlIn">Request XML</param>
        /// <param name="p_objXmlOut">Response XML</param>
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>
        /// <returns>True/False for Success/Failure of the method</returns>
        public bool PrintNotice(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            NoticeManager objNoticeManager = null;

            try
            {
                using (objNoticeManager = new NoticeManager(base.userLogin.objRiskmasterDatabase.DataSourceName,
                                                       base.userLogin.LoginName, base.userLogin.Password,base.ClientId))
                {
                    objNoticeManager.PrintNotice();
                }

                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return true;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("BillingAdaptor.PrintNotice.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return true;
            }
            finally
            {
                if (objNoticeManager != null)
                {
                    objNoticeManager = null;
                }
            }
        }




        
        /// <summary>                       
        /// Generates xml for invoices and notices awaiting printing
        /// </summary>                
        /// <param name="p_objXmlIn">Request XML</param>
        /// <param name="p_objXmlOut">Response XML</param>
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>
        /// <returns>True/False for Success/Failure of the method</returns>         
        public bool GetInvoicesandNoticesList(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            PrintScheduler objPrintScheduler = null;

            try
            {
                //Changed by Gagan for MITS 18499 : start
                objPrintScheduler = new PrintScheduler(base.userLogin.objRiskmasterDatabase.DataSourceName,base.ClientId);
                //Changed by Gagan for MITS 18499 : end
                p_objXmlOut = objPrintScheduler.GetInvoicesandNoticesList();                

                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return true;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("BillingAdaptor.GetInvoicesandNoticesList.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return true;
            }
            finally
            {
                if (objPrintScheduler != null)
                {
                    objPrintScheduler = null;
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool GetInvoicesNoticesPDF(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            XmlElement objXmlFileName = null;
            XmlElement objXmlElement = null;
            string sPdfFileName = String.Empty;
            string sPDFFilePath = String.Empty;
            MemoryStream objMemoryStream = null;

            try
            {
                objXmlFileName = (XmlElement)p_objXmlIn.SelectSingleNode("//FileName");
                sPdfFileName = objXmlFileName.InnerText;
                sPDFFilePath = Path.Combine(Function.GetSavePath(), sPdfFileName);

                //p_objCopyRightData.SetAttribute("CopyRightProp", m_MessageSettings["DEF_RMPROPRIETARY"].ToString());


                objMemoryStream = Function.CreateStream(sPDFFilePath,base.ClientId);

                objXmlElement = p_objXmlOut.CreateElement("BatchScheduler");
                p_objXmlOut.AppendChild(objXmlElement);

                objXmlElement = p_objXmlOut.CreateElement("File");
                objXmlElement.InnerText = Convert.ToBase64String(objMemoryStream.ToArray());
                p_objXmlOut.FirstChild.AppendChild(objXmlElement);

                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return true;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("BillingAdaptor.GetInvoicesNoticesPDF.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return true;
            }
            finally
            {
                objXmlFileName = null;
                objXmlElement = null;
                if (objMemoryStream != null)
                {
                    objMemoryStream.Dispose();
                }
            }
        }



        public bool RemoveFileFromDisk(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            XmlElement objXmlFileName = null;
            XmlElement objXmlElement = null;
            string sPdfFileName = String.Empty;
            string sPDFFilePath = String.Empty;

            try
            {
                objXmlFileName = (XmlElement)p_objXmlIn.SelectSingleNode("//FileName");
                sPdfFileName = objXmlFileName.InnerText;
                sPDFFilePath = Path.Combine(Function.GetSavePath(), sPdfFileName);

                File.Delete(sPDFFilePath);

                objXmlElement = p_objXmlOut.CreateElement("BillingScheduler");
                p_objXmlOut.AppendChild(objXmlElement); 

                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return true;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("BillingAdaptor.RemoveFileFromDisk.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return true;
            }
            finally
            {
                objXmlFileName = null;
                objXmlElement = null;
            }
        }







        

        #endregion
    }
}
