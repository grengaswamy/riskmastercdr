using System;
using Riskmaster.ExceptionTypes;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;

using System.Collections;
using Riskmaster.Application.AsyncManagement ;


namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for AsyncManagementAdaptor.
	/// </summary>
	public class AsyncManagementAdaptor: BusinessAdaptorBase
	{
		public AsyncManagementAdaptor()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		public bool InitAndPollForResults(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{   
			try
			{
				AsyncHandler objHandle =new AsyncHandler(base.ClientId);
				objHandle.InitAndPollForResults(ref p_objDocIn);
				p_objXmlOut = p_objDocIn;
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("AsyncManagementAdaptor.InitAndPollForResults.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;

			}
			finally
			{
			}
		}
	}
}
