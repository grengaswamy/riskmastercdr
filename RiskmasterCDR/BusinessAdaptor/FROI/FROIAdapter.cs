﻿
using System;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Settings;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using System.IO; 
using Riskmaster.Db; 
using Riskmaster.Application.AcrosoftWebserviceWrapper;
using Riskmaster.Application.SupportScreens;
using System.Net;
using Riskmaster.Application.MediaViewWrapper;
using Riskmaster.Application.FROINet6;
using RMConfig = Riskmaster.Common.RMConfigurator;
using Riskmaster.Application.RMUtilities;
using FROIPrepareInfo= Riskmaster.Application.FROINet6.PreparerInfo;


namespace Riskmaster.BusinessAdaptor

{   
	///************************************************************** 
	///* $File		: FROIAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 21-Feb-2005
	///* $Author	: Tanuj Narula
	///* $Comment	: 
	///* $Source	: 
	///************************************************************** 
	/// <summary>
	/// This is an interop class acting as a wrapper over FROI VB component.
	/// </summary>
	public class FROIAdaptor:BusinessAdaptorBase
	{

        private string m_strPDFTemplatePath = string.Empty, m_strPDFOutputPath = string.Empty;
        private string m_strPDFUrlPath = string.Empty;
        public enum FROIFails : int 
        {//MITS 19613            
            FF_NCCIBodyParts = 2,
            FF_NCCICauseCode = 4,
            FF_NCCIClassCode = 8,
            FF_NCCIIllness = 16,
            FF_NoDepartmentAssigned = 32,
            FF_NoClaimant = 64,
            FF_LoadFROIOptionsFailed = 128,
            FF_NonNumericClassCode = 256
        };

		#region Constructor
		/// <summary>
		/// This is the default constructor.
		/// </summary>
		public FROIAdaptor()
		{
            //Initiate all Acrosoft settings
            //TODO: Determine if this instantiation needs to be present in multiple classes
            //TODO: or if it can be instantiated and managed in a central location
            RMConfigurationManager.GetAcrosoftSettings();

            //set the value for the PDF Template Path
            m_strPDFTemplatePath = RMConfigurator.CombineFilePath(RMConfigurator.AppFilesPath, @"pdf-forms\");
            m_strPDFOutputPath = RMConfigurator.CombineFilePath(RMConfigurator.UserDataPath, @"froi");
            //rkaur27 : As FROI settings will be same for all the clients in cloud environment,
            // so they are fetched from appsetting.config and not from DB.
            m_strPDFUrlPath = RMConfigurationManager.GetNameValueSectionSettings("FROI",string.Empty, 0)["FROI_PdfUrlPath"];

		}
		#endregion

		#region Public functions
		/// Name		: GetFROIForms
		/// Author		: Tanuj Narula
		/// Date Created: 23-Feb-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function will return FROI forms.
		/// </summary>
		/// <param name="p_objXmlIn">
		/// Structure of the input Xml.
		/// <Document>
		///   <FROI>
		///    <ClaimId>
		///    </ClaimId>
		///    <FormId>
		///    </FormId>
		///    <State>
		///    </State>
		///    <DocumentPath>
		///    </DocumentPath>
		///    <AttachForm>
		///    </AttachForm>
		///    <DocStorageType>
		///    </DocStorageType>
		///   </FROI>
		/// </Document>
		/// </param>
		/// <param name="p_objXmlOut">
		/// Structure of the output Xml.
		/// <FROIForms>
		///   <FORM FormId="" FormName="">
		///   </FORM>
		///   <FORM FormId="" FormName="">
		///   </FORM>
		///   <FORM FormId="" FormName="">
		///   </FORM>
		///   <ClaimId/>
		///   <AttachForm/>
		/// </FROIForms>
		/// </param>
		/// <param name="p_objErrOut">This will contain the errors occured during the processing of this function.</param>
		/// <returns>'True' if function call succeeds or 'false' if it fails.</returns>
		public bool GetFROIForms(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{

			FROIForms objFroi=null;
			System.Array arrForms=null;
			int iUpperbound=0;
			int iCtr=0;
			XmlElement objChild=null;
			XmlElement objRoot=null;
            //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
            SysSettings objSettings = null;
            //Animesh Insertion Ends

           
			try
			{
                //Comment by kuladeep for maintain Back button functionality:Start
                //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                //objSettings = new SysSettings(connectionString);
                //if (objSettings.UsePaperVisionInterface)
                    //throw new RMAppException(Globalization.GetString("FROIAdaptor.PaperVision.Error"));
                //Animesh Insertion Ends
                //Comment by kuladeep for maintain Back button functionality:End
               p_objXmlOut = new XmlDocument();
               p_objErrOut = new BusinessAdaptorErrors(base.ClientId);
               objFroi=InitializeFROI();

               arrForms = (System.Array)objFroi.GetFROIForms(Conversion.ConvertObjToInt(p_objXmlIn.SelectSingleNode("//" + "ClaimId").InnerText, base.ClientId));
				//TR 2605 - Populate formlist only forms are avl
				objRoot=p_objXmlOut.CreateElement("FROIForms");
				p_objXmlOut.AppendChild(objRoot);
                //Add by kuladeep for maintain Back button functionality:Start
                objSettings = new SysSettings(connectionString, base.ClientId);//sharishkumar Jira 827
                if (objSettings.UsePaperVisionInterface)
                {
                    AppendNode( p_objXmlOut,  objRoot, "FormCount",out  objChild);
                    objChild.InnerText = iCtr.ToString();
                    throw new RMAppException(Globalization.GetString("FROIAdaptor.PaperVision.Error", base.ClientId));//sharishkumar Jira 827
                }
                //Add by kuladeep for maintain Back button functionality:End

				if (arrForms!=null)
				{ 
					iUpperbound=arrForms.GetUpperBound(1);
               
					for(int i=0; i<=iUpperbound; i++)
					{
						if(arrForms.GetValue(0,i)!=null || arrForms.GetValue(1,i)!=null)
						{
							AppendNode( p_objXmlOut, objRoot,"Form",out objChild);
							objChild.SetAttribute("FormId",Conversion.ConvertObjToStr(arrForms.GetValue(0,i)));
							objChild.SetAttribute("FormName",Conversion.ConvertObjToStr(arrForms.GetValue(1,i)));
							iCtr++;
						}
					}
				}
				AppendNode( p_objXmlOut, objRoot,"ClaimId",out objChild);
				objChild.InnerText=p_objXmlIn.SelectSingleNode("//"+ "ClaimId").InnerText;
				AppendNode( p_objXmlOut, objRoot,"AttachForm",out objChild);
				objChild.InnerText=p_objXmlIn.SelectSingleNode("//"+ "AttachForm").InnerText;
				AppendNode( p_objXmlOut, objRoot,"Name",out objChild);
				objChild.InnerText=p_objXmlIn.SelectSingleNode("//"+ "Name").InnerText;
				AppendNode( p_objXmlOut, objRoot,"Title",out objChild);
				objChild.InnerText=p_objXmlIn.SelectSingleNode("//"+ "Title").InnerText;
				AppendNode( p_objXmlOut, objRoot,"Phone",out objChild);
				objChild.InnerText=p_objXmlIn.SelectSingleNode("//"+ "Phone").InnerText;
				AppendNode( p_objXmlOut, objRoot,"FormId",out objChild);
				AppendNode( p_objXmlOut, objRoot,"FormCount",out objChild);
				objChild.InnerText=iCtr.ToString(); 

			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("FROIAdaptor.GetFROIForms.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
				return false;

			}
			finally
			{
				objFroi=null;
				arrForms=null;
				objChild=null;
				objRoot=null;
                //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                objSettings = null;
                //Animesh Insertion Ends
			}
			return true;
		}
		/// Name		: GetClaimForms
		/// Author		: Tanuj Narula
		/// Date Created: 24-Feb-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function will return Claim forms.
		/// </summary>
		/// <param name="p_objXmlIn">
		/// Structure of the input Xml.
		/// <Document>
		///   <FROI>
		///    <ClaimId>
		///    </ClaimId>
		///    <FormId>
		///    </FormId>
		///    <State>
		///    </State>
		///    <DocumentPath>
		///    </DocumentPath>
		///    <AttachForm>
		///    </AttachForm>
		///    <DocStorageType>
		///    </DocStorageType>
		///   </FROI>
		/// </Document>
		/// </param>
		/// <param name="p_objXmlOut">
		/// Structure of output xml-:
		/// <ClaimForms>
		///   <FORM FormId="" FormName="">
		///   </FORM>
		///   <FORM FormId="" FormName="">
		///   </FORM>
		///   <FORM FormId="" FormName="">
		///   </FORM>
		/// </ClaimForms>
		/// </param>
		/// <param name="p_objErrOut">This will contain the errors occured during the processing of this function.</param>
		/// <returns>'True' if function call succeeds or 'false' if it fails.</returns>
		public bool GetClaimForms(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			FROIForms objFroi=null;
			System.Array arrForms=null;
			int iUpperbound=0;
			int iCtr=0;
			XmlElement objChild=null;
			XmlElement objRoot=null;
            //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
            SysSettings objSettings = null;
            //Animesh Insertion Ends
			try
			{
                p_objXmlOut = new XmlDocument();
                p_objErrOut = new BusinessAdaptorErrors(base.ClientId);

                //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                objSettings = new SysSettings(connectionString, base.ClientId);//sharishkumar Jira 827
                if (objSettings.UsePaperVisionInterface)
                    throw new RMAppException(Globalization.GetString("FROIAdaptor.PaperVision.Error", base.ClientId));//sharishkumar Jira 827
                //Animesh Insertion Ends
				objFroi=InitializeFROI();
                arrForms = (System.Array)objFroi.GetClaimForms(Conversion.ConvertObjToInt(p_objXmlIn.SelectSingleNode("//" + "ClaimId").InnerText, base.ClientId));
				iUpperbound=arrForms.GetUpperBound(1);
				objRoot=p_objXmlOut.CreateElement("ClaimForms");
				p_objXmlOut.AppendChild(objRoot);
               
				for(int i=0; i<=iUpperbound; i++)
				{
					if(arrForms.GetValue(0,i)!=null || arrForms.GetValue(1,i)!=null)
					{
						AppendNode( p_objXmlOut, objRoot,"Form",out objChild);
						objChild.SetAttribute("FormId",Conversion.ConvertObjToStr(arrForms.GetValue(0,i)));
						objChild.SetAttribute("FormName",Conversion.ConvertObjToStr(arrForms.GetValue(1,i)));
						iCtr++;
					}
				}
				AppendNode( p_objXmlOut, objRoot,"ClaimId",out objChild);
				objChild.InnerText=p_objXmlIn.SelectSingleNode("//"+ "ClaimId").InnerText;
				AppendNode( p_objXmlOut, objRoot,"FormId",out objChild);	  
				AppendNode( p_objXmlOut, objRoot,"State",out objChild);
				objChild.InnerText=p_objXmlIn.SelectSingleNode("//"+ "State").InnerText;	
				AppendNode( p_objXmlOut, objRoot,"FormCount",out objChild);
				objChild.InnerText=iCtr.ToString(); 			
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("FROIAdaptor.GetClaimForms.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
				return false;

			}
			finally
			{
				objFroi=null;
				objChild=null;
				objRoot=null;
				arrForms=null;
                //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                objSettings = null;
                //Animesh Insertion Ends
			}
			return true;

		}
		/// Name		: GetWCForms
		/// Author		: Tanuj Narula
		/// Date Created: 24-Feb-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function will return WC forms.
		/// </summary>
		/// <param name="p_objXmlIn">
		/// Structure of the input Xml.
		/// <Document>
		///   <FROI>
		///    <ClaimId>
		///    </ClaimId>
		///    <FormId>
		///    </FormId>
		///    <State>
		///    </State>
		///    <DocumentPath>
		///    </DocumentPath>
		///    <AttachForm>
		///    </AttachForm>
		///    <DocStorageType>
		///    </DocStorageType>
		///   </FROI>
		/// </Document>
		/// </param>
		/// <param name="p_objXmlOut">
	    /// Structure of output xml-:
		/// <WCForms>
		///   <FORM FormId="" FormName="" FormCatDesc="">
		///   </FORM>
		///    <FORM FormId="" FormName="" FormCatDesc="">
		///   </FORM>
		///   <FORM FormId="" FormName="" FormCatDesc="">
		///   </FORM>
		/// </WCForms>
		/// </param>
		/// <param name="p_objErrOut">This will contain the errors occured during the processing of this function.</param>
		/// <returns>'True' if function call succeeds or 'false' if it fails.</returns>
		public bool GetWCForms(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			FROIForms objFroi=null;
			System.Array arrForms=null;
			int iUpperbound=0;
			int iCtr=0;
			XmlElement objChild=null;
			XmlElement objRoot=null;
            //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
            SysSettings objSettings = null;
            //Animesh Insertion Ends
			try
			{

                p_objXmlOut = new XmlDocument();
                p_objErrOut = new BusinessAdaptorErrors(base.ClientId);


                //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                objSettings = new SysSettings(connectionString, base.ClientId);//sharishkumar Jira 827
                if (objSettings.UsePaperVisionInterface)
                    throw new RMAppException(Globalization.GetString("FROIAdaptor.PaperVision.Error",base.ClientId));//sharishkumar Jira 827
                //Animesh Insertion Ends
				objFroi=InitializeFROI();
                arrForms = (System.Array)objFroi.GetWCForms(Conversion.ConvertObjToInt(p_objXmlIn.SelectSingleNode("//" + "ClaimId").InnerText, base.ClientId));
                if (arrForms != null)
                {
                    iUpperbound = arrForms.GetUpperBound(1);
                }
				objRoot=p_objXmlOut.CreateElement("ClaimForms");
				p_objXmlOut.AppendChild(objRoot);

                if (arrForms != null)
                {
                    for (int i = 0; i <= iUpperbound; i++)
                    {
                        if (arrForms.GetValue(0, i) != null || arrForms.GetValue(1, i) != null || arrForms.GetValue(2, i) != null)
                        {
                            AppendNode( p_objXmlOut,  objRoot, "Form", out objChild);
                            objChild.SetAttribute("FormId", Conversion.ConvertObjToStr(arrForms.GetValue(1, i)));
                            objChild.SetAttribute("FormNameAndTitle", Conversion.ConvertObjToStr(arrForms.GetValue(2, i)));
                            objChild.SetAttribute("FormCatDesc", Conversion.ConvertObjToStr(arrForms.GetValue(0, i)));
                            iCtr++;
                        }
                    }
                }
                AppendNode( p_objXmlOut,  objRoot, "FormCount", out objChild);
				objChild.InnerText=iCtr.ToString(); 

                //Yatharth: MITS 20033: Values of controls needed on the page --start
                AppendNode(p_objXmlOut, objRoot, "Name", out objChild);
                objChild.InnerText = p_objXmlIn.SelectSingleNode("//Name").InnerText;
                AppendNode(p_objXmlOut, objRoot, "Title", out objChild);
                objChild.InnerText = p_objXmlIn.SelectSingleNode("//Title").InnerText;
                AppendNode(p_objXmlOut, objRoot, "Phone", out objChild);
                objChild.InnerText = p_objXmlIn.SelectSingleNode("//Phone").InnerText;
                //Yatharth: MITS 20033 --end
			  }
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("FROIAdaptor.GetWCForms.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
				return false;

			}
			finally
			{
				objFroi=null;
				objChild=null;
				objRoot=null;
				arrForms=null;
                //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                objSettings = null;
                //Animesh Insertion Ends
			}
			return true;
		}
		/// Name		: InvokeFROI
		/// Author		: Tanuj Narula
		/// Date Created: 25-Feb-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function will print the FROI report and generate the .FDF file.
		/// </summary>
		/// <param name="p_objXmlIn">
		/// Structure of the input Xml.
		/// <Document>
		///   <FROI>
		///    <ClaimId>
		///    </ClaimId>
		///    <FormId>
		///    </FormId>
		///    <State>
		///    </State>
		///    <DocumentPath>
		///    </DocumentPath>
		///    <AttachForm>
		///    </AttachForm>
		///    <DocStorageType>
		///    </DocStorageType>
		///   </FROI>
		/// </Document>
		/// </param>
		/// <param name="p_objXmlOut">
		/// Structure of output xml-:
		/// <Froi>
		/// <File Name="">
		/// </File>
		/// </Froi>
		/// </param>
		/// <param name="p_objErrOut">This will contain the errors occured during the processing of this function.</param>
		/// <returns>'True' if function call succeeds or 'false' if it fails.</returns>
		public bool	InvokeFROI(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
            FROIForms objFroi=null;
            SysSettings objSettings = null;
            string sFilePath="";
			string sName="";
			string sTitle="";
			string sPhone="";
            int iErrorType = 0;//MGaba2: MITS 19613 
            bool bParse = false;//MGaba2: MITS 19613
           
			try
			{
                p_objXmlOut = new XmlDocument();
                p_objErrOut = new BusinessAdaptorErrors(base.ClientId);

                //Animesh Inserted MITS 16697
                objSettings = new SysSettings(connectionString, base.ClientId);//sharishkumar Jira 827
                if (objSettings.UsePaperVisionInterface)
                    throw new RMAppException(Globalization.GetString("FROIAdaptor.PaperVision.Error", base.ClientId));//sharishkumar Jira 827
                //Mona:PaperVisionMerge : Animesh Insertion Ends
				objFroi=this.InitializeFROI();
				objFroi.PDFPath=m_strPDFTemplatePath;
				// JP 06.26.2006 - Added the .Replace below. This is a *hack* because Adobe freaks out on https://servername:443.
				objFroi.PDFUrl= AppendSlash(p_objXmlIn.SelectSingleNode("//RequestHost").InnerText).Replace(":443/", "/").Replace(":80/", "/") + AppendSlash(m_strPDFUrlPath);
				this.SetProperties( p_objXmlIn,ref objFroi);
				//if globally set to true then it should be true
				objFroi.AttachForm = GetAttachFormValueFromFroiSettings();
				//if globally set to false and value set by user is true
				if(Conversion.ConvertStrToBool(p_objXmlIn.SelectSingleNode("//"+ "AttachForm").InnerText).Equals(true))
				{
					objFroi.AttachForm=true;
				}
                
				sName=p_objXmlIn.SelectSingleNode("//Name").InnerText;
				sTitle=p_objXmlIn.SelectSingleNode("//Title").InnerText;
				sPhone=p_objXmlIn.SelectSingleNode("//Phone").InnerText;
				if(sName!="" || sTitle!="" || sPhone!="")
                {                    
                    objFroi.SetPreparerInfo(sName, ref sTitle,ref sPhone);
				}
				
				//Raman Bhatia..08/08/2006
				//If Acrosoft is enabled then we do not want to use the lagacy Document Management...
				//Do not Attach Document in such a scenario while invoking FROI

                objFroi.FroiPrintType = "SINGL";//Add by kuladeep for froi issue 4/4/2012 mits:28100
                objSettings = new SysSettings(connectionString, base.ClientId);//sharishkumar Jira 827
                if(objFroi.AttachForm && (objSettings.UseAcrosoftInterface||objSettings.UseMediaViewInterface))
				{
					objFroi.AttachForm = false;
					objFroi.DocumentPath = "";
					sFilePath=objFroi.InvokeFROI(Conversion.ConvertObjToInt(p_objXmlIn.SelectSingleNode("//"+ "ClaimId").InnerText, base.ClientId),Conversion.ConvertObjToInt(p_objXmlIn.SelectSingleNode("//"+ "FormId").InnerText, base.ClientId));
					objFroi.AttachForm = true;

				} // if
				else
				{
					sFilePath=objFroi.InvokeFROI(Conversion.ConvertObjToInt(p_objXmlIn.SelectSingleNode("//"+ "ClaimId").InnerText, base.ClientId),Conversion.ConvertObjToInt(p_objXmlIn.SelectSingleNode("//"+ "FormId").InnerText, base.ClientId));
				} // else


				
				if(sFilePath!=null && sFilePath.Trim()!="")
                {//MGaba2: MITS 19613 : Start
                    bParse = int.TryParse(sFilePath, out iErrorType);
                    if (bParse)
                    {
                        switch (iErrorType)
                        {
                            case (int)FROIFails.FF_NCCIBodyParts  :
                            p_objErrOut.Add("NCCIBodyParts", Globalization.GetString("FROIAdaptor.InvokeFROI.NCCIBodyParts",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                            return false;
                            break;
                            case (int)FROIFails.FF_NCCICauseCode:
                            p_objErrOut.Add("NCCICause", Globalization.GetString("FROIAdaptor.InvokeFROI.NCCICause",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                            return false;
                            break;
                            case (int)FROIFails.FF_NCCIIllness:
                            p_objErrOut.Add("NCCIInjury/Illness", Globalization.GetString("FROIAdaptor.InvokeFROI.NCCIInjury/Illness",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                            return false;
                            break;
                            case (int)FROIFails.FF_NonNumericClassCode:
                            p_objErrOut.Add("NonNumericClassCode", Globalization.GetString("FROIAdaptor.InvokeFROI.NonNumericClassCode",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                            return false;
                            break;
                            case (int)FROIFails.FF_NCCIBodyParts + (int)FROIFails.FF_NCCICauseCode:
                            p_objErrOut.Add("NCCIBodyParts", Globalization.GetString("FROIAdaptor.InvokeFROI.NCCIBodyParts",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                            p_objErrOut.Add("NCCICause", Globalization.GetString("FROIAdaptor.InvokeFROI.NCCICause",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                            return false;
                           break ;
                            case (int)FROIFails.FF_NCCIBodyParts + (int)FROIFails.FF_NCCIIllness:
                           p_objErrOut.Add("NCCIBodyParts", Globalization.GetString("FROIAdaptor.InvokeFROI.NCCIBodyParts",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                           p_objErrOut.Add("NCCIInjury/Illness", Globalization.GetString("FROIAdaptor.InvokeFROI.NCCIInjury/Illness",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                           return false;
                           break;
                            case (int)FROIFails.FF_NCCIBodyParts + (int)FROIFails.FF_NonNumericClassCode:
                           p_objErrOut.Add("NCCIBodyParts", Globalization.GetString("FROIAdaptor.InvokeFROI.NCCIBodyParts",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                           p_objErrOut.Add("NonNumericClassCode", Globalization.GetString("FROIAdaptor.InvokeFROI.NonNumericClassCode",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                           return false;
                           break;
                            case (int)FROIFails.FF_NCCICauseCode + (int)FROIFails.FF_NCCIIllness:
                           p_objErrOut.Add("NCCICause", Globalization.GetString("FROIAdaptor.InvokeFROI.NCCICause",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                           p_objErrOut.Add("NCCIInjury/Illness", Globalization.GetString("FROIAdaptor.InvokeFROI.NCCIInjury/Illness",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                           return false;
                           break;
                            case (int)FROIFails.FF_NCCICauseCode + (int)FROIFails.FF_NonNumericClassCode:
                           p_objErrOut.Add("NCCICause", Globalization.GetString("FROIAdaptor.InvokeFROI.NCCICause",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                           p_objErrOut.Add("NonNumericClassCode", Globalization.GetString("FROIAdaptor.InvokeFROI.NonNumericClassCode",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                           return false;
                           break;
                            case (int)FROIFails.FF_NCCIIllness + (int)FROIFails.FF_NonNumericClassCode:
                           p_objErrOut.Add("NCCIInjury/Illness", Globalization.GetString("FROIAdaptor.InvokeFROI.NCCIInjury/Illness",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                           p_objErrOut.Add("NonNumericClassCode", Globalization.GetString("FROIAdaptor.InvokeFROI.NonNumericClassCode",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                           return false;
                           break;
                            case (int)FROIFails.FF_NCCIBodyParts + (int)FROIFails.FF_NCCICauseCode + (int)FROIFails.FF_NCCIIllness :
                           p_objErrOut.Add("NCCIBodyParts", Globalization.GetString("FROIAdaptor.InvokeFROI.NCCIBodyParts",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                           p_objErrOut.Add("NCCICause", Globalization.GetString("FROIAdaptor.InvokeFROI.NCCICause",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                           p_objErrOut.Add("NCCIInjury/Illness", Globalization.GetString("FROIAdaptor.InvokeFROI.NCCIInjury/Illness",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                           return false;
                           break;
                            case (int)FROIFails.FF_NCCIBodyParts + (int)FROIFails.FF_NCCIIllness + (int)FROIFails.FF_NonNumericClassCode:
                           p_objErrOut.Add("NCCIBodyParts", Globalization.GetString("FROIAdaptor.InvokeFROI.NCCIBodyParts",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                           p_objErrOut.Add("NCCIInjury/Illness", Globalization.GetString("FROIAdaptor.InvokeFROI.NCCIInjury/Illness",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                           p_objErrOut.Add("NonNumericClassCode", Globalization.GetString("FROIAdaptor.InvokeFROI.NonNumericClassCode",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                           return false;
                           break;
                            case (int)FROIFails.FF_NCCICauseCode + (int)FROIFails.FF_NCCIIllness + (int)FROIFails.FF_NonNumericClassCode:
                           p_objErrOut.Add("NCCICause", Globalization.GetString("FROIAdaptor.InvokeFROI.NCCICause",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                           p_objErrOut.Add("NCCIInjury/Illness", Globalization.GetString("FROIAdaptor.InvokeFROI.NCCIInjury/Illness",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                           p_objErrOut.Add("NonNumericClassCode", Globalization.GetString("FROIAdaptor.InvokeFROI.NonNumericClassCode",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                           return false;
                           break;
                           case (int)FROIFails.FF_NCCIBodyParts + (int)FROIFails.FF_NCCICauseCode + (int)FROIFails.FF_NCCIIllness + (int)FROIFails.FF_NonNumericClassCode:
                           p_objErrOut.Add("NCCIBodyParts", Globalization.GetString("FROIAdaptor.InvokeFROI.NCCIBodyParts",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                           p_objErrOut.Add("NCCICause", Globalization.GetString("FROIAdaptor.InvokeFROI.NCCICause",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                           p_objErrOut.Add("NCCIInjury/Illness", Globalization.GetString("FROIAdaptor.InvokeFROI.NCCIInjury/Illness",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                           p_objErrOut.Add("NonNumericClassCode", Globalization.GetString("FROIAdaptor.InvokeFROI.NonNumericClassCode",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                           return false;
                           break;
                           case (int)FROIFails.FF_NCCIBodyParts + (int)FROIFails.FF_NCCICauseCode + (int)FROIFails.FF_NonNumericClassCode:
                           p_objErrOut.Add("NCCIBodyParts", Globalization.GetString("FROIAdaptor.InvokeFROI.NCCIBodyParts",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                           p_objErrOut.Add("NCCICause", Globalization.GetString("FROIAdaptor.InvokeFROI.NCCICause",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                           p_objErrOut.Add("NonNumericClassCode", Globalization.GetString("FROIAdaptor.InvokeFROI.NonNumericClassCode",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                           return false;
                           break;                         
                           case (int)FROIFails.FF_NCCIClassCode:
                           p_objErrOut.Add("NCCIClass", Globalization.GetString("FROIAdaptor.InvokeFROI.NCCIClass",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                           return false;
                           break;                            
                           case (int)FROIFails.FF_NoDepartmentAssigned:
                           p_objErrOut.Add("NoDepartmentAssigned", Globalization.GetString("FROIAdaptor.InvokeFROI.NoDepartmentAssigned",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                           return false;
                           break;
                           case (int)FROIFails.FF_NoClaimant:
                           p_objErrOut.Add("NoClaimant", Globalization.GetString("FROIAdaptor.InvokeFROI.NoClaimant",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                           return false;
                           break;
                           case (int)FROIFails.FF_LoadFROIOptionsFailed:
                           p_objErrOut.Add("LoadFROIOptionsFailed", Globalization.GetString("FROIAdaptor.InvokeFROI.LoadFROIOptionsFailed",base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                           return false;
                           break;
                        }
                    }//MGaba2: MITS 19613 :End
                    else
                    {
					this.MakeOutputDocumentWithFileContent("Froi",ref p_objXmlOut,sFilePath);
					
                    //Raman Bhatia..07/27/2006
                    //Post the document to Acrosoft if Acrosoft Interface is enabled
                    
                    if (objFroi.AttachForm && objSettings.UseAcrosoftInterface)
                    {
                        int iClaimID = Conversion.ConvertObjToInt(p_objXmlIn.SelectSingleNode("//" + "ClaimId").InnerText, base.ClientId);
						Claim objClaim = null;
                        DataModelFactory objDataModelFactory = new DataModelFactory(base.userLogin, base.ClientId);//sharishkumar Jira 827
                        objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                        objClaim.MoveTo(iClaimID);
						objDataModelFactory.UnInitialize();

                        string sClaimNumber = objClaim.ClaimNumber;
						string sEventNumber = objClaim.EventNumber;
                        string sDocTitle = "FROI (" + sFilePath.Substring(sFilePath.IndexOf(sClaimNumber) + objClaim.ClaimNumber.Length) + ")";
                        string sAppExcpXml = "";
                        Acrosoft objAcrosoft = new Acrosoft(base.ClientId);//dvatsa-cloud
                        //rsolanki2 :  start updates for MCM mits 19200 
                        Boolean bSuccess = false;
                        string sTempAcrosoftUserId = base.userLogin.LoginName;
                        string sTempAcrosoftPassword = base.userLogin.Password;

                        if (Conversion.CastToType<Boolean>(AcrosoftSection.UseCommonAcrosoftUser, out bSuccess))
                        {
                            if (AcrosoftSection.AcroUserMapSection.UserMap[base.userLogin.LoginName] != null
                                && !string.IsNullOrEmpty(AcrosoftSection.AcroUserMapSection.UserMap[base.userLogin.LoginName].RmxUser))
                            {
                                sTempAcrosoftUserId = AcrosoftSection.AcroUserMapSection.UserMap[base.userLogin.LoginName].AcrosoftUserId;
                                sTempAcrosoftPassword = AcrosoftSection.AcroUserMapSection.UserMap[base.userLogin.LoginName].AcrosoftPassword;
                            }
                            else
                            {
                                sTempAcrosoftUserId = AcrosoftSection.AcrosoftUsername;
                                sTempAcrosoftPassword = AcrosoftSection.AcrosoftUserPassword;
                            }
                        }
                        
                        //creating folder
                        objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                            AcrosoftSection.AcrosoftAttachmentsTypeKey, sEventNumber, sClaimNumber,
                            AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                        //storing document
                        objAcrosoft.StoreObjectBuffer(sTempAcrosoftUserId, sTempAcrosoftPassword,
                            sFilePath, sDocTitle, "", sEventNumber, sClaimNumber, AcrosoftSection.AcrosoftAttachmentsTypeKey,
                            "", "", sTempAcrosoftUserId, out sAppExcpXml);
                    
                        //rsolanki2 :  end updates for MCM mits 19200 

                        ////creating folder
                        //objAcrosoft.CreateAttachmentFolder(base.userLogin.LoginName , base.userLogin.Password , AcrosoftSection.AcrosoftAttachmentsTypeKey , sEventNumber , sClaimNumber , 
                        //    AcrosoftSection.EventFolderFriendlyName , AcrosoftSection.ClaimFolderFriendlyName , out sAppExcpXml); 
                        ////storing document
                        //objAcrosoft.StoreObjectBuffer(base.userLogin.LoginName, base.userLogin.Password, sFilePath, sDocTitle, "" , sEventNumber , sClaimNumber, AcrosoftSection.AcrosoftAttachmentsTypeKey, 
                        //    "", "" , userLogin.LoginName , out sAppExcpXml);
                    }

                    if (objFroi.AttachForm && objSettings.UseMediaViewInterface)
                    {
                        int iClaimID = Conversion.ConvertObjToInt(p_objXmlIn.SelectSingleNode("//" + "ClaimId").InnerText, base.ClientId);
                        Claim objClaim = null;
                        DataModelFactory objDataModelFactory = new DataModelFactory(base.userLogin, base.ClientId);//sharishkumar Jira 827
                        objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                        objClaim.MoveTo(iClaimID);
                        objDataModelFactory.UnInitialize();

                        string sClaimNumber = objClaim.ClaimNumber;
                        string sEventNumber = objClaim.EventNumber;
                        string sDocTitle = "FROI (" + sFilePath.Substring(sFilePath.IndexOf(sClaimNumber) + objClaim.ClaimNumber.Length) + ")";
                        MediaView oMediaView = new MediaView();
                        Boolean bSuccess = false;
                        importList oList = new importList(sClaimNumber, "Claim", sDocTitle);
                        string sFileName = sFilePath.Substring(sFilePath.LastIndexOf(@"\") + 1);
                        string sPath = sFilePath.Substring( 0 , sFilePath.LastIndexOf(@"\"));
                        bSuccess = oMediaView.MMUploadDocument(sFileName, sPath, oList);
                        oMediaView.Dispose();
                    }
                                      
                    
                    //Delete the file
					System.IO.File.Delete(sFilePath);
                    }

				}
				else
				{
                    throw new RMAppException(Globalization.GetString("FROIAdaptor.InteropError", base.ClientId));//sharishkumar Jira 827
				}
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("FROIAdaptor.InvokeFROI.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
				return false;

			}
			finally
			{
				objFroi=null;
                objSettings = null;
            }
            return true;
		}
        /// <summary>
        /// Added by Navdeep - Returns the Name of FDF file Prepared along with 
        /// name of the Merged file that will be prepared using Adobe Writer        
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="objClaim"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <param name="sFDFPath"></param>
        /// <param name="sPDFPath"></param>
        /// <param name="sMergePDFPath"></param>
        /// <param name="sMergePwdPDFPath"></param>
        /// <returns></returns>
        public bool InvokeAutoFROI(XmlDocument p_objXmlIn, Claim objClaim, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut, ref string sFDFPath, ref string sPDFPath, ref string sMergePDFPath, ref string sMergePwdPDFPath)
        {
            FROIForms objFroi = null;      
            SysSettings objSettings = null;
            //string sFilePath = "";                        
            int iUser = 0;
            string[] sPdfSplitPath;
            string sFormId = "0";            
            int iErrorType = 0; //MGaba2:MITS 19613
            bool bParse = false; //MGaba2:MITS 19613
            try
            {
                p_objXmlOut = new XmlDocument();
                p_objErrOut = new BusinessAdaptorErrors(base.ClientId);

                sMergePDFPath = "";
                sMergePwdPDFPath = "";
                sFDFPath = "";
                objFroi = InitializeFROI();
                objFroi.DSN = objClaim.Context.DbConnLookup.ConnectionString;
                objFroi.LoginName = objClaim.Context.RMUser.LoginName;
                iUser = objClaim.Context.RMUser.objUser.UserId;
                                          
                objFroi.LoginId= iUser;      //Modified for Froi Migration- Mits 33585,33586,33587                          

                sFormId = p_objXmlIn.SelectSingleNode("//" + "FormId").InnerText;
                
                objFroi.PDFPath = m_strPDFTemplatePath;
                // JP 06.26.2006 - Added the .Replace below. This is a *hack* because Adobe freaks out on https://servername:443.
                objFroi.PDFUrl = m_strPDFTemplatePath;
                //Commented By NAVDEEP to correct the PDF template PATH Issue
                //sPDFPath = objFroi.PDFPath;
                
                ////Corrected PDF Path
                //sPDFPath = Riskmaster.Common.RMConfigurator.BasePath;
                //sPDFPath = sPDFPath.Replace("riskmaster", "RiskmasterUI\\UI\\pdf-forms\\");
                ////Corrected PDF Path
                 
                //Corrected PDF Path
                sPDFPath = RMConfig.BasePath;
                sPDFPath = sPDFPath.ToLower();
                sPdfSplitPath = sPDFPath.Split('\\');
                sPdfSplitPath[sPdfSplitPath.GetUpperBound(0)] = sPdfSplitPath[sPdfSplitPath.GetUpperBound(0)].Replace("riskmaster", "RiskmasterUI\\UI\\pdf-forms");
                sPDFPath = "";
                foreach (string sPath in sPdfSplitPath)
                {
                    sPDFPath = sPDFPath + sPath + "\\";
                }

                //01/19/10 Geeta 19341  : Split architecture issue  ACORD and FROI
                if(!Directory.Exists(sPDFPath))
                {
                    objFroi.PDFUrl = AppendSlash(p_objXmlIn.SelectSingleNode("//RequestHost").InnerText).Replace(":443/", "/").Replace(":80/", "/") + AppendSlash(m_strPDFUrlPath);
                    sPDFPath = RMConfig.BasePath + "\\userdata\\froi\\";
                    DownloadPdfToServer(objFroi.PDFUrl, sFormId, objFroi.DSN, sPDFPath,false);                   
                }

                this.SetFROIProperties(objClaim.Context.RMDatabase.GlobalDocPath, ref p_objXmlIn, ref objFroi);
                objFroi.AttachForm = false;
                objFroi.FroiPrintType = "SINGL";//Add by kuladeep for froi 4/4/2012
                //if globally set to true then it should be true                                                
                sFDFPath = objFroi.InvokeFROI(Conversion.ConvertObjToInt(p_objXmlIn.SelectSingleNode("//" + "ClaimId").InnerText, base.ClientId), Conversion.ConvertObjToInt(p_objXmlIn.SelectSingleNode("//" + "FormId").InnerText, base.ClientId));
                //MGaba2:MITS 19613
                //Checking if some error has been thrown from legacy froi net6 dll instead of path
                //if (sFDFPath != null && sFDFPath.Trim() != "" )                                          
                if (sFDFPath != null && sFDFPath.Trim() != "" && !(bParse= int.TryParse(sFDFPath,out iErrorType)))
                {                    
                    sMergePDFPath = Path.GetTempPath() + objClaim.ClaimNumber + "_FROI.pdf";
                    sMergePwdPDFPath = Path.GetTempPath() + objClaim.ClaimNumber + "_FROIForm.pdf";
                    return true;
                }
                else
                {
                    return false;
                }
                
            }
                           
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
           {
               p_objErrOut.Add(p_objException, Globalization.GetString("FROIAdaptor.InvokeAutoFROI.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                return false;

            }
            finally
            {
                objFroi = null;
                objSettings = null;
                if (objClaim != null)
                {
                    objClaim.Dispose();
                }
            }
            return true;
        }
		/// Name		: InvokeWCForms
		/// Author		: Tanuj Narula
		/// Date Created: 25-Feb-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///  This function will print the reports for WC.
		/// </summary>
		/// <param name="p_objXmlIn">
		/// Structure of the input Xml.
		/// <Document>
		///   <FROI>
		///    <ClaimId>
		///    </ClaimId>
		///    <FormId>
		///    </FormId>
		///    <State>
		///    </State>
		///    <DocumentPath>
		///    </DocumentPath>
		///    <AttachForm>
		///    </AttachForm>
		///    <DocStorageType>
		///    </DocStorageType>
		///   </FROI>
		/// </Document>
		/// </param>
		/// <param name="p_objXmlOut">
		/// Structure of output xml-:
		/// <Froi>
		/// <File Name="">
		/// </File>
		/// </Froi>
		/// </param>
		/// <param name="p_objErrOut">This will contain the errors occured during the processing of this function.</param>
		/// <returns>'True' if function call succeeds or 'false' if it fails.</returns>
		public bool	InvokeWCForms(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
            FROIForms objFroi=null;
            string sFilePath="";
			string sName="";
			string sTitle="";
			string sPhone="";
			int iClaimID = 0;
			int iFormID = 0;
			string sStateID = "";
			string sError = "";
            DbReader objReader = null;
            XmlDocument p_objTemp = null;//Yatharth MITS 20033
            string sTemp = string.Empty;//Yatharth MITS 20033
            //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
            SysSettings objSettings = null;
            //Animesh Insertion Ends
            SessionManager p_objSessionManager = null; //mkaran2-  WC Forms Session
            
            try
            {
                p_objXmlOut = new XmlDocument();
                p_objErrOut = new BusinessAdaptorErrors(base.ClientId);

                //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                objSettings = new SysSettings(connectionString,base.ClientId);//sharishkumar Jira 827
                if (objSettings.UsePaperVisionInterface)
                    throw new RMAppException(Globalization.GetString("FROIAdaptor.PaperVision.Error", base.ClientId));//sharishkumar Jira 827
                //Animesh Insertion Ends
                //Check if required transaction mappings are setup
                iClaimID = Conversion.ConvertObjToInt(p_objXmlIn.SelectSingleNode("//" + "ClaimId").InnerText, base.ClientId);
                iFormID = Conversion.ConvertObjToInt(p_objXmlIn.SelectSingleNode("//" + "FormId").InnerText, base.ClientId);
                sStateID = p_objXmlIn.SelectSingleNode("//" + "State").InnerText;
                sError = CheckTransMappings(iClaimID, iFormID, sStateID);
                if (sError != "")
                {
                    throw new RMAppException("FROIAdaptor.InvokeWCForms. InteropError: " + sError);
                }

                objFroi = InitializeFROI();
                objFroi.PDFPath = m_strPDFTemplatePath;
                objFroi.PDFUrl = AppendSlash(p_objXmlIn.SelectSingleNode("//RequestHost").InnerText).Replace(":443/", "/").Replace(":80/", "/") + AppendSlash(m_strPDFUrlPath);
                objFroi.OutputPath = m_strPDFOutputPath;
                objFroi.DocStorageType = (short)m_userLogin.objRiskmasterDatabase.DocPathType;
                SetProperties( p_objXmlIn, ref objFroi);

                sName = p_objXmlIn.SelectSingleNode("//Name").InnerText;
                sTitle = p_objXmlIn.SelectSingleNode("//Title").InnerText;
                sPhone = p_objXmlIn.SelectSingleNode("//Phone").InnerText;
                if (sName != "" || sTitle != "" || sPhone != "")
                {
                    //Code added by Yatharth: MITS 20033 - START
                    JurisPreparerInfo objJuris = new JurisPreparerInfo(base.userID.ToString(), base.connectionString, base.ClientId);
                    p_objTemp = p_objXmlIn;
                    sTemp = p_objTemp.InnerXml.ToString();
                    sTemp = sTemp.Replace("<FROI>", "<JurisPreparer>");
                    sTemp = sTemp.Replace("</FROI>", "</JurisPreparer>");
                    sTemp = sTemp.Replace("<Name>", "<preparerName>");
                    sTemp = sTemp.Replace("</Name>", "</preparerName>");
                    sTemp = sTemp.Replace("<Title>", "<preparerTitle>");
                    sTemp = sTemp.Replace("</Title>", "</preparerTitle>");
                    sTemp = sTemp.Replace("<Phone>", "<preparerPhone>");
                    sTemp = sTemp.Replace("</Phone>", "</preparerPhone>");
                    p_objTemp.LoadXml(sTemp);
                    objJuris.Save(p_objTemp);
                    
                    if(objJuris != null)
                        objJuris = null;
                    //Code added by Yatharth: MITS 20033 - END
                    //objFroi.SetPreparerInfo(sName, ref sTitle, ref sPhone);
                }
                //MITS-10753 - Checking utility setting for the selected State and OrgHierarchy
                //Setting AttachForm property value depending upon the condition
                Helper objHelper = new Helper(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password,base.ClientId);
                if (objHelper.GetWCPDFAttachToClaimFlag(p_objXmlIn))
                    objFroi.AttachForm = true;
                else
                    objFroi.AttachForm = false;
                //MITS-10753 End

                //Get State row id
                string sSQL = "SELECT STATE_ID FROM STATES WHERE STATE_ROW_ID = '" + sStateID + "'";
                objReader = DbFactory.GetDbReader(base.connectionString, sSQL);
                if (objReader.Read())
                    sStateID = objReader.GetString("STATE_ID");
                objReader.Close();

                //MITS 17484 For Acrosoft, set AttachForm to false to avoid attaching
                //SysSettings objSettings = new SysSettings(connectionString);
                if (objFroi.AttachForm && (objSettings.UseAcrosoftInterface||objSettings.UseMediaViewInterface))
                {
                    objFroi.AttachForm = false;
                    objFroi.DocumentPath = "";
                    sFilePath = objFroi.InvokeWCForms(iClaimID, iFormID, sStateID);
                    objFroi.AttachForm = true;
                } // if
                else
                {
                    sFilePath = objFroi.InvokeWCForms(iClaimID, iFormID, sStateID);
                } // else

                if (sFilePath != null && sFilePath.Trim() != "")
                {
                    this.MakeOutputDocumentWithFileContent("Froi", ref p_objXmlOut, sFilePath);

                    //MITS 17484 Handle Acrosoft
                    if (objFroi.AttachForm && objSettings.UseAcrosoftInterface)
                    {
                        Claim objClaim = null;
                        DataModelFactory objDataModelFactory = new DataModelFactory(base.userLogin, base.ClientId);//sharishkumar Jira 827
                        objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                        objClaim.MoveTo(iClaimID);
                        objDataModelFactory.UnInitialize();

                        string sClaimNumber = objClaim.ClaimNumber;
                        string sEventNumber = objClaim.EventNumber;
                        //sgupta243 Changes for MITS 22213 26/09/2012 
                        //string sDocTitle = "FROI (" + sFilePath.Substring(sFilePath.IndexOf(sClaimNumber)) + ")";
                        string sDocTitle = sFilePath.Substring(sFilePath.IndexOf(sClaimNumber) + objClaim.ClaimNumber.Length);
                        //End for MITS 22213 26/09/2012
                        
                        string sAppExcpXml = "";
                        Acrosoft objAcrosoft = new Acrosoft(base.ClientId);//dvatsa-cloud
                        //rsolanki2 :  start updates for MCM mits 19200                         
                        Boolean bSuccess = false;
                        string sTempAcrosoftUserId = base.userLogin.LoginName;
                        string sTempAcrosoftPassword = base.userLogin.Password;

                        if (Conversion.CastToType<Boolean>(AcrosoftSection.UseCommonAcrosoftUser, out bSuccess))
                        {
                            if (AcrosoftSection.AcroUserMapSection.UserMap[base.userLogin.LoginName] != null
                                && !string.IsNullOrEmpty(AcrosoftSection.AcroUserMapSection.UserMap[base.userLogin.LoginName].RmxUser))
                            {
                                sTempAcrosoftUserId = AcrosoftSection.AcroUserMapSection.UserMap[base.userLogin.LoginName].AcrosoftUserId;
                                sTempAcrosoftPassword = AcrosoftSection.AcroUserMapSection.UserMap[base.userLogin.LoginName].AcrosoftPassword;
                            }
                            else
                            {
                                sTempAcrosoftUserId = AcrosoftSection.AcrosoftUsername;
                                sTempAcrosoftPassword = AcrosoftSection.AcrosoftUserPassword;
                            }
                        }

                       
                        //creating folder
                        objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                            AcrosoftSection.AcrosoftAttachmentsTypeKey, sEventNumber, sClaimNumber,
                            AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                        //storing document
                        objAcrosoft.StoreObjectBuffer(sTempAcrosoftUserId, sTempAcrosoftPassword,
                            sFilePath, sDocTitle, "", sEventNumber, sClaimNumber, AcrosoftSection.AcrosoftAttachmentsTypeKey,
                            "", "", sTempAcrosoftUserId, out sAppExcpXml);
                       
                        //rsolanki2 :  end updates for MCM mits 19200 

                        ////creating folder
                        //objAcrosoft.CreateAttachmentFolder(base.userLogin.LoginName, base.userLogin.Password, AcrosoftSection.AcrosoftAttachmentsTypeKey, sEventNumber, sClaimNumber,
                        //    AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                        ////storing document
                        //objAcrosoft.StoreObjectBuffer(base.userLogin.LoginName, base.userLogin.Password, sFilePath, sDocTitle, "", sEventNumber, sClaimNumber, AcrosoftSection.AcrosoftAttachmentsTypeKey,
                        //    "", "", userLogin.LoginName, out sAppExcpXml);
                    }

                    if (objFroi.AttachForm && objSettings.UseMediaViewInterface)
                    {
                        Claim objClaim = null;
                        DataModelFactory objDataModelFactory = new DataModelFactory(base.userLogin, base.ClientId);//sharishkumar Jira 827
                        objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                        objClaim.MoveTo(iClaimID);
                        objDataModelFactory.UnInitialize();

                        string sClaimNumber = objClaim.ClaimNumber;
                        string sEventNumber = objClaim.EventNumber;
                        //sgupta243 Changes for MITS 22213 26/09/2012
                        //string sDocTitle = "FROI (" + sFilePath.Substring(sFilePath.IndexOf(sClaimNumber)) + ")";
                        string sDocTitle = sFilePath.Substring(sFilePath.IndexOf(sClaimNumber) + objClaim.ClaimNumber.Length);
                        // END for MITS 22213

                        MediaView oMediaView = new MediaView();                       
                        Boolean bSuccess = false;

                        importList oList = new importList(sClaimNumber, "Claim", sDocTitle);
                        string sFileName = sFilePath.Substring(sFilePath.LastIndexOf(@"\") + 1);
                        string sPath = sFilePath.Substring(0, sFilePath.LastIndexOf(@"\"));
                        bSuccess = oMediaView.MMUploadDocument(sFileName, sPath, oList);

                        oMediaView.Dispose();
                    }

                    //Delete the file
                    XmlElement objRootElement = null;
                    objRootElement = p_objXmlOut.CreateElement("StorageId");
                    p_objXmlOut.SelectSingleNode("//Froi").AppendChild(objRootElement).InnerText = objFroi.StorageId.ToString();
                    //MITS-10753 - Added two nodes - State and OrgHierarchy
                    if (p_objXmlIn.SelectSingleNode("//" + "OrgHierarchy") != null && p_objXmlIn.SelectSingleNode("//" + "State") != null)
                    {
                        objRootElement = p_objXmlOut.CreateElement("State");
                        p_objXmlOut.SelectSingleNode("//Froi").AppendChild(objRootElement).InnerText = p_objXmlIn.SelectSingleNode("//" + "State").InnerText;
                        objRootElement = p_objXmlOut.CreateElement("OrgHierarchy");
                        p_objXmlOut.SelectSingleNode("//Froi").AppendChild(objRootElement).InnerText = p_objXmlIn.SelectSingleNode("//" + "OrgHierarchy").InnerText;
                    }
                    else
                    {
                        throw new RMAppException("FROIAdaptor.InvokeWCFOrms. InteropError: Failed to access State and OrgHierarchy");
                    }
                    //MITS-10753 End
                    //mkaran2-  WC Forms Session - Start    
                    XmlDocument objSessionWCForm = (XmlDocument)p_objXmlOut.Clone();

                    if (objSessionWCForm.ChildNodes.Count !=0)
                    {
                        XmlNode fileNode = objSessionWCForm.SelectSingleNode("//File");
                        fileNode.ParentNode.RemoveChild(fileNode);

                        if (p_objXmlIn.SelectSingleNode("//ClaimId") != null)
                        {
                            objRootElement = objSessionWCForm.CreateElement("ClaimId");
                            objSessionWCForm.SelectSingleNode("//Froi").AppendChild(objRootElement).InnerText = p_objXmlIn.SelectSingleNode("//ClaimId").InnerText;

                        }
                        if (p_objXmlIn.SelectSingleNode("//FormId") != null)
                        {
                            objRootElement = objSessionWCForm.CreateElement("FormId");
                            objSessionWCForm.SelectSingleNode("//Froi").AppendChild(objRootElement).InnerText = p_objXmlIn.SelectSingleNode("//FormId").InnerText;
                        }
                        p_objSessionManager = base.GetSessionObject();
                        p_objSessionManager.SetBinaryItem("XML_WCPDFFORMS", Utilities.BinarySerialize(objSessionWCForm.OuterXml), base.ClientId);
                    }
                    //mkaran2-  WC Forms Session - End
                    System.IO.File.Delete(sFilePath);
                }
                else
                {
                    throw new RMAppException("FROIAdaptor.InvokeWCFOrms. InteropError: Failed to create PDF file");
                }
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("FROIAdaptor.InvokeWCForms.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                return false;

            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                objFroi = null;
                //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                objSettings = null;
                //Animesh Insertion Ends
            }
           return true;
		}
        /// Name		: InvokePolicyEnhForms
        /// Author		: Divya Bhagchandani
        /// Date Created: Jan 2nd 2007		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        ///  This function will print the forms for Enhanced Policy.
        /// </summary>
		public bool	InvokePolicyEnhForms(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
            FROIForms objFroi = null;
			string sFilePath="";
			string sName="";
			string sTitle="";
			string sPhone="";
			int iPolicyID = 0;
			int iFormID = 0;
            // Start Naresh 9/24/2007 MI forms Changes
            int iInsuredId = 0;
            // End Naresh 9/24/2007 MI forms Changes
            //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
            SysSettings objSettings = null;
            //Animesh Insertion Ends
			string sError = "";

			try
            {
                p_objXmlOut = new XmlDocument();
                p_objErrOut = new BusinessAdaptorErrors(base.ClientId);

                //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                objSettings = new SysSettings(connectionString, base.ClientId);//sharishkumar Jira 827
                if (objSettings.UsePaperVisionInterface)
                    throw new RMAppException(Globalization.GetString("FROIAdaptor.PaperVision.Error", base.ClientId));//sharishkumar Jira 827
                //Animesh Insertion Ends
				//Check if required transaction mappings are setup
				iPolicyID = Conversion.ConvertObjToInt(p_objXmlIn.SelectSingleNode("//PolicyEnh/PolicyId").InnerText, base.ClientId);
				iFormID = Conversion.ConvertObjToInt(p_objXmlIn.SelectSingleNode("//PolicyEnh/FormId").InnerText, base.ClientId);
                // Start Naresh 9/24/2007 MI forms Changes
                XmlNode xn = (p_objXmlIn.SelectSingleNode("//PolicyEnh/InsuredId"));
                if (xn != null)
                {
                    iInsuredId = Conversion.ConvertObjToInt(xn.InnerText, base.ClientId);
                }                
                // End Naresh 9/24/2007 MI forms Changes
				objFroi=InitializeFROI();
				objFroi.PDFPath=m_strPDFTemplatePath+"Admin";
				objFroi.PDFUrl= AppendSlash(p_objXmlIn.SelectSingleNode("//RequestHost").InnerText).Replace(":443/", "/") + AppendSlash(m_strPDFUrlPath)+"Admin";
				objFroi.OutputPath=m_strPDFOutputPath;
				SetProperties( p_objXmlIn,ref objFroi);


                objFroi.SelInsuredID = iInsuredId;
				sName=p_objXmlIn.SelectSingleNode("//Name").InnerText;
				sTitle=p_objXmlIn.SelectSingleNode("//Title").InnerText;
				sPhone=p_objXmlIn.SelectSingleNode("//Phone").InnerText;
				if(sName!="" || sTitle!="" || sPhone!="")
				{
					objFroi.SetPreparerInfo(sName, ref sTitle,ref sPhone);
				}

                // Start Naresh 9/24/2007 MI forms Changes
                //if (iInsuredId == 0)
                //{
                    sFilePath = objFroi.InvokePolicyEnh(iPolicyID, iFormID);
                //}
                //else
                //{
                //    // Add Logic to Call Froi Adaptor
                //    //sFilePath = objFroi.InvokePolicyEnhInsured(iPolicyID, iFormID, iInsuredId);
                //}
                // End Naresh 9/24/2007 MI forms Changes
				if(sFilePath!=null && sFilePath.Trim()!="")
				{
					this.MakeOutputDocumentWithFileContent("Froi",ref p_objXmlOut,sFilePath);
					//Delete the file
					//XmlElement objRootElement=null;
					//objRootElement=p_objXmlOut.CreateElement("StorageId");
					//p_objXmlOut.SelectSingleNode("//Froi").AppendChild(objRootElement).InnerText = objFroi.StorageId.ToString() ;
					System.IO.File.Delete(sFilePath);  
				}
				else
				{
					throw new RMAppException("FROIAdaptor.InvokePolicyEnhForms.InteropError: Failed to create PDF file");
				}
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("FROIAdaptor.InvokePolicyEnhForms.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
				return false;

			}
            finally
            {
                //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                objSettings = null;
                //Animesh Insertion Ends
            }
           return true;
		}
		/// Name		: InvokeClaimForms
		/// Author		: Tanuj Narula
		/// Date Created: 23-Feb-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function will print the reports for ClaimForms.
		/// </summary>
		/// <param name="p_objXmlIn">
		/// Structure of the input Xml.
		/// <Document>
		///   <FROI>
		///    <ClaimId>
		///    </ClaimId>
		///    <FormId>
		///    </FormId>
		///    <State>
		///    </State>
		///    <DocumentPath>
		///    </DocumentPath>
		///    <AttachForm>
		///    </AttachForm>
		///    <DocStorageType>
		///    </DocStorageType>
		///   </FROI>
		/// </Document>
		/// </param>
		/// </param>
		/// <param name="p_objXmlOut">
		/// Structure of output xml-:
		/// <Froi>
		/// <File Name="">
		/// </File>
		/// </Froi>
		/// </param>
		/// <param name="p_objErrOut">This will contain the errors occured during the processing of this function.</param>
		/// <returns>'True' if function call succeeds or 'false' if it fails.</returns>
		public bool	InvokeClaimForms(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
            FROIForms objFroi = null;
			string sFilePath="";
            //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
            SysSettings objSettings = null;
            //Animesh Insertion Ends
			try
			{
                p_objXmlOut = new XmlDocument();
                p_objErrOut = new BusinessAdaptorErrors(base.ClientId);

                //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                objSettings = new SysSettings(connectionString, base.ClientId);//sharishkumar Jira 827
                if (objSettings.UsePaperVisionInterface)
                    throw new RMAppException(Globalization.GetString("FROIAdaptor.PaperVision.Error", base.ClientId));//sharishkumar Jira 827
                //Animesh Insertion Ends
				objFroi=InitializeFROI();
				objFroi.PDFPath=m_strPDFTemplatePath + "ClaimForms\\";
				objFroi.PDFUrl= AppendSlash(p_objXmlIn.SelectSingleNode("//RequestHost").InnerText).Replace(":443/", "/").Replace(":80/", "/") + AppendSlash(m_strPDFUrlPath)+ "ClaimForms/";
				objFroi.OutputPath=m_strPDFOutputPath;
				SetProperties( p_objXmlIn,ref objFroi);

                sFilePath = objFroi.InvokeClaimForms(Conversion.ConvertObjToInt(p_objXmlIn.SelectSingleNode("//" + "ClaimId").InnerText, base.ClientId), Conversion.ConvertObjToInt(p_objXmlIn.SelectSingleNode("//" + "FormId").InnerText, base.ClientId), p_objXmlIn.SelectSingleNode("//" + "State").InnerText);

				if(sFilePath!=null && sFilePath.Trim()!="")
				{
					this.MakeOutputDocumentWithFileContent("Froi",ref p_objXmlOut,sFilePath);
					//Delete the file
					System.IO.File.Delete(sFilePath);
				}
				else
				{
					throw new RMAppException("FROIAdaptor.InteropError");
				}

			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("FROIAdaptor.InvokeClaimForms.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
				return false;

			}
            finally
            {
                //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                objSettings = null;
                //Animesh Insertion Ends
            }
            return true;
		}


		///****************Integrity Check functions deprecated***********************************
		/// Name		: IntegrityCheck
		/// Author		: Pankaj Chowdhury
		/// Date Created: 5-Aug-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function does integrity checks for all the type of forms.
		/// </summary>
		/// <param name="p_objXmlIn">
		/// Nothing.
		/// </param>
		/// <param name="p_objXmlOut">
		/// Structure of output xml document-:
		/// <IntegrityCheckWCForms>
		///   <File>
		///   </File>
		/// </IntegrityCheckWCForms>
		/// </param>
		/// <param name="p_objErrOut">This will contain the errors occured during the processing of this function.</param>
		/// <returns>'True' if function call succeeds or 'false' if it fails.</returns>
//		public bool IntegrityCheck(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
//		{
//			FROINet6.CFROIFormsClass objFroi=null;
//			XmlElement objChild=null;
//			XmlElement objRoot=null;
//			XmlElement objRootBase=null;
//			int iUpperbound=0;
//			try
//			{
//				objFroi=InitializeFROI(ref p_objXmlOut,ref p_objErrOut);
//				objFroi.PDFPath=m_strPDFTemplatePath;
//				VBA.Collection objColl=objFroi.IntegrityCheckWCForms();
//				iUpperbound=objColl.Count();
//				objRootBase=p_objXmlOut.CreateElement("IntegrityCheck");
//				objRoot=p_objXmlOut.CreateElement("IntegrityCheckWCForms");
//				objRootBase.AppendChild(objRoot);
//				p_objXmlOut.AppendChild(objRoot);               
//				for(int i=1; i<=iUpperbound; i++)
//				{
//					AppendNode(ref p_objXmlOut,ref objRoot,"File",out objChild);
//					object objTemp=i;
//					objChild.InnerText=Conversion.ConvertObjToStr(objColl.Item(ref objTemp));
//				}
//
//				objColl=objFroi.IntegrityCheckFroiForms();
//				iUpperbound=objColl.Count();
//				objRoot=p_objXmlOut.CreateElement("IntegrityCheckFroiForms");
//				objRootBase.AppendChild(objRoot);
//				for(int i=1; i<=iUpperbound; i++)
//				{
//					AppendNode(ref p_objXmlOut,ref objRoot,"File",out objChild);
//					object objTemp=i;
//					objChild.InnerText=Conversion.ConvertObjToStr(objColl.Item(ref objTemp));
//				}
//
//				objColl=objFroi.IntegrityCheckClaimForms();
//				iUpperbound=objColl.Count();
//				objRoot=p_objXmlOut.CreateElement("IntegrityCheckClaimForms");
//				objRootBase.AppendChild(objRoot);
//				for(int i=1; i<=iUpperbound; i++)
//				{
//					AppendNode(ref p_objXmlOut,ref objRoot,"File",out objChild);
//					object objTemp=i;
//					objChild.InnerText=Conversion.ConvertObjToStr(objColl.Item(ref objTemp));
//				}
//				return true;
//			}
//			catch(RMAppException p_objException)
//			{
//				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
//				return false;
//			}
//			catch(Exception p_objException)
//			{
//				p_objErrOut.Add(p_objException,Globalization.GetString("FROIAdaptor.IntegrityCheck.Error"),BusinessAdaptorErrorType.Error);
//				return false;
//			}
//			finally
//			{
//				objChild=null;
//				objFroi=null;
//				objRoot=null;
//				objRootBase=null;
//			}
//		}
//
//		/// Name		: IntegrityCheckWCForms
//		/// Author		: Tanuj Narula
//		/// Date Created: 23-Feb-2005		
//		///************************************************************
//		/// Amendment History
//		///************************************************************
//		/// Date Amended   *   Amendment   *    Author
//		///************************************************************
//		/// <summary>
//		/// This function checks for the integrity of WCForms forms.
//		/// </summary>
//		/// <param name="p_objXmlIn">
//		/// Nothing.
//		/// </param>
//		/// <param name="p_objXmlOut">
//		/// Structure of output xml document-:
//		/// <IntegrityCheckWCForms>
//		///   <File>
//		///   </File>
//		/// </IntegrityCheckWCForms>
//		/// </param>
//		/// <param name="p_objErrOut">This will contain the errors occured during the processing of this function.</param>
//		/// <returns>'True' if function call succeeds or 'false' if it fails.</returns>
//		public bool IntegrityCheckWCForms(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
//		{
//			FROINet6.CFROIFormsClass objFroi=null;
//			XmlElement objChild=null;
//			XmlElement objRoot=null;
//			int iUpperbound=0;
//			try
//			{
//				objFroi=InitializeFROI(ref p_objXmlOut,ref p_objErrOut);
//				objFroi.PDFPath=m_strPDFTemplatePath;
//				VBA.Collection objColl=objFroi.IntegrityCheckWCForms();
//				iUpperbound=objColl.Count();
//				objRoot=p_objXmlOut.CreateElement("IntegrityCheckWCForms");
//				p_objXmlOut.AppendChild(objRoot);
//               
//				for(int i=1; i<=iUpperbound; i++)
//				{
//					AppendNode(ref p_objXmlOut,ref objRoot,"File",out objChild);
//					object objTemp=i;
//					objChild.InnerText=Conversion.ConvertObjToStr(objColl.Item(ref objTemp));
//				}
//			}
//			catch(RMAppException p_objException)
//			{
//				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
//				return false;
//			}
//			catch(Exception p_objException)
//			{
//				p_objErrOut.Add(p_objException,Globalization.GetString("FROIAdaptor.IntegrityCheckWCForms.Error"),BusinessAdaptorErrorType.Error);
//				return false;
//			}
//			finally
//			{
//				objChild=null;
//				objFroi=null;
//				objRoot=null;
//			}
//			return true;
//		}
//		/// Name		: IntegrityCheckFroiForms
//		/// Author		: Tanuj Narula
//		/// Date Created: 23-Feb-2005		
//		///************************************************************
//		/// Amendment History
//		///************************************************************
//		/// Date Amended   *   Amendment   *    Author
//		///************************************************************
//		/// <summary>
//		///  This function checks for the integrity of Froi forms.
//		/// </summary>
//		/// <param name="p_objXmlIn">
//		/// Nothing.
//		/// </param>
//		/// <param name="p_objXmlOut">
//		/// Structure of output xml document-:
//		/// <IntegrityCheckFroiForms>
//		/// <File>
//		/// </File>
//		/// </IntegrityCheckFroiForms>
//		/// </param>
//		/// <param name="p_objErrOut">This will contain the errors occured during the processing of this function.</param>
//		/// <returns>'True' if function call succeeds or 'false' if it fails.</returns>
//		public bool IntegrityCheckFroiForms(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
//		{
//
//			FROINet6.CFROIFormsClass objFroi=null;
//			XmlElement objChild=null;
//			int iUpperbound=0;
//			XmlElement objRoot=null;
//			try
//			{
//				objFroi=InitializeFROI(ref p_objXmlOut,ref p_objErrOut);
//
//				objFroi.PDFPath=m_strPDFTemplatePath;
//				VBA.Collection objColl=objFroi.IntegrityCheckFroiForms();
//				iUpperbound=objColl.Count();
//				objRoot=p_objXmlOut.CreateElement("IntegrityCheckFroiForms");
//				p_objXmlOut.AppendChild(objRoot);
//               
//				for(int i=1; i<=iUpperbound; i++)
//				{
//					AppendNode(ref p_objXmlOut,ref objRoot,"File",out objChild);
//					object objTemp=i;
//					objChild.InnerText=Conversion.ConvertObjToStr(objColl.Item(ref objTemp));
//				}
//			}
//			catch(RMAppException p_objException)
//			{
//				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
//				return false;
//			}
//			catch(Exception p_objException)
//			{
//				p_objErrOut.Add(p_objException,Globalization.GetString("FROIAdaptor.IntegrityCheckFroiForms.Error"),BusinessAdaptorErrorType.Error);
//				return false;
//			}
//			finally
//			{
//				objChild=null;
//				objFroi=null;
//				objRoot=null;
//			}
//			return true;
//		}
//		/// Name		: IntegrityCheckClaimForms
//		/// Author		: Tanuj Narula
//		/// Date Created: 23-Feb-2005		
//		///************************************************************
//		/// Amendment History
//		///************************************************************
//		/// Date Amended   *   Amendment   *    Author
//		///************************************************************
//		/// <summary>
//		/// This function checks for the integrity of Claim forms.
//		/// </summary>
//		/// <param name="p_objXmlIn">
//		/// Nothing.
//		/// </param>
//		/// <param name="p_objXmlOut">
//		/// Structure of output Xml document-:
//		/// <IntegrityCheckClaimForms>
//		///    <File>
//		///    </File>
//		/// </IntegrityCheckClaimForms></param>
//		/// <param name="p_objErrOut">This will contain the errors occured during the processing of this function.</param>
//		/// <returns>'True' if function call succeeds or 'false' if it fails.</returns>
//		public bool IntegrityCheckClaimForms(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
//		{
//          
//			FROINet6.CFROIFormsClass objFroi=null;
//			XmlElement objChild=null;
//			XmlElement objRoot=null;
//			VBA.Collection arrlstColl=null;
//			int iUpperbound=0;
//			try
//			{
//				objFroi=InitializeFROI(ref p_objXmlOut,ref p_objErrOut);
//				objFroi.PDFPath=m_strPDFTemplatePath;
//				arrlstColl=objFroi.IntegrityCheckClaimForms();
//				iUpperbound=arrlstColl.Count();
//			    objRoot=p_objXmlOut.CreateElement("IntegrityCheckClaimForms");
//				p_objXmlOut.AppendChild(objRoot);
//               
//				for(int i=1; i<=iUpperbound; i++)
//				{
//					AppendNode(ref p_objXmlOut,ref objRoot,"File",out objChild);
//					object objTemp=i;
//					objChild.InnerText=Conversion.ConvertObjToStr(arrlstColl.Item(ref objTemp));
//				}
//			}
//			catch(RMAppException p_objException)
//			{
//				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
//				return false;
//			}
//			catch(Exception p_objException)
//			{
//				p_objErrOut.Add(p_objException,Globalization.GetString("FROIAdaptor.IntegrityCheckClaimForms.Error"),BusinessAdaptorErrorType.Error);
//				return false;
//			}
//			finally
//			{
//				objChild=null;
//				objFroi=null;
//				objRoot=null;
//				arrlstColl=null;
//			}
//			return true;
//		}
		
		/// Name		: SetPreparerInfo
		/// Author		: Tanuj Narula
		/// Date Created: 23-Feb-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function will set preparer info
		/// </summary>
		/// <param name="p_objXmlIn">
		/// <Document>
		/// <PreparerInfo>
		///  <Name></Name> 
		///  <Title></Title> 
		///  <Phone></Phone> 
		///  <SkipPrompt></SkipPrompt> 
		/// </PreparerInfo>
		/// </Document>
		/// </param>
		/// <param name="p_objXmlOut">Nothing</param>
		/// <param name="p_objErrOut">This will contain the errors occured during the processing of this function.</param>
		/// <returns>'True' if function call succeeds or 'false' if it fails.</returns>
		public bool SetPreparerInfo(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
            FROIForms objFroi = null;      
			string sTitle="";
			string sPhone="";
            //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
            SysSettings objSettings = null;
            //Animesh Insertion ends
			try
			{
                p_objErrOut = new BusinessAdaptorErrors(base.ClientId);
                //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                objSettings = new SysSettings(connectionString, base.ClientId);//sharishkumar Jira 827
                if (objSettings.UsePaperVisionInterface)
                    throw new RMAppException(Globalization.GetString("FROIAdaptor.PaperVision.Error", base.ClientId));//sharishkumar Jira 827
                //Animesh Insertion Ends
				sTitle=p_objXmlIn.SelectSingleNode("//"+ "Title").InnerText;
				sPhone=p_objXmlIn.SelectSingleNode("//"+ "Phone").InnerText;
				objFroi=InitializeFROI();
                objFroi.objPreparerInfo.SkipPrompt = Conversion.ConvertObjToBool(p_objXmlIn.SelectSingleNode("//" + "SkipPrompt").InnerText, base.ClientId);
				objFroi.SetPreparerInfo(p_objXmlIn.SelectSingleNode("//"+ "Name").InnerText,ref sTitle,ref sPhone);

			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("FROIAdaptor.SetPreparerInfo.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
				return false;

            }
            finally
            {
                //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                objSettings = null;
                //Animesh Insertion Ends
            }
			return true;
		}
		/// Name		: objPreparerInfo
		/// Author		: Tanuj Narula
		/// Date Created: 23-Feb-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function is used to get the preparer information.
		/// </summary>
		/// <param name="p_objXmlIn">
		/// Structure of the input xml-:
		/// </param>
		/// <param name="p_objXmlOut">
		/// Structure of the output xml-:
		/// <PreparerInfo>
		///  <Name></Name> 
		///  <Title></Title> 
		///  <Phone></Phone> 
		///  <Source></Source> 
		///</PreparerInfo>
		/// </param>
		/// <param name="p_objErrOut">This will contain the errors occured during the processing of this function.</param>
		/// <returns>'True' if function call succeeds or 'false' if it fails.</returns>
		public bool objPreparerInfo(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
            FROIForms objFroi=null;
            //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
            SysSettings objSettings = null; 
            //Animesh Insertion ends
			try
			{
                p_objXmlOut = new XmlDocument();
                p_objErrOut = new BusinessAdaptorErrors(base.ClientId);

                //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                objSettings = new SysSettings(connectionString, base.ClientId);//sharishkumar Jira 827
                if (objSettings.UsePaperVisionInterface)
                    throw new RMAppException(Globalization.GetString("FROIAdaptor.PaperVision.Error", base.ClientId));//sharishkumar Jira 827
                //Animesh Insertion Ends
				objFroi=InitializeFROI() ;
				GetPreparerInfoXml(ref p_objXmlOut,ref objFroi);

			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("FROIAdaptor.objPreparerInfo.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
				return false;
			}
			finally
			{
				objFroi=null;
                //Animesh Inserted MITS 16697
                objSettings = null;
                //Animesh Insertion Ends
			}
			return true;
		}
      
		/// Name		: GetPreparerInfoXml
		/// Author		: Tanuj Narula
		/// Date Created: 23-Feb-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function will output Preparer xml from PreparerInfo object
		/// </summary>
		/// <param name="p_objXml"> Output xml will be contained by this document.</param>
		/// <param name="p_objFroi">Instance of FROI interop object.</param>
		private void GetPreparerInfoXml(ref XmlDocument p_objXml,ref FROIForms p_objFroi)
		{
			
				XmlElement objChild=null;
				XmlElement objRoot=null;
			    int iSource=0;

				try
				{

					objRoot=p_objXml.CreateElement("PreparerInfo");
					p_objXml.AppendChild(objRoot);

					AppendNode(p_objXml, objRoot,"Name",out objChild);
					objChild.InnerText=p_objFroi.objPreparerInfo.Name;

					AppendNode(p_objXml,objRoot,"Title",out objChild);
					objChild.InnerText=p_objFroi.objPreparerInfo.Title;

					AppendNode(p_objXml,objRoot,"Phone",out objChild);
					objChild.InnerText=p_objFroi.objPreparerInfo.Phone;

					AppendNode(p_objXml,objRoot,"Source",out objChild);

                    FROIPrepareInfo.ePreparerSource enumPreparer = p_objFroi.objPreparerInfo.Source;
					iSource=(int)p_objFroi.objPreparerInfo.Source;
					objChild.InnerText=Conversion.ConvertObjToStr(iSource);

					
				}
				catch(RMAppException p_objException)
				{
					throw p_objException;
				}
			  catch(Exception p_objException)
			    {
					throw p_objException;
			    }
			finally
			{
				objChild=null;
				objRoot=null;
			}
		}
		/// Name		: SkipFroiPreparerPrompt
		/// Author		: Tanuj Narula
		/// Date Created: 23-Feb-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
        /// <summary>
        /// This function flags whether to skip 'Froi preparer prompt' or not.
        /// </summary>
		/// <param name="p_objXmlIn">
		/// Structure of the input Xml.
		/// <Document>
		///   <FROI>
		///    <ClaimId>
		///    </ClaimId>
		///    <FormId>
		///    </FormId>
		///    <State>
		///    </State>
		///    <DocumentPath>
		///    </DocumentPath>
		///    <AttachForm>
		///    </AttachForm>
		///    <DocStorageType>
		///    </DocStorageType>
		///   </FROI>
		/// </Document>
		/// </param>
		/// <param name="p_objXmlOut">
		/// Structure of output xml-:
		/// <PreparerInfo>
		/// <Name></Name> 
		///<Title></Title> 
		///<Phone></Phone> 
		///<Source></Source> 
		///<SkipFroiPreparerPrompt></SkipFroiPreparerPrompt> 
		///</PreparerInfo>
		/// </param>
		/// <param name="p_objErrOut">This will contain the errors occured during the processing of this function.</param>
		/// <returns>'True' if function call succeeds or 'false' if it fails.</returns>
		public bool SkipFroiPreparerPrompt(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{  
			bool bSkipPrompt=false;
			int iClaimId=0;
            FROIForms objFroi=null;
           
			XmlElement objElement=null;
            //Animesh Inserted MITS 16697
            SysSettings objSettings = null;
            //Animesh Insertion Ends
			try
			{
                p_objXmlOut = new XmlDocument();
                p_objErrOut = new BusinessAdaptorErrors(base.ClientId);

                //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                objSettings = new SysSettings(connectionString, base.ClientId);//sharishkumar Jira 827
                //Animesh Insertion Ends
				objFroi=this.InitializeFROI();

			    iClaimId=Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//" + "ClaimId").InnerText);
				bSkipPrompt=objFroi.objPreparerInfo.SkipFroiPreparerPrompt(ref iClaimId);
				this.GetPreparerInfoXml(ref p_objXmlOut,ref objFroi);
				objElement=p_objXmlOut.CreateElement("SkipFroiPreparerPrompt");
                objElement.InnerText = Conversion.ConvertObjToStr(Conversion.ConvertBoolToInt(bSkipPrompt, base.ClientId));
				p_objXmlOut.DocumentElement.AppendChild(objElement);

				objElement=p_objXmlOut.CreateElement("AttachForm");
				p_objXmlOut.DocumentElement.AppendChild(objElement);   
				objElement=p_objXmlOut.CreateElement("ClaimId");
				objElement.InnerText=iClaimId.ToString();
				p_objXmlOut.DocumentElement.AppendChild(objElement);
                //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                if (objSettings.UsePaperVisionInterface)
                    throw new RMAppException(Globalization.GetString("FROIAdaptor.PaperVision.Error", base.ClientId));//sharishkumar Jira 827
                //Animesh Insertion Ends
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("FROIAdaptor.SkipFroiPreparerPrompt.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
				return false;
			}
			finally
			{
				objFroi=null;
				objElement=null;
                //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                objSettings = null;
                //Animesh Insertion Ends
			}
			return true;
		}

		/// <summary>
		/// Name		: GetAttachFromValueFromFroiSettings
		/// Author		: Sumit
		/// Date Created: 09-Jan-2006		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************		
		/// </summary>
		/// <returns>returns values from Global froi settings 0 - True, 1-False
		/// </returns>
		private bool GetAttachFormValueFromFroiSettings()
		{
			using (DbReader objRdr=DbFactory.GetDbReader(m_connectionString,"SELECT ATTACH_TO_CLAIM FROM FROI_OPTIONS"))
			{
				if(objRdr.Read())
				{
					return (objRdr.GetInt32(0).Equals(0)?true:false) ;
				}
			}
			return false;
		}
		/// Name		: SkipJurisPreparerPrompt
		/// Author		: Tanuj Narula
		/// Date Created: 23-Feb-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function flags whether to skip 'Juris preparer prompt' or not.
		/// </summary>
		/// <param name="p_objXmlIn">
		/// Structure of the input Xml.
		/// <Document>
		///   <FROI>
		///    <ClaimId>
		///    </ClaimId>
		///    <FormId>
		///    </FormId>
		///    <State>
		///    </State>
		///    <DocumentPath>
		///    </DocumentPath>
		///    <AttachForm>
		///    </AttachForm>
		///    <DocStorageType>
		///    </DocStorageType>
		///   </FROI>
		/// </Document>
		/// </param>
		/// <param name="p_objXmlOut">
		/// Structure of output xml-:
		/// <PreparerInfo>
		/// <Name></Name> 
		///<Title></Title> 
		///<Phone></Phone> 
		///<Source></Source> 
		///<SkipJurisPreparerPrompt></SkipFroiPreparerPrompt> 
		///</PreparerInfo>
		///</param>
		/// <param name="p_objErrOut">This will contain the errors occured during the processing of this function.</param>
		/// <returns>'True' if function call succeeds or 'false' if it fails.</returns>
		public bool SkipJurisPreparerPrompt(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			
			bool bSkipPrompt=false;
			int iClaimId=0;
            FROIForms objFroi = null; 
			XmlElement objElement=null;
            //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
            SysSettings objSettings = null;
            //Animesh Insertion Ends
			try
			{
                p_objXmlOut = new XmlDocument();
                p_objErrOut = new BusinessAdaptorErrors(base.ClientId);

                //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                objSettings = new SysSettings(connectionString, base.ClientId);//sharishkumar Jira 827
                //Animesh Insertion Ends
				objFroi=InitializeFROI();
				iClaimId=Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//" + "ClaimId").InnerText);
				bSkipPrompt=objFroi.objPreparerInfo.SkipFroiPreparerPrompt(ref iClaimId);
				this.GetPreparerInfoXml(ref p_objXmlOut,ref objFroi);
				objElement=p_objXmlOut.CreateElement("SkipJurisPreparerPrompt");
                objElement.InnerText = Conversion.ConvertBoolToInt(bSkipPrompt, base.ClientId).ToString();
				p_objXmlOut.DocumentElement.AppendChild(objElement);
                //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                if (objSettings.UsePaperVisionInterface)
                    throw new RMAppException(Globalization.GetString("FROIAdaptor.PaperVision.Error", base.ClientId));//sharishkumar Jira 827
                //Animesh Insertion Ends
               
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("FROIAdaptor.SkipJurisPreparerPrompt.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
				return false;
			}
			finally
			{
				objElement=null;
				objFroi=null;
                //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                objSettings = null;
                //Animesh Insertion Ends
			}
			return true;
		}


		/// Name		: GetAcordForms
		/// Author		: Pankaj Chowdhury
		/// Date Created: 08-Aug-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function will return List of Acord Forms
		/// </summary>
		/// <param name="p_objXmlIn">
		/// Structure of the input Xml.
		/// <Document>
		///   <ACORD>
		///    <ClaimId>
		///    </ClaimId>
		///    <FormId>
		///    </FormId>
		///    <State>
		///    </State>
		///    <DocumentPath>
		///    </DocumentPath>
		///    <AttachForm>
		///    </AttachForm>
		///    <DocStorageType>
		///    </DocStorageType>
		///   </ACORD>
		/// </Document>
		/// </param>
		/// <param name="p_objXmlOut">
		/// Structure of output xml-:
		/// <ACORDForms>
		///		<Form FormId="" FormNameAndTitle="" FormCatDesc=""/>
		///   <Form FormId="" FormNameAndTitle="" FormCatDesc=""/>
		/// </ACORDForms>
		/// </param>
		/// <param name="p_objErrOut">This will contain the errors occured during the processing of this function.</param>
		/// <returns>'True' if function call succeeds or 'false' if it fails.</returns>
		public bool GetACORDForms(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
            FROIForms objFroi = null;      
			System.Array arrForms=null;
			int iUpperbound=0;
			int iCtr=0;
			XmlElement objChild=null;
			XmlElement objRoot=null;
			DbReader objRdr=null;
			LocalCache objCache=null;
            //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
            SysSettings objSettings = null;
            //Animesh Insertion Ends
			try
			{
                //Comment by kuladeep for maintain Back button functionality:Start
                //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                //objSettings = new SysSettings(connectionString);
                //if (objSettings.UsePaperVisionInterface)
                    //throw new RMAppException(Globalization.GetString("FROIAdaptor.PaperVision.Error"));
                //Animesh Insertion Ends
                //Comment by kuladeep for maintain Back button functionality:end
                p_objXmlOut = new XmlDocument();
                p_objErrOut = new BusinessAdaptorErrors(base.ClientId);

				objFroi=InitializeFROI();
                arrForms = (System.Array)objFroi.GetAcordForms(Conversion.ConvertObjToInt(p_objXmlIn.SelectSingleNode("//" + "ClaimId").InnerText, base.ClientId));
				iUpperbound=arrForms.GetUpperBound(1);
				objRoot=p_objXmlOut.CreateElement("ACORDForms");
				p_objXmlOut.AppendChild(objRoot);

                //Add by kuladeep for maintain Claim Id/Back button functionality:Start
                objSettings = new SysSettings(connectionString, base.ClientId);//sharishkumar Jira 827
                if (objSettings.UsePaperVisionInterface)
                {       
                    AppendNode( p_objXmlOut,  objRoot, "ClaimId",out  objChild);
                    objChild.InnerText = p_objXmlIn.SelectSingleNode("//" + "ClaimId").InnerText;
                    AppendNode( p_objXmlOut,  objRoot, "FormCount",out  objChild);
                    objChild.InnerText = iCtr.ToString();
                    throw new RMAppException(Globalization.GetString("FROIAdaptor.PaperVision.Error", base.ClientId));//sharishkumar Jira 827
                }
                //Add by kuladeep for maintain Claim Id/Back button functionality:End

				for(int i=0; i<=iUpperbound; i++)
				{
					if(arrForms.GetValue(0,i)!=null || arrForms.GetValue(1,i)!=null || arrForms.GetValue(2,i)!=null)
					{
						AppendNode( p_objXmlOut, objRoot,"Form",out objChild);
						objChild.SetAttribute("FormId",Conversion.ConvertObjToStr(arrForms.GetValue(0,i)));
						objChild.SetAttribute("FormNameAndTitle",Conversion.ConvertObjToStr(arrForms.GetValue(2,i)));
						objChild.SetAttribute("FormCatDesc",Conversion.ConvertObjToStr(arrForms.GetValue(1,i)));
						iCtr++;
					}
				}
				if(iCtr==0)
				{
					objCache = new LocalCache( m_connectionString,base.ClientId);
					objRdr=DbFactory.GetDbReader(m_connectionString,"SELECT CLAIM_TYPE_CODE FROM CLAIM WHERE CLAIM_ID =" + p_objXmlIn.SelectSingleNode("//"+ "ClaimId").InnerText);
					if(objRdr.Read())
					{
						string sCode="";
						string sDesc="";
						AppendNode( p_objXmlOut, objRoot,"ClaimType",out objChild);
						objCache.GetCodeInfo(objRdr.GetInt32(0),ref sCode,ref sDesc);
						objChild.InnerText= sCode + " - " + sDesc;
					}
				}
				AppendNode( p_objXmlOut, objRoot,"ClaimId",out objChild);
				objChild.InnerText=p_objXmlIn.SelectSingleNode("//"+ "ClaimId").InnerText;
				AppendNode( p_objXmlOut, objRoot,"FormId",out objChild);	  
				AppendNode( p_objXmlOut, objRoot,"FormCount",out objChild);
                objChild.InnerText = iCtr.ToString(); 
                //Geeta : Acord Forms Mits 13482
                AppendNode( p_objXmlOut,  objRoot, "ClaimNumber",out  objChild);
                objChild.InnerText = p_objXmlIn.SelectSingleNode("//" + "ClaimNumber").InnerText;
				
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("FROIAdaptor.GetACORDForms.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
				return false;

			}
			finally
			{
				objFroi=null;
				objChild=null;
				objRoot=null;
				arrForms=null;
				if(objRdr!=null)
				{
					objRdr.Dispose();
					objRdr=null; 
				}
				if(objCache!=null)
				{
					objCache.Dispose();
					objCache=null; 
				}
                //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                objSettings = null;
                //Animesh Insertion Ends
			}
			return true;
		}


		/// Name		: InvokeACORDForms
		/// Author		: Pankaj Chowdhury
		/// Date Created: 8-Aug-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///  This function return the ACORD form.
		/// </summary>
		/// <param name="p_objXmlIn">
		/// Structure of the input Xml.
		/// <Document>
		///   <FROI>
		///    <ClaimId>
		///    </ClaimId>
		///    <FormId>
		///    </FormId>
		///    <State>
		///    </State>
		///    <DocumentPath>
		///    </DocumentPath>
		///    <AttachForm>
		///    </AttachForm>
		///    <DocStorageType>
		///    </DocStorageType>
		///   </FROI>
		/// </Document>
		/// </param>
		/// <param name="p_objXmlOut">
		/// Structure of output xml-:
		/// <Froi>
		/// <File Name="">
		/// </File>
		/// </Froi>
		/// </param>
		/// <param name="p_objErrOut">This will contain the errors occured during the processing of this function.</param>
		/// <returns>'True' if function call succeeds or 'false' if it fails.</returns>
		public bool	InvokeACORDForms(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			
            FROIForms objFroi = null; 
			string sFilePath="";
			string sName="";
			string sTitle="";
			string sPhone="";
            SysSettings objSettings = null;
            string sPDFFileName = "";
            string sClaimNo = "";
            int iClaimID;        

			try
			{
                p_objXmlOut = new XmlDocument();
                p_objErrOut = new BusinessAdaptorErrors(base.ClientId);

                //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                objSettings = new SysSettings(connectionString, base.ClientId);//sharishkumar Jira 827
                if (objSettings.UsePaperVisionInterface)
                    throw new RMAppException(Globalization.GetString("FROIAdaptor.PaperVision.Error", base.ClientId));//sharishkumar Jira 827
                //Animesh Insertion Ends
				objFroi=InitializeFROI();
				objFroi.PDFPath=m_strPDFTemplatePath + "ClaimForms\\AC\\";
				objFroi.PDFUrl= AppendSlash(p_objXmlIn.SelectSingleNode("//RequestHost").InnerText).Replace(":443/", "/").Replace(":80/", "/") + AppendSlash(m_strPDFUrlPath) + "ClaimForms/AC/";
				objFroi.OutputPath=m_strPDFOutputPath;
				SetProperties(p_objXmlIn,ref objFroi);
                objFroi.AttachForm = true;
				sName=p_objXmlIn.SelectSingleNode("//Name").InnerText;
				sTitle=p_objXmlIn.SelectSingleNode("//Title").InnerText;
				sPhone=p_objXmlIn.SelectSingleNode("//Phone").InnerText;
                iClaimID =Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//ClaimId").InnerText);
                sClaimNo = p_objXmlIn.SelectSingleNode("//ClaimNumber").InnerText;

				if(sName!="" || sTitle!="" || sPhone!="")
				{
					objFroi.SetPreparerInfo(sName, ref sTitle,ref sPhone);
				}
            
               // objSettings = new SysSettings(connectionString);
                if (objFroi.AttachForm && (objSettings.UseAcrosoftInterface||objSettings.UseMediaViewInterface))
                {
                    objFroi.AttachForm = false;
                    objFroi.DocumentPath = "";
                    sFilePath = objFroi.InvokeAcordForms(Conversion.ConvertObjToInt(p_objXmlIn.SelectSingleNode("//" + "ClaimId").InnerText, base.ClientId), Conversion.ConvertObjToInt(p_objXmlIn.SelectSingleNode("//" + "FormId").InnerText, base.ClientId));
                    objFroi.AttachForm = true;

                } // if
                else
                {
                    sFilePath = objFroi.InvokeAcordForms(Conversion.ConvertObjToInt(p_objXmlIn.SelectSingleNode("//" + "ClaimId").InnerText, base.ClientId), Conversion.ConvertObjToInt(p_objXmlIn.SelectSingleNode("//" + "FormId").InnerText, base.ClientId));
                } // else
                
                if (sFilePath != null && sFilePath.Trim() != "")
                {
                    this.MakeOutputDocumentWithFileContent("Acord", ref p_objXmlOut, sFilePath);                    
                                        
                    switch (sName)
                    {

                        case "Property Loss Notice":
                            sPDFFileName = "acord-01.pdf";
                            break;
                        case "Automobile Loss Notice":
                            sPDFFileName = "acord-02.pdf";
                            break;
                        case "General Liability Notice of Occurrence/Claim":
                            sPDFFileName = "acord-03.pdf";
                            break;
                    }

                    objFroi.AttachOutput(ref sPDFFileName, ref sFilePath, ref sClaimNo, ref iClaimID);
                    
                    if (objFroi.AttachForm && objSettings.UseAcrosoftInterface)
                    {                       
                        Claim objClaim = null;
                        DataModelFactory objDataModelFactory = new DataModelFactory(base.userLogin, base.ClientId);//sharishkumar Jira 827
                        objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                        objClaim.MoveTo(iClaimID);
                        objDataModelFactory.UnInitialize();

                        string sClaimNumber = objClaim.ClaimNumber;
                        string sEventNumber = objClaim.EventNumber;
                        string sDocTitle = "ACORD (" + sFilePath.Substring(sFilePath.IndexOf(sClaimNumber)) + ")";
                        string sAppExcpXml = "";
                        Acrosoft objAcrosoft = new Acrosoft(base.ClientId);//dvatsa-cloud

                        //rsolanki2 :  start updates for MCM mits 19200 
                        
                        Boolean bSuccess = false;
                        string sTempAcrosoftUserId = base.userLogin.LoginName;
                        string sTempAcrosoftPassword = base.userLogin.Password;

                        if (Conversion.CastToType<Boolean>(AcrosoftSection.UseCommonAcrosoftUser, out bSuccess))
                        {
                            if (AcrosoftSection.AcroUserMapSection.UserMap[base.userLogin.LoginName] != null
                                && !string.IsNullOrEmpty(AcrosoftSection.AcroUserMapSection.UserMap[base.userLogin.LoginName].RmxUser))
                            {
                                sTempAcrosoftUserId = AcrosoftSection.AcroUserMapSection.UserMap[base.userLogin.LoginName].AcrosoftUserId;
                                sTempAcrosoftPassword = AcrosoftSection.AcroUserMapSection.UserMap[base.userLogin.LoginName].AcrosoftPassword;
                            }
                            else
                            {
                                sTempAcrosoftUserId = AcrosoftSection.AcrosoftUsername;
                                sTempAcrosoftPassword = AcrosoftSection.AcrosoftUserPassword;
                            }
                        }
                        //creating folder
                        objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                            AcrosoftSection.AcrosoftAttachmentsTypeKey, sEventNumber, sClaimNumber,
                            AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                        //storing document
                        objAcrosoft.StoreObjectBuffer(sTempAcrosoftUserId, sTempAcrosoftPassword,
                            sFilePath, sDocTitle, "", sEventNumber, sClaimNumber, AcrosoftSection.AcrosoftAttachmentsTypeKey,
                            "", "", sTempAcrosoftUserId, out sAppExcpXml);
                        
                        //rsolanki2 :  end updates for MCM mits 19200 
                        ////creating folder
                        //objAcrosoft.CreateAttachmentFolder(base.userLogin.LoginName, base.userLogin.Password, AcrosoftSection.AcrosoftAttachmentsTypeKey, sEventNumber, sClaimNumber,
                        //    AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                        ////storing document
                        //objAcrosoft.StoreObjectBuffer(base.userLogin.LoginName, base.userLogin.Password, sFilePath, sDocTitle, "", sEventNumber, sClaimNumber, AcrosoftSection.AcrosoftAttachmentsTypeKey,
                        //    "", "", userLogin.LoginName, out sAppExcpXml);
                    }
                    if (objFroi.AttachForm && objSettings.UseMediaViewInterface)
                    {
                        Claim objClaim = null;
                        DataModelFactory objDataModelFactory = new DataModelFactory(base.userLogin, base.ClientId);//sharishkumar Jira 827
                        objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                        objClaim.MoveTo(iClaimID);
                        objDataModelFactory.UnInitialize();

                        string sClaimNumber = objClaim.ClaimNumber;
                        string sEventNumber = objClaim.EventNumber;
                        string sDocTitle = "ACORD (" + sFilePath.Substring(sFilePath.IndexOf(sClaimNumber)) + ")";
                        MediaView oMediaView = new MediaView();
                        Boolean bSuccess = false;
                        importList oList = new importList(sClaimNumber, "Claim", sDocTitle);
                        string sFileName = sFilePath.Substring(sFilePath.LastIndexOf(@"\") + 1);
                        string sPath = sFilePath.Substring(0, sFilePath.LastIndexOf(@"\"));
                        bSuccess = oMediaView.MMUploadDocument(sFileName, sPath, oList);
                        oMediaView.Dispose();
                    }

                    //Delete the file
                    System.IO.File.Delete(sFilePath);
                }
                else
                {
                    throw new RMAppException(Globalization.GetString("FROIAdaptor.InteropError", base.ClientId));//sharishkumar Jira 827
                }              
				
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("FROIAdaptor.InvokeACORDForms.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
				return false;

			}
			finally
			{
				objFroi=null;
                //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                objSettings = null;
                //Animesh Insertion Ends
			}
			return true;
		}


        /// <summary>
        /// added by Navdeep - Returns the Name of FDF file Prepared along with 
        /// name of the Merged file that will be prepared using Adobe Writer        
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="objClaim"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <param name="sFDFPath"></param>
        /// <param name="sPDFPath"></param>
        /// <param name="sMergePDFPath"></param>
        /// <param name="sMergePwdPDFPath"></param>
        /// <returns></returns>
        public bool InvokeAutoACORD(XmlDocument p_objXmlIn, Claim objClaim, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut, ref string sFDFPath, ref string sPDFPath, ref string sMergePDFPath, ref string sMergePwdPDFPath)
        {
            FROIForms objFroi = null;      
            int iUser = 0;
            string[] sPdfSplitPath;
            string sFormId ="0";
            try
            {
                sMergePDFPath = "";
                sMergePwdPDFPath = "";
                sFDFPath = "";
                p_objXmlOut = new XmlDocument();
                p_objErrOut = new BusinessAdaptorErrors(base.ClientId);
                objFroi = InitializeFROI();                
                objFroi.DSN = objClaim.Context.DbConnLookup.ConnectionString;
                objFroi.LoginName = objClaim.Context.RMUser.LoginName;
                iUser = objClaim.Context.RMUser.objUser.UserId;
               
                objFroi.LoginId=iUser;// Modified for Froi Migration- Mits 33585,33586,33587
                
                sFormId = p_objXmlIn.SelectSingleNode("//" + "FormId").InnerText;
                objFroi.PDFPath = m_strPDFTemplatePath + "ClaimForms\\AC\\";
                objFroi.PDFUrl = m_strPDFTemplatePath + "ClaimForms\\AC\\";
                
                objFroi.OutputPath = m_strPDFUrlPath;

                //Commented By NAVDEEP to correct the PDF template PATH Issue
                //sPDFPath = objFroi.PDFUrl;

                //Corrected PDF Path
                //sPDFPath = Riskmaster.Common.RMConfigurator.BasePath;
                //sPDFPath = sPDFPath.Replace("riskmaster", "RiskmasterUI\\UI\\pdf-forms\\");
                //sPDFPath = sPDFPath + "ClaimForms\\AC\\";


                //Corrected PDF Path
                sPDFPath = RMConfig.BasePath;
                sPDFPath = sPDFPath.ToLower();
                sPdfSplitPath = sPDFPath.Split('\\');
                sPdfSplitPath[sPdfSplitPath.GetUpperBound(0)] = sPdfSplitPath[sPdfSplitPath.GetUpperBound(0)].Replace("riskmaster", "RiskmasterUI\\UI\\pdf-forms");
                sPDFPath = "";
                foreach (string sPath in sPdfSplitPath)
                {
                    sPDFPath = sPDFPath + sPath + "\\";
                }
                sPDFPath = sPDFPath + "ClaimForms\\AC\\";

                //01/19/10 Geeta 19341  : Split architecture issue  ACORD and FROI
                if (!Directory.Exists(sPDFPath))
                {
                    objFroi.PDFUrl = AppendSlash(p_objXmlIn.SelectSingleNode("//RequestHost").InnerText).Replace(":443/", "/").Replace(":80/", "/") + AppendSlash(m_strPDFUrlPath + "/ClaimForms/AC");
                    sPDFPath = RMConfig.BasePath + "\\userdata\\froi\\";
                    DownloadPdfToServer(objFroi.PDFUrl, sFormId, objFroi.DSN, sPDFPath,true);
                }
                
                SetFROIProperties(objClaim.Context.RMDatabase.GlobalDocPath,ref p_objXmlIn, ref objFroi);
                sFDFPath = objFroi.InvokeAcordForms(Conversion.ConvertObjToInt(p_objXmlIn.SelectSingleNode("//" + "ClaimId").InnerText, base.ClientId), Conversion.ConvertObjToInt(p_objXmlIn.SelectSingleNode("//" + "FormId").InnerText, base.ClientId));
                if (sFDFPath != null && sFDFPath.Trim() != "")
                {
                    sMergePDFPath = Path.GetTempPath()  + objClaim.ClaimNumber + "_ACORD.pdf";
                    sMergePwdPDFPath = Path.GetTempPath() + objClaim.ClaimNumber + "_ACORDForm.pdf";
                    return true;                    
                }
                else
                {
                    return false;
                }


            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("FROIAdaptor.InvokeAutoACORD.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
                return false;

            }
            finally
            {
                objFroi = null;
                if (objClaim != null)
                {
                    objClaim.Dispose();
                }
            }
            return true;
        }
		#endregion

		#region Private functions
		/// Name		: MakeOutputDocumentWithFileContent
		/// Author		: Tanuj Narula
		/// Date Created: 23-Feb-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function appends the output file to the output xml document.
		/// </summary>
		/// <param name="p_sModuleName">Name of the root element of the xml document.</param>
		/// <param name="p_objXmlOut">Document to which output file to be appended.</param>
		/// <param name="p_sOutFilepath">Name of the output file.</param>
		private void MakeOutputDocumentWithFileContent(string p_sModuleName,ref XmlDocument p_objXmlOut,string p_sOutFilepath)
		{
			XmlElement objRootElement=null;
			XmlElement objElement=null;
			MemoryStream objMs=null;

           try
			{
				objRootElement=p_objXmlOut.CreateElement(p_sModuleName);
				objElement=p_objXmlOut.CreateElement("File");
                objMs = Utilities.ConvertFilestreamToMemorystream(p_sOutFilepath, base.ClientId);
				
				objElement.SetAttribute("Name",p_sOutFilepath.Substring(p_sOutFilepath.LastIndexOf(@"\")+1));
                 
				objElement.InnerText=Convert.ToBase64String(objMs.GetBuffer());
				objRootElement.AppendChild(objElement);
				p_objXmlOut.AppendChild(objRootElement);
			}
			catch(Exception p_objErr)
			{
				throw p_objErr;
			}
			finally
			{
				objRootElement=null;
			    objElement=null;
			   if(objMs!=null)
			   {
				   objMs.Close();
				   objMs=null;
			   }
			}
		}

		/// Name		: MedPdf3500
		/// Date Created: 22-Sep-2006		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function will print the MedWatch Voluntary report and generate the .FDF file.
		/// </summary>
		/// <param name="p_objXmlIn"></param>
		/// <param name="p_objXmlOut"></param>
		/// <param name="p_objErrOut"></param>
		/// <returns></returns>
		public bool	MedPdf3500(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			
			FROIForms objMedWatch = null; 
			string sFilePath="";
            //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
            SysSettings objSettings = null;
		    //Animesh Insertion Ends
			try
			{
                p_objXmlOut = new XmlDocument();
                p_objErrOut = new BusinessAdaptorErrors(base.ClientId);

                //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                objSettings = new SysSettings(connectionString,base.ClientId);//sharishkumar Jira 827
                if (objSettings.UsePaperVisionInterface)
                    throw new RMAppException(Globalization.GetString("FROIAdaptor.PaperVision.Error", base.ClientId));//sharishkumar Jira 827
                //Animesh Insertion Ends
				objMedWatch=this.InitializeFROI();
                objMedWatch.PDFPath = m_strPDFTemplatePath  ;
				objMedWatch.PDFUrl= AppendSlash(p_objXmlIn.SelectSingleNode("//RequestHost").InnerText).Replace(":443/", "/") + AppendSlash(m_strPDFUrlPath);
               
                //Geeta Mits 18944 : Error is showing up when Print Medwatch Button is clicked in tool bar                
                objMedWatch.OutputPath = m_strPDFOutputPath;
				this.SetPropertiesMed(ref objMedWatch);

                sFilePath = objMedWatch.InvokeMedWatch(Conversion.ConvertObjToInt(p_objXmlIn.SelectSingleNode(@"//EventId").InnerText, base.ClientId), 2);
				if(sFilePath!=null && sFilePath.Trim()!="")
				{
					this.MakeOutputDocumentWithFileContent("MedWatch",ref p_objXmlOut,sFilePath);
					//Delete the file
					System.IO.File.Delete(sFilePath);
				}
				else
				{
                    throw new RMAppException(Globalization.GetString("FROIAdaptor.InteropError", base.ClientId));//sharishkumar Jira 827
				}
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("FROIAdaptor.MedPdf3500.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
				return false;
		
			}
			finally
			{
				objMedWatch=null;
                //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                objSettings = null;
                //Animesh Insertion Ends
			}
			return true;
		}

		/// Name		: MedPdf3500a
		/// Date Created: 22-Sep-2006		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function will print the MedWatch Mandatory report and generate the .FDF file.
		/// </summary>
		/// <param name="p_objXmlIn"></param>
		/// <param name="p_objXmlOut"></param>
		/// <param name="p_objErrOut"></param>
		/// <returns></returns>
		public bool	MedPdf3500a(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
            FROIForms objMedWatch = null;
			string sFilePath="";
            //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
            SysSettings objSettings = null;
            //Animesh Insertion ends
			try
			{
                p_objXmlOut = new XmlDocument();
                p_objErrOut = new BusinessAdaptorErrors(base.ClientId);
                //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                objSettings = new SysSettings(connectionString, base.ClientId);//sharishkumar Jira 827
                if (objSettings.UsePaperVisionInterface)
                    throw new RMAppException(Globalization.GetString("FROIAdaptor.PaperVision.Error", base.ClientId));//sharishkumar Jira 827
                //Animesh Insertion Ends
				objMedWatch=this.InitializeFROI();
				objMedWatch.PDFPath=m_strPDFTemplatePath;
				
				objMedWatch.PDFUrl= AppendSlash(p_objXmlIn.SelectSingleNode("//RequestHost").InnerText).Replace(":443/", "/") + AppendSlash(m_strPDFUrlPath);
				this.SetPropertiesMed(ref objMedWatch);

                sFilePath = objMedWatch.InvokeMedWatch(Conversion.ConvertObjToInt(p_objXmlIn.SelectSingleNode(@"//EventId").InnerText, base.ClientId), 1);
				if(sFilePath!=null && sFilePath.Trim()!="")
				{
					this.MakeOutputDocumentWithFileContent("MedWatch",ref p_objXmlOut,sFilePath);
					//Delete the file
					System.IO.File.Delete(sFilePath);
				}
				else
				{
                    throw new RMAppException(Globalization.GetString("FROIAdaptor.InteropError", base.ClientId));//sharishkumar Jira 827
				}
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("FROIAdaptor.MedPdf3500a.Error", base.ClientId), BusinessAdaptorErrorType.Error);//sharishkumar Jira 827
				return false;

			}
			finally
			{
				objMedWatch=null;
                //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                objSettings = null;
                //Animesh Insertion Ends
			}
			return true;
		}


		/// Name		: InitializeFROIInterOp
		/// Author		: Tanuj Narula
		/// Date Created: 23-Feb-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function instanstiate FROI interop class.
		/// </summary>
		/// <returns>Instance of FROI interop class.</returns>
		private FROIForms InitializeFROI()
		{
            FROIForms objFroi = null;
			int iUserId;
			try
			{
                objFroi = new FROIForms(this.m_userLogin, base.ClientId);//sharishkumar Jira 827
				objFroi.DSN=base.connectionString;
				iUserId=base.userID;
			
                objFroi.LoginId= iUserId;// Modified for Froi Migration- Mits 33585,33586,33587
				objFroi.LoginName=base.loginName;
			}

			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			return objFroi;
		}
		/// Name		: AppendNode
		/// Author		: Tanuj Narula
		/// Date Created: 23-Feb-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function appends node to a given node.
		/// </summary>
		/// <param name="p_objDoc">Xml document to which node to be appended.</param>
		/// <param name="p_objParent">Parent node to which a new node to be appended.</param>
		/// <param name="p_sChildNodeName">Name of the name to be appended.</param>
		/// <param name="p_objChildNode">Node to be appended.</param>
		private void AppendNode(XmlDocument p_objDoc,  XmlElement p_objParent, string p_sChildNodeName,out XmlElement p_objChildNode)
        {
            try
            {
                XmlElement objChild = p_objDoc.CreateElement(p_sChildNodeName);
                p_objParent.AppendChild(objChild);
                p_objChildNode = objChild;

            }
            catch (Exception p_objException)
            {
                throw p_objException;
            }

        }

     

		/// Name		: SetProperties
		/// Author		: Tanuj Narula
		/// Date Created: 23-Feb-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// 09 Jan 06  *   Added check for global Attachform flag *Sumit
		///************************************************************
		/// <summary>
		/// This function will set some common properties of FROI interop object.
		/// </summary>
		/// <param name="p_objDoc">Document containing the various properties to set.</param>
		/// <param name="p_objFroi">FROI interop object on which properties will be set.</param>
		private void SetProperties(XmlDocument p_objDoc, ref FROIForms p_objFroi)
		{
			try
			{
				
				p_objFroi.OutputPath=m_strPDFOutputPath;
				// Never filled in by Orbeon code.    p_objFroi.DocumentPath=p_objDoc.SelectSingleNode("//"+ "DocumentPath").InnerText;

                //Start rsushilaggar Date 03/28/2011
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    // use StoragePath set for the user
                    p_objFroi.DocumentPath = this.userLogin.DocumentPath;
                    p_objFroi.DocStorageType = (short)0;
                }
                else
                {

                    p_objFroi.DocumentPath = this.userLogin.objRiskmasterDatabase.GlobalDocPath;  // Always derives from security. Should not be passed in.
                    //commenting this implementation
                    //p_objFroi.AttachForm=Conversion.ConvertStrToBool(p_objDoc.SelectSingleNode("//"+ "AttachForm").InnerText);
                    if (p_objDoc.SelectSingleNode("//" + "DocStorageType").InnerText.Trim() != "")
                    {
                        p_objFroi.DocStorageType = (short)m_userLogin.objRiskmasterDatabase.DocPathType;
                    }
                }
                //End rsushilaggar
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
		}

        /// <summary>
        /// Added by Navdeep
        /// Doc Path was passed from InvokeAutoFROI and InvokeAutoAcord function. 
        /// </summary>
        /// <param name="sDocPath"></param>
        /// <param name="p_objDoc"></param>
        /// <param name="p_objFroi"></param>
	        private void SetFROIProperties(string sDocPath, ref XmlDocument p_objDoc, ref FROIForms p_objFroi)
        {
            try
            {

                p_objFroi.OutputPath = m_strPDFOutputPath;
                // Never filled in by Orbeon code.    p_objFroi.DocumentPath=p_objDoc.SelectSingleNode("//"+ "DocumentPath").InnerText;
                p_objFroi.DocumentPath = sDocPath;  // Always derives from security. Should not be passed in.
                //commenting this implementation
                //p_objFroi.AttachForm=Conversion.ConvertStrToBool(p_objDoc.SelectSingleNode("//"+ "AttachForm").InnerText);
                if (p_objDoc.SelectSingleNode("//" + "DocStorageType").InnerText.Trim() != "")
                {
                    p_objFroi.DocStorageType = (short)m_userLogin.objRiskmasterDatabase.DocPathType;
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw p_objException;
            }
        }
		private void SetPropertiesMed(ref FROIForms p_objFroi)
		{
			try
			{
				
				p_objFroi.OutputPath=m_strPDFOutputPath;
				// Never filled in by Orbeon code.    p_objFroi.DocumentPath=p_objDoc.SelectSingleNode("//"+ "DocumentPath").InnerText;
				p_objFroi.DocumentPath=this.userLogin.objRiskmasterDatabase.GlobalDocPath;  // Always derives from security. Should not be passed in.
				//commenting this implementation
				//p_objFroi.AttachForm=Conversion.ConvertStrToBool(p_objDoc.SelectSingleNode("//"+ "AttachForm").InnerText);
				p_objFroi.DocStorageType=(short)m_userLogin.objRiskmasterDatabase.DocPathType ;
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
		}

		private string AppendSlash(string p_surl)
		{
			if (!p_surl.EndsWith("/"))
				p_surl = p_surl + "/";
			return p_surl;
		}

		/// <summary>
		///  Check for if the required transaction mapping and From/To date are available
		/// </summary>
		/// <param name="iClaimID"></param>
		/// <param name="iFormID"></param>
		/// <param name="sState"></param>
		/// <returns>Error message</returns>
		private string CheckTransMappings(int iClaimID, int iFormID, string sStateID)
		{
			DbReader objReader=null;
			string sSQL = "";
			string sPDFFileName = "";
			int iStateRowID = 0;
			string sTransBenefitIDs = "";
			string sFailedTransBenefitIDs = "";
			string sFailedMappedNames = "";
			string sError = "";

			try
			{
				//Get From Name
				sSQL = "SELECT FILE_NAME,FORM_NAME FROM WCP_FORMS WHERE FORM_ID = " + iFormID.ToString();
				objReader = DbFactory.GetDbReader(base.connectionString, sSQL);
				if(objReader.Read())
					sPDFFileName = objReader.GetString("FILE_NAME").ToUpper();	
				objReader.Close();

				//Get State row id
                //sSQL = "SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = '" + sStateID + "'";
                //objReader = DbFactory.GetDbReader(base.connectionString, sSQL);
                //if(objReader.Read())
                //    iStateRowID = objReader.GetInt("STATE_ROW_ID");	
                //objReader.Close();
                bool bStateRowID = int.TryParse(sStateID, out iStateRowID);
				//Check for transaction mappings. The file names here are not a complete list, and more
				// should be added if other files needs to be checked in the future .
				switch( sPDFFileName )
				{
					//Jurisdiction = CA
					case "CA_RU-105_20030101.PDF":
						if( CheckSingleTransMapping(sPDFFileName, iStateRowID, 8) == -1 )
						{
							sFailedTransBenefitIDs += ",8";
						}
						if( CheckSingleTransMapping(sPDFFileName, iStateRowID, 9) == -1 )
						{
							sFailedTransBenefitIDs += ",9";
						}
						if( CheckSingleTransMapping(sPDFFileName, iStateRowID, 10) == -1 )
						{
							sFailedTransBenefitIDs += ",10";
						}
						if( CheckSingleTransMapping(sPDFFileName, iStateRowID, 11) == -1 )
						{
							sFailedTransBenefitIDs += ",11";
						}
						if( CheckSingleTransMapping(sPDFFileName, iStateRowID, 12) == -1 )
						{
							sFailedTransBenefitIDs += ",12";
						}
						if( CheckSingleTransMapping(sPDFFileName, iStateRowID, 13) == -1 )
						{
							sFailedTransBenefitIDs += ",13";
						}

						if( CheckSingleTransMapping(sPDFFileName, iStateRowID, 16) == -1 )
						{
							sFailedTransBenefitIDs += ",16";
						}
						if( CheckSingleTransMapping(sPDFFileName, iStateRowID, 17) == -1 )
						{
							sFailedTransBenefitIDs += ",17";
						}
						//TR:1016 04/28/2006  code commented By Nietsh
						//sTransBenefitIDs = "8,9,10,11,12,13,16,17";  
						break;
					case "DWC500A.PDF":
						if( CheckSingleTransMapping(sPDFFileName, iStateRowID, 14) == -1 )
						{
							sFailedTransBenefitIDs += ",14";
						}
						if( CheckSingleTransMapping(sPDFFileName, iStateRowID, 15) == -1 )
						{
							sFailedTransBenefitIDs += ",15";
						}
						if( CheckSingleTransMapping(sPDFFileName, iStateRowID, 65) == -1 )
						{
							sFailedTransBenefitIDs += ",65";
						}
						sTransBenefitIDs = "14,15,65";
						break;
					case "DWC500B.PDF":
						if( CheckSingleTransMapping(sPDFFileName, iStateRowID, 4) == -1 )
						{
							sFailedTransBenefitIDs += ",4";
						}
						if( CheckSingleTransMapping(sPDFFileName, iStateRowID, 5) == -1 )
						{
							sFailedTransBenefitIDs += ",5";
						}
						if( CheckSingleTransMapping(sPDFFileName, iStateRowID, 6) == -1 )
						{
							sFailedTransBenefitIDs += ",6";
						}
						if( CheckSingleTransMapping(sPDFFileName, iStateRowID, 7) == -1 )
						{
							sFailedTransBenefitIDs += ",7";
						}
						if( CheckSingleTransMapping(sPDFFileName, iStateRowID, 16) == -1 )
						{
							sFailedTransBenefitIDs += ",16";
						}
						sTransBenefitIDs = "4,5,6,7,16";
						break;
					case "DWC500M.PDF":
						if( CheckSingleTransMapping(sPDFFileName, iStateRowID, 16) == -1 )
						{
							sFailedTransBenefitIDs += ",16";
						}
						if( CheckSingleTransMapping(sPDFFileName, iStateRowID, 17) == -1 )
						{
							sFailedTransBenefitIDs += ",17";
						}
						if( CheckSingleTransMapping(sPDFFileName, iStateRowID, 71) == -1 )
						{
							sFailedTransBenefitIDs += ",71";
						}
						sTransBenefitIDs = "16,17,71";
						break;
					case "DWC500O.PDF":
						if( CheckSingleTransMapping(sPDFFileName, iStateRowID, 16) == -1 )
						{
							sFailedTransBenefitIDs += ",16";
						}
						if( CheckSingleTransMapping(sPDFFileName, iStateRowID, 17) == -1 )
						{
							sFailedTransBenefitIDs += ",17";
						}
						sTransBenefitIDs = "16,17";
						break;
				}

				// Get the failed transaction mappings
				if( sFailedTransBenefitIDs != "" )
				{
					if( sFailedTransBenefitIDs.Substring(0,1) == "," )
						sFailedTransBenefitIDs = sFailedTransBenefitIDs.Substring(1);
					sSQL = "SELECT JURIS_BENEFIT_DESC FROM WCP_BENEFIT_LKUP WHERE " +
						"BENEFIT_LKUP_ID IN ( " + sFailedTransBenefitIDs + ")";

					objReader = DbFactory.GetDbReader(base.connectionString, sSQL);
					while( objReader.Read() )
					{
						if( sFailedMappedNames != "")
							sFailedMappedNames += "; ";
						sFailedMappedNames += objReader.GetString("JURIS_BENEFIT_DESC");
					}
					sError = " Failed to find " + sStateID + " Transaction Mapping for:" + sFailedMappedNames;
				}
				else //Check for From/To Date of the related transcation
				{
					if( sTransBenefitIDs != "")
						sError = CheckFromToDate4WCForm(iClaimID, iStateRowID, sTransBenefitIDs);
				}
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("ExecutiveSummary.GenerateEventSumm.ErrorGenerate", base.ClientId), p_objException);//sharishkumar Jira 827
			}
			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
			}

			return sError;
		}


		/// <summary>
		/// Check if the transaction mapping is there.
		/// </summary>
		/// <param name="sPDFFileName"></param>
		/// <param name="iStateRowID"></param>
		/// <param name="iBenefitID"></param>
		/// <returns></returns>
		private int CheckSingleTransMapping(string sPDFFileName, int iStateRowID, int iBenefitID)
		{
			int iReturn = 0;
			DbReader objReader = null;
			string sSQL = "";

			try
			{
				sSQL = "SELECT DISTINCT CODE_ID FROM WCP_TRANS_TYPES WHERE STATE_ROW_ID = " +
					iStateRowID.ToString() + " AND BENEFIT_LKUP_ID = " + iBenefitID.ToString();

				objReader = DbFactory.GetDbReader(base.connectionString, sSQL);
				if( !objReader.Read() )
					iReturn = -1;
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("FROIAdapter.CheckSingleTransMapping", base.ClientId), p_objException);//sharishkumar Jira 827
			}
			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
			}

			return iReturn;
		}

		/// <summary>
		/// Check if the From/To date are available for these benefit related transactions
		/// </summary>
		/// <param name="iClaimID"></param>
		/// <param name="iStateRowID"></param>
		/// <param name="sBenefitIDs"></param>
		/// <returns></returns>
		private string CheckFromToDate4WCForm(int iClaimID, int iStateRowID, string sBenefitIDs)
		{
			DbReader objReader = null;
			string sSQL = "";
			string sControlNumbers = "";
			string sError = "";

			try
			{
				sSQL = "SELECT FUNDS.CTL_NUMBER, FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE," +
					"FUNDS_TRANS_SPLIT.AMOUNT, FUNDS_TRANS_SPLIT.FROM_DATE, " +
					"FUNDS_TRANS_SPLIT.TO_DATE FROM FUNDS, FUNDS_TRANS_SPLIT " +
					"WHERE FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID AND " +
					"VOID_FLAG = 0 AND FUNDS.CLAIM_ID = " + iClaimID.ToString() + " AND " +
					"(FUNDS_TRANS_SPLIT.FROM_DATE = '' OR FUNDS_TRANS_SPLIT.FROM_DATE IS NULL) AND " +
					"(FUNDS_TRANS_SPLIT.TO_DATE = '' OR FUNDS_TRANS_SPLIT.TO_DATE IS NULL) AND " +
					"FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE IN " +
					"(SELECT DISTINCT CODE_ID FROM WCP_TRANS_TYPES WHERE STATE_ROW_ID = " + iStateRowID.ToString() + 
					" AND BENEFIT_LKUP_ID IN (" + sBenefitIDs + ")) ORDER BY TRANS_TYPE_CODE,TO_DATE";

				objReader = DbFactory.GetDbReader(base.connectionString, sSQL);
				while( objReader.Read() )
				{
					if( sControlNumbers != "")
						sControlNumbers += ", ";
					//Nitesh 04/28/2006 TR: 1016 
					//sControlNumbers = objReader.GetString("CTL_NUMBER");
					sControlNumbers += objReader.GetString("CTL_NUMBER"); 
				}
				if( sControlNumbers != "")
                    sError = "The following funds transactions have invalid or missing dates: " + sControlNumbers;
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("FROIAdapter.CheckFromToDate4WCForm", base.ClientId), p_objException);//sharishkumar Jira 827
			}
			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
			}

			return sError;
		}


      /// <summary>
      /// Geeta 19341  : Split architecture issue  ACORD and FROI
      /// </summary>
      /// <param name="sPdfUrl"></param>
      /// <param name="sFormId"></param>
      /// <param name="sConnectionString"></param>
      /// <param name="sPDFPath"></param>
      /// <param name="bAutoAcord"></param>
      /// <returns></returns> 
      private string DownloadPdfToServer(string sPdfUrl, string sFormId, string sConnectionString, string sPDFPath,bool bAutoAcord)
      {
            DbReader objReader=null;
			string sSQL = "";
			string sPDFFileName = "";            
		
            try
            {
                WebClient objClient = new WebClient();
                
                //Get PDF Form Name for FROI Or ACORD
                if (!bAutoAcord)
                {
                    sSQL = "SELECT PDF_FILE_NAME,FORM_NAME FROM JURIS_FORMS WHERE FORM_ID = " + sFormId;
                }
                else
                {
                    sSQL = "SELECT FILE_NAME FROM CL_FORMS WHERE FORM_ID = " + sFormId;
                }

                objReader = DbFactory.GetDbReader(sConnectionString, sSQL);
                if (objReader.Read())
                    sPDFFileName = objReader.GetString(0);
                objReader.Close();

                sPdfUrl = sPdfUrl + sPDFFileName;
                sPDFPath = sPDFPath + sPDFFileName;
                objClient.DownloadFile(sPdfUrl, sPDFPath);
                
                objClient.Dispose();
            }
          
            catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			
            catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("FROIAdaptor.InvokeAutoFROI.Error", base.ClientId), p_objException);//sharishkumar Jira 827
			}
        
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();                    
                }                    
            }
            return sPDFPath;  
        }

		#endregion
	}
}
