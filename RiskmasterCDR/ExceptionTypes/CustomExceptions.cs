using System;
using System.Runtime.Serialization;
using System.Threading;

namespace Riskmaster
{

    /// <summary>
    /// Contains the Riskmaster Base exception classes.  
    ///  Note that they are Serializable for proper Remoting behavior and MUST 
    ///  be defined at both ends of the Remoting channel.  Otherwise the Runtime will 
    ///  return "file not found" errors for missing exception types.
    ///  They are also extended with complete logging information.
    /// </summary>

    /// </summary>
    namespace ExceptionTypes
    {
        //BASE RISKMASTER EXCEPTION CLASS
        [Serializable]
        public class RMAppException : ApplicationException
        {

            //PROPERTY STORAGE
            string m_MachineName;
            DateTime m_DateTimeCaught;
            string m_ExceptionTypeName;
            // JP 10.07.2005   Very expensive and not used.   string m_CallStack;
            string m_AppDomainName;
            string m_AssemblyFullName;
            int m_ThreadId;
            string m_ThreadPrincipalName;

            // CONSTRUCTORS
            public RMAppException()
                : base()
            {
                Initialize();
            }
            public RMAppException(string Message)
                : base(Message)
            {
                Initialize();
            }
            public RMAppException(string Message, Exception Inner)
                : base(Message, Inner)
            {
                Initialize();
            }
            //Populate our extra default properties (used for full logging.)
            protected void Initialize()
            {
                m_MachineName = Environment.MachineName;
                m_DateTimeCaught = DateTime.Now;
                m_ExceptionTypeName = this.GetType().FullName;
                // JP 10.07.2005   Very expensive and not used.   m_CallStack = Environment.StackTrace;
                m_AppDomainName = System.AppDomain.CurrentDomain.FriendlyName;
                m_AssemblyFullName = (System.Reflection.Assembly.GetEntryAssembly() == null) ? "Hosted CLR" : System.Reflection.Assembly.GetEntryAssembly().FullName;
                m_ThreadId = Thread.CurrentThread.ManagedThreadId;
                m_ThreadPrincipalName = System.Threading.Thread.CurrentPrincipal.Identity.Name;
            }

            // SERIALIZATION SUPPORT
            protected RMAppException(SerializationInfo Info, StreamingContext Context)
                : base(Info, Context)
            {
                m_MachineName = Info.GetString("MachineName");
                m_DateTimeCaught = Info.GetDateTime("DateTimeCaught");
                m_ExceptionTypeName = Info.GetString("ExceptionTypeName");
                // JP 10.07.2005   Very expensive and not used.m_CallStack = Info.GetString("CallStack");
                m_AppDomainName = Info.GetString("AppDomainName");
                m_AssemblyFullName = Info.GetString("AssemblyFullName");
                m_ThreadId = Info.GetInt32("ThreadId");
                m_ThreadPrincipalName = Info.GetString("ThreadPrincipalName");
            }
            public override void GetObjectData(SerializationInfo Info, StreamingContext Context)
            {
                Info.AddValue("MachineName", m_MachineName);
                Info.AddValue("DateTimeCaught", m_DateTimeCaught);
                Info.AddValue("ExceptionTypeName", m_ExceptionTypeName);
                // JP 10.07.2005   Very expensive and not used.   Info.AddValue("CallStack", m_CallStack);
                Info.AddValue("AppDomainName", m_AppDomainName);
                Info.AddValue("AssemblyFullName", m_AssemblyFullName);
                Info.AddValue("ThreadId", m_ThreadId);
                Info.AddValue("ThreadPrincipalName", m_ThreadPrincipalName);
                base.GetObjectData(Info, Context);
            }
            //CUSTOM PROPERTIES
            public string MachineName
            {
                get { return m_MachineName; }
            }
            public DateTime DateTimeCaught
            {
                get { return m_DateTimeCaught; }
            }
            public string ExceptionTypeName
            {
                get { return m_ExceptionTypeName; }
            }
            // JP 10.07.2005   Very expensive and not used.  public  string CallStack
            // JP 10.07.2005   Very expensive and not used.{
            // JP 10.07.2005   Very expensive and not used.	get{return m_CallStack;}
            // JP 10.07.2005   Very expensive and not used.}
            public int ThreadId
            {
                get { return m_ThreadId; }
            }
            public string ThreadPrincipalName
            {
                get { return ThreadPrincipalName; }
            }
            public string AppDomainName
            {
                get { return m_AppDomainName; }
            }
            public string AssemblyFullName
            {
                get { return m_AssemblyFullName; }
            }
        }

        // DERIVED CUSTOM EXCEPTION CLASSES
        [Serializable]
        public class DTGCryptException : RMAppException
        {
            public DTGCryptException()
            {
            }

            public DTGCryptException(string sMsg)
                : base(sMsg)
            {
            }

            public DTGCryptException(string sMsg, Exception inner)
                : base(sMsg, inner)
            {
            }

        }

        [Serializable]
        public class InvalidUserNameOrPasswordException : System.Security.Authentication.InvalidCredentialException
        {
            public InvalidUserNameOrPasswordException()
            {
            }
            public InvalidUserNameOrPasswordException(string message)
                : base(message)
            {
            }
            public InvalidUserNameOrPasswordException(string message, Exception inner)
                : base(message, inner)
            {
            }
        }
        [Serializable]
        public class PermissionExpiredException : RMAppException
        {
            public PermissionExpiredException()
            {
            }
            public PermissionExpiredException(string message)
                : base(message)
            {
            }
            public PermissionExpiredException(string message, Exception inner)
                : base(message, inner)
            {
            }
        }
        [Serializable]
        public class InvalidChecksumException : RMAppException
        {
            public InvalidChecksumException()
            {
            }
            public InvalidChecksumException(string message)
                : base(message)
            {
            }
            public InvalidChecksumException(string message, Exception inner)
                : base(message, inner)
            {
            }
        }
        [Serializable]
        public class OrgSecAccessViolationException : RMAppException
        {
            public OrgSecAccessViolationException()
            {
            }
            public OrgSecAccessViolationException(string message)
                : base(message)
            {
            }
            public OrgSecAccessViolationException(string message, Exception inner)
                : base(message, inner)
            {
            }
        }
        [Serializable]
        public class LicenseCountExceededException : RMAppException
        {
            public LicenseCountExceededException()
            {
            }
            public LicenseCountExceededException(string message)
                : base(message)
            {
            }
            public LicenseCountExceededException(string message, Exception inner)
                : base(message, inner)
            {
            }
        }
        [Serializable]
        public class ReferentialInegrityException : RMAppException
        {
            public ReferentialInegrityException()
            {
            }
            public ReferentialInegrityException(string message)
                : base(message)
            {
            }
            public ReferentialInegrityException(string message, Exception inner)
                : base(message, inner)
            {
            }
        }

        [Serializable]
        public class InitializationException : RMAppException
        {
            public InitializationException()
            {
            }
            public InitializationException(string message)
                : base(message)
            {
            }
            public InitializationException(string message, Exception inner)
                : base(message, inner)
            {
            }
        }
        [Serializable]
        public class DataModelException : RMAppException
        {
            public DataModelException()
            {
            }
            public DataModelException(string message)
                : base(message)
            {
            }
            public DataModelException(string message, Exception inner)
                : base(message, inner)
            {
            }
        }

        [Serializable]
        public class ReadOnlyException : DataModelException
        {
            public ReadOnlyException()
            {
            }
            public ReadOnlyException(string message)
                : base(message)
            {
            }
            public ReadOnlyException(string message, Exception inner)
                : base(message, inner)
            {
            }
        }
        [Serializable]
        public class NavigationLockedException : DataModelException
        {
            public NavigationLockedException()
            {
            }
            public NavigationLockedException(string message)
                : base(message)
            {
            }
            public NavigationLockedException(string message, Exception inner)
                : base(message, inner)
            {
            }
        }
        //For any of the login exception.Added by Tanuj Narula on 27/5/2004
        [Serializable]
        public class LoginException : System.Security.Authentication.AuthenticationException
        {
            public LoginException()
            {
            }
            public LoginException(string message)
                : base(message)
            {
            }
            public LoginException(string message, Exception inner)
                : base(message, inner)
            {
            }
        }
        //Excepton regarding security settings.Added by Tanuj Narula on 27/5/2004
        [Serializable]
        public class SecuritySettingException : RMAppException
        {
            public SecuritySettingException()
            {
            }
            public SecuritySettingException(string message)
                : base(message)
            {
            }
            public SecuritySettingException(string message, Exception inner)
                : base(message, inner)
            {
            }
        }
        //Exception regarding the license check failed.Added by Tanuj Narula on 27/5/2004
        [Serializable]
        public class LicenseCheckFailedException : RMAppException
        {
            public LicenseCheckFailedException()
            {
            }
            public LicenseCheckFailedException(string message)
                : base(message)
            {
            }
            public LicenseCheckFailedException(string message, Exception inner)
                : base(message, inner)
            {
            }
        }
        //Exception regarding the Date Not Y2KCompliant.Added by Tanuj Narula on 27/5/2004
        [Serializable]
        public class DateNotY2KCompliantException : RMAppException
        {
            public DateNotY2KCompliantException()
            {
            }
            public DateNotY2KCompliantException(string message)
                : base(message)
            {
            }
            public DateNotY2KCompliantException(string message, Exception inner)
                : base(message, inner)
            {
            }
        }
        //Exception regarding the interactive logon request.Added by Tanuj Narula on 27/5/2004
        [Serializable]
        public class ProcDeniedInteractiveLogonException : RMAppException
        {
            public ProcDeniedInteractiveLogonException()
            {
            }
            public ProcDeniedInteractiveLogonException(string message)
                : base(message)
            {
            }
            public ProcDeniedInteractiveLogonException(string message, Exception inner)
                : base(message, inner)
            {
            }
        }

        /// <summary>
        /// Collects collection related exceptions
        /// </summary>
        [Serializable]
        public class CollectionException : RMAppException
        {
            public CollectionException()
            {
            }
            public CollectionException(string message)
                : base(message)
            {
            }
            public CollectionException(string message, Exception inner)
                : base(message, inner)
            {
            }
        }

        /// <summary>
        /// Data with invalid length
        /// </summary>
        [Serializable]
        public class InvalidLengthException : RMAppException
        {
            public InvalidLengthException()
            {
            }
            public InvalidLengthException(string message)
                : base(message)
            {
            }
            public InvalidLengthException(string message, Exception inner)
                : base(message, inner)
            {
            }
        }
        /// <summary>
        /// Collects scripting related exceptions
        /// </summary>
        [Serializable]
        public class ScriptException : RMAppException
        {
            public ScriptException()
            {
            }
            public ScriptException(string message)
                : base(message)
            {
            }
            public ScriptException(string message, Exception inner)
                : base(message, inner)
            {
            }
        }

        /// <summary>
        /// Collects file upload related exception. Added by Atul Patel 07/02/2004.
        /// </summary>
        [Serializable]
        public class FileInputOutputException : System.IO.IOException
        {
            public FileInputOutputException()
            {
            }
            public FileInputOutputException(string p_sMessage)
                : base(p_sMessage)
            {
            }
            public FileInputOutputException(string p_sMessage, Exception p_objInner)
                : base(p_sMessage, p_objInner)
            {
            }
        }
        /// <summary>
        /// This exception will be thrown if one is trying to access persistable domain object 
        /// with the key which no more exists in database.Added by Tanuj Narula on 04-July-2004.
        /// </summary>
        [Serializable]
        public class RecordNotFoundException : DataModelException
        {
            public RecordNotFoundException()
            {
            }
            public RecordNotFoundException(string p_sMessage)
                : base(p_sMessage)
            {
            }
            public RecordNotFoundException(string p_sMessage, Exception p_objInner)
                : base(p_sMessage, p_objInner)
            {
            }
        }
        /// <summary>
        /// This exception will be thrown while checking the object's maximum capacity for data storage.
        /// Added by Tanuj Narula on 04-July-2004.
        /// </summary>
        [Serializable]
        public class OverFlowException : DataModelException
        {
            public OverFlowException()
            {
            }
            public OverFlowException(string p_sMessage)
                : base(p_sMessage)
            {
            }
            public OverFlowException(string p_sMessage, Exception p_objInner)
                : base(p_sMessage, p_objInner)
            {
            }
        }
        /// <summary>
        /// This exception will be thrown if some Xml related exception occurs.
        /// Added by Tanuj Narula on 04-July-2004.
        /// </summary>
        [Serializable]
        public class XmlOperationException : RMAppException
        {
            public XmlOperationException()
            {
            }
            public XmlOperationException(string p_sMessage)
                : base(p_sMessage)
            {
            }
            public XmlOperationException(string p_sMessage, Exception p_objInner)
                : base(p_sMessage, p_objInner)
            {
            }
        }
        /// <summary>
        /// Handles the exceptions generated while accesing the windows registry.
        /// Added by Aashish Bhateja on 04-July-2004.
        /// </summary>
        [Serializable]
        public class RegistryOperationException : RMAppException
        {

            public RegistryOperationException()
            {
            }
            public RegistryOperationException(string p_sMessage)
                : base(p_sMessage)
            {
            }
            public RegistryOperationException(string p_sMessage, Exception p_objInner)
                : base(p_sMessage, p_objInner)
            {
            }
        }
        /// <summary>
        /// This exception will be thrown if some invalid parameter value is passed to a method		 
        /// Added by Neelima on 16-August-2004.
        /// </summary>
        [Serializable]
        public class InvalidValueException : RMAppException
        {
            public InvalidValueException()
            {
            }
            public InvalidValueException(string p_sMessage)
                : base(p_sMessage)
            {
            }
            public InvalidValueException(string p_sMessage, Exception p_objInner)
                : base(p_sMessage, p_objInner)
            {
            }
        }
        /// <summary>
        /// Handles the exceptions generated while string manipulations.
        /// Added by Sumeet on 24-August-2004.
        /// </summary>
        [Serializable]
        public class StringException : RMAppException
        {
            public StringException()
            {
            }
            public StringException(string p_sMessage)
                : base(p_sMessage)
            {
            }
            public StringException(string p_sMessage, Exception p_objInner)
                : base(p_sMessage, p_objInner)
            {
            }
        }

        /// <summary>
        /// Handles the exceptions generated while initializing the connection string.
        /// Invalid Login information is input.
        /// Added by Neelima on 6-October-2004.
        /// </summary>
        [Serializable]
        public class ConnectionStringInitializationException : RMAppException
        {
            public ConnectionStringInitializationException()
            {
            }
            public ConnectionStringInitializationException(string p_sMessage)
                : base(p_sMessage)
            {
            }
            public ConnectionStringInitializationException(string p_sMessage, Exception p_objInner)
                : base(p_sMessage, p_objInner)
            {
            }
        }
        /// <summary>
        /// Handles the exceptions that would be raised while generating the executive summary report.		
        /// Added by Neelima on 6-October-2004.
        /// </summary>		
        public class ExecSummaryReportEngineException : RMAppException
        {
            public ExecSummaryReportEngineException()
            {
            }
            public ExecSummaryReportEngineException(string p_sMessage)
                : base(p_sMessage)
            {
            }
            public ExecSummaryReportEngineException(string p_sMessage, Exception p_objInner)
                : base(p_sMessage, p_objInner)
            {
            }
        }
        /// <summary>
        /// Handles the exceptions that would be generated whenever any invalid 
        /// criterion is encountered for the report.		
        /// Added by Neelima on 6-October-2004.
        /// </summary>		
        public class InvalidCriteriaException : RMAppException
        {
            public InvalidCriteriaException()
            {
            }
            public InvalidCriteriaException(string p_sMessage)
                : base(p_sMessage)
            {
            }
            public InvalidCriteriaException(string p_sMessage, Exception p_objInner)
                : base(p_sMessage, p_objInner)
            {
            }
        }
        /// <summary>
        /// Handles the exceptions that would be generated whenever report generation gets aborted.
        /// Added by Neelima on 6-October-2004.
        /// </summary>		
        public class AbortRequestedException : RMAppException
        {
            public AbortRequestedException()
            {
            }
            public AbortRequestedException(string p_sMessage)
                : base(p_sMessage)
            {
            }
            public AbortRequestedException(string p_sMessage, Exception p_objInner)
                : base(p_sMessage, p_objInner)
            {
            }
        }

        /// <summary>
        /// Base class for FDM exceptions
        /// </summary>		
        public class FDMException : RMAppException
        {
            public FDMException()
            {
            }
            public FDMException(string p_sMessage)
                : base(p_sMessage)
            {
            }
            public FDMException(string p_sMessage, Exception p_objInner)
                : base(p_sMessage, p_objInner)
            {
            }
        }

        /// <summary>
        /// Thrown when data is not found under 'SysExData' node
        /// </summary>		
        public class SysExDataNotFoundException : FDMException
        {
            public SysExDataNotFoundException()
            {
            }
            public SysExDataNotFoundException(string p_sMessage)
                : base(p_sMessage)
            {
            }
            public SysExDataNotFoundException(string p_sMessage, Exception p_objInner)
                : base(p_sMessage, p_objInner)
            {
            }
        }

        /// <summary>
        /// PermissionViolationException was designed to be caught and formatted appropriately.
        /// </summary>
        [Serializable]
        public class PermissionViolationException : RMAppException
        {
            public int accessType = 0;
            public int module = 0;

            public PermissionViolationException()
            {
            }
            public PermissionViolationException(int p_accessType, int p_module)
                : base("Permission Violation")
            {
                accessType = p_accessType;
                module = p_module;
            }
            public PermissionViolationException(int p_accessType, int p_module, Exception inner)
                : base("Permission Violation", inner)
            {
                accessType = p_accessType;
                module = p_module;
            }
        }

        /// <summary>
        /// Thrown when data is not found under 'SysExData' node
        /// </summary>		
        public class ExtensibilityException : RMAppException
        {
            public ExtensibilityException()
            {
            }
            public ExtensibilityException(string p_sMessage)
                : base(p_sMessage)
            {
            }
            public ExtensibilityException(string p_sMessage, Exception p_objInner)
                : base(p_sMessage, p_objInner)
            {
            }
        }
    }
}