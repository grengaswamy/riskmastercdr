﻿using System;
using System.Xml;
using System.Diagnostics;
using Riskmaster.BusinessAdaptor;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.BenefitCalculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Riskmaster.UnitTests.BusinessAdaptor.BenefitCalculator
{
    /// <summary>
    ///This is a test class for BenefitCalculatorAdaptorTest and is intended
    ///to contain all BenefitCalculatorAdaptorTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BenefitCalculatorAdaptorTest
    {
        private TestContext testContextInstance;
        private string m_strDSNName  = "RMX_JURIS_RULES";
        private string m_strUserName = "csc";
        private string m_strPassword = "csc";
        const string XML_BASE_PATH   = @"C:\WORKSPACES\RISKMASTER X R4 Main Line\Pre-release Source\Riskmaster\Riskmaster.UnitTests.BusinessAdaptor.BenefitCalculator\";

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        /// <summary>
        ///A test for GetXmlOutput
        ///</summary>
        [TestMethod()]
        public void GetXmlOutputTest()
        {
            BenefitCalculatorAdaptor target = new BenefitCalculatorAdaptor(m_strUserName, m_strPassword, m_strDSNName);
            BenefitCalcData objBenefitCalcData;
            XmlDocument expected = new XmlDocument();
            XmlDocument actual = new XmlDocument();

            expected.Load(XML_BASE_PATH + "instance.xml");
            int intClaimID = target.GetClaimID(expected);
            BenefitCalc objBenefitCalc = new BenefitCalc(m_strUserName, m_strPassword, m_strDSNName, intClaimID);
            objBenefitCalcData = objBenefitCalc.GetBasicRate();
            actual = target.GetXmlOutput(expected.OuterXml, objBenefitCalcData);

            Debug.Print("--------------------------------------------------------------------");
            Debug.Print("GetXmlOutputTest: 'expected' XML document is taken from instance.xml");
            Debug.Print("--------------------------------------------------------------------");
            Debug.Print(expected.OuterXml);
            Debug.Print(Environment.NewLine);
            Debug.Print("--------------------------------------------------------------------");
            Debug.Print("GetXmlOutputTest: 'actual' XML document is created from the instance.xml and the objBenefitCalcData object");
            Debug.Print("--------------------------------------------------------------------");
            Debug.Print(actual.OuterXml);
            Assert.AreNotEqual<string>(expected.OuterXml, actual.OuterXml);
        }

        /// <summary>
        ///A test for PopulateBenefitCalcData
        ///</summary>
        [TestMethod()]
        public void PopulateBenefitCalcDataTest()
        {
            int intClaimID = 107;
            BenefitCalculatorAdaptor target = new BenefitCalculatorAdaptor(m_strUserName, m_strPassword, m_strDSNName);
            BenefitCalcData expected = new BenefitCalcData();
            BenefitCalcData actual = target.PopulateBenefitCalcData(intClaimID);

            Debug.Print("--------------------------------------------------------------------");
            Debug.Print("PopulateBenefitCalcDataTest: 'actual' returns BenefitCalcData object");
            Debug.Print("--------------------------------------------------------------------");
            Debug.Print("AverageWage: " + actual.AverageWage.ToString());
            Debug.Print("CurrentDisabilityDateDBFormat: " + actual.CurrentDisabilityDateDBFormat);
            Debug.Print("CurrentMMIDateDBFormat: " + actual.CurrentMMIDateDBFormat);
            Debug.Print("CurrentMMIDisability: " + actual.CurrentMMIDisability.ToString());
            Debug.Print("EventDateDBFormat: " + actual.EventDateDBFormat);
            Debug.Print("MostRecentRetToWkDateDBFormat: " + actual.MostRecentRetToWkDateDBFormat);
            Debug.Print("NoCodeID: " + actual.NoCodeID.ToString());
            Debug.Print("OriginalDisabilityDateDBFormat: " + actual.OriginalDisabilityDateDBFormat);
            Debug.Print("RetroactivePeriodDays: " + actual.RetroactivePeriodDays.ToString());
            Debug.Print("RetroactivePerSatisfiedCode: " + actual.RetroactivePerSatisfiedCode.ToString());
            Debug.Print("WaitingPeriodDays: " + actual.WaitingPeriodDays.ToString());
            Debug.Print("WaitingPeriodSatisfiedCode: " + actual.WaitingPeriodSatisfiedCode.ToString());
            Debug.Print("WorkLossWageLossExistsCode: " + actual.WorkLossWageLossExistsCode.ToString());
            Debug.Print("YesCodeID: " + actual.YesCodeID.ToString());
            Debug.Print("TTAbbr: " + actual.TTAbbr);
            Debug.Print("TTRate: " + actual.TTRate.ToString());
            Debug.Print("PPAbbr: " + actual.PPAbbr);
            Debug.Print("PPRate: " + actual.PPRate.ToString());
            Debug.Print("PTAbbr: " + actual.PTAbbr);
            Debug.Print("PTRate: " + actual.PTRate.ToString());
            Debug.Print("Warning: " + actual.Warning);
            Debug.Print("--------------------------------------------------------------------");
            Debug.Print(Environment.NewLine);
            Assert.IsNotNull(actual);
        }

        /// <summary>
        ///A test for ValidateXmlIn
        ///</summary>
        [TestMethod()]
        public void ValidateXmlInTest()
        {
            BenefitCalculatorAdaptor target = new BenefitCalculatorAdaptor(m_strUserName, m_strPassword, m_strDSNName);
            XmlDocument expected = new XmlDocument();
            XmlDocument actual = target.ValidateXmlIn();
            expected.Load(XML_BASE_PATH + "testoutput.xml");

            Debug.Print("--------------------------------------------------------------------");
            Debug.Print("ValidateXmlInTest: 'actual' is created from the method and compares itself to...");
            Debug.Print("--------------------------------------------------------------------");
            Debug.Print(actual.OuterXml);
            Debug.Print(Environment.NewLine);
            Debug.Print("--------------------------------------------------------------------");
            Debug.Print("ValidateXmlInTest: 'expected' is an existing XML file (testoutput.xml)");
            Debug.Print("--------------------------------------------------------------------");
            Debug.Print(expected.OuterXml);
            Debug.Print(Environment.NewLine);
            Assert.AreEqual<string>(expected.OuterXml, actual.OuterXml);
        }

        /// <summary>
        ///A test for GetClaimID
        ///</summary>
        [TestMethod()]
        public void GetClaimIDTest()
        {
            int actual;
            int expected = 107;
            BenefitCalculatorAdaptor target = new BenefitCalculatorAdaptor(m_strUserName, m_strPassword, m_strDSNName);
            XmlDocument objXmlDocument = new XmlDocument();
            objXmlDocument.Load(XML_BASE_PATH + "instance.xml");
            actual = target.GetClaimID(objXmlDocument);

            Debug.Print("-------------------------------------------------------------------");
            Debug.Print("GetClaimIDTest: 'expected' and 'actual' should be the same CLAIM_ID");
            Debug.Print("-------------------------------------------------------------------");
            Debug.Print("actual: " + actual.ToString());
            Debug.Print("expected: " + expected.ToString());
            Assert.AreEqual<int>(expected, actual);
        }

        /// <summary>
        ///A test for Get
        ///</summary>
        ////[TestMethod()]
        //public void GetTest()
        //{
        //    BenefitCalculatorAdaptor target = new BenefitCalculatorAdaptor(); // TODO: Initialize to an appropriate value
        //    XmlDocument p_objXmlIn = null; // TODO: Initialize to an appropriate value
        //    XmlDocument p_objXmlOut = null; // TODO: Initialize to an appropriate value
        //    XmlDocument p_objXmlOutExpected = null; // TODO: Initialize to an appropriate value
        //    BusinessAdaptorErrors p_objErrOut = null; // TODO: Initialize to an appropriate value
        //    BusinessAdaptorErrors p_objErrOutExpected = null; // TODO: Initialize to an appropriate value
        //    bool expected = false; // TODO: Initialize to an appropriate value
        //    bool actual;
        //    actual = target.Get(p_objXmlIn, ref p_objXmlOut, ref p_objErrOut);
        //    Assert.AreEqual(p_objXmlOutExpected, p_objXmlOut);
        //    Assert.AreEqual(p_objErrOutExpected, p_objErrOut);
        //    Assert.AreEqual(expected, actual);
        //    Assert.Inconclusive("Verify the correctness of this test method.");
        //}
    }
}
