﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Riskmaster.Db
{
	public class ConnectionStringInfoTest
	{
		#region SQL Server ConnectionStrings
		public static string SQLSecurityConnectionString
		{
			get
			{
                return ConfigurationManager.ConnectionStrings["RiskmasterSecuritySQLServer"].ConnectionString;
			}//get
		}//property

        public static string ADONetSQLServerConnectionString 
		{ 
			get
			{
				return ConfigurationManager.ConnectionStrings["ADONetSQLServer"].ConnectionString;
			}
		}//property

        public static string SQLServerConnectionString 
        { 
            get
            {
                return ConfigurationManager.ConnectionStrings["RiskmasterSQLServer"].ConnectionString;
            }
        }//property
		

		public static string SQLNativeConnectionString 
		{ 
			get
			{
				return SQLServerConnectionString.Replace(DBDriverTypes.SQLServerLegacy, DBDriverTypes.SQLServer2005);
			}
		}//property

		public static string SQLServerNativeConnectionString 
		{ 
			get
			{
				return SQLServerConnectionString.Replace(DBDriverTypes.SQLServerLegacy, DBDriverTypes.SQLServer2008);
			}
		}//property 
	#endregion

        #region Oracle ConnectionStrings
		public static string ADONetOracleConnectionString 
        { 
            get
            {
                return ConfigurationManager.ConnectionStrings["ADONetOracle"].ConnectionString;
            }
        }//property

        public static string OracleConnectionString 
        { 
            get
            {
                return ConfigurationManager.ConnectionStrings["RiskmasterOracle"].ConnectionString;
            }
        }//property 
	#endregion

        #region Odbc ConnectionStrings
        public static string OdbcConnectionString
        {
            get
            {
                return "DSN=RMXSecurity;UID=rmx_sec;PWD=rmx_sec;";
            }
        }//property 
        #endregion

        #region OleDb ConnectionStrings
        public static string AccessConnectionString
        {
            get
            {
                return @"Driver={Microsoft Access Driver (*.mdb)};Dbq=D:\Code\Northwind\Nwind.mdb;Uid=;Pwd=;";
            }
        }//property

        public static string OleDbConnectionString
        {
            get
            {
                return @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\myFolder\myAccess2007file.accdb;Persist Security Info=False;";
            }
        }//property 
        #endregion

        /// <summary>
        /// Breaks the elements of a connection string into its individual strings
        /// </summary>
        /// <param name="strConnectionString">string containing the database connection string</param>
        /// <returns>Generic List collection containing the individual connection string elements</returns>
        public static List<string> GetConnectionStringElements(string strConnectionString)
        {
            string[] arrConnStrElements = strConnectionString.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

            return arrConnStrElements.ToList<string>();
        }//method: GetConnectionStringElements()

	}
}
