﻿using Riskmaster.Db;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text;
using System.Security.Principal;
using System.Collections.Specialized;
using System.Collections.Generic;
using System;
using System.Security;
using System.Xml.Linq;
using System.Configuration;


namespace Riskmaster.Db
{
    
    
    /// <summary>
    ///This is a test class for DbFactoryTest and is intended
    ///to contain all DbFactoryTest Unit Tests
    ///</summary>
    [TestClass()]
    public class DbFactoryTest
    {

        private TestContext testContextInstance;
        private StringBuilder SQL = new StringBuilder();
        //private StringDictionary dictParams = new StringDictionary();
        private Dictionary<string, string> dictParams = new Dictionary<string, string>();
        private string strSQLStmt = string.Empty;
        private string secConnectionString = string.Empty, databaseName = string.Empty, username = string.Empty, Password = string.Empty;

       


        /// <summary>
        /// Initializes all values prior to running any Unit Tests
        /// </summary>
        [TestInitialize]
        public void Setup()
        {
            
            databaseName = "RMXTraining";
            username = "csc"; 
            Password = "csc";
            dictParams.Add(DbConnectionParams.DatabaseName, databaseName);
            dictParams.Add(DbConnectionParams.UserName, username);
            dictParams.Add(DbConnectionParams.Password, Password);

            SQL.Append("SELECT DATA_SOURCE_TABLE.DSN, USER_DETAILS_TABLE.*, USER_TABLE.* ");
            SQL.Append("FROM USER_DETAILS_TABLE, DATA_SOURCE_TABLE, USER_TABLE ");
            SQL.Append(String.Format("WHERE DATA_SOURCE_TABLE.DSN=~{0}~ ", DbConnectionParams.DatabaseName));
            SQL.Append("AND USER_DETAILS_TABLE.DSNID=DATA_SOURCE_TABLE.DSNID ");
            SQL.Append(String.Format("AND USER_DETAILS_TABLE.LOGIN_NAME=~{0}~ ", DbConnectionParams.UserName));
            SQL.Append(String.Format("AND USER_DETAILS_TABLE.PASSWORD=~{0}~ ", DbConnectionParams.Password));
            SQL.Append("AND USER_TABLE.USER_ID=USER_DETAILS_TABLE.USER_ID");
            strSQLStmt = SQL.ToString(); 
        } // method: Setup

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for GetDbReader
        ///</summary>
        [TestMethod()]
        public void GetDbReaderTest()
        {
            using (Db.DbConnection db = DbFactory.GetDbConnection(secConnectionString))
            {
                db.Open();

                //DbReader expected = null; // TODO: Initialize to an appropriate value
                DbReader actual;
                DbCommand dbCmd = db.CreateCommand();

                foreach (string strParam in dictParams.Keys)
                {
                    DbParameter dbParam = dbCmd.CreateParameter();
                    dbParam.ParameterName = strParam;
                    dbParam.Value = dictParams[strParam];

                    
                    dbCmd.Parameters.Add(dbParam);
                } // foreach

                dbCmd.CommandText = strSQLStmt;


                //actual = DbFactory.GetDbReader(connectionString, SQL.ToString());
                actual = dbCmd.ExecuteReader();
                //actual = db.ExecuteReader(SQL.ToString());
                Assert.IsNotNull(actual);
                
            } // using
        }

        /// <summary>
        ///A test for ExecuteScalar
        ///</summary>
        [TestMethod()]
        public void ExecuteScalarTest()
        {
            SQL.Length = 0;
            SQL.Append("SELECT GROUP_NAME ");
            SQL.Append("FROM USER_GROUPS UG ");
            SQL.Append("INNER JOIN USER_MEMBERSHIP UM ");
	        SQL.Append("ON UG.GROUP_ID = UM.GROUP_ID ");
            SQL.Append("WHERE UM.USER_ID = ~USERID~ ");
            SQL.Append("AND UG.GROUP_NAME = ~GROUPNAME~");
            strSQLStmt = SQL.ToString();

            dictParams.Clear();
            dictParams.Add("USERID", "15");
            dictParams.Add("GROUPNAME", "Training");

            object expected = "Training";
            object actual;
            actual = DbFactory.ExecuteScalar(ConnectionStringInfoTest.SQLServerConnectionString, strSQLStmt, dictParams);
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected, actual.ToString());

        }

        /// <summary>
        ///Verifies if a non-existent record returns a null
        ///object reference
        ///</summary>
        [TestMethod()]
        public void ExecuteScalarNullTest()
        {
            SQL.Length = 0;
            SQL.Append("SELECT GROUP_NAME ");
            SQL.Append("FROM USER_GROUPS UG ");
            SQL.Append("INNER JOIN USER_MEMBERSHIP UM ");
            SQL.Append("ON UG.GROUP_ID = UM.GROUP_ID ");
            SQL.Append("WHERE UM.USER_ID = ~USERID~ ");
            SQL.Append("AND UG.GROUP_NAME = ~GROUPNAME~");
            strSQLStmt = SQL.ToString();

            dictParams.Add("USERID", "15");
            dictParams.Add("GROUPNAME", "Test");

            object expected = null;
            object actual;
            actual = DbFactory.ExecuteScalar(ConnectionStringInfoTest.SQLServerConnectionString, strSQLStmt, dictParams);
            //Assert.AreEqual(expected, actual);
            Assert.AreEqual(expected, actual);

        }

        /// <summary>
        ///Verifies if a non-existent record returns a null
        ///object reference
        ///</summary>
        [TestMethod()]
        public void ExecuteScalarIsDbNullTest()
        {
            SQL.Length = 0;
            SQL.Append("SELECT SMTP_SERVER FROM SETTINGS");
            strSQLStmt = SQL.ToString();



            object expected = System.DBNull.Value;
            object actual = null;
            actual = DbFactory.ExecuteScalar(ConnectionStringInfoTest.SQLSecurityConnectionString, strSQLStmt, dictParams);
            //Assert.AreEqual(expected, actual);
            Assert.AreEqual(expected, actual);

        }



        /// <summary>
        ///A test for ExecuteReader
        ///</summary>
        [TestMethod()]
        public void ExecuteReaderTest()
        {
            DbReader actual;
            DbReader actualSettings;
            string strSettingsSQL = "SELECT * FROM SYS_PARMS_LOB";
            actual = DbFactory.ExecuteReader(secConnectionString, strSQLStmt, dictParams);
            actualSettings = DbFactory.ExecuteReader(ConnectionStringInfoTest.SQLServerConnectionString, strSettingsSQL, dictParams);

            while (actualSettings.Read())
            {
                int intLOBCode = actualSettings.GetInt32("LINE_OF_BUS_CODE");
            }//while
            //Assert.AreEqual(true, actual.Read());
            //Assert.AreEqual(true, actualSettings.Read());
            
        }

        /// <summary>
        ///A test for ExecuteReader
        ///</summary>
        [TestMethod()]
        public void ExecuteAccessReaderTest()
        {
            

            using (DbReader dbAccessReader = DbFactory.GetDbReader(ConnectionStringInfoTest.AccessConnectionString, "SELECT * FROM Customers"))
            {
                while (dbAccessReader.Read())
                {
                    string strCompanyName = dbAccessReader.GetString("CompanyName");
                }    //while
            } // using
        }

        /// <summary>
        ///A test for ExecuteReader
        ///</summary>
        [TestMethod()]
        public void ExecuteOracleReaderTest()
        {
            int intLOBCode = 0;

            using (DbReader dbOracleReader = DbFactory.GetDbReader(ConnectionStringInfoTest.OracleConnectionString, "SELECT * FROM SYS_PARMS_LOB"))
            {
                while (dbOracleReader.Read())
                {
                    intLOBCode = dbOracleReader.GetInt32("LINE_OF_BUS_CODE");
                }    //while
            } // using  

            Assert.AreNotEqual(0, intLOBCode);
        } // method: ExecuteOracleReaderTest

        /// <summary>
        ///A test for ExecuteReader
        ///</summary>
        [TestMethod()]
        public void ExecuteSQLReaderTest()
        {
            int intLOBCode = 0;

            using (DbReader dbSQLReader = DbFactory.GetDbReader(ConnectionStringInfoTest.SQLServerConnectionString, "SELECT * FROM SYS_PARMS_LOB"))
            {
                while (dbSQLReader.Read())
                {
                    intLOBCode = dbSQLReader.GetInt32("LINE_OF_BUS_CODE");
                }    //while
            } // using  

            Assert.AreNotEqual(0, intLOBCode);
        } // method: ExecuteSQLReaderTest




      

        [TestMethod()]
        ///
        public void LINQTest()
        {
            XDocument xDoc = XDocument.Load(@"D:\Code\Main RMX Source\R5 Source\UI\RiskmasterUI\App_Data\BuildInfo.xml");
            string expectedBuildNumber = "995";
            string expectedReleaseVersion = "RMXR5";
            string expectedPatchsetVersion = "Patchset 2";
            XElement buildRoot = xDoc.Element("build");
            XElement buildNumber = buildRoot.Element("number");
            XElement releaseVersion = buildRoot.Element("release_version");
            XElement patchsetVersion = buildRoot.Element("patchset_version");
            Assert.AreEqual(expectedBuildNumber, buildNumber.Value.ToString());
            Assert.AreEqual(expectedReleaseVersion, releaseVersion.Value.ToString());
            Assert.AreEqual(expectedPatchsetVersion, patchsetVersion.Value.ToString());


            
        } // method: LINQTest

        [TestMethod()]
        public void GetOracleDbTypeTest()
        {
            eDatabaseType expected = eDatabaseType.DBMS_IS_ORACLE;
            eDatabaseType actual = DbFactory.GetDatabaseType(ConnectionStringInfoTest.OracleConnectionString);
            Assert.AreEqual(expected, actual);
        }//method: GetOracleDbTypeTest()

        [TestMethod()]
        public void IsOracleDatabaseTest()
        {
            bool expected = true;
            bool actual = DbFactory.IsOracleDatabase(ConnectionStringInfoTest.OracleConnectionString);
            Assert.AreEqual(expected, actual);
        }//method: IsOracleDatabaseTest()

        [TestMethod()]
        public void IsSQLServerDatabaseTest()
        {
            bool expected = true;
            bool actual = DbFactory.IsOracleDatabase(ConnectionStringInfoTest.SQLServerConnectionString);
            Assert.AreNotEqual(expected, actual);
        }//method: IsOracleDatabaseTest()

        [TestMethod()]
        public void GetSQLServerDbTypeTest()
        {
            eDatabaseType expected = eDatabaseType.DBMS_IS_SQLSRVR;
            eDatabaseType actual = DbFactory.GetDatabaseType(ConnectionStringInfoTest.SQLServerConnectionString);
            Assert.AreEqual(expected, actual);

        }//method: GetSQLServerDbTypeTest()


        /// <summary>
        ///A test for ExecuteTransaction
        ///</summary>
        [TestMethod()]
        public void ExecuteTransactionSQLServerTest()
        {
            string connectionString = secConnectionString;
            string SQL = "UPDATE SETTINGS SET UPDATED_BY_USER = 'csc'";
            DbFactory.ExecuteNonQueryTransaction(connectionString, SQL);
        }

        /// <summary>
        ///A test for ExecuteTransaction
        ///</summary>
        [TestMethod()]
        public void ExecuteTransactionOracleTest()
        {
            string connectionString = ConnectionStringInfoTest.OracleConnectionString;
            string SQL = "UPDATE SETTINGS SET UPDATED_BY_USER = 'csc'";
            DbFactory.ExecuteNonQueryTransaction(connectionString, SQL);
   
        }

        /// <summary>
        ///A test for ExecuteAsType
        ///</summary>
        public void ExecuteAsTypeTestString<T>()
            where T : IConvertible
        {
            string connectionString = ConnectionStringInfoTest.SQLServerConnectionString;
            string strSQLStmt = "SELECT SHORT_CODE FROM CODES WHERE CODE_ID = 1";
            T expectedString = (T)Convert.ChangeType("1", typeof(T));
            T actualString;
            actualString = DbFactory.ExecuteAsType<T>(connectionString, strSQLStmt);
            Assert.AreEqual(expectedString, actualString);
        }


        /// <summary>
        ///A test for ExecuteAsType
        ///</summary>
        public void ExecuteAsTypeTestInt<T>()
            where T : IConvertible
        {
            string connectionString = ConnectionStringInfoTest.SQLServerConnectionString;
            string strSQLStmt = "SELECT LINE_OF_BUS_CODE, CLAIM_ID FROM CLAIM WHERE CLAIM_ID = 1";
            T expectedInt = (T)Convert.ChangeType(241, typeof(T));
            T actualInt;
            actualInt = DbFactory.ExecuteAsType<T>(connectionString, strSQLStmt);
            Assert.AreEqual(expectedInt, actualInt);

        }


        [TestMethod()]
        public void ExecuteAsTypeTest()
        {
            ExecuteAsTypeTestInt<int>();
            ExecuteAsTypeTestString<string>();
        }


    }//class



    public struct DbConnectionParams
    {
        private static string m_strDatabaseName = "databaseName", m_strUserName = "username", m_strPassword = "Password";

        public static string DatabaseName
        {
            get{return m_strDatabaseName;}
        } // property DatabaseName
        

        public static string UserName
        {
            get{return m_strUserName;}
        } // property UserName
        
        public static string Password
        {
            get{return m_strPassword;}
        } // property Password



    } // struct

}
