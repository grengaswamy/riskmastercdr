﻿using Riskmaster.Application.BenefitCalculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Runtime.InteropServices;
using System.Collections;
using System.Diagnostics;

namespace Riskmaster.UnitTests.BenefitCalculator
{
    public enum Abbreviation { TT, PP, PT }

    /// <summary>
    ///This is a test class for BenefitCalcTest and is intended
    ///to contain all BenefitCalcTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BenefitCalcTest
    {
        private TestContext testContextInstance;
        private string m_strDSNName = "RMX_JURIS_RULES";
        private string m_strUserName = "csc";
        private string m_strPassword = "csc";

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        //[DeploymentItem("..\\..\\..\\..\\Riskmaster.UnitTests.BenefitCalculator\\claim_data.csv"), DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\claim_data.csv", "claim_data#csv", DataAccessMethod.Sequential), TestMethod()]
        [DataSource("System.Data.SqlClient", "Data Source=(local);Initial Catalog=RMX_JURIS_RULES;Persist Security Info=True;User ID=sa;Password=riskmaster", "CLAIM_AWW_UNIT_TEST", DataAccessMethod.Sequential), TestMethod()]
        public void ReadClaimData()
        {
            //     headers: filing_state_id, date_of_event, average_wage
            //example data:               7,      20000101,       250.05
            int CLAIM_ID        = Convert.ToInt32(TestContext.DataRow["CLAIM_ID"]);
            int iFilingStateID  = Convert.ToInt32(TestContext.DataRow["filing_state_id"]);
            string sDateOfEvent = TestContext.DataRow["date_of_event"].ToString();
            double dAverageWage = Convert.ToDouble(TestContext.DataRow["average_wage"]);

            Debug.Print("----------------------------------");
            Debug.Print("Test Claim Input");
            Debug.Print("----------------------------------");
            Debug.Print("CLAIM_ID: " + CLAIM_ID.ToString());
            Debug.Print("iFilingStateID: " + iFilingStateID.ToString());
            Debug.Print("sDateOfEvent: " + sDateOfEvent);
            Debug.Print("dAverageWage: " + dAverageWage.ToString());
            Debug.Print("----------------------------------\n");
            //double dImpairmentRate= TestContext.DataRow["impairment_rate"];
            //int iTaxStatusID      = TestContext.DataRow["tax_status_id"];
            //int iTaxExemption     = TestContext.DataRow["tax_exemption"];
            
            ArrayList arrParams = new ArrayList();
            arrParams.Add(CLAIM_ID);
            arrParams.Add(iFilingStateID);
            arrParams.Add(sDateOfEvent);
            arrParams.Add(dAverageWage);

            //Update the database with the new record
            DataAccess.ExecuteNonQuery("USP_UPDATE_UNIT_TESTS", arrParams.ToArray());

            Exception BenefitCalcException;

            try
            {
                double actual = 0.0;

                BenefitCalc target = new BenefitCalc(m_strUserName, m_strPassword, m_strDSNName, CLAIM_ID);
                BenefitCalcData objBenefitCalcData = new BenefitCalcData();

                double expected = (dAverageWage * (2.0/3.0)); //250.05 * (2 / 3) = 166.70
                expected = Math.Round(expected, 2);

                objBenefitCalcData = target.GetBasicRate();
                if (objBenefitCalcData != null)
                {
                    Debug.Print("----------------------------------");
                    Debug.Print("BenefitCalcData Object Output");
                    Debug.Print("----------------------------------");
                    Debug.Print("AverageWage: " + objBenefitCalcData.AverageWage.ToString());
                    Debug.Print("CurrentDisabilityDateDBFormat: " + objBenefitCalcData.CurrentDisabilityDateDBFormat);
                    Debug.Print("CurrentMMIDateDBFormat: " + objBenefitCalcData.CurrentMMIDateDBFormat);
                    Debug.Print("CurrentMMIDisability: " + objBenefitCalcData.CurrentMMIDisability.ToString());
                    Debug.Print("EventDateDBFormat: " + objBenefitCalcData.EventDateDBFormat);
                    Debug.Print("MostRecentRetToWkDateDBFormat: " + objBenefitCalcData.MostRecentRetToWkDateDBFormat);
                    Debug.Print("NoCodeID: " + objBenefitCalcData.NoCodeID.ToString());
                    Debug.Print("OriginalDisabilityDateDBFormat: " + objBenefitCalcData.OriginalDisabilityDateDBFormat);
                    Debug.Print("RetroactivePeriodDays: " + objBenefitCalcData.RetroactivePeriodDays.ToString());
                    Debug.Print("RetroactivePerSatisfiedCode: " + objBenefitCalcData.RetroactivePerSatisfiedCode.ToString());
                    Debug.Print("WaitingPeriodDays: " + objBenefitCalcData.WaitingPeriodDays.ToString());
                    Debug.Print("WaitingPeriodSatisfiedCode: " + objBenefitCalcData.WaitingPeriodSatisfiedCode.ToString());
                    Debug.Print("WorkLossWageLossExistsCode: " + objBenefitCalcData.WorkLossWageLossExistsCode.ToString());
                    Debug.Print("YesCodeID: " + objBenefitCalcData.YesCodeID.ToString());
                    Debug.Print("TTAbbr: " + objBenefitCalcData.TTAbbr);
                    Debug.Print("TTRate: " + objBenefitCalcData.TTRate.ToString());
                    Debug.Print("PPAbbr: " + objBenefitCalcData.PPAbbr);
                    Debug.Print("PPRate: " + objBenefitCalcData.PPRate.ToString());
                    Debug.Print("PTAbbr: " + objBenefitCalcData.PTAbbr);
                    Debug.Print("PTRate: " + objBenefitCalcData.PTRate.ToString());
                    Debug.Print("Warning: " + objBenefitCalcData.Warning);
                    Debug.Print("----------------------------------\n");

                    ///TODO: these fields are always empty
                    //Assert.IsFalse(String.IsNullOrEmpty(objBenefitCalcData.CurrentDisabilityDateDBFormat), "CurrentDisabilityDateDBFormat is null/empty");
                    //Assert.IsFalse(String.IsNullOrEmpty(objBenefitCalcData.CurrentMMIDateDBFormat), "CurrentMMIDateDBFormat is null/empty");
                    //Assert.IsTrue((objBenefitCalcData.CurrentMMIDisability > 0.0), "CurrentMMIDisability is 0");
                    //Assert.IsFalse(String.IsNullOrEmpty(objBenefitCalcData.MostRecentRetToWkDateDBFormat), "MostRecentRetToWkDateDBFormat is null/empty");
                    //Assert.IsFalse(String.IsNullOrEmpty(objBenefitCalcData.OriginalDisabilityDateDBFormat), "OriginalDisabilityDateDBFormat is null/empty");

                    Assert.IsTrue(String.IsNullOrEmpty(objBenefitCalcData.Warning), "Warning is NOT null/empty");
                    Assert.AreEqual<double>(dAverageWage, objBenefitCalcData.AverageWage);
                    Assert.IsTrue((objBenefitCalcData.YesCodeID > 0), "YesCodeID is 0");
                    Assert.IsTrue((objBenefitCalcData.NoCodeID > 0), "NoCodeID is 0");
                    Assert.IsFalse(String.IsNullOrEmpty(objBenefitCalcData.EventDateDBFormat), "EventDateDBFormat is null/empty");
                    Assert.IsTrue((objBenefitCalcData.RetroactivePeriodDays >= 0), "RetroactivePeriodDays is null");
                    Assert.IsTrue((objBenefitCalcData.RetroactivePerSatisfiedCode >= 0), "RetroactivePerSatisfiedCode is null");
                    Assert.IsTrue((objBenefitCalcData.WaitingPeriodDays >= 0), "WaitingPeriodDays is null");
                    Assert.IsTrue((objBenefitCalcData.WaitingPeriodSatisfiedCode >= 0), "WaitingPeriodSatisfiedCode is null");
                    Assert.IsTrue((objBenefitCalcData.WorkLossWageLossExistsCode >= 0), "WorkLossWageLossExistsCode is null");
                    actual = objBenefitCalcData.TTRate;
                    Assert.AreEqual<double>(expected, actual);
                    actual = objBenefitCalcData.PPRate;
                    Assert.AreEqual<double>(expected, actual);
                    actual = objBenefitCalcData.PTRate;
                    Assert.AreEqual<double>(expected, actual);
                    Assert.IsFalse(String.IsNullOrEmpty(objBenefitCalcData.TTAbbr), "TTAbbr is null/empty");
                    Assert.IsFalse(String.IsNullOrEmpty(objBenefitCalcData.PPAbbr), "PPAbbr is null/empty");
                    Assert.IsFalse(String.IsNullOrEmpty(objBenefitCalcData.PTAbbr), "PTAbbr is null/empty");
                }
                else
                {
                    Assert.Fail("objBenefitCalcData object is null");
                }
            }
            catch (COMException ex)
            {
                System.Diagnostics.Debug.Print(ex.Message);
                Assert.Fail(ex.Message);
            }
            catch (Exception ex)
            {
                BenefitCalcException = ex;
                System.Diagnostics.Debug.Print(BenefitCalcException.Message);
                Assert.Fail(BenefitCalcException.Message);
            }
        }

        //#region Get Rate TTD
        ///// <summary>
        /////A test for GetRate
        /////</summary>
        //[DataSource("System.Data.SqlClient", "Data Source=(local);Initial Catalog=RMX_JURIS_RULES;Persist Security Info=True;User ID=sa;Password=riskmaster", "CLAIM_UNIT_TEST", DataAccessMethod.Sequential), TestMethod()]
        //public void GetRateTTD()
        //{
        //    Exception BenefitCalcException;

        //    try
        //    {
        //        BenefitCalc target = new BenefitCalc(); // TODO: Initialize to an appropriate value
        //        ClaimClass objClaim = new ClaimClass();
        //        bool blnIsInitialized = false;
        //        //double expected = 0;
        //        double actual = 0;
        //        int iAbbr = (int)Abbreviation.TT; // TODO: Initialize to an appropriate value
        //        //int iClaimNum = 229; // TODO: Initialize to an appropriate value
        //        int intClaimID = Convert.ToInt32(TestContext.DataRow["CLAIM_ID"]);
        //        objClaim = target.GetClaim(intClaimID, TestContext.DataRow["CLAIM_NUMBER"].ToString(), TestContext.DataRow["STATE"].ToString());
        //        blnIsInitialized = target.GetRate(objClaim, iAbbr, intClaimID);
        //        actual = target.TTD;
        //        //Assert.AreEqual<double>(expected, actual);
        //        Assert.AreEqual<bool>(true, blnIsInitialized);
        //    }
        //    catch (COMException ex)
        //    {
        //        System.Diagnostics.Debug.Print(ex.Message);
        //        Assert.Fail();
        //    }
        //    catch (Exception ex)
        //    {
        //        BenefitCalcException = ex;

        //        System.Diagnostics.Debug.Print(BenefitCalcException.Message);
        //        Assert.Fail();
        //    }
        //}
        //#endregion

        //#region Get Rate PPD
        ///// <summary>
        /////A test for GetRate
        /////</summary>
        //[DataSource("System.Data.SqlClient", "Data Source=(local);Initial Catalog=RMX_JURIS_RULES;Persist Security Info=True;User ID=sa;Password=riskmaster", "CLAIM_UNIT_TEST", DataAccessMethod.Sequential), TestMethod()]
        //public void GetRatePPD()
        //{
        //    Exception BenefitCalcException;

        //    try
        //    {
        //        BenefitCalc target = new BenefitCalc(); // TODO: Initialize to an appropriate value
        //        ClaimClass objClaim = new ClaimClass();
        //        bool blnIsInitialized = false;
        //        //double expected = 0;
        //        double actual = 0;
        //        int iAbbr = (int)Abbreviation.PP; // TODO: Initialize to an appropriate value
        //        //int iClaimNum = 229; // TODO: Initialize to an appropriate value
        //        int intClaimID = Convert.ToInt32(TestContext.DataRow["CLAIM_ID"]);
        //        objClaim = target.GetClaim(intClaimID, TestContext.DataRow["CLAIM_NUMBER"].ToString(), TestContext.DataRow["STATE"].ToString());
        //        blnIsInitialized = target.GetRate(objClaim, iAbbr, intClaimID);
        //        actual = target.PPD;
        //        //Assert.AreEqual<double>(expected, actual);
        //        Assert.AreEqual<bool>(true, blnIsInitialized);
        //    }
        //    catch (COMException ex)
        //    {
        //        System.Diagnostics.Debug.Print(ex.Message);
        //        Assert.Fail();
        //    }
        //    catch (Exception ex)
        //    {
        //        BenefitCalcException = ex;

        //        System.Diagnostics.Debug.Print(BenefitCalcException.Message);
        //        Assert.Fail();
        //    }
        //}  
        //#endregion

        //#region Get Rate PTD
        ///// <summary>
        /////A test for GetRate
        /////</summary>
        //[DataSource("System.Data.SqlClient", "Data Source=(local);Initial Catalog=RMX_JURIS_RULES;Persist Security Info=True;User ID=sa;Password=riskmaster", "CLAIM_UNIT_TEST", DataAccessMethod.Sequential), TestMethod()]
        //public void GetRatePTD()
        //{
        //    Exception BenefitCalcException;

        //    try
        //    {
        //        BenefitCalc target = new BenefitCalc(); // TODO: Initialize to an appropriate value
        //        ClaimClass objClaim = new ClaimClass();
        //        bool blnIsInitialized = false;
        //        //double expected = 0;
        //        double actual = 0;
        //        int iAbbr = (int)Abbreviation.PT; // TODO: Initialize to an appropriate value
        //        //int iClaimNum = 229; // TODO: Initialize to an appropriate value
        //        int intClaimID = Convert.ToInt32(TestContext.DataRow["CLAIM_ID"]);
        //        objClaim = target.GetClaim(intClaimID, TestContext.DataRow["CLAIM_NUMBER"].ToString(), TestContext.DataRow["STATE"].ToString());
        //        blnIsInitialized = target.GetRate(objClaim, iAbbr, intClaimID);
        //        actual = target.PTD;
        //        //Assert.AreEqual<double>(expected, actual);
        //        Assert.AreEqual<bool>(true, blnIsInitialized);
        //    }
        //    catch (COMException ex)
        //    {
   
        //        System.Diagnostics.Debug.Print(ex.Message);
        //        Assert.Fail();
        //    }
        //    catch (Exception ex)
        //    {
        //        BenefitCalcException = ex;

        //        System.Diagnostics.Debug.Print(BenefitCalcException.Message);
        //        Assert.Fail();
        //    }
        //}   
        //#endregion

        /// <summary>
        ///A test for GetListOfClaims
        ///</summary>
        //[TestMethod()]
        //public void GetListOfClaimsTest()
        //{
        //    BenefitCalc target = new BenefitCalc();//"RMX_JURIS_RULES", "csc", "csc"); // TODO: Initialize to an appropriate value
        //    bool expected = false; // TODO: Initialize to an appropriate value
        //    bool actual;
        //    actual = target.GetListOfClaims();
        //    Assert.AreEqual(expected, actual);
        //}
    }
}
