﻿using Riskmaster.Application.ProgressNotes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.UnitTests.ProgressNotesTest
{
    
    
    /// <summary>
    ///This is a test class for ProgressNotesManagerTest and is intended
    ///to contain all ProgressNotesManagerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ProgressNotesManagerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for SaveTemplates
        ///</summary>
        [TestMethod()]
        public void SaveTemplatesTest()
        {
            string p_sDsnName = "Dev_R6"; //Hardcoding for testing
            string p_sUserName = "csc"; //Hardcoding for testing
            string p_sPassword = "csc"; //Hardcoding for testing
            ProgressNotesManager target = new ProgressNotesManager(p_sDsnName, p_sUserName, p_sPassword); // TODO: Initialize to an appropriate value
            int p_iTemplateId = 0;
            string p_sTemplateName = "TestTemplate2";
            string p_sTemplateMemo = "Testing";
            string p_sAddedBy = "csc";
            string p_sUpdatedBy = "csc";
            string p_sDateAdded = string.Empty;
            string p_sDateUpdated = string.Empty;
            string expected = "2"; //To be set to next CL_PRGN_TEMP_Id in CLAIM_PRG_NOTE_TEMPLATES
            string actual;
            actual = target.SaveTemplates(p_iTemplateId, p_sTemplateName, p_sTemplateMemo, p_sAddedBy, p_sUpdatedBy, p_sDateAdded, p_sDateUpdated);
            Assert.AreEqual(expected, actual);
        }
        [TestMethod()]
        public void DuplicateTemplatesTest()
        {
            string p_sDsnName = "Dev_R6";
            string p_sUserName = "csc";
            string p_sPassword = "csc";
            int p_iTemplateId = 0;
            string p_sTemplateName = "TestTemplate2"; //Same templatename as created in SaveTemplatesTest
            string p_sTemplateMemo = "Testing";
            string p_sAddedBy = "csc";
            string p_sUpdatedBy = "csc";
            string p_sDateAdded = string.Empty;
            string p_sDateUpdated = string.Empty;
            string expected = "Please enter a unique 'Template Name'"; 
            string actual;
            try
            {
                ProgressNotesManager target = new ProgressNotesManager(p_sDsnName, p_sUserName, p_sPassword); // TODO: Initialize to an appropriate value
                actual = target.SaveTemplates(p_iTemplateId, p_sTemplateName, p_sTemplateMemo, p_sAddedBy, p_sUpdatedBy, p_sDateAdded, p_sDateUpdated);

            }
            catch (RMAppException ex)
            {
                Assert.AreEqual(expected, ex.Message);
            }
        }
        [TestMethod()]
        public void UpdateTemplatesTest()
        {
            string p_sDsnName = "Dev_R6";
            string p_sUserName = "csc";
            string p_sPassword = "csc";
            ProgressNotesManager target = new ProgressNotesManager(p_sDsnName, p_sUserName, p_sPassword); // TODO: Initialize to an appropriate value
            int p_iTemplateId = 2; //To be set to id of Template just created.
            string p_sTemplateName = "TestTemplate2";
            string p_sTemplateMemo = "Testing Again";
            string p_sAddedBy = "csc";
            string p_sUpdatedBy = "csc";
            string p_sDateAdded = string.Empty;
            string p_sDateUpdated = string.Empty;
            string expected = "2"; //To be set to id of Template just created.
            string actual;
            actual = target.SaveTemplates(p_iTemplateId, p_sTemplateName, p_sTemplateMemo, p_sAddedBy, p_sUpdatedBy, p_sDateAdded, p_sDateUpdated);
            Assert.AreEqual(expected, actual);
        }
        [TestMethod()]
        public void DeleteTemplatesTest()
        {
            string p_sDsnName = "Dev_R6";
            string p_sUserName = "csc";
            string p_sPassword = "csc";
            ProgressNotesManager target = new ProgressNotesManager(p_sDsnName, p_sUserName, p_sPassword); // TODO: Initialize to an appropriate value
            int p_iTemplateId = 2; //To be set to id of Template just created.
            target.DeleteTemplate(p_iTemplateId);
        }
    }
}
