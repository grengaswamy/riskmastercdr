﻿using Riskmaster.Tools.UnitTest;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Riskmaster.Tools.GenerateSchema
{
    
    
    /// <summary>
    ///This is a test class for SchemaTestsTest and is intended
    ///to contain all SchemaTestsTest Unit Tests
    ///</summary>
    [TestClass()]
    public class SchemaTestsTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for GenerateSchema
        ///</summary>
        [TestMethod()]
        public void GenerateSchemaTest()
        {
            try
            {

                SchemaTests target = new SchemaTests(ConfigurationManager.AppSettings["WorkPath"], ConfigurationManager.AppSettings["DataSourceTarget"], ConfigurationManager.AppSettings["DataSourceUserId"], ConfigurationManager.AppSettings["DataSourcePassword"]);
                target.GenerateSchema();
                //Verify that the DataModelFactory object was properly instantiated
                Assert.IsNotNull(target.dmf);

                //foreach (System.Reflection.PropertyInfo pi in target.GetType().GetProperties())
                //{
                //    object objValue = target.GetType().GetField(pi.Name).GetValue(target);

                //    if (objValue.Equals(null))
                //    {
                //        System.Type t = objValue.GetType();

                //        switch (t.FullName)
                //        {

                //            case "System.String":
                //                target.GetType().GetField(pi.Name).SetValue(target, string.Empty);
                //                break;
                //            case "System.Int32":
                //                target.GetType().GetField(pi.Name).SetValue(target, 0);
                //                break;
                //            case "System.Double":
                //                target.GetType().GetField(pi.Name).SetValue(target, 0.0);
                //                break;
                //        }//switch
                //    }

                //}//foreach
            }
            catch (System.Exception ex)
            {

                System.Diagnostics.Debug.Print(ex.Message);
            }
            
        }
    }
}
