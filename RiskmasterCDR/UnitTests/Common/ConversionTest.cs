﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualBasic;
using Riskmaster.Db;

namespace Riskmaster.Common
{
    
    
    /// <summary>
    ///This is a test class for ConversionTest and is intended
    ///to contain all ConversionTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ConversionTest
    {


        private TestContext testContextInstance;
        const string m_strUnparsed = "This is a beautiful day.";

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for ToDbDate
        ///</summary>
        [TestMethod()]
        public void ToDbDateTest()
        {
            DateTime date = DateTime.Now;
            string expected = DateTime.Now.ToString("yyyyMMdd");
            string actual;
            actual = Conversion.ToDbDate(date);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ToDbTimeTest()
        {
            DateTime date = DateTime.Now;
            string expected = DateTime.Now.ToString("HHmmss");
            string actual;
            actual = Conversion.ToDbTime(date);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void VBRight()
        {
            string myString = "leftmiddleright";
            string vbRight = Strings.Right(myString, 5);
            string csharpRight = myString.Substring(myString.Length - 5);
            Assert.AreEqual(vbRight, csharpRight);

        }

        [TestMethod()]
        public void VBDayOfWeek()
        {
            int vbWeekDay = DateAndTime.Weekday(DateTime.Now, FirstDayOfWeek.Sunday);
            int csharpWeekDay = VBFunctions.VBDayOfWeek(DateTime.Now);
            Assert.AreEqual<int>(vbWeekDay, csharpWeekDay);
        }

        [TestMethod()]
        public void VBStringFormat()
        {
            int serialNumber = 909090;
            string vbFormat = Microsoft.VisualBasic.Strings.Format(serialNumber, "0000");
            string csharpFormat = string.Format("{0:0000}", serialNumber);
            Assert.AreEqual(vbFormat, csharpFormat);
        }

        [TestMethod()]
        public void VBMidFunctionWithLength()
        {
            
            string vbMid = Microsoft.VisualBasic.Strings.Mid(m_strUnparsed, 5, 2);
            string csharpMid = VBFunctions.VBMid(m_strUnparsed, 5, 2);
            Assert.AreEqual(vbMid, csharpMid);
        }//method: VBMidFunctionWithLength

        [TestMethod()]
        public void VBMidFunction()
        {
            string vbMid = Microsoft.VisualBasic.Strings.Mid(m_strUnparsed, 5);
            string csharpMid = VBFunctions.VBMid(m_strUnparsed, 5);
            Assert.AreEqual(vbMid, csharpMid);
        }//method: VBMidFunction

        [TestMethod()]
        public void VBInStrFunction()
        {
            int vbInStr = Microsoft.VisualBasic.Strings.InStr(m_strUnparsed, "beautiful", CompareMethod.Binary);
            int csharpInStr = VBFunctions.VBInStr(m_strUnparsed, "beautiful");
            Assert.AreEqual<int>(vbInStr, csharpInStr);
        }//method: VBInStrFunction

        [TestMethod()]
        public void VBLeftFunction()
        {
            string vbLeft = Microsoft.VisualBasic.Strings.Left(m_strUnparsed, 11);
            string csharpLeft = VBFunctions.VBLeft(m_strUnparsed, 11);
            Assert.AreEqual<string>(vbLeft, csharpLeft);
        }//method: VBLeftFunction

        [TestMethod()]
        public void VBRightFunction()
        {
            string vbRight = Microsoft.VisualBasic.Strings.Right(m_strUnparsed, 11);
            string csharpRight = VBFunctions.VBRight(m_strUnparsed, 11);
            Assert.AreEqual<string>(vbRight, csharpRight);
        }//method: VBRightFunction

        [TestMethod()]
        public void VBDateValueFunction()
        {
            
            DateTime dtVBDateValue = Microsoft.VisualBasic.DateAndTime.DateValue("02/12/1969 09:09:10");
            DateTime dtCSharpDateValue = VBFunctions.VBDateValue("02/12/1969 09:09:10");
            Assert.AreEqual<DateTime>(dtVBDateValue, dtCSharpDateValue);            
            Console.WriteLine(dtCSharpDateValue.ToString());
        }//method: VBDateValueFunction

        [TestMethod()]
        public void TestShortCode()
        {
            const string EMPTY_DOUBLE_QUOTES = "\" \"";

            string shortCode = string.Empty, strShortCode = string.Empty;
            string strconnString = "Driver={SQL Server};Server=CSCDBSSFL002;Database=Pediatrix;UID=sa;PWD=riskmaster.2k8;";
            string SQL = String.Format(@"SELECT SHORT_CODE FROM CODES WHERE CODE_ID={0}", 0);
            strShortCode = DbFactory.ExecuteScalar(strconnString, SQL) as string;
            if (!string.IsNullOrEmpty(strShortCode))
            {
                //Verify that the string is not equal to a pair of double quotes
                if (! strShortCode.Equals(EMPTY_DOUBLE_QUOTES))
                {
                    shortCode = strShortCode.Trim();
                }//if
            }//if
            Assert.AreEqual(string.Empty, shortCode);
        }//

    }
}
