﻿using Riskmaster.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Riskmaster.Common
{
    
    
    /// <summary>
    ///This is a test class for UtilitiesTest and is intended
    ///to contain all UtilitiesTest Unit Tests
    ///</summary>
    [TestClass()]
    public class UtilitiesTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for GetNextUID
        ///</summary>
        [DataSource("System.Data.SqlClient", "Data Source=CSCDBSSFL002;Initial Catalog=RMXR6_Training_MJP;User ID=sa;Password=riskmaster.2k8", "GLOSSARY_UNIT_TESTS", DataAccessMethod.Sequential), TestMethod()]
        public void GetNextUIDTest()
        {
            string strConnString = RMConfigurationManager.GetConnectionString("RiskmasterSQLServer");
            string tableName = "CLAIM";
            int expected = Convert.ToInt32(testContextInstance.DataRow["NEXT_UNIQUE_ID"]);
            int actual;
            actual = Utilities.GetNextUID(strConnString, tableName);
            Assert.AreEqual(expected++, actual);
        }

        /// <summary>
        ///A test for GetNextUID
        ///</summary>
        [DataSource("System.Data.SqlClient", "Data Source=CSCDBSSFL002;Initial Catalog=RMXR6_Training_MJP;User ID=sa;Password=riskmaster.2k8", "GLOSSARY_UNIT_TESTS", DataAccessMethod.Sequential), TestMethod()]
        [ExpectedException(typeof(Riskmaster.ExceptionTypes.RMAppException))]
        public void GetNextUIDInvalidTableTest()
        {
            string strConnString = RMConfigurationManager.GetConnectionString("RiskmasterSQLServer");
            string tableName = "CLAIM2";
            int expected = Convert.ToInt32(testContextInstance.DataRow["NEXT_UNIQUE_ID"]);
            int actual;
            actual = Utilities.GetNextUID(strConnString, tableName);
            Assert.AreEqual(expected++, actual);
        }
    }
}
