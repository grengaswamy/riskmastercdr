﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;

namespace Riskmaster.Common.Cache
{
    
    
    /// <summary>
    ///This is a test class for LocalCacheTest and is intended
    ///to contain all LocalCacheTest Unit Tests
    ///</summary>
    [TestClass()]
    public class LocalCacheTest
    {


        private TestContext testContextInstance;
        private string connectionString = string.Empty; 

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        [TestInitialize()]
        public void InitializeConnectionStrings()
        {
            connectionString = ConfigurationManager.ConnectionStrings["RMDatabase"].ConnectionString;
        }
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for GetRelatedCodeId
        ///</summary>
        [TestMethod()]
        public void GetRelatedCodeIdTest()
        {
            
            LocalCache target = new LocalCache(connectionString);
            int codeId = 10;
            int expected = 9; 
            int actual;
            actual = target.GetRelatedCodeId(codeId);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for GetRelatedCodeId
        ///</summary>
        [TestMethod()]
        public void GetRelatedCodeIdNullTest()
        {
            LocalCache target = new LocalCache(connectionString); 
            int codeId = 5152; 
            int expected = 0; 
            int actual;
            actual = target.GetRelatedCodeId(codeId);
            Assert.AreEqual(expected, actual);
        }
    }
}
