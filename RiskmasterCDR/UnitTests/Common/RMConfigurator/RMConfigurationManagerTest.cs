﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Specialized;
using Riskmaster.Common;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections;
using Microsoft.Win32;

namespace Riskmaster.Common
{
    
    
    /// <summary>
    ///This is a test class for RMConfigurationManagerTest and is intended
    ///to contain all RMConfigurationManagerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class RMConfigurationManagerTest
    {
        
        
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        [TestInitialize()]
        public void MyTestInitialize()
        {
            //RMConfigurationManager.OpenExeConfigurationFile(@"C:\inetpub\wwwroot\DotNet35WebsiteIIS\web.config");
            //RMConfigurationManager.OpenWebConfigurationFile("DotNet35WebsiteIIS");
        }
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for retrieving TaskManager settings
        ///</summary>
        [TestMethod()]
        public void GetTaskManagerSettings()
        {
            string strSectionName = "TaskManagerServiceSettings";
            NameValueCollection expected = new NameValueCollection();

            expected.Add("ArchiveJobs", "false");
            expected.Add("ArchiveDays", "6");

            NameValueCollection actual;
            actual = RMConfigurationManager.GetSectionSettings(strSectionName);
            Assert.AreNotEqual(expected.Count, actual.Count);
        }

        /// <summary>
        ///A test for retrieving LSS settings
        ///</summary>
        [TestMethod()]
        public void GetLSSSettings()
        {
            string strSectionName = "LSSInterface";
            //KeyValueConfigurationCollection expected = new KeyValueConfigurationCollection();
            NameValueCollection expected = new NameValueCollection();

            expected.Add("AccountId", "1");
            expected.Add("Logging", "true");

            //NameValueCollection actual;
            NameValueCollection actual;
            actual = RMConfigurationManager.GetSectionSettings(strSectionName);
            Assert.AreEqual(expected.Count, actual.Count);
        }

        /// <summary>
        ///A test for retrieving Acrosoft settings
        ///</summary>
        [TestMethod()]
        public void GetAcrosoftSettings()
        {
            string strSectionName = "Acrosoft";
            NameValueCollection expected = new NameValueCollection();

            expected.Add("AcrosoftAttachmentsTypeKey", "Riskmaster");
            expected.Add("AcrosoftUsersTypeKey", "RiskmasterUser");

            NameValueCollection actual;
            actual = RMConfigurationManager.GetSectionSettings(strSectionName);
            Assert.AreEqual(expected.Count, actual.Count);
        }

        /// <summary>
        ///A test for GetRMAdaptors
        ///</summary>
        [TestMethod()]
        public void GetRMAdaptorsTest()
        {

            int expected = 5;
            RMAdaptersCollection actual = RMConfigurationManager.GetRMAdaptors();
            Assert.AreEqual(expected, actual.Count);
        }

        /// <summary>
        ///A test for GetRMExtensibilityAdaptors
        ///</summary>
        [TestMethod()]
        public void GetRMExtensibilityAdaptorsTest()
        {

            int expected = 6;
            RMAdaptersCollection actual = RMConfigurationManager.GetRMExtensibilityAdaptors();
            Assert.AreEqual(expected, actual.Count);
        }

        /// <summary>
        ///A test for GetRMAdaptorsCollection
        ///</summary>
        [TestMethod()]
        public void GetRMAdaptorsCollectionTest()
        {

            int expected = 11;
            System.Data.DataTable actual = new System.Data.DataTable();
            actual = RMConfigurationManager.GetRMAdaptorsCollection();
            Assert.AreEqual(expected, actual.Rows.Count);
        }

        /// <summary>
        ///A test for Riskmaster.config paths
        ///</summary>
        [TestMethod()]
        public void GetRMConfigPaths()
        {

            string expected = ConfigurationManager.AppSettings["BasePath"];
            string appFilesPath = Path.Combine(expected, "appfiles");
            string userDataPath = Path.Combine(expected, "userdata");
            string tempPath = Path.Combine(expected, "temp");

            Assert.AreEqual(expected, RMConfigurator.BasePath);
            Assert.AreEqual(appFilesPath, RMConfigurator.AppFilesPath);
            Assert.AreEqual(userDataPath, RMConfigurator.UserDataPath);
            Assert.AreEqual(tempPath, RMConfigurator.TempPath);

        }

        /// <summary>
        ///A test for Acrosoft Settings
        ///</summary>
        [TestMethod()]
        public void GetAcrosoftSectionSettings()
        {

            string expected = "Riskmaster";
            string expectedUser = "RiskmasterUser";

            AcrosoftSection.GetAcrosoftSection();

            Assert.AreEqual(expected, AcrosoftSection.AcrosoftAttachmentsTypeKey);
            Assert.AreEqual(expectedUser, AcrosoftSection.AcrosoftUsersTypeKey);

        }

        /// <summary>
        ///A test for GetDictionarySectionSettings
        ///</summary>
        [TestMethod()]
        public void GetDictionarySectionSettingsTest()
        {
            string strSectionName = "DictionarySection";
            string expected = "2";
            Hashtable objDictSection;
            objDictSection = RMConfigurationManager.GetDictionarySectionSettings(strSectionName);
            List<string> arrDictSettings = new List<string>();

            foreach (var item in objDictSection.Values)
            {
                arrDictSettings.Add(item.ToString());
            }

            Assert.AreEqual(expected, objDictSection["1"].ToString());
            System.Diagnostics.Debug.Print(arrDictSettings[0]);
            Assert.AreEqual(objDictSection.Count, arrDictSettings.Count);
            
        }

        /// <summary>
        ///A test for GetNameValueSectionSettings
        ///</summary>
        [TestMethod()]
        public void GetNameValueSectionSettingsTest()
        {
            string strSectionName = "NameValueSection";
            string expected = "2";
            NameValueCollection nvCollSettings;
            nvCollSettings = RMConfigurationManager.GetNameValueSectionSettings(strSectionName);
            Assert.AreEqual(expected, nvCollSettings["1"].ToString());

        }

        /// <summary>
        ///A test for GetNameValueSectionSettings
        ///</summary>
        [TestMethod()]
        public void UpdateNameValueSectionSettingsTest()
        {
            string strSectionName = "NameValueSection";
            int expected = 3;
            NameValueCollection nvCollSettings;
            nvCollSettings = RMConfigurationManager.GetNameValueSectionSettings(strSectionName);
            NameValueCollection nvCollUpdatedSettings = new NameValueCollection();
            nvCollUpdatedSettings.Add(nvCollSettings);
            nvCollUpdatedSettings.Add("addnlkey", "addnlvalue");
            string[] arrNVCollSettings = new string[nvCollUpdatedSettings.Count];
            nvCollUpdatedSettings.CopyTo(arrNVCollSettings, 0);

            Assert.AreEqual(expected, nvCollUpdatedSettings.Count);
            System.Diagnostics.Debug.Print(arrNVCollSettings[0]);
            Assert.AreEqual(nvCollUpdatedSettings.Count, arrNVCollSettings.Length);
            

        }

        /// <summary>
        ///A test for GetSingleTagSectionSettings
        ///</summary>
        [TestMethod()]
        public void GetSingleTagSectionSettingsTest()
        {
            string strSectionName = "SingleTag";
            string expected = "2";
            Hashtable objSingleTagSection;
            objSingleTagSection = RMConfigurationManager.GetSingleTagSectionSettings(strSectionName);
            Assert.AreEqual(expected, objSingleTagSection["test"].ToString());
        }

        /// <summary>
        ///A test for GetCombinedPath
        ///</summary>
        [TestMethod()]
        public void GetCombinedPathTest()
        {
            string expected = Path.Combine(RMConfigurator.AppFilesPath, @"Report\ReportTemplate.xml");
            string actual = RMConfigurator.CombineFilePath(RMConfigurator.AppFilesPath, @"Report\ReportTemplate.xml");
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void GetScriptingEvents()
        {
            Hashtable objScriptingEvents = RMConfigurationManager.GetDictionarySectionSettings("ScriptingEvents");


            bool m_runValidation = System.Convert.ToBoolean(objScriptingEvents["ValidationEnabled"]);
            bool m_runInitialization = System.Convert.ToBoolean(objScriptingEvents["InitializationEnabled"]);
            bool m_runCalculateData = System.Convert.ToBoolean(objScriptingEvents["CalculationEnabled"]);
            bool m_runBeforeSave = System.Convert.ToBoolean(objScriptingEvents["BeforeSaveEnabled"]);
            bool m_runAfterSave = System.Convert.ToBoolean(objScriptingEvents["AfterSaveEnabled"]);

            Assert.IsTrue(m_runValidation);
            Assert.IsTrue(m_runInitialization);
            Assert.IsTrue(m_runCalculateData);
            Assert.IsTrue(m_runBeforeSave);
            Assert.IsTrue(m_runAfterSave);

        }

    }
}

