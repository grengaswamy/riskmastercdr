﻿using Riskmaster.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data;

namespace Riskmaster.Common
{
    
    
    /// <summary>
    ///This is a test class for RMConfigurationSettingsTest and is intended
    ///to contain all RMConfigurationSettingsTest Unit Tests
    ///</summary>
    [TestClass()]
    public class RMConfigurationSettingsTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for SetSMTPServer
        ///</summary>
        [TestMethod()]
        public void SetSMTPServerTest()
        {
            string strSMTPServer = "relay.csc.com";
            string strAdminEmailAddr = "risksupp@csc.com";
            int expected = 1;
            int actual;
            actual = RMConfigurationSettings.SetSMTPServer(strSMTPServer, strAdminEmailAddr);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for GetSMTPServerSettings
        ///</summary>
        [TestMethod()]
        public void GetSMTPServerSettingsTest()
        {
            DataTable expected = new DataTable();
            DataTable actual;
            actual = RMConfigurationSettings.GetSMTPServerSettings();

            Assert.AreEqual(string.Empty, actual.Rows[0]["SMTP_SERVER"].ToString());
        }

        /// <summary>
        ///A test for GetSMTPServer
        ///</summary>
        //[TestMethod()]
        //public void GetSMTPServerTest()
        //{
        //    string expected = "relay.csc.com";
        //    string actual;
        //    actual = RMConfigurationSettings.GetSMTPServer();
        //    Assert.AreEqual(expected, actual);
        //}
    }
}
