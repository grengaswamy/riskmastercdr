﻿using Riskmaster.Security.Authentication;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Security;
using System.Configuration;

namespace Riskmaster.Security.Authentication
{
    
    
    /// <summary>
    ///This is a test class for RiskmasterMembershipProviderTest and is intended
    ///to contain all RiskmasterMembershipProviderTest Unit Tests
    ///</summary>
    [TestClass()]
    public class RiskmasterMembershipProviderTest
    {


        private TestContext testContextInstance;
        private static string m_strSecDatabaseConnString = string.Empty;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void InitializeRiskmasterMembershipProviderTest(TestContext testContext)
        {
            m_strSecDatabaseConnString = ConfigurationManager.ConnectionStrings["RMXSecurity_SQLServer"].ConnectionString;

        }
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for GetUser
        ///</summary>
        [TestMethod()]
        public void GetUserTest()
        {
            string strDbConnString = m_strSecDatabaseConnString;
            RiskmasterMembershipProvider target = new RiskmasterMembershipProvider(strDbConnString);
            object providerUserKey = "csc";
            bool userIsOnline = false;
            MembershipUser expected = new RiskmasterMembershipUser(providerUserKey.ToString(), "RiskmasterMembershipProvider", m_strSecDatabaseConnString);
            MembershipUser actual;
            actual = target.GetUser(providerUserKey, userIsOnline);
            Assert.AreEqual(expected.UserName, actual.UserName);
        }
    }
}
