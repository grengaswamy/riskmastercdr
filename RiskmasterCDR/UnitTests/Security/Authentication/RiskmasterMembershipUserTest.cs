﻿using Riskmaster.Security.Authentication;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;
using System.Linq;

namespace Riskmaster.Security.Authentication
{
    
    
    /// <summary>
    ///This is a test class for RiskmasterMembershipUserTest and is intended
    ///to contain all RiskmasterMembershipUserTest Unit Tests
    ///</summary>
    [TestClass()]
    public class RiskmasterMembershipUserTest
    {


        private TestContext testContextInstance;
        private static string m_strSecDatabaseConnString = string.Empty;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void InitializeRiskmasterMembershipUserTest(TestContext testContext)
        {
            m_strSecDatabaseConnString = ConfigurationManager.ConnectionStrings["RMXSecurity_SQLServer"].ConnectionString;
        }
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for IsValidRiskmasterUser
        ///</summary>
        [TestMethod()]
        public void IsValidRiskmasterUserTest()
        {
            string strUserName = "dtg";
            string strSecConnectionString = m_strSecDatabaseConnString;
            bool expected = true;
            bool actual;
            actual = RiskmasterMembershipUser.IsValidRiskmasterUser(strSecConnectionString, strUserName);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for IsValidRiskmasterUser
        ///</summary>
        [TestMethod()]
        public void IsInvalidRiskmasterUserTest()
        {
            string strUserName = "rmx";
            string strSecConnectionString = m_strSecDatabaseConnString;
            bool expected = false;
            bool actual;
            actual = RiskmasterMembershipUser.IsValidRiskmasterUser(strSecConnectionString, strUserName);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        ///
        public void SortUserCollectionTest()
        {
            var Person1 = new {LastName="Vaidya", FirstName="Samir"};
            var Person2 = new {LastName="Vaidya", FirstName="Payal"};
            var Person3 = new {LastName="Palinski", FirstName="Mike"};
            var Person4 = new {LastName="Kumar", FirstName="Amit"};
            var people = new[] { Person1, Person2, Person3, Person4 };

            var sort1 = people
                    .OrderBy(c => c.LastName)
                    .ThenBy(c => c.FirstName);

            foreach (var person in sort1)
            {
                System.Diagnostics.Debug.WriteLine(person.FirstName);
                System.Diagnostics.Debug.WriteLine(person.LastName);
            }

            Assert.Inconclusive();
        } // method: SortUserCollectionTest


      
    }
}
