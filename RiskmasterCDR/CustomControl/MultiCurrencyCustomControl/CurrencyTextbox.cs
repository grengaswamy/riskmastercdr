﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Threading;
using System.Drawing;
using System.ComponentModel;
using System.Text;
using System.Collections.Specialized;

namespace MultiCurrencyCustomControl
{
    /// <summary>
    /// Created By: Deb Jena
    /// Date : 28-Nov-2011
    /// For Multi-Currency in Riskmaster 
    /// </summary>
    [
    DefaultProperty("Amount"),
    DefaultEvent("AmountChanged"),
    ToolboxBitmap(typeof(CurrencyTextbox))
    ]
    [ToolboxData("<{0}:CurrencyTextBox runat=server></{0}:CurrencyTextBox>")]
    public class CurrencyTextbox : CompositeControl,IPostBackDataHandler
    {
        private CurrentProperties _currentProps;
        #region .Declarations
        // Properties
        protected TextAlign _Alignment;
        protected int _Precision;
        protected string _OnFocus;
        protected string _OnKeyPress;
        protected string _OnBlur;
        protected decimal _MinAmount;
        protected decimal _MaxAmount;
        protected Color _NegativeColor = Color.Red;
        protected Color _PositiveColor;
        protected double _AmountInString;
        // Events
        [Browsable(true), Category("Action")]
        public event EventHandler AmountChanged;
        #endregion // Declarations

        #region .Constructor
        /// <summary>
        /// Initializes a new instance of the CurrencyTextbox class
        /// </summary>
        public CurrencyTextbox()
        {
            base.Init += new EventHandler(this.CurrencyTextbox_Init);
            this._currentProps = new CurrentProperties();
            _Alignment = TextAlign.Left;
            _Precision = 2;
            _MinAmount = -999999999999999M;
            _MaxAmount = 999999999999999M;
            _OnFocus = string.Empty;
            _OnKeyPress = string.Empty;
            _OnBlur = string.Empty;
        }
        #endregion // Constructor

        #region .Properties
        
        /// <summary>
        /// Gets or sets the BaseCurrencyCultureName.
        /// </summary>
        [
        Description("The BaseCurrencyCultureName displayed in the control")
        ]
        public string BaseCurrencyCultureName
        {
            get
            {
                return this._currentProps.BaseCurrencyCultureName;
            }
            set
            {
                this._currentProps.BaseCurrencyCultureName = value;
            }
        }
        /// <summary>
        /// Gets or sets the CurrencyCultureName.
        /// </summary>
        [
        Description("The CurrencyCultureName displayed in the control")
        ]
        public string CurrencyCultureName
        {
            get
            {
                return this._currentProps.CurrencyCultureName;
            }
            set
            {
                this._currentProps.CurrencyCultureName = value;
            }
        }
        /// <summary>
        /// Gets the amount as a string within the specifed culture.
        /// </summary>
        [
        Browsable(false)
        ]
        public string Text
        {
            get
            {
                return Amount.ToString(string.Format("c{0}", _Precision));
            }
        }
        /// <summary>
        /// Gets and Sets the amount as in string with culture as en-US.
        /// </summary>
        [
        Browsable(false)
        ]
        public string AmountInString
        {
            get
            {
                string sCulture = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
                Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                string sAmount = Convert.ToString(Amount);
                Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(sCulture);
                return sAmount;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                   value = "0";
                }
                string sCulture = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
                Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                //srajindersin 14/08/2014
                //this.Amount = decimal.Parse(value, NumberStyles.Any);
                decimal decReturnValue = 0.0M;
                //RMA-1230 START
				//bool blnSuccess = Decimal.TryParse(value, out decReturnValue);
                bool blnSuccess = decimal.TryParse(value, NumberStyles.Any, Thread.CurrentThread.CurrentCulture, out decReturnValue);
                //RMA-1230 END
                this.Amount = decimal.Parse(Convert.ToString(decReturnValue), NumberStyles.Any);
                Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(sCulture);
            }
        }
        /// <summary>
        /// Gets or sets the amount.
        /// </summary>
        [
        DefaultValue(typeof(decimal), "0"),
        Category("Appearance"),
        Bindable(true),
        Description("The amount displayed in the control")
        ]
        public decimal Amount
        {
            get
            {
                object amount = ViewState["amount"];
                if (amount == null)
                    return 0M;
                else
                    return (decimal)amount;
            }
            set
            {
                ViewState["amount"] = value;
            }
        }
        /// <summary>
        /// Gets or sets the name of a client script function to call when the 
        /// control gains focus.
        /// </summary>
        [
        Category("MultiCurrencyScripts"),
        Description("The name of the client script function(s) to call when the " +
            "control gains focus")
        ]
        public string OnFocus
        {
            get
            {
                return _OnFocus;
            }
            set
            {
                _OnFocus = value;
            }
        }
        /// <summary>
        /// Gets or sets the name of a client script function to call when the 
        /// control receives key press events.
        /// </summary>
        [
        Category("MultiCurrencyScripts"),
        Description("The name of the client script function(s) to call when the " +
            "control receives key press events")
        ]
        public string OnKeyPress
        {
            get
            {
                return _OnKeyPress;
            }
            set
            {
                _OnKeyPress = value;
            }
        }
        /// <summary>
        /// Gets or sets the name of the client script function(s) to call when the 
        /// control loses focus.
        [
        Category("MultiCurrencyScripts"),
        Description("The name of the client script function(s) to call when the " +
            "control loses focus")
        ]
        public string OnBlur
        {
            get
            {
                return _OnBlur;
            }
            set
            {
                _OnBlur = value;
            }
        }
        /// <summary>
        /// Gets or sets the alignment of the text in the control.
        /// </summary>
        [
        DefaultValue(typeof(TextAlign), "Left"),
        Category("Appearance"),
        Bindable(false),
        Description("The alignment of text in the control")
        ]
        public TextAlign Alignment
        {
            get
            {
                return _Alignment;
            }
            set
            {
                _Alignment = value;
            }
        }
        /// <summary>
        /// Gets of sets the minumum allowable amount.
        /// </summary>
        [
        DefaultValue(typeof(decimal), "-999999999999999"),
        Category("Appearance"),
        Bindable(true),
        Description("The minumum allowable amount")
        ]
        public decimal MinAmount
        {
            get
            {
                return _MinAmount;
            }
            set
            {
                _MinAmount = value;
            }
        }
        /// <summary>
        /// Gets of sets the maximum allowable amount.
        /// </summary>
        [
        DefaultValue(typeof(decimal), "999999999999999"),
        Category("Appearance"),
        Bindable(true),
        Description("The maximum allowable amount")
        ]
        public decimal MaxAmount
        {
            get
            {
                return _MaxAmount;
            }
            set
            {
                _MaxAmount = value;
            }
        }
        /// <summary>
        /// Gets the colour of the text.
        /// </summary>
        [
        Browsable(false)
        ]
        public new Color ForeColor
        {
            get
            {
                return base.ForeColor;
            }
        }
        //rupal:start , multicurrency enh
        //Setting Enabled to False will not retain the value of multicurrency textbox during postbacks so we have 
        //overrided the Enabled property which sets the control to readonly when it disabled. Thus we will have value of control available in viewstate during postbacks
        public new bool Enabled
        {
            set
            {
                if (value)
                {
                    base.Enabled = true;
                    this.Attributes.Remove("readonly");
                    this.Style.Add(HtmlTextWriterStyle.BackgroundColor, "");
                    
                }
                else
                {
                    this.Attributes.Add("readonly", "readonly");
                    this.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#F2F2F2");
                }
            }
        }

        public bool ReadOnly
        {
            set
            {
                if (value)
                {
                    this.Attributes.Add("readonly", "readonly");
                    this.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#F2F2F2");
                }
                else
                {
                    this.Enabled = true;
                    this.Attributes.Remove("readonly");
                    this.Style.Add(HtmlTextWriterStyle.BackgroundColor, "");
                }
            }
        }
        //rupal:end
        /// <summary>
        /// Gets or sets the text color if the Amount is positive.
        /// </summary>
        [
        DefaultValue(typeof(Color), "Empty"),
        Category("Appearance"),
        Bindable(true),
        Description("The colour of positive currency values")
        ]
        public Color PositiveColor
        {
            get
            {
                return _PositiveColor;
            }
            set
            {
                _PositiveColor = value;
                // Set the controls ForeColor if appropriate
                if (Amount >= 0)
                {
                    base.ForeColor = value;
                }
            }
        }
        /// <summary>
        /// Gets or sets the text color if the Amount is negative.
        /// </summary>
        [
        DefaultValue(typeof(Color), "Red"),
        Category("Appearance"),
        Bindable(true),
        Description("The colour of negative currency values")
        ]
        public Color NegativeColor
        {
            get
            {
                return _NegativeColor;
            }
            set
            {
                _NegativeColor = value;
                // Set the controls ForeColor if appropriate
                if (Amount < 0)
                    base.ForeColor = value;
            }
        }
        #endregion //Properties
       
        #region .IPostBackDataHandler interface
       /// <summary>
       /// Processes post back data for the control.
       /// </summary>
       /// <param name="postDataKey"></param>
       /// <param name="pCollection"></param>
       /// <returns></returns>
        public bool LoadPostData(string postDataKey, NameValueCollection pCollection)
        {
            string currency = pCollection[postDataKey];
            //nsachdeva2 - 02/20/2012 - MITS 27536
            if (string.IsNullOrEmpty(currency))
            {
                currency = "0";
            }
            //End MITS: 27536
            try
            {
                decimal amount = decimal.Parse(currency, NumberStyles.Any);
                if (amount != Amount)
                {
                    Amount = amount;
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                string sCulture = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
                Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(this.CurrencyCultureName);
                decimal amount = decimal.Parse(currency, NumberStyles.Any);
                Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(sCulture);
                if (amount != Amount)
                {
                    Amount = amount;
                    return true;
                }
                else
                    return false;
            }
        }

        /// <summary>
        /// RaisePostDataChangedEvent
        /// </summary>
        public void RaisePostDataChangedEvent()
        {
            OnAmountChanged(new EventArgs());
        }
        /// <summary>
        /// Raises the AmountChanged event.
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnAmountChanged(EventArgs e)
        {
            if (AmountChanged != null)
            {
                AmountChanged(this, e);
            }
        }
       /// <summary>
       /// Returns the System.Web.UI.HtmlTextWriterTag.
       /// </summary>
        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Input;
            }
        }
        #endregion

        #region .Protected methods
        /// <summary>
        /// Adds HTML attributes and styles that need to be rendered.
        /// </summary>
        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            base.AddAttributesToRender(writer);
            writer.AddAttribute(HtmlTextWriterAttribute.Type, "text");
            writer.AddAttribute(HtmlTextWriterAttribute.Name, UniqueID);
            writer.AddStyleAttribute("text-align", Alignment.ToString());
            string onFocus = "Format_Currency_To_Decimal_" + this.CurrencyCultureName.Replace('-', '_') + "(this)";
            if (OnFocus != string.Empty)
            {
                onFocus += ";" + OnFocus;
            }
            string onBlur = "Format_Decimal_To_Currency_" + this.CurrencyCultureName.Replace('-', '_') + "(this)";
            if (OnBlur != string.Empty)
            {
                onBlur += ";" + OnBlur;
            }
            string onKeyPress = "IsNumeric_"+ this.CurrencyCultureName.Replace('-', '_') +"()";
            if (OnKeyPress != string.Empty)
            {
                onKeyPress += ";" + OnKeyPress;
            }
            if (!string.IsNullOrEmpty(this.CurrencyCultureName))
            {
                string sCulture = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(this.CurrencyCultureName);
                writer.AddAttribute(HtmlTextWriterAttribute.Value, Text);
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(sCulture);
            }
            //else part can be removed as the CurrencyCultureName cant be empty here after OnPreRender but for safeside its here
            else
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Value, Text);
            }
            writer.AddAttribute("negativeColor", NegativeColor.Name);
            if (PositiveColor != Color.Empty)
            {
                writer.AddAttribute("positiveColor", PositiveColor.Name);
            }
            writer.AddAttribute("onkeypress", onKeyPress);
            writer.AddAttribute("minAmount", MinAmount.ToString());
            writer.AddAttribute("maxAmount", MaxAmount.ToString());
            writer.AddAttribute("onfocus", onFocus);
            writer.AddAttribute("onblur", onBlur);
            //rupal
            writer.AddAttribute("Amount", Convert.ToString(Amount));
        }
        /// <summary>
        /// LoadControlState
        /// </summary>
        /// <param name="savedState"></param>
        protected override void LoadControlState(object savedState)
        {
            this._currentProps = (CurrentProperties)savedState;
        }
        protected override object SaveControlState()
        {
            return this._currentProps;
        }
        /// <summary>
        /// Raises the PreRender event.
        /// </summary>
        protected override void OnPreRender(EventArgs e)
        {
            if (string.IsNullOrEmpty(this.CurrencyCultureName))
            {
                this.CurrencyCultureName = Thread.CurrentThread.CurrentCulture.Name;
            }
            else if (this.CurrencyCultureName!=this.BaseCurrencyCultureName)
            {
                this.CurrencyCultureName = Thread.CurrentThread.CurrentCulture.Name;
            }
            RegisterFormatScripts(Page, this.CurrencyCultureName);
            MultiCurrencyScriptHelper.RegisterCurrencyToDecimalScript(Page,this.CurrencyCultureName);
            MultiCurrencyScriptHelper.RegisterDecimalToCurrencyScript(Page,this.CurrencyCultureName);
            MultiCurrencyScriptHelper.RegisterIsNumericScript(Page,this.CurrencyCultureName);
            base.OnPreRender(e);
        }
        #endregion // Protected methods

        #region .Private Methods
        /// <summary>
        /// CurrencyTextbox_Init
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CurrencyTextbox_Init(object sender, EventArgs e)
        {
            this.Page.RegisterRequiresControlState(this);
        }
        /// <summary>
        ///  Registers the Format_Decimal_To_Currency() and Format_Currency_To_Decimal()
        ///  script functions.
        /// </summary>
        private void RegisterFormatScripts(Page page,string sCulture)
        {
            if (!page.ClientScript.IsClientScriptBlockRegistered("MultiCurrencyScripts_" + sCulture.Replace('-', '_')))
            {
                NumberFormatInfo format = new CultureInfo(sCulture).NumberFormat;
                string minusSign = ((int)format.NegativeSign[0]).ToString("X");
                StringBuilder script = new StringBuilder();
                script.Append(Environment.NewLine);
                script.Append("<script language=javascript>");
                script.Append(Environment.NewLine);
                script.Append("<!--");
                script.Append(Environment.NewLine);
                script.Append("function Format_Decimal_To_Currency_"+sCulture.Replace('-','_')+"(c)");
                script.Append(Environment.NewLine);
                script.Append("{");
                script.Append("var max=new Number(c.getAttribute('maxAmount'));");
                script.Append("if(c.value>max){c.value=max}");
                script.Append("var min=new Number(c.getAttribute('minAmount'));");
                script.Append("if(c.value<min){c.value=min}");
                //script.Append("c.value=Decimal_To_Currency_" + sCulture.Replace('-', '_') + "(c.value,");
                script.Append("c.value=Decimal_To_Currency_" + sCulture.Replace('-', '_') + "(c,");
                script.Append(_Precision.ToString());
                script.Append(");");
                script.Append("c.style.color=");
                script.Append(@"(c.value.match(/\x");
                script.Append(minusSign);
                script.Append("/)==null?");
                if (PositiveColor != Color.Empty)
                {
                    script.Append("c.getAttribute(\"positiveColor\"):");
                }
                else
                {
                    script.Append("\"black\":");
                }
                script.Append("c.getAttribute(\"negativeColor\"));");
                script.Append("}");
                script.Append(Environment.NewLine);
                script.Append("function Format_Currency_To_Decimal_" + sCulture.Replace('-', '_') + "(c)");
                script.Append(Environment.NewLine);
                script.Append("{");
                //script.Append("c.value=Currency_To_Decimal_" + sCulture.Replace('-', '_') + "(c.value);");
                script.Append("c.value=Currency_To_Decimal_" + sCulture.Replace('-', '_') + "(c);");
                script.Append("c.style.color=\"black\";");
                script.Append("c.select();");
                script.Append("}");
                script.Append(Environment.NewLine);
                script.Append("// -->");
                script.Append(Environment.NewLine);
                script.Append("</script>");
                page.ClientScript.RegisterClientScriptBlock(page.GetType(), "MultiCurrencyScripts_" + sCulture.Replace('-', '_'), script.ToString());
            }
        }
        #endregion

        public void SetProperties(string cultureName, string baseCultureName)
        {
            this.CurrencyCultureName = cultureName;
            this.BaseCurrencyCultureName = baseCultureName;
        }
        [Serializable]
        private struct CurrentProperties
        {
            //Add Properties to be saved for each control
            public string CurrencyCultureName;
            public string BaseCurrencyCultureName;
        }
    }
}
