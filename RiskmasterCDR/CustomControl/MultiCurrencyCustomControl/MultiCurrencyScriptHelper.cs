﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Threading;
using System.Drawing;
using System.ComponentModel;
using System.Text;
using System.Collections.Specialized;

namespace MultiCurrencyCustomControl
{
    /// <summary>
    /// Created By: Deb Jena
    /// Date : 28-Nov-2011
    /// </summary>
	public class MultiCurrencyScriptHelper
	{
        public static string GetCurrencySymbols(NumberFormatInfo format)
        {
            StringBuilder script = new StringBuilder();
            switch (format.CurrencyPositivePattern)
            {
                case 0: // $n
                    script.Append("'");
                    script.Append(format.CurrencySymbol);
                    script.Append("','',");
                    break;
                case 1: // n$
                    script.Append("'','");
                    script.Append(format.CurrencySymbol);
                    script.Append("',");
                    break;
                case 2: // $ n
                    script.Append("'");
                    script.Append(format.CurrencySymbol);
                    script.Append(" ','',");
                    break;
                case 3: // n $
                    script.Append("'',' ");
                    script.Append(format.CurrencySymbol);
                    script.Append("',");
                    break;
            }
            switch (format.CurrencyNegativePattern)
            {
                case 0: // ($n)
                    script.Append("'(");
                    script.Append(format.CurrencySymbol);
                    script.Append("',')'");
                    break;
                case 1:  // -$n
                    script.Append("'");
                    script.Append(format.NegativeSign);
                    script.Append(format.CurrencySymbol);
                    script.Append("',''");
                    break;
                case 2: // $-n
                    script.Append("'");
                    script.Append(format.CurrencySymbol);
                    script.Append(format.NegativeSign);
                    script.Append("',''");
                    break;
                case 3: // $n-
                    script.Append("'");
                    script.Append(format.CurrencySymbol);
                    script.Append("','");
                    script.Append(format.NegativeSign);
                    script.Append("'");
                    break;
                case 4: // (n$)
                    script.Append("'(','");
                    script.Append(format.CurrencySymbol);
                    script.Append(")'");
                    break;
                case 5: // -n$
                    script.Append("'");
                    script.Append(format.NegativeSign);
                    script.Append("','");
                    script.Append(format.CurrencySymbol);
                    script.Append("'");
                    break;
                case 6: // n-$
                    script.Append("'','");
                    script.Append(format.NegativeSign);
                    script.Append(format.CurrencySymbol);
                    script.Append("'");
                    break;
                case 7: // n$-
                    script.Append("'','");
                    script.Append(format.CurrencySymbol);
                    script.Append(format.NegativeSign);
                    script.Append("'");
                    break;
                case 8: // -n $
                    script.Append("'");
                    script.Append(format.NegativeSign);
                    script.Append("',' ");
                    script.Append(format.CurrencySymbol);
                    script.Append("'");
                    break;
                case 9: // -$ n
                    script.Append("'");
                    script.Append(format.NegativeSign);
                    script.Append(format.CurrencySymbol);
                    script.Append(" ',''");
                    break;
                case 10: // n $-
                    script.Append("'',' ");
                    script.Append(format.CurrencySymbol);
                    script.Append(format.NegativeSign);
                    script.Append("'");
                    break;
                case 11: // $ n-
                    script.Append("'");
                    script.Append(format.CurrencySymbol);
                    script.Append(" ','");
                    script.Append(format.NegativeSign);
                    script.Append("'");
                    break;
                case 12: // $ -n
                    script.Append("'");
                    script.Append(format.CurrencySymbol);
                    script.Append(" ");
                    script.Append(format.NegativeSign);
                    script.Append("',''");
                    break;
                case 13: // n- $
                    script.Append("'','");
                    script.Append(format.NegativeSign);
                    script.Append(" ");
                    script.Append(format.CurrencySymbol);
                    script.Append("'");
                    break;
                case 14: // ($ n)
                    script.Append("'(");
                    script.Append(format.CurrencySymbol);
                    script.Append(" ',')'");
                    break;
                case 15: // (n $)
                    script.Append("'(',' ");
                    script.Append(format.CurrencySymbol);
                    script.Append(")'");
                    break;
            }
            return script.ToString();
        }
        /// <summary>
        /// Constructs javascript code to declare a function
        /// </summary>
		public static string BeginScriptFunction(string name, params string[] parameters)
		{
			StringBuilder script = new StringBuilder();
			script.Append(Environment.NewLine);
			script.Append("<script language=javascript>");
			script.Append(Environment.NewLine);
			script.Append("<!--");
			script.Append(Environment.NewLine);
			script.Append("function ");
			script.Append(name);
			script.Append("(");
			for (int i = 0; i < parameters.Length; i++)
			{
				if (i > 0)
				{
					script.Append(",");
				}
				script.Append(parameters[i]);
			}
			script.Append(")");
			script.Append(Environment.NewLine);
			script.Append("{");
			return script.ToString();
		}
        /// <summary>
        /// Constructs javascript code to declare the end of a function
        /// </summary>
		public static string EndScriptFunction()
		{
			StringBuilder script = new StringBuilder();
			script.Append("}");
			script.Append(Environment.NewLine);
			script.Append("// -->");
			script.Append(Environment.NewLine);
			script.Append("</script>");
			return script.ToString();
		}
        /// <summary>
        /// Constructs javascript code to declare an array
        /// </summary>
		public static string DeclareArray(string name, params object[] values)
		{
			StringBuilder script = new StringBuilder();
			script.Append("var ");
			script.Append(name);
			script.Append("=new Array(");
			for (int i=0; i < values.Length; i++)
			{
				if (i > 0)
				{
					script.Append(",");
				}
				if (IsNumeric( values[i]))
				{
					script.Append(values[i].ToString());
				}
				else
				{
					script.Append("'");
					script.Append(values[i].ToString());
					script.Append("'");
				}
			}
			script.Append(");");
			return script.ToString();
		}
        /// <summary>
        /// Determines if the expresion is a number
        /// </summary>
		private static bool IsNumeric(object expression)
		{
			if (expression is int)
				return true;
			else if (expression is double)
				return true;
			else
				return false;
		}

        #region .Public Methods
        public static void RegisterCurrencyToDecimalScript(Page page,string sCulture)
        {
            if (!page.ClientScript.IsClientScriptBlockRegistered("Currency_To_Decimal_"+sCulture.Replace('-','_')))
            {
                NumberFormatInfo format = new CultureInfo(sCulture).NumberFormat;
                string separator = ((int)format.NumberDecimalSeparator[0]).ToString("X");
                string minusSign = ((int)format.NegativeSign[0]).ToString("X");                
                StringBuilder script = new StringBuilder();
                
                //rupal:start, r8 multicurrency
                //script.Append(MultiCurrencyScriptHelper.BeginScriptFunction("Currency_To_Decimal_"+sCulture.Replace('-','_'), "n"));
                script.Append(MultiCurrencyScriptHelper.BeginScriptFunction("Currency_To_Decimal_" + sCulture.Replace('-', '_'), "c"));                
                script.Append("var n = c.value;");
                //rupal:end
                script.Append("n=n.toString();");
                if (format.CurrencySymbol.IndexOf(format.CurrencyDecimalSeparator) >= 0)
                {
                    script.Append("n=n.replace(/");
                    script.Append(format.CurrencySymbol);
                    script.Append("/,'');");
                }
                int negPat = format.CurrencyNegativePattern;
                if (negPat == 0 || negPat == 4 || negPat == 14 || negPat == 15)
                {
                    script.Append(@"if (n.match(/\(/)!=null) {n='\x");
                    script.Append(minusSign);
                    script.Append("'+n;}");
                }
                script.Append(@"n=n.replace(/[^\d\x");
                script.Append(minusSign);
                script.Append(@"\x");
                script.Append(separator);
                script.Append("]/g,'');");
                //rupal
                //script.Append(@"alert('in Currency_To_Decimal, n is '+n);");
                script.Append("var sValue = '';");
                //script.Append(@"alert('svalue'+sValue);");
                script.Append(@"sValue=n.replace('" + format.CurrencyDecimalSeparator + "','.');");
                script.Append(@"c.setAttribute('Amount',sValue);");
                //script.Append(@"alert('in Currency_To_Decimal '+sValue);");
                //rupal
                script.Append("return n;");
                script.Append(MultiCurrencyScriptHelper.EndScriptFunction());
                page.ClientScript.RegisterClientScriptBlock(page.GetType(), "Currency_To_Decimal_"+sCulture.Replace('-','_'), script.ToString());
            }
        }
        public static void RegisterDecimalToCurrencyScript(Page page,string sCulture)
        {
            if (!page.ClientScript.IsClientScriptBlockRegistered("Decimal_To_Currency_" + sCulture.Replace('-', '_')))
            {
                NumberFormatInfo format = new CultureInfo(sCulture).NumberFormat;
                string separator = ((int)format.NumberDecimalSeparator[0]).ToString("X");
                string minusSign = ((int)format.NegativeSign[0]).ToString("X");
                StringBuilder script = new StringBuilder();
                script.Append(MultiCurrencyScriptHelper.BeginScriptFunction("Decimal_To_Currency_" + sCulture.Replace('-', '_'), "c", "p"));
                //rupal:start, r8 multicurrency
                //set Amount attribute
                script.Append("var n = c.value;");                
                script.Append("var sValue= n;");
                script.Append("sValue = sValue.replace('" + format.CurrencyDecimalSeparator + "','.');");
                script.Append("c.setAttribute('Amount',sValue);");                
                if (format.CurrencyDecimalSeparator != ".")
                {
                    script.Append("n=n.replace('.','" + format.CurrencyDecimalSeparator + "');");
                }
                //script.Append(@"alert('in Decimal_To_Currency '+sValue);");
                //rupal:end
                script.Append("n=n.toString();");
                script.Append("if(p==null){p=2;}");
                script.Append("var sy=new Array(");
                script.Append(MultiCurrencyScriptHelper.GetCurrencySymbols(format));
                script.Append(");");
                script.Append(@"var neg=(n.match(/\x");
                script.Append(minusSign);
                script.Append("/)!=null?true:false);");
                script.Append(@"n=n.replace(/[^\d\x");
                script.Append(separator);
                script.Append("]/g,'');");
                script.Append(@"var m=n.match(/(\d*)(\x");
                script.Append(separator);
                script.Append(@"*)(\d*)/);");
                script.Append("var f=m[3];");
                script.Append("if(f.length>p)");
                script.Append("{");
                script.Append("f=f/Math.pow(10,(f.length-p));");
                script.Append("f=Math.round(f);");
                script.Append("while(f.toString().length<p){f='0'+f};");
                script.Append("}else{");
                script.Append("while(f.toString().length<p){f+='0'};");
                script.Append("}");
                script.Append("var w=new Number(m[1]);");
                script.Append("if(f==Math.pow(10,p)){w+=1;f=f.toString().substr(1);}");
                if (format.CurrencyGroupSizes[0] != 0)
                {
                    script.Append("w=w.toString();");
                    string group = ((int)format.CurrencyGroupSeparator[0]).ToString("X");
                    if (format.PercentGroupSizes.Length == 1)
                    {
                        script.Append("var s=");
                        script.Append(format.CurrencyGroupSizes[0]);
                        script.Append(";");
                        script.Append("var l=w.length-s;");
                        script.Append("while(l>0)");
                        script.Append("{");
                        script.Append(@"w=w.substr(0,l)+'\x");
                        script.Append(group);
                        script.Append("'+w.substr(l);");
                        script.Append("l-=s;");
                        script.Append("}");
                    }
                    else
                    {
                        script.Append("var g=new Array(");
                        for (int i = 0; i < format.CurrencyGroupSizes.Length; i++)
                        {
                            if (i > 0) { script.Append(","); }
                            script.Append(format.CurrencyGroupSizes[i]);
                        }
                        script.Append(");");
                        script.Append("var i=0;");
                        script.Append("var s=g[i];");
                        script.Append("var l=w.length-s;");
                        script.Append("while(l>0)");
                        script.Append("{");
                        script.Append(@"w=w.substr(0,l)+'\x");
                        script.Append(group);
                        script.Append("'+w.substr(l);");
                        script.Append("if(i<g.length-1) {i++;}");
                        script.Append("s=g[i];");
                        script.Append("if (s==0) {break;}");
                        script.Append("l-=s;");
                        script.Append("}");
                    }
                }
                script.Append(@"if(p==0){m[2]='';f=''}else{m[2]='\x");
                script.Append(separator);
                script.Append("'}");

               //rupal

               
                //rupal:end

                script.Append("return (neg?");
                script.Append("sy[2]+w+m[2]+f+sy[3]:");
                script.Append("sy[0]+w+m[2]+f+sy[1]);");

                script.Append(MultiCurrencyScriptHelper.EndScriptFunction());
                page.ClientScript.RegisterClientScriptBlock(page.GetType(), "Decimal_To_Currency_" + sCulture.Replace('-', '_'), script.ToString());
            }
        }
        public static void RegisterIsNumericScript(Page page,string sCulture)
        {
            if (!page.ClientScript.IsClientScriptBlockRegistered("IsNumeric_" + sCulture.Replace('-', '_')))
            {
                NumberFormatInfo format = new CultureInfo(sCulture).NumberFormat;
                int separator = (int)format.NumberDecimalSeparator[0];
                int minusSign = (int)format.NegativeSign[0];
                StringBuilder script = new StringBuilder();
                script.Append(MultiCurrencyScriptHelper.BeginScriptFunction("IsNumeric_" + sCulture.Replace('-', '_')));
                script.Append("var k=window.event.keyCode;");
                script.Append("if(!((k>47&&k<58)||k==");
                script.Append(separator.ToString());
                script.Append("||k==");
                script.Append(minusSign.ToString());
                script.Append("))");
                script.Append("{window.event.returnValue=false;}");
                script.Append(MultiCurrencyScriptHelper.EndScriptFunction());
                page.ClientScript.RegisterClientScriptBlock(page.GetType(), "IsNumeric_" + sCulture.Replace('-', '_'), script.ToString());
            }
        }
        #endregion 
	}
}

