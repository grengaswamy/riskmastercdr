using System;
using System.Data;
using System.Data.Common;
using System.Data.Odbc;
using System.Data.SqlClient;
using Oracle.DataAccess.Client;

namespace Riskmaster.Db
{
	
     /// <summary>Riskmaster.Db.DbDataAdapter wraps a native provider DataAdapter object.</summary>
     /// <remarks>This object also raises two events for the ODBC, Oracle, SQLServer and IBM DB2 providers.  
     /// These events are "RowUpdating" and "RowUpdated."  It is possible to get at native provider objects
     /// via the parameters of these events...This may need to be removed.</remarks>
     public class DbDataAdapter : IDbDataAdapter 
	{
		private IDbDataAdapter m_anyAdapter = null;  //Star of the show

		private DbRowUpdatingEventHandler m_clientUpdatingHandler;// = null;
		private DbRowUpdatedEventHandler m_clientUpdatedHandler;// = null;
		
         /// <summary>
         /// Internal DbDataAdapter constructor taking a the IDbDataAdapter interface of the native Data provider
         /// object to wrap. Used only by DbFactory.</summary>
         /// <param name="anyAdapter">IDbDataAdapter interface of the native Adapter to wrap.</param>
         /// <remarks>This constructor also creates the appropriate provider specific event handlers and hooks 
         /// them up on</remarks>
         internal  DbDataAdapter(IDbDataAdapter anyAdapter)
		{
			m_anyAdapter = anyAdapter;	

			if (anyAdapter is OracleDataAdapter)
			{
				(anyAdapter as OracleDataAdapter).RowUpdating +=new OracleRowUpdatingEventHandler(OracleRowUpdatingConsumer);
				(anyAdapter as OracleDataAdapter).RowUpdated+=new OracleRowUpdatedEventHandler(OracleRowUpdatedConsumer);
			}

			if (anyAdapter is SqlDataAdapter)
			{
				(anyAdapter as SqlDataAdapter).RowUpdating +=new SqlRowUpdatingEventHandler(SqlRowUpdatingConsumer);
				(anyAdapter as SqlDataAdapter).RowUpdated+=new SqlRowUpdatedEventHandler(SqlRowUpdatedConsumer);
			}

			if (anyAdapter is OdbcDataAdapter)
			{
				(anyAdapter as OdbcDataAdapter).RowUpdating +=new OdbcRowUpdatingEventHandler(OdbcRowUpdatingConsumer);
				(anyAdapter as OdbcDataAdapter).RowUpdated+=new OdbcRowUpdatedEventHandler(OdbcRowUpdatedConsumer);
			}

		}

		/****
		 * Provider Specific Pass Thru Handlers
		 * ***/

		 /// <summary>Private function OracleRowUpdatingConsumer handles a
		 /// native provider event by sending it to a generic handler.</summary>
		 /// <param name="sender">Reference to the object raising the event.</param>
		 /// <param name="e">EventArgs style event information.</param>
		  protected void OracleRowUpdatingConsumer(object sender, OracleRowUpdatingEventArgs e){RowUpdatingConsumer(sender, e);}
		
		 /// <summary>Private function OracleRowUpdatedConsumer handles a
		 /// native provider event by sending it to a generic handler.</summary>
		 /// <param name="sender">Reference to the object raising the event.</param>
		 /// <param name="e">EventArgs style event information.</param>
		  protected void OracleRowUpdatedConsumer(object sender, OracleRowUpdatedEventArgs e){RowUpdatedConsumer(sender, e);}
		
		 /// <summary>Private function SqlRowUpdatingConsumer handles a
		 /// native provider event by sending it to a generic handler.</summary>
		 /// <param name="sender">Reference to the object raising the event.</param>
		 /// <param name="e">EventArgs style event information.</param>
		  protected void SqlRowUpdatingConsumer(object sender, SqlRowUpdatingEventArgs e){RowUpdatingConsumer(sender, e);}
		
		 /// <summary>Private function SqlRowUpdatedConsumer handles a
		 /// native provider event by sending it to a generic handler.</summary>
		 /// <param name="sender">Reference to the object raising the event.</param>
		 /// <param name="e">EventArgs style event information.</param>
		  protected void SqlRowUpdatedConsumer(object sender, SqlRowUpdatedEventArgs e){RowUpdatedConsumer(sender, e);}
		
		 /// <summary>Private function OdbcRowUpdatingConsumer handles a
		 /// native provider event by sending it to a generic handler.</summary>
		 /// <param name="sender">Reference to the object raising the event.</param>
		 /// <param name="e">EventArgs style event information.</param>
		  protected void OdbcRowUpdatingConsumer(object sender, OdbcRowUpdatingEventArgs e){RowUpdatingConsumer(sender, e);}
		
		 /// <summary>Private function OdbcRowUpdatedConsumer handles a
		 /// native provider event by sending it to a generic handler.</summary>
		 /// <param name="sender">Reference to the object raising the event.</param>
		 /// <param name="e">EventArgs style event information.</param>
		  protected void OdbcRowUpdatedConsumer(object sender, OdbcRowUpdatedEventArgs e){RowUpdatedConsumer(sender, e);}

		/****
		 * The Real Handlers (just raise to our clients if any.)
		 * ***/
		
		 /// <summary>Private function RowUpdatingConsumer handles all
		 /// native provider RowUpdating events by wrapping the arguments and raising
		 /// the event to any interested client.</summary>
		 /// <param name="sender">Reference to the object that originally raised the event.</param>
		 /// <param name="e">EventArgs style event information.</param>
		 /// <remarks>It is possible to get to provider specific objects this way... Consumer beware.</remarks>
		  protected void RowUpdatingConsumer(object sender, RowUpdatingEventArgs e)
		{
			if(m_clientUpdatingHandler !=null)
				m_clientUpdatingHandler(this, new DbRowUpdatingEventArgs(e));
			// m_clientUpdatingHandler(sender, new DbRowUpdatingEventArgs(e));
		 }
		
		 /// <summary>Private function RowUpdatedConsumer handles all
		 /// native provider RowUpdated events by wrapping the arguments and raising
		 /// the event to any interested client.</summary>
		 /// <param name="sender">Reference to the object that originally raised the event.</param>
		 /// <param name="e">EventArgs style event information.</param>
		 /// <remarks>It is possible to get to provider specific objects this way... Consumer beware.</remarks>
		  protected void RowUpdatedConsumer(object sender, RowUpdatedEventArgs e)
		{
			if(m_clientUpdatedHandler !=null)
				m_clientUpdatedHandler(this, new DbRowUpdatedEventArgs(e));
			 //m_clientUpdatedHandler(sender, new DbRowUpdatedEventArgs(e));
		}

		//Select Command Property

		 /// <summary>Wraps the method of the same name from the native data provider.  
		 /// Present only to satisfy the interface mapping requirements.</summary>
         /// <remarks>none</remarks>
         IDbCommand IDbDataAdapter.SelectCommand	
		{
			get { return this.SelectCommand; }
			set 
			{
				DbCommand _cmd = value as DbCommand;
				if (_cmd == null)
					throw new System.Exception(String.Format("DbDataAdapter recieved a {0} type command where a DbCommand type is required.",value.GetType().FullName));
				this.SelectCommand = _cmd;
			 }
		}
		
		 /// <summary>Riskmaster.Db.DbDataAdapter.SelectCommand wraps the Native property of the same name.</summary>
		 /// <remarks>See ADO.NET documentation for details.</remarks>
		 /// <example>No example available.</example>
          public DbCommand SelectCommand 
		{
			get { return new DbCommand(m_anyAdapter.SelectCommand); }
			set { m_anyAdapter.SelectCommand = value.AsNative(); }
		}

		//Delete Command Property
		
		 /// <summary>Wraps the method of the same name from the native data provider.  
		 /// Present only to satisfy the interface mapping requirements.</summary>
		 /// <remarks>none</remarks>
		 IDbCommand IDbDataAdapter.DeleteCommand	
		{
			get { return this.DeleteCommand; }
			set 
			{
				DbCommand _cmd = value as DbCommand;
				if (_cmd == null)
					throw new System.Exception(String.Format("DbDataAdapter recieved a {0} type command where a DbCommand type is required.",value.GetType().FullName));
				this.DeleteCommand = _cmd;
			}
		}
		
		 /// <summary>Riskmaster.Db.DbDataAdapter.DeleteCommand wraps the Native property of the same name.</summary>
		 /// <remarks>See ADO.NET documentation for details.</remarks>
		 /// <example>No example available.</example>
		  public DbCommand DeleteCommand 
		{
			get { return new DbCommand(m_anyAdapter.DeleteCommand); }
			set { m_anyAdapter.DeleteCommand = value.AsNative(); }
		}

		//Update Command Property
		
		 /// <summary>Wraps the method of the same name from the native data provider.  
		 /// Present only to satisfy the interface mapping requirements.</summary>
		 /// <remarks>none</remarks>
		 IDbCommand IDbDataAdapter.UpdateCommand	
		{
			get { return this.UpdateCommand; }
			set 
			{
				DbCommand _cmd = value as DbCommand;
				if (_cmd == null)
					throw new System.Exception(String.Format("DbDataAdapter recieved a {0} type command where a DbCommand type is required.",value.GetType().FullName));
				this.UpdateCommand = _cmd;
			}
		}
		
		 /// <summary>Riskmaster.Db.DbDataAdapter.UpdateCommand wraps the Native property of the same name.</summary>
		 /// <remarks>See ADO.NET documentation for details.</remarks>
		 /// <example>No example available.</example>
		  public DbCommand UpdateCommand 
		{
			get { return new DbCommand(m_anyAdapter.UpdateCommand); }
			set { m_anyAdapter.UpdateCommand = value.AsNative(); }
		}

		//Insert Command Property
		
		 /// <summary>Wraps the method of the same name from the native data provider.  
		 /// Present only to satisfy the interface mapping requirements.</summary>
		 /// <remarks>none</remarks>
		 IDbCommand IDbDataAdapter.InsertCommand	
		{
			get { return this.InsertCommand; }
			set 
			{
				DbCommand _cmd = value as DbCommand;
				if (_cmd == null)
					throw new System.Exception(String.Format("DbDataAdapter recieved a {0} type command where a DbCommand type is required.",value.GetType().FullName));
				this.InsertCommand = _cmd;
			}
		}
		
		 /// <summary>Riskmaster.Db.DbDataAdapter.UpdateCommand wraps the Native property of the same name.</summary>
		 /// <remarks>See ADO.NET documentation for details.</remarks>
		 /// <example>No example available.</example>
		  public DbCommand InsertCommand 
		{
			get { return new DbCommand(m_anyAdapter.InsertCommand); }
			set { m_anyAdapter.InsertCommand = value.AsNative(); }
		}

		 /// <summary>Wraps the method of the same name from the native data provider.  
		 /// Present only to satisfy the interface mapping requirements.</summary>
		 /// <remarks>none</remarks>
		 IDataParameter[] IDataAdapter.GetFillParameters(){return this.GetFillParameters();}
		
		 /// <summary>GetFillParameters wraps  the method of the same name from the native data provider.</summary>  
		 /// <remarks>See the ADO.Net documentation for details.</remarks>
         DbParameter[] GetFillParameters()
		{
			Array _arr = null;
			DbParameter[] _ret = null;
			int i=0;

			_arr = m_anyAdapter.GetFillParameters();
			_ret = new DbParameter[_arr.Length];
			foreach(IDbDataParameter _anyParam in _arr)
				_ret[i++] = new DbParameter(_anyParam);
			return _ret;
		}

		
		 /// <summary>FillSchema wraps  the method of the same name from the native data provider.  </summary>
		 /// <param name="ds"></param>
         /// <param name="schemaType"></param>
		 /// <remarks>See the ADO.Net documentation for details.</remarks>
		 public  DataTable[] FillSchema(DataSet ds, SchemaType schemaType){return m_anyAdapter.FillSchema(ds,schemaType);}
		
		 /// <summary>Update wraps  the method of the same name from the native data provider.</summary>
		 /// <param name="ds"></param>
		 /// <remarks>See the ADO.Net documentation for details.</remarks>
         public  int Update(DataSet ds){return m_anyAdapter.Update(ds);}
		
		 /// <summary>Fill wraps  the method of the same name from the native data provider.</summary>
		 /// <param name="ds"></param>
		 /// <remarks>See the ADO.Net documentation for details.</remarks>
		 public  int Fill(DataSet ds){return m_anyAdapter.Fill(ds);}

		
		 /// <summary>MissingMappingAction wraps  the property of the same name from the native data provider.</summary>
		 /// <remarks>See the ADO.Net documentation for details.</remarks>
		 public  MissingMappingAction MissingMappingAction{	get{return m_anyAdapter.MissingMappingAction;}set{m_anyAdapter.MissingMappingAction = value;}}
		
		 /// <summary>MissingSchemaAction wraps  the property of the same name from the native data provider.</summary>
		 /// <remarks>See the ADO.Net documentation for details.</remarks>
		 public  MissingSchemaAction MissingSchemaAction{	get{return m_anyAdapter.MissingSchemaAction;}set{m_anyAdapter.MissingSchemaAction= value;}}
		
		//TODO Are tablemapping objects & or the collection going to get us into trouble?  Do they need to be wrapped?
		
		 /// <summary>Wraps the method of the same name from the native data provider.  
		 /// Present only to satisfy the interface mapping requirements.</summary>
		 /// <remarks>none</remarks>
		 ITableMappingCollection IDataAdapter.TableMappings{get{return this.TableMappings;}}
		
		 /// <summary>TableMappings wraps  the property of the same name from the native data provider.</summary>
		 /// <remarks>See the ADO.Net documentation for details.</remarks>
         protected  ITableMappingCollection TableMappings{get{return m_anyAdapter.TableMappings;}}

		 /// <summary>The DbDataAdapter is about to update a row in the Dataset.</summary>
		 public event DbRowUpdatingEventHandler RowUpdating
		{
			add { m_clientUpdatingHandler += value; }
			remove { m_clientUpdatingHandler -= value; }
		}

		 /// <summary>The DbDataAdapter has just updated a row in the Dataset.</summary>
		 public event DbRowUpdatedEventHandler RowUpdated
		{
			add { m_clientUpdatedHandler += value; }
			remove { m_clientUpdatedHandler -= value; }
		}

         /// <summary>
         /// Gets or sets a value that enables or disables batch processing support, 
         /// and specifies the number of commands that can be executed in a batch.
         /// </summary>
         public int UpdateBatchSize
         {
             get 
             {
                 //Disables batch updating, default value is 1 if not set
                 int iUpdateBatchSize = 1;

                 if (m_anyAdapter is OracleDataAdapter)
                 {
                     iUpdateBatchSize = (m_anyAdapter as OracleDataAdapter).UpdateBatchSize;
                 }

                 if (m_anyAdapter is SqlDataAdapter)
                 {
                     iUpdateBatchSize = (m_anyAdapter as SqlDataAdapter).UpdateBatchSize;
                 }

                 return iUpdateBatchSize;
             }
             set
             {
                 if (m_anyAdapter is OracleDataAdapter)
                 {
                     (m_anyAdapter as OracleDataAdapter).UpdateBatchSize = value;
                 }

                 if (m_anyAdapter is SqlDataAdapter)
                 {
                     (m_anyAdapter as SqlDataAdapter).UpdateBatchSize = value;
                 }
             }
         }
	}
//
	
     /// <summary>Riskmaster.Db.DbRowUpdatingEventHandler is the delegate type used to consume DbRowUpdating events.</summary>
     /// <remarks>none</remarks>
     public delegate void DbRowUpdatingEventHandler(object sender, DbRowUpdatingEventArgs e);
	
     /// <summary>Riskmaster.Db.DbRowUpdatedEventHandler is the delegate type used to consuem DbRowUpdated evetns.</summary>
     /// <remarks>none</remarks>
     public delegate void DbRowUpdatedEventHandler(object sender, DbRowUpdatedEventArgs e);

	
	/// <summary>Riskmaster.Db.DbRowUpdatingEventArgs wraps the native RowUpdated event arguments.</summary>
	/// <remarks>The only pure native object type accessible to consumers of this class is 
	/// the errors collection which can vary by native provider.</remarks>   
	public class DbRowUpdatingEventArgs : RowUpdatingEventArgs
	{
		RowUpdatingEventArgs m_anyRowUpdatingEventArgs; //star of the show
		
		/// <summary>Constructor Riskmaster.Db.DbRowUpdatingEventArgs.DbRowUpdatingEventArgs 
		/// delegates responsibility to our base class using the property values present in our anyRowUpdatingEventArgs
		/// parameter.</summary>
         /// <param name="anyRowUpdatingEventArgs">The event args object that we are "cloning"</param>
         public DbRowUpdatingEventArgs(RowUpdatingEventArgs anyRowUpdatingEventArgs):base(anyRowUpdatingEventArgs.Row, anyRowUpdatingEventArgs.Command,anyRowUpdatingEventArgs.StatementType,anyRowUpdatingEventArgs.TableMapping)
		{m_anyRowUpdatingEventArgs = anyRowUpdatingEventArgs;}

			
		/// <summary>Riskmaster.Db.DbRowUpdatingEventArgs.Errors wraps  the property of the same name from the native data provider.</summary>
		/// <remarks>See the ADO.Net documentation for details.</remarks>
         new  public System.Exception Errors{get{return m_anyRowUpdatingEventArgs.Errors;}}
		
		/// <summary>Riskmaster.Db.DbRowUpdatingEventArgs.Row 
		/// wraps  the property of the same name from the native data provider.</summary>
		/// <remarks>See the ADO.Net documentation for details.</remarks>
         new  public System.Data.DataRow Row{get{return m_anyRowUpdatingEventArgs.Row;}}
		
		/// <summary>Riskmaster.Db.DbRowUpdatingEventArgs.StatementType 
		/// wraps  the property of the same name from the native data provider.</summary>
		/// <remarks>See the ADO.Net documentation for details.</remarks>
		new  public System.Data.StatementType StatementType	{get{return m_anyRowUpdatingEventArgs.StatementType;}}
		
         /// <summary>Riskmaster.Db.DbRowUpdatingEventArgs.Status
		/// wraps  the property of the same name from the native data provider.</summary>
		/// <remarks>See the ADO.Net documentation for details.</remarks>
		new  public System.Data.UpdateStatus Status{get{return m_anyRowUpdatingEventArgs.Status;}}
		
         /// <summary>Riskmaster.Db.DbRowUpdatingEventArgs.TableMapping
		/// wraps  the property of the same name from the native data provider.</summary>
		/// <remarks>See the ADO.Net documentation for details.</remarks>
		new  public System.Data.Common.DataTableMapping TableMapping{get{return m_anyRowUpdatingEventArgs.TableMapping;}}
		
         /// <summary>Riskmaster.Db.DbRowUpdatingEventArgs.Command
		/// wraps  the property of the same name from the native data provider.</summary>
		/// <remarks>See the ADO.Net documentation for details.</remarks>
		new  public DbCommand Command{	get { return new DbCommand(m_anyRowUpdatingEventArgs.Command); }}
	}

	
     /// <summary>Riskmaster.Db.DbRowUpdatedEventArgs wraps the native RowUpdated event arguments.</summary>
     /// <remarks>The only pure native object type accessible to consumers of this class is 
     /// the errors collection which can vary by native provider.</remarks>
     public class DbRowUpdatedEventArgs : RowUpdatedEventArgs
	{
		RowUpdatedEventArgs m_anyRowUpdatedEventArgs; //star of the show
		
		
		 /// <summary>Constructor Riskmaster.Db.DbRowUpdatedEventArgs.DbRowUpdatedEventArgs 
		 /// delegates responsibility to our base class using the property values present in our anyRowUpdatedEventArgs
		 /// parameter.</summary>
		 internal DbRowUpdatedEventArgs(RowUpdatedEventArgs anyRowUpdatedEventArgs): base(anyRowUpdatedEventArgs.Row, anyRowUpdatedEventArgs.Command, anyRowUpdatedEventArgs.StatementType,anyRowUpdatedEventArgs.TableMapping)
			{m_anyRowUpdatedEventArgs = anyRowUpdatedEventArgs;}

		
         /// <summary>Riskmaster.Db.DbRowUpdatedEventArgs.Errors  wraps  
         /// the property of the same name from the native data provider.</summary>
         ///<remarks>The only pure native object type accessible to consumers of this class is 
		 /// the errors collection which can vary by native provider.</remarks>
		 new  public System.Exception Errors{get{return m_anyRowUpdatedEventArgs.Errors;}}
		
         /// <summary>Riskmaster.Db.DbRowUpdatedEventArgs.RecordsAffected
		 /// wraps  the property of the same name from the native data provider.</summary>
		 /// <remarks>See the ADO.Net documentation for details.</remarks>
		 new  public int RecordsAffected{get{return m_anyRowUpdatedEventArgs.RecordsAffected;}}
		
         /// <summary>Riskmaster.Db.DbRowUpdatedEventArgs.Row
		 /// wraps  the property of the same name from the native data provider.</summary>
		 /// <remarks>See the ADO.Net documentation for details.</remarks>
		 new  public System.Data.DataRow Row{get{return m_anyRowUpdatedEventArgs.Row;}}
		
         /// <summary>Riskmaster.Db.DbRowUpdatedEventArgs.StatementType
		 /// wraps  the property of the same name from the native data provider.</summary>
		 /// <remarks>See the ADO.Net documentation for details.</remarks>
		 new  public System.Data.StatementType StatementType	{get{return m_anyRowUpdatedEventArgs.StatementType;}}
		
         /// <summary>Riskmaster.Db.DbRowUpdatedEventArgs.Status
		 /// wraps  the property of the same name from the native data provider.</summary>
		 /// <remarks>See the ADO.Net documentation for details.</remarks>
		 new  public System.Data.UpdateStatus Status{get{return m_anyRowUpdatedEventArgs.Status;}}
		
         /// <summary>Riskmaster.Db.DbRowUpdatedEventArgs.TableMapping
		 /// wraps  the property of the same name from the native data provider.</summary>
		 /// <remarks>See the ADO.Net documentation for details.</remarks>
		 new  public System.Data.Common.DataTableMapping TableMapping{get{return m_anyRowUpdatedEventArgs.TableMapping;}}
		
         /// <summary>Riskmaster.Db.DbRowUpdatedEventArgs.Command
         /// wraps  the property of the same name from the native data provider.</summary>
		 /// <remarks>See the ADO.Net documentation for details.</remarks>
		 new  public DbCommand Command{	get { return new DbCommand(m_anyRowUpdatedEventArgs.Command); }}
		}
	}

