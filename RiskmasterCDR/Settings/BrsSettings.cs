using System;
using System.Data;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Settings
{
	/// <summary>
	/// BRS_SYS_PARAMS settings.
	/// </summary>
	/// <remarks>
	/// Riskmaster.Settings.SysParms.BrsSettings.BrsSettings class is used to retrieve BRS system 
	/// setting parameters
	/// </remarks>
	public class BrsSettings
	{
		/// <summary>The connection string used by the database layer for connection with the database.</summary>
		private string m_sConnectString;
		/// <summary>ID of the row of data in BRS_SYS_PARAMS table.</summary>
		private int m_iRowId;
		/// <summary>KEEP_ON_NEXT value in BRS_SYS_PARAMS table.</summary>
		private string m_sKeepOnNext = string.Empty;
		/// <summary>GEN_EOB_CODE_FLAG value in BRS_SYS_PARAMS table.</summary>
		private int m_iGenEOBCodeFlag = 0;
		/// <summary>DISC_ON_SCHD_FLAG value in BRS_SYS_PARAMS table.</summary>
        private int m_iDiscOnSchdFlag = 0;
		/// <summary>DISC_ON_BILL_FLAG value in BRS_SYS_PARAMS table.</summary>
        private int m_iDiscOnBillFlag = 0;
		/// <summary>DISC_2ND_SCHD_FLAG value in BRS_SYS_PARAMS table.</summary>
        private int m_iDisc2ndSchdFlag = 0;
        //Mona: preventing duplication of BRS splits:Start
        /// <summary>DUP_SPLITS_FLAG value in BRS_SYS_PARAMS table.</summary>
        private int m_iDupSplitsFlag = 0;        
        /// <summary>DUP_SPLITS_CRITERIA value in BRS_SYS_PARAMS table.</summary>
        private int m_iDupSplitsCriteria = 0;
        //Mona: preventing duplication of BRS splits:End
		/// <summary>EOB_MI_PDF_FLAG value in BRS_SYS_PARAMS table.</summary>
        private int m_iEOBMiPdf = 0;
		/// <summary>Stores whether the data has been modified or not.</summary>
		private bool m_bDataChanged;
		private DataTable m_DataTable = null;  //fetch schema to check if field exists in table before reading from/writing to
        private int m_iClientId = 0;
		/// <summary>
		///Riskmaster.Settings.SysParms.BrsSettings.UserLogin overload sets the connection string and loads the data from database.
		/// </summary>
		/// <param name="sConnectString">Connection string</param>
        public BrsSettings(string sConnectString, int p_iClientId)
		{
			m_sConnectString=sConnectString;
            m_iClientId = p_iClientId;
			LoadSettings();
		}
		
		/// <summary>
		///Riskmaster.Settings.SysParms.BrsSettings.LoadSettings retrieves the BRS system parameters.
		/// </summary>		
		/// <returns>True if loading is successful.</returns>
		internal bool LoadSettings()
		{
			try
			{
                using (DbReader rdr = DbFactory.GetDbReader(m_sConnectString, "SELECT * FROM BRS_SYS_PARAMS"))
                {
                    m_DataTable = rdr.GetSchemaTable();
                    DataRow[] rows = null;

                    if (rdr != null)
                    {
                        while (rdr.Read())
                        {
                            this.RowId = rdr.GetInt32("ROW_ID");
                            this.KeepOnNext = rdr.GetString("KEEP_ON_NEXT");
                            this.GenEOBCodeFlag = rdr.GetInt32("GEN_EOB_CODE_FLAG");
                            this.DiscOnSchdFlag = rdr.GetInt32("DISC_ON_SCHD_FLAG");
                            this.DiscOnBillFlag = rdr.GetInt32("DISC_ON_BILL_FLAG");
                            this.Disc2ndSchdFlag = rdr.GetInt32("DISC_2ND_SCHD_FLAG");
                            //Mona: preventing duplication of BRS splits
                            this.DupSplitsFlag = rdr.GetInt32("DUP_SPLITS_FLAG");
                            this.DupSplitsCriteria = rdr.GetInt32("DUP_SPLITS_CRITERIA");                        
                            //Mona: preventing duplication of BRS splits
                            //for newer fields, check if they exist before reading from/writing to them
                            rows = m_DataTable.Select("ColumnName='EOB_MI_PDF_FLAG'");
                            if (rows.GetLength(0) == 1)
                                this.EOBMiPdf = rdr.GetInt32("EOB_MI_PDF_FLAG");
                        }
                        m_bDataChanged = false;
                    }
                }
			}
			catch(Exception e)
			{
                throw new DataModelException(Globalization.GetString("BrsSettings.LoadSettings.DataError", m_iClientId), e);
			}
			return true;
		}
		
		/// <summary>
		///Riskmaster.Settings.SysParms.BrsSettings.SaveSettings saves the BRS system parameters.
		/// </summary>	
		/// <returns>True if saving is successful.</returns>
		public bool SaveSettings()
		{
			try
			{
				if(!m_bDataChanged)
					return true;
				DataRow[] rows=null;

				DbWriter writer = DbFactory.GetDbWriter(this.m_sConnectString);
				writer.Tables.Add("BRS_SYS_PARAMS");
				writer.Where.Add("ROW_ID="+this.m_iRowId);
				writer.Fields.Add("KEEP_ON_NEXT", this.KeepOnNext);
				writer.Fields.Add("GEN_EOB_CODE_FLAG", this.GenEOBCodeFlag);
				writer.Fields.Add("DISC_ON_SCHD_FLAG", this.DiscOnSchdFlag);
				writer.Fields.Add("DISC_ON_BILL_FLAG", this.DiscOnBillFlag);
				writer.Fields.Add("DISC_2ND_SCHD_FLAG", this.Disc2ndSchdFlag);
                //Mona: preventing duplication of BRS splits
                writer.Fields.Add("DUP_SPLITS_FLAG", this.DupSplitsFlag);
                writer.Fields.Add("DUP_SPLITS_CRITERIA", this.DupSplitsCriteria);
                //Mona: preventing duplication of BRS splits
				//for newer fields, check if they exist before reading from/writing to them
				rows = m_DataTable.Select("ColumnName='EOB_MI_PDF_FLAG'");
				if(rows.GetLength(0)==1)
					writer.Fields.Add("EOB_MI_PDF_FLAG", Convert.ToInt16(this.EOBMiPdf));

				writer.Execute();
                
				m_bDataChanged=false;
			}
			catch(Exception e)
			{
                throw new DataModelException(Globalization.GetString("BrsSettings.SaveSettings.DataError", m_iClientId), e);
			}
			return true;
		}

		/// <summary>
		///Property to access m_iRowId.
		/// </summary>
		public int RowId
		{
			get{return m_iRowId;}
			set
			{
				if(m_iRowId!=value)
				{
					m_bDataChanged=true; 
					m_iRowId=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_sKeepOnNext.
		/// </summary>
		public string KeepOnNext
		{
			get{return m_sKeepOnNext;}
			set
			{
				if(m_sKeepOnNext!=value)
				{
					m_bDataChanged=true; 
					m_sKeepOnNext=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_iGenEOBCodeFlag.
		/// </summary>
		public int GenEOBCodeFlag
		{
			get{return m_iGenEOBCodeFlag;}
			set
			{
				if(m_iGenEOBCodeFlag!=value)
				{
					m_bDataChanged=true; 
					m_iGenEOBCodeFlag=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_iDiscOnSchdFlag.
		/// </summary>
		public int DiscOnSchdFlag
		{
			get{return m_iDiscOnSchdFlag;}
			set
			{
				if(m_iDiscOnSchdFlag!=value)
				{
					m_bDataChanged=true; 
					m_iDiscOnSchdFlag=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_iDiscOnBillFlag.
		/// </summary>
		public int DiscOnBillFlag
		{
			get{return m_iDiscOnBillFlag;}
			set
			{
				if(m_iDiscOnBillFlag!=value)
				{
					m_bDataChanged=true; 
					m_iDiscOnBillFlag=value;
				}
			}
		}


        /// <summary>
        /// Mona: preventing duplication of BRS splits
        ///Property to access m_iDupSplitsFlag.
        /// </summary>
        public int DupSplitsFlag
        {
            get { return m_iDupSplitsFlag; }
            set
            {
                if (m_iDupSplitsFlag != value)
                {
                    m_bDataChanged = true;
                    m_iDupSplitsFlag = value;
                }
            }
        }

        /// <summary>
        /// Mona: preventing duplication of BRS splits
        ///Property to access m_iDupSplitsCriteria.
        /// </summary>
        public int DupSplitsCriteria
        {
            get { return m_iDupSplitsCriteria; }
            set
            {
                if (m_iDupSplitsCriteria != value)
                {
                    m_bDataChanged = true;
                    m_iDupSplitsCriteria = value;
                }
            }
        }



		/// <summary>
		///Property to access m_iDisc2ndSchdFlag.
		/// </summary>
		public int Disc2ndSchdFlag
		{
			get{return m_iDisc2ndSchdFlag;}
			set
			{
				if(m_iDisc2ndSchdFlag!=value)
				{
					m_bDataChanged=true; 
					m_iDisc2ndSchdFlag=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_iEOBMiPdf.
		/// </summary>
		public int EOBMiPdf
		{
			get{return m_iEOBMiPdf;}
			set
			{
				if(m_iEOBMiPdf!=value)
				{
					m_bDataChanged=true;
					m_iEOBMiPdf=value;
				}
			}
		}		
		
	}
}
