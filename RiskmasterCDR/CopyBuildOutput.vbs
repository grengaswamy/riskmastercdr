'==========================================================================
'
' VBScript Source File -- Created with SAPIEN Technologies PrimalScript 2007
'
' NAME: CopyBuildOutput
'
' AUTHOR: CSC Financial Services Group , CSC
' DATE  : 01/07/2010
'
' COMMENT: Copies the required files to the build output paths
'
'==========================================================================
Const DLL_FILES = "*.dll"
Const DEBUG_FILES = "*.pdb"
Const XML_DOC_FILES = "*.xml"
Const WCF_WEB_SERVICE_OUTPUT = "WCFService\bin\"
Dim strWebServiceOutputPath, strSourcePath
Dim strSourceDLLPath, strSourcePDBPath, strSourceXMLDocPath, strDestFilePath
strSourcePath = WScript.Arguments(0)
strWebServiceOutputPath = WScript.Arguments(1)

'Build the source output paths
strSourceDLLPath = BuildOutputPaths(strSourcePath, DLL_FILES)
strSourcePDBPath = BuildOutputPaths(strSourcePath, DEBUG_FILES)
strSourceXMLDocPath = BuildOutputPaths(strSourcePath, XML_DOC_FILES)

'Update content to push binaries/DLLs to the WCF bin directory needed 
'for project references in the Riskmaster-business solution.
strDestFilePath = BuildOutputPaths(strWebServiceOutputPath, WCF_WEB_SERVICE_OUTPUT)

'Copy the files to the new WCF output directory
CopyFiles strSourceDLLPath, strDestFilePath & DLL_FILES
CopyFiles strSourcePDBPath, strDestFilePath & DEBUG_FILES
CopyFiles strSourceXMLDocPath, strDestFilePath & XML_DOC_FILES

'Function to copy files from one location to another using the xcopy command
Sub CopyFiles(strSourceFilePath, strDestFilePath)
	Dim objWSH
	Dim strCmd
	Const PAD_QUOTES = """"
	
	Set objWSH = CreateObject("WScript.Shell")
	
	'Execute the specified command
	strCmd = "xcopy " & PAD_QUOTES & strSourceFilePath & PAD_QUOTES & " " & PAD_QUOTES & strDestFilePath & PAD_QUOTES & " /Q /R /Y"
	'WScript.Echo strCmd
	
	'Execute the specified command
	objWSH.Run strCmd, 7, True
	
	'Clean up
	Set objWSH = Nothing
End Sub

Function BuildOutputPaths(strSourcePath, strAppendPath)
	Dim objFSO
	Dim strFullPath
	
	'Set objFSO = CreateObject("Scripting.FileSystemObject")
	
	strFullPath = strSourcePath & strAppendPath
	
	BuildOutputPaths = strFullPath
End Function
