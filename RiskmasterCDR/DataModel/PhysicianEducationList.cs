
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forPhysicianEducationList.
	/// </summary>
	public class PhysicianEducationList : DataCollection
	{
		internal PhysicianEducationList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"EDUC_ID";
			this.SQLFromTable =	"PHYS_EDUCATION";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "PhysicianEducation";
		}
		public new PhysicianEducation this[int keyValue]{get{return base[keyValue] as PhysicianEducation;}}
		public new PhysicianEducation AddNew(){return base.AddNew() as PhysicianEducation;}
		public  PhysicianEducation Add(PhysicianEducation obj){return base.Add(obj) as PhysicianEducation;}
		public new PhysicianEducation Add(int keyValue){return base.Add(keyValue) as PhysicianEducation;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}