using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forVehicleXInspctList.
	/// </summary>
	public class VehicleXInspctList : DataCollection
	{
		internal VehicleXInspctList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"UNIT_INSP_ROW_ID";
			this.SQLFromTable =	"VEHICLE_X_INSPCT";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "VehicleXInspct";
		}
		public new VehicleXInspct this[int keyValue]{get{return base[keyValue] as VehicleXInspct;}}
		public new VehicleXInspct AddNew(){return base.AddNew() as VehicleXInspct;}
		public  VehicleXInspct Add(VehicleXInspct obj){return base.Add(obj) as VehicleXInspct;}
		public new VehicleXInspct Add(int keyValue){return base.Add(keyValue) as VehicleXInspct;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}