﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.Common;

namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("SALVAGE", "SALVAGE_ROW_ID")]
    public class Salvage : DataObject
    {
        public static string ParentKeyFieldName = "PARENT_ID";
        public static string ParentNameFieldName = "PARENT_NAME";
        #region Database Field List
        private string[,] sFields = {
                                        		
														{"SalvageRowId", "SALVAGE_ROW_ID"},
														{"ParentId", "PARENT_ID"},
														{"ParentName", "PARENT_NAME"},
														{"IsInsured", "IS_INSURED"},
														{"IsOwnerRetained", "IS_OWNER_RETAINED"},
														{"SalvageType", "SALVAGE_TYPE"},
														{"InPosEid", "POSSESSION_OF"},
														{"ControlNumber", "CONTROLNUMBER"},
														{"SalvageYardAddr","SALVAGE_YARD_ADDR_ID"},
														{"SalvageStatus","SALVAGE_STATUS"},
														{"MoveDate","MOVE_DATE"},
														{"CloseDate","CLOSE_DATE"},
														{"StockNumber","STOCKNUMBER"},
                                                        {"CutoffDate", "CUTOFF_DATE"},
														{"DailyFees", "DAILYFEES"},
														{"CutoffReason", "CUTOFFREASON"},
														{"AppraisedDate", "APPRAISED_DATE"},
														{"BuyerEid", "BUYER_EID"},
														{"ActCashValue", "ACT_CASH_VALUE"},
                                                        {"EstSalvageValue", "EST_SALVAGE_VALUE"},
														{"TowCharge", "TOW_CHARGE"},
														{"StorageValue","STORAGE_VALUE"},
														{"OtherCharges","OTHER_CHARGES"},
														{"TotalCharges","TOTAL_CHARGES"},
														{"SoldDate","SOLD_DATE"},
														{"NetRecovery","NETRECOVERY"},
                                                        {"SalePrice", "SALE_PRICE"},  
														{"Recovery", "RECOVERY_RATE"},
                                                        //Added by agupta298 PMC GAP08 - Start  - RMA-4694
                                                        {"Mileage", "MILEAGE"},
                                                        {"TitleNumber", "TITLE_NUMBER"},
                                                        {"StateBrand", "STATE_BRAND_CODE"},
                                                        {"IsTheftRecovered", "THEFT_RECOVERED"},
                                                        {"SalvageDate", "SALVAGE_DSGND_DATE"},
                                                        {"SalvageReason", "SALVAGE_REASON"},
                                                        {"SalvageSubrogation", "SALVAGE_SUBROGATION"},
                                                        //Added by agupta298 PMC GAP08 - End  - RMA-4694
                                                      

		};


        public int SalvageRowId { get { return GetFieldInt("SALVAGE_ROW_ID"); } set { SetField("SALVAGE_ROW_ID", value); } }
        public bool IsInsured { get { return GetFieldBool("IS_INSURED"); } set { SetField("IS_INSURED", value); } }
        public int ParentId { get { return GetFieldInt("PARENT_ID"); } set { SetField("PARENT_ID", value); } }
        public string ParentName { get { return GetFieldString("PARENT_NAME"); } set { SetField("PARENT_NAME", value); } }
        public bool IsOwnerRetained { get { return GetFieldBool("IS_OWNER_RETAINED"); } set { SetField("IS_OWNER_RETAINED", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "SALVAGE TYPE")]
        public int SalvageType { get { return GetFieldInt("SALVAGE_TYPE"); } set { SetField("SALVAGE_TYPE", value); } }
    [ExtendedTypeAttribute(RMExtType.Entity, "ANY")]
        public int InPosEid { get { return GetFieldInt("POSSESSION_OF"); } set { SetFieldAndNavTo("POSSESSION_OF", value, "InPosEntity"); } }
        
        public string ControlNumber { get { return GetFieldString("CONTROLNUMBER"); } set { SetField("CONTROLNUMBER", value); } }
        [ExtendedTypeAttribute(RMExtType.EntityAddr)]
        public int SalvageYardAddr { get { return GetFieldInt("SALVAGE_YARD_ADDR_ID"); } set { SetField("SALVAGE_YARD_ADDR_ID", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "SALVAGE STATUS")]
        public int SalvageStatus { get { return GetFieldInt("SALVAGE_STATUS"); } set { SetField("SALVAGE_STATUS", value); } }

        public string MoveDate { get { return GetFieldString("MOVE_DATE"); } set { SetField("MOVE_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        public string CloseDate { get { return GetFieldString("CLOSE_DATE"); } set { SetField("CLOSE_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        public string  StockNumber{ get { return GetFieldString("STOCKNUMBER"); } set { SetField("STOCKNUMBER", value); } }
        public string CutoffDate { get { return GetFieldString("CUTOFF_DATE"); } set { SetField("CUTOFF_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        
        [ExtendedTypeAttribute(RMExtType.Entity, "ANY")]
        public int BuyerEid { get { return GetFieldInt("BUYER_EID"); } set { SetFieldAndNavTo("BUYER_EID", value, "BuyerEntity"); } }



        public double DailyFees { get { return GetFieldDouble("DAILYFEES"); } set { SetField("DAILYFEES", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "CUTOFF REASON")]
        public int CutoffReason { get { return GetFieldInt("CUTOFFREASON"); } set { SetField("CUTOFFREASON", value); } }
        public string AppraisedDate { get { return GetFieldString("APPRAISED_DATE"); } set { SetField("APPRAISED_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
      
        public double ActCashValue { get { return GetFieldDouble("ACT_CASH_VALUE"); } set { SetField("ACT_CASH_VALUE", value); } }

        public double EstSalvageValue { get { return GetFieldDouble("EST_SALVAGE_VALUE"); } set { SetField("EST_SALVAGE_VALUE", value); } }

        public double TowCharge { get { return GetFieldDouble("TOW_CHARGE"); } set { SetField("TOW_CHARGE", value); } }
        public double StorageValue { get { return GetFieldDouble("STORAGE_VALUE"); } set { SetField("STORAGE_VALUE", value); } }

        public double OtherCharges { get { return GetFieldDouble("OTHER_CHARGES"); } set { SetField("OTHER_CHARGES", value); } }
        public double TotalCharges { get { return GetFieldDouble("TOTAL_CHARGES"); } set { SetField("TOTAL_CHARGES", value); } }
        public string SoldDate { get { return GetFieldString("SOLD_DATE"); } set { SetField("SOLD_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        public double NetRecovery { get { return GetFieldDouble("NETRECOVERY"); } set { SetField("NETRECOVERY", value); } }

        public double SalePrice { get { return GetFieldInt("SALE_PRICE"); } set { SetField("SALE_PRICE", value); } }
        public int Recovery { get { return GetFieldInt("RECOVERY_RATE"); } set { SetField("RECOVERY_RATE", value); } }

        //Added by agupta298 PMC GAP08 - Start - RMA-4694
        public int Mileage { get { return GetFieldInt("MILEAGE"); } set { SetField("MILEAGE", value); } }
        public string TitleNumber { get { return GetFieldString("TITLE_NUMBER"); } set { SetField("TITLE_NUMBER", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "STATE_BRANDS")]
        public int StateBrand { get { return GetFieldInt("STATE_BRAND_CODE"); } set { SetField("STATE_BRAND_CODE", value); } }
        public bool IsTheftRecovered { get { return GetFieldBool("THEFT_RECOVERED"); } set { SetField("THEFT_RECOVERED", value); } }
        public string SalvageDate { get { return GetFieldString("SALVAGE_DSGND_DATE"); } set { SetField("SALVAGE_DSGND_DATE", Conversion.GetDate(value)); } }
        public string SalvageReason { get { return GetFieldString("SALVAGE_REASON"); } set { SetField("SALVAGE_REASON", value); } }
        public string SalvageSubrogation { get { return GetFieldString("SALVAGE_SUBROGATION"); } set { SetField("SALVAGE_SUBROGATION", value); } }
        //Added by agupta298 PMC GAP08 - End - RMA-4694

        #endregion

        #region Child Implementation
        private string[,] sChildren = { { "BuyerEntity", "Entity" },
                                        {"SalvageHistoryList","SalvageHistoryList"},
                                        {"InPosEntity","Entity"}};

      
        public SalvageHistoryList SalvageHistoryList
        {
            get
            {
                SalvageHistoryList objList = base.m_Children["SalvageHistoryList"] as SalvageHistoryList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.SQLFilter = this.KeyFieldName + "=" + this.KeyFieldValue;
                return objList;
            }
        }

        public Entity BuyerEntity
        {
            get
            {
                Entity objItem = base.m_Children["BuyerEntity"] as Entity;
                if ((objItem as IDataModel).IsStale)
                    objItem = this.CreatableMoveChildTo("BuyerEntity", (objItem as DataObject), this.BuyerEid) as Entity;
                return objItem;
            }
        }
        public Entity InPosEntity
        {
            get
            {
                Entity objItem = base.m_Children["InPosEntity"] as Entity;
                if ((objItem as IDataModel).IsStale)
                    objItem = this.CreatableMoveChildTo("InPosEntity", (objItem as DataObject), this.InPosEid) as Entity;
                return objItem;
            }
        }
        override internal void OnChildInit(string childName, string childType)
        {
            
            base.OnChildInit(childName, childType);
        }

        internal override void OnChildItemAdded(string childName, IDataModel itemValue)
        {
            switch (childName)
            {

                case "SalvageHistoryList":
                    (itemValue as SalvageHistory).salvagerowid = this.SalvageRowId;
                    break;

            }
            base.OnChildItemAdded(childName, itemValue);
        }
          internal override void LoadData(Riskmaster.Db.DbReader objReader)
        {
            base.LoadData(objReader);
            this.SetFieldAndNavTo("BUYER_EID", this.BuyerEid, "BuyerEntity", true);
            this.SetFieldAndNavTo("POSSESSION_OF", this.InPosEid, "InPosEntity", true);
            }
           
        internal override void OnChildPreSave(string childName, IDataModel childValue)
        {
            switch (childName)
            {
                case "BuyerEntity":
                    Entity obj = (childValue as Entity);
                    //dbisht6 jira RMA-9737
                    if (this.BuyerEid != (childValue as Entity).EntityId)
                        this.m_Fields["BUYER_EID"] = (childValue as Entity).EntityId;
                    //dbisht6 end
                  
                    if (obj.DataChanged)
                    {
                        if (obj.EntityTableId == 0)
                            obj.EntityTableId = Context.LocalCache.GetTableId("OTHER_PEOPLE");
                        (obj as IPersistence).Save();
                        if (this.BuyerEid != (childValue as Entity).EntityId)
                            this.m_Fields["BUYER_EID"] = (childValue as Entity).EntityId;
                    }
                    break;
               
            }   
        }

      
        internal override void OnChildDelete(string childName, IDataModel childValue)
        {
            base.OnChildDelete(childName, childValue);
        }

        override internal void OnChildPostSave(string childName, IDataModel childValue, bool isNew)
        {
          
            switch (childName)
            {
                case "SalvageHistoryList":
                    if (isNew)
                    {
                        (childValue as SalvageHistoryList).SQLFilter = string.Format("{0}={1}", "SALVAGE_ROW_ID", this.SalvageRowId);
                        foreach (SalvageHistory item in (childValue as SalvageHistoryList))
                            item.salvagerowid = this.SalvageRowId;
                        UpdateSalvageHistory();

                    }
                    (childValue as IPersistence).Save();
                    break;
              
            }
            base.OnChildPostSave(childName, childValue, isNew);
        }
   
        

        #endregion

        private void UpdateSalvageHistory()
        {

            SalvageHistory objHist = null;
            objHist = this.SalvageHistoryList.LastStatusChange();

            if (objHist == null   ||objHist.SalvageAddress != this.SalvageYardAddr || objHist.MoveDate != this.MoveDate || objHist.DailyFees != this.DailyFees || objHist.InPosEid != this.InPosEid)
            {
                objHist = this.SalvageHistoryList.AddNew();
                objHist.DailyFees = this.DailyFees;
                objHist.InPosEid = this.InPosEid;
                objHist.SalvageAddress = this.SalvageYardAddr;
                objHist.MoveDate = this.MoveDate;

            }
        }
        public override void Save()
        {
            if ((this as IDataModel).DataObjectState == DataObjectState.IsSaving)
            {	
                base.Save();
                return;
            }
          
            UpdateSalvageHistory();
           
            base.Save();

        

        }
		
        internal Salvage(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();
        }
        new private void Initialize()
        {
            this.m_sTableName = "SALVAGE";
            this.m_sKeyField = "SALVAGE_ROW_ID";
            this.m_sFilterClause = "";

            base.InitFields(sFields);
         
            base.InitChildren(sChildren);

            this.m_sParentClassName = this.ParentName;

            base.Initialize();  

        }

        #region Supplemental Fields Exposed
        //Supplementals are implemented in the base class but not exposed externally.
        //This allows the derived class to control whether the object appears to have
        //supplementals or not. 
        //Entity exposes Supplementals.
        internal override string OnBuildSuppTableName()
        {
            return "SALVAGE_SUPP";
        }

        public new Supplementals Supplementals
        {
            get
            {
                return base.Supplementals;
            }
        }

        #endregion
    }
}

