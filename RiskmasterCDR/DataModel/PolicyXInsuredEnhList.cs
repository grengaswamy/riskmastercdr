﻿using System;
using System.Diagnostics;
namespace Riskmaster.DataModel
{
    /// <summary>
    /// Add by kuladeep for Audit Issue MITS:24736
    /// child list of PolicyXInsuredEnh
    /// </summary>
    public class PolicyXInsuredEnhList : DataCollection
    {
        internal PolicyXInsuredEnhList(bool isLocked, Context context)
            : base(isLocked, context)
        {
            (this as IDataModel).Initialize(isLocked, context);
            this.SQLKeyColumn = "INSRD_ROW_ID";
            this.SQLFromTable = "POLICY_X_INSRD_ENH";
            this.TypeName = "PolicyXInsuredEnh";
        }
       // public new PolicyXInsuredEnh this[int keyValue] { get { return base[keyValue] as PolicyXInsuredEnh; } }
        public override DataObject this[int keyValue]
        {
            get
            {
                DataObject objPolicyXInsuredEnh = null;
                StackTrace stackTrace = new StackTrace();
                string sStackTrace = Convert.ToString(stackTrace);
                if (!base.m_keySet.ContainsKey(keyValue) && sStackTrace.Contains("Riskmaster.ActiveScript.ScriptHost.ExecuteMethod") && sStackTrace.Contains("Riskmaster.Application.FormProcessor.FormProcessor.ProcessForm"))
                {
                    int[] arrKeys = new int[base.m_keySet.Count];
                    base.m_keySet.Keys.CopyTo(arrKeys, 0);
                    objPolicyXInsuredEnh = base[arrKeys[keyValue - 1]] as DataObject;
                }
                else
                {
                    objPolicyXInsuredEnh = base[keyValue] as DataObject;
                }
                return objPolicyXInsuredEnh as PolicyXInsuredEnh;
            }
        }
        public new PolicyXInsuredEnh AddNew() { return base.AddNew() as PolicyXInsuredEnh; }
        public PolicyXInsuredEnh Add(PolicyXInsuredEnh obj) { return base.Add(obj) as PolicyXInsuredEnh; }
        public new PolicyXInsuredEnh Add(int keyValue) { return base.Add(keyValue) as PolicyXInsuredEnh; }

        public override string ToString()
        {
            string s = "";
            foreach (DataObject obj in this)
                s += obj.Dump();
            return s;
        }
    }
}
