using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forDefendantList.
	/// </summary>
	public class DefendantList : DataCollection
	{
		internal DefendantList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"DEFENDANT_ROW_ID";
			this.SQLFromTable =	"DEFENDANT";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "Defendant";
		}
		public new Defendant this[int keyValue]{get{return base[keyValue] as Defendant;}}
		public new Defendant AddNew(){return base.AddNew() as Defendant;}
		public  Defendant Add(Defendant obj){return base.Add(obj) as Defendant;}
		public new Defendant Add(int keyValue){return base.Add(keyValue) as Defendant;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}