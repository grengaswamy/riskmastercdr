﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
   public  class SalvageList : DataCollection
    {
       internal SalvageList(bool isLocked, Context context)
            : base(isLocked, context)
        {
            (this as IDataModel).Initialize(isLocked, context);
            this.SQLKeyColumn = "SALVAGE_ROW_ID";
            this.SQLFromTable = "SALVAGE";
            this.TypeName = "Salvage";
        }


       public new Salvage this[int keyValue] { get { return base[keyValue] as Salvage; } }
       public new Salvage AddNew() { return base.AddNew() as Salvage; }
       public Salvage Add(Salvage obj) { return base.Add(obj) as Salvage; }
       public new Salvage Add(int keyValue) { return base.Add(keyValue) as Salvage; }

       public override string ToString()
       {
           string s = "";
           foreach (DataObject obj in this)
               s += obj.Dump();
           return s;
       }
    }
}
