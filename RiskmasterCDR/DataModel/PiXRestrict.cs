
using System;

namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary of PiXRestrict AutoGenerated Class.
	/// Don't forget to add children and custom override behaviors if necessary.
	/// </summary>
	[Riskmaster.DataModel.Summary("PI_X_RESTRICT","PI_RESTRICT_ROW_ID")]
	public class PiXRestrict  : DataObject	
	{
		#region Database Field List
		private string[,] sFields = {
														{"PiRestrictRowId", "PI_RESTRICT_ROW_ID"},
														{"PiRowId", "PI_ROW_ID"},
														{"DateFirstRestrct", "DATE_FIRST_RESTRCT"},
														{"PercentDisabled", "PERCENT_DISABLED"},
														{"DateLastRestrct", "DATE_LAST_RESTRCT"},
														{"Duration", "DURATION"},
														{"PositionCode", "POSITION_CODE"},
		};

		public int PiRestrictRowId{get{return GetFieldInt("PI_RESTRICT_ROW_ID");}set{SetField("PI_RESTRICT_ROW_ID",value);}}
		public int PiRowId{get{return GetFieldInt("PI_ROW_ID");}set{SetField("PI_ROW_ID",value);}}
		public string DateFirstRestrct{get{return GetFieldString("DATE_FIRST_RESTRCT");}set{SetField("DATE_FIRST_RESTRCT",Riskmaster.Common.Conversion.GetDate(value));}}
		public int PercentDisabled{get{return GetFieldInt("PERCENT_DISABLED");}set{SetField("PERCENT_DISABLED",value);}}
		public string DateLastRestrct{get{return GetFieldString("DATE_LAST_RESTRCT");}set{SetField("DATE_LAST_RESTRCT",Riskmaster.Common.Conversion.GetDate(value));}}
		public int Duration{get{return GetFieldInt("DURATION");}set{SetField("DURATION",value);}}
        [ExtendedTypeAttribute(RMExtType.Code,"POSITIONS")]
        public int PositionCode{get{return GetFieldInt("POSITION_CODE");}set{SetField("POSITION_CODE",value);}}
		#endregion
		
		public override string Default
		{
			get{return String.Format("{0} - {1}", Common.Conversion.ToDate(this.DateFirstRestrct).ToShortDateString(), Common.Conversion.ToDate(this.DateLastRestrct).ToShortDateString());	}
		}


		internal PiXRestrict(bool isLocked,Context context):base(isLocked, context)
		{
			this.Initialize();
		}
		new private void Initialize()
		{

			// base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

			this.m_sTableName = "PI_X_RESTRICT";
			this.m_sKeyField = "PI_RESTRICT_ROW_ID";
			this.m_sFilterClause = "";

			//Add all obect Fields into the Field Collection from our Field List.
			base.InitFields(sFields);
			//Add all object Children into the Children collection from our "sChildren" list.
			//			base.InitChildren(sChildren);
			this.m_sParentClassName = "PiEmployee";
			base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

		}
		#region Supplemental Fields Exposed
		internal override string OnBuildSuppTableName()
		{
			return "PI_X_RESTRCT_SUPP";
		}

		//Supplementals are implemented in the base class but not exposed externally.
		//This allows the derived class to control whether the object appears to have
		//supplementals or not. 
		//Entity exposes Supplementals.
		public new Supplementals Supplementals
		{	
			get
			{
				return base.Supplementals;
			}
		}
		#endregion
	}
}