using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forBankAccSubList.
	/// </summary>
	public class BankAccSubList : DataCollection
	{
		internal BankAccSubList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"SUB_ROW_ID";
			this.SQLFromTable =	"BANK_ACC_SUB";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "BankAccSub";
		}
		public new BankAccSub this[int keyValue]{get{return base[keyValue] as BankAccSub;}}
		public new BankAccSub AddNew(){return base.AddNew() as BankAccSub;}
		public  BankAccSub Add(BankAccSub obj){return base.Add(obj) as BankAccSub;}
		public new BankAccSub Add(int keyValue){return base.Add(keyValue) as BankAccSub;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}