﻿using System;


namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("SITE_UNIT", "SITE_ID", "SiteNumber")]
    public class SiteUnit : DataObject	
    {

        #region Database Field List
        private string[,] sFields = {
                                        {"SiteId", "SITE_ID"},
                                        {"SiteNumber", "SITE_NUMBER"},
                                        {"Name", "NAME"},
                                        {"Optional", "OPTIONAL"},
                                        {"Adsress1", "ADDR1"},
                                        {"Adsress2", "ADDR2"},
                                        {"Adsress3", "ADDR3"},
                                        {"Adsress4", "ADDR4"},
                                        {"StateId", "STATE_ID"},
                                        {"City", "CITY"},
                                        {"ZipCode", "ZIP_CODE"},
                                        {"CountryId", "COUNTRY_ID"},
                                        {"PhoneNumber", "PHONE_NUMBER"},
                                        {"Contact", "CONTACT"},
                                        {"TaxLocation", "TAX_LOCATION"},
                                        {"UnemployementNumber", "UNEMPLOYEMENT_NUMBER"},
                                        {"NoOfEmp", "NUM_OF_EMP"},
                                        {"FEIN", "FEIN"},
                                        {"SIC", "SIC"},     
                                         {"DeletedFlag", "DELETED_FLAG"},
                                        {"DttmRcdAdded", "DTTM_RCD_ADDED"},
                                        {"DttmRcdLastUpd", "DTTM_RCD_LAST_UPD"},
                                        {"AddedByUser", "ADDED_BY_USER"},
                                        {"UpdatedByUser", "UPDATED_BY_USER"},
                                        {"RiskClause", "RISK_CLAUSE"} //Payal RMA:7893
                                    };

        public int SiteId { get { return GetFieldInt("SITE_ID"); } set { SetField("SITE_ID", value); } }
        public string SiteNumber { get { return GetFieldString("SITE_NUMBER"); } set { SetField("SITE_NUMBER", value); } }
        public string Name { get { return GetFieldString("NAME"); } set { SetField("NAME", value); } }
        public string Optional { get { return GetFieldString("OPTIONAL"); } set { SetField("OPTIONAL", value); } }
        public string Adsress1 { get { return GetFieldString("ADDR1"); } set { SetField("ADDR1", value); } }
        public string Adsress2 { get { return GetFieldString("ADDR2"); } set { SetField("ADDR2", value); } }
        public string Adsress3 { get { return GetFieldString("ADDR3"); } set { SetField("ADDR3", value); } }
        public string Adsress4 { get { return GetFieldString("ADDR4"); } set { SetField("ADDR4", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "STATES")]
        public int StateId { get { return GetFieldInt("STATE_ID"); } set { SetField("STATE_ID", value); } }
        public string City { get { return GetFieldString("CITY"); } set { SetField("CITY", value); } }
        public string ZipCode { get { return GetFieldString("ZIP_CODE"); } set { SetField("ZIP_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "COUNTRY")]
        public int CountryId { get { return GetFieldInt("COUNTRY_ID"); } set { SetField("COUNTRY_ID", value); } }
        public string PhoneNumber { get { return GetFieldString("PHONE_NUMBER"); } set { SetField("PHONE_NUMBER", value); } }
        public string Contact { get { return GetFieldString("CONTACT"); } set { SetField("CONTACT", value); } }
        public string TaxLocation { get { return GetFieldString("TAX_LOCATION"); } set { SetField("TAX_LOCATION", value); } }
        public string UnemployementNumber { get { return GetFieldString("UNEMPLOYEMENT_NUMBER"); } set { SetField("UNEMPLOYEMENT_NUMBER", value); } }
        public int NoOfEmp { get { return GetFieldInt("NUM_OF_EMP"); } set { SetField("NUM_OF_EMP", value); } }
        public string FEIN { get { return GetFieldString("FEIN"); } set { SetField("FEIN", value); } }
        public string SIC { get { return GetFieldString("SIC"); } set { SetField("SIC", value); } }
        public bool DeletedFlag { get { return GetFieldBool("DELETED_FLAG"); } set { SetField("DELETED_FLAG", value); } }
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        public string RiskClause { get { return GetFieldString("RISK_CLAUSE"); } set { SetField("RISK_CLAUSE", value); } } //Payal RMA:7893
                                                    
        #endregion

        #region Child Implementation

        //Handle adding the our key to any additions to the record collection properties.
        internal override void OnChildItemAdded(string childName, IDataModel itemValue)
        {
            base.OnChildItemAdded(childName, itemValue);
        }

        //Handle child saves.  Set the current key into the children since this could be a new record.
        override internal void OnChildPostSave(string childName, IDataModel childValue, bool isNew)
        {
            base.OnChildPostSave(childName, childValue, isNew);
        }
        #endregion

        internal SiteUnit(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();
        }

        new private void Initialize()
        {
            this.m_sTableName = "SITE_UNIT";
            this.m_sKeyField = "SITE_ID";
            this.m_sFilterClause = string.Empty;
            //Add all object Fields into the Field Collection from our Field List.
            base.InitFields(sFields);
            this.m_sParentClassName = string.Empty;
            //Moved after most init logic so that scripting can be called successfully.
            base.Initialize();
        }

        internal override string OnBuildDeleteSQL()
        {
            return "UPDATE SITE_UNIT SET DELETED_FLAG = -1 WHERE SITE_ID=" + this.SiteId;
        }

        internal override bool OnSuppDelete()
        {
            return false;
        }

       
        internal override void OnChildDelete(string childName, IDataModel childValue)
        {
            return;
        }

        internal override string OnApplyFilterClauseSQL(string CurrentSQL)
        {
            string sExtendedFilterClause = this.m_sFilterClause;
            string sSQL = CurrentSQL;
            if (!string.IsNullOrEmpty(sExtendedFilterClause))
            {
                sExtendedFilterClause += " AND ";
            }
            sExtendedFilterClause += "DELETED_FLAG=0";

            if (!string.IsNullOrEmpty(sExtendedFilterClause))
            {
                if (sSQL.IndexOf(" WHERE ") > 0)
                {
                    sSQL += " AND (" + sExtendedFilterClause + ")";
                }
                else
                {
                    sSQL += " WHERE " + sExtendedFilterClause;
                }
            }
            return sSQL;
        }

        protected override void OnBuildNewUniqueId()
        {
            base.OnBuildNewUniqueId();
            if (this.SiteNumber == "")
            {
                this.SiteNumber = GetSiteNumber();
            }
        }

        private string GetSiteNumber()
        {
            int iPin = this.Context.GetNextUID("SITE_NUMBERS");
            return iPin.ToString("0000000");
        }      

        #region Supplemental Fields Exposed

        /// <summary>
        /// Supplementals are implemented in the base class but not exposed externally.
        ///This allows the derived class to control whether the object appears to have
        ///supplementals or not. Entity exposes Supplementals.
        /// </summary>
        public new Supplementals Supplementals
        {
            get
            {
                return base.Supplementals;
            }
        }
        #endregion















    }
}
