﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.Db;//RMA-8753 nshah28(Added by ashish)
namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("ENTITY_X_ADDRESSES", "ROW_ID")]//RMA-8753 nshah28(Added by ashish)
    public class EntityXAddresses:DataObject
    {
        #region Database Field List
        private string[,] sFields ={
                                         {"RowId", "ROW_ID"},      //RMA-8753 nshah28(Added by ashish)
                                         { "AddressId","ADDRESS_ID"},
                                         { "EntityId","ENTITY_ID"},
                                         //RMA-8753 nshah28(Added by ashish) Start
                                         //{ "Addr1", "ADDR1"}, 
                                         //{ "Addr2", "ADDR2"}, 
                                         //{ "Addr3", "ADDR3"}, 
                                         //{ "Addr4", "ADDR4"},
                                         //{ "City", "CITY"}, 
                                         //{ "County",	"COUNTY"},
                                         //{ "Country", "COUNTRY_CODE"}, 
                                         //{ "State", "STATE_ID"}, 
                                         //{ "ZipCode", "ZIP_CODE"},
                                         //RMA-8753 nshah28(Added by ashish) End
                                         { "Email", "EMAIL_ADDRESS"},	
                                         { "Fax", "FAX_NUMBER"}, 
                                         { "PrimaryAddress","PRIMARY_ADD_FLAG"},
                                         {"EffectiveDate", "EFFECTIVE_DATE"}, //mbahl3 mits 30221
                                         {"ExpirationDate", "EXPIRATION_DATE"}, //mbahl3 mits 30221
                                         { "AddedByUser", "ADDED_BY_USER"},//RMA-8753 nshah28(Added by ashish)
                                         { "UpdatedByUser", "UPDATED_BY_USER"},//RMA-8753 nshah28(Added by ashish)
                                         { "DttmRcdAdded", "DTTM_RCD_ADDED"},//RMA-8753 nshah28(Added by ashish)
                                         { "DttmRcdLastUpd", "DTTM_RCD_LAST_UPD"},//RMA-8753 nshah28(Added by ashish)
                                        //  mits 35925 start
                                         //{"AddressSeqNum","ADDRESS_SEQ_NUM"},//RMA-8753 nshah28(Added by ashish)
                                        //mits 35925 end
										{ "AddressType", "ADDRESS_TYPE_CODE"} //MITS:34276 Entity Address Type
                                        //{"LegacyUniqueIdentifier","LEGACY_UNIQUE_IDENTIFIER"}//RMA-8753 nshah28(Added by ashish)
                                 };
       // public string LegacyUniqueIdentifier { get { return GetFieldString("LEGACY_UNIQUE_IDENTIFIER"); } set { SetField("LEGACY_UNIQUE_IDENTIFIER", value); } }//RMA-8753 nshah28(Added by ashish)
        //mbahl3 mits 30221
        public string EffectiveDate { get { return GetFieldString("EFFECTIVE_DATE"); } set { SetField("EFFECTIVE_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        public string ExpirationDate { get { return GetFieldString("EXPIRATION_DATE"); } set { SetField("EXPIRATION_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        //mbahl3 mits 30221
        public int RowId { get { return GetFieldInt("ROW_ID"); } set { SetField("ROW_ID", value); } }//RMA-8753 nshah28(Added by ashish)
        public int AddressId { get { return GetFieldInt("ADDRESS_ID"); } set { SetField("ADDRESS_ID", value); } }
        public int EntityId { get { return GetFieldInt("ENTITY_ID"); } set { SetField("ENTITY_ID", value); } }
        //RMA-8753 nshah28(Added by ashish) START
		//public string Addr1 { get { return GetFieldString("ADDR1"); } set { SetField("ADDR1", value); } }
        //public string Addr2 { get { return GetFieldString("ADDR2"); } set { SetField("ADDR2", value); } }
        //public string Addr3 { get { return GetFieldString("ADDR3"); } set { SetField("ADDR3", value); } }
        //public string Addr4 { get { return GetFieldString("ADDR4"); } set { SetField("ADDR4", value); } }
        //public string City { get { return GetFieldString("CITY"); } set { SetField("CITY", value); } }
        //public string County { get { return GetFieldString("COUNTY"); } set { SetField("COUNTY", value); } }
        //[ExtendedTypeAttribute(RMExtType.Code, "COUNTRY")]
        //public int Country { get { return GetFieldInt("COUNTRY_CODE"); } set { SetField("COUNTRY_CODE", value); } }
        //[ExtendedTypeAttribute(RMExtType.Code, "STATES")]
        //public int State { get { return GetFieldInt("STATE_ID"); } set { SetField("STATE_ID", value); } }
        //public string ZipCode { get { return GetFieldString("ZIP_CODE"); } set { SetField("ZIP_CODE", value); } }
        //RMA-8753 nshah28(Added by ashish) END
        public string Email { get { return GetFieldString("EMAIL_ADDRESS"); } set { SetField("EMAIL_ADDRESS", value); } }
        public string Fax { get { return GetFieldString("FAX_NUMBER"); } set { SetField("FAX_NUMBER", value); } }
        public int PrimaryAddress { get { return GetFieldInt("PRIMARY_ADD_FLAG"); } set { SetField("PRIMARY_ADD_FLAG", value); } }
        //RMA-8753 nshah28(Added by ashish) Start
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        //RMA-8753 nshah28(Added by ashish) End
         // mits35925 start
        //public int AddressSeqNum { get { return GetFieldInt("ADDRESS_SEQ_NUM"); } set { SetField("ADDRESS_SEQ_NUM", value); } }//RMA-8753 nshah28(Added by ashish)
        //mits 35925 end
 //MITS:34276 Entity Address Type START 
        [ExtendedTypeAttribute(RMExtType.Code, "ADDRESS_TYPE")]
        public int AddressType { get { return GetFieldInt("ADDRESS_TYPE_CODE"); } set { SetField("ADDRESS_TYPE_CODE", value); } }
        //MITS:34276 Entity Address Type END 
        #endregion
		//Ashish Ahuja Address Master RMA-8753 START
        private string[,] sChildren = {
                                      { "Address", "Address"} 
                                      };
		//Ashish Ahuja Address Master RMA-8753 END
        internal EntityXAddresses(bool isLocked, Context context): base(isLocked, context)
        {
            this.Initialize();
        }
       new private void Initialize()
		{

            this.m_sTableName = "ENTITY_X_ADDRESSES";
            this.m_sKeyField = "ROW_ID";//RMA-8753 nshah28(Added by ashish)
			this.m_sFilterClause = "";

			//Add all object Fields into the Field Collection from our Field List.
			base.InitFields(sFields);
            //Add all object Children into the Children	collection from	our	"sChildren"	list.
            base.InitChildren(sChildren);//RMA-8753 nshah28(Added by ashish)
			this.m_sParentClassName = "ENTITY";
			base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

		}
       #region Child Implementation

       //private string[,] sChildren = {{"AddressXPhoneInfoList","AddressXPhoneInfoList"}
       //                              };
       #endregion
				//Ashish Ahuja Address Master RMA-8753 START
        public Address Address
        {
            get
            {
                Address objItem = base.m_Children["Address"] as Address;
                if ((objItem as IDataModel).IsStale)
                    objItem = this.CreatableMoveChildTo("Address", (objItem as DataObject), this.AddressId) as Address;
                return objItem;
            }
        }
        internal override void LoadData(Riskmaster.Db.DbReader objReader)
        {
            base.LoadData(objReader);
            this.SetFieldAndNavTo("ADDRESS_ID", this.AddressId, "Address", true);
        }
        //RMA-8753 nshah28(Added by ashish)
        internal override void OnChildPreSave(string childName, IDataModel childValue)
        {
            switch (childName)
            { //RMA-8753(Adding a condition)

                case "Address":
                    if ((childValue as Address).Isdupeoverride != "IsSaveDup") // RMA-8753 for Dup popup(RMA-14262) nshah28
                    {
                        this.AddressId = Riskmaster.Common.CommonFunctions.CheckAddressDuplication(this.Address.SearchString, this.Context.DbConn.ConnectionString, this.Context.ClientId, this.Context.DbTrans);
                        this.Address.AddressId = this.AddressId;
                    }
                    if (this.AddressId == 0)
                    {
                        (childValue as Address).Save();
                        this.AddressId = (childValue as Address).AddressId;
                    }

                    break;
            }
        }
        internal override void OnChildPostSave(string childName, IDataModel childValue, bool isNew)
        {
            switch (childName)
            {
                case "Address":
                  /*  if (this.AddressId == 0)
                    {
                    (childValue as Address).Save();
                    this.AddressId = (childValue as Address).AddressId;
                    } */
                    break;
            }
        }
				//Ashish Ahuja Address Master RMA-8753 END
		//RMA-8753 nshah28(Added by ashish)
        internal override void OnChildDelete(string childName, IDataModel childValue)
        {
            if (childName == "Address")
                return;
            base.OnChildDelete(childName, childValue);
        }
		//RMA-8753 nshah28(Added by ashish)
       //override internal void OnChildInit(string childName, string childType)
       //{
       //    //Do default per-child processing.
       //    base.OnChildInit(childName, childType);


       //}
        // This	is where for collection	items, the foreign key values on new items 
		// can be set by the parent	at the time	of collection membership.
       //internal override void OnChildItemAdded(string childName, IDataModel itemValue)
       //{
       //    switch (childName)
       //    {
       //        case "AddressXPhoneInfoList":
       //            itemValue.Parent = this;
       //            (itemValue as AddressXPhoneInfo).ContactId = this.ContactId;
       //            break;
       //    }
       //    base.OnChildItemAdded(childName, itemValue);
       //}
       //internal override void OnChildPostSave(string childName, IDataModel childValue, bool isNew)
       //{
       //    switch (childName)
       //    {
       //        case "AddressXPhoneInfoList":
       //            foreach (AddressXPhoneInfo itemValue in (childValue as DataCollection))
       //                (itemValue as AddressXPhoneInfo).ContactId = this.ContactId;
       //            break;
       //    }

       //    base.OnChildPostSave(childName, childValue, isNew);
       //}
       //public AddressXPhoneInfoList AddressXPhoneInfoList
       //{
       //    get
       //    {
       //        AddressXPhoneInfoList objList = base.m_Children["AddressXPhoneInfoList"] as AddressXPhoneInfoList;
       //        if (!base.IsNew && (objList as IDataModel).IsStale)
       //            objList.SQLFilter = this.KeyFieldName + "=" + this.KeyFieldValue;
       //        return objList;
       //    }
       //}		
    }
}
