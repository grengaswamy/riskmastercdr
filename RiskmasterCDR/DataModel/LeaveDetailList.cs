using System;

namespace Riskmaster.DataModel
{
	/// <summary>
    /// Summary description for LeaveDetailList
	/// </summary>
	public class LeaveDetailList : DataCollection
	{
		internal LeaveDetailList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"LD_ROW_ID";
			this.SQLFromTable =	"CLAIM_LV_X_DETAIL";			
			this.TypeName = "LeaveDetail";
		}
		public new LeaveDetail this[int keyValue]{get{return base[keyValue] as LeaveDetail;}}
		public new LeaveDetail AddNew(){return base.AddNew() as LeaveDetail;}
		public  LeaveDetail Add(LeaveDetail obj){return base.Add(obj) as LeaveDetail;}
		public new LeaveDetail Add(int keyValue){return base.Add(keyValue) as LeaveDetail;}
		
		
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}