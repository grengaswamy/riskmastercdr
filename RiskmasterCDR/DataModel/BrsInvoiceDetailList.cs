
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forInvoiceDetailList.
	/// </summary>
	public class InvoiceDetailList : DataCollection
	{
		internal InvoiceDetailList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"INVOICE_DETAIL_ID";
			this.SQLFromTable =	"INVOICE_DETAIL";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "BrsInvoiceDetail";
		}
		public new BrsInvoiceDetail this[int keyValue]{get{return base[keyValue] as BrsInvoiceDetail;}}
		public new BrsInvoiceDetail AddNew(){return base.AddNew() as BrsInvoiceDetail;}
		public  BrsInvoiceDetail Add(BrsInvoiceDetail obj){return base.Add(obj) as BrsInvoiceDetail;}
		public new BrsInvoiceDetail Add(int keyValue){return base.Add(keyValue) as BrsInvoiceDetail;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}