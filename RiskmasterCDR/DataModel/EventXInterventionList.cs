using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description for EventXDatedTextList.
	/// </summary>
	public class EventXInterventionList:DataCollection
	{
		internal EventXInterventionList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"INTERVENT_ROW_ID";
			this.SQLFromTable =	"EVENT_X_INTERVENT";
			this.TypeName = "EventXIntervention";
		}
		public new EventXIntervention this[int keyValue]{get{return base[keyValue] as EventXIntervention;}}
		public new EventXIntervention AddNew(){return base.AddNew() as EventXIntervention;}
		public  EventXIntervention Add(EventXIntervention obj){return base.Add(obj) as EventXIntervention;}
		public new EventXIntervention Add(int keyValue){return base.Add(keyValue) as EventXIntervention;}
		
//		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}
