﻿using System;

namespace Riskmaster.DataModel
{
    public class OrgHierarchy : Entity
    {
        public int OrgHierarchyId
        {
            get
            {
                return this.EntityId;
            }
            set
            {
                this.EntityId = value;
            }
        }

        internal OrgHierarchy(bool isLocked, Context context): base(isLocked, context)
	    {
            ;
	    }
    }
}
