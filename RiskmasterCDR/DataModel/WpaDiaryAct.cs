
using System;

namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary of WpaDiaryAct AutoGenerated Class.
	/// Don't forget to add children and custom override behaviors if necessary.
	/// </summary>
	[Riskmaster.DataModel.Summary("WPA_DIARY_ACT","ACTIVITY_ID", "ActText")]
	public class WpaDiaryAct  : DataObject	
	{
		#region Database Field List
		private string[,] sFields = {
														{"ActivityId", "ACTIVITY_ID"},
														{"ParentEntryId", "PARENT_ENTRY_ID"},
														{"ActCode", "ACT_CODE"},
														{"ActText", "ACT_TEXT"},
                                                        {"LegacyUniqueIdentifier","LEGACY_UNIQUE_IDENTIFIER"}
		};
        public string LegacyUniqueIdentifier { get { return GetFieldString("LEGACY_UNIQUE_IDENTIFIER"); } set { SetField("LEGACY_UNIQUE_IDENTIFIER", value); } }
		public int ActivityId{get{return GetFieldInt("ACTIVITY_ID");}set{SetField("ACTIVITY_ID",value);}}
		public double ParentEntryId{get{return GetFieldDouble("PARENT_ENTRY_ID");}set{SetField("PARENT_ENTRY_ID",value);}}
        [ExtendedTypeAttribute(RMExtType.Code,"WPA_ACTIVITIES")]
        public int ActCode{get{return GetFieldInt("ACT_CODE");}set{SetField("ACT_CODE",value);}}
		public string ActText{get{return GetFieldString("ACT_TEXT");}set{SetField("ACT_TEXT",value);}}
		#endregion
		

		internal WpaDiaryAct(bool isLocked,Context context):base(isLocked, context)
		{
			this.Initialize();
		}
		new private void Initialize()
		{

			// base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

			this.m_sTableName = "WPA_DIARY_ACT";
			this.m_sKeyField = "ACTIVITY_ID";
			this.m_sFilterClause = "";

			//Add all obect Fields into the Field Collection from our Field List.
			base.InitFields(sFields);
			//Add all object Children into the Children collection from our "sChildren" list.
			//			base.InitChildren(sChildren);
			this.m_sParentClassName = "";
			base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

		}
	}
}