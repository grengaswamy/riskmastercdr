﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("UNIT_STAT", "UNIT_STAT_ROW_ID")]
    public class UnitStat : DataObject
    {
        #region Database Field List
        private string[,] sFields = {
														{"UnitStatRowId", "UNIT_STAT_ROW_ID"},
														{"ClaimId", "CLAIM_ID"},
														{"FraudClaimInd", "FRAUD_CLAIM_IND"},
														{"SettlementMethod", "SETTLEMENT_METHOD"},
														{"ReserveTypeCode", "RESERVE_TYPE"},
														{"MgndCareOrgTypeCode", "MGND_CARE_ORG_TYPE"},
														{"AnnuityPurchAmt", "ANNUITY_PURCH_AMT"},
														{"TotGrossIncurred", "TOT_GROSS_INCURRED"},
														{"CatastropheNBR", "CATASTROPHE_NBR"},
														{"DateAttyFormRecvd", "DATE_ATTY_FORM_RECVD"},
														{"AttyAuthRep", "ATTY_AUTH_REP"},
    													{"DisputedCaseFlag", "DISPUTED_CASE_FLAG"},
                                                        {"LumpSumInd", "LUMP_SUM_IND"},
                                                        //added by swati JIRA#5497
                                                        {"NcciLossTypeLossCode", "NCCI_LOSS_TYPE_LOSS_CODE"}, 
                                                        {"NcciLossTypeRecovCode", "NCCI_LOSS_TYPE_RECOV_CODE"}, 
                                                        {"ImpairmentPercentCode","IMP_PERCNT_BASIS_CODE"},
                                                        {"MedExtinguishInd","MED_EXTIN_IND_FLAG"},
                                                        {"ReturnToWorkInd","RTW_WORK_PAY_FLAG"},
                                                        {"LossEventClmInd","LOSS_EVT_CLM_FLAG"},
                                                        {"DciValLvlCode","DCI_VAL_LEVEL_CODE"},
                                                        //change end here by swati - JIRA#5497
                                                        {"PrevWCFlag", "PREV_WC_FLAG"},
                                                        {"PrevWCDate", "PREV_WC_DATE"},
                                                        {"PrevWCDesc", "PREV_WC_DESC"},                                                
		};

        
        public int UnitStatRowId { get { return GetFieldInt("UNIT_STAT_ROW_ID"); } set { SetField("UNIT_STAT_ROW_ID", value); } }
        public int ClaimId { get { return GetFieldInt("CLAIM_ID"); } set { SetField("CLAIM_ID", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "FRAUD_CLAIM_IND")]
        public int FraudClaimInd { get { return GetFieldInt("FRAUD_CLAIM_IND"); } set { SetField("FRAUD_CLAIM_IND", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "SETTLEMENT_METHOD")]
        public int SettlementMethod { get { return GetFieldInt("SETTLEMENT_METHOD"); } set { SetField("SETTLEMENT_METHOD", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "UNIT_STAT_RES_TYPE")]
        public int ReserveTypeCode { get { return GetFieldInt("RESERVE_TYPE"); } set { SetField("RESERVE_TYPE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "MGND_CARE_ORG_TYPE")]
        public int MgndCareOrgTypeCode { get { return GetFieldInt("MGND_CARE_ORG_TYPE"); } set { SetField("MGND_CARE_ORG_TYPE", value); } }
        //added by swati - JIRA#5497
        [ExtendedTypeAttribute(RMExtType.Code, "NCCI_VAL_LEVEL")]
        public int DciValLvlCode { get { return GetFieldInt("DCI_VAL_LEVEL_CODE"); } set { SetField("DCI_VAL_LEVEL_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "NCCI_CLAIM_STATUS")]
        public int NcciLossTypeLossCode { get { return GetFieldInt("NCCI_LOSS_TYPE_LOSS_CODE"); } set { SetField("NCCI_LOSS_TYPE_LOSS_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "NCCI_RECOV_CODE")]
        public int NcciLossTypeRecovCode { get { return GetFieldInt("NCCI_LOSS_TYPE_RECOV_CODE"); } set { SetField("NCCI_LOSS_TYPE_RECOV_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "NCCI_IMPAIRMENT")]
        public int ImpairmentPercentCode { get { return GetFieldInt("IMP_PERCNT_BASIS_CODE"); } set { SetField("IMP_PERCNT_BASIS_CODE", value); } }
        public bool MedExtinguishInd { get { return GetFieldBool("MED_EXTIN_IND_FLAG"); } set { SetField("MED_EXTIN_IND_FLAG", value); } }
        public bool ReturnToWorkInd { get { return GetFieldBool("RTW_WORK_PAY_FLAG"); } set { SetField("RTW_WORK_PAY_FLAG", value); } }
        public bool LossEventClmInd { get { return GetFieldBool("LOSS_EVT_CLM_FLAG"); } set { SetField("LOSS_EVT_CLM_FLAG", value); } } 
        //change end here by swati - JIRA#5497
        public double AnnuityPurchAmt { get { return GetFieldDouble("ANNUITY_PURCH_AMT"); } set { SetField("ANNUITY_PURCH_AMT", value); } }
        public double TotGrossIncurred { get { return GetFieldDouble("TOT_GROSS_INCURRED"); } set { SetField("TOT_GROSS_INCURRED", value); } }        

        public string CatastropheNBR { get { return GetFieldString("CATASTROPHE_NBR"); } set { SetField("CATASTROPHE_NBR", value); } }
        public string DateAttyFormRecvd { get { return GetFieldString("DATE_ATTY_FORM_RECVD"); } set { SetField("DATE_ATTY_FORM_RECVD",Riskmaster.Common.Conversion.GetDate(value)); } }
        public bool AttyAuthRep { get { return GetFieldBool("ATTY_AUTH_REP"); } set { SetField("ATTY_AUTH_REP", value); } }

        public bool DisputedCaseFlag { get { return GetFieldBool("DISPUTED_CASE_FLAG"); } set { SetField("DISPUTED_CASE_FLAG", value); } }
        public bool LumpSumInd { get { return GetFieldBool("LUMP_SUM_IND"); } set { SetField("LUMP_SUM_IND", value); } }
        public bool PrevWCFlag { get { return GetFieldBool("PREV_WC_FLAG"); } set { SetField("PREV_WC_FLAG", value); } }

        public string PrevWCDate { get { return GetFieldString("PREV_WC_DATE"); } set { SetField("PREV_WC_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        public string PrevWCDesc { get { return GetFieldString("PREV_WC_DESC"); } set { SetField("PREV_WC_DESC", value); } }        
        
        #endregion
		

        internal UnitStat(bool isLocked,Context context):base(isLocked, context)
		{
			this.Initialize();
		}
		new private void Initialize()
		{

			// base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

			this.m_sTableName = "UNIT_STAT";
			this.m_sKeyField = "UNIT_STAT_ROW_ID";
			this.m_sFilterClause = "";

			//Add all obect Fields into the Field Collection from our Field List.
			base.InitFields(sFields);
			//Add all object Children into the Children collection from our "sChildren" list.
			//base.InitChildren(sChildren);
			this.m_sParentClassName = "Claim";
			base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

		}

    }
}
