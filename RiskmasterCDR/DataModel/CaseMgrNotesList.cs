using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forCaseMgrNotesList.
	/// </summary>
	public class CaseMgrNotesList : DataCollection
	{
		internal CaseMgrNotesList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"CMGR_NOTES_ROW_ID";
			this.SQLFromTable =	"CASE_MGR_NOTES";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "CaseMgrNotes";
		}
		public new CaseMgrNotes this[int keyValue]{get{return base[keyValue] as CaseMgrNotes;}}
		public new CaseMgrNotes AddNew(){return base.AddNew() as CaseMgrNotes;}
		public  CaseMgrNotes Add(CaseMgrNotes obj){return base.Add(obj) as CaseMgrNotes;}
		public new CaseMgrNotes Add(int keyValue){return base.Add(keyValue) as CaseMgrNotes;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}