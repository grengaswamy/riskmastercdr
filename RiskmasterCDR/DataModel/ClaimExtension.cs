﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Riskmaster.DataModel
{
    /// <summary>
    /// Claim Extension class extends claim class to make claim compatible with PDS forms.
    /// </summary>
    public class ClaimExtension : Claim
    {
        #region Fields

        private ClaimAdjuster m_ClaimAdjuster  = null;

        private ClaimAWW m_ClaimAWW = null;

        private Claimant m_Claimant = null;

        #endregion
        #region Constructor
        internal ClaimExtension(bool isLocked, Context context)
            : base(isLocked, context)
        {
        }
        #endregion

        /// <summary>
        /// Gets the claimant.
        /// </summary>
        /// <value>
        /// The claimant.
        /// </value>
        //public Claimant Claimant
        //{
        //    get { return base.PrimaryClaimant; }
        //}

        /// <summary>
        /// Gets the defendant.
        /// </summary>
        /// <value>
        /// The defendant.
        /// </value>
        public Defendant Defendant
        {
            get 
            { 
                Defendant objDefendant = base.DefendantList.Cast<Defendant>().FirstOrDefault(); 
                if(object.ReferenceEquals(objDefendant, default(Defendant)))
                {
                    objDefendant = Context.Factory.GetDataModelObject("Defendant", false) as Defendant;
                }

                return objDefendant;
            }
        }

        /// <summary>
        /// Gets the claim adjuster.
        /// </summary>
        /// <value>
        /// The claim adjuster.
        /// </value>
        public ClaimAdjuster ClaimAdjuster
        {
            //get { return base.CurrentAdjuster; }
            get
            {
                if (object.ReferenceEquals(m_ClaimAdjuster, null))
                {
                    this.m_ClaimAdjuster = this.Context.Factory.GetDataModelObject("ClaimAdjuster", false) as ClaimAdjuster;
                    this.m_ClaimAdjuster.Parent = this;
                    this.m_ClaimAdjuster.ClaimId = this.ClaimId;
                    if (this.ClaimId != 0)
                    {
                        INavigation pINav = this.m_ClaimAdjuster;
                        pINav.Filter = string.Format("{0} = {1}", this.KeyFieldName, this.ClaimId);
                        pINav.MoveFirst();
                        if (base.AdjusterList.Count > 0 && !this.m_ClaimAdjuster.CurrentAdjFlag)
                        {
                            //int currentAdjusterId = base.AdjusterList.Cast<ClaimAdjuster>().ToList().FirstOrDefault(u => u.CurrentAdjFlag).AdjRowId;

                            ClaimAdjuster obj = base.AdjusterList.Cast<ClaimAdjuster>().ToList().FirstOrDefault(x => x.CurrentAdjFlag);

                            int currentAdjusterId = obj != null ? obj.AdjRowId : default(int);

                            if (currentAdjusterId != default(int))
                            {
                                pINav.MoveTo(currentAdjusterId);
                            }
                        }
                    }
                }
                return m_ClaimAdjuster;
            }
        }

        //public ClaimAWW ClaimAWW
        //{
        //    get 
        //    {
        //        if (object.ReferenceEquals(m_ClaimAWW, null))
        //        {
        //            this.m_ClaimAWW = this.Context.Factory.GetDataModelObject("ClaimAWW", false) as ClaimAWW;
        //            this.m_ClaimAWW.Parent = this;

        //            if (this.ClaimId != 0)
        //            {
        //                this.m_ClaimAWW.MoveTo(this.ClaimId);
        //            }
        //        }
        //        return m_ClaimAWW;
        //    }
            
        //}


        public Claimant Claimant
        {
            //get { return base.CurrentAdjuster; }
            get
            {
                if (object.ReferenceEquals(m_Claimant, null))
                {
                    this.m_Claimant = this.Context.Factory.GetDataModelObject("Claimant", false) as Claimant;
                    this.m_Claimant.Parent = this;
                    this.m_Claimant.ClaimId = this.ClaimId;
                    if (this.ClaimId != 0)
                    {
                        INavigation pINav = this.m_Claimant;
                        pINav.Filter = string.Format("{0} = {1}", this.KeyFieldName, this.ClaimId);
                        pINav.MoveFirst();
                    }
                }
                return m_Claimant;
            }
        }

        public ClaimXLitigation ClaimXLitigation
        {
            get 
            {
                ClaimXLitigation objClaimXLitigation = base.LitigationList.Cast<ClaimXLitigation>().FirstOrDefault();
                if(object.ReferenceEquals(objClaimXLitigation, default(ClaimXLitigation)))
                {
                    objClaimXLitigation = base.Context.Factory.GetDataModelObject("ClaimXLitigation", false) as ClaimXLitigation;
                }

                return objClaimXLitigation;
            }
        }


    }
}
