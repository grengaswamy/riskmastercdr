using System;
using System.Reflection;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.DataModel
{
	/// <summary>
	/// DataLazyLoader wraps an object and any information it will  take to successfully 
	/// create and initialize that object on demand.
	/// </summary>
	internal class DataLazyLoader : DataRoot, IDataModel
	{
		[Flags()]
		internal  enum eLoaderStatus:int
		{
			EMPTY = 0,
			TYPE_SELECTED = 2,
            RECORD_KEY_SELECTED = 4,
			PARENT_SELECTED = 8,
			INSTANCE_SELECTED = 16,
			FULLY_CACHED = 32,
			SQL_TEMPLATE_SELECTED = 64
		}
		DataObject m_anyObject = null;
		Type m_anyType = null;
		object m_keyValue = null;
		eLoaderStatus m_status = 0;
		string m_SQLTemplate = ""; //Used to populate/save DataSimpleList types.
/*
		internal DataLazyLoader(DataObject targetValue,bool isLocked):base(isLocked)
		{
			Initialize( targetValue,null,null);
		}
		internal DataLazyLoader(DataObject targetValue):base(true)
		{
			Initialize( targetValue,null,null);
		}

		internal DataLazyLoader(string  targetTypeName):base(true)
		{
			m_anyType = Type.GetType("Riskmaster.DataModel." + targetTypeName ,true);
			m_status = eLoaderStatus.TYPE_SELECTED;
		}

		internal DataLazyLoader(DataObject targetValue, object recordKeyValue):base(true)
		{
			Initialize( targetValue,recordKeyValue,null);
		}
		internal DataLazyLoader(DataObject targetValue, DataObject parentObject):base(true)
		{
			Initialize( targetValue,null,parentObject);
		}
		internal DataLazyLoader(DataObject targetValue, object recordKeyValue, DataObject parentObject):base(true)
		{
			Initialize( targetValue,recordKeyValue,parentObject);
		}	
	
		*/
		internal DataLazyLoader(bool isLocked, Context context):base(isLocked, context)
		{
		}/*		
		internal DataLazyLoader(string  targetTypeName, object recordKeyValue, DataObject parentObject, bool isLocked, Context context):base(isLocked, context)
		{
			Initialize( null,recordKeyValue,parentObject);
			m_anyType = Type.GetType("Riskmaster.DataModel." + targetTypeName ,true);
			m_status |= eLoaderStatus.TYPE_SELECTED;
		}
		internal DataLazyLoader(string  targetTypeName, object recordKeyValue, string SQLTemplate,  DataObject parentObject):base(true)
		{
			Initialize( null,recordKeyValue,parentObject);
			m_anyType = Type.GetType("Riskmaster.DataModel." + targetTypeName ,true);
			m_status |= eLoaderStatus.TYPE_SELECTED;
			if(SQLTemplate !="")
			{
					m_SQLTemplate = SQLTemplate;
					m_status |= eLoaderStatus.SQL_TEMPLATE_SELECTED;
			}
		}
*/
		internal void Initialize(DataObject  targetValue, object recordKeyValue, DataObject parentObject)
		{

			if(m_status != 0)
				return;

			m_status = 0;
			if(targetValue !=null)
			{	
				m_anyType =  targetValue.GetType();
				m_status |= eLoaderStatus.TYPE_SELECTED;
				m_anyObject = targetValue;
				m_status |= eLoaderStatus.INSTANCE_SELECTED;
			}
			if(parentObject !=null)
			{
				base.Parent = parentObject;
				m_status |= eLoaderStatus.PARENT_SELECTED;
			}
			if(recordKeyValue !=null)
			{	
				m_keyValue = recordKeyValue;
				m_status |= eLoaderStatus.RECORD_KEY_SELECTED;
			}
		}

		internal object KeyValue
		{
			get{return m_keyValue;}
			set
			{
				if(! base.Locked)
				{
						m_keyValue = value;
						m_status |= eLoaderStatus.RECORD_KEY_SELECTED;
				}
			}
		}

		internal eLoaderStatus Status
		{
			get{return m_status;}
			set{m_status= value;}
		}
		
		
		internal DataObject Instance
		{
			get
			{
				if((m_status & eLoaderStatus.FULLY_CACHED)!=0)
					return m_anyObject;

				// Check for Error Conditions...
				if(m_status ==  eLoaderStatus.EMPTY)
					throw new RMAppException(Globalization.GetString("DataLazyLoader.Instance.EmptyException")); 
				else if( (m_status & eLoaderStatus.RECORD_KEY_SELECTED)==0)
					throw new RMAppException(Globalization.GetString("DataLazyLoader.Instance.MissingRecordKeyException")); 
				else if( (m_status & eLoaderStatus.TYPE_SELECTED)==0)
					throw new RMAppException(Globalization.GetString("DataLazyLoader.Instance.MissingTypeException")); 
				else if(((m_status & eLoaderStatus.SQL_TEMPLATE_SELECTED)==0) && (m_anyType.Name.IndexOf("DataSimpleList")>=0))
					throw new RMAppException(Globalization.GetString("DataLazyLoader.Instance.MissingSQLTemplateException"));
					
				//We have enough info
				if( (m_status & eLoaderStatus.INSTANCE_SELECTED)==0) //Need an instance of inner object.
				{
					string s = m_anyType.Name;
					s = s.Substring(s.LastIndexOf(".") +1);
					m_anyObject = base.Context.Factory.GetDataModelObject(s,true) as DataObject;
					m_status |= eLoaderStatus.INSTANCE_SELECTED;
				}

				if((m_status & eLoaderStatus.PARENT_SELECTED)!=0) 
					if(base.Parent != null)
						m_anyObject.Parent =base.Parent;
							
				m_anyObject.MoveTo((int)m_keyValue);
				m_status = eLoaderStatus.FULLY_CACHED;
				return m_anyObject;
			}	
				
		}
	}
}
