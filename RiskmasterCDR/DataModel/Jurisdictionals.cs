using System;
using System.Xml;
using System.Collections;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using Riskmaster.Db;
using Riskmaster.Scripting;

namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description for Supplementals.
	/// </summary>
	public class Jurisdictionals :SupplementalObject , IDataModel, IPersistence, IEnumerable
	{

		internal  Jurisdictionals(bool isLocked, Context context) : base(isLocked, context){Initialize();}
		private void Initialize(){;}
		override protected string OnBuildSuppSectionName(){return "jurisgroup";}
		override protected string OnBuildSuppSectionTitle(){return "Jurisdictionals";}
	}
}
