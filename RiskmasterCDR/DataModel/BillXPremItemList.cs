
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forBillXPremItemList.
	/// </summary>
	public class BillXPremItemList : DataCollection
	{
		internal BillXPremItemList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"";
			this.SQLFromTable =	"BILL_X_PREM_ITEM";
//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "BillXPremItem";
		}
		public new BillXPremItem this[int keyValue]{get{return base[keyValue] as BillXPremItem;}}
		public new BillXPremItem AddNew(){return base.AddNew() as BillXPremItem;}
		public  BillXPremItem Add(BillXPremItem obj){return base.Add(obj) as BillXPremItem;}
		public new BillXPremItem Add(int keyValue){return base.Add(keyValue) as BillXPremItem;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}