﻿using System;

namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("SITE_UNIT_STGNG", "SITE_ID", "SiteNumber")]
    public class SiteUnitStgng:DataObject
    {
         #region Database Field List
        private string[,] sFields = {
                                        {"SiteId", "SITE_ID"},
                                        {"SiteNumber", "SITE_NUMBER"},
                                        {"Name", "NAME"},
                                        {"Optional", "OPTIONAL"},
                                        {"Adsress1", "ADDR1"},
                                        {"Adsress2", "ADDR2"},
                                        {"StateId", "STATE_ID"},
                                        {"City", "CITY"},
                                        {"ZipCode", "ZIP_CODE"},
                                        {"CountryId", "COUNTRY_ID"},
                                        {"PhoneNumber", "PHONE_NUMBER"},
                                        {"Contact", "CONTACT"},
                                        {"FEIN", "FEIN"},
                                        {"SIC", "SIC"},   
                                        {"Status", "STATUS"},
                                        {"TaxLocation", "TAX_LOCATION"},
                                        {"AuditBasis", "AUDIT_BASIS"},
                                        {"AuditType", "AUDIT_TYPE"},
                                        {"InterimAuditor", "INTERIM_AUDIT"},
                                        {"CheckAuditor", "CHECK_AUDIT"},
                                        {"FinalAuditor", "FINAL_AUDIT"},
                                        {"UnemplymntNum", "UNEMPLMYNT_NUM"},
                                        {"NumOfEmp", "NUM_OF_EMP"},
                                        {"GvrngClsCode", "GVRNG_CLS_CODE"},
                                        {"SeqNum", "SEQ_NUM"},
                                        {"GvrngClsDesc", "GVRNG_CLS_DESC"},
                                        {"DttmRcdAdded", "DTTM_RCD_ADDED"},
                                        {"DttmRcdLastUpd", "DTTM_RCD_LAST_UPD"},
                                        {"AddedByUser", "ADDED_BY_USER"},
                                        {"UpdatedByUser", "UPDATED_BY_USER"}  
                                    };

        public int SiteId { get { return GetFieldInt("SITE_ID"); } set { SetField("SITE_ID", value); } }
        public string SiteNumber { get { return GetFieldString("SITE_NUMBER"); } set { SetField("SITE_NUMBER", value); } }
        public string Name { get { return GetFieldString("NAME"); } set { SetField("NAME", value); } }
        public string Optional { get { return GetFieldString("OPTIONAL"); } set { SetField("OPTIONAL", value); } }
        public string Adsress1 { get { return GetFieldString("ADDR1"); } set { SetField("ADDR1", value); } }
        public string Adsress2 { get { return GetFieldString("ADDR2"); } set { SetField("ADDR2", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "STATES")]
        public string StateId { get { return GetFieldString("STATE_ID"); } set { SetField("STATE_ID", value); } }
        public string City { get { return GetFieldString("CITY"); } set { SetField("CITY", value); } }
        public string ZipCode { get { return GetFieldString("ZIP_CODE"); } set { SetField("ZIP_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "COUNTRY")]
        public string CountryId { get { return GetFieldString("COUNTRY_ID"); } set { SetField("COUNTRY_ID", value); } }
        public string PhoneNumber { get { return GetFieldString("PHONE_NUMBER"); } set { SetField("PHONE_NUMBER", value); } }
        public string Contact { get { return GetFieldString("CONTACT"); } set { SetField("CONTACT", value); } }
        public string FEIN { get { return GetFieldString("FEIN"); } set { SetField("FEIN", value); } }
        public string SIC { get { return GetFieldString("SIC"); } set { SetField("SIC", value); } }
        // Pradyumna 1/14/2014 - Fields Added fot Inquiry Screens - Start
        public string Status { get { return GetFieldString("STATUS"); } set { SetField("STATUS", value); } }
        public string TaxLocation { get { return GetFieldString("TAX_LOCATION"); } set { SetField("TAX_LOCATION", value); } }
        public string AuditBasis { get { return GetFieldString("AUDIT_BASIS"); } set { SetField("AUDIT_BASIS", value); } }
        public string AuditType { get { return GetFieldString("AUDIT_TYPE"); } set { SetField("AUDIT_TYPE", value); } }
        public string InterimAuditor { get { return GetFieldString("INTERIM_AUDIT"); } set { SetField("INTERIM_AUDIT", value); } }
        public string CheckAuditor { get { return GetFieldString("CHECK_AUDIT"); } set { SetField("CHECK_AUDIT", value); } }
        public string FinalAuditor { get { return GetFieldString("FINAL_AUDIT"); } set { SetField("FINAL_AUDIT", value); } }
        public string UnemplymntNum { get { return GetFieldString("UNEMPLMYNT_NUM"); } set { SetField("UNEMPLMYNT_NUM", value); } }
        public string NumOfEmp { get { return GetFieldString("NUM_OF_EMP"); } set { SetField("NUM_OF_EMP", value); } }
        public string GvrngClsCode { get { return GetFieldString("GVRNG_CLS_CODE"); } set { SetField("GVRNG_CLS_CODE", value); } }
        public string SeqNum { get { return GetFieldString("SEQ_NUM"); } set { SetField("SEQ_NUM", value); } }
        public string GvrngClsDesc { get { return GetFieldString("GVRNG_CLS_DESC"); } set { SetField("GVRNG_CLS_DESC", value); } }
        // Pradyumna 1/14/2014 - Fields Added fot Inquiry Screens - Ends
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }

        #endregion

         #region Child Implementation

        //Handle adding the our key to any additions to the record collection properties.
        internal override void OnChildItemAdded(string childName, IDataModel itemValue)
        {
            base.OnChildItemAdded(childName, itemValue);
        }

        //Handle child saves.  Set the current key into the children since this could be a new record.
        override internal void OnChildPostSave(string childName, IDataModel childValue, bool isNew)
        {
            base.OnChildPostSave(childName, childValue, isNew);
        }
        #endregion

        internal SiteUnitStgng(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();
        }

        new private void Initialize()
        {
            this.m_sTableName = "SITE_UNIT_STGNG";
            this.m_sKeyField = "SITE_ID";
            this.m_sFilterClause = string.Empty;
            //Add all object Fields into the Field Collection from our Field List.
            base.InitFields(sFields);
            this.m_sParentClassName = string.Empty;
            //Moved after most init logic so that scripting can be called successfully.
            base.Initialize();
        }

        internal override string OnBuildDeleteSQL()
        {
            return "UPDATE SITE_UNIT_STGNG SET DELETED_FLAG = -1 WHERE SITE_ID=" + this.SiteId;
        }

        internal override bool OnSuppDelete()
        {
            return false;
        }

       
        internal override void OnChildDelete(string childName, IDataModel childValue)
        {
            return;
        }

        internal override string OnApplyFilterClauseSQL(string CurrentSQL)
        {
            string sExtendedFilterClause = this.m_sFilterClause;
            string sSQL = CurrentSQL;
            if (!string.IsNullOrEmpty(sExtendedFilterClause))
            {
                sExtendedFilterClause += " AND ";
            }
            sExtendedFilterClause += "DELETED_FLAG=0";

            if (!string.IsNullOrEmpty(sExtendedFilterClause))
            {
                if (sSQL.IndexOf(" WHERE ") > 0)
                {
                    sSQL += " AND (" + sExtendedFilterClause + ")";
                }
                else
                {
                    sSQL += " WHERE " + sExtendedFilterClause;
                }
            }
            return sSQL;
        }

        protected override void OnBuildNewUniqueId()
        {
            base.OnBuildNewUniqueId();
            if (this.SiteNumber == "")
            {
                this.SiteNumber = GetSiteNumber();
            }
        }

        private string GetSiteNumber()
        {
            int iPin = this.Context.GetNextUID("SITE_NUMBERS");
            return iPin.ToString("0000000");
        }      

        //#region Supplemental Fields Exposed

        ///// <summary>
        ///// Supplementals are implemented in the base class but not exposed externally.
        /////This allows the derived class to control whether the object appears to have
        /////supplementals or not. Entity exposes Supplementals.
        ///// </summary>
        //public new Supplementals Supplementals
        //{
        //    get
        //    {
        //        return base.Supplementals;
        //    }
        //}
        //#endregion

    }
}
