
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forFundsList.
	/// </summary>
	public class FundsList : DataCollection
	{
		internal FundsList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"TRANS_ID";
			this.SQLFromTable =	"FUNDS";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "Funds";
		}
		public new Funds this[int keyValue]{get{return base[keyValue] as Funds;}}
		public new Funds AddNew(){return base.AddNew() as Funds;}
		public  Funds Add(Funds obj){return base.Add(obj) as Funds;}
		public new Funds Add(int keyValue){return base.Add(keyValue) as Funds;}
		
		internal void UpdateReserves()
		{
			foreach(Funds obj in this)
			{
				obj.UpdateReserves();
				return;
			}
			
		}
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}