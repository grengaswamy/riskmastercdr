using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forAccountList.
	/// </summary>
	public class AccountList : DataCollection
	{
		internal AccountList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"ACCOUNT_ID";
			this.SQLFromTable =	"ACCOUNT";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "Account";
		}
		public new Account this[int keyValue]{get{return base[keyValue] as Account;}}
		public new Account AddNew(){return base.AddNew() as Account;}
		public  Account Add(Account obj){return base.Add(obj) as Account;}
		public new Account Add(int keyValue){return base.Add(keyValue) as Account;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}