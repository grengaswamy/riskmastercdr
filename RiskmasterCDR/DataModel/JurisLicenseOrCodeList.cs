using System;

namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description for JurisAndLicenseCodesList.
	/// </summary>
	public class JurisLicenseOrCodeList : DataCollection
	{
		internal JurisLicenseOrCodeList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"TABLE_ROW_ID";
			this.SQLFromTable =	"ENTITY_X_CODELICEN";
			//this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "JurisLicenseOrCode";
		}
		
		public new JurisLicenseOrCode this[int keyValue]{get{return base[keyValue] as JurisLicenseOrCode;}}
		public new JurisLicenseOrCode AddNew(){return base.AddNew() as JurisLicenseOrCode;}
		public  JurisLicenseOrCode Add(JurisLicenseOrCode obj){return base.Add(obj) as JurisLicenseOrCode;}
		public new JurisLicenseOrCode Add(int keyValue){return base.Add(keyValue) as JurisLicenseOrCode;}

		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}
