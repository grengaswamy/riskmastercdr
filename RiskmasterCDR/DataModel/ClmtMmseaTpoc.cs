﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("CLMT_MMSEA_TPOC", "TPOC_ROW_ID")]
    public class ClmtMmseaTpoc : DataObject
    {
        #region Database Field List
        private string[,] sFields = {
                            {"TpocRowID","TPOC_ROW_ID"},
                            {"ClaimantRowID", "CLAIMANT_ROW_ID"},
                            {"PaymentObligationAmount","MMSEA_TPOC_AMT"},
                            {"DelayedBeyongTPOCStartDate","MMSEA_TPOC_DY_DATE"},
                            {"DateofWrittenAgreement", "AGREEMENT_DATE"},
                            {"DateofCourtApproval", "JUDGEMENT_DATE"},
                            {"DateofPaymentIssue","PAYMENT_DATE"},
                            {"AddedByUser","ADDED_BY_USER"},	
                            {"UpdatedByUser","UPDATED_BY_USER"},
                            {"DttmRcdAdded","DTTM_RCD_ADDED"},
                            {"DttmRcdLastUpd","DTTM_RCD_LAST_UPD"},
                            {"TpocEditFlag","TPOC_EDIT_FLAG"},
                            {"DeletedFlag","DELETED_FLAG"},
                            {"FilMdcreRcdDATE","FIL_MDCRE_RCD_DATE"},
                        };



        public int TpocRowID { get { return GetFieldInt("TPOC_ROW_ID"); } set { SetField("TPOC_ROW_ID", value); } }
        public int ClaimantRowID { get { return GetFieldInt("CLAIMANT_ROW_ID"); } set { SetField("CLAIMANT_ROW_ID", value); } }
        public double PaymentObligationAmount { get { return GetFieldDouble("MMSEA_TPOC_AMT"); } set { SetField("MMSEA_TPOC_AMT", value); } }
        public string DelayedBeyongTPOCStartDate { get { return GetFieldString("MMSEA_TPOC_DY_DATE"); } set { SetField("MMSEA_TPOC_DY_DATE", value); } }
        public string DateofWrittenAgreement { get { return GetFieldString("AGREEMENT_DATE"); } set { SetField("AGREEMENT_DATE", value); } }
        public string DateofCourtApproval { get { return GetFieldString("JUDGEMENT_DATE"); } set { SetField("JUDGEMENT_DATE", value); } }
        public string DateofPaymentIssue { get { return GetFieldString("PAYMENT_DATE"); } set { SetField("PAYMENT_DATE", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        public int TpocEditFlag { get { return GetFieldInt("TPOC_EDIT_FLAG"); } set { SetField("TPOC_EDIT_FLAG", value); } }
        public int DeletedFlag { get { return GetFieldInt("DELETED_FLAG"); } set { SetField("DELETED_FLAG", value); } }
        public string FilMdcreRcdDATE { get { return GetFieldString("FIL_MDCRE_RCD_DATE"); } set { SetField("FIL_MDCRE_RCD_DATE", value); } }
        
        #endregion


        internal ClmtMmseaTpoc(bool isLocked, Context context)
            : base(isLocked, context)
		{
			this.Initialize();
		}

        public override bool IsNew
        {
            get
            {
                return (this.KeyFieldValue <= 0);
            }
        }


        new private void Initialize()
        {

            // base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

            this.m_sTableName = "CLMT_MMSEA_TPOC";
            this.m_sKeyField = "TPOC_ROW_ID";
            this.m_sFilterClause = "";

            //Add all obect Fields into the Field Collection from our Field List.
            base.InitFields(sFields);
            //Add all object Children into the Children collection from our "sChildren" list.
            //			base.InitChildren(sChildren);
            this.m_sParentClassName = "ClaimantMmsea";
            base.Initialize();  //Moved after most init logic so that scripting can be called successfully.
        }
    }
}
