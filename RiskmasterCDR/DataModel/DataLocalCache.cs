/*using System;
using System.Collections;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;

namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description for LocalCache.
	/// </summary>
	internal class DataLocalCache : DataRoot
	{
		private Hashtable m_Cache = new  Hashtable(50);
		public DataLocalCache()
		{
		}
		public void Clear(){m_Cache = new Hashtable(50);}

		public int GetCodeId(string shortCode, string tableName)
		{
			string sKey = String.Format("CID_{0}_{1}",shortCode,tableName);
			try
			{
				if(m_Cache.ContainsKey(sKey))
					return	(int) m_Cache[sKey];
				else
				{
					int codeId = 0;
					string SQL = String.Format(@"SELECT CODE_ID FROM CODES,GLOSSARY 
																				WHERE GLOSSARY.SYSTEM_TABLE_NAME='{0}' AND 
																				GLOSSARY.TABLE_ID=CODES.TABLE_ID AND 
																				CODES.SHORT_CODE='{1}'",tableName,shortCode);
					codeId =  (int) Context.DbConn.ExecuteScalar(SQL);
					m_Cache.Add(sKey,codeId);
					return codeId;
				}
			}
			catch(Exception e){throw new DataModelException(Globalization.GetString("LocalCache.GetCodeId.Exception"),e);}
		}

		public int GetTableId(string tableName)
		{
			string sKey = String.Format("TID_{0}",tableName);
			try
			{
				if(m_Cache.ContainsKey(sKey))
					return	(int) m_Cache[sKey];
				else
				{
					int tableId = 0;
					string SQL = String.Format(@"SELECT TABLE_ID FROM GLOSSARY 
																				WHERE SYSTEM_TABLE_NAME ='{0}'" ,tableName);
					tableId =  (int) Context.DbConn.ExecuteScalar(SQL);
					m_Cache.Add(sKey,tableId);
					return tableId;
				}
			}
			catch(Exception e){throw new DataModelException(Globalization.GetString("LocalCache.GetTableId.Exception"),e);}
		}
	}
}*/